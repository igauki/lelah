package com.ekalife.applet.customersign;
import java.awt.BasicStroke;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.Line2D;
import java.util.ListIterator;
import java.util.Vector;

import org.jfree.chart.axis.ColorBar;



public class SignCanvas extends Canvas {

	private static final long serialVersionUID = -8693264932786151893L;
	private static final BasicStroke stroke = new BasicStroke(10.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
	private int x1 = 0, y1 = 0;
	public Vector plots = new Vector(), pdata = null;
	public String command;
	public String spaj;
	public Integer no;
	public String path;
	public String imageFormat;
	public String fileName;
	//
	public SignCanvas() {
		setBackground(Color.white);
		setForeground(Color.black);
		
		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				x1 = e.getX();
				y1 = e.getY();
				pdata = new Vector();
				pdata.add(new Point(x1, y1));
				Graphics2D g2d = (Graphics2D) getGraphics();
				g2d.setStroke(stroke);
				g2d.draw(new Line2D.Double(x1, y1, x1, y1));
				g2d.setBackground(Color.white);
				g2d.setColor(Color.black);

			}

			public void mouseReleased(MouseEvent e) {
				plots.add((Vector) pdata.clone());
			}
		});
		
		addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				int x2 = e.getX();
				int y2 = e.getY();
				pdata.add(new Point(x2, y2));
				Graphics2D g2d = (Graphics2D) getGraphics();
				g2d.setStroke(stroke);
				g2d.draw(new Line2D.Double(x1, y1, x2, y2));
				g2d.setBackground(Color.white);
				g2d.setColor(Color.black);
				x1 = x2;
				y1 = y2;
			}
		});
		
		setBackground(Color.white);
	}

	public void paint(Graphics g) {
		setBackground(Color.white);
		if(this.command!=null) {
			if(this.command.equals("Clear")) {
				super.paint(g);
			}else if(this.command.equals("Save")) {
				//tebelin
				Graphics2D g2d = (Graphics2D)(g);
				g2d.setStroke(stroke);
				g2d.setBackground(Color.white);
				
				ListIterator it = plots.listIterator();
				while (it.hasNext()) {
					Vector v = (Vector) it.next();
					Point p1 = (Point) v.get(0);
					for (int i = 1, size = v.size(); i < size; i += 2) {
						Point p2 = (Point) v.get(i);
						g2d.draw(new Line2D.Double(p1.x, p1.y, p2.x, p2.y));
						
						p1 = p2;
					}
				}
			}
		}
	}
	
}