package com.ekalife.applet.customersign;
import java.applet.Applet;
import java.awt.Button;
import java.awt.Color;
import java.awt.Graphics;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.image.RGBImageFilter
;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import org.apache.batik.ext.awt.image.GraphicsUtil;

public class SignApplet extends Applet implements ActionListener{

	private SignCanvas canvas;
	private JTextField textSpaj;
	private JTextField textNama;
	
	public void init() {
		setBackground(Color.gray);
		//Create and populate the panel.
		JPanel p = new JPanel(new SpringLayout());
		//
	    JLabel label = new JLabel("SPAJ:", JLabel.TRAILING);
	    p.add(label);
	    textSpaj = new JTextField(10);
	    label.setLabelFor(textSpaj);
	    p.add(textSpaj);	  
		//
	    label = new JLabel("Nama:", JLabel.TRAILING);
	    p.add(label);
	    textNama = new JTextField(10);
	    label.setLabelFor(textNama);
	    p.add(textNama);
		//
	    label = new JLabel("Tanda Tangan:", JLabel.TRAILING);
	    p.add(label);
		this.canvas = new SignCanvas();
		this.canvas.spaj = getParameter("spaj")==null?getParameter("spaj"):getParameter("spaj").replace(".", "");
		this.canvas.path = getParameter("path");
		//this.canvas.no = Integer.valueOf(getParameter("nono"));
		this.canvas.imageFormat = getParameter("imageFormat");
		this.canvas.fileName = getParameter("fileName");
		this.canvas.setSize(630, 420);
		this.canvas.setBackground(Color.white);
		
		
		p.add(this.canvas);
		//
	    label = new JLabel("", JLabel.TRAILING);
	    p.add(label);
		JPanel buttonBar = new JPanel();
		Button save = new Button("Save");
		save.addActionListener(this);
		save.setBackground(Color.lightGray);
		buttonBar.add(save);
		Button clear = new Button("Clear");
		clear.addActionListener(this);
		clear.setBackground(Color.lightGray);
		buttonBar.add(clear);
		p.add(buttonBar);
		//Lay out the panel.
		SpringUtilities.makeCompactGrid(p,
				4, 2,		//rows, cols
				6, 6,		//initX, initY
				6, 6);	//xPad, yPad
		//
		add(p);
	}

	public Insets getInsets() {
		return new Insets(3, 3, 3, 3);
	}

	public void actionPerformed(ActionEvent evt) {
		this.canvas.command = evt.getActionCommand();
		if (this.canvas.command.equals("Clear")) {
			this.canvas.plots.clear();
			this.canvas.repaint();
		} else if (this.canvas.command.equals("Save")) {
			
			if(this.textNama.getText().trim().equals("")) {
				JOptionPane.showMessageDialog(null, "Harap masukkan nama lengkap sesuai SPAJ pada kolom NAMA");
			}else {
				//UPLOAD IMAGE, REQUEST TO SERVLET, LALU TERIMA RESPONSENYA
				Rectangle rect = this.canvas.getBounds(); 
			
				
				Image image = createImage(rect.width, rect.height);
				
				Graphics g = image.getGraphics(); 
				
				
			
				this.canvas.paint(g);
			
				String spaj=this.textSpaj.getText();
				if(spaj.equals("")){
					spaj="XX";
				}
				//
				SignUtils.uploadImage(
						spaj, 
						this.textNama.getText(), 
						rect, 
						image, 
						g, 
						this.canvas.path, 
						this.canvas.fileName, 
						this.canvas.imageFormat);
			}
			
		}
	}

}