package com.ekalife.applet.customersign;

import java.awt.AlphaComposite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.awt.Color;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

import org.apache.batik.ext.awt.image.GraphicsUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.axis.ColorBar;

public class SignUtils {
	protected static final Log logger = LogFactory.getLog( SignUtils.class );
	
	public static final String lineEnd = "\r\n";
	public static final String twoHyphens = "--";
	public static final String boundary = "*****";
	
	public static void uploadImage(String spaj, String nama, Rectangle r, Image image, Graphics g, String path, String fileName, String imageFormat) {

		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		DataInputStream inStream = null;
		
		//resize dulu gambarnya, 1/2 ukuran aja
		image = SignUtils.createResizedCopy(image, r.width/2, r.height/2, true);
	
		

		
		try {
			// open a URL connection to the Servlet
			URL url = new URL(path+"?spaj="+spaj+"&nama="+nama.replace(" ", "%20")+"&imageFormat="+imageFormat);
			conn = (HttpURLConnection) url.openConnection();
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
			
			//1
			dos = new DataOutputStream(conn.getOutputStream());
			dos.writeBytes(twoHyphens + boundary + lineEnd);
			dos
					.writeBytes("Content-Disposition: form-data; name=\"upload\";"
							+ " filename=\""
							+ fileName + "." + imageFormat
							+ "\""
							+ lineEnd);
			dos.writeBytes(lineEnd);

			//2
			ImageIO.write((RenderedImage) image, imageFormat, dos);

			//3
			dos.writeBytes(lineEnd);
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

		} catch (MalformedURLException ex) {
			logger.info("From ServletCom CLIENT REQUEST:" + ex);
		} catch (IOException ioe) {
			logger.info("From ServletCom CLIENT REQUEST:" + ioe);
		} finally {
			// close streams
			try {
				dos.flush();
				dos.close();			
			} catch (IOException e) {
				logger.error("ERROR :", e);
			}
		}
		
		try {
			inStream = new DataInputStream(conn.getInputStream());
			String str;
			
			BufferedReader d = new BufferedReader(new InputStreamReader(inStream));
			
			while ((str = d.readLine()) != null) {
				if(str.equals("sukses")) {
					JOptionPane.showMessageDialog(null, "Tanda tangan berhasil di-upload. Silahkan pilih Tab Preview untuk melihat hasil.");
				}else if(str.equals("gagal")) {
					JOptionPane.showMessageDialog(null, "Tanda tangan gagal di-upload. Silahkan hubungi IT Department di 6257808 ext. 8105.");
				}
				logger.info("Server response is: " + str);
				logger.info("");
			}
		} catch (IOException ioex) {
			logger.info("From (ServerResponse): " + ioex);
		} finally {
			try {
				inStream.close();
			} catch (IOException e) {
				logger.error("ERROR :", e);
			}
		}
		
	}
	
	private static BufferedImage createResizedCopy(Image originalImage, int scaledWidth, int scaledHeight, boolean preserveAlpha) {
		int imageType = preserveAlpha ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
		BufferedImage scaledBI = new BufferedImage(scaledWidth, scaledHeight, imageType);
		Graphics2D g = scaledBI.createGraphics();
		if (preserveAlpha) g.setComposite(AlphaComposite.Src);
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		g.drawImage(originalImage, 0, 0, scaledWidth, scaledHeight, null);
		g.dispose();
		g.setBackground(Color.white);
		
		return scaledBI;
	}
	
}