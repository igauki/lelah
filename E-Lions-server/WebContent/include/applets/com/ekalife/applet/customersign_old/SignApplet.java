package com.ekalife.applet.customersign_old;
import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Insets;

import javax.swing.JPanel;

public class SignApplet extends Applet {

	public void init() {

		setBackground(Color.gray);
		setLayout(new BorderLayout(3, 3));

		SignCanvas canvas = new SignCanvas();
		canvas.setSpaj(getParameter("spaj"));
		canvas.setPath(getParameter("path"));
		canvas.setNo(Integer.valueOf(getParameter("nono")));
		canvas.setImageFormat(getParameter("imageFormat"));
		canvas.setFileName(getParameter("fileName"));
		add(canvas, BorderLayout.CENTER);

		JPanel buttonBar = new JPanel();
		add(buttonBar, BorderLayout.SOUTH);

		Button save = new Button("Save");
		save.addActionListener(canvas);
		save.setBackground(Color.lightGray);
		buttonBar.add(save);

		Button clear = new Button("Clear");
		clear.addActionListener(canvas);
		clear.setBackground(Color.lightGray);
		buttonBar.add(clear);

	}

	public Insets getInsets() {
		return new Insets(3, 3, 3, 3);
	} 

}