package com.ekalife.applet.customersign_old;
import java.awt.BasicStroke;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.Line2D;
import java.util.ListIterator;
import java.util.Vector;

import javax.swing.JOptionPane;

public class SignCanvas extends Canvas implements ActionListener {

	private static final long serialVersionUID = -8693264932786151893L;
	private static final BasicStroke stroke = new BasicStroke(10.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
	private int x1 = 0, y1 = 0;
	private Vector plots = new Vector(), pdata = null;
	private String command;
	private String spaj;
	private Integer no;
	private String path;
	private String imageFormat;
	private String fileName;
	//
	public void setFileName(String fileName) {this.fileName = fileName;}
	public void setImageFormat(String imageFormat) {this.imageFormat = imageFormat;}
	public void setPath(String path) {this.path = path;}
	public void setSpaj(String spaj) {this.spaj = spaj;}
	public void setNo(Integer no) {this.no = no;}	
	//
	
	public SignCanvas() {
		
		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				x1 = e.getX();
				y1 = e.getY();
				pdata = new Vector();
				pdata.add(new Point(x1, y1));
				Graphics2D g2d = (Graphics2D) getGraphics();
				g2d.setStroke(stroke);
				g2d.draw(new Line2D.Double(x1, y1, x1, y1));
				g2d.setBackground(Color.white);

			}

			public void mouseReleased(MouseEvent e) {
				plots.add((Vector) pdata.clone());
			}
		});
		
		addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				int x2 = e.getX();
				int y2 = e.getY();
				pdata.add(new Point(x2, y2));
				Graphics2D g2d = (Graphics2D) getGraphics();
				g2d.setStroke(stroke);
				g2d.draw(new Line2D.Double(x1, y1, x2, y2));
				g2d.setBackground(Color.white);
				x1 = x2;
				y1 = y2;
			}
		});
		
		setBackground(Color.white);
	}

	public void paint(Graphics g) {
		if(this.command!=null) {
			if(this.command.equals("Clear")) {
				super.paint(g);
			}else if(this.command.equals("Save")) {
				//tebelin
				Graphics2D g2d = (Graphics2D)(g);
				g2d.setStroke(stroke);
				g2d.setBackground(Color.white);
				
				ListIterator it = plots.listIterator();
				while (it.hasNext()) {
					Vector v = (Vector) it.next();
					Point p1 = (Point) v.get(0);
					for (int i = 1, size = v.size(); i < size; i += 2) {
						Point p2 = (Point) v.get(i);
						g2d.draw(new Line2D.Double(p1.x, p1.y, p2.x, p2.y));
						p1 = p2;
					}
				}
			}
		}
	}
	
	public void actionPerformed(ActionEvent evt) {
		this.command = evt.getActionCommand();
		if (this.command.equals("Clear")) {
			plots.clear();
			repaint();
			
//			Rectangle r = getBounds(); 
//			Image image = createImage(r.width, r.height);
//			JOptionPane.showMessageDialog(null, "X = " + r.width);			
//			JOptionPane.showMessageDialog(null, "Y = " + r.height);			
			
		} else if (this.command.equals("Save")) {
			//UPLOAD IMAGE, REQUEST TO SERVLET, LALU TERIMA RESPONSENYA
			Rectangle r = getBounds(); 
			Image image = createImage(r.width, r.height);
					Graphics g = image.getGraphics();
				
			paint(g);

			SignUtils.uploadImage(spaj, no, r, image, g, path, fileName, imageFormat);

		}
	}

}