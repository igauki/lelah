package com.ekalife.applet.jasper;
/*
 * ============================================================================
 * GNU Lesser General Public License
 * ============================================================================
 *
 * JasperReports - Free Java report-generating library.
 * Copyright (C) 2001-2005 JasperSoft Corporation http://www.jaspersoft.com
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 * 
 * JasperSoft Corporation
 * 185, Berry Street, Suite 6200
 * San Francisco CA 94107
 * http://www.jaspersoft.com
 */
import java.awt.BorderLayout;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;

import javax.swing.JOptionPane;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 * @author Teodor Danciu (teodord@users.sourceforge.net)
 * @version $Id: JasperViewerApplet.java,v 1.1 2020/11/04 04:32:34 iga Exp $
 */
public class JasperViewerApplet extends javax.swing.JApplet {

	private static final long serialVersionUID = -4822093157016234460L;
	private javax.swing.JPanel pnlMain;
	private JasperPrint jasperPrint = null;

	private void initComponents() {
		pnlMain = new javax.swing.JPanel();
		pnlMain.setLayout(new java.awt.BorderLayout());
		getContentPane().add(pnlMain, java.awt.BorderLayout.CENTER);
	}

	/** Creates new form AppletViewer */
	public JasperViewerApplet() {
		initComponents();
	}

	public void init() {
		String url = getParameter("REPORT_URL");
		int zoomIndex = Integer.parseInt(getParameter("ZOOM_INDEX"));

		if (url != null) {
			try {
				jasperPrint = (JasperPrint) JRLoader.loadObject(new URL(
						getCodeBase(), url));
				if (jasperPrint != null) {
					JasperViewer viewer = new JasperViewer(jasperPrint, zoomIndex); //1: defaultZoom = 75%, 2: defaultZoom = 100%
					this.pnlMain.add(viewer, BorderLayout.CENTER);
				}
			} catch (Exception e) {
				StringWriter swriter = new StringWriter();
				PrintWriter pwriter = new PrintWriter(swriter);
				e.printStackTrace(pwriter);
				JOptionPane.showMessageDialog(this,
						"Harap Hubungi EDP dengan hasil printscreen error ini.\n\n"
								+ swriter.toString());
			}
		} else {
			JOptionPane.showMessageDialog(this, "Source URL not specified");
		}
	}

}