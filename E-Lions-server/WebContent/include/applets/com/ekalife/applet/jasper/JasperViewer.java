package com.ekalife.applet.jasper;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.JViewport;
import javax.swing.border.LineBorder;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRPrintAnchorIndex;
import net.sf.jasperreports.engine.JRPrintElement;
import net.sf.jasperreports.engine.JRPrintHyperlink;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRPrintXmlLoader;
import net.sf.jasperreports.view.JRHyperlinkListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class JasperViewer extends JPanel implements JRHyperlinkListener {
	
	protected final Log logger = LogFactory.getLog( getClass() );

	private static final long serialVersionUID = 0x123f6ad6f4c6a6f6L;

	public static final int REPORT_RESOLUTION = 72;

	protected float MIN_ZOOM;

	protected float MAX_ZOOM;

	protected int zooms[] = { 50, 75, 100, 125, 150, 175, 200, 250 };

	protected int defaultZoomIndex;

	private int type;

	private boolean isXML;

	private String reportFileName;

	JasperPrint jasperPrint;

	private int pageIndex;

	private float zoom;

	private int screenResolution;

	private float realZoom;

	private DecimalFormat zoomDecimalFormat;

	private int downX;

	private int downY;

	private java.util.List hyperlinkListeners;

	private Map linksMap;

	private MouseListener mouseListener = new MouseAdapter() {

		public void mouseClicked(MouseEvent evt) {
			hyperlinkClicked(evt);
		}

	};

	protected JToggleButton btnActualSize;

	protected JButton btnFirst;

	protected JToggleButton btnFitPage;

	protected JToggleButton btnFitWidth;

	protected JButton btnLast;

	protected JButton btnNext;

	protected JButton btnPrevious;

	protected JButton btnZoomIn;

	protected JButton btnZoomOut;

	protected JComboBox cmbZoom;

	private JLabel jLabel1;

	private JPanel jPanel4;

	private JPanel jPanel5;

	private JPanel jPanel6;

	private JPanel jPanel7;

	private JPanel jPanel8;

	private JPanel jPanel9;

	private JLabel lblPage;

	protected JLabel lblStatus;

	private JPanel pnlInScroll;

	private JPanel pnlLinks;

	private JPanel pnlMain;

	private JPanel pnlPage;

	protected JPanel pnlSep01;

	protected JPanel pnlSep02;

	protected JPanel pnlSep03;

	protected JPanel pnlStatus;

	private JScrollPane scrollPane;

	protected JPanel tlbToolBar;

	protected JTextField txtGoTo;
	
	public JasperViewer(JasperPrint jrPrint, int zoomIndex) {
		MIN_ZOOM = 0.5F;
		MAX_ZOOM = 2.5F;
		defaultZoomIndex = zoomIndex;
		type = 1;
		isXML = false;
		reportFileName = null;
		jasperPrint = null;
		pageIndex = 0;
		zoom = 0.0F;
		screenResolution = 72;
		realZoom = 0.0F;
		zoomDecimalFormat = new DecimalFormat("#.##");
		downX = 0;
		downY = 0;
		hyperlinkListeners = new ArrayList();
		linksMap = new HashMap();
		setScreenDetails();
		setZooms();
		initComponents();
		loadReport(jrPrint);
		cmbZoom.setSelectedIndex(defaultZoomIndex);
		addHyperlinkListener(this);
	}

	private void setScreenDetails() {
		screenResolution = Toolkit.getDefaultToolkit().getScreenResolution();
	}

	public void clear() {
		emptyContainer(this);
		jasperPrint = null;
	}

	protected void setZooms() {
	}

	public void addHyperlinkListener(JRHyperlinkListener listener) {
		hyperlinkListeners.add(listener);
	}

	public void removeHyperlinkListener(JRHyperlinkListener listener) {
		hyperlinkListeners.remove(listener);
	}

	public JRHyperlinkListener[] getHyperlinkListeners() {
		return (JRHyperlinkListener[]) hyperlinkListeners
				.toArray(new JRHyperlinkListener[hyperlinkListeners.size()]);
	}

	public void gotoHyperlink(JRPrintHyperlink hyperlink) {
		switch (hyperlink.getHyperlinkType()) {
		case 1: // '\001'
		default:
			break;

		case 2: // '\002'
		{
			if (hyperlinkListeners != null && hyperlinkListeners.size() > 1) {
				logger.info("Hyperlink reference : "
						+ hyperlink.getHyperlinkReference());
				System.out
						.println("Implement your own JRHyperlinkListener to manage this type of event.");
			}
			break;
		}

		case 3: // '\003'
		{
			if (hyperlink.getHyperlinkAnchor() == null)
				break;
			Map anchorIndexes = jasperPrint.getAnchorIndexes();
			JRPrintAnchorIndex anchorIndex = (JRPrintAnchorIndex) anchorIndexes
					.get(hyperlink.getHyperlinkAnchor());
			if (anchorIndex.getPageIndex() != pageIndex) {
				setPageIndex(anchorIndex.getPageIndex());
				refreshPage();
			}
			Container container = pnlInScroll.getParent();
			if (!(container instanceof JViewport))
				break;
			JViewport viewport = (JViewport) container;
			int newX = (int) ((float) anchorIndex.getElement().getX() * realZoom);
			int newY = (int) ((float) anchorIndex.getElement().getY() * realZoom);
			int maxX = pnlInScroll.getWidth() - viewport.getWidth();
			int maxY = pnlInScroll.getHeight() - viewport.getHeight();
			if (newX < 0)
				newX = 0;
			if (newX > maxX)
				newX = maxX;
			if (newY < 0)
				newY = 0;
			if (newY > maxY)
				newY = maxY;
			viewport.setViewPosition(new Point(newX, newY));
			break;
		}

		case 4: // '\004'
		{
			int page = pageIndex + 1;
			if (hyperlink.getHyperlinkPage() != null)
				page = hyperlink.getHyperlinkPage();
			if (page < 1 || page > jasperPrint.getPages().size()
					|| page == pageIndex + 1)
				break;
			setPageIndex(page - 1);
			refreshPage();
			Container container = pnlInScroll.getParent();
			if (container instanceof JViewport) {
				JViewport viewport = (JViewport) container;
				viewport.setViewPosition(new Point(0, 0));
			}
			break;
		}

		case 5: // '\005'
		{
			if (hyperlinkListeners != null && hyperlinkListeners.size() > 1) {
				logger.info("Hyperlink reference : "
						+ hyperlink.getHyperlinkReference());
				logger.info("Hyperlink anchor    : "
						+ hyperlink.getHyperlinkAnchor());
				System.out
						.println("Implement your own JRHyperlinkListener to manage this type of event.");
			}
			break;
		}

		case 6: // '\006'
		{
			if (hyperlinkListeners != null && hyperlinkListeners.size() > 1) {
				logger.info("Hyperlink reference : "
						+ hyperlink.getHyperlinkReference());
				logger.info("Hyperlink page      : "
						+ hyperlink.getHyperlinkPage());
				System.out
						.println("Implement your own JRHyperlinkListener to manage this type of event.");
			}
			break;
		}
		}
	}

	private void initComponents() {
		tlbToolBar = new JPanel();
		pnlSep01 = new JPanel();
		btnFirst = new JButton();
		btnPrevious = new JButton();
		btnNext = new JButton();
		btnLast = new JButton();
		txtGoTo = new JTextField();
		pnlSep02 = new JPanel();
		btnActualSize = new JToggleButton();
		btnFitPage = new JToggleButton();
		btnFitWidth = new JToggleButton();
		pnlSep03 = new JPanel();
		btnZoomIn = new JButton();
		btnZoomOut = new JButton();
		cmbZoom = new JComboBox();
		DefaultComboBoxModel model = new DefaultComboBoxModel();
		for (int i = 0; i < zooms.length; i++)
			model.addElement(zooms[i] + "%");

		cmbZoom.setModel(model);
		pnlMain = new JPanel();
		scrollPane = new JScrollPane();
		scrollPane.getHorizontalScrollBar().setUnitIncrement(5);
		scrollPane.getVerticalScrollBar().setUnitIncrement(5);
		pnlInScroll = new JPanel();
		pnlPage = new JPanel();
		jPanel4 = new JPanel();
		pnlLinks = new JPanel();
		jPanel5 = new JPanel();
		jPanel6 = new JPanel();
		jPanel7 = new JPanel();
		jPanel8 = new JPanel();
		jLabel1 = new JLabel();
		jPanel9 = new JPanel();
		lblPage = new JLabel();
		pnlStatus = new JPanel();
		lblStatus = new JLabel();
		setLayout(new BorderLayout());
		setMinimumSize(new Dimension(450, 150));
		setPreferredSize(new Dimension(450, 150));
		tlbToolBar.setLayout(new FlowLayout(0, 0, 2));

		pnlSep01.setMaximumSize(new Dimension(10, 10));
		tlbToolBar.add(pnlSep01);
		btnFirst.setIcon(new ImageIcon(getClass().getResource(
				"/net/sf/jasperreports/view/images/first.GIF")));
		btnFirst.setToolTipText(ResourceBundle.getBundle(
				"net/sf/jasperreports/view/viewer").getString("first.page"));
		btnFirst.setMargin(new Insets(2, 2, 2, 2));
		btnFirst.setMaximumSize(new Dimension(23, 23));
		btnFirst.setMinimumSize(new Dimension(23, 23));
		btnFirst.setPreferredSize(new Dimension(23, 23));
		btnFirst.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				btnFirstActionPerformed();
			}

		});
		tlbToolBar.add(btnFirst);
		btnPrevious.setIcon(new ImageIcon(getClass().getResource(
				"/net/sf/jasperreports/view/images/previous.GIF")));
		btnPrevious.setToolTipText(ResourceBundle.getBundle(
				"net/sf/jasperreports/view/viewer").getString("previous.page"));
		btnPrevious.setMargin(new Insets(2, 2, 2, 2));
		btnPrevious.setMaximumSize(new Dimension(23, 23));
		btnPrevious.setMinimumSize(new Dimension(23, 23));
		btnPrevious.setPreferredSize(new Dimension(23, 23));
		btnPrevious.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				btnPreviousActionPerformed();
			}

		});
		tlbToolBar.add(btnPrevious);
		btnNext.setIcon(new ImageIcon(getClass().getResource(
				"/net/sf/jasperreports/view/images/next.GIF")));
		btnNext.setToolTipText(ResourceBundle.getBundle(
				"net/sf/jasperreports/view/viewer").getString("next.page"));
		btnNext.setMargin(new Insets(2, 2, 2, 2));
		btnNext.setMaximumSize(new Dimension(23, 23));
		btnNext.setMinimumSize(new Dimension(23, 23));
		btnNext.setPreferredSize(new Dimension(23, 23));
		btnNext.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				btnNextActionPerformed();
			}

		});
		tlbToolBar.add(btnNext);
		btnLast.setIcon(new ImageIcon(getClass().getResource(
				"/net/sf/jasperreports/view/images/last.GIF")));
		btnLast.setToolTipText(ResourceBundle.getBundle(
				"net/sf/jasperreports/view/viewer").getString("last.page"));
		btnLast.setMargin(new Insets(2, 2, 2, 2));
		btnLast.setMaximumSize(new Dimension(23, 23));
		btnLast.setMinimumSize(new Dimension(23, 23));
		btnLast.setPreferredSize(new Dimension(23, 23));
		btnLast.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				btnLastActionPerformed();
			}

		});
		tlbToolBar.add(btnLast);
		txtGoTo.setToolTipText(ResourceBundle.getBundle(
				"net/sf/jasperreports/view/viewer").getString("go.to.page"));
		txtGoTo.setMaximumSize(new Dimension(40, 23));
		txtGoTo.setMinimumSize(new Dimension(40, 23));
		txtGoTo.setPreferredSize(new Dimension(40, 23));
		txtGoTo.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				txtGoToActionPerformed();
			}

		});
		tlbToolBar.add(txtGoTo);
		pnlSep02.setMaximumSize(new Dimension(10, 10));
		tlbToolBar.add(pnlSep02);
		btnActualSize.setIcon(new ImageIcon(getClass().getResource(
				"/net/sf/jasperreports/view/images/actualsize.GIF")));
		btnActualSize.setToolTipText(ResourceBundle.getBundle(
				"net/sf/jasperreports/view/viewer").getString("actual.size"));
		btnActualSize.setMargin(new Insets(2, 2, 2, 2));
		btnActualSize.setMaximumSize(new Dimension(23, 23));
		btnActualSize.setMinimumSize(new Dimension(23, 23));
		btnActualSize.setPreferredSize(new Dimension(23, 23));
		btnActualSize.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				btnActualSizeActionPerformed();
			}

		});
		tlbToolBar.add(btnActualSize);
		btnFitPage.setIcon(new ImageIcon(getClass().getResource(
				"/net/sf/jasperreports/view/images/fitpage.GIF")));
		btnFitPage.setToolTipText(ResourceBundle.getBundle(
				"net/sf/jasperreports/view/viewer").getString("fit.page"));
		btnFitPage.setMargin(new Insets(2, 2, 2, 2));
		btnFitPage.setMaximumSize(new Dimension(23, 23));
		btnFitPage.setMinimumSize(new Dimension(23, 23));
		btnFitPage.setPreferredSize(new Dimension(23, 23));
		btnFitPage.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				btnFitPageActionPerformed();
			}

		});
		tlbToolBar.add(btnFitPage);
		btnFitWidth.setIcon(new ImageIcon(getClass().getResource(
				"/net/sf/jasperreports/view/images/fitwidth.GIF")));
		btnFitWidth.setToolTipText(ResourceBundle.getBundle(
				"net/sf/jasperreports/view/viewer").getString("fit.width"));
		btnFitWidth.setMargin(new Insets(2, 2, 2, 2));
		btnFitWidth.setMaximumSize(new Dimension(23, 23));
		btnFitWidth.setMinimumSize(new Dimension(23, 23));
		btnFitWidth.setPreferredSize(new Dimension(23, 23));
		btnFitWidth.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				btnFitWidthActionPerformed();
			}

		});
		tlbToolBar.add(btnFitWidth);
		pnlSep03.setMaximumSize(new Dimension(10, 10));
		tlbToolBar.add(pnlSep03);
		btnZoomIn.setIcon(new ImageIcon(getClass().getResource(
				"/net/sf/jasperreports/view/images/zoomin.GIF")));
		btnZoomIn.setToolTipText(ResourceBundle.getBundle(
				"net/sf/jasperreports/view/viewer").getString("zoom.in"));
		btnZoomIn.setMargin(new Insets(2, 2, 2, 2));
		btnZoomIn.setMaximumSize(new Dimension(23, 23));
		btnZoomIn.setMinimumSize(new Dimension(23, 23));
		btnZoomIn.setPreferredSize(new Dimension(23, 23));
		btnZoomIn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				btnZoomInActionPerformed();
			}

		});
		tlbToolBar.add(btnZoomIn);
		btnZoomOut.setIcon(new ImageIcon(getClass().getResource(
				"/net/sf/jasperreports/view/images/zoomout.GIF")));
		btnZoomOut.setToolTipText(ResourceBundle.getBundle(
				"net/sf/jasperreports/view/viewer").getString("zoom.out"));
		btnZoomOut.setMargin(new Insets(2, 2, 2, 2));
		btnZoomOut.setMaximumSize(new Dimension(23, 23));
		btnZoomOut.setMinimumSize(new Dimension(23, 23));
		btnZoomOut.setPreferredSize(new Dimension(23, 23));
		btnZoomOut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				btnZoomOutActionPerformed();
			}

		});
		tlbToolBar.add(btnZoomOut);
		cmbZoom.setEditable(true);
		cmbZoom.setToolTipText(ResourceBundle.getBundle(
				"net/sf/jasperreports/view/viewer").getString("zoom.ratio"));
		cmbZoom.setMaximumSize(new Dimension(80, 23));
		cmbZoom.setMinimumSize(new Dimension(80, 23));
		cmbZoom.setPreferredSize(new Dimension(80, 23));
		cmbZoom.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				cmbZoomActionPerformed();
			}

		});
		cmbZoom.addItemListener(new ItemListener() {

			public void itemStateChanged(ItemEvent evt) {
				cmbZoomItemStateChanged();
			}

		});
		tlbToolBar.add(cmbZoom);
		add(tlbToolBar, "North");
		pnlMain.setLayout(new BorderLayout());
		pnlMain.addComponentListener(new ComponentAdapter() {

			public void componentResized(ComponentEvent evt) {
				pnlMainComponentResized();
			}

		});
		scrollPane.setHorizontalScrollBarPolicy(32);
		scrollPane.setVerticalScrollBarPolicy(22);
		pnlInScroll.setLayout(new GridBagLayout());
		pnlPage.setLayout(new BorderLayout());
		pnlPage.setMinimumSize(new Dimension(100, 100));
		pnlPage.setPreferredSize(new Dimension(100, 100));
		jPanel4.setLayout(new GridBagLayout());
		jPanel4.setMinimumSize(new Dimension(100, 120));
		jPanel4.setPreferredSize(new Dimension(100, 120));
		pnlLinks.setLayout(null);
		pnlLinks.setMinimumSize(new Dimension(5, 5));
		pnlLinks.setPreferredSize(new Dimension(5, 5));
		pnlLinks.setOpaque(false);
		pnlLinks.addMouseListener(new MouseAdapter() {

			public void mousePressed(MouseEvent evt) {
				pnlLinksMousePressed(evt);
			}

			public void mouseReleased(MouseEvent evt) {
				pnlLinksMouseReleased();
			}

		});
		pnlLinks.addMouseMotionListener(new MouseMotionAdapter() {

			public void mouseDragged(MouseEvent evt) {
				pnlLinksMouseDragged(evt);
			}

		});
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.gridheight = 2;
		gridBagConstraints.fill = 1;
		jPanel4.add(pnlLinks, gridBagConstraints);
		jPanel5.setBackground(Color.gray);
		jPanel5.setMinimumSize(new Dimension(5, 5));
		jPanel5.setPreferredSize(new Dimension(5, 5));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.fill = 3;
		jPanel4.add(jPanel5, gridBagConstraints);
		jPanel6.setMinimumSize(new Dimension(5, 5));
		jPanel6.setPreferredSize(new Dimension(5, 5));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 2;
		jPanel4.add(jPanel6, gridBagConstraints);
		jPanel7.setBackground(Color.gray);
		jPanel7.setMinimumSize(new Dimension(5, 5));
		jPanel7.setPreferredSize(new Dimension(5, 5));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 2;
		gridBagConstraints.fill = 2;
		jPanel4.add(jPanel7, gridBagConstraints);
		jPanel8.setBackground(Color.gray);
		jPanel8.setMinimumSize(new Dimension(5, 5));
		jPanel8.setPreferredSize(new Dimension(5, 5));
		jLabel1.setText("jLabel1");
		jPanel8.add(jLabel1);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 2;
		jPanel4.add(jPanel8, gridBagConstraints);
		jPanel9.setMinimumSize(new Dimension(5, 5));
		jPanel9.setPreferredSize(new Dimension(5, 5));
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 0;
		jPanel4.add(jPanel9, gridBagConstraints);
		lblPage.setBackground(Color.white);
		lblPage.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblPage.setOpaque(true);
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridwidth = 2;
		gridBagConstraints.gridheight = 2;
		gridBagConstraints.fill = 1;
		gridBagConstraints.weightx = 1.0D;
		gridBagConstraints.weighty = 1.0D;
		jPanel4.add(lblPage, gridBagConstraints);
		pnlPage.add(jPanel4, "Center");
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.insets = new Insets(5, 5, 5, 5);
		pnlInScroll.add(pnlPage, gridBagConstraints);
		scrollPane.setViewportView(pnlInScroll);
		pnlMain.add(scrollPane, "Center");
		add(pnlMain, "Center");
		pnlStatus.setLayout(new FlowLayout(1, 0, 0));
		lblStatus.setFont(new Font("Dialog", 1, 10));
		lblStatus.setText("Page i of n");
		pnlStatus.add(lblStatus);
		add(pnlStatus, "South");
	}

	void txtGoToActionPerformed() {
		try {
			int pageNumber = Integer.parseInt(txtGoTo.getText());
			if (pageNumber != pageIndex + 1 && pageNumber > 0
					&& pageNumber <= jasperPrint.getPages().size()) {
				setPageIndex(pageNumber - 1);
				refreshPage();
			}
		} catch (NumberFormatException numberformatexception) {
		}
	}

	void cmbZoomItemStateChanged() {
		btnActualSize.setSelected(false);
		btnFitPage.setSelected(false);
		btnFitWidth.setSelected(false);
	}

	void pnlMainComponentResized() {
		if (btnFitPage.isSelected())
			setRealZoomRatio(((float) pnlInScroll.getVisibleRect().getHeight() - 20F)
					/ (float) jasperPrint.getPageHeight());
		else if (btnFitWidth.isSelected())
			setRealZoomRatio(((float) pnlInScroll.getVisibleRect().getWidth() - 20F)
					/ (float) jasperPrint.getPageWidth());
	}

	void btnActualSizeActionPerformed() {
		if (btnActualSize.isSelected()) {
			btnFitPage.setSelected(false);
			btnFitWidth.setSelected(false);
			setZoomRatio(1.0F);
		}
	}

	void btnFitWidthActionPerformed() {
		if (btnFitWidth.isSelected()) {
			btnActualSize.setSelected(false);
			btnFitPage.setSelected(false);
			setRealZoomRatio(((float) pnlInScroll.getVisibleRect().getWidth() - 20F)
					/ (float) jasperPrint.getPageWidth());
		}
	}

	void btnFitPageActionPerformed() {
		if (btnFitPage.isSelected()) {
			btnActualSize.setSelected(false);
			btnFitWidth.setSelected(false);
			setRealZoomRatio(((float) pnlInScroll.getVisibleRect().getHeight() - 20F)
					/ (float) jasperPrint.getPageHeight());
		}
	}

	void pnlLinksMouseDragged(MouseEvent evt) {
		Container container = pnlInScroll.getParent();
		if (container instanceof JViewport) {
			JViewport viewport = (JViewport) container;
			Point point = viewport.getViewPosition();
			int newX = point.x - (evt.getX() - downX);
			int newY = point.y - (evt.getY() - downY);
			int maxX = pnlInScroll.getWidth() - viewport.getWidth();
			int maxY = pnlInScroll.getHeight() - viewport.getHeight();
			if (newX < 0)
				newX = 0;
			if (newX > maxX)
				newX = maxX;
			if (newY < 0)
				newY = 0;
			if (newY > maxY)
				newY = maxY;
			viewport.setViewPosition(new Point(newX, newY));
		}
	}

	void pnlLinksMouseReleased() {
		pnlLinks.setCursor(new Cursor(0));
	}

	void pnlLinksMousePressed(MouseEvent evt) {
		pnlLinks.setCursor(new Cursor(13));
		downX = evt.getX();
		downY = evt.getY();
	}

	void btnPrintActionPerformed() {
		Thread thread = new Thread(new Runnable() {

			public void run() {
				try {
					JasperPrintManager.printReport(jasperPrint, true);
				} catch (Exception ex) {
					logger.error("ERROR :", ex);
					JOptionPane.showMessageDialog(null, ResourceBundle
							.getBundle("net/sf/jasperreports/view/viewer")
							.getString("error.printing"));
					return;
				}
			}

		});
		thread.start();
	}

	void btnLastActionPerformed() {
		setPageIndex(jasperPrint.getPages().size() - 1);
		refreshPage();
	}

	void btnNextActionPerformed() {
		setPageIndex(pageIndex + 1);
		refreshPage();
	}

	void btnPreviousActionPerformed() {
		setPageIndex(pageIndex - 1);
		refreshPage();
	}

	void btnFirstActionPerformed() {
		setPageIndex(0);
		refreshPage();
	}

	void btnReloadActionPerformed() {
		if (type == 1) {
			try {
				loadReport(reportFileName, isXML);
			} catch (JRException e) {
				logger.error("ERROR :", e);
				jasperPrint = null;
				setPageIndex(0);
				refreshPage();
				JOptionPane.showMessageDialog(this, ResourceBundle.getBundle(
						"net/sf/jasperreports/view/viewer").getString(
						"error.loading"));
			}
			zoom = 0.0F;
			realZoom = 0.0F;
			setZoomRatio(1.0F);
		}
	}

	void btnZoomInActionPerformed() {
		btnActualSize.setSelected(false);
		btnFitPage.setSelected(false);
		btnFitWidth.setSelected(false);
		int newZoomInt = (int) (100F * getZoomRatio());
		int index = Arrays.binarySearch(zooms, newZoomInt);
		if (index < 0)
			setZoomRatio((float) zooms[-index - 1] / 100F);
		else if (index < cmbZoom.getModel().getSize() - 1)
			setZoomRatio((float) zooms[index + 1] / 100F);
	}

	void btnZoomOutActionPerformed() {
		btnActualSize.setSelected(false);
		btnFitPage.setSelected(false);
		btnFitWidth.setSelected(false);
		int newZoomInt = (int) (100F * getZoomRatio());
		int index = Arrays.binarySearch(zooms, newZoomInt);
		if (index > 0)
			setZoomRatio((float) zooms[index - 1] / 100F);
		else if (index < -1)
			setZoomRatio((float) zooms[-index - 2] / 100F);
	}

	void cmbZoomActionPerformed() {
		float newZoom = getZoomRatio();
		if (newZoom < MIN_ZOOM)
			newZoom = MIN_ZOOM;
		if (newZoom > MAX_ZOOM)
			newZoom = MAX_ZOOM;
		setZoomRatio(newZoom);
	}

	void hyperlinkClicked(MouseEvent evt) {
		JPanel link = (JPanel) evt.getSource();
		JRPrintHyperlink element = (JRPrintHyperlink) linksMap.get(link);
		try {
			JRHyperlinkListener listener = null;
			for (int i = 0; i < hyperlinkListeners.size(); i++) {
				listener = (JRHyperlinkListener) hyperlinkListeners.get(i);
				listener.gotoHyperlink(element);
			}

		} catch (JRException e) {
			logger.error("ERROR :", e);
			JOptionPane.showMessageDialog(this, ResourceBundle.getBundle(
					"net/sf/jasperreports/view/viewer").getString(
					"error.hyperlink"));
		}
	}

	private void setPageIndex(int index) {
		pageIndex = index;
		if (jasperPrint != null && jasperPrint.getPages() != null
				&& jasperPrint.getPages().size() > 0) {
			btnFirst.setEnabled(pageIndex > 0);
			btnPrevious.setEnabled(pageIndex > 0);
			btnNext.setEnabled(pageIndex < jasperPrint.getPages().size() - 1);
			btnLast.setEnabled(pageIndex < jasperPrint.getPages().size() - 1);
			txtGoTo.setEnabled(btnFirst.isEnabled() || btnLast.isEnabled());
			txtGoTo.setText(String.valueOf(pageIndex + 1));
			lblStatus.setText(MessageFormat.format(ResourceBundle.getBundle(
					"net/sf/jasperreports/view/viewer").getString("page"),
					new Object[] { new Integer(pageIndex + 1),
							new Integer(jasperPrint.getPages().size()) }));
		} else {
			btnFirst.setEnabled(false);
			btnPrevious.setEnabled(false);
			btnNext.setEnabled(false);
			btnLast.setEnabled(false);
			txtGoTo.setEnabled(false);
			txtGoTo.setText("");
			lblStatus.setText("");
		}
	}

	protected void loadReport(String fileName, boolean isXmlReport)
			throws JRException {
		if (isXmlReport)
			jasperPrint = JRPrintXmlLoader.load(fileName);
		else
			jasperPrint = (JasperPrint) JRLoader.loadObject(fileName);
		type = 1;
		isXML = isXmlReport;
		reportFileName = fileName;

		setPageIndex(0);
	}

	protected void loadReport(InputStream is, boolean isXmlReport)
			throws JRException {
		if (isXmlReport)
			jasperPrint = JRPrintXmlLoader.load(is);
		else
			jasperPrint = (JasperPrint) JRLoader.loadObject(is);
		type = 2;
		isXML = isXmlReport;

		setPageIndex(0);
	}

	protected void loadReport(JasperPrint jrPrint) {
		jasperPrint = jrPrint;
		type = 3;
		isXML = false;

		setPageIndex(0);
	}

	protected void refreshPage() {
		if (jasperPrint == null || jasperPrint.getPages() == null
				|| jasperPrint.getPages().size() == 0) {
			pnlPage.setVisible(false);

			btnActualSize.setEnabled(false);
			btnFitPage.setEnabled(false);
			btnFitWidth.setEnabled(false);
			btnZoomIn.setEnabled(false);
			btnZoomOut.setEnabled(false);
			cmbZoom.setEnabled(false);
			if (jasperPrint != null)
				JOptionPane.showMessageDialog(this, ResourceBundle.getBundle(
						"net/sf/jasperreports/view/viewer").getString(
						"no.pages"));
			return;
		}
		pnlPage.setVisible(true);

		btnActualSize.setEnabled(true);
		btnFitPage.setEnabled(true);
		btnFitWidth.setEnabled(true);
		btnZoomIn.setEnabled(zoom < MAX_ZOOM);
		btnZoomOut.setEnabled(zoom > MIN_ZOOM);
		cmbZoom.setEnabled(true);
		java.awt.Image image = null;
		ImageIcon imageIcon = null;
		Dimension dim = new Dimension(
				(int) ((float) jasperPrint.getPageWidth() * realZoom) + 8,
				(int) ((float) jasperPrint.getPageHeight() * realZoom) + 8);
		pnlPage.setMaximumSize(dim);
		pnlPage.setMinimumSize(dim);
		pnlPage.setPreferredSize(dim);
		try {
			image = JasperPrintManager.printPageToImage(jasperPrint, pageIndex,
					realZoom);
			imageIcon = new ImageIcon(image);
		} catch (Exception e) {
			logger.error("ERROR :", e);
			JOptionPane.showMessageDialog(this, ResourceBundle.getBundle(
					"net/sf/jasperreports/view/viewer").getString(
					"error.displaying"));
		}
		pnlLinks.removeAll();
		linksMap = new HashMap();
		java.util.List pages = jasperPrint.getPages();
		JRPrintPage page = (JRPrintPage) pages.get(pageIndex);
		Collection elements = page.getElements();
		if (elements != null && elements.size() > 0) {
			String toolTip = null;
			JPanel link = null;
			JRPrintElement element = null;
			JRPrintHyperlink hyperlink = null;
			for (Iterator it = elements.iterator(); it.hasNext();) {
				element = (JRPrintElement) it.next();
				if ((element instanceof JRPrintHyperlink)
						&& ((JRPrintHyperlink) element).getHyperlinkType() != 1) {
					hyperlink = (JRPrintHyperlink) element;
					link = new JPanel();
					link.setCursor(new Cursor(12));
					link.setLocation((int) ((float) element.getX() * realZoom),
							(int) ((float) element.getY() * realZoom));
					link.setSize((int) ((float) element.getWidth() * realZoom),
							(int) ((float) element.getHeight() * realZoom));
					link.setOpaque(false);
					toolTip = null;
					switch (hyperlink.getHyperlinkType()) {
					default:
						break;

					case 2: // '\002'
						toolTip = hyperlink.getHyperlinkReference();
						break;

					case 3: // '\003'
						if (hyperlink.getHyperlinkAnchor() != null)
							toolTip = "#" + hyperlink.getHyperlinkAnchor();
						break;

					case 4: // '\004'
						if (hyperlink.getHyperlinkPage() != null)
							toolTip = "#page " + hyperlink.getHyperlinkPage();
						break;

					case 5: // '\005'
						toolTip = "";
						if (hyperlink.getHyperlinkReference() != null)
							toolTip = toolTip
									+ hyperlink.getHyperlinkReference();
						if (hyperlink.getHyperlinkAnchor() != null)
							toolTip = toolTip + "#"
									+ hyperlink.getHyperlinkAnchor();
						break;

					case 6: // '\006'
						toolTip = "";
						if (hyperlink.getHyperlinkReference() != null)
							toolTip = toolTip
									+ hyperlink.getHyperlinkReference();
						if (hyperlink.getHyperlinkPage() != null)
							toolTip = toolTip + "#page "
									+ hyperlink.getHyperlinkPage();
						break;
					}
					link.setToolTipText(toolTip);
					link.addMouseListener(mouseListener);
					pnlLinks.add(link);
					linksMap.put(link, element);
				}
			}

		}
		lblPage.setIcon(imageIcon);
	}

	private void emptyContainer(Container container) {
		java.awt.Component components[] = container.getComponents();
		if (components != null) {
			for (int i = 0; i < components.length; i++)
				if (components[i] instanceof Container)
					emptyContainer((Container) components[i]);

		}
		components = (java.awt.Component[]) null;
		container.removeAll();
		container = null;
	}

	private float getZoomRatio() {
		float newZoom = zoom;
		try {
			newZoom = zoomDecimalFormat.parse(
					String.valueOf(cmbZoom.getEditor().getItem())).floatValue() / 100F;
		} catch (ParseException parseexception) {
		}
		return newZoom;
	}

	private void setZoomRatio(float newZoom) {
		if (newZoom > 0.0F) {
			cmbZoom.getEditor().setItem(
					zoomDecimalFormat.format(newZoom * 100F) + "%");
			if (zoom != newZoom) {
				zoom = newZoom;
				realZoom = (zoom * (float) screenResolution) / 72F;
				refreshPage();
			}
		}
	}

	private void setRealZoomRatio(float newZoom) {
		if (newZoom > 0.0F && realZoom != newZoom) {
			zoom = (newZoom * 72F) / (float) screenResolution;
			realZoom = newZoom;
			cmbZoom.getEditor().setItem(
					zoomDecimalFormat.format(zoom * 100F) + "%");
			refreshPage();
		}
	}

}