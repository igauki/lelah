<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 10">
<meta name=Originator content="Microsoft Word 10">
<link rel=File-List href="BAS_FAQ_REVIEW1_files/filelist.xml">
<link rel=Edit-Time-Data href="BAS_FAQ_REVIEW1_files/editdata.mso">
<title>FREQUENTLY ASKED QUESTION</title>
<o:SmartTagType namespaceuri="urn:schemas-microsoft-com:office:smarttags"
 name="country-region"/>
<o:SmartTagType namespaceuri="urn:schemas-microsoft-com:office:smarttags"
 name="City"/>
<o:SmartTagType namespaceuri="urn:schemas-microsoft-com:office:smarttags"
 name="place"/>
<o:SmartTagType namespaceuri="urn:schemas-microsoft-com:office:smarttags"
 name="date"/>
<o:SmartTagType namespaceuri="urn:schemas-microsoft-com:office:smarttags"
 name="PersonName"/>
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>Monica</o:Author>
  <o:Template>Normal</o:Template>
  <o:LastAuthor>Ferry Harlim</o:LastAuthor>
  <o:Revision>2</o:Revision>
  <o:TotalTime>239</o:TotalTime>
  <o:Created>2007-11-05T01:55:00Z</o:Created>
  <o:LastSaved>2007-11-05T01:55:00Z</o:LastSaved>
  <o:Pages>1</o:Pages>
  <o:Words>3237</o:Words>
  <o:Characters>18455</o:Characters>
  <o:Company>ASURANSI JIWA EKA LIFE, PT.</o:Company>
  <o:Lines>153</o:Lines>
  <o:Paragraphs>43</o:Paragraphs>
  <o:CharactersWithSpaces>21649</o:CharactersWithSpaces>
  <o:Version>10.2625</o:Version>
 </o:DocumentProperties>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:HideSpellingErrors/>
  <w:HideGrammaticalErrors/>
  <w:ActiveWritingStyle Lang="EN-US" VendorID="64" DLLVersion="131077"
   NLCheck="1">1</w:ActiveWritingStyle>
  <w:ActiveWritingStyle Lang="EN-GB" VendorID="64" DLLVersion="131077"
   NLCheck="1">1</w:ActiveWritingStyle>
  <w:ActiveWritingStyle Lang="EN-US" VendorID="64" DLLVersion="131078"
   NLCheck="1">1</w:ActiveWritingStyle>
  <w:ActiveWritingStyle Lang="EN-GB" VendorID="64" DLLVersion="131078"
   NLCheck="1">1</w:ActiveWritingStyle>
  <w:GrammarState>Clean</w:GrammarState>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
 </w:WordDocument>
</xml><![endif]--><!--[if !mso]><object
 classid="clsid:38481807-CA0E-42D2-BF39-B33AF135CC4D" id=ieooui></object>
<style>
st1\:*{behavior:url(#ieooui) }
</style>
<![endif]-->
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;
	mso-font-charset:2;
	mso-generic-font-family:auto;
	mso-font-pitch:variable;
	mso-font-signature:0 268435456 0 0 -2147483648 0;}
@font-face
	{font-family:"Arial Narrow";
	panose-1:2 11 6 6 2 2 2 3 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:647 2048 0 0 159 0;}
@font-face
	{font-family:Verdana;
	panose-1:2 11 6 4 3 5 4 4 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-1593833729 1073750107 16 0 415 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
h1
	{mso-style-next:Normal;
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	page-break-after:avoid;
	mso-outline-level:1;
	font-size:11.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:Arial;
	mso-font-kerning:0pt;}
h2
	{mso-style-next:Normal;
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	page-break-after:avoid;
	mso-outline-level:2;
	font-size:10.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:Arial;}
h3
	{mso-style-next:Normal;
	margin-top:12.0pt;
	margin-right:0cm;
	margin-bottom:3.0pt;
	margin-left:0cm;
	text-align:justify;
	mso-pagination:widow-orphan;
	page-break-after:avoid;
	mso-outline-level:3;
	font-size:10.0pt;
	mso-bidi-font-size:13.0pt;
	font-family:Arial;
	mso-bidi-font-weight:normal;}
h4
	{mso-style-next:Normal;
	margin-top:12.0pt;
	margin-right:0cm;
	margin-bottom:3.0pt;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	page-break-after:avoid;
	mso-outline-level:4;
	font-size:10.0pt;
	mso-bidi-font-size:14.0pt;
	font-family:Arial;
	mso-bidi-font-family:"Times New Roman";
	font-style:italic;
	mso-bidi-font-style:normal;}
h5
	{mso-style-next:Normal;
	margin-top:12.0pt;
	margin-right:0cm;
	margin-bottom:3.0pt;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	mso-outline-level:5;
	font-size:13.0pt;
	font-family:Arial;
	mso-bidi-font-family:"Times New Roman";
	font-style:italic;}
h6
	{mso-style-next:Normal;
	margin-top:12.0pt;
	margin-right:0cm;
	margin-bottom:3.0pt;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	mso-outline-level:6;
	font-size:11.0pt;
	font-family:Arial;
	mso-bidi-font-family:"Times New Roman";}
p.MsoHeading7, li.MsoHeading7, div.MsoHeading7
	{mso-style-next:Normal;
	margin-top:12.0pt;
	margin-right:0cm;
	margin-bottom:3.0pt;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	mso-outline-level:7;
	font-size:10.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:Arial;
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";}
p.MsoHeading8, li.MsoHeading8, div.MsoHeading8
	{mso-style-next:Normal;
	margin-top:12.0pt;
	margin-right:0cm;
	margin-bottom:3.0pt;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	mso-outline-level:8;
	font-size:10.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:Arial;
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";
	font-style:italic;}
p.MsoHeading9, li.MsoHeading9, div.MsoHeading9
	{mso-style-next:Normal;
	margin-top:12.0pt;
	margin-right:0cm;
	margin-bottom:3.0pt;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	mso-outline-level:9;
	font-size:11.0pt;
	font-family:Arial;
	mso-fareast-font-family:"Times New Roman";}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	mso-pagination:widow-orphan;
	font-size:20.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Arial Narrow";
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";
	mso-ansi-language:EN-GB;}
p.MsoBodyTextIndent, li.MsoBodyTextIndent, div.MsoBodyTextIndent
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	text-indent:-36.0pt;
	mso-pagination:widow-orphan;
	tab-stops:18.0pt 36.0pt;
	font-size:10.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:Arial;
	mso-fareast-font-family:"Times New Roman";}
p.MsoBodyTextIndent2, li.MsoBodyTextIndent2, div.MsoBodyTextIndent2
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	text-indent:-36.0pt;
	mso-pagination:widow-orphan;
	mso-layout-grid-align:none;
	text-autospace:none;
	font-size:10.0pt;
	font-family:Arial;
	mso-fareast-font-family:"Times New Roman";
	color:black;}
p.MsoBodyTextIndent3, li.MsoBodyTextIndent3, div.MsoBodyTextIndent3
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:18.0pt;
	margin-bottom:.0001pt;
	text-align:justify;
	text-indent:-18.0pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";
	mso-ansi-language:EN-GB;}
a:link, span.MsoHyperlink
	{color:blue;
	text-decoration:underline;
	text-underline:single;}
a:visited, span.MsoHyperlinkFollowed
	{color:purple;
	text-decoration:underline;
	text-underline:single;}
p.x1, li.x1, div.x1
	{mso-style-name:x1;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:7.5pt;
	font-family:Verdana;
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:Arial;
	color:black;
	mso-ansi-language:EN-GB;}
span.GramE
	{mso-style-name:"";
	mso-gram-e:yes;}
@page Section1
	{size:595.45pt 841.7pt;
	margin:72.0pt 72.0pt 35.95pt 72.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.Section1
	{page:Section1;}
 /* List Definitions */
 @list l0
	{mso-list-id:4940393;
	mso-list-type:hybrid;
	mso-list-template-ids:1239449824 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l0:level1
	{mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-18.0pt;}
@list l1
	{mso-list-id:22707140;
	mso-list-type:hybrid;
	mso-list-template-ids:515516714 -1968805886 2089743496 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l1:level1
	{mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-18.0pt;
	text-decoration:none;
	text-line-through:none;}
@list l1:level2
	{mso-level-start-at:2;
	mso-level-text:%2;
	mso-level-tab-stop:90.0pt;
	mso-level-number-position:left;
	margin-left:90.0pt;
	text-indent:-18.0pt;
	color:windowtext;}
@list l2
	{mso-list-id:68503841;
	mso-list-type:hybrid;
	mso-list-template-ids:1881151062 1797577620 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l2:level1
	{mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l3
	{mso-list-id:101341506;
	mso-list-type:hybrid;
	mso-list-template-ids:690655576 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l3:level1
	{mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-18.0pt;}
@list l4
	{mso-list-id:121923729;
	mso-list-template-ids:-1893017264;}
@list l4:level1
	{mso-level-start-at:72;
	mso-level-tab-stop:24.75pt;
	mso-level-number-position:left;
	margin-left:24.75pt;
	text-indent:-24.75pt;}
@list l4:level2
	{mso-level-text:"%1\.%2\.";
	mso-level-tab-stop:42.75pt;
	mso-level-number-position:left;
	margin-left:42.75pt;
	text-indent:-24.75pt;}
@list l4:level3
	{mso-level-text:"%1\.%2\.%3\.";
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-36.0pt;}
@list l4:level4
	{mso-level-text:"%1\.%2\.%3\.%4\.";
	mso-level-tab-stop:90.0pt;
	mso-level-number-position:left;
	margin-left:90.0pt;
	text-indent:-36.0pt;}
@list l4:level5
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.";
	mso-level-tab-stop:126.0pt;
	mso-level-number-position:left;
	margin-left:126.0pt;
	text-indent:-54.0pt;}
@list l4:level6
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.";
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	margin-left:144.0pt;
	text-indent:-54.0pt;}
@list l4:level7
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.";
	mso-level-tab-stop:162.0pt;
	mso-level-number-position:left;
	margin-left:162.0pt;
	text-indent:-54.0pt;}
@list l4:level8
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.";
	mso-level-tab-stop:198.0pt;
	mso-level-number-position:left;
	margin-left:198.0pt;
	text-indent:-72.0pt;}
@list l4:level9
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.%9\.";
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	margin-left:216.0pt;
	text-indent:-72.0pt;}
@list l5
	{mso-list-id:163788531;
	mso-list-type:hybrid;
	mso-list-template-ids:-2106324344 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l5:level1
	{mso-level-tab-stop:85.5pt;
	mso-level-number-position:left;
	margin-left:85.5pt;
	text-indent:-18.0pt;}
@list l6
	{mso-list-id:271477536;
	mso-list-template-ids:276079468;}
@list l6:level1
	{mso-level-start-at:6;
	mso-level-tab-stop:18.0pt;
	mso-level-number-position:left;
	margin-left:18.0pt;
	text-indent:-18.0pt;}
@list l6:level2
	{mso-level-text:"%1\.%2\.";
	mso-level-tab-stop:18.0pt;
	mso-level-number-position:left;
	margin-left:18.0pt;
	text-indent:-18.0pt;}
@list l6:level3
	{mso-level-text:"%1\.%2\.%3\.";
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	margin-left:36.0pt;
	text-indent:-36.0pt;}
@list l6:level4
	{mso-level-text:"%1\.%2\.%3\.%4\.";
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	margin-left:36.0pt;
	text-indent:-36.0pt;}
@list l6:level5
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.";
	mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-54.0pt;}
@list l6:level6
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.";
	mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-54.0pt;}
@list l6:level7
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.";
	mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-54.0pt;}
@list l6:level8
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.";
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-72.0pt;}
@list l6:level9
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.%9\.";
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-72.0pt;}
@list l7
	{mso-list-id:349142868;
	mso-list-type:hybrid;
	mso-list-template-ids:544343240 -502348788 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l7:level1
	{mso-level-start-at:3;
	mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-18.0pt;}
@list l8
	{mso-list-id:371997920;
	mso-list-type:hybrid;
	mso-list-template-ids:-17918434 -560013346 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l8:level1
	{mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-18.0pt;}
@list l8:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	margin-left:108.0pt;
	text-indent:-18.0pt;}
@list l9
	{mso-list-id:440496698;
	mso-list-type:hybrid;
	mso-list-template-ids:-1242011142 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l9:level1
	{mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-18.0pt;}
@list l10
	{mso-list-id:486827154;
	mso-list-type:hybrid;
	mso-list-template-ids:-167478662 -1217350266 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l10:level1
	{mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-18.0pt;}
@list l11
	{mso-list-id:576944622;
	mso-list-type:hybrid;
	mso-list-template-ids:1876044130 -33499340 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l11:level1
	{mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l11:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l12
	{mso-list-id:624776262;
	mso-list-type:hybrid;
	mso-list-template-ids:2058122400 67698703 -512976482 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l12:level1
	{mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-18.0pt;}
@list l12:level2
	{mso-level-start-at:0;
	mso-level-number-format:bullet;
	mso-level-text:-;
	mso-level-tab-stop:90.0pt;
	mso-level-number-position:left;
	margin-left:90.0pt;
	text-indent:-18.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@list l13
	{mso-list-id:629364647;
	mso-list-type:hybrid;
	mso-list-template-ids:-1232829240 -118059442 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l13:level1
	{mso-level-start-at:0;
	mso-level-number-format:bullet;
	mso-level-text:-;
	mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-18.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@list l14
	{mso-list-id:644940152;
	mso-list-type:hybrid;
	mso-list-template-ids:481355802 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l14:level1
	{mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-18.0pt;}
@list l15
	{mso-list-id:747071272;
	mso-list-type:hybrid;
	mso-list-template-ids:-904502494 -512976482 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l15:level1
	{mso-level-start-at:0;
	mso-level-number-format:bullet;
	mso-level-text:-;
	mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-18.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@list l16
	{mso-list-id:767043595;
	mso-list-template-ids:442672660;}
@list l16:level1
	{mso-level-tab-stop:43.2pt;
	mso-level-number-position:left;
	margin-left:43.2pt;
	text-indent:-21.6pt;
	color:windowtext;}
@list l16:level2
	{mso-level-text:"%1\.%2";
	mso-level-tab-stop:50.4pt;
	mso-level-number-position:left;
	margin-left:50.4pt;
	text-indent:-28.8pt;
	color:windowtext;}
@list l16:level3
	{mso-level-text:"%1\.%2\.%3";
	mso-level-tab-stop:57.6pt;
	mso-level-number-position:left;
	margin-left:57.6pt;
	text-indent:-36.0pt;}
@list l16:level4
	{mso-level-text:"%1\.%2\.%3\.%4";
	mso-level-tab-stop:64.8pt;
	mso-level-number-position:left;
	margin-left:64.8pt;
	text-indent:-43.2pt;}
@list l16:level5
	{mso-level-text:"%1\.%2\.%3\.%4\.%5";
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-50.4pt;}
@list l16:level6
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6";
	mso-level-tab-stop:79.2pt;
	mso-level-number-position:left;
	margin-left:79.2pt;
	text-indent:-57.6pt;}
@list l16:level7
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7";
	mso-level-tab-stop:86.4pt;
	mso-level-number-position:left;
	margin-left:86.4pt;
	text-indent:-64.8pt;}
@list l16:level8
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8";
	mso-level-tab-stop:93.6pt;
	mso-level-number-position:left;
	margin-left:93.6pt;
	text-indent:-72.0pt;}
@list l16:level9
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.%9";
	mso-level-tab-stop:100.8pt;
	mso-level-number-position:left;
	margin-left:100.8pt;
	text-indent:-79.2pt;}
@list l17
	{mso-list-id:844591098;
	mso-list-type:hybrid;
	mso-list-template-ids:703608188 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l17:level1
	{mso-level-tab-stop:18.0pt;
	mso-level-number-position:left;
	margin-left:18.0pt;
	text-indent:-18.0pt;}
@list l18
	{mso-list-id:857739993;
	mso-list-type:hybrid;
	mso-list-template-ids:-1524069356 67698689 67698703 67698689 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l18:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	font-family:Symbol;}
@list l18:level2
	{mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l18:level3
	{mso-level-number-format:bullet;
	mso-level-text:\F0B7;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	font-family:Symbol;}
@list l19
	{mso-list-id:870991496;
	mso-list-type:hybrid;
	mso-list-template-ids:1267125144 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l19:level1
	{mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-18.0pt;}
@list l19:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:90.0pt;
	mso-level-number-position:left;
	margin-left:90.0pt;
	text-indent:-18.0pt;}
@list l20
	{mso-list-id:878737113;
	mso-list-type:hybrid;
	mso-list-template-ids:1748637706 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l20:level1
	{mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l21
	{mso-list-id:939723413;
	mso-list-template-ids:1331869828;}
@list l21:level1
	{mso-level-start-at:7;
	mso-level-tab-stop:18.0pt;
	mso-level-number-position:left;
	margin-left:18.0pt;
	text-indent:-18.0pt;}
@list l21:level2
	{mso-level-text:"%1\.%2\.";
	mso-level-tab-stop:18.0pt;
	mso-level-number-position:left;
	margin-left:18.0pt;
	text-indent:-18.0pt;}
@list l21:level3
	{mso-level-text:"%1\.%2\.%3\.";
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	margin-left:36.0pt;
	text-indent:-36.0pt;}
@list l21:level4
	{mso-level-text:"%1\.%2\.%3\.%4\.";
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	margin-left:36.0pt;
	text-indent:-36.0pt;}
@list l21:level5
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.";
	mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-54.0pt;}
@list l21:level6
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.";
	mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-54.0pt;}
@list l21:level7
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.";
	mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-54.0pt;}
@list l21:level8
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.";
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-72.0pt;}
@list l21:level9
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.%9\.";
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-72.0pt;}
@list l22
	{mso-list-id:973680860;
	mso-list-type:hybrid;
	mso-list-template-ids:1506034080 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l22:level1
	{mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-18.0pt;}
@list l22:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	margin-left:108.0pt;
	text-indent:-18.0pt;}
@list l23
	{mso-list-id:977490465;
	mso-list-template-ids:513973724;}
@list l23:level1
	{mso-level-text:%1;
	mso-level-tab-stop:18.0pt;
	mso-level-number-position:left;
	margin-left:18.0pt;
	text-indent:-18.0pt;
	mso-ansi-font-size:9.0pt;}
@list l23:level2
	{mso-level-start-at:2;
	mso-level-text:"%1\.%2";
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	margin-left:36.0pt;
	text-indent:-18.0pt;
	mso-ansi-font-size:9.0pt;}
@list l23:level3
	{mso-level-text:"%1\.%2\.%3";
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-36.0pt;
	mso-ansi-font-size:9.0pt;}
@list l23:level4
	{mso-level-text:"%1\.%2\.%3\.%4";
	mso-level-tab-stop:90.0pt;
	mso-level-number-position:left;
	margin-left:90.0pt;
	text-indent:-36.0pt;
	mso-ansi-font-size:9.0pt;}
@list l23:level5
	{mso-level-text:"%1\.%2\.%3\.%4\.%5";
	mso-level-tab-stop:126.0pt;
	mso-level-number-position:left;
	margin-left:126.0pt;
	text-indent:-54.0pt;
	mso-ansi-font-size:9.0pt;}
@list l23:level6
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6";
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	margin-left:144.0pt;
	text-indent:-54.0pt;
	mso-ansi-font-size:9.0pt;}
@list l23:level7
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7";
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	margin-left:180.0pt;
	text-indent:-72.0pt;
	mso-ansi-font-size:9.0pt;}
@list l23:level8
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8";
	mso-level-tab-stop:198.0pt;
	mso-level-number-position:left;
	margin-left:198.0pt;
	text-indent:-72.0pt;
	mso-ansi-font-size:9.0pt;}
@list l23:level9
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.%9";
	mso-level-tab-stop:234.0pt;
	mso-level-number-position:left;
	margin-left:234.0pt;
	text-indent:-90.0pt;
	mso-ansi-font-size:9.0pt;}
@list l24
	{mso-list-id:1024018742;
	mso-list-type:hybrid;
	mso-list-template-ids:-741473182 -512976482 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
@list l24:level1
	{mso-level-start-at:0;
	mso-level-number-format:bullet;
	mso-level-text:-;
	mso-level-tab-stop:90.0pt;
	mso-level-number-position:left;
	margin-left:90.0pt;
	text-indent:-18.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@list l25
	{mso-list-id:1067259952;
	mso-list-type:hybrid;
	mso-list-template-ids:-620209026 -1654361486 -1194046296 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l25:level1
	{mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-18.0pt;}
@list l25:level2
	{mso-level-tab-stop:90.0pt;
	mso-level-number-position:left;
	margin-left:90.0pt;
	text-indent:-18.0pt;}
@list l26
	{mso-list-id:1080057214;
	mso-list-type:hybrid;
	mso-list-template-ids:-738540416 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l26:level1
	{mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-18.0pt;}
@list l27
	{mso-list-id:1182818609;
	mso-list-type:hybrid;
	mso-list-template-ids:-1046044500 -1217350266 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l27:level1
	{mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-18.0pt;}
@list l27:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l28
	{mso-list-id:1184326954;
	mso-list-template-ids:-1362965562;}
@list l28:level1
	{mso-level-tab-stop:18.0pt;
	mso-level-number-position:left;
	margin-left:18.0pt;
	text-indent:-18.0pt;
	mso-ansi-font-size:9.0pt;}
@list l28:level2
	{mso-level-text:"%1\.%2\.";
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	margin-left:36.0pt;
	text-indent:-18.0pt;
	mso-ansi-font-size:9.0pt;}
@list l28:level3
	{mso-level-text:"%1\.%2\.%3\.";
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-36.0pt;
	mso-ansi-font-size:9.0pt;}
@list l28:level4
	{mso-level-text:"%1\.%2\.%3\.%4\.";
	mso-level-tab-stop:90.0pt;
	mso-level-number-position:left;
	margin-left:90.0pt;
	text-indent:-36.0pt;
	mso-ansi-font-size:9.0pt;}
@list l28:level5
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.";
	mso-level-tab-stop:126.0pt;
	mso-level-number-position:left;
	margin-left:126.0pt;
	text-indent:-54.0pt;
	mso-ansi-font-size:9.0pt;}
@list l28:level6
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.";
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	margin-left:144.0pt;
	text-indent:-54.0pt;
	mso-ansi-font-size:9.0pt;}
@list l28:level7
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.";
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	margin-left:180.0pt;
	text-indent:-72.0pt;
	mso-ansi-font-size:9.0pt;}
@list l28:level8
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.";
	mso-level-tab-stop:198.0pt;
	mso-level-number-position:left;
	margin-left:198.0pt;
	text-indent:-72.0pt;
	mso-ansi-font-size:9.0pt;}
@list l28:level9
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.%9\.";
	mso-level-tab-stop:234.0pt;
	mso-level-number-position:left;
	margin-left:234.0pt;
	text-indent:-90.0pt;
	mso-ansi-font-size:9.0pt;}
@list l29
	{mso-list-id:1212034960;
	mso-list-type:hybrid;
	mso-list-template-ids:-1287257622 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l29:level1
	{mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l30
	{mso-list-id:1267081247;
	mso-list-type:hybrid;
	mso-list-template-ids:461638194 1724646936 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l30:level1
	{mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-18.0pt;}
@list l31
	{mso-list-id:1280259788;
	mso-list-type:hybrid;
	mso-list-template-ids:-343524968 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l31:level1
	{mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-18.0pt;}
@list l32
	{mso-list-id:1399749805;
	mso-list-type:hybrid;
	mso-list-template-ids:-1183412116 -502348788 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l32:level1
	{mso-level-start-at:3;
	mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-18.0pt;}
@list l33
	{mso-list-id:1455323074;
	mso-list-type:hybrid;
	mso-list-template-ids:-17918434 67698693 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l33:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F0A7;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-18.0pt;
	font-family:Wingdings;}
@list l33:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	margin-left:108.0pt;
	text-indent:-18.0pt;}
@list l34
	{mso-list-id:1486816831;
	mso-list-type:hybrid;
	mso-list-template-ids:-17918434 1260429782 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l34:level1
	{mso-level-number-format:bullet;
	mso-level-text:\F071;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-18.0pt;
	mso-ansi-font-size:12.0pt;
	font-family:Wingdings;}
@list l34:level2
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	margin-left:108.0pt;
	text-indent:-18.0pt;}
@list l35
	{mso-list-id:1514760227;
	mso-list-template-ids:-1213565934;}
@list l35:level1
	{mso-level-start-at:7;
	mso-level-text:%1;
	mso-level-tab-stop:19.5pt;
	mso-level-number-position:left;
	margin-left:19.5pt;
	text-indent:-19.5pt;}
@list l35:level2
	{mso-level-text:"%1\.%2";
	mso-level-tab-stop:19.5pt;
	mso-level-number-position:left;
	margin-left:19.5pt;
	text-indent:-19.5pt;}
@list l35:level3
	{mso-level-text:"%1\.%2\.%3";
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	margin-left:36.0pt;
	text-indent:-36.0pt;}
@list l35:level4
	{mso-level-text:"%1\.%2\.%3\.%4";
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	margin-left:36.0pt;
	text-indent:-36.0pt;}
@list l35:level5
	{mso-level-text:"%1\.%2\.%3\.%4\.%5";
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	margin-left:36.0pt;
	text-indent:-36.0pt;}
@list l35:level6
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6";
	mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-54.0pt;}
@list l35:level7
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7";
	mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-54.0pt;}
@list l35:level8
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8";
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-72.0pt;}
@list l35:level9
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.%9";
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-72.0pt;}
@list l36
	{mso-list-id:1571188196;
	mso-list-type:hybrid;
	mso-list-template-ids:-883231552 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l36:level1
	{mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-18.0pt;}
@list l36:level2
	{mso-level-legal-format:yes;
	mso-level-text:"%1\.%2";
	mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-18.0pt;}
@list l36:level3
	{mso-level-legal-format:yes;
	mso-level-text:"%1\.%2\.%3";
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-36.0pt;}
@list l36:level4
	{mso-level-legal-format:yes;
	mso-level-text:"%1\.%2\.%3\.%4";
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-36.0pt;}
@list l36:level5
	{mso-level-legal-format:yes;
	mso-level-text:"%1\.%2\.%3\.%4\.%5";
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-36.0pt;}
@list l36:level6
	{mso-level-legal-format:yes;
	mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6";
	mso-level-tab-stop:90.0pt;
	mso-level-number-position:left;
	margin-left:90.0pt;
	text-indent:-54.0pt;}
@list l36:level7
	{mso-level-legal-format:yes;
	mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7";
	mso-level-tab-stop:90.0pt;
	mso-level-number-position:left;
	margin-left:90.0pt;
	text-indent:-54.0pt;}
@list l36:level8
	{mso-level-legal-format:yes;
	mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8";
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	margin-left:108.0pt;
	text-indent:-72.0pt;}
@list l36:level9
	{mso-level-legal-format:yes;
	mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.%9";
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	margin-left:108.0pt;
	text-indent:-72.0pt;}
@list l37
	{mso-list-id:1635257647;
	mso-list-type:hybrid;
	mso-list-template-ids:18616684 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l37:level1
	{mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-18.0pt;}
@list l38
	{mso-list-id:1656564484;
	mso-list-type:hybrid;
	mso-list-template-ids:-117041818 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l38:level1
	{mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;}
@list l39
	{mso-list-id:1844737796;
	mso-list-type:hybrid;
	mso-list-template-ids:1651024408 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l39:level1
	{mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-18.0pt;}
@list l40
	{mso-list-id:1874072849;
	mso-list-template-ids:-564776620;}
@list l40:level1
	{mso-level-start-at:7;
	mso-level-text:%1;
	mso-level-tab-stop:18.0pt;
	mso-level-number-position:left;
	margin-left:18.0pt;
	text-indent:-18.0pt;}
@list l40:level2
	{mso-level-start-at:2;
	mso-level-text:"%1\.%2";
	mso-level-tab-stop:18.0pt;
	mso-level-number-position:left;
	margin-left:18.0pt;
	text-indent:-18.0pt;}
@list l40:level3
	{mso-level-text:"%1\.%2\.%3";
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	margin-left:36.0pt;
	text-indent:-36.0pt;}
@list l40:level4
	{mso-level-text:"%1\.%2\.%3\.%4";
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	margin-left:36.0pt;
	text-indent:-36.0pt;}
@list l40:level5
	{mso-level-text:"%1\.%2\.%3\.%4\.%5";
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	margin-left:36.0pt;
	text-indent:-36.0pt;}
@list l40:level6
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6";
	mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-54.0pt;}
@list l40:level7
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7";
	mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-54.0pt;}
@list l40:level8
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8";
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-72.0pt;}
@list l40:level9
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.%9";
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-72.0pt;}
@list l41
	{mso-list-id:1882940860;
	mso-list-template-ids:1436088776;}
@list l41:level1
	{mso-level-start-at:2;
	mso-level-tab-stop:18.0pt;
	mso-level-number-position:left;
	margin-left:18.0pt;
	text-indent:-18.0pt;
	mso-ansi-font-size:9.0pt;}
@list l41:level2
	{mso-level-text:"%1\.%2\.";
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	margin-left:36.0pt;
	text-indent:-18.0pt;
	mso-ansi-font-size:9.0pt;}
@list l41:level3
	{mso-level-text:"%1\.%2\.%3\.";
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-36.0pt;
	mso-ansi-font-size:9.0pt;}
@list l41:level4
	{mso-level-text:"%1\.%2\.%3\.%4\.";
	mso-level-tab-stop:90.0pt;
	mso-level-number-position:left;
	margin-left:90.0pt;
	text-indent:-36.0pt;
	mso-ansi-font-size:9.0pt;}
@list l41:level5
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.";
	mso-level-tab-stop:126.0pt;
	mso-level-number-position:left;
	margin-left:126.0pt;
	text-indent:-54.0pt;
	mso-ansi-font-size:9.0pt;}
@list l41:level6
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.";
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	margin-left:144.0pt;
	text-indent:-54.0pt;
	mso-ansi-font-size:9.0pt;}
@list l41:level7
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.";
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	margin-left:180.0pt;
	text-indent:-72.0pt;
	mso-ansi-font-size:9.0pt;}
@list l41:level8
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.";
	mso-level-tab-stop:198.0pt;
	mso-level-number-position:left;
	margin-left:198.0pt;
	text-indent:-72.0pt;
	mso-ansi-font-size:9.0pt;}
@list l41:level9
	{mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.%9\.";
	mso-level-tab-stop:234.0pt;
	mso-level-number-position:left;
	margin-left:234.0pt;
	text-indent:-90.0pt;
	mso-ansi-font-size:9.0pt;}
@list l42
	{mso-list-id:1894540684;
	mso-list-type:hybrid;
	mso-list-template-ids:-674562706 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l42:level1
	{mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	margin-left:72.0pt;
	text-indent:-18.0pt;}
@list l43
	{mso-list-id:1941254586;
	mso-list-type:hybrid;
	mso-list-template-ids:-640411890 -230287696 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l43:level1
	{mso-level-start-at:7;
	mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-18.0pt;}
@list l44
	{mso-list-id:2120643505;
	mso-list-type:hybrid;
	mso-list-template-ids:543340358 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l44:level1
	{mso-level-tab-stop:54.0pt;
	mso-level-number-position:left;
	margin-left:54.0pt;
	text-indent:-18.0pt;}
ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman";}
</style>
<![endif]-->
</head>

<body lang=EN-US link=blue vlink=purple style='tab-interval:36.0pt'>

<div class=Section1>

<h1><span style='font-size:16.0pt;mso-bidi-font-size:12.0pt'>FREQUENTLY ASKED QUESTION (FAQ)<o:p></o:p></span></h1>

<h2><span class=GramE>DEPARTMENT :</span> CS FRONT LINERS</h2>

<p class=MsoNormal style='tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span>Nasabah.<o:p></o:p></span></p>

<p class=MsoNormal style='tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>CS<span style='mso-tab-count:1'>���� </span>:<span
style='mso-tab-count:1'>� </span></span><st1:PersonName><span style='font-size:
 10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>Customer Service</span></st1:PersonName><span
style='font-size:10.0pt;mso-bidi-font-size:12.0pt;font-family:Arial'>.<o:p></o:p></span></p>

<p class=MsoNormal style='tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>I<span style='mso-tab-count:1'>������� </span>:<span
style='mso-tab-count:1'>� </span>Instruksi untuk petugas CS.<o:p></o:p></span></p>

<p class=MsoNormal><span style='font-size:11.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<h2 style='text-align:justify'><span style='color:white;background:black;
mso-highlight:black'>BERKAITAN DENGAN APARAT MARKETING</span><span
style='color:white'><o:p></o:p></span></h2>

<p class=MsoNormal style='text-align:justify'><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Saya tidak dijelaskan manfaat polisnya
secara lengkap.</b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Bapak/Ibu,
pada saat permohonan diajukan dan proposal dibuat, Bapak/Ibu juga telah diberi
gambaran umum mengenai manfaat polis, Bapak/Ibu juga diminta untuk menanda
tangani proposal tersebut, sebagai pernyataan bahwa Bapak/Ibu mengerti isi dari
proposal tersebut. </p>

<p class=MsoBodyTextIndent style='text-align:justify'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:36.0pt'><span
style='mso-tab-count:1'>����������� </span><span class=GramE>Sebelum Bapak/Ibu
menandatangani Tanda Terima Polis, kami memberikan waktu kepada Bapak/Ibu agar
dapat membaca ulang dan mengerti isi polis.</span> Mungkin ada yang bisa kami <span
class=GramE>bantu</span> dimana letak ketidak jelasan Bapak/Ibu?</p>

<p class=MsoNormal style='text-align:justify'><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Kenapa produk yang saya miliki tidak sesuai
dengan kebutuhan saya?</b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Maaf,
Bapak/Ibu, apakah pada saat menerima polis, Bapak/Ibu sudah membaca ulang
manfaat polis yang ada? <span class=GramE>Atau mungkin ada yang bisa saya
jelaskan kembali?</span></p>

<p class=MsoBodyTextIndent style='text-align:justify'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
tab-stops:27.0pt 36.0pt'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Apakah Agen/Marketing yang menawari saya
produk ini masih bekerja di SinarmasMSIGlife?</b> <span class=GramE><b>Bisakah saya
menghubunginya?</b></span><b><o:p></o:p></b></p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
tab-stops:27.0pt 36.0pt'>CS<span style='mso-tab-count:1'>���� </span>:<span
style='mso-tab-count:1'>� </span>Bapak/Ibu, untuk Agen/Marketing tersebut saat
ini (masih/tidak) bekerja di perusahaan ini lagi, apabila ada pertanyaan dari
Bapak/Ibu mengenai polis, maka petugas CS kami <span class=GramE>akan</span>
siap membantu.</p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
tab-stops:27.0pt 36.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
tab-stops:27.0pt 36.0pt'>I<span style='mso-tab-count:1'>������� </span>:<span
style='mso-tab-count:1'>� </span>Apabila nasabah menanyakan nomor telepon
Agen/Marketing tersebut dan Agen/Marketing tersebut sudah tidak aktif.</p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
tab-stops:27.0pt 36.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
tab-stops:27.0pt 36.0pt'>CS<span style='mso-tab-count:1'>���� </span>:<span
style='mso-tab-count:1'>� </span>Untuk menghubunginya mohon maaf saat ini kami
belum bisa membantu, kami <span class=GramE>akan</span> melakukan konfirmasikan
terlebih dahulu kebagian terkait dan akan segera menghubungi Bapak/Ibu apabila
kami sudah mendapatkan nomor telepon yang bersangkutan.<span
style='mso-spacerun:yes'>�� </span></p>

<p class=MsoBodyTextIndent style='margin-top:0cm;margin-right:-7.55pt;
margin-bottom:0cm;margin-left:18.0pt;margin-bottom:.0001pt;text-align:justify;
text-indent:-18.0pt;tab-stops:36.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
tab-stops:27.0pt 36.0pt'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Mengapa saya tidak menerima </b><st1:City><st1:place><span
  class=GramE><b>surat</b></span></st1:place></st1:City><b> pemberitahuan jatuh
tempo pembayaran premi? <o:p></o:p></b></p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
tab-stops:27.0pt'>CS<span style='mso-tab-count:1'>���� </span>:<span
style='mso-tab-count:1'>� </span>Mohon maaf Bapak/Ibu, apakah alamat penagihan
sudah benar?<span style='mso-spacerun:yes'>� </span>Apakah masih <span
class=GramE>sama</span> dengan data yang tercantum pada database yang ada pada
kami?</p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
tab-stops:27.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
tab-stops:27.0pt'>I<span style='mso-tab-count:1'>������� </span>:<span
style='mso-tab-count:1'>� </span>Bacakan alamat customer di viewer, jika
alamatnya masih <span class=GramE>sama</span> dan benar, cek summary apakah ada
follow up mengenai <st1:City><st1:place>surat</st1:place></st1:City> jatuh
tempo yang kembali untuk daerah <st1:City><st1:place>Jakarta</st1:place></st1:City>
dan sekitarnya. <span class=GramE>Sedangkan untuk cabang, harus
mengkonfirmasikan terlebih dahulu ke bagian keuangan/finance.</span> </p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
tab-stops:27.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
text-indent:0cm;tab-stops:36.0pt'>Jika pernah ada follow up (berdasarkan
summary yang ada), jelaskan �Mohon maaf, Bapak/Ibu kami telah mengirimkan surat
pemberitahuan jatuh tempo premi ke alamat Bapak/Ibu namun surat tersebut
kembali dengan alasan (sebutkan alasannya, seperti pindah rumah, rumah dalam
keadaan kosong, alamat tidak lengkap, nama tidak dikenal, dll) dan kami telah
mencoba mengkonfirmasikan hal tersebut melalui telephone, namun tidak berhasil.
� </p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
text-indent:0cm;tab-stops:36.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
tab-stops:27.0pt 36.0pt'>CS<span style='mso-tab-count:1'>���� </span>:<span
style='mso-tab-count:1'>� </span>Apabila Bapak/Ibu <span class=GramE>akan</span>
melakukan pembayaran premi, maka dapat langsung melakukan pembayaran melalui
Bank yang ditunjuk. (<span class=GramE>sebutkan</span> nama Bank, dan no.
rekening perusahaan PT. AJ Sinarmas).</p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
text-indent:0cm;tab-stops:36.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
text-indent:0cm;tab-stops:36.0pt'>Jika tidak ada data follow up, maka jelaskan<span
class=GramE>: �</span>Mohon maaf, Bapak/Ibu kami telah mengirimkan <st1:City><st1:place>surat</st1:place></st1:City>
pemberitahuan jatuh tempo premi ke alamat Bapak/Ibu namun tidak ada informasi <st1:City><st1:place>surat</st1:place></st1:City>
tersebut kembali, mohon Bapak/Ibu mengkonfirmasikannya kembali pada pihak
keluarga dirumah�.<span style='mso-spacerun:yes'>����� </span></p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
text-indent:0cm;tab-stops:36.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
text-indent:0cm;tab-stops:36.0pt'>Apabila Customer tetap bersikeras dengan alasan
yang dapat diterima, maka petugas CS harus chek kembali pendistribusian <st1:City><st1:place><span
  class=GramE>surat</span></st1:place></st1:City> tsb dengan bagian terkait
(departemen Finance atau departemen GA).</p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
text-indent:0cm;tab-stops:36.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
text-indent:0cm;tab-stops:36.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
tab-stops:36.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
tab-stops:27.0pt 36.0pt'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Mengapa saya tidak dihubungi mengenai jatuh
tempo premi saya?</b> </p>

<p class=MsoBodyTextIndent style='margin-right:-7.55pt;text-align:justify;
tab-stops:27.0pt 36.0pt'>CS<span style='mso-tab-count:1'>���� </span>:<span
style='mso-tab-count:1'>� </span>Bapak/Ibu, untuk jatuh tempo premi Bapak/Ibu,
sudah kami kirimkan secara tertulis melalui <st1:City><st1:place><span
  class=GramE>surat</span></st1:place></st1:City> pemberitahuan jatuh tempo
premi dan melalui SMS yang kami kirim 7 (tujuh) hari sebelum dan saat jatuh
tempo premi. <span class=GramE>Kami tidak melakukan penagihan melalui telephone
atau kolektor.</span><span style='mso-spacerun:yes'>�� </span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Saya sudah membayar premi kepada Agen saya,
tetapi tidak dibayarkan kepada PT Asuransi Jiwa Sinarmas MSIG?</b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Terima
kasih atas pembayaran premi Bapak/Ibu, pembayaran premi yang sah adalah bila
dilakukan langsung ke rekening PT. Asuransi Jiwa Sinarmas MSIG, seperti yang diatur
dalam polis. Untuk menghindari hal yang tidak diinginkan, kami menghimbau agar
nasabah selalu melakukan transfer melalui Bank, kartu kredit atau ATM, dan
melakukan konformasi dengan mem-fax bukti setor pembayaran premi di no.
021-6257238. </p>

<p class=MsoBodyTextIndent style='text-align:justify'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Saya dijanjikan <span class=GramE>akan</span>
diberi discount untuk premi lanjutan.</b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Discount
pembayaran premi bergantung pada jenis produknya, dan <span class=GramE>akan</span>
diberikan oleh PT. AJ. <span class=GramE>Sinarmas jika memang ada, sesuai
dengan manfaat dari produk.</span></p>

<p class=MsoBodyTextIndent style='text-align:justify'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>N<span
style='mso-tab-count:1'>������ </span>:<span style='mso-tab-count:1'>� </span><b>Mengapa
Nilai Tunai yang saya peroleh tidak sebanding dengan premi yang sudah saya
bayarkan?</b> </p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Bapak/Ibu,
untuk penutupan polis sebelum masa kontrak asuransi berakhir, maka Nilai Tunai
yang Bapak/Ibu peroleh adalah Nilai Tunai sesuai ketentuan polis. <span
class=GramE>Untuk besarnya Nilai Tunai sudah diperhitungkan dengan kondisi
bahwa selama ini kami telah meng-cover atas jiwa tertanggung apabila terjadi
resiko pada Bapak/Ibu sebagai tertanggung.</span> Dan kami <span class=GramE>akan</span>
membayar sesuai dengan manfaat polis. (<span class=GramE>lihat</span> manfaat
polis jika tertanggung meninggal).</p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>N<span
style='mso-tab-count:1'>������ </span>:<span style='mso-tab-count:1'>� </span><b>Mengapa
hal ini tidak diinformasikan oleh Agen/Marketing saya?<o:p></o:p></b></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Bapak/Ibu,
untuk ketentuan tersebut, semua sudah diinformasikan secara lengkap di polis. <span
class=GramE>Saat menerima polis, kami memberikan waktu bagi Bapak/Ibu untuk
membaca dan menyetujui isi polis tersebut.</span><span
style='mso-spacerun:yes'>�� </span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<h2><span style='color:white;background:black;mso-highlight:black'>BERKAITAN
DENGAN PROSEDUR BACK-OFFICE</span><span style='color:white'><o:p></o:p></span></h2>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Saya tidak pernah dijelaskan bahwa jika
lewat masa tenggang waktu pembayaran premi, maka saya dikenakan denda bunga dan
bahkan pemulihan polis.</b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Hal
tersebut telah tercantum pada Syarat-syarat Umum Polis pada pasal yang mengatur
mengenai pembayaran premi terhenti dan Pemulihan Polis. <span class=GramE>Telah
diberikan masa leluasa pembayaran premi (Grace Period) sesuai dengan yang
tercantum pada polis.</span> </p>

<p class=MsoBodyTextIndent style='text-align:justify'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Bagaimana <span class=GramE>cara</span>
pemulihan polis?</b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Pemulihan
polis dapat dilakukan:</p>

<p class=MsoBodyTextIndent style='text-align:justify'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='text-align:justify;text-indent:0cm;
mso-list:l18 level1 lfo1;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol;mso-bidi-font-weight:bold'><span style='mso-list:Ignore'>�<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><b>Jika polis sudah lapse kurang dari 3 bulan
keterlambatan <o:p></o:p></b></p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:0cm;tab-stops:18.0pt 54.0pt'><span class=GramE>Membayar denda bunga
beserta preminya.</span></p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:0cm;tab-stops:18.0pt 54.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l18 level1 lfo1;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol'><span style='mso-list:Ignore'>�<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><b>Jika polis sudah lapse lebih dari 3 bulan,
petugas CS harus melihat pada tabel ketentuan Underwriting) dan melengkapi
persyaratan pemulihan polis sebagai berikut:</b></p>

<p class=MsoBodyTextIndent style='margin-left:72.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l18 level2 lfo1;tab-stops:54.0pt list 72.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Formulir
Pemulihan Polis yang diisi dan ditandatangani oleh pemegang polis (formulir
bisa didapat di <span class=GramE>kantor</span> cabang terdekat).</p>

<p class=MsoBodyTextIndent style='margin-left:72.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l18 level2 lfo1;tab-stops:54.0pt list 72.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
fotokopi Identitas Pemegang Polis yang masih berlaku (KTP/SIM/Pasport).</p>

<p class=MsoBodyTextIndent style='margin-left:72.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l18 level2 lfo1;tab-stops:54.0pt list 72.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
persyaratan medis, sesuai dengan ketentuan yg berlaku.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:0cm;tab-stops:54.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l18 level1 lfo1;tab-stops:list 54.0pt'><![if !supportLists]><span
style='font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol;mso-bidi-font-weight:bold'><span style='mso-list:Ignore'>�<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><b>Setelah selesai proses akseptasi, hasilnya
dapat berupa :<o:p></o:p></b></p>

<p class=MsoBodyTextIndent style='margin-left:72.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l8 level1 lfo31;tab-stops:54.0pt list 72.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Polis
kembali dipulihkan.</p>

<p class=MsoBodyTextIndent style='margin-left:72.0pt;text-align:justify;
text-indent:0cm;tab-stops:54.0pt'><span class=GramE>Nasabah mendapat </span><st1:City><st1:place><span
  class=GramE>Surat</span></st1:place></st1:City><span class=GramE> konfirmasi
pemulihan polis yang menyatakan polis sudah dapat dipulihkan kembali dan
sejumlah pembayaran premi tertunggak + bunga yang harus dibayar segera.</span></p>

<p class=MsoBodyTextIndent style='margin-left:72.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l8 level1 lfo31;tab-stops:54.0pt list 72.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Polis
dapat dipulihkan dengan <span class=GramE>dikenakan<span
style='mso-spacerun:yes'>� </span>extra</span> mortalita.</p>

<p class=MsoBodyTextIndent style='margin-left:72.0pt;text-align:justify;
text-indent:0cm;tab-stops:54.0pt'>Nasabah mendapat <st1:City><st1:place>Surat</st1:place></st1:City>
pemberitahuan hal <span class=GramE>jumlah<span style='mso-spacerun:yes'>�
</span>premi</span> extra mortalita yang harus dibayar ( diluar premi+ bunga).</p>

<p class=MsoBodyTextIndent style='margin-left:72.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l8 level1 lfo31;tab-stops:54.0pt list 72.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Ditolak
(polis tidak dapat dipulihkan).</p>

<p class=MsoBodyTextIndent style='margin-left:72.0pt;text-align:justify;
text-indent:0cm;tab-stops:54.0pt'><span class=GramE>Nasabah mendapat Surat
Penolakan yang isinya menyatakan polis tidak dapat dipulihkan.</span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:18.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Alamat penagihan saya tidak akurat/tidak
jelas.</b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Terima
kasih Bapak/Ibu, kami <span class=GramE>akan</span> mencocokkan kembali dengan
data Surat Permintaan Asuransi Jiwa yang pernah Bapak/Ibu isi. Untuk mengecek
ulang, kami <span class=GramE>akan</span> bacakan alamat penagihan Bapak/Ibu,
dan mengupdate dengan data terakhir.</p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>I<span
style='mso-tab-count:1'>������� </span>:<span style='mso-tab-count:1'>� </span>Petugas
CS wajib membacakan ulang semua data yang ada pada menu �Alamat Penagihan�,
termasuk no. HP dan alamat email.<span style='mso-spacerun:yes'>� </span>Jika
alamat email belum ada, tanyakan kembali alamat email pemegang polis, <span
class=GramE>dan</span> input ke dalam system. </p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Untuk
mengupdate alamat Bpk/Ibu, Bpk/Ibu dapat melakukan update <span class=GramE>melalui<span
style='mso-spacerun:yes'>� </span>internet</span> di situs <u><span
style='font-size:11.0pt;mso-bidi-font-size:12.0pt'><a
href="http://www.sinarmasmsiglife.co.id/E-Policy"><span style='color:windowtext'>www.sinarmasmsiglife.co.id/E-Policy</span></a>.</span></u></p>

<p class=MsoBodyTextIndent style='text-align:justify;text-indent:0cm;
tab-stops:36.0pt'>Dengan User: masukkan nomor identitas KTP/SIM/Pasport sesuai
dengan SPAJ dan Password: masukkan data tanggal lahir dengan format ddmmyyyy,
selanjutnya lakukan update data pada menu pembaharuan data.</p>

<p class=MsoBodyTextIndent style='text-align:justify'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Bagaimanakah <span class=GramE>cara</span>
mengajukan Klaim Tahapan? </b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Persyaratan
Klaim Tahapan adalah:</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l14 level1 lfo35;tab-stops:27.0pt 36.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Mengisi
&amp; menandatangani Formulir Konfirmasi Tahapan dengan lengkap.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l14 level1 lfo35;tab-stops:27.0pt 36.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
fotokopi identitas diri pemegang polis yang masih berlaku (KTP/SIM / Pasport).</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l14 level1 lfo35;tab-stops:27.0pt 36.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='mso-bidi-font-size:10.0pt'>Jika nomor rekening yang akan menerima
manfaat bukan atas nama pemegang polis, maka pemegang polis wajib m</span>engisi
dan menandatangani Surat Perintah Transfer* diatas <span class=GramE>materai<span
style='mso-spacerun:yes'>� </span>Rp</span>. 6.000.-, yang ditandatangani
dihadapan karyawan PT. AJ. Sinarmas (Branch Admin Cabang/CS/RM) dan melampirkan
fokotopi KTP penerima manfaat/transfer.</p>

<p class=MsoBodyTextIndent style='text-align:justify;text-indent:0cm;
tab-stops:18.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;tab-stops:36.0pt'><b><i><span style='font-size:9.0pt;
mso-bidi-font-size:12.0pt'>*)<span style='mso-tab-count:1'>���� </span></span></i></b><i>Surat
Perintah transfer<span style='mso-spacerun:yes'>� </span>harus ada persetujuan
dari direksi dan dilakukan konfirmasi ulang oleh petugas CS Kantor Pusat dan
dilengkapi berkas pendukung seperti kartu keluarga, akta nikah atau surat legal
lainnya yang menunjukkan hubungan penerima manfaat/transfer dengan pemegang
polis.<o:p></o:p></i></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Bagaimanakah <span class=GramE>cara</span>
mengajukan Klaim Nilai Tunai? </b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span><b>Persyaratan
Klaim Nilai Tunai untuk produk konvesional adalah:<o:p></o:p></b></p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l27 level1 lfo34;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
polis asli.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l27 level1 lfo34;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Pemegang
polis mengisi &amp; menandatangani Formulir Pengajuan Pemutusan Kontrak
Asuransi.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l27 level1 lfo34;tab-stops:27.0pt 36.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='mso-bidi-font-size:10.0pt'>Jika nomor rekening yang akan menerima
manfaat bukan atas nama pemegang polis, maka pemegang polis wajib m</span>engisi
dan menandatangani Surat Perintah Transfer* diatas <span class=GramE>materai<span
style='mso-spacerun:yes'>� </span>Rp</span>. 6.000.-, yang ditandatangani
dihadapan karyawan PT. AJ. Sinarmas (Branch Admin Cabang/CS/RM) dan melampirkan
fokotopi KTP penerima manfaat/transfer.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l27 level1 lfo34;tab-stops:list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Pemegang
polis menandatangani Formulir Perubahan Nomor Rekening* <span class=GramE>yang<span
style='mso-spacerun:yes'>� </span>ditandatangani</span> dihadapan karyawan PT.
AJ. Sinarmas (Branch Admin/CS/RM).</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l27 level1 lfo34;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
fokotopi identitas diri pemegang polis (KTP/SIM/ Pasport) yang masih berlaku.</p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:18.0pt'><b><o:p>&nbsp;</o:p></b></p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;tab-stops:54.0pt'><span class=GramE><i><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt'>*)<span style='mso-tab-count:
1'>���� </span></span></i></span><st1:City><st1:place><span class=GramE><i>Surat</i></span></st1:place></st1:City><span
class=GramE><i> Perintah transfer harus ada persetujuan dari direksi dan
dilakukan konfirmasi ulang oleh petugas CS Kantor Pusat.</i></span><i> Dan
dilengkapi berkas pendukung seperti kartu keluarga, akta nikah atau </i><st1:City><st1:place><span
  class=GramE><i>surat</i></span></st1:place></st1:City><i> legal lainnya yang
menunjukkan hubungan penerima manfaat/transfer dengan pemegang polis.<o:p></o:p></i></p>

<p class=MsoBodyTextIndent style='margin-left:45.0pt;text-align:justify;
text-indent:-9.0pt;tab-stops:36.0pt'><i><o:p>&nbsp;</o:p></i></p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;tab-stops:54.0pt'><i>**)<span style='mso-tab-count:1'>�� </span>Formulir
perubahan nomor rekening digunakan jika nomor rekening yang dicantumkan berbeda
dengan SPAJ (untuk saat ini berlaku hanya untuk polis jenis: unit link &amp;
investasi) dan harus <span class=GramE>sama</span> dengan nomor rekening yang
tercantum pada </i><st1:City><st1:place><i>Surat</i></st1:place></st1:City><i>
Perintah Transfer. <span class=GramE>Petugas CS melakukan validasi kepada
nasabah dan diajukan untuk diketahui dan disetujui oleh direksi.</span><o:p></o:p></i></p>

<p class=MsoBodyTextIndent style='margin-left:9.0pt;text-align:justify;
text-indent:-9.0pt;tab-stops:36.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='margin-left:9.0pt;text-align:justify;
text-indent:-9.0pt;tab-stops:36.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='margin-left:9.0pt;text-align:justify;
text-indent:-9.0pt;tab-stops:36.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='text-align:justify;text-indent:0cm;
tab-stops:36.0pt'><b>Persyaratan Klaim Nilai Tunai untuk produk Unit Link
adalah:<o:p></o:p></b></p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l11 level1 lfo27;tab-stops:36.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Selain
kelengkapan berkas yang tercantum pada point 1-5 diatas, pemegang polis wajib
mengisi dan menanda tangani Formulir Pengajuan Transaksi Produk Unit Link yang
menyatakan pengambilan seluruh unit yang ada. Formulir dan kelengkapan lainnya
tersebut di-fax terlebih dahulu ke no. 021-6257238.</p>

<p class=MsoBodyTextIndent style='margin-left:0cm;text-align:justify;
text-indent:0cm;tab-stops:36.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>I<span
style='mso-tab-count:1'>������� </span>:<span style='mso-tab-count:1'>� </span>Formulir
Pengajuan Transaksi Produk Unit Link yang sudah diberi catatan oleh Branch
Admin yang menyatakan telah menerima polis asli beserta kelengkapannya dapat
langsung diteruskan ke departemen Life Benefit untuk diproses, dan berkas asli
harus dikirimkan ke PT. AJ Sinarmas Kantor Pusat atau melalui kantor Cabang
terdekat.</p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:18.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='text-align:justify;text-indent:0cm;
tab-stops:36.0pt'><b>Persyaratan Klaim Nilai Tunai untuk produk Power Save
adalah:</b><u><o:p></o:p></u></p>

<p class=MsoBodyTextIndent style='text-align:justify;text-indent:0cm;
tab-stops:36.0pt'>Selain kelengkapan berkas yang tercantum pada point 1-5
diatas, pemegang polis wajib mengisi dan menanda tangani Surat Perjanjian
Hutang (SPH) tanpa materai. </p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:18.0pt'><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Bagaimana <span class=GramE>cara</span>
mengajukan Klaim Habis Kontrak? </b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Persyaratan
Klaim Habis Kontrak adalah:</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l10 level1 lfo41;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
polis Asli.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l10 level1 lfo41;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Pemegang
polis mengisi &amp; menandatangani Formulir Pengajuan Klaim Habis Kontrak.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l10 level1 lfo41;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
fotokopi identitas diri pemegang polis yang masih berlaku (Fotocopy
KTP/SIM/Pasport).</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l14 level1 lfo35;tab-stops:27.0pt 36.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='mso-bidi-font-size:10.0pt'>Jika nomor rekening yang akan menerima
manfaat bukan atas nama pemegang polis, maka pemegang polis wajib m</span>engisi
dan menandatangani Surat Perintah Transfer* diatas <span class=GramE>materai<span
style='mso-spacerun:yes'>� </span>Rp</span>. 6.000.-, yang ditandatangani
dihadapan karyawan PT. AJ. Sinarmas (Branch Admin Cabang/CS/RM) dan melampirkan
fokotopi KTP penerima manfaat/transfer.</p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:18.0pt'><s><o:p><span
 style='text-decoration:none'>&nbsp;</span></o:p></s></p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;tab-stops:54.0pt'><span class=GramE><i><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt'>*)<span style='mso-tab-count:
1'>���� </span></span></i></span><st1:City><st1:place><span class=GramE><i>Surat</i></span></st1:place></st1:City><span
class=GramE><i> Perintah transfer harus ada persetujuan dari direksi dan
dilakukan konfirmasi ulang oleh petugas CS Kantor Pusat.</i></span><i> Dan
dilengkapi berkas pendukung seperti kartu keluarga, akta nikah atau </i><st1:City><st1:place><span
  class=GramE><i>surat</i></span></st1:place></st1:City><i> legal lainnya yang
menunjukkan hubungan penerima manfaat/transfer dengan pemegang polis.<o:p></o:p></i></p>

<p class=MsoBodyTextIndent style='text-align:justify;text-indent:0cm;
tab-stops:18.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Bagaimana <span class=GramE>cara</span>
mengajukan Pinjaman Nilai Tunai (produk konvensional)? </b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Persyaratan
Pinjaman Nilai Tunai adalah:</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l22 level1 lfo16;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
polis asli.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l22 level1 lfo16;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Pemegang
polis mengisi &amp; menandatangani Formulir Permohonan Pinjaman Nilai Tunai. </p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l22 level1 lfo16;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Pemegang
polis menandatangani Surat Perjanjian Pinjaman Polis (SPH) yang dibuat diatas kertas
segel dan diberi materai Rp. 6.000.-</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l22 level1 lfo16;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Biaya
administrasi pinjaman untuk pertanggungan dalam mata uang Dollar adalah adalah
US$ 10, dan pertanggungan dalam mata uang Rupiah adalah Rp. 25.000.- </p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l22 level1 lfo16;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><i><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span></i><![endif]>Melampirkan
fotokopi identitas diri pemegang polis (KTP/SIM/Pasport) yang masih berlaku<span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt'>.</span><i><o:p></o:p></i></p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l22 level1 lfo16;tab-stops:27.0pt 36.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>6.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='mso-bidi-font-size:10.0pt'>Jika nomor rekening yang akan menerima
manfaat bukan atas nama pemegang polis, maka pemegang polis wajib m</span>engisi
dan menandatangani Surat Perintah Transfer* diatas <span class=GramE>materai<span
style='mso-spacerun:yes'>� </span>Rp</span>. 6.000.-, yang ditandatangani
dihadapan karyawan PT. AJ. Sinarmas (Branch Admin Cabang/CS/RM) dan melampirkan
fokotopi KTP penerima manfaat/transfer.</p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:18.0pt'><s><o:p><span
 style='text-decoration:none'>&nbsp;</span></o:p></s></p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;tab-stops:54.0pt'><span class=GramE><i><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt'>*)<span style='mso-tab-count:
1'>���� </span></span></i></span><st1:City><st1:place><span class=GramE><i>Surat</i></span></st1:place></st1:City><span
class=GramE><i> Perintah transfer harus ada persetujuan dari direksi dan
dilakukan konfirmasi ulang oleh petugas CS Kantor Pusat.</i></span><i> Dan
dilengkapi berkas pendukung seperti kartu keluarga, akta nikah atau </i><st1:City><st1:place><span
  class=GramE><i>surat</i></span></st1:place></st1:City><i> legal lainnya yang
menunjukkan hubungan penerima manfaat/transfer dengan pemegang polis.<o:p></o:p></i></p>

<p class=MsoBodyTextIndent style='margin-left:9.0pt;text-align:justify;
text-indent:-9.0pt;tab-stops:36.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:9.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>�������� </span>:<span
style='mso-tab-count:1'>�� </span></span><b><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Arial'>Bagaimana proses Top-Up dan
Penarikan Unit-Link? <o:p></o:p></span></b></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>CS<span style='mso-tab-count:1'>���� </span>:<span
style='mso-tab-count:1'>� </span>Persyaratan pengajuan Top-Up Unit Link:<o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l12 level1 lfo6;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Mengisi
&amp; menandatangani Formulir Pengajuan Transaksi Top <span class=GramE>Up</span>
Produk Unit-Link.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l12 level1 lfo6;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
fotokopi identitas diri pemegang polis yang masih berlaku (KTP/SIM/Pasport).</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l12 level1 lfo6;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
bukti setor premi Top Up.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l12 level1 lfo6;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Kelengkapan
berkas di-fax ke no. 021-6257238. </p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:18.0pt'><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt'><o:p>&nbsp;</o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt'><span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt'>N<span style='mso-tab-count:
1'>�������� </span>:<span style='mso-tab-count:1'>�� </span></span><b>Bagaimana
<span class=GramE>cara</span> pengajuan Withdrawal/Switching?</b> <span
style='font-size:9.0pt;mso-bidi-font-size:12.0pt'><span
style='mso-spacerun:yes'>���</span><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Persyaratan
pengajuan Withdrawal (Penarikan Dana Sebagian).</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l25 level1 lfo25;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Mengisi
&amp; menandatangani Formulir Pengajuan Transaksi Unit Link.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l25 level1 lfo25;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
fotokopi identitas diri pemegang polis yang masih berlaku (KTP/SIM/Pasport).</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l25 level1 lfo25;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><b><i><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span></i></b><![endif]>Melampirkan
Polis Asli.<b><i><o:p></o:p></i></b></p>

<p class=MsoBodyTextIndent style='text-align:justify;text-indent:0cm;
tab-stops:18.0pt'>Kelengkapan pendukung lainnya:<b><i><o:p></o:p></i></b></p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l25 level1 lfo25;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><st1:City><st1:place>Surat</st1:place></st1:City>
Perintah Transfer.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l25 level1 lfo25;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Form
Perubahan Rekening.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l25 level1 lfo25;tab-stops:list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>6.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Formulir
Pengajuan Transaksi Produk Unit Link &amp; dokumen pendukung lainnya di fax
terlebih dahulu ke no. 021-6257238.</p>

<p class=MsoBodyTextIndent style='text-align:justify;text-indent:0cm;
tab-stops:36.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Persyaratan
pengajuan Switching.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l25 level2 lfo25;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Mengisi
&amp; menandatangani Formulir Pengajuan Transaksi Unit Link.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l25 level2 lfo25;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
fotokopi identitas diri pemegang polis yang masih berlaku (KTP/SIM/Pasport).</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l25 level2 lfo25;tab-stops:list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Formulir
Pengajuan Transaksi Produk Unit Link &amp; dokumen pendukung lainnya di fax
terlebih dahulu ke no. 021-6257238.</p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:18.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Bagaimana memperpanjang Polis Super
Sehat/Super Sehat Plus/Super Protection saya?</b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Proses
untuk memperpanjang Polis Super Sehat/Super Sehat Plus/Super Protection:</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l37 level1 lfo8;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Mengisi
&amp; menanda tangani SPAJ Super sehat/Super Sehat Plus/Super Protection.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l37 level1 lfo8;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
fotokopi identitas diri pemegang polis yang masih berlaku (KTP/SIM/Pasport).</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l37 level1 lfo8;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
bukti pembayaran premi dalam masa grace period.</p>

<p class=MsoBodyTextIndent style='text-align:justify;text-indent:0cm;
tab-stops:18.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Bagaimana melakukan perubahan alamat/ahli
waris/nama dan hal-hal lain?</b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Proses
perubahan polis (Endorsement) dan caranya adalah sebagai berikut:</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l31 level1 lfo9;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Pemegang
polis mengisi &amp; menandatangani Formulir Permohonan Perubahan Polis.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l31 level1 lfo9;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
fotokopi identitas diri pemegang polis yang masih berlaku (KTP/SIM/Pasport).</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l31 level1 lfo9;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
dokumen penunjang lainnya sesuai dengan kebutuhan, misalnya: fotokopi Kartu
Keluarga, Akte Lahir, Akte Nikah, Surat Keterangan Pelunasan dari Bank, dll.</p>

<p class=MsoBodyTextIndent style='text-align:justify;text-indent:0cm;
tab-stops:18.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<a name="_Toc173576721"><span
style='mso-tab-count:1'>� </span>Untuk </a><span style='mso-bookmark:_Toc173576721'><span
lang=PT-BR style='mso-ansi-language:PT-BR'>Perubahan Nama/Ganti Nama (pada
orang yang <span class=GramE>sama</span>)</span></span><span lang=PT-BR
style='mso-ansi-language:PT-BR'>.<o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l26 level1 lfo44;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
fotokopi identitas diri pemegang polis yang masih berlaku (KTP/SIM/Pasport).</p>

<p class=x1 style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:54.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;mso-list:l26 level1 lfo44;
tab-stops:list 54.0pt left 67.5pt'><![if !supportLists]><span lang=EN-GB
style='font-size:10.0pt;mso-bidi-font-size:7.5pt;font-family:Arial;mso-fareast-font-family:
Arial;color:windowtext'><span style='mso-list:Ignore'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN-GB style='font-size:10.0pt;
mso-bidi-font-size:7.5pt;font-family:Arial;color:windowtext'>Formulir
Permohonan Perubahan Polis.<o:p></o:p></span></p>

<p class=x1 style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:54.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;mso-list:l26 level1 lfo44;
tab-stops:list 54.0pt left 67.5pt'><![if !supportLists]><span lang=EN-GB
style='font-size:10.0pt;mso-bidi-font-size:7.5pt;font-family:Arial;mso-fareast-font-family:
Arial;color:windowtext'><span style='mso-list:Ignore'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><st1:City><st1:place><span lang=EN-GB
  style='font-size:10.0pt;mso-bidi-font-size:7.5pt;font-family:Arial;
  color:windowtext'>Surat</span></st1:place></st1:City><span lang=EN-GB
style='font-size:10.0pt;mso-bidi-font-size:7.5pt;font-family:Arial;color:windowtext'>
Ganti Nama.<o:p></o:p></span></p>

<p class=x1 style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:54.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;mso-list:l26 level1 lfo44;
tab-stops:list 54.0pt left 67.5pt'><![if !supportLists]><span lang=EN-GB
style='font-size:10.0pt;mso-bidi-font-size:7.5pt;font-family:Arial;mso-fareast-font-family:
Arial;color:windowtext'><span style='mso-list:Ignore'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><st1:City><st1:place><span lang=EN-GB
  style='font-size:10.0pt;mso-bidi-font-size:7.5pt;font-family:Arial;
  color:windowtext'>Surat</span></st1:place></st1:City><span lang=EN-GB
style='font-size:10.0pt;mso-bidi-font-size:7.5pt;font-family:Arial;color:windowtext'>
Pernyataan Perbedaan Nama Pemegang Polis/Tertanggung.<o:p></o:p></span></p>

<p class=x1 style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:54.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;mso-list:l26 level1 lfo44;
tab-stops:list 54.0pt left 67.5pt'><![if !supportLists]><span lang=EN-GB
style='font-size:10.0pt;mso-bidi-font-size:7.5pt;font-family:Arial;mso-fareast-font-family:
Arial;color:windowtext'><span style='mso-list:Ignore'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><span lang=EN-GB style='font-size:10.0pt;
mso-bidi-font-size:7.5pt;font-family:Arial;color:windowtext'>Kartu
keluarga/akte lahir/akte perkawinan.<o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='margin-left:4.5pt;text-align:justify;
text-indent:0cm;tab-stops:18.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Bagaimana <span class=GramE>cara</span>
pembuatan Polis Duplikat?</b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Cara
pembuatan Polis Duplikat adalah sebagai <span class=GramE>berikut :</span></p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l1 level1 lfo10;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
Surat Lapor Kehilangan Polis dari kepolisian yang dilaporkan oleh pemegang
polis.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l1 level1 lfo10;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Pemegang
polis mengisi &amp; menandatangani Surat Pernyataan Kehilangan dan Permohonan
Pembuatan Polis Duplikat diatas materai Rp. 6000,-<s><o:p></o:p></s></p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l1 level1 lfo10;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
fotokopi identitas diri pemegang polis yang masih berlaku (KTP/SIM/Pasport).</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l1 level1 lfo10;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Membayar
biaya cetak Polis sebesar Rp. 25.000<span class=GramE>.-</span> atau US$ 5
untuk jenis polis <st1:country-region><st1:place>US</st1:place></st1:country-region>$.</p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:18.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Dapatkah saya membayar premi secara
otomatis dengan Kartu Kredit?</b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='margin-left:0cm;text-align:justify;
text-indent:0cm;tab-stops:0cm 27.0pt 36.0pt'>CS<span style='mso-tab-count:1'>���� </span>:<span
style='mso-tab-count:1'>� </span>Dapat, Bapak/Ibu cukup melakukan hal-hal
sebagai berikut:</p>

<p class=MsoBodyTextIndent style='text-align:justify;text-indent:0cm;
mso-list:l38 level1 lfo11;tab-stops:0cm 18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Mengisi
dan menandatangani Formulir Pendebetan Kartu Kredit oleh pemilik kartu kredit.</p>

<p class=MsoBodyTextIndent style='text-align:justify;text-indent:0cm;
mso-list:l38 level1 lfo11;tab-stops:0cm 18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
fotokopi KTP pemilik kartu kredit yang masih berlaku.</p>

<p class=MsoBodyTextIndent style='text-align:justify;text-indent:0cm;
mso-list:l38 level1 lfo11;tab-stops:0cm 18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
fotokopi kartu kredit yang <span class=GramE>akan</span> didebet.</p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Dapatkah saya membayar premi dengan Kartu
Kredit melalui telephone <span class=GramE>( tidak</span> otomatis)?</b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='margin-left:0cm;text-align:justify;
text-indent:0cm;tab-stops:0cm 27.0pt 36.0pt'>CS<span style='mso-tab-count:1'>���� </span>:<span
style='mso-tab-count:1'>� </span>Dapat, Bapak/Ibu hanya cukup melakukan hal-hal
sebagai berikut:</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l30 level1 lfo12;tab-stops:0cm 18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Menyebutkan
<span class=GramE>nama</span> bank asal kartu kredit, contoh BII Visa atau BII
Master.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l30 level1 lfo12;tab-stops:0cm 18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Menyebutkan
<span class=GramE>nama</span> pemilik kartu kredit.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l30 level1 lfo12;tab-stops:0cm 18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Menyebutkan
16 angka depan kartu kredit.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l30 level1 lfo12;tab-stops:0cm 18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Menyebutkan
masa berlaku kartu kredit.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l30 level1 lfo12;tab-stops:0cm 18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Menyebutkan
3 angka terakhir dibalik kartu kredit.</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l30 level1 lfo12;tab-stops:0cm 18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>6.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Menyebutkan
tanggal <span class=GramE>akan</span> dilakukannya pendebetan oleh PT. AJ
Sinarmas.</p>

<p class=MsoBodyTextIndent style='text-align:justify;text-indent:0cm;
tab-stops:0cm 18.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><i><span style='font-size:7.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></i></p>

<h2><span style='color:white;background:black;mso-highlight:black'>BERKAITAN
DENGAN KEBIJAKSANAAN PERUSAHAAN</span><span style='color:white'><o:p></o:p></span></h2>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Mengapa denda bunga yang dikenakan sangat
tinggi?</b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Mohon
maaf sebelumnya Bapak/Ibu. <span class=GramE>Untuk penetapan besarnya denda
bunga ini, kami mengacu pada bunga pinjaman bank yang ada di pasaran saat ini.</span>
<span class=GramE>Oleh karena itu, untuk menghindari denda bunga, kami
menghimbau agar pembayaran premi dilakukan tepat waktu.</span></p>

<p class=MsoBodyTextIndent style='text-align:justify'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Tapi saya tidak pernah menerima tagihan!</b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span><st1:City><st1:place>Surat</st1:place></st1:City>
Jatuh Tempo Premi kami kirimkan 2 (dua) bulan sebelum premi polis Bapak/Ibu
jatuh <span class=GramE>tempo</span>. Selain itu jika data HP Bapak/Ibu
tercatat lengkap, maka secara otomatis Bapak/Ibu juga <span class=GramE>akan</span>
dikirimi SMS pemberitahuan premi jatuh tempo. </p>

<p class=MsoBodyTextIndent style='text-align:justify'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='text-align:justify'>I<span style='mso-tab-count:
1'>���� </span>:<span style='mso-tab-count:1'>���� </span>Jika data HP belum
lengkap, petugas CS langsung menanyakan kepada nasabah untuk melengkapinya dan
lakukan update pada database.</p>

<p class=MsoBodyTextIndent style='text-align:justify'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Dapatkah dibebaskan dari denda bunga?</b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Mohon
maaf sebelumnya Bapak/Ibu. <span class=GramE>Denda bunga keterlambatan ini
tidak terkecuali.</span> <span class=GramE>Dengan sangat menyesal kami belum
bisa membebaskan denda bunga Bapak/Ibu.</span></p>

<p class=MsoBodyTextIndent style='text-align:justify'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Kenapa baru terlambat 3 bulan lebih, saya
sudah diminta medical check-up ulang?</b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Mohon
maaf sebelumnya Bapak/Ibu. <span class=GramE>Ketentuan perusahaan menetapkan
pemeriksaan kesehatan ulang bagi polis yang tidak aktif diatas 3 bulan.</span>
Ini pun bergantung dari jumlah uang pertanggungan, <span class=GramE>usia</span>,
dan lamanya polis tidak aktif. <span class=GramE>Kami mohon Bapak/Ibu dapat
memakluminya.</span></p>

<p class=MsoNormal><b><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:white;background:black;mso-highlight:black'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal><b><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:white;background:black;mso-highlight:black'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNormal><b><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial;color:white;background:black;mso-highlight:black'>BERKAITAN
DENGAN PRODUK</span></b><b><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial;color:white'><o:p></o:p></span></b></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Apakah saya dapat merubah produk yang saya
miliki?</b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span></p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l33 level1 lfo39;tab-stops:list 54.0pt'><![if !supportLists]><span
style='font-family:Wingdings;mso-fareast-font-family:Wingdings;mso-bidi-font-family:
Wingdings;mso-bidi-font-weight:bold'><span style='mso-list:Ignore'>�<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><b>Jika produk tidak dapat dikonversikan atau
pemegang polis rugi.<o:p></o:p></b></p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:0cm;tab-stops:list 54.0pt'><span class=GramE>Bapak/Ibu, pada
dasarnya semua produk dapat diubah/dikonversikan.</span> <span class=GramE>Akan
tetapi pada beberapa jenis produk, jika diubah justru tidak menguntungkan bagi
pemegang polis.</span> <span class=GramE>Dalam hal ini, kebetulan sekali polis
Bapak/Ibu ini kurang menguntungkan jika dikonversikan, sehingga kami sarankan
untuk tidak dikonversikan.</span></p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;tab-stops:36.0pt list 54.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l33 level1 lfo39;tab-stops:list 54.0pt'><![if !supportLists]><span
style='font-family:Wingdings;mso-fareast-font-family:Wingdings;mso-bidi-font-family:
Wingdings;mso-bidi-font-weight:bold'><span style='mso-list:Ignore'>�<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><b>Jika produk dapat dikonversikan. <o:p></o:p></b></p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:0cm;tab-stops:list 54.0pt'><span class=GramE>Ya, Bapak/Ibu dapat
mengkonversikan produk yang dimiliki.</span> Untuk menghitung ulang kelayakan
produk, jika Bapak/Ibu berminat, kami <span class=GramE>akan</span> meminta
bagian terkait untuk menghitungnya terlebih dahulu dan Bapak/Ibu bisa
mempelajari perhitungan tersebut. Apabila Bapak/Ibu setuju dengan perhitungan
produk baru, maka kami <span class=GramE>akan</span> mengkonversikan produk
yang Bapak/Ibu miliki menjadi produk baru dengan mengajukan perubahan polis dan
melengkapi persyaratan perubahan polis. </p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:0cm'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Bagaimana caranya melakukan perubahan
produk?</b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Cara
untuk melakukan perubahan produk adalah:</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l29 level1 lfo7;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Pemegang
polis mengisi Formulir Permohonan Perubahan Polis (setelah dihitungkan oleh
bagian Aktuaria).</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l29 level1 lfo7;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
polis asli (polis lama).</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l29 level1 lfo7;tab-stops:18.0pt list 36.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Melampirkan
fotokopi identitas diri pemegang polis yang masih berlaku (KTP/SIM/Pasport).</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l29 level1 lfo7;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Membayar
biaya cetak polis Rp. 20.000.-</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l29 level1 lfo7;tab-stops:18.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Jika
ada, membayar kekurangan premi sesuai perhitungan Perusahaan.</p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Mengapa produk Asuransi Sinarmas rumit
sekali?</b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Mohon
maaf sebelumnya Bapak/Ibu. <st1:City><st1:place><span class=GramE>Ada</span></st1:place></st1:City><span
class=GramE> beberapa produk unggulan kami dan tidak semuanya rumit.</span> <span
class=GramE>Kebetulan sekali penjelasan kami ini dilakukan melalui telepon,
sehingga kelihatannya rumit.</span> <span class=GramE>Akan tetapi jika
Bapak/Ibu ada waktu luang dan bisa membaca kembali Manfaat Produk yang
tercantum di polis, mungkin produk ini menjadi lebih mudah dimengerti.</span></p>

<p class=MsoBodyTextIndent style='text-align:justify'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Bagaimana mau dibaca, Syarat-syarat Umum
Polis Asuransi Jiwa Sinarmas banyak dan tulisannya kecil-kecil!</b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Mohon
maaf sebelumnya Bapak/Ibu. <span class=GramE>Karena Syarat-syarat Umum Polis
memuat banyak hal tentang aturan polis, dan tidak memungkinkan untuk dicetak
terlalu besar.</span> Jika Bapak/Ibu ada waktu luang dan bisa membaca kembali
Syarat-syarat Umum Polis tsb, mungkin <span class=GramE>akan</span> menjadi
lebih mudah dimengerti.</p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<h2><span style='color:white;background:black;mso-highlight:black'>BERKAITAN
DENGAN INTERAKSI KEPADA MASYARAKAT LUAS</span><span style='color:white'><o:p></o:p></span></h2>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Kok Asuransi Sinarmas tidak pernah
beriklan? <span class=GramE>Perusahaannya kecil ya?</span></b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Terima
kasih atas masukkan dari Bapak/Ibu. <span class=GramE>Mungkin Bapak/Ibu
kebetulan jarang melihat iklan kami?</span> <span class=GramE>Sebetulnya kami
juga beriklan di beberapa media, seperti media investasi Infobank, Proteksi,
Investor, bahkan harian Kompas.</span> Akan tetapi infromasi dari Bapak/Ibu ini
<span class=GramE>akan</span> menjadi masukan yang berharga bagi kami. <span
class=GramE>Perusahaan kami adalah 100% milik Sinar Mas Group, salah satu
konglomerat terbesar di </span><st1:country-region><st1:place><span
  class=GramE>Indonesia</span></st1:place></st1:country-region><span
class=GramE>.</span></p>

<p class=MsoBodyTextIndent style='text-align:justify'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-indent:
-36.0pt;tab-stops:27.0pt 36.0pt'><span style='font-size:10.0pt;mso-bidi-font-size:
12.0pt;font-family:Arial'>N<span style='mso-tab-count:1'>������ </span>:<span
style='mso-tab-count:1'>� </span><b>Bagaimana keadaan Sinar Mas Group sekarang?</b><o:p></o:p></span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Saat
ini Sinar Mas Group semakin eksis, tidak hanya di <st1:country-region><st1:place>Indonesia</st1:place></st1:country-region>,
melainkan juga di luar negeri. Pilar utama Sinar Mas Group terdiri dari: </p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l19 level1 lfo45;tab-stops:27.0pt 36.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Perkebunan
Kelapa Sawit (Smart, produknya: Filma).</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l19 level1 lfo45;tab-stops:27.0pt 36.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Pulp
&amp; Paper (APP, produknya: Tissue Paseo, kertas Sinar Dunia, dll).</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l19 level1 lfo45;tab-stops:27.0pt 36.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Properti
(Duta Pertiwi, proyeknya: Kota Wisata, BSD, Legenda Wisata, dll).</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l19 level1 lfo45;tab-stops:27.0pt 36.0pt list 54.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Serta
lembaga keuangan, seperti:</p>

<p class=MsoBodyTextIndent style='margin-left:72.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l19 level2 lfo45;tab-stops:27.0pt 36.0pt list 72.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>a.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Asuransi
Jiwa Sinarmas.</p>

<p class=MsoBodyTextIndent style='margin-left:72.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l19 level2 lfo45;tab-stops:27.0pt 36.0pt list 72.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>b.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Asuransi
Kerugian (Asuransi Sinar Mas).</p>

<p class=MsoBodyTextIndent style='margin-left:72.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l19 level2 lfo45;tab-stops:27.0pt 36.0pt list 72.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>c.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Bank
Sinarmas.</p>

<p class=MsoBodyTextIndent style='margin-left:72.0pt;text-align:justify;
text-indent:-18.0pt;mso-list:l19 level2 lfo45;tab-stops:27.0pt 36.0pt list 72.0pt'><![if !supportLists]><span
style='mso-fareast-font-family:Arial'><span style='mso-list:Ignore'>d.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Dan
Sekuritas (Sinarmas Sekuritas).</p>

<p class=MsoBodyTextIndent style='margin-left:54.0pt;text-align:justify;
text-indent:0cm;tab-stops:27.0pt 36.0pt'><o:p>&nbsp;</o:p></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>N<span
style='mso-tab-count:1'>������ </span>:<span style='mso-tab-count:1'>� </span>Bagaimana
status polis saya sehubungan dengan perubahan <span class=GramE>nama</span>
perusahaan dari PT. AJ. <span class=GramE>Ekalife menjadi PT. AJ.</span> <span
class=GramE>Sinarmas?</span></p>

<p class=MsoBodyTextIndent style='text-align:justify;tab-stops:27.0pt 36.0pt'>CS<span
style='mso-tab-count:1'>���� </span>:<span style='mso-tab-count:1'>� </span>Bapak/Ibu,
perubahan <span class=GramE>nama</span> tersebut tidak akan berpengaruh
terhadap status polis Bapak/Ibu.</p>

<p class=MsoBodyTextIndent style='text-align:justify'><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><i><span style='font-size:7.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'>FAQ <span class=GramE>Update :</span> 16 Januari 2006/MS.-<o:p></o:p></span></i></p>

<p class=MsoBodyTextIndent style='text-align:justify'><i><span
style='font-size:7.0pt;mso-bidi-font-size:12.0pt'>Revisi ke-1:<span
style='mso-spacerun:yes'>� </span></span></i><st1:date Year="2000" Day="25"
Month="9"><i><span style='font-size:7.0pt;mso-bidi-font-size:12.0pt'>25
 September 2006/Eri</span></i></st1:date><i><span style='font-size:7.0pt;
mso-bidi-font-size:12.0pt'>. Rika <o:p></o:p></span></i></p>

<p class=MsoBodyTextIndent style='text-align:justify'><i><span
style='font-size:7.0pt;mso-bidi-font-size:12.0pt'>Revisi ke-2:<span
style='mso-spacerun:yes'>� </span></span></i><st1:date Year="2007" Day="4"
Month="9"><i><span style='font-size:7.0pt;mso-bidi-font-size:12.0pt'>04
 September 2007</span></i></st1:date><i><span style='font-size:7.0pt;
mso-bidi-font-size:12.0pt'>/ Eri. Mega<o:p></o:p></span></i></p>

<p class=MsoBodyTextIndent style='text-align:justify'><i><span
style='font-size:7.0pt;mso-bidi-font-size:12.0pt'>Revisi ke-3:<span
style='mso-spacerun:yes'>� </span>10 Oktober 2007/Yune<o:p></o:p></span></i></p>

<p class=MsoBodyTextIndent style='text-align:justify'><i><span
style='font-size:7.0pt;mso-bidi-font-size:12.0pt'>Revisi ke-4: </span></i><st1:date
Year="2000" Day="1" Month="11"><i><span style='font-size:7.0pt;mso-bidi-font-size:
 12.0pt'>1 November 2007/Arief</span></i></st1:date></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span style='font-size:10.0pt;mso-bidi-font-size:12.0pt;
font-family:Arial'><o:p>&nbsp;</o:p></span></p>

</div>

</body>

</html>
