<%@ include file="/include/page/header.jsp"%>
<BODY>
<div id="contents">
		<table>
			<tr>
				<td>
					<div id="error">
						<c:choose>
							<c:when test="${param.jenis eq \"redirect\"}">
								Maaf tapi session anda sudah tidak aktif, anda akan diarahkan ke halaman utama dalam 1 detik
								<br><br><input type="button" value="Login" onclick="timeOut(self, '${path}', 0)">
								<script>
								timeOut(self, '${path}', 2000);
								</script>							
							</c:when>
							<c:when test="${param.jenis eq \"pegaprod\" }">
								Maaf POLIS ini merupakan POLIS PEGA, silahkan melakukan pengaksesan di PEGA System. Terima kasih.
							</c:when>
							<c:when test="${param.jenis eq \"blocked\" }">
								Saat ini aplikasi sedang diblokir. Silahkan hubungi EDP untuk konfirmasi. Terima kasih.
							</c:when>
							<c:when test="${param.jenis eq \"branch\" }">
								Maaf tetapi anda tidak mempunyai akses untuk melihat POLIS ini. Terima kasih.
							</c:when>
							<c:when test="${param.jenis eq \"uw\" }">
								Maaf tetapi menu ini hanya bisa diakses oleh UNDERWRITING. Terima kasih.
							</c:when>
							<c:when test="${param.jenis eq \"uat\" }">
								Maaf tetapi produk ini belum melalui User Acceptance Test / UAT (Belum di-support oleh aplikasi).
								<br>Silahkan konfirmasi dengan EDP. Terima kasih.
							</c:when>
							<c:when test="${param.jenis eq \"sph\" }">
								Maaf anda tidak mempunyai akses ke menu ini, bisa karena : 
								<br>- Anda tidak diberikan akses ke menu ini, atau
								<br>- Nomor register SPAJ yang anda masukkan bukan produk POWER SAVE.
							</c:when>
							<c:when test="${param.jenis eq \"referral\" }">
								Referrall Bank hanya digunakan untuk produk Bancassurance. Terima kasih.
							</c:when>
							<c:when test="${param.jenis eq \"otorisasi\" }">
							Maaf anda tidak mempunyai akses ke menu ini
							</c:when>
							<c:when test="${param.jenis eq \"process\" }">
								Maaf anda gagal mengakses ke proses ini, bisa karena : 
								<br>- Anda tidak diberikan akses ke proses ini, atau
								<br>- Link ke proses ini sudah tidak berlaku lagi.
							</c:when>
							<c:otherwise>

							</c:otherwise>
						</c:choose>
					</div>
				</td>
			</tr>
		</table>
</div>
</BODY>
<%@ include file="/include/page/footer.jsp"%>