<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<html>
<head>
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
</head>
<body>
<jsp:plugin type="applet" code="com/ekalife/applet/jasper/JasperViewerApplet.class"
	codebase="/E-Lions/include/applets" name="viewer"
	archive="
		/E-Lions/include/applets/JasperViewerApplet.jar,
		/E-Lions/include/applets/jasperreports-1.2.4.jar,
		/E-Lions/include/applets/jasperreports-1.2.4-applet.jar"
	align="middle" height="100%" width="100%" 
	nspluginurl="http://java.sun.com/javase/downloads/index.jsp"
	iepluginurl="http://java.sun.com/javase/downloads/index.jsp">
	<jsp:params>
		<jsp:param name="ZOOM_INDEX" value="${param.zoom_index}" />
		<jsp:param name="REPORT_URL" value="${param.report_url}" />
		<jsp:param name="SERVER_PATH" value="http://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}" />
	</jsp:params>
	<jsp:fallback>Apabila anda mengalami kesulitan harap menghubungi EDP</jsp:fallback>
</jsp:plugin>
</body>
</html>