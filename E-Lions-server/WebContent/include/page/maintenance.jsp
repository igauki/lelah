<%@ include file="/include/page/header.jsp"%>
<BODY>
<div id="contents">
		<table>
			<tr>
				<td>
					<div id="error">
						<c:choose>
							<c:when test="${param.jenis eq \"redirect\"}">
								Maaf tapi session anda sudah tidak aktif, anda akan diarahkan ke halaman utama dalam 1 detik
								<br><br><input type="button" value="Login" onclick="timeOut(self, '${path}', 0)">
								<script>
								timeOut(self, '${path}', 2000);
								</script>							
							</c:when>
							<c:when test="${param.jenis eq \"maintenance\" }">
								Maaf, Saat ini aplikasi sedang dalam Maintenance. Terima kasih.
							</c:when>
							<c:otherwise>

							</c:otherwise>
						</c:choose>
					</div>
				</td>
			</tr>
		</table>
</div>
</BODY>
<%@ include file="/include/page/footer.jsp"%>