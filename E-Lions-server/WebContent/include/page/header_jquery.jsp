<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>PT. Asuransi Jiwa Sinarmas MSIG</title>

<%-- jQuery UI 1.8.14 + jQuery 1.5.1 (24 Jul 2011) --%>
<link type="text/css" href="${path}/include/jquery/css/smoothness/jquery-ui-1.8.14.custom.css" rel="stylesheet" />
<script type="text/javascript" src="${path}/include/jquery/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="${path}/include/jquery/jquery-ui-1.8.14.custom.min.js"></script>

<%-- qtip2 (24 Jul 2011) (http://craigsworks.com/projects/qtip2/download) --%>
<link rel="stylesheet" href="${path}/include/jquery/qtip2/jquery.qtip.min.css" type="text/css" media="all" />
<script type="text/javascript" src="${path}/include/jquery/qtip2/jquery.qtip.min.js"></script>

<%--jQuery Form Plugin (26 Jul 2011) (http://jquery.malsup.com/form/#download) --%>
<script type="text/javascript" src="${path}/include/jquery/jquery.form.js"></script>

<%--jQuery Validation Plugin (27 Jul 2011) (http://bassistance.de/jquery-plugins/jquery-plugin-validation/) --%>
<script type="text/javascript" src="${path}/include/jquery/jquery.validate.min.js"></script>

<%--theme switcher untuk preview saja (FIXME remove nanti) 
<link type="text/css" rel="stylesheet" href="http://jqueryui.com/themes/base/ui.all.css" />
--%>

<%--theme priceformat --%>
<script type="text/javascript" src="${path}/include/jquery/jquery.price_format.2.0.js"></script>
<script type="text/javascript" src="${path}/include/jquery/jquery.price_format.2.0.min.js"></script>



</head>