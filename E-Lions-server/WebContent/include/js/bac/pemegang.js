function body_onload_sub_1(){
	if (frmParam.elements['datausulan.specta_save'].value == '1'){
		if(frmParam.specta_save1) frmParam.specta_save1.checked = true;
	}else{
		if(frmParam.specta_save1) frmParam.specta_save1.checked = false;
	}
	frmParam.alamat_sementara.style.display="none";
	frmParam.alamat_sementara1.style.display="none";
	frmParam.alamat_sementara2.style.display="none";
}

function body_onload_sub_2(){
	if(frmParam.elements['datausulan.jenis_pemegang_polis'].value == 1){
		frmParam.elements['contactPerson.mste_age'].style.backgroundColor ='#D4D4D4';
	}else{
		frmParam.elements['pemegang.mste_age'].style.backgroundColor ='#D4D4D4';
	}
	
	var flag_worksite = frmParam.elements['datausulan.flag_worksite'].value;
	
	if (((document.frmParam.elements['pemegang.mkl_tujuan'].value).toUpperCase() !=("Lain - Lain").toUpperCase()) && (document.frmParam.elements['pemegang.mkl_tujuan'].value!=null) )
	{
		document.frmParam.elements['pemegang.tujuana'].value='';
	}else{
		if ((document.frmParam.elements['pemegang.tujuana'].value).toUpperCase() == ("Lain - Lain").toUpperCase())
		{
			document.frmParam.elements['pemegang.tujuana'].value='';
		}
	}
	
	if  (((document.frmParam.elements['pemegang.mkl_pendanaan'].value).toUpperCase() != ("Lainnya").toUpperCase()) && (document.frmParam.elements['pemegang.mkl_pendanaan'].value!=null))
	{
		document.frmParam.elements['pemegang.danaa'].value='';
	}else{
		if ((document.frmParam.elements['pemegang.danaa'].value).toUpperCase() ==("Lainnya").toUpperCase())
		{
			document.frmParam.elements['pemegang.danaa'].value='';
		}
	}
	if (frmParam.elements['datausulan.jenis_pemegang_polis'].value == '1'){
		if ((document.frmParam.elements['addressbilling.msap_address'].value.toUpperCase()==document.frmParam.elements['contactPerson.alamat_rumah'].value.toUpperCase()) && (document.frmParam.elements['addressbilling.msap_zip_code'].value==document.frmParam.elements['contactPerson.kd_pos_rumah'].value) && (document.frmParam.elements['addressbilling.msap_area_code1'].value==document.frmParam.elements['contactPerson.area_code_rumah'].value) && (document.frmParam.elements['addressbilling.msap_phone1'].value==document.frmParam.elements['contactPerson.telpon_rumah'].value) )
		{
			document.frmParam.elements['addressbilling.tagih'].value='2';
		}else{
			if ((document.frmParam.elements['addressbilling.msap_address'].value==document.frmParam.elements['contactPerson.alamat_kantor'].value) && (document.frmParam.elements['addressbilling.msap_zip_code'].value==document.frmParam.elements['contactPerson.kd_pos_kantor'].value) && (document.frmParam.elements['addressbilling.msap_area_code1'].value==document.frmParam.elements['contactPerson.area_code_kantor'].value) && (document.frmParam.elements['addressbilling.msap_phone1'].value==document.frmParam.elements['contactPerson.telpon_kantor'].value) )
			{
				document.frmParam.elements['addressbilling.tagih'].value='3';
			}else{
				document.frmParam.elements['addressbilling.tagih'].value='1';
			}		
		}
	}else{
		if  (((document.frmParam.elements['pemegang.mkl_smbr_penghasilan'].value).toUpperCase() != ("Lainnya").toUpperCase()) && (document.frmParam.elements['pemegang.mkl_smbr_penghasilan'].value!=null))
		{
			document.frmParam.elements['pemegang.shasil'].value='';
		}else{
			if ((document.frmParam.elements['pemegang.shasil'].value).toUpperCase() ==("Lainnya").toUpperCase())
			{
				document.frmParam.elements['pemegang.shasil'].value='';
			}
		}
		if  (((document.frmParam.elements['pemegang.mkl_kerja'].value).toUpperCase()!=("Lainnya").toUpperCase()) && (document.frmParam.elements['pemegang.mkl_kerja'].value!=null))
		{
			document.frmParam.elements['pemegang.kerjaa'].value='';
		}else{
			if ((document.frmParam.elements['pemegang.kerjaa'].value).toUpperCase()==("Lainnya").toUpperCase())
			{
				document.frmParam.elements['pemegang.kerjaa'].value='';
			}
		}

		if (((document.frmParam.elements['pemegang.mkl_kerja'].value).toUpperCase()!=("Karyawan Swasta").toUpperCase()) && (document.frmParam.elements['pemegang.mkl_kerja'].value!=null))
		{
			document.frmParam.elements['pemegang.kerjab'].value='';
		}else{
			if ((document.frmParam.elements['pemegang.kerjab'].value).toUpperCase()==("Karyawan Swasta").toUpperCase())
			{
				document.frmParam.elements['pemegang.kerjab'].value='';
			} 
		}
		if ((document.frmParam.elements['addressbilling.msap_address'].value.toUpperCase()==document.frmParam.elements['pemegang.alamat_rumah'].value.toUpperCase()) && (document.frmParam.elements['addressbilling.msap_zip_code'].value==document.frmParam.elements['pemegang.kd_pos_rumah'].value) && (document.frmParam.elements['addressbilling.msap_area_code1'].value==document.frmParam.elements['pemegang.area_code_rumah'].value) && (document.frmParam.elements['addressbilling.msap_phone1'].value==document.frmParam.elements['pemegang.telpon_rumah'].value) )
		{
			document.frmParam.elements['addressbilling.tagih'].value='2';
		}else{
			if ((document.frmParam.elements['addressbilling.msap_address'].value==document.frmParam.elements['pemegang.alamat_kantor'].value) && (document.frmParam.elements['addressbilling.msap_zip_code'].value==document.frmParam.elements['pemegang.kd_pos_kantor'].value) && (document.frmParam.elements['addressbilling.msap_area_code1'].value==document.frmParam.elements['pemegang.area_code_kantor'].value) && (document.frmParam.elements['addressbilling.msap_phone1'].value==document.frmParam.elements['pemegang.telpon_kantor'].value) )
			{
				document.frmParam.elements['addressbilling.tagih'].value='3';
			}else{
				document.frmParam.elements['addressbilling.tagih'].value='1';
			}		
		}
	}
	
	loaddata_penagihan();
	
	if  (((document.frmParam.elements['pemegang.mkl_industri'].value).toUpperCase()!=("Lainnya").toUpperCase()) && (document.frmParam.elements['pemegang.mkl_industri'].value!=null))
	{
		document.frmParam.elements['pemegang.industria'].value='';
	}else{
		if  ((document.frmParam.elements['pemegang.industria'].value).toUpperCase()==("Lainnya").toUpperCase())
		{
			document.frmParam.elements['pemegang.industria'].value='';
		}
	}
	
	document.frmParam.carisumber.value='';
}

function data_penagihan() 
	{
		 if (document.frmParam.elements['addressbilling.tagih'].value == "1")
			{
					document.frmParam.elements['addressbilling.kota_tgh'].value='';
					document.frmParam.elements['addressbilling.kota_tgh'].disabled = false;
					document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#FFFFFF';
						
					document.frmParam.elements['addressbilling.msap_address'].readOnly = false;
					document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#FFFFFF';
					document.frmParam.elements['addressbilling.msap_address'].value='';
					document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = false;
					document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#FFFFFF';						
				 	document.frmParam.elements['addressbilling.msap_zip_code'].value='';
					document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = false;
					document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#FFFFFF';						
				 	document.frmParam.elements['addressbilling.msap_area_code1'].value='';
					document.frmParam.elements['addressbilling.msap_phone1'].readOnly = false;
					document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#FFFFFF';						
					document.frmParam.elements['addressbilling.msap_phone1'].value='';
					document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = false;
					document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#FFFFFF';						
				 	document.frmParam.elements['addressbilling.msap_area_code2'].value='';
					document.frmParam.elements['addressbilling.msap_phone2'].readOnly = false;
					document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#FFFFFF';						
					document.frmParam.elements['addressbilling.msap_phone2'].value='';
					document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = false;
					document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#FFFFFF';						
				 	document.frmParam.elements['addressbilling.msap_area_code3'].value='';
					document.frmParam.elements['addressbilling.msap_phone3'].readOnly = false;
					document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#FFFFFF';						
					document.frmParam.elements['addressbilling.msap_phone3'].value='';
					document.frmParam.elements['addressbilling.no_hp'].value=document.frmParam.elements['pemegang.no_hp'].value;
					document.frmParam.elements['addressbilling.no_hp2'].value=document.frmParam.elements['pemegang.no_hp2'].value;
		}else{
					if (document.frmParam.elements['addressbilling.tagih'].value=="2")
					{
							if (frmParam.elements['datausulan.jenis_pemegang_polis'].value == '1'){
								document.frmParam.elements['addressbilling.kota_tgh'].value=document.frmParam.elements['contactPerson.kota_rumah'].value;
								document.frmParam.elements['addressbilling.kota_tgh'].disabled = true;
								document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D4D4D4';
							
								document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
								document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D4D4D4';
								document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.elements['contactPerson.alamat_rumah'].value;
							
								document.frmParam.elements['addressbilling.msap_zip_code'].value=document.frmParam.elements['contactPerson.kd_pos_rumah'].value;		
								document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
								document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D4D4D4';
									
								document.frmParam.elements['addressbilling.msap_area_code1'].value=document.frmParam.elements['contactPerson.area_code_rumah'].value;		
								document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
								document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D4D4D4';
									
								document.frmParam.elements['addressbilling.msap_phone1'].value=document.frmParam.elements['contactPerson.telpon_rumah'].value;
								document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
								document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D4D4D4';
								
								document.frmParam.elements['addressbilling.msap_area_code2'].value=document.frmParam.elements['contactPerson.area_code_rumah2'].value;		
								document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;
								document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D4D4D4';
									
								document.frmParam.elements['addressbilling.msap_phone2'].value=document.frmParam.elements['contactPerson.telpon_rumah2'].value;
								document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
								document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D4D4D4';	
									
								document.frmParam.elements['addressbilling.msap_area_code3'].value='';		
								document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
								document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D4D4D4';
								
								document.frmParam.elements['addressbilling.msap_phone3'].value='';
								document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
								document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D4D4D4';
								
								document.frmParam.elements['addressbilling.no_hp'].value=document.frmParam.elements['pemegang.no_hp'].value;
								document.frmParam.elements['addressbilling.no_hp2'].value=document.frmParam.elements['pemegang.no_hp2'].value;
							}else{
								document.frmParam.elements['addressbilling.kota_tgh'].value=document.frmParam.elements['pemegang.kota_rumah'].value;
								document.frmParam.elements['addressbilling.kota_tgh'].disabled = true;
								document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D4D4D4';
							
								document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
								document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D4D4D4';
								document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.elements['pemegang.alamat_rumah'].value;
							
								document.frmParam.elements['addressbilling.msap_zip_code'].value=document.frmParam.elements['pemegang.kd_pos_rumah'].value;		
								document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
								document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D4D4D4';
									
								document.frmParam.elements['addressbilling.msap_area_code1'].value=document.frmParam.elements['pemegang.area_code_rumah'].value;		
								document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
								document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D4D4D4';
									
								document.frmParam.elements['addressbilling.msap_phone1'].value=document.frmParam.elements['pemegang.telpon_rumah'].value;
								document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
								document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D4D4D4';
								
								document.frmParam.elements['addressbilling.msap_area_code2'].value=document.frmParam.elements['pemegang.area_code_rumah2'].value;		
								document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;
								document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D4D4D4';
									
								document.frmParam.elements['addressbilling.msap_phone2'].value=document.frmParam.elements['pemegang.telpon_rumah2'].value;
								document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
								document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D4D4D4';	
									
								document.frmParam.elements['addressbilling.msap_area_code3'].value='';		
								document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
								document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D4D4D4';
								
								document.frmParam.elements['addressbilling.msap_phone3'].value='';
								document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
								document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D4D4D4';	
								
								document.frmParam.elements['addressbilling.no_hp'].value=document.frmParam.elements['pemegang.no_hp'].value;
								document.frmParam.elements['addressbilling.no_hp2'].value=document.frmParam.elements['pemegang.no_hp2'].value;
							}
								
					}else{
							if (document.frmParam.elements['addressbilling.tagih'].value=="3")
							{
								if (frmParam.elements['datausulan.jenis_pemegang_polis'].value == '1'){
									document.frmParam.elements['addressbilling.kota_tgh'].value=document.frmParam.elements['contactPerson.kota_kantor'].value;
									document.frmParam.elements['addressbilling.kota_tgh'].disabled = true;
									document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D4D4D4';
									
									document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D4D4D4';
									document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.elements['contactPerson.alamat_kantor'].value;
											
									document.frmParam.elements['addressbilling.msap_zip_code'].value=document.frmParam.elements['contactPerson.kd_pos_kantor'].value;		
									document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D4D4D4';
											
									document.frmParam.elements['addressbilling.msap_area_code1'].value=document.frmParam.elements['contactPerson.area_code_kantor'].value;		
									document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D4D4D4';
											
									document.frmParam.elements['addressbilling.msap_phone1'].value=document.frmParam.elements['contactPerson.telpon_kantor'].value;
									document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D4D4D4';
										
									document.frmParam.elements['addressbilling.msap_area_code2'].value=document.frmParam.elements['contactPerson.area_code_kantor2'].value;		
									document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D4D4D4';
											
									document.frmParam.elements['addressbilling.msap_phone2'].value=document.frmParam.elements['contactPerson.telpon_kantor2'].value;
									document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D4D4D4';
										
									document.frmParam.elements['addressbilling.msap_area_code3'].value='';		
									document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D4D4D4';
											
									document.frmParam.elements['addressbilling.msap_phone3'].value='';
									document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D4D4D4';		
									
									document.frmParam.elements['addressbilling.no_hp'].value=document.frmParam.elements['pemegang.no_hp'].value;
									document.frmParam.elements['addressbilling.no_hp2'].value=document.frmParam.elements['pemegang.no_hp2'].value;
								}else{
									document.frmParam.elements['addressbilling.kota_tgh'].value=document.frmParam.elements['pemegang.kota_kantor'].value;
									document.frmParam.elements['addressbilling.kota_tgh'].disabled = true;
									document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D4D4D4';
									
									document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D4D4D4';
									document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.elements['pemegang.alamat_kantor'].value;
											
									document.frmParam.elements['addressbilling.msap_zip_code'].value=document.frmParam.elements['pemegang.kd_pos_kantor'].value;		
									document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D4D4D4';
											
									document.frmParam.elements['addressbilling.msap_area_code1'].value=document.frmParam.elements['pemegang.area_code_kantor'].value;		
									document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D4D4D4';
											
									document.frmParam.elements['addressbilling.msap_phone1'].value=document.frmParam.elements['pemegang.telpon_kantor'].value;
									document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D4D4D4';
										
									document.frmParam.elements['addressbilling.msap_area_code2'].value=document.frmParam.elements['pemegang.area_code_kantor2'].value;		
									document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D4D4D4';
											
									document.frmParam.elements['addressbilling.msap_phone2'].value=document.frmParam.elements['pemegang.telpon_kantor2'].value;
									document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D4D4D4';
										
									document.frmParam.elements['addressbilling.msap_area_code3'].value='';		
									document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D4D4D4';
											
									document.frmParam.elements['addressbilling.msap_phone3'].value='';
									document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D4D4D4';	
									
									document.frmParam.elements['addressbilling.no_hp'].value=document.frmParam.elements['pemegang.no_hp'].value;
									document.frmParam.elements['addressbilling.no_hp2'].value=document.frmParam.elements['pemegang.no_hp2'].value;
								}															
							}else{
								if (document.frmParam.elements['addressbilling.tagih'].value=="4")
								{
									document.frmParam.elements['addressbilling.kota_tgh'].value=document.frmParam.elements['pemegang.kota_tpt_tinggal'].value;
									document.frmParam.elements['addressbilling.kota_tgh'].disabled = true;
									document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D4D4D4';
								
									document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D4D4D4';
									document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.elements['pemegang.alamat_tpt_tinggal'].value;
								
									document.frmParam.elements['addressbilling.msap_zip_code'].value=document.frmParam.elements['pemegang.kd_pos_tpt_tinggal'].value;		
									document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D4D4D4';
										
									document.frmParam.elements['addressbilling.msap_area_code1'].value=document.frmParam.elements['pemegang.area_code_rumah'].value;		
									document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D4D4D4';
										
									document.frmParam.elements['addressbilling.msap_phone1'].value=document.frmParam.elements['pemegang.telpon_rumah'].value;
									document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D4D4D4';
									
									document.frmParam.elements['addressbilling.msap_area_code2'].value=document.frmParam.elements['pemegang.area_code_rumah2'].value;		
									document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D4D4D4';
										
									document.frmParam.elements['addressbilling.msap_phone2'].value=document.frmParam.elements['pemegang.telpon_rumah2'].value;
									document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D4D4D4';	
										
									document.frmParam.elements['addressbilling.msap_area_code3'].value='';		
									document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D4D4D4';
									
									document.frmParam.elements['addressbilling.msap_phone3'].value='';
									document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
									document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D4D4D4';	
									
									document.frmParam.elements['addressbilling.no_hp'].value=document.frmParam.elements['pemegang.no_hp'].value;
									document.frmParam.elements['addressbilling.no_hp2'].value=document.frmParam.elements['pemegang.no_hp2'].value;
								}
								
								
							}						
						
					}		
			}
		}	
		
function isi()
{
	document.frmParam.elements['pemegang.mcl_first'].value = 'PT. GUTHRIE  PECCONINA  INDONESIA';
	document.frmParam.elements['pemegang.mcl_gelar'].value='';
	document.frmParam.elements['pemegang.mspe_mother'].value='-';
	document.frmParam.elements['pemegang.lside_id'].value='0';
	document.frmParam.elements['pemegang.mspe_no_identity'].value='-';
	document.frmParam.elements['pemegang.lsne_id'].value='1';
	document.frmParam.elements['pemegang.mspe_date_birth'].value='13/10/1995';
	document.frmParam.elements['pemegang.mste_age'].value='12';
	document.frmParam.elements['pemegang.mspe_place_birth'].value='PALEMBANG';
	document.frmParam.elements['pemegang.mspe_sex'].value='1';
	document.frmParam.elements['pemegang.mspe_sex'][0].checked=true;
	document.frmParam.elements['pemegang.mkl_sumber_premi'].value='1';
	document.frmParam.elements['pemegang.mkl_sumber_premi'][0].checked=true;
	document.frmParam.elements['pemegang.lsre_id_premi'].value='ADIK/KAKAK KANDUNG';
	document.frmParam.elements['pemegang.mkl_dana_from'].value='1';
	document.frmParam.elements['pemegang.mkl_dana_from'][0].checked=true;
	document.frmParam.elements['pemegang.mkl_hasil_from'].value='1';
	document.frmParam.elements['pemegang.mkl_hasil_from'][0].checked=true;
	document.frmParam.elements['pemegang.mkl_smbr_hasil_from'].value='1';
	document.frmParam.elements['pemegang.mkl_smbr_hasil_from'][0].checked=true;
	document.frmParam.elements['pemegang.mkl_sumber_premi'].value='1';
	document.frmParam.elements['pemegang.mkl_sumber_premi'][0].checked=true;
	document.frmParam.elements['pemegang.mspe_sts_mrt'].value='1';
	document.frmParam.elements['pemegang.lsag_id'].value='1';
	document.frmParam.elements['pemegang.lsed_id'].value='4';
	document.frmParam.elements['pemegang.alamat_rumah'].value='KOMP. ILIR BARAT PERMAI BLOK D-1 NO.23-24, 24 ILIR';
	document.frmParam.elements['pemegang.kd_pos_rumah'].value='';
	document.frmParam.elements['pemegang.kota_rumah'].value='PALEMBANG';
	document.frmParam.elements['pemegang.area_code_rumah'].value='-';
	document.frmParam.elements['pemegang.telpon_rumah'].value='-';
	document.frmParam.elements['pemegang.area_code_rumah2'].value='';
	document.frmParam.elements['pemegang.telpon_rumah2'].value='';
	document.frmParam.elements['pemegang.no_hp'].value='';
	document.frmParam.elements['pemegang.email'].value='';
	document.frmParam.elements['pemegang.no_hp2'].value='';
	document.frmParam.elements['pemegang.alamat_kantor'].value='KOMP. ILIR BARAT PERMAI BLOK D-1 NO.23-24, 24 ILIR';
	document.frmParam.elements['pemegang.kd_pos_kantor'].value='';
	document.frmParam.elements['pemegang.kota_kantor'].value='PALEMBANG';
	document.frmParam.elements['pemegang.area_code_kantor'].value='-';
	document.frmParam.elements['pemegang.telpon_kantor'].value='-';
	document.frmParam.elements['pemegang.area_code_kantor2'].value='';
	document.frmParam.elements['pemegang.telpon_kantor2'].value='';
	document.frmParam.elements['pemegang.area_code_fax'].value='';
	document.frmParam.elements['pemegang.no_fax'].value='';
	document.frmParam.elements['addressbilling.tagih'].value='2';
	document.frmParam.elements['addressbilling.msap_address'].value='KOMP. ILIR BARAT PERMAI BLOK D-1 NO.23-24, 24 ILIR';
	document.frmParam.elements['addressbilling.msap_zip_code'].value='';
	document.frmParam.elements['addressbilling.kota_tgh'].value='PALEMBANG';
	document.frmParam.elements['addressbilling.msap_area_code1'].value='-';
	document.frmParam.elements['addressbilling.msap_phone1'].value='-';
	document.frmParam.elements['addressbilling.msap_area_code2'].value='';
	document.frmParam.elements['addressbilling.msap_phone2'].value='';
	document.frmParam.elements['addressbilling.msap_area_code3'].value='';
	document.frmParam.elements['addressbilling.msap_phone3'].value='';
	document.frmParam.elements['addressbilling.no_hp'].value='';
	document.frmParam.elements['addressbilling.msap_area_code_fax1'].value='';
	document.frmParam.elements['addressbilling.msap_fax1'].value='';
	document.frmParam.elements['addressbilling.no_hp2'].value='';
	document.frmParam.elements['addressbilling.e_mail'].value='';
	document.frmParam.elements['pemegang.mkl_tujuan'].value='LAIN - LAIN';
	document.frmParam.elements['pemegang.tujuana'].value='';
	document.frmParam.elements['pemegang.mkl_penghasilan'].value='<= RP. 10 JUTA';
	document.frmParam.elements['pemegang.lsre_id_premi'].value='ADIK/KAKAK KANDUNG';
	document.frmParam.elements['pemegang.mkl_pendanaan'].value='LAINNYA';
	document.frmParam.elements['pemegang.mkl_kerja'].value='LAINNYA';
	document.frmParam.elements['pemegang.kerjaa'].value='';
	document.frmParam.elements['pemegang.kerjab'].value='';
	document.frmParam.elements['pemegang.mkl_industri'].value='LAINNYA';
	document.frmParam.elements['pemegang.industria'].value='';
	document.frmParam.elements['pemegang.lsre_id'].value='6';
}

function hidetip()
{
	if (document.layers) document.tooltip.visibility="hidden";
}

function next()
{
	if (frmParam.elements['datausulan.jenis_pemegang_polis'].value == '1'){
		if (document.frmParam.elements['addressbilling.no_hp'].value=='')
		{document.frmParam.elements['addressbilling.no_hp'].value = document.frmParam.elements['contactPerson.no_hp'].value;}
		if (document.frmParam.elements['addressbilling.no_hp2'].value == '')
		{document.frmParam.elements['addressbilling.no_hp2'].value = document.frmParam.elements['contactPerson.no_hp2'].value;}
		if (document.frmParam.elements['addressbilling.msap_area_code_fax1'].value =='')
		{document.frmParam.elements['addressbilling.msap_area_code_fax1'].value  = document.frmParam.elements['contactPerson.area_code_fax'].value;}
		if (document.frmParam.elements['addressbilling.msap_fax1'].value =='')
		{document.frmParam.elements['addressbilling.msap_fax1'].value  = document.frmParam.elements['contactPerson.no_fax'].value;}
		if (document.frmParam.elements['addressbilling.e_mail'].value =='')
		{document.frmParam.elements['addressbilling.e_mail'].value  = document.frmParam.elements['contactPerson.email'].value;}
	}else{
		if (document.frmParam.elements['addressbilling.no_hp'].value=='')
		{document.frmParam.elements['addressbilling.no_hp'].value = document.frmParam.elements['pemegang.no_hp'].value;}
		if (document.frmParam.elements['addressbilling.no_hp2'].value == '')
		{document.frmParam.elements['addressbilling.no_hp2'].value = document.frmParam.elements['pemegang.no_hp2'].value;}
		if (document.frmParam.elements['addressbilling.msap_area_code_fax1'].value =='')
		{document.frmParam.elements['addressbilling.msap_area_code_fax1'].value  = document.frmParam.elements['pemegang.area_code_fax'].value;}
		if (document.frmParam.elements['addressbilling.msap_fax1'].value =='')
		{document.frmParam.elements['addressbilling.msap_fax1'].value  = document.frmParam.elements['pemegang.no_fax'].value;}
		if (document.frmParam.elements['addressbilling.e_mail'].value =='')
		{document.frmParam.elements['addressbilling.e_mail'].value  = document.frmParam.elements['pemegang.email'].value;}
	}
	eval(" document.frmParam.elements['pemegang.indeks_halaman'].value = ((document.frmParam.hal.value)+1);");
	if(document.frmParam.specta_save1){
		if(document.frmParam.specta_save1.checked==true){
			document.frmParam.specta_save1.checked = true;
			document.frmParam.elements['datausulan.specta_save'].value = '1';
		}else{
			document.frmParam.specta_save1.checked = false;
			document.frmParam.elements['datausulan.specta_save'].value = '0';
		}
	}
}
	
function next1()
{
	if(document.frmParam.specta_save1){
		if(document.frmParam.specta_save1.checked==true){
			document.frmParam.specta_save1.checked = true;
			document.frmParam.elements['datausulan.specta_save'].value = '1';
		}else{
			document.frmParam.specta_save1.checked = false;
			document.frmParam.elements['datausulan.specta_save'].value = '0';
		}
	}
	if (frmParam.elements['datausulan.jenis_pemegang_polis'].value == '1'){
		if (document.frmParam.elements['addressbilling.no_hp'].value=='')
		{document.frmParam.elements['addressbilling.no_hp'].value = document.frmParam.elements['contactPerson.no_hp'].value;}
		if (document.frmParam.elements['addressbilling.no_hp2'].value == '')
		{document.frmParam.elements['addressbilling.no_hp2'].value = document.frmParam.elements['contactPerson.no_hp2'].value;}
		if (document.frmParam.elements['addressbilling.msap_area_code_fax1'].value =='')
		{document.frmParam.elements['addressbilling.msap_area_code_fax1'].value  = document.frmParam.elements['contactPerson.area_code_fax'].value;}
		if (document.frmParam.elements['addressbilling.msap_fax1'].value =='')
		{document.frmParam.elements['addressbilling.msap_fax1'].value  = document.frmParam.elements['contactPerson.no_fax'].value;}
		if (document.frmParam.elements['addressbilling.e_mail'].value =='')
		{document.frmParam.elements['addressbilling.e_mail'].value  = document.frmParam.elements['contactPerson.email'].value;}
	}else{
		if (document.frmParam.elements['addressbilling.no_hp'].value=='')
		{document.frmParam.elements['addressbilling.no_hp'].value = document.frmParam.elements['pemegang.no_hp'].value;}
		if (document.frmParam.elements['addressbilling.no_hp2'].value == '')
		{document.frmParam.elements['addressbilling.no_hp2'].value = document.frmParam.elements['pemegang.no_hp2'].value;}
		if (document.frmParam.elements['addressbilling.msap_area_code_fax1'].value =='')
		{document.frmParam.elements['addressbilling.msap_area_code_fax1'].value  = document.frmParam.elements['pemegang.area_code_fax'].value;}
		if (document.frmParam.elements['addressbilling.msap_fax1'].value =='')
		{document.frmParam.elements['addressbilling.msap_fax1'].value  = document.frmParam.elements['pemegang.no_fax'].value;}
		if (document.frmParam.elements['addressbilling.e_mail'].value =='')
		{document.frmParam.elements['addressbilling.e_mail'].value  = document.frmParam.elements['pemegang.email'].value;}
	}
}
		
//function specta_save
function specta_save_onClick(){
	if(document.frmParam.specta_save1){
		if(document.frmParam.specta_save1.checked==true){
			document.frmParam.specta_save1.checked = true;
			document.frmParam.elements['datausulan.specta_save'].value = '1';
		}else{
			document.frmParam.specta_save1.checked = false;
			document.frmParam.elements['datausulan.specta_save'].value = '0';
		}
	}
}