function disableVacc(value) {
	if (value == 3 || value == 4|| value == 5) {
		disable_sio(value);
	} else if (value == 2) {
		disable_sio(value);
	}
}

function disable_sio(value) {
	if (value == 3 || value == 4 || value == 5) {//ditutup  mcl_first_alias
		if(value == 4){
			document.frmParam.elements['pemegang.mspo_nasabah_dcif'].value = v_mspo_no_blanko;
			document.frmParam.elements['pemegang.mspo_nasabah_dcif'].readOnly = false;
			document.frmParam.elements['pemegang.mspo_nasabah_dcif'].style.backgroundColor = '#FFFFFF';
			document.frmParam.elements['pemegang.mspo_nasabah_dcif'].disabled = false;
		}else{
			document.frmParam.elements['pemegang.mspo_nasabah_dcif'].value = v_mspo_nasabah_dcif;
			document.frmParam.elements['pemegang.mspo_nasabah_dcif'].readOnly = true;
			document.frmParam.elements['pemegang.mspo_nasabah_dcif'].style.backgroundColor = '#D4D4D4';
			document.frmParam.elements['pemegang.mspo_nasabah_dcif'].disabled = true;
		}
		document.frmParam.elements['pemegang.mcl_first_alias'].value = v_mcl_first_alias;
		document.frmParam.elements['pemegang.mcl_first_alias'].readOnly = true;
		document.frmParam.elements['pemegang.mcl_first_alias'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.mcl_first_alias'].disabled = true;
		document.frmParam.elements['no_pb'].readOnly = true;
		document.frmParam.elements['no_pb'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['no_pb'].disabled = true;
		disableDataKeluarga(2);
		document.frmParam.elements['pemegang.pemegang_polis'].value = '2';
	} else {//dibuka
		document.frmParam.elements['pemegang.mspo_nasabah_dcif'].value = v_mspo_no_blanko;
		document.frmParam.elements['pemegang.mspo_nasabah_dcif'].readOnly = false;
		document.frmParam.elements['pemegang.mspo_nasabah_dcif'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.mspo_nasabah_dcif'].disabled = false;
		document.frmParam.elements['pemegang.mcl_first_alias'].value = v_mcl_first_alias;
		document.frmParam.elements['pemegang.mcl_first_alias'].readOnly = false;
		document.frmParam.elements['pemegang.mcl_first_alias'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.mcl_first_alias'].disabled = false;
		document.frmParam.elements['no_pb'].value = v_mcl_first_alias;
		document.frmParam.elements['no_pb'].readOnly = false;
		document.frmParam.elements['no_pb'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['no_pb'].disabled = false;
		disableDataKeluarga(2);
		document.frmParam.elements['pemegang.pemegang_polis'].value = '2';
	}
}

function disableDataKeluarga(value) {
	if (value == 2) {//lainnya
		document.frmParam.elements['pemegang.nama_suami'].value = v_nama_suami;
		document.frmParam.elements['pemegang.nama_suami'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.nama_suami'].disabled = true;
		document.frmParam.elements['pemegang.tgl_suami'].value = v_tgl_suami;
		document.frmParam.elements['pemegang.tgl_suami'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.tgl_suami'].disabled = true;
		document.frmParam.elements['pemegang.tgl_suami'].readOnly = true;
		document.frmParam.elements['pemegang.usia_suami'].value = v_usia_suami;
		document.frmParam.elements['pemegang.usia_suami'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.usia_suami'].disabled = true;
		document.frmParam.elements['pemegang.pekerjaan_suami'].value = v_pekerjaan_suami;
		document.frmParam.elements['pemegang.pekerjaan_suami'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.pekerjaan_suami'].disabled = true;
		document.frmParam.elements['pemegang.jabatan_suami'].value = v_jabatan_suami;
		document.frmParam.elements['pemegang.jabatan_suami'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.jabatan_suami'].disabled = true;
		document.frmParam.elements['pemegang.perusahaan_suami'].value = v_perusahaan_suami;
		document.frmParam.elements['pemegang.perusahaan_suami'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.perusahaan_suami'].disabled = true;
		document.frmParam.elements['pemegang.bidang_suami'].value = v_bidang_suami;
		document.frmParam.elements['pemegang.bidang_suami'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.bidang_suami'].disabled = true;
		document.frmParam.elements['pemegang.npwp_suami'].value = v_npwp_suami;
		document.frmParam.elements['pemegang.npwp_suami'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.npwp_suami'].disabled = true;
		document.frmParam.elements['pemegang.penghasilan_suami'].value = v_penghasilan_suami;
		document.frmParam.elements['pemegang.penghasilan_suami'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.penghasilan_suami'].disabled = true;

		document.frmParam.elements['pemegang.nama_ayah'].value = v_nama_ayah;
		document.frmParam.elements['pemegang.nama_ayah'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.nama_ayah'].disabled = true;
		document.frmParam.elements['pemegang.tgl_ayah'].value = v_tgl_ayah;
		document.frmParam.elements['pemegang.tgl_ayah'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.tgl_ayah'].disabled = true;
		document.frmParam.elements['pemegang.tgl_ayah'].readOnly = true;
		document.frmParam.elements['pemegang.usia_ayah'].value = v_usia_ayah;
		document.frmParam.elements['pemegang.usia_ayah'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.usia_ayah'].disabled = true;
		document.frmParam.elements['pemegang.pekerjaan_ayah'].value = v_pekerjaan_ayah;
		document.frmParam.elements['pemegang.pekerjaan_ayah'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.pekerjaan_ayah'].disabled = true;
		document.frmParam.elements['pemegang.jabatan_ayah'].value = v_jabatan_ayah;
		document.frmParam.elements['pemegang.jabatan_ayah'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.jabatan_ayah'].disabled = true;
		document.frmParam.elements['pemegang.perusahaan_ayah'].value = v_perusahaan_ayah;
		document.frmParam.elements['pemegang.perusahaan_ayah'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.perusahaan_ayah'].disabled = true;
		document.frmParam.elements['pemegang.bidang_ayah'].value = v_bidang_ayah;
		document.frmParam.elements['pemegang.bidang_ayah'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.bidang_ayah'].disabled = true;
		document.frmParam.elements['pemegang.npwp_ayah'].value = v_npwp_ayah;
		document.frmParam.elements['pemegang.npwp_ayah'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.npwp_ayah'].disabled = true;
		document.frmParam.elements['pemegang.penghasilan_ayah'].value = v_penghasilan_ayah;
		document.frmParam.elements['pemegang.penghasilan_ayah'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.penghasilan_ayah'].disabled = true;

		document.frmParam.elements['pemegang.nama_ibu'].value = v_nama_ibu;
		document.frmParam.elements['pemegang.nama_ibu'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.nama_ibu'].disabled = true;
		document.frmParam.elements['pemegang.tgl_ibu'].value = v_tgl_ibu;
		document.frmParam.elements['pemegang.tgl_ibu'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.tgl_ibu'].disabled = true;
		document.frmParam.elements['pemegang.tgl_ibu'].readOnly = true;
		document.frmParam.elements['pemegang.usia_ibu'].value = v_usia_ibu;
		document.frmParam.elements['pemegang.usia_ibu'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.usia_ibu'].disabled = true;
		document.frmParam.elements['pemegang.pekerjaan_ibu'].value = v_pekerjaan_ibu;
		document.frmParam.elements['pemegang.pekerjaan_ibu'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.pekerjaan_ibu'].disabled = true;
		document.frmParam.elements['pemegang.jabatan_ibu'].value = v_jabatan_ibu;
		document.frmParam.elements['pemegang.jabatan_ibu'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.jabatan_ibu'].disabled = true;
		document.frmParam.elements['pemegang.perusahaan_ibu'].value = v_perusahaan_ibu;
		document.frmParam.elements['pemegang.perusahaan_ibu'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.perusahaan_ibu'].disabled = true;
		document.frmParam.elements['pemegang.bidang_ibu'].value = v_bidang_ibu;
		document.frmParam.elements['pemegang.bidang_ibu'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.bidang_ibu'].disabled = true;
		document.frmParam.elements['pemegang.npwp_ibu'].value = v_npwp_ibu;
		document.frmParam.elements['pemegang.npwp_ibu'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.npwp_ibu'].disabled = true;
		document.frmParam.elements['pemegang.penghasilan_ibu'].value = v_penghasilan_ibu;
		document.frmParam.elements['pemegang.penghasilan_ibu'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.penghasilan_ibu'].disabled = true;
	} else if (value == 1) {//pelajar
		document.frmParam.elements['pemegang.nama_suami'].value = v_nama_suami;
		document.frmParam.elements['pemegang.nama_suami'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.nama_suami'].disabled = true;
		document.frmParam.elements['pemegang.tgl_suami'].value = v_tgl_suami;
		document.frmParam.elements['pemegang.tgl_suami'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.tgl_suami'].disabled = true;
		document.frmParam.elements['pemegang.tgl_suami'].readOnly = true;
		document.frmParam.elements['pemegang.usia_suami'].value = v_usia_suami;
		document.frmParam.elements['pemegang.usia_suami'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.usia_suami'].disabled = true;
		document.frmParam.elements['pemegang.pekerjaan_suami'].value = v_pekerjaan_suami;
		document.frmParam.elements['pemegang.pekerjaan_suami'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.pekerjaan_suami'].disabled = true;
		document.frmParam.elements['pemegang.jabatan_suami'].value = v_jabatan_suami;
		document.frmParam.elements['pemegang.jabatan_suami'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.jabatan_suami'].disabled = true;
		document.frmParam.elements['pemegang.perusahaan_suami'].value = v_perusahaan_suami;
		document.frmParam.elements['pemegang.perusahaan_suami'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.perusahaan_suami'].disabled = true;
		document.frmParam.elements['pemegang.bidang_suami'].value = v_bidang_suami;
		document.frmParam.elements['pemegang.bidang_suami'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.bidang_suami'].disabled = true;
		document.frmParam.elements['pemegang.npwp_suami'].value = v_npwp_suami;
		document.frmParam.elements['pemegang.npwp_suami'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.npwp_suami'].disabled = true;
		document.frmParam.elements['pemegang.penghasilan_suami'].value = v_penghasilan_suami;
		document.frmParam.elements['pemegang.penghasilan_suami'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.penghasilan_suami'].disabled = true;

		document.frmParam.elements['pemegang.nama_ayah'].value = v_nama_ayah;
		document.frmParam.elements['pemegang.nama_ayah'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.nama_ayah'].disabled = false;
		document.frmParam.elements['pemegang.tgl_ayah'].value = v_tgl_ayah;
		document.frmParam.elements['pemegang.tgl_ayah'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.tgl_ayah'].disabled = false;
		document.frmParam.elements['pemegang.tgl_ayah'].readOnly = false;
		document.frmParam.elements['pemegang.usia_ayah'].value = v_usia_ayah;
		document.frmParam.elements['pemegang.usia_ayah'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.usia_ayah'].disabled = false;
		document.frmParam.elements['pemegang.pekerjaan_ayah'].value = v_pekerjaan_ayah;
		document.frmParam.elements['pemegang.pekerjaan_ayah'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.pekerjaan_ayah'].disabled = false;
		document.frmParam.elements['pemegang.jabatan_ayah'].value = v_jabatan_ayah;
		document.frmParam.elements['pemegang.jabatan_ayah'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.jabatan_ayah'].disabled = false;
		document.frmParam.elements['pemegang.perusahaan_ayah'].value = v_perusahaan_ayah;
		document.frmParam.elements['pemegang.perusahaan_ayah'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.perusahaan_ayah'].disabled = false;
		document.frmParam.elements['pemegang.bidang_ayah'].value = v_bidang_ayah;
		document.frmParam.elements['pemegang.bidang_ayah'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.bidang_ayah'].disabled = false;
		document.frmParam.elements['pemegang.npwp_ayah'].value = v_npwp_ayah;
		document.frmParam.elements['pemegang.npwp_ayah'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.npwp_ayah'].disabled = false;
		document.frmParam.elements['pemegang.penghasilan_ayah'].value = v_penghasilan_ayah;
		document.frmParam.elements['pemegang.penghasilan_ayah'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.penghasilan_ayah'].disabled = false;

		document.frmParam.elements['pemegang.nama_ibu'].value = v_nama_ibu;
		document.frmParam.elements['pemegang.nama_ibu'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.nama_ibu'].disabled = false;
		document.frmParam.elements['pemegang.tgl_ibu'].value = v_tgl_ibu;
		document.frmParam.elements['pemegang.tgl_ibu'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.tgl_ibu'].disabled = false;
		document.frmParam.elements['pemegang.tgl_ibu'].readOnly = false;
		document.frmParam.elements['pemegang.usia_ibu'].value = v_usia_ibu;
		document.frmParam.elements['pemegang.usia_ibu'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.usia_ibu'].disabled = false;
		document.frmParam.elements['pemegang.pekerjaan_ibu'].value = v_pekerjaan_ibu;
		document.frmParam.elements['pemegang.pekerjaan_ibu'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.pekerjaan_ibu'].disabled = false;
		document.frmParam.elements['pemegang.jabatan_ibu'].value = v_jabatan_ibu;
		document.frmParam.elements['pemegang.jabatan_ibu'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.jabatan_ibu'].disabled = false;
		document.frmParam.elements['pemegang.perusahaan_ibu'].value = v_perusahaan_ibu;
		document.frmParam.elements['pemegang.perusahaan_ibu'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.perusahaan_ibu'].disabled = false;
		document.frmParam.elements['pemegang.bidang_ibu'].value = v_bidang_ibu;
		document.frmParam.elements['pemegang.bidang_ibu'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.bidang_ibu'].disabled = false;
		document.frmParam.elements['pemegang.npwp_ibu'].value = v_npwp_ibu;
		document.frmParam.elements['pemegang.npwp_ibu'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.npwp_ibu'].disabled = false;
		document.frmParam.elements['pemegang.penghasilan_ibu'].value = v_penghasilan_ibu;
		document.frmParam.elements['pemegang.penghasilan_ibu'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.penghasilan_ibu'].disabled = false;

	} else if (value == 0) {//ibu rumah tangga

		document.frmParam.elements['pemegang.nama_suami'].value = v_nama_suami;
		document.frmParam.elements['pemegang.nama_suami'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.nama_suami'].disabled = false;
		document.frmParam.elements['pemegang.tgl_suami'].value = v_tgl_suami;
		document.frmParam.elements['pemegang.tgl_suami'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.tgl_suami'].disabled = false;
		document.frmParam.elements['pemegang.tgl_suami'].readOnly = false;
		document.frmParam.elements['pemegang.usia_suami'].value = v_usia_suami;
		document.frmParam.elements['pemegang.usia_suami'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.usia_suami'].disabled = false;
		document.frmParam.elements['pemegang.pekerjaan_suami'].value = v_pekerjaan_suami;
		document.frmParam.elements['pemegang.pekerjaan_suami'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.pekerjaan_suami'].disabled = false;
		document.frmParam.elements['pemegang.jabatan_suami'].value = v_jabatan_suami;
		document.frmParam.elements['pemegang.jabatan_suami'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.jabatan_suami'].disabled = false;
		document.frmParam.elements['pemegang.perusahaan_suami'].value = v_perusahaan_suami;
		document.frmParam.elements['pemegang.perusahaan_suami'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.perusahaan_suami'].disabled = false;
		document.frmParam.elements['pemegang.bidang_suami'].value = v_bidang_suami;
		document.frmParam.elements['pemegang.bidang_suami'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.bidang_suami'].disabled = false;
		document.frmParam.elements['pemegang.npwp_suami'].value = v_npwp_suami;
		document.frmParam.elements['pemegang.npwp_suami'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.npwp_suami'].disabled = false;
		document.frmParam.elements['pemegang.penghasilan_suami'].value = v_penghasilan_suami;
		document.frmParam.elements['pemegang.penghasilan_suami'].style.backgroundColor = '#FFFFFF';
		document.frmParam.elements['pemegang.penghasilan_suami'].disabled = false;

		document.frmParam.elements['pemegang.nama_ayah'].value = v_nama_ayah;
		document.frmParam.elements['pemegang.nama_ayah'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.nama_ayah'].disabled = true;
		document.frmParam.elements['pemegang.tgl_ayah'].value = v_tgl_ayah;
		document.frmParam.elements['pemegang.tgl_ayah'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.tgl_ayah'].disabled = true;
		document.frmParam.elements['pemegang.tgl_ayah'].readOnly = true;
		document.frmParam.elements['pemegang.usia_ayah'].value = v_usia_ayah;
		document.frmParam.elements['pemegang.usia_ayah'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.usia_ayah'].disabled = true;
		document.frmParam.elements['pemegang.pekerjaan_ayah'].value = v_pekerjaan_ayah;
		document.frmParam.elements['pemegang.pekerjaan_ayah'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.pekerjaan_ayah'].disabled = true;
		document.frmParam.elements['pemegang.jabatan_ayah'].value = v_jabatan_ayah;
		document.frmParam.elements['pemegang.jabatan_ayah'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.jabatan_ayah'].disabled = true;
		document.frmParam.elements['pemegang.perusahaan_ayah'].value = v_perusahaan_ayah;
		document.frmParam.elements['pemegang.perusahaan_ayah'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.perusahaan_ayah'].disabled = true;
		document.frmParam.elements['pemegang.bidang_ayah'].value = v_bidang_ayah;
		document.frmParam.elements['pemegang.bidang_ayah'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.bidang_ayah'].disabled = true;
		document.frmParam.elements['pemegang.npwp_ayah'].value = v_npwp_ayah;
		document.frmParam.elements['pemegang.npwp_ayah'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.npwp_ayah'].disabled = true;
		document.frmParam.elements['pemegang.penghasilan_ayah'].value = v_penghasilan_ayah;
		document.frmParam.elements['pemegang.penghasilan_ayah'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.penghasilan_ayah'].disabled = true;

		document.frmParam.elements['pemegang.nama_ibu'].value = v_nama_ibu;
		document.frmParam.elements['pemegang.nama_ibu'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.nama_ibu'].disabled = true;
		document.frmParam.elements['pemegang.tgl_ibu'].value = v_tgl_ibu;
		document.frmParam.elements['pemegang.tgl_ibu'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.tgl_ibu'].disabled = true;
		document.frmParam.elements['pemegang.tgl_ibu'].readOnly = true;
		document.frmParam.elements['pemegang.usia_ibu'].value = v_usia_ibu;
		document.frmParam.elements['pemegang.usia_ibu'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.usia_ibu'].disabled = true;
		document.frmParam.elements['pemegang.pekerjaan_ibu'].value = v_pekerjaan_ibu;
		document.frmParam.elements['pemegang.pekerjaan_ibu'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.pekerjaan_ibu'].disabled = true;
		document.frmParam.elements['pemegang.jabatan_ibu'].value = v_jabatan_ibu;
		document.frmParam.elements['pemegang.jabatan_ibu'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.jabatan_ibu'].disabled = true;
		document.frmParam.elements['pemegang.perusahaan_ibu'].value = v_perusahaan_ibu;
		document.frmParam.elements['pemegang.perusahaan_ibu'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.perusahaan_ibu'].disabled = true;
		document.frmParam.elements['pemegang.bidang_ibu'].value = v_bidang_ibu;
		document.frmParam.elements['pemegang.bidang_ibu'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.bidang_ibu'].disabled = true;
		document.frmParam.elements['pemegang.npwp_ibu'].value = v_npwp_ibu;
		document.frmParam.elements['pemegang.npwp_ibu'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.npwp_ibu'].disabled = true;
		document.frmParam.elements['pemegang.penghasilan_ibu'].value = v_penghasilan_ibu;
		document.frmParam.elements['pemegang.penghasilan_ibu'].style.backgroundColor = '#D4D4D4';
		document.frmParam.elements['pemegang.penghasilan_ibu'].disabled = true;
	}
}

function loaddata_penagihan()
{
	if (document.frmParam.elements['addressbilling.tagih'].value == "1"){
		if (document.frmParam.elements['addressbilling.kota_tgh'].value==""){
			document.frmParam.elements['addressbilling.kota_tgh'].value="";
		}else{
			document.frmParam.elements['addressbilling.kota_tgh'].value=v_ad_kota_tgh;
		}
		document.frmParam.elements['addressbilling.kota_tgh'].disabled = false;
		document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#FFFFFF';
		document.frmParam.elements['addressbilling.msap_address'].readOnly = false;
		document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#FFFFFF';
		if ((document.frmParam.elements['addressbilling.msap_address'].value=="")||(document.frmParam.elements['addressbilling.msap_address'].value=="")){
			document.frmParam.elements['addressbilling.msap_address'].value='';
		}else{
			document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.alamat_sementara.value;
		}
		document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = false;
		document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#FFFFFF';						
		if ((document.frmParam.elements['addressbilling.msap_zip_code'].value=="")||(document.frmParam.elements['addressbilling.msap_zip_code'].value=="")){
			document.frmParam.elements['addressbilling.msap_zip_code'].value='';
		}else{
			document.frmParam.elements['addressbilling.msap_zip_code'].value=v_ad_msap_zip_code;
		}			
		document.frmParam.elements['addressbilling.lsne_id'].readOnly = false;
		document.frmParam.elements['addressbilling.lsne_id'].style.backgroundColor ='#FFFFFF';						
		if ((document.frmParam.elements['addressbilling.lsne_id'].value=="")||(document.frmParam.elements['addressbilling.lsne_id'].value=="")){
			document.frmParam.elements['addressbilling.lsne_id'].value='';
		}else{
			document.frmParam.elements['addressbilling.lsne_id'].value=v_ad_lsne_id;
		}			
		document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = false;
		document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#FFFFFF';						
		if ((document.frmParam.elements['addressbilling.msap_area_code1'].value=="")||(document.frmParam.elements['addressbilling.msap_area_code1'].value=="")){
			document.frmParam.elements['addressbilling.msap_area_code1'].value='';
		}else{
			document.frmParam.elements['addressbilling.msap_area_code1'].value=v_ad_msap_area_code1;
		}
		document.frmParam.elements['addressbilling.msap_phone1'].readOnly = false;
		document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#FFFFFF';						
		if ((document.frmParam.elements['addressbilling.msap_phone1'].value=="")||(document.frmParam.elements['addressbilling.msap_phone1'].value=="")){
			document.frmParam.elements['addressbilling.msap_phone1'].value='';
		}else{
			document.frmParam.elements['addressbilling.msap_phone1'].value=v_ad_msap_phone1;
		}
		document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = false;
		document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#FFFFFF';						
		if ((document.frmParam.elements['addressbilling.msap_area_code2'].value=="")||(document.frmParam.elements['addressbilling.msap_area_code2'].value=="")){
			document.frmParam.elements['addressbilling.msap_area_code2'].value='';
		}else{
			document.frmParam.elements['addressbilling.msap_area_code2'].value=v_ad_msap_area_code2;
		}
		document.frmParam.elements['addressbilling.msap_phone2'].readOnly = false;
		document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#FFFFFF';						
		if ((document.frmParam.elements['addressbilling.msap_phone2'].value=="")||(document.frmParam.elements['addressbilling.msap_phone2'].value=="")){
			document.frmParam.elements['addressbilling.msap_phone2'].value='';
		}else{
			document.frmParam.elements['addressbilling.msap_phone2'].value=v_ad_msap_phone2;
		}
		document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = false;
		document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#FFFFFF';						
		document.frmParam.elements['addressbilling.msap_area_code3'].value=v_ad_msap_area_code3;
		document.frmParam.elements['addressbilling.msap_phone3'].readOnly = false;
		document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#FFFFFF';						
		document.frmParam.elements['addressbilling.msap_phone3'].value=v_ad_msap_phone3;
	}else{
		if (document.frmParam.elements['addressbilling.tagih'].value=="2"){
			if (document.frmParam.elements['datausulan.jenis_pemegang_polis'].value == "1"){
				if (document.frmParam.elements['contactPerson.kota_rumah'].value == ""){
					document.frmParam.elements['addressbilling.kota_tgh'].value="";
				}else{
					document.frmParam.elements['addressbilling.kota_tgh'].value=v_cp_kota_rumah;
				}
				document.frmParam.elements['addressbilling.kota_tgh'].disabled = true;
				document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
				document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.alamat_sementara1.value;
				
				document.frmParam.elements['addressbilling.msap_zip_code'].value=v_cp_kd_pos_rumah;		
				document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
				document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D4D4D4';

				document.frmParam.elements['addressbilling.lsne_id'].value=v_cp_lsne_id;		
				document.frmParam.elements['addressbilling.lsne_id'].readOnly = true;
				document.frmParam.elements['addressbilling.lsne_id'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['addressbilling.msap_area_code1'].value=v_cp_area_code_rumah;		
				document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
				document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['addressbilling.msap_phone1'].value=v_cp_telpon_rumah;
				document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
				document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['addressbilling.msap_area_code2'].value=v_cp_area_code_rumah2;		
				document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;
				document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['addressbilling.msap_phone2'].value=v_cp_telpon_rumah2;
				document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
				document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D4D4D4';					
				document.frmParam.elements['addressbilling.msap_area_code3'].value='';		
				document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
				document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['addressbilling.msap_phone3'].value='';
				document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
				document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D4D4D4';	
				document.frmParam.elements['addressbilling.no_hp'].value=v_pp_no_hp;
				document.frmParam.elements['addressbilling.no_hp2'].value=v_pp_no_hp2;
			}else{
				if (document.frmParam.elements['pemegang.kota_rumah'].value == ""){
					document.frmParam.elements['addressbilling.kota_tgh'].value="";
				}else{
					document.frmParam.elements['addressbilling.kota_tgh'].value=v_pp_kota_rumah;
				}
				document.frmParam.elements['addressbilling.kota_tgh'].disabled = true;
				document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
				document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.alamat_sementara1.value;
				
				document.frmParam.elements['addressbilling.msap_zip_code'].value=v_pp_kd_pos_rumah;		
				document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
				document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['addressbilling.lsne_id'].value=v_pp_lsne_id;		
				document.frmParam.elements['addressbilling.lsne_id'].readOnly = true;
				document.frmParam.elements['addressbilling.lsne_id'].style.backgroundColor ='#D4D4D4';					
				
				document.frmParam.elements['addressbilling.msap_area_code1'].value=v_pp_area_code_rumah;		
				document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
				document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['addressbilling.msap_phone1'].value=v_pp_telpon_rumah;
				document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
				document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['addressbilling.msap_area_code2'].value=v_pp_area_code_rumah2;		
				document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;
				document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['addressbilling.msap_phone2'].value=v_pp_telpon_rumah2;
				document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
				document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D4D4D4';					
				document.frmParam.elements['addressbilling.msap_area_code3'].value='';		
				document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
				document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['addressbilling.msap_phone3'].value='';
				document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
				document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['addressbilling.no_hp'].value=v_pp_no_hp;
				document.frmParam.elements['addressbilling.no_hp2'].value=v_pp_no_hp2;	
			}
		}else{
			if (document.frmParam.elements['addressbilling.tagih'].value=="3"){
				if (document.frmParam.elements['datausulan.jenis_pemegang_polis'].value == "1"){
					if (document.frmParam.elements['contactPerson.kota_kantor'].value == ""){
						document.frmParam.elements['addressbilling.kota_tgh'].value="";
					}else{
						document.frmParam.elements['addressbilling.kota_tgh'].value=v_cp_kota_kantor;
					}
					document.frmParam.elements['addressbilling.kota_tgh'].disabled = true;
					document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.alamat_sementara2.value;
					
					document.frmParam.elements['addressbilling.msap_zip_code'].value=v_cp_kd_pos_kantor;		
					document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D4D4D4';
					
					document.frmParam.elements['addressbilling.lsne_id'].value=v_cp_lsne_id;		
					document.frmParam.elements['addressbilling.lsne_id'].readOnly = true;
					document.frmParam.elements['addressbilling.lsne_id'].style.backgroundColor ='#D4D4D4';						
					
					document.frmParam.elements['addressbilling.msap_area_code1'].value=v_cp_area_code_kantor;		
					document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone1'].value=v_cp_telpon_kantor;
					document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_area_code2'].value=v_cp_area_code_kantor2;		
					document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone2'].value=v_cp_telpon_kantor2;
					document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_area_code3'].value='';
					document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone3'].value='';
					document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D4D4D4';
				}else{
					if (document.frmParam.elements['pemegang.kota_kantor'].value == ""){
						document.frmParam.elements['addressbilling.kota_tgh'].value="";
					}else{
						document.frmParam.elements['addressbilling.kota_tgh'].value=v_pp_kota_kantor;
					}
					document.frmParam.elements['addressbilling.kota_tgh'].disabled = true;
					document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.alamat_sementara2.value;
					
					document.frmParam.elements['addressbilling.msap_zip_code'].value=v_pp_kd_pos_kantor;		
					document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D4D4D4';
					
					document.frmParam.elements['addressbilling.lsne_id'].value=v_pp_lsne_id;		
					document.frmParam.elements['addressbilling.lsne_id'].readOnly = true;
					document.frmParam.elements['addressbilling.lsne_id'].style.backgroundColor ='#D4D4D4';						
					
					document.frmParam.elements['addressbilling.msap_area_code1'].value=v_pp_area_code_kantor;		
					document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone1'].value=v_pp_telpon_kantor;
					document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_area_code2'].value=v_pp_area_code_kantor2;		
					document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone2'].value=v_pp_telpon_kantor2;
					document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_area_code3'].value='';
					document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone3'].value='';
					document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D4D4D4';				
				}
			}
		}
	}
}
