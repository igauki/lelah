	function brekele(ncek){
		if(ncek) alert('Perhatian! Dengan memilih breakable, maka akan digunakan rate terrendah untuk periode bersangkutan, dan jangan lupa menyertakan MEMO PERMINTAAN BREAKABLE dari Nasabah.');
		if(ncek) {
			document.getElementById('brek').value = '1'; 
		} else {
			document.getElementById('brek').value = '0';
		} 
	}
	
	function cek_cb_rider(ncek){
		if(ncek==1 || ncek==3) {
			document.getElementById('cb_rider').style.visibility = 'hidden';
		}else{
			document.getElementById('cb_rider').style.visibility = 'visible';
		}
	}
	
	function brekele2(ncek){
		if(ncek==false) alert('Apabila nasabah tidak menandatangani SPH, maka untuk pencairan dana dibawah 3 tahun akan dikenakan pajak. Harap pastikan hal ini ke nasabah.');
		if(ncek) {
			document.getElementById('brek2').value = '1'; 
		} else {
			document.getElementById('brek2').value = '0';
		} 
	}
	
	function telemarket(ncek){
		if(ncek) {
			document.getElementById('tele').value = '1'; 
		} else {
			document.getElementById('tele').value = '0';
		} 
	}
	
	function setPersentaseRate(mgi){
		var lsbs_id = v_lsbs_id;
		var lsdbs_number = v_lsdbs_number;
		var flag_bulanan = v_flag_bulanan;
		var flag_special = v_flag_special;
		var begdate = document.frmParam.elements['powersave.begdate_topup'].value;
		var tahun = begdate.substr(6,4);
		var bulan = begdate.substr(3,2);
		var tanggal = begdate.substr(0,2);
		
		var sept10=new Date(2009,8,10);
		sept10.setHours(0,0,0,0);
		//sept10.setFullYear(2009,8,19); // 8 = september, dimulai dari 0 = januari sampai 11 = desember
		
		var begdate2=new Date(tahun,bulan-1,tanggal);
		begdate2.setHours(0,0,0,0);
		//begdate2.setFullYear(tahun,parseInt(bulan)-1,parseInt(tanggal)+1);
		
		if(lsbs_id == 164 || lsbs_id == 174){
			if(begdate2<sept10){
				xmlData.src = v_path.concat("/xml/faktor_stable_"+lsdbs_number+".xml"); //1 = stable, 2 = stable BII
			}else {
				if(flag_special == 1) {
					xmlData.src = v_path.concat("/xml/faktor_stable_"+lsdbs_number+".xml");
				}else{
					xmlData.src = v_path.concat("/xml/faktor_stable_"+lsdbs_number+"_10sept.xml"); //1 = stable, 2 = stable BII
				}
			}
			if(flag_bulanan == 1) xmlData.src = v_path.concat("/xml/faktor_stable_bulanan.xml");
			
			var collPosition = xmlData.selectNodes( "//Position" );
			for( var i = 0; i < collPosition.length; i++ ){
				var id_ = collPosition.item(i).selectSingleNode("ID").text;
				var faktor_ = collPosition.item(i).selectSingleNode("FAKTOR").text;
				if(mgi == id_ || (mgi >= 12 && id_ == 0)){
					document.frmParam.elements['powersave.msl_bp_rate'].value = faktor_;
					break;
				}
			}
		}
	}
	
	//function xml rpenerima
	function generateXML_penerima( objParser, strID, strDesc ) {
		varOptionhub = "";
		var collPosition = objParser.selectNodes( "//Position" );
		for( var i = 0; i < collPosition.length; i++ ) varOptionhub += "<OPTION " + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
	}
	
	//function xml warganegara
	function generateXML_warganegara( objParser, strID, strDesc ) {
		varOptionlsne = "";
		var collPosition = objParser.selectNodes( "//Position" );
		for( var i = 0; i < collPosition.length; i++ ) varOptionlsne += "<OPTION " + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
	}
	
	function generateSex_Benef() {
		varOptSex = "";
		varOptSex += "<OPTION  VALUE='0'>Perempuan</OPTION>";
		varOptSex += "<OPTION  VALUE='1'>Laki-Laki</OPTION>";
	}
		
	function kuasa_onClick(){
		if(document.frmParam.mrc_kuasa1.checked == true){
			document.frmParam.mrc_kuasa1.checked = true;
			document.frmParam.elements['rekening_client.mrc_kuasa'].value = '1';
	
		}else{
			document.frmParam.mrc_kuasa1.checked = false;
			document.frmParam.elements['rekening_client.mrc_kuasa'].value = '0';
	
		}
	}
	
	function tgl_debet_onClick(){
		if(document.frmParam.tgl_debet.checked == true){
			document.frmParam.tgl_debet.checked = true;
			document.frmParam.elements['account_recur.flag_set_auto'].value = '1';
			document.frmParam.elements['_pemegang.mste_tgl_recur'].readOnly = false;
			document.frmParam.elements['_pemegang.mste_tgl_recur'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['_pemegang.mste_tgl_recur'].disabled = false;
			
	
		}else{
			document.frmParam.tgl_debet.checked = false;
			document.frmParam.elements['account_recur.flag_set_auto'].value = '0';
			document.frmParam.elements['_pemegang.mste_tgl_recur'].readOnly = true;
			document.frmParam.elements['_pemegang.mste_tgl_recur'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['_pemegang.mste_tgl_recur'].disabled = true;
	
		}
	}
	
	function jns_rate(){
		if((document.frmParam.elements['datausulan.lsbs_id'].value=='158') &&( (document.frmParam.elements['datausulan.lsdbs_number'].value=='5') ||
			(document.frmParam.elements['datausulan.lsdbs_number'].value=='8') || (document.frmParam.elements['datausulan.lsdbs_number'].value=='9') )){
			if((document.frmParam.elements['powersave.mps_jangka_inv'].value=='12') || (document.frmParam.elements['powersave.mps_jangka_inv'].value=='36')){
				if((document.frmParam.elements['powersave.mps_roll_over'].value=='')  || (document.frmParam.elements['powersave.mps_roll_over'].value=='1')){
					document.frmParam.elements['powersave.mps_roll_over'].value='2';
				}
			}
		}
				
		if ((document.frmParam.elements['datausulan.kode_flag'].value=='1') || (document.frmParam.elements['datausulan.kode_flag'].value=='11') ||
			(document.frmParam.elements['datausulan.kode_flag'].value=='16')){	
			if (document.frmParam.elements['powersave.mps_jenis_plan'].value == "0"){
				document.frmParam.elements['powersave.mps_rate'].readOnly=true;
				document.frmParam.elements['powersave.mps_rate'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['powersave.mpr_note'].readOnly=true;
				document.frmParam.elements['powersave.mpr_note'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['powersave.mpr_note'].value = "";
				document.frmParam.elements['powersave.fee_based_income'].readOnly=true;
				document.frmParam.elements['powersave.fee_based_income'].style.backgroundColor ='#D4D4D4';
			}else{
				document.frmParam.elements['powersave.mps_rate'].readOnly=false;
				document.frmParam.elements['powersave.mps_rate'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['powersave.mpr_note'].readOnly=false;
				document.frmParam.elements['powersave.mpr_note'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['powersave.fee_based_income'].readOnly=false;
				document.frmParam.elements['powersave.fee_based_income'].style.backgroundColor ='#FFFFFF';
			}
		}
	}
	
	function sts(hasil){
		document.frmParam.elements['datausulan.mste_flag_investasi'].value = hasil;
	}	
	
	function addRowDOM1(tableID, jml){
		var flag_add1 = 1;
   		var oTable = document.getElementById?document.getElementById(tableID):document.all[tableID];
		var oRow = oTable.insertRow(oTable.rows.length);
        var oCells = oRow.cells;
   		var tmp = 0;
   		var tmp1 = 0;
   		var idx = null;
   		var idx1 = null;
   		if(jml=='1'){
   			tmp = parseInt(document.getElementById("tableProd").rows.length);
   			idx = jmlpenerima + 1;
   		}else if(jml=='2'){
   			tmp1 = parseInt(document.getElementById("tabledth").rows.length);
   			idx1 = jmldth + 1;
   		}
   		
		if(document.getElementById || document.all) {
			if(jml=='1'){
				var cell = oRow.insertCell(oCells.length);
				cell.innerHTML = "<td >"+idx+"</td>";
				var cell = oRow.insertCell(oCells.length);
				cell.innerHTML = "<td ><input type=text id='benef.msaw_first"+idx+"' name='benef.msaw_first"+idx+"' value='' size=30></td>";
				var cell = oRow.insertCell(oCells.length);
				cell.innerHTML = "<td><input type=text id='tgllhr"+idx+"' name='tgllhr"+idx+"' value='' size=3>/<input type=text id='blnhr"+idx+"' name='blnhr"+idx+"' value='' size=3>/<input type=text id='thnhr"+idx+"' name='thnhr"+idx+"' value='' size=5><input type=hidden id='msaw_birth"+idx+"' name='msaw_birth"+idx+"' value='' size=5></td>";         
				var cell = oRow.insertCell(oCells.length);
				cell.innerHTML = "<td ><select id='benef.msaw_sex"+idx+"' name='benef.msaw_sex"+idx+"'>"+varOptSex+"</select></td>";
				var cell = oRow.insertCell(oCells.length);
				cell.innerHTML = "<td ><select id='benef.lsre_id"+idx+"' name='benef.lsre_id"+idx+"'>"+varOptionhub+"</select></td>";
				var cell = oRow.insertCell(oCells.length);
				cell.innerHTML = "<td><input type=text id='benef.msaw_persen"+idx+"' name='benef.msaw_persen"+idx+"' value='' size=5 ></td>";
				var cell = oRow.insertCell(oCells.length);
				cell.innerHTML = "<td ><select id='benef.lsne_id"+idx+"' name='benef.lsne_id"+idx+"'>"+varOptionlsne+"</select></td>";
				var cell = oRow.insertCell(oCells.length);
				cell.innerHTML = "<td ><input type=text id='benef.msaw_ket"+idx+"' name='benef.msaw_ket"+idx+"' value='' size=60></td>";
				var cell = oRow.insertCell(oCells.length);
				cell.innerHTML = "<td ><input type=checkbox id='cek"+idx+"' name='cek"+idx+"' class=\"noBorder\" ></td></tr>";
				var cell = oRow.insertCell(oCells.length);			
				cell.innerHTML = '';
				jmlpenerima = idx;
			}else if(jml=='2'){
				var cell = oRow.insertCell(oCells.length);
				cell.innerHTML = "<td >"+idx1+"</td>";
				var cell = oRow.insertCell(oCells.length);
				cell.innerHTML = "<td ><input type=text name='dth.jumlah"+idx1+"' value='' size=30></td>";
				var cell = oRow.insertCell(oCells.length);
				cell.innerHTML = "<td><input type=text name='tgltarik"+idx1+"' value='' size=3>/<input type=text name='blntarik"+idx1+"' value='' size=3>/<input type=text name='thntarik"+idx1+"' value='' size=5><input type=hidden name='tgl_penarikan"+idx1+"' value='' size=5></td>";         
				var cell = oRow.insertCell(oCells.length);
				cell.innerHTML ="<td><input type=text name='dth.keterangan"+idx1+"' value='' size=50 maxlength=100></td>";
				var cell = oRow.insertCell(oCells.length);
				cell.innerHTML ="<td ><input type=checkbox name=cek1"+idx1+" id=ck1"+idx1+"' class=\"noBorder\" ></td></tr>";
				var cell = oRow.insertCell(oCells.length);			
				cell.innerHTML = '';
				jmldth = idx1;
			}
		}
 	}
	
	function cancel1(jenis){		
		var idx = null;
		if(jenis=='1'){
			idx = jmlpenerima;
		}else if(jenis=='2'){
			idx = jmldth;
		}
		if(idx!=""){
			if(idx >0) flag_add1 = 1;
		}
		if(flag_add1==1){
			if(jenis=='1'){
				deleteRowDOM1('tableProd', '1');
			}else if(jenis=='2'){
				deleteRowDOM1('tabledth', '2');
			}
		}
	}
	
	function deleteRowDOM1(tableID, rowCount)
	{ 
	    var oTable = document.getElementById?document.getElementById(tableID):document.all[tableID];
	    var row = "";
	    var row1 = "";
	    var idx = null;
	    var idx1 = null;
	    var flag_row = 0;
		var flag_row1 = 0;
		var jumlah_cek = 0;
		var jumlah_cek1 = 0;
	
		if(rowCount=='1'){
			row = parseInt(document.getElementById("tableProd").rows.length);
			idx = jmlpenerima;
    		if(row>0){
				for(var i=1; i<((parseInt(row))); i++){		      
					if(eval("document.getElementById('cek"+i+"').checked")){
						idx = idx-1;
						for(var k=i; k<((parseInt(row))-1); k++){
					    	eval(" document.getElementById('benef.msaw_first"+k+"').value = document.getElementById('benef.msaw_first"+(k+1)+"').value;");   
							eval(" document.getElementById('tgllhr"+k+"').value = document.getElementById('tgllhr"+(k+1)+"').value;"); 
							eval(" document.getElementById('blnhr"+k+"').value = document.getElementById('blnhr"+(k+1)+"').value;");			      
							eval(" document.getElementById('thnhr"+k+"').value = document.getElementById('thnhr"+(k+1)+"').value;");			
							eval(" document.getElementById('msaw_birth"+k+"').value = document.getElementById('msaw_birth"+(k+1)+"').value;");			
							eval(" document.getElementById('benef.lsre_id"+k+"').value = document.getElementById('benef.lsre_id"+(k+1)+"').value;");			
							eval(" document.getElementById('benef.msaw_persen"+k+"').value = document.getElementById('benef.msaw_persen"+(k+1)+"').value;");			
							eval(" document.getElementById('benef.lsne_id"+k+"').value = document.getElementById('benef.lsne_id"+(k+1)+"'0.value;");
							eval(" document.getElementById('benef.msaw_ket"+k+"').value = document.getElementById('benef.msaw_ket"+(k+1)+"').value;");			
							eval(" document.getElementById('cek"+k+"').checked = document.getElementById('cek"+(k+1)+"').checked;");		    
						}
						oTable.deleteRow(parseInt(document.getElementById("tableProd").rows.length)-1);
						row = row-1;
						jmlpenerima = idx;
						i = i-1;
					}
				}
				row=parseInt(document.getElementById("tableProd").rows.length);
				if(row==1) flag_add = 0;
			}	
		}else if(rowCount=='2'){
			rowX = parseInt(document.getElementById("tabledth").rows.length);
			idx_X = jmldth;
			rowX = rowX-3;
    		if(rowX>0){
				for(var i=1; i<((parseInt(rowX))); i++){
					if( eval("document.frmParam.elements['cek1"+i+"'].checked")){
						idx_X = idx_X-1;
						for (var k=i; k<((parseInt(rowX))-1); k++){
					    	eval(" document.frmParam.elements['dth.jumlah"+k+"'].value =document.frmParam.elements['dth.jumlah"+(k+1)+"'].value;");   
							eval(" document.frmParam.elements['tgltarik"+k+"'].value =document.frmParam.elements['tglltarik"+(k+1)+"'].value;"); 
							eval(" document.frmParam.elements['blntarik"+k+"'].value = document.frmParam.elements['blntarik"+(k+1)+"'].value;");			      
							eval(" document.frmParam.elements['thntarik"+k+"'].value = document.frmParam.elements['thntarik"+(k+1)+"'].value;");			
							eval(" document.frmParam.elements['tgl_penarikan"+k+"'].value = document.frmParam.elements['tgl_penarikan"+(k+1)+"'].value;");			
							eval(" document.frmParam.elements['dth.keterangan"+k+"'].value = document.frmParam.elements['dth.keterangan"+(k+1)+"'].value;");			
							eval(" document.frmParam.elements['cek1"+k+"'].checked = document.frmParam.elements['cek1"+(k+1)+"'].checked;");
						}
						oTable.deleteRow(parseInt(document.getElementById("tabledth").rows.length)-1);
						jmldth = idx_X;
						rowX = rowX-1;
						i = i-1;
					}
				}
				if(rowX==1) flag_add = 0;
			}
    	}
	}
	
	function next(){
		document.getElementById("jmlbaris").value = document.getElementById("tableProd").rows.length-1;
		eval(" document.frmParam.elements['pemegang.indeks_halaman'].value = ((document.frmParam.hal.value)+1);");
		document.getElementById("jmlpenerima").value = jmlpenerima;
		document.getElementById("jmldth").value = jmldth;
	}
	
	function prev(){
		document.getElementById("jmlbaris").value = document.getElementById("tableProd").rows.length-1;
		document.getElementById("jmlpenerima").value = jmlpenerima;
		document.getElementById("jmldth").value = jmldth;
	}
	
	function showtip(current,e,text){
		if (document.all||document.getElementById){
			thetitle = text.split('<br>');
			if (thetitle.length>1){
				thetitles = '';
				for(var i=0; i<thetitle.length; i++) thetitles+=thetitle[i];
				current.title = thetitles;
			}else{
				current.title = text;
			}
		}else if(document.layers){
			document.tooltip.document.write('<layer bgColor="white" style="border:1px solid black;font-size:13px;">'+text+'</layer>');
			document.tooltip.document.close();
			document.tooltip.left = e.pageX+5;
			document.tooltip.top = e.pageY+5;
			document.tooltip.visibility = "show";
		}
	}
	
	function hidetip(){
		if (document.layers) document.tooltip.visibility="hidden";
	}
	
	function dofo(index, flag) {
		if(flag==1){
			var a = "rekening_client.mrc_no_ac_split["+(index+1)+"]";		
			document.frmParam.elements[a].focus();
		}else{
			var a = "account_recur.mar_acc_no_split["+(index+1)+"]";		
			document.frmParam.elements[a].focus();
		}
	}
	
	function dth_onclick(dth_cek){
		if(dth_cek) {
			document.getElementById('mste_dth').value = '1'; 
			document.getElementById('btnadd2').disabled = false;
			document.getElementById('btn_cancel2').disabled = false;
		} else {
			document.getElementById('mste_dth').value = '0';
			document.getElementById('btnadd2').disabled = true;
			document.getElementById('btn_cancel2').disabled = true;
		} 
	}
	
	function edit_onClick(){
		with(document.frmParam){
			if(edit_investasi.checked==true){
				document.getElementById('edit_investasi').checked = true;
				document.getElementById('edit_investasi').value = '1';
				elements['investasiutama.edit_investasi'].value = '1';
			}else{
				document.getElementById('edit_investasi').value = '0';
				elements['investasiutama.edit_investasi'].value = '0';	
			}
		}
	}
	
	/*
	function hideSelect(disp){
		//FIXME
		document.frmParam.elements['datausulan.lsbs_id'].style.visibility = disp;
		document.frmParam.elements['datausulan.plan'].style.visibility = disp;
		document.frmParam.elements['datausulan.tipeproduk'].style.visibility = disp;
		document.frmParam.elements['datausulan.kombinasi'].style.visibility = disp;
		document.frmParam.elements['datausulan.mste_flag_cc'].style.visibility = disp;
		document.frmParam.elements['pemegang.mste_pct_dplk'].style.visibility = disp;
		document.frmParam.elements['datausulan.lku_id'].style.visibility = disp;
		document.frmParam.elements['datausulan.kurs_p'].style.visibility = disp;
		document.frmParam.elements['datausulan.lscb_id'].style.visibility = disp;
	}			
	*/
	
	