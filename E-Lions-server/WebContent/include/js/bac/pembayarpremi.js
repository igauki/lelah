	function generateXML(intValue, objParser, nameSelection, strID, strDesc ,lbr,tgg,tab) {
		var strSelection;
		var strHTML = "<SELECT NAME='" + nameSelection  + "'  style='font-family: Verdana; width: " +lbr+"; height:"+tgg+"; font-size:8pt'>";
		var collPosition = objParser.selectNodes( "//Position" );

		for( var i = 0; i < collPosition.length; i++ ) {
			if ((intValue).toUpperCase() == (collPosition.item( i ).selectSingleNode(strID).text).toUpperCase())
				strSelection = "SELECTED";
			else
				strSelection = "";
			strHTML += "<OPTION " + strSelection + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
		}
		strHTML += "</SELECT>";

		return strHTML;
		}
	
	function proteksi(){
		with(document.frmParam) {
			if(tujuan_proteksi.checked==true){
				elements['pembayarPremi.tujuan_proteksi'].value = 'PROTEKSI';				
			}else{
				elements['pembayarPremi.tujuan_proteksi'].value = ' ';
			}
		}
	}
	
	function pendidikan(){
		with(document.frmParam) {
			if(tujuan_pendidikan.checked==true){
				elements['pembayarPremi.tujuan_pendidikan'].value = 'PENDIDIKAN';				
			}else{
				elements['pembayarPremi.tujuan_pendidikan'].value = ' ';
			}
		}
	}
	
	function investasi(){
		with(document.frmParam) {
			if(tujuan_investasi.checked==true){
				elements['pembayarPremi.tujuan_investasi'].value = 'INVESTASI';				
			}else{
				elements['pembayarPremi.tujuan_investasi'].value = ' ';
			}
		}
	}
	
	function tabungan(){
		with(document.frmParam) {
			if(tujuan_tabungan.checked==true){
				elements['pembayarPremi.tujuan_tabungan'].value = 'TABUNGAN';				
			}else{
				elements['pembayarPremi.tujuan_tabungan'].value = ' ';
			}
		}
	}
	
	function pensiun(){
		with(document.frmParam) {
			if(tujuan_pensiun.checked==true){
				elements['pembayarPremi.tujuan_pensiun'].value = 'PENSIUN';				
			}else{
				elements['pembayarPremi.tujuan_pensiun'].value = ' ';
			}
		}
	}
	
	function lainnya(){
		with(document.frmParam) {
			if(tujuan_lainnya.checked==true){
				elements['pembayarPremi.tujuan_lainnya'].value = 'LAINNYA';				
			}else{
				elements['pembayarPremi.tujuan_lainnya'].value = ' ';
			}
		}
	}
	
	function b_gaji(){
		with(document.frmParam) {
			if(bulan_gaji.checked==true){
				elements['pembayarPremi.bulan_gaji'].value = 'GAJI';				
			}else{
				elements['pembayarPremi.bulan_gaji'].value = ' ';
			}
		}
	}
	
	function b_penghasilan(){
		with(document.frmParam) {
			if(bulan_penghasilan.checked==true){
				elements['pembayarPremi.bulan_penghasilan'].value = 'PENGHASILAN SUAMI/ISTRI';				
			}else{
				elements['pembayarPremi.bulan_penghasilan'].value = ' ';
			}
		}
	}
	
	function b_orangtua(){
		with(document.frmParam) {
			if(bulan_orangtua.checked==true){
				elements['pembayarPremi.bulan_orangtua'].value = 'ORANG TUA';				
			}else{
				elements['pembayarPremi.bulan_orangtua'].value = ' ';
			}
		}
	}
	
	function b_usaha(){
		with(document.frmParam) {
			if(bulan_usaha.checked==true){
				elements['pembayarPremi.bulan_usaha'].value = 'HASIL USAHA';				
			}else{
				elements['pembayarPremi.bulan_usaha'].value = ' ';
			}
		}
	}
	
	function b_investasi(){
		with(document.frmParam) {
			if(bulan_investasi.checked==true){
				elements['pembayarPremi.bulan_investasi'].value = 'HASIL INVESTASI';				
			}else{
				elements['pembayarPremi.bulan_investasi'].value =  ' ';
			}
		}
	}
	
	function b_laba(){
		with(document.frmParam) {
			if(bulan_laba.checked==true){
				elements['pembayarPremi.bulan_laba'].value = 'LABA PERUSAHAAN';				
			}else{
				elements['pembayarPremi.bulan_laba'].value = ' ';
			}
		}
	}
	function b_lainnya(){
		with(document.frmParam) {
			if(bulan_lainnya.checked==true){
				elements['pembayarPremi.bulan_lainnya'].value = 'LAINNYA';				
			}else{
				elements['pembayarPremi.bulan_lainnya'].value = ' ';
			}
		}
	}
	
	function t_bonus(){
		with(document.frmParam) {
			if(tahun_bonus.checked==true){
				elements['pembayarPremi.tahun_bonus'].value = 'BONUS';				
			}else{
				elements['pembayarPremi.tahun_bonus'].value = ' ';
			}
		}
	}
	
	function t_komisi(){
		with(document.frmParam) {
			if(tahun_komisi.checked==true){
				elements['pembayarPremi.tahun_komisi'].value = 'KOMISI';				
			}else{
				elements['pembayarPremi.tahun_komisi'].value = ' ';
			}
		}
	}
	
	function t_aset(){
		with(document.frmParam) {
			if(tahun_aset.checked==true){
				elements['pembayarPremi.tahun_aset'].value = 'PENJUALAN ASET';				
			}else{
				elements['pembayarPremi.tahun_aset'].value = ' ';
			}
		}
	}
	
	function t_investasi(){
		with(document.frmParam) {
			if(tahun_investasi.checked==true){
				elements['pembayarPremi.tahun_investasi'].value = 'HASIL INVESTASI';				
			}else{
				elements['pembayarPremi.tahun_investasi'].value = ' ';
			}
		}
	}
	
	function t_hadiah(){
		with(document.frmParam) {
			if(tahun_hadiah.checked==true){
				elements['pembayarPremi.tahun_hadiah'].value = 'HADIAH/WARISAN';				
			}else{
				elements['pembayarPremi.tahun_hadiah'].value = ' ';
			}
		}
	}
	function t_lainnya(){
		with(document.frmParam) {
			if(tahun_lainnya.checked==true){
				elements['pembayarPremi.tahun_lainnya'].value = 'LAINNYA';				
			}else{
				elements['pembayarPremi.tahun_lainnya'].value = ' ';
			}
		}
	}
	
	function generateXML_onChange(intValue, objParser, nameSelection, strID, strDesc ,lbr,tgg,tab) {
		var strSelection;
		var strHTML = "<SELECT NAME='" + nameSelection  + "'  style='font-family: Verdana; width: " +lbr+"; height:"+tgg+"; font-size:8pt'>";
		var collPosition = objParser.selectNodes( "//Position" );
	
		for( var i = 0; i < collPosition.length; i++ ) {
			if ((intValue) == (collPosition.item( i ).selectSingleNode(strID).text))
				strSelection = "SELECTED";
			else
				strSelection = "";
			strHTML += "<OPTION " + strSelection + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";

		}
		strHTML += "</SELECT>";
		return strHTML;

	}

	function generateXML_sumberKyc( objParser, strID, strDesc ) {
		Optiondana = "";
		var collPosition = objParser.selectNodes( "//Position" );
		for( var i = 0; i < collPosition.length; i++ ) Optiondana += "<OPTION " + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
	}		
	
	function next(){
		eval( " document.frmParam.elements['pemegang.indeks_halaman'].value = ((document.frmParam.hal.value)+1);");
	}
	
	function tanyaPsave(spaj,flag_upload){
		
		window.location= v_path.concat('/bac/edit.htm?kopiSPAJ='+document.frmParam.kopiSPAJ.value+'&flag_upload='+document.frmParam.elements['pemegang.flag_upload'].value);
	}
	
	function buttonLinks(str){
		if(str=='cari'){
			spaj = document.frmParam.spaj.value;
			if(spaj==''){alert('Harap cari SPAJ terlebih dahulu');
			}else{document.getElementById('infoFrame').src= v_path.concat('/bac/edit.htm?showSPAJ='+document.frmParam.spaj.value);
			}
		}
	}
	
	function resetPemegangPolis(){
		parent.document.getElementById('jenis_pemegang_polis').value = document.frmParam.elements['datausulan.jenis_pemegang_polis'].value;
		parent.document.getElementById('info').click();
	}
	
	function flag_upload_onClick(){
		with(document.frmParam) {
			if(flag_upload.checked==true){
				elements['pemegang.flag_upload'].value = '1';				
				
			}else{
				elements['pemegang.flag_upload'].value = '0';
			}
		}
	}
	
	function disableData(value){
		
		if (value==1){
				document.frmParam.elements['pembayarPremi.tempat_kedudukan'].value=v_tempat_kedudukan;
				document.frmParam.elements['pembayarPremi.tempat_kedudukan'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['pembayarPremi.tempat_kedudukan'].disabled=true;
				document.frmParam.elements['pembayarPremi.bidang_usaha_badan_hukum'].value=v_bidang_usaha_badan_hukum;
				document.frmParam.elements['pembayarPremi.bidang_usaha_badan_hukum'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['pembayarPremi.bidang_usaha_badan_hukum'].disabled=true;
				document.frmParam.elements['pembayarPremi.tempat_lahir'].value= v_tempat_lahir;
				document.frmParam.elements['pembayarPremi.tempat_lahir'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['pembayarPremi.tempat_lahir'].disabled=false;
				
				
		}else{
				document.frmParam.elements['pembayarPremi.tempat_kedudukan'].value=v_tempat_kedudukan;
				document.frmParam.elements['pembayarPremi.tempat_kedudukan'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['pembayarPremi.tempat_kedudukan'].disabled=false;
				document.frmParam.elements['pembayarPremi.bidang_usaha_badan_hukum'].value=v_bidang_usaha_badan_hukum;
				document.frmParam.elements['pembayarPremi.bidang_usaha_badan_hukum'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['pembayarPremi.bidang_usaha_badan_hukum'].disabled=false;
				document.frmParam.elements['pembayarPremi.tempat_lahir'].value=v_tempat_lahir;
				document.frmParam.elements['pembayarPremi.tempat_lahir'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['pembayarPremi.tempat_lahir'].disabled=true;
				
		}
	
	}
