	//function xml rpenerima
	function generateXML_penerima( objParser, strID, strDesc ) {
		varOptionhub = "";
		var collPosition = objParser.selectNodes( "//Position" );
		for( var i = 0; i < collPosition.length; i++ )  
			varOptionhub += "<OPTION " + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
	}
	
	function Body_onload() {
		var nameSelection
		xmlData.async = false;
		xmlData.src = "/E-Lions/xml/RELATION.xml";
		generateXML_penerima( xmlData, 'ID','RELATION');
	
		var flag_account=document.frmParam.elements['datausulan.flag_account'].value ;
		var flagbungasimponi = document.frmParam.elements['datausulan.isBungaSimponi'].value;
		var flagbonustahapan= document.frmParam.elements['datausulan.isBonusTahapan'].value;
		var flag_bao =  document.frmParam.elements['datausulan.flag_bao'].value;
		var kode_flag = document.frmParam.elements['datausulan.kode_flag'].value;
		//alert(kode_flag);
		//alert(flag_account);
		if ( (flag_bao ==1) && ((flag_account==2)||(flag_account==3)) )
		{
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].disabled=false;
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].style.backgroundColor ='#FFFFFF';				
		}else{
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].value='0';	
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].disabled=true;
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].style.backgroundColor ='#D4D4D4';		
		}
		
		if (flagbungasimponi==1)
		{
			document.frmParam.elements['pemegang.mspo_under_table'].readOnly = true;
			document.frmParam.elements['pemegang.mspo_under_table'].style.backgroundColor ='#D4D4D4';
		}else{
			document.frmParam.elements['pemegang.mspo_under_table'].readOnly = true;
			document.frmParam.elements['pemegang.mspo_under_table'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['pemegang.mspo_under_table'].value='';
		}

		if (flagbonustahapan == 1)
		{
			document.frmParam.elements['pemegang.bonus_tahapan'].readOnly = false;
			document.frmParam.elements['pemegang.bonus_tahapan'].style.backgroundColor ='#FFFFFF';
		}else{
			document.frmParam.elements['pemegang.bonus_tahapan'].readOnly = true;
			document.frmParam.elements['pemegang.bonus_tahapan'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['pemegang.bonus_tahapan'].value='';	
		}		

		if ((document.frmParam.elements['datausulan.mste_flag_cc'].value=='1') || (document.frmParam.elements['datausulan.mste_flag_cc'].value=='2') || (flag_account== 3))
			{
				document.frmParam.elements['account_recur.mar_holder'].readOnly = false;
				document.frmParam.elements['account_recur.mar_holder'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.lbn_id'].disabled = false;
				document.frmParam.elements['account_recur.lbn_id'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no'].style.backgroundColor ='#FFFFFF';
		}else{
				document.frmParam.elements['account_recur.mar_holder'].readOnly = true;
				document.frmParam.elements['account_recur.mar_holder'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_holder'].value='';			
				document.frmParam.elements['account_recur.lbn_id'].disabled = true;
				document.frmParam.elements['account_recur.lbn_id'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.lbn_id'].value='NULL';
				document.frmParam.elements['account_recur.mar_acc_no'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no'].value='';
		}
		if ((flag_account ==2)||(flag_account ==3))
		{
			document.frmParam.elements['rekening_client.mrc_no_ac'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_nama'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_nama'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.lsbp_id'].readOnly = false;
			document.frmParam.elements['rekening_client.lsbp_id'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.kota'].readOnly = false;
			document.frmParam.elements['rekening_client.kota'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_cabang'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_cabang'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_jenis'].disabled = false;
			document.frmParam.elements['rekening_client.mrc_jenis'].style.backgroundColor ='#FFFFFF';
		}else{
			document.frmParam.elements['rekening_client.mrc_no_ac'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac'].value = '';
			document.frmParam.elements['rekening_client.mrc_nama'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_nama'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_nama'].value='';
			document.frmParam.elements['rekening_client.lsbp_id'].disabled = true;
			document.frmParam.elements['rekening_client.lsbp_id'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.lsbp_id'].value='NULL';
			document.frmParam.elements['rekening_client.kota'].readOnly = true;
			document.frmParam.elements['rekening_client.kota'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.kota'].value='';
			document.frmParam.elements['rekening_client.mrc_cabang'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_cabang'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_cabang'].value='';
			document.frmParam.elements['rekening_client.mrc_jenis'].disabled = true;
			document.frmParam.elements['rekening_client.mrc_jenis'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_jenis'].value='0';
		}
		
		/*if ((flag_account ==2)||(flag_account ==3))
		{
			document.frmParam.elements['rekening_client.mrc_no_ac'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_nama'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_nama'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.lsbp_id'].readOnly = false;
			document.frmParam.elements['rekening_client.lsbp_id'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.kota'].readOnly = false;
			document.frmParam.elements['rekening_client.kota'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_cabang'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_cabang'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_jenis'].disabled = false;
			document.frmParam.elements['rekening_client.mrc_jenis'].style.backgroundColor ='#FFFFFF';

			if ((flag_account ==3) || (document.frmParam.elements['datausulan.mste_flag_cc'].value!='0'))
			{
				document.frmParam.elements['account_recur.mar_holder'].readOnly = false;
				document.frmParam.elements['account_recur.mar_holder'].style.backgroundColor ='#FFFFFF';			
				document.frmParam.elements['account_recur.mar_acc_no'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.lbn_id'].disabled = false;
				document.frmParam.elements['account_recur.lbn_id'].style.backgroundColor ='#FFFFFF';
			}else{
				if (document.frmParam.elements['datausulan.mste_flag_cc'].value=='0')
				{
					document.frmParam.elements['account_recur.mar_holder'].readOnly = true;
					document.frmParam.elements['account_recur.mar_holder'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['account_recur.mar_holder'].value='';			
					document.frmParam.elements['account_recur.lbn_id'].disabled = true;
					document.frmParam.elements['account_recur.lbn_id'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['account_recur.lbn_id'].value='NULL';
					document.frmParam.elements['account_recur.mar_acc_no'].readOnly = true;
					document.frmParam.elements['account_recur.mar_acc_no'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['account_recur.mar_acc_no'].value='';
				}
			}
		}else{
			document.frmParam.elements['rekening_client.mrc_no_ac'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac'].value = '';
			document.frmParam.elements['rekening_client.mrc_nama'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_nama'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_nama'].value='';
			document.frmParam.elements['rekening_client.lsbp_id'].disabled = true;
			document.frmParam.elements['rekening_client.lsbp_id'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.lsbp_id'].value='NULL';
			document.frmParam.elements['rekening_client.kota'].readOnly = true;
			document.frmParam.elements['rekening_client.kota'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.kota'].value='';
			document.frmParam.elements['rekening_client.mrc_cabang'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_cabang'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_cabang'].value='';
			document.frmParam.elements['rekening_client.mrc_jenis'].disabled = true;
			document.frmParam.elements['rekening_client.mrc_jenis'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_jenis'].value='0';
			
			if(document.frmParam.elements['datausulan.mste_flag_cc'].value!='0')
			{
				document.frmParam.elements['account_recur.mar_holder'].readOnly = false;
				document.frmParam.elements['account_recur.mar_holder'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.lbn_id'].disabled = false;
				document.frmParam.elements['account_recur.lbn_id'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no'].style.backgroundColor ='#FFFFFF';
			}else{
				document.frmParam.elements['account_recur.mar_holder'].readOnly = true;
				document.frmParam.elements['account_recur.mar_holder'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_holder'].value='';			
				document.frmParam.elements['account_recur.lbn_id'].disabled = true;
				document.frmParam.elements['account_recur.lbn_id'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.lbn_id'].value='NULL';
				document.frmParam.elements['account_recur.mar_acc_no'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no'].value='';
			}
		}*/
/*0 - tutup
1 - Power save
2 - buka fixed & dyna
3 - buka aggresive
4 - buka fixed & dyna & aggresive
5 - buka secured & dyna dollar
6 - buka Syariah fixed & syariah dyna		*/		
		var jumlah_fund = document.frmParam.elements['investasiutama.jmlh_invest'].value;
		switch (Number(kode_flag))
		{
			case 0:
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_jumlah1'].value="";
			
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_jumlah1'].value="";
					
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_jumlah1'].value="";
				document.frmParam.elements['investasiutama.total_persen'].value="";

				document.frmParam.elements['powersave.mps_jangka_inv'].value="";
				document.frmParam.elements['powersave.mps_jangka_inv'].disabled=true;
				document.frmParam.elements['powersave.mps_jangka_inv'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['powersave.mps_rate'].value="";				
				document.frmParam.elements['powersave.mps_prm_interest'].value="";
				document.frmParam.elements['powersave.mps_prm_debet'].value="";
				
				document.frmParam.elements['powersave.mps_roll_over'].value="";
				document.frmParam.elements['powersave.mps_roll_over'].disabled=true;
				document.frmParam.elements['powersave.mps_roll_over'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['investasiutama.mu_periodic_tu'].value="";
				document.frmParam.elements['investasiutama.mu_periodic_tu'].disabled=true;
				document.frmParam.elements['investasiutama.mu_periodic_tu'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.mspr_premium.value="";
				
				document.frmParam.elements['investasiutama.mu_jlh_tu'].value="";
				document.frmParam.elements['investasiutama.mu_jlh_tu'].readOnly=true;
				document.frmParam.elements['investasiutama.mu_jlh_tu'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['datausulan.total_premi_rider'].value="";
				document.frmParam.elements['investasiutama.total_premi_sementara'].value="";								
			break;
			
			case 1:
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_jumlah1'].value="";
			
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_jumlah1'].value="";
					
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.total_persen'].value="";

				document.frmParam.elements['powersave.mps_jangka_inv'].disabled=false;
				document.frmParam.elements['powersave.mps_jangka_inv'].style.backgroundColor ='#FFFFFF';
				
				document.frmParam.elements['powersave.mps_roll_over'].disabled=false;
				document.frmParam.elements['powersave.mps_roll_over'].style.backgroundColor ='#FFFFFF';
				
				document.frmParam.elements['investasiutama.mu_periodic_tu'].value="";
				document.frmParam.elements['investasiutama.mu_periodic_tu'].disabled=true;
				document.frmParam.elements['investasiutama.mu_periodic_tu'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.mspr_premium.value="";
				
				document.frmParam.elements['investasiutama.mu_jlh_tu'].value="";
				document.frmParam.elements['investasiutama.mu_jlh_tu'].readOnly=true;
				document.frmParam.elements['investasiutama.mu_jlh_tu'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['datausulan.total_premi_rider'].value="";
				document.frmParam.elements['investasiutama.total_premi_sementara'].value="";								
			break;
			
			case 2:
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].readOnly=false;
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].style.backgroundColor ='#FFFFFF';				
			
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].readOnly=false;
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].style.backgroundColor ='#FFFFFF';
				
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_jumlah1'].value="";
					
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_jumlah1'].value="";

				document.frmParam.elements['powersave.mps_jangka_inv'].value="";
				document.frmParam.elements['powersave.mps_jangka_inv'].disabled=true;
				document.frmParam.elements['powersave.mps_jangka_inv'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['powersave.mps_rate'].value="";				
				document.frmParam.elements['powersave.mps_prm_interest'].value="";
				document.frmParam.elements['powersave.mps_prm_debet'].value="";
				
				document.frmParam.elements['powersave.mps_roll_over'].value="";
				document.frmParam.elements['powersave.mps_roll_over'].disabled=true;
				document.frmParam.elements['powersave.mps_roll_over'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['investasiutama.mu_periodic_tu'].disabled=false;
				document.frmParam.elements['investasiutama.mu_periodic_tu'].style.backgroundColor ='#FFFFFF';
				
				if (document.frmParam.elements['investasiutama.mu_jlh_tu'].value=="")
				{
					document.frmParam.elements['investasiutama.mu_jlh_tu'].value="0";
				}
				document.frmParam.elements['investasiutama.mu_jlh_tu'].readOnly=false;
				document.frmParam.elements['investasiutama.mu_jlh_tu'].style.backgroundColor ='#FFFFFF';
				

												
			break;			

			case 3:
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_jumlah1'].value="";
			
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].readOnly=false;
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].style.backgroundColor ='#FFFFFF';				
				
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_jumlah1'].value="";
					
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_jumlah1'].value="";

				document.frmParam.elements['powersave.mps_jangka_inv'].value="";
				document.frmParam.elements['powersave.mps_jangka_inv'].disabled=true;
				document.frmParam.elements['powersave.mps_jangka_inv'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['powersave.mps_rate'].value="";				
				document.frmParam.elements['powersave.mps_prm_interest'].value="";
				document.frmParam.elements['powersave.mps_prm_debet'].value="";
				
				document.frmParam.elements['powersave.mps_roll_over'].value="";
				document.frmParam.elements['powersave.mps_roll_over'].disabled=true;
				document.frmParam.elements['powersave.mps_roll_over'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['investasiutama.mu_periodic_tu'].disabled=false;
				document.frmParam.elements['investasiutama.mu_periodic_tu'].style.backgroundColor ='#FFFFFF';
				
				if (document.frmParam.elements['investasiutama.mu_jlh_tu'].value=="")
				{
					document.frmParam.elements['investasiutama.mu_jlh_tu'].value="0";
				}
				document.frmParam.elements['investasiutama.mu_jlh_tu'].readOnly=false;
				document.frmParam.elements['investasiutama.mu_jlh_tu'].style.backgroundColor ='#FFFFFF';
				
	
			break;
			
			case 4:
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].readOnly=false;
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].style.backgroundColor ='#FFFFFF';				
			
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].readOnly=false;
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].style.backgroundColor ='#FFFFFF';
				
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].readOnly=false;
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].style.backgroundColor ='#FFFFFF';				
				
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_jumlah1'].value="";
					
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_jumlah1'].value="";

				document.frmParam.elements['powersave.mps_jangka_inv'].value="";
				document.frmParam.elements['powersave.mps_jangka_inv'].disabled=true;
				document.frmParam.elements['powersave.mps_jangka_inv'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['powersave.mps_rate'].value="";				
				document.frmParam.elements['powersave.mps_prm_interest'].value="";
				document.frmParam.elements['powersave.mps_prm_debet'].value="";
				
				document.frmParam.elements['powersave.mps_roll_over'].value="";
				document.frmParam.elements['powersave.mps_roll_over'].disabled=true;
				document.frmParam.elements['powersave.mps_roll_over'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['investasiutama.mu_periodic_tu'].disabled=false;
				document.frmParam.elements['investasiutama.mu_periodic_tu'].style.backgroundColor ='#FFFFFF';
				
				if (document.frmParam.elements['investasiutama.mu_jlh_tu'].value=="")
				{
					document.frmParam.elements['investasiutama.mu_jlh_tu'].value="0";
				}				
				document.frmParam.elements['investasiutama.mu_jlh_tu'].readOnly=false;
				document.frmParam.elements['investasiutama.mu_jlh_tu'].style.backgroundColor ='#FFFFFF';

			break;
			
			case 5:
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_jumlah1'].value="";
			
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].readOnly=false;
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].style.backgroundColor ='#FFFFFF';				
					
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].readOnly=false;
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].style.backgroundColor ='#FFFFFF';				
				
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_jumlah1'].value="";
				
				document.frmParam.elements['powersave.mps_jangka_inv'].value="";
				document.frmParam.elements['powersave.mps_jangka_inv'].disabled=true;
				document.frmParam.elements['powersave.mps_jangka_inv'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['powersave.mps_rate'].value="";				
				document.frmParam.elements['powersave.mps_prm_interest'].value="";
				document.frmParam.elements['powersave.mps_prm_debet'].value="";
				
				document.frmParam.elements['powersave.mps_roll_over'].value="";
				document.frmParam.elements['powersave.mps_roll_over'].disabled=true;
				document.frmParam.elements['powersave.mps_roll_over'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['investasiutama.mu_periodic_tu'].disabled=false;
				document.frmParam.elements['investasiutama.mu_periodic_tu'].style.backgroundColor ='#FFFFFF';

				if (document.frmParam.elements['investasiutama.mu_jlh_tu'].value=="")
				{
					document.frmParam.elements['investasiutama.mu_jlh_tu'].value="0";
				}				
				document.frmParam.elements['investasiutama.mu_jlh_tu'].readOnly=false;
				document.frmParam.elements['investasiutama.mu_jlh_tu'].style.backgroundColor ='#FFFFFF';
				
			break;
			
			case 6:
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_jumlah1'].value="";
			
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_jumlah1'].value="";
					
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].readOnly=false;
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].style.backgroundColor ='#FFFFFF';				
				
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].readOnly=false;
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].style.backgroundColor ='#FFFFFF';				
				
				document.frmParam.elements['powersave.mps_jangka_inv'].disabled=true;
				document.frmParam.elements['powersave.mps_jangka_inv'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['powersave.mps_rate'].value="";				
				document.frmParam.elements['powersave.mps_prm_interest'].value="";
				document.frmParam.elements['powersave.mps_prm_debet'].value="";
				
				document.frmParam.elements['powersave.mps_roll_over'].value="";
				document.frmParam.elements['powersave.mps_roll_over'].disabled=true;
				document.frmParam.elements['powersave.mps_roll_over'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['investasiutama.mu_periodic_tu'].disabled=false;
				document.frmParam.elements['investasiutama.mu_periodic_tu'].style.backgroundColor ='#FFFFFF';
				
				if (document.frmParam.elements['investasiutama.mu_jlh_tu'].value=="")
				{
					document.frmParam.elements['investasiutama.mu_jlh_tu'].value="0";
				}				
				document.frmParam.elements['investasiutama.mu_jlh_tu'].readOnly=false;
				document.frmParam.elements['investasiutama.mu_jlh_tu'].style.backgroundColor ='#FFFFFF';

			break;														
		}
					
	}
				
	function addRowDOM1 (tableID, jml) {
		flag_add1=1
   		var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
		var tmp = parseInt(document.getElementById("tableProd").rows.length);
		var oRow = oTable.insertRow(oTable.rows.length);
        var oCells = oRow.cells;
		var i=tmp-1;
		var idx = jmlpenerima + 1;
	
		if (document.all) {
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td >"+idx+"</td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td ><input type=text name='benef.msaw_first"+idx+"' value='' size=50></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td><input type=text name='tgllhr"+idx+"' value='' size=3>/<input type=text name='blnhr"+idx+"' value='' size=3>/<input type=text name='thnhr"+idx+"' value='' size=5><input type=hidden name='msaw_birth"+idx+"' value='' size=5></td>";         
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML =  "<td ><select name='benef.msaw_sex"+idx+"'>"+varOptionhub+"</select></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML =  "<td ><select name='benef.lsre_id"+idx+"'>"+varOptionhub+"</select></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML ="<td><input type=text name='benef.msaw_persen"+idx+"' value='' size=5 ></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML ="<td ><input type=checkbox name=cek"+idx+" id=ck"+idx+"' class=\"noBorder\" ></td></tr>";
			var cell = oRow.insertCell(oCells.length);			
			cell.innerHTML = '';
			jmlpenerima = idx;
		}
 	}
 
 
	function next()
	{
		eval( " document.frmParam.elements['pemegang.indeks_halaman'].value = ((document.frmParam.hal.value)+1);");
		//alert(jmlpenerima);
		document.frmParam.jmlpenerima.value=jmlpenerima;
		//alert(document.frmParam.jmlpenerima.value);
	} 
	function prev()
	{
		document.frmParam.jmlpenerima.value=jmlpenerima;
	}	

	function cancel1()
	{		
		var idx = jmlpenerima;
	//(idx)!=null &&
		if  ( (idx)!="")
		{
			if (idx >0)
				flag_add1=1;
		}
		if(flag_add1==1)
		{
			deleteRowDOM1('tableProd', '1');
		}
	}		

function deleteRowDOM1 (tableID,rowCount)
 { 
    var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
    var row=parseInt(document.getElementById("tableProd").rows.length);
	var flag_row=0;
	var jumlah_cek=0;
	var idx = jmlpenerima;

	if(row>0)
	{
		flag_row=0;
		//alert(row);
		for (var i=1;i<((parseInt(row)));i++)
		{

			if (eval("document.frmParam.elements['cek"+i+"'].checked"))
			{
				idx=idx-1;

				for (var k =i ; k<((parseInt(row))-1);k++)
				{

					eval(" document.frmParam.elements['benef.msaw_first"+k+"'].value =document.frmParam.elements['benef.msaw_first"+(k+1)+"'].value;");   
					eval(" document.frmParam.elements['tgllhr"+k+"'].value =document.frmParam.elements['tgllhr"+(k+1)+"'].value;"); 
					eval(" document.frmParam.elements['blnhr"+k+"'].value = document.frmParam.elements['blnhr"+(k+1)+"'].value;");			      
					eval(" document.frmParam.elements['thnhr"+k+"'].value = document.frmParam.elements['thnhr"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['msaw_birth"+k+"'].value = document.frmParam.elements['msaw_birth"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['benef.lsre_id"+k+"'].value = document.frmParam.elements['benef.lsre_id"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['benef.msaw_persen"+k+"'].value = document.frmParam.elements['benef.msaw_persen"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['cek"+k+"'].checked = document.frmParam.elements['cek"+(k+1)+"'].checked;");
				}
				oTable.deleteRow(parseInt(document.getElementById("tableProd").rows.length)-1);
				row=row-1;
				//alert(jmlpenerima);
				jmlpenerima = idx;
				//alert(jmlpenerima);
				i=i-1;
			}
		}


		row=parseInt(document.getElementById("tableProd").rows.length);
			if(row==1)	
				flag_add=0;
		
	}
}

	if (!document.layers&&!document.all&&!document.getElementById)
		event="test"
		
	function showtip(current,e,text){
	
		if (document.all||document.getElementById){
			thetitle=text.split('<br>')
			if (thetitle.length>1){
				thetitles=''
				
			for (i=0;i<thetitle.length;i++)
				thetitles+=thetitle[i]
				
			current.title=thetitles
		}
		else
			current.title=text
	}
	
	else if (document.layers){
				document.tooltip.document.write('<layer bgColor="white" style="border:1px solid black;font-size:13px;">'+text+'</layer>')
				document.tooltip.document.close()
				document.tooltip.left=e.pageX+5
				document.tooltip.top=e.pageY+5
				document.tooltip.visibility="show"
			}
	}
	
	function hidetip(){
		if (document.layers)
			document.tooltip.visibility="hidden"
	}
		