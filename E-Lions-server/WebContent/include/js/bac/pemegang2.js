		
	function generateXML(intValue, objParser, nameSelection, strID, strDesc ,lbr,tgg,tab) {
		var strSelection;
		var strHTML = "<SELECT NAME='" + nameSelection  + "'  style='font-family: Verdana; width: " +lbr+"; height:"+tgg+"; font-size:8pt'>";
		var collPosition = objParser.selectNodes( "//Position" );

		for( var i = 0; i < collPosition.length; i++ ) {
			if ((intValue).toUpperCase() == (collPosition.item( i ).selectSingleNode(strID).text).toUpperCase())
				strSelection = "SELECTED";
			else
				strSelection = "";
			strHTML += "<OPTION " + strSelection + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
		}
		strHTML += "</SELECT>";
		return strHTML;
	}
		
	function generateXML_onChange(intValue, objParser, nameSelection, strID, strDesc ,lbr,tgg,tab) {
		var strSelection;
		var strHTML = "<SELECT NAME='" + nameSelection  + "'  style='font-family: Verdana; width: " +lbr+"; height:"+tgg+"; font-size:8pt'>";
		var collPosition = objParser.selectNodes( "//Position" );
	
		for( var i = 0; i < collPosition.length; i++ ) {
			if ((intValue) == (collPosition.item( i ).selectSingleNode(strID).text))
				strSelection = "SELECTED";
			else
				strSelection = "";
			strHTML += "<OPTION " + strSelection + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
		}
		strHTML += "</SELECT>";
		return strHTML;
	}

	function generateXML_sumberKyc( objParser, strID, strDesc ) {
		Optiondana = "";
		var collPosition = objParser.selectNodes( "//Position" );
		for( var i = 0; i < collPosition.length; i++ ) Optiondana += "<OPTION " + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
	}
	
	function pp_bulan(){
		with(document.frmParam){
			if(bulan_gaji_pp.checked==true){
				elements['pemegang.bulan_gaji'].value = 'GAJI';				
			}else{
				elements['pemegang.bulan_gaji'].value = ' ';
			}
		}
	}
	
	function pp_penghasilan(){
		with(document.frmParam){
			if(bulan_penghasilan_pp.checked==true){
				elements['pemegang.bulan_penghasilan'].value = 'TABUNGAN/DEPOSITO';				
			}else{
				elements['pemegang.bulan_penghasilan'].value = ' ';
			}
		}
	}
	
	function pp_warisan(){
		with(document.frmParam){
			if(warisan.checked==true){
				elements['pemegang.bulan_orangtua'].value = 'WARISAN';				
			}else{
				elements['pemegang.bulan_orangtua'].value = ' ';
			}
		}
	}
	
	function pp_usaha(){
		with(document.frmParam){
			if(bulan_usaha_pp.checked==true){
				elements['pemegang.bulan_usaha'].value = 'HIBAH';				
			}else{
				elements['pemegang.bulan_usaha'].value = ' ';
			}
		}
	}
	
	function pp_lainnya(){
		with(document.frmParam){
			if(bulan_lainnya_pp.checked==true){
				elements['pemegang.bulan_lainnya'].value = 'LAINNYA';				
			}else{
				elements['pemegang.bulan_lainnya'].value = ' ';
			}
		}
	}
	
	function pp_proteksi(){
		with(document.frmParam){
			if(tujuan_proteksi_pp.checked==true){
				elements['pemegang.tujuan_proteksi'].value = 'PROTEKSI';				
			}else{
				elements['pemegang.tujuan_proteksi'].value = ' ';
			}
		}
	}
	
	function pp_investasi(){
		with(document.frmParam){
			if(tujuan_investasi_pp.checked==true){
				elements['pemegang.tujuan_investasi'].value = 'INVESTASI';				
			}else{
				elements['pemegang.tujuan_investasi'].value = ' ';
			}
		}
	}
	
	function pptuj_lainnya(){
		with(document.frmParam){
			if(tujuan_lainnya_pp.checked==true){
				elements['pemegang.tujuan_lainnya'].value = 'LAINNYA';				
			}else{
				elements['pemegang.tujuan_lainnya'].value = ' ';
			}
		}
	}
	
	function cpy(hasil){
		document.frmParam.elements['pemegang.mkl_dana_from'].value = hasil;
		document.frmParam.tanda_pp.value = hasil;
		if (document.frmParam.tanda_pp.value == '0'){
			document.frmParam.elements['pemegang.lsre_id_premi'].disabled = true;
			document.frmParam.elements['pemegang.lsre_id_premi'].style.backgroundColor ='#FFFFFF';
			document.getElementById("mklSumberPremi").disabled = true;
		}else{
			document.getElementById("mklSumberPremi").disabled = false;
		}
 	}
 
 	function cpy2(hasil2){
		document.frmParam.elements['pemegang.mkl_sumber_premi'].value = hasil2;
		document.frmParam.tanda_hub.value = hasil2;
		if (document.frmParam.tanda_hub.value == '0'){
			document.frmParam.elements['pemegang.lsre_id_premi'].disabled = true;
		}else{
			document.frmParam.elements['pemegang.lsre_id_premi'].disabled = false;
		}
	}

	function bixx(flag){
		if(flag=='9'){						
			document.getElementById("expired").disabled = true;
		}else{
			document.getElementById("expired").disabled = false;
		}
	}
		
	function next(){
		eval( " document.frmParam.elements['pemegang.indeks_halaman'].value = ((document.frmParam.hal.value)+1);");
		document.frmParam.jmlDaftarKyc.value=jmlDaftarKyc;
		document.frmParam.jmlDaftarKyc2.value=jmlDaftarKyc2;
	}
	
	function checkAll(form){
		for (var i = 1; i < form.elements.length; i++){
		eval("form.elements[" + i + "].checked = form.elements[0].checked");
		};
	}
	
	function konfirm(){
		var pass;
		with(document.frmParam){         
			if(special_case.value==1){					
				document.getElementById('special_case').checked=false;
				elements['special_case'].value = '0';
				document.getElementById('flag_special_case').value='0';
				return false;
			}else{
				if(confirm('Apakah Anda yakin mau input/edit SPAJ special case?Jika tidak pilih cancel, jika iya pilih Ok dan pastikan data yang diinput sesuai proposal')) {
					pass=prompt(' harap masukkan password untuk special case spaj!');
					if(pass=="Msig2015"){
					 	special_case.checked==true;
						elements['special_case'].value = '1';
						document.getElementById('flag_special_case').value='1';
						return true;
					}else{
					 	alert('Password Salah');
					 	document.getElementById('special_case').checked=false;
						elements['special_case'].value = '0';
						document.getElementById('flag_special_case').value='0';
						return false;	
					};
				}else{
					document.getElementById('special_case').checked=false;
					elements['special_case'].value = '0';
					document.getElementById('flag_special_case').value='0';
					return false;	
				};					
			};
		};
	}
 
	function addRowDOM1 (tableID, jml){
		flag_add1 = 1;
   		var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
//		var tmp = parseInt(document.getElementById("tableDanax").rows.length);
		var oRow = oTable.insertRow(oTable.rows.length);
        var oCells = oRow.cells;
//		var i=tmp-1;
		var idx = jmlDaftarKyc + 1;
		
		if (document.all) {
			var cell = oRow.insertCell(oCells.length);			
				cell.innerHTML = '';
				cell.style.backgroundColor = "#FFF2CA";
			var cell = oRow.insertCell(oCells.length);			
				cell.innerHTML = '';
				cell.style.backgroundColor = "#FFF2CA";
			var cell = oRow.insertCell(oCells.length);
				cell.innerHTML ="<td bgcolor=\"#FFF2CA\"><select name='listkyc.kyc_desc1"+idx+"'>"+Optiondana+"</select></td>";
				cell.style.backgroundColor = "#FFF2CA";
			var cell = oRow.insertCell(oCells.length);
				cell.innerHTML = "<td ><input type=text name='listkyc.kyc_desc_x"+idx+"' value='' size=42 maxlength=50></td>";
				cell.style.backgroundColor = "#FFF2CA";
			var cell = oRow.insertCell(oCells.length);
				cell.innerHTML ="<td ><input type=checkbox class=\"noBorder\" name=cek"+idx+" id=ck"+idx+"' ></td></tr>";
				cell.style.backgroundColor = "#FFF2CA";
				jmlDaftarKyc = idx;
		};
	}
			
	function addRowDOM2 (tableID, jml) {
		flag_add2_x = 1;
   		var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
//		var tmp = parseInt(document.getElementById("tableDanax2").rows.length);
		var oRow = oTable.insertRow(oTable.rows.length);
        var oCells = oRow.cells;
//		var i=tmp-1;
		var idx = jmlDaftarKyc2 + 1;

		if (document.all) {
			var cell = oRow.insertCell(oCells.length);			
				cell.innerHTML = '';
				cell.style.backgroundColor = "#FFF2CA";
			var cell = oRow.insertCell(oCells.length);			
				cell.innerHTML = '';
				cell.style.backgroundColor = "#FFF2CA";
			var cell = oRow.insertCell(oCells.length);
				cell.innerHTML ="<td ><select name='listkyc2.kyc_desc2"+idx+"'>"+Optiondana+"</select></td>";
				cell.style.backgroundColor = "#FFF2CA";
			var cell = oRow.insertCell(oCells.length);
				cell.innerHTML = "<td ><input type=text name='listkyc2.kyc_desc2_x"+idx+"' value='' size=42 maxlength=50></td>";
				cell.style.backgroundColor = "#FFF2CA";
			var cell = oRow.insertCell(oCells.length);
				cell.innerHTML ="<td ><input type=checkbox class=\"noBorder\" name=cek2"+idx+" id=ck2"+idx+"' ></td></tr>";
				cell.style.backgroundColor = "#FFF2CA";
				jmlDaftarKyc2 = idx;
		};
	}		
	
	function deleteRowDOM1 (tableID, rowCount){ 
    	var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
    	var row=parseInt(document.getElementById("tableDanax").rows.length);
//		var flag_row=0;
//		var jumlah_cek=0;
		var idx = jmlDaftarKyc;

		if(row>0){
//			flag_row=0;
			for (var i=1;i<((parseInt(row)));i++){
				if (eval("document.frmParam.elements['cek"+i+"'].checked")){
				idx=idx-1;

				for (var k =i ; k<((parseInt(row))-1);k++){
					eval(" document.frmParam.elements['listkyc.kyc_desc1"+k+"'].value =document.frmParam.elements['listkyc.kyc_desc1"+(k+1)+"'].value;");
					eval(" document.frmParam.elements['listkyc.kyc_desc_x"+k+"'].value =document.frmParam.elements['listkyc.kyc_desc_x"+(k+1)+"'].value;");  
					eval(" document.frmParam.elements['cek"+k+"'].checked = document.frmParam.elements['cek"+(k+1)+"'].checked;");
				}
				oTable.deleteRow(parseInt(document.getElementById("tableDanax").rows.length)-1);
				row=row-1;
				jmlDaftarKyc = idx;
				i=i-1;
			};
		}
		row=parseInt(document.getElementById("tableDanax").rows.length);
			if(row==1)	
				flag_add=0;
		};
	}
	
	function deleteRowDOM2 (tableID, rowCount){ 
    	var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
    	var row=parseInt(document.getElementById("tableDanax2").rows.length);
//		var flag_row=0;
//		var jumlah_cek=0;
		var idx = jmlDaftarKyc2;

		if(row>0){
//			flag_row=0;
			for (var i=1;i<((parseInt(row)));i++){
				if (eval("document.frmParam.elements['cek2"+i+"'].checked")){
					idx=idx-1;

					for (var k =i ; k<((parseInt(row))-1);k++){
						eval(" document.frmParam.elements['listkyc2.kyc_desc2"+k+"'].value =document.frmParam.elements['listkyc2.kyc_desc2"+(k+1)+"'].value;");
						eval(" document.frmParam.elements['listkyc2.kyc_desc2_x"+k+"'].value =document.frmParam.elements['listkyc2.kyc_desc2_x"+(k+1)+"'].value;");  
						eval(" document.frmParam.elements['cek2"+k+"'].checked = document.frmParam.elements['cek2"+(k+1)+"'].checked;");
					}
					oTable.deleteRow(parseInt(document.getElementById("tableDanax2").rows.length)-1);
					row=row-1;
				jmlDaftarKyc2 = idx;
				i=i-1;
			};
		}
		row=parseInt(document.getElementById("tableDanax2").rows.length);
			if(row==1)	
				flag_add2=0;
		};
	}
	
	function cancel1(){		
		var idx = jmlDaftarKyc;

		if ((idx)!=null && (idx)!=""){
			if (idx >0) flag_add1=1;
		}
		if(flag_add1==1){
			deleteRowDOM1('tableDanax', '1');
		};
	}
	
	function cancel2(){		
		var idx = jmlDaftarKyc2;

		if ((idx)!=null && (idx)!=""){
			if (idx >0) flag_add2_x=1;
		}
		if(flag_add2_x==1){
			deleteRowDOM2('tableDanax2', '1');
		};
	}
	
/*
	if (!document.layers&&!document.all&&!document.getElementById)
		event="test"
			function showtip(current,e,text){
			if (document.all||document.getElementById){
				thetitle=text.split('<br>')
				if (thetitle.length>1){thetitles=''
					for (i=0;i<thetitle.length;i++)
						thetitles+=thetitle[i]
					current.title=thetitles
				}else {current.title=text}
			}else if (document.layers){
				document.tooltip.document.write('<layer bgColor="white" style="border:1px solid black;font-size:13px;">'+text+'</layer>')
				document.tooltip.document.close()
				document.tooltip.left=e.pageX+5
				document.tooltip.top=e.pageY+5
				document.tooltip.visibility="show"
			}}
*/
	function buttonLinks(str){
		if(str=='cari'){
			spaj = document.frmParam.spaj.value;
			if(spaj==''){
				alert('Harap cari SPAJ terlebih dahulu');
			}else{
				document.getElementById('infoFrame').src='${path}/bac/edit.htm?showSPAJ='+document.frmParam.spaj.value;
			}
		}
	}
			
	function resetPemegangPolis(){
		parent.document.getElementById('jenis_pemegang_polis').value = document.frmParam.elements['datausulan.jenis_pemegang_polis'].value;
		parent.document.getElementById('info').click();
	}
	
	function flag_upload_onClick(){
		with(document.frmParam) {
			if(flag_upload.checked==true){
				elements['pemegang.flag_upload'].value = '1';
			}else{
				elements['pemegang.flag_upload'].value = '0';
			}
		}
	}
	
	function flag_idProp_onClick(){
		with(document.frmParam){
			if(flag_upload2.checked==true){
				elements['pemegang.flag_upload'].value = '2';				
			}else if(flag_upload3.checked==true){
				elements['pemegang.flag_upload'].value = '3';
			}else{
				elements['pemegang.flag_upload'].value = '0';
			}
		}
	} 	
	
	function edit_onClick() {
		with (document.frmParam) {
			if (edit_pemegang.checked == true) {
				document.getElementById('edit_pemegang').checked = true;
				document.getElementById('edit_pemegang').value = '1';
				elements['pemegang.edit_pemegang'].value = '1';
			} else {
				document.getElementById('edit_pemegang').value = '0';
				elements['pemegang.edit_pemegang'].value = '0';
			}
		}
	}

	function flag_idGadget_checked() {
		document.getElementById('flag_upload3').value == '3';
		document.getElementById('flag_upload3').checked = true;
	}
	
	function fullautosales_onClick(){
		with(document.frmParam){
			if(full_autosales.checked==true){
				elements['datausulan.full_autosales'].value = '4';				
			}else{
				elements['datausulan.full_autosales'].value = '1';
			}
		}
	}