	function edit_onClick(){
			with(document.frmParam){
			if(edit_pemegang.checked==true){
				document.getElementById('edit_pemegang').checked=true;
				document.getElementById('edit_pemegang').value='1';
				elements['pemegang.edit_pemegang'].value = '1';
			}else{
				document.getElementById('edit_pemegang').value='0';
				elements['pemegang.edit_pemegang'].value = '0';	
			}
		}
	}
	
	function loaddata_penagihan()
	{
		if (document.frmParam.elements['addressbilling.tagih'].value == "1")
		{
			if (document.frmParam.elements['addressbilling.kota_tgh'].value=="")
			{document.frmParam.elements['addressbilling.kota_tgh'].value="";
			}else{document.frmParam.elements['addressbilling.kota_tgh'].value= v_kota_tgh;}
			document.frmParam.elements['addressbilling.kota_tgh'].disabled = false;
			document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['addressbilling.msap_address'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#FFFFFF';
			if ((document.frmParam.elements['addressbilling.msap_address'].value=="")||(document.frmParam.elements['addressbilling.msap_address'].value==""))
			{	document.frmParam.elements['addressbilling.msap_address'].value='';
			}else{document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.alamat_sementara.value;}
			document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#FFFFFF';						
			if ((document.frmParam.elements['addressbilling.msap_zip_code'].value=="")||(document.frmParam.elements['addressbilling.msap_zip_code'].value==""))
			{document.frmParam.elements['addressbilling.msap_zip_code'].value='';
			}else{document.frmParam.elements['addressbilling.msap_zip_code'].value= v_msap_zip_code;}			
			
			document.frmParam.elements['addressbilling.lsne_id'].readOnly = false;
			document.frmParam.elements['addressbilling.lsne_id'].style.backgroundColor ='#FFFFFF';						
			if ((document.frmParam.elements['addressbilling.lsne_id'].value=="")||(document.frmParam.elements['addressbilling.lsne_id'].value==""))
			{document.frmParam.elements['addressbilling.lsne_id'].value='';
			}else{document.frmParam.elements['addressbilling.lsne_id'].value= v_lsne_id;}
			
			document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#FFFFFF';						
			if ((document.frmParam.elements['addressbilling.msap_area_code1'].value=="")||(document.frmParam.elements['addressbilling.msap_area_code1'].value==""))
			{document.frmParam.elements['addressbilling.msap_area_code1'].value='';
			}else{document.frmParam.elements['addressbilling.msap_area_code1'].value= v_msap_area_code1;}
			document.frmParam.elements['addressbilling.msap_phone1'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#FFFFFF';						
			if ((document.frmParam.elements['addressbilling.msap_phone1'].value=="")||(document.frmParam.elements['addressbilling.msap_phone1'].value==""))
			{document.frmParam.elements['addressbilling.msap_phone1'].value='';
			}else{document.frmParam.elements['addressbilling.msap_phone1'].value= v_msap_phone1;}
			document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#FFFFFF';						
			if ((document.frmParam.elements['addressbilling.msap_area_code2'].value=="")||(document.frmParam.elements['addressbilling.msap_area_code2'].value==""))
			{document.frmParam.elements['addressbilling.msap_area_code2'].value='';
			}else{document.frmParam.elements['addressbilling.msap_area_code2'].value= v_msap_area_code2;}
			document.frmParam.elements['addressbilling.msap_phone2'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#FFFFFF';						
			if ((document.frmParam.elements['addressbilling.msap_phone2'].value=="")||(document.frmParam.elements['addressbilling.msap_phone2'].value==""))
			{document.frmParam.elements['addressbilling.msap_phone2'].value='';
			}else{document.frmParam.elements['addressbilling.msap_phone2'].value= v_msap_phone2;}
			document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#FFFFFF';						
			document.frmParam.elements['addressbilling.msap_area_code3'].value=v_msap_area_code3;
			document.frmParam.elements['addressbilling.msap_phone3'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#FFFFFF';						
			document.frmParam.elements['addressbilling.msap_phone3'].value= v_msap_phone3;
// 			document.frmParam.elements['addressbilling.no_hp'].value=v_p_no_hp;
// 			document.frmParam.elements['addressbilling.no_hp2'].value= v_p_no_hp2;
		}else{
			if (document.frmParam.elements['addressbilling.tagih'].value=="2")
			{if (document.frmParam.elements['datausulan.jenis_pemegang_polis'].value == "1"){
					if (document.frmParam.elements['contactPerson.kota_rumah'].value == "")
					{document.frmParam.elements['addressbilling.kota_tgh'].value="";
					}else{document.frmParam.elements['addressbilling.kota_tgh'].value= v_cp_kota_rumah;}
					document.frmParam.elements['addressbilling.kota_tgh'].disabled = true;
					document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.alamat_sementara1.value;
					document.frmParam.elements['addressbilling.msap_zip_code'].value= v_cp_kd_pos_rumah;		
					document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D4D4D4';
					
					document.frmParam.elements['addressbilling.lsne_id'].value= v_cp_lsne_id;		
					document.frmParam.elements['addressbilling.lsne_id'].readOnly = true;
					document.frmParam.elements['addressbilling.lsne_id'].style.backgroundColor ='#D4D4D4';
					
					document.frmParam.elements['addressbilling.msap_area_code1'].value= v_cp_area_code_rumah;		
					document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone1'].value= v_cp_telpon_rumah;
					document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_area_code2'].value= v_cp_area_code_rumah2;		
					document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone2'].value= v_cp_telpon_rumah2;
					document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D4D4D4';					
					document.frmParam.elements['addressbilling.msap_area_code3'].value='';		
					document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone3'].value='';
					document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D4D4D4';	
// 					document.frmParam.elements['addressbilling.no_hp'].value= v_cp_no_hp;
// 					document.frmParam.elements['addressbilling.no_hp2'].value= v_cp_no_hp2;
				}else{
					if (document.frmParam.elements['pemegang.kota_rumah'].value == "")
					{document.frmParam.elements['addressbilling.kota_tgh'].value="";
					}else{document.frmParam.elements['addressbilling.kota_tgh'].value=v_p_kota_rumah;}
					document.frmParam.elements['addressbilling.kota_tgh'].disabled = true;
					document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.alamat_sementara1.value;
					document.frmParam.elements['addressbilling.msap_zip_code'].value= v_p_kd_pos_rumah;		
					document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D4D4D4';
					
					document.frmParam.elements['addressbilling.lsne_id'].value= v_p_lsne_id;		
					document.frmParam.elements['addressbilling.lsne_id'].readOnly = true;
					document.frmParam.elements['addressbilling.lsne_id'].style.backgroundColor ='#D4D4D4';					
					
					document.frmParam.elements['addressbilling.msap_area_code1'].value= v_p_area_code_rumah;		
					document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone1'].value= v_p_telpon_rumah;
					document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_area_code2'].value= v_p_area_code_rumah2;		
					document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone2'].value= v_p_telpon_rumah2;
					document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D4D4D4';					
					document.frmParam.elements['addressbilling.msap_area_code3'].value='';		
					document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone3'].value='';
					document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D4D4D4';	
// 					document.frmParam.elements['addressbilling.no_hp'].value= v_p_no_hp;
// 					document.frmParam.elements['addressbilling.no_hp2'].value= v_p_no_hp2;
				}
			}else{
				if (document.frmParam.elements['addressbilling.tagih'].value=="3")
				{if (document.frmParam.elements['datausulan.jenis_pemegang_polis'].value == "1"){
						if (document.frmParam.elements['contactPerson.kota_kantor'].value == "")
						{document.frmParam.elements['addressbilling.kota_tgh'].value="";
						}else{document.frmParam.elements['addressbilling.kota_tgh'].value= v_cp_kota_kantor;}
						document.frmParam.elements['addressbilling.kota_tgh'].disabled = true;
						document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.alamat_sementara2.value;
						document.frmParam.elements['addressbilling.msap_zip_code'].value= v_cp_kd_pos_kantor;		
						document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D4D4D4';
						
						document.frmParam.elements['addressbilling.lsne_id'].value= v_cp_lsne_id;		
						document.frmParam.elements['addressbilling.lsne_id'].readOnly = true;
						document.frmParam.elements['addressbilling.lsne_id'].style.backgroundColor ='#D4D4D4';
						
						document.frmParam.elements['addressbilling.msap_area_code1'].value= v_cp_area_code_kantor;		
						document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_phone1'].value= v_cp_telpon_kantor;
						document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_area_code2'].value= v_cp_area_code_kantor2;		
						document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_phone2'].value= v_cp_telpon_kantor2;
						document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_area_code3'].value='';document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_phone3'].value='';document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D4D4D4';
// 						document.frmParam.elements['addressbilling.no_hp'].value= v_p_no_hp;
// 						document.frmParam.elements['addressbilling.no_hp2'].value=v_p_no_hp2;
					}else{
						if (document.frmParam.elements['pemegang.kota_kantor'].value == "")
						{document.frmParam.elements['addressbilling.kota_tgh'].value="";
						}else{document.frmParam.elements['addressbilling.kota_tgh'].value= v_p_kota_kantor;}
						document.frmParam.elements['addressbilling.kota_tgh'].disabled = true;
						document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.alamat_sementara2.value;
						document.frmParam.elements['addressbilling.msap_zip_code'].value= v_p_kd_pos_kantor;		
						document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D4D4D4';
						
						document.frmParam.elements['addressbilling.lsne_id'].value= v_p_lsne_id;		
						document.frmParam.elements['addressbilling.lsne_id'].readOnly = true;
						document.frmParam.elements['addressbilling.lsne_id'].style.backgroundColor ='#D4D4D4';						
						
						document.frmParam.elements['addressbilling.msap_area_code1'].value= v_p_area_code_kantor;		
						document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_phone1'].value= v_p_telpon_kantor;
						document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_area_code2'].value= v_p_area_code_kantor2;		
						document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_phone2'].value= v_p_telpon_kantor2;document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_area_code3'].value='';document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_phone3'].value='';document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D4D4D4';		
// 						document.frmParam.elements['addressbilling.no_hp'].value= v_p_no_hp;
// 						document.frmParam.elements['addressbilling.no_hp2'].value=v_p_no_hp2;			
				}	}	}	}   }
	
	function tanyaPsave(spaj){
		//uncomment yg dibawah ini
		/*if(confirm('Apakah Spaj ini merupakan konversi produk [Powersave / Simas Prima] ke [Stable Link / Simas Stabil Link] ?\nTekan OK bila YA, atau tekan CANCEL bila TIDAK.')){
			window.location=v_path.concat(/bac/edit.htm?isPsaveToSlink=true&kopiSPAJ='+document.frmParam.kopiSPAJ.value);
		}else{
			window.location=v_path.concat(/bac/edit.htm?kopiSPAJ='+document.frmParam.kopiSPAJ.value);
		}*/
		//comment yg dibawah ini
		window.location=v_path.concat('/bac/editspajnew.htm?kopiSPAJ='+document.frmParam.kopiSPAJ.value);
		/*return false;*/
	}
	
	function konfirm(){
		 var pass;
			with(document.frmParam) {           
            
				if(special_case.value==1){					
							document.getElementById('special_case').checked=false;
							elements['special_case'].value = '0';
							document.getElementById('flag_special_case').value='0';
							return false;
				
				}else{
					if(confirm('Apakah Anda yakin mau input/edit SPAJ special case?Jika tidak pilih cancel, jika iya pilih Ok dan pastikan data yang diinput sesuai proposal')) {
							 pass=prompt(' harap masukkan password untuk special case spaj!');
							 if(pass=="Msig2015"){
							 	special_case.checked==true;
								elements['special_case'].value = '1';
								document.getElementById('flag_special_case').value='1';
								return true;
							 }else{
							 	alert('Password Salah');
							 	document.getElementById('special_case').checked=false;
								elements['special_case'].value = '0';
								document.getElementById('flag_special_case').value='0';
								return false;	
							 };
							 
					}else{
							
						document.getElementById('special_case').checked=false;
								elements['special_case'].value = '0';
								document.getElementById('flag_special_case').value='0';
								return false;	
					};					
					
				};
			};
	} 
	
	function showtip(current,e,text){
		if (document.all||document.getElementById){
			thetitle=text.split('<br>');
			if (thetitle.length>1){thetitles='';
				for (i=0;i<thetitle.length;i++)
					thetitles+=thetitle[i];
				current.title=thetitles;
			}else {current.title=text};
		}else if (document.layers){
			document.tooltip.document.write('<layer bgColor="white" style="border:1px solid black;font-size:13px;">'+text+'</layer>');
			document.tooltip.document.close();
			document.tooltip.left=e.pageX+5;
			document.tooltip.top=e.pageY+5;
			document.tooltip.visibility="show";
		}}
	
	function bixx(flag){
		if(flag=='9'){						
			document.getElementById("expired").disabled = true;
		}else{
			document.getElementById("expired").disabled = false;
		}
	}
	
	function buttonLinks(str){
		if(str=='cari'){
			spaj = document.frmParam.spaj.value;
			if(spaj==''){alert('Harap cari SPAJ terlebih dahulu');
			}else{document.getElementById('infoFrame').src=v_path.concat('/bac/edit.htm?showSPAJ='+document.frmParam.spaj.value);
			}
		}
	}
	
	function resetPemegangPolis(){
		parent.document.getElementById('jenis_pemegang_polis').value = document.frmParam.elements['datausulan.jenis_pemegang_polis'].value;
		parent.document.getElementById('info').click();
	}