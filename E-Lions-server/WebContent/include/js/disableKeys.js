// ============= DisableKeys.js =============//

// Keys to be disabled can be added to the lists below.
// The number is the key code for the particular key
// and the text is the description displayed in the
// status window if the key [combination] is pressed.

var badKeys = new Object();
badKeys.single = new Object();
badKeys.single['8'] = 'Backspace outside text fields';
badKeys.single['13'] = 'Enter outside input fields';
badKeys.single['116'] = 'F5 (Refresh)';
badKeys.single['122'] = 'F11 (Full Screen)';

badKeys.alt = new Object();
badKeys.alt['37'] = 'Alt+Left Cursor';
badKeys.alt['39'] = 'Alt+Right Cursor';

badKeys.ctrl = new Object();
badKeys.ctrl['78'] = 'Ctrl+N';
badKeys.ctrl['79'] = 'Ctrl+O';

function checkKeyCode(type, code) {
	if (badKeys[type][code]) {
		return true;
	} else {
		return false;
	}
}

function getKeyText(type, code) {
	return badKeys[type][code];
}

var ie=document.all;
var w3c=document.getElementById&&!document.all;

function keyEventHandler(evt) {
	this.target = evt.target || evt.srcElement;
	this.keyCode = evt.keyCode || evt.which;
	var targtype = this.target.type;
	if (w3c) {
		if (document.layers) {
			this.altKey = ((evt.modifiers & Event.ALT_MASK) > 0);
			this.ctrlKey = ((evt.modifiers & Event.CONTROL_MASK) > 0);
			this.shiftKey = ((evt.modifiers & Event.SHIFT_MASK) > 0);
		} else {
			this.altKey = evt.altKey;
			this.ctrlKey = evt.ctrlKey;
		}
	// Internet Explorer
	} else {
		this.altKey = evt.altKey;
		this.ctrlKey = evt.ctrlKey;
	}
	// Find out if we need to disable this key combination
	var badKeyType = "single";
	if (this.ctrlKey) {
		badKeyType = "ctrl";
	} else if (this.altKey) {
		badKeyType = "alt";
	}
	if (checkKeyCode(badKeyType, this.keyCode)) {
		return cancelKey(evt, this.keyCode, this.target, getKeyText(badKeyType, this.keyCode));
	}
}

/* UN-COMMENT FUNGSI YANG INI APABILA INGIN MENGGUNAKAN TOMBOL ENTER UNTUK PINDAH ANTAR INPUT FIELD (AKAN MENIMBULKAN MASALAH PADA TEXTAREA)
function cancelKey(evt, keyCode, target, keyText) {
	//apabila tombol Enter atau Backspace
	if (keyCode==8 || keyCode==13) {
		// Apabila input fields kecuali radio button, maka apabila user menekan tombol ENTER, fungsinya sama seperti tombol TAB
		if (target.type == "text" || target.type == "textarea" || target.type == "password" || target.type == "select-one" || target.type == "checkbox" ) {
			window.status = "";
			if(keyCode==13) evt.keyCode=9; //convert ENTER ke TAB
			if(keyCode==8 && is_date_field) { //khusus datepicker
				is_date_field = false;
				return false;
			}else return true;
		}else if(target.type == "radio"){ //kalo radio button
			if(keyCode==13) {
				evt.keyCode=9;
				return true;
			}else return false;
		}else if (target.type == "button" || target.type == "submit" || target.type == "reset") {
			window.status = "";
			target.click(); //pressing Enter on buttons will press the button
			return false;
		}
	}
	
	//if(keyCode==13) evt.keyCode=9;
	
	if (evt.preventDefault) {
		evt.preventDefault();
		evt.stopPropagation();
	} else {
		evt.keyCode = 0;
		evt.returnValue = false;
	}
	window.status = keyText+" is disabled";
	return false;
}
*/

function cancelKey(evt, keyCode, target, keyText) {
	//if (keyCode==8 || keyCode==13) {
		// Don't want to disable Backspace or Enter in text fields
		//if (target.type == "text" || target.type == "textarea" || target.type == "password") {
		//	window.status = "";
		//	return true;
		//}
	//}
	if (keyCode==13) {
		if (target.type == "textarea") {
			window.status = "";
			return true;
		}
	}else if(keyCode==8){
		if (target.type == "text" || target.type == "textarea" || target.type == "password") {
			window.status = "";
			if(is_date_field) { //khusus datepicker
				is_date_field = false;
				return false;
			}else{
				return true;
			}
		}
	}
	if (evt.preventDefault) {
		evt.preventDefault();
		evt.stopPropagation();
	} else {
		evt.keyCode = 0;
		evt.returnValue = false;
	}
	window.status = keyText+" is disabled";
	return false;
}

function addEvent(obj, evType, fn, useCapture) {
	// General function for adding an event listener
	if (obj.addEventListener) {
		obj.addEventListener(evType, fn, useCapture);
		return true;
	} else if (obj.attachEvent) {
		var r = obj.attachEvent("on" + evType, fn);
		return r;
	} else {
		alert(evType+" handler could not be attached");
	}
}

function addKeyEvent() {
	// Specific function for this particular browser
	var e = (document.addEventListener) ? 'keypress' : 'keydown';
	addEvent(document,e,keyEventHandler,false);
}
addKeyEvent();
//To disable the right mouse button
document.oncontextmenu=new Function("return false");

// ============= DisableKeys.js =============//