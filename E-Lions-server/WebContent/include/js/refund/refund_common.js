REFUND_LOOK_UP = 0;
REFUND_EDIT = 1;
STD_DOWNLOAD_JSP = 2;
PREVIEW_LAMP_1_JSP = 3;
PREVIEW_LAMP_3_JSP = 4;
PREVIEW_INSTRUKSI_REDEMPT_JSP = 5;
SIGN_IN_JSP = 6;
PREVIEW_EDIT_JSP = 7;
REFUND_REKAP_LOOKUP_JSP = 8;
PREVIEW_EDIT_SETORAN = 9;
REFUND_LOOKUP_AKSEPTASI_JSP = 10;

CABANG_EDIT = 0;

alasanLain2 = '99';
tindakanRefundPremi = '2';
tindakanGantiTertanggung = '3';
tindakanGantiPlan = '4';

function submitForm( id )
{
    if( document.getElementById( 'textMsg' ) != null )
    {
        document.getElementById( 'textMsg' ).value = 'Sedang diproses...';
    }
    id = '_target' + id;
    var form = document.getElementById('formpost');
    var fullActionStr = form.action;
    var pureActionIdx = fullActionStr.lastIndexOf( 'htm' ) + 3;
    var pureActionStr = fullActionStr.substring( 0, pureActionIdx );
    form.action = pureActionStr + '?' + id;
    form.submit();
}

function showLabelOfDropDown( id )
{
    return document.getElementById(id)[ document.getElementById(id).selectedIndex ].innerHTML;
}

function showForm( paramsId, isShowed )
{
    if( isShowed == 'true' )
    {
        document.getElementById(paramsId).style.display = '';
    }
    else
    {
        document.getElementById(paramsId).style.display = 'none';
    }
}

function formatCurrency( num )
{
    num = num.toString().replace(/\,/g, '');
    if( isNaN(num) )
        num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num * 100 + 0.50000000001);
    cents = num % 100;
    num = Math.floor(num / 100).toString();
    if( cents < 10 )
        cents = "0" + cents;
    for( var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++ )
        num = num.substring(0, num.length - (4 * i + 3)) + ',' +
              num.substring(num.length - (4 * i + 3));
    return (((sign) ? '' : '-') + num + '.' + cents);
}

function showFormatCurrency( paramName, value )
{
    document.getElementById(paramName).value = formatCurrency(value);
}

function change_bgcolor( obj, color )
{
    obj.style.backgroundColor = color;
}

function checkForm( id, isChecked )
{
    document.getElementById(id).checked = isChecked;
}

function selDeselectAllChk()
{
    var ii;
    var bool;
    var form = document.getElementById('formpost');
    bool = form.chkAll.checked;
    for( ii = 0; ii < form.chkBox.length; ++ii )
    {
        form.chkBox[ii].checked = bool;
    }
}

function setSelectListToValue(value, selectId)
{
    var i, si, v, args=setSelectListToValue.arguments;
    if ((obj=document.getElementById(args[1])) != null){
        v = args[0];
        for(i=0; i<obj.length; i++){
            if(obj.options[i].value == v){
                si = i;
            }
        }
        obj.selectedIndex = si;
    }
}

function setDisabledBgColor() {
 var frm = document.forms[0] ;
 var len = frm.elements.length ;
var cnt = 0 ;
 for ( var i=0; i < len; i++) {
  var elem = frm.elements[i] ;
  if (elem.type != "hidden" && elem.type != "button" && elem.id != 'textMsg' ) {
		elem.style.backgroundColor = elem.disabled == true?"#FFFFB3":"#FFFFFF" ; 
    }
 }
}

