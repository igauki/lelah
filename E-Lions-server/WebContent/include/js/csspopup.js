/*
    * toggle(div_id)
    	This simply toggles the inserted div name from being displayed (display:block) to being hidden (display:none).
    * blanket_size(popUpDivVar)
    	This resizes the blanket to fit the height of the page because there is not height=100% attribute. This also centers the popUp vertically.
    * window_pos(popUpDivVar)
    	This centers the popUp vertically.
    * popup(windowname)
    	This function contains the other three to make life simple in the HTML file.

	Contoh:
	    	
	<div id="blanket" style=display:none;"></div>
	
	<div id="popUpDiv" style="display:none;">
		<a href="#" onclick="popup('popUpDiv')">Click Me To Close</a>
	</div>
	
	<a href="#" onclick="popup('popUpDiv')">Click Here To Open The Pop Up</a>
    	
*/

function toggle(div_id) {
	var el = document.getElementById(div_id);
	if (el.style.display == "none") {
		el.style.display = "block";
	} else {
		el.style.display = "none";
	}
}
function blanket_size(popUpDivVar) {
	if (typeof window.innerWidth != "undefined") {
		viewportheight = window.innerHeight;
	} else {
		viewportheight = document.documentElement.clientHeight;
	}
	if ((viewportheight > document.body.parentNode.scrollHeight) && (viewportheight > document.body.parentNode.clientHeight)) {
		blanket_height = viewportheight;
	} else {
		if (document.body.parentNode.clientHeight > document.body.parentNode.scrollHeight) {
			blanket_height = document.body.parentNode.clientHeight;
		} else {
			blanket_height = document.body.parentNode.scrollHeight;
		}
	}
	var blanket = document.getElementById("blanket");
	blanket.style.height = blanket_height + "px";
	var popUpDiv = document.getElementById(popUpDivVar);
	popUpDiv_height = blanket_height / 2 - 150;//150 is half popup's height
	popUpDiv.style.top = popUpDiv_height + "px";
}
function window_pos(popUpDivVar) {
	if (typeof window.innerWidth != "undefined") {
		viewportwidth = window.innerHeight;
	} else {
		viewportwidth = document.documentElement.clientHeight;
	}
	if ((viewportwidth > document.body.parentNode.scrollWidth) && (viewportwidth > document.body.parentNode.clientWidth)) {
		window_width = viewportwidth;
	} else {
		if (document.body.parentNode.clientWidth > document.body.parentNode.scrollWidth) {
			window_width = document.body.parentNode.clientWidth;
		} else {
			window_width = document.body.parentNode.scrollWidth;
		}
	}
	var popUpDiv = document.getElementById(popUpDivVar);
	window_width = window_width / 2 - 150;//150 is half popup's width
	popUpDiv.style.left = window_width + "px";
}
function popup(windowname) {
	blanket_size(windowname);
	window_pos(windowname);
	toggle("blanket");
	toggle(windowname);
}