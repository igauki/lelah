fixMozillaZIndex=true; //Fixes Z-Index problem  with Mozilla browsers but causes odd scrolling problem, toggle to see if it helps
_menuCloseDelay=10;
_menuOpenDelay=10;
_subOffsetTop=2;
_subOffsetLeft=-2;
images = new Array(
	pathElions + "/include/image/arrow.gif", //0
	pathElions + "/include/image/winxp.gif", //1
	pathElions + "/include/image/xpblank.gif" //2
);

with(XPMainStyle=new mm_style()){
styleid=1;
bordercolor="#8A867A";
borderstyle="solid";
borderwidth=1;
fontfamily = 'Verdana, Geneva, Arial, Helvetica, sans-serif';
fontsize="11px";
fontstyle="normal";
fontweight="normal";
offbgcolor="#ECE9D8";
offcolor="#000000";
onbgcolor="#C1D2EE";
onborder="1px solid #316AC5";
oncolor="#000000";
padding=3;
rawcss="padding-left:4px;padding-right:4px";
}

with(XPMenuStyle=new mm_style()){
bordercolor="#8A867A";
borderstyle="solid";
borderwidth=1;
fontfamily = 'Verdana, Geneva, Arial, Helvetica, sans-serif';
fontsize="11px";
fontstyle="normal";
fontweight="normal";
image=images[2];
imagepadding=3;
menubgimage=images[1];
offbgcolor="transparent";
offcolor="#000000";
onbgcolor="#C1D2EE";
onborder="1px solid #316AC5";
oncolor="#000000";
outfilter="randomdissolve(duration=0.05)";
overfilter="Fade(duration=0.05);Shadow(color=#999999, Direction=135, Strength=5)";
padding=1;
separatoralign="right";
separatorcolor="#C5C2B8";
separatorpadding=1;
separatorwidth="80%";
subimage=images[0];
subimagepadding=3;
menubgcolor="#ffffff";
}