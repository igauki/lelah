/*
	Small additional function to open iframes & display breadcrumbs (Yusuf)
*/
function openIFrame(iFrameId,winURL){
	ifId=gmobj(iFrameId);
	ifId.src=winURL;
	
	var breadCrumb = "";
	var i = _itemRef;
	do{
		if(breadCrumb == "") breadCrumb = '<b>' + _mi[i][1] + '</b>';
		else breadCrumb = _mi[i][1] + " > " + breadCrumb;
		i = getParentItemByItem(i);
	}while(!isNaN(i));
	document.getElementById('roti').innerHTML = breadCrumb;
}