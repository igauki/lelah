/*
Daftar fungsi yang ada dibawah : 
	1. formatSpaj, contoh : formatSpaj('01200600015') = '01.2006.00015'
	2. formatPolis, contoh : formatPolis('22045200400061') = '22.045.2004.00061'
	3. setupPanes dan showPane, untuk menampilkan halaman dalam tab-tab, contoh ada di elions/common/console.jsp
	4. checkDate, enableDisableDate, dan inputDate, untuk menampilkan datepicker, beserta fungsi pembantu-nya
	5. setSelectionRange, getSelectionStart, getSelectionEnd, fungsi2 yg berhubungan dgn posisi cursor dlm textbox/textarea
	6. addZero dan formatDate, mirip seperti SimpleDateFormat pada JAVA
	7. checkAll, untuk check / uncheck semua checkbox dgn nama yg sama
	8. hideLoadingMessage dan createLoadingMessage, untuk tampilkan/sembunyikan loading message seperti GMAIL
	9. printIFrame, cetak content dari iframe
	10. textCounter, membatasi jumlah text input pada textarea
	11. trim dan trimWhiteSpace, sama gunanya, untuk trim isi dari suatu text
	12. setFocus, untuk focus ke suatu elemen dalam html
	13. timeOut, menjalankan suatu fungsi setelah timeout delay bbrp detik 
	14.a. addOptionToSelect, menambahkan isi dari suatu select box
	14.b. removeOptionFromSelect, menghapus sebuah option yang dipilih dari select box
	15. showHideMenu, menyembunyikan/menampilkan suatu elemen
	16. changeCaption, merubah text / judul dari suatu elemen
	17. enableDisableAll, enable / disable semua elemen
	18. enableDisable, enable/disable suatu elemen
	19. setFrameSize, merubah ukuran iframe (tingginya saja)
	20. log_out, untuk konfirmasi log_out
	21. popWin, popWinToolbar, popWinModal, untuk Popup Window, persis ditengah layar
	22. isAnyUpper, merubah semua isi string menjadi upperCase
	23. resizeCenter, untuk merubah ukuran window, lalu memposisikan di tengah layar
	24. createCookie, buat cookie
	25. readCookie, baca cookie
	26. eraseCookie, hapus cookie
	27. formatNumber, contoh : formatNumber('21.65478923') = '21.65'
	28. formatCurrencyRupiah Menampilkan format jumlah uang dalam bentuk rupiah dan angka dibelakang koma diganti menjadi ,- . Contoh : 1.000.000,-
	29. loadXMLDoc, Untuk menampilkan data dari file XML(compatible untuk semua browser)
*/

var is_gecko = /gecko/i.test(navigator.userAgent);
var is_ie    = /MSIE/.test(navigator.userAgent);
var is_date_field = false;
var panes = new Array();

function loadXMLDoc(dname) {
		if (window.XMLHttpRequest) {
			xhttp=new XMLHttpRequest();
		  }
		else {
			xhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xhttp.open("GET",dname,false);
		xhttp.send("");
		return xhttp.responseXML;
	}

/* Format Nomor SPAJ */
function formatSpaj(nomor){
	if (nomor.length == 7) {
		return nomor;
	} else if (nomor.length == 9) {
		return (nomor.substring(0, 4) + "." + nomor.substring(4));
	} else if (nomor.length == 11) {
		return (nomor.substring(0, 2) + "." + nomor.substring(2, 6) + "."
		+ nomor.substring(6));
	} else 
		return nomor;	
}

/* Format Nomor Polis */
function formatPolis(nomor){
	if (nomor.length == 9) {
		return (nomor.substring(0, 2) + "." + nomor.substring(2));
	} else if (nomor.length == 11) {
		return (nomor.substring(0, 2) + "." + nomor.substring(2, 6) + "."
				+ nomor.substring(6));
	} else if (nomor.length == 14) {
		return (nomor.substring(0, 2) + "." + nomor.substring(2, 5) + "."
				+ nomor.substring(5, 9) + "." + nomor.substring(9));
	} else
		return (nomor);	
}

/* Dua fungsi dibawah, untuk tab-tab seperti di Viewer dan di Console */
function setupPanes(containerId, defaultTabId) {
  panes[containerId] = new Array();
  var maxHeight = 0; var maxWidth = 0;
  var container = document.getElementById(containerId);
  if(container){
  	var paneContainer = container.getElementsByTagName("div")[0];
	  var paneList = paneContainer.childNodes;
	  for (var i=0; i < paneList.length; i++ ) {
	    var pane = paneList[i];
	    if (pane.nodeType != 1) continue;
	    if (pane.offsetHeight > maxHeight) maxHeight = pane.offsetHeight;
	    if (pane.offsetWidth  > maxWidth ) maxWidth  = pane.offsetWidth;
	    panes[containerId][pane.id] = pane;
	    pane.style.display = "none";
	  }
	    paneContainer.style.height = maxHeight + "px";
	    paneContainer.style.width  = maxWidth + "px";
	    document.getElementById(defaultTabId).onclick();
	}	    
}

function showPane(paneId, activeTab) {
    for (var con in panes) {
    activeTab.blur();
    activeTab.className = "tab-active";
    if (panes[con][paneId] != null) { // tab and pane are members of this container
      var pane = document.getElementById(paneId);
      pane.style.display = "block";
      var container = document.getElementById(con);
      var tabs = container.getElementsByTagName("ul")[0];
      var tabList = tabs.getElementsByTagName("a")
      for (var i=0; i<tabList.length; i++ ) {
        var tab = tabList[i];
        if (tab != activeTab) tab.className = "tab-disabled";
      }

      for (var i in panes[con]) {
		if(i.substr(0,4)=='pane'){
			var pane = panes[con][i];
	        if (pane == undefined) continue;
	        if (pane.id == paneId) continue;
	        pane.style.display = "none"
		 }  
      }
    }
  }
  return false;    
}	

/*
Fungsi untuk membuat date input pada textbox biasa (digabung dengan fungsi inputDate dibawah)
 */
function checkDate(nama, elm, evt, onchg){
	
	keyCode = evt.keyCode;
	awal = getSelectionStart(elm);
	akhir = getSelectionEnd(elm);
	nilai = elm.value;
	var result = false;
	is_date_field = true;
	
	//kalau tekan enter atau tab
	if( keyCode==13 || keyCode==9 ){
		result = true;
		
	//kalau cursor pilihan > 1
	}else if(awal != akhir){
		var a=0;
		if(awal>=6) a=6; else if(awal>=3) a=3;
		setSelectionRange(elm, a, a);
	
	//kalau panah kiri atau kanan
	}else if( keyCode==37 || keyCode==39 ){
		result = true;

	//kalau tombol backspace
	}else if(keyCode == 8){
		if( awal==3 || awal==6 ) {
			evt.keyCode = 37;
			result = true;	
		}else if(awal==0){

		}else{
			elm.value = nilai.substring(0,awal-1) + '0' + nilai.substring(awal);
			var kurang = 1;
			if( awal==4 || awal==7 ) kurang = 2;
			setSelectionRange(elm, awal-kurang, awal-kurang);
		}

	//kalau cursor sebelum tanda slash ( / )
	}else if( awal==2 || awal==5 ) {
		setSelectionRange(elm, awal+1, awal+1);

	//kalau cursor ada di akhir textbox
	}else if(awal == 10) {
		
	//kalau di-click
	}else if( keyCode == 0 || !keyCode ){
		if( awal==2 || awal==5 ) setSelectionRange(elm, awal+1, awal+1);

	//kalau yang diinput adalah angka
	}else if( (keyCode>=48 && keyCode<=57) || (keyCode>=96 && keyCode<=105) ){
		if(keyCode>=96) keyCode -= 48;
		keyChar = String.fromCharCode(keyCode);
		elm.value = nilai.substring(0,awal) + keyChar + nilai.substring(awal+1);
		var tambah = 1;
		if( awal==1 || awal==4 ) tambah = 2; 
		setSelectionRange(elm, awal+tambah, awal+tambah);
	}


	if(elm.value == '__/__/____' || elm.value == '00/00/0000') document.getElementById(nama).value = '';
	else document.getElementById(nama).value = elm.value;
	
	if(elm.value.indexOf("_") == -1) eval(unescape(onchg));
	else document.getElementById(nama).value = '';
	
	return result;

}

/* Fungsi untuk mengganti disable/enable inputDate */
function enableDisableDate(nama){
	document.getElementById('_' + nama).disabled = !document.getElementById('_' + nama).disabled;
	document.getElementById('img_' + nama).disabled = !document.getElementById('img_' + nama).disabled;
}

function inputDate(nama, nilai, aktif, onchg, tabidx){
	var nilai2 = '';
	if(!nilai || nilai=='') nilai = '__/__/____';
	else nilai2 = nilai;
	
	if(!onchg) onchg='';
	
	var disabled = '';
		disabled2= '';
	if(aktif){
	 disabled = ' readOnly ';
	 disabled2= ' disabled ';
	}
	
	if(!tabidx) tabidx=''; else tabidx = 'tabindex="' +tabidx+ '"';
	
	/*tambahan untuk path subdomain [canpri]*/
	var servername = document.location.hostname;
	var pathname = '/E-Lions';
	if(servername=='elions.sinarmasmsiglife.co.id')pathname = '';

	document.write(
		'<input '+disabled2+'type="text" '+tabidx+' name="_'+nama+'" id="_'+nama+'" value="'+nilai+'" maxlength="10" size="12" style="text-align: center;"  '+
		'onmouseup="return checkDate(\''+nama+'\', this, event, \''+escape(onchg)+'\')" onkeydown="return checkDate(\''+nama+'\', this, event, \''+escape(onchg)+'\')" >\n' +
		'<input '+disabled+'type="hidden" name="'+nama+'" id="'+nama+'" value="'+nilai2+'">\n'+
		'<img '+disabled2+'src="'+pathname+'/include/image/calendar.jpg" align="baseline" title="Calendar" border="0" id="img_'+nama+'">'
		);

	function chg(cal){

        var date = cal.date;
        //var time = date.getTime()
        //var date2 = new Date(time);
        //field.value = date2.print("%Y-%m-%d %H:%M");


		document.getElementById(nama).value = date.print("%d/%m/%Y");
		eval(unescape(onchg));
	}
	
    Calendar.setup({
        inputField     :    "_"+nama,
        ifFormat       :    "%d/%m/%Y",
        button         :    "img_"+nama,
        align          :    "Tl",
        onUpdate : chg
    });
}

/*
Beberapa Fungsi yang berhubungan dengan posisi cursor (caret posision) dalam sebuah textbox
 */
function setSelectionRange(input, start, end) {
	if (is_gecko) {
		input.setSelectionRange(start, end);
	} else {
		// assumed IE
		var range = input.createTextRange();
		range.collapse(true);
		range.moveStart("character", start);
		range.moveEnd("character", end - start);
		range.select();
	}
};

function getSelectionStart(input) {
	if (is_gecko)
		return input.selectionStart;
	var range = document.selection.createRange();
	var isCollapsed = range.compareEndPoints("StartToEnd", range) == 0;
	if (!isCollapsed)
		range.collapse(true);
	var b = range.getBookmark();
	return b.charCodeAt(2) - 2;
};

function getSelectionEnd(input) {
	if (is_gecko)
		return input.selectionEnd;
	var range = document.selection.createRange();
	var isCollapsed = range.compareEndPoints("StartToEnd", range) == 0;
	if (!isCollapsed)
		range.collapse(false);
	var b = range.getBookmark();
	return b.charCodeAt(2) - 2;
};

/*
Fungsi tambahan untuk melengkapi formatDate dibawah
*/
function addZero(vNumber){ 
	return ((vNumber < 10) ? "0" : "") + vNumber 
} 
    
/*
Fungsi untuk format date mengikuti fungsi dateFormat di java
*/
function formatDate(vDate, vFormat){ 
	var vDay                      = addZero(vDate.getDate()); 
	var vMonth            = addZero(vDate.getMonth()+1); 
	var vYearLong         = addZero(vDate.getFullYear()); 
	var vYearShort        = addZero(vDate.getFullYear().toString().substring(3,4)); 
	var vYear             = (vFormat.indexOf("yyyy")>-1?vYearLong:vYearShort) 
	var vHour             = addZero(vDate.getHours()); 
	var vMinute           = addZero(vDate.getMinutes()); 
	var vSecond           = addZero(vDate.getSeconds()); 
	var vDateString       = vFormat.replace(/dd/g, vDay).replace(/MM/g, vMonth).replace(/y{1,4}/g, vYear) 
	vDateString           = vDateString.replace(/hh/g, vHour).replace(/mm/g, vMinute).replace(/ss/g, vSecond) 
	return vDateString 
} 

/*
Fungsi untuk checkAll checkbox dgn nama yg sama
*/
function checkAll(nama){
	var elemen = 	document.getElementsByName(nama);
	if(elemen) {
		for(i=0; i<elemen.length; i++){
			elemen[i].checked = !elemen[i].checked;
		}
	}
}

/*
Fungsi untuk hide Loading message seperti GMAIL
*/
function hideLoadingMessage(){
	if(parent)
		if(parent.document){
			var dZone = parent.document.getElementById('dZone');
			if(dZone) dZone.style.visibility = 'hidden';
			window.scrollbars = false;
		}
}

/*
Fungsi untuk show Loading message seperti GMAIL
*/
function createLoadingMessage(){
	var dZone = $('dZone');
	if(!dZone){
		dZone = document.createElement('div');
		dZone.setAttribute('id', 'dZone');
		dZone.style.position = "absolute";
		dZone.style.zIndex = "1000";
		dZone.style.left = "0px";
		dZone.style.top = "0px";
		dZone.style.width = "100%";
		dZone.style.height = "100%";
		document.body.appendChild(dZone);
		var mZone = document.createElement('div');
		mZone.setAttribute('id', 'mZone');
		mZone.style.position = "absolute";
		mZone.style.top = "0px";
		mZone.style.right = "0px";
		mZone.style.background = "red";
		mZone.style.color = "white";
		mZone.style.fontFamily = "Arial,Helvetica,sans-serif";
		mZone.style.padding = "4px";
		dZone.appendChild(mZone);
		var text = document.createTextNode('Loading');
		mZone.appendChild(text);
	}else{
		$('mZone').innerHTML = 'Loading';
		dZone.style.visibility = 'visible';
	}
}

/* cetak halaman yang ada dalam sebuah iframe */
function printIFrame(oTgt){
	oTgt.focus();
	oTgt.print();
}

/* limit teks dalam textarea */
function textCounter(field, maxlimit) {
    if (field.value.length > maxlimit)
    field.value = field.value.substring(0, maxlimit);
}

/* trim string */
function trim(sInString) {
  sInString = sInString.replace( /^\s+/g, "" );// strip leading
  return sInString.replace( /\s+$/g, "" );// strip trailing
}

/* trim semua space dalam string */
function trimWhitespace(sInString) {
  return sInString.replace(/\s+/g, ''); //strip semua spasi
}

/* set focus pada 1 elemen */
function setFocus(elementName){
	var tmp = document.getElementsByName(elementName)[0];
	if(tmp){
		if(!tmp.disabled){
			tmp.focus();
			if(tmp.type=='text') tmp.select();
		}
	}
}

/* redirect dalam x detik */
function timeOut(self, link, timeout){
	if(self.opener){
		if(self.opener.opener){
			self.opener.opener.setTimeout("top.location = '" +link+ "'",timeout);	
			self.opener.setTimeout("window.close();",timeout);
			self.setTimeout("window.close();",timeout);
		}else{
			self.opener.setTimeout("top.location = '" +link+ "'",timeout);
			self.setTimeout("window.close();",timeout);
		}
	}else{
	    self.setTimeout("top.location = '" +link+ "'",timeout);
	}
}

/* Fungsi menambahkan option pada select */
function addOptionToSelect(dokumen, elemen, teks, nilai){
	var anOption = dokumen.createElement("OPTION");
	elemen.options.add(anOption);
	anOption.text = teks;
	anOption.value = nilai;
	anOption.selected=true;
}

/* Fungsi menghapus option pada select */
function removeOptionFromSelect(id){
	var combo = document.getElementById(id);
	combo.options[combo.selectedIndex]=null;
}

/* Fungsi untuk menyembunyikan menu */
function showHideMenu(nama){
	if(document.getElementById(nama)){
		tmp = document.getElementById(nama).style.display;
		if(tmp == 'none'){
			document.getElementById(nama).style.display = 'block';
		}else{
			document.getElementById(nama).style.display = 'none';
		}
	}
}

/* Fungsi untuk mengganti text dari tombol */
function changeCaption(tombol, t1, t2){
	if(tombol.value==t1)tombol.value=t2;
	else if(tombol.value==t2)tombol.value=t1;
}

/* Fungsi untuk enable/disable semua elemen dalam satu form */
function enableDisableAll(f, flag){
	for(i=0; i<f.elements.length; i++){
		if(f.elements[i].type!='button' && f.elements[i].type!='submit' && f.elements[i].type!='reset')
		f.elements[i].disabled=flag;
	}
}

/* Fungsi untuk mengganti disable/enable */
function enableDisable(param){
	if(param)param.disabled=!param.disabled;
}

/* Fungsi pada halaman utama (untuk resize sesuai resolusi layar user) */
function setFrameSize(frame, minus){
	if(document.getElementById(frame)){
		document.getElementById(frame).height= (document.body.offsetHeight-minus)+'px';
	}
}

/* Fungsi untuk confirm logout */
function log_out(ling){
	if (confirm('Apakah anda yakin ingin LOGOUT?')){
		needConfirm=false;
		window.location = ling;
	}else{
		return false;
	}
}

/* Popup Window, persis ditengah layar */
function popWin(href, height, width,scrollbar,stat, resize) {
	var vWin;
	if(scrollbar!='no')scrollbar='yes';
	if(stat!='yes')stat='no';
	if(resize!='no')resize='yes';

	vWin = window.open(href,'','height='+height+',width='+width+
		',toolbar=no,directories=no,status=no,menubar=no,scrollbars='+scrollbar+',resizable='+resize+',modal=yes,status='+stat+
		',left='+((screen.availWidth-width)/2)+
		',top='+((screen.availHeight-height)/2));
	vWin.opener = self;
} 

function popWinToolbar(href, height, width,scrollbar,stat) {
	var vWin;
	if(scrollbar!='no')scrollbar='yes';
	if(stat!='yes')stat='no';

	vWin = window.open(href,'','height='+height+',width='+width+
		',toolbar=yes,directories=no,status=no,menubar=no,scrollbars='+scrollbar+',resizable=no,modal=yes,status='+stat+
		',left='+((screen.availWidth-width)/2)+
		',top='+((screen.availHeight-height)/2));
	vWin.opener = self;
} 

function popWinModal(href, height, width, wind){
	if (window.showModalDialog) {
		vWin = window.showModalDialog(href,wind,
		'center:yes; dialogWidth:'+width+'px; dialogHeight:'+height+'px; help:no; resizable:yes; status:no;');
		return vWin;
	}else{
		vWin = window.open(href,'','height='+height+',width='+width+
			',toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,modal=yes,left='+((screen.availWidth-width)/2)+
			',top='+((screen.availHeight-height)/2));
		vWin.opener = self;	
	}
}

//merubah jadi huruf besar semua
function isAnyUpper(strSource){
   var intRow;
   var blnValid;

   blnValid = false;

   for(intRow = 0; intRow < strSource.length; intRow++)
   {
      if ((strSource.charAt(intRow) >= "A") && 
	      (strSource.charAt(intRow) <= "Z"))
	  {
	     blnValid = true;
		 break;
	  }
   }

   return blnValid;
}

//resize
function resizeCenter(x,y){
	if(self.opener){
		window.resizeTo(x,y);
		window.moveTo(((screen.availWidth-x)/2), ((screen.availHeight-y)/2));
	}
}

//create cookie
function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

//read cookie
function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

//erase cookie
function eraseCookie(name) {
	createCookie(name,"",-1);
}

//  Returns true if strNum  is only number

function isNumber( strNum ) {

	var intCounter=0 , intLoop;

	strNum = Trim(strNum);

	for (intLoop = 0; intLoop < strNum.length; intLoop++) { // 0-9 only
		if (((strNum.charAt(intLoop) < "0") || (strNum.charAt(intLoop) > "9")
				))
			return false;
	}

	return true; // value is 0
}

/* Format dua digit di belakang koma 
NOTE: Fungsi ini digunakan hanya untuk hasil yg nilainya positif.
	  bila hasil yg diinginkan berupa negatif, maka harus dikalikan dengan -1 diluar fungsi ini.
*/

function formatNumber(pnumber,decimals){
	if (isNaN(pnumber)) { return 0};
	if (pnumber=='') { return 0};
	
	var snum = new String(pnumber);
	var sec = snum.split('.');
	var whole = parseFloat(sec[0]);
	var result = '';
	
	if(sec.length > 1){
		var dec = new String(sec[1]);
		dec = String(parseFloat(sec[1])/Math.pow(10,(dec.length - decimals)));
		dec = String(whole + Math.round(parseFloat(dec))/Math.pow(10,decimals));
		var dot = dec.indexOf('.');
		if(dot == -1){
			dec += '.'; 
			dot = dec.indexOf('.');
		}
		while(dec.length <= dot + decimals) { dec += '0'; }
		result = dec;
	} else{
		var dot;
		var dec = new String(whole);
		dec += '.';
		dot = dec.indexOf('.');		
		while(dec.length <= dot + decimals) { dec += '0'; }
		result = dec;
	}	
	return result;
}

function formatCurrencyRupiah( num ) 
{
	var num;
	var sign;
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num))
	num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	num = Math.floor(num/100).toString();
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
	num = num.substring(0,num.length-(4*i+3))+','+
	num.substring(num.length-(4*i+3));
	return (((sign)?'':'-') + num + '.-');
}

function loadImages() {
	if (document.getElementById) {  // DOM3 = IE5, NS6
		document.getElementById('hidepage').style.visibility = 'hidden';
	} else {
		if (document.layers) {  // Netscape 4
			document.hidepage.visibility = 'hidden';
		} else {  // IE 4
			document.all.hidepage.style.visibility = 'hidden';
		}   
	}
}



