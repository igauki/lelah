/* tanggal valid */
function DateValid(strDate) {

	var arrMonthDays = new Array (	"31", "29", "31",
					"30", "31", "30",
					"31", "31", "30",
					"31","30","31"	  );
	var intCount;
	var intDay;
	var intMonth;
	var	intYear;

	if (strDate.length != 10) return false;

    intDay = strDate.substring(0,2);
	intMonth = strDate.substring(3,5);
	intYear = strDate.substring(6,11);

	if ((!isNumber(intDay)) || (!isNumber(intMonth)) || (!isNumber(intYear))){
		return false;
	}
	if (intYear==0) {
	return false;
    }
    if ((intMonth>12) || (intMonth<=0)) {
	    return false;
	}

	if ((parseFloat(intDay) > parseFloat(arrMonthDays[intMonth-1])) || (parseFloat(intDay)<=0))  {
	    return false;
	}
	if ((intMonth==2)&&
	   (intDay==arrMonthDays[1])&&
	   (intYear%4>0) ) {
		//alert("The end of this month is 28");
	    return false;
	}

	return  true;
}

function toCurrency(num) {
	num = Math.round(num*100)/100;
	var currstring = num.toString();
	if (currstring.match(/\./)) {
		var curr = currstring.split('.');
	} else {
		var curr = [currstring, "00"];
	}
	curr[1] += "00";
	curr[2] = "";
	var returnval = "";
	var length = curr[0].length;
	
	for (var i = 0; i < 2; i++)
		curr[2] += curr[1].substr(i, 1);

	for (i = length; (i - 3) > 0; i = i - 3) {
		returnval = "." + curr[0].substr(i - 3, 3) + returnval;
	}
	returnval = curr[0].substr(0, i) + returnval + "," + curr[2];
	return(returnval);
}

//===Check to validate only 2 decimal
function is2DecCcy( strNum ) {

	var intCounter=0 , intLoop, intStartPos;

	strNum = Trim(strNum);
	if (strNum == '') return false; // Not number
	if (strNum == '.') return false; // Not number

	for (intLoop = 0; intLoop < strNum.length; intLoop++) { // One . only
		if (strNum.charAt(intLoop) == ".") intCounter++;
		if (intCounter > 1) return false;
	}


	for (intLoop = 0; intLoop < strNum.length; intLoop++) { // 0-9, . only
		if (((strNum.charAt(intLoop) < "0") || (strNum.charAt(intLoop) > "9")) &&
				(strNum.charAt(intLoop) != ".")	)
			return false;
	}

	intStartPos = strNum.indexOf("."); // four decimal only
	if (intStartPos != -1){
		if ((strNum.length - (intStartPos  + 1 )) > 2)
			return false;
	}
	return true; // value is 0
}

// eliminate the left and right spaces from the string.
function Trim(string) {
	
	var i;
	var intCount;
	
	
	//truncate the left spaces.
	// 1. get the number of spaces at left side of the string.
	// 2. get the new string without the left spaces.
	
	intCount = 0;
	for (i = 0; i < string.length; i++)
		if ((string.charAt(i)) != " ")
			break;
		else
			intCount = intCount + 1;
		
	string = string.substring(intCount, string.length);
	
		
	
	//truncate the right spaces.
	// 1. use the string that has been truncated just now and get
	//	  the number of spaces on the right side of the string.
	// 2. get the final string and return.
	
	intCount = 0;
	for (i = string.length-1; i >= 0; i--)
		if ((string.charAt(i)) != " ")
			break;
		else
			intCount = intCount + 1;
		
	
	string = string.substring(0, string.length-intCount);	
	return string;		
	
}

//  Returns true if strNum  is only number

function isNumber( strNum ) {

	var intCounter=0 , intLoop;

	strNum = Trim(strNum);

	for (intLoop = 0; intLoop < strNum.length; intLoop++) { // 0-9 only
		if (((strNum.charAt(intLoop) < "0") || (strNum.charAt(intLoop) > "9")
				))
			return false;
	}

	return true; // value is 0
}

//  Returns true if strNum  is alphabet

function isAlphabet( strNum ) {

	var intLoop, strUNum;

	strUNum= strNum.toUpperCase();
	for (intLoop = 0; intLoop < strUNum.length; intLoop++) {
		if (((strUNum.charAt(intLoop) < "A") || (strUNum.charAt(intLoop) > "Z"))
		      && (strUNum.charAt(intLoop) != " ") )
			return false;
	}
	return true;
}

function isAlphaNum( strNum ) {

	var intLoop, strUNum;

	strUNum= strNum.toUpperCase();
	for (intLoop = 0; intLoop < strUNum.length; intLoop++) {
		if ((((strUNum.charAt(intLoop) < "A") || (strUNum.charAt(intLoop) > "Z")) &&
			 ((strUNum.charAt(intLoop) < "0") || (strUNum.charAt(intLoop) > "9")))
		      && (strUNum.charAt(intLoop) != " "))
			return false;
	}
	return true;
}

//  Returns true if strNum  is a number

function isCcy( strNum ) {

	var intCounter=0 , intLoop, intStartPos;

	strNum = Trim(strNum);
	if (strNum == '') return false; // Not number
	if (strNum == '.') return false; // Not number

	for (intLoop = 0; intLoop < strNum.length; intLoop++) { // One . only
		if (strNum.charAt(intLoop) == ".") intCounter++;
		if (intCounter > 1) return false;
	}


	for (intLoop = 0; intLoop < strNum.length; intLoop++) { // 0-9, . only
		if (((strNum.charAt(intLoop) < "0") || (strNum.charAt(intLoop) > "9")) &&
				(strNum.charAt(intLoop) != ".")	)
			return false;
	}

	intStartPos = strNum.indexOf("."); // four decimal only
	if (intStartPos != -1){
		if ((strNum.length - (intStartPos  + 1 )) > 4)
			return false;
	}
	return true; // value is 0
}

function UpperCase(){
	if (event.keyCode >= 97 && event.keyCode <= 97 + 26)
		event.keyCode = event.keyCode - 32;
}

function isAnyUpper(strSource){
   var intRow;
   var blnValid;

   blnValid = false;

   for(intRow = 0; intRow < strSource.length; intRow++)
   {
      if ((strSource.charAt(intRow) >= "A") && 
	      (strSource.charAt(intRow) <= "Z"))
	  {
	     blnValid = true;
		 break;
	  }
   }

   return blnValid;
}

function isAnyLower(strSource){
   var intRow;
   var blnValid;

   blnValid = false;

   for(intRow = 0; intRow < strSource.length; intRow++)
   {
      if ((strSource.charAt(intRow) >= "a") && 
	      (strSource.charAt(intRow) <= "z"))
	  {
	     blnValid = true;
		 break;
	  }
   }

   return blnValid;
}

function isAnyNumeric(strSource){
   var intRow;
   var blnValid;

   blnValid = false;

   for(intRow = 0; intRow < strSource.length; intRow++)
   {
      if ((strSource.charAt(intRow) >= "0") && 
	      (strSource.charAt(intRow) <= "9"))
	  {
	     blnValid = true;
		 break;
	  }
   }

   return blnValid;
}

/* trim string */
function trim2(sInString) {
  sInString = sInString.replace( /^\s+/g, "" );// strip leading
  return sInString.replace( /\s+$/g, "" );// strip trailing
}

/* fungsi untuk mencari nilai dalam list xml */
function ajaxSearchXml(divName, filePath, colKey, colName, nilai){
	ajaxManager.searchXml(filePath, colKey, colName, nilai, {
	  callback:function(string) {

		DWRUtil.useLoadingMessage();
		
		divName.innerHTML = string;
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
}

/* fungsi untuk mem-populate select list secara realtime menggunakan ajax */
function ajaxSelect(selectType, selectLocation, selectName, selectValue, col1, col2, onChange){
	eval('ajaxManager.'+selectType)({
	  callback:function(list) {

		DWRUtil.useLoadingMessage();

		var text = '<select name="'+selectName+'"';
		if(onChange)if(trim2(onChange)!='') text+=' onchange="'+onChange+'" ';
		text+='>';
		
		for(i = 0; i < list.length; i++) {
			text += '<option value="'+eval('list[i].'+col1)+'"';
			if(selectValue==eval('list[i].'+col1)) text += ' selected ';
			text += '>'+eval('list[i].'+col2)+'</option>';
		}
		text += '</select>';
	
		
		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
	
}

/* fungsi untuk mem-populate select multiple list dengan ukuran lebih besar */
function ajaxSelectMultipleWithParam(p, selectType, selectLocation, selectName, col1, col2, ukuran){
	eval('ajaxManager.'+selectType)(p, {
	  callback:function(list) {

		DWRUtil.useLoadingMessage();

		var text = '<select id="'+selectName+'" name="'+selectName+'" multiple="multiple" size="'+ukuran+'">';
		
		for(i = 0; i < list.length; i++) {
			text += '<option value="'+eval('list[i].'+col1)+'"';
			text += '>'+eval('list[i].'+col2)+'</option>';
		}
		text += '</select>';
	
		
		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
	
}

/* fungsi untuk mem-populate select multiple list dengan ukuran lebih besar 2 parameter */
function ajaxSelectMultipleWithParam2(p,q, selectType, selectLocation, selectName, col1, col2, ukuran){
	eval('ajaxManager.'+selectType)(p,q, {
	  callback:function(list) {

		DWRUtil.useLoadingMessage();

		var text = '<select id="'+selectName+'" name="'+selectName+'" multiple="multiple" size="'+ukuran+'">';
		
		for(i = 0; i < list.length; i++) {
			text += '<option value="'+eval('list[i].'+col1)+'"';
			text += '>'+eval('list[i].'+col2)+'</option>';
		}
		text += '</select>';
	
		
		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
	
}

/* fungsi untuk mem-populate select multiple list dengan ukuran lebih besar 2 parameter */
function ajaxSelectMultipleWithParam2(p,q, selectType, selectLocation, selectName, col1, col2, ukuran){
	eval('ajaxManager.'+selectType)(p,q, {
	  callback:function(list) {

		DWRUtil.useLoadingMessage();

		var text = '<select id="'+selectName+'" name="'+selectName+'" multiple="multiple" size="'+ukuran+'">';
		
		for(i = 0; i < list.length; i++) {
			text += '<option value="'+eval('list[i].'+col1)+'"';
			text += '>'+eval('list[i].'+col2)+'</option>';
		}
		text += '</select>';
	
		
		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});	
}

function ajaxSelectMultipleWithParam3(p,q,r, selectType, selectLocation, selectName, col1, col2, ukuran){
	eval('ajaxManager.'+selectType)(p,q,r, {
	  callback:function(list) {

		DWRUtil.useLoadingMessage();

		var text = '<select id="'+selectName+'" name="'+selectName+'" multiple="multiple" size="'+ukuran+'">';
		
		for(i = 0; i < list.length; i++) {
			text += '<option value="'+eval('list[i].'+col1)+'"';
			text += '>'+eval('list[i].'+col2)+'</option>';
		}
		text += '</select>';
	
		
		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
	
}

function ajaxSelectWithParam(p, selectType, selectLocation, selectName, selectValue, col1, col2, onChange){

	eval('ajaxManager.'+selectType)(p, {
	  callback:function(list) {
		DWRUtil.useLoadingMessage();

		var text = '<select name="'+selectName+'"';
		if(onChange)if(trim2(onChange)!='') text+=' onchange="'+onChange+'" ';
		text+='>';
		
		for(i = 0; i < list.length; i++) {
			text += '<option value="'+eval('list[i].'+col1)+'"';
			if(selectValue==eval('list[i].'+col1)) text += ' selected ';
			text += '>'+eval('list[i].'+col2)+'</option>';
		}
		text += '</select>';

		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
	
}

function ajaxSelectWithParamReffBiiShinta(p,q,r,selectType, selectLocation, selectName, selectValue, col1, col2, onChange){
	
	eval('ajaxManager.'+selectType)(p,q,r, {
	  callback:function(list) {
		DWRUtil.useLoadingMessage();

		var text = '<select name="'+selectName+'"';
		if(onChange)if(trim2(onChange)!='') text+=' onchange="'+onChange+'" ';
		text+='><option/>';
		
		for(i = 0; i < list.length; i++) {
			text += '<option value="'+eval('list[i].'+col1)+'"';
			if(selectValue==eval('list[i].'+col1)) text += ' selected ';
			text += '>'+eval('list[i].'+col2)+'</option>';
		}
		text += '</select>';

		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
	
}


function ajaxSelectWithParam2(stss,p, selectType, selectLocation, selectName, selectValue, col1, col2, onChange){
	eval('ajaxManager.'+selectType)(p, {
	  callback:function(list) {

		DWRUtil.useLoadingMessage();

		var text = '<select name="'+selectName+'"';
		if ((stss.toUpperCase() =="UPDATE") || (stss =="")) text+= ' disabled ';

		if(onChange)if(trim2(onChange)!='') text+=' onchange="'+onChange+'" ';
		text+='>';
		
		for(i = 0; i < list.length; i++) {
			text += '<option value="'+eval('list[i].'+col1)+'"';
			if(selectValue==eval('list[i].'+col1)) text += ' selected ';
			text += '>'+eval('list[i].'+col2)+'</option>';
		}
		text += '</select>';
	
		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
	
}

function ajaxUL(ulName, ulLocation, p){ //untuk di otorisasi menu

	ajaxManager.listEmployeesInDepartment(p,
	//eval('ajaxManager.'+ulType)(p, 
	{callback:function(list) {

		DWRUtil.useLoadingMessage();
		
		var text;
		text = '<ul id="'+ulName+'" class="ulMenu">';
		
		for(i = 0; i < list.length; i++) {
			text += '<li>';
			text += '<a href="#" onclick="ajaxSelectWithParam(\''
				+p+'\', \'listEmployeesInDepartment\', \'copyFromDiv\', \'copyFromSelect\', \'\', \'LUS_ID\', \'USERNAME\', \'\'); '
				+ 'retrieveTree(\''+list[i].LUS_ID+'\', \'Retrieving Menu for '+list[i].USERNAME+'...\'); document.formpost.username.value=\''+list[i].USERNAME+'\';'
				+ 'return false;">'+list[i].USERNAME+'</a>';
			text += '</li>';
		}
		text += '</ul>';
		
		if(document.getElementById(ulLocation)){
			document.getElementById(ulLocation).innerHTML=text;
		}
		
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
	
}

function getkurs(kurs, tgl_kurs,tglsekarang, e_kurs, e_nilai_kurs)
{
	ajaxManager.selectnilaikurs(kurs,tgl_kurs,
	{callback:function(list) {

		DWRUtil.useLoadingMessage();

		if (kurs=="01")
		{
			e_nilai_kurs.value="1";
		}else{
			if(tgl_kurs.length<10 || tglsekarang<10){
				alert('Harap masukkan tanggal RK yang benar.');
			}else if ((tgl_kurs.substr(6,4)+tgl_kurs.substr(3,2)+tgl_kurs.substr(0,2)) > (tglsekarang.substr(6,4)+tglsekarang.substr(3,2)+tglsekarang.substr(0,2)))
			{
				alert("Maaf, tetapi Tanggal RK tidak boleh lebih besar dari tanggal hari ini");
				return false;
			}else{
				if (list.length!=0)
				{
					e_nilai_kurs.value=list[0].LKH_KURS_JUAL;
				}else{
					alert("Maaf, tetapi tidak ada kurs untuk tanggal RK yang dimasukkan");
					e_kurs.value='01';
					e_nilai_kurs.value="1";
				}
			}
		}

	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});	
	
}

function pesanTTP(spaj){
	ajaxManager.pesanTandaTerimaPolis(spaj,
	{callback:function(map) {
		DWRUtil.useLoadingMessage();
		elemen = document.getElementById('prePostError');
		if(map!=null){
			document.formpost.v1.value = map.MSCO_ID;
			document.formpost.v2.value = map.REG_SPAJ;
			document.formpost.v3.value = map.MCL_FIRST;
			document.formpost.v4.value = map.LEV_COMM;
		}
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});	
}

function pesanRek(spaj){
	ajaxManager.SelectRekAgent(spaj,
	{callback:function(map) {
		DWRUtil.useLoadingMessage();
		elemen = document.getElementById('prePostError');
		if(map!=null){
			document.formpost.v1.value = map.LSLE_ID;
			document.formpost.v2.value = map.MSAG_TABUNGAN;
		}
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});	
}

function cektglkirimpolis(spaj){
	ajaxManager.select_cektglkirimpolis(spaj,
	{callback:function(flag) {
		DWRUtil.useLoadingMessage();
		
		document.getElementById('cekkirimp').value = flag;
		
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});	
}

//function testingaja(spaj){
//	ajaxManager.select_testingaja(spaj,
//		{callback:function(xxx) {
//		DWRUtil.useLoadingMessage();
//		
//		document.getElementById('testingaja').value = xxx;
//		
//		},
//		timeout:180000,
//	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
//	});	
//}

function ajaxPesan(spaj, posisi){
	ajaxManager.getPrePostErrorMessages(spaj, posisi,
	{callback:function(list) {
		DWRUtil.useLoadingMessage();
		elemen = document.getElementById('prePostError');
		panjang = list.length;
		daftarKode = '';
		if(panjang>0){
			var text = 'Status:';
			for(i = 0; i < panjang; i++) {
				text += "<br>- " + list[i].pesan;
				daftarKode += ","+list[i].kode;
			}
			if(elemen){
				elemen.innerHTML = text;
				elemen.style.display='block';
			}
		}else{
			if(elemen) elemen.style.display='none';
		}
		helperAjaxPesan(daftarKode);
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});	

}

function disable(id){
	if(document.getElementById(id)) document.getElementById(id).disabled=true;
}
function enable(id){
	if(document.getElementById(id)) document.getElementById(id).disabled=false;
}

function helperAjaxPesan(kode){
	if(kode.indexOf('03')>-1) disable('btnTransferPrintPolis'); else enable('btnTransferPrintPolis');
	if(kode.indexOf('00')>-1) disable('btnPaymentTopup'); else enable('btnPaymentTopup');
	if(kode.indexOf('07')>-1) disable('btnInvestasi'); else enable('btnInvestasi');
	if(kode.indexOf('08')>-1) {
		disable('komisi');
		disable('proses');
		disable('transfer');
		disable('premi');
		disable('deduct');
		disable('jurnal');
	}else {
		enable('komisi');
		enable('proses');
		enable('transfer');
		enable('premi');
		enable('deduct');
		enable('jurnal');
	}
}

function ajaxSelectWithParam1(p, selectType, selectLocation, selectName, selectValue, col1, col2, onChange){
	eval('ajaxManager.'+selectType)(p, {
	  callback:function(list) {

		DWRUtil.useLoadingMessage();

		var text = '<select name="'+selectName+'"';
		if(onChange)if(trim2(onChange)!='') text+=' onchange="'+onChange+'" ';
		text+='>';
		text += '<option value="0~X0"';
		text += '>NONE</option>';		
		for(i = 0; i < list.length; i++) {
			text += '<option value="'+eval('list[i].'+col1)+'"';
			if(selectValue==eval('list[i].'+col1)) text += ' selected ';
			text += '>'+eval('list[i].'+col2)+'</option>';
		}
		text += '</select>';
	
		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
	   },
	  timeout:180000,	
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
	
}

function ajaxSelectWithParam1a(p, selectType, selectLocation, selectName, selectValue, col1, col2, onChange,selectNull,tabindex){
	
	if(p.length < 6 && selectType == 'select_simas_card') {
		alert('Harap masukkan minimal 6 huruf!');
		return;
	}else{
		eval('ajaxManager.'+selectType)(p, {
			  callback:function(list) {

				DWRUtil.useLoadingMessage();

				var text = '<select name="'+selectName+'"';
				if(onChange)if(trim2(onChange)!='') text+=' onchange="'+onChange+'" ';
				if(tabindex)if(trim2(tabindex)!='') text+=' tabindex="'+tabindex+'" ';
				text+='>';
				text += '<option value=""';
				text += '>'+selectNull+'</option>';	
				for(i = 0; i < list.length; i++) {
					text += '<option value="'+eval('list[i].'+col1)+'"';
					if(selectValue==eval('list[i].'+col1)) text += ' selected ';
					text += '>'+eval('list[i].'+col2)+'</option>';
				}
				text += '</select>';
			
				text += '<input type="button" name="btncari1" value="Cari" onclick="ajaxSelectWithParam1a(document.frmParam.carinomor.value,\'select_simas_card\',\'kartu\',\'mrc_no_kartu\',\'\', \'KARTU_ID\', \'NO_KARTU\', \'\',\'Silahkan pilih Nomor Kartu\',\'1\');">';
				
				if(document.getElementById(selectLocation)){
					document.getElementById(selectLocation).innerHTML=text;
				}
			   },
			  timeout:180000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
			});
	}	
}

function ajaxSelectWithParamMuamalat(p, selectType, selectLocation, selectName, selectValue, col1, col2, onChange,selectNull,tabindex,lsbs_id,lsdbs_number){
	var lsbs = lsbs_id; 
	var lsdbs = lsdbs_number;
	var infotambahan = 'Info Tambahan Muamalat';
	
	if((lsbs==153 && lsdbs==5) || (lsbs==170 && lsdbs==1) || (lsbs==171 && lsdbs==1)){
		var upperflagmuamalat = p.indexOf("MUAMALAT");
		var lowerflagmuamalat = p.indexOf("muamalat");
		if( !(upperflagmuamalat== -1 || lowerflagmuamalat== -1) ){
			document.frmParam.elements['account_recur.lbn_id2'].disabled=false;
			ajaxManager.cekmuamalat(
				{
					callback:function(list) 
						{
							DWRUtil.useLoadingMessage();
							var text = '<select name="'+selectName+'"';
							if(onChange)if(trim2(onChange)!='') text+=' onchange="'+onChange+'" ';
							if(tabindex)if(trim2(tabindex)!='') text+=' tabindex="'+tabindex+'" ';
							text+='>';
							text += '<option value=""';
							text += '>'+infotambahan+'</option>';		
							for(i = 0; i < list.length; i++) {
								text += '<option value="'+eval('list[i].LBN_ID2"');
								if('bankMuamalat'==eval('list[i].LBN_ID2')) text += ' selected ';
								text += '>'+eval('list[i].NAMA')+'</option>';
							}
							text += '</select>';
						
							if(document.getElementById('bankMuamalat')){
								document.getElementById('bankMuamalat').innerHTML=text;
							}
						},
					timeout:180000,
				  	errorHandler:function(message) 
				  		{ 
				  			alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); 
						}
				});	
				
			
		}else{
			eval('ajaxManager.'+selectType)(p, {
		  callback:function(list) {
	
			DWRUtil.useLoadingMessage();
	
			var text = '<select name="'+selectName+'"';
			if(onChange)if(trim2(onChange)!='') text+=' onchange="'+onChange+'" ';
			if(tabindex)if(trim2(tabindex)!='') text+=' tabindex="'+tabindex+'" ';
			text+='>';
			text += '<option value=""';
			text += '>'+selectNull+'</option>';		
			for(i = 0; i < list.length; i++) {
				text += '<option value="'+eval('list[i].'+col1)+'"';
				if(selectValue==eval('list[i].'+col1)) text += ' selected ';
				text += '>'+eval('list[i].'+col2)+'</option>';
			}
			text += '</select>';
		
			if(document.getElementById(selectLocation)){
				document.getElementById(selectLocation).innerHTML=text;
			}
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
		}
	}else{
		eval('ajaxManager.'+selectType)(p, {
		  callback:function(list) {
	
			DWRUtil.useLoadingMessage();
	
			var text = '<select name="'+selectName+'"';
			if(onChange)if(trim2(onChange)!='') text+=' onchange="'+onChange+'" ';
			if(tabindex)if(trim2(tabindex)!='') text+=' tabindex="'+tabindex+'" ';
			text+='>';
			text += '<option value=""';
			text += '>'+selectNull+'</option>';		
			for(i = 0; i < list.length; i++) {
				text += '<option value="'+eval('list[i].'+col1)+'"';
				if(selectValue==eval('list[i].'+col1)) text += ' selected ';
				text += '>'+eval('list[i].'+col2)+'</option>';
			}
			text += '</select>';
		
			if(document.getElementById(selectLocation)){
				document.getElementById(selectLocation).innerHTML=text;
			}
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
	
}

function cek_muamalat(name){
	var upperflagmuamalat = name.contains('MUAMALAT');
	var lowerflagmuamalat = name.contains('muamalat');
	
	if( upperflagmuamalat== true || lowerflagmuamalat==true){
	
	}
	ajaxManager.cekmuamalat(name,
		{
			callback:function(map) 
				{
					DWRUtil.useLoadingMessage();
					var flag_muamalat = document.frmParam.elements['flag_muamalat'].value; 
					alert(flag_muamalat);
					
				},
			timeout:180000,
		  	errorHandler:function(message) 
		  		{ 
		  			alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); 
				}
		});	
}

function ProductConfig(hasil)
{
	if(hasil != '191~X1'){
		deleteallrider('tablerider', '1');
	}
	ajaxSelectWithParam1(hasil,'select_detilprodukutama', 'sideMenu', 'datausulan.plan', 'cmd.datausulan.plan', 'plan', 'lsdbs_name', 'perproduk(this.options[this.selectedIndex].value);');
	eval(" document.frmParam.elements['datausulan.mspr_ins_period'].value ='';");			
	eval(" document.frmParam.elements['datausulan.mspo_pay_period'].value ='';");
	eval(" document.frmParam.elements['datausulan.mspo_installment'].value ='';");
	//alert(hasil);
	document.frmParam.elements['datausulan.kodeproduk'].value = hasil;
}

function ProductConfigBankSinarmas(hasil)
{
	//deleteallrider('tablerider', '1');
	ajaxSelectWithParam1(hasil,'select_detilprodukutama_banksinarmas', 'sideMenu', 'datausulan.plan', 'cmd.datausulan.plan', 'plan', 'lsdbs_name', 'perproduk(this.options[this.selectedIndex].value);');
	eval(" document.frmParam.elements['datausulan.mspr_ins_period'].value ='';");			
	eval(" document.frmParam.elements['datausulan.mspo_pay_period'].value ='';");
	eval(" document.frmParam.elements['datausulan.mspo_installment'].value ='';");
	//alert(hasil);
	document.frmParam.elements['datausulan.kodeproduk'].value = hasil;
}

function ProductConfigBsim(hasil)
{
	//deleteallrider('tablerider', '1');
	ajaxSelectWithParam1(hasil,'select_detilprodukutama_bsim', 'sideMenu', 'datausulan.plan', 'cmd.datausulan.plan', 'plan', 'lsdbs_name', 'perproduk(this.options[this.selectedIndex].value);');
	eval(" document.frmParam.elements['datausulan.mspr_ins_period'].value ='';");			
	eval(" document.frmParam.elements['datausulan.mspo_pay_period'].value ='';");
	eval(" document.frmParam.elements['datausulan.mspo_installment'].value ='';");
	//alert(hasil);
	document.frmParam.elements['datausulan.kodeproduk'].value = hasil;
}

function ProductConfigAdminMall(hasil)
{
	//deleteallrider('tablerider', '1');
	ajaxSelectWithParam1(hasil,'select_detilprodukutamaadminmall', 'sideMenu', 'datausulan.plan', 'cmd.datausulan.plan', 'plan', 'lsdbs_name', 'perproduk(this.options[this.selectedIndex].value);');
	eval(" document.frmParam.elements['datausulan.mspr_ins_period'].value ='';");			
	eval(" document.frmParam.elements['datausulan.mspo_pay_period'].value ='';");
	eval(" document.frmParam.elements['datausulan.mspo_installment'].value ='';");
	//alert(hasil);
	document.frmParam.elements['datausulan.kodeproduk'].value = hasil;
}

function ProductConfigMall(hasil)
{
	//deleteallrider('tablerider', '1');
	ajaxSelectWithParam1(hasil,'select_detilprodukutamamall', 'sideMenu', 'datausulan.plan', 'cmd.datausulan.plan', 'plan', 'lsdbs_name', 'perproduk(this.options[this.selectedIndex].value);');
	eval(" document.frmParam.elements['datausulan.mspr_ins_period'].value ='';");			
	eval(" document.frmParam.elements['datausulan.mspo_pay_period'].value ='';");
	eval(" document.frmParam.elements['datausulan.mspo_installment'].value ='';");
	//alert(hasil);
	document.frmParam.elements['datausulan.kodeproduk'].value = hasil;
}

function ProductConfigSekuritas(hasil,hasil1)
{
	deleteallrider('tablerider', '1');
	document.frmParam.elements['datausulan.kodeproduk'].value = hasil;
	ajaxSelectWithParam1(hasil,'select_detilprodukutama_sekuritas', 'sideMenu', 'datausulan.plan', hasil1, 'plan', 'lsdbs_name', 'perproduk(this.options[this.selectedIndex].value);');
	eval(" document.frmParam.elements['datausulan.mspr_ins_period'].value ='';");			
	eval(" document.frmParam.elements['datausulan.mspo_pay_period'].value ='';");
	eval(" document.frmParam.elements['datausulan.mspo_installment'].value ='';");
	document.frmParam.elements['datausulan.kodeproduk'].value = hasil;
}

function ProductConfigCfl(hasil)
{
	deleteallrider('tablerider', '1');
	document.frmParam.elements['datausulan.kodeproduk'].value = hasil;
	ajaxSelectWithParam1(hasil,'select_detilprodukutamacfl', 'sideMenu', 'datausulan.plan', 'cmd.datausulan.plan', 'plan', 'lsdbs_name', 'perproduk(this.options[this.selectedIndex].value);');
	eval(" document.frmParam.elements['datausulan.mspr_ins_period'].value ='';");			
	eval(" document.frmParam.elements['datausulan.mspo_pay_period'].value ='';");
	eval(" document.frmParam.elements['datausulan.mspo_installment'].value ='';");
	document.frmParam.elements['datausulan.kodeproduk'].value = hasil;
}

function ProductConfigOnline(hasil)
{
	deleteallrider('tablerider', '1');
	document.frmParam.elements['datausulan.kodeproduk'].value = hasil;
	ajaxSelectWithParam1(hasil,'select_detilprodukutamaonline', 'sideMenu', 'datausulan.plan', 'cmd.datausulan.plan', 'plan', 'lsdbs_name', 'perproduk(this.options[this.selectedIndex].value);');
	eval(" document.frmParam.elements['datausulan.mspr_ins_period'].value ='';");			
	eval(" document.frmParam.elements['datausulan.mspo_pay_period'].value ='';");
	eval(" document.frmParam.elements['datausulan.mspo_installment'].value ='';");
	//document.frmParam.elements['datausulan.kodeproduk'].value = hasil;
}

function ProductConfigPlatinumBII(hasil)
{
	deleteallrider('tablerider', '1');
	ajaxSelectWithParam1(hasil,'select_detilprodukutama_platinumbii', 'sideMenu', 'datausulan.plan', 'cmd.datausulan.plan', 'plan', 'lsdbs_name', 'perproduk(this.options[this.selectedIndex].value);');
	eval(" document.frmParam.elements['datausulan.mspr_ins_period'].value ='';");			
	eval(" document.frmParam.elements['datausulan.mspo_pay_period'].value ='';");
	eval(" document.frmParam.elements['datausulan.mspo_installment'].value ='';");
	//alert(hasil);
	document.frmParam.elements['datausulan.kodeproduk'].value = hasil;
}

function ProductConfig1(hasil,hasil1,jn_bank)
{
	document.frmParam.elements['datausulan.kodeproduk'].value = hasil;
	if(jn_bank=='2'){
		ajaxSelectWithParam1(hasil,'select_detilprodukutama_banksinarmas', 'sideMenu', 'datausulan.plan', hasil1, 'plan', 'lsdbs_name', 'perproduk(this.options[this.selectedIndex].value);');
	}
	else if(jn_bank=='16'){
		ajaxSelectWithParam1(hasil,'select_detilprodukutama_bsim', 'sideMenu', 'datausulan.plan', hasil1, 'plan', 'lsdbs_name', 'perproduk(this.options[this.selectedIndex].value);');
	}else if(jn_bank=='3'){
		ajaxSelectWithParam1(hasil,'select_detilprodukutama_sekuritas', 'sideMenu', 'datausulan.plan', hasil1, 'plan', 'lsdbs_name', 'perproduk(this.options[this.selectedIndex].value);');
	}else if(jn_bank=='4'){
		ajaxSelectWithParam1(hasil,'select_detilprodukutamaadminmall', 'sideMenu', 'datausulan.plan', hasil1, 'plan', 'lsdbs_name', 'perproduk(this.options[this.selectedIndex].value);');
	}else{
		ajaxSelectWithParam1(hasil,'select_detilprodukutama', 'sideMenu', 'datausulan.plan', hasil1, 'plan', 'lsdbs_name', 'perproduk(this.options[this.selectedIndex].value);');
	}
}

function kosong(test)
{
	if (test== "kurs_up")
	{
		eval("document.frmParam.elements['datausulan.kurs_p'].value = document.frmParam.elements['datausulan.lku_id'].value;");
		eval("document.frmParam.elements['datausulan.mspr_tsi'].value ='';");
		eval("document.frmParam.elements['datausulan.mspr_premium'].value ='';");
	}else if (test == "kurs_premi")
		{
			eval("document.frmParam.elements['datausulan.lku_id'].value = document.frmParam.elements['datausulan.kurs_p'].value;");
			eval("document.frmParam.elements['datausulan.mspr_tsi'].value ='';");
			eval("document.frmParam.elements['datausulan.mspr_premium'].value ='';");
		}else if(test == "cara_bayar")
			{
				eval("document.frmParam.elements['datausulan.mspr_tsi'].value ='';");
				eval("document.frmParam.elements['datausulan.mspr_premium'].value ='';");
			}else if(test == "paket")
				{
					if(document.frmParam.elements['datausulan.flag_paket'].value==6 || document.frmParam.elements['datausulan.flag_paket'].value==39 || document.frmParam.elements['datausulan.flag_paket'].value==40){
						document.getElementById("spaj_pakett").style.visibility="visible";
					}else{
						document.getElementById("spaj_pakett").style.visibility="hidden";
						document.frmParam.elements['datausulan.spaj_paket'].value="";
					}
				}
}

function count_ma_kesehatan(spaj)
{
ajaxManager.CountMAKesehatan(spaj,
		{callback:function(int) 
			{
				DWRUtil.useLoadingMessage();
				if(int.size<=0){
					document.frmParam.elements['total_kesehatan'].value = int.size;
					
				}	
			},
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
}

function cek_rider_include(plan_produk)
{
ajaxManager.listrider(plan_produk,
		{callback:function(map) {
	
			DWRUtil.useLoadingMessage();
			var jml_rider = document.frmParam.elements['datausulan.jmlrider'].value;
			//alert(map.kode_rider);
			//alert(map.nomor_rider);
			
				var i=document.frmParam.elements['datausulan.jmlrider'].value ;
				var j ;
				if (map.jmlh_rider>0)
				{
					if (i >0)
					{
						for ( j =1; j <= i ; j++)
						{
							for ( var k =0; k <map.kode_rider.length ; k++)
							{
								if (document.frmParam.elements['ride.plan_rider'+j].value == map.kode_plan[k])
								{
								
									document.frmParam.elements['ride.plan_rider'+j].disabled=true;
									document.frmParam.elements['ride.plan_rider'+j].style.backgroundColor ='#D4D4D4';
									document.frmParam.elements['ride.mspr_tt'+j].disabled=true;
									document.frmParam.elements['ride.mspr_tt'+j].style.backgroundColor ='#D4D4D4';
									document.frmParam.elements['cek'+j].disabled=true;
									document.frmParam.elements['cek'+j].style.backgroundColor ='#D4D4D4';
									document.frmParam.elements['ride.mspr_unit'+j].readOnly=true;
									document.frmParam.elements['ride.mspr_unit'+j].style.backgroundColor ='#D4D4D4';
									document.frmParam.elements['ride.mspr_class'+j].readOnly=true;
									document.frmParam.elements['ride.mspr_class'+j].style.backgroundColor ='#D4D4D4';
									document.frmParam.elements['ride.flag_include'+j].value='1';
								}
							}
							//alert(document.frmParam.elements['ride.plan_rider'+k].value);	
						}
					}
				}
			
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
		

}

function pilih_batch(produk){ //tambah pilihan batch nonbatch untuk simas primelink (rider save). helpdesk [128634/128295] //Chandra
	var judul = document.getElementById('PilihBatch');
	var pilihan = document.getElementById('PilihBatch2');
	
	if (produk == '134~X10') {
		judul.style.display = "block";
		pilihan.style.display = "block";
    } else {
    	judul.style.display = "none";
    	pilihan.style.display = "none";
	 	document.getElementById('pb1').checked = true;
	 	document.getElementById('pb2').checked = false;
    }
}

function showCampaign(value){ //kerjain helpdesk [137730] tambahin pilihan campaign
	var camplist = document.getElementById('camplist');
	
	if(value === "1"){
		camplist.style.display = "block";
	} else {
		camplist.style.display = "none";
	}
}

function perproduk1(hasil1)
{
	document.frmParam.kode_sementara.value =hasil1;
	var tanggal = document.frmParam.elements['datausulan.mste_beg_date'].value;
	var tanggal_pp = document.frmParam.tanggal_lahir_pp.value;
	tahun_s = tanggal_pp.substring(0,4);
	bulan_s = tanggal_pp.substring(4,6);
	tanggal_s = tanggal_pp.substring(6,8);
	tanggal_pp  = tanggal_s+"/"+bulan_s+"/"+tahun_s;
	var tanggal_ttg = document.frmParam.tanggal_lahir_ttg.value;
	tahun_s = tanggal_ttg.substring(0,4);
	bulan_s = tanggal_ttg.substring(4,6);
	tanggal_s = tanggal_ttg.substring(6,8);
	tanggal_ttg = tanggal_s+"/"+bulan_s+"/"+tahun_s;
	if (hasil1!="0~X0"){
		ajaxManager.listcara_bayar(hasil1,tanggal,tanggal_pp,tanggal_ttg,document.frmParam.elements['datausulan.mspr_ins_period'].value,
		{callback:function(map) {
	
			DWRUtil.useLoadingMessage();
				var text = '<select name=\"datausulan.lku_id\" onchange=\"kosong(\'kurs_up\');\" >';
				for(i = 0; i < map.nama_kurs.length; i++) {
					text += '<option value="'+map.kurs[i]+'"';
					if(document.frmParam.kurss.value==map.kurs[i]) text += ' selected ';
					text += '>'+map.nama_kurs[i]+'</option>';
				}
				text += '</select>';
				
				if(document.getElementById('kursup')){
					document.getElementById('kursup').innerHTML=text;
				}
				
				var text1 = '<select name=\"datausulan.kurs_p\" onchange=\"kosong(\'kurs_premi\');\" >';
				for(i = 0; i < map.nama_kurs.length; i++) {
					text1 += '<option value="'+map.kurs[i]+'"';
					if(document.frmParam.kurss.value==map.kurs[i]) text1 += ' selected ';
					text1 += '>'+map.nama_kurs[i]+'</option>';
				}
				text1 += '</select>';
	
				if(document.getElementById('kurspremi')){
					document.getElementById('kurspremi').innerHTML=text1;
				}	
				
				var text2 = '<select name=\"datausulan.lscb_id\" onchange=\"kosong(\'cara_bayar\');\" >';
				for(i = 1; i < map.cr_byr.length; i++) {
					text2 += '<option value="'+map.cr_byr[i]+'"';
					if(document.frmParam.cb.value==map.cr_byr[i]) text2 += ' selected ';
					text2 += '>'+map.nama_crbyr[i]+'</option>';
				}
				text2 += '</select>';

				if(document.getElementById('cara_bayar')){
					document.getElementById('cara_bayar').innerHTML=text2;
				}	
				
				if(hasil1 != '191~X1'){
					var text3 = '<select name=\"datausulan.flag_paket\" onchange=\"kosong(\'paket\');\" >';
					for(i = 0; i < map.kd_packet.length; i++) {
						text3 += '<option value="'+map.kd_packet[i]+'"';
						if(document.frmParam.pakett.value==map.kd_packet[i]) text3 += ' selected ';
						text3 += '>'+map.nama_packet[i]+'</option>';
					}
					text3 += '</select>';
					
					if(document.getElementById('paketan')){
						document.getElementById('paketan').innerHTML=text3;
					}	
				}
				
				pilih_batch(hasil1);
				
				//var tgl_end_date = map.tahun_end+map.bulan_end+map.tanggal_end;	
				document.frmParam.elements['datausulan.mspr_ins_period'].value =map.lama_tanggung;			
				document.frmParam.elements['datausulan.mspo_pay_period'].value =map.lama_bayar;
				
				//helpdesk [138638] produk baru SLP Syariah (223-2)
				setSLPSyariah(hasil1, map.umurttg);
				
				if (map.flag_platinum=="1")
				{
					document.frmParam.elements['datausulan.mspo_installment'].readOnly = false;
					document.frmParam.elements['datausulan.mspo_installment'].style.backgroundColor ='#FFFFFF';
					if (document.frmParam.elements['datausulan.mspo_installment'].value =="")
					{
						document.frmParam.elements['datausulan.mspo_installment'].value=map.lama_bayar;
					}
				}else{
					if(map.flag_cuti_premi== "1")
					{
						document.frmParam.elements['datausulan.mspo_installment'].readOnly = false;
						document.frmParam.elements['datausulan.mspo_installment'].style.backgroundColor ='#FFFFFF';
						/*if (document.frmParam.elements['datausulan.mspo_installment'].value =="")
						{
							document.frmParam.elements['datausulan.mspo_installment'].value=map.lama_bayar;
						}*/
					}else{
					
						document.frmParam.elements['datausulan.mspo_installment'].value ="";
						document.frmParam.elements['datausulan.mspo_installment'].readOnly = true;
						document.frmParam.elements['datausulan.mspo_installment'].style.backgroundColor ='#D4D4D4';
					}
				}
				document.frmParam.ins.value = map.lama_tanggung;
				document.frmParam.pay.value =map.lama_bayar;

				if (map.flag_class ==1)
				{
					document.frmParam.elements['datausulan.mspr_class'].readOnly=false;
					document.frmParam.elements['datausulan.mspr_class'].style.backgroundColor ='#FFFFFF';
				}else{
					document.frmParam.elements['datausulan.mspr_class'].readOnly=true;
					document.frmParam.elements['datausulan.mspr_class'].style.backgroundColor ='#D4D4D4';
				}

				if (hasil1=="0~X0")
				{
					document.frmParam.elements['datausulan.mste_end_date'].value="";
					document.frmParam.elements['_datausulan.mste_end_date'].value="";
				}else{
					listtglpolis(document.frmParam.elements['datausulan.mste_beg_date'].value, document.frmParam.elements['datausulan.mspr_ins_period'].value,hasil1)
				}
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}

}


function perproduk(hasil1)
{
	if(hasil1 != '191~X1'){
		deleteallrider('tablerider', '1');
	}
	var tanggal = document.frmParam.elements['datausulan.mste_beg_date'].value;
	var tanggal_pp = document.frmParam.tanggal_lahir_pp.value;
	tahun_s = tanggal_pp.substring(0,4);
	bulan_s = tanggal_pp.substring(4,6);
	tanggal_s = tanggal_pp.substring(6,8);
	tanggal_pp  = tanggal_s+"/"+bulan_s+"/"+tahun_s;
	var tanggal_ttg = document.frmParam.tanggal_lahir_ttg.value;
	tahun_s = tanggal_ttg.substring(0,4);
	bulan_s = tanggal_ttg.substring(4,6);
	tanggal_s = tanggal_ttg.substring(6,8);
	tanggal_ttg = tanggal_s+"/"+bulan_s+"/"+tahun_s;

	var lsbs;
	if(document.frmParam.elements['datausulan.lsbs_id']){
		lsbs = document.frmParam.elements['datausulan.lsbs_id'].value;
		if(lsbs != ''){
			ajaxManager.flagWarningAutodebet(lsbs,
				{callback:function(hasil) {
			
					DWRUtil.useLoadingMessage();
					document.frmParam.elements['flag_warning_autodebet'].value = hasil;
					
				   },
				  timeout:180000,
				  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
				});
		}
	}
	document.frmParam.elements['datausulan.mspr_ins_period'].value ="";			
 	document.frmParam.elements['datausulan.mspo_pay_period'].value="";
 	if (hasil1!="0~X0"){	
 	ajaxManager.listcara_bayar(hasil1,tanggal,tanggal_pp,tanggal_ttg,document.frmParam.elements['datausulan.mspr_ins_period'].value,
		{callback:function(map) {
			DWRUtil.useLoadingMessage();
				var text = '<select name=\"datausulan.lku_id\" onchange=\"kosong(\'kurs_up\');\" >';
				for(i = 0; i < map.nama_kurs.length; i++) {
					text += '<option value="'+map.kurs[i]+'"';
					if(document.frmParam.kurss.value==map.kurs[i]) text += ' selected ';
					text += '>'+map.nama_kurs[i]+'</option>';
				}
				text += '</select>';
				
				if(document.getElementById('kursup')){
					document.getElementById('kursup').innerHTML=text;
				}
				
				var text1 = '<select name=\"datausulan.kurs_p\" onchange=\"kosong(\'kurs_premi\');\" >';
				for(i = 0; i < map.nama_kurs.length; i++) {
					text1 += '<option value="'+map.kurs[i]+'"';
					if(document.frmParam.kurss.value==map.kurs[i]) text1 += ' selected ';
					text1 += '>'+map.nama_kurs[i]+'</option>';
				}
				text1 += '</select>';
	
				if(document.getElementById('kurspremi')){
					document.getElementById('kurspremi').innerHTML=text1;
				}	
				
				var text2 = '<select name=\"datausulan.lscb_id\" onchange=\"kosong(\'cara_bayar\');\" >';
				for(i = 1; i < map.cr_byr.length; i++) {
					text2 += '<option value="'+map.cr_byr[i]+'"';
					if(document.frmParam.cb.value==map.cr_byr[i]) text2 += ' selected ';
					text2 += '>'+map.nama_crbyr[i]+'</option>';
				}
				text2 += '</select>';

				if(document.getElementById('cara_bayar')){
					document.getElementById('cara_bayar').innerHTML=text2;
				}		
				
				if(hasil1 != '191~X1'){
					var text3 = '<select name=\"datausulan.flag_paket\" onchange=\"kosong(\'paket\');\" >';
					for(i = 0; i < map.kd_packet.length; i++) {
						text3 += '<option value="'+map.kd_packet[i]+'"';
						if(document.frmParam.pakett.value==map.kd_packet[i]) text3 += ' selected ';
						text3 += '>'+map.nama_packet[i]+'</option>';
					}
					text3 += '</select>';
					
					if(document.getElementById('paketan')){
						document.getElementById('paketan').innerHTML=text3;
					}
				}
				
				pilih_batch(hasil1);
								
				//var tgl_end_date = map.tahun_end+map.bulan_end+map.tanggal_end;	
				document.frmParam.elements['datausulan.mspr_ins_period'].value =map.lama_tanggung;			
				document.frmParam.elements['datausulan.mspo_pay_period'].value =map.lama_bayar;
				
				//helpdesk [138638] produk baru SLP Syariah (223-2)
				setSLPSyariah(hasil1, map.umurttg);
				
				if (map.flag_platinum=="1")
				{
					document.frmParam.elements['datausulan.mspo_installment'].readOnly = false;
					document.frmParam.elements['datausulan.mspo_installment'].style.backgroundColor ='#FFFFFF';
					if (document.frmParam.elements['datausulan.mspo_installment'].value =="")
					{
						document.frmParam.elements['datausulan.mspo_installment'].value=map.lama_bayar;
					}
				}else{
					if(map.flag_cuti_premi== "1")
					{
						document.frmParam.elements['datausulan.mspo_installment'].readOnly = false;
						document.frmParam.elements['datausulan.mspo_installment'].style.backgroundColor ='#FFFFFF';
						/*if (document.frmParam.elements['datausulan.mspo_installment'].value =="")
						{
							document.frmParam.elements['datausulan.mspo_installment'].value=map.lama_bayar;
						}*/
					}else{
					
						document.frmParam.elements['datausulan.mspo_installment'].value ="";
						document.frmParam.elements['datausulan.mspo_installment'].readOnly = true;
						document.frmParam.elements['datausulan.mspo_installment'].style.backgroundColor ='#D4D4D4';
					}
				}			
				document.frmParam.ins.value = map.lama_tanggung;
				document.frmParam.pay.value =map.lama_bayar;

				if (map.flag_class ==1)
				{
					document.frmParam.elements['datausulan.mspr_class'].readOnly=false;
					document.frmParam.elements['datausulan.mspr_class'].style.backgroundColor ='#FFFFFF';
				}else{
					document.frmParam.elements['datausulan.mspr_class'].readOnly=true;
					document.frmParam.elements['datausulan.mspr_class'].style.backgroundColor ='#D4D4D4';
				}				
			
				if (hasil1=="0~X0")
				{
					document.frmParam.elements['datausulan.mste_end_date'].value="";
					document.frmParam.elements['_datausulan.mste_end_date'].value="";
				}else{
					listtglpolis(document.frmParam.elements['datausulan.mste_beg_date'].value, document.frmParam.elements['datausulan.mspr_ins_period'].value,hasil1)
				}
				
				//yusuf - 19 nov 2008 - muamalat, mabrur, saqinah, ikhlas
				document.frmParam.elements['datausulan.mspr_tsi'].readOnly=false;
				document.frmParam.elements['datausulan.mspr_premium'].readOnly=false;
				//document.frmParam.elements['datausulan.mspr_tsi'].value = 0;
				//document.frmParam.elements['datausulan.mspr_premium'].value = 0;
				if(hasil1 == '153~X5' || hasil1.substring(0,3) == '164' || hasil1.substring(0,3) == '170' || hasil1 == '171~X1' || hasil1 == '171~X2' || hasil1.substring(0,3) == '177' || hasil1.substring(0,3) == '212' || hasil1.substring(0,3) == '186' || hasil1.substring(0,3) == '207'){
					if(hasil1 == '153~X5'){ //Mabrur (Ekalink 80+ Syariah)
						document.frmParam.elements['datausulan.mspr_tsi'].readOnly=true;
						document.frmParam.elements['datausulan.mspr_premium'].readOnly=true;
						document.frmParam.elements['datausulan.mspr_tsi'].value = 40000000;
						document.frmParam.elements['datausulan.mspr_premium'].value = 410000;
						document.frmParam.elements['datausulan.mste_flag_cc'].value = 5; //cara : DEBET MUAMALAT
					}else if(hasil1.substring(0,3) == '164'){
						document.frmParam.elements['datausulan.cara_premi'].value = 1;
						document.frmParam.elements['premi_persentase'].checked = true;
						document.frmParam.elements['datausulan.kombinasi'].disabled = false;
						document.frmParam.elements['datausulan.total_premi_kombinasi'].readOnly = false;		
						document.frmParam.elements['datausulan.total_premi_kombinasi'].style.backgroundColor ='#FFFFFF';
						document.frmParam.elements['datausulan.mspr_premium'].readOnly = true;		
						document.frmParam.elements['datausulan.mspr_premium'].value = '';
						document.frmParam.elements['datausulan.mspr_premium'].style.backgroundColor ='#D4D4D4';
					}else if(hasil1.substring(0,3) == '170'){ //IKHLAS (PA TERM SYARIAH)
						document.frmParam.elements['datausulan.mspr_tsi'].readOnly=true;
						document.frmParam.elements['datausulan.mspr_premium'].readOnly=true;
						document.frmParam.elements['datausulan.mspr_tsi'].value = 15000000;
						document.frmParam.elements['datausulan.mspr_premium'].value = 150000;
						document.frmParam.elements['datausulan.mste_flag_cc'].value = 5; //cara : DEBET MUAMALAT
					}else if(hasil1 == '171~X1'){ //SAQINAH (HCP STAND ALONE)
						document.frmParam.elements['datausulan.mspr_tsi'].readOnly=true;
						document.frmParam.elements['datausulan.mspr_premium'].readOnly=true;
						document.frmParam.elements['datausulan.mspr_tsi'].value = 25000000;
						document.frmParam.elements['datausulan.mspr_premium'].value = 200000;
						document.frmParam.elements['datausulan.mste_flag_cc'].value = 5; //cara : DEBET MUAMALAT
					}else if(hasil1 == '171~X2'){ //SAQINAH (HCP STAND ALONE)
						document.frmParam.elements['datausulan.mspr_tsi'].readOnly=true;
						document.frmParam.elements['datausulan.mspr_premium'].readOnly=true;
						document.frmParam.elements['datausulan.mspr_tsi'].value = 500000;
						document.frmParam.elements['datausulan.mspr_premium'].value = 1080000;
						document.frmParam.elements['datausulan.mste_flag_cc'].value = 0; //cara : tunai
					}else if(hasil1.substring(0,3) == '177' || hasil1.substring(0,3) == '207' ||  hasil1.substring(0,3) == '212' || hasil1.substring(0,3) == '186'){
						document.frmParam.elements['datausulan.mspr_ins_period'].readOnly=false;
						document.frmParam.elements['datausulan.mspr_ins_period'].style.backgroundColor ='#FFFFFF';
						document.frmParam.elements['datausulan.mspo_pay_period'].readOnly=false;
						document.frmParam.elements['datausulan.mspo_pay_period'].style.backgroundColor ='#FFFFFF';
					}
				}else if (hasil1.substring(0,3) == '142' || hasil1.substring(0,3) == '143' || hasil1.substring(0,3) == '144'){
					document.getElementById('flagbul1').disabled = true;
					document.getElementById('flagbul2').disabled = true;
				}
				
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
		
		//helpdesk [137730] tambahin pilihan campaign
		selectMstCampaignProduct(hasil1);
 	}
}

function cekPin(pin)
{
	ajaxManager.cekPin(pin,
		{callback:function(map) {
		
			DWRUtil.useLoadingMessage();
			if(map!=null){
				document.getElementById('produk').value = map.produk;
			}
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
}

function cekKartuPas(no)
{
	ajaxManager.cekKartuPas(no,
		{callback:function(map) {
		
			DWRUtil.useLoadingMessage();
			if(map!=null){
				document.getElementById('produk').value = map.produk;
			}
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
}

function cekPremiHcp(no, tgl_lahir)
{
	ajaxManager.cekPremiHcp(no, tgl_lahir,
		{callback:function(map) {
		
			DWRUtil.useLoadingMessage();
			if(map!=null){
				document.getElementById('msp_premi').value = map.premi;
			}
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
}

function qcOkControl(spaj, lus_id, lde_id, info)
{
	ajaxManager.qcOkControl(spaj, lus_id, lde_id, info,
		{callback:function(map) {
		
			DWRUtil.useLoadingMessage();
			if(map!=null){
				alert(map.peringatan);
			}
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
}

function approved(spaj, flag_approve, lus_id, lde_id, info)
{
		alert(info);
	ajaxManager.approved(spaj, flag_approve, lus_id,lde_id, info,
		{callback:function(map) {
		
			DWRUtil.useLoadingMessage();
			if(map!=null){
				alert(map.peringatan);
			}
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
}

function listdataagen2(kodeagen, lca_id, spaj)
{
	ajaxManager.listagenpas(kodeagen,
			{callback:function(map) {
		
				DWRUtil.useLoadingMessage();
				document.getElementById('nama_agen').value = map.nama_penutup;
				document.getElementById('cabang_agen').value = map.nama_regional;
				
			   },
			  timeout:180000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
			});
}

function listdataagen(kodeagen, lca_id, spaj)
{
	ajaxManager.listagen(kodeagen,lca_id,spaj,
		{callback:function(map) {
	
			DWRUtil.useLoadingMessage();
			if (kodeagen == "000000")
			{
				document.frmParam.elements['agen.kode_regional'].value = map.kode_regional;
				document.frmParam.elements['addressbilling.region'].value = map.kode_regional;
				document.frmParam.elements['agen.lsrg_nama'].value = map.nama_regional;		
			}else{
				document.frmParam.elements['agen.mcl_first'].value = map.nama_penutup;
				document.frmParam.elements['agen.kode_regional'].value = map.kode_regional;
				document.frmParam.elements['addressbilling.region'].value = map.kode_regional;
				document.frmParam.elements['agen.lsrg_nama'].value = map.nama_regional;
			}
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
}

function listdataagenao(kodeagen)
{
	ajaxManager.listagenao(kodeagen,
		{callback:function(map) {
	
			DWRUtil.useLoadingMessage();
				document.frmParam.elements['pemegang.nama_ao'].value = map.nama_ao;
			
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
}


//
//	function namapp(x)
//	{
//		ajaxManager.listnamapp(x,
//		{callback:function(map) {
//	
//			DWRUtil.useLoadingMessage();
//				x.value = map.nama_pemegang;
//			
//		   },
//		   timeout:180000,
//	   errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
//			});
//	}

function listdataagenleader(kodeagen)
{
	ajaxManager.listagenleader(kodeagen,
		{callback:function(map) {
	
			DWRUtil.useLoadingMessage();
				document.frmParam.elements['pemegang.nama_leader'].value = map.nama_leader;
			
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
}

function perproduk2(hasil1)
{

	var tanggal = document.frmParam.elements['datausulan.mste_beg_date'].value;
	var tanggal_pp = document.frmParam.tanggal_lahir_pp.value;
	tahun_s = tanggal_pp.substring(0,4);
	bulan_s = tanggal_pp.substring(4,6);
	tanggal_s = tanggal_pp.substring(6,8);
	tanggal_pp  = tanggal_s+"/"+bulan_s+"/"+tahun_s;
	var tanggal_ttg = document.frmParam.tanggal_lahir_ttg.value;
	tahun_s = tanggal_ttg.substring(0,4);
	bulan_s = tanggal_ttg.substring(4,6);
	tanggal_s = tanggal_ttg.substring(6,8);
	tanggal_ttg = tanggal_s+"/"+bulan_s+"/"+tahun_s;
	
	
		ajaxManager.listcara_bayar(hasil1,tanggal,tanggal_pp,tanggal_ttg,document.frmParam.elements['datausulan.mspr_ins_period'].value,
		{callback:function(map) {
	
			DWRUtil.useLoadingMessage();
	
				eval(" document.frmParam.elements['datausulan.mspr_ins_period'].value =map.lama_tanggung;");			
				eval(" document.frmParam.elements['datausulan.mspo_pay_period'].value =map.lama_bayar;");
				
				if (document.frmParam.ins.value!="")
				{
				eval(" document.frmParam.elements['datausulan.mspr_ins_period'].value =document.frmParam.ins.value;");
				}
				if (document.frmParam.pay.value!="")
				{
				eval(" document.frmParam.elements['datausulan.mspo_pay_period'].value =document.frmParam.pay.value;");	
				}
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	

}

function listtglpolis(tanggal_beg, lama_bayar,kode_bisnis)
{
if (!DateValid(tanggal_beg)){
			alert('Harap masukkan tanggal awal masa pertanggungan yang benar');
			return false;
}else{	
	var tanggal_pp = document.frmParam.tanggal_lahir_pp.value;
	tahun_s = tanggal_pp.substring(0,4);
	bulan_s = tanggal_pp.substring(4,6);
	tanggal_s = tanggal_pp.substring(6,8);
	tanggal_pp  = tanggal_s+"/"+bulan_s+"/"+tahun_s;
	var tanggal_ttg = document.frmParam.tanggal_lahir_ttg.value;
	tahun_s = tanggal_ttg.substring(0,4);
	bulan_s = tanggal_ttg.substring(4,6);
	tanggal_s = tanggal_ttg.substring(6,8);
	tanggal_ttg = tanggal_s+"/"+bulan_s+"/"+tahun_s;
	ajaxManager.tanggalenddate(tanggal_beg, lama_bayar,kode_bisnis,tanggal_pp,tanggal_ttg,
		{callback:function(map) {
	
			DWRUtil.useLoadingMessage();
			document.frmParam.elements['datausulan.mste_end_date'].value = map.tgl_end_date;
			document.frmParam.elements['_datausulan.mste_end_date'].value = map.tgl_end_date;
	   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
}

function listtglpolis1()
{
	var tanggal_beg = document.frmParam.elements['datausulan.mste_beg_date'].value;
	
if (!DateValid(tanggal_beg)){
			alert('Harap masukkan tanggal awal masa pertanggungan yang benar');
			return false;
}else{	
	var lama_bayar=document.frmParam.elements['datausulan.mspr_ins_period'].value;
	var kode_bisnis = document.frmParam.elements['datausulan.plan'].value;
	if (kode_bisnis=="0~X0")
	{
		alert("Silahkan pilih produk terlebih dahulu");
	}else{
	
		var tanggal_pp = document.frmParam.tanggal_lahir_pp.value;
		tahun_s = tanggal_pp.substring(0,4);
		bulan_s = tanggal_pp.substring(4,6);
		tanggal_s = tanggal_pp.substring(6,8);
		tanggal_pp  = tanggal_s+"/"+bulan_s+"/"+tahun_s;
		var tanggal_ttg = document.frmParam.tanggal_lahir_ttg.value;
		tahun_s = tanggal_ttg.substring(0,4);
		bulan_s = tanggal_ttg.substring(4,6);
		tanggal_s = tanggal_ttg.substring(6,8);
		tanggal_ttg = tanggal_s+"/"+bulan_s+"/"+tahun_s;
		perproduk2(kode_bisnis);
		ajaxManager.tanggalenddate(tanggal_beg, lama_bayar,kode_bisnis,tanggal_pp,tanggal_ttg,
			{callback:function(map) {
		
				DWRUtil.useLoadingMessage();
				document.frmParam.elements['datausulan.mste_end_date'].value = map.tgl_end_date;
				document.frmParam.elements['_datausulan.mste_end_date'].value = map.tgl_end_date;
				
				
			},
			  timeout:180000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
			});
		}
	}
}



function agenao(kodeao)
{
	document.frmParam.kode_ao.value=kodeao;
	ajaxManager.listagenba(kodeao,
		{callback:function(map) {
	
			DWRUtil.useLoadingMessage();
			document.frmParam.elements['pemegang.nama_ao'].value=map.nama_ao;
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
}


function ajaxautocomplete(p, selectType, selectLocation, selectName, selectValue, col1, col2, onChange){
	
	
	eval('ajaxManager.'+selectType)(p, {
	  callback:function(list) {

		DWRUtil.useLoadingMessage();

		var text = '<select name="'+selectName+'">';
	
		
		for(i = 0; i < list.length; i++) {
			text += '<option value="'+eval('list[i].'+col1)+'"';
			if(selectValue==eval('list[i].'+col1)) text += ' selected ';
			text += '>'+eval('list[i].'+col2)+'</option>';
		}
		text += '</select>';
	
		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
	
}

function ajaxSelectplan(p, q,selectType, selectLocation, selectName, selectValue, col1, col2, onChange){

	eval('ajaxManager.'+selectType)(p, q,{
	  callback:function(list) {
		DWRUtil.useLoadingMessage();

		var text = '<select name="'+selectName+'"';
		if(onChange)if(trim2(onChange)!='') text+=' onchange="'+onChange+'" ';
		text+='>';
		text+= '<option value=\"0~X0\">NONE</option>';
		for(i = 0; i < list.length; i++) {
			text += '<option value="'+eval('list[i].'+col1)+'"';
			if(selectValue==eval('list[i].'+col1)) text += ' selected ';
			text += '>'+eval('list[i].'+col2)+'</option>';
		}
		text += '</select>';

		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
	
}

function ajaxQuery(p, selectType, target1, target2){
	if(trim(p)!=''){
		eval('ajaxManager.'+selectType)(p, {
		  callback:function(map) {
			DWRUtil.useLoadingMessage();
			if(map!=null){
				target1.value = map.RESULT1;
				target2.value = map.RESULT2;
			}
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
}

function ajaxSelectWilayah(p, selectType, selectLocation, selectName, selectValue, col1, col2, onChange){

	eval('ajaxManager.'+selectType)(p, {
	  callback:function(list) {
		DWRUtil.useLoadingMessage();

		var text = '<select name="'+selectName+'"';
		if(onChange)if(trim2(onChange)!='') text+=' onchange="'+onChange+'" ';
		text+='>';
//		text+= '<option value=\"LAIN - LAIN\">LAIN - LAIN</option>';
		for(i = 0; i < list.length; i++) {
			text += '<option value="'+eval('list[i].'+col1)+'"';
			if(selectValue==eval('list[i].'+col1)) text += ' selected ';
			text += '>'+eval('list[i].'+col2)+'</option>';
		}
		text += '</select>';

		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
	
}
function ajaxProduk(tipe,nama,i,selectLocation,selectType,selectName,selectValue,col1, col2,onChange,width){
	eval('ajaxManager.'+selectType)(tipe, nama, {
	  callback:function(list) {
		DWRUtil.useLoadingMessage();

		var text = '<select style="'+width+'" name="'+selectName+'"';
		if(onChange)if(trim2(onChange)!='') text+=' onchange="'+onChange+'" ';
		text+='><option/>';
		for(i = 0; i < list.length; i++) {
			text += '<option value="'+eval('list[i].'+col1)+'"';
			if(selectValue==eval('list[i].'+col1)) text += ' selected ';
			text += '>'+eval('list[i].'+col2)+'</option>';
		}
		text += '</select>';
		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
	
}

function ajaxInfoTtp(spaj,selectLocation,selectType){
	eval('ajaxManager.'+selectType)(spaj, {
	  callback:function(info) {
		DWRUtil.useLoadingMessage();
		var text = info;
		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
	
}

function ajaxInfoRek(spaj,selectLocation,selectType){
	eval('ajaxManager.'+selectType)(spaj, {
	  callback:function(info) {
		DWRUtil.useLoadingMessage();
		var text = info;
		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
	
}

function ajaxProdukAsm(spaj,selectLocation,selectType){
	eval('ajaxManager.'+selectType)(spaj, {
	  callback:function(asm) {
		DWRUtil.useLoadingMessage();
		var text = asm;
		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
	
}

function tambahTahunEndDate(begDate,endDate,flag,selectLocation,selectType){
	eval('ajaxManager.'+selectType)(begDate,endDate,flag, {
	  callback:function(output) {
		DWRUtil.useLoadingMessage();
		var text = output;
		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
		alert(text);
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
}

function mallnonESPAJ(spaj){
	eval('ajaxManager.mallnonESPAJ')(spaj, {
	  callback:function(output) {
		DWRUtil.useLoadingMessage();
		var text = output;
		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
		alert(text);
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
}


function ajaxLstAgen(kodeagen,selectLocation,selectType){
	eval('ajaxManager.'+selectType)(kodeagen, {
	  callback:function(asm) {
		DWRUtil.useLoadingMessage();
			document.formpost.elements('btpp.mcl_first').value = asm.NM_PENUTUP;
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nKode Agen tidak terdaftar. ntolong input kode agen."); }
	});
	
}
function msgKdAgen(kodeagen){
	ajaxManager.lsSelectAgent(kodeagen,
	{callback:function(map) {
		DWRUtil.useLoadingMessage();
		elemen = document.getElementById('prePostError');
		if(map!=null){
			alert(map.NM_PENUTUP);
			document.formpost.getElementById('btpp.mcl_first').value = map.NM_PENUTUP;
		}
	   },
	  timeout:180000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nKode Agent tidak ada. Input kode agen."); }
	});	
}

function updateSimultan(id_sim,cekTbl,sts,userName)
{	
	ajaxManager.prosesUpdateSimultan(id_sim,cekTbl,userName,
		{callback:function(list) {
			var plus="";
			if(sts==1){//pemegang
				for(i = 0; i < list.length; i++) {
					//alert("1 "+list[i].CEK_TBL+";old="+list[i].ID_SIMULTAN_OLD+";new="+list[i].ID_SIMULTAN_NEW);
					document.formpost.idSimPp[list[i].CEK_TBL].value=list[i].ID_SIMULTAN_NEW;
					document.getElementById("idSimPp"+(parseInt(list[i].CEK_TBL)+1)).style.color = "red";
				
				}
				plus="Pemegang";
				indicatorPp.style.display='none';
			}else if(sts==2){//tertanggung
				for(i = 0; i < list.length; i++) {
					//alert("1 "+list[i].CEK_TBL+";old="+list[i].ID_SIMULTAN_OLD+";new="+list[i].ID_SIMULTAN_NEW);
					document.formpost.idSimTt[list[i].CEK_TBL].value=list[i].ID_SIMULTAN_NEW;
					document.getElementById("idSimTt"+(parseInt(list[i].CEK_TBL)+1)).style.color = "red";
				}
				plus="Tertanggung";
				indicatorTt.style.display='none';
			}else if(sts==3){//pemegang dan tertanggung
				for(i = 0; i < list.length; i++) {
					//alert("1 "+list[i].CEK_TBL+";old="+list[i].ID_SIMULTAN_OLD+";new="+list[i].ID_SIMULTAN_NEW);
					document.formpost.idSim[list[i].CEK_TBL].value=list[i].ID_SIMULTAN_NEW;
					document.getElementById("idSim"+(parseInt(list[i].CEK_TBL)+1)).style.color = "red";
				}
				
				indicator.style.display='none';
			}	
			//background-color:black
			alert("Berhasil Update Simultan ID "+plus);
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
}


function ajaxPjngRek(bankCode, flag, element){
	if(flag==1){
		eval('ajaxManager.select_panj_rek1')(bankCode, {
		  callback:function(map) {
			DWRUtil.useLoadingMessage();
			if(map!=null){
				var pjg=map;
				for(i=pjg;i<20;i++){	
					document.frmParam.elements[element+'['+i+']'].value = "";
					document.frmParam.elements[element+'['+i+']'].readOnly = true;
					document.frmParam.elements[element+'['+i+']'].style.backgroundColor ='#D4D4D4';
				}
				for(i=0;i<pjg;i++){	
					document.frmParam.elements[element+'['+i+']'].readOnly = false;
					document.frmParam.elements[element+'['+i+']'].style.backgroundColor ='#FFFFFF';
				}
			}
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}else{
		eval('ajaxManager.select_panj_rek2')(bankCode, {
		  callback:function(map) {
			DWRUtil.useLoadingMessage();
			if(map!=null){				
				var pjg=map;
				for(i=pjg;i<20;i++){					
					document.frmParam.elements[element+'['+i+']'].value = "";
					document.frmParam.elements[element+'['+i+']'].readOnly = true;
					document.frmParam.elements[element+'['+i+']'].style.backgroundColor ='#D4D4D4';
				}
				for(i=0;i<pjg;i++){	
					document.frmParam.elements[element+'['+i+']'].readOnly = false;
					document.frmParam.elements[element+'['+i+']'].style.backgroundColor ='#FFFFFF';
				}
				
			}
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
}

function transferInbox(spaj, lus_id)
{
	ajaxManager.transferInbox(spaj, lus_id,
		{callback:function(map) {
		
			DWRUtil.useLoadingMessage();
			if(map!=null){
				alert(map.peringatan);
			}
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
}

function listdatabroker(kodebroker)
{
	ajaxManager.listbroker(kodebroker,
	{callback:function(map) {
		DWRUtil.useLoadingMessage();
		document.frmParam.elements['broker.lsb_nama'].value = map.nama_broker;
		},
		timeout:180000,
		errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
}

//helpdesk [137730] tambahin pilihan campaign
function selectMstCampaignProduct(hasil){
	ajaxSelectWithParam1(hasil,'selectMstCampaignProduct', 'camplist', 'datausulan.campaign_id', 'cmd.datausulan.campaign_id', 'MCP_CAMPAIGN_ID', 'MCP_CAMPAIGN_NAME', null);
}

function setSLPSyariah(kode_bisnis, usiaTtg) { //helpdesk [138638] produk baru SLP Syariah (223-2)
	var camplist = document.getElementById('DDLSLP');
	
	if(kode_bisnis == '223~X2'){
		ajaxSetDDLSLP(usiaTtg);
		camplist.style.display = "block";
	}
	else
		camplist.style.display = "none";
}

function ajaxSetDDLSLP(usiaTtg){ //helpdesk [138638] produk baru SLP Syariah (223-2)
	var control = "";
	var lama = 0, lamaMax = 20, lamaTtg = 60, loop = 1, ddlValue = 10;
	if(usiaTtg <= 40)
		lama = lamaMax;
	else
		lama = lamaMax - ((usiaTtg + lamaMax) - lamaTtg);
	
	lama = (lama < 0 ? 0 : lama);
	
	control = "<select name='pilih_slp' onchange='setSLP(this.options[this.selectedIndex].value)'>";
	for(loop = 1; loop <= (lama - 9); loop++){
		control += "<option value='" + ddlValue + "'";
		if(document.frmParam.elements['datausulan.mspo_pay_period'].value == ddlValue) control += " selected";
		control += ">" + ddlValue + "</option>";
		ddlValue++;
	}
	control += "</select>";
	
	document.getElementById("DDLSLP").innerHTML = control;
	setSLP(10);
}

function setSLP(ddlValue){ //helpdesk [138638] produk baru SLP Syariah (223-2)
	document.frmParam.elements['datausulan.mspr_ins_period'].value = ddlValue;			
	document.frmParam.elements['datausulan.mspo_pay_period'].value = ddlValue;
}