<%@ page contentType="text/html" language="java"
	import="com.ekalife.utils.EncryptUtils" errorPage=""%>
<%@ include file="/include/page/taglibs.jsp"%>
<%

	String url = request.getRequestURL().toString().toLowerCase();
	String intranet = "http://intranet";
	String ajsjva = "http://128.21.32.14";
	String ip = request.getLocalAddr();
	
	if (ip.contains("128.") || ip.contains("196.")){
		ajsjva = "http://128.21.32.14";
	}else {
		intranet = "http://www.sinarmasmsiglife.com";
		ajsjva = "http://202.43.181.35";
	}
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="description" content="Sinarmas MSIG Life Insurance" />
<meta name="keywords" content="asuransi,asuransi jiwa,sinarmas,ekalife" />
<meta name="author"
	content="PT. Asuransi Jiwa Sinarmas MSIG  | Administered by: Yusuf Sutarko | Original design: Andreas Viklund - http://andreasviklund.com/" />
<link rel="stylesheet" type="text/css" href="include/css/andreas02.css"
	media="screen" title="andreas02 (screen)" />
<script type="text/javascript"
	src="<%=request.getContextPath()%>/include/js/default.js"></script>
<script>
	hideLoadingMessage();
</script>
<title>PT. Asuransi Jiwa Sinarmas MSIG</title>
</head>
<body>
	<div id="toptabs">
		<p>&nbsp;</p>
	</div>
	<div id="container">
		<div id="navitabs">
			<h2 class="hide">Site menu:</h2>
			<a class="navitab1"
				href="<%=request.getContextPath()%>/common/home.htm">&nbsp;</a>
		</div>
		<div class="block" style="text-align: center">
			<table align=center>
				<tr>
					<c:if test="${sessionScope.currentUser.jn_bank ne 2 and sessionScope.currentUser.jn_bank ne 3}">
						<td align=center><a href="/E-Policy" target="_blank"><img
								src="include/image_links/epol_s.gif" title="E-Policy" /><br />E-Policy</a>
						</td>
						<td align=center><a href="/E-Agency" target="_blank"><img
								src="include/image_links/eagency_s.gif" title="E-Agency" /><br />E-Agency</a>
						</td>
						
						<c:if test="${sessionScope.currentUser.lus_id ne 2527}">
							<td align=center>
								<a href="http://128.21.32.14/bas?auth=<%=EncryptUtils.encode("14041985".getBytes())%>"
									target="_blank"><img src="include/image_links/bas_s.gif"
									title="BAS" /><br />BAS (internal)
								</a>
							</td>
							<td align=center>
								<a href="http://202.43.181.35/bas?auth=<%=EncryptUtils.encode("14041985".getBytes())%>"
									target="_blank"><img src="include/image_links/bas_s.gif"
									title="BAS" /><br />BAS (eksternal)
								</a>
							</td>
						</c:if>
					</c:if>
					
					<c:if test="${sessionScope.currentUser.jn_bank eq 2}">
					<td align=center><a href="http://eproposal.sinarmasmsiglife.co.id/?web=elions&lusId=${sessionScope.currentUser.lus_id}&type=bsm" target="_blank"><img
							src="include/image_links/proposal.gif" title="E-Proposal" /><br />E-Proposal</a></c:if></td>
					<c:if test="${sessionScope.currentUser.jn_bank eq 3}">
					<td align=center><a href="http://eproposal.sinarmasmsiglife.co.id/?web=elions&lusId=${sessionScope.currentUser.lus_id}&type=sms" target="_blank"><img
							src="include/image_links/proposal.gif" title="E-Proposal" /><br />E-Proposal</a></c:if></td>
				</tr>
			</table>
			<table align=center>
				<tr>
					<td>
						<h3>Links</h3>
						<div style="text-align: left">
							<ul>
								<li><a href="<%=intranet%>/productinfo.asp">Product
										Info</a>
								</li>
								<li><a href="<%=intranet%>/faq.asp">FAQ</a>
								</li>
								<li><a href="<%=intranet%>/flash/pageFlip/fc.html">Brosur
										Produk</a>
								</li>
								<li><a href="<%=intranet%>/helpdesk">Helpdesk</a>
								</li>
							</ul>
						</div></td>
				</tr>
			</table>
		</div>
		<div id="footer" style="text-align: center;">
			Copyright &copy; 2006-2007 <br />IT Department - PT. Asuransi Jiwa
			Sinarmas MSIG
		</div>
	</div>
</body>
</html>