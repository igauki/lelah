<%@ include file="/include/page/header.jsp"%>
<%@page pageEncoding="UTF-8"%>

<script type="text/javascript">
	function testprod(value){
//  		var lsbs = document.getElementById('lsbs').value; 
//  		var lsdbs = document.getElementById('lsdbs').value; 
// 		document.getElementById('asd').innerHTML = kaki;
// 		alert(kaki);
// 		window.location="${path}/uw/worksheet_uw_renewal.htm?reg_spaj=${cmd.lsRenewal[0].REG_SPAJ}&lsbs="+lsbs+"&lsdbs="+lsdbs;
	}
</script>

<c:choose>
<c:when test="${not empty err}" >
	<div id="error" style="text-align:center; text-transform:uppercase;">${err}</div>
</c:when>
<c:otherwise>
<body style="height: 100%;">
	<div id="contents">
	<form:form commandName="cmd" id="formpost" method="post">
		<c:if test="${cmd.lsRenewal[0] ne null}" >
		<table class="entry2">
		<tr>
    	  		<td colspan="3">
    	  			<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
								info:<br>
									<c:forEach var="error" items="${status.errorMessages}">
												- <c:out value="${error}" escapeXml="false" />
										<br/>
									</c:forEach>
							</div>
						</c:if>									
					</spring:bind>
    	  		</td>
    	  	</tr>
			<tr>
				<th colspan="3">
					UNDERWRITING WORKSHEET RENEWAL
				</th>
			</tr>

			<tr>
				<th colspan="3">
					<fieldset>
						<legend>Data Polis</legend>
						<table class="entry2">
							<tr>
								<th width="30%">Nomor SPAJ / No Blanko</th>
								<td width="3%">:</td>
								<td width="67%">${cmd.lsRenewal[0].REG_SPAJ} / ${cmd.lsRenewal[0].NO_BLANKO} </td>
							</tr>
							<tr>
								<th width="30%">Nama Pemegang / Umur</th>
								<td width="1%">:</td>
								<td width="67%">${cmd.lsRenewal[0].PEMEGANG}</td>
							</tr>
							<tr>
								<th width="30%">Nama Tertanggung / Umur</th>
								<td width="1%">:</td>
								<td width="67%" id="asd">
									<select name="area6" id="area6">
										<c:forEach var="pr" items="${cmd.lsRenewal}">
											<option value="${pr.TERTANGGUNG}" <c:if test="${pr.TERTANGGUNG eq pr.ID }">selected</c:if>>
												${pr.TERTANGGUNG}
											</option>
										</c:forEach>
									</select>
								</td>	
							</tr>
							<tr>
								<th width="30%">Plan Renewal</th>
								<td width="1%">:</td>
								<td width="67%">
									<select name="prod1" id="prod1" onChange="testprod()">
										<option value="">--- Silahkan Pilih Poduk ---</option>
										<optgroup label="Produk Utama" >
										<c:forEach var="pr" items="${cmd.lsDataUsulan1}">
											<option value="${pr.PROD}" <c:if test="${pr.PROD eq cmd.prod1}">selected</c:if>>
												${pr.PROD} - [${pr.LSBS_ID}] - [${pr.LSDBS_NUMBER}] 
												<c:set var="lsbs" value="${pr.LSBS_ID}" />
												<c:set var="lsdbs" value="${pr.LSDBS_NUMBER}" />
											</option>
										</c:forEach>
										</optgroup>
										<c:if test="${not empty cmd.lsDataUsulan2}">
											<optgroup label="Produk Rider" >
												<c:forEach var="pr" items="${cmd.lsDataUsulan2}">
													<option value="${pr.PROD}" <c:if test="${pr.PROD eq cmd.prod1}">selected</c:if>>
														${pr.PROD} - [${pr.LSBS_ID}] - [${pr.LSDBS_NUMBER}]
														<c:set var="lsbs" value="${pr.LSBS_ID}" /> 
														<c:set var="lsdbs" value="${pr.LSDBS_NUMBER}" />
													</option>												
												</c:forEach>
											</optgroup>
										</c:if>
									</select>
									<input type="hidden" id="lsbs" name="lsbs" value="${lsbs}" />	
									<input type="hidden" id="lsdbs" name="lsdbs" value="${lsdbs}" />	
								</td>
							</tr>
							<tr>
								<th width="30%">Periode Polis</th>
								<td width="1%">:</td>
								<td width="67%">${cmd.lsRenewal[0].PERIODE_POLIS}</td>
							</tr>
							<tr>
								<th width="30%">Premi Periode Selanjutnya</th>
								<td width="1%">:</td>
								<td width="67%">
									<input type="text" style="width:250px;" name="area4" value="${cmd.area4}"/>
								</td>
							</tr>
						</table>
					</fieldset>
				</th>
			</tr>				
			<tr>
				<td width="30%">
					<fieldset>
						<legend>Attention List</legend>
						<table class="entry">
							<tr>
								<td>
									<textarea cols="80" rows="8" name="area1" >${cmd.area1}</textarea>
								</td>
							</tr>
						</table>
					</fieldset>
				</td>
			</tr>
			<tr>
				<td width="30%">
					<fieldset>
						<legend>Klaim Kesehatan</legend>
						<table class="entry">
							<tr>
								<td>
									<textarea cols="80" rows="8" name="area2">${cmd.area2}</textarea>
								</td>
							</tr>
						</table>
					</fieldset>
					
				</td>
				<td colspan="2">
				</td>
			</tr>
			<tr>
				<td width="30%">
					<fieldset>
						<legend>Simultan</legend>
						<table class="entry">
							<tr>
								<td>
									<textarea cols="80" rows="8" name="area3">${cmd.area3}</textarea>
								</td>
							</tr>
						</table>
					</fieldset>
					
				</td>
				<td colspan="2">
				</td>
			</tr>
			<tr>
				<td width="30%">
					<fieldset>
						<legend>Keputusan UW</legend>
						<table class="entry">
							<tr>
								<td>
									<textarea cols="80" rows="8" name="area5" id="area5">${cmd.area5}</textarea>
								</td>
							</tr>
						</table>
					</fieldset>
					
				</td>
				<td colspan="2">
				</td>
			</tr>
			<tr> 
		        <th nowrap="nowrap" colspan="4">
				    <input type="hidden" name="reg_spaj" value="${cmd.lsRenewal[0].REG_SPAJ}" >
				    <input type="hidden" name="flag" value="0" >
				    <input type="submit" name="Ok" value="Save" >
				    <input type="button" name="Cancel" value="Cancel" >
		        	
		        	<c:if test="${not empty submitSuccess }">
			        	<div id="success">
				        	<script type="text/javascript">
				        		var spaj = '${cmd.lsRenewal[0].REG_SPAJ}';
				        		var area1 = '${cmd.area1}';
				        			var ar1 = encodeURIComponent(area1);
				        		var area2 = '${cmd.area2}';
				        			var ar2 = encodeURIComponent(area2);
				        		var area3 = '${cmd.area3}';
				        			var ar3 = encodeURIComponent(area3);
				        		var area4 = '${cmd.area4}';
				        		var area5 = '${cmd.area5}';
				        			var ar5 = encodeURIComponent(area5);
				        		var area6 = '${cmd.area6}';
				        		var prod1 = '${cmd.prod1}';

								window.location='${path}/report/uw.htm?window=reportWorksheetUwRenewal&nospaj='+spaj+'&a2='+ar2+'&a3='+ar3+'&p1='+prod1+'&a4='+area4+'&a5='+ar5+'&a1='+ar1+'&a6='+area6;
// 				        		alert("Worksheet telah berhasil dibuat.");
				        	</script>
			        	</div>
			        </c:if>	
				</th>
    	  	</tr>
    	  	
		</table>
		</c:if>
		<c:if test="${cmd.lsRenewal[0] eq null}" >
			<script>
				alert("Polis Tidak dapat ditampilkan, silahkan hubungi EDP");
			</script>
		</c:if>

	</form:form>
	</div>
</body>

</c:otherwise>
</c:choose>

<%@ include file="/include/page/footer.jsp"%>