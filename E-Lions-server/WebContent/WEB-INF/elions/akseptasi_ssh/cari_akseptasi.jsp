<%@ include file="/include/page/header.jsp"%>
<script>
	function backToParent(spaj){
		//self.opener.document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj; 
		//addOptionToSelect(self.opener.document, self.opener.document.formpost.spaj, spaj , spaj);
		
		if(self.opener.document.getElementById('infoFrame') && self.opener.document.formpost.spaj){
			self.opener.document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj; 
			//self.opener.document.formpost.spaj.selected=spaj;
			addOptionToSelect(self.opener.document, self.opener.document.formpost.spaj, spaj , spaj);
		}else if(self.opener.document.formCari.txtnospaj){
				self.opener.document.frm_search.txtnospaj.value=spaj;
		}
		window.close();	
	}
</script>
<BODY onload="document.formpost.kata.select(); document.title='PopUp :: Cari Reinstate';">

<form method="post" name="formpost" action="${path }/uw/cari_akseptasi.htm">
	<div id="contents">
		<span class="subtitle">Cari Akseptasi</span>
		<input type="hidden" name="posisi" value="${param.posisi}">
		<table class="entry">
			<tr>
				<th>Cari:</th>
				<th>
					<select name="kategori">
						<option value="1" <c:if test="${param.kategori eq \"1\" }">selected</c:if>>No. SPAJ</option>
						<option value="2" <c:if test="${param.kategori eq \"2\" }">selected</c:if>>No. Polis</option>
						<option value="3" <c:if test="${param.kategori eq \"3\" }">selected</c:if>>Pemegang</option>
						<option value="4" <c:if test="${param.kategori eq \"4\" }">selected</c:if>>Tertanggung</option>
					</select>
					<input type="text" name="kata" size="50" value="${param.kata }">
					<input type="submit" name="search" value="Search">
				</th>
			</tr>
		</table>
		<table class="simple">
			<thead>
				<tr>
					<th>No. SPAJ</th>
					<th>No. Polis</th>
					<th>Pemegang</th>
					<th>Tertanggung</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="spaj" items="${cmd.listSpaj}" varStatus="stat">
					<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
						onclick="backToParent('${spaj.REG_SPAJ}');">
						<td>${spaj.REG_SPAJ}</td>
						<td>${spaj.MSPO_POLICY_NO}</td>
						<td>${spaj.TT}</td>
						<td>${spaj.PP}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<br>
		<input type="button" name="close" value="Close" onclick="window.close();">
	</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>