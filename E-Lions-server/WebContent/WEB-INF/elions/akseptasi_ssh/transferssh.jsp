<%@ include file="/include/page/header.jsp"%>
<script>
	function awal(){
		var info="${cmd.info}"	;
		//	0=ok 1=no polis error   2=no endor error
		if(info=='0'){
			alert("Proses Transfer Berhasil");
		}else if(info=='1'){
			alert("Error Pada saat Buat no Polis \nSilahKan Hubungi EDP");
		}else if(info=='2'){
			alert("Error Pada saat Buat no Endors \nSilahkan Hubungi EDP");
		}
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
	}
	
</script>
<body onload="awal();">
	<form>
		<table>
			<tr>
				<td><div id="success">Silahkan Tunggu... Sedang dilakukan Proses Transfer...</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
								ERROR:<br>
									<c:forEach var="error" items="${status.errorMessages}">
												- <c:out value="${error}" escapeXml="false" />
										<br/>
									</c:forEach>
							</div>
						</c:if>									
					</spring:bind>
		          
				</td>
			</tr>
			
		</table>
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>