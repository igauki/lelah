<%@ include file="/include/page/header.jsp"%>

<script>
	function ref(){
		formpost.flag.value=1;
		formpost.submit();
	}
	
	function test(){
		var ab = document.getElementById("area1").value;
		
		if (ab == 4){//extra premi
	  		document.getElementById("ket2").innerHTML = "Premi + Extra";
	  		document.getElementById("ket3").innerHTML = "Premi Top up";
	  		document.getElementById("abc2").style.visibility = "visible";
	  		document.getElementById("abc3").style.visibility = "visible";
	  		document.getElementById("tombol").style.visibility = "visible";
	  		document.getElementById("xtra").style.visibility = "visible";
	  		document.getElementById("xtra1").style.visibility = "visible";
	  		document.getElementById("crbyr1").style.visibility = "visible";
	  		document.getElementById("nmrsrt").style.visibility = "visible";
	  	}else{//borderline & akseptasi
	  		document.getElementById("ket2").innerHTML = "Keterangan";
	  		document.getElementById("abc2").style.visibility = "hidden";
	  		document.getElementById("abc3").style.visibility = "hidden";
	  		document.getElementById("tombol").style.visibility = "visible";
	  		document.getElementById("xtra").style.visibility = "hidden";
	  		document.getElementById("xtra1").style.visibility = "hidden";
	  		document.getElementById("crbyr1").style.visibility = "hidden";
	  		document.getElementById("nmrsrt").style.visibility = "hidden";
	  	}
	}
	
	function testprod() {
		var select = document.getElementById('prod1').value; 
		var cek = select.substr(0, 3);

		if (cek>300){
			document.getElementById("crbyr1").style.display = 'none';
		}else{
			document.getElementById("crbyr1").style.display = 'block';
		}
	
	}
	
</script>

<c:choose>
<c:when test="${not empty err}" >
	<div id="error" style="text-align:center; text-transform:uppercase;">${err}</div>
</c:when>
<c:otherwise>
<body style="height: 100%;">
	<div id="contents">
	<form:form commandName="cmd" id="formpost" method="post">
	<c:if test="${cmd.lsRenewal[0] ne null}" >
		<table class="entry2">
			<tr>
    	  		<td colspan="3">
    	  			<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
								info:<br>
									<c:forEach var="error" items="${status.errorMessages}">
												- <c:out value="${error}" escapeXml="false" />
										<br/>
									</c:forEach>
							</div>
						</c:if>									
					</spring:bind>
    	  		</td>
    	  	</tr>
			<tr>
				<th colspan="3">
					AKSEPTASI RENEWAL
				</th>
			</tr>

			<tr>
				<th colspan="3">
					<fieldset>
						<legend>Data User</legend>
						<table class="entry2">
								<tr>
									<th nowrap="nowrap">User</th>
									<th align="left">&nbsp;[${cmd.lus_id}]&nbsp;&nbsp;${cmd.lus_login}</th>
								</tr>
								<tr>
									<th nowrap="nowrap">Nama<br></th>
									<th align="left">&nbsp;${cmd.lus_full_name}&nbsp;[${cmd.lus_dept}]</th>
								</tr>
						</table>
					</fieldset>
				</th>
			</tr>
						<tr></tr>
		 	<tr></tr>	

		 	<tr>
			 <th nowrap="nowrap">Pilih Produk </th>
			 <th nowrap="nowrap" class="left">
						<select name="prod1" id="prod1" onChange="testprod()">
							<option value="">--- Silahkan Pilih Poduk ---</option>
							<optgroup label="Produk Utama" >
							<c:forEach var="pr" items="${cmd.lsDataUsulan1}">
								<option value="${pr.KODE}" <c:if test="${pr.KODE eq cmd.prod1}">selected</c:if>>
									${pr.PROD} - [${pr.LSBS_ID}]
								</option>
							</c:forEach>
							</optgroup>
							<c:if test="${not empty cmd.lsDataUsulan2}">
								<optgroup label="Produk Rider" >
									<c:forEach var="pr" items="${cmd.lsDataUsulan2}">
										<option value="${pr.KODE}" <c:if test="${pr.KODE eq cmd.prod1}">selected</c:if>>
											${pr.PROD} - [${pr.LSBS_ID}] - [${pr.LSDBS_NUMBER}]
										</option>
									</c:forEach>
								</optgroup>
							</c:if>
						</select>
						
			</th>
			</tr>		
			<tr>
			<th nowrap="nowrap">Pilih Akseptasi </th>
					   <th nowrap="nowrap" class="left"">
						<select name="area1" onChange="test();" id="area1">
							<c:forEach var="pil" items="${cmd.aksep}">
							<option
								<c:if test="${pil.ID eq '0'}"> SELECTED </c:if>
								value="${pil.ID}">${pil.AKSEP}
							</option>
							</c:forEach>
						</select>
						<span id="xtra" style="visibility:hidden;">
							&nbsp;&nbsp;&nbsp; Extra Premi Sebesar: <input type="text" style="width:35px;" name="area7" value="${cmd.area7}"> %
						</span>
					</th>

			</tr>
			<tr id="xtra1" style="visibility:hidden;">
				<th nowrap="nowrap">Keterangan Tambahan </th>
				<th nowrap="nowrap" class="left">
					<input type="text" name="area6" size="80" value="${cmd.area6}">
				</th>
			</tr>
			<tr id="crbyr1" style="visibility:hidden;">
			<th nowrap="nowrap">Cara Bayar </th>
					<th nowrap="nowrap" class="left">
						<select name="crbyr"  id="crbyr">
							<option value="">--- Silahkan Pilih ---</option>
							<option value="auto" <c:if test="${cmd.crbyr eq 'auto'}"> SELECTED </c:if>>AUTODEBET</option>
							<option value="trans" <c:if test="${cmd.crbyr eq 'trans'}"> SELECTED </c:if>>TRANSFER</option>
						</select>
					</th>
			</tr>
			<tr id="nmrsrt" style="visibility:hidden;">
				<th nowrap="nowrap">Nomor Surat </th>
				<th nowrap="nowrap" class="left">
				<input type ="hidden" size = "3" name="area3" id=area3 value="${cmd.nomorsrt}"/><span style="color:red; font-size:11px;">${cmd.nomorsrt}</span>
			</th>	
			</tr>
			<tr id="abc2" style="visibility:hidden;">
				<th width="200px" nowrap="nowrap"><span id="ket2">Keterangan</span></th>
				<th nowrap="nowrap" class="left">
					<input type="text" style="width:250px;" name="area4" value="${cmd.area4}"><span class="info">&nbsp;*Jika ada satuan sen, gunakan tanda titik.</span>
				</th>	
			</tr>
			<tr id="abc3" style="visibility:hidden;">
				<th width="200px" nowrap="nowrap"><span id="ket3">Keterangan</span></th>
				<th nowrap="nowrap" class="left">
					<input type="text" style="width:250px;" name="area2" id="area2" value="${cmd.area2}"><span class="info">&nbsp;*Jika tidak ada, harap dikosongkan</span>
				</th>	
			</tr>
			<tr></tr>
			<tr></tr>
			<tr> 
		        <th nowrap="nowrap" colspan="4" id="tombol" style="visibility:hidden;">
				    <input type="hidden" name="nospaj" value="${cmd.lsRenewal[0].REG_SPAJ}" >
				    <input type="hidden" name="flag" value="0" >
				    <input type="submit" name="Ok" value="Save" >
				    <input type="button" name="Cancel" value="Cancel" >
				    
		        	
		        	<c:if test="${not empty submitSuccess }">
			        	<div id="success">
				        	<script type="text/javascript">
				        		var spaj = '${cmd.lsRenewal[0].REG_SPAJ}';
				        		var area1 = '${cmd.area1}';
				        		var area2 = '${cmd.area2}';
				        		var area4 = '${cmd.area4}';
				        		var prod1 = '${cmd.prod1}';
				        		var crbyr = '${cmd.crbyr}';
				        		var ar6 = '${cmd.area6}';
				        			var area6 = encodeURIComponent(ar6);
				        		var area7 = '${cmd.area7}';
				        		var nmrsurat = '${cmd.area3}';
				        		
			        			if (confirm('Apakah Akseptasi sudah benar?')){
			        				window.location='${path}/report/uw.htm?window=printSuratRenewal&nospaj='+spaj+'&a1='+area1+'&a2='+area2+'&a4='+area4+'&p1='+prod1+'&c1='+crbyr+'&a7='+area7+'&nm='+nmrsurat+'&a6='+area6;
// 						        	alert("Email Telah Terkirim ke Departemen Life Benefit.");
					        	}
				        	</script>
			        	</div>
			        </c:if>	
				</th>
    	  	</tr>
		</table>
	</c:if>
	<c:if test="${cmd.lsRenewal[0] eq null}" >
		<script>
			alert("Polis Tidak dapat ditampilkan, silahkan hubungi EDP");
		</script>
	</c:if>
	</form:form>
	</div>
</body>
</c:otherwise>
</c:choose>
<%@ include file="/include/page/footer.jsp"%>