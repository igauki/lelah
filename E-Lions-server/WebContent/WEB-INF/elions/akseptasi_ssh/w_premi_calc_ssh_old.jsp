<%@ include file="/include/page/header.jsp"%>
<style type="text/css">
	html,body{
	     overflow: hidden;
	}
</style>
<script>
	function add(){
		rb();
		formpost.flag.value="1";
		formpost.submit();
	}
	
	function ubah(i){
		alert(i);
		rb();
		formpost.flag.value="2"; 
		formpost.brs.value=i;
		formpost.item.value="1";
		formpost.submit();
	}
	
	function rb(){
		if(document.formpost.standard[0].checked)
			document.formpost.rb.value="0"
		else if(document.formpost.standard[1].checked)
			document.formpost.rb.value="1"
	}
	
	function ekstra(i){
		alert(i);
		rb();
		formpost.flag.value="2"; 
		formpost.brs.value=i;
		formpost.item.value="2";
		formpost.submit();
	
	}
	
	function bunga(i){
		alert(i);
		rb();
		formpost.flag.value="2"; 
		formpost.brs.value=i;
		formpost.item.value="3";
		formpost.submit();
	
	}
	
</script>
<body onresize="setFrameSize('infoFrame', 45);">
<form name="formpost" method="post">
<div class="tabcontent">
<table class="entry" width="98%">
 <span class="subtitle">Premium Calculation</span> 
   
	<tr>
		<td>
			<spring:bind path="cmd.standard"> 
				<input type="radio" name="${status.expression}" value="${status.value}"
					<c:if test="${status.value eq 0 }" > checked </c:if>>
		    	Standard 
				<input type="radio" name="${status.expression}" value="${status.value}"
					<c:if test="${status.value eq 1 }" > checked </c:if>>
		    		Non Standard
		    	<input type="hidden" name="rb" >
    		</spring:bind>
    	</td>
    		
	</tr>
	<tr>
		<th>Product</th>
		<th>Sum Insured</th>
		<th>EM</th>
		<th>Rate</th>
		<th>Premium</th>
		<th>Discount</th>
		<th>Total</th>
		<th>%</th>
		<th>Ins. Period</th>
		<th>End Date</th>
	</tr>
	<spring:bind path="cmd.lsProduk">
		<c:forEach var="s" items="${status.value}" varStatus="xt">
			<tr>
				<td width="50">
					<c:choose >
						<c:when test="${s.tambah ne 1 }">
							<select name="produk" disabled>
								<c:forEach var="x" items="${bisnisIdAll}">
									<option value="${x.BISNIS_ID}"
										<c:if test="${s.lsbs_id eq  x.LSBS_ID }">Selected</c:if>>
											${x.LSDBS_NAME}
									</option>
								</c:forEach>
							</select>
						</c:when>	
						<c:when test="${s.tambah eq 1}">
							<select name="produk" onChange="ubah(${xt.index });">
								<c:forEach var="x" items="${bisnisIdRider}">
									<option value="${x.BISNIS_ID}"
										<c:if test="${s.selectIndex eq  x.LSBS_ID }">Selected</c:if>>
											${x.LSDBS_NAME}
									</option>
								</c:forEach>
							</select>
						</c:when>
					</c:choose>	
				</td>
				<td>
					<input type="text" readonly name="tsi" value=
					<fmt:formatNumber value="${s.mspr_tsi}" type="currency" currencySymbol=""/>	
					 >
				</td>
				<td>
					<input type="text" name="extra" maxlength="5" size="5" onChange="ekstra(${xt.index });"
					<c:if test="${s.tambah ne 1 }"> readonly</c:if>
					value=<fmt:formatNumber value="${s.mspr_extra}" type="currency" currencySymbol=""/>	
					 >
				</td>
				<td>
					<input type="text"  name="rate" maxlength="5" size="5" onChange="bunga(${xt.index });"
					<c:if test="${s.tambah ne 1 }"> readonly</c:if>
					value=<fmt:formatNumber value="${s.mspr_rate}" type="currency" currencySymbol=""  />	
					 >
				</td>
				<td>
					<input type="text" readonly name="premi" value=
					<fmt:formatNumber value="${s.mspr_premium}" type="currency" currencySymbol=""  />	
					>
				</td>
				<td>
					<input type="text" readonly name="discount"  size="5" value=
					<fmt:formatNumber value="${s.mspr_discount}" type="currency" currencySymbol=""  />	
					>
				</td>
				<td>
					<input type="text" readonly name="total" value=
					<fmt:formatNumber value="${s.total}" type="currency" currencySymbol=""  />	
					>
				</td>
				<td>
					<input type="text" readonly name="persen" size="5" value=
					<fmt:formatNumber value="${s.mspr_persen}" type="currency" currencySymbol=""  />	
					>
				</td>
				<td>
					<input size="5" type="text" readonly name="insPeriod" value="${s.mspr_ins_period} Y" >
				</td>
				<td>
					<input type="text" readonly name="endDate" value=
					<fmt:formatDate value="${s.mspr_beg_date}" pattern="dd/MM/yyyy"/>>
				</td>
				
			</tr>			
		</c:forEach>
	</spring:bind>
</table>
<table width="100%">
	<tr>
		<td></td>
		<td align="center">
			<input type="hidden" name="flag" >
			<input type="hidden" name="brs" >
			<input type="text" name="item" >
			<input type="button" name="btn_add" value="Add" onClick="add();">
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<c:if test="${not empty submitSuccess }">
	        	<div id="success">
		        	Berhasil
	        	</div>
	        </c:if>	
  			<spring:bind path="cmd.*">
				<c:if test="${not empty status.errorMessages}">
					<div id="error">
						ERROR:<br>
							<c:forEach var="error" items="${status.errorMessages}">
										- <c:out value="${error}" escapeXml="false" />
								<br/>
							</c:forEach>
					</div>
				</c:if>									
			</spring:bind>
          
		</td>
	</tr>
</table>
</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>