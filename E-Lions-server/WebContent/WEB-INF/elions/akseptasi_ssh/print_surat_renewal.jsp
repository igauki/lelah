<%@ include file="/include/page/header.jsp"%>
<body style="height: 100%;">
<script>
	function ref(){
		formpost.flag.value=1;
		formpost.submit();
	}
	
</script>
	<div id="contents">
	<form:form commandName="cmd" id="formpost" method="post">
		<table class="entry2">
			<tr>
				<th colspan="3">
					PRINT SURAT AKSEPTASI
				</th>
			</tr>

			<tr>
				<th colspan="3">
					<fieldset>
						<legend>Data User</legend>
						<table class="entry2">
								<tr>
									<th nowrap="nowrap">User</th>
									<th align="left">&nbsp;[${cmd.lus_id}]&nbsp;&nbsp;${cmd.lus_login}</th>
								</tr>
								<tr>
									<th nowrap="nowrap">Nama<br></th>
									<th align="left">&nbsp;${cmd.lus_full_name}&nbsp;[${cmd.lus_dept}]</th>
								</tr>
						</table>
					</fieldset>
				</th>
			</tr>				
			<tr></tr>
			<tr></tr>
			<tr>
				<th nowrap="nowrap">Nomor Surat </th>
				<th nowrap="nowrap" class="left">
					<input type ="text" size = "40" name="area3" id="area3" value=""/>
				</th>	
			</tr>
			<tr></tr>
			<tr></tr>
			<tr> 
		        <th nowrap="nowrap" colspan="4">
				    <input type="hidden" name="reg_spaj" value="" >
				    <input type="hidden" name="flag" value="0" >
				    <input type="hidden" name="area1" value="${cmd.area1}" >
		            <input type="hidden" name="area2" value="${cmd.area2}" >
		            <input type="hidden" name="area4" value="${cmd.area4}" >
		            <input type="hidden" name="prod1" value="${cmd.prod1}" >
		            <input type="hidden" name="crbyr" value="${cmd.crbyr}" >
		            <input type="hidden" name="area7" value="${cmd.area7}" >
				    <input type="submit" name="Ok" value="Print">
				    <input type="button" name="Cancel" value="Cancel" >
		        	
		        	
		        	<c:if test="${not empty submitSuccess }">
			        	<div id="success">
				        	<script type="text/javascript">
		        				if (confirm('Apakah nomor surat sudah benar?')){
									window.location="${path}/report/uw.htm?window=printSuratRenewal&nospaj=${cmd.lsRenewal[0].REG_SPAJ}&a1=${cmd.area1}&a2=${cmd.area2}&a3=${cmd.area3}&a4=${cmd.area4}&a7=${cmd.area7}&p1=${cmd.prod1}&c1=${cmd.crbyr}";
				        			window.location="${path}/uw/view.htm?showSPAJ=${cmd.lsRenewal[0].REG_SPAJ}";
				        			alert("Surat Berhasil Dibuat dan Email Telah Terkirim ke Departemen Life Benefit.");
				        		}
						       	
				        	</script>
			        	</div>
			        </c:if>	
				</th>
    	  	</tr>
    	  	<tr>
    	  		<td colspan="3">
    	  			<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
								info:<br>
									<c:forEach var="error" items="${status.errorMessages}">
												- <c:out value="${error}" escapeXml="false" />
										<br/>
									</c:forEach>
							</div>
						</c:if>									
					</spring:bind>
    	  		</td>
    	  	</tr>
		</table>

	</form:form>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>