<%@ include file="/include/page/header.jsp"%>
</style>
<script>
	function awal(){
		p=formpost.statAksep.length;
		formpost.statAksep[p-1].disabled=false;
		formpost.txtmsps_desc[p-1].disabled=false;
		if(formpost.sukses.value=='1'){
			formpost.btn_save.disabled=true;
			
		}	
	}
	function simpan(){
		alert("Simpan");
		alert(formpost.txtmsps_desc)
		formpost.submit();
	}
</script>
<body onload="awal();"onresize="setFrameSize('infoFrame', 45);">
<form name="formpost" method="post">
<div class="tabcontent">
<table class="entry" width="60%">
 <span class="subtitle">Status </span> 
	<tr>
		<th>Tanggal	</th>		
		<th>Akseptor	</th>		
		<th>Status	</th>		
		<th>Keterangan	</th>		
	</tr>	   
	<c:forEach var="s" items="${cmd.lsStatAksep}" varStatus="xt">
		<tr>
			<spring:bind path="cmd.lsStatAksep[${xt.index }].msps_date">
				<td>${status.value }</td>
			</spring:bind>	
			<td>${s.lus_login_name}</td>
			<td>
				<select name="statAksep" onChange="ubah(${xt.index });" disabled>
					<option />
					<c:forEach var="x" items="${lsStatus}">
						<option value="${x.LSSA_ID}"
							<c:if test="${s.lssa_id eq  x.LSSA_ID }">Selected</c:if>>
								${x.STATUS_ACCEPT}
						</option>
					</c:forEach>
				</select>
			</td>
			<td><textarea cols="35"  rows="5" disabled name=txtmsps_desc >${s.msps_desc }</textarea></td>
			
		</tr>
	</c:forEach>
</table>
<table width="50%"> 
	<tr >
		<td align="center">
			<input type="hidden" name="sukses" value="${sukses }">
			<input type="button" name="btn_save" value="Save" onClick="simpan()">
		</td>
		
	</tr>
</table>
<table width="50%">
	<tr>
		<td colspan="2">
			<c:if test="${not empty submitSuccess }">
	        	<div id="success">
		        	Berhasil
	        	</div>
	        </c:if>	
  			<spring:bind path="cmd.*">
				<c:if test="${not empty status.errorMessages}">
					<div id="error">
						ERROR:<br>
							<c:forEach var="error" items="${status.errorMessages}">
										- <c:out value="${error}" escapeXml="false" />
								<br/>
							</c:forEach>
					</div>
				</c:if>									
			</spring:bind>
          
		</td>
	</tr>
</table>
</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>