<%@ include file="/include/page/header.jsp"%>
<style type="text/css">
	html,body{
	     overflow: hidden;
	}
</style>
<script>
	function add(){
		if(document.formpost.standard[0].checked){
			alert("Silahkan pilih non standard terlebih dahulu");
		}else{
			rb();
			formpost.btn_save.disabled=false;
			formpost.btn_cancel.disabled=false;
			formpost.btn_edit.disabled=true;
			formpost.btn_add.disabled=true;
			formpost.flag.value="1";
			formpost.proses.value="1";
			formpost.submit();
		}	
	}
	
	function ubah(i){
		alert(i);
		rb();
		formpost.flag.value="2"; 
		formpost.brs.value=i;
		formpost.item.value="1";
		produk=formpost.produk[i].value;
		bts=produk.indexOf("~");
		//alert(produk+" "+produk.substring(0,bts));
		//alert(produk.substring(bts+2,produk.length));
		formpost.lsbs_id[i].value=produk.substring(0,bts);
		formpost.lsdbs_number[i].value=produk.substring(bts+2,produk.length);
		
		formpost.submit();
	}
	
	function rb(){
		if(document.formpost.standard[0].checked)
			document.formpost.rb.value="0"
		else if(document.formpost.standard[1].checked)
			document.formpost.rb.value="1"
	}
	
	function ekstra(i){
		alert(i);
		rb();
		formpost.flag.value="2"; 
		formpost.brs.value=i;
		formpost.item.value="2";
		formpost.submit();
	
	}
	
	function bunga(i){
		alert(i);
		rb();
		formpost.flag.value="2"; 
		formpost.brs.value=i;
		formpost.item.value="3";
		formpost.submit();
	
	}
	
	function batal(){
		alert("batal")
	}
	
	function edit(){
		alert("edit");
		formpost.btn_save.disabled=false;
		formpost.btn_cancel.disabled=false;
		formpost.btn_edit.disabled=true;
		formpost.btn_add.disabled=true;
		formpost.proses.value="2";
		for(i=1;i<formpost.lsbs_id.length;i++){
			formpost.rate[i].readOnly=false;
			formpost.extra[i].readOnly=false;
		}
			
		
	}
	
	function simpan(){
		alert("save")
		rb();
		formpost.flag.value="3"; 
		formpost.submit();
	}
	
	function awal(){
		var flag=formpost.flag_tombol.value;
		if(flag==1 || flag==2){//add //edit
			formpost.btn_save.disabled=false;
			formpost.btn_cancel.disabled=false;
			formpost.btn_edit.disabled=true;
			formpost.btn_add.disabled=true;
		}
	}
	
</script>
<body onLoad="awal();" onresize="setFrameSize('infoFrame', 45);">
<form name="formpost" method="post">
<div class="tabcontent">
<table class="entry" width="98%">
 <span class="subtitle">Premium Calculation</span> 
   
	<tr>
		<td colspan="2">
			<spring:bind path="cmd.standard"> 
				<input type="radio" name="${status.expression}" value="${status.value}"
					<c:if test="${status.value eq 0 }" > checked </c:if>>
		    	Standard 
				<input type="radio" name="${status.expression}" value="${status.value}"
					<c:if test="${status.value eq 1 }" > checked </c:if>>
		    		Non Standard
		    	<input type="hidden" name="rb" >
    		</spring:bind>
    	</td>
    		
	</tr>
	<tr>
		<th>Product</th>
		<th>Sum Insured</th>
		<th>EM</th>
		<th>Rate</th>
		<th>Premium</th>
		<th>Discount</th>
		<th>Total</th>
		<th>%</th>
		<th>Ins. Period</th>
		<th>End Date</th>
	</tr>
		<c:forEach var="s" items="${cmd.lsProduk}" varStatus="xt">
			<tr>
				<td width="50">
					<c:choose >
						<c:when test="${s.tambah eq 0  }">
							<select name="produk" disabled>
									<option value="${s.lsbs_id}">
											${s.lsdbs_name}
									</option>
							</select>
						</c:when>	
						<c:when test="${s.tambah > 0}">
							<select name="produk" onChange="ubah(${xt.index });">
								<option />
								<c:forEach var="x" items="${bisnisIdRider}">
									<option value="${x.BISNIS_ID}"
										<c:if test="${s.selectIndex eq  x.LSBS_ID }">Selected</c:if>>
											${x.LSDBS_NAME}
									</option>
								</c:forEach>
							</select>
						</c:when>
					</c:choose>	
					<spring:bind path="cmd.lsProduk[${xt.index }].lsbs_id">
						<input type="hidden" name="lsbs_id" value="${status.value }">
					</spring:bind>	
					<spring:bind path="cmd.lsProduk[${xt.index }].lsdbs_number">
						<input type="hidden" name="lsdbs_number" value="${status.value}">
					</spring:bind>	
				</td>
				<td>
					<spring:bind path="cmd.lsProduk[${xt.index}].mspr_tsi">
						<input type="text" readonly name="${status.expression }" value="${status.value}" >
					</spring:bind>	
				</td>
				<td>
					<spring:bind path="cmd.lsProduk[${xt.index }].mspr_extra">
						<input type="text" name="extra" maxlength="5" size="5" onChange="ekstra(${xt.index });"
						<spring:bind path="cmd.lsProduk[${xt.index }].tambah">	
							<c:if test="${status.value ne 1 }"> readonly</c:if>
						</spring:bind>
						value="${status.value}" >
					</spring:bind>	
				</td>
				<td>
					<spring:bind path="cmd.lsProduk[${xt.index }].mspr_rate">
						<input type="text"  name="rate" maxlength="5" size="5" onChange="bunga(${xt.index });"
						<spring:bind path="cmd.lsProduk[${xt.index }].tambah">
							<c:if test="${status.value ne 1 }"> readonly</c:if>
						</spring:bind>
						value="${status.value}" >
					</spring:bind>	
				</td>
				<td>
					<spring:bind path="cmd.lsProduk[${xt.index }].mspr_premium">
						<input type="text" readonly name="${status.expression }" value="${status.value}" >
					</spring:bind>	
				</td>
				<td>
					<spring:bind path="cmd.lsProduk[${xt.index }].mspr_discount">
						<input type="text" readonly name="${status.expression }"  size="5" value="${status.value}" >
					</spring:bind>
				</td>
				<td>
					<spring:bind path="cmd.lsProduk[${xt.index }].total">
						<input type="text" readonly name="${status.expression }" value="${status.value}" >
					</spring:bind>	
				</td>
				<td>
					<spring:bind path="cmd.lsProduk[${xt.index }].mspr_persen">
						<input type="text" readonly name="${status.expression }" size="3" value="${status.value}" >
					</spring:bind>	
				</td>
				<td>
					<spring:bind path="cmd.lsProduk[${xt.index }].mspr_ins_period">
						<input size="2" type="text" readonly name="${status.expression }" value="${status.value}" >
						<c:if test="${status.value > 0 }"> 
							<input size="1" type="text" readonly name="year" value="Y" >
						</c:if>
					</spring:bind>	
				</td>
				<td>
					<spring:bind path="cmd.lsProduk[${xt.index }].mspr_end_date">
						<input type="text" readonly name="${status.expression }" value="${status.value}" >
					</spring:bind>	
				</td>
				
			</tr>			
		</c:forEach>
		<tr>
			<td colspan="3"></td>
			<th>Total</th>
			<td>
			<spring:bind path="cmd.totalPremi">
				<input type="text" name="${status.expression }" value="${status.value}"></td>
			</spring:bind>
			</td>	
			<td></td>
			<td>
			<spring:bind path="cmd.totalTot">
				<input type="text" name="${status.expression }" value="${status.value}"></td>
			</spring:bind>
			</td>	
		</tr>

</table>
<table width="100%">
	<tr>
		<td></td>
		<td align="center">
			<input type="hidden" name="flag" >
			<spring:bind path="cmd.proses">
				<input type="hidden" name="${status.expression}" value="${status.value }">
			</spring:bind>
			<input type="hidden" name="brs" >
			<input type=hidden name="item" >
			<input type="hidden" name="flag_tombol" value="${cmd.tombol }">
			<input type="button" name="btn_add" value="Add" onClick="add();">
			<input type="button" name="btn_edit" value="Edit" onClick="edit();">
			<input type="button" name="btn_save" value="Save" disabled onClick="simpan();">
			<input type="button" name="btn_cancel" value="Cancel" disabled onClick="batal();">
			
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<c:if test="${not empty submitSuccess }">
	        	<div id="success">
		        	Berhasil
	        	</div>
	        </c:if>	
  			<spring:bind path="cmd.*">
				<c:if test="${not empty status.errorMessages}">
					<div id="error">
						ERROR:<br>
							<c:forEach var="error" items="${status.errorMessages}">
										- <c:out value="${error}" escapeXml="false" />
								<br/>
							</c:forEach>
					</div>
				</c:if>									
			</spring:bind>
          
		</td>
	</tr>
</table>
</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>