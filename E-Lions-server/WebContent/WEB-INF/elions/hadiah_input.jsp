<%@ include file="/include/page/header_jquery.jsp"%>
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (jQueryUI Tabs) init tab2 Utama
		$("#tabs").tabs();

		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title], label[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		
		// button icons
		$( "#btnShow" ).button({icons: {primary: "ui-icon-search"}});
		$( "#btnDetail" ).button({icons: {primary: "ui-icon-note"}});
		$( "#btnHistory" ).button({icons: {primary: "ui-icon-script"}});		
		
		// untuk semua textfield, onfocus maka select all text
		$("input:text, textarea, file").focus(function(){
		    this.select();
		});
		
		// user memilih kategori hadiah, tampilkan pilihan hadiah nya
		$("#lhc_id").change(function() {
			$("#lh_id").empty();
			var url2 = "${path}/bas/hadiah.htm?window=json&t=lhc&lhc_id=" + $("#lhc_id").val();
			$.getJSON(url2, function(result2) {
				$("<option/>").val("").html("").appendTo("#lh_id");
				$.each(result2, function() {
					$("<option/>").val(this.key).html(this.value).appendTo("#lh_id");
				});
				$("#lh_id option:first").attr('selected','selected');
			});
			
		});
		
		// user memilih hadiah, tarik beberapa nilai default (seperti harga)
		$("#lh_id").change(function() {
			var url = "${path}/bas/hadiah.htm?window=json&t=lh&lhc_id=" + $("#lhc_id").val() + "&lh_id=" + $("#lh_id").val();
			$.getJSON(url, function(result) {
				//$("#mh_budget").val(result.lh_harga);
				$("#mh_harga").val(result.lh_harga);
			});
			
		});
		
		//Radio Button
		$('input:radio[name=mh_flag_kirim]').change(function() {
				var a = document.getElementById('flag_kirim1').checked;
				var b = document.getElementById('flag_kirim2').checked;
				if(a==1){
					$('input:radio[name=mh_flag_kirim]').val(0);
					var z = $('input:radio[name=mh_flag_kirim]').val();
				}else{
					$('input:radio[name=mh_flag_kirim]').val(1);
					var z = $('input:radio[name=mh_flag_kirim]').val();
				}
		});
		
		//tes file exists
		function yourNameFor_doesFileExist(yourNameFor_urlToTest){
		  $.ajax({
		    url: yourNameFor_urlToTest, 
		    type:'HEAD',
		    error:
		        function(){ 
		             alert("File not found.");
		        },
		    success:
		        function(){
		            alert("File found.");
		        }
		  });  
		}
				
		// user menginput spaj/polis, tampilkan datanya
		$('#reg_spaj, #policy_no').change(function() {
			
			var url_cek = "${path}/bas/hadiah.htm?window=json&t=cek&spajPolis=" + this.value;
			var val = this.value;
			$.getJSON(url_cek, function(result_cek){
				if(result_cek.length == 0){
					//Baru pertama kali
					var url = "${path}/bas/hadiah.htm?window=json&t=polis&spajPolis=" + val;
					$.getJSON(url, function(result) {
						if(result.length == 0){
							alert('SPAJ/Polis tidak ditemukan. Harap cek ulang nomor yang dimasukkan');
						}else{
							$.each(result, function() {
								$('#reg_spaj').val(this.reg_spaj);
								$('#policy_no').val(this.policy_no);
								$('#pemegang').val(this.pemegang);
								$('#beg_date').val(this.beg_date);
								$('#end_date').val(this.end_date);
								$('#tgl_aksep').val(this.tgl_aksep);
								$('#mh_alamat').val(this.mh_alamat);
								$('#mh_kodepos').val(this.mh_kodepos);
								$('#mh_kota').val(this.mh_kota);
								$('#mh_telepon').val(this.mh_telepon);
								
								$('input[name="btnTransfer"]').attr('disabled','disabled');
								$('input[name="btnPrintMemo"]').attr('disabled','disabled');
								$('input[name="btnCancel"]').attr('disabled','disabled');
								$('button[id="btnHistory"]').attr('disabled','disabled');								
							});
							
						}				
					});
				}else{
					//Data Sudah Ada				
					var url = "${path}/bas/hadiah.htm?window=json&t=polis2&spajPolis=" + val;
					$.getJSON(url, function(result) {
						if(result.length == 0){
							//Kolom Vendor dan Bank Kosong
							var url2 = "${path}/bas/hadiah.htm?window=json&t=polis3&spajPolis=" + val;
							$.getJSON(url2, function(result) {
								if(result.length == 0){
									alert('SPAJ/Polis tidak ditemukan. Harap cek ulang nomor yang dimasukkan');
								}else{
									$.each(result, function() {
									
										$('#reg_spaj').val(this.reg_spaj);
										$('#policy_no').val(this.policy_no);
										$('#pemegang').val(this.pemegang);
										$('#beg_date').val(this.beg_date);
										$('#end_date').val(this.end_date);
										$('#tgl_aksep').val(this.tgl_aksep);
										$('#mh_alamat').val(this.mh_alamat);
										$('#mh_kodepos').val(this.mh_kodepos);
										$('#mh_kota').val(this.mh_kota);
										$('#mh_telepon').val(this.mh_telepon);
										
										//tambahan
										
										$('input:radio[name=mh_flag_kirim]').val(this.mh_flag_kirim);
										var a = $('input:radio[name=mh_flag_kirim]').val();
										
										$('#mh_no').val(this.mh_no);
										$('#lhc_id').val(this.lhc_id);
										$('#lh_id').val(this.lh_id);				
										$('#mh_budget').val(this.mh_budget);
										$('#mh_harga').val(this.mh_harga);
										$('#mh_quantity').val(this.mh_quantity);
										$('#supplier_id').val(this.supplier_id);
										$('#lbn_id').val(this.lbn_id);
										$('#mh_rek_no').val(this.mh_rek_no);
										$('#mh_rek_nama').val(this.mh_rek_nama);
										$('#lspd_id').val(this.lspd_id);
										$('#mh_keterangan').val(this.mh_keterangan);
										$('#bank_name').val(this.bank_name);
										$('#supplier_name').val(this.supplier_name);
										$('#lh_nama').val(this.lh_nama);
										
										$("#reg_spaj, #policy_no").removeClass(); 
										$("#reg_spaj, #policy_no").addClass("ui-state-default");
										$("#reg_spaj, #policy_no").attr('readonly', true);
										 								
		 								if(a==0){
		 									$('input:radio[name=mh_flag_kirim]')[0].checked = true;
		 								}else{
		 									$('input:radio[name=mh_flag_kirim]')[1].checked = true;
		 								}
		 								
		 								//yourNameFor_doesFileExist("D:\\tes.txt");								    
										
										var lspd = $('#lspd_id').val();
										
										if(lspd==84){
											//$('input[name="btnTransfer"]').attr('disabled','disabled');
										}else if(lspd==85){
											$('input[name="btnTransfer"]').attr('disabled','disabled');
											$('input[name="btnPrintMemo"]').attr('disabled','disabled');
										}else if(lspd==86){
											$('input[name="btnSave"]').attr('disabled','disabled');
											$('input[name="btnTransfer"]').attr('disabled','disabled');
											$('input[name="btnPrintMemo"]').attr('disabled','disabled');
										}else{
											$('input[name="btnSave"]').attr('disabled','disabled');
											$('input[name="btnCancel"]').attr('disabled','disabled');
											$('input[name="btnTransfer"]').attr('disabled','disabled');
											$('input[name="btnPrintMemo"]').attr('disabled','disabled');
										}
										
									});
								}				
							});
						}else{
							$.each(result, function() {
							
								$('#reg_spaj').val(this.reg_spaj);
								$('#policy_no').val(this.policy_no);
								$('#pemegang').val(this.pemegang);
								$('#beg_date').val(this.beg_date);
								$('#end_date').val(this.end_date);
								$('#tgl_aksep').val(this.tgl_aksep);
								$('#mh_alamat').val(this.mh_alamat);
								$('#mh_kodepos').val(this.mh_kodepos);
								$('#mh_kota').val(this.mh_kota);
								$('#mh_telepon').val(this.mh_telepon);
								
								//tambahan
								
								$('input:radio[name=mh_flag_kirim]').val(this.mh_flag_kirim);
								var a = $('input:radio[name=mh_flag_kirim]').val();
								
								$('#mh_no').val(this.mh_no);
								$('#lhc_id').val(this.lhc_id);
								$('#lh_id').val(this.lh_id);				
								$('#mh_budget').val(this.mh_budget);
								$('#mh_harga').val(this.mh_harga);
								$('#mh_quantity').val(this.mh_quantity);
								$('#supplier_id').val(this.supplier_id);
								$('#lbn_id').val(this.lbn_id);
								$('#mh_rek_no').val(this.mh_rek_no);
								$('#mh_rek_nama').val(this.mh_rek_nama);
								$('#lspd_id').val(this.lspd_id);
								$('#mh_keterangan').val(this.mh_keterangan);
								$('#bank_name').val(this.bank_name);
								$('#supplier_name').val(this.supplier_name);
								$('#lh_nama').val(this.lh_nama);
								
								$("#reg_spaj, #policy_no").removeClass(); 
								$("#reg_spaj, #policy_no").addClass("ui-state-default");
								$("#reg_spaj, #policy_no").attr('readonly', true);
								 								
 								if(a==0){
 									$('input:radio[name=mh_flag_kirim]')[0].checked = true;
 								}else{
 									$('input:radio[name=mh_flag_kirim]')[1].checked = true;
 								}
 								
 								//yourNameFor_doesFileExist("D:\\tes.txt");								    
								
								var lspd = $('#lspd_id').val();
								
								if(lspd==84){
									//$('input[name="btnTransfer"]').attr('disabled','disabled');
								}else if(lspd==85){
									$('input[name="btnTransfer"]').attr('disabled','disabled');
									$('input[name="btnPrintMemo"]').attr('disabled','disabled');
								}else if(lspd==86){
									$('input[name="btnSave"]').attr('disabled','disabled');
									$('input[name="btnTransfer"]').attr('disabled','disabled');
									$('input[name="btnPrintMemo"]').attr('disabled','disabled');
								}else{
									$('input[name="btnSave"]').attr('disabled','disabled');
									$('input[name="btnCancel"]').attr('disabled','disabled');
									$('input[name="btnTransfer"]').attr('disabled','disabled');
									$('input[name="btnPrintMemo"]').attr('disabled','disabled');
								}
								
							});
						}				
					});
				}
			});
			
			//
			var url84 = "${path}/bas/hadiah.htm?window=json&t=tglHistory&reg_spaj=" +  $("#reg_spaj").val();
			$.getJSON(url84, function(result) {
				if(result.length == 0){
					//alert('SPAJ/Polis tidak ditemukan. Harap cek ulang nomor yang dimasukkan');
				}else{
					$.each(result, function() {
						$('#mh_tgl_input').val(this.MH_TGL_INPUT);
						$('#create_id').val(this.USERNAME);
						$('#mh_tgl_aksep').val(this.MH_TGL_AKSEP);
						$('#mh_tgl_paid').val(this.MH_TGL_PAID);
						$('#mh_tgl_kirim_vendor').val(this.MH_TGL_KIRIM_VENDOR);
						$('#mh_tgl_terima_ajs').val(this.MH_TGL_TERIMA_AJS);
						$('#mh_tgl_kirim_ajs').val(this.MH_TGL_KIRIM_AJS);
						$('#mh_tgl_terima_nsbh').val(this.MH_TGL_TERIMA_NSBH);
					});
					
				}				

			});
			//
		});

		// validation vendor
		$('#supplier_name').change(function() {
			
			var url = "${path}/bas/hadiah.htm?window=json&t=vendor&vendorname=" + this.value;
			$.getJSON(url, function(result) {
				if(result.length == 0){
					alert('Vendor tidak ditemukan. Harap cek ulang Vendor');
					$('#supplier_name').val("");
				}else{
					$.each(result, function() {
						$('#supplier_name').val(this.supplier_name);
					});
				}				
			});
			
		});
		
		// validation bank
		$('#bank_name').change(function() {
			
			var url = "${path}/bas/hadiah.htm?window=json&t=val_bank&bankname=" + this.value;
			$.getJSON(url, function(result) {
				if(result.length == 0){
					alert('Bank tidak ditemukan. Harap cek ulang Bank');
					$('#bank_name').val("");
				}else{
					$.each(result, function() {
						$('#bank_name').val(this.bank_name);
					});
				}				
			});
			
		});
		
		//autocomplete kota
		$( "#mh_kota" ).autocomplete({
			minLength: 3,
			source: "${path}/bas/hadiah.htm?window=json&t=kota",
			focus: function( event, ui ) {
				$( "#mh_kota" ).val( ui.item.key );
				return false;
			},
			select: function( event, ui ) {
				$( "#mh_kota" ).val( ui.item.key);
				return false;
			}
		})
		.data( "autocomplete" )._renderItem = function( ul, item ) {
			return $( "<li></li>" )
				.data( "item.autocomplete", item )
				.append( "<a>" + item.key + "</a>" )
				.appendTo( ul );
		};		
		
		
		//autocomplete vendor
		$( "#supplier_name" ).autocomplete({
			minLength: 3,
			source: "${path}/bas/hadiah.htm?window=json&t=supplier",
			focus: function( event, ui ) {
				$( "#supplier_name" ).val( ui.item.SUPPLIER_NAME );
				return false;
			},
			select: function( event, ui ) {
				$( "#supplier_name" ).val( ui.item.SUPPLIER_NAME );
				$( "#supplier_id" ).val( ui.item.SUPPLIER_ID);
				return false;
			}
		})
		.data( "autocomplete" )._renderItem = function( ul, item ) {
			return $( "<li></li>" )
				.data( "item.autocomplete", item )
				.append( "<a>" 	+ 
					"<strong>Nama: </strong>" + item.SUPPLIER_NAME + " (" + item.DIST_NAME + ")<br>" + 
					"<strong>Alamat: </strong>" + item.ADDRESS + " (" + item.CITY + ")<br>" + 
					"<strong>Telepon: </strong>" + item.PHONE + " / " + item.FAX + "</a>" )
				.appendTo( ul );
		};		

		//autocomplete bank
		$( "#bank_name" ).autocomplete({
			minLength: 3,
			source: "${path}/bas/hadiah.htm?window=json&t=bank",
			focus: function( event, ui ) {
				$( "#bank_name" ).val( ui.item.BANK_NAMA );
				return false;
			},
			select: function( event, ui ) {
				$( "#bank_name" ).val( ui.item.BANK_NAMA );
				$( "#lbn_id" ).val( ui.item.BANK_ID);
				return false;
			}
		})
		.data( "autocomplete" )._renderItem = function( ul, item ) {
			return $( "<li></li>" )
				.data( "item.autocomplete", item )
				.append( "<a>" + item.BANK_NAMA + "</a>" )
				.appendTo( ul );
		};		
		
		//Cancel Hadiah
		$("#btnCancel").click(function(){
		
			var spaj = $('#reg_spaj').val();
			/*if(spaj==""){
				alert('Mohon isi nomor SPAJ');
			}else{
				cancel();
			}*/
			
			var res = confirm("Cancel hadiah untuk no. spaj "+spaj+"?");

			if(res){
				return true;
			}else{
				return false;
			}
		});
		
		//Transfer Hadiah
		$("#btnTransfer").click(function(){
		
			var spaj = $('#reg_spaj').val();
			/*if(spaj==""){
				alert('Mohon isi nomor SPAJ');
			}else{
				cancel();
			}*/
			
			var tr = confirm("Transfer dokumen untuk no. spaj "+spaj+"?");

			if(tr){
				return true;
			}else{
				return false;
			}
		});		
		
		// user menekan tombol View History, ada 2 step (pertama populate tabel dulu, kedua tampilkan dalam bentuk modal window)
		$("#btnHistory").click(function(){
			//tarik history
			$("#histBody").empty();
			var url3 = "${path}/bas/hadiah.htm?window=json&t=hist&reg_spaj=" + $("#reg_spaj").val() + "&mh_no=" + $("#mh_no").val();
			$.getJSON(url3, function(result) {
				var hit = 0;
				$.each(result, function() {
					$('#histTable > tbody:last').append(
							'<tr><td>' + (++hit) + 
							'</td><td>' + this.TGL + 
							'</td><td>' + this.USERNAME + 
							'</td><td>' + this.POSISI + 
							'</td><td>' + this.KETERANGAN + '</td></tr>');
				});
			});				
		});		
		$("#btnHistory").qtip({
			style: {
				classes: 'ui-tooltip-light ui-tooltip-rounded',
				width: 800
			},
			content: {
    			text: $('#historyHadiah'),
    			title: {
       				text: 'History',
       				button: true}
			},
			position: {
				my: 'center', // ...at the center of the viewport
				at: 'center',
				target: $(window),
				adjust: {scroll: false}
			},
			show: {
				event: 'click', // Show it on click...
				solo: true, // ...and hide all other tooltips...
				modal: true // ...and make it modal
			},
			hide: false
		});
		
		//bila ada pesan
		var pesan = '${command.pesan}';
		if(pesan != '') alert(pesan);
		
		//onload, bila sudah ada datanya, spaj dan polis tidak bisa dirubah lagi
		if($("#reg_spaj").val() != '') {
			$("#reg_spaj").change();
			
			$("#reg_spaj, #policy_no").removeClass(); 
			$("#reg_spaj, #policy_no").addClass("ui-state-default");
			$("#reg_spaj, #policy_no").attr('readonly', true);
		}
		
		function gup( name )
		{
		  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
		  var regexS = "[\\?&]"+name+"=([^&#]*)";
		  var regex = new RegExp( regexS );
		  var results = regex.exec( window.location.href );
		  if( results == null )
		    return "";
		  else
		    return results[1];
		}	
		var a = gup('spaj');
		$('#reg_spaj').val(a);
		//alert('ok '+a);
	});
	
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 23em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 23.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 23em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

	/* untuk tombol2 kecil di report */
	ul#icons {margin: 0; padding: 0;}
	ul#icons li {margin: 0 1px 0 0; position: relative; padding: 0px; cursor: pointer; float: left;  list-style: none;}
	ul#icons span.ui-icon {float: left; margin: 0px;}

	/* styling untuk server-side validation error */
	.errorField { border: 1px solid red; }
	.errorMessage { color: red; display: block;}
	
	/* styling untuk table input */
	table.inputTable{
		width: 700px;
	}
	table.inputTable tr th{
		width: 200px;
	}
	
	/* styling untuk table info tanggal */
	div#infoTanggal{
		text-align: center;
		background-color: white;
		width: 345px;
		padding: 8px;
		border: 1px solid gray;
		position: fixed;
		left: 580px;
		top: 50px;
	}

</style>

<body>

	<form:form method="post" name="formpost" id="formpost" commandName="command" action="${path}/bas/hadiah.htm?window=input">
	<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>Input Hadiah</div></legend>
		
		<table class="inputTable">
			<tr>
				<th>Nomor SPAJ:<em>*</em></th>
				<td>
					<form:input maxlength="11" title="Nomor SPAJ" path="reg_spaj" cssErrorClass="errorField"/><form:errors path="reg_spaj" cssClass="errorMessage"/>
					<form:hidden path="mh_no"/>
					<form:hidden path="lspd_id"/>
					<form:hidden path="lh_nama"/>
					<input type="submit" name="btnCek" id="btnCek" title="Cek SPAJ" value="Cek">
				</td>
			</tr>
			<tr>
				<th>Nomor Polis:<em>*</em></th>
				<td><form:input maxlength="14" title="Nomor Polis" path="policy_no" cssErrorClass="errorField"/><form:errors path="policy_no" cssClass="errorMessage"/></td>
			</tr>
			<tr>
				<th>Pemegang Polis:</th>
				<td><form:input title="Nama Pemegang Polis" path="pemegang" cssClass="ui-state-default" readonly="true"/></td>
			</tr>
			<tr>
				<th>Periode Polis:</th>
				<td><form:input title="Tanggal Mulai Berlaku Polis" path="beg_date" cssClass="ui-state-default" readonly="true"/> s/d 
				<form:input title="Tanggal Akhir Berlaku Polis" path="end_date" cssClass="ui-state-default" readonly="true"/></td>
			</tr>
			<tr>
				<th>Tgl Aksep UW:</th>
				<td><form:input title="Tanggal Akseptasi Underwriting" path="tgl_aksep" cssClass="ui-state-default" readonly="true"/></td>
			</tr>
			<tr>
				<td colspan="2"><hr></td>
			</tr>
			<tr>
				<th>Kategori Unit Hadiah:<em>*</em></th>
				<td>
					<form:select title="Kategori Unit Hadiah" path="lhc_id" cssErrorClass="errorField">
						<form:option value="" label=""/>
						<form:options items="${listHC}" itemLabel="value" itemValue="key"/>
					</form:select>
					<form:errors path="lhc_id" cssClass="errorMessage"/>
				</td>
			</tr>
			<tr>
				<th>Nama Unit Hadiah:<em>*</em></th>
				<td>
					<form:select title="Nama Unit Hadiah" path="lh_id" cssErrorClass="errorField">
						<form:option value="" label="" />
						<form:options items="${listH}" itemLabel="value" itemValue="key"/>
					</form:select>
					<form:errors path="lh_id" cssClass="errorMessage"/>
				</td>
			</tr>
			<tr>
				<th>Jumlah Pembelian Unit:<em>*</em></th>
				<td><form:input maxlength="2" title="Jumlah Pembelian Unit" path="mh_quantity" cssErrorClass="errorField"/><form:errors path="mh_quantity" cssClass="errorMessage"/></td>
			</tr>
			<tr>
				<th>Budget Pembelian Unit:<em>*</em></th>
				<td><form:input title="Budget Pembelian Unit" path="mh_budget" cssErrorClass="errorField"/><form:errors path="mh_budget" cssClass="errorMessage"/></td>
			</tr>
			<tr>
				<th>Harga Unit:</th>
				<td><form:input title="Harga Unit" path="mh_harga" cssClass="ui-state-default" readonly="true"/></td>
			</tr>
			<tr>
				<td colspan="2"><hr></td>
			</tr>
			<tr>
				<th>Jenis Pengiriman:<em>*</em></th>
				<td>
					<form:radiobutton id="flag_kirim1" path="mh_flag_kirim" value="0"/><form:label title="Barang langsung dikirim dari Vendor ke Nasabah" for="flag_kirim1" path="mh_flag_kirim">Langsung ke Nasabah</form:label>
					<form:radiobutton id="flag_kirim2" path="mh_flag_kirim" value="1"/><form:label title="Barang dikirim ke AJS MSIG dahulu baru ke Nasabah" for="flag_kirim2" path="mh_flag_kirim">Via AJS MSIG</form:label>
				</td>
			</tr>
			<tr>
				<th>Alamat Nasabah:<em>*</em></th>
				<td>
					<form:textarea title="Alamat lengkap Nasabah" path="mh_alamat" cols="45" rows="3" cssErrorClass="errorField"/>
					<br/>
					Kota:<em>*</em> <form:input maxlength="30" title="Kota (Silahkan masukkan minimal 3 huruf untuk pencarian Nama Kota)" path="mh_kota" size="14" cssErrorClass="errorField"/>
					Kode Pos:<em>*</em> <form:input maxlength="5" title="Kode Pos" path="mh_kodepos" size="6" cssErrorClass="errorField"/>
					<form:errors path="mh_alamat" cssClass="errorMessage"/>
					<form:errors path="mh_kota" cssClass="errorMessage"/>
					<form:errors path="mh_kodepos" cssClass="errorMessage"/>
				</td>
			</tr>
			<tr>
				<th>Telepon:</th>
				<td><form:input maxlength="20" title="Nomor Telepon/Handphone yang Bisa Dihubungi" path="mh_telepon" size="14"/></td>
			</tr>
			<tr>
				<td colspan="2"><hr></td>
			</tr>
			<tr>
				<th>Vendor:<em>*</em></th>
				<td>
					<form:input path="supplier_name" title="Nama Vendor (Silahkan masukkan minimal 3 huruf untuk pencarian ID Vendor atau Nama Vendor)" cssClass="lebar" cssErrorClass="errorField"/>
					<form:hidden path="supplier_id"/>
					<form:errors path="supplier_name" cssClass="errorMessage"/>
				</td>
			</tr>
			<tr>
				<th>Bank Rekening Vendor:<em>*</em></th>
				<td>
					<form:input title="Bank Vendor (Silahkan masukkan minimal 3 huruf untuk pencarian Nama Bank)" path="bank_name" cssClass="lebar" cssErrorClass="errorField"/>
					<form:hidden path="lbn_id"/>
					<form:errors path="bank_name" cssClass="errorMessage"/>
				</td>
			</tr>
			<tr>
				<th>Nomor Rekening Vendor:</th>
				<td><form:input maxlength="50" title="Nomor Rekening Vendor" path="mh_rek_no" cssClass="lebar"/></td>
			</tr>
			<tr>
				<th>Atas Nama Rekening Vendor:</th>
				<td><form:input maxlength="100" title="Nama Pemegang Rekening Vendor" path="mh_rek_nama" cssClass="lebar"/></td>
			</tr>
			<tr>
				<td colspan="2"><hr></td>
			</tr>
			<tr>
				<th>Keterangan:</th>
				<td><form:textarea title="Keterangan Tambahan" path="mh_keterangan" cols="45" rows="3"/></td>
			</tr>
			<tr>
				<th></th>
				<td>
					<%
						String reg_spaj = request.getParameter("reg_spaj");
						String mh_no = request.getParameter("mh_no");
					%>
					<input type="submit" name="btnSave" id="btnSave" title="Simpan Input Hadiah" value="Simpan">
					<input type="button" name="btnReset" id="btnReset" title="Reset Input Hadiah" onClick="window.location='${path}/bas/hadiah.htm?window=input';" value="Reset">
					<input type="submit" name="btnCancel" id="btnCancel" title="Cancel Input Hadiah" value="Cancel">
					<input type="submit" name="btnPrintMemo" id="btnPrintMemo" title="Print Memo" value="Print">
					<input type="submit" name="btnTransfer" id="btnTransfer" title="Transfer ke Purchasing" value="Transfer">
					<button id="btnHistory" title="View History">View History</button>
					<!-- <button id="btnHist" title="View Hist" onClick="window.open('${path}/bas/hadiah.htm?window=history&spaj=<%=reg_spaj%>&no=<%=mh_no%>','History','width=700,height=450')">View Hist</button>-->
					
<!--					<input type="submit" name="btnTransfer" id="btnTransfer" title="Transfer ke Purchasing" value="Transfer">-->
<!--					<input type="submit" name="btnDelete" id="btnDelete" title="Hapus Input Hadiah" value="Hapus">-->
				</td>
			</tr>
		</table>

</fieldset>
	</form:form>
    
    <form:form method="post" name="formpost2" id="formpost2" commandName="command" action="${path}/bas/hadiah.htm?window=input">
	<fieldset class="ui-widget ui-widget-content">

		<div id="infoTanggal">
			<table class="jtable">
				<caption>Informasi Tanggal</caption>
				<tr>
					<th>Tanggal Input:</th>
					<td><form:input title="Tanggal Input Hadiah" path="mh_tgl_input" cssClass="ui-state-default" readonly="true"/></td>
				</tr>
				<tr>
					<th>User Input:</th>
					<td><form:input title="User Input Hadiah" path="create_id" cssClass="ui-state-default" readonly="true" size="15"/></td>
				</tr>
				<tr>
					<th>Tanggal Aksep:</th>
					<td><form:input title="Tanggal Akseptasi Pembelian Hadiah oleh Purchasing" path="mh_tgl_aksep" cssClass="ui-state-default" readonly="true"/></td>
				</tr>
				<tr>
					<th>Tanggal Paid:</th>
					<td><form:input title="Tanggal Paid oleh Finance" path="mh_tgl_paid" cssClass="ui-state-default" readonly="true"/></td>
				</tr>
				<tr>
					<th>Tanggal Kirim Vendor:</th>
					<td><form:input title="Tanggal Hadiah dikirim oleh Vendor" path="mh_tgl_kirim_vendor" cssClass="ui-state-default" readonly="true"/></td>
				</tr>
				<tr>
					<th>Tanggal Terima AJS MSIG:</th>
					<td><form:input title="Tanggal Hadiah Diterima oleh AJS MSIG (Hanya bila Jenis Pengiriman via AJS MSIG)" path="mh_tgl_terima_ajs" cssClass="ui-state-default" readonly="true"/></td>
				</tr>
				<tr>
					<th>Tanggal Kirim AJS MSIG:</th>
					<td><form:input title="Tanggal Hadiah dikirim oleh AJS MSIG (Hanya bila Jenis Pengiriman via AJS MSIG)" path="mh_tgl_kirim_ajs" cssClass="ui-state-default" readonly="true"/></td>
				</tr>
				<tr>
					<th>Tanggal Terima Nasabah:</th>
					<td><form:input title="Tanggal Hadiah Diterima oleh Nasabah" path="mh_tgl_terima_nsbh" cssClass="ui-state-default" readonly="true"/></td>
				</tr>
			</table>
			
			<br/>
			<table class="jtable">
				<caption>Informasi Posisi Saat Ini</caption>
				<c:set var="zzz" value="84"/>
				<c:forEach items="${listPosisi}" var="p">
					<tr>
						<td <c:if test="${command.lspd_id eq p.key}"> style="background: #ffc; color: red;"</c:if>>${p.key} ${p.value}</td>
					</tr>
				</c:forEach>
			</table>
			
			<br/>
			<!-- <button id="btnHistory" title="View History">View History</button>-->			
		</div>

	</fieldset>
	</form:form>
	
	<div id="historyHadiah" style="display: none; font-size: 1.2em;">
	<div style="overflow: auto; height: 400px;">
		<table id="histTable" class="jtable">
			<thead>
				<tr>
					<th>No.</th>
					<th>Tanggal</th>
					<th>User</th>
					<th>Posisi</th>
					<th>Keterangan</th>
				</tr>
			</thead>
			<tbody id="histBody">
			</tbody>
		</table>
	</div>
	</div>
	
</body>
</html>