<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script>
			function tampilkan(msf_id){	
				if(parent) {
					parent.document.getElementById('msf_id').value = msf_id;
					parent.document.getElementById('submitMode').value = 'show';
					parent.document.getElementById('formpost').submit();
				}
			}
		</script>
	</head>

	<body style="height: 100%;">

		<c:choose>
			<c:when test="${empty cmd.daftarForm}">
				<div class="tabcontent" style="border: none;">
					<div id="success" style="text-transform: none">
						${cmd.pesan}
					</div>
				</div>
			</c:when>
			<c:otherwise>
				<table class="displaytag">
					<thead>
						<th>Cabang</th>
						<th>Nomor</th>
						<th>Status</th>
					</thead>
					<tbody>
						<c:forEach items="${cmd.daftarForm}" var="f">
							<tr style="cursor: pointer; background-color: ${f.warna};" onclick="tampilkan('${f.msf_id}');">
								<td style="text-align: center; white-space: nowrap">${f.lca_nama}</td>
								<td style="text-align: center; white-space: nowrap">${f.msf_id}</td>
								<td style="text-align: center; white-space: nowrap">${f.status_form}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</c:otherwise>
		</c:choose>

	</body>
</html>