<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<script>
			function tampilkan(btn){

				if(btn.name=='simpan') {
					if(!confirm('Simpan Perubahan?')) {
						return false;
					}
				} else if(btn.name=='batal') {
						if(!confirm('Batalkan Permintaan?')) {
							return false;
						}
					}
			 
					
				document.getElementById('submitMode').value = btn.name;
				btn.disabled=true;
				document.getElementById('formpost').submit();
			}
			
			hideLoadingMessage();
		</script>
	</head>

	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Form Pertanggungjawaban KERTAS POLIS</a>
				</li>
			</ul>
	
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form:form commandName="cmd" id="formpost" method="post">
						<form:hidden id="submitMode" path="submitMode"/>
						<table class="entry2">
							<tr>
								<td style="vertical-align: top;">
									<fieldset>
										<legend>Jumlah KERTAS POLIS di Cabang yang tercatat saat ini</legend>

										<table class="displaytag" style="width:auto;">
											<thead>
												<tr>
													<c:forEach var="s" items="${cmd.daftarStokSpaj}">
														<th style="width: 50px; " title="${s.lsjs_desc}">${s.lsjs_prefix}</th>
														<th style="width: 50px; " title="${s.damage}">RUSAK</th>
													</c:forEach>
												</tr>
											</thead>
											<tbody>
												<tr>
													<c:forEach var="s" items="${cmd.daftarStokSpaj}">
														<td title="${s.lsjs_desc}">${s.mss_amount}</td>
														<td  title="${s.damage}">${s.damage}</td>
													</c:forEach>
												</tr>
											</tbody>
										</table>

										<br/>
										
									</fieldset>									
									<fieldset>
										<legend>Form Pertanggungjawaban KERTAS POLIS ke Kantor Pusat</legend>
											<table class="entry2">
																	<tr>
														<th>Cabang</th>
														<td>
															<input type="text" class="readOnly" readonly="true" size="40" value="${cmd.currentUser.cabang}" />
														</td>
													</tr>
													<tr>
														<th>Penanggungjawab</th>
														<td>
															<input type="text" class="readOnly" readonly="true" size="40" value="${cmd.currentUser.name}"  />
														</td>
													</tr>
													<tr>
														<th>Detail Pertanggungjawaban</th>
														<td>
															<table class="displaytag" style="width:auto;">
															<c:forEach var="s" items="${cmd.daftarStokSpaj}" varStatus = "i">
																<thead>
																	<tr>
																		<th rowspan="2">Jenis</th>
																		<th rowspan="2">Jumlah KERTAS POLIS Rusak</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td style="text-align: left;">${s.lsjs_desc}</td>
																		<td style="text-align: center;">
																			<spring:bind path="cmd.daftarStokSpaj[${i.index}].damage">
																			<input size="30" type="text" name="${status.expression }" value="${status.value}">
																		</spring:bind> 
																</td>
																	</tr>
																</tbody>
															</c:forEach>
															</table>
														</td>
													</tr>
													<tr>
														<th></th>
														<td>
															<input type="button" value="Simpan" name="simpan" onclick="return tampilkan(this);">
															<input type="button" value="Batalkan" name="batal" onclick="return tampilkan(this);">
															<input type="button" value="Laporan Pemakaian" name="laporan" onclick="popWin('${path}/blanko/blanko.htm?window=lappemakaian', 550, 750); ">
															<c:choose>
																<c:when test="${not empty infoMessage }"><div id="success" style="text-transform: none;">${infoMessage}</div></c:when>
																<c:otherwise>
																	<spring:hasBindErrors name="cmd">
																		<div id="error" style="text-transform: none;">
																			<form:errors path="*"/>
																		</div>
																	</spring:hasBindErrors>
																</c:otherwise>
															</c:choose>
														</td>
													</tr>
												</table>											

									</fieldset>									
								</td>
							</tr>
						</table>
					</form:form>
				</div>
				
			</div>
			
		</div>

	</body>
	<script>
		<c:if test="${not empty param.sukses}">
			alert('${param.sukses}');
		</c:if>
	</script>
</html>