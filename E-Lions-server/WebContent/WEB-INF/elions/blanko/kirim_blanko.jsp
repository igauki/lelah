<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<script>
			function tampilkan(btn){

				if(btn.name=='send') {
					if(!confirm('Dengan menekan tombol kirim, maka cabang akan diinformasikan bahwa KERTAS POLIS sedang dalam pengiriman. Lanjutkan?')) {
						return false;
					}

				} else if(btn.name=='show') {
					var lca_id = document.getElementById('lca_id').value;
					if(lca_id=='') {
						alert('Silahkan pilih salah satu cabang terlebih dahulu');				
					}else{
						document.getElementById('formFrame').src = '${path}/blanko/blanko.htm?window=daftarForm&posisi=1&lca_id=' + lca_id;
					}
					return false;
				}
					
				document.getElementById('submitMode').value = btn.name;
				btn.disabled=true;
				document.getElementById('formpost').submit();
			}
			
			hideLoadingMessage();
		</script>
	</head>

	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Pengiriman Permintaan KERTAS POLIS</a>
					<a href="#" onClick="return showPane('pane2', this)" id="tab2">History Posisi</a>
					<a href="#" onClick="return showPane('pane3', this)" id="tab3">Legend</a>
				</li>
			</ul>
	
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form:form commandName="cmd" id="formpost" method="post">
						<form:hidden id="submitMode" path="submitMode"/>
						<table class="entry2">
							<tr>
								<th style="width: 150px;">
									<select id="lca_id" name="lca_id" size="${sessionScope.currentUser.comboBoxSize}
											" style="width: 140px;" onclick="document.getElementById('show').click();">
										<c:forEach var="d" items="${cmd.daftarCabang}">
											<option value="${d.KEY}" 
												<c:if test="${d.KEY eq cmd.lca_id}"> selected </c:if>
											>${d.VALUE}</option>
										</c:forEach>
									</select>
									<input type="button" id="show" name="show" value="Show" style="width: 55px;" onclick="return tampilkan(this);">
								</th>
								<td style="vertical-align: top;">								
									<fieldset>
										<legend>Daftar Permintaan KERTAS POLIS</legend>
										<iframe id="formFrame" src="${path}/blanko/blanko.htm?window=daftarForm&posisi=1&lca_id=${cmd.lca_id}"></iframe>
									</fieldset>
									<fieldset>
										<legend>Pengiriman Permintaan KERTAS POLIS</legend>
										
										<table class="entry2">
											<tr>
												<th>Jumlah KERTAS POLIS di Cabang</th>
												<td>
													<table class="displaytag" style="width:auto;">
														<thead>
															<tr>
																<c:forEach var="s" items="${cmd.daftarStokSpaj}">
																	<th style="width: 50px; " title="${s.lsjs_desc}">${s.lsjs_prefix}</th>
																</c:forEach>
															</tr>
														</thead>
														<tbody>
															<tr>
																<c:forEach var="s" items="${cmd.daftarStokSpaj}">
																	<td title="${s.lsjs_desc}">${s.mss_amount}</td>
																</c:forEach>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<th>Nomor Form</th>
												<td>
													<form:input id="msf_id" path="msf_id" cssClass="readOnly" readonly="true" size="20"/>												
												</td>
											</tr>
											<c:if test="${not empty cmd.daftarFormSpaj}">
												<tr>
													<th>Status</th>
													<td>
														<input type="text" value="${cmd.daftarFormSpaj[0].status_form}" readonly="true" size="20" value="${cmd.daftarFormSpaj[0].status_form}" style="background-color: ${cmd.daftarFormSpaj[0].warna};">
													</td>
												</tr>
												<tr>
													<th>Cabang</th>
													<td>
														<input type="text" class="readOnly" readonly="true" size="40" value="${cmd.daftarFormSpaj[0].lca_nama}"/>
													</td>
												</tr>
												<tr>
													<th>User</th>
													<td>
														<input type="text" class="readOnly" readonly="true" size="40" value="${sessionScope.currentUser.name}"/>
													</td>
												</tr>
												<tr>
													<th>Detail Permintaan</th>
													<td>
														<table class="displaytag" style="width:auto;">
															<thead>
																<tr>
																	<th rowspan="2">No.</th>
																	<th rowspan="2">Jenis KERTAS POLIS</th>
																	<th rowspan="2">Prefix</th>
																	<th colspan="2">Jumlah KERTAS POLIS</th>
																	<th rowspan="2">Status</th>
																</tr>
																<tr>
																	<th>Diminta</th>
																	<th>Disetujui</th>
																</tr>
															</thead>
															<tbody>
															<c:forEach var="daftar" items="${cmd.daftarFormSpaj}" varStatus="st">
																<tr>
																	<td style="text-align: left;">${st.count}.</td>
																	<td style="text-align: left;">${daftar.lsjs_desc}</td>
																	<td style="text-align: center;">${daftar.lsjs_prefix}</td>
																	<td style="text-align: center;">
																		<form:input path="daftarFormSpaj[${st.index}].msf_amount_req" size="10" cssStyle="text-align: right;" cssClass="readOnly" readonly="true"/>
																	</td>
																	<td style="text-align: center;">
																		<form:input path="daftarFormSpaj[${st.index}].msf_amount" size="10" cssStyle="text-align: right;" onfocus="this.select();"/>
																		<span class="error">*</span>
																	</td>
																	<td style="text-align: center;">${daftar.status_form}</td>
																</tr>
															</c:forEach>
															</tbody>
														</table>
													</td>
												</tr>
												<tr>
													<th>Keterangan</th>
													<td>
														<form:textarea path="formHist.msfh_desc" cols="70" rows="2" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "/>
														<span class="error">*</span>
													</td>
												</tr>
												<tr>
													<th>Kirim</th>
													<td>
														<input type="button" value="Kirim" name="send" onclick="return tampilkan(this);">
														<c:choose>
															<c:when test="${not empty infoMessage }"><div id="success" style="text-transform: none;">${infoMessage}</div></c:when>
															<c:otherwise>
																<spring:hasBindErrors name="cmd">
																	<div id="error" style="text-transform: none;">
																		<form:errors path="*"/>
																	</div>
																</spring:hasBindErrors>
															</c:otherwise>
														</c:choose>
													</td>
												</tr>
											</c:if>
										</table>								
									</fieldset>
								</td>
							</tr>
						</table>
					</form:form>
				</div>
				<div id="pane2" class="panes">
					<fieldset>
						<legend>History Permintaan KERTAS POLIS</legend>
						<display:table id="detail" name="cmd.daftarFormHistory" class="displaytag">
							<display:column property="msf_urut" title="No." style="text-align: left;"/>
							<display:column property="msf_id" title="No. Form" style="text-align: center;" />
							<display:column property="status_form" title="Posisi" style="text-align: center;" />
							<display:column property="lus_login_name" title="User" style="text-align: center;" />
							<display:column property="msfh_dt" title="Tanggal" format="{0, date, dd/MM/yyyy (hh:mm)}" style="text-align: center;" />
							<display:column property="msfh_desc" title="Keterangan" style="text-align: center;" />
						</display:table>
					</fieldset>
				</div>	
				<div id="pane3" class="panes">
					<fieldset>
						<legend>Legend</legend>
						<table class="entry2">
							<tr>
								<th>Status Permintaan</th>
								<td>

									<c:forEach var="warna" items="${cmd.daftarWarna}" varStatus="st">
										<c:if test="${st.count ne 1}">
										&nbsp;
										<img src="${path}/include/image/arrow.gif">
										&nbsp;
										</c:if>
										<input type="button" value="${warna.key}" style="background-color: ${warna.value}; ">
									</c:forEach>
								</td>
							</tr>
						</table>
					</fieldset>
				</div>													
			</div>
		</div>
	</body>
	<script>
		<c:if test="${not empty param.sukses}">
			alert('${param.sukses}');
		</c:if>
	</script>
</html>