<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<script>
			function tampilkan(btn){

				if(btn.name=='save') {
					if(!confirm('Simpan Perubahan?')) {
						return false;
					}
				} else if(btn.name=='cancel') {
					if(!confirm('Batalkan Permintaan?')) {
						return false;
					}
				} else if(btn.name=='received') {
					if(!confirm('Tandai permintaan sebagai RECEIVED?\nJumlah KERTAS POLIS akan bertambah secara otomatis di sistem. Lanjutkan?')) {
						return false;
					}
				} else if(btn.name=='show') {
					if(document.getElementById('msf_id').value=='') {
						alert('Silahkan pilih salah satu nomor terlebih dahulu');				
						return false;
					}
				}
					
				document.getElementById('submitMode').value = btn.name;
				btn.disabled=true;
				document.getElementById('formpost').submit();
			}
			
			hideLoadingMessage();
		</script>
	</head>

	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Form Permintaan KERTAS POLIS</a>
					<a href="#" onClick="return showPane('pane2', this)" id="tab2">History Posisi</a>
					<a href="#" onClick="return showPane('pane3', this)" id="tab3">Legend</a>
				</li>
			</ul>
	
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form:form commandName="cmd" id="formpost" method="post">
						<form:hidden id="submitMode" path="submitMode"/>
						<table class="entry2">
							<tr>
								<th style="width: 120px;">
									<select id="msf_id" name="msf_id" size="${sessionScope.currentUser.comboBoxSize}" style="width: 110px;" onclick="document.getElementById('show').click();">
										<c:forEach var="d" items="${cmd.daftarNomorFormSpaj}">
											<option value="${d.msf_id}" style="background-color: ${d.warna};"
												<c:if test="${d.msf_id eq cmd.msf_id}"> selected </c:if>
											>${d.msf_id}</option>
										</c:forEach>
									</select>
									<input type="button" id="show" name="show" value="Show" style="width: 55px;" onclick="return tampilkan(this);">
									<input type="button" name="new" value="New" style="width: 55px;" onclick="return tampilkan(this);">
								</th>
								<td style="vertical-align: top;">
									<fieldset>
										<legend>Jumlah KERTAS POLIS di Cabang yang tercatat saat ini</legend>

										<table class="displaytag" style="width:auto;">
											<thead>
												<tr>
													<c:forEach var="s" items="${cmd.daftarStokSpaj}">
														<th style="width: 50px; " title="${s.lsjs_desc}">${s.lsjs_prefix}</th>
													</c:forEach>
												</tr>
											</thead>
											<tbody>
												<tr>
													<c:forEach var="s" items="${cmd.daftarStokSpaj}">
														<td title="${s.lsjs_desc}">${s.mss_amount}</td>
													</c:forEach>
												</tr>
											</tbody>
										</table>

										<br/>
										
									</fieldset>									
									<fieldset>
										<legend>Form Permintaan KERTAS POLIS ke Kantor Pusat</legend>
										<c:choose>
											<c:when test="${not empty cmd.daftarFormSpaj}">
												<table class="entry2">
													<tr>
														<th>Nomor Form</th>
														<td>
															<form:hidden path="daftarFormSpaj[0].status_permintaan" />
															<form:input path="daftarFormSpaj[0].msf_id" cssClass="readOnly" readonly="true" size="20"/>
														</td>
													</tr>
													<tr>
														<th>Status</th>
														<td>
															<input type="text" value="${cmd.daftarFormSpaj[0].status_form}" readonly="true" size="20" value="${cmd.daftarFormSpaj[0].status_form}" style="background-color: ${cmd.daftarFormSpaj[0].warna};">
														</td>
													</tr>
													<tr>
														<th>Cabang</th>
														<td>
															<input type="text" class="readOnly" readonly="true" size="40" value="${cmd.daftarFormSpaj[0].lca_nama}"/>
														</td>
													</tr>
													<tr>
														<th>Pemohon</th>
														<td>
															<input type="text" class="readOnly" readonly="true" size="40" value="${cmd.formHistUser.lus_login_name}"/>
														</td>
													</tr>
													<tr>
														<th>Detail Permintaan</th>
														<td>
															<table class="displaytag" style="width:auto;">
																<thead>
																	<tr>
																		<th rowspan="2">No.</th>
																		<th rowspan="2">Jenis</th>
																		<th rowspan="2">Prefix</th>
																		<th colspan="2">Jumlah KERTAS POLIS</th>
																		<th rowspan="2">Status</th>
																	</tr>
																	<tr>
																		<th>Diminta</th>
																		<th>Disetujui</th>
																	</tr>
																</thead>
																<tbody>
																<c:forEach var="daftar" items="${cmd.daftarFormSpaj}" varStatus="st">
																	<tr>
																		<td style="text-align: left;">${st.count}.</td>
																		<td style="text-align: left;">${daftar.lsjs_desc}</td>
																		<td style="text-align: center;">${daftar.lsjs_prefix}</td>
																		<td style="text-align: center;">
																			<form:input path="daftarFormSpaj[${st.index}].msf_amount_req" size="10" cssStyle="text-align: right;" onfocus="this.select();"/>
																			<span class="error">*</span>
																		</td>
																		<td style="text-align: center;">
																			<form:input path="daftarFormSpaj[${st.index}].msf_amount" size="10" cssStyle="text-align: right;" cssClass="readOnly" readonly="true"/>
																		</td>
																		<td style="text-align: center;">${daftar.status_form}</td>
																	</tr>
																</c:forEach>
																</tbody>
															</table>
														</td>
													</tr>
													<tr>
														<th></th>
														<td>
															<input type="button" value="Simpan" name="save" onclick="return tampilkan(this);"
																<c:if test="${cmd.daftarFormSpaj[0].posisi ne 0}">disabled</c:if>>
															<input type="button" value="Batalkan" name="cancel" onclick="return tampilkan(this);"
																<c:if test="${cmd.daftarFormSpaj[0].posisi ne 0 and cmd.daftarFormSpaj[0].posisi ne 1}">disabled</c:if>>
															
															<input type="button" value="Telah Diterima" name="received" onclick="return tampilkan(this);"
																<c:if test="${cmd.daftarFormSpaj[0].posisi ne 4}">disabled</c:if>>
															<c:choose>
																<c:when test="${not empty infoMessage }"><div id="success" style="text-transform: none;">${infoMessage}</div></c:when>
																<c:otherwise>
																	<spring:hasBindErrors name="cmd">
																		<div id="error" style="text-transform: none;">
																			<form:errors path="*"/>
																		</div>
																	</spring:hasBindErrors>
																</c:otherwise>
															</c:choose>
														</td>
													</tr>
												</table>											
											</c:when>
											<c:otherwise>
												<div id="success">
													Silahkan pilih tombol SHOW atau NEW di sebelah kiri
												</div>
											</c:otherwise>
										</c:choose>
										
									</fieldset>									
								</td>
							</tr>
						</table>
					</form:form>
				</div>
				<div id="pane2" class="panes">
					<fieldset>
						<legend>History Permintaan KERTAS POLIS</legend>
						<display:table id="detail" name="cmd.daftarFormHistory" class="displaytag">
							<display:column property="msf_urut" title="No." style="text-align: left;"/>
							<display:column property="msf_id" title="No. Form" style="text-align: center;" />
							<display:column property="status_form" title="Posisi" style="text-align: center;" />
							<display:column property="lus_login_name" title="User" style="text-align: center;" />
							<display:column property="msfh_dt" title="Tanggal" format="{0, date, dd/MM/yyyy (hh:mm)}" style="text-align: center;" />
							<display:column property="msfh_desc" title="Keterangan" style="text-align: center;" />
						</display:table>
					</fieldset>
				</div>
				<div id="pane3" class="panes">
					<fieldset>
						<legend>Legend</legend>
						<table class="entry2">
							<tr>
								<th>Status Permintaan</th>
								<td>

									<c:forEach var="warna" items="${cmd.daftarWarna}" varStatus="st">
										<c:if test="${st.count ne 1}">
										&nbsp;
										<img src="${path}/include/image/arrow.gif">
										&nbsp;
										</c:if>
										<input type="button" value="${warna.key}" style="background-color: ${warna.value}; ">
									</c:forEach>
									
								</td>
							</tr>
						</table>
					</fieldset>
				</div>
			</div>
			
		</div>

	</body>
	<script>
		<c:if test="${not empty param.sukses}">
			alert('${param.sukses}');
		</c:if>
	</script>
</html>