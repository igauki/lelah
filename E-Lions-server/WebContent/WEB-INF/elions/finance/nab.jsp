<%@ include file="/include/page/header.jsp"%>
<script>
	function submitForm(tombol, modus){
		document.formpost.modus.value = modus;
		tombol.value = 'Silahkan tunggu...';
		tombol.disabled = true;
		document.formpost.submit();
		return false;
	}
</script>
<body style="height: 100%;" onload='setupPanes("container1", "tab1");'>
<div id="contents">
	<form method="post" name="formpost">
		<input type="hidden" name="modus">
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li><a href="#" onClick="return showPane('pane1', this)" id="tab1">Upload NAB Reksadana Pendapatan Tetap</a></li>
				<li><a href="#" onClick="return showPane('pane2', this)" id="tab2">Upload NAB Reksadana Campuran</a></li>
				<li><a href="#" onClick="return showPane('pane3', this)" id="tab3">Upload NAB Reksadana Saham</a></li>
				<li><a href="#" onClick="return showPane('pane4', this)" id="tab4">Upload NAB Reksadana Indeks ETF</a></li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<table class="entry2">
						<tr>
							<th>Tanggal NAB</th>
							<td>
								<script>inputDate('tanggalNAB', '${tanggalNAB}', false);</script>
							</td>
						</tr>
						<tr>
							<th>Data</th>
							<td>
								<table class="entry2">
								  <tr>
								    <th>Pendapatan Tetap</th>
								    <th>Campuran</th>
								    <th>Saham</th>
								    <th>Indeks ETF</th>
								  </tr>
								  <tr>
								    <td align="center"><textarea name="textarea1" id="textarea1" cols="45" rows="3"></textarea></td>
								    <td align="center"><textarea name="textarea2" id="textarea2" cols="45" rows="3"></textarea></td>
								    <td align="center"><textarea name="textarea3" id="textarea3" cols="45" rows="3"></textarea></td>
								    <td align="center"><textarea name="textarea4" id="textarea4" cols="45" rows="3"></textarea></td>
								  </tr>
								</table>
							</td>
						</tr>
						<tr>
							<th>Tarik data NAB Reksadana Pendapatan Tetap:</th>
							<td><input type="button" name="tarik" value="Tarik" onclick="submitForm(this,'tarik');"></td>
						</tr>
						<tr>
							<th>Informasi NAB</th>
							<td>
								<table class="simple">
									<thead>
										<tr>
											<td>No</td>
											<td>Reksa Dana</td>
											<td>NAB/UP</td>
											<td>1 hari (%)</td>
											<!-- <td>1 minggu (%)</td> -->
											<td>1 bulan (%)</td>
											<td>1 tahun (%)</td>
											<td>3 tahun (%)</td>
										</tr>
									</thead>
									<tbody>
								         <c:forEach var="reksa" items="${sessionScope.daftarPendTtp}" varStatus="st">
											<tr> 
												<td nowrap="nowrap">${st.count}.</td>
												<td nowrap="nowrap">${reksa.ire_reksa_name}</td>
												<td nowrap="nowrap"><fmt:formatNumber value="${reksa.ird_nab_up}"/></td>
												<td nowrap="nowrap"><fmt:formatNumber value="${reksa.ird_last_1d}" /></td>
												<%-- <td nowrap="nowrap"><fmt:formatNumber value="${reksa.ird_last_7d}" /></td> --%>
												<td nowrap="nowrap"><fmt:formatNumber value="${reksa.ird_last_30d}" /></td>
												<td nowrap="nowrap"><fmt:formatNumber value="${reksa.ird_last_oneyr}" /></td>
												<td nowrap="nowrap"><fmt:formatNumber value="${reksa.ird_last_3yr}" /></td>
											</tr>
								         </c:forEach>
							         </tbody>
								</table>
							</td>
						</tr>
						<c:if test="${not empty sessionScope.daftarPendTtp}">
							<tr>
								<th>Simpan data reksadana untuk tanggal berikut</th>
								<td>
									${tgl}<br>
									<input type="checkbox" name="checkbox" id="checkbox" style="border: none">
									<script>inputDate('tanggal', '${tanggal}', false);</script> s/d 
									<script>inputDate('tanggalAkhir', '${tanggalAkhir}', false);</script><em>   Cheklist untuk upload tanggal terpilih</em>
								</td>
							</tr>
							<tr>
								<th></th>
								<td><input type="button" name="simpan" value="Simpan" onclick="if(confirm('Anda yakin?')) return submitForm(this,'simpan');"></td>
							</tr>
						</c:if>
					</table>
				</div>
				<div id="pane2" class="panes">
					<table class="entry2">
						<tr>
							<th>Tarik data NAB Reksadana Campuran:</th>
							<td><input type="button" name="tarik" value="Tarik" onclick="return submitForm(this,'tarik');"></td>
						</tr>
						<tr>
							<th>Informasi NAB</th>
							<td>
								<table class="simple">
									<thead>
										<tr>
											<td>No</td>
											<td>Reksa Dana</td>
											<td>NAB/UP</td>
											<td>1 hari (%)</td>
											<!-- <td>1 minggu (%)</td> -->
											<td>1 bulan (%)</td>
											<td>1 tahun (%)</td>
											<td>3 tahun (%)</td>
										</tr>
									</thead>
									<tbody>
								         <c:forEach var="reksa2" items="${sessionScope.daftarCamp}" varStatus="st">
											<tr>
												<td nowrap="nowrap">${st.count}.</td>
												<td nowrap="nowrap">${reksa2.ire_reksa_name}</td>
												<td nowrap="nowrap"><fmt:formatNumber value="${reksa2.ird_nab_up}"/></td>
												<td nowrap="nowrap"><fmt:formatNumber value="${reksa2.ird_last_1d}" /></td>
												<%-- <td nowrap="nowrap"><fmt:formatNumber value="${reksa2.ird_last_7d}" /></td> --%>
												<td nowrap="nowrap"><fmt:formatNumber value="${reksa2.ird_last_30d}" /></td>
												<td nowrap="nowrap"><fmt:formatNumber value="${reksa2.ird_last_oneyr}" /></td>
												<td nowrap="nowrap"><fmt:formatNumber value="${reksa2.ird_last_3yr}" /></td>
											</tr>
								         </c:forEach>
							         </tbody>
								</table>
							</td>
						</tr>
						<c:if test="${not empty sessionScope.daftarCamp}">
							<tr>
								<th>Simpan data reksadana untuk tanggal berikut</th>
								<td>
									${tgl}
								</td>
							</tr>
							<tr>
								<th></th>
								<td><input type="button" name="simpan" value="Simpan" onclick="if(confirm('Anda yakin?')) return submitForm(this,'simpan');"></td>
							</tr>
						</c:if>
					</table>
				</div>
				<div id="pane3" class="panes">
					<table class="entry2">
						<tr>
							<th>Tarik data NAB Reksadana Saham:</th>
							<td><input type="button" name="tarik" value="Tarik" onclick="return submitForm(this,'tarik');"></td>
						</tr>
						<tr>
							<th>Informasi NAB</th>
							<td>
								<table class="simple">
									<thead>
										<tr>
											<td>No</td>
											<td>Reksa Dana</td>
											<td>NAB/UP</td>
											<td>1 hari (%)</td>
											<!-- <td>1 minggu (%)</td> -->
											<td>1 bulan (%)</td>
											<td>1 tahun (%)</td>
											<td>3 tahun (%)</td>
										</tr>
									</thead>
									<tbody>
								         <c:forEach var="reksa3" items="${sessionScope.daftarSaham}" varStatus="st">
											<tr>
												<td nowrap="nowrap">${st.count}.</td>
												<td nowrap="nowrap">${reksa3.ire_reksa_name}</td>
												<td nowrap="nowrap"><fmt:formatNumber value="${reksa3.ird_nab_up}"/></td>
												<td nowrap="nowrap"><fmt:formatNumber value="${reksa3.ird_last_1d}" /></td>
												<%-- <td nowrap="nowrap"><fmt:formatNumber value="${reksa3.ird_last_7d}" /></td> --%>
												<td nowrap="nowrap"><fmt:formatNumber value="${reksa3.ird_last_30d}" /></td>
												<td nowrap="nowrap"><fmt:formatNumber value="${reksa3.ird_last_oneyr}" /></td>
												<td nowrap="nowrap"><fmt:formatNumber value="${reksa3.ird_last_3yr}" /></td>
											</tr>
								         </c:forEach>
							         </tbody>
								</table>
							</td>
						</tr>
						<c:if test="${not empty sessionScope.daftarSaham}">
							<tr>
								<th>Simpan data reksadana untuk tanggal berikut</th>
								<td>
									${tgl}
								</td>
							</tr>
							<tr>
								<th></th>
								<td><input type="button" name="simpan" value="Simpan" onclick="if(confirm('Anda yakin?')) return submitForm(this,'simpan');"></td>
							</tr>
						</c:if>
					</table>
				</div>
				<div id="pane4" class="panes">
					<table class="entry2">
						<tr>
							<th>Tarik data NAB Reksadana Indeks ETF:</th>
							<td><input type="button" name="tarik" value="Tarik" onclick="return submitForm(this,'tarik');"></td>
						</tr>
						<tr>
							<th>Informasi NAB</th>
							<td>
								<table class="simple">
									<thead>
										<tr>
											<td>No</td>
											<td>Reksa Dana</td>
											<td>NAB/UP</td>
											<td>1 hari (%)</td>
											<!-- <td>1 minggu (%)</td> -->
											<td>1 bulan (%)</td>
											<td>1 tahun (%)</td>
											<td>3 tahun (%)</td>
										</tr>
									</thead>
									<tbody>
								         <c:forEach var="reksa4" items="${sessionScope.daftarIndeksETF}" varStatus="st">
											<tr>
												<td nowrap="nowrap">${st.count}.</td>
												<td nowrap="nowrap">${reksa4.ire_reksa_name}</td>
												<td nowrap="nowrap"><fmt:formatNumber value="${reksa4.ird_nab_up}"/></td>
												<td nowrap="nowrap"><fmt:formatNumber value="${reksa4.ird_last_1d}" /></td>
												<%-- <td nowrap="nowrap"><fmt:formatNumber value="${reksa3.ird_last_7d}" /></td> --%>
												<td nowrap="nowrap"><fmt:formatNumber value="${reksa4.ird_last_30d}" /></td>
												<td nowrap="nowrap"><fmt:formatNumber value="${reksa4.ird_last_oneyr}" /></td>
												<td nowrap="nowrap"><fmt:formatNumber value="${reksa4.ird_last_3yr}" /></td>
											</tr>
								         </c:forEach>
							         </tbody>
								</table>
							</td>
						</tr>
						<c:if test="${not empty sessionScope.daftarIndeksETF}">
							<tr>
								<th>Simpan data reksadana untuk tanggal berikut</th>
								<td>
									${tgl}
								</td>
							</tr>
							<tr>
								<th></th>
								<td><input type="button" name="simpan" value="Simpan" onclick="if(confirm('Anda yakin?')) return submitForm(this,'simpan');"></td>
							</tr>
						</c:if>
					</table>
				</div>
			</div>
		</div>
	</form>
</div>
<c:if  test="${not empty successMessage}">	
			<script type="text/javascript">													
				alert('${successMessage}');	
			</script>
</c:if>
</body>
<%@ include file="/include/page/footer.jsp"%>