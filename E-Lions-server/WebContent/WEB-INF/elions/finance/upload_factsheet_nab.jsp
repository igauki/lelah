<%@ include file="/include/page/header.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script type="text/javascript">
	function confirmUpload(){
		var id = document.getElementById("lstnab");
		var jenisInvest = id.options[id.selectedIndex].text;
		var file = document.getElementById("file").value;
		
		if(file.length > 0){
			return confirm('Apakah anda yakin ingin mengupload Fact Sheet ' + jenisInvest + '?');
		}else{
			alert('Harap masukkan file yang ingin di upload.');
			return false;
		}
	}
	
	function confirmEmail(){
		return confirm('Apakah anda yakin ingin mengirim E-Mail ?');
	}
</script>

<body onload="setupPanes('container1', 'tab1');">
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Upload Fact Sheet NAB</a>
			</li>
		</ul>
		
		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form:form id="uploadform" commandName="factSheet" method="POST" enctype="multipart/form-data">
					<table class="entry2">
						<tr>
							<th style="width:200px">Upload Fact Sheet NAB<br><span class="info">* Max. 5 Mb (5120 Kb)</span></th>
							<td>
								<select id="lstnab" name="id">
									<c:forEach var="nab" items="${lstNab}">
										<option value="${nab.id}"><c:out value="${nab.jenis}" /></option>
									</c:forEach>
									<option value="DPLK Eka Dana Deposito">DPLK Eka Dana Deposito</option>
									<option value="DPLK Eka Dana Syariah">DPLK Eka Dana Syariah</option>
									<option value="DPLK Eka Dinamika">DPLK Eka Dinamika</option>
									<option value="DPLK Eka Hasil Pasti">DPLK Eka Hasil Pasti</option>
								</select>								
							</td>
						</tr>
						<tr>
							<th></th>
							<td>
								<input type="file" id="file" name="file" size="80" />
							</td>
						</tr>
						<tr>
						<tr>
							<th>Tahun</th>
							<td>
								<select id="tahun" name="tahun">
									<option value="">== Select Year ==</option>
									<option value="2016">2016</option>
									<option value="2017">2017</option>
									<option value="2018" selected>2018</option>
									<option value="2019">2019</option>
									<option value="2020">2020</option>
									<option value="2021">2021</option>
									<option value="2022">2022</option>
									<option value="2023">2023</option>
									<option value="2024">2024</option>
								</select>
							</td>
						</tr>
						<tr>
						<tr>
							<th>Bulan</th>
							<td>
								<select id="bulan" name="bulan">
									<option value="">== Select Month ==</option>
									<option value="Jan">Januari</option>
									<option value="Feb">Februari</option>
									<option value="Mar">Maret</option>
									<option value="Apr">April</option>
									<option value="May">Mei</option>
									<option value="Jun">Juni</option>
									<option value="Jul">Juli</option>
									<option value="Aug">Agustus</option>
									<option value="Sep">September</option>
									<option value="Oct">Oktober</option>
									<option value="Nov">November</option>
									<option value="Dec">Desember</option>
								</select>
							</td>
						</tr>
						<tr>
						
							<th></th>
							<td>
								<input type="submit" name="upload" value="Upload" onclick="return confirmUpload();" />
								<c:choose>
									<c:when test="${not empty errorMessage}">
										<div id="error">
										ERROR:<br>
										- <c:out value="${errorMessage}" />
										</div>
									</c:when>
									<c:when test="${not empty requestScope['org.springframework.validation.BindingResult.factSheet'].allErrors}">
										<div id="error">
										ERROR:<br>
										<c:forEach var="err" items="${requestScope['org.springframework.validation.BindingResult.factSheet'].allErrors}">
											- <c:out value="${err.defaultMessage}" />
										</c:forEach>
										</div>
									</c:when>
									<c:when test="${not empty successMessage}">
										<div id="success" style="text-transform: none;">
											<c:out value="${successMessage}" />
										</div>
									</c:when>
									<c:otherwise>
										<div id="success" style="text-transform: none;">
											File harus bertipe PDF (.pdf)
										</div>
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
						<tr>
						<th>
						Automatic Email
						</th>
						<td>
						
						<input type="submit" name="email" value="em@il"  onclick="return confirmEmail();" />
						
						</td>
						</tr>
					</table>
				</form:form>
			</div>
		</div>
	</div>
</body>

<%@ include file="/include/page/footer.jsp"%>