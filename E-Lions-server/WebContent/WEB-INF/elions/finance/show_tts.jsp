<%@ include file="/include/page/header.jsp"%>
<script>
	function awal_show(){
		setFrameSize('infoFrame', 45);
		var h=parent.document.getElementById('infoFrame').height;
		if(h==1)
			parent.document.getElementById('infoFrame').height=800-45;
		
	}
	
	function cari_batal(nomor){
		addOptionToSelect(parent.document, parent.document.formpost.nomor, nomor, nomor+'~');
		parent.tampildata();
	}
	
</script>
<body onload="awal_show();" onresize="setFrameSize('infoFrame', 45);" style="height: 100%;">
<form name="formpost" method="post">
<table class="entry2" width="100%">
		<tr align="left">
			<th>Nomor</th>
			<td>${tts.mst_noFormated}</td>
			<th>Tgl Input</th>
			<td>
				<fmt:formatDate value="${tts.mst_tgl_input}" pattern="dd/MM/yyyy"/>
			</td>
			
		</tr>
		<tr align="left">
			<th>Sudah Terima Dari </th>
			<td>
				${tts.mst_nm_pemegang }
			</td>
			<th>Nama Admin</th>
			<td colspan="2">
				${tts.lus_full_name }
			</td>
		</tr>
		<tr align="left">	
			<th rowspan="2">Keterangan</th>
			<td rowspan="2">
				<textarea name="textarea" cols="50" rows="3" onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); ">${tts.mst_ket}</textarea>
			</td>
			<th>Tanggal Bayar</th>
			<td>
				<c:choose>
					<c:when test="${tts.mst_tgl_rk eq null}">
						00/00/0000
					</c:when>
					<c:otherwise>
						<fmt:formatDate value="${tts.mst_tgl_rk}" pattern="dd/MM/yyyy"/>
					</c:otherwise>	
				</c:choose>	
			</td>
		</tr>
		<tr align="left">
			<th>Tanggal Setor</th>
			<td>
				<c:choose>
					<c:when test="${tts.mst_tgl_setor eq null}">
						00/00/0000
					</c:when>
					<c:otherwise>
						<fmt:formatDate value="${tts.mst_tgl_setor}" pattern="dd/MM/yyyy"/>
					</c:otherwise>	
				</c:choose>	
			</td>		
		</tr>
		<tr align="left">
			<th>Status Tts</th>
			<td>
				<c:if test="${tts.mst_flag_batal eq 1}">
					Batal
				</c:if>
				<c:if test="${tts.mst_flag_batal eq 0}">
					Ok
				</c:if>
			</td>
			<th>Nama Cabang</th>
			<td>
				${tts.lca_nama }
			</td>		
		</tr>
		<tr align="left">
			<th>No.Telp Penyetor</th>
			<td>
				${tts.mst_no_telp}
			</td>
		</tr>
		<c:if test="${tts.mst_no_reff_btl ne null}">
			<tr align="left">
				<th>Referensi No Tts yang Batal</th>
				<td><strong><a href="#" onClick="cari_batal('${tts.mst_no_reff_btl}');">${tts.mst_no_reff_btl}</a></strong></i></td>
			</tr>
		</c:if>	
		<c:if test="${tts.mst_no_reff_new ne null}">
			<tr align="left">
				<th>Referensi No Tts yang Baru</th>
				<td><strong><a href="#" onClick="cari_batal('${tts.mst_no_reff_new}');">${tts.mst_no_reff_new}</a></strong></i></td>
			</tr>
		</c:if>	
		
</table>
<table width="100%">	
	<tr align="left">
	<td>
		<fieldset>
		<legend>Daftar Polis </legend>
		<display:table id="baris" name="lsPolicyTts" class="displayTag" decorator="org.displaytag.decorator.TotalTableDecorator">   
			<display:column property="no_urut" title="NO" />                                                                                        
			<display:column property="premi_ke" title="Premi Ke" />                                                                                        
			<display:column property="mst_no_polis_formated" title="NO POLIS" />  
			<display:column property="mcl_first" title="NAMA PEMEGANG" />  
			<display:column property="beg_date" title="Beg Date" format="{0, date, dd/MM/yyyy}"/>                                                                                
			<display:column property="end_date" title="End Date" format="{0, date, dd/MM/yyyy}"/>                                                                                
			<display:column property="lku_symbol" title="KURS"  />                                                                                      
			<display:column property="lsbun_bunga" title="BUNGA" format="{0, number, #,##0.00;(#,##0.00)}" />                                                                                  
			<display:column property="msbi_stamp" title="Materai" format="{0, number, #,##0.00;(#,##0.00)}" />                                                                                  
			<display:column property="mst_premium" title="PREMI" format="{0, number, #,##0.00;(#,##0.00)}" />                                                                                  
			<display:column property="mst_discount" title="DISCOUNT" format="{0, number, #,##0.00;(#,##0.00)}" />                                                                                  
			<display:column property="mst_jumlah" title="JUMLAH"  format="{0, number, #,##0.00;(#,##0.00)}" />                                                                                  
			<display:column property="mst_jumlah_byr_rp" title="JUMLAH Yang Di di Terima Rp" total="true" format="{0, number, #,##0.00;(#,##0.00)}" />                                                                                  
			<display:column property="mst_jumlah_byr_dlr" title="JUMLAH Yang Di di Terima US$" total="true" format="{0, number, #,##0.00;(#,##0.00)}" />                                                                                  
			<display:column property="lku_symbol_tahapan" title=""  />                                                                                  
			<display:column property="mstah_jumlah" title="JUMLAH Potongan Tahapan" total="true" format="{0, number, #,##0.00;(#,##0.00)}" />                                                                                  
			<display:column property="mst_jum_hari" title="Jum Hari" total="true" />                                                                                  
		</display:table>       
		</fieldset>
	</td>	
	</tr>
</table>
<table width="800">	
	<tr align="left">
	<td>
		<fieldset>
		<legend>Informasi Pembayaran</legend>
		<display:table id="baris" name="lsCaraByr" class="simple" >   
			<display:column property="no_urut" title="NO" />                                                                                        
			<display:column property="lsjb_type" title="CARA BAYAR" />                                                                                        
			<display:column property="mst_no_rekening" title="NO REKENING" />                                                                        
			<display:column property="mst_nama_bank" title="NAMA BANK" />                                                                            
			<display:column property="tgl_jth_tempo" title="TANGGAL JATUH TEMPO" format="{0, date, dd/MM/yyyy}"/>                                                                            
		</display:table>    
		</fieldset>
	</td>	
	</tr>
</table>

<table width="800">	
	<tr align="left">
	<td>
		<fieldset>
		<c:if test="${size2 ne 1}">
			<legend>History Tts Belum Ada</legend>
		</c:if>
		<c:if test="${size2 eq 1}">
			<legend>History Tts</legend>
				<display:table id="baris" name="lsHistoryTts" class="simple" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="" export="true" pagesize="20">   
				<display:column property="NO" title="NO"  sortable="true"/>                                                                                                  
				<display:column property="MST_TGL" title="MST_TGL" format="{0, date, dd/MM/yyyy}"  sortable="true"/>                                                         
				<display:column property="LUS_FULL_NAME" title="LUS_FULL_NAME"  sortable="true"/>                                                                            
				<display:column property="MST_DESC" title="MST_DESC"  sortable="true"/>                                                                                      
				</display:table>                                                                                                                                             
		</fieldset>
		</c:if> 
	</td>	
	</tr>
</table>

<table width="800">	
	<tr align="left">
	<td>
		<fieldset>
		<c:if test="${size ne 1}">
			<legend>History Print Tts Belum Ada</legend>
		</c:if>
		<c:if test="${size eq 1}">
			<legend>History Print Tts</legend>
			<display:table id="baris" name="lsHistoryPrint" class="simple" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="" export="true" pagesize="20">   
			<display:column property="KE" title="KE"  sortable="true"/>                                                                                                  
			<display:column property="TGL_PRINT" title="Tanggal Print" format="{0, date , dd/MM/yyyy hh:mm:ss}"  sortable="true"/>                                                     
			<display:column property="KET_PRINT" title="Keterangan"  sortable="true"/>                                                                                    
			<display:column property="LUS_FULL_NAME" title="User"  sortable="true"/>                                                                            
			</display:table>                                                                                                                                             
		</fieldset>
		</c:if> 
	</td>	
	</tr>
</table>

</form>
</body>
<%@ include file="/include/page/footer.jsp"%>