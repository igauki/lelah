<%@ include file="/include/page/header.jsp"%>
<script>
	
	function tampildata(){
		nomor=trim(formpost.nomor.value);
		nomor=nomor.substring(0,nomor.indexOf('~'));
		document.getElementById('infoFrame').src='${path }/finance/tts.htm?window=show_tts&nomor='+nomor;
	}
	
	function cetak_tts(){
		nomor=trim(formpost.nomor.value);
		nomor=nomor.substring(0,nomor.indexOf('~'));
		//document.getElementById('infoFrame').src='${path }/report/uw.htm?window=cetak_tts&mst_no='+nomor;
		//document.getElementById('infoFrame').height= '1 px';
		document.getElementById('infoFrame').src='${path }/report/uw.print?window=cetak_tts&mst_no='+nomor+'&print=true';
		//popWinToolbar("../report/uw.pdf?window=titipanpremi&show=pdf&user="+document.frmParam.user.value+"&spaj="+document.frmParam.reg_spaj.value, 500, 800);	
	}
	
	function edit_tts(){
		//proses=1==>input proses=2==>edit proses=3==>edit tgl setor
		nomor=trim(formpost.nomor.value);
		desc=prompt("Silahkan Masukan Alasan Untuk Edit TTS","");
		if(desc!=null){
			document.getElementById('infoFrame').src='${path}/finance/input_ttsNew.htm?nomor='+nomor+'&proses=2&desc='+desc; 
		}
	}
	
	function input_tts(){
		document.getElementById('infoFrame').src='${path }/finance/input_ttsNew.htm?proses=1';
	}
	
	function editTglSetor(){
		//proses=1==>input proses=2==>edit proses=3==>edit tgl setor
		nomor=trim(formpost.nomor.value);
		document.getElementById('infoFrame').src='${path}/finance/input_ttsNew.htm?nomor='+nomor+'&proses=3'; 
	}
	
	function batal(){
		nomor=trim(formpost.nomor.value);
		nomorNew=nomor.substring(0,nomor.indexOf('~'));
		
		if(confirm("Apakah Anda ingin membatalkan No Tts ("+nomorNew+") ?")){
			alasan=prompt("Masukan ALasan Pembatalan Polis?","");
			if(alasan!=null){
				document.getElementById('infoFrame').src='${path}/finance/input_ttsNew.htm?nomor='+nomor+'&proses=4&alasan='+alasan; 
			}
		}
	}
	
	
</script>
<body onload="setFrameSize('infoFrame', 45); tampildata();" onresize="setFrameSize('infoFrame', 45);" style="height: 100%;" >
<form name="formpost" method="post">
<table class="entry2" width="98%">
	<tr align="left">
		<th>Nomer</th>
		<td>
			<select name="nomor">
				<c:forEach var="s" items="${lsTts}">
					<option value="${s.mst_no}~
						<c:choose>
							<c:when test="${s.flag_print eq null || s.flag_print eq 0}">0</c:when>
						<c:otherwise>${s.flag_print }</c:otherwise>
						</c:choose>	
					" >
						${s.mst_no}
					</option>
				</c:forEach>
			</select>
			<input type="button" name="cari" value="Cari Nomor" onclick="popWin('${path}/finance/tts.htm?window=cari_nomor', 350, 500);"
				accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();" >
			<input type="button" name="tampil" value="Tampilkan Data" onClick="tampildata()"
			accesskey="T" onmouseover="return overlib('Alt-T', AUTOSTATUS, WRAP);" onmouseout="nd();">
		</td>
	</tr>
	<tr>
		<th align="left">Proses</th>
		<td>
			<input type="button" name="input" value="Input Tts" onClick="input_tts();"
			accesskey="I" onmouseover="return overlib('Alt-I', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" name="edit" value="Edit" onclick="edit_tts();"
			accesskey="E" onmouseover="return overlib('Alt-E', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" name="print" value="Print" onclick="cetak_tts();"
			accesskey="P" onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" name="edit_tgl_setor" value="Edit Tgl Setor" onclick="editTglSetor();"
			accesskey="D" onmouseover="return overlib('Alt-D', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" name="btn_batal" value="Batal" onclick="batal();"
			accesskey="B" onmouseover="return overlib('Alt-B', AUTOSTATUS, WRAP);" onmouseout="nd();">
		</td>
	</tr>
	<tr align="left">
	<td colspan="2">
		<iframe src="" name="infoFrame" id="infoFrame"
			width="100%"  > Please Wait... </iframe>
	</td>
	</tr>
</table>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>