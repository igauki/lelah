<%@ include file="/include/page/header.jsp"%>
<script>
	function awal_tts(){
	//	pilihAll();
		var info='${cmd.info}';
		var proses='${cmd.proses}';
		formpost.nama.select();
		formpost.nama.focus();
		if(info==1){
			formpost.btnSave.disabled=true;
			alert("Anda Tidak Berhak Merubah Data Tts ini");
			window.location='${path}/finance/tts.htm?window=show_tts&nomor=${cmd.mst_no}';
		}else if(info==2){
			formpost.btnSave.disabled=true;
			alert("TTS sudah Di print tidak Bisa di edit");
			window.location='${path}/finance/tts.htm?window=show_tts&nomor=${cmd.mst_no}';
		}else if(info==3){
			formpost.btnSave.disabled=true;
			alert("TTS sudah di batalkan tidak Bisa di Edit atau Di batalkan ulang");
			window.location='${path}/finance/tts.htm?window=show_tts&nomor=${cmd.mst_no}';
		}else if(info==4){
			alert("Tanggal Setor Telah Di update Tidak BIsa di edit ulang");
			window.location='${path}/finance/tts.htm?window=show_tts&nomor=${cmd.mst_no}';
		}else{
			//setCaraBayar();
			/*if(proses=='2'){
				alert("edit");
				len='${cmd.size}';
				for(i=0;i<len;i++){
					caraBayar(i);
				}
			}*/
			var suc='${submitSuccess}';
			if(suc=='1'||suc=='2' || suc=='3'){
				parent.addOptionToSelect(parent.document, parent.document.formpost.nomor, '${nomor}', '${nomor}~');
				document.formpost.btnSave.disabled=true;
				document.formpost.addPolis.disabled=true;
				document.formpost.bykbayar.disabled=true;
			}
		}
			
		
	}
	
	function caraBayar(i){
		len='${cmd.size}';
		if(len=='1')
			formpost.elements['lsPembayaran['+i+'].lsjb_id'].value=formpost.carabayar.value;
		else
			formpost.elements['lsPembayaran['+i+'].lsjb_id'].value=formpost.carabayar[i].value;
	}
	
	function bykBayar(){
		formpost.flag.value="2";
		formpost.submit();
	}
	
	function simpanTts(){
		p='${cmd.lsPolis}';
		
		if(p==''){
			alert("Data Polis tidak ada, Tidak Bisa Simpan");
			return 0;
		}	
		formpost.flag.value="3";
		formpost.submit();
	}
	
	
	function add_noPolis(){
		formpost.flag.value="1";
		nopolis=prompt("Masukan Nopolis","");
		if(nopolis==null)
			alert("Masukan Nopolis Dengan Benar");
		else{
			formpost.nopolis.value=nopolis;
			formpost.submit();
		}		
	}
	
	/*function setCaraBayar(){
		var len=${cmd.size};
		for(i=0;i<len;i++){
			var tempCb=formpost.elements['lsPembayaran['+i+'].lsjb_id'].value
			//
			if(len==1){
				for(j=0;j<6;j++){
					if(tempCb==j+1)
						formpost.carabayar.selectedIndex=j;
				}
				for(j=6;j<=22;j++){
					if(tempCb==j+1)
						formpost.carabayar.selectedIndex=j-3;
				}
				if(tempCb=='99')
					formpost.carabayar.selectedIndex=19;
			}else{
				for(j=0;j<6;j++){
					if(tempCb==j+1)
						formpost.carabayar[i].selectedIndex=j;
				}
				for(j=6;j<=22;j++){
					if(tempCb==j+1)
						formpost.carabayar[i].selectedIndex=j-3;
				}
				if(tempCb=='99')
					formpost.carabayar[i].selectedIndex=19;
					
			}	
		}
	}	*/
	function pilihAll(){
		var pil=document.formpost.pilih;
		bykPolis=document.formpost.bykPolis.value;
		if(bykPolis>1){
			if(formpost.pil_all.value=='1'){
				formpost.pil_all.value='0';
				cek=true;
				val=1;
			}else{
				formpost.pil_all.value='1';
				cek=false;
				val=0;
			}
	
			for(i=0;i<pil.length;i++){
				document.formpost.pilih[i].checked=cek;
				document.formpost.pil[i].value=val;
				
			}
		}else{
			cek=document.formpost.pilih.checked;
			if(cek){
				document.formpost.pilih.checked=false;
				document.formpost.pil.value='0';
			}else{
				document.formpost.pilih.checked=true;
				document.formpost.pil.value='1';
			}
		}	
	}
	
	function centang(i){
		bykPolis=document.formpost.bykPolis.value;
		if(bykPolis>1){
			cek=document.formpost.pilih[i].checked;
			if(cek)
				document.formpost.pil[i].value="1"
			else
				document.formpost.pil[i].value="0"
		}else{
			cek=document.formpost.pilih.checked;
			if(cek)
				document.formpost.pil.value="1"
			else
				document.formpost.pil.value="0"
		}		
	}
	
</script>
<body  onload="awal_tts()"onresize="setFrameSize('infoFrame', 45);" style="height: 100%;">
<form name="formpost" method="post">
<fieldset>
	<legend>Data Utama</legend>
	<table class="entry2" width="400">
			<tr align="left">
				<th >Nama Cabang </th>
				<td>
					<input type="text" size="30" readonly name="inputcabang" value="${cmd.nama_cabang}">
				</td>
				<c:if test="${cmd.proses eq 4 }">
					<th>Reff No Tts yang Batal : </th>
					<th>${cmd.mstNoBatal }</th>
				</c:if>
			</tr>
			<tr align="left">
				<th>Nama Admin</th>
				<td>
					<input type="text" size="30" readonly name="nama_admin" value="${cmd.nama_admin}">
				</td>
				<c:if test="${cmd.proses eq 4 }">
					<th valign="top">Alasan Pembatalan : </th>
					<th valign="top" rowspan="3">
						<textarea  readOnly cols="50" rows="3" >${cmd.alasanBatal}</textarea>
					</th>
				</c:if>	
			</tr>
			<tr align="left">
				<th>Sudah Terima Dari </th>
				<td>
					<spring:bind path="cmd.nama">
						<input type="text"  name="${status.expression }" size="30" value="${status.value}"
							<c:if test="${ cmd.editTglSetor eq 1}">
								readOnly
							</c:if>
						>
					</spring:bind>	
				</td>
			</tr>
			<tr  align="left">	
				<th>No. Telp Penyetor</th>
				<td>
					<spring:bind path="cmd.noTelp">
						<input type="text"  name="${status.expression }" size="30" value="${status.value}"
							<c:if test="${ cmd.editTglSetor eq 1}">
								readOnly
							</c:if>
						>
					</spring:bind>	
				</td>	
			</tr>
			<tr align="left">
				<th>Keterangan</th>
				<td>
					<spring:bind path="cmd.keterangan">
						<textarea name="${status.expression }" cols="50" rows="3" onkeyup="textCounter(this, 200); "
							onkeydown="textCounter(this, 200);"
							<c:if test="${ cmd.editTglSetor eq 1}">
								readOnly
							</c:if>>${status.value}</textarea>
					</spring:bind>		
				</td>
			</tr>
			<tr align="left">	
				<th>Tanggal Bayar</th>
					<c:choose>
					<c:when test="${ cmd.editTglSetor eq 1}">
						<td>
						<fmt:formatDate value="${cmd.tglRk}" pattern="dd/MM/yyyy"/>  
						</td>
						<th><font color="#FF0000">Tanggal Setor</font>
						<script>inputDate('tanggalSetor', '${cmd.s_tgl_setor}', false, '', 9);</script>
						</th>
					</c:when>
					<c:otherwise>
						<td>
						<c:choose>
							<c:when test="${cmd.tglRk eq null}">
								<script>inputDate('tanggalrk', '00/00/0000', false, '', 9);</script>
							</c:when>
							<c:otherwise>		
								<script>inputDate('tanggalrk', '${cmd.s_tgl_rk}', false, '', 9);</script>
							</c:otherwise>	
						</c:choose>
						</td>	
					</c:otherwise>	
					</c:choose>
			</tr>
	</table>
</fieldset>	
<fieldset>
	<legend>Polis</legend>
	<table class="entry2" width="700">
		<tr align="left">
			<th colspan="2">Input No Polis </th>
			<td>
				<input type="button"name="addPolis" value="Add" onClick="add_noPolis();"
				<c:if test="${ (cmd.editTglSetor eq 1) or (cmd.tahapan.reg_spaj ne null) }">
					disabled
				</c:if>
				accesskey="A" onmouseover="return overlib('Alt-A', AUTOSTATUS, WRAP);" onmouseout="nd();">
			</td>
		</tr>
		<tr>
			<th><em>No</em></th>
			<th><em>Premi Ke</em></th>
			<th><em>No Polis</em></th>
			<th><em>Nama Pemegang</em></th>
			<th><em>Beg Date</em></th>
			<th><em>Due Date</em></th>
			<th><em>End Date</em></th>
			<th><em>Kurs</em></th>
			<th><em>Premi</em></th>
			<th><em>Discount</em></th>
			<th><em>Bunga</em></th>
			<th><em>Materai</em></th>
			<th><em>Jumlah</em></th>
			<th><em>Jlh hari</em></th>
			<th rowspan="2" valign="center"><em>Pilih</em></th>
		</tr>
		<tr>
			<th colspan="5"></th>
			<th colspan="2">Jumlah yang di Terima RP</th>
			<th colspan="2">Jumlah yang di Terima US$</th>
			<c:if test="${cmd.tahapan.reg_spaj ne null }">
				<th colspan="2">Jumlah Potongan Tahapan (${cmd.tahapan.lku_symbol})</th>
			</c:if>	
		</tr>
		<c:forEach var="id" items="${cmd.lsPolis}" varStatus="xt">
			<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;">
				<th><em>${id.no_urut}.</em></th>
				<td align="center"><em>${id.premi_ke}</em></td>
				<td><em>${id.mst_no_polis}</em></td>
				<td><em>${id.mcl_first}</em></td>
				<td><em><fmt:formatDate value="${id.beg_date}" pattern="dd/MM/yyyy"/> </em></td>
				<td><em><fmt:formatDate value="${id.due_date}" pattern="dd/MM/yyyy"/> </em></td>
				<td><em><fmt:formatDate value="${id.end_date}" pattern="dd/MM/yyyy"/> </em></td>
				<td align="right"><em>${id.lku_symbol}</em></td>
				<td><em>${id.mst_premium}</em></td>
				<td align="center"><em>${id.mst_discount}</em></td>
				<td align="center"><em>${id.lsbun_bunga}</em></td>
				<td align="center"><em>${id.msbi_stamp}</em></td>
				<td align="center"><em><fmt:formatNumber value="${id.mst_jumlah}" type="number" /> </em></td>
				<td align="center"><em>${id.mst_jum_hari}</em></td>
				<td rowspan="2" valign="center">
					<input type="checkbox" name="pilih" onClick="centang('${xt.index }');" value="0"
					onmouseover="return overlib('Silahkan Pilih Polis ini', AUTOSTATUS, WRAP);" onmouseout="nd();" 
						<c:if test="${id.pil eq 1}">
							checked
						</c:if>
					<c:if test="${ cmd.editTglSetor eq 1}">
						disabled
					</c:if>
					>			
					<input type="hidden" name="pil" 
					<c:choose>
						<c:when test="${id.pil eq 1}">
							value="1"
						</c:when>
						<c:otherwise>
							value="0"
						</c:otherwise>
					</c:choose>	
					>	
				</td>	
			</tr>
			<tr>
				<td colspan="5"></td>
				<td colspan="2" align="left">
					
					<spring:bind path="cmd.lsPolis[${xt.index }].mst_jumlah_byr_rp">
						<input 
						<c:if test="${ cmd.editTglSetor eq 1 }">
							disabled
						</c:if>
						type="text" name="${status.expression }" value="<fmt:formatNumber pattern="###" value="${status.value}" type="number" /> " >
					</spring:bind>
				</td>
				<td colspan="2" align="left">
					<spring:bind path="cmd.lsPolis[${xt.index }].mst_jumlah_byr_dlr">
						<input 
						<c:if test="${ cmd.editTglSetor eq 1 }">
							disabled
						</c:if>
						type="text" name="${status.expression }" value="${status.value}" >
					</spring:bind>
				</td>
				<c:if test="${cmd.tahapan.reg_spaj ne null }">
					<td colspan="2" align="left">
						<spring:bind path="cmd.lsPolis[${xt.index }].mstah_jumlah">
							<input 
							<c:if test="${ cmd.editTglSetor eq 1 }">
								disabled
							</c:if>
							type="text" name="${status.expression }" value="${status.value}" >
						</spring:bind>
					</td>
				</c:if>
			</tr>
		</c:forEach>
		<tr align="center">
			<td colspan="3"></td>
			<c:if test="${ cmd.lsPolis ne null}">
			<th colspan="2">Total Pembayaran</th>
			<th colspan="2" align="left">
				<fmt:formatNumber value="${cmd.gtBayarRp}" type="number" /> 
			</th>	
			<th colspan="2" align="left">
				<fmt:formatNumber value="${cmd.gtBayarDlr}" type="number" /> 
			</th>	
			<c:if test="${cmd.tahapan.reg_spaj ne null }">
				<th colspan="2" align="left">
					<fmt:formatNumber value="${cmd.gtTahapan}" type="number" /> 
				</th>
			</c:if>
			<c:if test="${cmd.tahapan.reg_spaj eq null }">
				<td colspan="4"></td>
			</c:if>			
			<td align="left">
				<input type="hidden" name="pil_all" value="0">
				<a 
					<c:if test="${ cmd.editTglSetor ne 1}">
						href="#"
					</c:if>
					onClick="pilihAll();" onmouseover="return overlib('Check/Uncheck All', AUTOSTATUS, WRAP);" onmouseout="nd();">Check/Uncheck All
				</a> 
			</td>
			</c:if>
		</tr>
        <c:if test="${cmd.tahapan.reg_spaj ne null}">
			<tr>
				<td colspan="7">
				<div id="error">
					No Polis Ini Mempunyai Tahapan Sebesar : ${cmd.tahapan.lku_symbol} <fmt:formatNumber value="${cmd.tahapan.mstah_jumlah}" type="number" /> (Pembayaran Premi Dapat dilakukan dengan potongan tahapan)
				</div>
				</td>
			</tr>
		</c:if>
		<spring:bind path="cmd.flag">
			<input  type="hidden" name="${status.expression }" value="${status.value}">
		</spring:bind>	
		<input type="hidden" name="nopolis" >
		<input type="hidden" name="bykPolis" value="${cmd.size2}">
		<input type="hidden"  name="info" value="0">
		<input type="hidden"  name="lanjut" value="0">

	</table>
</fieldset>

<fieldset>
	<legend>Pembayaran</legend>
	<table class="entry2" width="600">
		<tr align="left">
			<th>Banyak Bayar</th>
			<td>
				<select name="bykbayar" onChange="bykBayar();" 
				<c:if test="${ cmd.editTglSetor eq 1}">
					disabled
				</c:if>
				>
					<option <c:if test="${\"1\" eq cmd.size}">selected</c:if> value="1">1</option>
					<option <c:if test="${\"2\" eq cmd.size}">selected</c:if> value="2">2</option>
					<option <c:if test="${\"3\" eq cmd.size}">selected</c:if> value="3">3</option>
				</select>	
			</td>	
		</tr>
		<tr align="left">
			<th>No.</th>
			<th>Cara Bayar</th>
			<th>Nomor (Giro/Check)</th>
			<th>Nama Bank</th>
			<th>Tanggal Jatuh Tempo (Khusus Giro/Check)</th>
		</tr>
		<c:forEach var="id" items="${cmd.lsPembayaran}" varStatus="xt">
		<tr align="left">
			<td>
				<spring:bind path="cmd.lsPembayaran[${xt.index }].no_urut">
					<input disabled size="3" type="text" name="${status.expression }" value="${status.value}">
				</spring:bind>	
			</td>	
			<td>
				<spring:bind path="cmd.lsPembayaran[${xt.index }].lsjb_id">
					<input type=hidden name="${status.expression }" value="${status.value}" >
				</spring:bind>
				<select name="carabayar" onChange="caraBayar(${xt.index })"
					<c:if test="${ cmd.editTglSetor eq 1}">
						disabled
					</c:if>
				>	
					<c:forEach var="var" items="${lsCaraBayar}" varStatus="status">
						<option value="${var.LSJB_ID}" 
							<c:if test="${id.lsjb_id eq var.LSJB_ID}">selected</c:if>
						>
							${var.LSJB_TYPE }
						</option>
					</c:forEach>
				</select>
			</td>	
			<td>
				<spring:bind path="cmd.lsPembayaran[${xt.index }].mst_no_rekening">
					<input  size="20" type="text" name="${status.expression }" value="${status.value}"
						<c:if test="${ cmd.editTglSetor eq 1}">
							readOnly
						</c:if>
					>
				</spring:bind>	
			</td>	
			<td>
				<spring:bind path="cmd.lsPembayaran[${xt.index }].mst_nama_bank">
					<input  size="20" type="text" name="${status.expression }" value="${status.value}"
						<c:if test="${ cmd.editTglSetor eq 1}">
							readOnly
						</c:if>
					>
				</spring:bind>	
			</td>	
			<td>
				<script>inputDate('tgl_jth_tempo${xt.index}', '${cmd.lsPembayaran[xt.index].s_tgl_jth_tempo}', false, '', 9);</script>
			</td>	
		</tr>
		</c:forEach>
		<tr align="left">
			<td colspan="4" >
				<div align="center">
					<input type="button" name="btnSave" value="save" onClick="simpanTts();"
					accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
				</div>
			</td>
		</tr>
		<tr align="left">
		<td colspan="3">
			<c:choose>
				<c:when test="${submitSuccess eq 1}">
		        	<div id="success">
			        	Berhasil Input Tts (No. TTS : ${cmd.mst_no})
		        	</div>
		        </c:when>	
				<c:when test="${submitSuccess eq 2}">
		        	<div id="success">
			        	Berhasil Edit TTS
		        	</div>
		        </c:when>
				<c:when test="${submitSuccess eq 3}">
		        	<div id="success">
			        	Berhasil Edit Tanggal Setor
		        	</div>
		        </c:when>
	        </c:choose>	
  			<spring:bind path="cmd.*">
				<c:choose>
					<c:when test="${not empty status.errorMessages}">
						<div id="error">
							Informasi:<br>
								<c:forEach var="error" items="${status.errorMessages}">
											- <c:out value="${error}" escapeXml="false" />
									<br/>
								</c:forEach>
						</div>
					</c:when>
					<c:when test="${ cmd.editTglSetor eq 1 && submitSuccess eq null}">
						<div id="error">
							Informasi: Silahkan Ubah tanggal setor
						</div>
					</c:when>
				</c:choose>										
			</spring:bind>
          
		</td>
	</tr>
	</table>
</fieldset>
</form>		
<%@ include file="/include/page/footer.jsp"%>