<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript">
	function prosesTransfer(){
		createLoadingMessage();
		var flagPalembang = '${cmd.flagPalembang}';
		if(confirm('Informasi Billing:\n- SPAJ: ${param.spaj}\n- Tahun Ke: ${cmd.tahun}\n- Premi Ke: ${cmd.premi}\nAnda yakin untuk transfer billing komisi ini ke filling?')){
			if(flagPalembang!=''){
				if(confirm('Polis Ultra Sejahtera Khusus Palembang.\n Tambahkan Potongan Komisi sebesar Rp. 20.000,- ke agent (${cmd.komisiAgen[0].msag_id}) ${cmd.komisiAgen[0].mcl_first}?\nTekan OK apabila Ya, atau tekan Cancel apabila tidak.')){
					document.formpost.palembang.value='ya';
				}else{
					document.formpost.palembang.value='tidak';
				}
			}else{
				return true;
			}
		}else{
			return false;
		}
	}
</script>
<body style="height: 100%;">
<c:choose>
<c:when test="${not empty param.kosong}">
	<div id="contents">
		<div id="error">Harap cari SPAJ terlebih dahulu.</div>
	</div>
</c:when>
<c:when test="${empty cmd.komisiAgen}">
	<div id="contents">
		<div id="error">Informasi Komisi tidak ada.</div>
	</div>
</c:when>
<c:when test="${not empty cmd.errorMessage.sukses}">
	<div id="contents">
		<div id="success">${cmd.errorMessage.sukses}</div>
	</div>
</c:when>
<c:otherwise>
<form name="formpost" method="post"><input type="hidden" name="posisi"
	value="${cmd.posisi}"><input type="hidden" name="palembang" value="">
<div id="contents">
<fieldset>
<legend>Informasi Komisi per Billing</legend>
<table class="entry">
	<tr>
		<th>Tahun Ke: <input name="tahun" type="text" readonly size="5" value="${cmd.tahun }" style="text-align: center;"> Premi
		Ke: <input name="premi" type="text" readonly size="5" value="${cmd.premi }" style="text-align: center;"> SPAJ: <input type="text" readonly
			name="spaj" value="<elions:spaj nomor="${param.spaj }"/>" style="text-align: center;"></th>
	</tr>
	<tr>
		<th>Periode: <input name="textfield3" type="text" readonly size="12" value="<fmt:formatDate value="${cmd.komisiAgen[0].msbi_beg_date }" pattern="dd/MM/yyyy"/>"> s/d <input
			name="textfield4" type="text" readonly size="12" value="<fmt:formatDate value="${cmd.komisiAgen[0].msbi_end_date }" pattern="dd/MM/yyyy"/>"> &nbsp;&nbsp; <input
			type="submit" name="prev" value="&laquo; Prev" ${cmd.prev} 
			title="Lihat Billing Tahun/Premi sebelumnya"> <input type="submit"
			name="next" value="Next &raquo;" ${cmd.next} 
			title="Lihat Billing Tahun/Premi berikutnya"></th>
	</tr>
	<tr>
		<th>
			<input type="submit" name="transfer" id="transfer" value="Transfer ke Filling" 
				onclick="return prosesTransfer();">
		</th>
	</tr>
</table>
<c:if test="${not empty cmd.errorMessage.error}">
	<div id="error" align="left">
		ERROR: <br/>
		- ${cmd.errorMessage.error}
	</div>
</c:if>
<table width="100%">
	<c:forEach var="komisi" items="${cmd.komisiAgen }" varStatus="st">
		<c:set var="nilai" value="${komisi.total}" />
		<tr>
			<td>${st.count }.</td>
			<td>
			<table class="simple" style="border: 1px solid black;">
				<tr>
					<th>Level:</th>
					<td colspan="3"><input name="textfield6" type="text" readonly size="5" value="${komisi.lev_kom}" style="text-align: center;"> <input
						name="textfield7" type="text" readonly size="30" value="${komisi.lev_nama}"> <input name="textfield8"
						type="text" readonly size="15" value="${komisi.msag_id}" style="text-align: center;"></td>
					<td rowspan="3">
					<c:if test="${komisi.msbi_flag_topup eq 1}">
						<div id="error">KOMISI TOP-UP</div>
					</c:if>
					</td>
				</tr>
				<tr>
					<th>Nama Lengkap:</th>
					<td colspan="3"><input name="textfield9" type="text" readonly size="11" value="${komisi.mcl_gelar}"> <input
						name="textfield10" type="text" readonly size="42" value="${komisi.mcl_first}"></td>
				</tr>
				<tr>
					<th>Nomor Tabungan:</th>
					<td><input name="textfield11" type="text" readonly size="25" value="${komisi.msag_tabungan}" style="text-align: center;"></td>
					<th>Nilai Kurs:</th>
					<td><input type="text" readonly name="textfield12" value="<fmt:formatNumber value="${komisi.msco_nilai_kurs}" type="currency" currencySymbol="Rp. " maxFractionDigits="2" minFractionDigits="0" />" style="text-align: right;"></td>
				</tr>
				<tr>
					<th>Komisi Agen:</th>
					<td colspan="4">
					<table class="simple">
						<thead>
							<tr>
								<td><strong>Komisi</strong></td>
								<td width="20">&nbsp;</td>
								<td><strong>Pajak</strong></td>
								<td width="20">&nbsp;</td>
								<td><strong>Total Komisi</strong></td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
								<div align="center"><input name="textfield132" type="text" readonly
									size="30" value="<fmt:formatNumber value="${komisi.msco_comm}" type="currency" currencySymbol="Rp. " maxFractionDigits="2" minFractionDigits="0" />" style="text-align: right;"></div>
								</td>
								<td align="center"><strong>-</strong></td>
								<td>
								<div align="center"><input name="textfield142" type="text" readonly
									size="30" value="<fmt:formatNumber value="${komisi.msco_tax}" type="currency" currencySymbol="Rp. " maxFractionDigits="2" minFractionDigits="0" />" style="text-align: right;"></div>
								</td>
								<td align="center"><strong>=</strong></td>
								<td>
								<div align="center"><input name="textfield152" type="text" readonly
									size="30" value="<fmt:formatNumber value="${komisi.total}" type="currency" currencySymbol="Rp. " maxFractionDigits="2" minFractionDigits="0" />" style="text-align: right;"></div>
								</td>
							</tr>
							<c:forEach var="dd" items="${komisi.deduct}">
								<c:set var="nilai" value="${nilai - dd.MSDD_DEDUCT}" />
								<tr>
									<th colspan="3">
									<div align="right">${dd.LSJD_JENIS }</div>
									</th>
									<td>
									<div align="center"><strong>=</strong></div>
									</td>
									<td>
									<div align="center">( <input name="textfield162" type="text" readonly
										size="30" value="<fmt:formatNumber value="${dd.MSDD_DEDUCT }" type="currency" currencySymbol="Rp. " maxFractionDigits="2" minFractionDigits="0" />" style="text-align: right; color: red;"> )</div>
									</td>
								</tr>
							</c:forEach>
							<tr>
								<th colspan="3">
								<div align="right">Total Bayar</div>
								</th>
								<td>
								<div align="center"><strong>=</strong></div>
								</td>
								<td>
								<div align="center"><input name="textfield172" type="text" readonly
									size="30" value="<fmt:formatNumber value="${nilai}" type="currency" currencySymbol="Rp. " maxFractionDigits="2" minFractionDigits="0" />" style="text-align: right;"></div>
								</td>
							</tr>
						</tbody>
					</table>
					<input type="button" name="deduct" id="deduct" value="Deduct"
						onclick="popWin('${path}/finance/komisi.htm?window=deduct&msco_id=${komisi.co_id}&spaj=${param.spaj}&nama=${komisi.mcl_first}&lev_comm=${komisi.lev_kom}', 400, 720); ">
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</c:forEach>
</table>
</fieldset>
</div>
</form>
</c:otherwise>
</c:choose>
</body>
<%@ include file="/include/page/footer.jsp"%>