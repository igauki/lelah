<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript">
	function sembunyi(){
		var flags = document.getElementsByName('flag');
		for(i=0; i<flags.length; i++){
			if(flags[i].value!='I') flags[i].value = 'U';
		}
	}
	function closed(cal) {
		sembunyi();
		//dua baris dibawah jangan dihapus
		cal.hide();
		return true;
	}
	function tutup(){
		if(self.opener)
			if(self.opener.parent)
				if(self.opener.parent.document.formpost)
					if(self.opener.parent.document.formpost.komisi)
						self.opener.parent.document.formpost.komisi.click();
		window.close();
	}
</script>
<body style="height: 100%;">
	<div id="contents">
	<form method="post" name="formpost">
		<fieldset>
		<legend>Proses Deduct untuk Agen ${param.nama} [<elions:spaj nomor="${param.spaj }"/>] level ${param.lev_comm}</legend>
		<input type="hidden" name="msco_id" value="${param.msco_id}">
		<table class="simple">
			<thead>
				<tr>
					<td>No</td>
					<td>Jenis</td>
					<td>Tanggal</td>
					<td>Jumlah</td>
					<td>Keterangan</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="ded" items="${cmd.deductList}" varStatus="st">
					<c:choose>
						<c:when test="${ded.FLAG eq \"I\"}"><c:set var="warna" value="#FFFFFF"/><c:set var="disableAdd" value="disabled"/></c:when>
						<c:otherwise><c:set var="warna" value="#FFFFE0"/></c:otherwise>
					</c:choose>
					<tr>
						<td>
							<input type="text" readonly size="3" name="MSDD_NUMBER" value="${st.count}" style="background-color: ${warna};">
						</td>
						<td >
							<input type="hidden" name="FLAG" value="${ded.FLAG}">
							<select name="LSJD_ID" style="background-color: ${warna};" onchange="sembunyi()">
								<c:forEach var="jn" items="${cmd.jenisDeduct}">
									<option value="${jn.LSJD_ID}" <c:if test="${ded.LSJD_ID eq jn.LSJD_ID}">selected</c:if>>${jn.LSJD_JENIS}</option>
								</c:forEach>
							</select>
						</td>
						<td >
							<input type="text" id="tgl${st.index}" readonly name="MSDD_DATE" value="<fmt:formatDate value="${ded.MSDD_DATE}" pattern="dd/MM/yyyy"/>" size="12" style="background-color: ${warna};">
							<script type="text/javascript">
							    Calendar.setup({
							        inputField     :    "tgl${st.index}",
							        ifFormat       :    "%d/%m/%Y",
							        align          :    "Tl",
							        onClose    : closed
							    });
							</script>														
						</td>
						<td >
							<input type="text" onkeydown="sembunyi()" name="MSDD_DEDUCT" value="<fmt:formatNumber value="${ded.MSDD_DEDUCT}" type="currency" currencySymbol="" maxFractionDigits="2" minFractionDigits="0" />" size="15" style="text-align: right" style="background-color: ${warna};">
						</td>
						<td >
							<textarea onkeydown="sembunyi()" rows="4" cols="45" name="MSDD_DESC" style="background-color: ${warna};">${ded.MSDD_DESC}</textarea>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<br/>
		<c:if test="${cmd.disabled ne \"yes\" }">
			<input type="submit" name="add" onclick="createLoadingMessage();" value="Add" ${disableAdd}>
			<input type="submit" name="save" onclick="createLoadingMessage();" value="Save" <c:if test="${empty cmd.deductList}">disabled</c:if>>
			<input type="submit" name="cancel" onclick="createLoadingMessage();" value="Cancel">
		</c:if>
		<input type="button" name="close" value="Close" onclick="tutup();">
	</fieldset>
	</form>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>