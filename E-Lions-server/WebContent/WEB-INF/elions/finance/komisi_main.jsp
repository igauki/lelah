<%@ include file="/include/page/header.jsp"%>
<style type="text/css">
	html,body{
	     overflow: hidden;
	}
</style>
<script>
	function cekSpaj(){
		if(trim(document.formpost.spaj.value)=='') {
			alert('Harap cari SPAJ terlebih dahulu');
			popWin('${path}/uw/spaj.htm?posisi=8&win=komisi', 350, 450);
			//popWin('${path}/uw/carinasabah.htm', 600, 800); 
		}else return true;
		return false;
	}
	
	function tampilkan(nama){
		if(document.getElementById('infoFrame')){
			if(nama=='info') document.getElementById('infoFrame').src = '${path}/uw/view.htm?showSPAJ='+document.formpost.spaj.value;
			else if(nama=='komisi') document.getElementById('infoFrame').src = '${path}/finance/komisi.htm?window=komisi&spaj='+document.formpost.spaj.value;
			else if(nama=='jurnal') document.getElementById('infoFrame').src = '${path}/finance/komisi.htm?window=jurnal';
		}
	}
</script>
<body style="height: 100%;"
	onload="setFrameSize('infoFrame', 68);"
	onresize="setFrameSize('infoFrame', 68);">
<form name="formpost" method="post">
<div class="tabcontent">
<table class="entry" width="98%">
	<tr>
		<th>
			Window Komisi Finance 
			<input type="hidden" id="spaj" name="spaj" value="${param.spaj}" >
		</th>
		<td>
			<input type="button" value="Cari Polis" name="search"
				onclick="popWin('${path}/uw/spaj.htm?posisi=8&win=komisi', 500, 500);"> <!-- &search=yes -->	
			<input type="button" name="info" value="Info Polis"
				onclick="if(cekSpaj()) tampilkan('info'); ">			
			<input type="button" name="komisi" value="Info Komisi"
				onclick="if(cekSpaj()) tampilkan('komisi'); ">
			<input type="button" name="premi" id="premi" value="Info Premi" onclick="alert('tunggu dikerjain ferry dulu');">
			<input type="button" name="jurnal" id="jurnal" value="Journal"
				onclick="tampilkan('jurnal'); ">
		</td>
		<td><div id="prePostError" style="display:none;"></div></td>
	</tr>
	<tr>
		<td colspan="3">
			<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
				width="100%"> Please Wait... </iframe>
		</td>
	</tr>
</table>
</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>