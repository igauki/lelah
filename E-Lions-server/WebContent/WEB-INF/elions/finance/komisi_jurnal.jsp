<%@ include file="/include/page/header.jsp"%>
<body style="height: 100%;">
	<div id="contents">
	<fieldset>
	<form method="post" name="formpost">
		<legend>Window Jurnal Komisi</legend>
		<table class="entry">
			<tr>
				<th>Jurnal Komisi:</th>
				<td>
					<label for="radio1"><input id="radio1" type="radio" checked="checked" class="noBorder" name="jenis" value="1"> Komisi Tahun dan Premi Pertama</label><br>
					<label for="radio2"><input id="radio2" type="radio" <c:if test="${jenis eq 2}">checked</c:if> class="noBorder" name="jenis" value="2"> Komisi Tahun Pertama dan Premi > 1</label><br>
					<label for="radio3"><input id="radio3" type="radio" <c:if test="${jenis eq 3}">checked</c:if> class="noBorder" name="jenis" value="3"> Komisi Tahun > 1</label>
				</td>
			</tr>
			<tr>
				<th>Tanggal Jurnal:</th>
				<td>
					<script>inputDate('startDate', '<fmt:formatDate value="${startDate}" pattern="dd/MM/yyyy"/>', false);</script>
					s/d 
					<script>inputDate('endDate', '<fmt:formatDate value="${endDate}" pattern="dd/MM/yyyy"/>', false);</script>
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<input type="submit" name="jurnal" onclick="createLoadingMessage();" value="Jurnal!" onclick="alert('Maaf tetapi modul ini masih dalam tahap pengembangan'); return false;">
				</td>
			</tr>
		</table>
	</form>
	</fieldset>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>