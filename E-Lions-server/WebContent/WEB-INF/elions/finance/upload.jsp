<%@ include file="/include/page/header.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<script>
	function konfirmasi(tipe){
		var cuitcuit = '';
		if(tipe != 'BONDS' && tipe != 'TRANSAKSI REKSADANA') cuitcuit = 's/d ['+document.formpost.tanggalAkhir.value+']';
		return confirm('Simpan data '+tipe+' untuk tanggal ['+document.formpost.tanggal.value+'] sampai ['+document.formpost.tanggalAkhir.value+']'+cuitcuit+'?');
	}
	function konfirmasi2(tipe){
		var cuitcuit = '';
		if(tipe != 'BONDS' && tipe != 'TRANSAKSI REKSADANA') cuitcuit = 's/d ['+document.formpost.tanggalAkhir.value+']';
		return confirm('Hapus data '+tipe+' untuk tanggal ['+document.formpost.tanggal.value+'] '+cuitcuit+'?\nDATA YANG SUDAH DIHAPUS, TIDAK DAPAT DIKEMBALIKAN. ANDA YAKIN?');
	}
	
	function fileupload(filename,no){
		if(no==1){
			if(filename=="" || filename==null){
				alert('Harap isi file yang akan diupload');
			}else{
				ufile2.style.display = "block";
				//utgl2.style.display = "block";
			}
		}else if(no==2){
			if(filename=="" || filename==null){
				alert('Harap isi file yang akan diupload');
			}else{
				ufile3.style.display = "block";
				//utgl3.style.display = "block";
			}
		}else if(no==3){
			if(filename=="" || filename==null){
				alert('Harap isi file yang akan diupload');
			}else{
				ufile4.style.display = "block";
				//utgl4.style.display = "block";
			}
		}else{
			if(filename=="" || filename==null){
				alert('Harap isi file yang akan diupload');
			}else{
				ufile5.style.display = "block";
				//utgl5.style.display = "block";
			}
		}
	}

</script>
<body onload="setupPanes('container1', 'tab1');">
	
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Upload Stocks / Reksadana Penyertaan Terbatas / Bonds</a>
				<a href="#" onClick="return showPane('pane2', this)" id="tab2">Help</a>
			</li>
		</ul>

		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form name="formpost" method="post" enctype="multipart/form-data">
					<table class="entry2">
						<tr>
							<th>Upload File <span class="info">* Max. 1 Mb (1024 Kb)</span></th>
							<td>
								<input type="file" name="file1" size="70">&nbsp;<input type="button" name="newfile" value="New Upload" onclick="fileupload(this.form.file1.value,1);">
								<div id="ufile2" style="display:none;"><input type="file" name="file2" size="70">&nbsp;<input type="button" name="newfile2" value="New Upload" onclick="fileupload(this.form.file2.value,2);"></div>
								<div id="ufile3" style="display:none;"><input type="file" name="file3" size="70">&nbsp;<input type="button" name="newfile3" value="New Upload" onclick="fileupload(this.form.file3.value,3);"></div>
								<div id="ufile4" style="display:none;"><input type="file" name="file4" size="70">&nbsp;<input type="button" name="newfile4" value="New Upload" onclick="fileupload(this.form.file4.value,4);"></div>
								<div id="ufile5" style="display:none;"><input type="file" name="file5" size="70"></div>
							</td>
						</tr>
						<tr>
							<th>Masukkan Tanggal Data yang Diupload</th>
							<td>
								<script>inputDate('tanggal', '${tanggal}', false);</script> s/d 
								<script>inputDate('tanggalAkhir', '${tanggalAkhir}', false);</script>
								
								<!-- <div id="utgl2" style="display:none;"><script>inputDate('tanggal2', '${tanggal2}', false);</script> s/d 
									<script>inputDate('tanggalAkhir2', '${tanggalAkhir2}', false);</script> <em>Tanggal upload ke-2</em></div>
								<div id="utgl3" style="display:none;"><script>inputDate('tanggal3', '${tanggal3}', false);</script> s/d 
									<script>inputDate('tanggalAkhir3', '${tanggalAkhir3}', false);</script> <em>Tanggal upload ke-3</em></div>
								<div id="utgl4" style="display:none;"><script>inputDate('tanggal4', '${tanggal4}', false);</script> s/d 
									<script>inputDate('tanggalAkhir4', '${tanggalAkhir4}', false);</script> <em>Tanggal upload ke-4</em></div>
								<div id="utgl5" style="display:none;"><script>inputDate('tanggal5', '${tanggal5}', false);</script> s/d 
									<script>inputDate('tanggalAkhir5', '${tanggalAkhir5}', false);</script> <em>Tanggal upload ke-5</em></div>-->
							</td>
						</tr>
						<tr>
							<th>Upload Saham (Stocks)</th>
							<td>
								<input type="submit" name="preview_stocks" value="UPLOAD">
								<input type="submit" name="delete_stocks" value="DELETE" onclick="return konfirmasi2('SAHAM');">
							</td>
						</tr>
						<tr>
							<th>Upload Reksadana Penyertaan Terbatas</th>
							<td>
								<input type="submit" name="preview_pooled_funds" value="UPLOAD">
								<input type="submit" name="delete_pooled_funds" value="DELETE" onclick="return konfirmasi2('POOLED FUNDS');">
							</td>
						</tr>
						<tr>
							<th>Upload Obligasi (Bonds)</th>
							<td>
								<input type="submit" name="preview_bonds" value="UPLOAD">
								<input type="submit" name="delete_bonds" value="DELETE" onclick="return konfirmasi2('BONDS');">
							</td>
						</tr>
						<tr>
							<th>Upload Transaksi Reksadana</th>
							<td>
								<input type="submit" name="preview_trans_reksadana" value="UPLOAD" onclick="alert('Upload transaksi reksadana tanggal '+formpost.tanggal.value);">
							</td>
						</tr>
						<c:choose>
							<c:when test="${not empty hasilProses}">
								<tr>
									<th></th>
									<td>
										<div id="error">
											DATA SELESAI DIPROSES. BERIKUT PENJELASANNYA : <br>
											<c:forEach var="h" items="${hasilProses}">
														- <c:out value="${h}" escapeXml="false" />
												<br />
											</c:forEach>
											<c:if test="${not empty hasilProses2}">
												Upload ke - 2<br>
												<c:forEach var="h" items="${hasilProses2}">
														- <c:out value="${h}" escapeXml="false" />
												<br />
												</c:forEach>
											</c:if>
											<c:if test="${not empty hasilProses3}">
												Upload ke - 3<br>
												<c:forEach var="h" items="${hasilProses3}">
														- <c:out value="${h}" escapeXml="false" />
												<br />
												</c:forEach>
											</c:if>
											<c:if test="${not empty hasilProses4}">
												Upload ke - 4<br>
												<c:forEach var="h" items="${hasilProses4}">
														- <c:out value="${h}" escapeXml="false" />
												<br />
												</c:forEach>
											</c:if>
											<c:if test="${not empty hasilProses5}">
												Upload ke - 5<br>
												<c:forEach var="h" items="${hasilProses5}">
														- <c:out value="${h}" escapeXml="false" />
												<br />
												</c:forEach>
											</c:if>
										</div>
									</td>
								</tr>
							</c:when>
							<c:when test="${not empty errorMessages}">
								<tr>
									<th></th>
									<td>
										<div id="error">
											DATA TIDAK BERHASIL DIUPLOAD !!!<br>
											<c:forEach var="error" items="${errorMessages}">
														- <c:out value="${error}" escapeXml="false" />
												<br />
											</c:forEach>
										</div>
									</td>
								</tr>
							</c:when>
							<c:when test="${not empty resultStocks}">
								<tr>
									<th></th>
									<td>
										<table class="displaytag" style="width: auto;">
											<thead>
												<tr>
													<th style="width: 100px; ">SYMBOL</th>
													<th style="width: 100px; ">VALUE</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${resultStocks}" var="s">
													<tr>
														<td style="text-align: center;">${s.SYMBOL}</td>
														<td><fmt:formatNumber value="${s.VALUE}" /></td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<br/>
										<input type="submit" name="save_stocks" value="Simpan" onclick="return konfirmasi('SAHAM');">
									</td>
								</tr>
							</c:when>
							<c:when test="${not empty resultPooledFunds}">
								<tr>
									<th></th>
									<td>
										<table class="displaytag" style="width: auto;">
											<thead>
												<tr>
													<th style="width: 200px; ">FUND</th>
													<th style="width: 100px; ">NAB</th>
													<th style="width: 100px; ">JENIS</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${resultPooledFunds}" var="s">
													<tr>
														<td style="text-align: center;">${s.FUND}</td>
														<td><fmt:formatNumber value="${s.NAB}" /></td>
														<td>
															<select name="${s.FUND}_type" id="${s.FUND}_type">
																<c:forEach items="${s.TYPE}" var="x">
																	<option value="${x.value}">${x.key}</option>
																</c:forEach>		
															</select>
														</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<br/>
										<input type="submit" name="save_pooled_funds" value="Simpan" onclick="return konfirmasi('POOLED FUNDS');">
									</td>
								</tr>
							</c:when>
							<c:when test="${not empty resultBonds1}">
								<tr>
									<th></th>
									<td>
										<table class="displaytag" style="width: auto;">
											<thead>
												<tr>
													<th style="width: 100px; ">KODE</th>
													<th style="width: 100px; ">SERI</th>
													<th style="width: 100px; ">MARKET PRICE</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${resultBonds1}" var="s">
													<tr>
														<td style="text-align: center;">${s.KODE}</td>
														<td style="text-align: center;">${s.SERI}</td>
														<td><fmt:formatNumber type="percent" value="${s.PRICE}" maxFractionDigits="3" minFractionDigits="3" /></td>
													</tr>
												</c:forEach>
												<c:if test="${not empty resultBonds2}">
												<tr><td colspan="3" bgcolor="#ebebeb" style="text-align: center;"><strong>upload 2</strong></td>
												</tr>
												<c:forEach items="${resultBonds2}" var="s">
													<tr>
														<td style="text-align: center;">${s.KODE}</td>
														<td style="text-align: center;">${s.SERI}</td>
														<td><fmt:formatNumber type="percent" value="${s.PRICE}" maxFractionDigits="3" minFractionDigits="3" /></td>
													</tr>
												</c:forEach>
												</c:if>
												<c:if test="${not empty resultBonds3}">
												<tr><td colspan="3" bgcolor="#ebebeb" style="text-align: center;"><strong>upload 3</strong></td>
												</tr>
												<c:forEach items="${resultBonds3}" var="s">
													<tr>
														<td style="text-align: center;">${s.KODE}</td>
														<td style="text-align: center;">${s.SERI}</td>
														<td><fmt:formatNumber type="percent" value="${s.PRICE}" maxFractionDigits="3" minFractionDigits="3" /></td>
													</tr>
												</c:forEach>
												</c:if>
												<c:if test="${not empty resultBonds4}">
												<tr><td colspan="3" bgcolor="#ebebeb" style="text-align: center;"><strong>upload 4</strong></td>
												</tr>
												<c:forEach items="${resultBonds4}" var="s">
													<tr>
														<td style="text-align: center;">${s.KODE}</td>
														<td style="text-align: center;">${s.SERI}</td>
														<td><fmt:formatNumber type="percent" value="${s.PRICE}" maxFractionDigits="3" minFractionDigits="3" /></td>
													</tr>
												</c:forEach>
												</c:if>
												<c:if test="${not empty resultBonds5}">
												<tr><td colspan="3" bgcolor="#ebebeb" style="text-align: center;"><strong>upload 5</strong></td>
												</tr>
												<c:forEach items="${resultBonds5}" var="s">
													<tr>
														<td style="text-align: center;">${s.KODE}</td>
														<td style="text-align: center;">${s.SERI}</td>
														<td><fmt:formatNumber type="percent" value="${s.PRICE}" maxFractionDigits="3" minFractionDigits="3" /></td>
													</tr>
												</c:forEach>
												</c:if>
											</tbody>
										</table>
										<br/>
										<input type="submit" name="save_bonds" value="Simpan" onclick="return konfirmasi('BONDS');">
									</td>
								</tr>
							</c:when>
							<c:when test="${not empty resultTransaksiReksadana}">
								<tr>
									<th></th>
									<td>
										<table class="displaytag" style="width: auto;">
											<thead>
												<tr>
													<th>KODE</th>
													<th>NAMA</th>
													<th>TANGGAL</th>
													<th>TRANSAKSI</th>
													<th>TOTAL COST</th>
													<th>NAB</th>
													<th>TOTAL UNIT</th>
													<th>REDEMP</th>
													<th>GAIN/LOSS</th>
													<th>AVG COST</th>
													<th>KETERANGAN</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${resultTransaksiReksadana}" var="s">
													<tr>
														<td>${s.ire_reksa_no}</td>
														<td>${s.ire_reksa_name}</td>
														<td><fmt:formatDate value="${s.irt_trans_date}" pattern="dd/MM/yyyy" /></td>
														<td>
															<c:choose>
																<c:when test="${s.irt_rtrans_jn eq 1}">Buying</c:when>
																<c:when test="${s.irt_rtrans_jn eq 0}">Selling</c:when>
																<c:otherwise>ERROR!!!</c:otherwise>
															</c:choose>
														</td>
														<td style="background-color: #FFFFCC;"><fmt:formatNumber type="currency" currencySymbol="" value="${s.irt_total_cost}" minFractionDigits="2" maxFractionDigits="2"/></td>
														<td><fmt:formatNumber type="currency" currencySymbol="" value="${s.irt_nav}" minFractionDigits="4" maxFractionDigits="4" /></td>
														<td><fmt:formatNumber type="currency" currencySymbol="" value="${s.irt_subscribe_unit}" minFractionDigits="4" maxFractionDigits="4" /></td>
														<td><fmt:formatNumber type="currency" currencySymbol="" value="${s.irt_amount}" minFractionDigits="4" maxFractionDigits="4" /></td>
														<td style="background-color: #FFFFCC;"><fmt:formatNumber type="currency" currencySymbol="" value="${s.irt_gain_loss}" minFractionDigits="2" maxFractionDigits="2" /></td>
														<!--<td style="background-color: #FFFFCC;"><fmt:formatNumber type="currency" currencySymbol="" value="${s.irt_total_cost + s.irt_amount}" minFractionDigits="2" maxFractionDigits="2" /></td>-->
														<td style="background-color: #FFFFCC;"><fmt:formatNumber type="currency" currencySymbol="" value="${s.irt_average_cost}" minFractionDigits="4" maxFractionDigits="4" /></td>
														<td>${s.irt_note}</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<br/>
										<input type="submit" name="save_trans_reksadana" value="Simpan" onclick="return confirm('Simpan Transaksi Reksadana tanggal '+formpost.tanggal.value+' ?');">
									</td>
								</tr>
							</c:when>
							<c:otherwise>
							</c:otherwise>
						</c:choose>
					</table>
					
				</form>
			</div>
			<div id="pane2" class="panes">
				<p>Panduan Upload Saham</p>
				<img alt="Panduan Upload Saham" src="${path}/include/image/xls_saham.jpg">
				<p>Panduan Upload Pooled Funds</p>
				<img alt="Panduan Upload Pooled Funds" src="${path}/include/image/xls_fund2.jpg">
				<p>Panduan Upload Bonds</p>
				<img alt="Panduan Upload Bonds" src="${path}/include/image/xls_bond.jpg">
				<p>Panduan Upload Transaksi Reksadana</p>
				<img alt="Panduan Upload Transaksi Reksadana" src="${path}/include/image/xls_trans_reksadana.jpg">
			</div>
		</div>
	</div>
	
</body>
<%@ include file="/include/page/footer.jsp"%>