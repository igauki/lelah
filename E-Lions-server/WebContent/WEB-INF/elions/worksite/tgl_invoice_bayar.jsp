<%@ include file="/include/page/header.jsp"%>
<style type="text/css">
	html,body{
	     overflow: hidden;
	}
</style>
<script>

	function tampilkanList()
    {
        if (document.frmParam.namaperusahaan.value=="NONE"){
			alert('Silahkan pilih nama perusahaan terlebih dahulu');
		}else{
			document.frmParam.showButton.value = 'true';
            frmParam.submit();
        }
    }
    
    function tampilkanpdfnexcelList(pdfexcel)
    {
    	if(pdfexcel == 'pdf'){
        	popWin('${path}/report/bac.pdf?window=summary_ws_det&mcl_id='+document.frmParam.namaperusahaan.value+'&jenis='+document.frmParam.jenis.value,600,800);
        }else if(pdfexcel == 'excel'){
        	popWin('${path}/report/bac.xls?window=summary_ws_det&mcl_id='+document.frmParam.namaperusahaan.value+'&jenis='+document.frmParam.jenis.value,600,800);
        }
    }
    
    function input_invoice(){
    	var day = document.frmParam.day.value;
    	var month = document.frmParam.month.value;
    	var year = document.frmParam.year.value;
    	if (document.frmParam.namaperusahaan.value=="NONE"){
			alert('Silahkan pilih nama perusahaan terlebih dahulu');
		}else{
			popWin('${path}/worksite/input_invoice.htm?mcl_id='+document.frmParam.namaperusahaan.value+'&dayStart='+day+'&monthStart='+month+'&yearStart='+year,400,500);
        }
    }

	function input_bayar_ws(nomor, periode, tgl_invoice, jml_invoice){
    	var day = document.frmParam.day.value;
    	var month = document.frmParam.month.value;
    	var year = document.frmParam.year.value;
		popWin('${path}/worksite/input_bayar_ws.htm?mcl_id='+document.frmParam.namaperusahaan.value+'&dayStart='+day+'&monthStart='+month+'&yearStart='+year+'&nomor='+nomor+'&periode='+periode+'&tgl_invoice='+tgl_invoice+'&jml_invoice='+jml_invoice,400,500);
    }
</script>
<body 
	onload="setFrameSize('infoFrame', 68);"
	onresize="setFrameSize('infoFrame', 68);" style="height: 100%;" >
<form name="frmParam" method="post">
<div class="tabcontent">
    <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  >
		
	   </th>
      </tr>
      <tr> 
        <th width="13%" >Nama Perusahaan</th>
        <th width="83%">
			<select name="namaperusahaan" tabindex="14" id="namaperusahaan">
					<option value="NONE">NONE</option>
					<c:forEach var="r" items="${listperusahaan}" varStatus="perush"> 
					<option value="${r.MCL_ID}" <c:if test="${r.MCL_ID eq namaperusahaan }">Selected</c:if>>${r.MCL_FIRST}</option> 
					</c:forEach> 
			</select> 

		</th>
      </tr>
      
      
      <tr> 
        <th width="13%" >Jenis</th>
        <th width="83%">
			<select name="jenis" tabindex="14" id="jenis">
					<option value="NONE">NONE</option>
					<c:forEach var="r" items="${jenisList}" varStatus="jns"> 
					<option value="${r.key}" <c:if test="${r.key eq jenis }">Selected</c:if>>${r.value}</option> 
					</c:forEach> 
			</select> 
			&nbsp;
			&nbsp;
		<input type="button" value="Show PDF" name="showpdf" id="showpdf" onClick="tampilkanpdfnexcelList('pdf');">
			&nbsp;
			&nbsp;
		<input type="button" value="Show EXCEL" name="showexcel" id="showexcel" onClick="tampilkanpdfnexcelList('excel');">
		</th>
      </tr>
      <tr> 
        <th colspan="2" ><input type="button" value="Show" name="show" id="show" onClick="tampilkanList();">
        &nbsp; &nbsp;
        <input type="button" value="Input Tgl Invoice" name="input_tgl_invoice"  onClick="input_invoice();">  
        <input type="hidden" id="showButton" name="showButton"/>
        <input type="hidden" id="day" name="day" value="${day}"/>
        <input type="hidden" id="month" name="month" value="${month}"/>
        <input type="hidden" id="year" name="year" value="${year}"/>
        
		</th>
      </tr>
      <tr> 
        <td> </td>
      </tr>

    </table>
    <table id="thetable" width="1000" align="center" border=1 cellPadding=2 cellSpacing=1 class="nobottom" style="table-LAYOUT: fixed;" >
        <colgroup>
          	<col style="width: 40px;">
            <col style="width: 180px;">
            <col style="width: 80px;">
            <col style="width: 110px;">
            <col style="width: 80px;">
            <col style="width: 110px;">
            <col style="width: 110px;">
            <tbody>
            
            <tr>
             		<td class="thead"><font style="font: bolder;">PERIODE</font></td>
                    <td class="thead"><font style="font: bolder;">PERUSAHAAN</font></td>
                    <td class="thead"><font style="font: bolder;">TGL INVOICE</font> </td>
                    <td class="thead"><font style="font: bolder;">JML INVOICE</font> </td>
                    <td class="thead"><font style="font: bolder;">TGL BAYAR</font> </td>
                    <td class="thead"><font style="font: bolder;">JML BAYAR</font> </td>
                    <td class="thead"><font style="font: bolder;">ACTION</font> </td>
                </tr>
                
		<c:set var="backGroundColor" value="#EAEAEA"/>
		<c:set var="textColor" value="black"/>
		<c:set var="titleAlasan" value=""/>
		<c:forEach var="companyWsList" items="${companyWsList}" varStatus="xt">
		<c:choose>
        		<c:when test='${"#EAEAEA" == 	backGroundColor}'>
        			<c:set var="backGroundColor" value="#FFFFFF"/>
        		</c:when>
        		<c:otherwise>
            		<c:set var="backGroundColor" value="#EAEAEA"/>
        		</c:otherwise>
    		</c:choose>
    		
		   <tr class="rowClass" style=" background-color: ${backGroundColor};"  valign="top" id="barisTable">
		      <td>
                    <label for="companyWsList[${xt.index}].PERIODE" >
                    ${companyWsList.PERIODE}
                    </label>
                </td>
                <td>
                    <label for="companyWsList[${xt.index}].MCL_FIRST" >
                    ${companyWsList.MCL_FIRST}
                    </label>
                </td>
                
                  <td>
                    <label for="companyWsList[${xt.index}].TGL_INVOICE" >
                    <fmt:formatDate value="${companyWsList.TGL_INVOICE}" pattern="dd/MM/yyyy" />
                    </label>
                </td>
                
                 <td>
                    <label for="companyWsList[${xt.index}].JUMLAH_INVOICE" >
                  Rp.    <fmt:formatNumber pattern="#,###.00" value="${companyWsList.JUMLAH_INVOICE}" type="currency" />
                    </label>
                </td>
                
              <td>
               <c:choose>
                  <c:when test="${companyWsList.TGL_BAYAR eq null}">
               		-
                  </c:when>
                  <c:otherwise>
                    <label for="companyWsList[${xt.index}].TGL_BAYAR" >
                    	<fmt:formatDate value="${companyWsList.TGL_BAYAR}" pattern="dd/MM/yyyy" />
                    </label>
                  </c:otherwise>
                </c:choose>
                </td>
                
                  <td>
                  
                  <c:choose>
                  <c:when test="${companyWsList.JUMLAH_BAYAR eq '0'}">
               		-
                  </c:when>
                  <c:otherwise>
                     <label for="companyWsList[${xt.index}].JUMLAH_BAYAR" >
                  Rp.    <fmt:formatNumber pattern="#,###.00" value="${companyWsList.JUMLAH_BAYAR}" type="currency" />
                    </label>
                  </c:otherwise>
                </c:choose>
                  
                </td>
                
                   <td>
                   <input type="hidden" value=" <fmt:formatDate value="${companyWsList.TGL_INVOICE}" pattern="dd/MM/yyyy" />" id="tglFmt" name="tglFmt">
                  
                   <input type="button" value="Input Tgl Bayar" name="input_tgl_bayar"  
                   onClick="input_bayar_ws('${companyWsList.NOMOR}','${companyWsList.PERIODE}','<fmt:formatDate value="${companyWsList.TGL_INVOICE}" pattern="dd/MM/yyyy" />','${companyWsList.JUMLAH_INVOICE}');"
                   <c:if test="${companyWsList.JUMLAH_BAYAR != '0'}" > disabled="disabled" </c:if>>  
                </td>
            </tr>
		</c:forEach>
		
		</tbody>
		</colgroup>
		</table>
		
</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>