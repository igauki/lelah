<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript">
	hideLoadingMessage();
	
	function backToParent(spaj, polis,region){
		if(self.opener.document.frmParam){
			self.opener.document.getElementById('infoFrame').src='${path}/worksite/viewer_worksite.htm?window=kwitansi&spaj='+spaj;
		}
		else if(self.opener.document.getElementById('infoFrame') && self.opener.document.formpost.spaj){
					if(document.formpost.flag.value=='1')
						self.opener.document.getElementById('infoFrame').src='${path}/worksite/worksite.htm';
					else{
						self.opener.document.getElementById('spaj').value=spaj; 
						self.opener.document.getElementById('infoFrame').src='${path}/worksite/worksite.htm';
					}
		
					if(self.opener.document.formpost.spaj.type=='select-one')
						addOptionToSelect(self.opener.document, self.opener.document.formpost.spaj, spaj + " - " + polis, spaj);
				}else if(self.opener.document.formCari.txtnospaj){
					self.opener.document.frm_search.txtnospaj.value=spaj;
				}	
		window.close();	
	}
</script>
</head>
<BODY onload="document.formpost.kata.select(); document.title='PopUp :: Cari SPAJ';" style="height: 100%;">

<form method="post" name="formpost" action="${path }/worksite/cariworksite.htm" style="text-align: center;">
	<div id="contents">
	<fieldset>
		<legend>Cari SPAJ</legend>
		<input type="hidden" name="posisi" value="${param.posisi}">
		<table class="entry">
			<tr>
				<th>Cari:</th>
				<th>
					<select name="tipe">
						<option value="0" <c:if test="${param.tipe eq \"0\" }">selected</c:if>>No. SPAJ</option>
						<option value="1" <c:if test="${param.tipe eq \"1\" }">selected</c:if>>No. Polis</option>
						<option value="2" <c:if test="${param.tipe eq \"2\" }">selected</c:if>>Pemegang</option>
						<option value="3" <c:if test="${param.tipe eq \"3\" }">selected</c:if>>Tertanggung</option>
					</select>
					<select name="pilter">
						<option <c:if test="${\"LIKE%\" eq param.pilter}">selected</c:if> value="LIKE%">LIKE%</option>
						<option <c:if test="${\"%LIKE\" eq param.pilter}">selected</c:if> value="%LIKE">%LIKE</option>
						<option <c:if test="${\"%LIKE%\" eq param.pilter}">selected</c:if> value="%LIKE%">%LIKE%</option>
						<option <c:if test="${\"LT\" eq param.pilter}">selected</c:if> value="LT"><</option>
						<option <c:if test="${\"LE\" eq param.pilter}">selected</c:if> value="LE"><=</option>
						<option <c:if test="${\"EQ\" eq param.pilter}">selected</c:if> value="EQ">=</option>
						<option <c:if test="${\"GE\" eq param.pilter}">selected</c:if> value="GE">>=</option>
						<option <c:if test="${\"GT\" eq param.pilter}">selected</c:if> value="GT">></option>
					</select>					
					<input type="text" name="kata" size="40" value="${param.kata }" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
					<input type="submit" name="search" value="Search" onclick="createLoadingMessage();">
				</th>
			</tr>
		</table>
		<table class="simple">
			<thead>
				<tr>
					<th>No. SPAJ</th>
					<th>No. Polis</th>
					<th>Pemegang Polis</th>
					<th>Tertanggung</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="spaj" items="${cmd.listSpaj}" varStatus="stat">
					<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
						onclick="backToParent('${spaj.reg_spaj }', '${spaj.cariSpaj.mspo_policy_no}' , '${spaj.cariSpaj.lsrg_nama}');">
						<td><elions:spaj nomor="${spaj.reg_spaj }"/></td>
						<td><elions:polis nomor="${spaj.cariSpaj.mspo_policy_no}"/></td>
						<td>${spaj.cariSpaj.pp}</td>
						<td>${spaj.cariSpaj.tt}</td>
					</tr>
					<c:set var="jml" value="${stat.count}"/>
					<c:if test="${stat.count eq 1}">
						<c:set var="v1" value="${spaj.reg_spaj}"/>
						<c:set var="v2" value="${spaj.cariSpaj.mspo_policy_no}"/>
						<c:set var="v3" value="${spaj.cariSpaj.lsrg_nama}"/>
					</c:if>
				</c:forEach>
			</tbody>
		</table>
		<br>
		<input type="hidden" name="flag" value="${cmd.flag }" >
		<input type="button" name="close" value="Close" onclick="window.close();">
		<br>
		<br>
	</fieldset>
	</div>
</form>
<c:if test="${jml eq 1}">
<script>backToParent('${v1}', '${v2}' , '${v3}');</script>
</c:if>
</body>
<%@ include file="/include/page/footer.jsp"%>