<%@ include file="/include/page/header.jsp"%>
<style type="text/css">
	html,body{
	     overflow: hidden;
	}
</style>
<script>

function Right(str, n){
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}


function DateAdd(objDate, intDays)
{
  var iSecond=1000;	 // Dates are represented in milliseconds
  var iMinute=60*iSecond;
  var iHour=60*iMinute;
  var iDay=24*iHour;
  var objReturnDate=new Date();
  objReturnDate.setTime(objDate.getTime()+(intDays*iDay));
  return objReturnDate;
}

function listtgl()
{
	var tanggal_beg = document.frmParam.tgl1.value;
	
	var arrMonthDays = new Array (	"31", "29", "31",
					"30", "31", "30",
					"31", "31", "30",
					"31","30","31"	  );

		var tahun_s = tanggal_beg.substring(6,10);
		var bulan_s = tanggal_beg.substring(3,5);
		var tanggal_s = tanggal_beg.substring(0,2);
		var tanggal  = tanggal_s+"/"+bulan_s+"/"+tahun_s;
		
	if ((bulan_s==2)&&  (tahun_s%4>0) ) {
	    tanggal_s=28;
	}else{
		tanggal_s=arrMonthDays[bulan_s-1];
	}	
	tanggal  = tanggal_s+"/"+bulan_s+"/"+tahun_s;

	document.frmParam._tgl2.value=tanggal;
	document.frmParam.tgl2.value=tanggal;

}

function caridaftar()
{
	if ( document.frmParam.tgl1.value=="__/__/____" || document.frmParam.tgl1.value=="")
	{
		alert('Silahkan isi tanggal terlebih dahulu');
	}else{
		var tanggal_beg = document.frmParam.tgl1.value;
		var tahun_s = tanggal_beg.substring(6,10);
		var bulan_s = tanggal_beg.substring(3,5);
		var tanggal_s = tanggal_beg.substring(0,2);
		var tanggal1  = tahun_s+bulan_s+tanggal_s;
		var tanggal_end = document.frmParam.tgl2.value;
		tahun_s = tanggal_end.substring(6,10);
		bulan_s = tanggal_end.substring(3,5);
		tanggal_s = tanggal_end.substring(0,2);
		var tanggal2  = tahun_s+bulan_s+tanggal_s;
		var formats = document.frmParam.formats;
		var format_ = 'pdf';
		for(i=0; i<formats.length; i++) if(formats[i].checked) format_ = formats[i].value;
		//popWin('${path}/report/uw.htm?window=list_peserta&show=html&tgl1=20060801&tgl2=20060901&customer=010000000092', 480, 640);
		popWin('${path}/report/uw.'+format_+'?window=list_peserta_lanjutan&show=pdf&tgl1='+tanggal1+'&tgl2='+tanggal2+'&customer='+document.frmParam.namaperusahaan.value,500, 800);
		
		//popWinToolbar("${path}/report/uw.htm?window=list_peserta&tgl1="+tanggal1+"&tgl2="+tanggal2+"&customer="+document.frmParam.namaperusahaan.value+"&show=pdf", 500,800);
		//popWinToolbar("../../ReportServer/?rs=WORKSITE/ListPeserta&new=true&1tanggal1="+tanggal1+"&1id="+document.frmParam.namaperusahaan.value+"&1tanggal2="+tanggal2, 500,800);	
	
	}
	
}

	
function listtgl2()
{
	tanggal = document.frmParam._tgl2.value;
	document.frmParam.tgl2.value=tanggal;
}
	
</script>
<body 
	onload="setFrameSize('infoFrame', 68);"
	onresize="setFrameSize('infoFrame', 68);" style="height: 100%;">
<form name="frmParam" method="post">
<div class="tabcontent">
    <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  >
	   </th>
      </tr>
      <tr> 
        <th width="13%" >Nama Perusahaan</th>
        <th width="83%">
			<select name="namaperusahaan" tabindex="14">
					<option value="">NONE</option>
					<c:forEach var="r" items="${listperusahaan}" varStatus="perush"> 
					<option value="${r.MCL_ID}">${r.MCL_FIRST}</option> 
					</c:forEach> 
			</select> 

		</th>
      </tr>
      <tr> 
        <th >Tanggal</th>
        <th>
              <script>inputDate('tgl1', '', false,'listtgl();');</script>
		
			s/d
              <script>inputDate('tgl2', '', false,'listtgl2();');</script>

		</th>
      </tr>
      <tr> 
        <th >Format</th>
        <th>
			<input type="radio" name="formats" value="pdf" class="noborder"> PDF
			<input type="radio" name="formats" value="xls" class="noborder"> Excel
			<input type="radio" name="formats" value="html" class="noborder"> HTML
		</th>
      </tr>
      <tr> 
        <th colspan="2" ><input type="button" value="View" name="search" onClick="caridaftar();"> 
		</th>
      </tr>
      <tr> 
        <td> </td>
      </tr>

    </table>
</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>