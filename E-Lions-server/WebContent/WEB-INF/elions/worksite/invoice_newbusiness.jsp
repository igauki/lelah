<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path}/include/js/disableKeys.js"></script>
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script>
	hideLoadingMessage();
	
</script>
</head>
<body style="height: 100%;">
				<form id="formpost" name="formpost" method="post" enctype="multipart/form-data">
					<fieldset>
						<legend>Invoice New Business</legend>
						<table class="entry2">
							<tr>
								<th>
									To:
								</th>
								<td>
									<input type="text" name="emailto" size="100" value="${emailto}" style="text-transform: none;">
								</td>
							</tr>
							<tr>
								<th>
									Cc:
								</th>
								<td>
									<input type="text" name="emailcc" size="100" value="${emailcc}" style="text-transform: none;">
									<br/>
								</td>
							</tr>
							<tr>
								<td align="right">*<br/>&nbsp;</td>
								<td>
									Harap isi alamat email dengan benar.
									<br/>Untuk lebih dari satu alamat email, pisahkan dengan tanda " ; "
								</td>
							</tr>
							<tr>
								<th>
									Subject:
								</th>
								<td>
									<input type="text" name="emailsubject2" size="100" value="${emailsubject}" disabled="disabled" style="text-transform: none;">
									<input type="hidden" name="emailsubject" size="100" value="${emailsubject}">
								</td>
							</tr>
							<tr>
								<th>
									Attachment:
								</th>
								<td>
									<input type="text" name="emailattach2" size="100" value="${emailattach}" disabled="disabled" style="text-transform: none;">
									<input type="hidden" name="emailattach" size="100" value="${emailattach}">
								</td>
							</tr>
							<tr>
								<th>
									Attach New File:
								</th>
								<td>
									<input type="file" name="file1" size="50" />
								</td>
							</tr>
							<tr>
								<th>
									Message:
								</th>
								<td>
									<textarea rows="12" cols="125" name="emailmessage" style="text-transform: none;">${emailmessage}</textarea>
								</td>
							</tr>
							<tr>
								<th></th>
								<td>
									<c:if test="${not empty errors}">
										<div id="error">${errors}</div>
									</c:if>
									<input type="submit" name="kirim" value="Kirim" onclick="return confirm('Apakah anda sudah yakin untuk mengirim Email tersebut?');"
										accesskey="V" onmouseover="return overlib('Alt-V', AUTOSTATUS, WRAP);" onmouseout="nd();">
								</td>
							</tr>
						</table>
					</fieldset>				
				</form>
</body>
</html>