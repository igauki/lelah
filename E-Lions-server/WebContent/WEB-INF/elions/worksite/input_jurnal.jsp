<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript">
	hideLoadingMessage();
	
	function backToParent(periode){
		self.opener.document.frmParam.show.click();
		alert ('${cmd.noPre}');
		//self.opener.document.getElementById('infoFrame').src='${path}/worksite/viewer_worksite.htm?window=list_summary_komisi&periode='+periode;
		window.close();	
	}
	function updateNominal(){
		var edit_nominal = document.getElementById('editNominalJurnal').value;
		var jenis = document.getElementById('jenisNominalJurnal').value;
		var periode = document.getElementById('periodeCd').value;
		if( jenis == '1'){
			jenis = 'kelebihan bayar';
		}else if(jenis == '2'){
			jenis = 'kekurangan bayar';
		}
		if( edit_nominal == '' ){
			alert('kolom Besarnya update Nominal harus diisi');
		}else{
		var format_edit_nominal = formatCurrencyRupiah(edit_nominal);
		if(confirm('Jenis = '+ jenis + '\nPeriode = ' + periode + '\nEdit sebesar = Rp '+ format_edit_nominal )){
			document.getElementById('event').value='update';
			formpost.submit();		
		}
		}
	}
</script>
</head>
<BODY onload="document.title='PopUp :: Input jurnal';" style="height: 100%;">

<form method="post" name="formpost" style="text-align: center;">
	<div id="contents">
	<fieldset>
		<legend>Input Jurnal</legend>
		<input type="hidden" name="posisi" value="${param.posisi}">
		<table class="entry">
		<tr>
		<th>Perusahaan :</th>
			<th>${cmd.perusahaan}</th>
		</tr>
		<tr>
		<th>Pemilik rekening :</th>
			<th>${cmd.nama_rek}</th>
		</tr>
			<tr>
		<th>Data Rekening :</th>
			<th>${cmd.no_rek}</th>
		</tr>
		<tr>
		<th></th>
			<th>${cmd.bank_descr}</th>
		</tr>
		
		
		
		<tr>
		<th>Nominal :</th>
			<th>   Rp.    <fmt:formatNumber pattern="#,###.00" value="${cmd.nominal}" type="currency" /></th>
		</tr>
		
		<tr>
		<th>Jenis Badan :</th>
			<th>${cmd.jenisBdn}</th>
		</tr>
		
		<tr>
		<th>NPWP :</th>
			<th>${cmd.npwp}</th>
		</tr>
		
			<tr>
				<th>Bank :</th>
				<th>
				<select name="bankId" id="bankId" tabindex="14" <c:if test="${cmd.no_pre != ''}" > disabled="disabled" </c:if> >
					<c:forEach var="r" items="${cmd.bankList}" varStatus="bank">
					    <option value="${r.ID}" <c:if test="${r.ID eq cmd.bankId }">Selected</c:if>>
                            ${r.BANK}
                        </option>
					</c:forEach>
				</select>			
					
				</th>
			</tr>
			
			
			<tr>
				<th>No Giro :</th>
				<th>
				<input type="text" name="giro" id="giro" value="${cmd.giro}" <c:if test="${cmd.no_pre != ''}" > disabled="disabled" </c:if> />
				</th>
			</tr>
			
			
			 <c:if test="${cmd.no_pre != ''}" >
			 
			 <tr>
			 <td>
			 </td>
			</tr>
			<br>
				<br>
				<tr>
		<th>No Pre :</th>
			<th>${cmd.no_pre}
		</tr>
		<tr>
		<th>Bkt ptg pajak :</th>
			<th>${cmd.tax_no}
		</tr>
		<tr>
		<table id="thetable" width="100%" align="center" border=1 bordercolor="BLACK;" cellSpacing=1 class="nobottom" style="table-layout: fixed;" >
		<colgroup>
            <col style="width: 180px;">
            <col style="width: 60px;">
            <col style="width: 20px;">
            <col style="width: 80px;">
            <col style="width: 30px;">
            <tbody>
             <tr>
                	<td class="thead"><font style="font: bolder;">KETERANGAN</font></td>
                    <td class="thead"><font style="font: bolder;">GIRO</font></td>
                    <td class="thead"><font style="font: bolder;">C/D</font> </td>
                    <td class="thead"><font style="font: bolder;">JUMLAH</font> </td>
                    <td class="thead"><font style="font: bolder;"> KODE</font> </td>
                </tr>
                
                <c:forEach var="listMstDBank" items="${cmd.listMstDBank}" varStatus="xt">
                <tr class="rowClass" valign="top" id="barisTable">
                 <td>
                    <label for="listMstDBank[${xt.index}].KETERANGAN" >
                    ${listMstDBank.KETERANGAN}
                    </label>
                </td>
                 <td>
                 <c:choose>
                 <c:when test="${listMstDBank.GIRO eq null}">
                 -
                 </c:when>
                 <c:otherwise>
                    <label for="listMstDBank[${xt.index}].GIRO" >
                    ${listMstDBank.GIRO}
                    </label>
                    </c:otherwise>
                    </c:choose>
                </td>
                 <td>
                    <label for="listMstDBank[${xt.index}].KAS" >
                    ${listMstDBank.KAS}
                    </label>
                </td>
                 <td>
                    <label for="listMstDBank[${xt.index}].JUMLAH" >
                    ${listMstDBank.JUMLAH}
                    </label>
                </td>
                 <td>
                 <c:choose>
                 <c:when test="${listMstDBank.KODE_CASH_FLOW eq null}">
                 -
                 </c:when>
                 <c:otherwise>
                    <label for="listMstDBank[${xt.index}].KODE_CASH_FLOW" >
                    ${listMstDBank.KODE_CASH_FLOW}
                    </label>
                    </c:otherwise>
                    </c:choose>
                </td>
                </tr>
                </c:forEach>
		</table>
		</tr>
		</c:if>
		</table>
<c:if test="${cmd.showDetail == 'true'}" >
		<table id="thetable" width="100%" align="center" border=1 bordercolor="RED" cellSpacing=1 class="nobottom" style="table-layout: fixed;" >
		      <col style="width: 100px;">
            <col style="width: 60px;">
            <col style="width: 60px;">
            <col style="width: 100px;">
            <tbody>
             <tr>
                	<td class="thead"><font style="font: bolder;">JENIS UPDATE</font></td>
                    <td class="thead"><font style="font: bolder;">TANGGAL EDIT</font></td>
                    <td class="thead"><font style="font: bolder;">OLEH</font> </td>
                    <td class="thead"><font style="font: bolder;">JUMLAH EDIT</font> </td>
                </tr>
                 <c:forEach var="listDetailUpdate" items="${cmd.detailUpdateJurnal}" varStatus="xt">
                <tr class="rowClass" valign="top" id="barisTable">
                 <td>
                    <label for="listDetailUpdate[${xt.index}].JENIS_UPDATE" >
                    ${listDetailUpdate.JENIS_UPDATE}
                    </label>
                </td>
                  <td>
                    <label for="listDetailUpdate[${xt.index}].TANGGAL_UPDATE" >
                    ${listDetailUpdate.TANGGAL_UPDATE}
                    </label>
                </td>
                  <td>
                    <label for="listDetailUpdate[${xt.index}].NAMA" >
                    ${listDetailUpdate.NAMA}
                    </label>
                </td>
                  <td>
                    <label for="listDetailUpdate[${xt.index}].JUMLAH_UPDATE" >
                    ${listDetailUpdate.JUMLAH_UPDATE}
                    </label>
                </td>
                </c:forEach>
		</table>
		</c:if>
		<br>
		<input type="hidden" name="flag" value="${cmd.flag }" >
		<input type="hidden" name="event" id="event">
		<input type="submit" name="transfer" value="Transfer" onclick="createLoadingMessage();" <c:if test="${cmd.no_pre != ''}" > disabled="disabled" </c:if>>
		<input type="button" name="close" value="Close" onclick="window.close();">
		<br>
		<br>
	</fieldset>
	</div>
</form>
<script type="text/javascript">
	if( 'sukses' == '${cmd.sukses}' ){
		backToParent('${cmd.periode}');
	}else if( 'gagal' == '${cmd.sukses}' ){
		alert('transfer gagal!');
	}
</script>
</body>
<%@ include file="/include/page/footer.jsp"%>