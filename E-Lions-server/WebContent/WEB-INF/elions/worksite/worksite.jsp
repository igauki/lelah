<%@ include file="/include/page/header.jsp"%>

<script type="text/javascript">
	hideLoadingMessage();
	
function cari(str){
	switch (str) {
	case "daftar" : 
		document.getElementById('infoFrame').src='${path}/worksite/viewer_worksite.htm?window=daftar'; 
		break;
	case "invoice" :
		document.getElementById('infoFrame').src='${path}/worksite/viewer_worksite.htm?window=invoice'; 
		break;
	case "kwitansi_pt" :
		document.getElementById('infoFrame').src='${path}/worksite/viewer_worksite.htm?window=kwitansi_pt'; 
		break;		
	case "kwitansi" :
		//document.getElementById('infoFrame').src='${path}/worksite/viewer_worksite.htm?window=kwitansi'; 
		break;	
	case "list_komisi" :
		document.getElementById('infoFrame').src = '${path}/worksite/viewer_worksite.htm?window=list_summary_komisi'
		break;	
	case "ut" :
		document.getElementById('infoFrame').src='${path}/report/uw.htm?window=undertable';
		break;
	case "billing_info" :
		document.getElementById('infoFrame').src='${path}/worksite/viewer_worksite.htm?window=billing_info';
		break;
	case "tgl invoice bayar" :
		document.getElementById('infoFrame').src='${path}/worksite/viewer_worksite.htm?window=tgl_invoice_bayar';
		break;
	}
}
	
	

</script>
<body style="height: 100%;"
	onload="setFrameSize('infoFrame', 44);"
	onresize="setFrameSize('infoFrame', 44);">
<form name="frmParam" method="post">

<div class="tabcontent">
<table class="entry" width="98%">
	<tr>
		<th>
			Window Worksite
		</th>
		<td>
			<input type="button" value="Daftar Karyawan" name="search" onClick="cari('daftar');"> 
			<input type="button" value="Invoice" name="search1" onClick="cari('invoice');">
			<input type="button" value="Kwitansi" name="search2" onclick="popWin('${path}/worksite/cariworksite.htm?posisi=6&flag=1', 350, 450); ">
			<input type="button" value="Kwitansi Per Perusahaan" name="search4" onClick="cari('kwitansi_pt');">
			<input type="button" value="List Pengembalian Diskon" name="search3" onClick="cari('ut');">
			<input type="button" value="List Summary Komisi" name="search6" onClick="cari('list_komisi');">
			<input type="button" value="Billing Info" name="search5" onClick="cari('billing_info');">
			<input type="button" value="Tgl Invoice & Bayar" name="search7" onClick="cari('tgl invoice bayar');">
		</td>
		<td><div id="prePostError" style="display:none;"></div></td>
	</tr>
	<tr>
		<td colspan="3">
			<iframe src="${path}/worksite/worksite.htm" name="infoFrame" id="infoFrame"
				width="100%"> Please Wait... </iframe>
		</td>
	</tr>
</table>
</div>
</form>
</body>

<%@ include file="/include/page/footer.jsp"%>