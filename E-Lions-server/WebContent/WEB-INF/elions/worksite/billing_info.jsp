<%@ include file="/include/page/header.jsp"%>
<style type="text/css">
    html, body {
        overflow: hidden;
    }
</style>
<script>

    function tampilkanBillInfo()
    {
        var pesan = '';

        if( document.frmParam.namaperusahaan.value == "NONE" )
        {
            pesan = pesan + 'Silahkan pilih nama perusahaan terlebih dahulu.\n';
        }
        if( document.frmParam.monthFrom.value == "NONE" || document.frmParam.monthTo.value == "NONE")
        {
            pesan = pesan + 'Silahkan pilih bulan dahulu.\n';
        }

        if( pesan == '')
        {
            frmParam.submit();
        }
        else
        {
            alert( pesan );
        }
    }

    function downloadFile(downloadFileSession)
    {
        downloadFileFrame.document.getElementById('fileName').value = downloadFileSession;
        if(!(downloadFileSession == '' || downloadFileSession == null))
        {
            downloadFileFrame.document.getElementById('downloadFileButton').click();
        }
    }

</script>
<body
        onload="setFrameSize('infoFrame', 68);"
        onresize="setFrameSize('infoFrame', 68);" style="height: 100%; overflow: scroll;">
<form name="frmParam" method="post">
    <div class="tabcontent">
        <table class="entry" width="98%">
            <tr>
                <th colspan="2">

                </th>
            </tr>
            <tr>
                <th width="13%">Nama Perusahaan</th>
                <th width="83%">
                    <select name="namaperusahaan" tabindex="14">
                        <option value="NONE">NONE</option>
                        <c:forEach var="r" items="${companyList}" varStatus="perush">
                            <option value="${r.MCL_ID}" <c:if test="${r.MCL_ID eq namaperusahaan }">Selected</c:if>>
                                ${r.MCL_FIRST}
                            </option>
                        </c:forEach>
			        </select>

		</th>
      </tr>
      <tr>
        <th width="13%" >Periode Pembayaran Diskon</th>
        <th width="83%">
			<select name="monthFrom" tabindex="14">
					<c:forEach var="r" items="${monthList}" varStatus="month">
					    <option value="${r.ID}" <c:if test="${r.ID eq monthFrom }">Selected</c:if>>
                            ${r.VALUE}
                        </option>
					</c:forEach>
			</select>
            <select name="yearFrom" tabindex="14">
					<c:forEach var="r" items="${yearList}" varStatus="year">
					<option value="${r.ID}" <c:if test="${r.ID eq yearFrom }">Selected</c:if>>
                        ${r.VALUE}
                    </option>
					</c:forEach>
			</select>
            S/D
            <select name="monthTo" tabindex="14">
					<c:forEach var="r" items="${monthList}" varStatus="month">
					<option value="${r.ID}" <c:if test="${r.ID eq monthTo }">Selected</c:if>>
                        ${r.VALUE}
                    </option>
					</c:forEach>
			</select>
            <select name="yearTo" tabindex="14">
					<c:forEach var="r" items="${yearList}" varStatus="year">
					<option value="${r.ID}" <c:if test="${r.ID eq yearTo }">Selected</c:if>>
                        ${r.VALUE}
                    </option>
					</c:forEach>
            </select>
        </th>
      </tr>

            <tr>
                <th colspan="2"><input type="button" value="TAMPILKAN BILLING INFO" name="billingInfo"
                                       onClick="tampilkanBillInfo();">
                </th>
            </tr>
            <tr>
                <td></td>
            </tr>

        </table>

        <table style="margin: 0 0 0 0; padding: 0 0 0 0; width: 0; height: 0">
            <tr>
                <td style="width: 0; height: 0">
                    <iframe src="${path}/worksite/download.htm" name="downloadFileFrame" id="downloadFileFrame" width="0" height="0">
                    </iframe>
                </td>
            </tr>
        </table>

        <fieldset>
            <legend>Billing Info</legend>
            <display:table id="baris" name="historyList" class="displaytag" >
                <display:column property="NO" title="HISTORY ID" style="text-align: center;"/>
                <display:column property="PERIODE" title="PERIODE" style="text-align: center;" />
                <display:column property="PROCESS_DATE" title="TGL PROSES" style="text-align: center;" />
                <display:column property="USER_NAME" title="USER" style="text-align: left;" />
                <display:column property="DESCR" title="KETERANGAN" style="text-align: left;" />
                <display:column title="DOWNLOAD FILE" style="text-align: left;">
                    <c:forEach items="${baris.FILE_NAME_ARRAY}" varStatus="perush" >
                        <a href="#" onclick="downloadFile('${baris.FILE_NAME_ARRAY[perush.index]}'); return false;" title="clik disini untuk download">
                            ${baris.FILE_NAME_ARRAY[perush.index]}
                        </a>;
                    </c:forEach>
                </display:column>
            </display:table>
        </fieldset>

    </div>

</form>
</body>
<%@ include file="/include/page/footer.jsp"%>