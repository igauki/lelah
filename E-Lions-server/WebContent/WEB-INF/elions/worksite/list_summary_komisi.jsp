<%@ include file="/include/page/header.jsp"%>
<style type="text/css">
    html, body {
        overflow: hidden;
    }
</style>
<script>

    function tampilkanListSummaryKomisi()
    {
        var pesan = '';
        if( document.frmParam.monthFrom.value == "NONE")
        {
            pesan = pesan + 'Silahkan pilih periode dahulu.\n';
        }
        if( pesan == '')
        {
        
        	document.frmParam.showButton.value = 'true';
            frmParam.submit();
        }
        else
        {
            alert( pesan );
        }
    }

    function tampilkanPdf()
    {
        var pesan = '';
        if( document.frmParam.monthFrom.value == "NONE")
        {
            pesan = pesan + 'Silahkan pilih periode dahulu.\n';
        }
        if( pesan == '')
        {
          popWin('${path}/report/uw.pdf?window=list_summary_komisi&monthFrom='+document.frmParam.monthFrom.value+'&yearFrom='+document.frmParam.yearFrom.value+'&bank='+document.frmParam.bank.value,600,800)
        }
        else
        {
            alert( pesan );
        }
    }

    function downloadFile(downloadFileSession)
    {
        downloadFileFrame.document.getElementById('fileName').value = downloadFileSession;
        if(!(downloadFileSession == '' || downloadFileSession == null))
        {
            downloadFileFrame.document.getElementById('downloadFileButton').click();
        }
    }

</script>
<body
        onload="setFrameSize('infoFrame', 68);"
        onresize="setFrameSize('infoFrame', 68);" style="height: 100%; overflow: scroll;">
<form name="frmParam" method="post">
    <div class="tabcontent">
        <table class="entry" width="98%">
      <tr>
        <th width="13%" >Periode</th>
        <th width="83%">
			<select name="monthFrom" id="monthFrom" tabindex="14">
					<c:forEach var="r" items="${monthList}" varStatus="month">
					    <option value="${r.ID}" <c:if test="${r.ID eq monthFrom }">Selected</c:if>>
                            ${r.VALUE}
                        </option>
					</c:forEach>
			</select>
            <select name="yearFrom" id="yearFrom" tabindex="14">
					<c:forEach var="r" items="${yearList}" varStatus="year">
					<option value="${r.ID}" <c:if test="${r.ID eq yearFrom }">Selected</c:if>>
                        ${r.VALUE}
                    </option>
					</c:forEach>
			</select>
			
        </th>
      </tr>
      <tr>
       <th width="13%">Nama Perusahaan </th>
                <th width="83%">
                <input type="text" id="nama" name="nama" value="${nama}"/>
      </tr>
      
       <tr>
                <th width="13%">Bank </th>
                <th width="83%">
                <input type="text" id="bank" name="bank" value="${bank}"/>
				<input type="hidden" id="showButton" name="showButton"/>
				<input type="hidden" id="showPdf" name="showPdf"/>
		</th>
		
		       <tr>
                <th width="13%">Transfer </th>
                <th width="83%">
			<select name="transfer" id="transfer" tabindex="14">
				<c:forEach var="r" items="${transferList}" varStatus="transferVar">
					<option value="${r.ID}" <c:if test="${r.ID eq transfer }">Selected</c:if>>
                        ${r.VALUE}
                    </option>
				</c:forEach>
			</select>
		</th>
      </tr>

            <tr>
                <th colspan="2"><input type="button" value="Show" name="show" id="show"
                                       onClick="tampilkanListSummaryKomisi();">
                                       &nbsp;&nbsp;
                   <input type="button" value="Print PDF" name="printPdf" id="printPdf" onClick="tampilkanPdf();">
                </th>
                 
            </tr>
            <tr>
                <td></td>
            </tr>

        </table>

        <table style="margin: 0 0 0 0; padding: 0 0 0 0; width: 0; height: 0">
            <tr>
                <td style="width: 0; height: 0">
                    <iframe src="${path}/worksite/download.htm" name="downloadFileFrame" id="downloadFileFrame" width="0" height="0">
                    </iframe>
                </td>
            </tr>
        </table>
    
 <table id="thetable" width="1000" align="center" border=1 cellPadding=2 cellSpacing=1 class="nobottom" style="table-LAYOUT: fixed;" >
        <colgroup>
            <col style="width: 40px;">
            <col style="width: 150px;">
            <col style="width: 120px;">
            <col style="width: 90px;">
            <col style="width: 280px;">
            <col style="width: 100px;">
            <col style="width: 100px;">
            <col style="width: 100px;">
            <col style="width: 100px;">
            <col style="width: 100px;">
             <col style="width: 100px;">
            <tbody>
            
            <tr>
                	<td class="thead"><font style="font: bolder;">ACTION</font></td>
                    <td class="thead"><font style="font: bolder;">PERUSAHAAN</font></td>
                    <td class="thead"><font style="font: bolder;">PEMILIK REKENING</font> </td>
                    <td class="thead"><font style="font: bolder;">NO REKENING</font> </td>
                    <td class="thead"><font style="font: bolder;">DATA REKENING</font> </td>
                    <td class="thead"><font style="font: bolder;">NOMINAL</font> </td>
                     <td class="thead"><font style="font: bolder;">PAJAK</font> </td>
                      <td class="thead"><font style="font: bolder;">NETT</font> </td>
                    <td class="thead"><font style="font: bolder;">PERIODE</font> </td>
                    <td class="thead"><font style="font: bolder;">NO PRE</font> </td>
                      <td class="thead"><font style="font: bolder;">MCL_ID</font> </td>
                </tr>
                
		<c:set var="backGroundColor" value="#EAEAEA"/>
		<c:set var="textColor" value="black"/>
		<c:set var="titleAlasan" value=""/>
		<c:if test="${flag eq '1'}">
		<c:forEach var="listSummaryKomisi" items="${listSummaryKomisi}" varStatus="xt">
		<c:choose>
        		<c:when test='${"#EAEAEA" == 	backGroundColor}'>
        			<c:set var="backGroundColor" value="#FFFFFF"/>
        		</c:when>
        		<c:otherwise>
            		<c:set var="backGroundColor" value="#EAEAEA"/>
        		</c:otherwise>
    		</c:choose>
    		
		   <tr class="rowClass" style=" background-color: ${backGroundColor};"  valign="top" id="barisTable">
				<td align="center">
			 	<img src="${path}/include/image/edit.gif" width="18" 
			 	height="18" style="cursor: pointer;" border="0" title="INPUT JURNAL" 
			 	onclick="popWin('${path}/worksite/input_jurnal.htm?perusahaan=${listSummaryKomisi.MCL_FIRST}&nominal=${listSummaryKomisi.NOMINAL}&bank_descr=${listSummaryKomisi.BANK_DESCR}&periode=${listSummaryKomisi.PERIODE}&mcl_id=${listSummaryKomisi.MCL_ID}&no_pre=${listSummaryKomisi.NO_PRE}&position=${listSummaryKomisi.POSITION}&bank_id=${listSummaryKomisi.MTB_GL_NO}&tax_no=${listSummaryKomisi.TAX_NO}&nama_rek=${listSummaryKomisi.REK_NAMA}&no_rek=${listSummaryKomisi.REK_NO}&nett=${listSummaryKomisi.NETT}&full_periode=${listSummaryKomisi.FULL_PERIODE}&editNominalCd=${listSummaryKomisi.JUMLAH_EDIT_NOMINAL}&jenisNominalCd=${listSummaryKomisi.JENIS_EDIT_NOMINAL}', 500, 500);"/>
			 	</td>
                <td>
                    <label for="listSummaryKomisi[${xt.index}].MCL_FIRST" >
                    ${listSummaryKomisi.MCL_FIRST}
                    </label>
                </td>
                <td>
                <c:choose>
                <c:when test="${listSummaryKomisi.REK_NAMA eq null}">
                -
                </c:when>
                <c:otherwise>
                    <label for="listSummaryKomisi[${xt.index}].REK_NAMA" >
                    ${listSummaryKomisi.REK_NAMA}
                      
                    </label>
                    </c:otherwise>
                </c:choose>
                </td>
                <td>
                <c:choose>
                <c:when test="${listSummaryKomisi.REK_NO eq null}">
                -
                </c:when>
                <c:otherwise>
                    <label for="listSummaryKomisi[${xt.index}].REK_NO" >
                    ${listSummaryKomisi.REK_NO}
                    </label>
                  </c:otherwise>
                  </c:choose>
                </td>
                
                  <td>
                    <label for="listSummaryKomisi[${xt.index}].BANK_DESCR" >
                    ${listSummaryKomisi.BANK_DESCR}
                        
                    </label>
                </td>
                
                <td>
                    <label for="listSummaryKomisi[${xt.index}].NOMINAL" >
                   Rp.  <fmt:formatNumber pattern="#,###.00" value="${listSummaryKomisi.NOMINAL}" type="currency" />
                    </label>
                </td>
                    <td>
                    <label for="listSummaryKomisi[${xt.index}].PAJAK" >
                    Rp.   <fmt:formatNumber pattern="#,###.00" value="${listSummaryKomisi.PAJAK}" type="currency" />
                    </label>
                </td>
                    <td>
                    <label for="listSummaryKomisi[${xt.index}].NETT" >
                  Rp.    <fmt:formatNumber pattern="#,###.00" value="${listSummaryKomisi.NETT}" type="currency" />
                    </label>
                </td>
                <td>
                    <label for="listSummaryKomisi[${xt.index}].FULL_PERIODE" >
                    	${listSummaryKomisi.FULL_PERIODE}
                    </label>
                </td>
                  <td>
                  <c:choose>
                  <c:when test="${listSummaryKomisi.NO_PRE eq null}">
                -
                    </c:when>
                    <c:otherwise>
                        <label for="listSummaryKomisi[${xt.index}].NO_PRE" >
                    	${listSummaryKomisi.NO_PRE}
                    </label>
                    </c:otherwise>
                    </c:choose>
                    </td>
                     <td>
                    <label for="listSummaryKomisi[${xt.index}].MCL_ID" >
                    ${listSummaryKomisi.MCL_ID}
                        
                    </label>
                </td>
            </tr>
		</c:forEach>
		</c:if>
		
		</tbody>
		</colgroup>
		</div>
</form>

    <script type="text/javascript">
        if( '' != '${alert}' )
        {
            alert( '${alert}');
        }
    </script>
</body>
<%@ include file="/include/page/footer.jsp"%>