<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript">
	hideLoadingMessage();
	
	function backToParent(mcl_id){
		alert('insert succesfully');
		self.opener.document.frmParam.show.click();
		//self.opener.document.getElementById('infoFrame').src='${path}/worksite/viewer_worksite.htm?window=list_summary_komisi&periode='+periode;
		window.close();	
	}
	
</script>
</head>
<BODY onload="document.title='PopUp :: Input Invoice';" style="height: 100%;">

<form method="post" name="formpost" style="text-align: center;">
	<div id="contents">
	<fieldset>
		<legend>Input Tanggal Invoice</legend>
		<input type="hidden" name="posisi" value="${param.posisi}">
		<table class="entry">
		<tr>
		<th>Perusahaan :</th>
			<th>${cmd.nama_perusahaan}</th>
		</tr>
		 <tr>
        <th width="13%" >Tgl Invoice :</th>
        <th width="83%">
        <select name="dayId" id="dayId" tabindex="14">
					<c:forEach var="r" items="${cmd.dayList}" varStatus="day">
					    <option value="${r.ID}" <c:if test="${r.ID eq cmd.dayId }">Selected</c:if>>
                            ${r.VALUE}
                        </option>
					</c:forEach>
			</select>
			
			<select name="monthId" id="monthId" tabindex="14">
					<c:forEach var="r" items="${cmd.monthList}" varStatus="month">
					    <option value="${r.ID}" <c:if test="${r.ID eq cmd.monthId }">Selected</c:if>>
                            ${r.VALUE}
                        </option>
					</c:forEach>
			</select>
            <select name="yearId" id="yearId" tabindex="14">
					<c:forEach var="r" items="${cmd.yearList}" varStatus="year">
					<option value="${r.ID}" <c:if test="${r.ID eq cmd.yearId }">Selected</c:if>>
                        ${r.VALUE}
                    </option>
					</c:forEach>
			</select>
			
        </th>
      </tr>
      
      
       <tr>
        <th width="13%" >Periode :</th>
        <th width="83%">
			
			<select name="monthPeriode" id="monthPeriode" tabindex="14">
					<c:forEach var="r" items="${cmd.monthList}" varStatus="month">
					    <option value="${r.ID}" <c:if test="${r.ID eq cmd.monthPeriode }">Selected</c:if>>
                            ${r.VALUE}
                        </option>
					</c:forEach>
			</select>
            <select name="yearPeriode" id="yearPeriode" tabindex="14">
					<c:forEach var="r" items="${cmd.yearList}" varStatus="year">
					<option value="${r.ID}" <c:if test="${r.ID eq cmd.yearPeriode }">Selected</c:if>>
                        ${r.VALUE}
                    </option>
					</c:forEach>
			</select>
			
        </th>
      </tr>
			
			
			<tr>
				<th>Jumlah :</th>
				<th>
				<input type="text" name="jumlahInvoice" id="jumlahInvoice" value="${cmd.jumlahInvoice}"/>
				</th>
			</tr>
			
			
		</table>
		<br>
		<input type="hidden" name="flag" value="${cmd.flag }" >
		<input type="submit" name="submit" id="submit" value="Submit" onclick="createLoadingMessage();" >
		<input type="button" name="close" value="Close" onclick="window.close();">
		<br>
		<br>
	</fieldset>
	</div>
</form>
<script type="text/javascript">
	if( 'sukses' == '${cmd.sukses}' ){
		backToParent('${cmd.mcl_id}');
	}else if( 'gagal' == '${cmd.sukses}' ){
		alert('transfer gagal!');
	}
</script>
</body>
<%@ include file="/include/page/footer.jsp"%>