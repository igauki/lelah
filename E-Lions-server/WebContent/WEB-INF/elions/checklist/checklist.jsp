<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<meta HTTP-EQUIV="Expires" CONTENT="-1">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link href="${path}/include/image/eas.ico" rel="shortcut icon">
		<!--  -->
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<!--  -->
		<script type="text/javascript" src="${path }/include/js/default.js"></script>
		<!-- Ajax Related -->
		<script type="text/javascript" src="${path}/include/js/ajax.js"></script>
		<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script>
		<script type="text/javascript" src="${path }/dwr/engine.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>
		<script type="text/javascript">
			hideLoadingMessage();
			
			<c:if test="${param.sukses eq 1}">
				alert('Data Berhasil Disimpan');
			</c:if>
			
			<c:if test="${param.sukses eq 0}">
				alert('Data Tidak Berhasil Disimpan. Harap Input Checklist Terlebih Dahulu.');
			</c:if>
			
			
			function kopi(src, dest){
				var tot = 500;
				for(i=1; i<tot; i++){ //mulai dari 1, karena yg 0 tidak ditampilkan
					sumber = document.formpost.elements['listChecklist['+i+'].'+src];
					tujuan = document.formpost.elements['listChecklist['+i+'].'+dest];

					if(sumber && tujuan) {
						sumberAsli = document.formpost.elements['listChecklist['+i+'].'+src][0];
						sumberScan = document.formpost.elements['listChecklist['+i+'].'+src][1];
						tujuanAsli = document.formpost.elements['listChecklist['+i+'].'+dest][0];
						tujuanScan = document.formpost.elements['listChecklist['+i+'].'+dest][1];
						
						if(sumberAsli && tujuanAsli && sumberScan && tujuanScan) {
							if(sumberAsli.checked) tujuanAsli.checked = true;
							if(sumberScan.checked) tujuanScan.checked = true;
						}
					}else break;
				}
			}
			
		</script>
	</head>
	<body onload="setupPanes('container1','tab1');" style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Check List Dokumen Polis</a>
				</li>
			</ul>

			<div class="tab-panes">

				<div id="pane1" class="panes">
					<form:form commandName="cmd" name="formpost" id="formpost">
						<fieldset>
							<table class="entry2" style="width: auto;">
								<tr>
									<th nowrap="nowrap">Nomor Register SPAJ</th>
									<td>
										<c:choose>
											<c:when test="${empty cmd.reg_spaj}">
												<form:input path="reg_spaj" size="18" id="espeaje"
													onkeyup="if (window.event) if(window.event.keyCode == 13) document.getElementById('tampol').click();"
												/>
												<input type="button" value="Tampilkan" id="tampol"
													onclick="window.location='${path}/checklist.htm?reg_spaj='+document.getElementById('espeaje').value;">
											</c:when>
											<c:otherwise>
												<form:input path="reg_spaj" size="18" readonly="true" cssClass="readOnly"/>
											</c:otherwise>
										</c:choose>
									</td>
									<c:if test="${cmd.flagbancass eq 1}">
									<th>
										<td>
											<input class="noBorder" type="checkbox" name="centang" id="print" value="1">
										</td>
									</th>
									<th>Check Box untuk Check List Print Polis</th>
									</c:if>
								</tr>
								<spring:hasBindErrors name="cmd">
									<tr>
										<td colspan="2">
											<div id="error">
												<strong>Data yang dimasukkan tidak lengkap. Mohon lengkapi data-data berikut:</strong>
												<br />
												<form:errors path="*" delimiter="<br>" />
											</div>
										</td>
									</tr>
								</spring:hasBindErrors>
								<tr>
									<td colspan="2">
										<table class="displaytag">
											<thead>
												<tr>
													<th colspan="2" rowspan="2">Nama / Tanggal</th>
													<th style="width: 100px; color: red;">Admin: ${cmd.listChecklist[0].user_adm}</th>
													<c:if test="${cmd.lca_id ne '58'}">
														<th style="width: 100px; color: red;">Bancass: ${cmd.listChecklist[0].user_bancass}</th>
													</c:if>	
													<th style="width: 100px; color: red;">UW: ${cmd.listChecklist[0].user_uw}</th>
													<th style="width: 100px; color: red;">Print: ${cmd.listChecklist[0].user_print}</th>
													<th style="width: 100px; color: red;">Filling: ${cmd.listChecklist[0].user_filling}</th>
													<th rowspan="3">Keterangan</th>
												</tr>
												<tr>
													<th style="color: blue;">Tgl: <fmt:formatDate pattern="dd/MM/yyyy" value="${cmd.listChecklist[0].tgl_adm}" /></th>
													<c:if test="${cmd.lca_id ne '58'}">
														<th style="color: blue;">Tgl: <fmt:formatDate pattern="dd/MM/yyyy" value="${cmd.listChecklist[0].tgl_bancass}" /></th>
													</c:if>
													<th style="color: blue;">Tgl: <fmt:formatDate pattern="dd/MM/yyyy" value="${cmd.listChecklist[0].tgl_uw}" /></th>
													<th style="color: blue;">Tgl: <fmt:formatDate pattern="dd/MM/yyyy" value="${cmd.listChecklist[0].tgl_print}" /></th>
													<th style="color: blue;">Tgl: <fmt:formatDate pattern="dd/MM/yyyy" value="${cmd.listChecklist[0].tgl_filling}" /></th>
												</tr>
												<tr>
													<th colspan="2">Jenis Dokumen</th>
													<th>Asli/Scan/Tidak Ada</th>
													<c:if test="${cmd.lca_id ne '58'}">
														<th>Asli/Scan/Tidak Ada</th>
													</c:if>
													<th>Asli/Scan/Tidak Ada</th>
													<th>Asli/Scan/Tidak Ada</th>
													<th>Asli/Scan/Tidak Ada</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td></td>
													<c:if test="${cmd.lca_id ne '58'}">
														<td></td>
													</c:if>
													
													<c:choose>
														<c:when test="${cmd.lspd_id eq 1}">
															<td></td>
															<td></td>
															<td></td>
															<td></td>
														</c:when>
														<c:when test="${cmd.lspd_id eq 2}">
															<td style="text-align: center;"><input type="button" value="Copy &raquo;" onclick="kopi('flag_adm', 'flag_uw');"></td>
															<td></td>
															<td></td>
															<td></td>
														</c:when>
														<c:when test="${cmd.lspd_id eq 6}">
															<td></td>
															<td style="text-align: center;"><input type="button" value="Copy &raquo;" onclick="kopi('flag_uw', 'flag_print');"></td>
															<td></td>
															<td></td>
														</c:when>
														<c:when test="${cmd.lspd_id eq 7}">
															<td></td>
															<td></td>
															<td style="text-align: center;"><input type="button" value="Copy &raquo;" onclick="kopi('flag_print', 'flag_filling');"></td>
															<td></td>
														</c:when>
														<c:otherwise>
															<td style="text-align: center;"><input type="button" value="Copy &raquo;" onclick="kopi('flag_adm', 'flag_uw');"></td>
															<td style="text-align: center;"><input type="button" value="Copy &raquo;" onclick="kopi('flag_uw', 'flag_print');"></td>
															<td style="text-align: center;"><input type="button" value="Copy &raquo;" onclick="kopi('flag_print', 'flag_filling');"></td>
															<td></td>
														</c:otherwise>
													</c:choose>
													
													<td></td>
												</tr>
												<c:set var="urut" value="0" />
												<c:forEach items="${cmd.listChecklist}" var="c" varStatus="s">
													<c:choose>
														<c:when test="${cmd.lca_id eq '58'}">
															<c:if test="${((c.lc_parent_id eq 0) or (c.lc_parent_id eq 2) or (c.lc_parent_id eq 15) or (c.lc_parent_id eq 19) or (c.lc_parent_id eq 20) or (c.lc_parent_id eq 58) or (c.lc_parent_id eq 27) or (c.lc_parent_id eq 36)) and (c.lc_id ne 8) and (c.lc_id ne 26) and (c.lc_id ne 32) and (c.lc_id ne 29) and (c.lc_id ne 55) and (c.lc_id ne 31) and (c.lc_id ne 34) and (c.lc_id ne 37) and  (c.lc_id lt 61) }">
																<tr>
																	<td style="text-align: left;">
																		<c:if test="${c.level eq 2}">
																			<c:set var="urut" value="${urut + 1}" />
																			${urut}. 
																		</c:if>
																	</td>
																	<td style="text-align: left;"><span>${c.lc_nama}</span></td>
																	
																	<c:choose>
																		<c:when test="${cmd.lspd_id eq 1}">
																			<td style="text-align: center; background-color: #ffffcc;">
																				<label for="adm1_${s.index}"><form:radiobutton cssClass="noBorder" id="adm1_${s.index}" path="listChecklist[${s.index}].flag_adm" value="1"/></label>
																				<label for="adm2_${s.index}"><form:radiobutton cssClass="noBorder" id="adm2_${s.index}" path="listChecklist[${s.index}].flag_adm" value="2"/></label>
																				<label for="adm3_${s.index}"><form:radiobutton cssClass="noBorder" id="adm3_${s.index}" path="listChecklist[${s.index}].flag_adm" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="uw1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="uw1_${s.index}" path="listChecklist[${s.index}].flag_uw" value="1"/></label>
																				<label for="uw2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="uw2_${s.index}" path="listChecklist[${s.index}].flag_uw" value="2"/></label>
																				<label for="uw3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="uw3_${s.index}" path="listChecklist[${s.index}].flag_uw" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="print1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="print1_${s.index}" path="listChecklist[${s.index}].flag_print" value="1"/></label>
																				<label for="print2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="print2_${s.index}" path="listChecklist[${s.index}].flag_print" value="2"/></label>
																				<label for="print3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="print3_${s.index}" path="listChecklist[${s.index}].flag_print" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="filling1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="filling1_${s.index}" path="listChecklist[${s.index}].flag_filling" value="1"/></label>
																				<label for="filling2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="filling2_${s.index}" path="listChecklist[${s.index}].flag_filling" value="2"/></label>
																				<label for="filling3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="filling3_${s.index}" path="listChecklist[${s.index}].flag_filling" value="3"/></label>&nbsp;
																			</td>
																		</c:when>
																		<c:when test="${cmd.lspd_id eq 2}">
																			<td style="text-align: center; background-color: #ffffcc;">
																				<label for="adm1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="adm1_${s.index}" path="listChecklist[${s.index}].flag_adm" value="1"/></label>
																				<label for="adm2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="adm2_${s.index}" path="listChecklist[${s.index}].flag_adm" value="2"/></label>
																				<label for="adm3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="adm3_${s.index}" path="listChecklist[${s.index}].flag_adm" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="uw1_${s.index}"><form:radiobutton cssClass="noBorder" id="uw1_${s.index}" path="listChecklist[${s.index}].flag_uw" value="1"/></label>
																				<label for="uw2_${s.index}"><form:radiobutton cssClass="noBorder" id="uw2_${s.index}" path="listChecklist[${s.index}].flag_uw" value="2"/></label>
																				<label for="uw3_${s.index}"><form:radiobutton cssClass="noBorder" id="uw3_${s.index}" path="listChecklist[${s.index}].flag_uw" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="print1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="print1_${s.index}" path="listChecklist[${s.index}].flag_print" value="1"/></label>
																				<label for="print2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="print2_${s.index}" path="listChecklist[${s.index}].flag_print" value="2"/></label>
																				<label for="print3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="print3_${s.index}" path="listChecklist[${s.index}].flag_print" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="filling1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="filling1_${s.index}" path="listChecklist[${s.index}].flag_filling" value="1"/></label>
																				<label for="filling2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="filling2_${s.index}" path="listChecklist[${s.index}].flag_filling" value="2"/></label>
																				<label for="filling3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="filling3_${s.index}" path="listChecklist[${s.index}].flag_filling" value="3"/></label>&nbsp;
																			</td>
																		</c:when>
																		<c:when test="${cmd.lspd_id eq 6}">
																			<td style="text-align: center; background-color: #ffffcc;">
																				<label for="adm1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="adm1_${s.index}" path="listChecklist[${s.index}].flag_adm" value="1"/></label>
																				<label for="adm2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="adm2_${s.index}" path="listChecklist[${s.index}].flag_adm" value="2"/></label>
																				<label for="adm3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="adm3_${s.index}" path="listChecklist[${s.index}].flag_adm" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="uw1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="uw1_${s.index}" path="listChecklist[${s.index}].flag_uw" value="1"/></label>
																				<label for="uw2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="uw2_${s.index}" path="listChecklist[${s.index}].flag_uw" value="2"/></label>
																				<label for="uw3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="uw3_${s.index}" path="listChecklist[${s.index}].flag_uw" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="print1_${s.index}"><form:radiobutton cssClass="noBorder" id="print1_${s.index}" path="listChecklist[${s.index}].flag_print" value="1"/></label>
																				<label for="print2_${s.index}"><form:radiobutton cssClass="noBorder" id="print2_${s.index}" path="listChecklist[${s.index}].flag_print" value="2"/></label>
																				<label for="print3_${s.index}"><form:radiobutton cssClass="noBorder" id="print3_${s.index}" path="listChecklist[${s.index}].flag_print" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="filling1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="filling1_${s.index}" path="listChecklist[${s.index}].flag_filling" value="1"/></label>
																				<label for="filling2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="filling2_${s.index}" path="listChecklist[${s.index}].flag_filling" value="2"/></label>
																				<label for="filling3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="filling3_${s.index}" path="listChecklist[${s.index}].flag_filling" value="3"/></label>&nbsp;
																			</td>
																		</c:when>
																		<c:when test="${cmd.lspd_id eq 7}">
																			<td style="text-align: center; background-color: #ffffcc;">
																				<label for="adm1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="adm1_${s.index}" path="listChecklist[${s.index}].flag_adm" value="1"/></label>
																				<label for="adm2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="adm2_${s.index}" path="listChecklist[${s.index}].flag_adm" value="2"/></label>
																				<label for="adm3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="adm3_${s.index}" path="listChecklist[${s.index}].flag_adm" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="uw1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="uw1_${s.index}" path="listChecklist[${s.index}].flag_uw" value="1"/></label>
																				<label for="uw2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="uw2_${s.index}" path="listChecklist[${s.index}].flag_uw" value="2"/></label>
																				<label for="uw3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="uw3_${s.index}" path="listChecklist[${s.index}].flag_uw" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="print1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="print1_${s.index}" path="listChecklist[${s.index}].flag_print" value="1"/></label>
																				<label for="print2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="print2_${s.index}" path="listChecklist[${s.index}].flag_print" value="2"/></label>
																				<label for="print3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="print3_${s.index}" path="listChecklist[${s.index}].flag_print" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="filling1_${s.index}"><form:radiobutton cssClass="noBorder" id="filling1_${s.index}" path="listChecklist[${s.index}].flag_filling" value="1"/></label>
																				<label for="filling2_${s.index}"><form:radiobutton cssClass="noBorder" id="filling2_${s.index}" path="listChecklist[${s.index}].flag_filling" value="2"/></label>
																				<label for="filling3_${s.index}"><form:radiobutton cssClass="noBorder" id="filling3_${s.index}" path="listChecklist[${s.index}].flag_filling" value="3"/></label>&nbsp;
																			</td>
																		</c:when>
																		<c:otherwise>
																			<td style="text-align: center; background-color: #ffffcc;">
																				<label for="adm1_${s.index}"><form:radiobutton cssClass="noBorder" id="adm1_${s.index}" path="listChecklist[${s.index}].flag_adm" value="1"/></label>
																				<label for="adm2_${s.index}"><form:radiobutton cssClass="noBorder" id="adm2_${s.index}" path="listChecklist[${s.index}].flag_adm" value="2"/></label>
																				<label for="adm3_${s.index}"><form:radiobutton cssClass="noBorder" id="adm3_${s.index}" path="listChecklist[${s.index}].flag_adm" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="uw1_${s.index}"><form:radiobutton cssClass="noBorder" id="uw1_${s.index}" path="listChecklist[${s.index}].flag_uw" value="1"/></label>
																				<label for="uw2_${s.index}"><form:radiobutton cssClass="noBorder" id="uw2_${s.index}" path="listChecklist[${s.index}].flag_uw" value="2"/></label>
																				<label for="uw3_${s.index}"><form:radiobutton cssClass="noBorder" id="uw3_${s.index}" path="listChecklist[${s.index}].flag_uw" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="print1_${s.index}"><form:radiobutton cssClass="noBorder" id="print1_${s.index}" path="listChecklist[${s.index}].flag_print" value="1"/></label>
																				<label for="print2_${s.index}"><form:radiobutton cssClass="noBorder" id="print2_${s.index}" path="listChecklist[${s.index}].flag_print" value="2"/></label>
																				<label for="print3_${s.index}"><form:radiobutton cssClass="noBorder" id="print3_${s.index}" path="listChecklist[${s.index}].flag_print" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="filling1_${s.index}"><form:radiobutton cssClass="noBorder" id="filling1_${s.index}" path="listChecklist[${s.index}].flag_filling" value="1"/></label>
																				<label for="filling2_${s.index}"><form:radiobutton cssClass="noBorder" id="filling2_${s.index}" path="listChecklist[${s.index}].flag_filling" value="2"/></label>
																				<label for="filling3_${s.index}"><form:radiobutton cssClass="noBorder" id="filling3_${s.index}" path="listChecklist[${s.index}].flag_filling" value="3"/></label>&nbsp;
																			</td>
																		</c:otherwise>
																	</c:choose>
																	<td style="text-align: left;">
																		<form:input path="listChecklist[${s.index}].mc_desc" size="40"/>
																	</td>
																</tr>
															</c:if>
														</c:when>
														<c:otherwise>
															<c:if test="${c.lc_id ne 0}">
																<tr>
																	<td style="text-align: left;">
																		<c:if test="${c.level eq 2}">
																			<c:set var="urut" value="${urut + 1}" />
																			${urut}.
																		</c:if>
																	</td>
																	<td style="text-align: left;"><span>${c.lc_nama}</span></td>
																	
																	<c:choose>
																		<c:when test="${cmd.lspd_id eq 1}">
																			<td style="text-align: center; background-color: #ffffcc;">
																				<label for="adm1_${s.index}"><form:radiobutton cssClass="noBorder" id="adm1_${s.index}" path="listChecklist[${s.index}].flag_adm" value="1"/></label>
																				<label for="adm2_${s.index}"><form:radiobutton cssClass="noBorder" id="adm2_${s.index}" path="listChecklist[${s.index}].flag_adm" value="2"/></label>&nbsp;
																				<label for="adm3_${s.index}"><form:radiobutton cssClass="noBorder" id="adm3_${s.index}" path="listChecklist[${s.index}].flag_adm" value="3"/></label>
																			</td>
																			<td style="text-align: center;">
																				<label for="bancass1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="bancass1_${s.index}" path="listChecklist[${s.index}].flag_bancass" value="1"/></label>
																				<label for="bancass2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="bancass2_${s.index}" path="listChecklist[${s.index}].flag_bancass" value="2"/></label>
																				<label for="bancass3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="bancass3_${s.index}" path="listChecklist[${s.index}].flag_bancass" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="uw1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="uw1_${s.index}" path="listChecklist[${s.index}].flag_uw" value="1"/></label>
																				<label for="uw2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="uw2_${s.index}" path="listChecklist[${s.index}].flag_uw" value="2"/></label>
																				<label for="uw3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="uw3_${s.index}" path="listChecklist[${s.index}].flag_uw" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="print1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="print1_${s.index}" path="listChecklist[${s.index}].flag_print" value="1"/></label>
																				<label for="print2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="print2_${s.index}" path="listChecklist[${s.index}].flag_print" value="2"/></label>
																				<label for="print3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="print3_${s.index}" path="listChecklist[${s.index}].flag_print" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="filling1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="filling1_${s.index}" path="listChecklist[${s.index}].flag_filling" value="1"/></label>
																				<label for="filling2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="filling2_${s.index}" path="listChecklist[${s.index}].flag_filling" value="2"/></label>
																				<label for="filling3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="filling3_${s.index}" path="listChecklist[${s.index}].flag_filling" value="3"/></label>&nbsp;
																			</td>
																		</c:when>
																		<c:when test="${cmd.lspd_id eq 2}">
																			<td style="text-align: center; background-color: #ffffcc;">
																				<label for="adm1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="adm1_${s.index}" path="listChecklist[${s.index}].flag_adm" value="1"/></label>
																				<label for="adm2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="adm2_${s.index}" path="listChecklist[${s.index}].flag_adm" value="2"/></label>
																				<label for="adm3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="adm3_${s.index}" path="listChecklist[${s.index}].flag_adm" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="bancass1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="bancass1_${s.index}" path="listChecklist[${s.index}].flag_bancass" value="1"/></label>
																				<label for="bancass2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="bancass2_${s.index}" path="listChecklist[${s.index}].flag_bancass" value="2"/></label>
																				<label for="bancass3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="bancass3_${s.index}" path="listChecklist[${s.index}].flag_bancass" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="uw1_${s.index}"><form:radiobutton cssClass="noBorder" id="uw1_${s.index}" path="listChecklist[${s.index}].flag_uw" value="1"/></label>
																				<label for="uw2_${s.index}"><form:radiobutton cssClass="noBorder" id="uw2_${s.index}" path="listChecklist[${s.index}].flag_uw" value="2"/></label>
																				<label for="uw3_${s.index}"><form:radiobutton cssClass="noBorder" id="uw3_${s.index}" path="listChecklist[${s.index}].flag_uw" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="print1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="print1_${s.index}" path="listChecklist[${s.index}].flag_print" value="1"/></label>
																				<label for="print2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="print2_${s.index}" path="listChecklist[${s.index}].flag_print" value="2"/></label>
																				<label for="print3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="print3_${s.index}" path="listChecklist[${s.index}].flag_print" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="filling1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="filling1_${s.index}" path="listChecklist[${s.index}].flag_filling" value="1"/></label>
																				<label for="filling2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="filling2_${s.index}" path="listChecklist[${s.index}].flag_filling" value="2"/></label>
																				<label for="filling2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="filling2_${s.index}" path="listChecklist[${s.index}].flag_filling" value="2"/></label>&nbsp;
																			</td>
																		</c:when>
																		<c:when test="${cmd.lspd_id eq 6}">
																			<td style="text-align: center; background-color: #ffffcc;">
																				<label for="adm1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="adm1_${s.index}" path="listChecklist[${s.index}].flag_adm" value="1"/></label>
																				<label for="adm2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="adm2_${s.index}" path="listChecklist[${s.index}].flag_adm" value="2"/></label>
																				<label for="adm3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="adm3_${s.index}" path="listChecklist[${s.index}].flag_adm" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="bancass1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="bancass1_${s.index}" path="listChecklist[${s.index}].flag_bancass" value="1"/></label>
																				<label for="bancass2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="bancass2_${s.index}" path="listChecklist[${s.index}].flag_bancass" value="2"/></label>
																				<label for="bancass3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="bancass3_${s.index}" path="listChecklist[${s.index}].flag_bancass" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="uw1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="uw1_${s.index}" path="listChecklist[${s.index}].flag_uw" value="1"/></label>
																				<label for="uw2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="uw2_${s.index}" path="listChecklist[${s.index}].flag_uw" value="2"/></label>
																				<label for="uw3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="uw3_${s.index}" path="listChecklist[${s.index}].flag_uw" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="print1_${s.index}"><form:radiobutton cssClass="noBorder" id="print1_${s.index}" path="listChecklist[${s.index}].flag_print" value="1"/></label>
																				<label for="print2_${s.index}"><form:radiobutton cssClass="noBorder" id="print2_${s.index}" path="listChecklist[${s.index}].flag_print" value="2"/></label>
																				<label for="print3_${s.index}"><form:radiobutton cssClass="noBorder" id="print3_${s.index}" path="listChecklist[${s.index}].flag_print" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="filling1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="filling1_${s.index}" path="listChecklist[${s.index}].flag_filling" value="1"/></label>
																				<label for="filling2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="filling2_${s.index}" path="listChecklist[${s.index}].flag_filling" value="2"/></label>
																				<label for="filling3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="filling3_${s.index}" path="listChecklist[${s.index}].flag_filling" value="3"/></label>&nbsp;
																			</td>
																		</c:when>
																		<c:when test="${cmd.lspd_id eq 7}">
																			<td style="text-align: center; background-color: #ffffcc;">
																				<label for="adm1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="adm1_${s.index}" path="listChecklist[${s.index}].flag_adm" value="1"/></label>
																				<label for="adm2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="adm2_${s.index}" path="listChecklist[${s.index}].flag_adm" value="2"/></label>
																				<label for="adm3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="adm2_${s.index}" path="listChecklist[${s.index}].flag_adm" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="bancass1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="bancass1_${s.index}" path="listChecklist[${s.index}].flag_bancass" value="1"/></label>
																				<label for="bancass2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="bancass2_${s.index}" path="listChecklist[${s.index}].flag_bancass" value="2"/></label>
																				<label for="bancass3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="bancass3_${s.index}" path="listChecklist[${s.index}].flag_bancass" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="uw1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="uw1_${s.index}" path="listChecklist[${s.index}].flag_uw" value="1"/></label>
																				<label for="uw2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="uw2_${s.index}" path="listChecklist[${s.index}].flag_uw" value="2"/></label>
																				<label for="uw3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="uw3_${s.index}" path="listChecklist[${s.index}].flag_uw" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="print1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="print1_${s.index}" path="listChecklist[${s.index}].flag_print" value="1"/></label>
																				<label for="print2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="print2_${s.index}" path="listChecklist[${s.index}].flag_print" value="2"/></label>
																				<label for="print3_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="print3_${s.index}" path="listChecklist[${s.index}].flag_print" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="filling1_${s.index}"><form:radiobutton cssClass="noBorder" id="filling1_${s.index}" path="listChecklist[${s.index}].flag_filling" value="1"/></label>
																				<label for="filling2_${s.index}"><form:radiobutton cssClass="noBorder" id="filling2_${s.index}" path="listChecklist[${s.index}].flag_filling" value="2"/></label>&nbsp;
																				<label for="filling3_${s.index}"><form:radiobutton cssClass="noBorder" id="filling3_${s.index}" path="listChecklist[${s.index}].flag_filling" value="3"/></label>&nbsp;
																			</td>
																		</c:when>
																		<c:otherwise>
																			<td style="text-align: center; background-color: #ffffcc;">
																				<label for="adm1_${s.index}"><form:radiobutton cssClass="noBorder" id="adm1_${s.index}" path="listChecklist[${s.index}].flag_adm" value="1"/></label>
																				<label for="adm2_${s.index}"><form:radiobutton cssClass="noBorder" id="adm2_${s.index}" path="listChecklist[${s.index}].flag_adm" value="2"/></label>
																				<label for="adm2_${s.index}"><form:radiobutton cssClass="noBorder" id="adm2_${s.index}" path="listChecklist[${s.index}].flag_adm" value="2"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="bancass1_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="bancass1_${s.index}" path="listChecklist[${s.index}].flag_bancass" value="1"/></label>
																				<label for="bancass2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="bancass2_${s.index}" path="listChecklist[${s.index}].flag_bancass" value="2"/></label>
																				<label for="bancass2_${s.index}"><form:radiobutton disabled="true" cssClass="noBorder" id="bancass2_${s.index}" path="listChecklist[${s.index}].flag_bancass" value="2"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="uw1_${s.index}"><form:radiobutton cssClass="noBorder" id="uw1_${s.index}" path="listChecklist[${s.index}].flag_uw" value="1"/></label>
																				<label for="uw2_${s.index}"><form:radiobutton cssClass="noBorder" id="uw2_${s.index}" path="listChecklist[${s.index}].flag_uw" value="2"/></label>
																				<label for="uw3_${s.index}"><form:radiobutton cssClass="noBorder" id="uw3_${s.index}" path="listChecklist[${s.index}].flag_uw" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="print1_${s.index}"><form:radiobutton cssClass="noBorder" id="print1_${s.index}" path="listChecklist[${s.index}].flag_print" value="1"/></label>
																				<label for="print2_${s.index}"><form:radiobutton cssClass="noBorder" id="print2_${s.index}" path="listChecklist[${s.index}].flag_print" value="2"/></label>
																				<label for="print3_${s.index}"><form:radiobutton cssClass="noBorder" id="print3_${s.index}" path="listChecklist[${s.index}].flag_print" value="3"/></label>&nbsp;
																			</td>
																			<td style="text-align: center;">
																				<label for="filling1_${s.index}"><form:radiobutton cssClass="noBorder" id="filling1_${s.index}" path="listChecklist[${s.index}].flag_filling" value="1"/></label>
																				<label for="filling2_${s.index}"><form:radiobutton cssClass="noBorder" id="filling2_${s.index}" path="listChecklist[${s.index}].flag_filling" value="2"/></label>
																				<label for="filling3_${s.index}"><form:radiobutton cssClass="noBorder" id="filling3_${s.index}" path="listChecklist[${s.index}].flag_filling" value="3"/></label>&nbsp;
																			</td>
																		</c:otherwise>
																	</c:choose>
																	<td style="text-align: left;">
																		<form:input path="listChecklist[${s.index}].mc_desc" size="40"/>
																	</td>
																</tr>
															</c:if>
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</tbody>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<c:if test="${cmd.editable eq true}">
											<input type="submit" name="simpan" value="Simpan" onclick="return confirm('Simpan? Data Akan Disimpan Berdasarkan Tanggal Hari Ini.');">
											<input type="reset" name="batal" value="Batal / Reset">
											
										</c:if>
									</td>
									<td>
										<span class="info">
											* Pilih ASLI untuk Dokumen Asli, SCAN untuk Dokumen Hasil Scan, atau KOSONGKAN apabila tidak ada dokumen.
										</span>
									</td>
								</tr>
								<c:if test="${lsError ne null}">
									<tr>
										<td colspan="3">
											<div style="font: bold; color: #FF0000;" align="left">
												- ${lsError}
											</div>
										</td>
									</tr>
								</c:if>
							</table>
						</fieldset>					
					</form:form>
				</div>
			</div>
			
		</div>

	</body>
</html>