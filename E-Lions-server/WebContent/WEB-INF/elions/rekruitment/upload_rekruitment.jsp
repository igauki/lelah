<%@ include file="/include/page/header.jsp"%>
<style type="text/css">
	html,body{
	     overflow: hidden;
	}
</style>
<script>

	function mulai(){
		<c:if test="${not empty daftarPesan}">
			<c:forEach items="${daftarPesan}" var="d">
				alert('${d}');
			</c:forEach>
		</c:if>
		
		//tampilkan('info',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);
	}
    
	function get(id){
		return document.getElementById(id);
	}

	function cek(asdf){
		if(asdf == '') {
			alert('Harap pilih SPAJ terlebih dahulu!');
			return false;
		}else{
			return true;
		}
	}
        function qc_ok_control(info, konfirmasi,asdf)
         {
        if(cek(asdf) && confirm(konfirmasi)){
            qcOkControl(trim(document.formpost.spaj.value),'${sessionScope.currentUser.lus_id}','${sessionScope.currentUser.lde_id}',info);
            }
    }
	function tampilkan(kode, asdf){
		var referal = 0;
		var jenis = document.formpost.jenis.options[document.formpost.jenis.selectedIndex].value;
		
		if(get('error')) get('error').style.display='none';
		if(get('success')) get('success').style.display='none';
		
		if(get('infoFrame')){
			get('infoFrame').style.display='block';
			if(kode=='info'){
				if(cek(asdf)) {
					get('infoFrame').src='${path }/rekruitment/view.htm?showSPAJ='+asdf;	
					if(document.getElementById('docFrame')) document.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+asdf;
				}
			}else if(kode=='jenis'){
				document.getElementById('formpost').submit();
			}else if(kode=='upload'){
				if(cek(asdf)) get('infoFrame').src='${path }/rekruitment/upload_rekruitment_detail.htm?agent='+asdf+'&jenis='+jenis;
			}
			
			return false;
		}
	
	}
</script>
<body onload="setFrameSize('infoFrame', 100); setFrameSize('docFrame', 100); mulai();" onresize="setFrameSize('infoFrame', 100); setFrameSize('docFrame', 100); " style="height: 100%;">
<c:choose>
	<c:when test="${not empty transfer}">
		<div class="tabcontent">
			<c:choose>
				<c:when test="${not empty error}">
					<div id="error" style="text-transform: none;">
						${error}
					</div>
				</c:when>
				<c:when test="${not empty success}">
					<div id="success" style="text-transform: none;">
						${success}
					</div>
				</c:when>
			</c:choose>
		</div>
	</c:when>
	<c:otherwise>

		<form name="formpost" method="post">
		<div class="tabcontent">
		<table class="entry2" style="width: 98%">
		<tr>
				<th>Jenis</th>
				<td>
		
					<select name="jenis" onchange="tampilkan('jenis',this.value)">
						<option value=""></option>
						<c:forEach var="s" items="${jenisList}">
							<option value="${s.key}"  
								<c:if test="${s.key eq param.jenis }">selected</c:if>>${s.value}
							</option>
						</c:forEach>
					</select>
						
				</td>
				<td rowspan=2><div id="prePostError" style="display:none;"></div></td>
			</tr>
			
			<tr>
				<th>Agent</th>
				<td>
		
					<select name="agent" >
						<option value="">[--- Silahkan Pilih/Cari Agent ---]</option>
						<c:forEach var="s" items="${agentList}">
							<option value="${s.KODE_AGENT }"
								<c:if test="${s.KODE_AGENT eq param.kode_agent }">selected</c:if>>${s.NAMA_AGENT}</option>
						</c:forEach>
					</select>
					
						<input type="button" value="Upload" name="upload" accesskey="U"
							onmouseover="return overlib('Alt-U', AUTOSTATUS, WRAP);" onmouseout="nd();"
							onclick="tampilkan('upload', document.formpost.agent.options[document.formpost.agent.selectedIndex].value);">
				</td>
				<td rowspan=2><div id="prePostError" style="display:none;"></div></td>
			</tr>
		
			<tr>
				<td colspan="3">
		
					<table style="margin: 0 0 0 0; padding: 0 0 0 0; width: 100%;">
						<tr>
							<c:choose>
								<c:when test="${sessionScope.currentUser.wideScreen}">
									<td style="width: 100%;">
										<c:choose>
											<c:when test="${not empty error}">
												<div id="error" style="text-transform: none;">
													${error}
												</div>
											</c:when>
											<c:when test="${not empty success}">
												<div id="success" style="text-transform: none;">
													${success}
												</div>
											</c:when>
										</c:choose>
										
										<iframe name="infoFrame" id="infoFrame" width="100%">
											Please Wait...
										</iframe>
									</td>
								</c:when>
								<c:otherwise>
									<td>
										<c:choose>
											<c:when test="${not empty error}">
												<div id="error" style="text-transform: none;">
													${error}
												</div>
											</c:when>
											<c:when test="${not empty success}">
												<div id="success" style="text-transform: none;">
													${success}
												</div>
											</c:when>
										</c:choose>
										
										<iframe name="infoFrame" id="infoFrame" width="100%">
											Please Wait...
										</iframe>
									</td>
								</c:otherwise>
							</c:choose>
						</tr>
					</table>		
				</td>
			</tr>
		</table>
		</div>
		</form>

	</c:otherwise>
</c:choose>
<c:if test="${not empty warning}"><script>alert('${warning}');</script></c:if>
</body>
<%@ include file="/include/page/footer.jsp"%>