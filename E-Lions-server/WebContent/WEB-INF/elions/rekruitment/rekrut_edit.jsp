<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />

<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<body>
<form action="${path}/rekruitment/multi.htm?window=edit_calonaaji" method="post">
<h2><u><b><font face="Verdana" color="#996600"><p align="center">Edit data calon agen</p></font></b></u></h2>
<b><p align="center"><font face="Verdana" size="3" color="red"><c:if test="${pesan ne null or pesan ne \"\" }">${pesan}</c:if></font></p></b>
<input type="hidden" id="posisi" name="posisi" value="${hasil.posisi}">
<input type="hidden" id="mku_region" name="mku_region" value="${hasil.mku_region}">
<table>
	<c:if test="${hasil eq null}">
		<tr>
			<th>Cari No Register</th>
			<td>
				<input type="text" id="mku_noreg" name="mku_noreg" size="30" maxlength="10" tabindex="1">
				<input type="submit" id="cari" name="cari" value="Search">
			</td>
			<td colspan="4">&nbsp;</td>
		</tr>
	</c:if>
	<tr>
		<th>No Register Rekruitment</th>
		<td><input type="text" readonly="readonly" style='background-color:#D4D4D4' value="${hasil.mku_no_reg}" id="mku_no_reg" name="mku_no_reg" size="30" maxlength="10" tabindex="1"> </td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<th>Tanggal Pengisian</th>
		<td><input type="text" class="tgl" value="${hasil.mku_tglkues_str}" id="mku_tglkues" name="mku_tglkues" readonly="readonly" style='background-color:#D4D4D4'> </td>
	</tr>
	<tr>
		<th>Jenis Distribusi</th>
		<td><input type="text" readonly="readonly" style='background-color:#D4D4D4' value="${hasil.mku_jenis_cabang_str}" id="mku_jenis_cabang_str" name="mku_jenis_cabang_str"></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<th>Tempat Pengisian</th>
		<td><input type="text" readonly="readonly" style='background-color:#D4D4D4' value="${hasil.mku_tempatkues}" id="mku_tempatkues" name="mku_tempatkues" size="30" maxlength="30" tabindex="8"></td>
	</tr>
	<tr>
		<th>Posisi Pengajuan</th>
		<td><input type="text" readonly="readonly" style='background-color:#D4D4D4' value="${hasil.mku_posisi_agen}" id="mku_posisi_agen" name="mku_posisi_agen"></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<th>Cabang</th>
		<td><input type="text" readonly="readonly" style='background-color:#D4D4D4' value="${hasil.mku_region_nama}" id="mku_region_nama" name="mku_region_nama" size="50" tabindex="8"></td>
	</tr>
	<tr>
		<th colspan="6"><hr>&nbsp;</th>
	</tr>
	<tr>
		<th>Nama</th>
		<td><input type="text" id="mku_first" name="mku_first" value="${hasil.mku_first}"> </td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<th>No KTP</th>
		<td><input type="text" id="mku_no_identity" name="mku_no_identity" value="${hasil.mku_no_identity}"></td>
	</tr>
	<tr>
		<th>Tempat & Tanggal Lahir</th>
		<td>
			<input type="text" id="mku_place_birth" name="mku_place_birth" value="${hasil.mku_place_birth}"> &nbsp;
			<input type="text" class="tgl" id="mku_date_birth_str" name="mku_date_birth_str" value="${hasil.mku_date_birth_str}">
		</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<th>No. NPWP</th>
		<td><input type="text" id="mku_no_npwp" name="mku_no_npwp" value="${hasil.mku_no_npwp}"></td>
	</tr>
	<tr>
		<th>Alamat</br>maks(200)</th>
		<td><textarea rows="5" cols="30"  onkeyup="textCounter(this, 200);"	onkeydown="textCounter(this, 200);">${hasil.mku_alamat}</textarea> </td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<th>Agama</th>
		<td>
			<select id="mku_agama" name="mku_agama">
				<c:forEach items="${lst_agama}" var="l">
					<option <c:if test="${hasil.mku_agama eq l.key}"> SELECTED </c:if>					
						value="${l.key}">${l.value}</option>
				</c:forEach>
			</select>
		</td>
	</tr>
	<tr>
		<th>Propinsi</th>
		<td>
			<select id="mku_provinsi" name="mku_provinsi">
				<c:forEach items="${lst_propinsi}" var="p">
					<option <c:if test="${hasil.mku_provinsi eq p.key }"> SELECTED </c:if> value="${p.key}" >${p.value }</option>
				</c:forEach>
			</select>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<th>No. Telp</th>
		<td>
			<input type="text" id="mku_area_rumah" name="mku_area_rumah" value="${hasil.mku_area_rumah}" size="5" maxlength="4" tabindex="22"> &nbsp;
			<input type="text" id="mku_tlprumah" name="mku_tlprumah" value="${hasil.mku_tlprumah}" size="21" maxlength="10" tabindex="23">
		</td>
	</tr>
	<tr>
		<th>Kota</th>
		<td><input type="text" id="mku_kota" name="mku_kota" value="${hasil.mku_kota }"></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<th>No. HP</th>
		<td>
			<!-- <input type="text" id="mku_area_hp" name="mku_area_hp" value="${hasil.mku_area_hp}" size="5" maxlength="4" tabindex="22"> &nbsp; -->
			<input type="text" id="mku_hp" name="mku_hp" value="${hasil.mku_hp}" size="21" maxlength="13" tabindex="23">
		</td>
	</tr>
	<tr>
		<th>Kode Pos</th>
		<td><input type="text" id="mku_kd_pos" name="mku_kd_pos" value="${hasil.mku_kd_pos}"></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<th>Email</th>
		<td><input type="text" id="mku_email_personal" name="mku_email_personal" value="${hasil.mku_email_personal}" size="30" maxlength="60" tabindex="8"></td>
	</tr>
	<tr>
		<th>Pendidikan Terakhir</th>
		<td colspan="4">
			<select name="mku_pendidikan" id="mku_pendidikan">
				<option
					<c:if test="${hasil.mku_pendidikan eq 0}">   SELECTED </c:if> value="0">
					Lainnya
				</option>
				<option
					<c:if test="${hasil.mku_pendidikan eq 1}">   SELECTED </c:if> value="1">
					SD
				</option>
				<option
					<c:if test="${hasil.mku_pendidikan eq 2}">   SELECTED </c:if> value="2">
					SLTP
				</option>
				<option
					<c:if test="${hasil.mku_pendidikan eq 3}">   SELECTED </c:if> value="3">
					SLTA
				</option>
				<option
					<c:if test="${hasil.mku_pendidikan eq 4}">   SELECTED </c:if> value="4">
					D1
				</option>
				<option
					<c:if test="${hasil.mku_pendidikan eq 5}">   SELECTED </c:if> value="5">
					D2
				</option>
				<option
					<c:if test="${hasil.mku_pendidikan eq 6}">   SELECTED </c:if> value="6">
					D3
				</option>
				<option
					<c:if test="${hasil.mku_pendidikan eq 7}">   SELECTED </c:if> value="7">
					D4
				</option>
				<option
					<c:if test="${hasil.mku_pendidikan eq 8}">   SELECTED </c:if> value="8">
					S1
				</option>
				<option
					<c:if test="${hasil.mku_pendidikan eq 9}">   SELECTED </c:if> value="9">
					S2
				</option>
				<option
					<c:if test="${hasil.mku_pendidikan eq 10}">   SELECTED </c:if> value="10">
					S3
				</option>
			</select>
			Jika lainnya : <input type="text" id="mku_pendidikan_lain" name="mku_pendidikan_lain" value="${hasil.mku_pendidikan_lain }"> 
		</td> 
	</tr>
	<tr>
		<th>Jenis Kelamin</th>
		<td>
			<select name="mku_sex" id="mku_sex" tabindex="9">
				<option <c:if test="${hasil.mku_sex eq 1}"> SELECTED </c:if> value="1">
					Pria / Male
				</option>
				<option <c:if test="${hasil.mku_sex eq 0}"> SELECTED </c:if> value="0">
					Wanita / Female
				</option>
			</select>
		</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<th>Status Perkawinan</th>
		<td>
			<select name="mku_status" tabindex="24">
				<option <c:if test="${hasil.mku_status eq 1}">   SELECTED </c:if> value="1">
					Single (Tidak Kawin)
				</option>
				<option <c:if test="${hasil.mku_status eq 2}">   SELECTED </c:if> value="2">
					Married (Kawin)
				</option>
			</select>
		</td>
	</tr>
	<tr>
		<th colspan="6">&nbsp;</th>
	</tr>
	<tr>
		<th colspan="6"><u><b><font face="Verdana" size="1" color="#996600"><p align="center">Bila Anda Menikah, Isilah Data Dibawah Ini :</p></font></b></u></th>
	</tr>
	<tr>
		<th>Nama Suami/Istri</th>
		<td><input type="text" id="mku_nama_pasangan" name="mku_nama_pasangan" value="${hasil.mku_nama_pasangan }" size="30" maxlength="20" tabindex="5"></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<th>Jumlah Tanggungan</th>
		<td><input type="text" id="mku_tanggungan" name="mku_tanggungan" value="${hasil.mku_tanggungan }" size="10" maxlength="3" tabindex="5"> Orang</td>
	</tr>
	<tr>
		<th>Pekerjaan Suami/Istri</th>
		<td colspan="5"><input type="text" id="mku_pekerjaan_pasangan" name="mku_pekerjaan_pasangan" value="${hasil.mku_pekerjaan_pasangan }" size="30" maxlength="40" tabindex="16"></td>
	</tr>
	<tr>
		<th colspan="6">&nbsp;</th>
	</tr>
	<tr>
		<th colspan="6"><u><b><font face="Verdana" size="1" color="#996600"><p align="center">Informasi Rekening</p></font></b></u></th>
	</tr>
	<tr>
		<th>Nama Bank</th>
		<td>
			<select name="mku_bank_id2" id="mku_bank_id2">
				<c:forEach var="xxx" items="${select_bank}">
					<option <c:if test="${hasil.mku_bank_id2 eq xxx.value}"> SELECTED </c:if> value="${xxx.value}">
						${xxx.key}
					</option>
				</c:forEach>
			</select>
		</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<th>No Rekening</th>
		<td><input type="text" id="mku_acc_cust" name="mku_acc_cust" value="${hasil.mku_acc_cust }" size="30" maxlength="20" tabindex="5"></td>
	</tr>
	<tr>
		<th colspan="6">&nbsp;</th>
	</tr>
	<tr>
		<th colspan="6"><u><b><font face="Verdana" size="1" color="#996600"><p align="center">Atasan Langsung Calon</p></font></b></u></th>
	</tr>
	<tr>
		<th>Nama Atasan</th>
		<td><input type="text" id="mku_nama_atasan" name="mku_nama_atasan" value="${hasil.mku_nama_atasan }" size="30" maxlength="20" tabindex="5" readonly="readonly" style='background-color:#D4D4D4'></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<th>Posisi</th>
		<td><input type="text" id="mku_posisi_atasan" name="mku_posisi_atasan" value="${hasil.mku_posisi_atasan }" size="30" maxlength="20" tabindex="5" readonly="readonly" style='background-color:#D4D4D4'></td>
	</tr>
	<tr>
		<th>Kode Agen</th>
		<td colspan="5"><input type="text" id="mst_leader" name="mst_leader" value="${hasil.mst_leader }" size="30" maxlength="20" tabindex="5"></td>
	</tr>
	<tr>
		<th colspan="6"><u><b><font face="Verdana" size="1" color="#996600"><p align="center">Data Perekrut Calon</p></font></b></u></th>
	</tr>
	<tr>
		<th>Jenis Rekrut</th>
		<td colspan="5">
			<select id="mku_jenis_rekrut" name="mku_jenis_rekrut">
				<c:forEach items="${jenis_rekrut}" var="j">
					<option <c:if test="${hasil.mku_jenis_rekrut eq j.ID}"> SELECTED </c:if> value="${j.ID}">${j.JENIS}</option>
				</c:forEach>
			</select>
		</td>
	</tr>
	<tr>
		<th>Nama Rekruter</th>
		<td><input type="text" id="mku_rekruiter" name="mku_rekruiter" value="${hasil.mku_rekruiter}" size="30" maxlength="20" tabindex="5" readonly="readonly" style='background-color:#D4D4D4'></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<th>Posisi</th>
		<td><input type="text" id="mku_posisi_rekruter" name="mku_posisi_rekruter" value="${hasil.mku_posisi_rekruter}" size="15" maxlength="10" tabindex="5" readonly="readonly" style='background-color:#D4D4D4'></td>
	</tr>
	<tr>
		<th>Kode Agen</th>
		<td colspan="5">
			<input type="text" id="msrk_id" name="msrk_id" value="${hasil.msrk_id}" size="15" maxlength="10" tabindex="5">
			<input type="hidden" id="mku_acc_rekruiter" name="mku_acc_rekruiter" value="${hasil.mku_acc_rekruiter}">
			<input type="hidden" id="mku_bank_id" name="mku_bank_id" value="${hasil.mku_bank_id }">
		</td>
	</tr>
	<tr>
		<th colspan="6">&nbsp;</th>
	</tr>
	<tr>
		<th class="subtitle" colspan="6" height="20">
			<p align="center">
			<b><font face="Verdana" size="1" color="#FFFFFF">KUESIONER</font></b>
		</th>
	</tr>
	<tr style="text-align: left;">
		<th style="text-align: center;"><b> <font face="Verdana" size="1">1. </font> </b></th>
		<th colspan="4"><b> <font face="Verdana" size="1">Apakah anda pernah bergabung menjadi tenaga pemasaran PT. Asuransi Jiwa Sinarmas MSIG ?</font></b></th>
		<td>
			<select id="mku_gabung_pernah" name="mku_gabung_pernah" tabindex="38">
				<option <c:if test="${hasil.mku_gabung_pernah eq 1}">   SELECTED </c:if> value="1"> YA </option>
				<option <c:if test="${hasil.mku_gabung_pernah eq 2}">   SELECTED </c:if> value="2"> TIDAK </option>
			</select>
		</td>
	</tr>
	<tr style="text-align: left;">
		<th style="text-align: center;"><b><font face="Verdana" size="1">2. </font></b></th>
		<th colspan="4"><b><font face="Verdana" size="1">Apakah dalam 6 bulan terakhir anda pernah bergabung dengan perusahaan asuransi jiwa lain ?</font></b></th>
		<td>
			<select id="mku_6bln" name="mku_6bln" tabindex="38">
				<option <c:if test="${hasil.mku_6bln eq 1}">   SELECTED </c:if> value="1"> YA </option>
				<option <c:if test="${hasil.mku_6bln eq 0}">   SELECTED </c:if> value="0"> TIDAK </option>
			</select>
		</td>
	</tr>
	<tr style="text-align: left;">
		<th style="text-align: center;"><b><font face="Verdana" size="1">3. </font></b></th>
		<th colspan="5"><b><font face="Verdana" size="1">Pengalaman kerja di asuransi jiwa :</font></b></th>
	</tr>
	<tr style="text-align: left;">
		<td>&nbsp;</td>
		<th><b><font face="Verdana" size="1">Nama Perusahaan</font></b> &nbsp; <input type="text" id="mku_nama_experusahaan" name="mku_nama_experusahaan" value="${hasil.mku_nama_experusahaan}" size="30" maxlength="50" tabindex="8"></th>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<th>No. Lisensi</th>
		<td><input type="text" id="mku_lisensi" name="mku_lisensi" value="${hasil.mku_lisensi}" size="30" maxlength="20" tabindex="8"></td>
	</tr>
	<tr style="text-align: left;">
		<td>&nbsp;</td>
		<th>
			<b><font face="Verdana" size="1">Lama Bekerja</font></b> &nbsp; 
			<input type="text" class="tgl" id="mku_tglmsk_experusahaan" name="mku_tglmsk_experusahaan" value="${hasil.mku_tglmsk_experusahaan_str}"> &nbsp;
			<b><font face="Verdana" size="1" color="#996600">s/d</font></b> &nbsp;
			<input type="text" class="tgl" id="mku_tglklr_experusahaan" name="mku_tglklr_experusahaan" value="${hasil.mku_tglklr_experusahaan_str}">
		</th>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<th><b><font face="Verdana" size="1">Tgl. Kadaluarsa</font></b></th>
		<td><input type="text" class="tgl" id="mku_tglexp_lisensi" name="mku_tglexp_lisensi" value="${hasil.mku_tglexp_lisensi_str}"></td>
	</tr>
	<tr style="text-align: left;">
		<th style="text-align: center;"><b><font face="Verdana" size="1">4. </font></b></th>
		<td colspan="3"><b><font face="Verdana" size="1">Apakah anda pernah dinyatakan bangkrut/pailit oleh pengadilan ?</font></b></td>
		<td>
			<c:choose>
				<c:when test="${hasil.mku_bangkrut eq 0 or hasil.mku_bangkrut eq null}">
					<input type="radio" id="mku_bangkrut_1" name="mku_bangkrut" checked value="0"> Tidak 
					<input type="radio" id="mku_bangkrut_2" name="mku_bangkrut" value="1"> Ya, Jelaskan 
				</c:when>
				<c:otherwise>
					<input type="radio" id="mku_bangkrut_1" name="mku_bangkrut" value="0"> Tidak 
					<input type="radio" id="mku_bangkrut" name="mku_bangkrut" checked value="1"> Ya, Jelaskan 
				</c:otherwise>
			</c:choose>
		</td>
		<td><input type="text" id="mku_ket_bangkrut" name="mku_ket_bangkrut" value="${hasil.mku_ket_bangkrut}"></td>
	</tr>
	<tr style="text-align: left;">
		<th style="text-align: center;"><b><font face="Verdana" size="1">5. </font></b></th>
		<td colspan="3"><b><font face="Verdana" size="1">Apakah anda pernah terlibat perkara kriminal ?</font></b></td>
		<td>
			<c:choose>
				<c:when test="${hasil.mku_kriminal eq 0 or hasil.mku_kriminal eq null}">
					<input type="radio" id="mku_kriminal_1" name="mku_kriminal" checked value="0"> Tidak 
					<input type="radio" id="mku_kriminal_2" name="mku_kriminal" value="1"> Ya, Jelaskan 
				</c:when>
				<c:otherwise>
					<input type="radio" id="mku_kriminal_1" name="mku_kriminal" value="0"> Tidak 
					<input type="radio" id="mku_kriminal_2" name="mku_kriminal" checked value="1"> Ya, Jelaskan 
				</c:otherwise>
			</c:choose>
		</td>
		<td><input type="text" id="mku_ket_kriminal" name="mku_ket_kriminal" value="${hasil.mku_ket_kriminal}"></td>
	</tr>
	<tr style="text-align: left;">
		<th style="text-align: center;"><b><font face="Verdana" size="1">6. </font></b></th>
		<td colspan="3"><b><font face="Verdana" size="1">Apakah anda sudah pernah menjadi BP / AP di perusahaan Sinarmas lainnya ?</font></b></td>
		<td>
			<c:choose>
				<c:when test="${hasil.mku_flag_bp eq 1 or hasil.mku_flag_bp eq null}">
					<input type="radio" id="mku_flag_bp_1" name="mku_flag_bp" checked value="1"> Tidak 
					<input type="radio" id="mku_flag_bp_2" name="mku_flag_bp" value="0"> Ya, Sebutkan 
				</c:when>
				<c:otherwise>
					<input type="radio" id="mku_flag_bp_1" name="mku_flag_bp" value="1"> Tidak 
					<input type="radio" id="mku_flag_bp_2" name="mku_flag_bp" checked value="0"> Ya, Sebutkan  
				</c:otherwise>
			</c:choose>
		</td>
		<td><input type="text" id="mku_flag_bp_ket" name="mku_flag_bp_ket" value="${hasil.mku_flag_bp_ket}"></td>
	</tr>
	<tr>
		<th colspan="6">&nbsp;</th>
	</tr>
	<tr style="text-align: left;">
		<th>&nbsp;</th>
		<td colspan="5"><u><b><font face="Verdana" size="1" color="#996600">Data Usaha yang dimiliki Business Partner (Data ini diisi, jika anda mempunyai usaha)</font></b></u></td>
	</tr>
	<tr style="text-align: left;">
		<th>&nbsp;</th>
		<th>
			<b><font face="Verdana" size="1">Nama Perusahaan</font></b> &nbsp; <input type="text" id="mku_nama_perusahaan" name="mku_nama_perusahaan" value="${hasil.mku_nama_perusahaan}" size="30" maxlength="50" tabindex="8">
		</th>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<th><b><font face="Verdana" size="1">Jabatan</font></b></th>
		<td><input type="text" id="mku_jabatan_perusahaan" name="mku_jabatan_perusahaan" value="${hasil.mku_jabatan_perusahaan }" size="30" maxlength="50" tabindex="8"></td>
	</tr>
	<tr style="text-align: left;">
		<th>&nbsp;</th>
		<th> 
			<b><font face="Verdana" size="1">Jenis Usaha</font></b> &nbsp; <input type="text" id="mku_jenis_perusahaan" name="mku_jenis_perusahaan" value="${hasil.mku_jenis_perusahaan }" size="30" maxlength="50" tabindex="8">
		</th>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<th><b><font face="Verdana" size="1">Telepon</font></b></th>
		<td><input type="text" id="mku_telepon_perusahaan" name="mku_telepon_perusahaan" value="${hasil.mku_telepon_perusahaan}" size="30" maxlength="50" tabindex="8"></td>
	</tr>
	<tr style="text-align: left;">
		<th>&nbsp;</th>
		<th>
			<b><font face="Verdana" size="1">Alamat Kantor</font></b>
			<textarea cols="40" rows="4" id="mku_alamat_perusahaan" name="mku_alamat_perusahaan" 
				tabindex="15" onkeyup="textCounter(this, 200);"	onkeydown="textCounter(this, 200);">${hasil.mku_alamat_perusahaan}
			</textarea>
		</th>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<th><b><font face="Verdana" size="1">Kode Pos</font></b></th>
		<td><input type="text" id="mku_kodepos_perusahaan" name="mku_kodepos_perusahaan" value="${hasil.mku_kodepos_perusahaan}" size="30" maxlength="50" tabindex="8"></td>
	</tr>
	<tr>
		<th colspan="6">&nbsp;</th>
	</tr>
	<tr>
		<th colspan="6"  class="subtitle" height="20">
			<p align="center"><input type="submit" id="edit" name="edit" value="Update"></th>
	</tr>
	
</table>
</form>
<script type="text/javascript">
$().ready(function(){
	// (jQueryUI datepicker) init semua field datepicker
	$(".tgl").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: "dd/mm/yy" 
	});
	
	if('${hasil}'!='') {
		get_leader_detil('${hasil.mst_leader}','3');
	}
	
	$('#mku_jenis_rekrut').change(function(){
		var jen = $('#mku_jenis_rekrut').val();
		if(jen=='0'){
			$('#mku_rekruiter').value='';
			$('#mku_rekruiter').attr('readonly',true);
			$('#mku_rekruiter').css('background-color','#D4D4D4');
			$('#msrk_id').attr('readonly',true);
			$('#msrk_id').css('background-color','#D4D4D4');
		}else{
			$('#mku_rekruiter').value='';
			$('#mku_rekruiter').attr('readonly',true);
			$('#mku_rekruiter').css({'background-color':'#D4D4D4','fontColor':'#D4D4D4'});
			$('#mku_acc_rekruiter').attr('readonly',true);
			$('#mku_acc_rekruiter').css('background-color','#D4D4D4');
			$('#mku_bank_id').attr('readonly',true);
			$('#mku_bank_id').css('background-color','#D4D4D4');
			$('#msrk_id').attr('readonly',false);
			$('#msrk_id').css('background-color','#FFFFFF');
		}
		switch(jen){
		case '1':
	    	alert('Silahkan masukkan kode rekruter pada kolom isian kode rekruter');
	    	break;
	    case '2':
	    	alert('Silahkan masukkan kode rekruter pada kolom isian kode rekruter');
	    	break;
	    case '3':
	       	alert('Silahkan masukkan kode agent pada kolom isian kode rekruter');
	    	break;
	    case '4':
	       	alert('Silahkan masukkan no polis pada kolom isian kode rekruter');
	    	break;
	    case '5':
	       	alert('Silahkan masukkan nik karyawan pada kolom isian kode rekruter');
	    	break;
		}
	});
	
	$('#mst_leader').blur(function(){
		var kode = $(this).val();
		var url = '${path}/rekruitment/multi.htm?window=ajax&pages=rekrut_edit&jn=mst_leader&kode='+kode+'&kodepil=3';
		$.getJSON(url,function(result){
			$('#mku_nama_atasan').val(result.data_leader.nama);
			$('#mku_posisi_atasan').val(result.data_leader.jabatan);
		});
	});
	
	function get_leader_detil(kode, kodepil){
		var url = '${path}/rekruitment/multi.htm?window=ajax&pages=rekrut_edit&jn=mst_leader&kode='+kode+'&kodepil='+kodepil;
		$.getJSON(url,function(result){
			$('#mku_nama_atasan').val(result.data_leader.nama);
			$('#mku_posisi_atasan').val(result.data_leader.jabatan);
		});
	}
	
	$('#msrk_id').blur(function(){
		var kode = $(this).val();
		var kodepil = $('#mku_jenis_rekrut').val();
		var url = '${path}/rekruitment/multi.htm?window=ajax&pages=rekrut_edit&jn=mst_leader&kode='+kode+'&kodepil='+kodepil;
		$.getJSON(url,function(result){
			$('#mku_rekruiter').val(result.data_leader.nama);
			$('#mku_posisi_rekruter').val(result.data_leader.jabatan);
		});
	});
	
});
</script>
</body>
