<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<html>
<head>
	<jsp:include page="/include/page/header_jquery.jsp">
		<jsp:param value="${path}" name="path"/>
	</jsp:include>
</head>
  
<body>
<form method="post" name="formpost" style="text-align: center;" enctype="multipart/form-data">
<fieldset>
	<legend>INPUT AAJI KANDIDAT KARYAWAN</legend>
	<table class="entry2">
		<tr>
			<th>No KTP</th>
			<td>
				<input type="text" name="ktp" id="ktp" size="50">
			</td>
		</tr>
		<tr>
			<th>NAMA</th>
			<td>
				<input type="text" name="nama" id="nama" size="50">
			</td>
		</tr>
		<tr>
			<th>Jenis Kelamin</th>
			<td>
				<select name="jk" id="jk">
					<option value="1" selected="selected">Laki-Laki</option>
					<option value="2">Perempuan</option>
				</select>
			</td>
		</tr>
		<tr>
			<th>Alamat</th>
			<td>
				<textarea rows="4" cols="50" id="alamat" name="alamat"></textarea>
			</td>
		</tr>
		<tr>
			<th>Tempat Lahir</th>
			<td>
				<input type="text" name="tempat" id="tempat"  title="tempat">
			</td>
		</tr>
		<tr>
			<th>Tanggal Lahir</th>
			<td>
				<input type="date" name="tl" id="tl"  title="tanggal lahir">
			</td>
		</tr>
		<tr>
			<th>Upload Dokumen</th>
			<td>
				<input type="file" name="file1" id="file1" size="50">
				<br/>format pdf
			</td>
		</tr>
		<tr>
			<td>
				&nbsp;
			</td>
			<td>
				<input type="submit" value="Simpan" name="btnSubmit" id="btnSubmit">
			</td>
		</tr>
	</table>
</fieldset>
</form>
<script type="text/javascript">
$(document).ready(function() {

	$("#tl").datepicker({
		dateFormat: 'dd-mm-yy',
        yearRange: '1900:c+1' ,
		changeMonth :true,
		changeYear : true
	});
	
	var pesan = '${pesan}';
	if(pesan!=null && pesan!=''){
		alert(pesan);
	}
// 	$("#btnSubmit").click();
	
    $("#ktp").keydown(function (e) { 
    	// Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});
</script>
</body>
</html>
