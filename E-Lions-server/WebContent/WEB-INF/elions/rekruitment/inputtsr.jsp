<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	    $().ready(function() {
		$("#tabs").tabs();
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		
		   var rows=$('table.some tr');
			
			var black=rows.filter('.black');
			var white=rows.filter('.white');
			black.hide();
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}	
		$("#btnSearch").click(function() {
			var nama=$("#searchNama").val();	
				
			if(nama==null || nama==""){	
				alert("Isi Nama yang ingin dicari terlebih dahulu");
				
			}else{
			$("#dp").empty();	
				var url = "${path}/rekruitment/printtsr.htm?window=handleRequest&json=1&nama="+nama;
				 			
				 $.getJSON(url, function(result) {
				 $("<option/>").val("0").html("------DAFTAR NAMA---------").appendTo("#dp");					
					$.each(result, function() {
						$("<option/>").val(this.KEY).html(this.VALUE).appendTo("#dp");
					});
					$("#dp option:first").attr('selected','selected');
				});	
				
			}			
	
		});
		
		$("#dp").change(function() {
			var z=$("#dp").val();
			
					
			if(z==0 || z==null){	
					
				alert("Pilih nama terlebih dahulu");
				
			}else{
				var url = "${path}/rekruitment/printtsr.htm?window=handleRequest&json=2&nama="+z;
				 			
				 $.getJSON(url, function(result) {
				  $.each(result, function() {
						 $("#nama").val(this.NAMA);
						 $("#ktp").val(this.KTP);
						 $("#alamat").val(this.ALAMAT);
						 $("#jabatan").val(this.JABATAN);
						 $("#bdate").val(this.BDATE);
						 $("#edate").val(this.EDATE);
						 $("#jangka").val(this.JANGKA);
					 	 $("#atasan").val(this.NAMA_ATASAN);
					 	 $("#jbt").val(this.JABATAN_ATASAN);
					 	 $("#tl").val(this.KOTA_LAHIR);
					 	 $("#tgll").val(this.TGL_LAHIR);
					 	 $("#jkl").val(this.KELAMIN);
					 	 $("#nip").val($("#dp").val());
				  });	
				});	
			
			
			}			
	
		});
		
		$("#dept2").change(function() {
			black.show();
			white.hide();
			
		});
		$("#dept").change(function() {
			white.show();
			black.hide();
			
		});
		$("#dept1").change(function() {
			white.show();
			black.hide();
			
		});
		$("#btnReset").click(function() {
						$("#nama").val("");
						 $("#ktp").val("");
						 $("#alamat").val("");
						 $("#jabatan").val("");
						 $("#bdate").val("");
						 $("#edate").val("");
						 $("#jangka").val("");
					 	 $("#atasan").val("");
					 	 $("#jbt").val("");
					 	  $("#nip").val("");
					 	  $("#tl").val("");
					 	 $("#tgll").val("");
					 	 $("#jkl").val("");
					 	 $("#dp").empty();
					 	  $("<option/>").val("0").html("------DAFTAR NAMA---------").appendTo("#dp");	
		
		});
		var index = $('div#tabs li a').index($('a[href="#tab-${showTab}"]').get(0));
		$('div#tabs').tabs({selected:index});
				
	  
		
	});
	
	function showBSB(){			
			var nip=document.getElementById('nip').value;
			var dept=document.getElementById('dept').checked;
			var dept1=document.getElementById('dept1').checked;
			var nama=document.getElementById('nama').value;
			var ktp=document.getElementById('ktp').value;
			var alamat=document.getElementById('alamat').value;
			var bdate=document.getElementById('bdate').value;
			var edate=document.getElementById('edate').value;
			var jangka=document.getElementById('jangka').value;
			var jabatan=document.getElementById('jabatan').value;
			var atasan=document.getElementById('atasan').value;
			var jbt=document.getElementById('jbt').value;
			var eva=document.getElementById('eva').value;
			var sk=document.getElementById('sk').value;	
			var noSurat=document.getElementById('no_surat').value;
			var tl=document.getElementById('tl').value;	
			var jkl=document.getElementById('jkl').value;	
			var tgll=document.getElementById('tgll').value;
			var jabatan2=document.getElementById('jabatan2').value;					
			if(document.getElementById('dept').checked==true){
				dept="63";
			}else if(document.getElementById('dept1').checked==true){
				dept="49";
			}else if(document.getElementById('dept2').checked==true){
				dept="7B";
			}	
			
			
 			if(nip=="" || dept=="" || nama=="" || ktp=="" || alamat=="" || jangka=="" || jabatan ==""
				||atasan=="" || jbt=="" || eva=="" || sk=="" || noSurat=="" || tl=="" || jkl=="" || tgll==""||jabatan2==""){
 			   alert("Harap lengkapi data inputan , Jika tidak ada datanya cukup isi dengan karakter -");
//             if(nip=="" || dept=="" || dept1=="" ){
//              alert("Harap lengkapi data inputan , Jika tidak ada datanya cukup isi dengan karakter -");
            
			}else{
				popWin('${path}/rekruitment/multi.htm?window=previewTsr&nip='+nip+'&dept='+dept+'&dept1='+dept1+'&nama='+nama+'&ktp='+ktp+'&alamat='+alamat+'&bdate='+bdate+'&edate='+edate+'&jangka='+jangka+'&jabatan='+jabatan+'&atasan='+atasan+'&jbt='+jbt+'&eva='+eva+'&sk='+sk+'&noSurat='+noSurat+'&tl='+tl+'&tgll='+tgll+'&jkl='+jkl+'&jabatan2='+jabatan2, 600, 700);
			}
				
			
		
     
	}
	
	
	function showSPAJ(nip){	
			
		popWin('${path}/rekruitment/multi.htm?window=viewDataTSR&nip='+nip, 400, 700);
	}
	
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

</style>


<body>
		<div id="tabs">		
				<ul>
					<li><a href="#tab-1">Input Data TSR </a></li>
					<li><a href="#tab-2">Report Input Surat</a></li>
					<li><a href="#tab-3">History</a></li>					
				</ul>
				<div id="tab-1">
					<div class="rowElem">
						<fieldset class="ui-widget ui-widget-content">	
								<legend class="ui-widget-header ui-corner-all"><div><h5>Cari Dari E-Recruitment</h5></div></legend>
								<div class="rowElem"> 
					 										<label>Masukan Nama:</label> 
					 										<input type="text" name="searchNama" id="searchNama" size="50">
					 										<input type="button" value="Search" name="btnSearch" id="btnSearch">
					 											
								</div>
								
								<div class="rowElem"> 
					 										<label>Pilih:</label> 
					 										<select  name="dp" id="dp" title="Silahkan pilih nama">												
																<c:forEach var="c" items="${daftarNama}" varStatus="s">
																	<option value="${c.key}">${c.value}</option>
														        </c:forEach> 
														    </select> 
					 										
					 											
								</div>	 	 
								
						</fieldset>
					</div>
						<form method="post" name="formpost" enctype="multipart/form-data">
							<fieldset class="ui-widget ui-widget-content">	
								<legend class="ui-widget-header ui-corner-all"><div><h5>Print Surat Kemitraan TSR</h5></div></legend>
								
									<table class = "some" >
									<tr>
												<th >DEPARTEMEN</th>
												<td>
													<label class="helvetica20 darkGrey alignMiddle"><input type="radio" name="dept" value="63" id="dept" <c:if test="${recheck eq 1}">checked="checked"</c:if>/><span>MALLASSURANCE</span></label>
													<label class="helvetica20 darkGrey alignMiddle"><input type="radio" name="dept" value="49" id="dept1" <c:if test="${recheck eq 2}">checked="checked"</c:if>/><span>DMTM</span></label>
													<label class="helvetica20 darkGrey alignMiddle"><input type="radio" name="dept" value="7B" id="dept2" <c:if test="${recheck eq 3}">checked="checked"</c:if>/><span>BANCASSURANCE</span></label>
												</td>
												
												
											</tr>
									<tr>
											<th>NIP</th>
											<td>
												<input type="text" name="nip" id="nip" value="${nip}" size="50">
											</td>
										</tr>			
										<tr>
											<th>NAMA</th>
											<td>
												<input type="text" name="nama" id="nama" size="50" value="${nama}">
											</td>
										</tr>
										<tr>
											<th>NO.KTP</th>
											<td>
												<input type="text" name="ktp" id="ktp" size="50" value="${ktp}">
											</td>
										</tr>										
										<tr>
											<th>ALAMAT</th>
											<td>
												<textarea rows="4" cols="50" id="alamat" name="alamat" >${alamat}</textarea>
											</td>
										</tr>
										<tr>
											<th>TERHITUNG SEJAK</th>
											<td>
													<input name="bdate" id="bdate" type="text" class="datepicker" title="Tanggal Awal" value="${bdate}"> s/d 
													<input name="edate" id="edate" type="text" class="datepicker" title="Tanggal Akhir" value="${edate}">
											
											</td>
										</tr>
										
										<tr>
											<th>JANGKA WAKTU</th>
											<td>
												<input type="text" name="jangka" id="jangka" size="3" value="${jangka}"><span>BULAN</span>
											</td>
										</tr>
										
										<tr class = "white">
											<th>JABATAN</th>
											<td>
												<input type="text" name="jabatan" id="jabatan" size="50" value="${jabatan}">
											</td>
										</tr>
										<tr class = "black" >
											<th>JABATAN</th>
											<td>
												<select  name="jabatan2" id="jabatan2" title="Silahkan pilih jabatan">															
													<option value="Junior FA">Junior</option>
													<option value="Senior FA">Senior</option>
													<option value="Executive FA">Executive</option>														       
											    </select> 
											</td>
										</tr>	
																	
																			
										<tr>
											<th>ATASAN LANGSUNG</th>
											<td>
												<input type="text" name="atasan" id="atasan" size="30" value="${atasan}">&nbsp;&nbsp;<span><label><b>JABATAN ATASAN</b></label><input type="text" name="jbt" id="jbt" size="50" value="${jbt}"></span>
											</td>
										</tr>
										
										<tr>
											<th>EVALUASI TETAP</th>
											<td>
												<input type="text" name="eva" id="eva" size="3" value="${eva}"><span>BULAN</span>
											</td>
										</tr>
										
										<tr>
											<th>SURAT KUASA</th>
											<td>
												<input type="text" name="sk" id="sk" size="50" value="${sk}">
											</td>
										</tr>
										<tr>
											<th>NOMOR SURAT</th>
											<td>
												<input type="text" name="no_surat" id="no_surat" size="50" value="${no_surat}">												
											</td>
										</tr>
										<tr>
											<th>TANGGAL LAHIR</th>
											<td>
												<input type="text" name="tl" id="tl" size="50" value="${tl}">												
											</td>
										</tr>
										<tr>
											<th>TEMPAT LAHIR</th>
											<td>
												<input type="text" name="tgll" id="tgll" size="50" value="${tgll}">												
											</td>
										</tr>
										<tr>
											<th>JENIS KELAMIN</th>
											<td>
												<input type="text" name="jkl" id="jkl" size="50" value="${jkl}">												
											</td>
										</tr>												
										<tr>
											<td>
												&nbsp;
												&nbsp;
												&nbsp;
											</td>
											<td>
												
											</td>
										</tr>
									</table>
									<div class="rowElem" style="margin-left: 500px;">
										<span><input type="button" value="Preview & Send" name="preview" id="preview" onclick="showBSB();"></span>&nbsp;&nbsp;<span><input type="button" value="Reset" name=btnReset id="btnReset"></span>
										<input type="hidden" name="app" value="1"> 
									</div>
							</fieldset>
						</form>
										
<!-- 						<div>						 -->
<!-- 							<iframe src="" name="docFrame" id="docFrame" width="100%">E-Lions</iframe> -->
						
<!-- 						</div> -->
										
									
				</div>
				
				<div id="tab-2">
						<form id="formPost1" name="formPost1" method="post" target="_blank"  >									
												<fieldset class="ui-widget ui-widget-content">	
													<div class="rowElem" id="dropdown1"> 
					 										<label>Jenis Distribusi:</label> 
					 											<select  name="dist1" id="dist1" title="Silahkan pilih jenis distribusi">												
					 												<c:forEach var="c" items="${listdist1}" varStatus="s">
																		<option value="${c.key}">${c.value}</option>
																	</c:forEach> 
					 											</select>									
																															
													</div>				
													
													<div class="rowElem">
																	<label>Periode Tanggal Kirim Surat :</label>
																	<input name="bdate1" id="bdate1" type="text" class="datepicker" title="Tanggal Awal" value="${bdate}"> s/d 
																	<input name="edate1" id="edate1" type="text" class="datepicker" title="Tanggal Akhir" value="${edate}">
													</div> 
					 								
												    <div class="rowElem"> 
															<input type="submit" name="showPDF" id="showPDF" value="Show (PDF)">
															<input type="submit" name="showXLS" id="showXLS" value="Show (Excel)">					 										
															<input type="hidden" name="distribusi1" id="distribusi1" value="ALL">	 
															<input type="hidden" name="company1" id="company1" value="ALL">	
															<input type="hidden" name="showReport" value="1"> 
													</div>	 	
													
											</fieldset>													
											
								</form>		       
				
				</div>
				
				<div id="tab-3">
						
						<form id="formPost2" name="formPost2" method="post" target=""  >									
												<fieldset class="ui-widget ui-widget-content">	
													<div class="rowElem" id="dropdown1"> 
					 										<label>Jenis Distribusi:</label> 
					 											<select  name="dist2" id="dist2" title="Silahkan pilih jenis distribusi">												
					 												<c:forEach var="c" items="${listdist2}" varStatus="s">
																		<option value="${c.key}">${c.value}</option>
																	</c:forEach> 
					 											</select>									
																															
													</div>				
													
													<div class="rowElem">
																	<label>Periode Tanggal Kirim Surat :</label>
																	<input name="bdate2" id="bdate2" type="text" class="datepicker" title="Tanggal Awal" value="${bdate}"> s/d 
																	<input name="edate2" id="edate2" type="text" class="datepicker" title="Tanggal Akhir" value="${edate}">
													</div> 
					 								
												    <div class="rowElem"> 
															<input type="submit" name="showPolis" id="showPolis" value="Show Data">
															
															<input type="hidden" name="showPolicy" value="1"> 
													</div>	 	
													
											</fieldset>
											 <fieldset class="ui-widget ui-widget-content">

											        <table class="displaytag" cellpadding='0' cellspacing='0' border="1" align="center" style="font-size: 11px">
														<tr>															
															
																											
															<th nowrap align="center" width="200" bgcolor="#b7b7b7">Nama</th>															
															<th nowrap align="center" width="60" bgcolor="#b7b7b7">Tanggal Awal</th>
															<th nowrap align="center" width="60" bgcolor="#b7b7b7">Tanggal Akhir</th>
															<th nowrap align="center" width="60" bgcolor="#b7b7b7">Evaluasi</th>														
															<th nowrap align="center" width="60" bgcolor="#b7b7b7">Jangka Waktu</th>
															<th nowrap align="center" width="200" bgcolor="#b7b7b7">Distirbution Chanel</th>
															<th nowrap align="center" width="100" bgcolor="#b7b7b7">Jabatan</th> 
															<th  width="100" bgcolor="#b7b7b7">Action</th>
															 
														</tr>
														<c:forEach items="${dpolis}" var="d">													
														<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';return true;" onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;">															
															
															<td align="center" width="200">${d.NAMA}</td>
															<td align="center" width="60">${d.TGL_AWAL}</td>															
															<td align="center" width="60">${d.TGL_AKHIR}</td>	
															<td align="center" width="60">${d.EVALUASI}</td>														
															<td align="center" width="60">${d.JANGKA}</td>															
															<td align="center" width="200">${d.DIS}</td>
															<td align="center" width="100">${d.JABATAN}</td>
															  <td>
																 	<input type="button" name="checkSpaj" id="checkSpaj" value="Detail" onclick="showSPAJ('${d.NIP}');">
																 	
															 	</td>
																							
														</tr>
													</c:forEach>		
											</table>
											</fieldset>
															
								
								</form>		       
				</div>			
		</div>		

			
	 </body>
</html>

