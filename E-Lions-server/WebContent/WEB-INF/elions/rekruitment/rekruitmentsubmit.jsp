<%@ include file="/include/page/header.jsp"%>
<%@ page import="com.ekalife.elions.model.User" %>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>
<script type="text/javascript">
function tools(){
	window.location.href = '${path}/rekruitment/multi.htm?window=rekrut_tools';
}
</script>
<body  bgcolor="ffffff" >


<form name="frmParam" method="post" >
	<table border="0" width="100%"  cellspacing="0" cellpadding="0" class="entry">
		<tr>
			<td>&nbsp;</td>
		</tr>	
          <tr > 
         
            <th class="subtitle" colspan="4" height="20" > 
              <p align="center"><b> <font face="Verdana" size="2" color="#FFFFFF">FORM REKRUITMENT KEAGENAN</font></b> 
               	
            </th>
          </tr>
        </table>
		<c:if test="${status eq \"input\"}">
			<c:if test="${mku_no_reg eq \"\"}">
				<table border="0" width="100%"  cellspacing="0" cellpadding="0" class="entry2">
				  <tr> 
				    <td bgcolor="#DDDDDD">&nbsp;</td>
				  </tr>
				  <tr>
				    <td >&nbsp;</td>
				  </tr>
				  <tr> 
				    <td width="67%" rowspan="12" align="left"  bgcolor="#800000"> 
					
				      <p align="center"><font face="Verdana" size="2" color="#FFFFFF"> 
				        <center>
				          <b> Rekruitment tidak berhasil di submit. Silahkan dicoba kembali atau hubungi administrator.
				          </b> 
		
				        </center>
				        </font></p>
				    </td>
				  </tr>
				</table>
			</c:if>
			<c:if test="${mku_no_reg ne \"\"}">	
				<table border="0" width="100%"  cellspacing="0" cellpadding="0" class="entry2">
				  <tr> 
				    <td bgcolor="#DDDDDD">&nbsp;</td>
				  </tr>
				  <tr>
				    <td >&nbsp;</td>
				  </tr>
				  <tr> 
				    <td width="67%" rowspan="12" align="left"  bgcolor="#800000"> 
					
				      <p align="center"><font face="Verdana" size="2" color="#FFFFFF"> 
				        <center>
				          <b> Rekruitment berhasil di submit. Silahkan catat nomor register rekruitment : <elions:mku_no_reg nomor="${mku_no_reg}"/>
				          </b> 
				          <br/><br/>
				          <input type="button" name="tools" id="tools" value="Tools &raquo;" tabindex="47" onclick="window.location.href = '${path}/rekruitment/multi.htm?window=rekrut_tools';">
				        </center>
				        </font></p>
				    </td>
				  </tr>
				</table>
			</c:if>
		</c:if>
		<c:if test="${status eq \"edit\"}">
			<c:if test="${mku_no_reg eq \"\"}">
			  <table border="0" width="100%"  cellspacing="0" cellpadding="0" class="entry2">
			    <tr> 
			      <td bgcolor="#DDDDDD">&nbsp;</td>
			    </tr>
			    <tr> 
			      <td >&nbsp;</td>
			    </tr>
			    <tr> 
			      <td width="67%" rowspan="13" align="left"  bgcolor="#800000"> <p align="center"><font face="Verdana" size="2" color="#FFFFFF"> 
			          <center>
			            <p><b> Rekruitment tidak berhasil diedit, Silahkan hubungi administrator. 
			              </b></p>
			          </center>
			          </font></p></td>
			    </tr>
			    <tr></tr>
			  </table>
			</c:if>
			<c:if test="${mku_no_reg ne \"\"}">
				<table border="0" width="100%"  cellspacing="0" cellpadding="0" class="entry2">
				  <tr> 
				    <td bgcolor="#DDDDDD">&nbsp;</td>
				  </tr>
				  <tr>
				    <td >&nbsp;</td>
				  </tr>
				  <tr> 
				    <td width="67%" rowspan="12" align="left"  bgcolor="#800000"> 
					
				      <p align="center"><font face="Verdana" size="2" color="#FFFFFF"> 
				        <center>
				          <b> Rekruitment  untuk nomor register rekruitment : <elions:mku_no_reg nomor="${mku_no_reg}"/> telah berhasil diedit.</b> 
				        </center>
				        </font></p>
				    </td>
				  </tr>
				</table>
			</c:if>
		</c:if>
		<!-- upload -->
		<c:if test="${status eq \"upload\"}">
			<c:if test="${mku_no_reg ne \"\"}">	
				<table border="0" width="100%"  cellspacing="0" cellpadding="0" class="entry2">
				  <tr> 
				    <td bgcolor="#DDDDDD">&nbsp;</td>
				  </tr>
				  <tr>
				    <td >&nbsp;</td>
				  </tr>
				  <tr> 
				    <td width="67%" rowspan="12" align="left"  bgcolor="#800000"> 
					
				      <p align="center"><font face="Verdana" size="2" color="#FFFFFF"> 
				        <center>
				          <b> ${keterangan }
				          </b> 
				        </center>
				        </font></p>
				    </td>
				  </tr>
				</table>
			</c:if>
		</c:if>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>