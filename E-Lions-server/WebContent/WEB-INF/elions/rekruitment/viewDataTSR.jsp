 <%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript">

	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		$("#tabs").tabs();
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		
		$("#btnSearch").click(function() {
			var nama=$("#searchNama").val();	
				
			if(nama==null || nama==""){	
				alert("Isi Nama yang ingin dicari terlebih dahulu");
				
			}else{
			$("#dp").empty();	
				var url = "${path}/rekruitment/printtsr.htm?window=handleRequest&json=1&nama="+nama;
				 			
				 $.getJSON(url, function(result) {
				 $("<option/>").val("0").html("------DAFTAR NAMA---------").appendTo("#dp");					
					$.each(result, function() {
						$("<option/>").val(this.KEY).html(this.VALUE).appendTo("#dp");
					});
					$("#dp option:first").attr('selected','selected');
				});	
				
			}			
	
		});
		
		$("#dp").change(function() {
			var z=$("#dp").val();
			alert (z);	
					
			if(z==0 || z==null){	
					
				alert("Pilih nama terlebih dahulu");
				
			}else{
				var url = "${path}/rekruitment/printtsr.htm?window=handleRequest&json=2&nama="+z;
				 			
				 $.getJSON(url, function(result) {
				  $.each(result, function() {
						 $("#nama").val(this.NAMA);
						 $("#ktp").val(this.KTP);
						 $("#alamat").val(this.ALAMAT);
						 $("#jabatan").val(this.JABATAN);
						 $("#bdate").val(this.BDATE);
						 $("#edate").val(this.EDATE);
						 $("#jangka").val(this.JANGKA);
					 	 $("#atasan").val(this.NAMA_ATASAN);
					 	 $("#jbt").val(this.JABATAN_ATASAN);
					 	  $("#nip").val($("#dp").val());
				  });	
				});	
			
			
			}			
	
		});
		$("#btnReset").click(function() {
						$("#nama").val("");
						 $("#ktp").val("");
						 $("#alamat").val("");
						 $("#jabatan").val("");
						 $("#bdate").val("");
						 $("#edate").val("");
						 $("#jangka").val("");
					 	 $("#atasan").val("");
					 	 $("#jbt").val("");
					 	  $("#nip").val("");
					 	 $("#dp").empty();
					 	  $("<option/>").val("0").html("------DAFTAR NAMA---------").appendTo("#dp");	
		
		});
		
		 var index = $('div#tabs li a').index($('a[href="#tab-${showTab}"]').get(0));
        $('div#tabs').tabs({selected:index}); 
       
		
	});
	
	function showBSB(){			
			var nip=document.getElementById('nip').value;
			alert(nip);			
			popWin('${path}/rekruitment/multi.htm?window=previewTsr&nip='+nip, 600, 700);
	}
	
	
	function showSPAJ(nip){	
	
		popWin('${path}/rekruitment/multi.htm?window=viewDataTSR&nip='+nip, 400, 700);
	}
	
	
	
</script>


<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

</style>

<body>
		<div id="tabs">		
				<ul>
					<li><a href="#tab-1">View Data TSR </a></li>
								
				</ul>
				<div id="tab-1">					
						<form method="post" name="formpost" enctype="multipart/form-data">
							<fieldset class="ui-widget ui-widget-content">	
								<legend class="ui-widget-header ui-corner-all"><div><h5></h5></div></legend>
								
									<table >
									
									<tr>
											<th>NIP</th>
											<td>
												<input type="text" name="nip" id="nip" value="${dpolis.NIP}" size="50">
											</td>
										</tr>			
										<tr>
											<th>NAMA</th>
											<td>
												<input type="text" name="nama" id="nama" size="50" value="${dpolis.NAMA}">
											</td>
										</tr>
										<tr>
											<th>NO.KTP</th>
											<td>
												<input type="text" name="ktp" id="ktp" size="50" value="${dpolis.NO_KTP}">
											</td>
										</tr>										
										<tr>
											<th>ALAMAT</th>
											<td>
												<textarea rows="4" cols="50" id="alamat" name="alamat" >${dpolis.ALAMAT}</textarea>
											</td>
										</tr>
										<tr>
											<th>TERHITUNG SEJAK</th>
											<td>
													<input name="bdate" id="bdate" type="text"  title="Tanggal Awal" value="${dpolis.TGL_AWAL}"> s/d 
													<input name="edate" id="edate" type="text"  title="Tanggal Akhir" value="${dpolis.TGL_AKHIR}">
											
											</td>
										</tr>
										
										<tr>
											<th>JANGKA WAKTU</th>
											<td>
												<input type="text" name="jangka" id="jangka" size="3" value="${dpolis.JANGKA}"><span>BULAN</span>
											</td>
										</tr>
										
										<tr>
											<th>JABATAN</th>
											<td>
												<input type="text" name="jabatan" id="jabatan" size="50" value="${dpolis.JABATAN}">
											</td>
										</tr>									
																			
										<tr>
											<th>ATASAN LANGSUNG</th>
											<td>
												<input type="text" name="atasan" id="atasan" size="30" value="${dpolis.NAMA_ATASAN}">&nbsp&nbsp<span><label><b>JABATAN ATASAN</b></label><input type="text" name="jbt" id="jbt" size="50" value="${dpolis.JABATAN_ATASAN}"></span>
											</td>
										</tr>
										
										<tr>
											<th>EVALUASI TETAP</th>
											<td>
												<input type="text" name="eva" id="eva" size="3" value="${dpolis.EVALUASI}"><span>BULAN</span>
											</td>
										</tr>
										
										<tr>
											<th>SURAT KUASA</th>
											<td>
												<input type="text" name="sk" id="sk" size="50" value="${dpolis.NO_SURAT_DIREKSI}">
											</td>
										</tr>
										
											
										<tr>
											<th>NO. SURAT</th>
											<td>
												<input type="text" name="" id="noSurat" size="50" value="${dpolis.NO_SURAT}">
											</td>
										</tr>																			
										
										<tr>
											<td>
												&nbsp;
												&nbsp;
												&nbsp;
											</td>
											<td>
												
											</td>
										</tr>
									</table>
									
							</fieldset>
						</form>
										
<!-- 						<div>						 -->
<!-- 							<iframe src="" name="docFrame" id="docFrame" width="100%">E-Lions</iframe> -->
						
<!-- 						</div> -->
										
									
				</div>
				
				
		</div>		

			
	 </body>
</html>


