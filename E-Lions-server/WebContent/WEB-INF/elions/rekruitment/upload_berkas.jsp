<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />

<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->

<script type="text/javascript">
$().ready(function() {
	
	$('#tbHist').empty();
	var no_reg = $('#no_reg').val();
	var url = '${path}/rekruitment/multi.htm?window=ajax&pages=rekrut_tools&jn=showhist&no_reg='+no_reg;
	$.getJSON(url, function(result){
		$.each(result.history,function(id,val){
			$('<tr>').append(
				$('<td>').text(val.tgl),
				$('<td>').text(val.ket),
				$('<td>').text(val.pos),
				$('<td>').text(val.user),
				$('<td>').text(val.dept)
			).appendTo('#tbHist');
		});
	});
	$("#upload").removeAttr('disabled');
	$("#trans").removeAttr('disabled');
	
	$("#upload").click(function(){
		$('#tbHist').empty();
// 		var no_reg = $('#no_reg').val();
// 		var url = '${path}/rekruitment/multi.htm?window=ajax&pages=rekrut_tools&jn=showhist&no_reg='+no_reg;
		$.getJSON(url, function(result){
			$.each(result.history,function(id,val){
				$('<tr>').append(
					$('<td>').text(val.tgl),
					$('<td>').text(val.ket),
					$('<td>').text(val.pos),
					$('<td>').text(val.user),
					$('<td>').text(val.dept)
				).appendTo('#tbHist');
			});
		});
		$("#upload").removeAttr('disabled');
		$("#trans").removeAttr('disabled');
	});
});
hideLoadingMessage();
</script>
<body>
<form action="${path}/rekruitment/multi.htm?window=upload_berkas" method="post" enctype="multipart/form-data">
	<input type="hidden" id="no_reg" name="no_reg" value="${no_reg}">
	<input type="hidden" id="lus_id" name="lus_id" value="${sessionScope.currentUser.lus_id }">
	<c:if test="${not empty pesan }">
	<div id="success">
		${pesan }
	</div>
	</c:if>
	<fieldset> Upload Berkas
	<table>
		<tr>
			<th>Jenis Berkas</th>
			<td>:</td>
			<td>
				<select id="listUpload" name="listUpload">
					<c:forEach items="${listUpload}" var="l">
						<option value="${l.ID}">${l.KET }</option>
					</c:forEach>
				</select>
			</td>
		</tr>
		<tr>
			<th>File</th>
			<td>:</td>
			<td><input type="file" id="file1" name="file1"></td>
		</tr>
		<tr>
			<th>&nbsp;</th>
			<td>&nbsp;</td>
			<td><input type="submit" id="upload" name="upload" value="Save"></td>
		</tr>
		<tr>
			<td colspan="3"><em style="color: red;">* Format file pdf</em></th>
		</tr>
	</table>
	</fieldset>
	History Upload
	<table>
		<tr>
			<th>Tanggal</th>
			<th>Deskripsi</th>
			<th>Posisi</th>
			<th>User</th>
			<th>Department</th>
		</tr>
		<tr>
			<tbody id="tbHist"></tbody>
		</tr>
	</table>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>