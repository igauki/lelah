<%@ include file="/include/page/header.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<%User currentUser = (User) session.getAttribute("currentUser");%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>

<script language="JavaScript">
<!--

<spring:bind path="cmd.juml_daftarTanggungan">
		var juml_x=0;
	</spring:bind>
	function Body_onload() {
	document.frmParam.submit1.disabled = false;
		if ('${cmd.mku_jenis_rekrut}' == '1' )
		{
			document.frmParam.mku_rekruiter.readOnly=false;
			document.frmParam.mku_rekruiter.style.backgroundColor ='#FFFFFF';
			document.frmParam.msrk_id.readOnly=true;
			document.frmParam.msrk_id.style.backgroundColor ='#D4D4D4';
		}else if ('${cmd.mku_jenis_rekrut}' == '0')
			{
				document.frmParam.mku_rekruiter.readOnly=true;
				document.frmParam.mku_rekruiter.style.backgroundColor ='#D4D4D4';	
				document.frmParam.msrk_id.readOnly=true;
				document.frmParam.msrk_id.style.backgroundColor ='#D4D4D4';
			}else{
		document.frmParam.mku_rekruiter.readOnly=true;
				document.frmParam.mku_rekruiter.style.backgroundColor ='#D4D4D4';

				document.frmParam.msrk_id.readOnly=false;
				document.frmParam.msrk_id.style.backgroundColor ='#FFFFFF';
			}	
	}
	
	function DateValid(strDate) {
	
		var arrMonthDays = new Array (	"31", "29", "31",
						"30", "31", "30",
						"31", "31", "30",
						"31","30","31"	  );
		var intCount;
		var intDay;
		var intMonth;
		var	intYear;
	
		if (strDate.length != 10) return false;
	
	    intDay = strDate.substring(0,2);
		intMonth = strDate.substring(3,5);
		intYear = strDate.substring(6,11);
	
		if ((!isNumber(intDay)) || (!isNumber(intMonth)) || (!isNumber(intYear))){
			return false;
		}
		if (intYear==0) {
		return false;
	    }
	    if ((intMonth>12) || (intMonth<=0)) {
		    return false;
		}
	
		if ((parseFloat(intDay) > parseFloat(arrMonthDays[intMonth-1])) || (parseFloat(intDay)<=0))  {
		    return false;
		}
		if ((intMonth==2)&&
		   (intDay==arrMonthDays[1])&&
		   (intYear%4>0) ) {
			//alert("The end of this month is 28");
		    return false;
		}
	
		return  true;
	}	

	
// -->
</script>

<script language="JavaScript">
<!--
	if (!document.layers&&!document.all&&!document.getElementById)
		event="test"
		
	function showtip(current,e,text){
	
		if (document.all||document.getElementById){
			thetitle=text.split('<br>')
			if (thetitle.length>1){
				thetitles=''
				
			for (i=0;i<thetitle.length;i++)
				thetitles+=thetitle[i]
				
			current.title=thetitles
		}
		else
			current.title=text
	}
	
	else if (document.layers){
				document.tooltip.document.write('<layer bgColor="white" style="border:1px solid black;font-size:13px;">'+text+'</layer>')
				document.tooltip.document.close()
				document.tooltip.left=e.pageX+5
				document.tooltip.top=e.pageY+5
				document.tooltip.visibility="show"
			}
	}
	
	function hidetip(){
		if (document.layers)
			document.tooltip.visibility="hidden"
	}
			
	function kotak()
	{
		if (document.frmParam.msrk_id.value != "")
		{
			agen_rekruter_detil(document.frmParam.msrk_id.value, document.frmParam.mku_jenis_rekrut.value);
		}
	}
	
function agen_rekruter_detil(kodeagen,kodepil)
{
	ajaxManager.agen_rekruter_detil(kodeagen,kodepil,
		{callback:function(map) {
	
			DWRUtil.useLoadingMessage();
				document.frmParam.mku_rekruiter.value = map.nama;
				if (map.nama == "")
				{
					alert('Rekruter tersebut belum terdaftar');
				}
		   },
		  timeout:60000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
}	

function ganti_jenis()
{
	document.frmParam.msrk_id.value ="";
	if (document.frmParam.mku_jenis_rekrut.value == "1")
	{
		document.frmParam.mku_rekruiter.readOnly=false;
		document.frmParam.mku_rekruiter.style.backgroundColor ='#FFFFFF';
		document.frmParam.msrk_id.readOnly=true;
		document.frmParam.msrk_id.style.backgroundColor ='#D4D4D4';
		document.frmParam.msrk_id.value ='000000';
		document.frmParam.mku_rekruiter.value ='';
	}else if (document.frmParam.mku_jenis_rekrut.value == "0")
		{
			document.frmParam.mku_rekruiter.readOnly=true;
			document.frmParam.mku_rekruiter.style.backgroundColor ='#D4D4D4';	
			document.frmParam.msrk_id.readOnly=true;
			document.frmParam.msrk_id.style.backgroundColor ='#D4D4D4';
		}else{
			document.frmParam.mku_rekruiter.readOnly=true;
			document.frmParam.mku_rekruiter.style.backgroundColor ='#D4D4D4';
			document.frmParam.msrk_id.readOnly=false;
			document.frmParam.msrk_id.style.backgroundColor ='#FFFFFF';		
		}	
	 switch (document.frmParam.mku_jenis_rekrut.value) {
	 	case '1':
	    	alert('Silahkan masukkan nama rekruter pada kolom isian nama rekruter');
	    	break;
	    case '2':
	    	alert('Silahkan masukkan kode rekruter pada kolom isian kode rekruter');
	    	break;
	    case '3':
	       	alert('Silahkan masukkan kode agent pada kolom isian kode rekruter');
	    	break;
	    case '4':
	       	alert('Silahkan masukkan no polis pada kolom isian kode rekruter');
	    	break;
	    case '5':
	       	alert('Silahkan masukkan nik karyawan pada kolom isian kode rekruter');
	    	break;
	}
}

function ganti_region()
{
	document.frmParam.mku_region.value = document.frmParam.mku_regional.value ;
}	

		
function next()
{
	document.frmParam.mku_region.value = document.frmParam.mku_regional.value;
	document.frmParam.juml_x.value=juml_x;
	
	document.frmParam.submit();
	document.frmParam.submit1.disabled = true;
}		
			
// -->
</script>

<body bgcolor="ffffff" onLoad="Body_onload();">

 <XML ID=xmlData></XML>
<table border="0" width="100%" height="677" cellspacing="0" cellpadding="0" class="entry2">
  <tr > 
    <td width="67%" rowspan="5" align="left" valign="top"> 
      <form name="frmParam" method="post" >
		<table border="0" width="100%"  cellspacing="0" cellpadding="0" class="entry">
          <tr >
            <td colspan="4" height="20">&nbsp;
            	<spring:bind path="cmd.status">
					<input type="hidden" name="${status.expression}" value="${status.value }" size="30" maxlength="20" >
				</spring:bind>
				<spring:bind path="cmd.keterangan">
					<input type="hidden" name="${status.expression}" value="${status.value }" size="30" maxlength="20" >
				</spring:bind></td>
          </tr>
          <tr > 
            <th class="subtitle" colspan="4" height="20" > 
              <p align="center"><b> <font face="Verdana" size="1" color="#FFFFFF">FORM REKRUITMENT REGIONAL</font></b> 
               	
            </th>
          </tr>
        </table>       
        <table border="0" width="100%" cellspacing="0" cellpadding="0" class="entry">
			<tr>
							<th>
								&nbsp;
							</th>
							<th>
								<b><font color="#996600" size="1" face="Verdana">No Register Rekruitment</font>
								</b>
							</th>
							<th colspan="5">
								<spring:bind path="cmd.mku_no_reg">
									<input type="text" name="${status.expression}" value="${status.value }" size="30" maxlength="10" tabindex="1" readOnly style='background-color:#D4D4D4'>
								</spring:bind>
							</th>
						</tr>        
						<tr>
							<th>
								<b><font face="Verdana" size="1" color="#996600">1. </font>
								</b>
							</th>
							<th>
								<b><font color="#996600" size="1" face="Verdana">Jenis Rekrut</font>
								</b>
							</th>
							<th colspan="5">
								<select name="mku_jenis_rekrut" tabindex="2"
									onchange="ganti_jenis();"
									<c:if test="${ not empty status.errorMessage}">style='background-color:#FFE1FD'</c:if>>
									<c:forEach var="rekrut" items="${jenis_rekrut}">
										<option
											<c:if test="${cmd.mku_jenis_rekrut eq rekrut.ID}"> SELECTED </c:if>
											value="${rekrut.ID}">
											${rekrut.JENIS}
										</option>
									</c:forEach>
								</select><font color="#CC3300">*</font>
							</th>
						</tr>
						<tr>
							<th width="21">
								<b> <font face="Verdana" size="1" color="#996600">2</font><font
									face="Verdana" size="1" color="#996600">. </font>
								</b>
							</th>
							<th width="216">
								<b> <font face="Verdana" size="1" color="#996600">Kode
										Rekruter</font>
								</b>
							</th>
							<th width="96">
								<spring:bind path="cmd.msrk_id">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="15" tabindex="3"
										onchange="kotak();"
										<c:if test="${ not empty status.errorMessage}"> 
              style='background-color :#FFE1FD' </c:if>>
									<font color="#CC3300">*</font>
								</spring:bind>
							</th>
							<th width="165">
								<b> <font face="Verdana" size="1" color="#996600">Nama
										Rekruter</font>
								</b>
							</th>
							<th colspan="3">
								<spring:bind path="cmd.mku_rekruiter">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="50" tabindex="4"
										<c:if test="${ not empty status.errorMessage}"> 
              style='background-color :#FFE1FD' </c:if>>
									<font color="#CC3300">*</font>
								</spring:bind>
							</th>
						</tr>
						<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">3</font><font
									face="Verdana" size="1" color="#996600">. </font> </b>
							</th>
							<th>
								<b><font face="Verdana" size="1" color="#996600">Bank Rekruter</font></b>
							</th>
							<th>
								<input type="text" name="caribank2"
									onkeypress="if(event.keyCode==13){ document.frmParam.btncari2.click(); return false;}">
								<input type="button" name="btncari2" value="Cari"
									onclick="ajaxSelectWithParam(document.frmParam.caribank2.value,'select_bank2','bank2','mku_bank_id','', 'BANK_ID', 'BANK_NAMA', '');">
								<br>
								<div id="bank2">${mku_bank_id}
									<select name="mku_bank_id"
										<c:if test="${ not empty status.errorMessage}"> style='background-color :#FFE1FD' </c:if>>
										<option value="${cmd.mku_bank_id}">
											${cmd.mku_lbn_nama}
										</option>
									</select><font color="#CC3300">*</font>
							</th>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">No
										ACC Rekruter</font> </b>
							</th>
							<th>
								<spring:bind path="cmd.mku_acc_rekruiter">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="20" tabindex="5"
										<c:if test="${ not empty status.errorMessage}"> 
              style='background-color :#FFE1FD' </c:if>>
									<font color="#CC3300">*</font>
								</spring:bind>
							</th>
						</tr>
						<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">4</font><font
									face="Verdana" size="1" color="#996600">. </font> </b>
							</th>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">Kode
										Regional</font>
								</b>
							</th>
							<th>
								<spring:bind path="cmd.mku_region">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="10"
										style='background-color :#D4D4D4' readOnly>
								</spring:bind>
							</th>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">Regional
										/ Cabang</font>
								</b>
							</th>
							<th>
								<select name="mku_regional" tabindex="6"
									onchange="ganti_region();"
									<c:if test="${ not empty status.errorMessage}"> 
              style='background-color :#FFE1FD' </c:if>>
									<c:forEach var="regional" items="${select_regional}">
										<c:if test="${fn:substring(regional.ID, 0, 2) ne \"09\" &&  fn:substring(regional.ID, 0, 2) ne \"08\" && fn:substring(regional.ID, 0, 2) ne \"37\" && fn:substring(regional.ID, 0, 2) ne \"52\" && fn:substring(regional.ID, 0, 2) ne \"16\" }"> 
										<option
											<c:if test="${cmd.mku_regional eq regional.ID}"> SELECTED </c:if>
											value="${regional.ID}">
											${regional.REGION}
										</option>
										</c:if>
									</c:forEach>
								</select><font color="#CC3300">*</font>
							</th>
						</tr>
						<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">5</font><font
									face="Verdana" size="1" color="#996600">. </font> </b>
							</th>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">Tanggal</font>
								</b>
							</th>
							<th>
								<spring:bind path="cmd.mku_tglkues">
									<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
								</spring:bind><font color="#CC3300">*</font>
							</th>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">Diundang Oleh</font> </b>
							</th>
							<th>
								<spring:bind path="cmd.mku_diundang">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="50" tabindex="7"
										<c:if test="${ not empty status.errorMessage}">   style='background-color :#FFE1FD' </c:if>>
									<font color="#CC3300">*</font>
								</spring:bind>
							</th>
						</tr>
						<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">6</font><font
									face="Verdana" size="1" color="#996600">. </font> </b>
							</th>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">Nama</font>
								</b>
							</th>
							<th>
								<spring:bind path="cmd.mku_first">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="50" tabindex="8"
										<c:if test="${ not empty status.errorMessage}"> 
              style='background-color :#FFE1FD' </c:if>>
									<font color="#CC3300">*</font>
								</spring:bind>
							</th>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">Jenis
										Kelamin</font> </b>
							</th>
							<th>
								<select name="mku_sex" tabindex="9"
									<c:if test="${ not empty status.errorMessage}"> 
              style='background-color :#FFE1FD' </c:if>>
									<option
										<c:if test="${cmd.mku_sex eq 1}"> 
              SELECTED </c:if>
										value="1">
										Pria
									</option>
									<option
										<c:if test="${cmd.mku_sex eq 0}"> 
              SELECTED </c:if>
										value="0">
										Wanita
									</option>
								</select><font color="#CC3300">*</font>
							</th>
						</tr>
						<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">7</font><font
									face="Verdana" size="1" color="#996600">. </font> </b>
							</th>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">Jenis
										Identitas</font> </b>
							</th>
							<th>
								<select name="mku_identity_id" tabindex="10"
									<c:if test="${ not empty status.errorMessage}"> 
              style='background-color :#FFE1FD' </c:if>>
									<option
										<c:if test="${cmd.mku_identity_id eq 1}"> 
              SELECTED </c:if>
										value="1">
										KTP
									</option>
									<option
										<c:if test="${cmd.mku_identity_id eq 2}"> 
              SELECTED </c:if>
										value="2">
										SIM
									</option>
								</select><font color="#CC3300">*</font>
							</th>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">No
										Identitas</font> </b>
							</th>
							<th>
								<spring:bind path="cmd.mku_no_identity">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="30" tabindex="11"
										<c:if test="${ not empty status.errorMessage}"> 
              style='background-color :#FFE1FD' </c:if>>
									<font color="#CC3300">*</font>
								</spring:bind>
							</th>
						</tr>
						<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">8</font><font
									face="Verdana" size="1" color="#996600">. </font> </b>
							</th>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">Tempat
										Lahir</font>
								</b>
							</th>
							<th>
								<spring:bind path="cmd.mku_place_birth">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="30" tabindex="12"
										<c:if test="${ not empty status.errorMessage}"> 
              style='background-color :#FFE1FD' </c:if>>
									<font color="#CC3300">*</font>
								</spring:bind>
							</th>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">Tanggal
										Lahir / Usia</font> </b>
							</th>
							<th>
								<spring:bind path="cmd.mku_date_birth">
									<script>inputDate('${status.expression}', '${status.value}', false,'');</script>
								</spring:bind><font color="#CC3300">*</font>
								
								<select name="mku_usia" tabindex="13"
									<c:if test="${ not empty status.errorMessage}"> 
             						 style='background-color :#FFE1FD' </c:if>>
									<option
										<c:if test="${cmd.mku_usia eq 1}">   SELECTED </c:if> value="1">
										< 23 tahun
									</option>
									<option
										<c:if test="${cmd.mku_usia eq 2}">   SELECTED </c:if> value="2">
										23 - 24 tahun
									</option>
									<option
										<c:if test="${cmd.mku_usia eq 3}">   SELECTED </c:if> value="3">
										25 - 29 tahun
									</option>
									<option
										<c:if test="${cmd.mku_usia eq 4}">   SELECTED </c:if> value="4">
										30 - 39 tahun
									</option>
									<option
										<c:if test="${cmd.mku_usia eq 5}">   SELECTED </c:if> value="5">
										> 40 tahun
									</option>	
								</select>
							</th>
						</tr>
						<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">9</font><font
									face="Verdana" size="1" color="#996600">. </font> </b>
							</th>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">Jenis
										Alamat</font> </b>
							</th>
							<th colspan="3">
								<select name="mku_jns_alamat" tabindex="14"
									<c:if test="${ not empty status.errorMessage}"> 
              style='background-color :#FFE1FD' </c:if>>
									<option
										<c:if test="${cmd.mku_jns_alamat eq 1}"> 
              SELECTED </c:if>
										value="1">
										Alamat Rumah
									</option>
									<option
										<c:if test="${cmd.mku_jns_alamat eq 2}"> 
              SELECTED </c:if>
										value="2">
										Alamat Kantor
									</option>
								</select><font color="#CC3300">*</font>
							</th>
						</tr>
						<tr>
							<th rowspan="2">
							</th>
							<th rowspan="2">
								<b> <font face="Verdana" size="1" color="#996600">Alamat</font>
								</b>
							</th>
							<th rowspan="2">
								<spring:bind path="cmd.mku_alamat">
									<textarea cols="40" rows="7" name="${status.expression }"
										tabindex="15" onkeyup="textCounter(this, 200); "
										onkeydown="textCounter(this, 200); " maxlength="30" size ="30"
										<c:if test="${ not empty status.errorMessage}"> 
              style='background-color :#FFE1FD' </c:if>>${status.value }</textarea>
									<font color="#CC3300">*</font>
								</spring:bind>
							</th>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">Kota</font>
								</b>
							</th>
							<th>
								<spring:bind path="cmd.mku_kota">
									<br>
									<input type="text" name="${status.expression }"
										id="${status.expression }" value="${status.value }"
										onfocus="this.select();" tabindex="16" maxlength="40"
										<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
									<span id="indicator_rumah" style="display:none;"><img
											src="${path}/include/image/indicator.gif" />
									</span>
								</spring:bind><font color="#CC3300">*</font>
							</th>
							<th rowspan="2"></th>
						</tr>
						<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">Kode
										Pos</font> </b>
							</th>
							<th>
								<spring:bind path="cmd.mku_kd_pos">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="10" maxlength="10" tabindex="17"
										<c:if test="${ not empty status.errorMessage}"> 
              style='background-color :#FFE1FD' </c:if>>
									<font color="#CC3300">*</font>
								</spring:bind>
							</th>
						</tr>
						<tr>
							<th>
							</th>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">Telpon
										Rumah</font>
								</b>
							</th>
							<th>
								<spring:bind path="cmd.mku_area_rumah">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="5" maxlength="4" tabindex="18"
										<c:if test="${ not empty status.errorMessage}"> 
              style='background-color :#FFE1FD' </c:if>>
									<font color="#CC3300">*</font>
								</spring:bind>
								<spring:bind path="cmd.mku_tlprumah">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="20" tabindex="19"
										<c:if test="${ not empty status.errorMessage}"> 
              style='background-color :#FFE1FD' </c:if>>
									<font color="#CC3300">*</font>
								</spring:bind>
							</th>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">Telpon
										Kantor</font> </b>
							</th>
							<th>
								<spring:bind path="cmd.mku_area_kantor">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="5" maxlength="4" tabindex="20"
										<c:if test="${ not empty status.errorMessage}"> 
              style='background-color :#FFE1FD' </c:if>>
									<font color="#CC3300">*</font>
								</spring:bind>
								<spring:bind path="cmd.mku_tlpkantor">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="20" tabindex="21"
										<c:if test="${ not empty status.errorMessage}"> 
              style='background-color :#FFE1FD' </c:if>>
									<font color="#CC3300">*</font>
								</spring:bind>
							</th>
						</tr>
						<tr>
							<th></th>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">HP</font>
								</b>
							</th>
							<th>
								<spring:bind path="cmd.mku_area_hp">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="5" maxlength="4" tabindex="22"
										<c:if test="${ not empty status.errorMessage}"> 
              style='background-color :#FFE1FD' </c:if>>
									<font color="#CC3300">*</font>
								</spring:bind>
								<spring:bind path="cmd.mku_hp">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="20" tabindex="23"
										<c:if test="${ not empty status.errorMessage}"> 
              style='background-color :#FFE1FD' </c:if>>
									<font color="#CC3300">*</font>
								</spring:bind>
							</th>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">Tanggal Terima Berkas</font>
								</b>
							</th>
							<th>
								<spring:bind path="cmd.mku_tgl_berkas">
									<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
								</spring:bind><font color="#CC3300">*</font>
							</th>
						</tr>
						
						<%-- req pak yusuf--%>
						<tr>
						<th>
                                <b> <font face="Verdana" size="1" color="#996600">10</font><font
                                    face="Verdana" size="1" color="#996600">. </font> </b>
                            </th>
                            <th>
                                <b> <font face="Verdana" size="1" color="#996600">Bank
                                        </font> </b>
                            </th>
                          <th colspan="1">
                                <select name="mku_bank_id2" tabindex="2"
                                    <c:if test="${ not empty status.errorMessage}">style='background-color 
              :#FFE1FD'</c:if>>
                                    <c:forEach var="bank" items="${bank}">
                                        <option
                                            <c:if test="${cmd.mku_bank_id2 eq bank.ID}"> SELECTED </c:if>
                                            value="${bank.ID}">
                                            ${bank.NAMA}
                                        </option>
                                    </c:forEach>
                                </select><font color="#CC3300">*</font>
                            </th>
                            <th>
                                <b> <font face="Verdana" size="1" color="#996600">No
                                        Rekening</font> </b>
                            </th>
                            <th>
                                <spring:bind path="cmd.mku_acc_cust">
                                    <input type="text" name="${status.expression}"
                                        value="${status.value }" size="30" maxlength="20" tabindex="5"
                                        <c:if test="${ not empty status.errorMessage}"> 
              style='background-color :#FFE1FD' </c:if>>
                                    <font color="#CC3300">*</font>
                                </spring:bind>
                            </th>
						</tr>
						
						<tr>
							<th class="subtitle" colspan="5" height="20">
								<p align="center">
									<b> <font face="Verdana" size="1" color="#FFFFFF">KUESIONER</font>
									</b>
							</th>
						</tr>
<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">1</font><font
									face="Verdana" size="1" color="#996600">. </font> </b>
							</th>
							<th colspan="2">
								<b> <font face="Verdana" size="1" color="#996600">Status
										anda saat ini :</font>
								</b>
							</th>
							<th colspan="2">
								<select name="mku_status" tabindex="24"
									<c:if test="${ not empty status.errorMessage}"> 
             						 style='background-color :#FFE1FD' </c:if>>
									<option
										<c:if test="${cmd.mku_status eq 1}">   SELECTED </c:if> value="1">
										Menikah
									</option>
									<option
										<c:if test="${cmd.mku_status eq 2}">   SELECTED </c:if> value="2">
										Lajang
									</option>
									<option
										<c:if test="${cmd.mku_status eq 3}">   SELECTED </c:if> value="3">
										Cerai
									</option>
								</select><font color="#CC3300">*</font>
							</th>
						</tr>
<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">2</font><font
									face="Verdana" size="1" color="#996600">. </font> </b>
							</th>
							<th colspan="2">
								<b> <font face="Verdana" size="1" color="#996600">Orang yang menjadi tanggungan anda saat ini :</font>
								</b>
							</th>
							<th colspan="2">
							<select name="mku_tanggungan" tabindex="25"
									<c:if test="${ not empty status.errorMessage}"> 
             						 style='background-color :#FFE1FD' </c:if>>
									<option
										<c:if test="${cmd.mku_tanggungan eq 1}">   SELECTED </c:if> value="1">
										Tidak Ada
									</option>
									<option
										<c:if test="${cmd.mku_tanggungan eq 2}">   SELECTED </c:if> value="2">
										1 orang
									</option>
									<option
										<c:if test="${cmd.mku_tanggungan eq 3}">   SELECTED </c:if> value="3">
										2 - 4 orang
									</option>
									<option
										<c:if test="${cmd.mku_tanggungan eq 4}">   SELECTED </c:if> value="4">
										5 orang atau lebih
									</option>
								</select><font color="#CC3300">*</font>
							</th>
						</tr>
						<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">3</font><font
									face="Verdana" size="1" color="#996600">. </font> </b>
							</th>
							<th colspan="2">
								<b> <font face="Verdana" size="1" color="#996600">Pendidikan terakhir anda adalah :</font>
								</b>
							</th>
							<th colspan="2">
							<select name="mku_pendidikan" tabindex="26"
									<c:if test="${ not empty status.errorMessage}"> 
             						 style='background-color :#FFE1FD' </c:if>>
									<option
										<c:if test="${cmd.mku_pendidikan eq 1}">   SELECTED </c:if> value="1">
										Sarjana / S1
									</option>
									<option
										<c:if test="${cmd.mku_pendidikan eq 2}">   SELECTED </c:if> value="2">
										Kuliah
									</option>
									<option
										<c:if test="${cmd.mku_pendidikan eq 3}">   SELECTED </c:if> value="3">
										SMA / Sederajat
									</option>
									<option
										<c:if test="${cmd.mku_pendidikan eq 4}">   SELECTED </c:if> value="4">
										Tidak lulus SMA
									</option>
								</select><font color="#CC3300">*</font>
							</th>
						</tr>
						<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">4</font><font
									face="Verdana" size="1" color="#996600">. </font> </b>
							</th>
							<th colspan="2">
								<b> <font face="Verdana" size="1" color="#996600">Pekerjaan anda saat ini : </font>
								</b>
								<select name="mku_pekerjaan" tabindex="27"
									<c:if test="${ not empty status.errorMessage}"> 
             						 style='background-color :#FFE1FD' </c:if>>
									<option
										<c:if test="${cmd.mku_pekerjaan eq 1}">   SELECTED </c:if> value="1">
										Profesional :
									</option>
									<option
										<c:if test="${cmd.mku_pekerjaan eq 2}">   SELECTED </c:if> value="2">
										Karyawan Tetap
									</option>
									<option
										<c:if test="${cmd.mku_pekerjaan eq 3}">   SELECTED </c:if> value="3">
										Manager
									</option>
									<option
										<c:if test="${cmd.mku_pekerjaan eq 4}">   SELECTED </c:if> value="4">
										Sales & Marketing
									</option>
									<option
										<c:if test="${cmd.mku_pekerjaan eq 5}">   SELECTED </c:if> value="5">
										Staff Administrasi
									</option>
									<option
										<c:if test="${cmd.mku_pekerjaan eq 6}">   SELECTED </c:if> value="6">
										Wiraswasta
									</option>
									<option
										<c:if test="${cmd.mku_pekerjaan eq 7}">   SELECTED </c:if> value="7">
										Karyawan Paruh Waktu
									</option>
									<option
										<c:if test="${cmd.mku_pekerjaan eq 8}">   SELECTED </c:if> value="8">
										Ibu Rumah Tangga
									</option>
									<option
										<c:if test="${cmd.mku_pekerjaan eq 9}">   SELECTED </c:if> value="9">
										Pensiunan
									</option>
									<option
										<c:if test="${cmd.mku_pekerjaan eq 10}">   SELECTED </c:if> value="10">
										Mahasiswa / Pelajar
									</option>
									<option
										<c:if test="${cmd.mku_pekerjaan eq 11}">   SELECTED </c:if> value="11">
										Tidak Bekerja
									</option>
								</select><font color="#CC3300">*</font>
							</th>
							<th colspan="2">
							<b> <font face="Verdana" size="1" color="#996600">Keterangan : </font>
							<spring:bind path="cmd.mku_ket_pekerjaan">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="30" tabindex="28"
										<c:if test="${ not empty status.errorMessage}"> 
              								style='background-color :#FFE1FD' </c:if>>
									<font color="#CC3300">*</font>
								</spring:bind>
								</b>
							</th>
						</tr>
						<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">5</font><font
									face="Verdana" size="1" color="#996600">. </font> </b>
							</th>
							<th colspan="2">
								<b> <font face="Verdana" size="1" color="#996600">Penghasilan anda per bulan :</font>
								</b>
							</th>
							<th colspan="2">
							<select name="mku_penghasilan" tabindex="29"
									<c:if test="${ not empty status.errorMessage}"> 
             						 style='background-color :#FFE1FD' </c:if>>
									<option
										<c:if test="${cmd.mku_penghasilan eq 1}">   SELECTED </c:if> value="1">
										< Rp. 1,5 Juta
									</option>
									<option
										<c:if test="${cmd.mku_penghasilan eq 2}">   SELECTED </c:if> value="2">
										Rp. 1,5 juta - Rp. 3 juta
									</option>
									<option
										<c:if test="${cmd.mku_penghasilan eq 3}">   SELECTED </c:if> value="3">
										Rp. 3 juta - Rp. 5 juta
									</option>
									<option
										<c:if test="${cmd.mku_penghasilan eq 4}">   SELECTED </c:if> value="4">
										Rp. 5 juta - Rp. 7 juta
									</option>
									<option
										<c:if test="${cmd.mku_penghasilan eq 5}">   SELECTED </c:if> value="5">
										Rp. 7 juta - Rp. 9 juta
									</option>
									<option
										<c:if test="${cmd.mku_penghasilan eq 6}">   SELECTED </c:if> value="6">
										> Rp. 9 juta
									</option>
								</select><font color="#CC3300">*</font>
							</th>
						</tr>
						<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">6</font><font
									face="Verdana" size="1" color="#996600">. </font> </b>
							</th>
							<th colspan="2">
								<b> <font face="Verdana" size="1" color="#996600">Anda sudah mempunyai pengalaman bekerja selama :</font>
								</b>
							</th>
							<th colspan="2">
								<select name="mku_pengalaman" tabindex="30"
									<c:if test="${ not empty status.errorMessage}"> 
             						 style='background-color :#FFE1FD' </c:if>>
									<option
										<c:if test="${cmd.mku_pengalaman eq 1}">   SELECTED </c:if> value="1">
										Tidak Bekerja
									</option>
									<option
										<c:if test="${cmd.mku_pengalaman eq 2}">   SELECTED </c:if> value="2">
										< 1 Tahun
									</option>
									<option
										<c:if test="${cmd.mku_pengalaman eq 3}">   SELECTED </c:if> value="3">
										1 - 4 Tahun
									</option>
									<option
										<c:if test="${cmd.mku_pengalaman eq 4}">   SELECTED </c:if> value="4">
										5 - 9 Tahun
									</option>
									<option
										<c:if test="${cmd.mku_pengalaman eq 5}">   SELECTED </c:if> value="5">
										> 10 Tahun
									</option>
								</select><font color="#CC3300">*</font>
							</th>
						</tr>
						<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">7</font><font
									face="Verdana" size="1" color="#996600">. </font> </b>
							</th>
							<th colspan="2">
								<b> <font face="Verdana" size="1" color="#996600">Apakah anda memiliki kendaraan :</font>
								</b>
							</th>
							<th colspan="2">
							<select name="mku_kendaraan" tabindex="31"
									<c:if test="${ not empty status.errorMessage}"> 
             						 style='background-color :#FFE1FD' </c:if>>
									<option
										<c:if test="${cmd.mku_kendaraan eq 1}">   SELECTED </c:if> value="1">
										Mobil
									</option>
									<option
										<c:if test="${cmd.mku_kendaraan eq 2}">   SELECTED </c:if> value="2">
										Sepeda Motor
									</option>
									<option
										<c:if test="${cmd.mku_kendaraan eq 3}">   SELECTED </c:if> value="3">
										Tidak Ada
									</option>
								</select><font color="#CC3300">*</font>
							</th>
						</tr>
						<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">8</font><font
									face="Verdana" size="1" color="#996600">. </font> </b>
							</th>
							<th colspan="2">
								<b> <font face="Verdana" size="1" color="#996600">Organisasi yang anda ikuti saat ini :</font>
								</b>
							</th>
							<th colspan="2">
								<select name="mku_organisasi" tabindex="32"
									<c:if test="${ not empty status.errorMessage}"> 
             						 style='background-color :#FFE1FD' </c:if>>
									<option
										<c:if test="${cmd.mku_organisasi eq 1}">   SELECTED </c:if> value="1">
										Tidak Ada
									</option>
									<option
										<c:if test="${cmd.mku_organisasi eq 2}">   SELECTED </c:if> value="2">
										1 Organisasi
									</option>
									<option
										<c:if test="${cmd.mku_organisasi eq 3}">   SELECTED </c:if> value="3">
										2 Organisasi
									</option>
									<option
										<c:if test="${cmd.mku_organisasi eq 4}">   SELECTED </c:if> value="4">
										> 3 Organisasi
									</option>
								</select><font color="#CC3300">*</font>
								<spring:bind path="cmd.mku_ket_organisasi">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="40" tabindex="33"
										<c:if test="${ not empty status.errorMessage}"> 
              								style='background-color :#FFE1FD' </c:if>>
									<font color="#CC3300">*</font>
								</spring:bind>
							</th>
						</tr>
						<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">9</font><font
									face="Verdana" size="1" color="#996600">. </font> </b>
							</th>
							<th colspan="2">
								<b> <font face="Verdana" size="1" color="#996600">Anda tinggal di rumah :</font>
								</b>
							</th>
							<th colspan="2">
							<select name="mku_tinggal" tabindex="34"
									<c:if test="${ not empty status.errorMessage}"> 
             						 style='background-color :#FFE1FD' </c:if>>
									<option
										<c:if test="${cmd.mku_tinggal eq 1}">   SELECTED </c:if> value="1">
										Milik Sendiri
									</option>
									<option
										<c:if test="${cmd.mku_tinggal eq 2}">   SELECTED </c:if> value="2">
										Kredit
									</option>
									<option
										<c:if test="${cmd.mku_tinggal eq 3}">   SELECTED </c:if> value="3">
										Dinas / Asrama
									</option>
									<option
										<c:if test="${cmd.mku_tinggal eq 4}">   SELECTED </c:if> value="4">
										Kontrak / Sewa
									</option>
									<option
										<c:if test="${cmd.mku_tinggal eq 5}">   SELECTED </c:if> value="5">
										Keluarga / Orang Tua
									</option>
								</select><font color="#CC3300">*</font>
								<spring:bind path="cmd.mku_ket_tinggal">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="40" tabindex="35"
										<c:if test="${ not empty status.errorMessage}"> 
              								style='background-color :#FFE1FD' </c:if>>
									<font color="#CC3300">*</font>
								</spring:bind>
							</th>
						</tr>
						<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">10</font><font
									face="Verdana" size="1" color="#996600">. </font> </b>
							</th>
							<th colspan="2">
								<b> <font face="Verdana" size="1" color="#996600">Berapa banyak anda mempunyai kenalan di lingkungan anda  ?</font>
								</b>
							</th>
							<th colspan="2">
								<select name="mku_kenalan" tabindex="36"
									<c:if test="${ not empty status.errorMessage}"> 
             						 style='background-color :#FFE1FD' </c:if>>
									<option
										<c:if test="${cmd.mku_kenalan eq 1}">   SELECTED </c:if> value="1">
										101 hingga 150 orang
									</option>
									<option
										<c:if test="${cmd.mku_kenalan eq 2}">   SELECTED </c:if> value="2">
										51 hingga 100 orang
									</option>
									<option
										<c:if test="${cmd.mku_kenalan eq 3}">   SELECTED </c:if> value="3">
										0 hingga 50 orang
									</option>
								</select>
								<font color="#CC3300">*</font>
							</th>
						</tr>
						<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">11</font><font
									face="Verdana" size="1" color="#996600">. </font> </b>
							</th>
							<th colspan="2">
								<b> <font face="Verdana" size="1" color="#996600">Apakah anda pernah menjual polis asuransi jiwa ?</font>
								</b>
							</th>
							<th colspan="2">
								<select name="mku_pernah_jual" tabindex="37"
									<c:if test="${ not empty status.errorMessage}"> 
             						 style='background-color :#FFE1FD' </c:if>>
									<option
										<c:if test="${cmd.mku_pernah_jual eq 1}">   SELECTED </c:if> value="1">
										Ya
									</option>
									<option
										<c:if test="${cmd.mku_pernah_jual eq 2}">   SELECTED </c:if> value="2">
										Tidak
									</option>
								</select><font color="#CC3300">*</font>
							</th>
						</tr>
						<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">12</font><font
									face="Verdana" size="1" color="#996600">. </font> </b>
							</th>
							<th colspan="2">
								<b> <font face="Verdana" size="1" color="#996600">Apakah anda pernah bergabung menjadi tenaga pemasaran EKA 
        LIFE ?</font>
								</b>
							</th>
							<th colspan="2">
							<select name="mku_gabung_pernah" tabindex="38"
									<c:if test="${ not empty status.errorMessage}"> 
             						 style='background-color :#FFE1FD' </c:if>>
									<option
										<c:if test="${cmd.mku_gabung_pernah eq 1}">   SELECTED </c:if> value="1">
										Ya
									</option>
									<option
										<c:if test="${cmd.mku_gabung_pernah eq 2}">   SELECTED </c:if> value="2">
										Tidak
									</option>
								</select><font color="#CC3300">*</font>
							</th>
						</tr>
						<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">13</font><font
									face="Verdana" size="1" color="#996600">. </font> </b>
							</th>
							<th colspan="2">
								<b> <font face="Verdana" size="1" color="#996600">Apakah anda ingin bergabung menjadi tenaga pemasaran EKA 
        LIFE ? </font>
								</b>
							</th>
							<th colspan="2">
							<select name="mku_tenaga_ekalife" tabindex="39"
									<c:if test="${ not empty status.errorMessage}"> 
             						 style='background-color :#FFE1FD' </c:if>>
									<option
										<c:if test="${cmd.mku_ingin_gabung eq 1}">   SELECTED </c:if> value="1">
										Ya
									</option>
									<option
										<c:if test="${cmd.mku_ingin_gabung eq 2}">   SELECTED </c:if> value="2">
										Tidak
									</option>
								</select><font color="#CC3300">*</font>
							</th>
						</tr>
						<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">14</font><font
									face="Verdana" size="1" color="#996600">. </font> </b>
							</th>
							<th colspan="4">
								<b> <font face="Verdana" size="1" color="#996600">Dua nama yang anda referensikan untuk diundang pada Business 
        Opportunity Program yang akan datang :</font>
								</b>
							</th>
						</tr>
						<tr>
							<th>
								&nbsp;
							</th>
							<th colspan="2">
								<b> <font face="Verdana" size="1" color="#996600">Nama</font>
								</b>
							</th>
							<th colspan="2">
							<b> <font face="Verdana" size="1" color="#996600">Telp</font>
								</b>
							</th>
						</tr>
				<tr>
							<th>
								&nbsp;
							</th>
							<th colspan="2">
								<spring:bind path="cmd.mku_nama_1">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="40" tabindex="40"
										<c:if test="${ not empty status.errorMessage}"> 
              								style='background-color :#FFE1FD' </c:if>>
								</spring:bind>
							</th>
							<th colspan="2">
								<spring:bind path="cmd.mku_tlp_1">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="20" tabindex="41"
										<c:if test="${ not empty status.errorMessage}"> 
              								style='background-color :#FFE1FD' </c:if>>
								</spring:bind>
							</th>
						</tr>
				<tr>
							<th>
								&nbsp;
							</th>
							<th colspan="2">
								<spring:bind path="cmd.mku_nama_2">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="40" tabindex="42"
										<c:if test="${ not empty status.errorMessage}"> 
              								style='background-color :#FFE1FD' </c:if>>
								</spring:bind>
							</th>
							<th colspan="2">
								<spring:bind path="cmd.mku_tlp_2">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="20" tabindex="43"
										<c:if test="${ not empty status.errorMessage}"> 
              								style='background-color :#FFE1FD' </c:if>>
								</spring:bind>
							</th>
						</tr>
				<tr>
							<th>
								<b> <font face="Verdana" size="1" color="#996600">15</font><font
									face="Verdana" size="1" color="#996600">. </font> </b>
							</th>
							<th colspan="4">
								<b> <font face="Verdana" size="1" color="#996600">Direkomendasi Oleh :</font>
								</b>
							</th>
						</tr>
				<tr>
							<th>
								<select name="mku_level_rekomendasi" tabindex="44"
									<c:if test="${ not empty status.errorMessage}"> 
             						 style='background-color :#FFE1FD' </c:if>>
									<option
										<c:if test="${cmd.mku_level_rekomendasi eq 1}">   SELECTED </c:if> value="1">
										RM
									</option>
									<option
										<c:if test="${cmd.mku_level_rekomendasi eq 2}">   SELECTED </c:if> value="2">
										SBM
									</option>
									<option
										<c:if test="${cmd.mku_level_rekomendasi eq 3}">   SELECTED </c:if> value="3">
										BM
									</option>
									<option
										<c:if test="${cmd.mku_level_rekomendasi eq 4}">   SELECTED </c:if> value="4">
										AM
									</option>
									<option
										<c:if test="${cmd.mku_level_rekomendasi eq 5}">   SELECTED </c:if> value="5">
										UM
									</option>
								</select>
							</th>
							<th colspan="2">
								<font color="#CC3300">*</font>
							</th>
							<th colspan="2">
								<spring:bind path="cmd.mku_rekomendasi_nama">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="50" tabindex="45"
										<c:if test="${ not empty status.errorMessage}"> 
              								style='background-color :#FFE1FD' </c:if>>
									<font color="#CC3300">*</font>
								</spring:bind>
							</th>
						</tr>												
						<tr>
							<th class="subtitle" colspan="7">
								<div align="center">
									<input type="submit" name= "submit1" value="Submit &raquo;" tabindex="46" onclick="next();">
								</div>
							</th>
						</tr>
						<tr>
							<td colspan="9">
								<p align="left">
									<b> <font face="Verdana" size="2" color="#CC3300">Note
											: * Wajib diisi</font>
									</b>
								</p>
							</td>
						</tr>
					</table>
		<table border="0" width="100%"  cellspacing="0" cellpadding="0" class="entry">
          <tr >
			<td>		     
			<spring:bind path="cmd.*">
				<c:if test="${not empty status.errorMessages}">
				<div id="error">
				 ERROR:<br>
				<c:forEach var="error" items="${status.errorMessages}">
				 - <c:out value="${error}" escapeXml="false" />
				<br />
				</c:forEach></div>
				</c:if>									
			</spring:bind> 
			</td>
			</tr>
          <tr >
            <td colspan="4" height="20">${keterangan}</td>
          </tr>

        </table> 

      </form>
    </td>

  </tr>
</table>
<ajax:autocomplete
				  source="mku_kota"
				  target="mku_kota"
				  baseUrl="${path}/servlet/autocomplete?s=mku_kota&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_rumah"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />
</body>
<%@ include file="/include/page/footer.jsp"%>