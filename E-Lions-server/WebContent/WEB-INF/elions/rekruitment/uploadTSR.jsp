 <%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript">

	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		$("#tabs").tabs();
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		
		
		
		 var index = $('div#tabs li a').index($('a[href="#tab-${showTab}"]').get(0));
        $('div#tabs').tabs({selected:index}); 
       
		
	});
	
	function showBSB(){			
			var nip=document.getElementById('nip').value;
			alert(nip);			
			popWin('${path}/rekruitment/multi.htm?window=previewTsr&nip='+nip, 600, 700);
	}
	
	
	function showSPAJ(nip){	
	
		popWin('${path}/rekruitment/multi.htm?window=viewDataTSR&nip='+nip, 400, 700);
	}
	
	function download(file,product,tab){			
			window.open('${path}/rekruitment/multi.htm?window=uploadDataTSR&file='+file+'&product='+product+'&tab='+tab);
  			return false;
		}
	
	
	
</script>


<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

</style>

<body>
		<div id="tabs">		
				<ul>
					<li><a href="#tab-1">Upload</a></li>
					<li><a href="#tab-2">Download</a></li>
									
				</ul>
				<div id="tab-1">					
						<form method="post" name="formpost" enctype="multipart/form-data">
							<fieldset class="ui-widget ui-widget-content">	
								<legend class="ui-widget-header ui-corner-all"><div><h5>Upload Surat</h5></div></legend>
								<div class="rowElem">
								<label>Distribusi:</label>
								<select name="dist" id="dist" title="Silahkan pilih Distribusi">
									<c:forEach var="c" items="${dist}" varStatus="s">
										<option value="${c.key}" <c:if test="${c.key eq jp}">selected="selected"</c:if>>${c.value}</option>
									</c:forEach>
								</select>
							</div>
								<div class="rowElem"> 
															<label>File PDF(*.pdf) :</label> 
					 										<input type="file" name="file1" id="file1" size="70" />
															<input type="hidden" name="file_fp" id="file_fp"/>	
					 										<input type="submit" name="upload" id="upload" value="Upload">
					 										
															<input type="hidden" name="distribusi" id="distribusi" value="ALL">	 
															<input type="hidden" name="company" id="company2 value="ALL">	 
								</div>	 
							</fieldset>
						</form>	
								
									
									
				</div>
				
				<div id="tab-2">
							<form id="formPost" name="formPost" method="post">
						<fieldset class="ui-widget ui-widget-content">
							<div class="rowElem">
								<label>Distribusi:</label>
								<select name="dist1" id="dist1" title="Silahkan pilih Distribusi">
									<c:forEach var="c" items="${dist1}" varStatus="s">
										<option value="${c.key}" <c:if test="${c.key eq jp}">selected="selected"</c:if>>${c.value}</option>
									</c:forEach>
								</select>
							</div>
							
							<div class="rowElem">
								<label>Periode :</label>
								<input name="bulan" id="bulan" type="text" class="datepicker" title="Bulan" value="${tgl }">
							</div>		
							
							
							<div class="rowElem">
								<label></label>
								<input type="submit" name="search" id="seacrh" value="Search">
								</div>
						</fieldset>
						
						<c:if test="${not empty dokumen }">
							<fieldset class="ui-widget ui-widget-content">
							<legend class="ui-widget-header ui-corner-all"><div>List Dokumen</div></legend>
							<div style="padding: 10px 0 10px 0;">
								<table class="jtable styleTable">
									<thead class="ui-widget-header">
										<tr>
											<th>Dokumen</th>
											<th>Tanggal</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody class="ui-widget-content">
									<c:forEach items="${dokumen}" var="d">
										<tr>
											<td>${d.dok}</td>
											<td>${d.tgldok }</td>
											<td><input type="button" name ="btnDownload" id="btnDownload" title="Download" value="View" onclick="download('${d.dok}','${jp}','${showTab }');"></td>
											</tr>
										</c:forEach>
										</tbody>
									</table>
								</div>
							</fieldset>
						</c:if>
						</form>	
					
					
					</div>
				
		</div>		

			
	 </body>
</html>


