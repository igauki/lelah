<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<html>
<head>
	<jsp:include page="/include/page/header_jquery.jsp">
		<jsp:param value="${path}" name="path"/>
	</jsp:include>
</head>
  
<body>
<form method="post" name="formpost">
<fieldset>
	<legend>UPDATE AAJI KANDIDAT KARYAWAN</legend>
	<table class="entry2">
		<tr>
			<th>NO. KTP</th>
			<td>
				<c:choose>
					<c:when test="${not empty ktp}">
						<input type="text" name="ktp" id="ktp" size="50" value="${ktp}">
					</c:when>
					<c:otherwise>
						<input type="text" name="ktp" id="ktp" size="50">
					</c:otherwise>
				</c:choose>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<input type="submit" name="cari" id="cari" value="Search">
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<c:if test="${not empty calon}">
			<c:forEach items="${calon}" var="c">
				<tr>
					<th>NO. KTP</th>
					<td>
						<input type="text" name="ktp2" id="ktp2" size="50" readonly="readonly" value='${c.KTP}'>
					</td>
				</tr>
				<tr>
					<th>Nama</th>
					<td>
						<input type="text" name="nama" id="nama" size="50" readonly="readonly" value='${c.NAMA}'>
					</td>
				</tr>
				<tr>
					<th>Jenis Kelamin</th>
					<td>
						<input type="text" name="jk" id="jk" size="50" readonly="readonly" value='${c.JK}'>
					</td>
				</tr>
				<tr>
					<th>Alamat</th>
					<td>
						<textarea rows="4" cols="50" id="alamat" name="alamat" readonly="readonly" >${c.ALAMAT}</textarea>
					</td>
				</tr>
				<tr>
					<th>Tempat Tempat Lahir</th>
					<td>
						<input type="text" name="ttl" id="ttl" size="50" readonly="readonly" value='${c.TEMPAT_LAHIR}, ${c.TANGGAL_LAHIR} | ${c.TTL}'>
					</td>
				</tr>
			</c:forEach>
			<tr>
				<td colspan="2"><hr></td>
			</tr>
			<tr>
				<th>Blacklist</th>
				<td>
					<select name="blacklist" id="blacklist">
						<option value="1" selected="selected">Tidak Bermasalah</option>
						<option value="2">Bermasalah</option>
					</select>
				</td>
			</tr>
			<tr>
				<th>Perusahaan Lama</th>
				<td>
					<select name="pt" id="pt">
						<option value="1" selected="selected">Active</option>
						<option value="2">Inactive</option>
						<option value="3">Retaker</option>
						<option value="4">Tenggarai </option>
					</select>
				</td>
			</tr>
			<tr>
				<th>Status Join</th>
				<td>
					<select name="join" id="join">
						<option value="1" selected="selected">Bisa</option>
						<option value="2">Tidak Bisa</option>
					</select>
				</td>
			</tr>
			<tr id="keterangan">
				<th>Keterangan</th>
				<td>
					<textarea rows="4" cols="50" id="ket" name="ket"></textarea>
				</td>
			</tr>
			<tr>
				<td>
					&nbsp;
				</td>
				<td>
					<input type="submit" value="Submit" name="btnUpdate" id="btnUpdate">
				</td>
			</tr>
		</c:if>
	</table>
</fieldset>
</form>
<script type="text/javascript">
$(document).ready(function(){
	$("#keterangan").hide();
	$("#join").change(function(){
		var sel = $(this).val();
		if(sel=="2"){
			$("#keterangan").show();
		}
		else{
			$("#keterangan").hide();
		}
	});
	var pesan = '${pesan}';
	if(pesan!=null && pesan!=''){
		alert(pesan);
	}
});
</script>
</body>
</html>
