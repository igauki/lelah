<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<%User currentUser = (User) session.getAttribute("currentUser");%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>
<script type="text/javascript" src="${path }/include/js/bac/pemegang.js"></script>

<script>
		
	function Body_onload(){
		xmlData.async = false;
		xmlData.src = "${path}/xml/RELATION_KUESIONER.xml";
		generateXML_penerima( xmlData, 'ID','RELATION');
		xmlData.src = "${path}/xml/PENDIDIKAN_KUESIONER.xml";
		generateXML_pendidikan( xmlData, 'ID','PENDIDIKAN');
		
		document.frmParam.submit1.disabled = false;
		document.frmParam.mku_str_lv1.style.backgroundColor ='#D4D4D4';
		document.frmParam.mku_str_lv1.readOnly=true;
		document.frmParam.mku_str_lv2.style.backgroundColor ='#D4D4D4';
		document.frmParam.mku_str_lv2.readOnly=true;
		document.frmParam.mku_str_lv3.style.backgroundColor ='#D4D4D4';
		document.frmParam.mku_str_lv3.readOnly=true;
		document.frmParam.mku_str_lv4.style.backgroundColor ='#D4D4D4';
		document.frmParam.mku_str_lv4.readOnly=true;
	
		/* if ('${cmd.mku_jenis_rekrut}' == '1' ){
			document.frmParam.mku_rekruiter.readOnly=false;
			document.frmParam.mku_rekruiter.style.backgroundColor ='#FFFFFF';
			document.frmParam.msrk_id.readOnly=true;
			document.frmParam.msrk_id.style.backgroundColor ='#D4D4D4';
		}else */ if ('${cmd.mku_jenis_rekrut}' == '0'){
			document.frmParam.mku_rekruiter.readOnly=true;
			document.frmParam.mku_rekruiter.style.backgroundColor ='#D4D4D4';	
			document.frmParam.msrk_id.readOnly=true;
			document.frmParam.msrk_id.style.backgroundColor ='#D4D4D4';
		}else{
			document.frmParam.mku_rekruiter.readOnly=true;
			document.frmParam.mku_rekruiter.style.backgroundColor ='#D4D4D4';
			document.frmParam.msrk_id.readOnly=false;
			document.frmParam.msrk_id.style.backgroundColor ='#FFFFFF';
		}
	}
	
	<spring:bind path="cmd.juml_daftarTanggungan">
		var juml_x=0;
// 		alert(${status.value});
	</spring:bind>
	
	var flag_add2 = 0;
	var flag_add2_x = 0;
	var varOptionhub;
	var varOptionPend;
	
	function generateXML_penerima( objParser, strID, strDesc ) {
		varOptionhub = "";
		var collPosition = objParser.selectNodes( "//Position" );
		for(var i=0;i<collPosition.length;i++) varOptionhub += "<OPTION " + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
	}
	
	function generateXML_pendidikan( objParser, strID, strDesc ) {
		varOptionpend = "";
		var collPosition = objParser.selectNodes( "//Position" );
		for(var i=0;i<collPosition.length;i++) varOptionpend += "<OPTION " + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
	}
	
	function addRowDOM2(tableID, jml) {
		flag_add2_x = 1;
   		var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
		var tmp = parseInt(document.getElementById("tableTanggung").rows.length);
		var oRow = oTable.insertRow(oTable.rows.length);
        var oCells = oRow.cells;
		var i=tmp-1;
		var idx = juml_x + 1;

		if (document.all) {
			var cell = oRow.insertCell(oCells.length);			
				cell.innerHTML = "<td ><input type=text name='listTanggungan.mkt_nama"+idx+"' value='' size=40 maxlength=50></td>";
				cell.style.backgroundColor = "#FFF2CA";
			var cell = oRow.insertCell(oCells.length);
				cell.innerHTML = "<td ><input type=text name='listTanggungan.mkt_tempat_lahir"+idx+"' value='' size=25 maxlength=50></td>";
				cell.style.backgroundColor = "#FFF2CA";	
			var cell = oRow.insertCell(oCells.length);
				cell.innerHTML = "<td><input type=text name='tgllhr"+idx+"' value='' size=2 maxlength=2>/<input type=text name='blnhr"+idx+"' value='' size=2 maxlength=2>/<input type=text name='thnhr"+idx+"' value='' size=4 maxlength=4><input type=hidden name='listTanggungan.mkt_tgl_lahir"+idx+"' value='' size=5></td>";
				cell.style.backgroundColor = "#FFF2CA";
			var cell = oRow.insertCell(oCells.length);
				cell.innerHTML =  "<td ><select name='listTanggungan.mkt_pendidikan"+idx+"'>"+varOptionpend+"</select></td>";
				cell.style.backgroundColor = "#FFF2CA";
// 			var cell = oRow.insertCell(oCells.length);
// 				cell.innerHTML ="<td><input type=text name='listTanggungan.mkt_umur"+idx+"' value='' size=3 maxlength=3 readOnly></td>";
// 				cell.style.backgroundColor = "#FFF2CA";
			var cell = oRow.insertCell(oCells.length);
				cell.innerHTML =  "<td ><select name='listTanggungan.mkt_hubungan"+idx+"'>"+varOptionhub+"</select></td>";
				cell.style.backgroundColor = "#FFF2CA";
			var cell = oRow.insertCell(oCells.length);
				cell.innerHTML ="<td ><input type=checkbox class=\"noBorder\" name=cek2"+idx+" id=ck2"+idx+"' ></td></tr>";
				cell.style.backgroundColor = "#FFF2CA";
				juml_x = idx;
		}
	}
	
	function deleteRowDOM2 (tableID, rowCount){ 
    	var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
    	var row=parseInt(document.getElementById("tableTanggung").rows.length);
		var flag_row=0;
		var jumlah_cek=0;
		var idx = juml_x;

		if(row>0){
			flag_row=0;
			for (var i=1;i<((parseInt(row)));i++){
				if (eval("document.frmParam.elements['cek2"+i+"'].checked")){
					idx=idx-1;

					for (var k =i ; k<((parseInt(row))-1);k++){
						eval(" document.frmParam.elements['listTanggungan.nama"+k+"'].value = document.frmParam.elements['listTanggungan.nama"+(k+1)+"'].value;");
						eval(" document.frmParam.elements['listTanggungan.tempat_lhr"+k+"'].value = document.frmParam.elements['listTanggungan.tempat_lhr"+(k+1)+"'].value;");
						eval(" document.frmParam.elements['tgllhr"+k+"'].value = document.frmParam.elements['tgllhr"+(k+1)+"'].value;");	
						eval(" document.frmParam.elements['blnhr"+k+"'].value = document.frmParam.elements['blnhr"+(k+1)+"'].value;");			      
						eval(" document.frmParam.elements['thnhr"+k+"'].value = document.frmParam.elements['thnhr"+(k+1)+"'].value;");			
						eval(" document.frmParam.elements['listTanggungan.tgl_lhr"+k+"'].value = document.frmParam.elements['listTanggungan.tgl_lhr"+(k+1)+"'].value;");
						eval(" document.frmParam.elements['listTanggungan.pendidikan"+k+"'].value = document.frmParam.elements['listTanggungan.pendidikan"+(k+1)+"'].value;");
// 						eval(" document.frmParam.elements['listTanggungan.umur"+k+"'].value = document.frmParam.elements['listTanggungan.umur"+(k+1)+"'].value;");
						eval(" document.frmParam.elements['listTanggungan.relasi"+k+"'].value = document.frmParam.elements['listTanggungan.relasi"+(k+1)+"'].value;");
						eval(" document.frmParam.elements['cek2"+k+"'].checked = document.frmParam.elements['cek2"+(k+1)+"'].checked;");
					}
					oTable.deleteRow(parseInt(document.getElementById("tableTanggung").rows.length)-1);
					row=row-1;
					juml_x = idx;
					i=i-1;
				}
			}
			row=parseInt(document.getElementById("tableTanggung").rows.length);
		if(row==1) flag_add2=0;
		
		}
	}
	
	function cancel2(){		
		var idx = juml_x;

		if((idx)!=null && (idx)!=""){
			if (idx >0) flag_add2_x=1;
		}
		if(flag_add2_x==1){
			deleteRowDOM2('tableTanggung', '1');
		}
	}
	
	function DateValid(strDate) {
	
		var arrMonthDays = new Array (	"31", "29", "31",
						"30", "31", "30",
						"31", "31", "30",
						"31","30","31"	  );
		var intCount;
		var intDay;
		var intMonth;
		var	intYear;
	
		if (strDate.length != 10) return false;
	
	    intDay = strDate.substring(0,2);
		intMonth = strDate.substring(3,5);
		intYear = strDate.substring(6,11);
	
		if ((!isNumber(intDay)) || (!isNumber(intMonth)) || (!isNumber(intYear))){
			return false;
		}
		if (intYear==0) {
			return false;
	    }
	    if ((intMonth>12) || (intMonth<=0)) {
		    return false;
		}
	
		if ((parseFloat(intDay) > parseFloat(arrMonthDays[intMonth-1])) || (parseFloat(intDay)<=0))  {
		    return false;
		}
		if ((intMonth==2)&&
		   (intDay==arrMonthDays[1])&&
		   (intYear%4>0) ) {
			//alert("The end of this month is 28");
		    return false;
		}
	
		return  true;
	}
</script>

<script language="JavaScript">

	if(!document.layers&&!document.all&&!document.getElementById)
		event="test";
		
	function showtip(current,e,text){
	
		if (document.all||document.getElementById){
			thetitle=text.split('<br>');
			if (thetitle.length>1){
				thetitles='';
				for (var i=0;i<thetitle.length;i++){
					thetitles+=thetitle[i];
				}
				current.title=thetitles;
			}else{
				current.title=text;
			}
		}else if(document.layers){
			document.tooltip.document.write('<layer bgColor="white" style="border:1px solid black;font-size:13px;">'+text+'</layer>');
			document.tooltip.document.close();
			document.tooltip.left=e.pageX+5;
			document.tooltip.top=e.pageY+5;
			document.tooltip.visibility="show";
		}
	}
	
	function hidetip(){
		if (document.layers) document.tooltip.visibility="hidden";
	}
	
	function bpap(val){
		if(val==0){
			document.frmParam.mku_nama_perusahaan.readOnly=true;
			document.frmParam.mku_nama_perusahaan.style.backgroundColor ='#D4D4D4';
			
			document.frmParam.mku_jenis_perusahaan.readOnly=true;
			document.frmParam.mku_jenis_perusahaan.style.backgroundColor ='#D4D4D4';
			
			document.frmParam.mku_alamat_perusahaan.readOnly=true;
			document.frmParam.mku_alamat_perusahaan.style.backgroundColor ='#D4D4D4';
			
			document.frmParam.mku_jabatan_perusahaan.readOnly=true;
			document.frmParam.mku_jabatan_perusahaan.style.backgroundColor ='#D4D4D4';
			
			document.frmParam.mku_telepon_perusahaan.readOnly=true;
			document.frmParam.mku_telepon_perusahaan.style.backgroundColor ='#D4D4D4';
			
			document.frmParam.mku_kodepos_perusahaan.readOnly=true;
			document.frmParam.mku_kodepos_perusahaan.style.backgroundColor ='#D4D4D4';
			
		}else{
			document.frmParam.mku_nama_perusahaan.readOnly=false;
			document.frmParam.mku_nama_perusahaan.style.backgroundColor ='#FFFFFF';
			
			document.frmParam.mku_jenis_perusahaan.readOnly=false;
			document.frmParam.mku_jenis_perusahaan.style.backgroundColor ='#FFFFFF';
			
			document.frmParam.mku_alamat_perusahaan.readOnly=false;
			document.frmParam.mku_alamat_perusahaan.style.backgroundColor ='#FFFFFF';
			
			document.frmParam.mku_jabatan_perusahaan.readOnly=false;
			document.frmParam.mku_jabatan_perusahaan.style.backgroundColor ='#FFFFFF';
			
			document.frmParam.mku_telepon_perusahaan.readOnly=false;
			document.frmParam.mku_telepon_perusahaan.style.backgroundColor ='#FFFFFF';
			
			document.frmParam.mku_kodepos_perusahaan.readOnly=false;
			document.frmParam.mku_kodepos_perusahaan.style.backgroundColor ='#FFFFFF';
		}
	}
	
	function struktur_agent(msag_id,lvl){
		ajaxManager.struktur_agent(msag_id,
			{callback:function(map) {
				DWRUtil.useLoadingMessage();
				if (map=='' || map==null){
					alert('Tidak ada data');
					document.frmParam.mku_str_lv_rm.value = '';
					document.frmParam.mku_str_lv4.value = '';
					document.frmParam.mku_str_lv_sbm.value = '';
					document.frmParam.mku_str_lv3.value = '';
					document.frmParam.mku_str_lv_bm.value = '';
					document.frmParam.mku_str_lv2.value = '';
					document.frmParam.mku_str_lv_um.value = '';
					document.frmParam.mku_str_lv1.value = '';
				}else{
					document.frmParam.mku_str_lv_rm.value = '';
					document.frmParam.mku_str_lv4.value = '';
					document.frmParam.mku_str_lv_sbm.value = '';
					document.frmParam.mku_str_lv3.value = '';
					document.frmParam.mku_str_lv_bm.value = '';
					document.frmParam.mku_str_lv2.value = '';
					document.frmParam.mku_str_lv_um.value = '';
					document.frmParam.mku_str_lv1.value = '';
					
					for(var i=0 ; i<map.length ;i++){
					
						if(map[i].SBM=='1'){
							document.frmParam.mku_str_lv_sbm.value = map[i].MSAG_ID;
							document.frmParam.mku_str_lv3.value = map[i].MCL_FIRST;
						}else if(map[i].BM=='1'){
							document.frmParam.mku_str_lv_bm.value = map[i].MSAG_ID;
							document.frmParam.mku_str_lv2.value = map[i].MCL_FIRST;
						}else if(map[i].LSLE_NAME=='Regional Manager'){
							document.frmParam.mku_str_lv_rm.value = map[i].MSAG_ID;
							document.frmParam.mku_str_lv4.value = map[i].MCL_FIRST;
						}else if(map[i].LSLE_NAME=='Agency Manager'){
							document.frmParam.mku_str_lv_bm.value = map[i].MSAG_ID;
							document.frmParam.mku_str_lv2.value = map[i].MCL_FIRST;
						}else if(map[i].LSLE_NAME=='Unit Manager'){
							document.frmParam.mku_str_lv_um.value = map[i].MSAG_ID;
							document.frmParam.mku_str_lv1.value = map[i].MCL_FIRST;
						}
					
						/* if(map[i].LSLE_ID==1){
							document.frmParam.mku_str_lv_rm.value = map[i].MSAG_ID;
							document.frmParam.mku_str_lv4.value = map[i].MCL_FIRST;
						}else if(map[i].LSLE_ID==2){
							document.frmParam.mku_str_lv_sbm.value = map[i].MSAG_ID;
							document.frmParam.mku_str_lv3.value = map[i].MCL_FIRST;
						}else if(map[i].LSLE_ID==3){
							document.frmParam.mku_str_lv_bm.value = map[i].MSAG_ID;
							document.frmParam.mku_str_lv2.value = map[i].MCL_FIRST;
						}else if(map[i].LSLE_ID==4){
							document.frmParam.mku_str_lv_um.value = map[i].MSAG_ID;
							document.frmParam.mku_str_lv1.value = map[i].MCL_FIRST;
						} */
					}
				}
		   	},
		  	timeout:60000,
		  	errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
			
	function kotak_x1(){
		if (document.frmParam.mku_str_lv_um.value != ""){
			agen_rekruter_UM(document.frmParam.mku_str_lv_um.value);
		}
	}
	function agen_rekruter_UM(kodeagen){
		ajaxManager.agen_rekruter_UM(kodeagen,
		{callback:function(map) {
			DWRUtil.useLoadingMessage();
				document.frmParam.mku_str_lv1.value = map.nama;
				if (map.nama == ""){
					alert("Agen SM / UM tersebut belum terdaftar");}
		   },
		  timeout:60000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
	
	function kotak_x2(){
		if (document.frmParam.mku_str_lv_bm.value != ""){
			agen_rekruter_BM(document.frmParam.mku_str_lv_bm.value, 3);
		}
	}
	function agen_rekruter_BM(kodeagen){
		ajaxManager.agen_rekruter_BM(kodeagen,
		{callback:function(map) {
			DWRUtil.useLoadingMessage();
				document.frmParam.mku_str_lv2.value = map.nama;
				if (map.nama == ""){
					alert("Agen AM / BM tersebut belum terdaftar");}
		   },
		  timeout:60000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
	
	function kotak_x3(){
		if (document.frmParam.mku_str_lv_sbm.value != ""){
			agen_rekruter_SBM(document.frmParam.mku_str_lv_sbm.value, 3);
		}
	}
	function agen_rekruter_SBM(kodeagen){
		ajaxManager.agen_rekruter_SBM(kodeagen,
		{callback:function(map) {
			DWRUtil.useLoadingMessage();
				document.frmParam.mku_str_lv3.value = map.nama;
				if (map.nama == ""){
					alert("Agen SBM tersebut belum terdaftar");}
		   },
		  timeout:60000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
	
	function kotak_x4(){
		if (document.frmParam.mku_str_lv_rm.value != ""){
			agen_rekruter_RM(document.frmParam.mku_str_lv_rm.value, 3);
		}
	}
	function agen_rekruter_RM(kodeagen){
		ajaxManager.agen_rekruter_RM(kodeagen,
		{callback:function(map) {
			DWRUtil.useLoadingMessage();
				document.frmParam.mku_str_lv4.value = map.nama;
				if (map.nama == ""){
					alert("Agen AD / RM tersebut belum terdaftar");}
		   },
		  timeout:60000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
	
	function kotak(flag){
		if (document.frmParam.msrk_id.value != "" && document.frmParam.mku_jenis_rekrut.value != 0){
			if(flag == 0){
				agen_rekruter_detil(document.frmParam.msrk_id.value, document.frmParam.mku_jenis_rekrut.value);
			}
		}
		
		if (document.frmParam.mst_leader.value != ""){
			if(flag == 1){
				agen_atasan_detil(document.frmParam.mst_leader.value, 3);
			}
		}

	}
	function agen_rekruter_detil(kodeagen,kodepil){
		ajaxManager.agen_rekruter_detil(kodeagen,kodepil,
		{callback:function(map) {
			DWRUtil.useLoadingMessage();
				document.frmParam.mku_rekruiter.value = map.nama;
				document.frmParam.mku_acc_rekruiter.value = map.acc;
				document.frmParam.mku_bank_id.value = map.namabank;
				document.frmParam.mku_posisi_rekruter.value = map.jabatan;
// 				document.frmParam.mku_lbn_nama.value = map.namabank2;
				if (map.nama == "")
				{
					alert('Rekruter tersebut belum terdaftar');
				}
		   },
		  timeout:60000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
	function agen_atasan_detil(kodeagen,kodepil){
		ajaxManager.agen_rekruter_detil(kodeagen,kodepil,
		{callback:function(map) {
			DWRUtil.useLoadingMessage();
				document.frmParam.mku_nama_atasan.value = map.nama;
				document.frmParam.mku_posisi_atasan.value = map.jabatan;
				if (map.nama == "")
				{
					alert("Kode Agen tersebut belum terdaftar");
				}
		   },
		  timeout:60000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
	
	function ganti_cabang(){
// 	 	if (document.frmParam.mku_jenis_cabang.value != "0"){
		fungsi_ganti_cabang(document.frmParam.mku_jenis_cabang.value, 'fungsi_ganti_cabang', 'xxx', 'mku_regional');
// 	 	}
	}
	function fungsi_ganti_cabang(cabang, selectType, selectLocation, selectName){
		eval('ajaxManager.'+selectType)(cabang, {
		  callback:function(list) {
			DWRUtil.useLoadingMessage();
	
			var text = '<select name="'+selectName+'" onchange=ganti_region()';
			text+='>';
			text += '<option value="0~X0"';
			text += '>NONE</option>';	
			for(i = 0; i < list.length; i++) {
				text += '<option value="'+list[i].KEY+'"';
				text += ' selected ';
				text += '>'+list[i].VALUE+'</option>';
			}
			text += '</select>';
		
			if(document.getElementById(selectLocation)){
				document.getElementById(selectLocation).innerHTML=text;
			}
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
	
	function ganti_posisi(){
		fungsi_ganti_posisi(document.frmParam.mku_jenis_cabang.value, 'fungsi_ganti_posisi', 'posisiagen', 'mku_posisi_agen');
	}
	function fungsi_ganti_posisi(cabang, selectType, selectLocation, selectName){
		eval('ajaxManager.'+selectType)(cabang, {
		  callback:function(list) {
			DWRUtil.useLoadingMessage();
	
			var text = '<select name="'+selectName+'"';
			text+='>';
			text += '<option value="0"';
			text += '>NONE</option>';	
			for(i = 0; i < list.length; i++) {
				text += '<option value="'+list[i].KEY+'"';
				text += ' selected ';
				text += '>'+list[i].VALUE+'</option>';
			}
			text += '</select>';
		
			if(document.getElementById(selectLocation)){
				document.getElementById(selectLocation).innerHTML=text;
			}
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
	

	function populateUmur(){
		var tgl_lahir = document.frmParam.mku_date_birth.value;
		ajaxManager.hitung_umur(tgl_lahir, {
		  callback:function(hasil) {
			DWRUtil.useLoadingMessage();
			if(hasil) {
			
				if(hasil.umur<23){
					document.frmParam.mku_usia.value = 1;
				}else if(hasil.umur >=23 && hasil.umur <= 24){
					document.frmParam.mku_usia.value = 2;
				}else if(hasil.umur>=25 && hasil.umur<=29){
					document.frmParam.mku_usia.value = 3;
				}else if(hasil.umur>=30 && hasil.umur<=39){
					document.frmParam.mku_usia.value = 4;
				}else{
					document.frmParam.mku_usia.value = 5;
				}
			}
		  },
		  timeout:180000,
		  errorHandler:function(message) {
		  	alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); 
		  }
		});
	}
	
	function ganti_jenis(){
		document.frmParam.msrk_id.value ="";
		/* if (document.frmParam.mku_jenis_rekrut.value == "1"){
			document.frmParam.mku_rekruiter.value ='';
			document.frmParam.mku_rekruiter.readOnly=false;
			document.frmParam.mku_rekruiter.style.backgroundColor ='#FFFFFF';
			document.frmParam.msrk_id.readOnly=true;
			document.frmParam.msrk_id.style.backgroundColor ='#D4D4D4';
			document.frmParam.msrk_id.value ='000000';
			document.frmParam.mku_acc_rekruiter.value ='';
			document.frmParam.mku_acc_rekruiter.readOnly=false;
			document.frmParam.mku_acc_rekruiter.style.backgroundColor ='#FFFFFF';
			document.frmParam.mku_bank_id.value ='';
			document.frmParam.mku_bank_id.readOnly=false;
			document.frmParam.mku_bank_id.disabled=false;
			document.frmParam.mku_bank_id.style.backgroundColor ='#FFFFFF';
		}else  */if (document.frmParam.mku_jenis_rekrut.value == "0"){
			document.frmParam.mku_rekruiter.value ='';
			document.frmParam.mku_rekruiter.readOnly=true;
			document.frmParam.mku_rekruiter.style.backgroundColor ='#D4D4D4';	
			document.frmParam.msrk_id.readOnly=true;
			document.frmParam.msrk_id.style.backgroundColor ='#D4D4D4';
		}else{
			document.frmParam.mku_rekruiter.value ='';
			document.frmParam.mku_rekruiter.readOnly=true;
			document.frmParam.mku_rekruiter.fontColor = '#D4D4D4';
			document.frmParam.mku_rekruiter.style.backgroundColor ='#D4D4D4';
			document.frmParam.mku_acc_rekruiter.readOnly=true;
			document.frmParam.mku_acc_rekruiter.style.backgroundColor ='#D4D4D4';
			document.frmParam.mku_bank_id.readOnly=true;
			document.frmParam.mku_bank_id.style.backgroundColor ='#D4D4D4';
			document.frmParam.msrk_id.readOnly=false;
			document.frmParam.msrk_id.style.backgroundColor ='#FFFFFF';		
		}
	 	switch (document.frmParam.mku_jenis_rekrut.value) {
	 	case '1':
	    	alert('Silahkan masukkan kode rekruter pada kolom isian kode rekruter');
	    	break;
	    case '2':
	    	alert('Silahkan masukkan kode rekruter pada kolom isian kode rekruter');
	    	break;
	    case '3':
	       	alert('Silahkan masukkan kode agent pada kolom isian kode rekruter');
	    	break;
	    case '4':
	       	alert('Silahkan masukkan no polis pada kolom isian kode rekruter');
	    	break;
	    case '5':
	       	alert('Silahkan masukkan nik karyawan pada kolom isian kode rekruter');
	    	break;
		}
	}
	
	function ganti_region(){
		document.frmParam.mku_region.value = document.frmParam.mku_regional.value ;
	}
	
	function next(){
		document.frmParam.mku_region.value = document.frmParam.mku_regional.value;
		document.frmParam.juml_x.value=juml_x;
		
		document.frmParam.submit();
		document.frmParam.submit1.disabled = true;
	}		
	
	function link_kontrak(){
		alert('kontrak');
	}
	
	function link_doc(){
		alert('doc');
	}
	
	function load_cabang_bank(){
// 		var lbn_id = document.getElementById("mku_bank_id2").value;
		var text = document.getElementById('mku_bank_id2').options[document.getElementById('mku_bank_id2').selectedIndex].text;
		document.getElementById('mku_cabang_bank').value = text;
	}
</script>

<body bgcolor="ffffff" onLoad="Body_onload();">
<XML ID=xmlData></XML>

<table border="0" width="100%" height="677" cellspacing="0" cellpadding="0" class="entry2">
	<tr > 
		<td width="67%" rowspan="5" align="left" valign="top"> 
			<form name="frmParam" method="post" enctype="multipart/form-data" >
				<table border="0" width="100%"  cellspacing="0" cellpadding="0" class="entry">
          			<tr >
            			<td colspan="4" height="20">&nbsp;
            				<spring:bind path="cmd.status">
								<input type="hidden" name="${status.expression}" value="${status.value }" size="30" maxlength="20" >
							</spring:bind>
							<spring:bind path="cmd.keterangan">
								<input type="hidden" name="${status.expression}" value="${status.value }" size="30" maxlength="20" >
							</spring:bind>
							<spring:bind path="cmd.jenisHalaman">
								<input type="hidden" name="${status.expression}" value="1" size="30" maxlength="20" >
							</spring:bind>
						</td>
          			</tr>
          			<c:if test="${not empty cmd.currentUser.msag_id_ao }">
           			<tr >
           	 			<th >
							Status Pendaftaran
					  	</th>
			  			<th colspan="3">
			  				<c:choose>
			  					<c:when test="${not empty cmd.mku_no_reg and empty cmd.mku_tgl_transfer_admin}">
									<c:choose>
						  				<c:when test="${empty cmd.mku_flag_ktp}">
						  					<a href="${path}/rekruitment/upload_rekruitment_new.htm?window=main&kode_rekrut=${cmd.mku_no_reg}">
						  					STEP 2 : Silahkan Upload KTP Agen
						  					</a>
						  				</c:when>
						  				<c:when test="${empty cmd.mku_flag_foto}">
						  					<a href="${path}/rekruitment/upload_rekruitment_new.htm?window=main&kode_rekrut=${cmd.mku_no_reg}">
						  					STEP 2 : Silahkan Upload FOTO Agen
						  					</a>
						  				</c:when>
						  				<c:when test="${empty cmd.mku_flag_buku_rek}">
						  					<a href="${path}/rekruitment/upload_rekruitment_new.htm?window=main&kode_rekrut=${cmd.mku_no_reg}">
						  					STEP 2 : Silahkan Upload Buku Rekening Agen
						  					</a>
						  				</c:when>
						  				<c:when test="${empty cmd.mku_flag_bsb_ujian}">
						  					<a href="${path}/rekruitment/upload_rekruitment_new.htm?window=main&kode_rekrut=${cmd.mku_no_reg}">
						  					STEP 2 : Silahkan Upload BSB Ujian Agen
						  					</a>
						  				</c:when>
						  				<c:otherwise>
						  					<a href="${path}/rekruitment/rekrut.htm?window=main&kode_rekrut=${cmd.mku_no_reg}">
						  					STEP 3 : Silahkan Kirim Data Ke Admin
						  					</a>
						  				</c:otherwise>
				  					</c:choose>
			  					</c:when>
						  		<c:when test="${empty cmd.mku_no_reg}">
						  			<a href="${path}/rekruitment/rekrut_keagenan.htm?window=main">
						  			STEP 1 : Silahkan Input data Agen Baru
						  			</a>
						  		</c:when>
			  				</c:choose>
						</th>
          			</tr>
          			</c:if>
		         	<tr> 
		            	<th class="subtitle" colspan="4" height="20" > 
		              		<p align="center"><b> <font face="Verdana" size="1" color="#FFFFFF">FORM REKRUITMENT KEAGENAN</font></b> 
		            	</th>
		          	</tr>
        		</table>
        		<div style="margin: 10px 0 0 50px;">
<!-- 					<spring:bind path="cmd.tools"> -->
						<input type="button" name="tools" value="Tools &raquo;" tabindex="47" onclick="window.location.href = '${path}/rekruitment/multi.htm?window=rekrut_tools';">
<!-- 					</spring:bind> -->
				</div>
				<table border="0" width="100%" cellspacing="1" cellpadding="1" class="entry">
					<%-- <tr>
						<th>
							&nbsp;
						</th>
						<th>
							<b><font color="#996600" size="1" face="Verdana">Cari No Register</font></b>
						</th>
						<th colspan="5">
							<spring:bind path="cmd.mku_noreg">
								<input type="text" name="${status.expression}" value="${status.value }" size="30" maxlength="10" tabindex="1">
							</spring:bind>
							<spring:bind path="cmd.submit2">
								<input type="submit" name= "submit2" value="Search &raquo;" tabindex="47" onclick="next();">
							</spring:bind>
						</th>
					</tr> --%>
					<tr>
						<th>
							&nbsp;
						</th>
						<th>
							<b><font color="#996600" size="1" face="Verdana">No Register Rekruitment</font></b>
						</th>
						<th colspan="5">
							<spring:bind path="cmd.mku_no_reg">
								<input type="text" name="${status.expression}" value="${status.value }" size="30" maxlength="10" tabindex="1" readOnly style='background-color:#D4D4D4'>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th></th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Tanggal Pengisian</font></b>
						</th>
						<th>
							<spring:bind path="cmd.mku_tglkues">
								<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
							</spring:bind>
							<font color="#CC3300">*</font>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Tempat Pengisian</font></b>
						</th>
						<th colspan="3">
							<spring:bind path="cmd.mku_tempatkues">
								<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="30" tabindex="8"
										<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD' </c:if>>
								<font color="#CC3300">*</font>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Jenis Distribusi</font></b>
						</th>
						<th colspan="5">
							<select name="mku_jenis_cabang" tabindex="2" onchange="ganti_cabang();ganti_posisi();"
								<c:if test="${ not empty status.errorMessage}">style='background-color:#FFE1FD'</c:if>>
								<option value="0">NONE</option>
								<c:forEach var="cabang" items="${select_jalurdist}">
									<option <c:if test="${cmd.mku_jenis_cabang eq cabang.key}"> SELECTED </c:if>
										value="${cabang.key}">${cabang.value}
									</option>
								</c:forEach>
							</select>
							<font color="#CC3300">*</font>
						</th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Posisi Pengajuan</font></b>
						</th>
						<th>
							<div id="posisiagen"> </div>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Cabang</font></b>
						</th>
						<th colspan="3"><div id="xxx"> </div></th>
						<spring:bind path="cmd.mku_region">
							<input type="hidden" name="${status.expression}" value="1" size="10" maxlength="10" >
						</spring:bind>
					</tr>
					<tr>
						<th colspan="7"><hr>&nbsp;</th>
					</tr>
					
					<!-- DATA CALON AGENT -->
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Nama Sesuai KTP</font></b>
						</th>
						<th>
							<spring:bind path="cmd.mku_first">
								<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="50" tabindex="8"
										<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD' </c:if>>
								<font color="#CC3300">*</font>
							</spring:bind>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">No. KTP</font></b>
						</th>
						<th colspan="3">
							<spring:bind path="cmd.mku_no_identity">
								<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="20" tabindex="17"
										<c:if test="${ not empty status.errorMessage}"> style='background-color :#FFE1FD' </c:if>>
								<font color="#CC3300">*</font>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Tempat & Tanggal Lahir</font></b>
						</th>
						<th>
							<spring:bind path="cmd.mku_place_birth">
								<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="30" tabindex="12"
										<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD' </c:if>>
								<font color="#CC3300"></font>
							</spring:bind>
							<spring:bind path="cmd.mku_date_birth">
								<script>inputDate('${status.expression}', '${status.value}', false, 'onchange=populateUmur();');</script>
							</spring:bind>
							<spring:bind path="cmd.mku_usia">
								<input type="hidden" name="${status.expression}" value="1" size="10" maxlength="10" >
							</spring:bind>
							<font color="#CC3300">*</font>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">No. NPWP</font></b>
						</th>
						<th colspan="3">
							<spring:bind path="cmd.mku_no_npwp">
								<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="15" tabindex="17"
										<c:if test="${ not empty status.errorMessage}"> style='background-color :#FFE1FD' </c:if>>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Jenis Alamat</font></b>
						</th>
						<th>
							<select name="mku_jns_alamat" tabindex="9"
								<c:if test="${ not empty status.errorMessage}"> style='background-color :#FFE1FD' </c:if>>
								<option <c:if test="${cmd.mku_jns_alamat eq 1}"> SELECTED </c:if> value="1">
									Alamat Rumah
								</option>
								<option <c:if test="${cmd.mku_jns_alamat eq 2}"> SELECTED </c:if> value="2">
									Alamat Kantor
								</option>
							</select><font color="#CC3300">*</font>
						</th>
						<th width="165">
							<b><font face="Verdana" size="1" color="#996600">Agama</font></b>
						</th>
						<th colspan="3">
							<spring:bind path="cmd.mku_agama">
								<select name="${status.expression}">
									<c:forEach var="d" items="${select_agama}">
										<option <c:if test="${status.value eq d.key}"> SELECTED </c:if>
												value="${d.key}">${d.value}</option>
									</c:forEach>
								</select>
								<font color="#CC3300">*</font>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
						</th>
						<th rowspan="2">
							<b><font face="Verdana" size="1" color="#996600">Alamat</font></b>
						</th>
						<th colspan="5" rowspan="2">
							<spring:bind path="cmd.mku_alamat">
								<textarea cols="50" rows="4" name="${status.expression }"
									tabindex="15" onkeyup="textCounter(this, 200); "
									onkeydown="textCounter(this, 200); " maxlength="30" size ="30"
									<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD' </c:if>>${status.value }</textarea>
								<font color="#CC3300">*</font>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th></th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Kota</font></b>
						</th>
						<th>
							<spring:bind path="cmd.mku_kota">
								<input type="text" name="${status.expression }"
										id="${status.expression }" value="${status.value }"
										onfocus="this.select();" tabindex="16" size="30" maxlength="30"
										<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
								<span id="indicator_rumah" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
							</spring:bind>
							<font color="#CC3300">*</font>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">No. Telp</font></b>
						</th>
						<th colspan="3">
							<spring:bind path="cmd.mku_area_rumah">
								<input type="text" name="${status.expression}"
										value="${status.value }" size="5" maxlength="4" tabindex="18"
										<c:if test="${ not empty status.errorMessage}">  style='background-color :#FFE1FD' </c:if>>
							</spring:bind>
							<spring:bind path="cmd.mku_tlprumah">
								<input type="text" name="${status.expression}"
										value="${status.value }" size="21" maxlength="10" tabindex="19"
										<c:if test="${ not empty status.errorMessage}"> style='background-color :#FFE1FD' </c:if>>
								<font color="#CC3300">*</font>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Propinsi</font></b>
						</th>
						<th>
							<spring:bind path="cmd.mku_provinsi">
								<select name="${status.expression}">
									<c:forEach var="d" items="${select_propinsi}">
										<option <c:if test="${status.value eq d.key}"> SELECTED </c:if>
											value="${d.key}">${d.value}
										</option>
									</c:forEach>
								</select>
							<font color="#CC3300">*</font>
							</spring:bind>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">No. HP</font></b>
						</th>
						<th colspan="3">
						<!-- Iga 23092020|| Update pengisian No. Telp, No. HP & email di menu Rekrutmen Agent (Wasisti)
						<spring:bind path="cmd.mku_area_hp">
								<input type="text" name="${status.expression}"
										value="${status.value }" size="5" maxlength="4" tabindex="22"
										<c:if test="${ not empty status.errorMessage}"> style='background-color :#FFE1FD' </c:if>>
							</spring:bind> 
							-->
							
							<spring:bind path="cmd.mku_hp">
								<input type="text" name="${status.expression}"
										value="${status.value }" size="21" maxlength="13" tabindex="23"
										<c:if test="${ not empty status.errorMessage}"> style='background-color :#FFE1FD' </c:if>>
								<font color="#CC3300">*</font>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Kode Pos</font></b>
						</th>
						<th>
							<spring:bind path="cmd.mku_kd_pos">
								<input type="text" name="${status.expression}"
										value="${status.value }" size="10" maxlength="6" tabindex="17"
										<c:if test="${ not empty status.errorMessage}"> style='background-color :#FFE1FD' </c:if>>
								<font color="#CC3300">*</font>
							</spring:bind>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Email</font></b>
						</th>
						<th colspan="3">
							<spring:bind path="cmd.mku_email">
								<input type="text" name="${status.expression}"
										value="${status.value }" size="30" maxlength="50" tabindex="8"
										<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD' </c:if>>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Pendidikan Terakhir</font></b>
						</th>
						<th colspan="5">
							<spring:bind path="cmd.mku_pendidikan">
								<select name="mku_pendidikan" tabindex="26"
									<c:if test="${ not empty status.errorMessage}"> 
             						 style='background-color :#FFE1FD' </c:if>>
									<option
										<c:if test="${cmd.mku_pendidikan eq 0}">   SELECTED </c:if> value="0">
										Lainnya
									</option>
									<option
										<c:if test="${cmd.mku_pendidikan eq 1}">   SELECTED </c:if> value="1">
										SD
									</option>
									<option
										<c:if test="${cmd.mku_pendidikan eq 2}">   SELECTED </c:if> value="2">
										SLTP
									</option>
									<option
										<c:if test="${cmd.mku_pendidikan eq 3}">   SELECTED </c:if> value="3">
										SLTA
									</option>
									<option
										<c:if test="${cmd.mku_pendidikan eq 4}">   SELECTED </c:if> value="4">
										D1
									</option>
									<option
										<c:if test="${cmd.mku_pendidikan eq 5}">   SELECTED </c:if> value="5">
										D2
									</option>
									<option
										<c:if test="${cmd.mku_pendidikan eq 6}">   SELECTED </c:if> value="6">
										D3
									</option>
									<option
										<c:if test="${cmd.mku_pendidikan eq 7}">   SELECTED </c:if> value="7">
										D4
									</option>
									<option
										<c:if test="${cmd.mku_pendidikan eq 8}">   SELECTED </c:if> value="8">
										S1
									</option>
									<option
										<c:if test="${cmd.mku_pendidikan eq 9}">   SELECTED </c:if> value="9">
										S2
									</option>
									<option
										<c:if test="${cmd.mku_pendidikan eq 10}">   SELECTED </c:if> value="10">
										S3
									</option>
								</select>
								<font color="#CC3300">*</font>
							</spring:bind>
							&nbsp;&nbsp;
							<font color="#996600">Jika lainnya, sebutkan :</font>
							<spring:bind path="cmd.mku_pendidikan_lain">
								<input type="text" name="${status.expression}"
										value="${status.value }" size="15" maxlength="20" tabindex="8"
										<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD' </c:if>>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
						</th>
						<th>
							<font face="Verdana" size="1" color="#996600">Jenis Kelamin</font>
						</th>
						<th>
							<select name="mku_sex" tabindex="9"
								<c:if test="${ not empty status.errorMessage}"> style='background-color :#FFE1FD' </c:if>>
								<option <c:if test="${cmd.mku_sex eq 1}"> SELECTED </c:if> value="1">
									Pria / Male
								</option>
								<option <c:if test="${cmd.mku_sex eq 0}"> SELECTED </c:if> value="0">
									Wanita / Female
								</option>
							</select>
							<font color="#CC3300">*</font>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Status Perkawinan</font></b>
						</th>
						<th colspan="3">
							<select name="mku_status" tabindex="24"
								<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD' </c:if>>
								<option <c:if test="${cmd.mku_status eq 1}">   SELECTED </c:if> value="1">
									Single (Tidak Kawin)
								</option>
								<option <c:if test="${cmd.mku_status eq 2}">   SELECTED </c:if> value="2">
									Married (Kawin)
								</option>
							</select>
							<font color="#CC3300">*</font>
						</th>
					</tr>
					<tr>
						<th colspan="7">&nbsp;</th>
					</tr>
					<tr>
						<th>&nbsp;</th>
						<th colspan="6"><u><b><font face="Verdana" size="1" color="#996600">Bila Anda Menikah, Isilah Data Dibawah Ini :</font></b></u></th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
                        </th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Nama Suami/Istri</font></b>
						</th>
						<th>
							<spring:bind path="cmd.mku_nama_pasangan">
								<input type="text" name="${status.expression}"
                                        value="${status.value }" size="30" maxlength="20" tabindex="5"
                                        <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
							</spring:bind>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Jumlah Tanggungan</font></b>
						</th>
						<th colspan="3">
							<spring:bind path="cmd.mku_tanggungan">
								<input type="text" name="${status.expression}"
                                        value="${status.value }" size="10" maxlength="3" tabindex="5"
                                        <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
								<b><font face="Verdana" size="1" color="#996600">Orang</font></b>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Pekerjaan Suami/Istri</font></b>
						</th>
						<th colspan="5">
							<spring:bind path="cmd.mku_pekerjaan_pasangan">
								<input type="text" name="${status.expression }"
										value="${status.value }" size="30" maxlength="40" tabindex="16" 
										<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th colspan="7">&nbsp;</th>
					</tr>
					<tr>
						<th>&nbsp;</th>
						<th colspan="6"><u><b><font face="Verdana" size="1" color="#996600">Informasi Rekening</font></b></u></th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
                        </th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Nama Bank</font></b>
						</th>
						<th>
							<spring:bind path="cmd.mku_bank_id2">
								<select name="${status.expression}" id="mku_bank_id2" onchange="load_cabang_bank()">
									<c:forEach var="xxx" items="${select_bank}">
										<option <c:if test="${status.value eq xxx.value}"> SELECTED </c:if> value="${xxx.value}">
											${xxx.key}
										</option>
 									</c:forEach>
								</select>
 								<font color="#CC3300">*</font>
 							</spring:bind><br/>
 							<spring:bind path="cmd.mku_cabang_bank">
 								<input type="hidden" name="${status.expression}" value="${status.value}" id="mku_cabang_bank">
 							</spring:bind>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">No Rekening</font></b>
						</th>
						<th colspan="3">
							<spring:bind path="cmd.mku_acc_cust">
								<input type="text" name="${status.expression}"
                                        value="${status.value }" size="30" maxlength="20" tabindex="5"
                                        <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
								<font color="#CC3300">*</font>
							</spring:bind>
						</th>
					</tr>
					<%-- <tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Cabang Bank</font></b>
						</th>
						<th colspan="5">
							<spring:bind path="cmd.mku_cabang_bank">
								</select>
									<input type="text" name="${status.expression }"
										value="${status.value }" size="30" maxlength="40" tabindex="16" 
										<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
							</spring:bind>
							<font color="#CC3300">*</font>
						</th>
					</tr> --%>
					<tr>
						<th colspan="7">&nbsp;</th>
					</tr>
					<tr>
						<th>&nbsp;</th>
						<th colspan="6"><u><b><font face="Verdana" size="1" color="#996600">Atasan Langsung Calon</font></b></u></th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
                        </th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Nama Atasan</font></b>
						</th>
						<th>
							<spring:bind path="cmd.mku_nama_atasan">
								<input type="text" name="${status.expression}" value="${status.value }"
										size="30" maxlength="20" tabindex="5" readonly="readonly" style='background-color:#D4D4D4'
                                        <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
								<font color="#CC3300">*</font>
							</spring:bind>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Posisi</font></b>
						</th>
						<th colspan="3">
							<spring:bind path="cmd.mku_posisi_atasan">
								<input type="text" name="${status.expression}" value="${status.value }"
										size="15" maxlength="10" tabindex="5" readonly="readonly" style='background-color:#D4D4D4'
                                        <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
								<font color="#CC3300">*</font>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
                        </th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Kode Agen</font></b>
						</th>
						<th colspan="5">
							<spring:bind path="cmd.mst_leader">
								<input type="text" name="${status.expression}"
                                        value="${status.value }" size="15" maxlength="10" tabindex="5" onchange="kotak(1);"
                                        <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
								<font color="#CC3300">*</font>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th colspan="7">&nbsp;</th>
					</tr>
					<tr>
						<th>&nbsp;</th>
						<th colspan="6"><u><b><font face="Verdana" size="1" color="#996600">Data Perekrut Calon</font></b></u></th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
						</th>
						<th>
							<b><font color="#996600" size="1" face="Verdana">Jenis Rekrut</font></b>
						</th>
						<th colspan="5">
							<select name="mku_jenis_rekrut" tabindex="2" onchange="ganti_jenis();"
									<c:if test="${ not empty status.errorMessage}">style='background-color:#FFE1FD'</c:if>>
								<c:forEach var="rekrut" items="${jenis_rekrut}">
									<option <c:if test="${cmd.mku_jenis_rekrut eq rekrut.ID}"> SELECTED </c:if>
										value="${rekrut.ID}">${rekrut.JENIS}
									</option>
								</c:forEach>
							</select>
						</th>
					</tr>
	 				<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
                        </th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Nama Rekruter</font></b>
						</th>
						<th>
							<spring:bind path="cmd.mku_rekruiter">
								<input type="text" name="${status.expression}" value="${status.value }"
										size="30" maxlength="20" tabindex="5" readonly="readonly" style='background-color:#D4D4D4'
                                        <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
							</spring:bind>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Posisi</font></b>
						</th>
						<th colspan="3">
							<spring:bind path="cmd.mku_posisi_rekruter">
								<input type="text" name="${status.expression}" value="${status.value }"
										size="15" maxlength="10" tabindex="5" readonly="readonly" style='background-color:#D4D4D4'
                                        <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
                        </th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Kode Agen</font></b>
						</th>
						<th colspan="5">
							<spring:bind path="cmd.msrk_id">
								<input type="text" name="${status.expression}"
                                        value="${status.value }" size="15" maxlength="10" tabindex="5" onchange="kotak(0);"
                                        <c:if test="${ not empty cmd.currentUser.msag_id_ao}">disabled="disabled"</c:if>
                                        <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
							</spring:bind>
						</th>
						<spring:bind path="cmd.mku_acc_rekruiter">
							<input type="hidden" name="${status.expression}" value="1" size="10" maxlength="20" >
						</spring:bind>
						<spring:bind path="cmd.mku_bank_id">
							<input type="hidden" name="${status.expression}" value="1" size="10" maxlength="50" >
						</spring:bind>
					</tr>
					<tr>
						<th colspan="7">&nbsp;</th>
					</tr>
					<tr>
						<th class="subtitle" colspan="7" height="20">
							<p align="center">
							<b><font face="Verdana" size="1" color="#FFFFFF">KUESIONER</font></b>
						</th>
					</tr>
					<tr>
						<th>
							<b> <font face="Verdana" size="1" color="#996600">1. </font> </b>
						</th>
						<th colspan="2">
							<b> <font face="Verdana" size="1" color="#996600">Apakah anda pernah bergabung menjadi tenaga pemasaran PT. Asuransi Jiwa Sinarmas MSIG ?</font>
							</b>
						</th>
						<th colspan="4">
							<select name="mku_gabung_pernah" tabindex="38"
								<c:if test="${ not empty status.errorMessage}"> style='background-color :#FFE1FD' </c:if>>
								<option <c:if test="${cmd.mku_gabung_pernah eq 1}">   SELECTED </c:if> value="1">
									Ya
								</option>
								<option <c:if test="${cmd.mku_gabung_pernah eq 2}">   SELECTED </c:if> value="2">
									Tidak
								</option>
							</select>
							<font color="#CC3300">*</font>
						</th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600">2. </font></b>
						</th>
						<th colspan="2">
							<b><font face="Verdana" size="1" color="#996600">Apakah dalam 6 bulan terakhir anda pernah bergabung dengan perusahaan asuransi jiwa lain ?</font></b>
						</th>
						<th colspan="4">
							<select name="mku_6bln" tabindex="38"
								<c:if test="${ not empty status.errorMessage}"> style='background-color :#FFE1FD' </c:if>>
								<option <c:if test="${cmd.mku_6bln eq 1}">   SELECTED </c:if> value="1">
									Ya
								</option>
								<option <c:if test="${cmd.mku_6bln eq 0}">   SELECTED </c:if> value="0">
									Tidak
								</option>
							</select>
							<font color="#CC3300">*</font>
						</th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600">3. </font></b>
						</th>
						<th colspan="6">
							<b><font face="Verdana" size="1" color="#996600">Pengalaman kerja di asuransi jiwa :</font></b>
						</th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Nama Perusahaan</font></b>
						</th>
						<th>
							<spring:bind path="cmd.mku_nama_experusahaan">
								<input type="text" name="${status.expression}"
									value="${status.value }" size="30" maxlength="50" tabindex="8"
									<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD' </c:if>>
							</spring:bind>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">No. Lisensi</font></b>
						</th>
						<th colspan="3">
							<spring:bind path="cmd.mku_lisensi">
								<input type="text" name="${status.expression}"
									value="${status.value }" size="30" maxlength="20" tabindex="8"
									<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD' </c:if>>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Lama Bekerja</font></b>
						</th>
						<th>
							<spring:bind path="cmd.mku_tglmsk_experusahaan">
								<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
							</spring:bind>
							<b><font face="Verdana" size="1" color="#996600">s/d</font></b>
							<spring:bind path="cmd.mku_tglklr_experusahaan">
								<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
							</spring:bind>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Tgl. Kadaluarsa</font></b>
						</th>
						<th colspan="3">
							<spring:bind path="cmd.mku_tglexp_lisensi">
								<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600">4. </font></b>
						</th>
						<th colspan="2">
							<b><font face="Verdana" size="1" color="#996600">Apakah anda pernah dinyatakan bangkrut/pailit oleh pengadilan ?</font></b>
						</th>
						<th colspan="4">
							<spring:bind path="cmd.mku_bangkrut">
								<label for="bangkrut1"> 
									<input type="radio" class=noBorder name="${status.expression}" value="0"
									<c:if test="${cmd.mku_bangkrut eq 0 or cmd.mku_bangkrut eq null}">checked</c:if> id="bangkrut1">Tidak
								</label>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<label for="bangkrut2">
									<input type="radio" class=noBorder name="${status.expression}" value="1"
									<c:if test="${cmd.mku_bangkrut eq 1}">checked</c:if> id="bangkrut2">Ya
								</label>
								<font face="Verdana" size="1" color="#996600">, Jelaskan : </font>								
							</spring:bind>
							<spring:bind path="cmd.mku_ket_bangkrut">
								<input type="text" name="${status.expression}" value="${status.value }" size="30" maxlength="50" tabindex="8"
								<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD' </c:if>>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600">5. </font></b>
						</th>
						<th colspan="2">
							<b><font face="Verdana" size="1" color="#996600">Apakah anda pernah terlibat perkara kriminal ?</font></b>
						</th>
						<th colspan="4">
							<spring:bind path="cmd.mku_kriminal">
								<label for="kriminal1"> 
									<input type="radio" class=noBorder name="${status.expression}" value="0"
									<c:if test="${cmd.mku_kriminal eq 0 or cmd.mku_kriminal eq null}">checked</c:if> id="kriminal1">Tidak
								</label>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<label for="kriminal2">
									<input type="radio" class=noBorder name="${status.expression}" value="1"
									<c:if test="${cmd.mku_kriminal eq 1}">checked</c:if> id="kriminal2">Ya
								</label>
								<font face="Verdana" size="1" color="#996600">, Jelaskan : </font>								
							</spring:bind>
							<spring:bind path="cmd.mku_ket_kriminal">
								<input type="text" name="${status.expression}" value="${status.value }" size="30" maxlength="50" tabindex="8"
								<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD' </c:if>>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600">6. </font></b>
						</th>
						<th colspan="2">
							<b><font face="Verdana" size="1" color="#996600">Apakah anda sudah pernah menjadi BP / AP di perusahaan Sinarmas lainnya ?</font></b>
						</th>
						<th colspan="4">
							<spring:bind path="cmd.mku_flag_bp">
								<label for="flag_bp1"> 
									<input type="radio" class=noBorder name="${status.expression}" value="1"
									<c:if test="${cmd.mku_flag_bp eq 1 or cmd.mku_flag_bp eq null}">checked</c:if> id="flag_bp1">Tidak
								</label>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<label for="flag_bp2">
									<input type="radio" class=noBorder name="${status.expression}" value="0"
									<c:if test="${cmd.mku_flag_bp eq 0}">checked</c:if> id="flag_bp2">Ya
								</label>
								<font face="Verdana" size="1" color="#996600">, Sebutkan : </font>								
							</spring:bind>
							<spring:bind path="cmd.mku_flag_bp_ket">
								<input type="text" name="${status.expression}" value="${status.value }" size="30" maxlength="50" tabindex="8"
								<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD' </c:if>>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th colspan="7"></th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
						</th>
						<th colspan="6">
							<u><b><font face="Verdana" size="1" color="#996600">Data Usaha yang dimiliki Business Partner (Data ini diisi, jika anda mempunyai usaha)</font></b></u>
						</th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Nama Perusahaan</font></b>
						</th>
						<th>
							<spring:bind path="cmd.mku_nama_perusahaan">
								<input type="text" name="${status.expression}"
									value="${status.value }" size="30" maxlength="50" tabindex="8"
									<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD' </c:if>>
							</spring:bind>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Jabatan</font></b>
						</th>
						<th colspan="3">
							<spring:bind path="cmd.mku_jabatan_perusahaan">
								<input type="text" name="${status.expression}"
									value="${status.value }" size="30" maxlength="50" tabindex="8"
									<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD' </c:if>>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Jenis Usaha</font></b>
						</th>
						<th>
							<spring:bind path="cmd.mku_jenis_perusahaan">
								<input type="text" name="${status.expression}"
									value="${status.value }" size="30" maxlength="50" tabindex="8"
									<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD' </c:if>>
							</spring:bind>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Telepon</font></b>
						</th>
						<th colspan="3">
							<spring:bind path="cmd.mku_telepon_perusahaan">
								<input type="text" name="${status.expression}"
									value="${status.value }" size="30" maxlength="50" tabindex="8"
									<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD' </c:if>>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th>
							<b><font face="Verdana" size="1" color="#996600"></font></b>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Alamat Kantor</font></b>
						</th>
						<th>
							<spring:bind path="cmd.mku_alamat_perusahaan">
								<textarea cols="50" rows="4" name="${status.expression }"
									tabindex="15" onkeyup="textCounter(this, 200); "
									onkeydown="textCounter(this, 200); " maxlength="30" size ="30"
									<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD' </c:if>>${status.value }</textarea>
								<font color="#CC3300">*</font>
							</spring:bind>
						</th>
						<th>
							<b><font face="Verdana" size="1" color="#996600">Kode Pos</font></b>
						</th>
						<th colspan="3">
							<spring:bind path="cmd.mku_kodepos_perusahaan">
								<input type="text" name="${status.expression}"
									value="${status.value }" size="10" maxlength="5" tabindex="8"
									<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD' </c:if>>
							</spring:bind>
						</th>
					</tr>
					<tr>
						<th colspan="7">&nbsp;</th>
					</tr>
					
					<!-- Upload -->
					<%-- <tr>
						<th class="subtitle" colspan="7" height="20">
							<p align="center"> <b><font face="Verdana" size="1" color="#FFFFFF">UPLOAD DOC</font></b>
						</th>
					</tr>
					<tr>
						<th>
							<spring:bind path="cmd.chbox">
								<input type="checkbox" name="${status.expression}" style="border: none;margin-top:-5px;">
							</spring:bind>
						</th>
						<th width="23%">
							<b><font color="#996600" size="1" face="Verdana">Untuk Upload Dokumen</font></b>
						</th>
						<th colspan="5" align="left">
							&nbsp;
						</th>
					</tr>   
					<tr>
						<th>
							&nbsp;
						</th>
						<th width="23%">
							<b><font color="#996600" size="1" face="Verdana">No Register Rekruitment</font></b>
						</th>
						<th colspan="5" align="left">
							<spring:bind path="cmd.mku_no_reg_upload">
								<input type="text" name="${status.expression}" value="${status.value }" size="30" maxlength="10">
							</spring:bind>
						</th>
					</tr>   
					<tr>
						<th>
							&nbsp;
						</th>
						<th width="23%">
							<b><font color="#996600" size="1" face="Verdana">Dokumen</font></b>
						</th>
						<th colspan="5" align="left">
							<spring:bind path="cmd.file1">
								<input name="${status.expression}" type="file"/>
							</spring:bind>
						</th>
					</tr>  
					<tr>
						<th>
							&nbsp;
						</th>
						<th width="23%">
							<input type="submit" name="btnUpload" id="btnUpload" value="Upload" title="Upload">
						</th>
						<th colspan="5" align="left">
							&nbsp;
						</th>
					</tr> --%>
					<tr>
						<th colspan="7">&nbsp;</th>
					</tr>
					<!-- End Upload -->
					
					<tr>
						<th class="subtitle" colspan="7">
							<div align="center">
								<c:if test="${EDITNOTIMPLEMENT ne \"1\" }">
									<spring:bind path="cmd.submit1">
										<input type="submit" name= "submit1" value="Submit &raquo;" tabindex="47" onclick="next();">
									</spring:bind>
								</c:if>
							</div>
						</th>
					</tr>
					<tr>
						<td colspan="9">
							<p align="left">
								<b> <font face="Verdana" size="2" color="#CC3300">Note
										: * Wajib diisi</font>
								</b>
							</p>
						</td>
					</tr>
				</table>
				
				<table border="0" width="100%"  cellspacing="0" cellpadding="0" class="entry">
					<tr>
						<td>		     
							<spring:bind path="cmd.*">
								<c:if test="${not empty status.errorMessages}">
								<div id="error">ERROR:<br>
									<c:forEach var="error" items="${status.errorMessages}"> - <c:out value="${error}" escapeXml="false" />
										<br />
									</c:forEach>
								</div>
								</c:if>									
							</spring:bind> 
						</td>
					</tr>
          			<tr>
            			<td colspan="4" height="20">${keterangan}</td>
          			</tr>
        		</table> 
			</form>
		</td>
	</tr>
</table>

<ajax:autocomplete
				  source="mku_kota"
				  target="mku_kota"
				  baseUrl="${path}/servlet/autocomplete?s=mku_kota&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_rumah"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />

</body>
<%@ include file="/include/page/footer.jsp"%>