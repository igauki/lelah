<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path}/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path}/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path}/include/js/ajaxtags/overlibmws.js"></script>
<script>
	hideLoadingMessage();
</script>
</head>
<BODY onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Upload Hasil Scan Dokumen New Business</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" style="text-align: center;" enctype="multipart/form-data">
			
					<table class="entry2">
						<tr >
				           	 <th >
									Status Pendaftaran
							  </th>
							  <th colspan="3">
							  		<c:choose>
								  		<c:when test="${not empty data.mku_no_reg and empty data.mku_tgl_transfer_admin}">								  			
								  			<c:choose>
								  				<c:when test="${empty data.mku_flag_ktp}">
								  					<a href="${path}/rekruitment/upload_rekruitment_new.htm?window=main&kode_rekrut=${data.mku_no_reg}">
								  					STEP 2 : Silahkan Upload KTP Agen
								  					</a>
								  				</c:when>
								  				<c:when test="${empty data.mku_flag_foto}">
								  					<a href="${path}/rekruitment/upload_rekruitment_new.htm?window=main&kode_rekrut=${data.mku_no_reg}">
								  					STEP 2 : Silahkan Upload FOTO Agen
								  					</a>
								  				</c:when>
								  				<c:when test="${empty data.mku_flag_buku_rek}">
								  					<a href="${path}/rekruitment/upload_rekruitment_new.htm?window=main&kode_rekrut=${data.mku_no_reg}">
								  					STEP 2 : Silahkan Upload Buku Rekening Agen
								  					</a>
								  				</c:when>
								  				<c:when test="${empty data.mku_flag_bsb_ujian}">
								  					<a href="${path}/rekruitment/upload_rekruitment_new.htm?window=main&kode_rekrut=${data.mku_no_reg}">
								  					STEP 2 : Silahkan Upload BSB Ujian Agen
								  					</a>
								  				</c:when>
								  				<c:otherwise>
								  					<a href="${path}/rekruitment/rekrut.htm?window=main&kode_rekrut=${data.mku_no_reg}">
								  					STEP 3 : Silahkan Kirim Data Ke Admin
								  					</a>
								  				</c:otherwise>
								  				     
								  			</c:choose>
								  		</c:when>
								  		<c:when test="${empty data.mku_no_reg}">
								  			<a href="${path}/rekruitment/rekrut_keagenan.htm?window=main">
								  			STEP 1 : Silahkan Input data Agen Baru
								  			</a>
								  		</c:when>
								  		
								  	</c:choose>
							  
							  </th>
				          </tr>
						<tr>
							<th style="vertical-align: top;" colspan="4">
								
								<fieldset>
									<legend>Daftar File Saat ini</legend>
									<table class="displaytag">
										<thead>
											<tr>
												<th>File</th>
												<th>Last Modified</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="s" items="${daftarAda}">
												<tr>
													<td style="background-color: white; text-align: left;">${s.key}</td>
													<td style="background-color: white; text-align: center;">${s.value}</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</fieldset>
								
								<fieldset>
									<legend>Upload File untuk Register SPAJ ${reg_spaj}</legend>
																		
									<table class="entry2">
										
											<tr>
												<th>Tipe Berkas :
													<select name="berkas">
														<option value="0">[Silahkan pilih tipe Berkas]</option>
														<c:forEach var="d" items="${select_berkas}">
															<option
																<c:if test="${berkas eq d.ID}"> SELECTED </c:if>
																	value="${d.ID}">${d.BERKAS}</option>
				 										</c:forEach>
													</select>
												</th>
												<td>
													<input type="file" name="file1" size="60">
												</td>
											</tr>
										
										<!--
										<tr>
											<td colspan="2" class="error">
												* Syarat untuk dapat mencetak Polis
											</td>
										</tr>
										-->
									</table>
								</fieldset>
								
								<fieldset>
									<legend>Action</legend>
									<input type="submit" name="upload" value="Upload" onclick="return confirm('Sudah cek kebenaran datanya?');">
								</fieldset>

							</th>
						</tr>
					</table>			

				</form>
			</div>
		</div>
	</div>

</form>
</body>
<c:if test="${not empty pesan}">
	<script>alert('${pesan}');</script>
</c:if>
<%@ include file="/include/page/footer.jsp"%>