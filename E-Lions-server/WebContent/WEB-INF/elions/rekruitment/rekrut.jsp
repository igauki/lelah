<%@ include file="/include/page/header.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	hideLoadingMessage();
	
	
	function buttonLinks(str){
		if(str=='cari'){
			spaj = document.frmParam.spaj.value;
			
			if(spaj==''){
				alert('Harap pilih No Registrasi terlebih dahulu');
			}else{
				document.getElementById('infoFrame').src='${path}/rekruitment/rekrut_keagenan.htm?window=main&kode_rekrut='+document.frmParam.spaj.value;
			}
		}else{
			spaj = document.frmParam.spaj.value;
			if(spaj==''){
				alert('Harap pilih No Registrasi terlebih dahulu');
			}else{
			
				 switch (str) {
				case "upload" :
					document.getElementById('infoFrame').src='${path}/rekruitment/upload_rekruitment_new.htm?window=main&kode_rekrut='+spaj;
					break;							
				} 
				
			}
		}
	}
	
	function trans(){
		spaj = document.frmParam.spaj.value;
		if(spaj==''){
			alert('Harap  pilih No Registrasi terlebih dahulu');
		}else{
			document.getElementById('infoFrame').src='${path}/rekruitment/rekrut.htm?transfer=rek&kode_rekrut='+document.frmParam.spaj.value;
		}
	}

	function awal(){
		
		setFrameSize('infoFrame', 64);
	}
	
	
	
</script>
<body 
	onload="awal(); setFrameSize('infoFrame', 85);"
	onresize="setFrameSize('infoFrame', 85);" style="height: 100%;">

<form name="frmParam" method="post">
<div class="tabcontent">
<table class="entry2" style="width: 98.4%;">
	<tr>
		<th>No Registrasi</th>
		<td>
			
			<input type="button" value="Refresh" name="refresh" onclick="document.frames['infoFrame'].location.reload()" style="background-color: yellow;">
			
			

	
			
			<select name="spaj" onChange="cariregion(this.options[this.selectedIndex].value,'region');" id="spaj">
				<option value="">[--- Silahkan Pilih/Cari No Registrasi ---]</option>
				<c:forEach var="s" items="${daftarSPAJ}">
					<option value="${s.MKU_NO_REG }" style="background-color: ${s.BG};" 
						<c:if test="${s.MKU_NO_REG eq param.NO_REG }">selected</c:if>>${s.NO_REG}</option>
				</c:forEach>
			</select>

			<input type="button" value="INFO" name="info" onclick="return buttonLinks('cari');" 
				accesskey="T" onmouseover="return overlib('Edit Form Rekruitment Keagenan', AUTOSTATUS, WRAP);" onmouseout="nd();"> 
							
		</td>
	</tr>
	<tr>
		<th >Proses</th>
		<td >
		
			<input type="button" value="Step 1 - Input" name="info" id="info"
				onclick="document.getElementById('infoFrame').src='${path}/rekruitment/rekrut_keagenan.htm?window=main';" 
				accesskey="I" onmouseover="return overlib('Input Agen Baru', AUTOSTATUS, WRAP);" onmouseout="nd();"
				<c:if test="${sessionScope.currentUser.flag_approve eq 1}"> disabled="true" </c:if>>
			
			<input type="button" value="Step 2 - Upload Dokumen" name="upload" 
			onclick="return buttonLinks('upload');" 
			accesskey="E" onmouseover="return overlib('Upload Kelengkapan Dokumen', AUTOSTATUS, WRAP);" onmouseout="nd();">
			
			<input type="button" value="Step 3 - Kirim Ke Admin" name="kirim" 
			onclick="trans();"  
			accesskey="T" onmouseover="return overlib('Transfer Data Ke Admin', AUTOSTATUS, WRAP);" onmouseout="nd();">

		</td>
	</tr>
	

	<tr>
		<td colspan="2">

			<iframe src="${path}/rekruitment/rekrut_keagenan.htm?window=main" name="infoFrame" id="infoFrame"
			width="100%" style="width: 100%;"> Please Wait... </iframe>

		</td>
	</tr>
</table>
</div>
</form>

</body>
<%@ include file="/include/page/footer.jsp"%>