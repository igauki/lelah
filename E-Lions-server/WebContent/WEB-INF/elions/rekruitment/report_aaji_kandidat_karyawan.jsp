<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<html>
<head>
	<jsp:include page="/include/page/header_jquery.jsp">
		<jsp:param value="${path}" name="path"/>
	</jsp:include>
</head>
  
<body>
<fieldset>
	<legend>REPORT CEK KANDIDAT KARYAWAN BARU</legend>
	<table class="entry2">
		<tr>
			<th>Cek Kandidat</th>
			<td>
				<input type="text" id="tglA" name="tglA" class="tgl"> &nbsp;s/d&nbsp;
				<input type="text" id="tglB" name="tglB" class="tgl">
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><input type="button" name="print" id="print" value="Print"></td>
		</tr>
	</table>
</fieldset>
<script type="text/javascript">
$(document).ready(function(){
	$(".tgl").datepicker({
		changeMonth : true,
		changeYear : true
	});
});
</script>
</body>
</html>
