<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />

<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript">
$().ready(function() {
	var pesan = '${pesan}';
	if(pesan != '') alert(pesan);
	
	$("#upload").attr('disabled','disabled');
	$("#trans").attr('disabled','disabled');
	$("#edit").attr('disabled','disabled');
		
	$("#tabs").tabs();
	// (jQueryUI datepicker) init semua field datepicker
	$(".tgl").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: "dd/mm/yy" 
	});
	
	/* var uri = "${path}/rekruitment/multi.htm?window=ajax&pages=rekrut_tools&jn=carilistnoreg";
	$.getJSON(uri, function(result){
		$.each(result.data, function(id,val){
			var no_reg = val.MKU_NO_REG;
			$('<div class="dataKues" id ='+ no_reg+'>').append(
				val.MKU_NO_REG+"<br/>",
				val.MKU_FIRST+"<br/><hr>"
			).appendTo('.dataKues');
		});
	}); */
	
	$('#history').click(function(){
		var lus_id = $('#lus_id').val();
		window.location.href = '${path}/rekruitment/multi.htm?window=rekrut_tools&lus_id='+lus_id;
	});
	
	$(".dataKues").click(function(){
		$('#tbHist').empty();
		var no_reg = $(this).attr('id');
		$('#no_reg').val(no_reg);
		var url = '${path}/rekruitment/multi.htm?window=ajax&pages=rekrut_tools&jn=showhist&no_reg='+no_reg;
		$.getJSON(url, function(result){
			$.each(result.history,function(id,val){
				$('<tr>').append(
					$('<td>').text(val.tgl),
					$('<td>').text(val.ket),
					$('<td>').text(val.pos),
					$('<td>').text(val.user),
					$('<td>').text(val.dept)
				).appendTo('#tbHist');
			});
		});
		$("#upload").removeAttr('disabled');
		$("#trans").removeAttr('disabled');
		$("#edit").removeAttr('disabled');
// 		$("#cari").attr('disabled','disabled');
	});
	
	$('#upload').click(function(){
		var no_reg = $('#no_reg').val();
		popWin('${path}/rekruitment/multi.htm?window=upload_berkas&no_reg='+no_reg, 400, 700);
	});
	
	$('#trans').click(function(){
		var no_reg = $('#no_reg').val();
		var url = '${path}/rekruitment/multi.htm?window=ajax&pages=rekrut_tools&jn=transfer&no_reg='+no_reg;
		$.getJSON(url, function(result){
			alert(result.pesan);
			location.reload();
		});
	});
	
	$('#edit').click(function(){
		var no_reg = $('#no_reg').val();
		popWin('${path}/rekruitment/multi.htm?window=edit_calonaaji&mku_no_reg='+no_reg, 1200, 1800);
	});
	
	$('#cari').click(function(){
		var no_reg = 'null';
		popWin('${path}/rekruitment/multi.htm?window=edit_calonaaji&mku_no_reg='+no_reg,1200,1800);
	});
	
// 	$('#trans').on('click',function(){
// 		var no_reg = $('#no_reg').val();
// 		alert('click '+no_reg);
// 		var url = '${path}/rekruitment/multi.htm?window=ajax&pages=rekrut_tools&jn=transfer&no_reg='+no_reg;
// 		$.getJSON(url, function(result){
// 			alert(result.pesan);
// 			location.reload();
// 		});
// 	});
	
// 	$('#input').on('click',function(){
// 		window.location.href = '${path}/rekruitment/rekrut_keagenan.htm?window=main';
// 	});
	$('#input').click(function(){
		window.location.href = '${path}/rekruitment/rekrut_keagenan.htm?window=main';
	});
	
	/* $('#s_no_reg').on('input',function(e){
		$('.datakues').empty();
		var no_reg = $('#s_no_reg').val();
		var url = '${path}/rekruitment/multi.htm?window=ajax&pages=rekrut_tools&jn=carinoreg&no_reg='+no_reg;
		$.getJSON(url,function(result){
			$.each(result.noreg,function(id,val){
				$('.datakues').html(val.no_reg<br/>val.nama<br/><hr> );
			});
			$("#upload").removeAttr('disabled');
			$("#trans").removeAttr('disabled');
		});
    }); */
	/* function carinoreg(){
		$(function(){
			$('.datakues').empty();
			var no_reg = $('#s_no_reg').val();
			var url = '${path}/rekruitment/multi.htm?window=ajax&pages=rekrut_tools&jn=carinoreg&no_reg='+no_reg;
			$.getJSON(url,function(result){
				$.each(result.noreg,function(id,val){
					$('.datakues').html(val.no_reg<br/>val.nama<br/><hr> );
				});
				$("#upload").removeAttr('disabled');
				$("#trans").removeAttr('disabled');
			});
		});
	} */
	
});

</script>
<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	input{width: 200px;}
	button{width:125px;height: 30px;}
	
	input[type="submit"], input[type="button"]{
		width:125px;
		height: 20px;
		border:1px solid red;
    	text-decoration:none;
	}
	
	.dataKues{
		cursor:pointer;
		cursor: hand;
	}
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }
	
	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }
</style>
<body>
<!-- <c:if test="${not empty pesan }"> -->
<!-- <div id="success"> -->
<!-- 	${pesan } -->
<!-- </div> -->
<!-- </c:if> -->
	<div id="tab-1">
		<input type="hidden" name="no_reg" id="no_reg">
		<input type="hidden" name="lus_id" id="lus_id" value="${sessionScope.currentUser.lus_id }">
		<table class="entry2">
			<!-- <tr>
				<td colspan="2">
					<input type="text" name="s_no_reg" id="s_no_reg" placeholder="No Register">
				</td>
			</tr> -->
			<tr>
				<th style="width: 120px;" valign="top">
					<label>No. Register</label><br/>
					<label>Nama</label><br/><hr>
<!-- 					<div class="dataKues"></div> -->
					
					<c:forEach items="${data}" var="d">
						<div class="dataKues" id="${d.MKU_NO_REG}">
						<c:choose>
							<c:when test="${d.POSISI eq \'1\'}">
								<font color="blue" >
									${d.MKU_NO_REG} <br/>
									${d.MKU_FIRST } <br/>
								</font>
							</c:when>
							<c:otherwise>
								<font color="red">
									${d.MKU_NO_REG} <br/>
									${d.MKU_FIRST } <br/>
								</font>
							</c:otherwise>
						</c:choose>
						<hr>
						</div>
					</c:forEach>
				</th>
				<td valign="top">
					<fieldset>
<!-- 						<c:if test="${not empty pesan }"> -->
<!-- 						<div id="success"> -->
<!-- 							pesan : ${pesan } -->
<!-- 						</div> -->
<!-- 						</c:if>  -->
						<div id="data">
							<table>
								<tr>
									<td>
										<input type="button" value="Refresh" name="history" id="history">
										<input type="button" value="Upload" name="upload" id="upload">
										<input type="button" value="Transfer" name="trans" id="trans">
										<input type="button" value="Input" name="input" id="input">
										<input type="button" value="Search" name="cari" id="cari">
										<input type="button" value="Edit" name="edit" id="edit">
									</td>
								</tr>
								<tr>
									<td>
										<table>
											<tr>
												<th>Tanggal</th>
												<th>Deskripsi</th>
												<th>Posisi</th>
												<th>User</th>
												<th>Department</th>
											</tr>
											<tr>
												<tbody id="tbHist"></tbody>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</div>
					</fieldset>
				</td>
			</tr>
		</table>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>