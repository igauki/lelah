<%@ include file="/include/page/header.jsp"%>
<body style="height: 100%;">
	<form method=post>
		<fieldset>
			<legend>Testing</legend>
			<table class="entry">
				<tr>
					<th>SPAJ</th>
					<td>
						<input type="text" name="spaj" value="${spaj}">
						<input type="submit" name="show" value="SHOW">
					</td>
				</tr> 
			</table>

			<display:table id="baris2" name="nilaitunai2" class="simple" requestURI="" export="true" pagesize="100">
				<display:column property="KOL0" title="KOL0" sortable="true" />
				<display:column property="KOL1" title="KOL1" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true" />
				<display:column property="KOL2" title="KOL2" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true" />
				<display:column property="KOL3" title="KOL3" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true" />
				<display:column property="KOL4" title="KOL4" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true" />
				<display:column property="KOL5" title="KOL5" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true" />
			</display:table>
						
			<display:table id="baris" name="nilaitunai" class="simple" requestURI="" export="true" pagesize="100">   
				<display:caption title="Nilai Tunai hasil perhitungan" />
				<display:column property="tahun" title="TAHUN"  sortable="true"/>                                                                                            
				<display:column property="tsi" title="TSI" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                                      
				<display:column property="nilai_tunai" title="NILAI_TUNAI" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true" />                                                                                
				<display:column property="tahapan" title="TAHAPAN" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                                                                        
				<display:column property="bonus" title="BONUS" format="{0, number, #,##0.00;(#,##0.00)}"   sortable="true"/>                                                                                            
				<display:column property="saving" title="SAVING" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                                                                          
				<display:column property="deviden" title="DEVIDEN" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                                                                        
				<display:column property="maturity" title="MATURITY" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                                                                      
			</display:table>

		</fieldset>
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>
