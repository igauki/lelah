<%@ include file="/include/page/header.jsp"%>
<script>
	function pil(i){
		if(i=='2'){
			if(confirm("Ubah Status Medis menjadi Non Medis")){
				formpost.medis.value="0";
			}else if(confirm("Ubah Status Non Medis Menjadi Medis"))
				formpost.medis.value="1";

		}else if(i=='3'){
			formpost.submit();
		}else if(i=='4'){
			s=prompt("Silahkan Masukan Status AKsep Polis","1");
			if(s!=null)
				formpost.aksep.value=a;
			
		}
	}
	function awal(){
		if('${cmd.error}'==1)
			setupPanes('container1', 'tab2');
		else
			setupPanes('container1', 'tab1');
		if('1'=='${cmd.i}'){
			alert("ANda Tidak Punya AKses");
			window.location='${path}/common/menu.htm?frame=treemenu';
		}
	}
	
	function proses(flag){
		if(flag=='g'){
			formpost.count.value=1;
			formpost.submit();
		}else if(flag=='u')
			window.location="${path}/common/util.htm?window=updateStatusMstInsured";	
	}
	
	function updateCH(){
			window.location="${path}/common/util.htm?window=updateMstClientHistory";	
	}
	
	function insertReasTempNew(){
			window.location="${path}/common/util.htm?window=insertMReasTempNew";	
	}
</script>
<BODY onload="awal()" style="height: 100%;">

<form:form id="formpost" name="formpost" commandName="cmd">
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li><a href="#" onClick="return showPane('pane1', this)" id="tab1">Main</a></li>
			<c:if test="${cmd.error eq 1}">
				<li><a href="#" onClick="return showPane('pane2', this)" id="tab2">Detail</a></li>
			</c:if>
			<li><a href="#" onClick="return showPane('pane3', this)" id="tab3">Tambahan</a></li>
		</ul>
		<div class="tab-panes">
			<div id="pane1" class="panes">
				<table class="entry2">
					<tr>
						<th>Utilities Tool</th>
						<td>
							<input type="hidden" name="medis" >
							<input type="hidden" name="aksep" >
							<form:radiobutton cssClass="noborder" path="flagId" id="flagId" onclick="pil(1)" value="1"/>Benerin Tertangung
							<form:radiobutton cssClass="noborder" path="flagId" id="flagId" onclick="pil(2)" value="2"/>Ubah Medis Setelah Reas
						</td>
					</tr>
					<tr>
						<th></th>
						<td>
							<form:radiobutton disabled="true" cssClass="noborder" path="flagId" id="flagId" onclick="pil(3)" value="3"/>Edit Spaj Setelah Fund
							<form:radiobutton cssClass="noborder" path="flagId" id="flagId" onclick="pil(4)" value="4"/>Ubah Status AKsep Polis
						</td>
					</tr>
					<tr>
						<th>Atas Permintaan User</th>
						<td>
							<select name="userReq">
								<option value=""/>
								<c:forEach items="${user}" var="x" varStatus="xt">
									<option value="${x.VALUE}">${x.VALUE }</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<th>No.Spaj</th>
						<td><form:input path="spaj" maxlength="11"/></td>
					</tr>
					<tr>
					</tr>	
					<tr>
					 <td colspan="4">
						<c:if test="${submitSuccess eq true}">
				        	<div id="success">
					        	 Berhasil
					    	</div>
				        </c:if>	
			  			<spring:bind path="cmd.*">
							<c:if test="${not empty status.errorMessages}">
								<div id="error">
									 Informasi:<br>
										<c:forEach var="error" items="${status.errorMessages}">
													 - <c:out value="${error}" escapeXml="false" />
											<br/>
										</c:forEach>
								</div>
							</c:if>									
						</spring:bind>
					 </td>
					</tr>
					<tr>
						<th></th>
						<td>	
							<input type="button" name="btnProses" onclick="proses();" value="Go"></input>
							<input type="button" name="btnUpdate" onclick="updateCH();" value="Update CLient History"></input>
							<input type="button" name="btnInsert" onclick="insertReasTempNew();" value="Insert RIDER LINK"></input>
							
						</td>
					</tr>
				</table>
			</div>
			<c:if test="${cmd.error eq 1}">
				<div id="pane2" class="panes">
					<table class="entry2" >
					<c:forEach items="${cmd.lsNab}" var="x" varStatus="xt">
						<tr>
							<th>
								Nilai NAB - ${x.lji_invest }
							</th>
							<td>
								<form:input path="lsNab[${xt.index }].mtu_nab" />
							</td>
						</tr>
					</c:forEach>	
					</table>
				<fieldset>	
				<legend>Tabel Detail Ulink</legend>
					<display:table id="baris" name="cmd.lsDetUlink" class="simple" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="" export="false" pagesize="20">   
						<display:column property="mu_ke" title="MU_KE"  sortable="true"/>                                                                                            
						<display:column property="lji_id" title="LJI_ID"  sortable="true"/>                                                                                          
						<display:column property="reg_spaj" title="REG_SPAJ"  sortable="true"/>                                                                                      
						<display:column property="mdu_persen" title="MDU_PERSEN" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                        
						<display:column property="mdu_jumlah" title="MDU_JUMLAH" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                        
						<display:column property="mdu_unit" title="MDU_UNIT" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                            
						<display:column property="mdu_saldo_unit" title="MDU_SALDO_UNIT" format="{0, number, #,##0.00000;(#,##0.00000)}"  sortable="true"/>                                
						<display:column property="mdu_persen_tu" title="MDU_PERSEN_TU" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                  
						<display:column property="mdu_last_trans" title="MDU_LAST_TRANS" format="{0, date, dd/MM/yyyy}"  sortable="true"/>                                           
						<display:column property="mdu_flag_min" title="MDU_FLAG_MIN"  sortable="true"/>                                                                              
						<display:column property="mdu_aktif" title="MDU_AKTIF"  sortable="true"/>                                                                                    
						<display:column property="mdu_saldo_unit_pp" title="MDU_SALDO_UNIT_PP" format="{0, number, #,##0.00000;(#,##0.00000)}"  sortable="true"/>                          
						<display:column property="mdu_saldo_unit_tu" title="MDU_SALDO_UNIT_TU" format="{0, number, #,##0.00000;(#,##0.00000)}"  sortable="true"/>                          
					</display:table>                                 				
				</fieldset>
				<fieldset>
				<legend>Tabel Trans Ulink</legend>
					<display:table id="baris2" name="cmd.lsTransUlink" class="simple" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="" export="false" pagesize="20">   
						<display:column property="mu_ke" title="MU_KE"  sortable="true"/>                                                                                            
						<display:column property="mtu_ke" title="MTU_KE"  sortable="true"/>                                                                                          
						<display:column property="lt_id" title="LT_ID"  sortable="true"/>                                                                                            
						<display:column property="lji_id" title="LJI_ID"  sortable="true"/>                                                                                          
						<display:column property="mtu_desc" title="MTU_DESC"  sortable="true"/>                                                                                      
						<display:column property="mtu_jumlah" title="MTU_JUMLAH" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                        
						<display:column property="mtu_nab" title="MTU_NAB" format="{0, number, #,##0.00000;(#,##0.00000)}"  sortable="true"/>                                              
						<display:column property="mtu_unit" title="MTU_UNIT" format="{0, number, #,##0.00000;(#,##0.00000)}"  sortable="true"/>                                            
						<display:column property="mtu_saldo_unit" title="MTU_SALDO_UNIT" format="{0, number, #,##0.00000;(#,##0.00000)}"  sortable="true"/>                                
						<display:column property="mtu_tahun_ke" title="MTU_TAHUN_KE"  sortable="true"/>                                                                              
						<display:column property="mtu_premi_ke" title="MTU_PREMI_KE"  sortable="true"/>                                                                              
						<display:column property="lspd_id" title="LSPD_ID"  sortable="true"/>                                                                                        
						<display:column property="mtu_saldo_unit_pp" title="MTU_SALDO_UNIT_PP" format="{0, number, #,##0.00000;(#,##0.00000)}"  sortable="true"/>                          
						<display:column property="mtu_saldo_unit_tu" title="MTU_SALDO_UNIT_TU" format="{0, number, #,##0.00000;(#,##0.00000)}"  sortable="true"/>                          
					</display:table>      
				</fieldset>	
				<fieldset>
				<legend>Tabel Trans Ulink New</legend>
					<display:table id="baris2" name="cmd.lsTransUlinkNew" class="simple" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="" export="false" pagesize="20">   
						<display:column property="mu_ke" title="MU_KE"  sortable="true"/>                                                                                            
						<display:column property="mtu_ke" title="MTU_KE"  sortable="true"/>                                                                                          
						<display:column property="lt_id" title="LT_ID"  sortable="true"/>                                                                                            
						<display:column property="lji_id" title="LJI_ID"  sortable="true"/>                                                                                          
						<display:column property="mtu_desc" title="MTU_DESC"  sortable="true"/>                                                                                      
						<display:column property="mtu_jumlah" title="MTU_JUMLAH" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                        
						<display:column property="mtu_nab" title="MTU_NAB" format="{0, number, #,##0.00000;(#,##0.00000)}"  sortable="true"/>                                              
						<display:column property="mtu_unit" title="MTU_UNIT" format="{0, number, #,##0.00000;(#,##0.00000)}"  sortable="true"/>                                            
						<display:column property="mtu_saldo_unit" title="MTU_SALDO_UNIT" format="{0, number, #,##0.00000;(#,##0.00000)}"  sortable="true"/>                                
						<display:column property="mtu_tahun_ke" title="MTU_TAHUN_KE"  sortable="true"/>                                                                              
						<display:column property="mtu_premi_ke" title="MTU_PREMI_KE"  sortable="true"/>                                                                              
						<display:column property="lspd_id" title="LSPD_ID"  sortable="true"/>                                                                                        
						<display:column property="mtu_saldo_unit_pp" title="MTU_SALDO_UNIT_PP" format="{0, number, #,##0.00000;(#,##0.00000)}"  sortable="true"/>                          
						<display:column property="mtu_saldo_unit_tu" title="MTU_SALDO_UNIT_TU" format="{0, number, #,##0.00000;(#,##0.00000)}"  sortable="true"/>                          
					</display:table>      
				</fieldset>	
				<table class="entry2">
					<tr>
					 <td colspan="4">
						<c:if test="${submitSuccess eq true}">
				        	<div id="success">
					        	 Berhasil
					    	</div>
				        </c:if>	
			  			<spring:bind path="cmd.*">
							<c:if test="${not empty status.errorMessages}">
								<div id="error">
									 Informasi:<br>
										<c:forEach var="error" items="${status.errorMessages}">
													 - <c:out value="${error}" escapeXml="false" />
											<br/>
										</c:forEach>
								</div>
							</c:if>									
						</spring:bind>
					 </td>
					</tr>
					<tr>
						<th></th>
						<td>	
							<input type="button" name="btnProses" value="Go" onclick="proses('g')"></input>
						</td>
					</tr>				
				</table>
				</div>		
			</c:if>	
			<div id="pane3" class="panes">
				<input type="button" onclick="proses('u')" value="Update">
			</div>
		</div>	
	</div>		
	<form:hidden path="count" />
</form:form>
</body>
<%@ include file="/include/page/footer.jsp"%>