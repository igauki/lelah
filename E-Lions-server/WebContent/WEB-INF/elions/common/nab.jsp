<%@ include file="/include/page/header.jsp"%>
<body style="height: 100%;">
<div class="tabcontent">
<table class="displaytag" style="width: auto;">
	<thead>
		<tr>
			<th>File</th>
			<th>Date Created</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="s" items="${daftarFile}">
			<tr>
				<td class="left">
					<a href="${path}/common/util.htm?window=nab&file=${s.key}&dir=${s.desc}">${s.key}</a>
				</td>
				<td class="center">${s.value}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
</div>
</body>
<%@ include file="/include/page/footer.jsp"%>