<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
	<title>PT. Asuransi Jiwa Sinarmas MSIG</title>
	<meta name="verify-v1" content="NWau46kEyEm+WnYb1AfPtB0ZUV07pSP40di+b+kxleA=" />
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta name="Description" content="SinarmasLife">
	<link rel="Stylesheet" type="text/css" href="${path }/include/css/default.css" media="screen">
	<link href="${path}/include/image/eas.ico" rel="shortcut icon">
	<script type="text/javascript" src="${path }/include/js/default.js"></script>
	<script type="text/javascript">
		function handleEnter(inField, e) {
		    var charCode;
		    
		    if(e && e.which){
		        charCode = e.which;
		    }else if(window.event){
		        e = window.event;
		        charCode = e.keyCode;
		    }
		
		    if(charCode == 13) {
		        document.formpost.btnLogin.click();
		    }
		}	
		
		function log_in(btn){
			btn.disabled=true;
			document.formpost.screenWidth.value = screen.width;
			document.formpost.screenHeight.value = screen.height;
			document.formpost.submit();
		}

		function on_load(){
			if(document.formpost.name.value.length > 1)
				document.formpost.pass.focus(); 
			else 
				document.formpost.name.focus();
		}

		function vrsn_splash() {
			tbar = "location=yes,status=yes,resizable=yes,scrollbars=yes,width=560,height=500";
			sw = window.open("https://seal.verisign.com/splash?form_file=fdf/splash.fdf&dn=www.sinarmasmsiglife.co.id&lang=en", 'VRSN_Splash', tbar);
			sw.focus();
		}
		
	</script>
</head>
<body onload="on_load();" style="height: 100%; width: 100%; text-align: center;">

	<c:choose>
		<c:when test="${notAllowed}">
			<script type="text/javascript">
			alert('Maaf anda tidak dapat mengakses halaman ini. Terima kasih.');
			window.close();
		</script>
		</c:when>
		<c:otherwise>

		<form method="post" name="formpost" action="">
			<p>
			  <input type="hidden" name="screenWidth">
			  <input type="hidden" name="screenHeight">
			  <input type="hidden" name="deebee" value="${deebee}">
			  <input type="hidden" name="login" value="Login">
			  <input type="hidden" name="kmid" value="${kmid}">
</p>
			<p><b>
			<table border="1" bordercolor="gray" width="350" height="250">
			<tr bgcolor="#909090">
				<td style="font-size: 16; font-weight: bold;" >
					LOGIN&nbsp;&nbsp;AKSES&nbsp;&nbsp;ATTENTION LIST
				</td>
			</tr>
			<tr>
			<td align="center">
			<font size="3"><b>E-LIONS</b></font>
			<table border="1" bordercolor="gray" >
				<tr bgcolor="#909090">
					<td align="center" colspan="2">
						<b>USER LOGIN</b>
					</td>
				</tr>
				<tr>
					<td align="right">
						User<br>
						<spring:bind path="user.name">
							<input type="text" name="${status.expression}" id="${status.expression}" value="${status.value}" onfocus="this.select();" onkeyup="handleEnter(this, event);">
						</spring:bind>
						<br>
						Password<br>
						<spring:bind path="user.pass">
							<input type="password" name="${status.expression}" id="${status.expression}" onfocus="this.select();" onkeyup="handleEnter(this, event);">
						</spring:bind>
						<br>
						<spring:bind path="user.*">
							<c:if test="${not empty status.errorMessages}">
								<div id="error">
									<c:forEach var="error" items="${status.errorMessages}">
										- <c:out value="${error}" escapeXml="false" /><br>
									</c:forEach>
								</div>
							</c:if>
						</spring:bind>
					</td>
					<td>
						<input type="button" name="btnLogin" value="Login" style="height: 60px; width: 60px" onclick="log_in(this);">
					</td>
				</tr>
		  </table>
		  </td>
			</tr>
			</table>
			
			<div id="footer_login"><br>
			  <spring:message code="copyright" />
				Sinarmas MSIG Life IT Department [${sessionScope.deebee}]
		  </div>
			
		</form>
		
		</c:otherwise>
	</c:choose>
</body>
</html>