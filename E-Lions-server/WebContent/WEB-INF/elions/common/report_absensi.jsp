<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-blue.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript">
			function cari(){
				if (document.getElementById('admin') != null) admin = document.getElementById('admin').value;
				else admin = '';
				msag_id = document.getElementById('lsAgenAbsen').value;
				tgl1 = document.getElementById('tanggalAwal').value;
				tgl2 = document.getElementById('tanggalAkhir').value;
				if(tgl1 == '' || tgl2 == '') alert("masukkan tanggal terlebih dahulu");
				else popWin('${path}/report/bac.pdf?window=report_absensi&msag_id='+msag_id+'&tanggalAwal='+tgl1+'&tanggalAkhir='+tgl2+'&admin='+admin,600,800);
			}
			
			function selectAdmin() {
				document.formpost.submit();
			}
		</script>
		
	</head>
  
	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Report Absensi</a>
				</li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form id="formpost" name="formpost" method="post">
						<table class="entry2">
							<tr>
								<th width="10%">Tanggal :</th>
								<td width="30%" colspan="2">
									<script>inputDate('tanggalAwal', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>
									 s/d 
									 <script>inputDate('tanggalAkhir', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>
								</td>
								<td align="left" rowSpan="3"></td>
							</tr>
							<c:if test="${not empty lsAdminActive}">
								<tr>
									<th>Admin :</th>
									<td colspan="2">
										<select id="admin" name="admin" onchange="selectAdmin()">
											<option value=""></option>
											<c:forEach var="d" items="${lsAdminActive}">
												<option value="${d.value}" <c:if test="${d.value eq admin}">selected="selected"</c:if>>${d.key}</option>
											</c:forEach>
										</select>	
									</td>
								</tr>
							</c:if>
							<tr>
								<th>Agen :</th>
								<td colspan="2">
									<select id="lsAgenAbsen" name="lsAgenAbsen">
										<option value="0">All</option>
										<c:forEach var="d" items="${lsAgent}">
											<option value="${d.value}">${d.key}</option>
										</c:forEach>
									</select>	
								</td>
							</tr>
							<tr>
								<th>&nbsp;</th>
								<td><input type="button" name="btnCari" value="Cari" onClick="cari();"></td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>
