<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript" src="${path }/include/js/dhtmlx/dhtmlXCommon.js"></script><!-- TreeView -->
<script type="text/javascript" src="${path }/include/js/dhtmlx/dhtmlXTree.js"></script>
<script>
	function reload(){
		document.getElementById('treebox1').innerHTML='';
		tree=new dhtmlXTreeObject("treebox1","100%","100%",0);
		tree.setImagePath("${path}/include/js/dhtmlx/img/");
		tree.enableDragAndDrop(1);
		tree.loadXML("${path}/common/menu.htm?frame=xml&tipe=cabangNotRegistered&spaj=&lus_id=${lus}");	
		
		document.getElementById('treebox2').innerHTML='';
		tree2=new dhtmlXTreeObject("treebox2","100%","100%",0);
		tree2.setImagePath("${path}/include/js/dhtmlx/img/");
		tree2.enableDragAndDrop(1);
		tree2.loadXML("${path}/common/menu.htm?frame=xml&tipe=cabangRegistered&spaj=&lus_id=${lus}");	
		
	}
	
	function klik(p){
		if(p=='add'){
			return confirm('Tambahkan akses semua cabang ke user ${username}?\nPerubahan akan langsung tersimpan.');
		}else if(p=='save'){
			cek = tree2.getSubItems(0).split(",");
			document.formpost.nilai.value= cek;
			return confirm('Simpan perubahan user ${username}?');
		}else if(p=='remove'){
			return confirm('Hapus semua akses cabang user ${username}?\nPerubahan akan langsung tersimpan.');
		}
	}
</script>
<body style="height: 100%;">
	<form name="formpost" method="post">
		<div class="tabcontent">
			<span class="subtitle">Akses Cabang untuk User ${username}</span>
			<table class="entry">
				<tr>
					<th>
						<div id="treebox1" style="width:283px; height:313px;background-color:#f5f5f5;border :1px solid Silver;; overflow:auto;"/>
					</th>
					<td valign="middle" align="center">
						<input type="hidden" name="nilai" value="">
						<input type="submit" name="add" value="Add All" style="width: 80px; height: 30px;" onclick="return klik('add');"><br><br>
						<input type="submit" name="remove" value="Remove All" style="width: 80px; height: 30px;"onclick="return klik('remove');"><br><br>
						<input type="button" name="reset" value="Reset" style="width: 80px; height: 30px;" onclick="reload();"><br><br>
						<input type="submit" name="save" value="Save" style="width: 80px; height: 30px;" onclick="return klik('save');"><br><br>
						<input type="button" name="close" value="Close" style="width: 80px; height: 30px;" onclick="window.close();"><br><br>
					</td>
					<th>
						<div id="treebox2" style="width:283px; height:313px;background-color:#f5f5f5;border :1px solid Silver;; overflow:auto;"/>
					</th>
				</tr>
				<tr>
					<td colspan=3>
						<div class="info">* Geser Menu dari sebelah kiri ke kanan untuk menambahkan</div>
					</td>
				</tr>
			</table>
		</div>
	</form>
</body>
<script>
	reload();	
	//hideLoadingMessage();
</script>
<%@ include file="/include/page/footer.jsp"%>