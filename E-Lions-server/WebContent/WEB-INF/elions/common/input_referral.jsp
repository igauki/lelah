<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>
<script>
	function pil(pro){
		formpost.proses.value=pro;
		if(pro=='agen'){
			document.formpost.elements['nasabah.mns_nama_lead'].value=document.formpost.elements['nasabah.referrer_fa'].label
		}else if(pro=='referrer'){
			document.formpost.elements['nasabah.mns_nama_lead'].value=document.formpost.elements['nasabah.referrer_id'].label
		}
		formpost.submit();
	}
	
	function awal(){
		if('${cmd.error}'=='1'){
			alert("No Reff SUdah Pernah Digunakan");
			window.location='${path }/common/input_referral.htm?nomor=${cmd.nasabah.mns_kd_nasabah}&flag=0';
		}		
	}
	
	function connect(){
		popWin('${path }/common/referral_nasabah.htm?nomor=${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}&flag=0', 150, 640);
	}
	
	
	function prosesConfirm(){
		var centang = document.getElementById('nasabah.mns_ok_saran').value;
		//alert(centang);
		if(centang==1){
			var x =window.confirm('Pastikan data yang Anda isi telah lengkap dan benar !\n Transfer ke Fact Find ?');
			if(x){
				//alert('masuk');
				document.getElementById('formpost').submit();
			}
		}else {
			var x =window.confirm('Pastikan data yang Anda isi telah lengkap dan benar !\n Yakin tidak ingin mengisi Form JIFFY dan Transfer ke Closing ?');
			if(x){
				//alert('masuk');
				document.getElementById('formpost').submit();
			}
		}
	}
	
	function centang(){
		var a = document.getElementById('nasabah.mns_ok_saran').checked;
		//alert(a);
		if(a==true){
			document.getElementById('nasabah.mns_ok_saran').value=1;
		}else {
			document.getElementById('nasabah.mns_ok_saran').value=0;
		}
	}
	
</script>
<BODY style="height: 100%;" onLoad="awal();">

<form:form id="formpost" name="formpost" commandName="cmd">
	<table class="entry2">
		<tr>
			<th>Kode Nasabah : ${cmd.nasabah.mns_kd_nasabah }</th>
		</tr>
		<tr>
			<th>Cabang :  
				<form:select path="nasabah.kode" onchange="pil('kode')">
					<form:option label="" value=""/>
					<form:options items="${lsCabBii}" itemLabel="value" itemValue="key" />
				</form:select> 

			</th>
			<th>${cmd.nasabah.cabang_detail}</th>
		</tr>
		<tr>
			<th>BFA :
				<form:select path="nasabah.msag_id" onchange="pil('bfa')">
					<form:option label="" value=""/>
					<form:options items="${lsBfa}" itemLabel="nama_bfa" itemValue="msag_id" />
				</form:select> 
			</th>
			<th>${cmd.nasabah.bfa_detail}</th>
		</tr>
		<tr>
			<th colspan="2">
				<fieldset>
				<legend>Informasi Referral</legend>
					<table class="entry2">
						<tr>
							<th width="50%" >
								No Referral  : ${cmd.nasabah.mns_no_ref}
							</th>
							<th width="50%">
								Tanggal : <script>inputDate('tgl', '${cmd.nasabah.s_mns_tgl_app}', false, '', 9);</script>
							</th>
						</tr>
						<tr>
							<th>
								<fieldset>
									<legend>Referral</legend>
									<table class="entry2">
										<tr>
											<th>Nama</th>
											<td>
												<form:input cssStyle="border-color:#6893B0;" path="nasabah.mns_nama" size="30"/>
											</td>
										</tr>
										<tr>
											<th>DCIF</th>
											<td>
												<form:input cssStyle="border-color:#6893B0;" path="nasabah.mns_dcif" size="30"/>
											</td>
										</tr>
										<tr>
											<th>Tipe Nasabah</th>
											<td>
												<form:radiobutton cssClass="noborder" path="nasabah.mns_tipe_nasabah" id="nasabah.mns_tipe_nasabah" value="1"/>
												Baru
												<form:radiobutton cssClass="noborder" path="nasabah.mns_tipe_nasabah" id="nasabah.mns_tipe_nasabah" value="0"/>
												Lama
											</td>
										</tr>
									</table>
								</fieldset>
							</th>
							<th>
								<fieldset>
									<legend>Telepon</legend>
									<table class="entry2">
										<tr>
											<th>Kantor</th>
											<td width="10">
												<form:input cssStyle="border-color:#6893B0;" path="nasabah.mns_area_kantor" size="5" maxlength="4"/>
											</td>
											<td>
												<form:input cssStyle="border-color:#6893B0;" path="nasabah.mns_tlpkantor" size="20" maxlength="15"/>
											</td>
										</tr>
										<tr>
											<th>Rumah</th>
											<td width="10">
												<form:input cssStyle="border-color:#6893B0;" path="nasabah.mns_area_rumah" size="5" maxlength="4"/>
											</td>
											<td>
												<form:input cssStyle="border-color:#6893B0;" path="nasabah.mns_tlprumah" size="20" maxlength="15"/>
											</td>
										</tr>
										<tr>
											<th>HP</th>
											<td/>
											<td>
												<form:input cssStyle="border-color:#6893B0;" path="nasabah.mns_hp" size="20" maxlength="15"/>
											</td>
										</tr>
									</table>
								</fieldset>
							</th>
						</tr>	
						<tr>
							<th colspan="2">
								<fieldset>
									<legend>Telepon</legend>
									<table class="entry2" >
										<tr>
											<td colspan="3">
												Waktu Yang Tepat Untuk Dihubungi
											</td>
										</tr>
										<tr>
											<td colspan="3">
												Jika diminta oleh Nasabah atau lainnya, Bagaimana Nasabah Mengetahui mengenal layanan ini?
											</td>
										</tr>
										<tr>
											<td>
												<form:radiobutton cssClass="noborder" path="nasabah.mns_layanan" id="nasabah.mns_layanan" value="1"/>
												Direct Marketing
											</td>
											<td>
												<form:radiobutton cssClass="noborder" path="nasabah.mns_layanan" id="nasabah.mns_layanan" value="3"/>
												Call Centre BII
											</td>
											<td>	
												<form:radiobutton cssClass="noborder" path="nasabah.mns_layanan" id="nasabah.mns_layanan" value="5"/>
												Teman/Saudara
											</td>
										</tr>
										<tr>
											<td>
												<form:radiobutton cssClass="noborder" path="nasabah.mns_layanan" id="nasabah.mns_layanan" value="2"/>
												Website
											</td>
											<td>	
												<form:radiobutton cssClass="noborder" path="nasabah.mns_layanan" id="nasabah.mns_layanan" value="4"/>
												Call Centre Ekalife
											</td>
										</tr>
										<tr>
											<th colspan="2">
												Apakah Tujuan Layanan ini telah dijelaskan ke nasabah : 
												<form:radiobutton cssClass="noborder" path="nasabah.mns_st_jelas" id="nasabah.mns_st_jelas" value="1"/>
												Ya
												<form:radiobutton cssClass="noborder" path="nasabah.mns_st_jelas" id="nasabah.mns_st_jelas" value="0"/>
												Tidak
											</th>
										</tr>
									</table>
								</fieldset>
							</th>
						</tr>
						<tr>
							<th colspan="2">
								<fieldset>
									<legend>Informasi Pemberi LEAD</legend>
									<table class="entry2">
										<tr>
											<th>Jenis Lead</th>
											<td>
												<form:radiobutton cssClass="noborder" path="nasabah.jn_lead" id="nasabah.jn_lead" value="1"/>REFERRER
												<form:radiobutton cssClass="noborder" path="nasabah.jn_lead" id="nasabah.jn_lead" value="2"/>SELFGEN
											</td>
										</tr>
										<tr>
											<th>Sumber Lead</th>
											<td>
												<form:select path="nasabah.ljl_jenis" onchange="pil('lead')">
													<form:option label="" value="0"/>
													<form:options items="${lsLead}" itemLabel="ljl_ket" itemValue="ljl_jenis" />
												</form:select> 
											</td>
											<c:choose>
												<c:when test="${cmd.nasabah.flag eq 1}">
													<th>
														${cmd.nasabah.namaLead }
													</th>
													<td>
														<c:choose>
															<c:when test="${cmd.nasabah.namaLead eq 'Nama Agen'}">
																<form:select path="nasabah.referrer_fa" onchange="pil('agen')">
																	<form:option label="" value=""/>
																	<form:options items="${lsReferrer}" itemLabel="nama_bfa" itemValue="msag_id" />
																</form:select> 
																<form:hidden path="nasabah.mns_nama_lead" />
															</c:when>	
															<c:when test="${cmd.nasabah.namaLead eq 'Nama Referrer'}">
																<form:select path="nasabah.referrer_id" onchange="pil('referrer')">
																	<form:option label="" value=""/>
																	<form:options items="${lsReferrerBii}" itemLabel="value" itemValue="key" />
																</form:select> 
																<form:hidden path="nasabah.mns_nama_lead" />
															</c:when>
															<c:when test="${cmd.nasabah.namaLead eq 'Nama'}">
																<form:input cssStyle="border-color:#6893B0;" path="nasabah.mns_nama_lead" size="50"/>
															</c:when>
														</c:choose>	
													</td>	
												</c:when>
											</c:choose>
										</tr>
										<tr>
											<th/><td/>
											<c:choose>
												<c:when test="${cmd.nasabah.flag eq 1}">
													<c:choose>
														<c:when test="${cmd.nasabah.namaLead eq 'Nama Agen'}">
															<th>Cabang</th>
															<td>
																<form:input cssStyle="border-color:#6893B0;" path="nasabah.mns_reff_cab" size="50"/>
															</td>
														</c:when>
														<c:when test="${cmd.nasabah.namaLead eq 'Nama Referrer'}">
															<th>Cabang</th>
															<td>
																<form:input cssStyle="border-color:#6893B0;" path="nasabah.mns_reff_cab" size="50"/>
															</td>
														</c:when>
														<c:when test="${cmd.nasabah.namaLead eq 'Nama'}">
															<td>
																
															</td>	
														</c:when>
													</c:choose>		
												</c:when>
											</c:choose>		
										</tr>
										<c:if test="${lsreferrer_id ne 0 }">
											<tr>
												<th colspan="4">
													<table class="entry2">
														<tr>
															<th>leader1</th>
															<th>leader2</th>
															<th colspan="2">leader3</th>
														</tr>
														<tr>
															<td>
																<form:select cssStyle="width: 8cm" path="nasabah.leader_ref1">
																	<form:options items="${lsReferrerLeader1}" itemLabel="value" itemValue="key" />
																</form:select> 
															</td>
															<td>
																<form:select cssStyle="width: 8cm" path="nasabah.leader_ref2">
																	<form:options items="${lsReferrerLeader1}" itemLabel="value" itemValue="key" />
																</form:select> 
															</td>
															<td colspan="2">
																<form:select cssStyle="width: 8cm" path="nasabah.leader_ref3">
																	<form:options items="${lsReferrerLeader1}" itemLabel="value" itemValue="key" />
																</form:select> 
															</td>
														</tr>
													</table>
												</th>
											</tr>
										</c:if>
										
										<tr>
											<th colspan="4">
												<form:checkbox onclick="centang();" cssClass="noborder" path="nasabah.mns_ok_saran" id="nasabah.mns_ok_saran" value="${cmd.nasabah.mns_ok_saran}" />
												Bersedia Mengisi JIFFY
											</th>
										</tr>
									</table>
								</fieldset>
							</th>
						</tr>
					</table>
				</fieldset>
			</th>
		</tr>
		<tr>
		 <td colspan="4">
			<c:if test="${submitSuccess eq true}">
				<c:choose>
					<c:when test="${cmd.flagAdd eq 1}">
						<script>
							alert("Berhasil Transfer");
							//addOptionToSelect(parent.document, parent.document.formpost.nomor, '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}', '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}');
							window.parent.tes();
						</script>
					</c:when>
					<c:otherwise>
						<script>
							//alert("Berhasil Simpan");
							//addOptionToSelect(parent.document, parent.document.formpost.nomor, '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}', '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}');
							//window.location='${path }/common/input_referral.htm?nomor=${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}&flag=0';
							popWin('${path }/common/referral_nasabah.htm?nomor=${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}&flag=0', 150, 640);
						</script>
					</c:otherwise>
				</c:choose>
	        	<div id="success">
		        	 Berhasil
		    	</div>
	        </c:if>	
  			<spring:bind path="cmd.*">
				<c:if test="${not empty status.errorMessages}">
					<div id="error">
						 Informasi:<br>
							<c:forEach var="error" items="${status.errorMessages}">
										 - <c:out value="${error}" escapeXml="false" />
								<br/>
							</c:forEach>
					</div>
				</c:if>									
			</spring:bind>
		 </td>
		</tr>
		<tr>
			<th colspan="2">	
				<input type="hidden" name="proses" value="0"></input>
				<input type="button" name="btnConnect" value="ConnectLead" onclick="connect();"></input>
				<input type="submit" name="btnsave" value="Save"></input>
				<input type="button" name="btntrans" value="Trans" onclick="prosesConfirm();"></input>
			</th>
		</tr>
	</table>
</form:form>
</body>
<script>
if(document.getElementById('nasabah.mns_ok_saran').value ==1){
	document.getElementById('nasabah.mns_ok_saran').checked = true;
}else 
document.getElementById('nasabah.mns_ok_saran').checked = false;



</script>

<%@ include file="/include/page/footer.jsp"%>