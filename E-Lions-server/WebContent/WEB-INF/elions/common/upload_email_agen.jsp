<%@ include file="/include/page/header.jsp"%>
<body onload="setupPanes('container1', 'tab1');">
	
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Upload Email Agen</a>
			</li>
		</ul>

		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form name="formpost" method="post" enctype="multipart/form-data">
					<table class="entry2">
						<tr>
							<th style="width: 200px;">Upload Email Agen<br><span class="info">* Max. 1 Mb (1024 Kb)</span></th>
							<td>
								<input type="file" name="file1" />
							</td>
						</tr>
						<tr>
							<th></th>
							<td>
								<input type="submit" name="upload" value="Upload" onclick="return confirm('Yakin update email agen bersangkutan? Harap pastikan data yang anda masukkan sudah benar.');">
								<c:choose>
									<c:when test="${not empty cmd.errorMessages}">
										<div id="error">
										ERROR:<br>
										<c:forEach var="error" items="${cmd.errorMessages}">
													- <c:out value="${error}" escapeXml="false" />
											<br />
										</c:forEach></div>
									</c:when>
									<c:otherwise>
										<div id="success" style="text-transform: none;">
											File harus bertipe EXCEL (.xls) dan terdiri dari 3 kolom saja : KODE_AGEN, NAMA_AGEN, EMAIL_AGEN.
										</div>
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
						<tr>
							<th></th>
							<td>
								<table class="displaytag" style="width: auto;">
									<tr><th>No.</th><th>Agen</th><th>Email Agen</th><th>Status Upload</th></tr>
									<c:forEach items="${cmd.daftarStatus }" var="a" varStatus="s">
										<tr>
											<td style="text-align: left;">${s.count}.</td>
											<td style="text-align: center;">${a.key}</td>
											<td style="text-align: left;">${a.value}</td>
											<td style="text-align: left;">
												<c:choose>
													<c:when test="${a.desc gt 0}">
														<span style="color: green;">SUKSES</span>
													</c:when>
													<c:otherwise>
														<span style="color: red;">GAGAL</span>
													</c:otherwise>
												</c:choose>
											</td>
										</tr>
									</c:forEach>
								</table>
							</td>
						</tr>
					</table>
					
				</form>
			</div>
		</div>
	</div>
	
</body>
<%@ include file="/include/page/footer.jsp"%>