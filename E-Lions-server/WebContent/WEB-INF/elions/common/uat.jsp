<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script>
	hideLoadingMessage();
</script>
</head>
<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">E-Lions</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" style="text-align: left; text-transform: none; font-size: 12px;">
				<br>
				<ul>
					<li>
						Dear All, untuk meminimalisasi kesalahan yang terjadi pada aplikasi, maka disediakan formulir : <br><br>
						<ul>
<!--							<li>-->
<!--								<STRONG>Permohonan penambahan pada program</STRONG>, -->
<!--								dapat diakses di menu <br><br>-->
<!--								<a href="${path }/include/form/form_permohonan.doc">HELP > DOWNLOAD FORM PERMINTAAN PROGRAM</a>-->
<!--								<br><br>-->
<!--							</li>-->
							<li>
								<STRONG>Uji kelayakan produk baru (new business)</STRONG>, 
								dapat diakses di <br><br>
								<a href="${path }/include/form/form_uat.doc">DOWNLOAD FORM UAT (USER ACCEPTANCE TEST)</a><br>
								<a href="${path }/include/form/form_permohonan.doc">DOWNLOAD FORM PERMOHONAN PERUBAHAN PROGRAM</a>
								<br><br>
							</li>
						</ul>
					</li>
					<li>
						- Untuk setiap produk baru, akan melalui proses UAT (uji kelayakan/kesalahan program) terlebih dahulu, <br>
						- Apabila belum melalui proses UAT, maka produk bersangkutan tidak dapat dicetak polisnya (menu <strong>Print Polis</strong>), <br>
						- Setelah proses UAT selesai dan disetujui oleh user, maka proses Print Polis untuk produk bersangkutan dapat dilakukan.
					
<!--						Dimana untuk selanjutnya, untuk setiap produk baru maupun perubahan/modifikasi program dapat langsung melengkapi form diatas kemudian menyerahkannya kepada EDP.-->
					</li>
				</ul>
				</form>
			</div>
			
		</div>
	</div>

	<c:if test="${not empty pepesan}">
		<script>alert('${pepesan}');</script>
	</c:if>
</body>
</html>