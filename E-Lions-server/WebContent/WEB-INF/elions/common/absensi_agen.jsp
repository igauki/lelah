<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<script type="text/javascript" src="${path}/include/js/jquery-1.3.2.min.js"></script>
<script>
	<c:if test="${not empty pesan}">alert('${pesan}')</c:if>
	
	$(document).ready(function() {
		$("#msag_id,#birth_date").keydown(function(e) {
			var key = e.charCode || e.keyCode || 0;
			//alert(key);
            // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
            return (key == 8 || 
	                key == 9 ||
	                key == 13 ||
	                key == 46 ||
	                (key >= 37 && key <= 40) ||
	                (key >= 48 && key <= 57) ||
	                (key >= 96 && key <= 105)
	                );
		});
		
	});	
</script>
<body onload="setupPanes('container1', 'tab1');">
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Absensi Agen</a>
			</li>
		</ul>
		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form name="formpost" method="post">
					<table class="entry2">
						<tr>
							<th width="200px" style="font-size: 20px">Kode Agen</th>
							<td>
								<input type="text" name="msag_id" id="msag_id" value="${msag_id}" size="10" style="font-size: 20px">
							</td>
						</tr>
						<tr>
							<th style="font-size: 20px">Tanggal Lahir</th>
							<td>
								<input type="text" name="birth_date" id="birth_date" value="${birth_date}" size="10" maxlength="8" style="font-size: 20px">
								<span class="info" style="font-size: 15px"> *ddmmyyyy</span>
							</td>
						</tr>
						<tr>
							<th>&nbsp;</th>
							<td><input type="submit" name="absen" value="absen" style="font-size: 20px"></td>
						</tr>
						<c:if test="${not empty error}">
							<tr>
								<td colspan="2">
									<div id="error" style="margin: 5px 5px 5px 5px;font-size: 15px">
										${error}
									</div>	
								</td>
							</tr>
						</c:if>
						<c:if test="${not empty absen}">
							<tr>
								<th>&nbsp;</th>
								<td>
									<fieldset>
										<legend style="font-size: 15px">Data Absensi (10 orang terakhir)</legend>
										<table class="displaytag" style="width: auto;">
											<tr>
												<th style="font-size: 15px">Kode Agen</th>
												<th style="font-size: 15px">Tanggal</th>
												<th style="font-size: 15px">Jam Masuk</th>
												<th style="font-size: 15px">Jam Keluar</th>
											</tr>
											<c:set var="hitung" value="0" />
											<c:forEach var="baris" items="${absen}" varStatus="st">
												<c:choose>
													<c:when test="${st.index % 2 ne 0}"><tr></c:when>
													<c:otherwise><tr style="background-color: #EBEBEB;"></c:otherwise>
												</c:choose>
													<td style="white-space: nowrap; text-align: center;font-size: 15px">${baris.msag_id}</td>												
													<td style="white-space: nowrap; text-align: center;font-size: 15px"><fmt:formatDate pattern="dd/MM/yyyy" value="${baris.tanggal}" /></td>
													<td style="white-space: nowrap; text-align: center;font-size: 15px"><fmt:formatDate pattern="HH:mm:ss" value="${baris.jam_masuk}" /></td>
													<td style="white-space: nowrap; text-align: center;font-size: 15px"><fmt:formatDate pattern="HH:mm:ss" value="${baris.jam_keluar}" /></td>
												</tr>
											</c:forEach>
										</table>
									</fieldset>
								</td>
							</tr>
						</c:if>
					</table>	
				</form>	
			</div>
		</div>	
	</div>	
</body>
<%@ include file="/include/page/footer.jsp"%>