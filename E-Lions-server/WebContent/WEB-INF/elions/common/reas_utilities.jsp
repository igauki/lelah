<%@ include file="/include/page/header.jsp"%>
<script>
	function awal(){
		setupPanes('container1', 'tab1');
		if('${msg}'!='')
			alert("${msg}");
	}
	
	function proses(btn){
		if(btn=='p'){
			window.location='${path}/common/util.htm?window=insert_reas';
		}else if(btn=='create' || btn=='insert' ){
			awal=formpost.tglAwal.value;
			akhir=formpost.tglAkhir.value;
			window.location='${path}/common/util.htm?window=reas_utilities&tglAwal='+awal+'&tglAkhir='+akhir+'&proses='+btn;
		}else if(btn=='create2'){
			awal2=formpost.tglAwal2.value;
			akhir2=formpost.tglAkhir2.value;
			window.location='${path}/common/util.htm?window=reas_utilities&tglAwal2='+awal2+'&tglAkhir2='+akhir2+'&proses='+btn;
		}else if(btn=='p2'){
			window.location='${path}/common/util.htm?window=insertLossMstReinsProduct';
		} 	
	}

</script>
<BODY onload="awal()" style="height: 100%;">

<form:form id="formpost" name="formpost" commandName="cmd">
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li><a href="#" onClick="return showPane('pane1', this)" id="tab1">Reas Utilities 1</a></li>
			<li><a href="#" onClick="return showPane('pane2', this)" id="tab2">Reas Utilities 2</a></li>
		</ul>
		<div class="tab-panes">
			<div id="pane1" class="panes">
				<table class="entry2">
					<tr>
						<td>Proses Untuk data link sebelum 14/01/2008</td>
					</tr>
					<tr>
						<th width="50%">Proses Insert Ke Tabel Eka.M_REAS_TEMP_NEW
													Tanggal : 
							<script>inputDate('tglAwal', '${tglAwal}', false, '', 9);</script> 
							s/d
							<script>inputDate('tglAkhir', '${tglAkhir}', false, '', 9);</script>
						
						</th>
						<td>
							<input type="button" name="btn_insert" value="Insert" onClick="proses('insert')"; />
						</td>
					</tr>
					<tr>
						<th>Laporan Bulanan Rider Link </th>
						<td>
							<input type="button" name="btn_create" value="Create Excell" onClick="proses('create'); "/>
						</td>
					</tr>
					<tr>
						<td>Proses Untuk Buat Reporting ke Excell berdasarkan tanggal prod_date </td>
					</tr>
					<tr>
						<th width="50%">Proses Insert Ke Tabel Eka.M_REAS_TEMP_NEW
													Tanggal : 
							<script>inputDate('tglAwal2', '${tglAwal2}', false, '', 9);</script> 
							s/d
							<script>inputDate('tglAkhir2', '${tglAkhir2}', false, '', 9);</script>
						
						</th>
						<td>
							<input type="button" name="btn_create2" value="Create Excell" onClick="proses('create2'); "/>
						</td>
					</tr>
					
				</table>
			</div>
			<div id="pane2" class="panes">
				<table class="entry2">
					<tr>
						<th width="50%">Proses Insert Rider Link Dari table EKA.M_REAS_TEMP_NEW ke EKA.MST_REINS dan REINS_PRODUCT
						</th>
						<td>
							<input type="button" name="btn_proses" value="Proses" onClick="proses('p')"; />
						</td>
					</tr>
					<tr>
						<th width="50%">Proses Insert Reins dan Reins Product dari query
						</th>
						<td>
							<input type="button" name="btn_proses2" value="Proses" onClick="proses('p2')"; />
						</td>
					</tr>
				</table>	
			</div>
		</div>	
	</div>		
</form:form>
</body>
<%@ include file="/include/page/footer.jsp"%>