<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path}/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path}/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script>
	hideLoadingMessage();
	
	function pesan(){
		var pesan = '${pesan}';
		if(pesan != '') alert(pesan);
	}
	
	function checkAll( value ){
		var jmlFile = document.getElementById('jmlFile').value;
		if( value == 1 ){
			for( i = 1 ; i <= jmlFile ; i ++ ){
				document.getElementById('checkBoxFile'+i).checked = 1 ;
			}
		}else{
			for( i = 1 ; i <= jmlFile ; i ++ ){
				document.getElementById('checkBoxFile'+i).checked = 0 ;
			}
		}
	}
</script>
</head>
<BODY onload="setupPanes('container1', 'tab1'); pesan();" style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Upload File</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" style="text-align: center;" enctype="multipart/form-data">
			
					<table class="entry2">
						<tr>
							<td class="left">
								<fieldset>
									<legend>Blanko</legend>
									Cari No.Blanko : &nbsp; 
									<font style="font: bold;"> ASM </font>
									<input type="text" name="no_blanko" value="${no_blanko}">&nbsp;<input type="submit" name="inputSpaj" value="Input SPAJ">
								<c:if test="${not empty dataBlanko }">
								<div id="test">
								</div>
									Masukan No.SPAJ : &nbsp; 
								<input type="text" name="no_spaj" value="${no_spaj}">&nbsp;<input type="submit" name="show" value="Show File">
						</c:if>
							<c:if test="${not empty errorMessage}">
										<div id="error">
											Pesan:<br>
											- ${errorMessage}
										</div>
									</c:if>
								</fieldset>
							</td>
						</tr>
	
						<c:if test="${not empty daftarFile }">
							<tr>
								<td>
									<fieldset>
										<legend>Daftar File</legend>
										<table border="1" cellpadding="1" cellspacing="1" style="font-size: 8pt;" width="40%" id="uploadTable">
										<thead>
											<tr>
												<th style="width: 20px;">No.</th>
												<th style="width: 450px;">Nama Dokumen</th>
												<th style="width: 10px;">Action</th>
											</tr>
										</thead>
										<tbody>
										<tr style="background-color: #D1D1D1;">
										<td colspan="2" align="center"> All </td>
										<td><input type="checkbox" id="all" name="all"  onclick="checkAll(this.checked);"/></td>
										</tr>
											<c:forEach var="i" varStatus="st" items="${daftarFile}" >
												<tr>
													<td>${st.count}.</td>
													<td align="left">
													${i.name}
													</td>
													<td> <input type="checkbox" id="checkBoxFile${st.count}" name="checkBoxFile${st.count}" value="${st.count}"/> </td>
												</tr>
											</c:forEach>
										</tbody>
										</table>
									</fieldset>
									<fieldset>
										<legend>UPLOAD</legend>
										<input type="hidden" id="jmlFile" value="${jmlFile}" name="jmlFile"/>
										<input type="submit" name="submit" value="Submit" onclick="return confirm('File-file yang dicentang akan diproses');">
									</fieldset>							
								</td>
							</tr>
						</c:if>
					</table>			

				</form>
			</div>
		</div>
	</div>

</form>
</body>
<%@ include file="/include/page/footer.jsp"%>