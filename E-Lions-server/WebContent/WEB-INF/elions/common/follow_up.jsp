<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>

<BODY style="height: 100%;" >
	<form:form id="formpost" name="formpost" commandName="cmd">
		<fieldset>
			<legend>Aktivitas</legend>
				<table class="entry2">
					<tr>
						<th align="left" width="20%">Tanggal</th>
						<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;"><fmt:formatDate value="${cmd.sysdate}" pattern="dd/MM/yyyy hh:mm:ss" /></td>
					</tr>
					<tr>
						<th align="left">Aktivitas</th>
						<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Follow Up</td>
					</tr>
					<tr>
						<th align="left">Keterangan</th>
						<td><form:textarea cssStyle="border-color:#6893B0;" path="aktivitas.keterangan" id="aktivitas" tabindex="6" cols="58" rows="3" onkeyup="textCounter(this, 100); " onkeydown="textCounter(this, 100);" />
							<span class="info" style="vertical-align: top;">*</span>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<span class="info">* Max 100 karakter</span>
						</td>
					</tr>
				</table>
		</fieldset>
		<table class="entry2">
			<tr>
				<th>
					<input type="submit" name="save" id="save" value="Save"/>
					<input type="button" name="close" id="close" value="Close" onclick="window.close();"/>
				</th>
			</tr>
		</table>
		<table>
				<tr>
					<td colspan="4">
						<c:if test="${submitSuccess eq true}">
									<script>
										alert("Berhasil Simpan");
										window.close();
									</script>
				        </c:if>
					 </td>
				</tr>
		</table>
	</form:form>
</BODY>

<%@ include file="/include/page/footer.jsp"%>