<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript">
	var flag_sukses = '${flag_sukses}';
	if(flag_sukses==1){//1 untuk sukses speedy
		//alert('Proses speedy berhasil.');
		alert('${successMessage}'+' '+'${lsError}');
		parent.location.href='${path}/uw/uw.htm?window=main&posisi_uw=27';
	}else if(flag_sukses==2){//2 untuk sukses transfer dari uw helpdesk ke uw
		alert('${successMessage}');
		parent.location.href='${path}/uw/uw.htm?window=main&posisi_uw=209';
	}else if(flag_sukses==3){//2 untuk sukses transfer dari bas qa 1 ke bas qa 2
		alert('${successMessage}');
		parent.location.href='${path}/report/bas.htm?window=qa_1';
	}else if(flag_sukses==4){//2 untuk sukses transfer dari bas qa 1 ke bas qa 2
		alert('${successMessage}');
		parent.location.href='${path}/report/bas.htm?window=qa_2';
	}else if(flag_sukses==5){//5 Untuk Sukses Collection
		alert('${successMessage}');
		parent.location.href='${path}/uw/uw.htm?window=collection';
	}else if(flag_sukses==6){//6 Untuk Proses
		alert('${successMessage}'+' '+'${lsError}');
		parent.location.href='${path}/uw/uw.htm?window=main&posisi_uw=2';
	}
	
	function awal(){
		document.getElementById("load").style.display = "none"; 
	}
</script>
<form name="framepost"  method="post">
<body onload="awal();">
	<table class="entry" width="60%">
	<div id="load">
					<img src="${path}/include/image/loading.gif" alt="loading" /> <br />
					<br />
					<h3>Loading Please Wait....</h3>
				</div>
		<tr>
			 <td colspan="4">
			 		<c:if test="${not empty successMessage }">
			        	<div id="success">
				        	Success:<br>
											- <c:out value="${successMessage}" escapeXml="false" />
									<br/>
			        	</div>
			        </c:if>	
					<c:if test="${not empty lsError}">
						<div id="error">
							Informasi:<br>
											- <c:out value="${lsError}" escapeXml="false" />
									<br/>
						</div>
					</c:if>									
			</td>
		</tr>
	</table>
</body>                                                                                   
</form>  
<%@ include file="/include/page/footer.jsp"%>