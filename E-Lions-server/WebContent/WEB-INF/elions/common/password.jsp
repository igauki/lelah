<%@ include file="/include/page/header.jsp"%>
<script>
	function cek(pw, inp){
		if(pw.value.toUpperCase()==inp.value.toUpperCase()){
			if('${param.caller}'=='viewer'){
				window.returnValue = true;
			}
		}else{
			window.returnValue = false;
		} 
		window.close();
	}
</script>
<body style="height: 100%;">
	<fieldset>
		<legend>Password</legend>
		<table class="entry">
			<tr>
				<th>Masukkan Password</th>
				<td>
					<input type="hidden" value="${p_w }" name="p_w">
					<input type="hidden" name="tipe" value="${param.tipe }">
					<input name="password" type=password size=30 value="" onkeydown="if(window.event.keyCode == 13)cek(document.getElementById('p_w'),document.getElementById('password'));">
				</td>
			</tr> 
			<tr>
				<th></th>
				<td>
					<input type="button" name="verify" value="Verify!" onclick="cek(document.getElementById('p_w'),document.getElementById('password'));">
					<input type="button" name="close" value="Close" onclick="window.close();">
				</td>
			</tr>
		</table>
	</fieldset>
</body>
<%@ include file="/include/page/footer.jsp"%>
