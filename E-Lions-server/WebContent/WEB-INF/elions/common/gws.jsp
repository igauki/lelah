<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<head>
	<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta NAME="Description" CONTENT="EkaLife">
	<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
	<script type="text/javascript" src="${path}/include/js/default.js"></script>
	<script type="text/javascript" src="${path }/dwr/util.js"></script>	
	<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
	<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
	<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
	<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>
	<script>
	    hideLoadingMessage();
	    
	    function ayo(pencetan, perintah){
	        dok = document.formpost;
	        if('cari' == perintah){
	            mcl_id = dok.elements['personal.mcl_id'].value;
	            mcl_jenis = dok.elements['personal.mcl_jenis'].value;
	            popWin('${path}/common/menu.htm?frame=cariprofile&mcl_id='+mcl_id+'&mcl_jenis='+mcl_jenis, 400, 500);
	        }else if('tampil' == perintah){
	            document.formpost.submit();
	        }else if('simpan'== perintah){
	            document.formpost.s.value='1';
	            alert("ok");
	            document.formpost.submit();
	        }
	        return false;
	    }
	    
	    function ganti(i){
	        formpost.flag_ut.value = i;
	    }
	    
	    function pilihGws() {
	       document.formpost.pilih_gws.value = 'true';
	       document.formpost.show.value = 'true';
	       document.formpost.submit();
	    }
	</script>
</head>

<body style="height: 100%;" onload="setupPanes('container1', 'tab1');">
	<form:form id="formpost" name="formpost" commandName="cmd">
		<input type="hidden" name="show" value="">
		<form:hidden path="personal.mcl_jenis"/>
		<table class="entry2" style="width: auto;">
			<tr>
				<td>
					<fieldset>
						<legend>Data Perusahaan</legend>
						<table class="entry2">
							<tr>
								<th nowrap="nowrap" style="width: 120px;">Nama</th>
								<td colspan="5">
									<form:input path="personal.mcl_id" cssClass="readOnly" readonly="readonly" size="16" />
									<form:select path="personal.mcl_gelar">
										<form:option label="" value=""/>
										<form:options items="${gelar}" itemLabel="value" itemValue="value" />
									</form:select> 
									<form:input path="personal.mcl_first" size="60"/>
								</td>
							</tr>
							<tr>
								<th nowrap="nowrap">Jenis Usaha</th>
								<td>
									<form:select path="personal.lju_id" cssStyle="width: 220px;">
										<form:option value=""/>
										<form:options items="${jenisUsaha}" itemLabel="value" itemValue="key" />
									</form:select> 
								</td>
								<th nowrap="nowrap" style="width: 150px;">No. NPWP</th>
								<td colspan="3">
									<form:input path="personal.mpt_npwp"/>
								</td>
							</tr>
							<tr>
								<th nowrap="nowrap" rowspan="2">Alamat</th>
								<td rowspan="2">
									<form:textarea path="personal.address.alamat_kantor" cols="40" rows="3" 
										onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "/>
								</td>
								<th nowrap="nowrap">Kode Pos</th>
								<td style="width: 150px;">
									<form:input path="personal.address.kd_pos_kantor" />
								</td>
								<th nowrap="nowrap" style="width: 150px;">Kota</th>
								<td>
									<form:input path="personal.address.kota_kantor" />
								</td>
							</tr>
							<tr>
								<th nowrap="nowrap">Telp 1</th>
								<td>
									<form:input path="personal.address.telpon_kantor" />
								</td>
								<th nowrap="nowrap">Telp 2</th>
								<td>
									<form:input path="personal.address.telpon_kantor2" />
								</td>
							</tr>
						</table>
					</fieldset>
					<fieldset id="payrollData" >
						<legend>Data Personel Pengurus Administrasi Asuransi</legend>
						<table class="entry2" style="width: auto;">
                            <tr>
                                <th nowrap="nowrap" style="width: 120px;">Nama Lengkap </th>
                                <td colspan="2">
                                    <form:input path="personal.lsContactPerson[0].nama_lengkap"/>
                                </td>
                            </tr>
                            <tr>
                                <th nowrap="nowrap">Telepon Kantor</th>
                                <td colspan="3">
                                    <form:input path="personal.lsContactPerson[0].telp_kantor"/>
                                </td>
                            </tr>
                            <tr>
                                <th nowrap="nowrap">No. HP</th>
                                <td colspan="3">
                                    <form:input path="personal.lsContactPerson[0].telp_hp"/>
                                </td>
                            </tr>
                            <tr>
                                <th nowrap="nowrap">Alamat Email</th>
                                <td colspan="3">
                                    <form:input path="personal.lsContactPerson[0].email"/>
                                </td>
                            </tr>
                        </table>	
					</fieldset>
					<fieldset>
					    <legend>Jenis Kompensasi</legend>
					    <input type="hidden" name="pilih_gws" value="">
					    <table class="entry2" style="width: auto;">
					        <tr>
					            <c:forEach items="${options}" var="opt">
					                <th nowrap="nowrap">
	                                    <form:radiobutton path="personal.flag_gws" value="${opt.key}" onclick="pilihGws();" cssClass="noBorder"/>
	                                </th>
	                                <td style="width: 70px;">
	                                    ${opt.value}
	                                </td>
					            </c:forEach>
					        </tr>
					    </table>
					</fieldset>
					<c:if test="${cmd.row > 1 && cmd.personal.flag_gws > 0}">
					    <fieldset>
					        <legend>Data Personel Pengurus Administrasi Asuransi Setiap Layer</legend>
					        <div class="tab-container" id="container1">
	                            <ul class="tabs">
	                                <c:forEach items="${cmd.personal.lsContactPerson}" var="x" varStatus="xt">
	                                    <c:if test="${xt.index > 0}">
	                                        <li>
	                                            <a href="#" onClick="return showPane('pane${xt.index}', this)"  
	                                            onmouseover="return overlib('Alt-${xt.index}', AUTOSTATUS, WRAP);" onmouseout="nd();"
	                                            onfocus="return showPane('pane${xt.index}', this)" accesskey="${xt.index}" id="tab${xt.index}">
	                                                Layer ${xt.index} 
	                                                <c:choose>
	                                                    <c:when test="${xt.index eq 1 && cmd.personal.flag_gws eq 1}">
	                                                        (2%)
	                                                    </c:when>
	                                                    <c:when test="${xt.index eq 2 && cmd.personal.flag_gws eq 1}">
	                                                        (3%)
	                                                    </c:when>
	                                                    <c:otherwise>
	                                                        (5%)
	                                                    </c:otherwise>
	                                                </c:choose>
	                                            </a>
	                                        </li>
	                                    </c:if>
	                                </c:forEach>    
	                            </ul>
	                            <div class="tab-panes">
	                                <c:forEach items="${cmd.personal.lsContactPerson}" var="x" varStatus="xt">
	                                    <c:if test="${xt.index > 0}">
	                                        <div id="pane${xt.index}" class="panes">
	                                            <table class="entry2" style="width: auto;">
	                                                <tr>
	                                                    <th nowrap="nowrap" style="width: 120px;">Under Table</th>
	                                                    <td>
		                                                    <input type="radio" name="ut" onClick="ganti(${xt.index+1});"
		                                                    onmouseover="return overlib('Penerima Kompensasi Under Table', AUTOSTATUS, WRAP);" onmouseout="nd();"
		                                                    <c:if test="${cmd.flag_ut eq xt.index+1}">checked</c:if>>
		                                                </td>
	                                                </tr>
	                                                <tr>
	                                                    <th nowrap="nowrap">Jenis Badan</th>
	                                                    <td>
	                                                    <form:radiobutton path="personal.lsContactPerson[${xt.index }].jenis_badan" value="1" title="Individu" id="jenis_badan" cssClass="noBorder"/> Individu
	                                                    </td>
	                                                    <td>
	                                                        <form:radiobutton path="personal.lsContactPerson[${xt.index }].jenis_badan" value="2" title="Perusahaan" id="jenis_badan" cssClass="noBorder"/>  Perusahaan
	                                                    </td>
	                                                </tr>
	                                                <tr>
	                                                    <th nowrap="nowrap">NPWP</th>
	                                                    <td colspan="3">
	                                                        <form:input path="personal.lsContactPerson[${xt.index }].npwp"/>
	                                                    </td>
	                                                </tr>
	                                                <tr>
	                                                    <th nowrap="nowrap" rowspan="5">Cara Bayar</th>
	                                                    <td rowspan="5">
	                                                        <input type="hidden" name="personal.lsContactPerson[${xt.index}].flag_komisi" value="1">
	                                                        <input type="hidden" name="personal.lsContactPerson[${xt.index}].cara_bayar" value="0">
	                                                        <form:label path="personal.lsContactPerson[${xt.index }].cara_bayar" for="cara_bayar" >Transfer Bank</form:label>
	                                                    </td>
	                                                    <th nowrap="nowrap">Nomor Rekening</th>
	                                                    <td>
	                                                        <form:input path="personal.lsContactPerson[${xt.index }].rek_no" id="rek_no${xt.index }"/>
	                                                    </td>
	                                                </tr>
	                                                <tr>
	                                                    <th nowrap="nowrap">Atas Nama</th>
	                                                    <td>
	                                                        <form:input path="personal.lsContactPerson[${xt.index }].rek_nama" id="rek_nama${xt.index }"/>
	                                                    </td>
	                                                </tr>
	                                                <tr>
	                                                    <th nowrap="nowrap">Nama Bank</th>
	                                                    <td>
	                                                        <form:input path="personal.lsContactPerson[${xt.index }].rek_bank" id="rek_bank${xt.index }"/>
	                                                    </td>
	                                                </tr>
	                                                <tr>
	                                                    <th nowrap="nowrap">Cabang</th>
	                                                    <td>
	                                                        <form:input path="personal.lsContactPerson[${xt.index }].rek_bank_cabang" id="rek_bank_cabang${xt.index }"/>
	                                                    </td>
	                                                </tr>
	                                                <tr>
	                                                    <th nowrap="nowrap">Kota</th>
	                                                    <td>
	                                                        <form:input path="personal.lsContactPerson[${xt.index }].rek_bank_kota" id="rek_bank_kota${xt.index }"/>
	                                                    </td>
	                                                </tr>
	                                            </table>
	                                        </div>
	                                    </c:if>
	                                </c:forEach>
	                            </div>
	                        </div>
					    </fieldset>
					</c:if>
				</td>
			</tr>
			<tr>
				 <td colspan="3">
					<c:if test="${submitSuccess eq true}">
			        	<div id="success">
				        	 Berhasil
			        	</div>
			        </c:if>	
		  			<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
								 Informasi:<br>
									<c:forEach var="error" items="${status.errorMessages}">
												 - <c:out value="${error}" escapeXml="false" />
										<br/>
									</c:forEach>
							</div>
						</c:if>									
					</spring:bind>
				</td>
			</tr>
			<tr>
				<td>
					<input type="button" name="cari" value="Cari (Edit)" onclick="ayo(this,'cari');">
					<input type="button" name="simpan" value="Simpan" onclick="this.disabled=true; ayo(this,'simpan');"
					<c:if test="${submitSuccess eq true}">
						disabled
					</c:if>
					>
				</td>
				<form:hidden path="flag_ut"/>
				<input type="hidden" name="s" value="0"/>
			</tr>
		</table>
	</form:form>

</body>
</html>