<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>

<BODY style="height: 100%;" >
	<form:form id="formpost" name="formpost" commandName="cmd">
		<fieldset>
			<legend>Input SPAJ</legend>
				<table class="entry2">
					<tr>
						<th align="left" width="20%">Kode Nasabah</th>
						<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
							${cmd.nasabah.mns_kd_nasabah}
						</td>
					</tr>
					<tr>
						<th align="left" width="20%">Jenis Referral</th>
						<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
							${cmd.nasabah.nm_jenis}
						</td>
					</tr>
					<tr>
						<th align="left">Reg Spaj</th>
						<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
							<c:choose>
								<c:when test="${cmd.count ne 1}">
									<form:input cssStyle="border-color:#6893B0;" path="reg_spaj" size="30"  />
									<input type="submit" name="cekspaj" id="cekspaj" value="Check SPAJ yang diinput">
								</c:when>
								<c:otherwise>
									${cmd.reg_spaj}
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
					<c:if test="${cmd.count eq 1 or cmd.nama_produk ne null}">
						<tr>
							<th align="left">Nama Produk</th>
							<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
								${cmd.nama_produk}
							</td>
						</tr>
					</c:if>
				</table>
		</fieldset>
		<table class="entry2">
			<tr>
				<th>
					<input type="submit" name="save" id="save" value="Save" <c:if test="${cmd.count eq 1}">disabled="disabled"</c:if>  />
				</th>
			</tr>
		</table>
		<table>
				<tr>
					<td colspan="4">
						<c:if test="${submitSuccess eq true}">
							<script>
								alert("Berhasil Simpan");
								window.close();
								//window.location='${path }/common/input_referral.htm?nomor=${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}&flag=0';
							</script>
				        </c:if>
				        <spring:bind path="cmd.*">
							<c:if test="${not empty status.errorMessages}">
								<div id="error">
									 Informasi:<br>
										<c:forEach var="error" items="${status.errorMessages}">
													 - <c:out value="${error}" escapeXml="false" />
											<br/>
										</c:forEach>
								</div>
							</c:if>									
						</spring:bind>
					 </td>
				</tr>
		</table>
	</form:form>
</BODY>

<%@ include file="/include/page/footer.jsp"%>