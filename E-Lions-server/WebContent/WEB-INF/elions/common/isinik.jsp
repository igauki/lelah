<%@ include file="/include/page/header.jsp"%>
<style type="text/css">
	html,body{
	     overflow: hidden;
	}
</style>
<script>
	function cek(pw, inp){
		if(pw.value.toUpperCase()==inp.value.toUpperCase()){
			if('${param.caller}'=='viewer'){
				window.returnValue = true;
			}
		}else{
			window.returnValue = false;
		}
		window.close();
	}
	
	function tes(form){
	 
	    var nik=document.formpost.nik.value;
	    var confn=document.formpost.confirmnik.value;	
	    var x = '';
	    	var nama = '';
	    	var dept = '';
	    	 	   
	    if(nik!=confn)	  {
		    alert("NIK yang dinput tidak sama dengan Confirm NIK");
		    return false;
	    }else if ((nik==null && confn==null) || (nik=='' && confn=='')){
		    alert("Harap Isi Data Dengan Lengkap");
		    return false;
	   
      }else{
	    ajaxManager.nik(nik,
	    		{callback:function(map) {
			       
					DWRUtil.useLoadingMessage();
					x=map.nik2;
					 nama=map.nama;
					dept=map.dept;
					if(x=='s'){
						alert("DATA NIK YANG ANDA INPUT TIDAK TERDAFTAR SEBAGAI KARYAWAN");
						return false;
					}else{
						var pesan=confirm("Apakah NIK yang ada masukan sudah benar:\n"+"1.NIK		: "+x +"\n"+"2. Nama Karyawan	: " +nama+"\n"+"3. Departemen	: "+dept);
						if (pesan==true){
							
							document.formpost.submit(true);
						}else{
							return false;
						}
					} 
					
				   },
				  timeout:15000,
				  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
			});
			
			}
	   
	}
	
</script>
<body style="height: 100%;" onload="setupPanes('container1', 'tab1'); ">

	<div class="tab-container" id="container1">
		
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">INPUT NIK KARYAWAN</a>
			</li>
		</ul>

		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form method="post" name="formpost" onsubmit="return tes(this);">
					<table class="entry2"> 
						<tr>
							<th style="width: 150px;">User</th>
							<td>
								<input name="username" type=text value="${sessionScope.currentUser.name}" size="30" disabled>
								<input name="username" type=text value="${sessionScope.currentUser.lus_id}" size="5" disabled>
							</td>
						</tr>
						<tr>
							<th>NIK</th>
							<td>
								<input name="nik" type="text" size="38" value="${nik }">							
								
							</td>
						</tr>
						
						<tr>
							<th>Confirm NIK</th>
							<td>
								<input name="confirmnik" type="text" size="38" value="${confn }">
							</td>
						</tr>
						<tr>
							<th></th>
							<td>
								<c:if test="${not empty err}">
									<div id="error">INFO:<br>
										<c:forEach var="error" items="${err}">
									 <c:out value="${error}" escapeXml="false" />
											<br />
										</c:forEach>
									</div>				
								</c:if>
								<c:if test="${not empty succ}">
									<div id="success">${succ}</div>	
									<script> alert("NIK berhasil diinput");
									
									parent.window.location='${path}/';
									</script>			
								</c:if>
								<input type="hidden" name="save" value="save">
								<input type="button" name="btnSave" value="Save" onclick="tes(this)"
									accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
							</td>
						</tr>
					</table>
				</form>			
			</div>
		</div>
		
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>