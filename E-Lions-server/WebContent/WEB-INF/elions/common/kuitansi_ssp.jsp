<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		
		var pesan = '${pesan}';
		if(pesan!=null && pesan !=''){
			alert(pesan);
		}
	});
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

</style>

<body>

<form id="formPost" name="formPost" method="post">
<fieldset class="ui-widget ui-widget-content">
	<legend class="ui-widget-header ui-corner-all"><div>Kuitansi SSP</div></legend>
	
	<div class="rowElem">
		<label>Cabang :</label>
		<select name="cabang" id="cabang" title="Silahkan pilih Cabang Bank Sinarmas">
			<c:forEach var="c" items="${cabang}" varStatus="s">
				<option value="${c.key}" <c:if test="${c.key eq cab }">selected="selected"</c:if>>${c.value}</option>
			</c:forEach>
		</select>
	</div>
	
	<div class="rowElem">
		<label>Bulan :</label>
		<input name="bulan" id="bulan" type="text" class="datepicker" title="Bulan" value="${tgl }">
	</div>
	
	<div class="rowElem">
		<label>Dokumen :</label>
		<c:if test="${not empty dokumen}">
			<div style="width: 100%;margin-left: 156px;margin-top: -20px;padding: 5px 0 5px 0">
			<c:forEach items="${dokumen }" var="d">
				<a href="${path}/common/util.htm?window=kuitansi_ssp&file=${d.dok}&cab=${cab}" target="_blank">${d.dok}</a><br>
			</c:forEach>
			</div>
		</c:if>
	</div>
	
	<div class="rowElem">
		<label></label>
		<input type="submit" name="search" id="seacrh" value="Search">
</fieldset>
</form>

</body>
</html>

<%-- <%@ include file="/include/page/header.jsp"%>
<body style="height: 100%;" onload="setupPanes('container1', 'tab1'); ">
	<div class="tab-container" id="container1">
		
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Daftar Dokumen Lanjutan ${folder} SPAJ ${spaj} (Diluar New Business)</a>
			</li>
		</ul>

		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form method="post" name="formpost">
					<table class="entry2">
						<tr>
							<th>SPAJ/No Polis</th>
							<td><input type="text" name="spaj" value="${spaj}"><input type="submit" value="Tampilkan"></td>
						</tr>
						<tr>
							<th>Surat Rollover / Top-up</th>
							<td>
								<c:if test="${not empty pesan}">
									<div id="error">${pesan}</div>
								</c:if>
								<table class="displaytag" style="width: auto;">
									<tr>
										<th>Dokumen</th>
										<th>Periode</th>
										<th>Tanggal Print</th>
										<th>Jenis Rollover</th>
										<th>Rate</th>
										<th>Deposit</th>
										<th>Interest</th>
										<th>Filename</th>
									</tr>
									<c:forEach var="dok" items="${daftarFile}">
										<c:if test="${not empty dok.fileName}">
											<tr>
												<td style="text-align: left;"><a href="${path}/common/util.htm?window=dokumen_ro&spaj=${spaj}&file=${dok.fileName}" target="_blank">${dok.msl_desc}</a></td>
												<td style="text-align: center;"><fmt:formatDate pattern="dd/MM/yyyy" value="${dok.mps_deposit_date}" /><fmt:formatDate pattern="dd/MM/yyyy" value="${dok.msl_bdate}" /> - <fmt:formatDate pattern="dd/MM/yyyy" value="${dok.mpr_mature_date}" /><fmt:formatDate pattern="dd/MM/yyyy" value="${dok.msl_edate}" /></td>
												<td style="text-align: center;">${dok.fileCreated}</td>
												<td style="text-align: center;">
													<c:choose>
														<c:when test="${dok.mpr_jns_ro eq 1}">ALL</c:when>
														<c:when test="${dok.mpr_jns_ro eq 2}">PREMI</c:when>
														<c:when test="${dok.msl_ro eq 1}">ALL</c:when>
														<c:when test="${dok.msl_ro eq 2}">PREMI</c:when>
														<c:otherwise>-</c:otherwise>
													</c:choose>
												</td>
												<td style="text-align: right;"><fmt:formatNumber value="${dok.mpr_rate}"/><fmt:formatNumber value="${dok.msl_rate}"/>%</td>
												<td style="text-align: right;"><fmt:formatNumber value="${dok.mpr_deposit}"/><fmt:formatNumber value="${dok.msl_premi}"/></td>
												<td style="text-align: right;"><fmt:formatNumber value="${dok.mpr_interest}"/><fmt:formatNumber value="${dok.msl_bunga}"/></td>
												<td style="text-align: left;">${dok.fileName}</td>
											</tr>
										</c:if>
									</c:forEach>
								</table>
							</td>
						</tr>
						<tr>
							<th>Surat Endorsement</th>
							<td>
								<table class="displaytag" style="width: auto;">
									<tr>
										<th>Dokumen</th>
									</tr>
									<c:forEach var="dok" items="${daftarFile2}">
										<tr>
											<td style="text-align: left;"><a href="${path}/common/util.htm?endors=1&window=dokumen_ro&spaj=${spaj}&file=${dok.key}" target="_blank">${dok.key}</a></td>
										</tr>
									</c:forEach>
								</table>
							</td>
						</tr>
					</table>
				</form>			
			</div>
		</div>
		
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%> --%>