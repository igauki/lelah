<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head> 
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/jquery.checkboxtree.css" media="screen">
<!--		<script type="text/javascript" src="${path}/include/js/disableKeys.js"></script>-->
		<script type="text/javascript" src="${path }/dwr/util.js"></script>
		<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
		<script type="text/javascript" src="${path}/include/js/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="${path}/include/js/jquery.checkboxtree.js"></script>
		<script>hideLoadingMessage();</script>
		<style>
			.link{
				font-size: 8pt;
				text-decoration: none;
				text-transform: none;
			}
		</style>
		<script>
			function update_email(){
				var email = prompt("Silahkan masukkan alamat email anda: ", "${sessionScope.currentUser.email}");
				if(email){
					window.location = '${path}/common/menu.htm?frame=treemenu&new_email=' + email;
				}
			}
		</script>
		<script>
$(document).ready(function() {
	if ($('#imageTree').length != 0) {
		$('#imageTree').checkboxTree();		
	}
	
	//Deddy - 13 Jul 2017
	//Ditutup dulu karena lama prosesnya.
// 	<c:if test="${sessionScope.currentUser.jn_bank eq 2}">
// 		popWin('${path}/uw/uw.htm?window=top_seller', 600, 800);
// 	</c:if>
	<c:if test="${sessionScope.currentUser.jn_bank ne 2 && sessionScope.currentUser.lus_nik eq null && sessionScope.currentUser.jn_bank ne 3 && sessionScope.currentUser.jn_bank ne 16 }">
		window.location.href='${path}/common/menu.htm?frame=isinik&warning=1';
	</c:if>

	//Anta
	//Untuk user MallAssurance, Yune, Ningrum, Apriyani S, Inge, dan Sari Sutini
	<c:if test="${sessionScope.currentUser.jn_bank eq 4 || sessionScope.currentUser.lus_id eq 5 || sessionScope.currentUser.lus_id eq 495 || sessionScope.currentUser.lus_id eq 651 || sessionScope.currentUser.lus_id eq 687 || sessionScope.currentUser.lus_id eq 1569}">
		popWin('${path}/uw/uw.htm?window=spaj_mall_untransf', 600, 800);
	</c:if>
	
});
		</script>
	</head>
<body style="height: 100%;" onload='setupPanes("container1", "tab1");'>
<div class="tab-container" id="container1">
	<ul class="tabs">
		<li><a href="#" onClick="showPane('pane1', this)" id="tab1">Menu List</a></li>
		<c:choose>
			<c:when test="${sessionScope.currentUser.jn_bank eq 2}">
				<li><a href="#" onClick="showPane('pane2', this)" id="tab1">List Pending</a></li>
			</c:when>
			<c:otherwise>
				<li><a href="#" onClick="showPane('pane2', this)" id="tab1">DOWNLOAD</a></li>
			</c:otherwise>
		</c:choose>
		<c:if test="${sessionScope.currentUser.lde_id eq 19}">
		<li><a href="#" onClick="showPane('pane3', this)" id="tab1">No.Ext Pusat</a></li>
		</c:if>
	</ul>
	<div class="tab-panes">
	
		<div id="pane1" class="panes" style="text-align: left;">
			
			<table class="entry2">
				<tr>
					<td>
						<c:forEach var="baris" items="${cmd.allMenu}">
							<c:if test="${baris.level lt tingkat}">
								<c:forEach var="hit" begin="1" end="${tingkat - baris.level}" step="1">
									</ul></li>
								</c:forEach>
							</c:if>
							<c:choose>
								<c:when test="${baris.level eq 1}"><ul><li><strong class="link">${baris.nama_menu}</strong><ul></c:when>
								<c:when test="${empty baris.link_menu}"><li><strong class="link">${baris.nama_menu}</strong><ul></c:when>
								<c:when test="${not empty baris.link_menu}"><li><a href="${path}/${baris.link_menu}" class="link">${baris.nama_menu}</a></li></c:when>
								<c:otherwise><ul><li>${baris.nama_menu}</li></c:otherwise>
							</c:choose>
							<c:set var="tingkat" value="${baris.level}"/>
						</c:forEach>
					</td>
					<th style="vertical-align: top;">
						<c:if test="${not empty cmd.daftarUpdateProposal}">
							<fieldset>
								<legend><h4 style="color: red;">Informasi Update Proposal & Absensi Terbaru</h3></legend>
								<table class="displaytag" style="width: auto;">
									<tr>
										<th>File Name</th>
										<th>Description</th>
										<th>Last Modified</th>
									</tr>
									<c:forEach var="dok" items="${cmd.daftarUpdateProposal}">
										<tr>
											<td style="text-align: left;"><a href="${path}/update_proposal/update.htm?window=proposal&download=${dok.key}">${dok.key}</a></td>
											<td style="text-align: left;">${dok.desc}</td>
											<td style="text-align: left;">${dok.value}</td>
										</tr>
									</c:forEach>
								</table>
								
							</fieldset>
						</c:if>
						<c:if test="${not empty cmd.messageOfTheDay}">
							<fieldset>
								<legend><h4 style="color: red;">Message of the Day</h3></legend>
								<table>
									<tr>
										<td>${cmd.messageOfTheDay.MSMD_MESSAGE}</td>
									</tr>
								</table>
							</fieldset>
						</c:if>
						<c:if test="${not empty cmd.closing}">
							<fieldset>
							<legend>Informasi Tanggal Penting</legend>
							<table>
								<c:forEach items="${cmd.closing}" var="c">
									<tr>
										<td><span style="font: small-caps sans-serif; color: ${c.COLOR}; font-size: 20px;">${c.MSDEF_CHAR}</span></td>
										<td> : </td>
										<td>
											<span style="font: small-caps sans-serif; color: ${c.COLOR}; font-size: 20px;">
												<%-- <fmt:formatDate value="${c.MSDEF_DATE}" pattern="dd MMM yyyy"/> --%>
												${c.UCUP}
											</span>
										</td>
									</tr>
								</c:forEach>
							</table>
							<fieldset>
								<legend>Mohon Diperhatikan!!!</legend>
								<ol style="text-align: left; margin: 5 20 5 20; font-size: 12px">
									<c:forEach items="${cmd.closing}" var="c">
										<c:if test="${c.KET ne '-'}"><li>${c.KET}</li></c:if>
									</c:forEach>
								</ol>
							</fieldset>
							</fieldset>
						</c:if>
						<c:if test="${not empty cmd.infoDetailUser}">
							<fieldset>
							<legend>Informasi User</legend>
							<table>
								<tr>
									<td>User</td>
									<td>: [${cmd.infoDetailUser.LUS_ID}] ${cmd.infoDetailUser.LUS_LOGIN_NAME}</td>
								</tr>
								<tr>
									<td>Nama</td>
									<td>: ${cmd.infoDetailUser.LUS_FULL_NAME}</td>
								</tr>
								<tr>
									<td>Cabang</td>
									<td>: ${cmd.infoDetailUser.LCA_NAMA}</td>
								</tr>
								<tr>
									<td>Departemen</td>
									<td>: ${cmd.infoDetailUser.LDE_DEPT}</td>
								</tr>
								<tr>
									<td>E-mail</td>
									<td>: ${sessionScope.currentUser.email} <a href="#" style="text-transform: none; color: blue;" onclick="update_email();">Update</a></td>
								</tr>
								<c:if test="${cmd.infoDetailUser.JN_BANK eq 2 || cmd.infoDetailUser.JN_BANK eq 16}">
									<tr>
										<td>User Valid 1</td>
										<td>: [${cmd.infoDetailUser.LUS_ID_1}] ${cmd.infoDetailUser.LUS_LOGIN_NAME_1}</td>
									</tr>
									<tr>
										<td>User Valid 2</td>
										<td>: [${cmd.infoDetailUser.LUS_ID_2}] ${cmd.infoDetailUser.LUS_LOGIN_NAME_2}</td>
									</tr>
									<tr>
										<td>Cabang BSM/SMS</td>
										<td>: [${cmd.infoDetailUser.LCB_NO}-${cmd.infoDetailUser.KODE}] ${cmd.infoDetailUser.NAMA_CABANG}</td>
									</tr>
									<tr>
										<td>Email Cabang BSM/SMS</td>
										<td>: ${cmd.infoDetailUser.EMAIL_CAB}</td>
									</tr>
									<tr>
										<td>List Top Seller BSIM</td>
										<td>: <a href="#" style="text-transform: none; color: blue;" onclick="popWin('${path}/uw/uw.htm?window=top_seller', 600, 800); return false;">View</a></td>
									</tr>
								</c:if>
							</table>
							</fieldset>
						</c:if>
						 <c:if test="${(sessionScope.currentUser.lus_id eq '590' || sessionScope.currentUser.lus_id eq '687' ||sessionScope.currentUser.lus_id eq '1161' ||sessionScope.currentUser.lus_id eq '2071')}">
							<fieldset>
								<legend>Perusahaan Peserta Worksite Yang Harus Di Kirimkan List Peserta: </legend>
									<c:forEach items="${cmd.reimender}" var="x" varStatus="xt">
									<table>
									    <tr>
									    	<td class="center"><span style="font: small-caps sans-serif; color: ${x.color}; font-size: 16px;">${x.mcl_first}</span></td>
<!--									    	<td> : </td>-->
											<td>
											<span style="font: small-caps sans-serif; color: ${x.color}; font-size: 16px;">
												<%-- <fmt:formatDate value="${c.MSDEF_DATE}" pattern="dd MMM yyyy"/> --%>
												
											</span>
										</td>
									    </tr>
									</table>
									
									</c:forEach>
							</fieldset>
						</c:if>
						<c:if test="${not empty cmd.summaryNB}">
							<table class="displaytag">
								<caption>Posisi Dokumen</caption>
								<thead>
									<th>Distribusi</th>
									<td>Input</td>
									<td>U/W</td>
									<td>Payment</td>
									<td>Print</td>
									<td>TTP</td>
									<td>Cek TTP</td>
									<td>Komisi</td>
								</thead>
								<tbody>
								<c:forEach items="${cmd.summaryNB}" var="s">
									<tr>
										<td>${s.DIST}</td>
										<td>${s.BAC}</td>
										<td>${s.UW}</td>
										<td>${s.PAYMENT}</td>
										<td>${s.PRINTPOLIS}</td>
										<td>${s.TTP}</td>
										<td>${s.CEKTTP}</td>
										<td>${s.KOMISI}</td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
							<table class="displaytag">
								<caption>Status Aksep</caption>
								<thead>
									<th>Distribusi</th>
									<td>Input</td>
									<td>Decline</td>
									<td>Further</td>
									<td>Extra</td>
									<td>Aksep</td>
									<td>Fund</td>
									<td>Postpone</td>
									<td>AksepKhusus</td>
								</thead>
								<tbody>
								<c:forEach items="${cmd.summaryNB}" var="s">
									<tr>
										<td>${s.DIST}</td>
										<td>${s.ENTRY}</td>
										<td>${s.DECLINE}</td>
										<td>${s.FURTHER}</td>
										<td>${s.EXTRA}</td>
										<td>${s.AKSEP}</td>
										<td>${s.FUND}</td>
										<td>${s.POSTPONE}</td>
										<td>${s.AKSEPKHUSUS}</td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</c:if>
						<c:if test="${not empty cmd.currentRateBSM}">
							<fieldset>
								<legend>Informasi Rate Simas Prima dan Simas Stabil Link</legend>
								<div style="height: 150px; overflow: auto;">
									<table class="displaytag">
										<caption>Rate Saat Ini</caption>
										<thead>
											<th>Periode</th>
											<th>Kurs</th>
											<th>Produk</th>
											<th>MGI/MTI</th>
											<th>Range Premi</th>
											<th>Rate(%)</th>
										</thead>
										<tbody>
										<c:forEach items="${cmd.currentRateBSM}" var="r">
											<tr>
												<td class="center">
													<fmt:formatDate value="${r.LPR_BEGDATE}" pattern="dd/MM/yyyy"/> - 
													<fmt:formatDate value="${r.LPR_ENDDATE}" pattern="dd/MM/yyyy"/>
												</td>
												<td class="left">${r.KURS}</td>
												<td class="left">${r.PRODUK}</td>
												<td class="center">${r.MGI}</td>
												<td>
													<fmt:formatNumber value="${r.LPR_DARI}"/> s/d 
													<fmt:formatNumber value="${r.LPR_AKHIR}"/>
												</td>
												<td>
													<fmt:formatNumber value="${r.LPR_RATE}"/> %
												</td>
											</tr>
										</c:forEach>
										</tbody>
									</table>
									<c:if test="${not empty cmd.futureRateBSM}">
										<table class="displaytag">
											<caption>Rate Yang Akan Datang</caption>
											<thead>
												<th>Periode</th>
												<th>Kurs</th>
												<th>Produk</th>
												<th>MGI/MTI</th>
												<th>Range Premi</th>
												<th>Rate(%)</th>
											</thead>
											<tbody>
											<c:forEach items="${cmd.futureRateBSM}" var="r">
												<tr>
													<td class="center">
														<fmt:formatDate value="${r.LPR_BEGDATE}" pattern="dd/MM/yyyy"/> - 
														<fmt:formatDate value="${r.LPR_ENDDATE}" pattern="dd/MM/yyyy"/>
													</td>
													<td class="left">${r.KURS}</td>
													<td class="left">${r.PRODUK}</td>
													<td class="center">${r.MGI}</td>
													<td>
														<fmt:formatNumber value="${r.LPR_DARI}"/> s/d 
														<fmt:formatNumber value="${r.LPR_AKHIR}"/>
													</td>
													<td>
														<fmt:formatNumber value="${r.LPR_RATE}"/> %
													</td>
												</tr>
											</c:forEach>
											</tbody>
										</table>
									</c:if>
								</div>
							</fieldset>
						</c:if>
						<c:if test="${(sessionScope.currentUser.jn_bank eq '16')}">
						<img src="${path}/include/image/bsim.png"/> 
						
						</c:if>
						<!-- 14 user terpilih dari akm, 8 user terpilih dari bas, sisanya termasuk regional/hybrid  -->
						<!-- akses full - anna yulia  -->
						 <c:if test="${(sessionScope.currentUser.lus_id eq '48' || sessionScope.currentUser.lus_id eq '148' ||
						 				sessionScope.currentUser.lus_id eq '461' || sessionScope.currentUser.lus_id eq '592' || sessionScope.currentUser.lus_id eq '1530' ||
						 				sessionScope.currentUser.lus_id eq '1531' || sessionScope.currentUser.lus_id eq '771' || sessionScope.currentUser.lus_id eq '1378' ||
						 				sessionScope.currentUser.lus_id eq '1532' || sessionScope.currentUser.lus_id eq '1533' || sessionScope.currentUser.lus_id eq '1461' ||
						 				sessionScope.currentUser.lus_id eq '1272' || sessionScope.currentUser.lus_id eq '1380' || sessionScope.currentUser.lus_id eq '1534' ||
						 				sessionScope.currentUser.lus_id eq '1379' || sessionScope.currentUser.lus_id eq '1401' || sessionScope.currentUser.lus_id eq '3790' ||
						 				sessionScope.currentUser.lus_id eq '3675')||
						 				(sessionScope.currentUser.lde_id eq '29' && sessionScope.currentUser.lus_admin eq '1') || (!(sessionScope.currentUser.lca_id eq '01' || sessionScope.currentUser.lca_id eq '09'|| sessionScope.currentUser.lca_id eq '55') && sessionScope.currentUser.lde_id eq '19' && sessionScope.currentUser.jn_bank eq '-1' )}">
							<fieldset>
							<legend>List SE/IM/SK</legend>
								<iframe src="${path}/common/list_sk.htm" width="100%" height="600px" frameborder="0" scrolling="yes"> 
									Please Wait... 
								</iframe>
							</fieldset>
						</c:if>
					</th>
				</tr>
			</table>
			
		</div>
		<div id="pane2" class="panes" style="text-align: left;">
			<c:choose>
				<c:when test="${sessionScope.currentUser.jn_bank eq 2}">
					<table class="displaytag">
						<caption>List Dokumen Pending (Tidak dapat dicairkan)</caption>
						<thead>
							<th>POLIS</th>
							<th>PEMEGANG</th>
							<th>TGL INPUT</th>
							<th>KETERANGAN</th>
						</thead>
						<tbody>
						<c:forEach items="${cmd.listPending}" var="r">
							<tr>
								<td class="left">${r.MSPO_POLICY_NO_FORMAT}</td>
								<td class="left">${r.MCL_FIRST}</td>
								<td class="center">${r.MSPO_INPUT_DATE}</td>
								<td class="left">${r.KETERANGAN}</td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</c:when>
				<c:otherwise>
					<form method="post" name="formpost">
						<input type="hidden" name="fileName">
						<input type="hidden" name="fileDir">
					</form>
					Daftar file yang dapat anda download :<br/><br/>
					
					Untuk File per User (List Aging, Gagal Debet, dll) silahkan <a href="${path}/common/menu.htm?frame=download_per_user">CLICK DISINI</a><br/>
					
					${cmd.treeMenu}
					<!--<table>	
						<tr>
							<th></th>
							<td>Daftar file yang dapat anda download:</td>
						</tr>
						<tr>
							<th>&nbsp;</th>
							<td>${cmd.treeMenu}</td>
						</tr>
						<tr>
							<th></th>
							<td>
								<form method="post" name="formpost">
									<input type="hidden" name="fileName">
									<input type="hidden" name="fileDir">
								</form>
								<c:if test="${not empty cmd.fileListAgency}">
								<display:table id="file" name="cmd.fileListAgency" class="displaytag" style="width: auto;">
									<display:caption>File Agency</display:caption>
									<display:column title="Nama File" style="text-align: left;">
										<a href="#" onclick="document.formpost.fileName.value='${file.key}';document.formpost.fileDir.value='${file.desc}';document.formpost.submit();return false;">${file.key}</a>
									</display:column>
									<display:column title="Tanggal" style="text-align: center;" property="value" />
								</display:table>
								<br/>
								</c:if>
								<display:table id="file" name="cmd.fileListPerUser" class="displaytag" style="width: auto;">
									<display:caption>File Per user</display:caption>
									<display:column title="Nama File" style="text-align: left;">
										<a href="#" onclick="document.formpost.fileName.value='${file.key}';document.formpost.fileDir.value='${file.desc}';document.formpost.submit();return false;">${file.key}</a>
									</display:column>
									<display:column title="Tanggal" style="text-align: center;" property="value" />
								</display:table>
								<br/>
								<c:if test="${not empty cmd.fileListCabangAgency}">
								<display:table id="file" name="cmd.fileListCabangAgency" class="displaytag" style="width: auto;">
									<display:caption>File Cabang AGENCY</display:caption>
									<display:column title="Dir" style="text-align: left;" property="desc" />
									<display:column title="Nama File" style="text-align: left;">
										<a href="#" onclick="document.formpost.fileName.value='${file.name}';document.formpost.fileDir.value='${file.dir}';document.formpost.submit();return false;">${file.name}</a>
									</display:column>
									<display:column title="Tanggal" style="text-align: center;" property="dateModified" />
								</display:table>
								<br/>
								</c:if>
								<c:if test="${not empty cmd.fileListCabangAKM}">
								<display:table id="file" name="cmd.fileListCabangAKM" class="displaytag" style="width: auto;">
									<display:caption>File Cabang AKM</display:caption>
									<display:column title="Dir" style="text-align: left;" property="desc" />
									<display:column title="Nama File" style="text-align: left;">
										<a href="#" onclick="document.formpost.fileName.value='${file.name}';document.formpost.fileDir.value='${file.dir}';document.formpost.submit();return false;">${file.name}</a>
									</display:column>
									<display:column title="Tanggal" style="text-align: center;" property="dateModified" />
								</display:table>
								<br/>
								</c:if>
								<c:if test="${not empty cmd.fileListCabangBancass}">
								<display:table id="file" name="cmd.fileListCabangBancass" class="displaytag" style="width: auto;">
									<display:caption>File Bancassurance</display:caption>
									<display:column title="Dir" style="text-align: left;" property="desc" />
									<display:column title="Nama File" style="text-align: left;">
										<a href="#" onclick="document.formpost.fileName.value='${file.name}';document.formpost.fileDir.value='${file.dir}';document.formpost.submit();return false;">${file.name}</a>
									</display:column>
									<display:column title="Tanggal" style="text-align: center;" property="dateModified" />
								</display:table>
								<br/>
								</c:if>
								<c:if test="${not empty cmd.fileList}">
								<display:table id="file" name="cmd.fileList" class="displaytag" style="width: auto;">
									<display:caption>File Lainnya</display:caption>
									<display:column title="Nama File" style="text-align: left;">
										<a href="${path}/${file.value}">${file.key}</a>
									</display:column>
									<display:column title="Keterangan" style="text-align: left;" property="desc" />
								</display:table>
								<br/>
								</c:if>
							</td>
						</tr>
					</table>-->
				</c:otherwise>
			</c:choose>
		</div>
		<div id="pane3" class="panes" style="text-align: left;">
			<display:table id="file" name="cmd.extList" class="displaytag" style="width: auto;">
				<display:caption>Daftar Extension Kantor Pusat (021) 625 7808</display:caption>
				<display:column title="Dept" style="text-align: left;" property="key" />
				<display:column title="Ext" style="text-align: left;" property="value" />
				<display:column title="Nama" style="text-align: left;" property="desc" />
			</display:table>
		<div>
		
		</div>
		
	</div>
</div>
</body>
</html>