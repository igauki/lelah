<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path}/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path}/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path}/include/js/ajaxtags/overlibmws.js"></script>
<script>
	hideLoadingMessage();
	
	function pesan(tombol){
		if(document.formpost.file1.value == '') {
			alert('Silahkan pilih file terlebih dahulu');
			return false;
		}else{
			if(confirm('Yakin untuk upload file ini?')==true){
				tombol.value = 'Loading...';
				return true;
			}else return false;
		}
	}
	
	function showDetail(){
	
	}
	function pindah(){
		formpost.lsKode.value=GetCurrentListValues(formpost.lar_id);
		formpost.submit();
	}
	
	function GetSelectValues(CONTROL){
		var strTemp = "";
		var j=0;
		for(var i = 0;i < CONTROL.length;i++){
		if(CONTROL.options[i].selected == true){
			if(j==0)
				strTemp += CONTROL.options[i].value;
			else
				strTemp += ","+CONTROL.options[i].value;
			j=1;
		}
		}
		return strTemp;
	}
	
	function GetCurrentListValues(CONTROL){
		var strValues = "";
		strValues = GetSelectValues(CONTROL);
		return arrOldValues = strValues.split(",")
	}
	
</script>
</head>
<BODY onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Upload File</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" style="text-align: center;" enctype="multipart/form-data">
			
					<table class="entry2">
						<tr>
							<th style="vertical-align: top;">
								<fieldset>
									<legend>Upload File</legend>
									<table class="entry2">
										<tr>
											<th>File 1</th>
											<td>
												<input type="file" name="file1" size="70">
											</td>
										</tr>
									</table>
								</fieldset>
								<fieldset>
									<legend>Action</legend>
<!--									<input type="button" name="admin" value="View Admin Cabang">-->
									<input type="submit" name="upload" value="Upload" onClick="return pesan(this);">
								</fieldset>
							</th>
						</tr>
					</table>			

				</form>
			</div>
		</div>
	</div>

</form>
</body>
<c:if test="${not empty pesan}">
	<script>alert('${pesan}');</script>
</c:if>
<%@ include file="/include/page/footer.jsp"%>