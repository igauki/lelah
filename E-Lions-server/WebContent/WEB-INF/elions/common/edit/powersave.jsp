<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<meta HTTP-EQUIV="Expires" CONTENT="-1">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link href="${path}/include/image/eas.ico" rel="shortcut icon">
		<!--  -->
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<!--  -->
		<script type="text/javascript" src="${path }/include/js/default.js"></script>
		<!-- Ajax Related -->
		<script type="text/javascript" src="${path}/include/js/ajax.js"></script>
		<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script>
		<script type="text/javascript" src="${path }/dwr/engine.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>
		<script type="text/javascript" src="${path }/include/js/refund/refund_common.js"></script>
		<script type="text/javascript">
			hideLoadingMessage();

			function awal(){
				var errorAwal = '';
				<c:forEach var="e" items="${errorAwal}">
					errorAwal += '- ${e}\n';
				</c:forEach>
				
				var pesan = '${pesan}';
				
				if(errorAwal != ''){
					alert('ERROR:\n' + errorAwal);
					window.location = '${path}/uw/view.htm?p=v&showSPAJ=${reg_spaj}';
				}else if(pesan != ''){
					alert(pesan);
				}
			}
		</script>
	</head>
	<body onload="setupPanes('container1','tab1'); awal();" style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Edit Data Powersave</a>
				</li>
			</ul>

			<div class="tab-panes">
				<div id="pane1" class="panes" style="text-align: left;">
					<fieldset>
						<form method="post" name="formpost">
						<table class="displaytag" style="width: auto;">
							<thead>
							<tr>
								<th>Jenis</th>
								<th>Perubahan Data</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td class="left">
									<label for="ubah_mgi"><input type="radio" class="noBorder" id="ubah_mgi" name="jenis" value="Ubah MGI">Ubah MGI</label>
								</td>
								<td class="left">
									Dari <strong>${powersave.mps_jangka_inv} bln</strong> menjadi 
									<select name="mgi">
						                <c:forEach var="m" items="${select_mgi}">
						                	<option value="${m}">${m} Bln</option>
						                </c:forEach>
									</select>																
								</td>
							</tr>
							<tr>
								<td class="left">
									<label for="ubah_rollover"><input type="radio" class="noBorder" id="ubah_rollover" name="jenis" value="Ubah Rollover">Ubah Rollover</label>
								</td>
								<td class="left">
									Dari <strong>${rollover}</strong> menjadi 
									<select name="rollover">
						                <c:forEach var="roll" items="${select_rollover}">
						                	<option value="${roll.ID}">${roll.ROLLOVER}</option>
						                </c:forEach>
									</select>								
								</td>
							</tr>
							<tr>
								<td class="left">
									<label for="ubah_rate"><input type="radio" class="noBorder" id="ubah_rate" name="jenis" value="Ubah Rate">Ubah Rate</label>
								</td>
								<td class="left">
									Dari <strong>${powersave.mps_rate}%</strong> menjadi 
									<input type="text" size="5" style="text-align: right;" name="rate" value="${powersave.mps_rate}"> % 
									dengan Nomor MEMO : 
									<input type="text" name="memo">
								</td>
							</tr>
							<tr>
								<td class="left">
									<label for="ubah_begdate"><input type="radio" class="noBorder" id="ubah_begdate" name="jenis" value="Ubah Beg Date">Ubah Beg Date</label>
								</td>
								<td class="left">
									Dari <strong><fmt:formatDate value="${powersave.mps_deposit_date}" pattern="dd/MM/yyyy"/></strong> menjadi 
									<script>inputDate('begdate', '<fmt:formatDate value="${powersave.mps_deposit_date}" pattern="dd/MM/yyyy"/>', false);</script>
								</td>
							</tr>
							<tr>
								<td class="left">
									<label for="ubah_premi"><input type="radio" class="noBorder" id="ubah_premi" name="jenis" value="Ubah Premi">Ubah Premi</label>
								</td>
								<td class="left">
									Dari <strong><fmt:formatNumber value="${powersave.mps_prm_deposit}" type="number" /></strong> menjadi 
									<input type="text" style="text-align: right;"
										name="premi" onfocus="showForm( 'premiumHelper', 'true' );" onblur="showForm( 'premiumHelper', 'false' );" 
										onchange="this.value=formatCurrency( this.value );" onkeyup="showFormatCurrency('premiumHelper', this.value);">
									<input type="text" id="premiumHelper" disabled="disabled" style="display: none;" tabindex="-1"/>
								</td>
							</tr>
							</tbody>
						</table>
						<br/>
						Di-request Oleh: <input type="text" name="desc" size="75" maxlength="50">
						<br/>
						<div id="success" style="text-transform: none;">
						Syarat Edit Powersave:<br/>
						- Belum ada pembayaran bunga<br/>
						- Belum ada rollover<br/>
						</div>
						<input type="submit" name="save" value="Hajar Bleh!" onclick="return confirm('Proses Data? History perubahan akan dicatat di sistem.');">
						</form>
					</fieldset>
				</div>
			</div>
			
		</div>

	</body>
</html>