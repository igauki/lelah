<%@ include file="/include/page/header.jsp"%>
<style type="text/css">
	html,body{
	     overflow: hidden;
	}
</style>
<script>
	function cekSpaj(){
		var infoFrame 	= document.getElementById('infoFrame');
		var spaj 		= document.formpost.spaj.value;

		if(trim(spaj)=='') {
			alert('Harap cari nomor SPAJ terlebih dahulu');
			infoFrame.src = '${path}/uw/spaj.htm?posisi=-1&win=viewer';
		}else {
			return true;
		}
		return false;
	}
	
	function tampil(y){
		var infoFrame 	= document.getElementById('infoFrame');
		var spaj 		= document.formpost.spaj.value;
		
		if('cari' == y) {
			infoFrame.src = '${path}/uw/spaj.htm?posisi=-1&win=viewer';
		}else if('info' == y && cekSpaj()) {
			infoFrame.src='${path}/uw/view.htm?p=v&showSPAJ='+spaj;
		}else if('powersave' == y && cekSpaj()) {
			infoFrame.src='${path}/common/edit.htm?window=powersave&reg_spaj='+spaj;
		}
	}
	
	function awal(){
		var pesan = 'CATATAN:';
		
		pesan += '\n- Edit data hanya dapat dilakukan apabila Polis masih berada di New Business (Input / UW / Payment / PrintPolis)';
		pesan += '\n- Edit data hanya dapat dilakukan maksimal 30 hari dari Beg Date Polis';

		pesan += '\n\nSemua perubahan data akan di-LOG di sistem!!!';
		alert(pesan);
	}

</script>
<body onload="setFrameSize('infoFrame', 58); awal();" onresize="setFrameSize('infoFrame', 58);" style="height: 100%;">
	<form name="formpost" method="post">
		<div class="tabcontent">
			<table class="entry2" style="width: 98%;">
				<tr>
					<th style="width: 50px;">Spaj</th>
					<th style="width: 150px;"><input type="text" size="15" readonly="readonly" class="readOnly" id="spaj" name="spaj"></th>
					<th><input type="button" value="Cari" onclick="tampil('cari');" style="width: 35px;"></th>
					<td>
						<input type="button" value="Powersave" onclick="tampil('powersave');">
						
						<input type="button" value="Button" disabled="disabled">
						<input type="button" value="Button" disabled="disabled">
						<input type="button" value="Button" disabled="disabled">
						<input type="button" value="Button" disabled="disabled">
					</td>
				</tr>
				<tr>
					<th>Polis</th>
					<th><input type="text" size="20" readonly="readonly" class="readOnly" id="polis" name="polis"></th>
					<th style="width: 100px;"><input type="button" value="Info" onclick="tampil('info');" style="width: 35px;"></th>
					<td>
						<input type="button" value="Button" disabled="disabled">
						<input type="button" value="Button" disabled="disabled">
						<input type="button" value="Button" disabled="disabled">
						<input type="button" value="Button" disabled="disabled">
						<input type="button" value="Button" disabled="disabled">
					</td>
				</tr>

				<tr>
					<td colspan="4">
						<table style="margin: 0 0 0 0; padding: 0 0 0 0; width: 100%;">
							<tr>
								<td>
									<iframe src="${path}/uw/spaj.htm?posisi=-1&win=viewer"
										name="infoFrame" id="infoFrame" width="100%">
										Please Wait...
									</iframe>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>
