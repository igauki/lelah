<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head> 
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/disableKeys.js"></script>
		<script type="text/javascript" src="${path}/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>
		<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
		<script>hideLoadingMessage();</script>
	</head>
	<script>
		function proses_menu(pro,id){
			var flag=1
			//pro 1=add, 2=delete, 3=edit
			if(pro==2){
				if(confirm("Yakin Ingin Hapus Menu")==false)
					flag=0
			}
			
			if(flag==1){		
				document.formpost.proses.value=pro;	
				document.formpost.menu_id.value=id;
				formpost.submit();
			}
		}
		
		function awal(){
			if('${cmd.tag2}'==1){
				setupPanes("container1", "tab2");
			}else{
				setupPanes("container1", "tab1");
			}
			if('${not emptystatus.errorMessages}'){
				window.location='#bottom';
			}
							
			
		}
	</script>
<body style="height: 100%;" onload='awal();'>
<div id="contents">
	<form:form id="formpost" name="formpost" commandName="cmd">
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li><a href="#" onClick="return showPane('pane1', this)" id="tab1">Menu Overview</a></li>
				<c:if test="${cmd.tag2 ne null }">
					<li><a href="#" onClick="return showPane('pane2', this)" id="tab2">Add Menu</a></li>
				</c:if>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<table class="entry2">
						<tr>
						<c:forEach var="baris" items="${allMenu}">
							<c:if test="${baris.level lt tingkat}">
								<c:forEach var="hit" begin="1" end="${tingkat - baris.level}" step="1">
									</ul></li>
								</c:forEach>
							</c:if>
							<c:choose>
								<c:when test="${baris.level eq 1}">
									<ul><li>
										<c:if test="${not empty baris.icon}">
											<img src="${path}/${baris.icon}"/>
										</c:if>
										<strong class="link">${baris.nama_menu}</strong>
										<a onclick="proses_menu(1,'${baris.menu_id}');" href="#"><img src="${path}/include/image/add.gif" width="11" height="11"  border="0" onmouseover="return overlib('Add', AUTOSTATUS, WRAP);" onmouseout="nd();" > </a>
										<a onclick="proses_menu(2,'${baris.menu_id}');" href="#"><img src="${path}/include/image/del.gif" width="11" height="11"  border="0" onmouseover="return overlib('Delete', AUTOSTATUS, WRAP);" onmouseout="nd();" > </a>
										<a onclick="proses_menu(3,'${baris.menu_id}');" href="#"><img src="${path}/include/image/edit.gif" width="11" height="11"  border="0" onmouseover="return overlib('Edit', AUTOSTATUS, WRAP);" onmouseout="nd();" > </a>
									<ul>
								</c:when>
								<c:when test="${empty baris.link_menu}">
									<li>
										<c:if test="${not empty baris.icon}">
											<img src="${path}/${baris.icon}"/>
										</c:if>
										<strong class="link">${baris.nama_menu}</strong>
										<a onclick="proses_menu(1,'${baris.menu_id}');" href="#"><img src="${path}/include/image/add.gif" width="11" height="11"  border="0" onmouseover="return overlib('Add', AUTOSTATUS, WRAP);" onmouseout="nd();" > </a>
										<a onclick="proses_menu(2,'${baris.menu_id}');" href="#"><img src="${path}/include/image/del.gif" width="11" height="11"  border="0" onmouseover="return overlib('Delete', AUTOSTATUS, WRAP);" onmouseout="nd();" > </a>
										<a onclick="proses_menu(3,'${baris.menu_id}');" href="#"><img src="${path}/include/image/edit.gif" width="11" height="11"  border="0" onmouseover="return overlib('Edit', AUTOSTATUS, WRAP);" onmouseout="nd();" > </a>
									<ul>
								</c:when>
								<c:when test="${not empty baris.link_menu}">
									<li>
										<c:if test="${not empty baris.icon}">
											<img src="${path}/${baris.icon}"/>
										</c:if>
										<a href="${path}/${baris.link_menu}" class="link">${baris.nama_menu}</a>
										<a onclick="proses_menu(1,'${baris.menu_id}');" href="#"><img src="${path}/include/image/add.gif" width="11" height="11"  border="0" onmouseover="return overlib('Add', AUTOSTATUS, WRAP);" onmouseout="nd();" > </a>
										<a onclick="proses_menu(2,'${baris.menu_id}');" href="#"><img src="${path}/include/image/del.gif" width="11" height="11"  border="0" onmouseover="return overlib('Delete', AUTOSTATUS, WRAP);" onmouseout="nd();" > </a>
										<a onclick="proses_menu(3,'${baris.menu_id}');" href="#"><img src="${path}/include/image/edit.gif" width="11" height="11"  border="0" onmouseover="return overlib('Edit', AUTOSTATUS, WRAP);" onmouseout="nd();" > </a>
									</li>
								</c:when>
								<c:otherwise>
									<ul><li>
										<c:if test="${not empty baris.icon}">
											<img src="${path}/${baris.icon}"/>
										</c:if>
										${baris.nama_menu}
										<a onclick="proses_menu(1,'${baris.menu_id}');" href="#"><img src="${path}/include/image/add.gif" width="11" height="11"  border="0" onmouseover="return overlib('Add', AUTOSTATUS, WRAP);" onmouseout="nd();" > </a>
										<a onclick="proses_menu(2,'${baris.menu_id}');" href="#"><img src="${path}/include/image/del.gif" width="11" height="11"  border="0" onmouseover="return overlib('Delete', AUTOSTATUS, WRAP);" onmouseout="nd();" > </a>
										<a onclick="proses_menu(3,'${baris.menu_id}');" href="#"><img src="${path}/include/image/edit.gif" width="11" height="11"  border="0" onmouseover="return overlib('Edit', AUTOSTATUS, WRAP);" onmouseout="nd();" > </a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:set var="tingkat" value="${baris.level}"/>
						</c:forEach>
						
						</tr>
						<tr> 
					        <td nowrap="nowrap" colspan="4">
							    <input type="hidden" name="flag" value="0">
							  <c:choose>
							  	<c:when test="${submitSuccess eq 1 }">
							  		<script>
							  			alert("Berhasil Tambah Menu");
							  			window.location="${path}/common/menumaintenance.htm";
							  		</script>
							  	</c:when>
							  	<c:when test="${submitSuccess eq 2 }">
							  		<script>
							  			alert("Berhasil hapus Menu");
							  			window.location="${path}/common/menumaintenance.htm";
							  		</script>
							  	</c:when>
							  	<c:when test="${submitSuccess eq 3 }">
							  		<script>
							  			alert("Berhasil Edit Menu");
							  			window.location="${path}/common/menumaintenance.htm";
							  		</script>
							  	</c:when>
							  	
							  </c:choose>
							  <spring:bind path="cmd.*">
									<c:if test="${not empty status.errorMessages}">
										<div id="error">
							        		Pesan:<br>
												<c:forEach var="error" items="${status.errorMessages}">
															- <c:out value="${error}" escapeXml="false" />
													<br/>
												</c:forEach>
										</div>
									</c:if>									
								</spring:bind>
								<a name="bottom"></a>
								<form:hidden path="menu_id"  />
								<form:hidden path="proses" />
								<input type="hidden" name="save" value="0">
							</td>
				   		 </tr>
				   		 						
					</table>
				</div>
			<c:if test="${cmd.tag2 ne null }">
				<div id="pane2" class="panes">
					<table class="entry2">
						<tr>
							<th>Nama Menu</th>
							<td>
								<form:input path="menu.nama_menu" size="100"  />
							</td>
						</tr>
						<tr>
							<th>Link Menu</th>
							<td>
								<form:input path="menu.link_menu" size="100"  />
							</td>
						</tr>
						<tr>
							<th>Flag Aktif</th>
							<td>
								<form:radiobutton cssClass="noBorder" path="menu.flag_aktif" id="menu.flag_aktif" value="Y" />Y
								<form:radiobutton cssClass="noBorder" path="menu.flag_aktif" id="menu.flag_aktif" value="N"	/>Y
							</td>
						</tr>
						<tr>
							<th>Flag Public</th>
							<td>
								<form:radiobutton cssClass="noBorder" path="menu.flag_public" id="menu.flag_public" value="Y"/>Y
								<form:radiobutton cssClass="noBorder" path="menu.flag_public" id="menu.flag_public" value="N"/>N
							</td>
						</tr>
						<tr>
							<th>Ikon</th>
							<td>
								<form:hidden path="menu.icon" />
								<a href="#"><img name="ikon" class="noBorder" src="${path}/${cmd.menu.icon}" onClick="popWin('${path}/common/menu.htm?frame=loadIkon', 350, 450); "></a>
							</td>
						</tr>
				  		  <tr>
							<th></th>
							<td>
								<input type="button" name="btnSave" value="Save" onclick="this.disabled=true;document.formpost.save.value='1';document.formpost.submit();"
									accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
							</td>
						  </tr>
						  <tr> 
					        <td> 
					        	<spring:bind path="cmd.*">
									<c:if test="${not empty status.errorMessages}">
										<div id="error">
							        		Pesan:<br>
												<c:forEach var="error" items="${status.errorMessages}">
															- <c:out value="${error}" escapeXml="false" />
													<br/>
												</c:forEach>
										</div>
									</c:if>									
								</spring:bind>
								
							</td>
				   		 </tr>
					</table>
				</div>
			</c:if>
			</div>
		</div>
	</form:form>
</div>
</body>
</html>