<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
	<title>PT. Asuransi Jiwa Sinarmas MSIG</title>
	<meta name="verify-v1" content="NWau46kEyEm+WnYb1AfPtB0ZUV07pSP40di+b+kxleA=" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-Frame-Options" content="deny">
	<meta name="Description" content="SinarmasMSIGLife">
	<link rel="Stylesheet" type="text/css" href="${path }/include/css/default.css" media="screen">
	<link href="${path}/include/image/eas.ico" rel="shortcut icon">
	<script type="text/javascript" src="${path }/include/js/default.js"></script>
	<script type="text/javascript" src="${path }/include/js/common/login.js"></script>

	<style id="antiClickjack">body{display:none !important;}</style>		
</head>
<body onload="on_load();" style="height: 100%; width: 100%; text-align: center;">

	<c:choose>
		<c:when test="${notAllowed}">
			<script type="text/javascript">
			alert('Maaf anda tidak dapat mengakses halaman ini. Terima kasih.');
			window.close();
		</script>
		</c:when>
		<c:otherwise>

		<form method="post" name="formpost" action="">
		<spring:htmlEscape defaultHtmlEscape="true">
			<input type="hidden" id="screenWidth" name="screenWidth">
			<input type="hidden" id="screenHeight" name="screenHeight">
			<input type="hidden" id="deebee" name="deebee" value="${fn:escapeXml(deebee)}">
			<input type="hidden" id="login" name="login" value="Login">
			
			<div style="padding: 75px; padding-bottom: 110px; margin-top: 30px;"><img src="${path }/include/image/logo_ajs_new.png" alt="SinarmasLife" /></div>
			
			<table align="center">
				<tr >
					<td align="right">
						User<br>
						<spring:bind path="user.name">
							<input type="text" name="${status.expression}" id="${status.expression}" value="${fn:escapeXml(status.value)}" onfocus="this.select();" onkeyup="handleEnter(this, event);">
						</spring:bind>
						<br>
						Password<br>
						<spring:bind path="user.pass">
							<input type="password" name="${status.expression}" id="${status.expression}" value="" onfocus="this.select();" onkeyup="handleEnter(this, event);" autocomplete="off">
						</spring:bind>
						<br>
						<spring:bind path="user.*">
							<c:if test="${not empty status.errorMessages}">
								<div id="error">
									<c:forEach var="error" items="${status.errorMessages}">
										- <c:out value="${error}" escapeXml="false" /><br>
									</c:forEach>
								</div>
							</c:if>
						</spring:bind>
					</td>
					<td>
						<input type="button" name="btnLogin" value="Login" style="height: 60px; width: 60px" onclick="log_in(this);">
					</td>
				</tr>
			</table>
			
			<!--remove this, untuk testing session replication saja 
			<br><br>
			Tambah = ${sessionScope.tambah}
			<br>
			ID = ${pageContext.session.id}
			<br>
			Online Time = ${(pageContext.session.lastAccessedTime-pageContext.session.creationTime)/1000}
			<br>
			-->
			
			<div id="footer_login">
				<%-- <object height="60" width="90">
					<param name="movie" value="somefilename.swf"/>
					  <param name="loop" value="true" />
	                    <param name="menu" value="false" />
	                    <param name="quality" value="best" />
	                    <param name="wmode" value="transparent" />
					<embed src="${path}/include/image/getseal.swf" height="60" width="90" loop="true" menu="false" quality="best" wmode="transparent" swliveconnect="FALSE">
					</embed>
				</object>
				<br/>
				&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.verisign.com/ssl/ssl-information-center/" target="_blank" style="color:black; text-decoration:none; font:bold 8px;">ABOUT SSL CERTIFICATES </a> --%>
				
				<script language="JavaScript" type="text/javascript">
					TrustLogo("https://elions.sinarmasmsiglife.co.id/include/image/comodo_secure.png", "SC5", "none");
				</script>
				<a href="http://www.instantssl.com/wildcard-ssl.html" id="comodoTL">Wildcard SSL</a>
				
				<br><spring:message code="copyright" /> ${copyrightYear} Sinarmas MSIG Life IT Department [${sessionScope.deebee}]
				<br/> <%=request.getLocalAddr()%>
			</div>
		</spring:htmlEscape>	
		</form>
		
		</c:otherwise>
	</c:choose>
		 <%
			String sIPAddress = request.getServerName();
			
			
			if((sIPAddress.contains("ajsjava"))){
			
		%>
		<script type="text/javascript">
				window.location="http://www.sinarmasmsiglife.co.id/E-Lions";
		</script>
		<%
			}else{
		%>
		
		<%}
		%> 
		
		<script type="text/javascript" src="${path }/include/js/common/login_bottom.js"></script>
</body>
</html>