<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>

<script>

function prosesConfirm(){
	var x=window.confirm('Yakin Transfer ke Filling ?');
	if(x){
		//alert('masuk');
		document.getElementById('formpost').submit();
	}
}



</script>

<BODY style="height: 100%;" >
	<c:choose>
		<c:when test="${cmd.flagAdd eq 2}">
			<form:form id="formpost" name="formpost" commandName="cmd">
				<table class="entry2" >
					<tr >
						<td valign="top">
							<fieldset>
								<legend>J. Customer Non-Completion of this Form</legend>
								<table class="entry2">
									<tr>
										<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
										<br />
											Atas permintaan saya/kami telah meminta Bancassurance Financial Advisor untuk tidak menggunakan formulir ini dalam menentukan kebutuhan saya/kami, walaupun dia telah menjelaskan kepada saya/kami bahwa formulir ini bisa membuatnya memastikan bahwa usulan/saran yang diberikannya sesuai dengan keadaan saya/kami.
										</td>
									</tr>
									<tr>
										<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
										<br />	
											Saya/kami telah meminta Advisor untuk memberikan perincian secara detail tentang produk/layanan yang telah saya/kami beli, dan saya putuskan ini tepat untuk saya/kami dan memtuskan untuk berinvestasi tanpa saran dari Bancassurance Financial Advisor.
										</td>	
									</tr>
								</table>
								<br />
								<br />
								<table class="entry2">
									<tr>
										<th align="left" width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Produk yang saya/kami pilih untuk dibeli adalah</th>
										<td align="left">
											<form:select disabled="disabled" path="nasabah.lsbs_id">
												<form:option label="" value=""/>
												<form:options items="${lstProduct}" itemLabel="value" itemValue="key"/>
											</form:select>
										</td>
									</tr>
									<tr>
										<th align="left">Dan jumlah Uang Pertanggungan yang telah saya/kami pilih sebesar</th>
										<td align="left"><form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="nasabah.mns_up" id="mns_up" /> </td>
									</tr>
									<tr></tr><tr></tr><tr></tr>
								</table>
								<table class="entry2">	
									<tr>
										<td align="center" width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Tanda Tangan Nasabah</td>
										<td align="left" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
											Tanggal &nbsp;
											<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="nasabah.mns_tgl_tt" id="mns_tgl_tt" maxlength="10" size="12"/>
											<span class="info">*</span>
										</td>
									</tr>
									<tr></tr><tr></tr><tr></tr>
									<tr></tr><tr></tr><tr></tr>
									<tr></tr><tr></tr><tr></tr>
									<tr></tr><tr></tr><tr></tr>
									<tr></tr><tr></tr><tr></tr>
									<tr></tr><tr></tr><tr></tr>
								</table>
							</fieldset>
						</td>
					<tr>
					<tr>
						<td>
							<span class="info"  style=" text-align: left; font-size: 8pt;">
								* Format tanggal dd/MM/yyyy
							</span>
						<td>
					</tr>
					<tr>
						<th align="center">
							<input type="hidden" name="save" id="save" value="Save"/>
							<input type="hidden" name="trans" id="trans" value="Trans" onclick="prosesConfirm();" /> 
						</th>
					</tr>
				</table>
				<table class="entry2">
					<tr>
						<td>	        	
							<spring:bind path="cmd.*">
								<c:if test="${not empty status.errorMessages}">
									<div id="error">
										Informasi:<br>
										<c:forEach var="error" items="${status.errorMessages}">
											 - <c:out value="${error}" escapeXml="false" />
											<br/>
										</c:forEach>
									</div>
								</c:if>									
							</spring:bind>
						</td>
					</tr>
				</table>
				<table>
					<tr>
						<td colspan="4">
							<c:if test="${submitSuccess eq true}">
								<c:choose>
									<c:when test="${cmd.flagAdd eq 1}">
										<script>
											alert("Berhasil Transfer");
											addOptionToSelect(parent.document, parent.document.formpost.nomor, '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}', '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}');
											window.parent.tes();
										</script>
									</c:when>
									<c:otherwise>
										<script>
											alert("Berhasil Simpan");
											addOptionToSelect(parent.document, parent.document.formpost.nomor, '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}', '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}');
											window.location='${path }/common/input_closingnon.htm?nomor=${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}';
										</script>
									</c:otherwise>
								</c:choose>
					        	<div id="success">
						        	 Berhasil
						    	</div>
					        </c:if>
						 </td>
					</tr>
				</table>	
			</form:form>
		</c:when>
		<c:otherwise>
			<form:form id="formpost" name="formpost" commandName="cmd">
				<table class="entry2" >
					<tr >
						<td valign="top">
							<fieldset>
								<legend>J. Customer Non-Completion of this Form</legend>
								<table class="entry2">
									<tr>
										<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
										<br />
											Atas permintaan saya/kami telah meminta Bancassurance Financial Advisor untuk tidak menggunakan formulir ini dalam menentukan kebutuhan saya/kami, walaupun dia telah menjelaskan kepada saya/kami bahwa formulir ini bisa membuatnya memastikan bahwa usulan/saran yang diberikannya sesuai dengan keadaan saya/kami.
										</td>
									</tr>
									<tr>
										<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
										<br />	
											Saya/kami telah meminta Advisor untuk memberikan perincian secara detail tentang produk/layanan yang telah saya/kami beli, dan saya putuskan ini tepat untuk saya/kami dan memtuskan untuk berinvestasi tanpa saran dari Bancassurance Financial Advisor.
										</td>	
									</tr>
								</table>
								<br />
								<br />
								<table class="entry2">
									<tr>
										<th align="left" width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Produk yang saya/kami pilih untuk dibeli adalah</th>
										<td align="left">
											<form:select path="nasabah.lsbs_id">
												<form:option label="" value=""/>
												<form:options items="${lstProduct}" itemLabel="value" itemValue="key"/>
											</form:select>
										</td>
									</tr>
									<tr>
										<th align="left">Dan jumlah Uang Pertanggungan yang telah saya/kami pilih sebesar</th>
										<td align="left"><form:input cssStyle="border-color:#6893B0;" path="nasabah.mns_up" id="mns_up" /> </td>
									</tr>
									<tr></tr><tr></tr><tr></tr>
								</table>
								<table class="entry2">	
									<tr>
										<td align="center" width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Tanda Tangan Nasabah</td>
										<td align="left" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
											Tanggal &nbsp;
											<form:input cssStyle="border-color:#6893B0;" path="nasabah.mns_tgl_tt" id="mns_tgl_tt" maxlength="10" size="12"/>
											<span class="info">*</span>
										</td>
									</tr>
									<tr></tr><tr></tr><tr></tr>
									<tr></tr><tr></tr><tr></tr>
									<tr></tr><tr></tr><tr></tr>
									<tr></tr><tr></tr><tr></tr>
									<tr></tr><tr></tr><tr></tr>
									<tr></tr><tr></tr><tr></tr>
								</table>
							</fieldset>
						</td>
					<tr>
					<tr>
						<td>
							<span class="info"  style=" text-align: left; font-size: 8pt;">
								* Format tanggal dd/MM/yyyy
							</span>
						<td>
					</tr>
					<tr>
						<th align="center">
							<input type="submit" name="save" id="save" value="Save"/>
							<input type="button" name="trans" id="trans" value="Trans" onclick="prosesConfirm();" /> 
						</th>
					</tr>
				</table>
				<table class="entry2">
					<tr>
						<td>	        	
							<spring:bind path="cmd.*">
								<c:if test="${not empty status.errorMessages}">
									<div id="error">
										Informasi:<br>
										<c:forEach var="error" items="${status.errorMessages}">
											 - <c:out value="${error}" escapeXml="false" />
											<br/>
										</c:forEach>
									</div>
								</c:if>									
							</spring:bind>
						</td>
					</tr>
				</table>
				<table>
					<tr>
						<td colspan="4">
							<c:if test="${submitSuccess eq true}">
								<c:choose>
									<c:when test="${cmd.flagAdd eq 1}">
										<script>
											alert("Berhasil Transfer");
											addOptionToSelect(parent.document, parent.document.formpost.nomor, '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}', '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}');
											window.parent.tes();
										</script>
									</c:when>
									<c:otherwise>
										<script>
											alert("Berhasil Simpan");
											addOptionToSelect(parent.document, parent.document.formpost.nomor, '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}', '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}');
											window.location='${path }/common/input_closingnon.htm?nomor=${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}';
										</script>
									</c:otherwise>
								</c:choose>
					        	<div id="success">
						        	 Berhasil
						    	</div>
					        </c:if>
						 </td>
					</tr>
				</table>	
			</form:form>
		</c:otherwise>
	</c:choose>
</body>

<%@ include file="/include/page/footer.jsp"%>