<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>

<body onload="setupPanes('container1','tab1');" style="height: 50%;width: 50%;" >
<script>
		function eventController(event){
			if(event == 'gotoPage'){
				document.getElementById('theEvent').value = 'gotoPage';
				document.getElementById('search').click();
			}else if(event == 'first'){
				document.getElementById('theEvent').value = 'first';
				document.getElementById('search').click();
			}else if(event == 'prev'){
				document.getElementById('theEvent').value = 'prev';
				document.getElementById('search').click();
			}else if(event == 'next'){
				document.getElementById('theEvent').value = 'next';
				document.getElementById('search').click();
			}else if(event == 'last'){
				document.getElementById('theEvent').value = 'last';
				document.getElementById('search').click();
			}
		}
		
		function openPdf( value )
		{
	/* 		var char1 = String.fromCharCode(191); //inverted question mark
			var char2 = String.fromCharCode(8230); //horizontal ellipsis
			var char3 = String.fromCharCode(8220); //left double quotation mark
			var char4 = String.fromCharCode(8221); //right double quotation mark
			var result1 = value.replace(char1, char2); 
			var result2 = ReplaceAll(value, char1, char2); 
			
			var server_name = document.location.hostname;
			alert(value);
			alert(result1);
			alert(result2); */
			
			/* var url = "//intranet/PDF/"; */
			//alert(value);
			var url ="\\\\intranet\\DocumentCenter\\"+value;
			//alert(url);
			popWin('${path}/common/util.htm?window=downloadFile&dir='+url, 400, 550);
			
		/* 	if(server_name == 'www.sinarmasmsiglife.com' || server_name == 'www.sinarmasmsiglife.co.id' || server_name == 'sinarmasmsiglife'){
				url = "http://www.sinarmasmsiglife.com/e-hrd";
			} */
			
			
			/* 	var url = "http://intranet/e-hrd/pdf/";
			
			if(server_name == 'www.sinarmasmsiglife.com' || server_name == 'www.sinarmasmsiglife.co.id' || server_name == 'sinarmasmsiglife'){
				url = "http://www.sinarmasmsiglife.com/e-hrd/pdf/";
			}
			 */
			
		/* 	if(result1 != result2){
				var result2 = value.replace(char1, char3); 
				var result2 = result2.replace(char1, char4); 	
				window.open(url + result2, "pdf", "pdf");
				//document.getElementById('theEvent').value = 'download';
				//document.getElementById('filename').value = result2;
				//document.getElementById('search').click();
			}else{
				window.open(url + result1, "pdf", "pdf");
				//document.getElementById('theEvent').value = 'download';
				//document.getElementById('filename').value = result1;
				//document.getElementById('search').click();
			} */
			
		}
		
		function ReplaceAll(Source,stringToFind,stringToReplace){
  		var temp = Source;
    	var index = temp.indexOf(stringToFind);
        while(index != -1){
            temp = temp.replace(stringToFind,stringToReplace);
            index = temp.indexOf(stringToFind);
        }
        return temp;
		}
		
		function change_bgcolor( obj, color )
        {
            obj.style.backgroundColor = color;
        }
	</script>
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">List SE/IM/SK </a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
<form:form id="formpost" commandName="jobstreetCompanyForm">
		<fieldset style="margin: 0px 7px 7px 7px; padding: 7px 7px 7px 7px; text-align: left;">
			<legend>
				List Surat
            </legend>
		<div id="pane1" class="entry2">
            <table style="width: 100%" >
            	<tr>
                    <td style="text-align: right; font-family: sans-serif;">
                    	Jenis
                    </td>
                    <td>
                    	<select name="jenisSurat" id="jenisSurat"">
							<option value="" selected>ALL</option>
							<option value="1" >SK</option>
							<option value="2" >SE</option>
							<option value="3" >IM</option>
						</select> 
						<script type="text/javascript">document.getElementById('jenisSurat').value = '${jenisSurat}';</script>
                    </td>
                    <td style="text-align: right; font-family: sans-serif;">
                    	View
                    </td>
                    <td>
                    	<select name="viewSurat" id="viewSurat">
							<option value="0" >ALL</option>
							<option value="" selected>MINE/SELF</option>
						</select> 
						<script type="text/javascript">document.getElementById('viewSurat').value = '${viewSurat}';</script>
                    </td>
                    <td style="text-align: right; font-family: sans-serif;">
                        Th.Update
                    </td>
                    <td colspan="2">
                    	<input type="text" name="thUpdate" value="${thUpdate}" style="width: 35px;" maxlength="4"/>
                    	bln.
                    	<select name="lstBln" id="lstBln"">
							<option value="" selected>All</option>
							<option value="9" >9 bln. terakhir</option>
							<option value="6" >6 bln. terakhir</option>
							<option value="3" >3 bln. terakhir</option>
							<option value="1" >1 bln. terakhir</option>
						</select> 
						<script type="text/javascript">document.getElementById('lstBln').value = '${lstBln}';</script>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right; font-family: sans-serif;">
                    	No.
                    </td>
                    <td>
                    	<input type="text" name="noSurat" value="${noSurat}"/>
                    </td>
                    <td style="text-align: right; font-family: sans-serif;">
                        Judul
                    </td>
                    <td>
                    	<input type="text" name="judulSurat" value="${judulSurat}"/>
                    </td>
                    <td style="text-align: left font-family: sans-serif;" colspan="2">
                    	<input type="submit" name="search" id="search" value="search"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <hr/>
                    </td>
               </tr>
            </table>
            <table style="width: 100%">
                <tr style="background-color: burlywood;">
					<td style="width: 20%">Nomor</td>
					<td style="width: 35%">Judul</td>
					<td style="width: 15%">Last Update</td>
					<td style="width: 10%">Action</td>
                </tr>
                <c:set var="backGroundColor" value="#EAEAEA"/>
                <c:forEach items="${daftarSurat}" var="x" varStatus="xt">

                    <c:choose>
                        <c:when test='${"#EAEAEA" == backGroundColor}'>
                            <c:set var="backGroundColor" value="#FFFFFF"/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="backGroundColor" value="#EAEAEA"/>
                        </c:otherwise>
                    </c:choose>
                    
                    <tr style="background-color: ${backGroundColor};"
                        onMouseOver="change_bgcolor( this, '#FFD6CA' );"
                        onMouseOut="change_bgcolor(this, '${backGroundColor}');">
                        <c:set var="disabledAddition" value=""/>
                        <td style="text-align: left; vertical-align: text-top;">
                                ${x.no_surat}
                        </td>
                        <td style="text-align: left; vertical-align: text-top;">
                        	<c:choose>
                        		<c:when test="${filter eq 'bas' && (x.grant_user eq nik || x.grant_user eq 'ALL')}">
                        			${x.jdl_surat}
                        		</c:when>
                        		<c:when test="${filter eq 'regional' && x.ljb_id == 16}">
                        			${x.jdl_surat}
                        		</c:when>
                        		<c:when test="${filter eq 'akm' && x.akm_flag == 1}">
                        			${x.jdl_surat}
                        		</c:when>
                        		<c:when test="${filter eq 'none' && filterEx eq 'bas' && (x.grant_user eq nik || x.grant_user eq 'ALL')}">
                        			${x.jdl_surat}
                        		</c:when>
                        		<c:when test="${filter eq 'none' && filterEx eq 'regional' && x.ljb_id == '16 '}">
                        			${x.jdl_surat}
                        		</c:when>
                        		<c:when test="${filter eq 'none' && filterEx eq 'akm' && x.akm_flag == 1}">
                        			${x.jdl_surat}
                        		</c:when>
                        		<c:when test="${filter eq 'none' && filterEx eq 'none'}">
                        			${x.jdl_surat}
                        		</c:when>
                        		<c:otherwise></c:otherwise>
                        	</c:choose>
                        </td>
                        <td style="text-align: center; vertical-align: text-top;">
                                <fmt:formatDate value="${x.tgl_edit}" pattern="dd/MM/yyyy hh:mm:ss"/>
                        </td>
                        <td style="text-align: center; vertical-align: text-top;">
                        	<c:choose>
                        		<c:when test="${filter eq 'bas' && (x.grant_user eq nik || x.grant_user eq 'ALL')}">
                        			<input type="button" value="Download" onClick="openPdf('${x.nm_file}');" />
                        		</c:when>
                        		<c:when test="${filter eq 'regional' && x.ljb_id == 16}">
                        			<input type="button" value="Download" onClick="openPdf('${x.nm_file}');" />
                        		</c:when>
                        		<c:when test="${filter eq 'akm' && x.akm_flag == 1}">
                        			<input type="button" value="Download" onClick="openPdf('${x.nm_file}');" />
                        		</c:when>
                        		<c:when test="${filter eq 'none' && filterEx eq 'bas' && (x.grant_user eq nik || x.grant_user eq 'ALL')}">
                        			<input type="button" value="Download" onClick="openPdf('${x.nm_file}');" />
                        		</c:when>
                        		<c:when test="${filter eq 'none' && filterEx eq 'regional' && x.ljb_id == '16 '}">
                        			<input type="button" value="Download" onClick="openPdf('${x.nm_file}');" />
                        		</c:when>
                        		<c:when test="${filter eq 'none' && filterEx eq 'akm' && x.akm_flag == 1}">
                        			<input type="button" value="Download" onClick="openPdf('${x.nm_file}');" />
                        		</c:when>
                        		<c:when test="${filter eq 'none' && filterEx eq 'none'}">
                        			<input type="button" value="Download" onClick="openPdf('${x.nm_file}');" />
                        		</c:when>
                        		<c:otherwise></c:otherwise>
                        	</c:choose>
                        </td>
                        
                    </tr>
                </c:forEach>
                <tr style="background-color: tan;">
                    <td>page ${currPage} of ${lastPage}</td>
                    <td colspan="1" style="text-align: right;">
                    	<a href="#" onclick="eventController('gotoPage');">goto page</a> <input type="text" name="goTo" style="width: 35"value="${currPage}" /> 
                    </td>
                    <td colspan="2" style="text-align: right;">
                        <a href="#" onclick="eventController('first');">first</a>
                        <a href="#" onclick="eventController('prev');">prev</a>
                        <a href="#" onclick="eventController('next');">next</a>
                        <a href="#" onclick="eventController('last');">last</a>
                    </td>
                </tr>
            </table>
            </div>

            <spring:hasBindErrors name="command">
				<div class="errorBox">
					Harap lengkapi informasi berikut:<br/>
					<form:errors path="*" />
				</div>
			</spring:hasBindErrors>

            <br/>
            <c:forEach var="msg" items="${pageMessageList}" varStatus="xt">
                    <span style="vertical-align: text-top; color:brown;">
                            ${msg}
                    </span>
                <br/>
            </c:forEach>

        </fieldset>
        <input type="hidden" id="targetParam"/>
        <input type="hidden" name="currPage" id="currPage"/>
        <input type="hidden" name="lastPage" id="lastPage"/>
        <input type="hidden" name="theEvent" id="theEvent"/>
        <input type="hidden" name="filename" id="filename"/>
		<input type="hidden" name="noSuratTemp" id="noSuratTemp" value="${noSuratTemp}"/>
		<input type="hidden" name="viewSuratTemp" id="viewSuratTemp" value="${viewSuratTemp}"/>
        <input type="hidden" name="judulSuratTemp" id="judulSuratTemp" value="${judulSuratTemp}"/>
        <input type="hidden" name="jenisSuratTemp" id="jenisSuratTemp" value="${jenisSuratTemp}"/>
        <input type="hidden" name="thUpdateTemp" id="thUpdateTemp" value="${thUpdateTemp}"/>
        <input type="hidden" name="lstBlnTemp" id="lstBlnTemp" value="${lstBlnTemp}"/>
    </form:form>

<script type="text/javascript">
	document.getElementById('currPage').value = '${currPage}';
	document.getElementById('lastPage').value = '${lastPage}';
    if( '${pageMessage}' != '' )
    {
        alert( '${pageMessage}' );
    }
</script>
<script type="text/javascript">
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		</script>
		<script type="text/javascript">
		try {
		var pageTracker = _gat._getTracker("UA-8107264-3");
		pageTracker._trackPageview();
		} catch(err) {}</script>	
</div>
		</div>
	</div>
	
</body>

<%@ include file="/include/page/footer.jsp"%>