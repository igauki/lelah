<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript">

function CekRevisi(){
	var a = document.getElementById('jenisupload').value;
	window.location='${path}/common/uploadagen.htm?flag=1&jenisupload='+a;
}

function SubmitForm(path){
		alert(path);
		//window.location='${path }/common/input_referral.htm?nomor=${cmd.nasabah.mns_kd_nasabah}&flag=0';
	}

function ubahStatus(value) {
		
		document.getElementById("idDiv").innerHTML = ""
		input = document.createElement("select");
		input.setAttribute("name","level");
		input.setAttribute("id", "level");
		var opti1;
		var data;
		
					
		if(value == "PERJANJIAN") {
			data = new Array("","PERJANJIAN KEAGENAN PT ASURANSI JIWA SINARMAS MSIG","PERJANJIAN KEAGENAN PT ARTHAMAS KONSULINDO","PERJANJIAN KEAGENAN RO","PERJANJIAN KEAGENAN CRO");
			
			document.getElementById("ket").innerHTML = " JENIS ";
			document.getElementById('tgl').style.visibility = "hidden";
			document.getElementById('idDiv').style.visibility = "visible";
			document.getElementById('viewbutton').style.visibility = "visible";
			document.getElementById('row').style.visibility = "visible";
		}
			else if(value == "STATUS" ) {
			data = new Array("","AKTIF","TIDAK AKTIF");
			document.getElementById("ket").innerHTML = " KONDISI ";
			document.getElementById('tgl').style.visibility = "hidden";
			document.getElementById('idDiv').style.visibility = "visible";
			document.getElementById('viewbutton').style.visibility = "visible";
			document.getElementById('row').style.visibility = "visible";
		}
			else if(value == "TANGGAL") {
			
			document.getElementById('ket').innerHTML = " MULAI DARI ";
			document.getElementById('tgl').style.visibility = "visible";				
			document.getElementById('idDiv').style.visibility = "hidden";
			document.getElementById('viewbutton').style.visibility = "visible";
			document.getElementById('row').style.visibility = "visible";
		}
			else if(value == "") {
			
			document.getElementById('ket').innerHTML = "";	
			document.getElementById('tgl').style.visibility = "hidden";
			document.getElementById('idDiv').style.visibility = "hidden";
			document.getElementById('viewbutton').style.visibility = "hidden";
			document.getElementById('row').style.visibility = "hidden";
		}
			
		
		// buat option baru
		for(var isi in data) {
			opti1 = document.createElement("option");
			opti1.setAttribute("value",data[isi]);
			opti1.appendChild(document.createTextNode(data[isi]));
			input.appendChild(opti1);			
		}
		document.getElementById("idDiv").appendChild(input);
	}

</script>


<BODY onload="document.title='Upload Perjanjian Keagenan'" style="height: 100%;">
	<form method="post" name="formpost" style="text-align: center;" enctype="multipart/form-data">
		<table class="entry2" style="width: 100%" align="left">
			<tr>
				<td>
					<fieldset>
						<legend>Upload Perjanjian Keagenan</legend>
						<table class="entry2">
							<tr>
								<th>Jenis/Judul Perjanjian</th>
								<td align="left"><input type="hidden" disabled="disabled" size="30" id="judul" name="judul" value="Perjanjian Keagenan">Perjanjian Keagenan</td>
							</tr>
							<tr>
								<th>Jenis Perjanjian Keagenan</th>
								<th align="left">
									<select name="jenisupload" id=""jenisupload"" onchange="CekRevisi();">
										<option></option>
										<option value="PERJANJIAN KEAGENAN PT ASURANSI JIWA SINARMAS MSIG">PERJANJIAN KEAGENAN PT ASURANSI JIWA SINARMAS MSIG</option>
										<option value="PERJANJIAN KEAGENAN PT ARTHAMAS KONSULINDO">PERJANJIAN KEAGENAN PT ARTHAMAS KONSULINDO</option>
										<option value="PERJANJIAN KEAGENAN RO">PERJANJIAN KEAGENAN RO</option>
										<option value="PERJANJIAN KEAGENAN CRO">PERJANJIAN KEAGENAN CRO</option>
									</select> 
								</th>
							</tr>
							<tr>
								<th>Revisi ke/Tanggal</th>
								<th align="left"><input style="text-align: center; background-color: #FFFFD2;" disabled="disabled" type="text" size="2" id="revisi" name="revisi" value="${revisi}">
									<script>inputDate('tanggalrevisi', '${status.value}', false, '', 9);</script>
								</th>
							</tr>
							<tr>
								<th>Alasan Penggantian/Revisi</th>
								<th align="left"><input type="text" size="30" name="keterangan" ></th>
							</tr>
							<tr>
								<th>Coding Perjanjian</th>
								<th align="left"><input style="text-align: left; background-color: #FFFFD2;" disabled="disabled" type="text" size="35" id="kode" name="kode" value="${kode}"></th>
							</tr>
							<tr>
								<th>Menggantikan Perjanjian dengan Code</th>
								<th align="left"><input style="text-align: left; background-color: #FFFFD2;" disabled="disabled" type="text" size="35" id="oldkode" name="oldkode" value="${oldkode}"> </th>
							</tr>
							<tr>
								<th>File Perjanjian Keagenan</th>
								<th align="left"><input type="file" name="file1" id="file1" size="48" value="${file1}" /> <span class="info">*Jenis upload file dalam bentuk .pdf</span></th>
							</tr>
							<tr>
								<th></th>
								<th align="left"><input type="submit" name="uploadbutton" value="Upload"></th>
							</tr>
							<tr>
								<td>
									<span class="info">
									*Semua kolom harus diisi, Tidak boleh kosong<br>
									 </span>
								</td>
							</tr>
						</table>
						 
					</fieldset>
					
				</td>
			</tr>
			<tr>
				<td>
					<fieldset>
						<legend>List Dokumen Perjanjian Keagenan</legend>
						<table class="entry2">
							<tr>
								<th align="left">Pencarian Dokumen berdasarkan : 
									<select name="jenis" onchange="ubahStatus(value)">
										<option></option>
										<option value="PERJANJIAN">PERJANJIAN</option>
										<option value="STATUS">STATUS</option>
										<option value="TANGGAL">TANGGAL REVISI</option>
									</select>
									<span id="ket"></span>
									<span id="idDiv">
										<select id="level" name="level" style="visibility: hidden;">
											<option></option>
										</select>
									</span>
									<span id="tgl" style="visibility:hidden;">
										<script>inputDate('tglawal', '${status.value}', false, '', 9);</script>
										S/D
										<script>inputDate('tglakhir', '${status.value}', false, '', 9);</script>
									</span>
								</th>
							</tr>
							<tr id="row" style="visibility: hidden;">
								<th>
									<input style="visibility: hidden;" type="submit" id="viewbutton" name="viewbutton" value="View">
								</th>
							</tr>
						</table>
					</fieldset>
				</td>
			</tr>
			<c:if test="${not empty list}">
			<tr>
				<th>
					<table class="entry2">
						<thead>
							<tr>
								<th width="5%">No</th>
								<th width="40%">Dokumen</th>
								<th width="10%">Tanggal Revisi</th>
								<th>Status</th>
								<th>User</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${list}" var="s" varStatus="st">
								<tr>
									<th>${st.count}</th>
									<th align="left">${s.TEMP_FILENAME}</th>
									<th align="center"><fmt:formatDate value="${s.REVISI_DATE}" pattern="dd/MM/yyyy" /></th>
									<th>${s.STATUS}</th>
									<th>${s.LUS_LOGIN_NAME}</th>
									<td align="center">
										<a style="color: blue;" href="uploadagen.htm?download=download&uploadid=${s.UPLOAD_ID}&jenis=${jenis}&level=${level}&tglawal=${tglawal}&tglakhir=${tglakhir}" name="download" id="download" >Download</a>
										<input type="hidden" name="uploadid" id="uploadid" value="${s.UPLOAD_ID}"> 
									</td>
									
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</th>
			</tr>
			</c:if>
		</table>
		<table class="entry2">
			<c:if test="${not empty errors}">
				<tr>
					<td>
						<div id="error" style="text-transform: none;">ERROR:<br>
							<c:forEach var="err" items="${errors}">
								- <c:out value="${err}" escapeXml="false" />
								<br />
								<script>
									pesan += '${err}\n';
								</script>
							</c:forEach>
						</div>
					</td>
				</tr>
			</c:if>
		</table>
	</form>
</BODY>
<c:if test="${not empty pesan}">
	<script>alert('${pesan}');</script>
</c:if>

<c:if test="${not empty jenisupload}">
	<script type="text/javascript">
		var a = '${jenisupload}';
		//alert(a);
		document.getElementById('jenisupload').value = a;
	</script>
</c:if>

<%@ include file="/include/page/footer.jsp"%>