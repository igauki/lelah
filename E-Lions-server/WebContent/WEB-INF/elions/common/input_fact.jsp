<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>

<script>

function pesan(){
	var pesan=document.formpost.pesan.value;
	if(pesan != ""){
		alert(pesan);
	}
}

function tampilPromptRef(){
	no=parent.document.formpost.nomor.value;
	//alert(no);
	f=1;
	row=prompt("Masukkan row yang akan didelete  ","");
	if(row==null){
		alert("Tidak memasukkan row yang ingin didelete");
		f=0;
	}
	
	if(f==1){
		window.location='${path }/common/input_fact.htm?nomor='+no+'&flag=1&tipe=0&row='+row;
	}
}

function prosesConfirm(){
	var x=window.confirm('Pastikan data yang Anda isi telah lengkap dan benar !\n Transfer ke Analysis ?');
	if(x){
		//alert('masuk');
		document.getElementById('formpost').submit();
	}
}

function tampilPromptChild(){
	no=parent.document.formpost.nomor.value;
	//alert(no);
	f=1;
	row=prompt("Masukkan row yang akan didelete  ","");
	if(row==null){
		alert("Tidak memasukkan row yang ingin didelete");
		f=0;
	}
	
	if(f==1){
		window.location='${path }/common/input_fact.htm?nomor='+no+'&flag=1&tipe=1&row='+row;
	}
}

</script>

<c:if test="${not empty pesan} ">
	<script type="text/javascript">
		alert('${pesan}');
	</script>
</c:if>

<body style="height: 100%;" onload="setupPanes('container1', 'tab${cmd.showTab}'); pesan();">
	<c:choose>
		<c:when test="${cmd.flagAdd eq 2}">
			<form:form id="formpost" name="formpost" commandName="cmd">
				<div class="tab-container" id="container1">
					<ul class="tabs">
						<li>
							<a href="#" onClick="return showPane('pane1', this)" id="tab1">JOB(PEKERJAAN)</a>
							<a href="#" onClick="return showPane('pane2', this)" id="tab2">INTEREST(HOBBY)</a>
							<a href="#" onClick="return showPane('pane3', this)" id="tab3">FAMILY(KELUARGA)</a>
							<a href="#" onClick="return showPane('pane4', this)" id="tab4">FRIENDS(TEMAN-TEMAN)</a>
							<a href="#" onClick="return showPane('pane5', this)" id="tab5">YOU(ANDA)</a>
						</li>
					</ul>
					<fieldset>
						<div class="tab-panes">
							<div id="pane1" class="panes">
								<table class="entry2">
									<tr>
										<th align="left">Dimana Anda Bekerja?</th>
										<th align="left">
										<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="jiffy.mjf_perusahaan" id="perusahaan" maxlength="30" size="50"/>
										</th>
									</tr>
									<tr>
										<th align="left">Berapa Lama?</th>
										<th align="left">
										<form:input disabled="disabled" cssStyle="text-align: center;border-color:#6893B0;" path="jiffy.mjf_lama_kerja" maxlength="2" id="tahun" size="2"/>&nbsp; tahun
										<form:input disabled="disabled" cssStyle="text-align: center;border-color:#6893B0;" path="jiffy.mjf_lama_kerja_bln" maxlength="2" id="bulan" size="2"/>&nbsp; bulan
										</th>
									</tr>
									<tr>
										<th align="left">Apa Jenis Pekerjaan</th>
										<th align="left">
										<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="jiffy.mjf_jn_pekerjaan" id="jobdesk" maxlength="30" size="30"/>
										</th>
									</tr>
									<tr>
										<th align="left">Alamat Tempat Kerja</th>
										<th align="left">
										<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="jiffy.mjf_alamat_ktr" id="alamatkantor" maxlength="60" size="60"/>
										</th>
									</tr>
									<tr>
										<th align="left"></th>
										<th align="left">
											Kota
											<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="jiffy.mjf_kota" id="kota" maxlength="30" size="30"/>&nbsp;&nbsp;&nbsp;										
											KodePos
											<form:input disabled="disabled" cssStyle="text-align: center;border-color:#6893B0;" path="jiffy.mjf_kd_pos_ktr" id="kodepos" maxlength="10" size="10"/>
										</th>
									</tr>
									<tr>
										<th align="left">Rencana Karir Anda?</th>
										<th align="left">
											<form:select disabled="disabled" id="karirplan" path="jiffy.mjf_renc_karir" >
												<form:option label="" value=""/>
												<form:options items="${lstKarir}" itemLabel="value" itemValue="key"/>
											</form:select>
											<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="jiffy.mjf_ket_renc_karir" maxlength="30" size="30"/>
										</th>
									</tr>
									<tr>
										<th align="left">Kapan Hal tersebut Diharapkan?</th>
										<th align="left">
										<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="jiffy.mjf_renc_realisasi" id="realisasi" maxlength="30" size="30"/>
										</th>
									</tr>
									<tr>
										<th align="left">Catatan</th>
										<th align="left">
										<form:textarea disabled="disabled" cssStyle="border-color:#6893B0;"  path="jiffy.mjf_ket_pekerjaan" id="ketpekerjaan" tabindex="6" cols="58" rows="3" onkeyup="textCounter(this, 100); " onkeydown="textCounter(this, 100);"/>
										</th>
									</tr>
								</table>
							</div>
							<div id="pane2" class="panes">
								<table class="entry2">
									<tr>
										<th align="left" width="50%">Apa yang Anda lakukan dalam menghabiskan waktu senggang Anda saat ini?</th>
										<th align="left">
											<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="jiffy.mjf_hobby" id="hobby" maxlength="100" size="80"/>
										</th>
									</tr>
									<tr>
										<th align="left">Mengapa Anda menyukai hal tersebut?</th>
										<th align="left">
											<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="jiffy.mjf_alasan_hobby" id="alasanhobby" maxlength="100" size="80"/>
										</th>
									</tr>
									<tr>
										<th align="left">Seberapa sering Anda melakukannya?</th>
										<th align="left">
											<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="jiffy.mjf_frek_hobby" id="frekhobby" maxlength="30" size="30"/>
										</th>
									</tr>
									<tr>
										<th align="left">Dengan siapa Anda melakukannya?</th>
										<th align="left">
											<form:select disabled="disabled" id="partnerhobby" path="jiffy.mjf_partner_hobby">
												<form:option label="" value=""/>
												<form:options items="${lstPartner}" itemLabel="value" itemValue="key"/>
											</form:select>
											<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="jiffy.mjf_ket_partner" maxlength="30" size="30"/>
										</th>
									</tr>
									<tr>
										<th align="left">Catatan</th>
										<th align="left">
											<form:textarea disabled="disabled" cssStyle="border-color:#6893B0;" path="jiffy.mjf_ket_hobby" id="kethobby" tabindex="6" cols="58" rows="3" onkeyup="textCounter(this, 100); " onkeydown="textCounter(this, 100);" />
										</th>
									</tr>
								</table>
							</div>
							<div id="pane3" class="panes">
								<fieldset>
									<table class="entry2">
										<tr>
											<th align="left" width="25%">Apakah Anda belum menikah?</th>
											<th align="left">
												<label for="menikah_ya"><form:radiobutton disabled="disabled" cssClass="noborder" path="jiffy.mjf_menikah_yt" value="1" id="menikah_ya" />&nbsp; Ya</label>
												<label for="menikah_tdk"><form:radiobutton disabled="disabled" cssClass="noborder" path="jiffy.mjf_menikah_yt" value="0" id="menikah_tdk" />&nbsp; Tidak</label>&nbsp;
												<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="jiffy.mjf_menikah_ket" id="ketmenikah" maxlength="30" size="30"/>
											</th>
										</tr>
										<tr>
											<th align="left">Apakah memiliki Anak?</th>
											<th align="left">
												<label for="anak_ya"><form:radiobutton disabled="disabled" cssClass="noborder" path="jiffy.mjf_anak_yt" value="1" id="anak_ya" />&nbsp; Ya</label>
												<label for="anak_tdk"><form:radiobutton disabled="disabled" cssClass="noborder" path="jiffy.mjf_anak_yt" value="0" id="anak_tdk" />&nbsp; Tidak</label>&nbsp;
												<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="jiffy.mjf_anak_ket" id="ketanak" maxlength="30" size="30"/>
											</th>
										</tr>
										<tr>
											<th align="left">Apakah memiliki Cucu?</th>
											<th align="left">
												<label for="cucu_ya"><form:radiobutton disabled="disabled" cssClass="noborder" path="jiffy.mjf_cucu_yt" value="1" id="cucu_ya" />&nbsp; Ya</label>
												<label for="cucu_tdk"><form:radiobutton disabled="disabled" cssClass="noborder" path="jiffy.mjf_cucu_yt" value="0" id="cucu_tdk" />&nbsp; Tidak</label>&nbsp;
												<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="jiffy.mjf_cucu_ket" id="ketcucu" maxlength="30" size="30"/>
											</th>
										</tr>
										<tr>
											<th align="left">Catatan</th>
											<th align="left">
												<form:textarea disabled="disabled" cssStyle="border-color:#6893B0;" path="jiffy.mjf_ket_family" id="ketfamily" tabindex="6" cols="58" rows="3" onkeyup="textCounter(this, 100); " onkeydown="textCounter(this, 100);" />
											</th>
										</tr>
									</table>
									<table align="left">
										<span class="info" style="left: 100%">
											* Data pribadi dan rencana pendidikan Anak/Cucu diisi pada kotak di bawah.
										</span>
									</table>
								</fieldset>
								<fieldset>
									<legend>Rencana Pendidikan</legend>
										<table class="entry2">
											<thead>
												<tr>
													<th>No</th>
													<th>Relasi</th>
													<th>Nama</th>
													<th>Tanggal Lahir</th>
													<th>Usia</th>
													<th>Pekerjaan</th>
													<th>Rencana Pendidikan</th>
													<th>Dimana?</th>
													<th>Kapan?</th>
													<th>Perkiraan Biaya</th>
													<th>Rencana saat ini</th>
													<th>Pengaturan</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${cmd.children.listChildren}" var="b" varStatus="s">
													<tr>
														<td>${s.count}.</td>
														<td>
															<form:select disabled="disabled" path="children.listChildren[${s.index}].mch_jn_relasi" id="relationchild">
													    		<form:option label="" value=""/>
													    		<form:options items="${lstRelasiChild}" itemLabel="value" itemValue="key"/> 
												    		</form:select>	
														</td>
														<td><form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="children.listChildren[${s.index}].mch_nama" id="namechild" maxlength="30" size="20"/> </td>
														<td><form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="children.listChildren[${s.index}].mch_birth_date" id="birthdatechild" size="10"/> </td>
														<td><form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="children.listChildren[${s.index}].mch_usia" id="agechild" size="5"/> </td>
														<td><form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="children.listChildren[${s.index}].mch_pekerjaan" id="jobchild" size="10"/> </td>
														<td><form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="children.listChildren[${s.index}].mch_renc_studi" id="planstudychild" size="15"/> </td>
														<td><form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="children.listChildren[${s.index}].mch_tempat_studi" id="placechild" size="10"/> </td>
														<td><form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="children.listChildren[${s.index}].mch_waktu_realisasi" id="timechild" size="15"/> </td>
														<td><form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="children.listChildren[${s.index}].mch_biaya" id="costchild" size="13"/> </td>
														<td><form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="children.listChildren[${s.index}].mch_renc_skrg" id="planchild" /> </td>
														<td><form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="children.listChildren[${s.index}].mch_pengaturan" id="managechild" size="15"/> </td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<table class="entry2" align="left">
											<tr>
												<td>
													<span class="info" style="left: 100%">
														* Format tanggal dd/mm/yyyy.
													</span>
												</td>
											</tr>
											<tr>
												<th>
													<input type="hidden" name="AddRowChild" id="AddRowChild" value="AddRow"/>
													<input type="hidden" name="deleteRowChild" id="deleteRowChild" value="DeleteRow" onclick="tampilPromptChild();" />
												</th>
											</tr>
										</table>
								</fieldset>
							</div>
							<div id="pane4" class="panes">
								<table class="entry2">
									<tr>
										<th align="left">Seberapa sering Anda bertemu dengan teman-teman Anda?</th>
										<th align="left">
											<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="jiffy.mjf_frek_ktm_teman" id="frekteman" maxlength="30" size="40"/>
										</th>
									</tr>
									<tr>
										<th align="left">Apakah Anda ingin bertemu mereka lebih sering?</th>
										<th align="left">
											<label for="teman_ya"><form:radiobutton disabled="disabled" cssClass="noborder" path="jiffy.mjf_ingin_sering_yt" value="1" id="teman_ya" />&nbsp; Ya</label>
											<label for="teman_tdk"><form:radiobutton disabled="disabled" cssClass="noborder" path="jiffy.mjf_ingin_sering_yt" value="0" id="teman_tdk" />&nbsp; Tidak</label>&nbsp;
											<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="jiffy.mjf_ingin_sering_ket" id="ketsering" maxlength="30" size="40"/>
										</th>
									</tr>
									<tr>
										<th align="left">Menurut Anda apakah ini akan terjadi ketika Anda pensiun?</th>
										<th align="left">
											<label for="pensiun_ya"><form:radiobutton disabled="disabled" cssClass="noborder" path="jiffy.mjf_real_pensiun_yt" value="1" id="pensiun_ya" />&nbsp; Ya</label>
											<label for="pensiun_tdk"><form:radiobutton disabled="disabled" cssClass="noborder" path="jiffy.mjf_real_pensiun_yt" value="0" id="pensiun_tdk" />&nbsp; Tidak</label>&nbsp;
											<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="jiffy.mjf_real_pensiun_ket" id="ketpensiun" maxlength="30" size="40"/>
										</th>
									</tr>
									<tr>
										<th align="left">Catatan</th>
										<th align="left">
											<form:textarea disabled="disabled" cssStyle="border-color:#6893B0;" path="jiffy.mjf_ket_friend" id="ketfriend" tabindex="6" cols="58" rows="3" onkeyup="textCounter(this, 100); " onkeydown="textCounter(this, 100);" />
										</th>
									</tr>
								</table>
							</div>
							<div id="pane5" class="panes">
								<table class="entry2">
									<tr>
										<th align="left" width="50%">Kami telah mengetahui apa yang akan Anda lakukan - Sekarang kami mengerti berapa besar yang Anda butuhkan untuk tidak bekerja?</th>
										<th align="left">
											Rp.&nbsp;<form:input disabled="disabled" cssStyle="text-align: right;border-color:#6893B0;" path="jiffy.mjf_besar_biaya" id="besarbiaya" maxlength="15" size="20"/>	
										</th>
									</tr>
									<tr>
										<th align="left">Dan berapa banyak yang Anda akan butuhkan untuk melakukan semua hal yang sudah kita diskusikan sejauh ini?</th>
										<th align="left">
											Rp.&nbsp;<form:input disabled="disabled" cssStyle="text-align: right;border-color:#6893B0;" path="jiffy.mjf_kebutuhan" id="kebutuhan" maxlength="15" size="20"/>
										</th>
									</tr>
									<tr>
										<th align="left">Pendidikan Anak-Anak?</th>
										<th align="left">
											Rp.&nbsp;<form:input disabled="disabled" cssStyle="text-align: right;border-color:#6893B0;" path="jiffy.mjf_biaya_pendidikan" id="biayapendidikan" maxlength="15" size="20"/>
										</th>
									</tr>
									<tr>
										<th align="left">Pensiun?</th>
										<th align="left">
											Rp.&nbsp;<form:input disabled="disabled" cssStyle="text-align: right;border-color:#6893B0;" path="jiffy.mjf_biaya_pensiun" id="biayapensiun" maxlength="15" size="20"/>
										</th>
									</tr>
									<tr>
										<th align="left">Waktu senggang/waktu tidak bekerja?</th>
										<th align="left">
											Rp.&nbsp;<form:input disabled="disabled" cssStyle="text-align: right;border-color:#6893B0;" path="jiffy.mjf_biaya_pleasure" id="biayasenggang" maxlength="15" size="20"/>
										</th>
									</tr>
									<tr>
										<th align="left">Catatan</th>
										<th align="left">
											<form:textarea disabled="disabled" cssStyle="border-color:#6893B0;" path="jiffy.mjf_ket_you" id="ketfriend" tabindex="6" cols="58" rows="3" onkeyup="textCounter(this, 100); " onkeydown="textCounter(this, 100);" />
										</th>
									</tr>
								</table>
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend>Referensi</legend>
						<table class="entry2">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama</th>
									<th>Alamat</th>
									<th>Telepon</th>
									<th>Relasi dengan Nasabah</th>
									<th>Alasan</th>
									<th>Bank</th>
									<th>Referrer</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${cmd.relasiNasabah.listRelasiNasabah}" var="b" varStatus="s">
									<tr>
										<td>${s.count}.</td>
										<td><form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="relasiNasabah.listRelasiNasabah[${s.index}].mrn_nama" id="namarelasi" size="20" /></td>
										<td><form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="relasiNasabah.listRelasiNasabah[${s.index}].mrn_alamat" id="alamatrelasi" size="20"/></td>
										<td><form:input disabled="disabled" cssStyle="text-align: center;border-color:#6893B0;" path="relasiNasabah.listRelasiNasabah[${s.index}].mrn_contact_no" id="norelasi" size="20"/></td>
										<td><form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="relasiNasabah.listRelasiNasabah[${s.index}].mrn_ket_relasi" id="ketrelasi" size="20"/></td>
										<td><form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="relasiNasabah.listRelasiNasabah[${s.index}].mrn_alasan" id="alasanrelasi" size="20"/></td>
										<td align="center"><form:checkbox disabled="disabled" cssClass="noBorder" path="relasiNasabah.listRelasiNasabah[${s.index}].mrn_ref_to_bii" value="1" id="biirelasi"/></td>
										<td>
											<form:select disabled="disabled" path="relasiNasabah.listRelasiNasabah[${s.index}].referrer_id" id="idrelasi">
									    		<form:option label="" value=""/>
									    		<form:options items="${lstBii}" itemLabel="value" itemValue="key"/> 
									    	</form:select>	
										</td>
									</tr>	
								</c:forEach>
							</tbody>
						</table>
						<table class="entry2">
							<tr>
								<th>
									<input type="hidden" id="addRowRef" name="addRowRef" value="addRow" />
									<input type="hidden" id="deleteRowRef" name="deleteRowRef" value="deleteRow" onclick="tampilPromptRef();" />
								</th>
							</tr>
						</table>
						
					</fieldset>
					<tr>
						<td colspan="4">
							<c:if test="${submitSuccess eq true}">
								<c:choose>
									<c:when test="${cmd.flagAdd eq 1}">
										<script>
											alert("Berhasil Transfer");
											addOptionToSelect(parent.document, parent.document.formpost.nomor, '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}', '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}');
											window.parent.tes();
										</script>
									</c:when>
									<c:otherwise>
										<script>
											alert("Berhasil Simpan");
											addOptionToSelect(parent.document, parent.document.formpost.nomor, '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}', '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}');
											window.location='${path }/common/input_fact.htm?nomor=${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}';
										</script>
									</c:otherwise>
								</c:choose>
					        	<div id="success">
						        	 Berhasil
						    	</div>
					        </c:if>	
				  			<spring:bind path="cmd.*">
								<c:if test="${not empty status.errorMessages}">
									<div id="error">
										 Informasi:<br>
											<c:forEach var="error" items="${status.errorMessages}">
														 - <c:out value="${error}" escapeXml="false" />
												<br/>
											</c:forEach>
									</div>
								</c:if>									
							</spring:bind>
						 </td>
					</tr>
					<table class="entry2">
						<tr>
							<th>
								<input type="hidden" disabled="disabled" class="button" name="save" id="save" value="save" />
								<input type="hidden" disabled="disabled" name="trans" id="trans" value="trans" onclick="prosesConfirm();">
							</th>
						</tr>
					</table>
					
					<input type="hidden" name="nomor" id="nomor" value="${cmd.jiffy.mns_kd_nasabah}" />
					<input type="hidden" value="${pesan}" name="pesan" />
				</div>
			</form:form>			
		</c:when>
		<c:otherwise>
			<form:form id="formpost" name="formpost" commandName="cmd">
				<div class="tab-container" id="container1">
					<ul class="tabs">
						<li>
							<a href="#" onClick="return showPane('pane1', this)" id="tab1">JOB(PEKERJAAN)</a>
							<a href="#" onClick="return showPane('pane2', this)" id="tab2">INTEREST(HOBBY)</a>
							<a href="#" onClick="return showPane('pane3', this)" id="tab3">FAMILY(KELUARGA)</a>
							<a href="#" onClick="return showPane('pane4', this)" id="tab4">FRIENDS(TEMAN-TEMAN)</a>
							<a href="#" onClick="return showPane('pane5', this)" id="tab5">YOU(ANDA)</a>
						</li>
					</ul>
					<fieldset>
						<div class="tab-panes">
							<div id="pane1" class="panes">
								<table class="entry2">
									<tr>
										<th align="left">Dimana Anda Bekerja?</th>
										<th align="left">
										<form:input cssStyle="border-color:#6893B0;" path="jiffy.mjf_perusahaan" id="perusahaan" maxlength="30" size="50"/>
										</th>
									</tr>
									<tr>
										<th align="left">Berapa Lama?</th>
										<th align="left">
										<form:input cssStyle="text-align: center;border-color:#6893B0;" path="jiffy.mjf_lama_kerja" maxlength="2" id="tahun" size="2"/>&nbsp; tahun
										<form:input cssStyle="text-align: center;border-color:#6893B0;" path="jiffy.mjf_lama_kerja_bln" maxlength="2" id="bulan" size="2"/>&nbsp; bulan
										</th>
									</tr>
									<tr>
										<th align="left">Apa Jenis Pekerjaan</th>
										<th align="left">
										<form:input cssStyle="border-color:#6893B0;" path="jiffy.mjf_jn_pekerjaan" id="jobdesk" maxlength="30" size="30"/>
										</th>
									</tr>
									<tr>
										<th align="left">Alamat Tempat Kerja</th>
										<th align="left">
										<form:input cssStyle="border-color:#6893B0;" path="jiffy.mjf_alamat_ktr" id="alamatkantor" maxlength="60" size="60"/>
										</th>
									</tr>
									<tr>
										<th align="left"></th>
										<th align="left">
											Kota
											<form:input cssStyle="border-color:#6893B0;" path="jiffy.mjf_kota" id="kota" maxlength="30" size="30"/>&nbsp;&nbsp;&nbsp;										
											KodePos
											<form:input cssStyle="text-align: center;border-color:#6893B0;" path="jiffy.mjf_kd_pos_ktr" id="kodepos" maxlength="10" size="10"/>
										</th>
									</tr>
									<tr>
										<th align="left">Rencana Karir Anda?</th>
										<th align="left">
											<form:select id="karirplan" path="jiffy.mjf_renc_karir" >
												<form:option label="" value=""/>
												<form:options items="${lstKarir}" itemLabel="value" itemValue="key"/>
											</form:select>
											<form:input cssStyle="border-color:#6893B0;" path="jiffy.mjf_ket_renc_karir" maxlength="30" size="30"/>
										</th>
									</tr>
									<tr>
										<th align="left">Kapan Hal tersebut Diharapkan?</th>
										<th align="left">
										<form:input cssStyle="border-color:#6893B0;" path="jiffy.mjf_renc_realisasi" id="realisasi" maxlength="30" size="30"/>
										</th>
									</tr>
									<tr>
										<th align="left">Catatan</th>
										<th align="left">
										<form:textarea cssStyle="border-color:#6893B0;"  path="jiffy.mjf_ket_pekerjaan" id="ketpekerjaan" tabindex="6" cols="58" rows="3" onkeyup="textCounter(this, 100); " onkeydown="textCounter(this, 100);"/>
										</th>
									</tr>
								</table>
							</div>
							<div id="pane2" class="panes">
								<table class="entry2">
									<tr>
										<th align="left" width="50%">Apa yang Anda lakukan dalam menghabiskan waktu senggang Anda saat ini?</th>
										<th align="left">
											<form:input cssStyle="border-color:#6893B0;" path="jiffy.mjf_hobby" id="hobby" maxlength="100" size="80"/>
										</th>
									</tr>
									<tr>
										<th align="left">Mengapa Anda menyukai hal tersebut?</th>
										<th align="left">
											<form:input cssStyle="border-color:#6893B0;" path="jiffy.mjf_alasan_hobby" id="alasanhobby" maxlength="100" size="80"/>
										</th>
									</tr>
									<tr>
										<th align="left">Seberapa sering Anda melakukannya?</th>
										<th align="left">
											<form:input cssStyle="border-color:#6893B0;" path="jiffy.mjf_frek_hobby" id="frekhobby" maxlength="30" size="30"/>
										</th>
									</tr>
									<tr>
										<th align="left">Dengan siapa Anda melakukannya?</th>
										<th align="left">
											<form:select id="partnerhobby" path="jiffy.mjf_partner_hobby">
												<form:option label="" value=""/>
												<form:options items="${lstPartner}" itemLabel="value" itemValue="key"/>
											</form:select>
											<form:input cssStyle="border-color:#6893B0;" path="jiffy.mjf_ket_partner" maxlength="30" size="30"/>
										</th>
									</tr>
									<tr>
										<th align="left">Catatan</th>
										<th align="left">
											<form:textarea cssStyle="border-color:#6893B0;" path="jiffy.mjf_ket_hobby" id="kethobby" tabindex="6" cols="58" rows="3" onkeyup="textCounter(this, 100); " onkeydown="textCounter(this, 100);" />
										</th>
									</tr>
								</table>
							</div>
							<div id="pane3" class="panes">
								<fieldset>
									<table class="entry2">
										<tr>
											<th align="left" width="25%">Apakah Anda belum menikah?</th>
											<th align="left">
												<label for="menikah_ya"><form:radiobutton cssClass="noborder" path="jiffy.mjf_menikah_yt" value="1" id="menikah_ya" />&nbsp; Ya</label>
												<label for="menikah_tdk"><form:radiobutton cssClass="noborder" path="jiffy.mjf_menikah_yt" value="0" id="menikah_tdk" />&nbsp; Tidak</label>&nbsp;
												<form:input cssStyle="border-color:#6893B0;" path="jiffy.mjf_menikah_ket" id="ketmenikah" maxlength="30" size="30"/>
											</th>
										</tr>
										<tr>
											<th align="left">Apakah memiliki Anak?</th>
											<th align="left">
												<label for="anak_ya"><form:radiobutton cssClass="noborder" path="jiffy.mjf_anak_yt" value="1" id="anak_ya" />&nbsp; Ya</label>
												<label for="anak_tdk"><form:radiobutton cssClass="noborder" path="jiffy.mjf_anak_yt" value="0" id="anak_tdk" />&nbsp; Tidak</label>&nbsp;
												<form:input cssStyle="border-color:#6893B0;" path="jiffy.mjf_anak_ket" id="ketanak" maxlength="30" size="30"/>
											</th>
										</tr>
										<tr>
											<th align="left">Apakah memiliki Cucu?</th>
											<th align="left">
												<label for="cucu_ya"><form:radiobutton cssClass="noborder" path="jiffy.mjf_cucu_yt" value="1" id="cucu_ya" />&nbsp; Ya</label>
												<label for="cucu_tdk"><form:radiobutton cssClass="noborder" path="jiffy.mjf_cucu_yt" value="0" id="cucu_tdk" />&nbsp; Tidak</label>&nbsp;
												<form:input cssStyle="border-color:#6893B0;" path="jiffy.mjf_cucu_ket" id="ketcucu" maxlength="30" size="30"/>
											</th>
										</tr>
										<tr>
											<th align="left">Catatan</th>
											<th align="left">
												<form:textarea cssStyle="border-color:#6893B0;" path="jiffy.mjf_ket_family" id="ketfamily" tabindex="6" cols="58" rows="3" onkeyup="textCounter(this, 100); " onkeydown="textCounter(this, 100);" />
											</th>
										</tr>
									</table>
									<table align="left">
										<span class="info" style="left: 100%">
											* Data pribadi dan rencana pendidikan Anak/Cucu diisi pada kotak di bawah.
										</span>
									</table>
								</fieldset>
								<fieldset>
									<legend>Rencana Pendidikan</legend>
										<table class="entry2">
											<thead>
												<tr>
													<th>No</th>
													<th>Relasi</th>
													<th>Nama</th>
													<th>Tanggal Lahir</th>
													<th>Usia</th>
													<th>Pekerjaan</th>
													<th>Rencana Pendidikan</th>
													<th>Dimana?</th>
													<th>Kapan?</th>
													<th>Perkiraan Biaya</th>
													<th>Rencana saat ini</th>
													<th>Pengaturan</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${cmd.children.listChildren}" var="b" varStatus="s">
													<tr>
														<td>${s.count}.</td>
														<td>
															<form:select path="children.listChildren[${s.index}].mch_jn_relasi" id="relationchild">
													    		<form:option label="" value=""/>
													    		<form:options items="${lstRelasiChild}" itemLabel="value" itemValue="key"/> 
												    		</form:select>	
														</td>
														<td><form:input cssStyle="border-color:#6893B0;" path="children.listChildren[${s.index}].mch_nama" id="namechild" maxlength="30" size="20"/> </td>
														<td><form:input cssStyle="border-color:#6893B0;" path="children.listChildren[${s.index}].mch_birth_date" id="birthdatechild" size="11"/> </td>
														<td><form:input cssStyle="border-color:#6893B0;" path="children.listChildren[${s.index}].mch_usia" id="agechild" size="5"/> </td>
														<td><form:input cssStyle="border-color:#6893B0;" path="children.listChildren[${s.index}].mch_pekerjaan" id="jobchild" size="10"/> </td>
														<td><form:input cssStyle="border-color:#6893B0;" path="children.listChildren[${s.index}].mch_renc_studi" id="planstudychild" size="15"/> </td>
														<td><form:input cssStyle="border-color:#6893B0;" path="children.listChildren[${s.index}].mch_tempat_studi" id="placechild" size="10"/> </td>
														<td><form:input cssStyle="border-color:#6893B0;" path="children.listChildren[${s.index}].mch_waktu_realisasi" id="timechild" size="15"/> </td>
														<td><form:input cssStyle="border-color:#6893B0;" path="children.listChildren[${s.index}].mch_biaya" id="costchild" size="13"/> </td>
														<td><form:input cssStyle="border-color:#6893B0;" path="children.listChildren[${s.index}].mch_renc_skrg" id="planchild" /> </td>
														<td><form:input cssStyle="border-color:#6893B0;" path="children.listChildren[${s.index}].mch_pengaturan" id="managechild" size="15"/> </td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<table class="entry2" align="left">
											<tr>
												<td>
													<span class="info" style="left: 100%">
														* Format tanggal dd/mm/yyyy.
													</span>
												</td>
											</tr>
											<tr>
												<th>
													<input type="submit" name="AddRowChild" id="AddRowChild" value="AddRow"/>
													<input type="button" name="deleteRowChild" id="deleteRowChild" value="DeleteRow" onclick="tampilPromptChild();" />
												</th>
											</tr>
										</table>
								</fieldset>
							</div>
							<div id="pane4" class="panes">
								<table class="entry2">
									<tr>
										<th align="left">Seberapa sering Anda bertemu dengan teman-teman Anda?</th>
										<th align="left">
											<form:input cssStyle="border-color:#6893B0;" path="jiffy.mjf_frek_ktm_teman" id="frekteman" maxlength="30" size="40"/>
										</th>
									</tr>
									<tr>
										<th align="left">Apakah Anda ingin bertemu mereka lebih sering?</th>
										<th align="left">
											<label for="teman_ya"><form:radiobutton cssClass="noborder" path="jiffy.mjf_ingin_sering_yt" value="1" id="teman_ya" />&nbsp; Ya</label>
											<label for="teman_tdk"><form:radiobutton cssClass="noborder" path="jiffy.mjf_ingin_sering_yt" value="0" id="teman_tdk" />&nbsp; Tidak</label>&nbsp;
											<form:input cssStyle="border-color:#6893B0;" path="jiffy.mjf_ingin_sering_ket" id="ketsering" maxlength="30" size="40"/>
										</th>
									</tr>
									<tr>
										<th align="left">Menurut Anda apakah ini akan terjadi ketika Anda pensiun?</th>
										<th align="left">
											<label for="pensiun_ya"><form:radiobutton cssClass="noborder" path="jiffy.mjf_real_pensiun_yt" value="1" id="pensiun_ya" />&nbsp; Ya</label>
											<label for="pensiun_tdk"><form:radiobutton cssClass="noborder" path="jiffy.mjf_real_pensiun_yt" value="0" id="pensiun_tdk" />&nbsp; Tidak</label>&nbsp;
											<form:input cssStyle="border-color:#6893B0;" path="jiffy.mjf_real_pensiun_ket" id="ketpensiun" maxlength="30" size="40"/>
										</th>
									</tr>
									<tr>
										<th align="left">Catatan</th>
										<th align="left">
											<form:textarea cssStyle="border-color:#6893B0;" path="jiffy.mjf_ket_friend" id="ketfriend" tabindex="6" cols="58" rows="3" onkeyup="textCounter(this, 100); " onkeydown="textCounter(this, 100);" />
										</th>
									</tr>
								</table>
							</div>
							<div id="pane5" class="panes">
								<table class="entry2">
									<tr>
										<th align="left" width="50%">Kami telah mengetahui apa yang akan Anda lakukan - Sekarang kami mengerti berapa besar yang Anda butuhkan untuk tidak bekerja?</th>
										<th align="left">
											Rp.&nbsp;<form:input cssStyle="text-align: right;border-color:#6893B0;" path="jiffy.mjf_besar_biaya" id="besarbiaya" maxlength="15" size="20"/>	
										</th>
									</tr>
									<tr>
										<th align="left">Dan berapa banyak yang Anda akan butuhkan untuk melakukan semua hal yang sudah kita diskusikan sejauh ini?</th>
										<th align="left">
											Rp.&nbsp;<form:input cssStyle="text-align: right;border-color:#6893B0;" path="jiffy.mjf_kebutuhan" id="kebutuhan" maxlength="15" size="20"/>
										</th>
									</tr>
									<tr>
										<th align="left">Pendidikan Anak-Anak?</th>
										<th align="left">
											Rp.&nbsp;<form:input cssStyle="text-align: right;border-color:#6893B0;" path="jiffy.mjf_biaya_pendidikan" id="biayapendidikan" maxlength="15" size="20"/>
										</th>
									</tr>
									<tr>
										<th align="left">Pensiun?</th>
										<th align="left">
											Rp.&nbsp;<form:input cssStyle="text-align: right;border-color:#6893B0;" path="jiffy.mjf_biaya_pensiun" id="biayapensiun" maxlength="15" size="20"/>
										</th>
									</tr>
									<tr>
										<th align="left">Waktu senggang/waktu tidak bekerja?</th>
										<th align="left">
											Rp.&nbsp;<form:input cssStyle="text-align: right;border-color:#6893B0;" path="jiffy.mjf_biaya_pleasure" id="biayasenggang" maxlength="15" size="20"/>
										</th>
									</tr>
									<tr>
										<th align="left">Catatan</th>
										<th align="left">
											<form:textarea cssStyle="border-color:#6893B0;" path="jiffy.mjf_ket_you" id="ketfriend" tabindex="6" cols="58" rows="3" onkeyup="textCounter(this, 100); " onkeydown="textCounter(this, 100);" />
										</th>
									</tr>
								</table>
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend>Referensi</legend>
						<table class="entry2">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama</th>
									<th>Alamat</th>
									<th>Telepon</th>
									<th>Relasi dengan Nasabah</th>
									<th>Alasan</th>
									<th>Bank</th>
									<th>Referrer</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${cmd.relasiNasabah.listRelasiNasabah}" var="b" varStatus="s">
									<tr>
										<td>${s.count}.</td>
										<td><form:input cssStyle="border-color:#6893B0;" path="relasiNasabah.listRelasiNasabah[${s.index}].mrn_nama" id="namarelasi" size="20" /></td>
										<td><form:input cssStyle="border-color:#6893B0;" path="relasiNasabah.listRelasiNasabah[${s.index}].mrn_alamat" id="alamatrelasi" size="20"/></td>
										<td><form:input cssStyle="text-align: center;border-color:#6893B0;" path="relasiNasabah.listRelasiNasabah[${s.index}].mrn_contact_no" id="norelasi" size="20"/></td>
										<td><form:input cssStyle="border-color:#6893B0;" path="relasiNasabah.listRelasiNasabah[${s.index}].mrn_ket_relasi" id="ketrelasi" size="20"/></td>
										<td><form:input cssStyle="border-color:#6893B0;" path="relasiNasabah.listRelasiNasabah[${s.index}].mrn_alasan" id="alasanrelasi" size="20"/></td>
										<td align="center"><form:checkbox cssClass="noBorder" path="relasiNasabah.listRelasiNasabah[${s.index}].mrn_ref_to_bii" value="1" id="biirelasi"/></td>
										<td>
											<form:select path="relasiNasabah.listRelasiNasabah[${s.index}].referrer_id" id="idrelasi">
									    		<form:option label="" value=""/>
									    		<form:options items="${lstBii}" itemLabel="value" itemValue="key"/> 
									    	</form:select>	
										</td>
									</tr>	
								</c:forEach>
							</tbody>
						</table>
						<table class="entry2">
							<tr>
								<th>
									<input type="submit" id="addRowRef" name="addRowRef" value="addRow" />
									<input type="button" id="deleteRowRef" name="deleteRowRef" value="deleteRow" onclick="tampilPromptRef();" />
								</th>
							</tr>
						</table>
						
					</fieldset>
					<tr>
						<td colspan="4">
							<c:if test="${submitSuccess eq true}">
								<c:choose>
									<c:when test="${cmd.flagAdd eq 1}">
										<script>
											alert("Berhasil Transfer");
											addOptionToSelect(parent.document, parent.document.formpost.nomor, '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}', '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}');
											window.parent.tes();
										</script>
									</c:when>
									<c:otherwise>
										<script>
											alert("Berhasil Simpan");
											addOptionToSelect(parent.document, parent.document.formpost.nomor, '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}', '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}');
											window.location='${path }/common/input_fact.htm?nomor=${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}';
										</script>
									</c:otherwise>
								</c:choose>
					        	<div id="success">
						        	 Berhasil
						    	</div>
					        </c:if>	
				  			<spring:bind path="cmd.*">
								<c:if test="${not empty status.errorMessages}">
									<div id="error">
										 Informasi:<br>
											<c:forEach var="error" items="${status.errorMessages}">
														 - <c:out value="${error}" escapeXml="false" />
												<br/>
											</c:forEach>
									</div>
								</c:if>									
							</spring:bind>
						 </td>
					</tr>
					<table class="entry2">
						<tr>
							<th>
								<input type="submit" class="button" name="save" id="save" value="save" />
								<input type="button" name="trans" id="trans" value="trans" onclick="prosesConfirm();">
							</th>
						</tr>
					</table>
					
					<input type="hidden" name="nomor" id="nomor" value="${cmd.jiffy.mns_kd_nasabah}" />
					<input type="hidden" value="${pesan}" name="pesan" />
				</div>
			</form:form>
		</c:otherwise>
	</c:choose>
	
</body>


<%@ include file="/include/page/footer.jsp"%>