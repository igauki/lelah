<%@ include file="/include/page/header.jsp"%>
<script>
	function backToParent(nomor){
		//alert(nomor);
		//alert(self.opener.document.getElementById('nomor').value);
		//addOptionToSelect(self.opener.document, self.opener.document.formpost.nomor, nomor, nomor);
		self.opener.document.getElementById('nomor').value = nomor;
		//parent.window.document.getElementById('nomor').value = nomor;
		//window.parent.document.getElementById('nomor').value = nomor;
		self.opener.document.getElementById('infoFrame').src='${path }/common/input_referral.htm?nomor='+nomor+'&flag=0';
		//window.parent.location ='${path }/common/input_referral.htm?nomor='+nomor+'&flag=0';
		window.close();
	}
	

</script>
<BODY onload="setFocus('nomor'); document.title='PopUp :: Cari Nasabah';" style="height: 100%;">
<form method="post" name="formpost" >
	<fieldset>
		<legend>Cari Data Nasabah</legend>
		<table class="entry2">
			<tr>
				<th>Cari Nasabah</th>
				<td>
					<select name="tipe" >
						<option value="1" <c:if test="${tipe eq 1 }" >selected</c:if>>Kode Nasabah</option>
						<option value="2" <c:if test="${tipe eq 2 }" >selected</c:if>>No Referrel</option>
					</select>
					<input type="text" name="nomor" maxLength="20" size="20" value="${nomor}" />
					<input type="submit" name="btnCari" value="Cari">
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<table class="simple">
						<thead>
							<tr>
								<th>Kode Nasabah</th>
								<th>No Referral</th>
								<th>Nama Nasabah</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="x" items="${lsCariNasabah}" varStatus="xt">
								<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
									onclick="backToParent('${x.mns_no_ref}~${x.mns_kd_nasabah}');">
									<td>${x.mns_kd_nasabah }</td>
									<td>${x.mns_no_ref}</td>
									<td>${x.mns_nama}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<th colspan="2"><input type="button" name="btnClose" value="Close" onClick="window.close()"></th>
				<input type="hidden" name="flag" value="1" >
			</tr>
		</table>
	</fieldset>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>