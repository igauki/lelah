<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		
		/* ------------------------------------------------------------------------------ */

		// user memilih folder1, maka tampilkan data di folder2		
		$("#folder1").change(function() {
			//kosongkan data dulu
			$("#folder2, #folder3").empty();
			
			$("<option/>").val("").html("Silahkan tunggu...").appendTo("#folder2, #folder3");
			
			var url = "${path}/common/menu.htm?frame=download_per_user&json=1&folder=" +$("#folder1").val();
			$.getJSON(url, function(result) {
				$("#folder2, #folder3").empty();
				if(result.length == 0){
					$("<option/>").val("").html("Tidak ada data").appendTo("#folder2, #folder3");
				}else{
					$.each(result, function() {
						$("<option/>").val(this.key).html(this.value).appendTo("#folder2");
					});
				}
			});
		});

		// user memilih folder2, maka tampilkan data di folder3
		$("#folder2").change(function() {
			//kosongkan data dulu
			$("#folder3").empty();
			$("<option/>").val("").html("Silahkan tunggu...").appendTo("#folder3");
			
			var url2 = "${path}/common/menu.htm?frame=download_per_user&json=2&folder=" + $("#folder2").val();
			$.getJSON(url2, function(result2) {
				$("#folder3").empty();
				if(result2.length == 0){
					$("<option/>").val("").html("Tidak ada data").appendTo("#folder3");
				}else{
					$.each(result2, function() {
						$("<option/>").val(this.desc).html(this.key).appendTo("#folder3");
					});
				}
			});
		});

		$("#folder3").change(function() {
			$("#fileName").val($("#folder3 option:selected").text());
			$("#fileDir").val($("#folder3").val());
		});		
		
	});
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

</style>

<body>

<form id="formPost" name="formPost" method="post" target="_blank">
<fieldset class="ui-widget ui-widget-content">
	<legend class="ui-widget-header ui-corner-all"><div>Download File per User ${sessionScope.currentUser.name}</div></legend>

	<div class="rowElem">
		<label>Silahkan Pilih:</label>
		<select size="10" name="folder1" id="folder1" title="Silahkan pilih jenis report">
			<c:forEach var="c" items="${listFolder}" varStatus="s">
				<option value="${c.key}">${c.value}</option>
			</c:forEach>
		</select><em> &raquo; </em>
		<select size="10" name="folder2" id="folder2" title="Silahkan pilih periode">
			<option>&laquo; Silahkan pilih jenis report terlebih dahulu</option>
		</select><em> &raquo; </em>
		<select size="10" name="folder3" id="folder3" title="Silahkan pilih file, lalu tekan tombol DOWNLOAD">
			<option>&laquo; Silahkan pilih periode terlebih dahulu</option>
		</select>
		<input type="hidden" name="fileName" id="fileName">
		<input type="hidden" name="fileDir" id="fileDir">
	</div>
	
	<div class="rowElem">
		<label></label>
		<input type="submit" name="download" id="download" value="Download File">
	</div>
</fieldset>
</form>

</body>
</html>