<!DOCTYPE>
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />

<html>
<head>
	<title>PT Asuransi Jiwa SinarmasMSIGLife</title>
	
	<link rel="stylesheet" href="${path}/include/css/jqueryFileTree.css" />
	
	<script src="${path}/include/js/treeview/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="${path}/include/js/treeview/jqueryFileTree.js"></script>
    <script type="text/javascript" src="${path}/include/js/default.js"></script>
   
   <style type="text/css">
		BODY,
		HTML {
			padding: 0px;
			margin: 0px;
		}
		BODY {
			font-family: Verdana, Arial, Helvetica, sans-serif;
			font-size: 11px;
			background: #EEE;
			padding: 15px;
		}
		
		H1 {
			font-family: Georgia, serif;
			font-size: 20px;
			font-weight: normal;
		}
		
		H2 {
			font-family: Georgia, serif;
			font-size: 16px;
			font-weight: normal;
			margin: 0px 0px 10px 0px;
		}
		
		.example {
			float: left;
			margin: 15px;
		}
		
		.demo {
			width: 100%;
			height: 470px;
			border-top: solid 1px #BBB;
			border-left: solid 1px #BBB;
			border-bottom: solid 1px #FFF;
			border-right: solid 1px #FFF;
			background: #FFF;
			overflow: scroll;
			padding: 5px;
		}
		
	</style>
  <script type="text/javascript">
			
		$(document).ready( function() {
			$('#fileTreeDemo_1').fileTree({ 
				root: '${directory}',
				multiFolder: false,
				script: '${path}/include/connectors/jqueryFileTree.jsp' }, 
			function(file) { 
				//alert(file);
				popWin('${path}/common/util.htm?window=downloadFile&dir='+file, 400, 550);
			});
						
		});
		hideLoadingMessage();
	</script>
    
	
	</head>
	<body>
	
	<div id="main">
	
	<h4>${aplikasi}</h4>
	<div class="example">
        <h2> F i l e</h2>
        <div id="fileTreeDemo_1" class="demo">
        	
        </div>
    </div>
	
	</div>
 
</body></html>