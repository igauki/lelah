<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript">
	hideLoadingMessage();
	function tampil(timpal){
		document.getElementById('consoleFrame').src='${path}/common/console.htm?window='+timpal;
	}
</script>
<body onload="setFrameSize('consoleFrame', 85);" onresize="setFrameSize('consoleFrame', 85);" style="height: 100%;">
	<form name="frmParam" method="post">
		<div class="tabcontent">
			<table class="entry2" style="width: 98.4%;">
				<tr>
					<th rowspan="2">Console</th>
					<td>
						<input type="button" value="Login List" name="login" onclick="tampil('login');">
						<input type="button" value="Report Maintenance" name="report" onclick="tampil('report');">
						<input type="button" value="Branch Admin List" name="login" onclick="tampil('branch');">
					</td>
				</tr>
				<tr>
					<td>
						<input type="button" value="Other Tools" name="other" onclick="tampil('other');">
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<iframe src="${path}/common/console.htm?window=login" name="consoleFrame" id="consoleFrame" width="100%" style="width: 100%;">
							Please Wait...
						</iframe>
					</td>
				</tr>
			</table>
		</div>
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>