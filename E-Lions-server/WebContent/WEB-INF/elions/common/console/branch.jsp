<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path}/include/js/disableKeys.js"></script><!-- Disable Some Keys -->
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript">
	hideLoadingMessage();
</script>
</head>
<body style="height: 100%;" onload='setupPanes("container1", "tab1");'>
<div id="contents">
	<form method="post" name="formpost">
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li><a href="#" onClick="return showPane('pane1', this)" id="tab1">Branch Admin List</a></li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<table class="entry2">
						<tr>
							<th>Department</th>
							<td>
								<select name="lde_id" onchange="document.formpost.submit();">
									<option value="">-- ALL --</option>
									<c:forEach items="${daftarDept}" var="d">
										<option value="${d.LDE_ID}" <c:if test="${lde_id eq d.LDE_ID}">selected</c:if>>${d.LDE_DEPT}</option>
									</c:forEach>
								</select>
							</td>
							<th>Cabang</th>
							<td>
								<select name="lca_id" onchange="document.formpost.submit();">
									<option value="">-- ALL --</option>
									<c:forEach items="${daftarCabang}" var="d">
										<option value="${d.lca_id}" <c:if test="${lca_id eq d.lca_id}">selected</c:if>>${d.cabang}</option>
									</c:forEach>
								</select>
							</td>
							<th>Show</th>
							<td>
								<select name="onoff" onchange="document.formpost.submit();">
									<option value="1" <c:if test="${onoff eq 1}">selected</c:if>>Online Only</option>
									<option value="0" <c:if test="${onoff eq 0}">selected</c:if>>Online and Offline</option>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan="6">
								<table class="simple">
									<thead>
										<tr>
											<td>Status</td>
											<td>Branch</td>
											<td>Department</td>
											<td>Division</td>
											<td colspan="2">User</td>
											<td>IP Address</td>
											<td>Login Time</td>
											<td>SPAJ Processed Today</td>
										</tr>
									</thead>
									<tbody>
								         <c:forEach var="user" items="${daftarUser}">
											<tr>
												<td nowrap="nowrap">
													<c:choose>
														<c:when test="${user.online eq 1}">
															<span style="color: green;">Online</span>
														</c:when>
														<c:otherwise>
															<span style="color: red;">Offline</span>
														</c:otherwise>
													</c:choose>
												</td>
												<td nowrap="nowrap">${user.cabang}</td>
												<td nowrap="nowrap">${user.dept}</td>
												<td nowrap="nowrap">${user.division}</td>
												<td nowrap="nowrap">${user.lus_id}</td>
												<td nowrap="nowrap"><a href="${path}/common/console.htm?window=activity&lus_id=${user.lus_id}">${user.name}</a></td>
												<td nowrap="nowrap">${user.ip}</td>
												<td nowrap="nowrap"><fmt:formatDate value="${user.loginTime}" pattern="dd/MM/yyyy (hh:mm:ss)"/></td>
												<td nowrap="nowrap">${user.jumlahSpaj}</td>
											</tr>
								         </c:forEach>
							         </tbody>
								</table>
								<input type="hidden" name="lus_id">
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</form>
</div>
</body>
<%@ include file="/include/page/footer.jsp"%>