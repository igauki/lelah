<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path}/include/js/disableKeys.js"></script><!-- Disable Some Keys -->
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript">
	hideLoadingMessage();
</script>
</head>
<body style="height: 100%;" onload='setupPanes("container1", "tab1");'>
<div id="contents">
	<form method="post" name="formpost">
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li><a href="#" onClick="return showPane('pane1', this)" id="tab1">Activity Detail</a></li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<table class="entry2" style="width: auto;">
						<tr>
							<th>Login Time</th>
							<td><fmt:formatDate value="${user.loginTime}" pattern="dd/MM/yyyy (hh:mm:ss)"/></td>
						</tr>
						<tr>
							<th>Total SPAJ Processed</th>
							<td>${jumlahSpaj}</td>
						</tr>
						<tr>
							<td colspan="2">
								<table class="simple">
									<thead>
										<tr>
											<td>Waktu</td>
											<td>Spaj</td>
											<td>URL</td>
										</tr>
									</thead>
									<tbody>
								         <c:forEach var="d" items="${detil}">
											<tr>
												<td nowrap="nowrap"><fmt:formatDate value="${d.MSAH_DATE}" pattern="dd/MM/yyyy (hh:mm:ss)"/></td>
												<td nowrap="nowrap">${d.MSAH_SPAJ}</td>
												<td nowrap="nowrap">${d.MSAH_URI}</td>
											</tr>
								         </c:forEach>
							         </tbody>
								</table>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</form>
</div>
</body>
<%@ include file="/include/page/footer.jsp"%>