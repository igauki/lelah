<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path}/include/js/disableKeys.js"></script><!-- Disable Some Keys -->
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript">
	hideLoadingMessage();

	function konfirmasi(lusid){
		if(confirm('Awas salah tendang!?')){
			document.formpost.lus_id.value=lusid;
			return true;
		}
		return false;
	}
	
	function ping(p){
		ajaxManager.ping(p, {
		  callback:function(str) {
			DWRUtil.useLoadingMessage();
			alert(str);
		   },
		  timeout:60000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
	
</script>
</head>
<body style="height: 100%;" onload='setupPanes("container1", "tab1");'>
<div id="contents">
	<form method="post" name="formpost">
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li><a href="#" onClick="return showPane('pane1', this)" id="tab1">User List</a></li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<table class="entry2">
						<tr>
							<th colspan="2">User List</th>
						</tr>
						<tr>
							<td colspan="2">
								<table class="simple">
									<thead>
										<tr>
											<td>Fungsi</td>
											<td>Branch</td>
											<td>Department</td>
											<td>Division</td>
											<td colspan="2">User</td>
											<td>IP Address</td>
											<!-- 
											<td>Session ID</td>
											<td>Session Creation Time</td>
											-->
											<td>Login Time</td>
										</tr>
									</thead>
									<tbody>
								         <c:forEach var="user" items="${users}">
											<tr>
												<td nowrap="nowrap">
													<input type="submit" name="tendang" value="Kick" onclick="return konfirmasi('${user.value.user.lus_id}');">
													<input type="button" value="Ping" onclick="ping('${user.value.user.ip}');">
												</td>
												<td nowrap="nowrap">${user.value.user.cabang}</td>
												<td nowrap="nowrap">${user.value.user.dept}</td>
												<td nowrap="nowrap">${user.value.user.division}</td>
												<td nowrap="nowrap">${user.value.user.lus_id}</td>
												<td nowrap="nowrap">${user.value.user.name}</td>
												<td nowrap="nowrap">${user.value.user.ip}</td>
												<td nowrap="nowrap"><fmt:formatDate value="${user.value.user.loginTime}" pattern="dd/MM/yyyy (hh:mm:ss)"/></td>
											</tr>
								         </c:forEach>
							         </tbody>
								</table>
								<br/>
								<input type="submit" name="tendangYangMati" value="Kick All Offline Users" onclick="return confirm('Tendang Semua?');">
								<span class="info">Note: Force Logout akan me-LOGOUT user secara paksa, sehingga akan otomatis redirect ke halaman depan</span>
								<input type="hidden" name="lus_id">
								<c:if test="${not empty daftar}">
									<br/><br/>Hasil:<br/>
									<c:forEach items="${daftar}" var="d">
										${d}<br/>
									</c:forEach>
								</c:if>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</form>
</div>
</body>
<%@ include file="/include/page/footer.jsp"%>