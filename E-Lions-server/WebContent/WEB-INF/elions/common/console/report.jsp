<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path}/include/js/disableKeys.js"></script><!-- Disable Some Keys -->
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript">
	hideLoadingMessage();
</script>
</head>
<body style="height: 100%;" onload='setupPanes("container1", "tab1");'>
<div id="contents">
	<form method="post" name="formpost">
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li><a href="#" onClick="return showPane('pane1', this)" id="tab1">Jasper Reports</a></li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<table class="entry2">
						<tr>
							<th>Compile Jasper Report</th>
							<td>
								<select size="30" name="selectedReport" style="text-transform: none;" multiple="multiple">
									<c:forEach var="r" items="${reports }">
										<option value="${r}">${r}</option>
									</c:forEach>
								</select><br>
								<input type="submit" name="recompileSingleReport" value="Compile">
								<input type="submit" name="recompileReports" value="Compile ALL!" onclick="return confirm('Recompile All Jasper Reports?');">
							</td>
						</tr>
						<c:if test="${not empty compiledReports}">
							<tr>
								<th>Status Compile : </th>
								<td>
									<c:forEach items="${compiledReports}" var="a" varStatus="st">
										${a} - 
										<c:choose>
											<c:when test="${compiledStatus[st.index] eq \"SUCCESS\"}">
												<span style="color: green;">${compiledStatus[st.index]}</span>
											</c:when>
											<c:otherwise>
												<span style="color: red;">${compiledStatus[st.index]}</span>
											</c:otherwise>
										</c:choose>
										<br/>
									</c:forEach>
								</td>
							</tr>
						</c:if>
					</table>
				</div>
			</div>
		</div>
	</form>
</div>
</body>
<%@ include file="/include/page/footer.jsp"%>