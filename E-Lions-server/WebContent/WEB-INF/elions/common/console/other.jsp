<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path}/include/js/disableKeys.js"></script><!-- Disable Some Keys -->
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript">
	hideLoadingMessage();
</script>
</head>
<body style="height: 100%;" onload='setupPanes("container1", "tab1");'>
<div id="contents">
	<form method="post" name="formpost">
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li><a href="#" onClick="return showPane('pane1', this)" id="tab1">Other Tools</a></li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<table class="entry2">
						<tr>
							<th>Reset ibatisCache</th>
							<td>
								<input type="submit" name="resetIbatisCache" value="Reset iBatis Cache" onclick="return confirm('Reset iBATIS Cache?');" 
									accesskey="R" onmouseover="return overlib('Alt-R > Melakukan reset terhadap semua cache dari query sql', AUTOSTATUS, WRAP);" onmouseout="nd();">
							</td>
						</tr>
						<tr>
							<th>Application Status</th>
							<td>
								<input type="submit" name="block" value="Block" <c:if test="${appStatus eq \"0\"}">disabled</c:if> 
									accesskey="B" onmouseover="return overlib('Alt-B > Memblok akses ke seluruh aplikasi.', AUTOSTATUS, WRAP);" onmouseout="nd();"
									onclick="return confirm('Fungsi ini memblok SEMUA AKSES ke aplikasi E-LIONS, lanjutkan?');">
								<input type="submit" name="unblock" value="Unblock" <c:if test="${appStatus eq \"1\"}">disabled</c:if>
									accesskey="U" onmouseover="return overlib('Alt-U > Membuka blok akses ke seluruh aplikasi.', AUTOSTATUS, WRAP);" onmouseout="nd();">
							</td>
						</tr>
						<tr>
							<th>Reload ekalife.properties</th>
							<td>
								<input type="submit" name="reloadProperties" value="Reload Properties" onclick="return confirm('Reload properties?');" 
									accesskey="R" onmouseover="return overlib('Alt-P > Melakukan reload terhadap file ekalife.properties', AUTOSTATUS, WRAP);" onmouseout="nd();">
							</td>
						</tr>
						<tr>
							<th>View ekalife.properties</th>
							<td>
								<textarea rows="32" cols="150">${props}</textarea>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</form>
</div>
</body>
<%@ include file="/include/page/footer.jsp"%>