<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>

<script>

	function RedirectButton(lspd_id){
		no=parent.document.formpost.nomor.value;
		if(lspd_id == '3'){//fact find
			popWin('${path }/common/input_fact.htm?flag=2&nomor='+no, 600, 800);
		}else if(lspd_id == '4'){//analyst
			popWin('${path }/common/input_analyst.htm?flag=2&nomor='+no, 600, 800);
		}else if(lspd_id == '5'){//recommend
			popWin('${path }/common/input_recommend.htm?flag=2&nomor='+no, 600, 800);
		}else if(lspd_id == '6'){//closing non_completion
			popWin('${path }/common/input_closingnon.htm?flag=2&nomor='+no, 600, 800);
		}
	}
	
	function ClosingButton(){
		no=parent.document.formpost.nomor.value;
		saran = document.getElementById('nasabah.mns_ok_saran').value;
		if(saran == '1'){
			popWin('${path }/common/input_closing.htm?flag=2&nomor='+no, 600, 800);
		}else {
			popWin('${path }/common/input_closingnon.htm?flag=2&nomor='+no, 600, 800);
		}
	}
	
</script>

<c:if test="${not empty pesan} ">
	<script type="text/javascript">
		alert('${pesan}');
	</script>
</c:if>

<body style="height: 100%;" onload="setupPanes('container1', 'tab${cmd.showTab}');">
	<form:form id="formpost" name="formpost" commandName="cmd">
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Lead Gen</a>
					<a href="#" onClick="return showPane('pane2', this)" id="tab2">Activity</a>
				</li>
			</ul>
			<table class="entry2">
				<tr>
					<th width="10%" align="left">View</th>
					<th align="left">
						<c:choose>
							<c:when test="${cmd.nasabah.mns_kd_nasabah eq null}">
								<input type="button" disabled="disabled" name="fact_find" id="fact_find" value="Fact_Find" onclick="RedirectButton(3);" />
								<input type="button" disabled="disabled" name="analyst" id="analyst" value="Analyst" onclick="RedirectButton(4);"/>
								<input type="button" disabled="disabled" name="recommend" id="recommend" value="Recommend" onclick="RedirectButton(5);"/>
								<input type="button" disabled="disabled" name="closing" id="closing" value="Closing" onclick="ClosingButton();" />
							</c:when>
							<c:when test="${cmd.nasabah.mns_ok_saran ne 1}">
								<input type="button" disabled="disabled" name="fact_find" id="fact_find" value="Fact_Find" onclick="RedirectButton(3);" />
								<input type="button" disabled="disabled" name="analyst" id="analyst" value="Analyst" onclick="RedirectButton(4);"/>
								<input type="button" disabled="disabled" name="recommend" id="recommend" value="Recommend" onclick="RedirectButton(5);"/>
								<input type="button" name="closing" id="closing" value="Closing" onclick="ClosingButton();" />
							</c:when>
							<c:otherwise>
								<input type="button" name="fact_find" id="fact_find" value="Fact_Find" onclick="RedirectButton(3);" />
								<input type="button" name="analyst" id="analyst" value="Analyst" onclick="RedirectButton(4);" />
								<input type="button" name="recommend" id="recommend" value="Recommend" onclick="RedirectButton(5);" />
								<input type="button" name="closing" id="closing" value="Closing" onclick="ClosingButton();" />
							</c:otherwise>
						</c:choose>
					</th>
				</tr>
			</table>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<fieldset>
						<table class="entry2">
							<tr>
								<th>Kode Nasabah : ${cmd.nasabah.mns_kd_nasabah }</th>
								
							</tr>
							<tr>
								<th>Cabang :  
									<form:select disabled="disabled" path="nasabah.kode" onchange="pil('kode')">
										<form:option label="" value=""/>
										<form:options items="${lsCabBii}" itemLabel="value" itemValue="key" />
									</form:select> 
								</th>
								<th>${cmd.nasabah.cabang_detail}</th>
							</tr>
							<tr>
								<th>BFA :
									<form:select disabled="disabled" path="nasabah.msag_id" onchange="pil('bfa')">
										<form:option label="" value=""/>
										<form:options items="${lsBfa}" itemLabel="nama_bfa" itemValue="msag_id" />
									</form:select> 
								</th>
								<th>${cmd.nasabah.bfa_detail}</th>
							</tr>
							<tr>
								<th colspan="2">
									<fieldset>
									<legend>Informasi Referral</legend>
										<table class="entry2">
											<tr>
												<th width="50%">
													No Referral  : ${cmd.nasabah.mns_no_ref }
												</th>
												<th width="50%" style="border-color: #6893B0">
													Tanggal : <script>inputDate('tgl', '${cmd.nasabah.s_mns_tgl_app}', false, '', 9);</script>
												</th>
											</tr>
											<tr>
												<th>
													<fieldset>
														<legend>Referral</legend>
														<table class="entry2">
															<tr>
																<th>Nama</th>
																<td>
																	<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="nasabah.mns_nama" size="30"/>
																</td>
															</tr>
															<tr>
																<th>DCIF</th>
																<td>
																	<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="nasabah.mns_dcif" size="30"/>
																</td>
															</tr>
															<tr>
																<th>Tipe Nasabah</th>
																<td>
																	<form:radiobutton disabled="disabled" cssClass="noborder" path="nasabah.mns_tipe_nasabah" id="nasabah.mns_tipe_nasabah" value="1"/>
																	Baru
																	<form:radiobutton disabled="disabled" cssClass="noborder" path="nasabah.mns_tipe_nasabah" id="nasabah.mns_tipe_nasabah" value="0"/>
																	Lama
																</td>
															</tr>
														</table>
													</fieldset>
												</th>
												<th>
													<fieldset>
														<legend>Telepon</legend>
														<table class="entry2">
															<tr>
																<th>Kantor</th>
																<td width="10">
																	<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="nasabah.mns_area_kantor" size="5" maxlength="4"/>
																</td>
																<td>
																	<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="nasabah.mns_tlpkantor" size="20" maxlength="15"/>
																</td>
															</tr>
															<tr>
																<th>Rumah</th>
																<td width="10">
																	<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="nasabah.mns_area_rumah" size="5" maxlength="4"/>
																</td>
																<td>
																	<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="nasabah.mns_tlprumah" size="20" maxlength="15"/>
																</td>
															</tr>
															<tr>
																<th>HP</th>
																<td/>
																<td>
																	<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="nasabah.mns_hp" size="20" maxlength="15"/>
																</td>
															</tr>
														</table>
													</fieldset>
												</th>
											</tr>	
											<tr>
												<th colspan="2">
													<fieldset>
														<legend>Telepon</legend>
														<table class="entry2" >
															<tr>
																<td colspan="3">
																	Waktu Yang Tepat Untuk Dihubungi
																</td>
															</tr>
															<tr>
																<td colspan="3">
																	Jika diminta oleh Nasabah atau lainnya, Bagaimana Nasabah Mengetahui mengenal layanan ini?
																</td>
															</tr>
															<tr>
																<td>
																	<form:radiobutton disabled="disabled" cssClass="noborder" path="nasabah.mns_layanan" id="nasabah.mns_layanan" value="1"/>
																	Direct Marketing
																</td>
																<td>
																	<form:radiobutton disabled="disabled" cssClass="noborder" path="nasabah.mns_layanan" id="nasabah.mns_layanan" value="3"/>
																	Call Centre BII
																</td>
																<td>	
																	<form:radiobutton disabled="disabled" cssClass="noborder" path="nasabah.mns_layanan" id="nasabah.mns_layanan" value="5"/>
																	Teman/Saudara
																</td>
															</tr>
															<tr>
																<td>
																	<form:radiobutton disabled="disabled" cssClass="noborder" path="nasabah.mns_layanan" id="nasabah.mns_layanan" value="2"/>
																	Website
																</td>
																<td>	
																	<form:radiobutton disabled="disabled" cssClass="noborder" path="nasabah.mns_layanan" id="nasabah.mns_layanan" value="4"/>
																	Call Centre Ekalife
																</td>
															</tr>
															<tr>
																<th colspan="2">
																	Apakah Tujuan Layanan ini telah dijelaskan ke nasabah : 
																	<form:radiobutton disabled="disabled" cssClass="noborder" path="nasabah.mns_st_jelas" id="nasabah.mns_st_jelas" value="1"/>
																	Ya
																	<form:radiobutton disabled="disabled" cssClass="noborder" path="nasabah.mns_st_jelas" id="nasabah.mns_st_jelas" value="0"/>
																	Tidak
																</th>
															</tr>
														</table>
													</fieldset>
												</th>
											</tr>
											<tr>
												<th colspan="2">
													<fieldset>
														<legend>Informasi Pemberi LEAD</legend>
														<table class="entry2">
															<tr>
																<th>Sumber Lead</th>
																<td>
																	<form:select disabled="disabled" path="nasabah.ljl_jenis" onchange="pil('lead')">
																		<form:option label="" value="0"/>
																		<form:options items="${lsLead}" itemLabel="ljl_ket" itemValue="ljl_jenis" />
																	</form:select> 
					
																</td>
																<c:choose>
																	<c:when test="${cmd.nasabah.flag eq 1}">
																		<th>
																			${cmd.nasabah.namaLead }
																		</th>
																		<td>
																			<c:choose>
																				<c:when test="${cmd.nasabah.namaLead eq 'Nama Agen'}">
																					<form:select disabled="disabled" path="nasabah.referrer_fa" onchange="pil('agen')">
																						<form:option label="" value=""/>
																						<form:options items="${lsReferrer}" itemLabel="nama_bfa" itemValue="msag_id" />
																					</form:select> 
																					<form:hidden path="nasabah.mns_nama_lead" />
																				</c:when>	
																				<c:when test="${cmd.nasabah.namaLead eq 'Nama Referrer'}">
																					<form:select disabled="disabled" path="nasabah.referrer_id" onchange="pil('referrer')">
																						<form:option label="" value=""/>
																						<form:options items="${lsReferrerBii}" itemLabel="value" itemValue="key" />
																					</form:select> 
																					<form:hidden path="nasabah.mns_nama_lead" />
																				</c:when>
																				<c:when test="${cmd.nasabah.namaLead eq 'Nama'}">
																					<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="nasabah.mns_nama_lead" size="50"/>
																				</c:when>
																			</c:choose>	
																		</td>	
																	</c:when>
																</c:choose>	
															</tr>
															<tr>
																<th/><td/>
																<c:choose>
																	<c:when test="${cmd.nasabah.flag eq 1}">
																		<c:choose>
																			<c:when test="${cmd.nasabah.namaLead eq 'Nama Agen'}">
																				<th>Cabang</th>
																				<td>
																					<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="nasabah.mns_reff_cab" size="50"/>
																				</td>
																			</c:when>
																			<c:when test="${cmd.nasabah.namaLead eq 'Nama Referrer'}">
																				<th>Cabang</th>
																				<td>
																					<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="nasabah.mns_reff_cab" size="50"/>
																				</td>
																			</c:when>
																			<c:when test="${cmd.nasabah.namaLead eq 'Nama'}">
																				<td>
																					
																				</td>	
																			</c:when>
																		</c:choose>		
																	</c:when>
																</c:choose>		
															</tr>
															<tr>
																<th>
																	<form:checkbox disabled="disabled" cssClass="noborder" path="nasabah.mns_ok_saran" id="nasabah.mns_ok_saran" value="${cmd.nasabah.mns_ok_saran}" />
																	Bersedia Mengisi JIFFY
																</th>
															</tr>
														</table>
													</fieldset>
												</th>
											</tr>
										</table>
									</fieldset>
								</th>
							</tr>
						</table>
					</fieldset>
				</div>
				<div id="pane2" class="panes">
					<table class="entry2">
						<thead>
							<tr>
								<th width="2%">No</th>
								<th width="16%">Aktivitas</th>
								<th width="5%">Tanggal</th>
								<th width="50%">Pembicaraan</th>
								<th width="8%">Approve</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${cmd.aktivitas.listAktivitas}" var="s" varStatus="st">
								<tr>
									<td align="center"><form:input cssStyle="border-color:#6893B0;text-align: center;" disabled="disabled" path="aktivitas.listAktivitas[${st.index}].pert_ke" id="pert_ke" size="2" /> </td>
									<td>
										<form:select cssStyle="border-color:#6893B0;" disabled="disabled" path="aktivitas.listAktivitas[${st.index}].kd_aktivitas" id="kd_aktivitas">
											<form:options items="${lsAktivitas}" itemLabel="value" itemValue="key"/>
										</form:select>
									</td>
									<td align="center"><form:input cssStyle="border-color:#6893B0;" disabled="disabled" path="aktivitas.listAktivitas[${st.index}].tgl_pert" id="tgl_pert" size="12" /></td>
									<td align="center"><form:input cssStyle="border-color:#6893B0;" disabled="disabled" path="aktivitas.listAktivitas[${st.index}].keterangan" id="keterangan" size="120"/></td>
									<td align="center"><form:checkbox cssClass="noborder" path="aktivitas.listAktivitas[${st.index}].approval" id="approval" value="1"/> </td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<br />
					<table class="entry2">
						<tr>
							<th><input type="submit" name="save" id="save" value="Save"></th>
						</tr>
					</table>
				</div>
				<tr>
					<td colspan="4">
						<c:if test="${submitSuccess eq true}">
							<script>
								alert("Berhasil Simpan");
								//addOptionToSelect(parent.document, parent.document.formpost.nomor, '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}', '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}');
								window.location='${path }/common/input_activity.htm?nomor=${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}';
							</script>
							<div id="success">
				        		 Berhasil
				    		</div>
						</c:if>
					</td>
				</tr>
			</div>
			<input type="hidden" value="${pesan}" name="pesan" />
		</div>
		
	</form:form>
</body>

<script>
if(document.getElementById('nasabah.mns_ok_saran').value ==1){
	document.getElementById('nasabah.mns_ok_saran').checked = true;
}else if(document.getElementById('nasabah.mns_ok_saran').value ==0){
document.getElementById('nasabah.mns_ok_saran').checked = false;
}
</script>

<%@ include file="/include/page/footer.jsp"%>