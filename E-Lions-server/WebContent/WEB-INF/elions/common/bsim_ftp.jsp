<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		// (jQueryUI Tabs) init tab2 Utama (Pemegang, Tertanggung, dll)
		$("#tabs").tabs();
		
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
	
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		
		var pesan = '${pesan}';
		if(pesan!=null && pesan !=''){
			alert(pesan);
		}
		var index = $('div#tabs li a').index($('a[href="#tab-${showTab}"]').get(0));
				$('div#tabs').tabs({selected: index});
	});
	
	
	function download(file,product,tab,bulanan){			
			window.open('${path}/common/util.htm?window=bsim_ftp&file='+file+'&product='+product+'&tab='+tab+'&bulan1='+bulanan);
  			return false;
		}
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

</style>

<body>
	<div id="tabs">
		<ul>
			<li><a href="#tab-1">File Bank Sinarmas</a>
			</li>
			<li><a href="#tab-2">Backup File On Server</a>
			</li>
			<li><a href="#tab-3">File On BSIM - Sales</a>
			</li>

		</ul>
		<div id="tab-1">
			<form id="formPost" name="formPost" method="post">
				<fieldset class="ui-widget ui-widget-content">
					<div class="rowElem">
						<label>Jenis Produk :</label> <select name="jenisProduk"
							id="jenisProduk" title="Silahkan pilih Jenis Produk">
							<c:forEach var="c" items="${jenisProduk}" varStatus="s">
								<option value="${c.key}"
									<c:if test="${c.key eq jp}">selected="selected"</c:if>>${c.value}</option>
							</c:forEach>
						</select>
					</div>

					<div class="rowElem">
						<label>Periode Upload BSIM :</label> <input name="bulan"
							id="bulan" type="text" class="datepicker" title="Bulan"
							value="${tgl}">
					</div>


					<div class="rowElem">
						<label></label> <input type="submit" name="search" id="seacrh"
							value="Search">
				</fieldset>

				<c:if test="${not empty dokumen }">
					<fieldset class="ui-widget ui-widget-content">
						<legend class="ui-widget-header ui-corner-all">
							<div>List Dokumen</div>
						</legend>
						<div style="padding: 10px 0 10px 0;">
							<table class="jtable styleTable">
								<thead class="ui-widget-header">
									<tr>
										<th>No</th>
										<th>Dokumen</th>
										<th>Tanggal Upload</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody class="ui-widget-content">
									<c:forEach items="${dokumen}" var="d">
										<tr>
											<td>${d.no}</td>
											<td>${d.dok}</td>
											<td>${d.tgldok }</td>
											<td><input type="button" name="btnDownload"
												id="btnDownload" title="Download" value="Download"
												onclick="download('${d.dok}','${jp}','${showTab }','${tgl}');">
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</fieldset>
				</c:if>
			</form>


		</div>
		<div id="tab-2">
			<form id="formPost2" name="formPost2" method="post">
				<fieldset class="ui-widget ui-widget-content">
					<div class="rowElem">
						<label>Jenis Produk :</label> <select name="jenisProduk1"
							id="jenisProduk" title="Silahkan pilih Jenis Produk">
							<c:forEach var="c" items="${jenisProduk1}" varStatus="s">
								<option value="${c.key}"
									<c:if test="${c.key eq jp1}">selected="selected"</c:if>>${c.value}</option>
							</c:forEach>
						</select>
					</div>

					<div class="rowElem">
						<label>Periode Download:</label> <input name="bulan1" id="bulan1"
							type="text" class="datepicker" title="Bulan" value="${tgl1 }">
					</div>

					<div class="rowElem">
						<label></label> <input type="submit" name="search1" id="seacrh1"
							value="Search">
				</fieldset>

				<c:if test="${not empty dokumen1 }">
					<fieldset class="ui-widget ui-widget-content">
						<legend class="ui-widget-header ui-corner-all">
							<div>List Dokumen</div>
						</legend>
						<div style="padding: 10px 0 10px 0;">
							<table class="jtable styleTable">
								<thead class="ui-widget-header">
									<tr>
										<th>No</th>
										<th>Dokumen</th>
										<th>Tanggal</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody class="ui-widget-content">
									<c:forEach items="${dokumen1}" var="d">
										<tr>
											<td>${d.no1}</td>
											<td>${d.dok1}</td>
											<td>${d.tgldok1}</td>
											<td><input type="button" name="btnDownload1"
												id="btnDownload1" title="Download" value="Download"
												onclick="download('${d.dok1}','${jp1}','${showTab }','${tgl1}');">
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</fieldset>
				</c:if>
			</form>
		</div>

		<div id="tab-3">
			<form id="formPost" name="formPost" method="post">
				<fieldset class="ui-widget ui-widget-content">
					<div class="rowElem">
						<label>Jenis Produk :</label> <select name="jenisProduk3"
							id="jenisProduk" title="Silahkan pilih">
							<c:forEach var="c" items="${jenisProduk3}" varStatus="s">
								<option value="${c.key}"
									<c:if test="${c.key eq jp}">selected="selected"</c:if>>${c.value}</option>
							</c:forEach>
						</select>
					</div>


					<div class="rowElem">
						<label></label> <input type="submit" name="search3" id="search3"
							value="Search">
				</fieldset>

				<c:if test="${not empty dokumen3 }">
					<fieldset class="ui-widget ui-widget-content">
						<legend class="ui-widget-header ui-corner-all">
							<div>List Dokumen</div>
						</legend>
						<div style="padding: 10px 0 10px 0;">
							<table class="jtable styleTable">
								<thead class="ui-widget-header">
									<tr>
										<th>No</th>
										<th>Dokumen</th>
										<th>Tanggal Upload</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody class="ui-widget-content">
									<c:forEach items="${dokumen3}" var="d">
										<tr>
											<td>${d.no2}</td>
											<td>${d.dok2}</td>
											<td>${d.tgldok2 }</td>
											<td><input type="button" name="btnDownload"
												id="btnDownload" title="Download" value="Download"
												onclick="download('${d.dok2}','${jp2}','${showTab }','${tgl2}');">
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</fieldset>
				</c:if>
			</form>
		</div>
	</div>
</body>
</html>

