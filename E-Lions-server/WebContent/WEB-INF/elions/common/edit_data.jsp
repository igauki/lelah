<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<meta HTTP-EQUIV="Expires" CONTENT="-1">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link href="${path}/include/image/eas.ico" rel="shortcut icon">
		<!--  -->
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<!--  -->
		<script type="text/javascript" src="${path }/include/js/default.js"></script>
		<!-- Ajax Related -->
		<script type="text/javascript" src="${path}/include/js/ajax.js"></script>
		<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script>
		<script type="text/javascript" src="${path }/dwr/engine.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>
		<script type="text/javascript">
			hideLoadingMessage();
			
			var current = "TitipanPremi";
			
			function tampil(id){
				if(id == '') id = current;
				
				if(document.getElementById(trimWhitespace(id))) {
					document.getElementById(trimWhitespace(id)).style.display = 'block';
				}else{
					if(id == 'Rekening') popWin('${path}/uw/viewer.htm?window=rekening&spaj=${cmd.reg_spaj}', 400, 700);
					else alert('Menu ini belum selesai dibuat. Silahkan konfirmasi dengan IT.');
				}
				if(document.getElementById(current) && id != current) document.getElementById(current).style.display = 'none';
				current = trimWhitespace(id);
				
			}

			function pesan(){
				<c:if test="${not empty param.pesan}">
					alert('${param.pesan}');
				</c:if>			
			}

		</script>
	</head>
	<body onload="setupPanes('container1','tab1'); tampil('${param.e}'); pesan();" style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Edit Data</a>
				</li>
			</ul>

			<div class="tab-panes">

				<div id="pane1" class="panes">
					<form:form commandName="cmd" name="formpost" id="formpost" cssStyle="text-align: left;">
						<fieldset>
							<legend></legend>
							<table class="entry2" style="width: auto;">
								<tr>
									<th nowrap="nowrap">Nomor Register SPAJ</th>
									<th><form:input path="reg_spaj" size="18" readonly="true" cssStyle="text-align: center;" cssClass="readOnly"/></th>
									<th nowrap="nowrap">User Name</th>
									<th><input type="text" size="30" readonly="true" class="readOnly" value="${sessionScope.currentUser.name}"></th>
									<th nowrap="nowrap">Waktu</th>
									<th><input style="text-align: center;" type="text" size="23" readonly="true" class="readOnly" value="<fmt:formatDate value="${now}" pattern="dd/MM/yyyy (HH:mm)"/>"></th>
								</tr>
								<tr>
									<th nowrap="nowrap">Pilihan Edit</th>
									<th colspan="5">
										<select id="listEdit" name="listEdit" onchange="tampil(this.value);">
											<c:forEach items="${listEdit}" var="e">
												<option value="${e.key}"
													<c:if test="${param.e eq e.key}"> selected="true" </c:if>
												>${e.value}</option>
											</c:forEach>
										</select>
									</th>
								</tr>
								<spring:hasBindErrors name="cmd">
									<tr>
										<td colspan="2">
											<div id="error">
												<strong>Data yang dimasukkan tidak lengkap. Mohon lengkapi data-data berikut:</strong>
												<br />
												<form:errors path="*" delimiter="<br>" />
											</div>
										</td>
									</tr>
								</spring:hasBindErrors>
								<tr>
									<td colspan="6">
										<div id="error">Perhatian! Segala Aktivitas Edit Data Akan Di-LOG Di Sistem!!!</div>
									</td>
								</tr>
							</table>
						</fieldset>
						
						<fieldset id="TitipanPremi" style="display: none;">
							<legend>Edit Data Titipan Premi</legend>
							<table class="displaytag">
								<thead>
									<tr>
										<th style="white-space: nowrap;">Edit</th>
										<th style="white-space: nowrap;">Ke</th>
										<th style="white-space: nowrap;">Kurs</th>
										<th style="white-space: nowrap;">Jumlah</th>
										<th style="white-space: nowrap;">Tgl RK</th>
										<th style="white-space: nowrap;">Tgl Bayar</th>
										<th style="white-space: nowrap;">Tgl Jatuh Tempo</th>
										<th style="white-space: nowrap;">Rekening AJS</th>
										<th style="white-space: nowrap;">No Pre</th>
										<th style="white-space: nowrap;">No Voucher</th>
										<th style="white-space: nowrap;">Jenis Bayar</th>
										<%-- <th style="white-space: nowrap;">No KTTP</th> --%>
										<th style="white-space: nowrap;">No Polis Lama</th>
										<th style="white-space: nowrap;">Nilai Kurs</th>
										<%--
										<th style="white-space: nowrap;">Client Bank</th>
										<th style="white-space: nowrap;">Giro/Cek/Credit Card</th>
										<th style="white-space: nowrap;">Keterangan</th>
										--%>
										<th style="white-space: nowrap;">User Input</th>
										<th style="white-space: nowrap;">Tgl Input</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${cmd.listTitipanPremi}" var="t" varStatus="s">
										<tr>
											<td>
												<form:checkbox path="listTitipanPremi[${s.index}].checked" value="true" cssClass="noBorder"/>
											</td>
											<td class="left" style="white-space: nowrap;">
												<form:hidden path="listTitipanPremi[${s.index}].reg_spaj"/>
												<form:input size="2" path="listTitipanPremi[${s.index}].msdp_number" cssClass="readOnly" readonly="true"/>
											</td>
											<td class="left" style="white-space: nowrap;">
												<form:select path="listTitipanPremi[${s.index}].lku_id">
													<form:option value="" label=""/>
													<form:options items="${listKurs}" itemValue="key" itemLabel="value"/>
												</form:select>												
											</td>
											<td style="white-space: nowrap;">
												<form:input size="15" cssStyle="text-align: right;" path="listTitipanPremi[${s.index}].msdp_payment" />
											</td>
											<td class="center" style="white-space: nowrap;">
												<spring:bind path="cmd.listTitipanPremi[${s.index}].msdp_date_book">
													<script>inputDate('${status.expression}', '${status.value}', false);</script>
												</spring:bind>												
											</td>
											<td class="center" style="white-space: nowrap;">
												<spring:bind path="cmd.listTitipanPremi[${s.index}].msdp_pay_date">
													<script>inputDate('${status.expression}', '${status.value}', false);</script>
												</spring:bind>												
											</td>
											<td class="center" style="white-space: nowrap;">
												<spring:bind path="cmd.listTitipanPremi[${s.index}].msdp_due_date">
													<script>inputDate('${status.expression}', '${status.value}', false);</script>
												</spring:bind>												
											</td>
											<td class="left" style="white-space: nowrap;">
												<form:select path="listTitipanPremi[${s.index}].lsrek_id">
													<form:option value="" label=""/>
													<form:options items="${listRekEkalife}" itemValue="key" itemLabel="value"/>
												</form:select>												
											</td>
											<td class="left" style="white-space: nowrap;">
												<form:input size="13" path="listTitipanPremi[${s.index}].msdp_no_pre" />
											</td>
											<td class="left" style="white-space: nowrap;">
												<form:input size="13" path="listTitipanPremi[${s.index}].msdp_no_voucher" />
											</td>
											<td class="left" style="white-space: nowrap;">
												<form:select path="listTitipanPremi[${s.index}].lsjb_id">
													<form:option value="" label=""/>
													<form:options items="${listJenisBayar}" itemValue="key" itemLabel="value"/>
												</form:select>												
											</td>
											<%--
											<td style="white-space: nowrap;">
												<form:input size="15" path="listTitipanPremi[${s.index}].no_kttp" />
											</td>
											--%>
											<td style="white-space: nowrap;">
												<form:input size="15" path="listTitipanPremi[${s.index}].msdp_old_policy" />
											</td>
											<td style="white-space: nowrap;">
												<form:input size="15" path="listTitipanPremi[${s.index}].msdp_selisih_kurs" />
											</td>
											<%--
											<td class="left" style="white-space: nowrap;">
												<form:select path="listTitipanPremi[${s.index}].client_bank">
													<form:option value="" label=""/>
													<form:options items="${listBankPusat}" itemValue="key" itemLabel="value"/>
												</form:select>
											</td>
											<td style="white-space: nowrap;">
												<form:input size="15" path="listTitipanPremi[${s.index}].msdp_no_rek" />
											</td>
											<td style="white-space: nowrap;">
												<form:input size="15" path="listTitipanPremi[${s.index}].msdp_desc" />
											</td>
											--%>
											<td style="white-space: nowrap;">${t.lus_login_name}</td>
											<td style="white-space: nowrap;"><fmt:formatDate value="${t.msdp_input_date}" pattern="dd/MM/yyyy (HH:mm)" /></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</fieldset>

						<fieldset id="Payment" style="display: none;">
							<legend>Edit Data Payment</legend>
							<table class="displaytag">
								<thead>
									<tr>
										<th style="white-space: nowrap;">Edit</th>
										<th style="white-space: nowrap;">Ke</th>
										<th style="white-space: nowrap;">Kurs</th>
										<th style="white-space: nowrap;">Jumlah</th>
										<th style="white-space: nowrap;">Tgl RK</th>
										<th style="white-space: nowrap;">Tgl Bayar</th>
										<th style="white-space: nowrap;">Tgl Jatuh Tempo</th>
										<th style="white-space: nowrap;">Rekening AJS</th>
										<th style="white-space: nowrap;">No Pre</th>
										<th style="white-space: nowrap;">No Voucher</th>
										<th style="white-space: nowrap;">Jenis Bayar</th>
										<th style="white-space: nowrap;">No Polis Lama</th>
										<th style="white-space: nowrap;">Nilai Kurs</th>
										<th style="white-space: nowrap;">Client Bank</th>
										<th style="white-space: nowrap;">Giro/Cek/Credit Card</th>
										<th style="white-space: nowrap;">Keterangan</th>
										<th style="white-space: nowrap;">User Input</th>
										<th style="white-space: nowrap;">Tgl Input</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${cmd.listPayment}" var="t" varStatus="s">
										<tr>
											<td class="left" style="white-space: nowrap;"><form:checkbox path="listPayment[${s.index}].checked" value="true" cssClass="noBorder"/></td>
											<td class="left" style="white-space: nowrap;">${s.count}.</td>
											<td class="left" style="white-space: nowrap;">
												<form:select path="listPayment[${s.index}].lku_id">
													<form:option value="" label=""/>
													<form:options items="${listKurs}" itemValue="key" itemLabel="value"/>
												</form:select>												
											</td>
											<td style="white-space: nowrap;">
												<form:input size="15" cssStyle="text-align: right;" path="listPayment[${s.index}].mspa_payment" />
											</td>
											<td class="center" style="white-space: nowrap;">
												<spring:bind path="cmd.listPayment[${s.index}].mspa_date_book">
													<script>inputDate('${status.expression}', '${status.value}', false);</script>
												</spring:bind>												
											</td>
											<td class="center" style="white-space: nowrap;">
												<spring:bind path="cmd.listPayment[${s.index}].mspa_pay_date">
													<script>inputDate('${status.expression}', '${status.value}', false);</script>
												</spring:bind>												
											</td>
											<td class="center" style="white-space: nowrap;">
												<spring:bind path="cmd.listPayment[${s.index}].mspa_due_date">
													<script>inputDate('${status.expression}', '${status.value}', false);</script>
												</spring:bind>												
											</td>
											<td class="left" style="white-space: nowrap;">
												<form:hidden path="listPayment[${s.index}].reg_spaj"/>
												<form:hidden path="listPayment[${s.index}].mspa_payment_id"/>
												<form:select path="listPayment[${s.index}].lsrek_id">
													<form:option value="" label=""/>
													<form:options items="${listRekEkalife}" itemValue="key" itemLabel="value"/>
												</form:select>												
											</td>
											<td class="left" style="white-space: nowrap;">
												<form:input size="13" path="listPayment[${s.index}].mspa_no_pre" />
											</td>
											<td class="left" style="white-space: nowrap;">
												<form:input size="13" path="listPayment[${s.index}].mspa_no_voucher" />
											</td>
											<td class="left" style="white-space: nowrap;">
												<form:select path="listPayment[${s.index}].lsjb_id">
													<form:option value="" label=""/>
													<form:options items="${listJenisBayar}" itemValue="key" itemLabel="value"/>
												</form:select>												
											</td>
											<td style="white-space: nowrap;">
												<form:input size="15" path="listPayment[${s.index}].mspa_old_policy" />
											</td>
											<td style="white-space: nowrap;">
												<form:input size="15" path="listPayment[${s.index}].mspa_nilai_kurs" />
											</td>
											<td class="left" style="white-space: nowrap;">
												<form:select path="listPayment[${s.index}].client_bank">
													<form:option value="" label=""/>
													<form:options items="${listBankPusat}" itemValue="key" itemLabel="value"/>
												</form:select>
											</td>
											<td style="white-space: nowrap;">
												<form:input size="15" path="listPayment[${s.index}].mspa_no_rek" />
											</td>
											<td style="white-space: nowrap;">
												<form:input size="15" path="listPayment[${s.index}].mspa_desc" />
											</td>
											<td style="white-space: nowrap;">${t.lus_login_name}</td>
											<td style="white-space: nowrap;"><fmt:formatDate value="${t.mspa_input_date}" pattern="dd/MM/yyyy (HH:mm)" /></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</fieldset>

						<fieldset>
							<div class="info">Silahkan check/centang data yang akan diedit, lalu tekan tombol simpan</div>
							<br/>
							<input type="submit" name="save" value="Simpan" onclick="return confirm('Simpan Perubahan? Apakah Anda Sudah EMAIL Ke Dept Terkait?');">
						</fieldset>

					</form:form>
				</div>
			</div>
			
		</div>

	</body>
</html>