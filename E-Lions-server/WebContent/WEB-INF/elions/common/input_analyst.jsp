<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>

<script>

function pesan(){
	var pesan=document.formpost.pesan.value;
	if(pesan != ""){
		alert(pesan);
	}
}

function tampilPromptProduk(){
	no=parent.document.formpost.nomor.value;
	//alert(no);
	f=1;
	row=prompt("Masukkan row yang akan didelete  ","");
	if(row==null){
		alert("Tidak memasukkan row yang ingin didelete");
		f=0;
	}
	
	if(f==1){
		window.location='${path }/common/input_analyst.htm?nomor='+no+'&flag=1&tipe=0&row='+row;
	}
}

function tampilPromptAktivitas(){
	no=parent.document.formpost.nomor.value;
	//alert(no);
	f=1;
	row=prompt("Masukkan pertemuan ke-berapa yang akan didelete  ","");
	if(row==null){
		alert("Tidak memasukkan no pertemuan yang ingin didelete");
		f=0;
	}
	
	if(f==1){
		window.location='${path }/common/input_analyst.htm?nomor='+no+'&flag=1&tipe=1&row='+row;
	}
}

function prosesConfirm(){
	var x=window.confirm('Pastikan data yang Anda isi telah lengkap dan benar !\n Transfer ke Recommend ?');
	if(x){
		//alert('masuk');
		document.getElementById('formpost').submit();
	}
}

</script>

<c:if test="${not empty pesan} ">
	<script type="text/javascript">
		alert('${pesan}');
	</script>
</c:if>

<body style="height: 100%;" onload="setupPanes('container1', 'tab${cmd.showTab}'); pesan();">
	<c:choose>
		<c:when test="${cmd.flagAdd eq 2}">
			<form:form id="formpost" name="formpost" commandName="cmd">
			<div class="tab-container" id="container1">
					<ul class="tabs">
						<li>
							<a href="#" onClick="return showPane('pane1', this)" id="tab1" style="width: 33%">A</a>
							<a href="#" onClick="return showPane('pane2', this)" id="tab2" style="width: 33%">B</a>
							<a href="#" onClick="return showPane('pane3', this)" id="tab3" style="width: 34%">C</a>
						</li>
					</ul>
					<div class="tab-panes">
						<div id="pane1" class="panes">
							<fieldset>
								<legend>A. Apa yang paling penting dalam Hidup Anda dan Mengapa?</legend>
								<table class="entry2">
									<c:forEach items="${cmd.kebutuhan.listKebutuhan}" var="s" varStatus="st">
										<tr>
											<td width="2%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
												${st.count}
											</td>
											<td width="35%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
												${s.ljk_ket}
											</td>
											<td width="10%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
												Rank &nbsp;
												<form:input disabled="disabled" cssStyle="border-color:#6893B0; text-align: center;" path="kebutuhan.listKebutuhan[${st.index}].mkb_rank" id="mkb_rank" size="3" maxlength="2"/>
											</td>
											<td width="40%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
												Alasan &nbsp;
												<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="kebutuhan.listKebutuhan[${st.index}].mkb_alasan" id="mkb_alasan" size="60" maxlength="60"/>
											</td>
										</tr>
									</c:forEach>
								</table>
							</fieldset>
							<fieldset>
								<legend>B. Berapa yang Anda inginkan untuk setiap kebutuhan Anda?</legend>
								<table class="entry2">
									<thead>
										<tr>
											<th></th>
											<th>Tujuan</th>
											<th width="15%">Jumlah yang diinginkan</th>
											<th width="20%">Pendapatan yang dibutuhkan</th>
											<th width="20%" colspan="4">Waktu yang dibutuhkan</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${cmd.kebutuhan.listKebutuhan}" var="s" varStatus="st">
											<tr>
												<th>${st.count}</th>
												<th align="left">${s.ljk_ket}</th>
												<td><form:input disabled="disabled" cssStyle="border-color:#6893B0; text-align: right;" path="kebutuhan.listKebutuhan[${st.index}].mkb_jml_butuh" id="mkb_jml_butuh" size="30" maxlength="15"/></td>
												<td><form:input disabled="disabled" cssStyle="border-color:#6893B0; text-align: right;" path="kebutuhan.listKebutuhan[${st.index}].mkb_pdpt_butuh" id="mkb_pdpt_butuh" size="40" maxlength="15"/> </td>
												<td><form:input disabled="disabled" cssStyle="border-color:#6893B0; text-align: center;" path="kebutuhan.listKebutuhan[${st.index}].mkb_wkt_butuh_th" id="mkb_wkt_butuh_th" size="7" maxlength="2"/> </td>
												<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Thn</td>
												<td><form:input disabled="disabled" cssStyle="border-color:#6893B0; text-align: center;" path="kebutuhan.listKebutuhan[${st.index}].mkb_wkt_butuh_bl" id="mkb_wkt_butuh_bl" size="7" maxlength="2"/></td>
												<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Bln</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</fieldset>
							<fieldset>
								<legend>C. Berapa besar yang sudah Anda peroleh untuk setiap kebutuhan Anda?</legend>
								<table class="entry2">
									<thead>
										<tr>
											<th></th>
											<th>Tujuan</th>
											<th width="15%">Jumlah sudah diperoleh</th>
											<th width="20%">Pendapatan yang diperoleh</th>
											<th width="20%" colspan="4">Waktu yang dibutuhkan</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${cmd.kebutuhan.listKebutuhan}" var="s" varStatus="st">
											<tr>
												<th>${st.count}</th>
												<th align="left">${s.ljk_ket}</th>
												<td><form:input disabled="disabled" cssStyle="border-color:#6893B0; text-align: right;" path="kebutuhan.listKebutuhan[${st.index}].mkb_jml_dpoleh" id="mkb_jml_dpoleh" size="30" maxlength="15"/></td>
												<td><form:input disabled="disabled" cssStyle="border-color:#6893B0; text-align: right;" path="kebutuhan.listKebutuhan[${st.index}].mkb_pdpt_dpoleh" id="mkb_pdpt_dpoleh" size="40" maxlength="15"/> </td>
												<td><form:input disabled="disabled" cssStyle="border-color:#6893B0; text-align: center;" path="kebutuhan.listKebutuhan[${st.index}].mkb_wkt_dpoleh_th" id="mkb_wkt_dpoleh_th" size="7" maxlength="2"/> </td>
												<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Thn</td>
												<td><form:input disabled="disabled" cssStyle="border-color:#6893B0; text-align: center;" path="kebutuhan.listKebutuhan[${st.index}].mkb_wkt_dpoleh_bl" id="mkb_wkt_dpoleh_bl" size="7" maxlength="2"/></td>
												<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Bln</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</fieldset>
							<fieldset>
								<legend>D. Seberapa penting bagi Anda untuk mencapai Tujuan ini?</legend>
								<table class="entry2">
									<c:forEach items="${cmd.kebutuhan.listKebutuhan}" var="s" varStatus="st">
										<tr>
											<td width="2%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">${st.count}</th>
											<td width="40%" align="left" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">${s.ljk_ket}</th>
											<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
												<form:radiobutton disabled="disabled" cssClass="noborder" path="kebutuhan.listKebutuhan[${st.index}].mkb_penting" id="mkb_penting" value="1"/>
												Sangat Penting &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<form:radiobutton disabled="disabled" cssClass="noborder" path="kebutuhan.listKebutuhan[${st.index}].mkb_penting" id="mkb_penting" value="2"/>
												Penting &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<form:radiobutton disabled="disabled" cssClass="noborder" path="kebutuhan.listKebutuhan[${st.index}].mkb_penting" id="mkb_penting" value="3"/>
												Tidak Penting
											</td>
										</tr>
									</c:forEach>
								</table>
							</fieldset>
							<table class="entry2">
								<tr>
									<td>	        	
							  			<spring:bind path="cmd.*">
											<c:if test="${not empty status.errorMessages}">
												<div id="error">
													 Informasi:<br>
														<c:forEach var="error" items="${status.errorMessages}">
																	 - <c:out value="${error}" escapeXml="false" />
															<br/>
														</c:forEach>
												</div>
											</c:if>									
										</spring:bind>
									 </td>
								</tr>
							</table>
							<table class="entry2">
								<tr>
									<th>
										<input type="hidden" name="save" id="save" value="Save"/>
										<input type="hidden" name="trans" id="trans" value="Trans" onclick="prosesConfirm();"> 
									</th>
								</tr>
							</table>
						</div>
						<div id="pane2" class="panes">
							<c:set var="prod" value="" />
							<fieldset>
								<legend>E. Bagaimana Anda menggambarkan sikap Anda dalam menghadapi resiko?</legend>
									<table class="entry2">
										<tr>
											<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;"><form:radiobutton disabled="disabled" cssClass="noborder" path="jiffy.mjf_sikap_resiko" id="mjf_sikap_resiko" value="1"/>Berhati-hati</td>
											<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Saya lebih baik menerima hasil pengembalian yang sedikit, ini merefleksikan pendekatan berhati-hati.</td>
										</tr>
										<tr>
											<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;"><form:radiobutton disabled="disabled" cssClass="noborder" path="jiffy.mjf_sikap_resiko" id="mjf_sikap_resiko" value="2"/>Seimbang</td>
											<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Saya mempersiapkan diri untuk berinvestasi pada pasar saham, dan berharap untuk mendapatkan hasil pengembalian diatas inflasi.</td>
										</tr>
										<tr>
											<td width="25%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;"><form:radiobutton disabled="disabled" cssClass="noborder" path="jiffy.mjf_sikap_resiko" id="mjf_sikap_resiko" value="3"/>Berani dalam mengambil Resiko</td></td>
											<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Saya mempersiapkan diri untuk berinvestasi sampai dengan 100% pada saham, mengambil resiko yang lebih besar dalam mengharapkan hasil pengembalian yang tinggi.</td>
										</tr>
									</table>				
							</fieldset>
							<fieldset>
								<legend>F. Apakah dari produk-produk Bank berikut ini yang Anda miliki dan dari Bank mana?</legend>
									<table class="entry2">
										<thead>
											<tr>
												<th>No</th>
												<th>Jenis Produk</th>
												<th>Nama Bank</th>
												<th>Nama Produk Bank</th>
												<th>Jumlah</th>
												<th>Dimana</th>
												<th>Keterangan</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${cmd.prodBank.listProdBank}" var="s" varStatus="st">
												<tr>
													<td>${st.count}</td>
													<td>
														<form:select disabled="disabled" path="prodBank.listProdBank[${st.index}].lpb_id" id="lpb_id">
															<form:option label="" value="" />
															<form:options items="${lstJnProduk}" itemLabel="value" itemValue="key"/>
														</form:select> 
													</td>
													<td align="center"><form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="prodBank.listProdBank[${st.index}].mpb_nm_bank" id="mpb_nm_bank" maxlength="25"/></td>
													<td align="center"><form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="prodBank.listProdBank[${st.index}].mpb_nm_tabungan" id="mpb_nm_tabungan" maxlength="25"/> </td>
													<td align="center"><form:input disabled="disabled" cssStyle="border-color:#6893B0; text-align: right;" path="prodBank.listProdBank[${st.index}].mpb_jml_tabungan" id="mpb_jml_tabungan" /> </td>
													<td align="center"><form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="prodBank.listProdBank[${st.index}].mpb_ket_nm_bank" id="mpb_ket_nm_bank" size="25" maxlength="25"/> </td>
													<td align="center"><form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="prodBank.listProdBank[${st.index}].mpb_note" id="mpb_note" size="50" maxlength="50"/> </td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
									<table class="entry2">
										<tr>
											<th><input type="hidden" name="AddProduk" id="AddProduk" value="AddRow"/> 
												<input type="hidden" name="deleteProduk" id="deleteProduk" value="DeleteRow" onclick="tampilPromptProduk();" />
											</th>
										</tr>
									</table>							
							</fieldset>
							<fieldset>
								<legend>G. Pendapatan/Pengeluaran?</legend>
									<table class="entry2">
										<tr>
											<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Pada saat ini darimanakah Anda mendapatkan pendapatan? Beri tanda v pada kotak berikut ini dan sebutkan alasannya</td>
										</tr>
									</table>
									<table class="entry2">
										<thead>
											<tr>
												<th></th>
												<th>Jenis</th>
												<th>Ya</th>
												<th>Alasan</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${cmd.pendapatan.listPendapatan}" var="s" varStatus="st">
												<tr>
													<c:if test="${s.lsp_in_out eq prod}">
														<th></th>
													</c:if>
													<c:if test="${s.lsp_in_out ne prod}">
														<c:set var="prod" value="${s.lsp_in_out}" />
															<c:if test="${prod eq 1}">
																<th style="text-align: left;" width="30%">
																	Pendapatan
																</th>
															</c:if>
															<c:if test="${prod ne 1}">
																<th style="text-align: left;" width="30%">
																	Pengeluaran
																</th>
															</c:if>
													</c:if>
													<th style="text-align: left;" width="20%">${s.lsp_ket}</th>
													<td style="text-align: center;" width="1%"><form:checkbox disabled="disabled" path="pendapatan.listPendapatan[${st.index}].mpp_value" id="mpp_value" cssClass="noBorder" value="1"/> </td>
													<td><form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="pendapatan.listPendapatan[${st.index}].mpp_value_alasan" id="mpp_value_alasan" maxlength="60" size="100"/> </td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
							</fieldset>
							<fieldset>
								<legend>H. Budget manakah yang dapat Anda alokasikan untuk menyelesaikan persoalan ini?</legend>
									<table class="entry2">
										<tr>
											<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Budget</td>
											<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
												Rp&nbsp;&nbsp;
												<form:input disabled="disabled" cssStyle="text-align: right;border-color:#6893B0;" path="jiffy.mjf_budget" id="mjf_budget" maxlength="15"/>
											</td>
										</tr>
									</table>
							</fieldset>
							<table class="entry2">
								<tr>
									<td>	        	
							  			<spring:bind path="cmd.*">
											<c:if test="${not empty status.errorMessages}">
												<div id="error">
													 Informasi:<br>
														<c:forEach var="error" items="${status.errorMessages}">
																	 - <c:out value="${error}" escapeXml="false" />
															<br/>
														</c:forEach>
												</div>
											</c:if>									
										</spring:bind>
									 </td>
								</tr>
							</table>
							<table class="entry2">
								<tr>
									<th>
										<input type="hidden" name="save" id="save" value="Save"/>
										<input type="hidden" name="trans" id="trans" value="Trans" onclick="prosesConfirm();">
									</th>
								</tr>
							</table>
						</div>
						<div id="pane3" class="panes">
							<fieldset>
								<legend>I. Pernyataan Nasabah</legend>
								<table class="entry2">
									<tr>
										<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Saya/Kami mengerti data-data(fact) yang saya/kami berikan pada formulir ini, telah digunakan dalam pemberian saran yang telah diberikan oleh Bancassurance Financial Advisor, dan saya/kami senang atas solusi yang disarankan dapat memenuhi kebutuhan saya/kami.</td>
									</tr>
									<tr></tr><tr></tr><tr></tr>
								</table>
								<table class="entry2">
									<tr>
										<td width="30%" align="center" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Tandatangan Nasabah</td>
										<td align="center" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
											Tanggal &nbsp;&nbsp;
											<fmt:formatDate pattern="dd/MM/yyyy" value="${cmd.nasabah.mns_tgl_tt}" var="s"/>	
											<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="nasabah.mns_tgl_tt" id="mns_tgl_tt" size="12" maxlength="10"/>
											<span class="info">*
										</td>
									</tr>
								</table>
								<br />
								<br />
							</fieldset>
							<fieldset>
								<legend>J. Review Keuangan</legend>
								<table class="entry2">
									<tr>
										<td width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Seberapa sering Anda berharap untuk mereview pengaturan keuangan Anda?</td>
										<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
											Setiap &nbsp;
											<form:select disabled="disabled" path="nasabah.mns_frek_review" id="mns_frek_review">
												<form:option label="" value=""/>
												<form:options items="${lstReviewUang}" itemLabel="value" itemValue="key"/>
											</form:select>
											&nbsp;bulan
										</td>
									</tr>
								</table>
								<br />
							</fieldset>
							<fieldset>
								<legend>Pertemuan Selanjutnya</legend>
								<table class="entry2">
									<thead>
										<tr >
											<th width="1%">No</th>
											<th width="3%">Tanggal &nbsp; <span class="info">* </th>
											<th width="5%">Pert Ke</th>
											<th width="8%%">Aktivitas</th>
											<th width="20%">Pembicaraan</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${cmd.aktivitas.listAktivitas}" var="s" varStatus="st">
											<tr>
												<td>${st.count}</td>
												<td align="center">
													<fmt:formatDate pattern="dd/MM/yyyy" value="${s.tgl_pert}" var="auxvarfechatoprint"/>	
													<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="aktivitas.listAktivitas[${st.index}].tgl_pert" id="tgl_pert" size="12" maxlength="10"/>
												</td>
												<td align="center"><form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="aktivitas.listAktivitas[${st.index}].pert_ke" id="pert_ke" maxlength="2"/> </td>
												<td>
													<form:select disabled="disabled" path="aktivitas.listAktivitas[${st.index}].kd_aktivitas">
														<form:option label="" value=""/>
														<form:options items="${lstAktivitas}" itemLabel="value" itemValue="key"/>
													</form:select>
												</td>
												<td><form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="aktivitas.listAktivitas[${st.index}].keterangan" id="keterangan" maxlength="100" size="90"/> </td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
								<table class="entry2">
									<tr>
										<th><input type="hidden" name="AddAktivitas" id="AddAktivitas" value="AddRow"/>
											<input type="hidden" name="deleteAktivitas" id="deleteAktivitas" value="DeleteRow" onclick="tampilPromptAktivitas();" />
										</th> 
									</tr>
								</table>
							</fieldset>
							<table class="entry2">
								<tr>
									<td>
										<span class="info"  style=" text-align: left; font-size: 8pt;">
										* Format tanggal dd/MM/yyyy
										</span>
									</td>
								</tr>
							</table>
							<table class="entry2">
								<tr>
									<td>	        	
							  			<spring:bind path="cmd.*">
											<c:if test="${not empty status.errorMessages}">
												<div id="error">
													 Informasi:<br>
														<c:forEach var="error" items="${status.errorMessages}">
																	 - <c:out value="${error}" escapeXml="false" />
															<br/>
														</c:forEach>
												</div>
											</c:if>									
										</spring:bind>
									 </td>
								</tr>
							</table>
							<table class="entry2">
								<tr>
									<th>
										<input type="hidden" name="save" id="save" value="Save"/>
										<input type="hidden" name="trans" id="trans" value="Trans" onclick="prosesConfirm();"> 
									</th>
								</tr>
							</table>
						</div>
					</div>
					<table>
						<tr>
							<td colspan="4">
								<c:if test="${submitSuccess eq true}">
									<c:choose>
										<c:when test="${cmd.flagAdd eq 1}">
											<script>
												alert("Berhasil Transfer");
												addOptionToSelect(parent.document, parent.document.formpost.nomor, '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}', '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}');
												window.parent.tes();
											</script>
										</c:when>
										<c:otherwise>
											<script>
												alert("Berhasil Simpan");
												addOptionToSelect(parent.document, parent.document.formpost.nomor, '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}', '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}');
												window.location='${path }/common/input_analyst.htm?nomor=${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}';
											</script>
										</c:otherwise>
									</c:choose>
						        	<div id="success">
							        	 Berhasil
							    	</div>
						        </c:if>
							 </td>
						</tr>
					</table>
					<input type="hidden" value="${pesan}" name="pesan" />
				</div>
			</form:form>
		</c:when>
		<c:otherwise>
			<form:form id="formpost" name="formpost" commandName="cmd">
				<div class="tab-container" id="container1">
					<ul class="tabs">
						<li>
							<a href="#" onClick="return showPane('pane1', this)" id="tab1" style="width: 33%">A</a>
							<a href="#" onClick="return showPane('pane2', this)" id="tab2" style="width: 33%">B</a>
							<a href="#" onClick="return showPane('pane3', this)" id="tab3" style="width: 34%">C</a>
						</li>
					</ul>
					<div class="tab-panes">
						<div id="pane1" class="panes">
							<fieldset>
								<legend>A. Apa yang paling penting dalam Hidup Anda dan Mengapa?</legend>
								<table class="entry2">
									<c:forEach items="${cmd.kebutuhan.listKebutuhan}" var="s" varStatus="st">
										<tr>
											<td width="2%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
												${st.count}
											</td>
											<td width="35%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
												${s.ljk_ket}
											</td>
											<td width="10%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
												Rank &nbsp;
												<form:input cssStyle="border-color:#6893B0; text-align: center;" path="kebutuhan.listKebutuhan[${st.index}].mkb_rank" id="mkb_rank" size="3" maxlength="2"/>
											</td>
											<td width="40%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
												Alasan &nbsp;
												<form:input cssStyle="border-color:#6893B0;" path="kebutuhan.listKebutuhan[${st.index}].mkb_alasan" id="mkb_alasan" size="60" maxlength="60"/>
											</td>
										</tr>
									</c:forEach>
								</table>
							</fieldset>
							<fieldset>
								<legend>B. Berapa yang Anda inginkan untuk setiap kebutuhan Anda?</legend>
								<table class="entry2">
									<thead>
										<tr>
											<th></th>
											<th>Tujuan</th>
											<th width="15%">Jumlah yang diinginkan</th>
											<th width="20%">Pendapatan yang dibutuhkan</th>
											<th width="20%" colspan="4">Waktu yang dibutuhkan</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${cmd.kebutuhan.listKebutuhan}" var="s" varStatus="st">
											<tr>
												<th>${st.count}</th>
												<th align="left">${s.ljk_ket}</th>
												<td><form:input cssStyle="border-color:#6893B0; text-align: right;" path="kebutuhan.listKebutuhan[${st.index}].mkb_jml_butuh" id="mkb_jml_butuh" size="30" maxlength="15"/></td>
												<td><form:input cssStyle="border-color:#6893B0; text-align: right;" path="kebutuhan.listKebutuhan[${st.index}].mkb_pdpt_butuh" id="mkb_pdpt_butuh" size="40" maxlength="15"/> </td>
												<td><form:input cssStyle="border-color:#6893B0; text-align: center;" path="kebutuhan.listKebutuhan[${st.index}].mkb_wkt_butuh_th" id="mkb_wkt_butuh_th" size="7" maxlength="2"/> </td>
												<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Thn</td>
												<td><form:input cssStyle="border-color:#6893B0; text-align: center;" path="kebutuhan.listKebutuhan[${st.index}].mkb_wkt_butuh_bl" id="mkb_wkt_butuh_bl" size="7" maxlength="2"/></td>
												<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Bln</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</fieldset>
							<fieldset>
								<legend>C. Berapa besar yang sudah Anda peroleh untuk setiap kebutuhan Anda?</legend>
								<table class="entry2">
									<thead>
										<tr>
											<th></th>
											<th>Tujuan</th>
											<th width="15%">Jumlah sudah diperoleh</th>
											<th width="20%">Pendapatan yang diperoleh</th>
											<th width="20%" colspan="4">Waktu yang dibutuhkan</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${cmd.kebutuhan.listKebutuhan}" var="s" varStatus="st">
											<tr>
												<th>${st.count}</th>
												<th align="left">${s.ljk_ket}</th>
												<td><form:input cssStyle="border-color:#6893B0; text-align: right;" path="kebutuhan.listKebutuhan[${st.index}].mkb_jml_dpoleh" id="mkb_jml_dpoleh" size="30" maxlength="15"/></td>
												<td><form:input cssStyle="border-color:#6893B0; text-align: right;" path="kebutuhan.listKebutuhan[${st.index}].mkb_pdpt_dpoleh" id="mkb_pdpt_dpoleh" size="40" maxlength="15"/> </td>
												<td><form:input cssStyle="border-color:#6893B0; text-align: center;" path="kebutuhan.listKebutuhan[${st.index}].mkb_wkt_dpoleh_th" id="mkb_wkt_dpoleh_th" size="7" maxlength="2"/> </td>
												<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Thn</td>
												<td><form:input cssStyle="border-color:#6893B0; text-align: center;" path="kebutuhan.listKebutuhan[${st.index}].mkb_wkt_dpoleh_bl" id="mkb_wkt_dpoleh_bl" size="7" maxlength="2"/></td>
												<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Bln</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</fieldset>
							<fieldset>
								<legend>D. Seberapa penting bagi Anda untuk mencapai Tujuan ini?</legend>
								<table class="entry2">
									<c:forEach items="${cmd.kebutuhan.listKebutuhan}" var="s" varStatus="st">
										<tr>
											<td width="2%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">${st.count}</th>
											<td width="40%" align="left" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">${s.ljk_ket}</th>
											<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
												<form:radiobutton cssClass="noborder" path="kebutuhan.listKebutuhan[${st.index}].mkb_penting" id="mkb_penting" value="1"/>
												Sangat Penting &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<form:radiobutton cssClass="noborder" path="kebutuhan.listKebutuhan[${st.index}].mkb_penting" id="mkb_penting" value="2"/>
												Penting &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<form:radiobutton cssClass="noborder" path="kebutuhan.listKebutuhan[${st.index}].mkb_penting" id="mkb_penting" value="3"/>
												Tidak Penting
											</td>
										</tr>
									</c:forEach>
								</table>
							</fieldset>
							<table class="entry2">
								<tr>
									<td>	        	
							  			<spring:bind path="cmd.*">
											<c:if test="${not empty status.errorMessages}">
												<div id="error">
													 Informasi:<br>
														<c:forEach var="error" items="${status.errorMessages}">
																	 - <c:out value="${error}" escapeXml="false" />
															<br/>
														</c:forEach>
												</div>
											</c:if>									
										</spring:bind>
									 </td>
								</tr>
							</table>
							<table class="entry2">
								<tr>
									<th>
										<input type="submit" name="save" id="save" value="Save"/>
										<input type="button" name="trans" id="trans" value="Trans" onclick="prosesConfirm();"> 
									</th>
								</tr>
							</table>
						</div>
						<div id="pane2" class="panes">
							<c:set var="prod" value="" />
							<fieldset>
								<legend>E. Bagaimana Anda menggambarkan sikap Anda dalam menghadapi resiko?</legend>
									<table class="entry2">
										<tr>
											<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;"><form:radiobutton cssClass="noborder" path="jiffy.mjf_sikap_resiko" id="mjf_sikap_resiko" value="1"/>Berhati-hati</td>
											<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Saya lebih baik menerima hasil pengembalian yang sedikit, ini merefleksikan pendekatan berhati-hati.</td>
										</tr>
										<tr>
											<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;"><form:radiobutton cssClass="noborder" path="jiffy.mjf_sikap_resiko" id="mjf_sikap_resiko" value="2"/>Seimbang</td>
											<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Saya mempersiapkan diri untuk berinvestasi pada pasar saham, dan berharap untuk mendapatkan hasil pengembalian diatas inflasi.</td>
										</tr>
										<tr>
											<td width="25%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;"><form:radiobutton cssClass="noborder" path="jiffy.mjf_sikap_resiko" id="mjf_sikap_resiko" value="3"/>Berani dalam mengambil Resiko</td></td>
											<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Saya mempersiapkan diri untuk berinvestasi sampai dengan 100% pada saham, mengambil resiko yang lebih besar dalam mengharapkan hasil pengembalian yang tinggi.</td>
										</tr>
									</table>				
							</fieldset>
							<fieldset>
								<legend>F. Apakah dari produk-produk Bank berikut ini yang Anda miliki dan dari Bank mana?</legend>
									<table class="entry2">
										<thead>
											<tr>
												<th>No</th>
												<th>Jenis Produk</th>
												<th>Nama Bank</th>
												<th>Nama Produk Bank</th>
												<th>Jumlah</th>
												<th>Dimana</th>
												<th>Keterangan</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${cmd.prodBank.listProdBank}" var="s" varStatus="st">
												<tr>
													<td>${st.count}</td>
													<td>
														<form:select path="prodBank.listProdBank[${st.index}].lpb_id" id="lpb_id">
															<form:option label="" value="" />
															<form:options items="${lstJnProduk}" itemLabel="value" itemValue="key"/>
														</form:select> 
													</td>
													<td align="center"><form:input cssStyle="border-color:#6893B0;" path="prodBank.listProdBank[${st.index}].mpb_nm_bank" id="mpb_nm_bank" maxlength="25"/></td>
													<td align="center"><form:input cssStyle="border-color:#6893B0;" path="prodBank.listProdBank[${st.index}].mpb_nm_tabungan" id="mpb_nm_tabungan" maxlength="25"/> </td>
													<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="prodBank.listProdBank[${st.index}].mpb_jml_tabungan" id="mpb_jml_tabungan" /> </td>
													<td align="center"><form:input cssStyle="border-color:#6893B0;" path="prodBank.listProdBank[${st.index}].mpb_ket_nm_bank" id="mpb_ket_nm_bank" size="25" maxlength="25"/> </td>
													<td align="center"><form:input cssStyle="border-color:#6893B0;" path="prodBank.listProdBank[${st.index}].mpb_note" id="mpb_note" size="50" maxlength="50"/> </td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
									<table class="entry2">
										<tr>
											<th><input type="submit" name="AddProduk" id="AddProduk" value="AddRow"/> 
												<input type="button" name="deleteProduk" id="deleteProduk" value="DeleteRow" onclick="tampilPromptProduk();" />
											</th>
										</tr>
									</table>							
							</fieldset>
							<fieldset>
								<legend>G. Pendapatan/Pengeluaran?</legend>
									<table class="entry2">
										<tr>
											<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Pada saat ini darimanakah Anda mendapatkan pendapatan? Beri tanda v pada kotak berikut ini dan sebutkan alasannya</td>
										</tr>
									</table>
									<table class="entry2">
										<thead>
											<tr>
												<th></th>
												<th>Jenis</th>
												<th>Ya</th>
												<th>Alasan</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${cmd.pendapatan.listPendapatan}" var="s" varStatus="st">
												<tr>
													<c:if test="${s.lsp_in_out eq prod}">
														<th></th>
													</c:if>
													<c:if test="${s.lsp_in_out ne prod}">
														<c:set var="prod" value="${s.lsp_in_out}" />
															<c:if test="${prod eq 1}">
																<th style="text-align: left;" width="30%">
																	Pendapatan
																</th>
															</c:if>
															<c:if test="${prod ne 1}">
																<th style="text-align: left;" width="30%">
																	Pengeluaran
																</th>
															</c:if>
													</c:if>
													<th style="text-align: left;" width="20%">${s.lsp_ket}</th>
													<td style="text-align: center;" width="1%"><form:checkbox  path="pendapatan.listPendapatan[${st.index}].mpp_value" id="mpp_value" cssClass="noBorder" value="1"/> </td>
													<td><form:input cssStyle="border-color:#6893B0;" path="pendapatan.listPendapatan[${st.index}].mpp_value_alasan" id="mpp_value_alasan" maxlength="60" size="100"/> </td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
							</fieldset>
							<fieldset>
								<legend>H. Budget manakah yang dapat Anda alokasikan untuk menyelesaikan persoalan ini?</legend>
									<table class="entry2">
										<tr>
											<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Budget</td>
											<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
												Rp&nbsp;&nbsp;
												<form:input cssStyle="text-align: right;border-color:#6893B0;" path="jiffy.mjf_budget" id="mjf_budget" maxlength="15"/>
											</td>
										</tr>
									</table>
							</fieldset>
							<table class="entry2">
								<tr>
									<td>	        	
							  			<spring:bind path="cmd.*">
											<c:if test="${not empty status.errorMessages}">
												<div id="error">
													 Informasi:<br>
														<c:forEach var="error" items="${status.errorMessages}">
																	 - <c:out value="${error}" escapeXml="false" />
															<br/>
														</c:forEach>
												</div>
											</c:if>									
										</spring:bind>
									 </td>
								</tr>
							</table>
							<table class="entry2">
								<tr>
									<th>
										<input type="submit" name="save" id="save" value="Save"/>
										<input type="button" name="trans" id="trans" value="Trans" onclick="prosesConfirm();">
									</th>
								</tr>
							</table>
						</div>
						<div id="pane3" class="panes">
							<fieldset>
								<legend>I. Pernyataan Nasabah</legend>
								<table class="entry2">
									<tr>
										<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Saya/Kami mengerti data-data(fact) yang saya/kami berikan pada formulir ini, telah digunakan dalam pemberian saran yang telah diberikan oleh Bancassurance Financial Advisor, dan saya/kami senang atas solusi yang disarankan dapat memenuhi kebutuhan saya/kami.</td>
									</tr>
									<tr></tr><tr></tr><tr></tr>
								</table>
								<table class="entry2">
									<tr>
										<td width="30%" align="center" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Tandatangan Nasabah</td>
										<td align="center" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
											Tanggal &nbsp;&nbsp;
											<fmt:formatDate pattern="dd/MM/yyyy" value="${cmd.nasabah.mns_tgl_tt}" var="s"/>	
											<form:input cssStyle="border-color:#6893B0;" path="nasabah.mns_tgl_tt" id="mns_tgl_tt" size="12" maxlength="10"/>
											<span class="info">*
										</td>
									</tr>
								</table>
								<br />
								<br />
							</fieldset>
							<fieldset>
								<legend>J. Review Keuangan</legend>
								<table class="entry2">
									<tr>
										<td width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Seberapa sering Anda berharap untuk mereview pengaturan keuangan Anda?</td>
										<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">
											Setiap &nbsp;
											<form:select path="nasabah.mns_frek_review" id="mns_frek_review">
												<form:option label="" value=""/>
												<form:options items="${lstReviewUang}" itemLabel="value" itemValue="key"/>
											</form:select>
											&nbsp;bulan
										</td>
									</tr>
								</table>
								<br />
							</fieldset>
							<fieldset>
								<legend>Pertemuan Selanjutnya</legend>
								<table class="entry2">
									<thead>
										<tr >
											<th width="1%">No</th>
											<th width="3%">Tanggal &nbsp; <span class="info">* </th>
											<th width="5%">Pert Ke</th>
											<th width="8%%">Aktivitas</th>
											<th width="20%">Pembicaraan</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${cmd.aktivitas.listAktivitas}" var="s" varStatus="st">
											<tr>
												<td>${st.count}</td>
												<td align="center">
													<fmt:formatDate pattern="dd/MM/yyyy hh:mm:ss" value="${s.tgl_pert}" var="auxvarfechatoprint"/>	
													<form:input cssStyle="border-color:#6893B0;" path="aktivitas.listAktivitas[${st.index}].tgl_pert" id="tgl_pert" size="12" maxlength="10"/>
												</td>
												<td align="center"><form:input cssStyle="border-color:#6893B0;text-align: right;" path="aktivitas.listAktivitas[${st.index}].pert_ke" id="pert_ke" maxlength="2"/> </td>
												<td>
													<form:select path="aktivitas.listAktivitas[${st.index}].kd_aktivitas">
														<form:option label="" value=""/>
														<form:options items="${lstAktivitas}" itemLabel="value" itemValue="key"/>
													</form:select>
												</td>
												<td><form:input cssStyle="border-color:#6893B0;" path="aktivitas.listAktivitas[${st.index}].keterangan" id="keterangan" maxlength="100" size="90"/> </td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
								<table class="entry2">
									<tr>
										<th><input type="submit" name="AddAktivitas" id="AddAktivitas" value="AddRow"/>
											<input type="button" name="deleteAktivitas" id="deleteAktivitas" value="DeleteRow" onclick="tampilPromptAktivitas();" />
										</th> 
									</tr>
								</table>
							</fieldset>
							<table class="entry2">
								<tr>
									<td>
										<span class="info"  style=" text-align: left; font-size: 8pt;">
										* Format tanggal dd/MM/yyyy
										</span>
									</td>
								</tr>
							</table>
							<table class="entry2">
								<tr>
									<td>	        	
							  			<spring:bind path="cmd.*">
											<c:if test="${not empty status.errorMessages}">
												<div id="error">
													 Informasi:<br>
														<c:forEach var="error" items="${status.errorMessages}">
																	 - <c:out value="${error}" escapeXml="false" />
															<br/>
														</c:forEach>
												</div>
											</c:if>									
										</spring:bind>
									 </td>
								</tr>
							</table>
							<table class="entry2">
								<tr>
									<th>
										<input type="submit" name="save" id="save" value="Save"/>
										<input type="button" name="trans" id="trans" value="Trans" onclick="prosesConfirm();"> 
									</th>
								</tr>
							</table>
						</div>
					</div>
					<table>
						<tr>
							<td colspan="4">
								<c:if test="${submitSuccess eq true}">
									<c:choose>
										<c:when test="${cmd.flagAdd eq 1}">
											<script>
												alert("Berhasil Transfer");
												addOptionToSelect(parent.document, parent.document.formpost.nomor, '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}', '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}');
												window.parent.tes();
											</script>
										</c:when>
										<c:otherwise>
											<script>
												alert("Berhasil Simpan");
												addOptionToSelect(parent.document, parent.document.formpost.nomor, '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}', '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}');
												window.location='${path }/common/input_analyst.htm?nomor=${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}';
											</script>
										</c:otherwise>
									</c:choose>
						        	<div id="success">
							        	 Berhasil
							    	</div>
						        </c:if>
							 </td>
						</tr>
					</table>
					<input type="hidden" value="${pesan}" name="pesan" />
				</div>
			</form:form>
		</c:otherwise>
	</c:choose>
</body>

<%@ include file="/include/page/footer.jsp"%>