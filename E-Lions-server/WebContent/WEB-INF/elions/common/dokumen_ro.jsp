<%@ include file="/include/page/header.jsp"%>
<body style="height: 100%;" onload="setupPanes('container1', 'tab1'); ">
	<div class="tab-container" id="container1">
		
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Daftar Dokumen Lanjutan ${folder} SPAJ ${spaj} (Diluar New Business)</a>
			</li>
		</ul>

		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form method="post" name="formpost">
					<table class="entry2">
						<tr>
							<th>SPAJ/No Polis</th>
							<td><input type="text" name="spaj" value="${spaj}"><input type="submit" value="Tampilkan"></td>
						</tr>
						<tr>
							<th>Surat Rollover / Top-up</th>
							<td>
								<c:if test="${not empty pesan}">
									<div id="error">${pesan}</div>
								</c:if>
								<table class="displaytag" style="width: auto;">
									<tr>
										<th>Dokumen</th>
										<th>Periode</th>
										<th>Tanggal Print</th>
										<th>Jenis Rollover</th>
										<th>Rate</th>
										<th>Deposit</th>
										<th>Interest</th>
										<th>Filename</th>
									</tr>
									<c:forEach var="dok" items="${daftarFile}">
										<c:if test="${not empty dok.fileName}">
											<tr>
												<td style="text-align: left;"><a href="${path}/common/util.htm?window=dokumen_ro&spaj=${spaj}&file=${dok.fileName}" target="_blank">${dok.msl_desc}</a></td>
												<td style="text-align: center;"><fmt:formatDate pattern="dd/MM/yyyy" value="${dok.mps_deposit_date}" /><fmt:formatDate pattern="dd/MM/yyyy" value="${dok.msl_bdate}" /> - <fmt:formatDate pattern="dd/MM/yyyy" value="${dok.mpr_mature_date}" /><fmt:formatDate pattern="dd/MM/yyyy" value="${dok.msl_edate}" /></td>
												<td style="text-align: center;">${dok.fileCreated}</td>
												<td style="text-align: center;">
													<c:choose>
														<c:when test="${dok.mpr_jns_ro eq 1}">ALL</c:when>
														<c:when test="${dok.mpr_jns_ro eq 2}">PREMI</c:when>
														<c:when test="${dok.msl_ro eq 1}">ALL</c:when>
														<c:when test="${dok.msl_ro eq 2}">PREMI</c:when>
														<c:otherwise>-</c:otherwise>
													</c:choose>
												</td>
												<td style="text-align: right;"><fmt:formatNumber value="${dok.mpr_rate}"/><fmt:formatNumber value="${dok.msl_rate}"/>%</td>
												<td style="text-align: right;"><fmt:formatNumber value="${dok.mpr_deposit}"/><fmt:formatNumber value="${dok.msl_premi}"/></td>
												<td style="text-align: right;"><fmt:formatNumber value="${dok.mpr_interest}"/><fmt:formatNumber value="${dok.msl_bunga}"/></td>
												<td style="text-align: left;">${dok.fileName}</td>
											</tr>
										</c:if>
									</c:forEach>
								</table>
							</td>
						</tr>
						<tr>
							<th>Surat Endorsement</th>
							<td>
								<table class="displaytag" style="width: auto;">
									<tr>
										<th>Dokumen</th>
									</tr>
									<c:forEach var="dok" items="${daftarFile2}">
										<tr>
											<td style="text-align: left;"><a href="${path}/common/util.htm?endors=1&window=dokumen_ro&spaj=${spaj}&file=${dok.key}" target="_blank">${dok.key}</a></td>
										</tr>
									</c:forEach>
								</table>
							</td>
						</tr>
					</table>
				</form>			
			</div>
		</div>
		
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>