<%@ include file="/include/page/header.jsp"%>
<script>
	//http://ajsjava/E-Lions/common/util.htm?window=doc&file=asdf&spaj=01200700020
	/*
	function tampil(fil,frame){
		var msag = document.getElementById('msag_id').value;
		if(msag !=''){
			v_mid = fil.substring(0, fil.indexOf("~", 0));
			v_filename = fil.substring(fil.indexOf("~", 0)+1);
			if(fil != '') document.getElementById('docFrame'+frame).src = '${path}/common/util.htm?window=doc_bp&file='+v_filename+'&mid=' + msag;
		}else{
			v_spaj = fil.substring(0, fil.indexOf("~", 0));
			v_filename = fil.substring(fil.indexOf("~", 0)+1);
			if(fil != '') document.getElementById('docFrame'+frame).src = '${path}/common/util.htm?window=doc&file='+v_filename+'&spaj=' + v_spaj;
		}
	}*/
	
	function tampil(fil,frame){
		var msag = document.getElementById('msag_id').value;
		pos = fil.indexOf("~~", 0);
		
		if(pos>0){
			v_mid = fil.substring(0, fil.indexOf("~", 0));
			pos2 = fil.indexOf("~", 0);
			v_filename = fil.substring(pos2+1,fil.indexOf("~~", 0));		
			//if(fil != '') document.getElementById('docFrame'+frame).src = '${path}/common/util.htm?window=doc_bp&file='+v_filename+'&mid=' + msag;
			if(fil != '') document.getElementById('docFrame'+frame).src = 'https://ews.sinarmasmsiglife.co.id:8443/E-Lions/common/util.htm?window=doc_bp&file='+v_filename+'&mid=' + msag;
		}else{
			v_spaj = fil.substring(0, fil.indexOf("~", 0));
			v_filename = fil.substring(fil.indexOf("~", 0)+1);
			//if(fil != '') document.getElementById('docFrame'+frame).src = '${path}/common/util.htm?window=doc&file='+v_filename+'&spaj=' + v_spaj;
			if(fil != '') document.getElementById('docFrame'+frame).src = 'https://ews.sinarmasmsiglife.co.id:8443/E-Lions/common/util.htm?window=doc&file='+v_filename+'&spaj=' + v_spaj;
		}
	}
	
</script>
<body onload="setFrameSize('docFrame1', 50);setFrameSize('docFrame2', 50);" onresize="setFrameSize('docFrame1', 50);setFrameSize('docFrame2', 50);" style="height: 100%;">
<form name="formpost" method="post">
<div class="tabcontent">
<table class="entry2" style="width: 100%">
<tr>
	<c:forEach var="s"  items="${perulangan}" varStatus="xt">
	<th>
		<table class="entry2" style="width:100%;">
			<tr>
				<th>Pilih Dokumen : </th>
				<td>
					<select name="dok" id="dok" onChange="tampil(this.value,${xt.index+1});">
						<option value="">---=-=-=-=-=-= [ Pilih Dokumen ] =-=-=-=-=-=---</option>
						<optgroup label="Polis Utama (${spaj})">
						<c:forEach var="s" items="${daftarFile}">
							<option value="${spaj}~${s.key}">${s.key} [${s.value}]</option>
						</c:forEach>
						</optgroup>
						<c:if test="${not empty daftarFile2}">
							<optgroup label="Polis Surrender Endorsement (${spaj2})">
							<c:forEach var="s" items="${daftarFile2}">
								<option value="${spaj2}~${s.key}">${s.key} [${s.value}]</option>
							</c:forEach>
							</optgroup>
						</c:if>
						<c:if test="${not empty daftarFile3}">
							<optgroup label="Polis BP (${spaj3})">
							<c:forEach var="s" items="${daftarFile3}">
								<option value="${spaj3}~${s.key}~~${msag_id}">${s.key} [${s.value}]</option>
							</c:forEach>
							</optgroup>
						</c:if>
						<c:if test="${not empty daftarFile4}">
							<optgroup label="Surat Batal/Refund (${spaj4})">
							<c:forEach var="s" items="${daftarFile4}">
								<option value="${spaj4}~${s.key}">${s.key} [${s.value}]</option>
							</c:forEach>
							</optgroup>
						</c:if>
						<c:if test="${not empty daftarFile5}">
							<optgroup label="Perpanjangan Polis (${spaj5})">
							<c:forEach var="s" items="${daftarFile5}">
								<option value="${spaj5}~${s.key}">${s.key} [${s.value}]</option>
							</c:forEach>
							</optgroup>
						</c:if>
					</select>
					<input type="hidden" name="msag_id" id="msag_id" value="${msag_id}">
					<input type="hidden" name="pass">
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<iframe name="docFrame${xt.index+1}" src="" id="docFrame${xt.index+1}" width="100%"  height="100%"> Please Wait... </iframe>
				</td>
			</tr>
		</table>
	</th>
	</c:forEach>
</tr>
</table>
</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>