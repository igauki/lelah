<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script>
	hideLoadingMessage();
</script>
</head>
<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Support Team E-Lions</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" style="text-align: left;">
					<ul>
						<li>
								Anda dapat menghubungi pada <STRONG>extension 8109</STRONG> dengan :
								<ul>
									<li><STRONG>Yusuf S.</STRONG> atau</li>
									<li><STRONG>Dian N.</STRONG> atau</li>
									<li><STRONG>Bertho R.</STRONG></li>
								</ul>
							</li>
					</ul>
					<ul>
						<li>
							Anda dapat juga mengirim keluhan/kritik/saran dengan menggunakan form dibawah
							<table style="border: none;">
								<tr>
									<td>Kepada:
									</td>
									<td>
										<select name="to">
											<option value="yusuf">Yusuf S.</option>
										</select>
									</td>
								</tr>
								<tr>
									<td>Subyek:
									</td>
									<td>
										<input type="text" name="subject" size="99">
									</td>
								</tr>
								<tr>
									<td>Pesan:
									</td>
									<td>
										<textarea name="message" cols="75" rows="12"></textarea>
									</td>
								</tr>
								<tr>
									<td></td>
									<td><input type="submit" name="send" value="Kirim!" onclick="return cek();"></td>
								</tr>
							</table>
						</li>
					</ul>				
				</form>
			</div>
			
		</div>
	</div>

	<c:if test="${not empty pepesan}">
		<script>alert('${pepesan}');</script>
	</c:if>
</body>
</html>