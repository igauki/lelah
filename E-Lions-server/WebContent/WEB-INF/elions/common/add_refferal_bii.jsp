<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>

<body onload="setupPanes('container1','tab1');" style="height: 50%;width: 50%;" >
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Daftar Input Pencairan</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form:form id="formpost" name="formpost" commandName="cmd">
					<table class="entry2" width="100%">
						<tr>
							<th>Nama Reff</th>
							<td>
								<form:input cssStyle="border-color:#6893B0;" path="addReffBii.namaReff" size="30"/>
								<span class="info">*</span>
							</td>
						</tr>
						<tr>
							<th>Nama Cabang</th>
							<td>
								<form:select path="addReffBii.kode">
									<form:options items="${lstCabangBii}" itemLabel="value" itemValue="key" />
								</form:select> 
							</td>
						</tr>
						<tr>
							<th>NPK</th>
							<td><form:input cssStyle="border-color:#6893B0;" path="addReffBii.npk" size="30"/></td>
						</tr>
						<tr>
							<th>Kode Agent</th>
							<td>
								<form:input cssStyle="border-color:#6893B0;" path="addReffBii.kodeAgent" size="30" maxlength="5"/>
								<span class="info">*</span>
							</td>
						</tr>
						<tr>
							<th>Lisensi</th>
							<td>
								<form:radiobutton cssClass="noborder" path="addReffBii.lisensi" id="addReffBii.lisensi" value="1"/>
								Ada
								<form:radiobutton cssClass="noborder" path="addReffBii.lisensi" id="addReffBii.lisensi" value="0"/>
								Tidak Ada
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<span class="info">*Data harus diisi</span>
							</td>
						</tr>
						<tr>
							 <td colspan="2">
								<c:if test="${submitSuccess eq true}">
									<script>
										alert("Berhasil Simpan");
									</script>
						        	<div id="success">
							        	 Berhasil
							    	</div>
						        </c:if>	
					  			<spring:bind path="cmd.*">
									<c:if test="${not empty status.errorMessages}">
										<div id="error">
											 Informasi:<br>
												<c:forEach var="error" items="${status.errorMessages}">
															 - <c:out value="${error}" escapeXml="false" />
													<br/>
												</c:forEach>
										</div>
									</c:if>									
								</spring:bind>
							 </td>
						</tr>
						<tr>
							<td colspan="2">	
								<input type="submit" name="btnsave" value="Save"></input>
							</td>
						</tr>
					</table>
				</form:form>
			</div>
		</div>
	</div>
	
</body>

<%@ include file="/include/page/footer.jsp"%>