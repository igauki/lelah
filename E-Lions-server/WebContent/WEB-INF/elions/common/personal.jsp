<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<head>
	<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta NAME="Description" CONTENT="EkaLife">
	<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
	<script type="text/javascript" src="${path}/include/js/default.js"></script>
	<script type="text/javascript" src="${path }/dwr/util.js"></script>	
	<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
	<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
	<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
	<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>


</head>
<script>
	function ayo(pencetan, perintah){
		dok = document.formpost;
		if('cari' == perintah){
			mcl_id = dok.elements['personal.mcl_id'].value;
			mcl_jenis = dok.elements['personal.mcl_jenis'].value;
			popWin('${path}/common/menu.htm?frame=cariprofile&mcl_id='+mcl_id+'&mcl_jenis='+mcl_jenis, 400, 500);
		}else if('tampil' == perintah){
			document.formpost.submit();
		}else if('simpan'== perintah){
			document.formpost.s.value='1';
			alert("ok");
			document.formpost.submit();
		}
		return false;
	}
	
	function ganti(i){
		formpost.flag_ut.value=i;
	}
	
	function carabayar(i,j){
		flag=true;
		if(i==0)
			flag=false;
		//
		formpost.elements['personal.lsContactPerson['+j+'].rek_no'].readOnly=flag	
		formpost.elements['personal.lsContactPerson['+j+'].rek_nama'].readOnly=flag	
		formpost.elements['personal.lsContactPerson['+j+'].rek_bank'].readOnly=flag	
		formpost.elements['personal.lsContactPerson['+j+'].rek_bank_cabang'].readOnly=flag	
		formpost.elements['personal.lsContactPerson['+j+'].rek_bank_kota'].readOnly=flag	
	}
	

	function komisi(komisiValue, index){
		if(komisiValue == 0){
			document.getElementById('rek_no'+index).disabled = true;
			document.getElementById('rek_nama'+index).disabled = true;
			document.getElementById('rek_bank'+index).disabled = true;
			document.getElementById('rek_bank_cabang'+index).disabled = true;
			document.getElementById('rek_bank_kota'+index).disabled = true;
			document.getElementById('cara_bayar0').checked = false;
			document.getElementById('cara_bayar1').checked = true;
			document.getElementById('cara_bayar0').disabled = true;
		}else if(komisiValue == 1){
			document.getElementById('rek_no'+index).disabled = false;
			document.getElementById('rek_nama'+index).disabled = false;
			document.getElementById('rek_bank'+index).disabled = false;
			document.getElementById('rek_bank_cabang'+index).disabled = false;
			document.getElementById('rek_bank_kota'+index).disabled = false;
			document.getElementById('cara_bayar0').disabled = false;
		}
	}
	
	function disableMenuAdmin(flagPayroll){
		if(flagPayroll==0){
			//alert(flagPayroll);
			document.getElementById('payrollData').style.visibility = "visible";
			document.getElementById('pay0').checked = true;
			document.getElementById('pay1').checked = false;
		}else if(flagPayroll==1){
			//alert(flagPayroll);
			document.getElementById('payrollData').style.visibility = "hidden";
			document.getElementById('pay0').checked = false;
			document.getElementById('pay1').checked = true;
		}
	}
</script>

<body style="height: 100%;" onload="setupPanes('container1', 'tab1');">
	<form:form id="formpost" name="formpost" commandName="cmd">
		<input type="hidden" name="show" value="">
		<form:hidden path="personal.mcl_jenis"/>
		<table class="entry2" style="width: auto;">
			<tr>
				<td>
					<fieldset>
						<legend>Data Perusahaan</legend>
						<table class="entry2">
							<tr>
								<th nowrap="nowrap" style="width: 120px;">Nama</th>
								<td colspan="5">
									<form:input path="personal.mcl_id" cssClass="readOnly" readonly="readonly" size="16" />
									<form:select path="personal.mcl_gelar">
										<form:option label="" value=""/>
										<form:options items="${gelar}" itemLabel="value" itemValue="value" />
									</form:select> 
									<form:input path="personal.mcl_first" size="60"/>
								</td>
							</tr>
							<tr>
								<th nowrap="nowrap">Jenis Usaha</th>
								<td>
									<form:select path="personal.lju_id" cssStyle="width: 220px;">
										<form:option value=""/>
										<form:options items="${jenisUsaha}" itemLabel="value" itemValue="key" />
									</form:select> 
								</td>
								<th nowrap="nowrap">No. NPWP</th>
								<td>
									<form:input path="personal.mpt_npwp"/>
								</td>
								<th nowrap="nowrap">Jangka Waktu</th>
								<td>
									<form:input path="personal.mpt_term"/>
								</td>
							</tr>
							<tr>
								<th nowrap="nowrap">Status</th>
								<td>
									<table class="displaytag" style="width: auto;">
										<tr>
											<c:forEach var="stutas" items="${cmd.personal.status}">
												<td>${stutas.LJC_NOTE}</td>
											</c:forEach>
										</tr>
									</table>
								</td>
								<th nowrap="nowrap">Wilayah</th>
								<td colspan="3">
									<form:hidden path="personal.region_id" id="region_id"/>
									<form:input path="personal.lsrg_nama" size="60" id="lsrg_nama"/>
									<span id="indicator" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
								</td>
							</tr>
							<tr>
								<th nowrap="nowrap" rowspan="2">Alamat</th>
								<td rowspan="2">
									<form:textarea path="personal.address.alamat_kantor" cols="40" rows="3" 
										onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "/>
								</td>
								<th nowrap="nowrap">Kode Pos</th>
								<td>
									<form:input path="personal.address.kd_pos_kantor" />
								</td>
								<th nowrap="nowrap">Kota</th>
								<td>
									<form:input path="personal.address.kota_kantor" />
								</td>
							</tr>
							<tr>
								<th nowrap="nowrap">Telp 1</th>
								<td>
									<form:input path="personal.address.telpon_kantor" />
								</td>
								<th nowrap="nowrap">Telp 2</th>
								<td>
									<form:input path="personal.address.telpon_kantor2" />
								</td>
							</tr>					
							<tr><th colspan="2"></th>
								<th>Tanggal Print Invoice</th>
								<td>
									<spring:bind path="cmd.personal.s_tgl_print_inv">
										<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
									</spring:bind>	
								</td>
								<th>Tanggal Print Kwitansi</th>
								<td>
									<spring:bind path="cmd.personal.s_tgl_print_kw">
										<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
									</spring:bind>	
								</td>
							</tr>					
							<tr>
								<th colspan="2"></th>
								<th colspan="3">
									<form:radiobutton path="personal.flag_ws" value="0" cssClass="noBorder"/>
										None
									<form:radiobutton path="personal.flag_ws" value="1" cssClass="noBorder"/>
										Perusahaan Peserta Worksite
									<form:radiobutton path="personal.flag_ws" value="2" cssClass="noBorder"/>
										Perusahaan Untuk Produk Individu
								</th>
							</tr>
							<tr>
								<th nowrap="nowrap">Group SINARMAS / Non SINARMAS</th>
								<th colspan="5" align="left">
									<form:radiobutton path="personal.flag_group" value="1" cssClass="noBorder"/>
										Group SINARMAS
									<form:radiobutton path="personal.flag_group" value="0" cssClass="noBorder"/>
										Non SINARMAS
								</th>
							</tr>	
							<tr>
								<th nowrap="nowrap">Admin WS</th>
								<td colspan="2">
									<form:select path="personal.lus_admin_ws">
										<form:options items="${daftarAdminWs}" itemLabel="value" itemValue="key" />
									</form:select> 
								</td>
								<th></th>
								<th>
									<input id="pay0" type="radio" class="noBorder" onclick="disableMenuAdmin(0)"/>Payroll
									<input id="pay1" type="radio" class="noBorder" onclick="disableMenuAdmin(1)"/>Non Payroll
								</th>
							</tr>	
							
							
						</table>
					</fieldset>
					<fieldset id="payrollData" >
						<legend>Data Personel Pengurus Administrasi Asuransi (Bag. Payroll)</legend>
						<div class="tab-container" id="container1">
							<ul class="tabs">
								<c:forEach items="${cmd.personal.lsContactPerson}" var="x" varStatus="xt">
									<li>
										<a href="#" onClick="return showPane('pane${xt.index+1}', this)"  
										onmouseover="return overlib('Alt-${xt.index+1}', AUTOSTATUS, WRAP);" onmouseout="nd();"
										onfocus="return showPane('pane${xt.index+1}', this)" accesskey="${xt.index+1}" id="tab${xt.index+1}">Contact Person ${xt.index+1}</a>
									</li>
								</c:forEach>	
							</ul>
							<div class="tab-panes">
								<c:forEach items="${cmd.personal.lsContactPerson}" var="x" varStatus="xt">
									<div id="pane${xt.index+1}" class="panes">
										<table class="entry2" style="width: auto;">
											<tr>
												<th nowrap="nowrap" style="width: 120px;">Nama Lengkap </th>
												<td colspan="2">
													<form:input path="personal.lsContactPerson[${xt.index }].nama_lengkap"/>
												</td>
												<td>
													<input type="radio" name="ut" onClick="ganti(${xt.index+1});"
													onmouseover="return overlib('Penerima Kompensasi Under Table', AUTOSTATUS, WRAP);" onmouseout="nd();"
													<c:if test="${cmd.flag_ut eq xt.index+1}">checked</c:if>>
												</td>
											</tr>
											<tr>
												<th>Jenis Kelamin</th>
												<td>
													<form:radiobutton 
														path="personal.lsContactPerson[${xt.index }].mste_sex" 
														id="mste_sex" value="1" cssClass="noBorder"/>
													Pria
													<form:radiobutton 
														path="personal.lsContactPerson[${xt.index }].mste_sex" 
														id="mste_sex" value="0" cssClass="noBorder"/>
													Wanita
												</td>
												
											</tr>
											<tr>
												<th nowrap="nowrap">Telepon Kantor</th>
												<td colspan="3">
													<form:input path="personal.lsContactPerson[${xt.index }].telp_kantor"/>
												</td>
											</tr>
											<tr>
												<th nowrap="nowrap">No. HP</th>
												<td colspan="3">
													<form:input path="personal.lsContactPerson[${xt.index }].telp_hp"/>
												</td>
											</tr>
											<tr>
												<th nowrap="nowrap">Alamat Email</th>
												<td colspan="3">
													<form:input path="personal.lsContactPerson[${xt.index }].email"/>
												</td>
											</tr>
											<tr>
												<th nowrap="nowrap">Komisi dibayarkan</th>
												<td>
													<form:radiobutton 
														path="personal.lsContactPerson[${xt.index }].flag_komisi" 
														id="flag_komisi" value="1" cssClass="noBorder" onclick="komisi(this.value,'${xt.index }')"/>
													Ya
													<form:radiobutton 
														path="personal.lsContactPerson[${xt.index }].flag_komisi" 
														id="flag_komisi" value="0" cssClass="noBorder" onclick="komisi(this.value,'${xt.index }')"/>
													Tidak
												</td>
											</tr>
											<tr>
												<th nowrap="nowrap">Jenis Badan</th>
												<td>
												<form:radiobutton path="personal.lsContactPerson[${xt.index }].jenis_badan" value="1" title="Individu" id="jenis_badan" cssClass="noBorder"/> Individu
												</td>
												<td>
													<form:radiobutton path="personal.lsContactPerson[${xt.index }].jenis_badan" value="2" title="Perusahaan" id="jenis_badan" cssClass="noBorder"/>  Perusahaan
												</td>
											</tr>
											<tr>
												<th nowrap="nowrap">NPWP</th>
												<td colspan="3">
													<form:input path="personal.lsContactPerson[${xt.index }].npwp"/>
												</td>
											</tr>
											<tr>
												<th nowrap="nowrap" rowspan="5">Cara Bayar</th>
												<td rowspan="5">
													<form:radiobutton onclick="carabayar(0,${xt.index});" path="personal.lsContactPerson[${xt.index }].cara_bayar" id="cara_bayar0" value="0" cssClass="noBorder"/>
													<form:label path="personal.lsContactPerson[${xt.index }].cara_bayar" for="cara_bayar" >Transfer Bank</form:label>
												</td>
												<th nowrap="nowrap">Nomor Rekening</th>
												<td>
													<form:input path="personal.lsContactPerson[${xt.index }].rek_no" id="rek_no${xt.index }"/>
												</td>
											</tr>
											<tr>
												<th nowrap="nowrap">Atas Nama</th>
												<td>
													<form:input path="personal.lsContactPerson[${xt.index }].rek_nama" id="rek_nama${xt.index }"/>
												</td>
											</tr>
											<tr>
												<th nowrap="nowrap">Nama Bank</th>
												<td>
													<form:input path="personal.lsContactPerson[${xt.index }].rek_bank" id="rek_bank${xt.index }"/>
												</td>
											</tr>
											<tr>
												<th nowrap="nowrap">Cabang</th>
												<td>
													<form:input path="personal.lsContactPerson[${xt.index }].rek_bank_cabang" id="rek_bank_cabang${xt.index }"/>
												</td>
											</tr>
											<tr>
												<th nowrap="nowrap">Kota</th>
												<td>
													<form:input path="personal.lsContactPerson[${xt.index }].rek_bank_kota" id="rek_bank_kota${xt.index }"/>
												</td>
											</tr>
											<tr>
												<th></th>
												<td onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	
													onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;">
													<form:radiobutton onclick="carabayar(1,${xt.index});" path="personal.lsContactPerson[${xt.index }].cara_bayar" id="cara_bayar1" value="1" cssClass="noBorder"/>
													<form:label path="personal.lsContactPerson[${xt.index }].cara_bayar" for="cara_bayar" >Giro</form:label>
												</td>
												<th colspan="2"></th>
											</tr>
										</table>
									</div>
								</c:forEach>
							</div>
						</div>		
					</fieldset>
				</td>
			</tr>
			<tr>
				 <td colspan="3">
					<c:if test="${submitSuccess eq true}">
			        	<div id="success">
				        	 Berhasil
			        	</div>
			        </c:if>	
		  			<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
								 Informasi:<br>
									<c:forEach var="error" items="${status.errorMessages}">
												 - <c:out value="${error}" escapeXml="false" />
										<br/>
									</c:forEach>
							</div>
						</c:if>									
					</spring:bind>
				</td>
			</tr>
			<tr>
				<td>
					<input type="button" name="cari" value="Cari (Edit)" onclick="ayo(this,'cari');">
					<input type="button" name="simpan" value="Simpan" onclick="this.disabled=true; ayo(this,'simpan');"
					<c:if test="${submitSuccess eq true}">
						disabled
					</c:if>
					>
				</td>
				<form:hidden path="flag_ut"/>
				<input type="hidden" name="s" value="0"/>
			</tr>
		</table>
	</form:form>

<ajax:autocomplete
	  source="lsrg_nama"
	  target="region_id"
	  baseUrl="${path}/servlet/autocomplete?s=personal.lsrg_nama&q=region2"
	  className="autocomplete"
	  indicator="indicator"
	  minimumCharacters="5"
	  parser="new ResponseXmlToHtmlListParser()" />

</body>
</html>