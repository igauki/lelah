<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link rel="stylesheet" href="${path}/include/js/jdmenu/jquery.jdMenu.css" type="text/css" />
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path}/include/js/disableKeys.js"></script>
<script type="text/javascript" src="${path}/include/js/default.js"></script>
<script type="text/javascript" src="${path}/dwr/util.js"></script>
<script type="text/javascript" src="${path}/include/js/jdmenu/jquery.js"></script>
<script type="text/javascript" src="${path}/include/js/jdmenu/jquery.dimensions.js"></script>
<script type="text/javascript" src="${path}/include/js/jdmenu/jquery.positionBy.js"></script>
<script type="text/javascript" src="${path}/include/js/jdmenu/jquery.bgiframe.js"></script>
<script type="text/javascript" src="${path}/include/js/jdmenu/jquery.jdMenu.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	function pergi(ling){
		document.getElementById('mainFrame').src= ling; 
		return false;
	}
</script>
</head>
	<BODY onload="setFrameSize('mainFrame', 54);" onresize="setFrameSize('mainFrame', 54);" style="background-color: #ECE9D8; overflow: hidden;" style="height: 100%;">
		<table width="100%" height="24px">
			<tr>
				<td style="text-transform: none;">
					<c:forEach var="baris" items="${cmd.allMenu}">
						<c:if test="${baris.level lt tingkat}">
							<c:forEach var="hit" begin="1" end="${tingkat - baris.level}" step="1">
								</ul></li>
							</c:forEach>
						</c:if>
						<c:choose>
							<c:when test="${baris.level eq 1}"><ul class="jd_menu"></c:when>
							<c:when test="${baris.level eq 2}"><li><strong class="link">${baris.nama_menu}</strong><ul></c:when>
							<c:when test="${empty baris.link_menu}"><li class="accessible"><strong class="link">${baris.nama_menu} &raquo;</strong><ul></c:when>
							<c:when test="${not empty baris.link_menu}"><li><a href="#" onclick="return pergi('${path}/${baris.link_menu}');" class="link">${baris.nama_menu}</a></li></c:when>
							<c:otherwise><ul><li>${baris.level} ${baris.nama_menu}</li></c:otherwise>
						</c:choose>
						<c:set var="tingkat" value="${baris.level}"/>
					</c:forEach>
				</td>
				<td align="right" id="header">
					<%=request.getLocalAddr()%> - 
					Logged in as
					<strong>${cmd.currentUser.name }</strong>
					[${cmd.currentUser.dept}]
				</td>
			</tr>
		</table>

		<iframe src="${path}/common/menu.htm?frame=treemenu" 
			name="mainFrame"  id="mainFrame" 
			width="99%" height="91%" marginwidth="0px" marginheight="0px" align="center" scrolling="yes" frameborder="yes">
		</iframe>
		<div id="footer">
			<table>
				<tr>
					<td style="text-align: left;">
						<a href="#" style="font-weight: bold; text-decoration: none; color: black;"
								onclick="document.getElementById('mainFrame').src='${path}/common/menu.htm?frame=treemenu'; return false;">
							Menu<c:if test="${cmd.currentUser.lde_id eq \"01\"}"> [${sessionScope.deebee}]</c:if> : </a>
						<font id="roti">Awal</font>
					</td>
					<td style="text-align: right;">
						<c:if test="${not empty cmd.currentUser.loginTime}">
							<a href="#" 
									title="<spring:message code="copyright" /> Sinarmas MSIG Life IT Department"
									onclick="document.getElementById('mainFrame').src=document.getElementById('mainFrame').src; return false;" 
									style="text-decoration: none; color: black;">
								Login Time: 
								<fmt:formatDate value="${cmd.currentUser.loginTime}" pattern="[hh:mm]"/>
							</a>
						</c:if> 
					</td>
				</tr>
			</table>
		</div> 
	</BODY>	
<%@ include file="/include/page/footer.jsp"%>