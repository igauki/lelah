<%@ include file="/include/page/header.jsp"%>

<!--Added by Samuel -->
<!--to make sure that if error occurs on popup win then it willl focus on this very popup win-->
<head>
    <script type="text/javascript">
        window.focus();
    </script>
</head>

<body style="height: 100%;">
<div id="contents">
	<table>
		<tr>
			<td style="text-transform: none;">
				<h3>
					Maaf, tetapi telah terjadi kesalahan pada aplikasi. 
					<br>Detail mengenai kesalahan tersebut bisa dikonfirmasikan ke department IT.
				</h3>
				<div id="error">
					Anda bisa mengkonfirmasi masalah ini dengan department IT WEB melalui :
					<ul> 
						<li>Email : itweb@sinarmasmsiglife.co.id </li>
						<li>Telp : 021-50597777 extension 8113 , 8125  , 8983 , 8992</li>						
					</ul>
				</div>
			</td>
		</tr>
	</table>
</div>
</body>
<%@ include file="/include/page/footer.jsp"%>