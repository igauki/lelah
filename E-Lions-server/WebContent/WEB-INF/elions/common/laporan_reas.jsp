<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />

<html>
	<head> 
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/disableKeys.js"></script>
		<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
		<script>hideLoadingMessage();</script>
	</head>
<body style="height: 100%;" >
	<table class="entry2">
		<tr>
			<td>
				<c:forEach items="${lsPage}" var="page">
					<a href="${path}/${page.link}">${page.no}</a>
				</c:forEach>
			</td>
		</tr>
		<tr>
			<display:table id="lsHasil" name="lsHasil" class="simple" decorator="org.displaytag.decorator.TotalTableDecorator"  requestURI="${requestScope.requestURL}" export="true">   
				<display:column property="number" title="No."  sortable="true"/>                                                                                      
				<display:column property="reg_spaj" title="REG_SPAJ"  sortable="true"/>                                                                                      
				<display:column property="mspo_policy_no" title="NO POLIS"  sortable="true"/>                                                                                      
				<display:column property="mcl_first" title="Nama TT"  sortable="true"/>                                                                                      
				<display:column property="mste_age" title="Umur TT"  sortable="true"/>                                                                                      
				<display:column property="mste_medical" title="MSTE_MEDICAL"  sortable="true"/>                                                                              
				<display:column property="lsdbs_name" title="NAMA PRODUK"  sortable="true"/>                                                                              
				<display:column property="lsbs_id" title="LSBS_ID"  sortable="true"/>                                                                                        
				<display:column property="lsdbs_number" title="LSDBS_NUMBER"  sortable="true"/>                                                                              
				<display:column property="lku_id" title="Kurs"  sortable="true"/>                                                                                          
				<display:column property="mspr_beg_date" title="BEG_DATE" format="{0, date, dd/MM/yyyy}"  sortable="true"/>                                             
				<display:column property="mspr_tsi" title="UP/TSI" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                            
				<display:column property="retensi" title="RETENSI" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                            
				<display:column property="reas" title="REAS" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                            
			</display:table>
		</tr>
	</table>
	
</div>
</body>
</html>