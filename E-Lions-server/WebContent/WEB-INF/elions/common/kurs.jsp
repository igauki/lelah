<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen"> <!-- CSS untuk DatePicker Script (jscalendar) -->
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script><!-- DatePicker Script (jscalendar) -->
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script><!-- DatePicker Script (jscalendar) -->
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script><!-- DatePicker Script (jscalendar) -->
<script type="text/javascript" src="${path}/include/js/disableKeys.js"></script>
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script>
	hideLoadingMessage();
	
	function ayo(pencetan, perintah){
		if(perintah=='delete') document.formpost.del.value=pencetan.name;
		pencetan.disabled=true;
		document.formpost.show.value=perintah;
		document.formpost.submit();
	}
	
</script>
</head>
<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Kurs Harian AJS MSIG</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form:form id="formpost" name="formpost" commandName="cmd" action="${path}/common/kurs.htm">
					<input type="hidden" name="show">
					<input type="hidden" name="del">
					<table class="entry2">
						<tr>
							<th>Range Tanggal</th>
							<td>
								<spring:bind path="cmd.tglAwal">
									<script>inputDate('${status.expression}', '${status.value}', false);</script>
								</spring:bind>
								s/d
								<spring:bind path="cmd.tglAkhir">
									<script>inputDate('${status.expression}', '${status.value}', false);</script>
								</spring:bind>
								<input type="button" name="btnShow" value="Show" onclick="return ayo(this, 'show');"
									accesskey="O" onmouseover="return overlib('Alt-A', AUTOSTATUS, WRAP);" onmouseout="nd();">
							</td>
						</tr>
						<tr>
							<th>Kurs Harian</th>
							<td>
								<fieldset>
									<table class="displaytag" style="width: auto;">
										<thead>
											<tr>
												<th></th>
												<th>Mata Uang</th>
												<th>Tanggal</th>
												<th>Kurs Tengah</th>
												<th>Kurs Beli</th>
												<th>Kurs Jual</th>
											</tr>
										</thead>
										<tbody>
										<c:forEach items="${cmd.daftarKurs}" var="kurs" varStatus="st">
											<tr>
												<c:choose>
													<c:when test="${cmd.daftarKurs[st.index].flag_insert eq \"0\"}">
														<td></td>
														<td>
															<spring:bind path="cmd.daftarKurs[${st.index}].lku_id">
																<select name="${status.expression}" disabled="disabled">
																<c:forEach items="${select_kurs}" var="mata">
																	<option value="${mata.ID}" <c:if test="${mata.ID eq status.value}">selected</c:if>>${mata.SYMBOL}</option>
																</c:forEach>
																</select>
															</spring:bind>
														</td>
														<td>
															<spring:bind path="cmd.daftarKurs[${st.index}].lkh_date">
																<script>inputDate('${status.expression}', '${status.value}', true);</script>	
															</spring:bind>
														</td>
													</c:when>
													<c:otherwise>
														<td>
															<img alt="Delete" src="${path}/include/image/delete.gif" style="cursor: pointer;" onclick="return ayo(this, 'delete');" name="${st.index}">
														</td>
														<td>
															<spring:bind path="cmd.daftarKurs[${st.index}].lku_id">
																<select name="${status.expression}">
																<c:forEach items="${select_kurs}" var="mata">
																	<option value="${mata.ID}" <c:if test="${mata.ID eq status.value}">selected</c:if>>${mata.SYMBOL}</option>
																</c:forEach>
																</select>
															</spring:bind>
														</td>
														<td>
															<spring:bind path="cmd.daftarKurs[${st.index}].lkh_date">
																<script>inputDate('${status.expression}', '${status.value}', false);</script>	
															</spring:bind>
														</td>
													</c:otherwise>
												</c:choose>
														
												<td><form:input path="daftarKurs[${st.index}].lkh_currency" cssStyle="text-align: right;"/></td>
												<td><input type="text" value="${kurs.lkh_kurs_beli}" class="disabled" style="text-align: right;" readonly="readonly"/></td>
												<td><input type="text" value="${kurs.lkh_kurs_jual}" class="disabled" style="text-align: right;" readonly="readonly"/></td>
											</tr>
										</c:forEach>
										</tbody>
									</table>
									<br>
									
									<c:if test="${sessionScope.currentUser.lde_id eq '01' or sessionScope.currentUser.lde_id eq '15'}">
									<input type="button" name="btnAdd" value="Add" onclick="return ayo(this, 'add');"
										accesskey="A" onmouseover="return overlib('Alt-A', AUTOSTATUS, WRAP);" onmouseout="nd();">
									<input type="button" name="btnAdd" value="Save" onclick="return ayo(this, 'save');"
										accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
									</c:if>
									
									<spring:bind path="cmd.*">
										<c:if test="${not empty status.errorMessages}">
											<div id="error">ERROR:<br>
											<c:forEach var="error" items="${status.errorMessages}">
																- <c:out value="${error}" escapeXml="false" />
												<br />
											</c:forEach>
											</div>
										</c:if>
									</spring:bind>
										
								</fieldset>
							</td>
						</tr>
					</table>
				
				</form:form>
			</div>
			
		</div>
	</div>

	<script>
		<c:if test="${not empty param.sukses}">
			alert('Kurs berhasil diinput');
		</c:if>
		<c:if test="${not empty kursBelomAda}">
			alert('${kursBelomAda}');
		</c:if>
	</script>
	
</body>
</html>