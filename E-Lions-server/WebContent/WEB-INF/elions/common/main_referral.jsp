<%@ include file="/include/page/header.jsp"%>
<script>
	function tampil(flag){
		var noRef='';
		no=document.formpost.nomor.value;
		if(flag=='1'){//input
			document.getElementById('infoFrame').src='${path }/common/referral.htm?window=pilih_referral&nomor='+no+'&flag='+flag+'&p=1';
		}else if(flag == '2'){
			popWin('${path}/common/follow_up.htm?nomor='+no, 150, 640);
		}else
			document.getElementById('infoFrame').src='${path }/common/input_referral.htm?nomor='+no+'&flag='+flag;
	}
	
	function tes(){//bagian ini untuk merefresh parent saat melakukan submit di iframe(childnya).Yang jadi child ada di halaman input_referral.jsp
		//alert('Masuk kesini');
		window.location='${path}/common/referral.htm?window=main_referral';
		
	}
	
	function nomorNasabah(nomor){
	 alert('back to parent');
	 document.getElementById('nomor2').value = nomor;
	}
	
</script>
<body onload="setFrameSize('infoFrame', 45); " onresize="setFrameSize('infoFrame', 45);" style="height: 100%;" >

<form:form id="formpost" name="formpost" commandName="cmd">
	<table class="entry2" width="98%">
		<tr>
			<th>Nomor</th>
			<td>
				<input style="border-color:#6893B0;" type="text" disabled="disabled" id="nomor" name="nomor" value="${nomor}" size="35">
				<c:if test="${nomor eq ''}">
					<script>
						alert("Masukkan Kode Nasabah/No Referral Terlebih Dahulu");
						popWin('${path}/common/referral.htm?window=cari_referral', 500, 500);
					</script>
				</c:if>
				<input type="button" value="Cari" onClick="popWin('${path}/common/referral.htm?window=cari_referral', 500, 500);" />
				<input type="button" value="SHow" onClick="tampil('0');" />
				<input type="button" value="Input Referral" onClick="tampil('1');" />
				<input type="button" value="Follow Up" onclick="tampil('2');" />
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<iframe src="${path}/common/input_referral.htm?nomor=${nomor}&flag=0" name="infoFrame" id="infoFrame"
					width="100%"  > Please Wait... </iframe>
			</td>
		</tr>
	</table>
</form:form>
</body>
<%@ include file="/include/page/footer.jsp"%>