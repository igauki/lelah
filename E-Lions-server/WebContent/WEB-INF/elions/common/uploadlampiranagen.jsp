<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript">

function CekRevisi(){
	var a = document.getElementById('jenisupload').value;
	window.location='${path}/common/uploadlampiranagen.htm?flag=1&jenisupload='+a;
}

function SubmitForm(path){
		alert(path);
		//window.location='${path }/common/input_referral.htm?nomor=${cmd.nasabah.mns_kd_nasabah}&flag=0';
	}

function ubahStatus(value) {
		
		document.getElementById("idDiv").innerHTML = ""
		input = document.createElement("select");
		input.setAttribute("name","level");
		input.setAttribute("id", "level");
		var opti1;
		var data;
		
					
		if(value == "PERJANJIAN") {
			data = new Array("","LAMPIRAN AD INKUBATOR(AGENCY)","LAMPIRAN AD KANTOR MANDIRI(AGENCY)","LAMPIRAN AM(AGENCY)","LAMPIRAN SM(AGENCY)","LAMPIRAN SE(AGENCY)","LAMPIRAN RD(HYBRID)","LAMPIRAN RM(HBYRID)","LAMPIRAN DM(HYBRID)","LAMPIRAN BM(HYBRID)","LAMPIRAN SM(HYBRID)","LAMPIRAN FC(HYBRID)","LAMPIRAN RM(REGIONAL)", "LAMPIRAN SBM(REGIONAL)", "LAMPIRAN BM(REGIONAL)","LAMPIRAN UM(REGIONAL)","LAMPIRAN ME(REGIONAL)","LAMPIRAN BRIDGE AD","LAMPIRAN BRIDGE AM","LAMPIRAN BRIDGE SM","LAMPIRAN BRIDGE SE","LAMPIRAN AD INKUBATOR(NEW AGENCY)","LAMPIRAN AD KANTOR MANDIRI(NEW AGENCY)","LAMPIRAN AM(NEW AGENCY)","LAMPIRAN SM(NEW AGENCY)","LAMPIRAN SE(NEW AGENCY)");
			
			document.getElementById("ket").innerHTML = " JENIS ";
			document.getElementById('tgl').style.visibility = "hidden";
			document.getElementById('idDiv').style.visibility = "visible";
			document.getElementById('viewbutton').style.visibility = "visible";
			document.getElementById('row').style.visibility = "visible";
		}
			else if(value == "STATUS" ) {
			data = new Array("","AKTIF","TIDAK AKTIF");
			document.getElementById("ket").innerHTML = " KONDISI ";
			document.getElementById('tgl').style.visibility = "hidden";
			document.getElementById('idDiv').style.visibility = "visible";
			document.getElementById('viewbutton').style.visibility = "visible";
			document.getElementById('row').style.visibility = "visible";
		}
			else if(value == "TANGGAL") {
			
			document.getElementById('ket').innerHTML = " MULAI DARI ";
			document.getElementById('tgl').style.visibility = "visible";				
			document.getElementById('idDiv').style.visibility = "hidden";
			document.getElementById('viewbutton').style.visibility = "visible";
			document.getElementById('row').style.visibility = "visible";
		}
			else if(value == "") {
			
			document.getElementById('ket').innerHTML = "";	
			document.getElementById('tgl').style.visibility = "hidden";
			document.getElementById('idDiv').style.visibility = "hidden";
			document.getElementById('viewbutton').style.visibility = "hidden";
			document.getElementById('row').style.visibility = "hidden";
		}
			
		
		// buat option baru
		for(var isi in data) {
			opti1 = document.createElement("option");
			opti1.setAttribute("value",data[isi]);
			opti1.appendChild(document.createTextNode(data[isi]));
			input.appendChild(opti1);			
		}
		document.getElementById("idDiv").appendChild(input);
	}

</script>


<BODY onload="document.title='Upload Lampiran Perjanjian Keagenan'" style="height: 100%;">
	<form method="post" name="formpost" style="text-align: center;" enctype="multipart/form-data">
		<table class="entry2">
			<tr>
				<td>
					<fieldset>
						<legend>Upload Lampiran Perjanjian Keagenan</legend>
						<table class="entry2">
							<tr>
								<th>Jenis/Judul Perjanjian</th>
								<td align="left"><input type="hidden" disabled="disabled" size="30" id="judul" name="judul" value="Lampiran Perjanjian Keagenan">Lampiran Perjanjian Keagenan</td>
							</tr>
							<tr>
								<th>Jenis Lampiran Perjanjian Keagenan</th>
								<th align="left">
									<select name="jenisupload" onchange="CekRevisi();">
										<option></option>
										<option value="Lampiran AD Inkubator(AGENCY)">Lampiran AD Inkubator(AGENCY)</option>
										<option value="Lampiran AD Kantor Mandiri(AGENCY)">Lampiran AD Kantor Mandiri(AGENCY)</option>
										<option value="Lampiran AM(AGENCY)">Lampiran AM(AGENCY)</option>
										<option value="Lampiran SM(AGENCY)">Lampiran SM(AGENCY)</option>
										<option value="Lampiran SE(AGENCY)">Lampiran SE(AGENCY)</option>
										<option value="Lampiran RD(HYBRID)">Lampiran RD(HYBRID)</option>
										<option value="Lampiran RM(HYBRID)">Lampiran RM(HYBRID)</option>
										<option value="Lampiran DM(HYBRID)">Lampiran DM(HYBRID)</option>
										<option value="Lampiran BM(HYBRID)">Lampiran BM(HYBRID)</option>
										<option value="Lampiran SM(HYBRID)">Lampiran SM(HYBRID)</option>
										<option value="Lampiran FC(HYBRID)">Lampiran FC(HYBRID)</option>
										<option value="Lampiran RM(REGIONAL)">Lampiran RM(REGIONAL)</option>
										<option value="Lampiran SBM(REGIONAL)">Lampiran SBM(REGIONAL)</option>
										<option value="Lampiran BM(REGIONAL)">Lampiran BM(REGIONAL)</option>
										<option value="Lampiran UM(REGIONAL)">Lampiran UM(REGIONAL)</option>
										<option value="Lampiran ME(REGIONAL)">Lampiran ME(REGIONAL)</option>
										<option value="Lampiran BRIDGE AD">Lampiran BRIDGE AD</option>
										<option value="Lampiran BRIDGE AM">Lampiran BRIDGE AM</option>
										<option value="Lampiran BRIDGE SM">Lampiran BRIDGE SM</option>
										<option value="Lampiran BRIDGE SE">Lampiran BRIDGE SE</option>
										<option value="Lampiran AD Inkubator(NEW AGENCY)">Lampiran AD Inkubator(NEW AGENCY)</option>
										<option value="Lampiran AD Kantor Mandiri(NEW AGENCY)">Lampiran AD Kantor Mandiri(NEW AGENCY)</option>
										<option value="Lampiran AM(NEW AGENCY)">Lampiran AM(NEW AGENCY)</option>
										<option value="Lampiran SM(NEW AGENCY)">Lampiran SM(NEW AGENCY)</option>
										<option value="Lampiran SE(NEW AGENCY)">Lampiran SE(NEW AGENCY)</option>
									</select> 
								</th>
							</tr>
							<tr>
								<th>Revisi ke/Tanggal</th>
								<th align="left"><input style="text-align: center;background-color: #FFFFD2;" disabled="disabled" type="text" size="2" id="revisi" name="revisi" value="${revisi}">
									<script>inputDate('tanggalrevisi', '${status.value}', false, '', 9);</script>
								</th>
							</tr>
							<tr>
								<th>Jabatan Agen</th>
								<th align="left"><input style="text-align: left; background-color: #FFFFD2;" disabled="disabled" type="text" size="30" id="jabatan" name="jabatan" value="${jabatan}"></th>
							</tr>
							<tr>
								<th>Alasan Penggantian/Revisi Lampiran Perjanjian</th>
								<th align="left"><input type="text" size="30" name="keterangan" ></th>
							</tr>
							<tr>
								<th>Coding Lampiran Perjanjian</th>
								<th align="left"><input style="text-align: left; background-color: #FFFFD2;" disabled="disabled" type="text" size="38" id="kode" name="kode" value="${kode}"></th>
							</tr>
							<tr>
								<th>Menggantikan Lampiran Perjanjian dengan Code</th>
								<th align="left"><input style="text-align: left; background-color: #FFFFD2;" disabled="disabled" type="text" size="38" id="oldkode" name="oldkode" value="${oldkode}"> </th>
							</tr>
							<tr>
								<th>Ketentuan yang Mendasari perubahan Lampiran</th>
								<th align="left"><input type="text" size="30" name="ketentuan"> </th>
							</tr>
							<tr>
								<th>File Perjanjian Keagenan</th>
								<th align="left"><input type="file" name="file1" size="36" /> <span class="info">*Jenis upload file dalam bentuk .pdf</span></th>
							</tr>
							<tr>
								<th></th>
								<td><input type="submit" name="uploadbutton" value="Upload"></td>
							</tr>
							<tr>
								<td>
									<span class="info">*Semua kolom harus diisi, Tidak boleh kosong</span>
								</td>
							</tr>
						</table>
					</fieldset>
				</td>
			</tr>
			<tr>
				<td>
					<fieldset>
						<legend>List Dokumen Perjanjian Keagenan</legend>
						<table class="entry2">
							<tr>
								<th align="left">Pencarian Dokumen berdasarkan : 
									<select name="jenis" onchange="ubahStatus(value)">
										<option></option>
										<option value="PERJANJIAN">PERJANJIAN</option>
										<option value="STATUS">STATUS</option>
										<option value="TANGGAL">TANGGAL REVISI</option>
									</select>
									<span id="ket"></span>
									<span id="idDiv">
										<select id="level" name="level" style="visibility: hidden;">
											<option></option>
										</select>
									</span>
									<span id="tgl" style="visibility:hidden;">
										<script>inputDate('tglawal', '${status.value}', false, '', 9);</script>
										S/D
										<script>inputDate('tglakhir', '${status.value}', false, '', 9);</script>
									</span>
								</th>
							</tr>
							<tr id="row" style="visibility: hidden;">
								<th>
									<input style="visibility: hidden;" type="submit" id="viewbutton" name="viewbutton" value="View">
								</th>
							</tr>
						</table>
					</fieldset>
				</td>
			</tr>
			<c:if test="${not empty list}">
			<tr>
				<th>
					<table class="entry2">
						<thead>
							<tr>
								<th width="5%">No</th>
								<th width="40%">Dokumen</th>
								<th width="10%">Tanggal Revisi</th>
								<th>Status</th>
								<th>User</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${list}" var="s" varStatus="st">
								<tr>
									<th>${st.count}</th>
									<th align="left">${s.TEMP_FILENAME}</th>
									<th align="center"><fmt:formatDate value="${s.REVISI_DATE}" pattern="dd/MM/yyyy" /></th>
									<th>${s.STATUS}</th>
									<th>${s.LUS_LOGIN_NAME}</th>
									<td align="center">
										<a style="color: blue;" href="uploadlampiranagen.htm?download=download&uploadid=${s.UPLOAD_ID}&jenis=${jenis}&level=${level}&tglawal=${tglawal}&tglakhir=${tglakhir}" name="download" id="download" >Download</a>
										<input type="hidden" name="uploadid" id="uploadid" value="${s.UPLOAD_ID}"> 
									</td>
									
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</th>
			</tr>
			</c:if>
		</table>
		<table class="entry2">
			<c:if test="${not empty errors}">
				<tr>
					
					<td>
						<div id="error" style="text-transform: none;">ERROR:<br>
							<c:forEach var="err" items="${errors}">
								- <c:out value="${err}" escapeXml="false" />
								<br />
								<script>
									pesan += '${err}\n';
								</script>
							</c:forEach>
						</div>
					</td>
				</tr>
			</c:if>
		</table>
	</form>
</BODY>
<c:if test="${not empty pesan}">
	<script>alert('${pesan}');</script>
</c:if>

<c:if test="${not empty jenisupload}">
	<script type="text/javascript">
		var a = '${jenisupload}';
		document.getElementById('jenisupload').value = a;
	</script>
</c:if>

<%@ include file="/include/page/footer.jsp"%>