<%@ include file="/include/page/header.jsp"%>
<script>
</script>
<body style="height: 100%;" onload='setupPanes("container1", "tab1");'>

<div class="tab-container" id="container1">
	<ul class="tabs">
		<li><a href="#" onClick="return showPane('pane1', this)" id="tab1">Message of the Day</a></li>
	</ul>
	<div class="tab-panes">
		<div id="pane1" class="panes" style="text-align: left;">
			<form name="formpost" method="post">
			<table class="entry2" style="width: 100%;">
				<tr>
					<th>Message of the Day<br>(10 hari terakhir)</th>
					<td>
						<table class="displaytag" style="width: 580px">
							<thead>
								<tr>
									<th>Date</th>
									<th>Message</th>
									<th>Create Date</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${listMotd}" var="m">
								<tr>
									<td><fmt:formatDate value="${m.MSMD_ID}" pattern="dd/MM/yyyy" /></td>
									<td><textarea readonly="readonly" rows="2" cols="80">${m.MSMD_MESSAGE}</textarea></td>
									<td><fmt:formatDate value="${m.MSMD_CREATE_DT}" pattern="dd/MM/yyyy" /></td>
								</tr>
								</c:forEach>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<th>Baru</th>
					<td>
						<table class="displaytag" style="width: 580px">
							<thead>
								<tr>
									<th>Date</th>
									<th>Message</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<script>inputDate('tanggal', '${sysDate}', false);</script>
									</td>
									<td><textarea name="pesan" rows="4" cols="80" onkeyup="textCounter(this, 200);" onkeydown="textCounter(this, 200); "></textarea></td>
								</tr>
							</tbody>
						</table>
						<div class="info">
							Catatan: Data akan menimpa pesan pada hari yang sama
						</div>
						<input type="submit" value="Simpan" name="simpan">
					</td>
				</tr>
			</table>
			</form>
		</div>
</div>
</body>
<%@ include file="/include/page/footer.jsp"%>