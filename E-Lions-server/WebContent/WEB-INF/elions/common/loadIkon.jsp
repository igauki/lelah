<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head> 
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/disableKeys.js"></script>
		<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
		<script>hideLoadingMessage();</script>
	</head>
	<script>
		function pilih(value,value2){
			self.opener.document.formpost.ikon.src=value;
			self.opener.document.formpost.elements('menu.icon').value=value2;
			window.close();
		}
	</script>
<body style="height: 100%;">
	<form:form id="formpost" name="formpost" commandName="cmd">
		<table class="entry2">
		<c:forEach items="${lsIkonNew}" var="x" >	
			<tr>
				<c:forEach items="${x.LSBARIS}" var="y">	
				<th>
					<a href="#"><img class="noBorder" onClick="pilih('${y.VALUE}','${y.VALUE2}');" src="${y.VALUE}"></a>
				</th>
				</c:forEach>
			</tr>
		</c:forEach>	
		<tr>
			<th colspan="5"><input type="button" value="close" onClick="window.close();"></th>
		</tr>
		</table>
	</form:form>
</div>
</body>
</html>