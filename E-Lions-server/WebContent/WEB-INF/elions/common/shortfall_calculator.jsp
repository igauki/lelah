<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>

<script>

function JumlahA(){
	jumlah = document.getElementById('jumlah_butuh').value;
	tahun = document.getElementById('tahun_butuh').value;
	rate = document.getElementById('rate_inflasi').value;
	jumlahwithoutcomma = jumlah.replace(/,/g,"");
	if(jumlah !='' && tahun != '' && rate !=''){
		tingkat = TotalInflation(jumlahwithoutcomma, rate, tahun) ;
		document.getElementById('jumlah_mningkat').value =formatNumber(tingkat,2); 
	}
}

function JumlahB(){
	jumlah = document.getElementById('jumlah_tersedia').value;
	tahun = document.getElementById('tahun_butuh').value;
	rate = document.getElementById('pct_invest').value;
	jumlahwithoutcomma = jumlah.replace(/,/g,"");
	if(jumlah!='' && rate !='' && tahun !=''){
		tingkat = TotalInflation(jumlahwithoutcomma, rate, tahun);
		document.getElementById('jumlah_harapan').value=formatNumber(tingkat,2);
		document.getElementById('jumlah_harapan2').value=formatNumber(tingkat,2);
	}
	if(rate !=''){
		document.getElementById('pct_invest2').value = rate;
	}
}

function JumlahC(){
	tambahan = document.getElementById('tambahan').value;
	rate = document.getElementById('pct_invest').value;
	tahun = document.getElementById('tahun_butuh').value;
	tambahanwithoutcomma = tambahan.replace(/,/g,"");
	if(tambahan != '' && rate != '' && tahun != ''){
		c = TotalNeedMoney(tambahanwithoutcomma, rate, tahun);
		document.getElementById('jumlah_simpan').value = formatNumber(c,2);
		document.getElementById('jumlah_simpan2').value = formatNumber(c,2);
	}
}

function JumlahTotalAB(){
	jumlahA = document.getElementById('jumlah_mningkat').value;
	jumlahB = document.getElementById('jumlah_harapan').value;
	if(jumlahA != '' && jumlahB != ''){
		jumlahAB = TotalPlusMin(jumlahA, jumlahB);
		document.getElementById('TotalAB').value = formatNumber((parseFloat(-1) * jumlahAB),2);
	}
}

function JumlahTotalABC(){
	AB = document.getElementById('TotalAB').value; 
	C = document.getElementById('jumlah_simpan2').value;
	if(AB != '' && C != ''){
		ABC = TotalPlusMin(AB, C);
		document.getElementById('total_surplus').value = formatNumber(( parseFloat(-1) * ABC),2);
	}
}

function JumlahIncome1(){
	yincome1 = document.getElementById('yincome1').value;
	oincome1 = document.getElementById('oincome1').value;
	yincomewithoutcomma = yincome1.replace(/,/g,"");
	oincomewithoutcomma = oincome1.replace(/,/g,"");
	if(yincome1 != '' && oincome1 !=''){
		jincome = TotalPlus(yincomewithoutcomma, oincomewithoutcomma);
		document.getElementById('income1').value =formatNumber(parseFloat(jincome),2); 
	}
}

function JumlahIncome2(){
	yincome2 = document.getElementById('yincome2').value;
	oincome2 = document.getElementById('oincome2').value;
	yincomewithoutcomma = yincome2.replace(/,/g,"");
	oincomewithoutcomma = oincome2.replace(/,/g,"");
	if(yincome2 != '' && oincome2 !=''){
		jincome = TotalPlus(yincomewithoutcomma, oincomewithoutcomma);
		document.getElementById('income2').value =formatNumber(parseFloat(jincome),2); 
	}
}

function JumlahReduction1(){
	kpr1 = document.getElementById('rkpr1').value;
	credit1 = document.getElementById('rcredit1').value;
	personal1 = document.getElementById('rperson1').value;
	saving1 = document.getElementById('rsaving1').value;
	premi1 = document.getElementById('rpremi1').value;
	kprwithoutcomma = kpr1.replace(/,/g,"");
	creditwithoutcomma = credit1.replace(/,/g,"");
	personalwithoutcomma = personal1.replace(/,/g,"");
	savingwithoutcomma = saving1.replace(/,/g,"");
	premiwithoutcomma = premi1.replace(/,/g,"");
	if(kpr1 != '' && credit1 !='' && personal1 != '' && saving1 != '' && premi1 != ''){
		jreduction = TotalReductionYear(kprwithoutcomma, creditwithoutcomma, personalwithoutcomma, savingwithoutcomma, premiwithoutcomma);
		document.getElementById('reduction1').value = formatNumber(jreduction,2); 
	}
}

function JumlahReduction2(){
	kpr2 = document.getElementById('rkpr2').value;
	credit2 = document.getElementById('rcredit2').value;
	personal2 = document.getElementById('rperson2').value;
	saving2 = document.getElementById('rsaving2').value;
	premi2 = document.getElementById('rpremi2').value;
	kprwithoutcomma = kpr2.replace(/,/g,"");
	creditwithoutcomma = credit2.replace(/,/g,"");
	personalwithoutcomma = personal2.replace(/,/g,"");
	savingwithoutcomma = saving2.replace(/,/g,"");
	premiwithoutcomma = premi2.replace(/,/g,"");
	if(kpr2 != '' && credit2 !='' && personal2 != '' && saving2 != '' && premi2 != ''){
		jreduction = TotalReductionYear(kprwithoutcomma, creditwithoutcomma, personalwithoutcomma, savingwithoutcomma, premiwithoutcomma);
		document.getElementById('reduction2').value = formatNumber(jreduction,2); 
	}
}

function JumlahStandar1(){
	income1 = document.getElementById('income1').value;
	reduction1 = document.getElementById('reduction1').value;
	incomewithoutcomma = income1.replace(/,/g,"");
	reductionwithoutcomma = reduction1.replace(/,/g,"");
	if(income1 != '' && reduction1 != ''){
		jstandar = TotalPlusMin(incomewithoutcomma, reductionwithoutcomma);
		document.getElementById('standar1').value = formatNumber(formatNumber(jstandar,2) * parseFloat(-1),2);
	}
}

function JumlahStandar2(){
	income2 = document.getElementById('income2').value;
	reduction2 = document.getElementById('reduction2').value;
	incomewithoutcomma = income2.replace(/,/g,"");
	reductionwithoutcomma = reduction2.replace(/,/g,"");
	if(income2 != '' && reduction2 != ''){
		jstandar = TotalPlusMin(incomewithoutcomma, reductionwithoutcomma);
		document.getElementById('standar2').value = formatNumber(formatNumber(jstandar,2) * parseFloat(-1),2);
	}
}

function JumlahBiayaPlus1(){
	child1 = document.getElementById('child1').value;
	medical1 = document.getElementById('medical1').value;
	childwithoutcomma = child1.replace(/,/g,"");
	medicalwithoutcomma = medical1.replace(/,/g,"");
	if(child1 != '' && medical1 != ''){
		jbiaya = TotalPlus(childwithoutcomma, medicalwithoutcomma);
		document.getElementById('biayaplus1').value = formatNumber(jbiaya,2);
	}
}

function JumlahBiayaPlus2(){
	child2 = document.getElementById('child2').value;
	medical2 = document.getElementById('medical2').value;
	childwithoutcomma = child2.replace(/,/g,"");
	medicalwithoutcomma = medical2.replace(/,/g,"");
	if(child2 != '' && medical2 != ''){
		jbiaya = TotalPlus(childwithoutcomma, medicalwithoutcomma);
		document.getElementById('biayaplus2').value = formatNumber(jbiaya,2);
	}
}

function Jumlahbutuh1(){
	standar1 = document.getElementById('standar1').value;
	biaya1 = document.getElementById('biayaplus1').value;
	if(standar1 != '' && biaya1 != ''){
		tingkatbutuh = TotalMin(standar1, biaya1);
		document.getElementById('tingkatbutuh1').value = formatNumber((tingkatbutuh * parseFloat(-1)),2) 
	}
}

function Jumlahbutuh2(){
	standar2 = document.getElementById('standar2').value;
	biaya2 = document.getElementById('biayaplus2').value;
	if(standar2 != '' && biaya2 != ''){
		tingkatbutuh = TotalMin(standar2, biaya2);
		document.getElementById('tingkatbutuh2').value = formatNumber((tingkatbutuh * parseFloat(-1)),2) 
	}
}

function JumlahIncomePlus1(){
	benefit1 = document.getElementById('benefit1').value;
	pensiun1 = document.getElementById('pensiun1').value;
	benefitwithoutcomma = benefit1.replace(/,/g,"");
	pensiunwithoutcomma = pensiun1.replace(/,/g,"");
	if(benefit1 != '' && pensiun1 != ''){
		incomeplus = TotalIncomePlus(benefitwithoutcomma, pensiunwithoutcomma);
		document.getElementById('incomeplus1').value = formatNumber(incomeplus,2);
	}
}

function JumlahIncomePlus2(){
	benefit2 = document.getElementById('benefit2').value;
	pensiun2 = document.getElementById('pensiun2').value;
	benefitwithoutcomma = benefit2.replace(/,/g,"");
	pensiunwithoutcomma = pensiun2.replace(/,/g,"");
	if(benefit2 != '' && pensiun2 != ''){
		incomeplus = TotalIncomePlus(benefitwithoutcomma, pensiunwithoutcomma);
		document.getElementById('incomeplus2').value = formatNumber(incomeplus,2);
	}
}

function JumlahKurangButuh1(){
	tingkatbutuh1 = document.getElementById('tingkatbutuh1').value;
	incomeplus1 = document.getElementById('incomeplus1').value;
	if(tingkatbutuh1 != '' && incomeplus1 != ''){
		kurangbutuh = TotalPlusMin(tingkatbutuh1, incomeplus1);
		document.getElementById('kurangbutuh1').value = formatNumber((parseFloat(kurangbutuh) * parseFloat(-1)),2);
		document.getElementById('kurangbutuh11').value = formatNumber((parseFloat(kurangbutuh) * parseFloat(-1)),2);
	}
}

function JumlahKurangButuh2(){
	tingkatbutuh2 = document.getElementById('tingkatbutuh2').value;
	incomeplus2 = document.getElementById('incomeplus2').value;
	if(tingkatbutuh2 != '' && incomeplus2 != ''){
		kurangbutuh = TotalPlusMin(tingkatbutuh2, incomeplus2);
		document.getElementById('kurangbutuh2').value = formatNumber((parseFloat(kurangbutuh) * parseFloat(-1)),2);
		document.getElementById('kurangbutuh21').value = formatNumber((parseFloat(kurangbutuh) * parseFloat(-1)),2);
	}
}

function JumlahModal1(){
	kurangbutuh = document.getElementById('kurangbutuh11').value;
	interest = document.getElementById('interest1').value;
	if(kurangbutuh != '' && interest != ''){
		modal = TotalCapital(kurangbutuh, interest);
		document.getElementById('modal1').value = formatNumber((modal * parseFloat(-1)) ,2);
	}
}

function JumlahModal2(){
	kurangbutuh = document.getElementById('kurangbutuh21').value;
	interest = document.getElementById('interest2').value;
	if(kurangbutuh != '' && interest != ''){
		modal = TotalCapital(kurangbutuh, interest);
		document.getElementById('modal2').value = formatNumber((modal * parseFloat(-1)), 2);
	}
}

function JumlahUP1(){
	modal = document.getElementById('modal1').value;
	capital =document.getElementById('capital1').value;
	csaving = document.getElementById('csaving1').value;
	lumpsum =document.getElementById('lumpsum1').value;
	capitalwithoutcomma = capital.replace(/,/g,"");
	csavingwithoutcomma = csaving.replace(/,/g,"");
	lumpsumwithoutcomma = lumpsum.replace(/,/g,"");
	if(modal != '' && capital != '' && csaving != '' && lumpsum != ''){
		jUP = TotalUP(modal, capitalwithoutcomma, csavingwithoutcomma, lumpsumwithoutcomma);
		document.getElementById('UP1').value = formatNumber((jUP * parseFloat(-1)), 2);
	}
}

function JumlahUP2(){
	modal = document.getElementById('modal2').value;
	capital =document.getElementById('capital2').value;
	csaving = document.getElementById('csaving2').value;
	lumpsum =document.getElementById('lumpsum2').value;
	capitalwithoutcomma = capital.replace(/,/g,"");
	csavingwithoutcomma = csaving.replace(/,/g,"");
	lumpsumwithoutcomma = lumpsum.replace(/,/g,"");
	if(modal != '' && capital != '' && csaving != '' && lumpsum != ''){
		jUP = TotalUP(modal, capitalwithoutcomma, csavingwithoutcomma, lumpsumwithoutcomma);
		document.getElementById('UP2').value = formatNumber((jUP * parseFloat(-1)), 2);
	}
}

function JumlahModalUbah(){
	aincome = document.getElementById('aincome').value;
	pinterest = document.getElementById('pinterest1').value;
	aincomewithoutcomma = aincome.replace(/,/g,"");
	if(aincome != '' && pinterest != ''){
		jModalUbah = TotalChangeCapital(aincomewithoutcomma, pinterest);
		document.getElementById('jmodalbrubah').value = formatNumber(jModalUbah, 2);
	}
}

function JumlahPendapatan(){
	capital = document.getElementById('capital').value;
	pinterest = document.getElementById('pinterest2').value;
	pyear = document.getElementById('pyear').value;
	capitalwithoutcomma = capital.replace(/,/g,"");
	if(capital != '' && pinterest != '' && pyear != ''){
		jPendapatan = TotalIncome(capitalwithoutcomma, pinterest, pyear);
		document.getElementById('jpendapatan').value = formatNumber(jPendapatan, 2);
	}
}

</script>

<body id="calculator" style="height: 100%;" onload="AllFunctionCalculation();setupPanes('container1', 'tab${cmd.showTab}'); pesan();" onresize="resizeTab();">
	<form:form id="formpost" name="formpost" commandName="cmd">
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1" style="width: 33%">Surplus Calculator</a>
					<a href="#" onClick="return showPane('pane2', this)" id="tab2" style="width: 33%">Protection Planner</a>
					<a href="#" onClick="return showPane('pane3', this)" id="tab3" style="width: 34%">Income Planner</a>
				</li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<table class="entry2">
						<tr>
							<td width="60%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Prediksi Kekurangan atau Kelebihan</td>
							<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Nama Nasabah : ${cmd.nasabah.mns_nama}</td>
						</tr>
					</table>
					<fieldset>
						<legend>Apakah yang Anda Inginkan?</legend>
						<table class="entry2">
							<tr>
								<th>Jumlah yang dibutuhkan untuk mencapai tujuan</th>
								<td width="10%">
									<form:input cssStyle="border-color:#6893B0; text-align: right;" path="surplusCalc.jumlah_butuh" id="jumlah_butuh" onchange="JumlahA();JumlahTotalAB();JumlahTotalABC();" size="20"/></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<th>Tahun yang dibutuhkan untuk mencapai tujuan</th>
								<td><form:input cssStyle="border-color:#6893B0; text-align: right;" path="surplusCalc.tahun_butuh" id="tahun_butuh" onchange="JumlahA();JumlahB();JumlahTotalAB();JumlahTotalABC();" size="20"/></td>
								<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;" align="left">Tahun</td>
								<td></td>
							</tr>
							<tr>
								<th width="50%">Pengaruh dari inflasi-berapa rate inflasi yang Anda inginkan sewaktu menghitung jumlah yang Anda butuhkan</th>
								<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="surplusCalc.rate_inflasi" id="rate_inflasi" maxlength="3" onchange="JumlahA();JumlahTotalAB();JumlahTotalABC();"/></td>
								<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;" align="left">%</td>
								<td></td>
							</tr>
							<tr>
								<th>Oleh karena itu, jumlah yang dibutuhkan meningkat menjadi..</th>
								<td></td>
								<td style="font-weight: bold; text-transform:none; font-size: 7.5pt; text-align: left;">A</td>
								<td><input style="border-style: none; text-align: right;" type="text" name="jumlah_mningkat" id="jumlah_mningkat" readonly="readonly" value=<fmt:formatNumber pattern="#,###.00" value="" type="currency" />></td>
							</tr>
						</table>
					</fieldset>
					<fieldset>
						<legend>Apa yang Anda miliki?</legend>
						<table class="entry2">
							<tr>
								<th width="50%">Jumlah saat ini yang tersedia untuk tujuan ini adalah..</th>
								<td width="10%"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="surplusCalc.jumlah_tersedia" id="jumlah_tersedia" onchange="JumlahB();JumlahTotalAB();JumlahTotalABC();"/></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<th>% Hasil investasi yang Anda dapatkan dari ini sebesar</th>
								<td><form:input cssStyle="border-color:#6893B0; text-align: right;" path="surplusCalc.pct_invest" id="pct_invest" maxlength="3" onchange="JumlahB();JumlahTotalAB();JumlahTotalABC();"/></td>
								<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;" align="left">%</td>
								<td></td>
							</tr>
							<tr>
								<th width="50%">Pada saat Anda membutuhkan uang Anda(Asumsi % rate sama) Jumlah yang bisa Anda harapkan diperkirakan adalah</th>
								<td><input style="border-style: none; text-align: right;" type="text" name="jumlah_harapan" id="jumlah_harapan" readonly="readonly" value=""></td>
								<td style="font-weight: bold; text-transform:none; font-size: 7.5pt; text-align: left;">B</td>
								<td><input style="border-style: none; text-align: right;" type="text" name="jumlah_harapan2" id="jumlah_harapan2" readonly="readonly" value=""></td>
							</tr>
							<tr>
								<th>Mengindikasikan potensi untuk kekurangan/kelebihan dari</th>
								<td></td>
								<td style="font-weight: bold; text-transform:none; font-size: 7.5pt; text-align: left;">(A-B)</td>
								<td><input style="border-style: none; text-align: right;" type="text" name="TotalAB" id="TotalAB" readonly="readonly" value=""></td>
							</tr>
						</table>
					</fieldset>
					<fieldset>
						<legend>Pengaruh dari tambahan simpanan yang dapat Anda lakukan?</legend>
						<table class="entry2">
							<tr>
								<th width="50%">Jumlah dari tambahan reguler yang Anda lakukan pada saldo tabungan Anda per bulan</th>
								<td width="10%"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="surplusCalc.tambahan" id="tambahan" onchange="JumlahC();JumlahTotalABC();"/></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<th>% Hasil investasi yang Anda dapatkan(asumsikan % rate sama) adalah..</th>
								<td><input style="border-style: none; text-align: right;" type="text" name="pct_invest2" id="pct_invest2" maxlength="3" readonly="readonly" value=""></td>
								<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;" align="left">%</td>
								<td></td>
							</tr>
							<tr>
								<th>Pada saat Anda membutuhkan Uang Anda (asumsikan % rate sama) adalah..</th>
								<td><input style="border-style: none; text-align: right;" type="text" name="jumlah_simpan" id="jumlah_simpan" readonly="readonly" value=""></td>
								<td width="9%" style="font-weight: bold; text-transform:none; font-size: 7.5pt; text-align: left;">C</td>
								<td><input style="border-style: none; text-align: right;" type="text" name="jumlah_simpan2" id="jumlah_simpan2" readonly="readonly" value=""></td>
							</tr>
						</table>
					</fieldset>
					<fieldset>
						<legend>Menghasilkan kekurangan atau kelebihan?</legend>
						<table class="entry2">
							<tr>
								<th width="50%">Kelebihan ditandai dengan tanda "-"</th>
								<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;" align="center">(A-B+C)</td>
								<td><input style="border-style: none; text-align: right;" type="text" name="total_surplus" id="total_surplus" readonly="readonly" value=""></td>
							</tr>
						</table>
					</fieldset>
					<table class="entry2">
						<tr>
							<th>
								<input type="submit" name="save" id="save" value="Save"/>
								<input type="button" name="close" id="close" value="Close" onclick="window.close();"/>
							</th>
						</tr>
					</table>	
				</div>
				<div id="pane2" class="panes">
					<table class="entry2">
						<tr>
							<td width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Perencanaan Uang Pertanggungan</td>
							<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;text-align: center;">Suami</td>
							<td width="1%"></td>
							<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;text-align: center;">Istri</td>
							<td width="1%"></td>
						</tr>
						<tr>
							<td width="50%"></td>
							<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: left;" path="protectCalc.nama1" id="nama1" /> </td>
							<td></td>
							<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: left;" path="protectCalc.nama2" id="nama2" /> </td>
							<td></td>
						</tr>
					</table>
					<fieldset>
						<legend>Pendapatan yang akan digantikan</legend>
						<table class="entry2">
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Jumlah pendapatan yang diterima per tahun</th>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.yearly_income1" id="yincome1" onchange="JumlahIncome1();JumlahStandar1();Jumlahbutuh1();JumlahKurangButuh1();JumlahModal1();JumlahUP1();"/> </td>
								<td width="1%"></td>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.yearly_income2" id="yincome2" onchange="JumlahIncome2();JumlahStandar2();Jumlahbutuh2();JumlahKurangButuh2();JumlahModal2();JumlahUP2();"/> </td>
								<td width="1%"></td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Pendapatan lainnya</th>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.other_income1" id="oincome1" onchange="JumlahIncome1();JumlahStandar1();Jumlahbutuh1();JumlahKurangButuh1();JumlahModal1();JumlahUP1();"/> </td>
								<td></td>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.other_income2" id="oincome2" onchange="JumlahIncome2();JumlahStandar2();Jumlahbutuh2();JumlahKurangButuh2();JumlahModal2();JumlahUP2();"/> </td>
								<td></td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Jumlah pendapatan saat ini</th>
								<td align="center"><input style="border-style: none; text-align: right;" type="text" name="income1" id="income1" readonly="readonly" value=""></td>
								<td></td>
								<td align="center"><input style="border-style: none; text-align: right;" type="text" name="income2" id="income2" readonly="readonly" value=""></td>
								<td></td>
							</tr>
						</table>
					</fieldset>
					<fieldset>
						<legend>Pengurangan dari pengeluaran bulanan yang disebabkan
						<br/> oleh kejadian(Resiko Meninggal Dunia)
						</legend>
						<table class="entry2">
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">KPR yang memiliki Uang Pertanggungan</th>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.reduction_kpr1" id="rkpr1" onchange="JumlahReduction1();JumlahStandar1();Jumlahbutuh1();JumlahKurangButuh1();JumlahModal1();JumlahUP1();" /> </td>
								<td width="1%"></td>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.reduction_kpr2" id="rkpr2" onchange="JumlahReduction2();JumlahStandar2();Jumlahbutuh2();JumlahKurangButuh2();JumlahModal2();JumlahUP2();" /> </td>
								<td width="1%"></td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Pinjaman atau Kredit lainnya yang memiliki Uang Pertanggungan</th>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.reduction_credit1" id="rcredit1" onchange="JumlahReduction1();JumlahStandar1();Jumlahbutuh1();JumlahKurangButuh1();JumlahModal1();JumlahUP1();" /> </td>
								<td></td>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.reduction_credit2" id="rcredit2" onchange="JumlahReduction2();JumlahStandar2();Jumlahbutuh2();JumlahKurangButuh2();JumlahModal2();JumlahUP2();" /> </td>
								<td></td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Pengurangan atas pengeluaran pribadi(kalau ada)</th>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.reduction_personal1" id="rperson1" onchange="JumlahReduction1();JumlahStandar1();Jumlahbutuh1();JumlahKurangButuh1();JumlahModal1();JumlahUP1();" /> </td>
								<td></td>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.reduction_personal2" id="rperson2" onchange="JumlahReduction2();JumlahStandar2();Jumlahbutuh2();JumlahKurangButuh2();JumlahModal2();JumlahUP2();" /> </td>
								<td></td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Tabungan regular yang dapat dihentikan</th>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.reduction_saving1" id="rsaving1" onchange="JumlahReduction1();JumlahStandar1();Jumlahbutuh1();JumlahKurangButuh1();JumlahModal1();JumlahUP1();" /> </td>
								<td></td>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.reduction_saving2" id="rsaving2" onchange="JumlahReduction2();JumlahStandar2();Jumlahbutuh2();JumlahKurangButuh2();JumlahModal2();JumlahUP2();" /> </td>
								<td></td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Premi asuransi yang dapat dihentikan</th>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.reduction_premi1" id="rpremi1" onchange="JumlahReduction1();JumlahStandar1();Jumlahbutuh1();JumlahKurangButuh1();JumlahModal1();JumlahUP1();" /> </td>
								<td></td>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.reduction_premi2" id="rpremi2" onchange="JumlahReduction2();JumlahStandar2();Jumlahbutuh2();JumlahKurangButuh2();JumlahModal2();JumlahUP2();" /> </td>
								<td></td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Jumlah yang disetahunkan</th>
								<td align="center"><input style="border-style: none; text-align: right;" type="text" name="reduction1" id="reduction1" readonly="readonly" value=""></td>
								<td></td>
								<td align="center"><input style="border-style: none; text-align: right;" type="text" name="reduction2" id="reduction2" readonly="readonly" value=""></td>
								<td></td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Total pendapatan yang dibutuhkan untuk menjaga kehidupan yang standar</th>
								<td align="center"><input style="border-style: none; text-align: right;" type="text" name="standar1" id="standar1" readonly="readonly" value=""></td>
								<td></td>
								<td align="center"><input style="border-style: none; text-align: right;" type="text" name="standar2" id="standar2" readonly="readonly" value=""></td>
								<td></td>
							</tr>
						</table>
					</fieldset>
					<fieldset>
						<legend>Tambahan biaya kehidupan yang mungkin terjadi</legend>
						<table class="entry2">
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Peningkatan biaya pemeliharaan Anak per tahun</th>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.inc_child_cost1" id="child1" onchange="JumlahBiayaPlus1();Jumlahbutuh1();JumlahKurangButuh1();JumlahModal1();JumlahUP1();" /> </td>
								<td width="1%"></td>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.inc_child_cost2" id="child2" onchange="JumlahBiayaPlus2();Jumlahbutuh2();JumlahKurangButuh2();JumlahModal2();JumlahUP2();" /> </td>
								<td width="1%"></td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Biaya medis per tahun</th>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.medical_expense1" id="medical1" onchange="JumlahBiayaPlus1();Jumlahbutuh1();JumlahKurangButuh1();JumlahModal1();JumlahUP1();" /> </td>
								<td></td>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.medical_expense2" id="medical2" onchange="JumlahBiayaPlus2();Jumlahbutuh2();JumlahKurangButuh2();JumlahModal2();JumlahUP2();" /> </td>
								<td></td>
							</tr>
							<tr>
								<th width="50%"></th>
								<td align="center"><input style="border-style: none; text-align: right;" type="text" name="biayaplus1" id="biayaplus1" readonly="readonly" value=""></td>
								<td></td>
								<td align="center"><input style="border-style: none; text-align: right;" type="text" name="biayaplus2" id="biayaplus2" readonly="readonly" value=""></td>
								<td></td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Maka total pendapatan yang dibutuhkan meningkat menjadi</th>
								<td align="center"><input style="border-style: none; text-align: right;" type="text" name="tingkatbutuh1" id="tingkatbutuh1" readonly="readonly" value=""></td>
								<td></td>
								<td align="center"><input style="border-style: none; text-align: right;" type="text" name="tingkatbutuh2" id="tingkatbutuh2" readonly="readonly" value=""></td>
								<td></td>
							</tr>
						</table>						
					</fieldset>
					<fieldset>
						<legend>Tambahan pendapatan yang disebabkan kejadian(resiko Meninggal Dunia)</legend>
						<table class="entry2">
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Uang Pertanggungan yang diperoleh</th>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.benefit_insurance1" id="benefit1" onchange="JumlahIncomePlus1();JumlahKurangButuh1();JumlahModal1();JumlahUP1();" /> </td>
								<td width="1%"></td>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.benefit_insurance2" id="benefit2" onchange="JumlahIncomePlus2();JumlahKurangButuh2();JumlahModal2();JumlahUP2();" /> </td>
								<td width="1%"></td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Biaya medis per tahun</th>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.benefit_pensiun1" id="pensiun1" onchange="JumlahIncomePlus1();JumlahKurangButuh1();JumlahModal1();JumlahUP1();" /> </td>
								<td></td>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.benefit_pensiun2" id="pensiun2" onchange="JumlahIncomePlus2();JumlahKurangButuh2();JumlahModal2();JumlahUP2();" /> </td>
								<td></td>
							</tr>
							<tr>
								<th width="50%"></th>
								<td align="center"><input style="border-style: none; text-align: right;" type="text" name="incomeplus1" id="incomeplus1" readonly="readonly" value=""></td>
								<td></td>
								<td align="center"><input style="border-style: none; text-align: right;" type="text" name="incomeplus2" id="incomeplus2" readonly="readonly" value=""></td>
								<td></td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Maka total pendapatan yang dibutuhkan meningkat menjadi</th>
								<td align="center"><input style="border-style: none; text-align: right;" type="text" name="kurangbutuh1" id="kurangbutuh1" readonly="readonly" value=""></td>
								<td></td>
								<td align="center"><input style="border-style: none; text-align: right;" type="text" name="kurangbutuh2" id="kurangbutuh2" readonly="readonly" value=""></td>
								<td></td>
							</tr>
						</table>
					</fieldset>
					<fieldset>
						<legend>Kebutuhan Modal</legend>
						<table class="entry2">
							<tr>
								<th width="51%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Jumlah pendapatan yang dibutuhkan per tahun</th>
								<td align="center"><input style="border-style: none; text-align: right;" type="text" name="kurangbutuh11" id="kurangbutuh11" readonly="readonly" value=""></td>
								<td width="1%"></td>
								<td align="center"><input style="border-style: none; text-align: right;" type="text" name="kurangbutuh21" id="kurangbutuh21" readonly="readonly" value=""></td>
								<td width="1%"></td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Rata-rata persentase(%) suku bunga yang terjadi(sesuai bunga saat ini)</th>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.pct_interest1" id="interest1" maxlength="3" onchange="JumlahModal1();JumlahUP1();" /></td>
								<td width="1%" align="left" style="text-align: left;font-weight: bold; text-transform:none; font-size: 7.5pt;">%</td>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.pct_interest2" id="interest2" maxlength="3" onchange="JumlahModal2();JumlahUP2();" /></td>
								<td width="1%" align="left" style="text-align: left;font-weight: bold; text-transform:none; font-size: 7.5pt;">%</td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Jumlah modal yang dibutuhkan dalam merancang pendapatan yang tidak tetap</th>
								<td align="center"><input style="border-style: none; text-align: right;" type="text" name="modal1" id="modal1" readonly="readonly" value=""></td>
								<td></td>
								<td align="center"><input style="border-style: none; text-align: right;" type="text" name="modal2" id="modal2" readonly="readonly" value=""></td>
								<td></td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Modal tambahan yang dibutuhkan, contoh: mengganti mobil perusahaan</th>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.add_capital1" id="capital1" onchange="JumlahUP1();" /> </td>
								<td></td>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.add_capital2" id="capital2" onchange="JumlahUP2();" /> </td>
								<td></td>
							</tr>
							<tr>
								<td width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Dikurangi :</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Investasi/Simpanan Modal yang tersedia</th>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.capital_saving1" id="csaving1" onchange="JumlahUP1();" /> </td>
								<td></td>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.capital_saving2" id="csaving2" onchange="JumlahUP2();" /> </td>
								<td></td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Sejumlah dana yang diterima dari polis asuransi saat ini, dan lain-lain</th>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.capital_lump_sums1" id="lumpsum1" onchange="JumlahUP1();" /> </td>
								<td></td>
								<td align="center"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="protectCalc.capital_lump_sums2" id="lumpsum2" onchange="JumlahUP2();" /> </td>
								<td></td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Jumlah Uang Pertanggungan yang dibutuhkan adalah</th>
								<td align="center"><input style="border-style: none; text-align: right;" type="text" name="UP1" id="UP1" readonly="readonly" value=""></td>
								<td></td>
								<td align="center"><input style="border-style: none; text-align: right;" type="text" name="UP2" id="UP2" readonly="readonly" value=""></td>
								<td></td>
							</tr>
						</table>
					</fieldset>
					<table class="entry2">
						<tr>
							<th>
								<input type="submit" name="save" id="save" value="Save"/>
								<input type="button" name="close" id="close" value="Close" onclick="window.close();"/>
							</th>
						</tr>
					</table>	
				</div>
				<div id="pane3" class="panes">
					<table class="entry2">
						<tr>
							<td width="60%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Perencanaan Pendapatan Berjalan dan Pendapatan Tidak Tetap</td>
							<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Nama Nasabah : ${cmd.nasabah.mns_nama}</td>
						</tr>
					</table>
					<fieldset>
						<legend>Perencanaan Pendapatan yang sedang berjalan</legend>
						<table class="entry2">
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Jumlah Pendapatan yang dibutuhkan per tahun</th>
								<td width="20%"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="incomeCalc.annual_income" id="aincome" onchange="JumlahModalUbah();"/>  </td>
								<td></td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Rata-rata persentase(%) suku bunga yang terjadi(sesuai bunga saat ini)</th>
								<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="incomeCalc.pct_interest1" id="pinterest1" maxlength="3" onchange="JumlahModalUbah();"/></td>
								<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;" align="left">%</td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Jumlah modal yang dibutuhkan dalam merancang pendapatan yang berubah</th>
								<td><input style="border-style: none; text-align: right;" type="text" name="jmodalbrubah" id="jmodalbrubah" readonly="readonly" value=""></td>
								<td></td>
							</tr>
						</table>
					</fieldset>
					<fieldset>
						<legend>Perencanaan Pendapatan tidak tetap(asumsi pendapatan diterima sekali setahun)</legend>
						<table class="entry2">
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Jumlah Modal</th>
								<td width="20%"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="incomeCalc.capital" id="capital" onchange="JumlahPendapatan();"/>  </td>
								<td></td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Rata-rata persentase(%) suku bunga yang terjadi(sesuai bunga saat ini)</th>
								<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="incomeCalc.pct_interest2" id="pinterest2" maxlength="3" onchange="JumlahPendapatan();"/></td>
								<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;" align="left">%</td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Periode Waktu</th>
								<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;"><form:input cssStyle="border-color:#6893B0; text-align: right;" path="incomeCalc.period_year" id="pyear" onchange="JumlahPendapatan();"/></td>
								<td style="font-weight: bold; text-transform:none; font-size: 7.5pt;" align="left">Tahun</td>
							</tr>
							<tr>
								<th width="50%" style="font-weight: bold; text-transform:none; font-size: 7.5pt;">Perkiraan pendapatan yang diterima</th>
								<td><input style="border-style: none; text-align: right;" type="text" name="jpendapatan" id="jpendapatan" readonly="readonly" value=""></td>
								<td></td>
							</tr>
						</table>
					</fieldset>
					<table class="entry2">
						<tr>
							<th>
								<input type="submit" name="save" id="save" value="Save"/>
								<input type="button" name="close" id="close" value="Close" onclick="window.close();"/>
							</th>
						</tr>
					</table>
				</div>
			</div>
			<table>
				<tr>
					<td colspan="4">
						<c:if test="${submitSuccess eq true}">
									<script>
										alert("Berhasil Simpan");
										window.location='${path }/common/shortfall_calculator.htm?nomor=${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}';
									</script>
				        </c:if>
					 </td>
				</tr>
			</table>
		</div>
	</form:form>
</body>


	



<%@ include file="/include/page/footer.jsp"%>