<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	function cari(angka){
		if(angka==1){
			if(trim(document.formpost.kataPersonal.value)=='') return false;
			else createLoadingMessage();
		}else if(angka==2){
			if(trim(document.formpost.kataCompany.value)=='') return false;
			else createLoadingMessage();
		}else return false;
	}
	
	function backToParent(mcl_id, mcl_jenis){
		var dok = self.opener.document;
		dok.formpost.elements['personal.mcl_id'].value=mcl_id;
		dok.formpost.elements['personal.mcl_jenis'].value=mcl_jenis;
		dok.formpost.elements['show'].value='true';
		self.opener.ayo(dok,'tampil');
		window.close();
	}
	
</script>
</head>
<BODY onload="document.title='PopUp :: Cari Profile'; setupPanes('container1', 'tab2');" style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li><a href="#" onClick="return showPane('pane1', this)" id="tab1">Personal</a></li>
			<li><a href="#" onClick="return showPane('pane2', this)" id="tab2">Perusahaan</a></li>
		</ul>

		<form method="post" name="formpost" action="${path }/common/menu.htm" style="text-align: center;">
			<input type="hidden" name="frame" value="cariprofile">
			<div class="tab-panes">

				<div id="pane1" class="panes">
					<!-- 
					<table class="entry2">
						<tr>
							<th>Cari:</th>
							<th class="left">
								<select name="tipePersonal">
									<c:forEach var="tipe" items="${cmd.daftarTipePersonal}">
										<option value="${tipe.key}" <c:if test="${param.tipePersonal eq tipe.key }">selected</c:if>>${tipe.value}</option>
									</c:forEach>
								</select>			
								<input type="text" name="kataPersonal" size="35" value="${param.kataPersonal}" onkeypress="if(event.keyCode==13) {document.formpost.searchPersonal.click(); return false;}">
								<input type="submit" name="searchPersonal" value="Search" onclick="return cari(1);"
									accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
								<input type="button" name="close" value="Close" onclick="window.close();"
									accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">
							</th>
						</tr>
					</table> -->
				</div>
				
				<div id="pane2" class="panes">
					<table class="entry2">
						<tr>
							<th>Cari:</th>
							<th class="left">
								<select name="tipeCompany">
									<c:forEach var="tipe" items="${cmd.daftarTipeCompany}">
										<option value="${tipe.key}" <c:if test="${param.tipeCompany eq tipe.key }">selected</c:if>>${tipe.value}</option>
									</c:forEach>
								</select>			
								<input type="text" name="kataCompany" size="35" value="${param.kataCompany }" onkeypress="if(event.keyCode==13) {document.formpost.searchCompany.click(); return false;}">
								<input type="submit" name="searchCompany" value="Search" onclick="return cari(2);"
									accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
								<input type="button" name="close" value="Close" onclick="window.close();"
									accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">
							</th>
						</tr>
					</table>
				</div>
			</div>

			<table class="simple">
				<thead>
					<tr>
						<th>MCL_ID</th>
						<th>Nama</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="daftar" items="${cmd.daftar}" varStatus="stat">
						<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
							onclick="backToParent('${daftar.MCL_ID}', '${daftar.JENIS}');">
							<td>${daftar.MCL_ID}</td>
							<td>${daftar.MCL_FIRST}</td>
							<td></td>
						</tr>
						<c:set var="farfaan" value="${st.count}" />
					</c:forEach>
				</tbody>
			</table>

		</form>
	</div>

</form>

<script>
	<c:choose>
		<c:when test="${not empty param.searchCompany}">
			setupPanes('container1', 'tab2'); 
		</c:when>
		<c:otherwise>
			setupPanes('container1', 'tab1'); 
		</c:otherwise>
	</c:choose>
</script>
</body>
<%@ include file="/include/page/footer.jsp"%>