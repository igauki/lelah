<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<meta HTTP-EQUIV="Expires" CONTENT="-1">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link href="${path}/include/image/eas.ico" rel="shortcut icon">
		<!--  -->
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/andreas02.css" media="screen">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<!--  -->
		<script type="text/javascript" src="${path }/include/js/default.js"></script>
		<!-- Ajax Related -->
		<script type="text/javascript" src="${path}/include/js/ajax.js"></script>
		<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script>
		<script type="text/javascript" src="${path }/dwr/engine.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>
		<script type="text/javascript">
			hideLoadingMessage();
		</script>
	</head>
<body style="height: 100%;" >
	<form:form id="formpost" name="formpost" commandName="cmd" method="post">
		<table class="entry2">			
		<tr><td>
			<fieldset>
				<legend>
	</legend>
		<table style="font-size: 8pt;" width="100%">
	<tr>
	<td align="center">
	<font style="color: red;">
	<spring:hasBindErrors name="cmd">
											<div id="error">
												<strong>Gagal disimpan harap cek data terlebih dahulu!</strong><br/>
											</div>
										</spring:hasBindErrors>
										</font>
	</td>
	
	</tr>
	</table>
		<table style="font-size: 8pt;" width="100%">
	<tr>
	<td><font style="font: bold;">
	Name Polling </font>
<br/> <br/> 
	</td>
	</tr>
	<tr>
	<td>
	Perusahaan yang baik harus memiliki dan dikenal akan sebuah identitas kuat yang melekat pada dirinya. Hal inilah yang disebut brand. Untuk menentukan identitas atau brand perusahaan, kita harus lebih dulu mengetahui tujuan dan target pasar perusahaan tersebut. Pahami dengan benar kebutuhan dan keinginan nasabah.
<br/> <br/> 
	</td>
	</tr>
	
		<tr>
	<td>
	Brand ini akan tetap tinggal di dalam hati dan pikiran nasabah. Namun, perlu diingat, brand bukan sekedar mudah diingat. Ia harus berfungsi untuk membedakan perusahaan dengan pesaingnya. Dari Brand, juga dapat terlihat Unique Selling Point perusahaan itu. Apa kelebihan fitur-fitur yang ditawarkan, pelayanan dan harga yang diberikan, dan lain-lain dibanding perusahaan lain yang sejenis.
<br/> <br/> 
	</td>
	</tr>
	
	<tr>
	<td>
Bila telah dapat menyampaikan pesan dengan jelas, menegaskan kredibilitasnya, meraih sisi emosi, memotivasi, dan memupuk loyalitas nasabah, maka dapat dikatakan brand tersebut telah sukses diciptakan.
	<br/> <br/>  </td>
	</tr>
	
	<tr>
	<td>
Brand dibuat bukan hanya untuk membuat nasabah lebih memilih perusahaan dibanding pesaingnya, melainkan dapat secara tegas membuat nasabah hanya dapat melihat perusahaan sebagai satu-satunya pihak yang bisa memberikan solusi bagi permasalahan mereka. Melalui brand ini perusahaan dapat memberikan janji-janjinya.	
<br/> <br/> </td>
	</tr>
	
	<tr>
	<td>
Untuk itu kami meminta kesediaan Bapak / Ibu dan rekan-rekan sekalian untuk memberikan waktunya sebentar dan berpartisipasi dalam Polling Pendapat mengenai Nama yang akan dijadikan Anchor Brand untuk dimiliki oleh PT. Asuransi Jiwa Sinarmas MSIG. Sebagai informasi, dengan Anchor Brand ini nama perusahaan masih tetap sama, tetapi Anchor Brand akan terlihat pada nama produk-produk yang kita miliki.	
<br/> <br/> </td>
	</tr>
	
	<tr>
	<td>
Kami mengharapkan Polling ini dapat diselesaikankan paling lambat tanggal 27 Februari 2012 jam 09.00 pagi. 	
<br/> <br/> </td>
	</tr>
	
	<tr>
	<td>
Terima kasih atas perhatian serta kesediaan Bapak / Ibu dan rekan-rekan sekalian untuk mengisi Polling ini.
<br/> <br/> <br/> <br/> </td>
	</tr>
	
	<tr>
	<td>
</td>
	</tr>
	
	<tr>
	<td>
	Salam Sukses,
	<br/> <br/> <br/> 
	
</td>
	</tr>
	
	<tr>
	<td>
	Rentania
</td>
	</tr>
	<tr>
	<td>
	Marketing Support
	<br/><br/>
</td>
	</tr>
	</table>

	<table border="1" cellpadding="1" cellspacing="1" style="font-size: 8pt;" width="100%">
	
	<tr >
							<th width="100%">
							Isilah semua pertanyaan dengan 5 jawaban yang paling tepat sesuai preferensi anda.<br/>
Harap pastikan tidak ada jawaban yang sama / duplikat pada baris yang sama.
							</th>
	</tr>
	</table>
			<table border="1" cellpadding="1" cellspacing="1" style="font-size: 8pt;" width="100%">
						<tr >
							<th width="15">
							
							</th>
							<th width="400">
								
							</th>
							<th width="80">
							</th>
							<th width="80">
							</th>
							<th width="80">
							</th>
							<th width="80">
							</th>
							<th width="80">
							</th>
						</tr>
						
						<tr>
							<td bgcolor="#ABD0BC">
								<font size="2">1.)</font>
							</td>
							<td colspan="6" bgcolor="#ABD0BC">
								<font size="2"> CORPORATE RELEVANCE</font>
							</td>
						</tr>
						<tr>
							<td style="">
								1.
							</td>
							<td>
							Manakah yang menurut anda mencerminkan 'Sinarmas-MSIG' ?
							<br/>
							<form:errors path="mkb_cr_msig_1" cssStyle="color: red;"/>
							</td>
							<td align="center">
								<form:select path="mkb_cr_msig_1" >
												<form:option value="0" label="" />
												<form:options items="${cmd.corporateList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_cr_msig_2" >
												<form:option value="0" label="" />
												<form:options items="${cmd.corporateList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_cr_msig_3" >
												<form:option value="0" label="" />
												<form:options items="${cmd.corporateList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_cr_msig_4" >
												<form:option value="0" label="" />
												<form:options items="${cmd.corporateList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_cr_msig_5" >
												<form:option value="0" label="" />
												<form:options items="${cmd.corporateList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
						</tr>
						<tr>
							<td>
								2.
							</td>
							<td>
							Manakah yang menurut anda menggambarkan 'kekuatan aset' ?
							<br/>
							<form:errors path="mkb_cr_aset_1" cssStyle="color: red;"/>
							</td>
							<td align="center">
								<form:select path="mkb_cr_aset_1" >
												<form:option value="0" label="" />
												<form:options items="${cmd.corporateList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_cr_aset_2" >
												<form:option value="0" label="" />
												<form:options items="${cmd.corporateList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_cr_aset_3" >
												<form:option value="0" label="" />
												<form:options items="${cmd.corporateList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
							<form:select path="mkb_cr_aset_4" >
												<form:option value="0" label="" />
												<form:options items="${cmd.corporateList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
							<form:select path="mkb_cr_aset_5" >
												<form:option value="0" label="" />
												<form:options items="${cmd.corporateList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
						</tr>
						<tr>
							<td>
								3.
							</td>
							<td>
							Manakah yang menurut anda menggambarkan 'fokus terhadap kebutuhan pelanggan' ?
							<br/>
							<form:errors path="mkb_cr_pelanggan_1" cssStyle="color: red;"/>
							</td>
							<td align="center">
								<form:select path="mkb_cr_pelanggan_1" >
												<form:option value="0" label="" />
												<form:options items="${cmd.corporateList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_cr_pelanggan_2" >
												<form:option value="0" label="" />
												<form:options items="${cmd.corporateList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_cr_pelanggan_3" >
												<form:option value="0" label="" />
												<form:options items="${cmd.corporateList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_cr_pelanggan_4" >
												<form:option value="0" label="" />
												<form:options items="${cmd.corporateList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_cr_pelanggan_5" >
												<form:option value="0" label="" />
												<form:options items="${cmd.corporateList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
						</tr>
						<tr>
							<td>
								4.
							</td>
							<td>
							Manakah yang menurut anda menggambarkan 'pelayanan yang prima' ?
							<br/>
							<form:errors path="mkb_cr_prima_1" cssStyle="color: red;"/>
							</td>
							<td align="center">
								<form:select path="mkb_cr_prima_1" >
												<form:option value="0" label="" />
												<form:options items="${cmd.corporateList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_cr_prima_2" >
												<form:option value="0" label="" />
												<form:options items="${cmd.corporateList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_cr_prima_3" >
												<form:option value="0" label="" />
												<form:options items="${cmd.corporateList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_cr_prima_4" >
												<form:option value="0" label="" />
												<form:options items="${cmd.corporateList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_cr_prima_5" >
												<form:option value="0" label="" />
												<form:options items="${cmd.corporateList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
						</tr>
						<tr>
							<td bgcolor="#ABD0BC">
								<font size="2">2.)</font>
							</td>
							<td colspan="6" bgcolor="#ABD0BC">
								<font size="2">PRODUK RELEVANCE</font>
							</td>
						</tr>
						<tr>
							<td>
								1.
							</td>
							<td>
							Manakah yang menurut anda mencerminkan 'yang terbaik' ?
							<br/>
							<form:errors path="mkb_pr_terbaik_1" cssStyle="color: red;"/>
							</td>
							<td align="center">
								<form:select path="mkb_pr_terbaik_1" >
												<form:option value="0" label="" />
												<form:options items="${cmd.productList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_pr_terbaik_2" >
												<form:option value="0" label="" />
												<form:options items="${cmd.productList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_pr_terbaik_3" >
												<form:option value="0" label="" />
												<form:options items="${cmd.productList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_pr_terbaik_4" >
												<form:option value="0" label="" />
												<form:options items="${cmd.productList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_pr_terbaik_5" >
												<form:option value="0" label="" />
												<form:options items="${cmd.productList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
						</tr>
						<tr>
							<td>
								2.
							</td>
							<td>
							Manakah yang menurut anda menggambarkan 'fleksibilitas' ?
							<br/>
							<form:errors path="mkb_pr_fleksibilias_1" cssStyle="color: red;"/>
							</td>
							<td align="center">
								<form:select path="mkb_pr_fleksibilias_1" >
												<form:option value="0" label="" />
												<form:options items="${cmd.productList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_pr_fleksibilias_2" >
												<form:option value="0" label="" />
												<form:options items="${cmd.productList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_pr_fleksibilias_3" >
												<form:option value="0" label="" />
												<form:options items="${cmd.productList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_pr_fleksibilias_4" >
												<form:option value="0" label="" />
												<form:options items="${cmd.productList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_pr_fleksibilias_5" >
												<form:option value="0" label="" />
												<form:options items="${cmd.productList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
						</tr>
						<tr>
							<td>
								3.
							</td>
							<td>
							Manakah yang menurut anda menggambarkan 'asuransi' ?
							<br/>
							<form:errors path="mkb_pr_asuransi_1" cssStyle="color: red;"/>
							</td>
							<td align="center">
								<form:select path="mkb_pr_asuransi_1" >
												<form:option value="0" label="" />
												<form:options items="${cmd.productList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_pr_asuransi_2" >
												<form:option value="0" label="" />
												<form:options items="${cmd.productList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_pr_asuransi_3" >
												<form:option value="0" label="" />
												<form:options items="${cmd.productList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_pr_asuransi_4" >
												<form:option value="0" label="" />
												<form:options items="${cmd.productList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_pr_asuransi_5" >
												<form:option value="0" label="" />
												<form:options items="${cmd.productList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
						</tr>
						<tr>
							<td>
								4.
							</td>
							<td>
							Manakah yang menurut anda menggambarkan 'keragaman produk' ?
							<br/>
							<form:errors path="mkb_pr_produk_1" cssStyle="color: red;"/>
							</td>
							<td align="center">
								<form:select path="mkb_pr_produk_1" >
												<form:option value="0" label="" />
												<form:options items="${cmd.productList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_pr_produk_2" >
												<form:option value="0" label="" />
												<form:options items="${cmd.productList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_pr_produk_3" >
												<form:option value="0" label="" />
												<form:options items="${cmd.productList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_pr_produk_4" >
												<form:option value="0" label="" />
												<form:options items="${cmd.productList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_pr_produk_5" >
												<form:option value="0" label="" />
												<form:options items="${cmd.productList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
						</tr>
						<tr>
							<td bgcolor="#ABD0BC">
								<font size="2">3.)</font>
							</td>
							<td colspan="6" bgcolor="#ABD0BC">
								<font size="2">IMAGE</font>
							</td>
						</tr>
						<tr>
							<td>
								1.
							</td>
							<td>
							Manakah yang menurut anda mencerminkan 'kesan internasional' ?
							<br/>
							<form:errors path="mkb_im_internasional_1" cssStyle="color: red;"/>
							</td>
							<td align="center">
								<form:select path="mkb_im_internasional_1" >
												<form:option value="0" label="" />
												<form:options items="${cmd.imageList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_im_internasional_2" >
												<form:option value="0" label="" />
												<form:options items="${cmd.imageList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_im_internasional_3" >
												<form:option value="0" label="" />
												<form:options items="${cmd.imageList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_im_internasional_4" >
												<form:option value="0" label="" />
												<form:options items="${cmd.imageList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_im_internasional_5" >
												<form:option value="0" label="" />
												<form:options items="${cmd.imageList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
						</tr>
						<tr>
							<td>
								2.
							</td>
							<td>
							Manakah yang menurut anda menggambarkan 'sikap yang positif' ?
							<br/>
							<form:errors path="mkb_im_positif_1" cssStyle="color: red;"/>
							</td>
							<td align="center">
								<form:select path="mkb_im_positif_1" >
												<form:option value="0" label="" />
												<form:options items="${cmd.imageList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_im_positif_2" >
												<form:option value="0" label="" />
												<form:options items="${cmd.imageList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_im_positif_3" >
												<form:option value="0" label="" />
												<form:options items="${cmd.imageList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_im_positif_4" >
												<form:option value="0" label="" />
												<form:options items="${cmd.imageList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_im_positif_5" >
												<form:option value="0" label="" />
												<form:options items="${cmd.imageList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
						</tr>
						<tr>
							<td>
								3.
							</td>
							<td>
							 Manakah yang menurut anda menggambarkan kesan yang 'moderen' ?
							 <br/>
							 <form:errors path="mkb_im_moderen_1" cssStyle="color: red;"/>
							</td>
							<td align="center">
								<form:select path="mkb_im_moderen_1" >
												<form:option value="0" label="" />
												<form:options items="${cmd.imageList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_im_moderen_2" >
												<form:option value="0" label="" />
												<form:options items="${cmd.imageList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_im_moderen_3" >
												<form:option value="0" label="" />
												<form:options items="${cmd.imageList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_im_moderen_4" >
												<form:option value="0" label="" />
												<form:options items="${cmd.imageList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_im_moderen_5" >
												<form:option value="0" label="" />
												<form:options items="${cmd.imageList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
						</tr>
						<tr>
							<td>
								4.
							</td>
							<td>
							Manakah yang menurut anda menggambarkan 'komitmen' ?
							<br/>
							 <form:errors path="mkb_im_komitmen_1" cssStyle="color: red;"/>
							</td>
							<td align="center">
								<form:select path="mkb_im_komitmen_1" >
												<form:option value="0" label="" />
												<form:options items="${cmd.imageList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_im_komitmen_2" >
												<form:option value="0" label="" />
												<form:options items="${cmd.imageList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_im_komitmen_3" >
												<form:option value="0" label="" />
												<form:options items="${cmd.imageList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_im_komitmen_4" >
												<form:option value="0" label="" />
												<form:options items="${cmd.imageList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_im_komitmen_5" >
												<form:option value="0" label="" />
												<form:options items="${cmd.imageList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
						</tr>
						<tr>
							<td bgcolor="#ABD0BC">
								<font size="2">4.)</font>
							</td>
							<td colspan="6" bgcolor="#ABD0BC">
								<font size="2">EMOTIONAL</font>
							</td>
						</tr>
						<tr>
							<td>
								1.
							</td>
							<td>
							 Manakah yang menurut anda mencerminkan 'mitra yang waspada' ?
							 <br/>
							 <form:errors path="mkb_em_waspada_1" cssStyle="color: red;"/>
							</td>
							<td align="center">
								<form:select path="mkb_em_waspada_1" >
												<form:option value="0" label="" />
												<form:options items="${cmd.emotionalList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_em_waspada_2" >
												<form:option value="0" label="" />
												<form:options items="${cmd.emotionalList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_em_waspada_3" >
												<form:option value="0" label="" />
												<form:options items="${cmd.emotionalList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_em_waspada_4" >
												<form:option value="0" label="" />
												<form:options items="${cmd.emotionalList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_em_waspada_5" >
												<form:option value="0" label="" />
												<form:options items="${cmd.emotionalList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
						</tr>
						<tr>
							<td>
								2.
							</td>
							<td>
							Manakah yang menurut anda menggambarkan 'kenyamanan' ?
							<br/>
							 <form:errors path="mkb_em_kenyamanan_1" cssStyle="color: red;"/>
							</td>
							<td align="center">
								<form:select path="mkb_em_kenyamanan_1" >
												<form:option value="0" label="" />
												<form:options items="${cmd.emotionalList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_em_kenyamanan_2" >
												<form:option value="0" label="" />
												<form:options items="${cmd.emotionalList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_em_kenyamanan_3" >
												<form:option value="0" label="" />
												<form:options items="${cmd.emotionalList}" itemValue="key" itemLabel="value" />
								</form:select>
							<td align="center">
								<form:select path="mkb_em_kenyamanan_4" >
												<form:option value="0" label="" />
												<form:options items="${cmd.emotionalList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_em_kenyamanan_5" >
												<form:option value="0" label="" />
												<form:options items="${cmd.emotionalList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
						</tr>
						<tr>
							<td>
								3.
							</td>
							<td>
							 Manakah yang menurut anda menggambarkan 'pengertian' ?
							 <br/>
							 <form:errors path="mkb_em_pengertian_1" cssStyle="color: red;"/>
							</td>
							<td align="center">
								<form:select path="mkb_em_pengertian_1" >
												<form:option value="0" label="" />
												<form:options items="${cmd.emotionalList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_em_pengertian_2" >
												<form:option value="0" label="" />
												<form:options items="${cmd.emotionalList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_em_pengertian_3" >
												<form:option value="0" label="" />
												<form:options items="${cmd.emotionalList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_em_pengertian_4" >
												<form:option value="0" label="" />
												<form:options items="${cmd.emotionalList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_em_pengertian_5" >
												<form:option value="0" label="" />
												<form:options items="${cmd.emotionalList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
						</tr>
						<tr>
							<td>
								4.
							</td>
							<td>
							Manakah yang menurut anda menggambarkan 'ketulusan' ?
							<br/>
							 <form:errors path="mkb_em_ketulusan_1" cssStyle="color: red;"/>
							</td>
							<td align="center">
								<form:select path="mkb_em_ketulusan_1" >
												<form:option value="0" label="" />
												<form:options items="${cmd.emotionalList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_em_ketulusan_2" >
												<form:option value="0" label="" />
												<form:options items="${cmd.emotionalList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_em_ketulusan_3" >
												<form:option value="0" label="" />
												<form:options items="${cmd.emotionalList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_em_ketulusan_4" >
												<form:option value="0" label="" />
												<form:options items="${cmd.emotionalList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
							<td align="center">
								<form:select path="mkb_em_ketulusan_5" >
												<form:option value="0" label="" />
												<form:options items="${cmd.emotionalList}" itemValue="key" itemLabel="value" />
								</form:select>
							</td>
						</tr>
					</table>
				
	</fieldset>
		<table class="entry2">
			
			
			<tr>
				<td>
					<input type="hidden" name="enter" value="enter">
					<input type="submit" value="SAVE" id="save"/>
				</td>
			</tr>
			
		</table>
		</td></tr></table>
		
			
	</form:form>		

	</body>
	<script>
		<c:if test="${not empty param.pesan}">
			alert('${param.pesan}');
		</c:if>
	</script>
</html>