<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>

<script>

function pesan(){
	var pesan=document.formpost.pesan.value;
	if(pesan != ""){
		alert(pesan);
	}
}

function prosesConfirm(){
	var x=window.confirm('Pastikan data yang Anda isi telah lengkap dan benar !\n Transfer ke Closing ?');
	if(x){
		//alert('masuk');
		document.getElementById('formpost').submit();
	}
}

<c:if test="${not empty pesan} ">
	<script type="text/javascript">
		alert('${pesan}');
	</script>
</c:if>

</script>

<body style="height: 100%;" onload="setupPanes('container1', 'tab${cmd.showTab}'); pesan();">
	<c:choose>
		<c:when test="${cmd.flagAdd eq 2}">
			<form:form id="formpost" name="formpost" commandName="cmd">
				<div class="tab-container" id="container1">
					<ul class="tabs">
						<li>
							<a href="#" onClick="return showPane('pane1', this)" id="tab1">MATRIX ADVANCED</a>
							<a href="#" onClick="return showPane('pane2', this)" id="tab2">RECOMMEND</a>
						</li>
					</ul>
					<div class="tab-panes">
						<div id="pane1" class="panes">
							<c:set var="prod" value="" />
							<table class="entry2">
								<thead>
									<tr>
										<th colspan="2" width="40%">
											KEBUTUHAN NASABAH
										</th>
										<th colspan="3" width="15%">
											PENDIDIKAN
										</th>
										<th colspan="3" width="15%">
											PENSIUN
										</th>
										<th colspan="3" width="15%">
											PERLINDUNGAN
										</th>
										<th colspan="3" width="15%">
											TABUNGAN
										</th>
									</tr>
									<tr>
										<th colspan="2">
											ATR(ATTITUDE to RISK)
										</th>
										<th width="5%" style="text-align: center">
											C
										</th>
										<th width="5%" style="text-align: center">
											B
										</th>
										<th width="5%" style="text-align: center">
											A
										</th>
										<th width="5%" style="text-align: center">
											C
										</th>
										<th width="5%" style="text-align: center">
											B
										</th>
										<th width="5%" style="text-align: center">
											A
										</th>
										<th width="5%" style="text-align: center">
											C
										</th>
										<th width="5%" style="text-align: center">
											B
										</th>
										<th width="5%" style="text-align: center">
											A
										</th>
										<th width="5%" style="text-align: center">
											C
										</th>
										<th width="5%" style="text-align: center">
											B
										</th>
										<th width="5%" style="text-align: center">
											A
										</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${cmd.matrix.listMatrix}" var="b" varStatus="s">
										<tr>
											<c:if test="${b.lsbs_name ne prod}">
												<c:set var="prod" value="${b.lsbs_name}" />
													<th style="text-align: left; font-weight: bold;">
														${prod}	
													</th>
											</c:if>
											<c:if test="${b.msma_fund eq 0}">
												<th></th>
											</c:if>
											<c:if test="${b.msma_fund eq 1}">
												<th style="text-align: left; font-weight: bold;">FIXED</th>
											</c:if>
											<c:if test="${b.msma_fund eq 2}">
												<th></th>
												<th style="text-align: left; font-weight: bold;">DYNAMIC</th>
											</c:if>
											<c:if test="${b.msma_fund eq 3}">
												<th></th>
												<th style="text-align: left; font-weight: bold;">AGGRESIVE</th>
											</c:if>
											<td style="text-align: center;"><form:checkbox disabled="disabled" cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_studi_c" value="1" id="msma_studi_c"/>
											</td>
											<td style="text-align: center;"><form:checkbox disabled="disabled" cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_studi_b" value="1" id="msma_studi_b"/>
											</td>
											<td style="text-align: center;"><form:checkbox disabled="disabled" cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_studi_a" value="1" id="msma_studi_a"/>
											</td>
											<td style="text-align: center;"><form:checkbox disabled="disabled" cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_pensiun_c" value="1" id="msma_pensiun_c"/>
											</td>
											<td style="text-align: center;"><form:checkbox disabled="disabled" cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_pensiun_b" value="1" id="msma_pensiun_b"/>
											</td>
											<td style="text-align: center;"><form:checkbox disabled="disabled" cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_pensiun_a" value="1" id="msma_pensiun_a"/>
											</td>
											<td style="text-align: center;"><form:checkbox disabled="disabled" cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_proteksi_c" value="1" id="msma_proteksi_c"/>
											</td>
											<td style="text-align: center;"><form:checkbox disabled="disabled" cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_proteksi_b" value="1" id="msma_proteksi_b"/>
											</td>
											<td style="text-align: center;"><form:checkbox disabled="disabled" cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_proteksi_a" value="1" id="msma_proteksi_a"/>
											</td>
											<td style="text-align: center;"><form:checkbox disabled="disabled" cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_saving_c" value="1" id="msma_saving_c"/>
											</td>
											<td style="text-align: center;"><form:checkbox disabled="disabled" cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_saving_b" value="1" id="msma_saving_b"/>
											</td>
											<td style="text-align: center;"><form:checkbox disabled="disabled" cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_saving_a" value="1" id="msma_saving_a"/>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<table class="entry2" align="left">
								<tr>
									<td>
										<span class="info"  style="text-align: left; font-size: 8pt;">
											C = Cautions (Berhati-hati)
											<br />
											B = Balance (Seimbang)
											<br />
											A = Advantage (Berani Mengambil Resiko)
										</span>
									</td>
								</tr>
								<tr>
									<th>
										<input type="hidden" class="button" name="save" id="save" value="save" />
										<input type="hidden" name="trans" id="trans" value="trans" onclick="prosesConfirm();">
									</th>
								</tr>
							</table>
							
						</div>
						<div id="pane2" class="panes">
							<fieldset>
								<legend>Kesimpulan</legend>
								<table class="entry2">
									<tr style="text-transform: none; font-weight: bold; font-size: 8pt;">
										<td>
											Melanjutkan awal pertemuan kita pada tanggal
										</td>
									</tr>
									<tr style="text-transform: none; font-weight: bold; font-size: 8pt;">
										<td>
											Saya dapat mengkonfirmasikan bahwa Aspirasi Anda adalah sebagai berikut :
										</td>
									</tr>
									<c:forEach items="${cmd.aspirasi.listAspirasi}" var="a" varStatus="st">
										<tr style="text-transform: none; font-weight: bold; font-size: 8pt;">
											<td>
												${st.count}.
												<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="aspirasi.listAspirasi[${st.index}].msas_aspirasi" id="msas_aspirasi" maxlength="100" size="100"/>
											</td>
										</tr>
									</c:forEach>
								</table>
							</fieldset>
							<br />
							<fieldset>
								<legend>Saran</legend>
								<table class="entry2">
									<tr style="text-transform: none; font-weight: bold; font-size: 8pt;">
										<td>
											Setelah meneliti kebutuhan Anda, melihat dari penggambaran Anda menghadapi resiko 'berhati-hati'/'seimbang'/'berani mengambil resiko'
										</td>
									</tr>
									<tr style="text-transform: none; font-weight: bold; font-size: 8pt;">
										<td>
											Saya konfirmasi bahwa saya menyarankan perencanaan sebagai berikut :
										</td>
									</tr>
									<c:forEach items="${cmd.rekomendasi.listRekomendasi}" var="a" varStatus="st">
										<tr style="text-transform: none; font-weight: bold; font-size: 8pt;">
											<td>
												${st.count}.
												<form:select disabled="disabled" path="rekomendasi.listRekomendasi[${st.index}].lsbs_id" id="lsbs_id">
													<form:option label="" value=""/>
									    			<form:options items="${lstRekomendasi}" itemLabel="value" itemValue="key"/>
												</form:select>
											</td>
										</tr>
										<tr style="text-transform: none; font-weight: bold; font-size: 8pt;">
											<td>
												&nbsp;&nbsp;&nbsp;&nbsp;Karena &nbsp;
												<form:input disabled="disabled" cssStyle="border-color:#6893B0;" path="rekomendasi.listRekomendasi[${st.index}].msrek_alasan" id="msrek_alasan" size="100" maxlength="100"/>
											</td>
										</tr>
									</c:forEach>
								</table>
							</fieldset>
							<table class="entry2">
								<tr>
									<th>
										<input type="hidden" class="button" name="save" id="save" value="save" />
										<input type="hidden" name="trans" id="trans" value="trans" onclick="prosesConfirm();">
									</th>
								</tr>
							</table>
						</div>
					</div>
					<tr>
						<td colspan="4">
							<c:if test="${submitSuccess eq true}">
								<c:choose>
									<c:when test="${cmd.flagAdd eq 1}">
										<script>
											alert("Berhasil Transfer");
											addOptionToSelect(parent.document, parent.document.formpost.nomor, '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}', '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}');
											window.parent.tes();
										</script>
									</c:when>
									<c:otherwise>
										<script>
											alert("Berhasil Simpan");
											addOptionToSelect(parent.document, parent.document.formpost.nomor, '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}', '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}');
											window.location='${path }/common/input_recommend.htm?nomor=${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}';
										</script>
									</c:otherwise>
								</c:choose>
					        	<div id="success">
						        	 Berhasil
						    	</div>
					        </c:if>	
				  			<spring:bind path="cmd.*">
								<c:if test="${not empty status.errorMessages}">
									<div id="error">
										 Informasi:<br>
											<c:forEach var="error" items="${status.errorMessages}">
														 - <c:out value="${error}" escapeXml="false" />
												<br/>
											</c:forEach>
									</div>
								</c:if>									
							</spring:bind>
							<input type="hidden" value="${pesan}" name="pesan" />
						 </td>
					</tr>
				</div>
			</form:form>
		</c:when>
		<c:otherwise>
			<form:form id="formpost" name="formpost" commandName="cmd">
				<div class="tab-container" id="container1">
					<ul class="tabs">
						<li>
							<a href="#" onClick="return showPane('pane1', this)" id="tab1">MATRIX ADVANCED</a>
							<a href="#" onClick="return showPane('pane2', this)" id="tab2">RECOMMEND</a>
						</li>
					</ul>
					<div class="tab-panes">
						<div id="pane1" class="panes">
							<c:set var="prod" value="" />
							<table class="entry2">
								<thead>
									<tr>
										<th colspan="2" width="40%">
											KEBUTUHAN NASABAH
										</th>
										<th colspan="3" width="15%">
											PENDIDIKAN
										</th>
										<th colspan="3" width="15%">
											PENSIUN
										</th>
										<th colspan="3" width="15%">
											PERLINDUNGAN
										</th>
										<th colspan="3" width="15%">
											TABUNGAN
										</th>
									</tr>
									<tr>
										<th colspan="2">
											ATR(ATTITUDE to RISK)
										</th>
										<th width="5%" style="text-align: center">
											C
										</th>
										<th width="5%" style="text-align: center">
											B
										</th>
										<th width="5%" style="text-align: center">
											A
										</th>
										<th width="5%" style="text-align: center">
											C
										</th>
										<th width="5%" style="text-align: center">
											B
										</th>
										<th width="5%" style="text-align: center">
											A
										</th>
										<th width="5%" style="text-align: center">
											C
										</th>
										<th width="5%" style="text-align: center">
											B
										</th>
										<th width="5%" style="text-align: center">
											A
										</th>
										<th width="5%" style="text-align: center">
											C
										</th>
										<th width="5%" style="text-align: center">
											B
										</th>
										<th width="5%" style="text-align: center">
											A
										</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${cmd.matrix.listMatrix}" var="b" varStatus="s">
										<tr>
											<c:if test="${b.lsbs_name ne prod}">
												<c:set var="prod" value="${b.lsbs_name}" />
													<th style="text-align: left; font-weight: bold;">
														${prod}	
													</th>
											</c:if>
											<c:if test="${b.msma_fund eq 0}">
												<th></th>
											</c:if>
											<c:if test="${b.msma_fund eq 1}">
												<th style="text-align: left; font-weight: bold;">FIXED</th>
											</c:if>
											<c:if test="${b.msma_fund eq 2}">
												<th></th>
												<th style="text-align: left; font-weight: bold;">DYNAMIC</th>
											</c:if>
											<c:if test="${b.msma_fund eq 3}">
												<th></th>
												<th style="text-align: left; font-weight: bold;">AGGRESIVE</th>
											</c:if>
											<td style="text-align: center;"><form:checkbox cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_studi_c" value="1" id="msma_studi_c"/>
											</td>
											<td style="text-align: center;"><form:checkbox cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_studi_b" value="1" id="msma_studi_b"/>
											</td>
											<td style="text-align: center;"><form:checkbox cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_studi_a" value="1" id="msma_studi_a"/>
											</td>
											<td style="text-align: center;"><form:checkbox cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_pensiun_c" value="1" id="msma_pensiun_c"/>
											</td>
											<td style="text-align: center;"><form:checkbox cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_pensiun_b" value="1" id="msma_pensiun_b"/>
											</td>
											<td style="text-align: center;"><form:checkbox cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_pensiun_a" value="1" id="msma_pensiun_a"/>
											</td>
											<td style="text-align: center;"><form:checkbox cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_proteksi_c" value="1" id="msma_proteksi_c"/>
											</td>
											<td style="text-align: center;"><form:checkbox cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_proteksi_b" value="1" id="msma_proteksi_b"/>
											</td>
											<td style="text-align: center;"><form:checkbox cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_proteksi_a" value="1" id="msma_proteksi_a"/>
											</td>
											<td style="text-align: center;"><form:checkbox cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_saving_c" value="1" id="msma_saving_c"/>
											</td>
											<td style="text-align: center;"><form:checkbox cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_saving_b" value="1" id="msma_saving_b"/>
											</td>
											<td style="text-align: center;"><form:checkbox cssClass="noBorder" path="matrix.listMatrix[${s.index}].msma_saving_a" value="1" id="msma_saving_a"/>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<table class="entry2" align="left">
								<tr>
									<td>
										<span class="info"  style="text-align: left; font-size: 8pt;">
											C = Cautions (Berhati-hati)
											<br />
											B = Balance (Seimbang)
											<br />
											A = Advantage (Berani Mengambil Resiko)
										</span>
									</td>
								</tr>
								<tr>
									<th>
										<input type="submit" class="button" name="save" id="save" value="save" />
										<input type="button" name="trans" id="trans" value="trans" onclick="prosesConfirm();">
									</th>
								</tr>
							</table>
							
						</div>
						<div id="pane2" class="panes">
							<fieldset>
								<legend>Kesimpulan</legend>
								<table class="entry2">
									<tr style="text-transform: none; font-weight: bold; font-size: 8pt;">
										<td>
											Melanjutkan awal pertemuan kita pada tanggal
										</td>
									</tr>
									<tr style="text-transform: none; font-weight: bold; font-size: 8pt;">
										<td>
											Saya dapat mengkonfirmasikan bahwa Aspirasi Anda adalah sebagai berikut :
										</td>
									</tr>
									<c:forEach items="${cmd.aspirasi.listAspirasi}" var="a" varStatus="st">
										<tr style="text-transform: none; font-weight: bold; font-size: 8pt;">
											<td>
												${st.count}.
												<form:input cssStyle="border-color:#6893B0;" path="aspirasi.listAspirasi[${st.index}].msas_aspirasi" id="msas_aspirasi" maxlength="100" size="100"/>
											</td>
										</tr>
									</c:forEach>
								</table>
							</fieldset>
							<br />
							<fieldset>
								<legend>Saran</legend>
								<table class="entry2">
									<tr style="text-transform: none; font-weight: bold; font-size: 8pt;">
										<td>
											Setelah meneliti kebutuhan Anda, melihat dari penggambaran Anda menghadapi resiko 'berhati-hati'/'seimbang'/'berani mengambil resiko'
										</td>
									</tr>
									<tr style="text-transform: none; font-weight: bold; font-size: 8pt;">
										<td>
											Saya konfirmasi bahwa saya menyarankan perencanaan sebagai berikut :
										</td>
									</tr>
									<c:forEach items="${cmd.rekomendasi.listRekomendasi}" var="a" varStatus="st">
										<tr style="text-transform: none; font-weight: bold; font-size: 8pt;">
											<td>
												${st.count}.
												<form:select path="rekomendasi.listRekomendasi[${st.index}].lsbs_id" id="lsbs_id">
													<form:option label="" value=""/>
									    			<form:options items="${lstRekomendasi}" itemLabel="value" itemValue="key"/>
												</form:select>
											</td>
										</tr>
										<tr style="text-transform: none; font-weight: bold; font-size: 8pt;">
											<td>
												&nbsp;&nbsp;&nbsp;&nbsp;Karena &nbsp;
												<form:input cssStyle="border-color:#6893B0;" path="rekomendasi.listRekomendasi[${st.index}].msrek_alasan" id="msrek_alasan" size="100" maxlength="100"/>
											</td>
										</tr>
									</c:forEach>
								</table>
							</fieldset>
							<table class="entry2">
								<tr>
									<th>
										<input type="submit" class="button" name="save" id="save" value="save" />
										<input type="button" name="trans" id="trans" value="trans" onclick="prosesConfirm();">
									</th>
								</tr>
							</table>
						</div>
					</div>
					<tr>
						<td colspan="4">
							<c:if test="${submitSuccess eq true}">
								<c:choose>
									<c:when test="${cmd.flagAdd eq 1}">
										<script>
											alert("Berhasil Transfer");
											addOptionToSelect(parent.document, parent.document.formpost.nomor, '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}', '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}');
											window.parent.tes();
										</script>
									</c:when>
									<c:otherwise>
										<script>
											alert("Berhasil Simpan");
											addOptionToSelect(parent.document, parent.document.formpost.nomor, '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}', '${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}');
											window.location='${path }/common/input_recommend.htm?nomor=${cmd.nasabah.mns_no_ref}~${cmd.nasabah.mns_kd_nasabah}';
										</script>
									</c:otherwise>
								</c:choose>
					        	<div id="success">
						        	 Berhasil
						    	</div>
					        </c:if>	
				  			<spring:bind path="cmd.*">
								<c:if test="${not empty status.errorMessages}">
									<div id="error">
										 Informasi:<br>
											<c:forEach var="error" items="${status.errorMessages}">
														 - <c:out value="${error}" escapeXml="false" />
												<br/>
											</c:forEach>
									</div>
								</c:if>									
							</spring:bind>
							<input type="hidden" value="${pesan}" name="pesan" />
						 </td>
					</tr>
				</div>
			</form:form>
		</c:otherwise>
	</c:choose>
</body>



<%@ include file="/include/page/footer.jsp"%>