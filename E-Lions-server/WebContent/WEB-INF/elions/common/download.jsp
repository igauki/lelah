<%@ include file="/include/page/header.jsp"%>
<script>
</script>
<body style="height: 100%;" onload="setupPanes('container1', 'tab1'); ">
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Download</a>
			</li>
		</ul>
		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form method="post" name="formpost">
					<table class="entry2">
						<tr>
							<th>
							</th>
							<td>
								<table class="displaytag" style="width: auto;">
									<caption>Silahkan Pilih File:</caption>
									<tr>
										<th>File Name</th>
										<th>Description</th>
										<th>Last Modified</th>
									</tr>
									<c:forEach var="dok" items="${daftarFile}">
										<tr>
											<td style="text-align: left;"><a href="${path}/download.htm?download=${dok.key}">${dok.key}</a></td>
											<td style="text-align: left;">${dok.desc}</td>
											<td style="text-align: left;">${dok.value}</td>
										</tr>
									</c:forEach>
								</table>
							</td>
						</tr>

					</table>
				</form>			
			</div>
		</div>
		
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>