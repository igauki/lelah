<%@ include file="/include/page/header.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<body onload="setupPanes('container1', 'tab1');">
	
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Upload Cafetari MNC</a>
				<a href="#" onClick="return showPane('pane2', this)" id="tab2">Petunjuk Upload</a>
			</li>
		</ul>

		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form name="formpost" method="post" enctype="multipart/form-data">
					<table class="entry2">
						<tr>
							<th>Contoh Template XLS</th>
							<td>
							<input type="submit" name="dl_cafmnc" id="dl_cafmnc" value="Download Template Xls Cafetari MNC" title="contoh template excel untuk uploadan Cafetari MNC" />
							
							</td>						
						</tr>
						<tr>
							<th>Upload File</th>
							<td>
								<input type="file" name="file1" id="file1" size="100"/>&nbsp;
								<input type="hidden" name="file_fp" id="file_fp"/>
							</td>							
						</tr>
						<tr>
							<th></th>
							
							<td>
							<div id="success" style="text-transform: none;">
							File harus bertipe EXCEL (.xls) dan terdiri dari 10 kolom : NO, NAMA_PERUSAHAAN, NAMA_PEMEGANG_POLIS, NIK, WARGA_NEGARA, TANGGAL_LAHIR, KOTA_LAHIR, JNS_KELAMIN, STATUS, AGAMA 
							</div>
							</td>
						</tr>
						<tr>
							<th></th>
							<td>
								<input type="submit" name="upload"  value="Upload" onclick="document.getElementById('file_fp').value=document.getElementById('file1').value" />
								
							</td>							
						</tr>
						<tr>
							<th><span class="info">Status Upload :</span></th>
							<td>
							<c:forEach items="${successMessage}" var="d" varStatus="st">
							<c:choose>
							<c:when test="${d.sts eq 'FAILED'}">
							<font color="red">${st.index+1}. ${d.msg}</font>
							</c:when>
							<c:otherwise>
							<font color="blue">${st.index+1}. ${d.msg}</font>
							<br/>
								<table class="displaytag" style="width: auto;">
									<tr>
									<th>File</th>
									<th>Action</th>
									</tr>
								<c:forEach items="${successMessage3}" var="i" varStatus="j">
										<tr>
									
											<td style="text-align: left;">												
												<font color="red">${i.pdfStamper}</font>
												<input type="hidden" name="${i.pdfStamper}" id="${i.pdfStamper}" size="10" value="${i.pdfStamper}" />				
											</td>
											<td>
											<input type="submit" name="dl_pdf" value="Download" onclick="document.getElementById('file_fp').value=document.getElementById('${i.pdfStamper}').value"/>
								
											</td>		
										</tr>
								</c:forEach>							
								</table>
											
											
							</c:otherwise>
							</c:choose>
							
							<br/>
							</c:forEach>
							</td>
						</tr>
						<tr>
							<th><span class="info">Validasi Data Upload :</span></th>
							<td>
						
								<table class="displaytag" style="width: auto;">
									<tr>
										<th>No.</th>
										<th>NAMA_PERUSAHAAN</th>
										<th>NAMA_PEMEGANG_POLIS</th>
										<th>NIK</th>
										<th>WARGA_NEGARA</th>
										<th>TANGGAL_LAHIR</th>	
										<th>KOTA_LAHIR</th>
										<th>JNS_KELAMIN</th>	
										<th>STATUS</th>
										<th>AGAMA</th>	
										<th>**  KETERANGAN  **</th>
											
									</tr>
									<c:forEach items="${successMessage2}" var="a" varStatus="s">
										<tr>
									
											<td style="text-align: left;">
												<c:choose>
												<c:when test="${a.no eq '** KOSONG **'}">
												<font color="red">${a.no}</font>
												</c:when>
												<c:otherwise>
												<font color="blue">${a.no}</font>
												</c:otherwise>
												</c:choose>											
											</td>
											
											<td style="text-align: left;">
												<c:choose>
												<c:when test="${a.nama_kantor eq '** KOSONG **'}">
												<font color="red">${a.nama_kantor}</font>
												</c:when>
												<c:otherwise>
												<font color="blue">${a.nama_kantor}</font>
												</c:otherwise>
												</c:choose>											
											</td>
											<td style="text-align: left;">
												<c:choose>
												<c:when test="${a.nama eq '** KOSONG **'}">
												<font color="red">${a.nama}</font>
												</c:when>
												<c:otherwise>
												<font color="blue">${a.nama}</font>
												</c:otherwise>
												</c:choose>											
											</td>
											<td style="text-align: left;">
												<c:choose>
												<c:when test="${a.nik eq '** KOSONG **'}">
												<font color="red">${a.nik}</font>
												</c:when>
												<c:otherwise>
												<font color="blue">${a.nik}</font>
												</c:otherwise>
												</c:choose>											
											</td>
											<td style="text-align: left;">
												<c:choose>
												<c:when test="${a.warga_negara eq '** KOSONG **'}">
												<font color="red">${a.warga_negara}</font>
												</c:when>
												<c:otherwise>
												<font color="blue">${a.warga_negara}</font>
												</c:otherwise>
												</c:choose>											
											</td>
											<td style="text-align: left;">
												<c:choose>
												<c:when test="${a.tgl_lahir eq '** KOSONG **'}">
												<font color="red">${a.tgl_lahir}</font>
												</c:when>
												<c:otherwise>
												<font color="blue">${a.tgl_lahir}</font>
												</c:otherwise>
												</c:choose>											
											</td>
											<td style="text-align: left;">
												<c:choose>
												<c:when test="${a.tempat_lahir eq '** KOSONG **'}">
												<font color="red">${a.tempat_lahir}</font>
												</c:when>
												<c:otherwise>
												<font color="blue">${a.tempat_lahir}</font>
												</c:otherwise>
												</c:choose>											
											</td>
											<td style="text-align: left;">
												<c:choose>
												<c:when test="${a.sex eq '** KOSONG **'}">
												<font color="red">${a.sex}</font>
												</c:when>
												<c:otherwise>
												<font color="blue">${a.sex}</font>
												</c:otherwise>
												</c:choose>											
											</td>
											<td style="text-align: left;">
												<c:choose>
												<c:when test="${a.sts_mrt eq '** KOSONG **'}">
												<font color="red">${a.sts_mrt}</font>
												</c:when>
												<c:otherwise>
												<font color="blue">${a.sts_mrt}</font>
												</c:otherwise>
												</c:choose>											
											</td>
											<td style="text-align: left;">
												<c:choose>
												<c:when test="${a.agama eq '** KOSONG **'}">
												<font color="red">${a.agama}</font>
												</c:when>
												<c:otherwise>
												<font color="blue">${a.agama}</font>
												</c:otherwise>
												</c:choose>											
											</td>
											<td style="text-align: left;">
												<c:choose>
												<c:when test="${a.ket eq '**   OK   **'}">
												<font color="blue">${a.ket}</font>
												</c:when>
												<c:otherwise>
												<font color="red">${a.ket}</font>
												</c:otherwise>
												</c:choose>											
											</td>
										</tr>
									</c:forEach>							
								</table>
							</td>
						</tr>			
					</table>
					
				</form>
			</div>
			<div id="pane2" class="panes">
				<br/><br/>
				<table  style="width: auto;">
				<tr>
				<td align="left">
				<u>Petunjuk Upload PDFStamper Template PRE_SPAJ (Surat Permintaan Asuransi Jiwa) Individu Worksite MNC :</u>
				</td>
				</tr>
				<tr>
				<td align="left">
				(1). Format Kolom File Xls untuk Cafetari MNC : NO, NAMA_PERUSAHAAN, NAMA_PEMEGANG_POLIS, NIK, WARGA_NEGARA, TANGGAL_LAHIR, KOTA_LAHIR, JNS_KELAMIN, STATUS, AGAMA 
				</td>
				</tr>
				<tr>
				<td align="left">
				(2). Hasil Upload berupa file Pdf untuk template PRE_SPAJ Individu, Yakni Surat Permintaan Asuransi Jiwa Worksite MNC, yang sudah di-PDFStamper dengan data-data calon Pemegang_Polis dari file upload
				</td>
				</tr>
				<tr>
				<td align="left">
				(3). Hasil Upload disimpan di:  \\EBSERVER\pdfind\PRE_SPAJ\CAFETARI_MNC\
				</td>
				</tr>
				</table>
				<br/><br/>
				<img alt="Panduan Upload PRE_SPAJ Individu Worksite MNC" src="${path}/include/image/xls_cafmnc.jpg">
				<br/><br/>
		
			</div>
		</div>
	</div>
	
</body>
<%@ include file="/include/page/footer.jsp"%>