<%@ include file="/include/page/header.jsp"%>
<style type="text/css">
	html,body{
	     overflow: hidden;
	}
</style>
<script>
	function cek(pw, inp){
		if(pw.value.toUpperCase()==inp.value.toUpperCase()){
			if('${param.caller}'=='viewer'){
				window.returnValue = true;
			}
		}else{
			window.returnValue = false;
		}
		window.close();
	}
	var pesan = '${berhasil}';
		if(pesan==1){
			alert('Password Anda berhasil Diganti menjadi Password baru');
 			window.location='${path}/common/menu.htm?frame=treemenu';
			
		}
		
</script>
<body style="height: 100%;" onload="setupPanes('container1', 'tab1'); ">

	<div class="tab-container" id="container1">
		
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Change Login Password</a>
			</li>
		</ul>

		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form method="post" name="formpost">
					<table class="entry2">
						<tr>
							<th style="width: 150px;">User</th>
							<td>
								<input name="username" type=text value="${sessionScope.currentUser.name}" size="30" disabled>
								<input name="username" type=text value="${sessionScope.currentUser.lus_id}" size="5" disabled>
							</td>
						</tr>
						<tr>
							<th>Old Password</th>
							<td>
								<input name="oldpassword" type="password" size="38" value="">
							</td>
						</tr>
						<tr>
							<th>New Password</th>
							<td>
								<input name="newpassword" type="password" size="38" value="">
							</td>
						</tr>
						<tr>
							<th>Confirm New Password</th>
							<td>
								<input name="confirmpassword" type="password" size="38" value="">
							</td>
						</tr>
						<tr>
							<th></th>
							<td>
								<c:if test="${not empty err}">
									<div id="error">ERROR:<br>
										<c:forEach var="error" items="${err}">
															- <c:out value="${error}" escapeXml="false" />
											<br />
										</c:forEach>
									</div>				
								</c:if>
								<c:if test="${not empty succ}">
									<div id="success">${succ}</div>				
								
								</c:if>
								<input type="hidden" name="save" value="save">
								<input type="button" name="btnSave" value="Save" onclick="this.disabled=true;document.formpost.submit();"
									accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
							</td>							
						</tr>						
					</table>
					<c:if test="${empty succ}">
						<table class="entry2">						
							<tr>
								<td id="success">
								
								 Petunjuk Pembuatan Password Baru:<br>
									-Jumlah angka minimal/paling sedikit ada 1<br>
									-Jumlah huruf kecil minimal/paling sedikit ada 1<br>
									-Jumlah huruf besar minimal/paling sedikit ada 1<br>
									-Password baru tidak boleh sama dengan password lama<br>
									-Password minimal 8 Karakter<br>
									-Password Anda akan expired setelah 90 hari<br>
									
								</td>
							</tr>
				   			
							
					  </table>	
				  </c:if>				
				</form>			
			</div>		
			</div>	
			</div>
</body>
<%@ include file="/include/page/footer.jsp"%>