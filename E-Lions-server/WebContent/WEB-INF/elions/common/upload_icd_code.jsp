<%@ include file="/include/page/header.jsp"%>
	<body onload="setupPanes('container1', 'tab1');">
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Upload ICD Code</a>
					<!--<a href="#" onClick="return showPane('pane2', this)" id="tab2">Help</a>-->
				</li>
			</ul>		
		</div>
		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form name="formpost" method="post" enctype="multipart/form-data">
					<table class="entry2">
						<tr>
							<th style="width: 200px;">Upload ICD Code<br><span class="info">* Max. 1 Mb (1024 Kb)</span></th>
							<td>
								<input type="file" name="file1" />
							</td>
						</tr>
						<tr>
							<th></th>
							<td>
								<input type="submit" name="upload" value="Upload" onclick="return confirm('Apakah data yang dimasukkan sudah benar ?');">
								<c:choose>
									<c:when test="${not empty cmd.errorMessages}">
										<div id="error">
										ERROR:<br>
										<c:forEach var="error" items="${cmd.errorMessages}">
													- <c:out value="${error}" escapeXml="false" />
											<br />
										</c:forEach></div>
									</c:when>
									<c:otherwise>
										<div id="success" style="text-transform: none;">
											File harus bertipe EXCEL (.xls) dan terdiri dari 3 kolom saja : LIC_ID, LIC_DESC, LIC_TYPE.
										</div>
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
						<tr>
							<th></th>
							<td>
								<table class="displaytag" style="width: auto;">
									<tr>
										<th>No.</th>
										<th>LIC_ID</th>
										<th>LIC_DESC</th>
										<th>LIC_TYPE</th>						
									</tr>
									<c:forEach items="${cmd.daftarStatus }" var="a" varStatus="s">
										<tr>
											<td style="text-align: left;">${s.count}.</td>
											<td style="text-align: center;">${a.lic_id}</td>
											<td style="text-align: left;">${a.lic_desc}</td>
											<td style="text-align: center;">${a.lic_type}</td>
										</tr>
									</c:forEach>							
								</table>
							</td>
						</tr>											
					</table>
				</form>
			</div>
		</div>			
	</body>
<%@ include file="/include/page/footer.jsp"%>
