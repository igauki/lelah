<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<link type="text/css" href="${path}/include/js/jquery/theme/ui.all.css" rel="Stylesheet" />	
<script type="text/javascript" src="${path}/include/js/jquery/jquery-1.2.6.js"></script>
<script type="text/javascript" src="${path}/include/js/jquery/jquery-ui-personalized-1.5.3.js"></script>

<script>
	$(function() {
		$('#datepicker').datepicker({
			changeMonth: true,
			changeYear: true
		});
	});
</script>

<body onload="document.formpost.spaj.focus();">
	<form method=post name=formpost>
		<fieldset>
			<legend>File Downloader</legend>
			<table class="entry">
				<tr>
					<th>SPAJ</th>
					<td>
						<input type="text" name="spaj" value="${spaj}">
						<input type="submit" name="hitung" value="Hitung">
					</td>
				</tr> 
			</table>
		</fieldset>
		<br/>

		<div class="demo">
			<input type="text" name="pesan" size="200" value="${pesan}">
		</div>

	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>
