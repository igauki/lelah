<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<%

	String url = request.getRequestURL().toString().toLowerCase();
	String intranet = "http://intranet";
	String ajsjva = "http://128.21.32.14";
	String ip = request.getLocalAddr();
	
	if (ip.contains("128.") || ip.contains("196.")){
		ajsjva = "http://128.21.32.14";
	}else {
		intranet = "http://www.sinarmasmsiglife.com";
		ajsjva = "http://202.43.181.35";
	}
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="description" content="Sinarmas MSIG Life Insurance" />
<meta name="keywords" content="asuransi,asuransi jiwa,sinarmas,ekalife" />
<meta name="author"
	content="PT. Asuransi Jiwa Sinarmas MSIG  | Administered by: Yusuf Sutarko | Original design: Andreas Viklund - http://andreasviklund.com/" />
<link rel="stylesheet" type="text/css" href="${path }/include/css/andreas02.css"
	media="screen" title="andreas02 (screen)" />
<script type="text/javascript"
	src="${path }/include/js/default.js"></script>
<script>
	hideLoadingMessage();
	
	if ( window.XDomainRequest ) {
		jQuery.ajaxTransport(function( s ) {
			if ( s.crossDomain && s.async ) {
				if ( s.timeout ) {
					s.xdrTimeout = s.timeout;
					delete s.timeout;
				}
				var xdr;
				return {
					send: function( _, complete ) {
						function callback( status, statusText, responses, responseHeaders ) {
							xdr.onload = xdr.onerror = xdr.ontimeout = jQuery.noop;
							xdr = undefined;
							complete( status, statusText, responses, responseHeaders );
						}
						xdr = new XDomainRequest();
						xdr.onload = function() {
							callback( 200, "OK", { text: xdr.responseText }, "Content-Type: " + xdr.contentType );
						};
						xdr.onerror = function() {
							callback( 404, "Not Found" );
						};
						xdr.onprogress = jQuery.noop;
						xdr.ontimeout = function() {
							callback( 0, "timeout" );
						};
						xdr.timeout = s.xdrTimeout || Number.MAX_VALUE;
						xdr.open( s.type, s.url );
						xdr.send( ( s.hasContent && s.data ) || null );
					},
					abort: function() {
						if ( xdr ) {
							xdr.onerror = jQuery.noop;
							xdr.abort();
						}
					}
				};
			}
		});
	}
	
	function logProp(value){
		var kd = "${sessionScope.currentUser.lus_id}";
        var id = "-";
        var web = "elions";
        var type = value;
        
        var servername = window.location.host;
        var url2 = "http://eproposal.sinarmasmsiglife.co.id?";
        
        if(servername.indexOf("elions") > -1) url2 = "http://eproposal.sinarmasmsiglife.co.id?";
        else url2 = "http://"+ servername +"/E-Proposal/?";
        
		$.ajax({
		    type: 'POST',
		    url: 'http://ews.sinarmasmsiglife.co.id/api/json/login_proposal',
		    data: '{"kode": "'+kd+'", "web": "'+web+'", "id": "'+id+'", "type": "'+type+'"}', 
		    success: function(data) {
					    window.open(url2+data, '_blank');
					 },
			error : function(data, response){
					console.log(response);
			},
		    contentType: "application/json"
		});
	}
			
	function running(number){
// 		popWin('${path}/common/links.htm?window=main&running='+number, 600,800);
		var popup = window.open('${path}/common/links.htm?window=main&running='+number, '_blank');
  		window.focus();
  		if (!popup || popup.closed || typeof popup.closed=='undefined'){
	    	//Worked For IE and Firefox
	    	alert("Popup Blocker is enabled! Please turn it off to open this site.");
		}
	}
	
	
</script>
<title>PT. Asuransi Jiwa Sinarmas MSIG</title>
</head>
<body>
	<div id="toptabs">
		<p>&nbsp;</p>
	</div>
	<div id="container">
		<div id="navitabs">
			<h2 class="hide">Site menu:</h2>
			<a class="navitab1"
				href="${path }/common/home.htm">&nbsp;</a>
		</div>
		<div class="block" style="text-align: center">
			<table align=center>
				<tr>
					<input type="hidden" id="lus_id" name="lus_id" value="${lus_id}">
					<c:if test="${sessionScope.currentUser.jn_bank ne 2 and sessionScope.currentUser.jn_bank ne 3}">
						<td align=center>
							<a onclick="running(1);" target="_blank" ><img
								src="${path }/include/image_links/epol_s.gif" title="E-Policy" /><br />E-Policy
							</a>
						</td>
						<td align=center><a onclick="running(2);" target="_blank"><img
								src="${path }/include/image_links/eagency_s.gif" title="E-Agency" /><br />E-Agency</a>
						</td>
						
						<c:if test="${sessionScope.currentUser.lus_id ne 2527}">
							<td align=center><a onclick="running(3);"
								target="_blank"><img src="${path }/include/image_links/bas_s.gif"
									title="BAS" /><br />BAS (internal)</a>
							</td>
							<td align=center><a
								onclick="running(4);"
								target="_blank"><img src="${path }/include/image_links/bas_s.gif"
									title="BAS" /><br />BAS (eksternal)</a>
							</td>
						</c:if>
					</c:if>
					
					<c:if test="${sessionScope.currentUser.jn_bank eq 2 || sessionScope.currentUser.jenis_hardcoded_user eq 7}">
					<td align=center><a onclick="running(5);" target="_blank"><img
							src="${path }/include/image_links/proposal.gif" title="E-Proposal" /><br />E-Proposal</a></c:if></td>
					<c:if test="${sessionScope.currentUser.jn_bank eq 3}">
					<td align=center><a onclick="running(6);" target="_blank"><img
							src="${path }/include/image_links/proposal.gif" title="E-Proposal" /><br />E-Proposal</a></c:if></td>
				</tr>
			</table>
			<table align=center>
				<tr>
					<td>
						<h3>Links</h3>
						<div style="text-align: left">
							<ul>
								<li><a href="<%=intranet%>/productinfo.asp">Product
										Info</a>
								</li>
								<li><a href="<%=intranet%>/faq.asp">FAQ</a>
								</li>
								<li><a href="<%=intranet%>/flash/pageFlip/fc.html">Brosur
										Produk</a>
								</li>
								<li><a href="<%=intranet%>/helpdesk">Helpdesk</a>
								</li>
							</ul>
						</div></td>
				</tr>
			</table>
		</div>
		<div id="footer" style="text-align: center;">
			Copyright &copy; 2006-2007 <br />IT Department - PT. Asuransi Jiwa
			Sinarmas MSIG
		</div>
	</div>
</body>
</html>