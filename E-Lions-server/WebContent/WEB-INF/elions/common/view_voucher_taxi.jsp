<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
    <head>
        <title>PT. Asuransi Jiwa Sinarmas MSIG</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <meta name="Description" content="Ekalife">
        <link rel="Stylesheet" type="text/css" href="${path}/include/css/default.css" media="screen">
        <link rel="shortcut icon" href="${path}/include/image/eas.ico">
        <style>
            body,legend, table.entry th,
            table.entry td, .button {
                font-size: 12px;
            }
            
            input {
                font-size: 10px;
                padding-left: 10px;
                height: 20px;
            }
            
            .button {
                padding: 5px;
                height: 30px;
                cursor: pointer;
            }
            
            table.entry th {
                text-align: center;
                vertical-align: middle;
                padding-top: 5px;
                padding-bottom: 5px;
            }
            
            table#inputTbl.entry td {
                padding-left: 10px;
            }
        </style>
        <!-- DatePicker Script (jscalendar) -->
        <link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
        <script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
        <script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
        <script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
        <script type="text/javascript" src="${path}/include/js/default.js"></script>
        <script type="text/javascript">
            hideLoadingMessage();
        </script>
    </head>
    <body>
        <div id="contents">
            <fieldset>
                <legend>View Report Voucher Taxi</legend>
                <form method="POST">
                    <table class="entry" style="margin: 0 auto;">
                        <tr>
                            <th style="width: 35%;">Departemen</th>
                            <td>
                                <select name="dept">
                                    <c:forEach items="${deptList}" var="dept">
                                        <option value="${dept.value}">
                                            <c:choose>
                                                <c:when test="${empty dept.key}">All</c:when>
                                                <c:otherwise>${dept.key}</c:otherwise>
                                            </c:choose>
                                        </option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>Periode</th>
                            <td>
                                <script>
                                    inputDate('from_date', '', false);
                                </script>
                                S/D
                                <script>
                                    inputDate('to_date', '', false);
                                </script>
                            </td>
                        </tr>
                        <tr>
                            <th style="background-color: #FFFFFF;"> </th>
                            <td style="text-align: center;">
                                <input type="submit" name="submit" value="View PDF">
                                <input type="submit" name="submit" value="View XLS">
                            </td>
                        </tr>
                    </table>
                </form>
            </fieldset>
        </div>
    </body>
</html>