<%@ include file="/include/page/header.jsp"%>
<script>

</script>
<BODY style="height: 100%;">

<form:form id="formpost" name="formpost" commandName="cmd">
	<table class="entry2">
		<tr>
			<th>Kode Nasabah : ${cmd.nasabah.mns_kd_nasabah }</th>
			<th>No Referral  : ${cmd.nasabah.mns_no_ref }</th>
		</tr>
		<tr>
			<th>Cabang</th>
		</tr>
		<tr>
		 <td colspan="4">
			<c:if test="${submitSuccess eq true}">
	        	<div id="success">
		        	 Berhasil
		    	</div>
	        </c:if>	
  			<spring:bind path="cmd.*">
				<c:if test="${not empty status.errorMessages}">
					<div id="error">
						 Informasi:<br>
							<c:forEach var="error" items="${status.errorMessages}">
										 - <c:out value="${error}" escapeXml="false" />
								<br/>
							</c:forEach>
					</div>
				</c:if>									
			</spring:bind>
		 </td>
		</tr>
		<tr>
			<th></th>
			<td>	
				<input type="button" name="btnsave" onclick="proses();" value="Save"></input>
			</td>
		</tr>
	</table>
</form:form>
</body>
<%@ include file="/include/page/footer.jsp"%>