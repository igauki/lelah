<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
	<title>PT. Asuransi Jiwa Sinarmas MSIG</title>
	<meta name="verify-v1" content="NWau46kEyEm+WnYb1AfPtB0ZUV07pSP40di+b+kxleA=" />
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta name="Description" content="SinarmasLife">
	<link rel="Stylesheet" type="text/css" href="${path }/include/css/default.css" media="screen">
	<link href="${path}/include/image/eas.ico" rel="shortcut icon">
	<script type="text/javascript" src="${path }/include/js/default.js"></script>
	<script type="text/javascript"><!--
		function handleEnter(inField, e) {
		    var charCode;
		    
		    if(e && e.which){
		        charCode = e.which;
		    }else if(window.event){
		        e = window.event;
		        charCode = e.keyCode;
		    }
		
		    if(charCode == 13) {
		        document.formpost.btnLogin.click();
		    }
		}	
		
		function log_in(btn){
			btn.disabled=true;
			document.formpost.screenWidth.value = screen.width;
			document.formpost.screenHeight.value = screen.height;
			document.formpost.submit();
		}

		function on_load(){
			document.formpost.btnLogin.click();
		}

		function vrsn_splash() {
			tbar = "location=yes,status=yes,resizable=yes,scrollbars=yes,width=560,height=500";
			sw = window.open("https://seal.verisign.com/splash?form_file=fdf/splash.fdf&dn=www.sinarmasmsiglife.co.id&lang=en", 'VRSN_Splash', tbar);
			sw.focus();
		}
		
	--></script>
</head>
<body onload="on_load();" style="height: 100%; width: 100%; text-align: center;">

	<c:choose>
		<c:when test="${notAllowed}">
			<script type="text/javascript">
			alert('Maaf anda tidak dapat mengakses halaman ini. Terima kasih.');
			window.close();
		</script>
		</c:when>
		<c:otherwise>

		<form method="post" name="formpost" action="">
			<input type="hidden" name="screenWidth">
			<input type="hidden" name="screenHeight">
			<input type="hidden" name="deebee" value="${deebee}">
			<input type="hidden" name="login" value="Login">
			<input type="hidden" name="ac" value="${ac}">
			<div style="padding: 75px; padding-bottom: 150px;"><img src="${path }/include/image/SIMASLIFE-700x259.gif" alt="SinarmasLife" /></div>
			<input type="button" name="btnLogin" value="Login" style="height: 60px; width: 60px;visibility: hidden;" onclick="log_in(this);">
			
			<div id="footer_login">
				<object height="60" width="90">
					<param name="movie" value="somefilename.swf"/>
					  <param name="loop" value="true" />
	                    <param name="menu" value="false" />
	                    <param name="quality" value="best" />
	                    <param name="wmode" value="transparent" />
					<embed src="${path}/include/image/getseal.swf" height="60" width="90" loop="true" menu="false" quality="best" wmode="transparent" swliveconnect="FALSE">
					</embed>
				</object><br/>
				&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.verisign.com/ssl/ssl-information-center/" target="_blank" style="color:black; text-decoration:none; font:bold 8px;">ABOUT SSL CERTIFICATES </a>								
				<br><spring:message code="copyright" />
				Sinarmas MSIG Life IT Department [${sessionScope.deebee}]
			</div>
			
		</form>
		
		</c:otherwise>
	</c:choose>
</body>
</html>