<%@ include file="/include/page/header.jsp"%>
<script>
	function tampil(flag){
		var noRef='';
		no=document.formpost.nomor.value;
		if(flag == '0'){
		document.getElementById('infoFrame').src='${path }/common/input_analyst.htm?nomor='+no+'&flag='+flag;
		}else if(flag == '1'){
			popWin('${path}/common/shortfall_calculator.htm?nomor='+no, 480, 640);
		}else if(flag == '2'){
			popWin('${path}/common/follow_up.htm?nomor='+no, 150, 640);
		}
	}
	
	function tes(){//bagian ini untuk merefresh parent saat melakukan submit di iframe(childnya).Yang jadi child ada di halaman input_referral.jsp
		//alert('Masuk kesini');
		window.location='${path}/common/analyst.htm?window=main_analyst';
		
	}
</script>
<body onload="setFrameSize('infoFrame', 45); " onresize="setFrameSize('infoFrame', 45);" style="height: 100%;" >

<form:form id="formpost" name="formpost" commandName="cmd">
	<table class="entry2" width="98%">
		<tr>
			<th>Nomor</th>
			<td>
				<select name="nomor" >
					<c:forEach items="${lsNasabah}" var="x" varStatus="xt">
						<option value="${x.mns_no_ref}~${x.mns_kd_nasabah}">${x.mns_no_ref}~${x.mns_kd_nasabah}</option>
					</c:forEach>
				</select>
				<input type="button" value="Cari" onClick="popWin('${path}/common/analyst.htm?window=cari_analyst', 500, 500);" />
				<input type="button" value="SHow" onClick="tampil('0');" />
				<input type="button" value="Shortfall Calculator" onclick="tampil('1');" />
				<input type="button" value="Follow Up" onclick="tampil('2');" />
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<iframe src="${path}/common/input_analyst.htm?nomor=${nomor}" name="infoFrame" id="infoFrame"
					width="100%"  > Please Wait... </iframe>
			</td>
		</tr>
	</table>
</form:form>
</body>
<%@ include file="/include/page/footer.jsp"%>