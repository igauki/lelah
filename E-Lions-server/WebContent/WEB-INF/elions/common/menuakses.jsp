<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript" src="${path }/include/js/dhtmlx/dhtmlXCommon.js"></script><!-- TreeView -->
<script type="text/javascript" src="${path }/include/js/dhtmlx/dhtmlXTree.js"></script>
<script>
	function ubah(nama, nama2, nama3){
		ajaxSelectWithParam(nama3, 'listEmployeesInDepartment', 'copyFromDiv', 'copyFromSelect', '', 'MCL_ID', 'USERNAME', '');
		if(document.getElementById(nama) && document.getElementById(nama2)){
			if(document.getElementById(nama2).style.display!='none') document.getElementById(nama2).style.display='none';
			else document.getElementById(nama2).style.display = 'block';
			return false;
		}else{
			return true;
		}
	}

	function retrieveTree(p,msg, kopi){
		document.getElementById('treeboxbox_tree').innerHTML=msg;
		if(!kopi)document.formpost.lus_id.value=p;
		tree=new dhtmlXTreeObject("treeboxbox_tree","100%","100%",0);
		tree.setImagePath("${path}/include/js/dhtmlx/img/");
		tree.enableCheckBoxes(1);
		tree.loadXML("${path}/common/menu.htm?frame=xml&tipe=otorisasimenu&spaj=&jenis="+document.formpost.jenisAplikasi.value+"&aplikasi="+document.formpost.jenisAplikasi.options[document.formpost.jenisAplikasi.selectedIndex].text+"&lus_id="+p);	
		tree.enableThreeStateCheckboxes(true);
	}
	
	function btn(a){
		if(document.formpost.lus_id.value==''){
			alert('Harap pilih user terlebih dahulu');
			return false;
		}if(a=='copy' && document.formpost.copyFromSelect){
			if(confirm('Copy Menu dari user '+document.formpost.copyFromSelect.options[document.formpost.copyFromSelect.selectedIndex].text + '?')){
				retrieveTree(
					document.formpost.copyFromSelect.value, 
					'Copying Menu from '+document.formpost.copyFromSelect.options[document.formpost.copyFromSelect.selectedIndex].text +' to ' + document.formpost.username.value,
					'kopi');
			}
		}else if(a=='simpan'){
			cek = tree.getAllCheckedBranches();
			if(cek==''){ alert('Harap pilih menu terlebih dahulu'); return false;}
			document.formpost.nilai.value= cek;
			return confirm('Simpan Perubahan?');
		}else if(a=='nonaktif'){
			return confirm('Anda yakin me-nonaktifkan user ini?');
		}else if(a=='akses'){
			popWin('${path}/common/menu.htm?frame=cabang&lus='+document.formpost.lus_id.value+'&username='+document.formpost.username.value
				, 400, 700, 'yes','yes');
			//popWin('${path}/common/menu.htm?frame=framecabang&lus='
			//+document.formpost.lus_id.value+'&username='+document.formpost.username.value, 400, 700, 'yes','yes');
		}
		return false;
	}

</script>
<body style="height: 100%;">

	<div id="contents">
	<form name="formpost" method="post">
		<fieldset>
			<legend>Menu Authorization</legend>
			<table>
				<tr>
					<td valign="top">
						User List: 
						<div class="sideMenu">
						<ul>
							<c:forEach var="dept" items="${allDept}" varStatus="st">
								<li >
									<a href="#" 
											onClick="if(ubah('ul${st.count}','div${st.count}','${dept.LDE_ID}'))ajaxUL('ul${st.count}', 'div${st.count}', '${dept.LDE_ID}'); return false;">
										<strong>${dept.LDE_DEPT}</strong></a>
									<div id="div${st.count}"></div>
								</li>
							</c:forEach>
						</ul>
						</div>
					</td>
					<td valign="top">
						<table class="entry">
							<tr>
								<th>Jenis Aplikasi:</th>
								<td>
									<select name="jenisAplikasi" onchange="retrieveTree(document.formpost.lus_id.value, 'Loading Menu ' + document.formpost.jenisAplikasi.options[document.formpost.jenisAplikasi.selectedIndex].text + ' for ' + document.formpost.username.value );">
										<c:forEach var="s" items="${allApp}">
											<option value="${s.JENIS_ID}">${s.NAMA_APP}</option>
										</c:forEach>
									</select>
									<input type="hidden" name="nilai" value="">
									<input type="hidden" name="lus_id" value="">
									<input type="hidden" name="username" value="">
								</td>
							</tr>
							<tr>
								<th>Copy semua Menu dari:</th>
								<td><div id="copyFromDiv"></div></td>
							</tr>
							<tr>
								<th></th>
								<td>
									<input type="button" name="copy" value="Copy" onclick="return btn('copy');"
										accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">
									<input type="submit" name="simpan" value="Simpan" onclick="return btn('simpan');"
										accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
									<input type="submit" name="nonaktif" value="Non-Aktifkan" onclick="return btn('nonaktif');"
										accesskey="N" onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
									<input type="button" name="akses" value="Akses Cabang" onclick="return btn('akses');"
										accesskey="A" onmouseover="return overlib('Alt-A', AUTOSTATUS, WRAP);" onmouseout="nd();">
								</td>
							</tr>
							<tr>
								<th valign="top">Menu:</th>
								<td>
									<div id="treeboxbox_tree" style="width:610; height:395;background-color:#f5f5f5;border :1px solid Silver;; overflow:auto;"/>
								</td>
							</tr>
							<tr>
								<th></th>
								<td>
									<div class="info">* Menu Biru adalah menu yang dapat diakses semua user</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</fieldset>
	</form>

    <c:if test="${not empty jenisAplikasi}">
        <script>
            document.formpost.jenisAplikasi.value='${jenisAplikasi}';
        </script>
    </c:if>
	<c:if test="${not empty s1}">	
		<script>
			retrieveTree('${s1}', '${s3}'); 
			document.formpost.lus_id.value='${s1}';
			document.formpost.username.value='${s2}';
			alert('${s3}');
		</script>
	</c:if>	

</div>
</body><%@ include file="/include/page/footer.jsp"%>