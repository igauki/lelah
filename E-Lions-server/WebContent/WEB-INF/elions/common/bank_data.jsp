<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">

$().ready(function() {
		
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("#tabs").tabs();
		$("input[title], select[title], textarea[title], button[title], a[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		
		$(".checkall").click(function () {
			tot = $(this).parent().find(":hidden");
			if(tot.val() == "true") tot.val(""); else tot.val("true");
			$(this).parent().parent().find(":checkbox").attr("checked", tot.val());
			return false;
		});	
		
		$("#select_data").change(function() {
			$("#select_kriteria").empty();
			var url = "${path}/common/bankdata.htm?json=1&jenis=" +$("#select_data").val(); ///uw/ubah_reas.htm?
			$.getJSON(url, function(result) {
				$("<option/>").val(" ").html("").appendTo("#select_kriteria");
				$.each(result, function() {
					$("<option/>").val(this.LBD_ID).html(this.LBD_NAME).appendTo("#select_kriteria");
				});
				$("#select_kriteria option:first").attr('selected','selected');
			});
	
		});
		
	$("#select_kriteria").change(function() {
			$("#select_sub_kriteria").empty();
			var url = "${path}/common/bankdata.htm?json=2&jenis=" +$("#select_kriteria").val(); ///uw/ubah_reas.htm?
			$.getJSON(url, function(result) {
				//$("<option/>").val("PILIH WILAYAH").html("").appendTo("#select_sub_kriteria");
				$.each(result, function() {
					$("<option/>").val(this.LBD_ID).html(this.LBD_NAME).appendTo("#select_sub_kriteria");
				});
				$("#select_sub_kriteria option:first").attr('selected','selected');
			});
	
		});
		
		
	$("#select_data_view").change(function() {
			$("#select_kriteria_view").empty();
			var url = "${path}/common/bankdata.htm?json=1&jenis=" +$("#select_data_view").val(); ///uw/ubah_reas.htm?
			$.getJSON(url, function(result) {
				$("<option/>").val(" ").html("").appendTo("#select_kriteria_view");
				$.each(result, function() {
					$("<option/>").val(this.LBD_ID).html(this.LBD_NAME).appendTo("#select_kriteria_view");
				});
				$("#select_kriteria_view option:first").attr('selected','selected');
			});
	
		});
		
	$("#select_kriteria_view").change(function() {
			$("#select_sub_kriteria_view").empty();
			var url = "${path}/common/bankdata.htm?json=2&jenis=" +$("#select_kriteria_view").val(); ///uw/ubah_reas.htm?
			$.getJSON(url, function(result) {
				$("<option/>").val(" ").html("").appendTo("#select_sub_kriteria_view");
				$.each(result, function() {
					$("<option/>").val(this.LBD_ID).html(this.LBD_NAME).appendTo("#select_sub_kriteria_view");
				});
				$("#select_sub_kriteria_view option:first").attr('selected','selected');
			});
	
		});
			
	});

function CekRevisi(){
	var a = document.getElementById('jenisupload').value;
	window.location='${path}/common/uploadagen.htm?flag=1&jenisupload='+a;
}

function SubmitForm(path){
		alert(path);
		//window.location='${path }/common/input_referral.htm?nomor=${cmd.nasabah.mns_kd_nasabah}&flag=0';
	}

function ubahStatus(value) {
		
		document.getElementById("idDiv").innerHTML = ""
		input = document.createElement("select");
		input.setAttribute("name","level");
		input.setAttribute("id", "level");
		var opti1;
		var data;
		
					
		if(value == "PERJANJIAN") {
			data = new Array("","PERJANJIAN KEAGENAN PT AJ SINARMAS","PERJANJIAN KEAGENAN PT ARTHAMAS KONSULINDO","PERJANJIAN KEAGENAN RO","PERJANJIAN KEAGENAN CRO");
			
			document.getElementById("ket").innerHTML = " JENIS ";
			document.getElementById('tgl').style.visibility = "hidden";
			document.getElementById('idDiv').style.visibility = "visible";
			document.getElementById('viewbutton').style.visibility = "visible";
			document.getElementById('row').style.visibility = "visible";
		}
			else if(value == "STATUS" ) {
			data = new Array("","AKTIF","TIDAK AKTIF");
			document.getElementById("ket").innerHTML = " KONDISI ";
			document.getElementById('tgl').style.visibility = "hidden";
			document.getElementById('idDiv').style.visibility = "visible";
			document.getElementById('viewbutton').style.visibility = "visible";
			document.getElementById('row').style.visibility = "visible";
		}
			else if(value == "TANGGAL") {
			
			document.getElementById('ket').innerHTML = " MULAI DARI ";
			document.getElementById('tgl').style.visibility = "visible";				
			document.getElementById('idDiv').style.visibility = "hidden";
			document.getElementById('viewbutton').style.visibility = "visible";
			document.getElementById('row').style.visibility = "visible";
		}
			else if(value == "") {
			
			document.getElementById('ket').innerHTML = "";	
			document.getElementById('tgl').style.visibility = "hidden";
			document.getElementById('idDiv').style.visibility = "hidden";
			document.getElementById('viewbutton').style.visibility = "hidden";
			document.getElementById('row').style.visibility = "hidden";
		}
			
		
		// buat option baru
		for(var isi in data) {
			opti1 = document.createElement("option");
			opti1.setAttribute("value",data[isi]);
			opti1.appendChild(document.createTextNode(data[isi]));
			input.appendChild(opti1);			
		}
		document.getElementById("idDiv").appendChild(input);
	}

</script>
<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.6em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	table tr td label { display: inline-block; width: 30em;}
	
	/* styling untuk table th */
	table tr th { padding: 1em;}
	
</style>

<BODY style="height: 100%;">
<div id="tabs">		
				<ul>
					<li><a href="#tab-1">MARKETING RESEARCH BANK DATA</a></li>
					<!-- <li><a href="#tab-3">MAINTANCE RESEARCH BANK DATA</a></li> -->
					
					
				</ul>
	<div id="tab-1">
	<form method="post" name="formpost" style="text-align: center;" enctype="multipart/form-data">
		<table class="entry2" style="width: 100%" align="left">
			<c:if test="${sessionScope.currentUser.lde_id eq \"01\" or sessionScope.currentUser.lde_id eq \"17\"}">
			<tr>
				<td>
				<fieldset class="ui-widget ui-widget-content">
					<legend class="ui-widget-header ui-corner-all"><div>Bank Data Marketing Research Dan Intellegence</div></legend>
						<table class="entry2">
						
					<tr style="background-color: #F5F5F5;" align="left">
						<th>Jenis Data</th>
							<th class="left" style="white-space: nowrap;">
			                   <select name="select_data" id="select_data">
			                   <option value ="ALL">[PILIH JENIS DATA]</option>
			                        <c:forEach var="x" items="${select_data}">
			                          <option value="${x.LBD_ID}">${x.LBD_NAME}
                          			  </option>
			                        </c:forEach>
			                    </select>
             		 	  </th>
							</tr>
						<tr style="background-color: #F5F5F5;" align="left">
								<th>Kriteria</th>
								<th class="left" style="white-space: nowrap;">
			                    <select name="select_kriteria" id="select_kriteria">
			                      <option value ="ALL">[PILIH KRITERIA]</option>
			                        <c:forEach var="x" items="${select_kriteria}">
			                          <option value="${x.LBD_ID}">${x.LBD_NAME}
                          			  </option>
			                        </c:forEach>
			                    </select>
             		 	  </th>
						</tr>
						<tr style="background-color: #F5F5F5;" align="left">
								<th>Sub Kriteria</th>
								<th class="left" style="white-space: nowrap;">
			                    <select name="select_sub_kriteria" id="select_sub_kriteria">
			                      <option value ="ALL">[PILIH SUB KRITERIA]</option>
			                        <c:forEach var="x" items="${select_sub_kriteria}">
			                          <option value="${x.LBD_ID}">${x.LBD_NAME}
                          			  </option>
			                        </c:forEach>
			                    </select>
             		 	  </th>
							</tr>
							<tr style="background-color: #F5F5F5;" align="left">
								<th>Judul Data </th>
								<th colspan = "2" nowrap = "nowrap" align ="left"><textarea rows="3" cols="30" style="width: 70%; text-transform: uppercase;" name=judul_upload></textarea></font></th>
							</tr>
							<tr style="background-color: #F5F5F5;" align="left">
								<th>Sumber Data</th>
								<th align="left"><input type="text" size="30" name="sumber" ></th>
							</tr>
							<tr style="background-color: #F5F5F5;" align="left">
								<th>Kode Data</th>
								<th align="left"><input type="text" size="30" maxlength="30" name="kode_id" ></th>
							</tr>
							<tr style="background-color: #F5F5F5;" align="left">
								<th>Tanggal Pengumpulan Data</th>
								<th align="left">
								<input name="tgl_kumpul" id="tgl_kumpul" type="text" class="datepicker" title="Tanggal Pengumpulan" value="${today}">
								</th>
							</tr> 
							<tr style="background-color: #F5F5F5;" align="left">
								<th>File Upload Data</th>
								<th align="left"><input type="file" name="file1" id="file1" size="48" value="${file1}" /></th>
							</tr>
							<tr style="background-color: #F5F5F5;" align="left">
								<th></th>
								<th align="left"><input type="submit" name="uploadbutton" value="Upload"></th>
							</tr>
							<tr>
								<td>
									<span class="info">
									*Semua kolom harus diisi, Tidak boleh kosong<br>
									 </span>
								</td>
							</tr>
						</table>
					</fieldset>
				</td>
			</tr>
			</c:if>
			<tr>
				<td>
				<fieldset class="ui-widget ui-widget-content">
					<legend class="ui-widget-header ui-corner-all"><div>List Bank Data Marketing Research Dan Intellegence</div></legend>
						<table class="entry2">
							<tr style="background-color: #F5F5F5;" align="left">
								<th align="left">Pencarian Dokumen berdasarkan : 
									 <select name="select_data_view" id="select_data_view">
									 <option value ="ALL">[PILIH JENIS DATA]</option>
			                        <c:forEach var="x" items="${select_data}">
			                          <option value="${x.LBD_ID}">${x.LBD_NAME}
                          			  </option>
			                        </c:forEach>
			                    </select>
			                    <select name="select_kriteria_view" id="select_kriteria_view">
			                    <option value ="ALL">[PILIH KRITERIA]</option>
			                        <c:forEach var="x" items="${select_kriteria}">
			                          <option value="${x.LBD_ID}">${x.LBD_NAME}
                          			  </option>
			                        </c:forEach>
			                    </select>
			                    <select name="select_sub_kriteria_view" id="select_sub_kriteria_view">
			                    <option value ="ALL">[PILIH SUB KRITERIA]</option>
			                        <c:forEach var="x" items="${select_sub_kriteria}">
			                          <option value="${x.LBD_ID}">${x.LBD_NAME}
                          			  </option>
			                        </c:forEach>
			                    </select>
									<span id="ket"></span>
									<span id="idDiv">
										<select id="level" name="level" style="visibility: hidden;">
											<option></option>
										</select>
									</span>
								</th>
							</tr>
							<tr style="background-color: #F5F5F5;" align="left">
							<th align="left">Berdasarkan Tanggal Upload : 
							<input name="bdate" id="bdate" type="text" class="datepicker" title="Tanggal Awal" value="${bdate}"> s/d 
							<input name="edate" id="edate" type="text" class="datepicker" title="Tanggal Akhir" value="${edate}">	
							</tr> 
							<tr id="row">
								<th>
									<input type="submit" id="viewbutton" name="viewbutton" value="View">
								</th>
							</tr>
						</table>
					</fieldset>
				</td>
			</tr>
			<c:if test="${not empty list}">
			<tr>
				<th>
					<table class="entry2">
						<thead>
							<tr style="background-color: #F5F5F5;" align="left">
								<th width="5%">No</th>
								<th width="40%">Dokumen</th>
								<th width="10%">Tanggal Pengumpulan Data</th>
								<th>Sumber</th>
								<th>User</th>
								<th>Kode Data</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${list}" var="s" varStatus="st">
								<tr style="background-color: #F5F5F5;" align="left">
									<th>${st.count}</th>
									<th align="left">${s.FILENAME}</th>
									<th align="center"><fmt:formatDate value="${s.REVISI_DATE}" pattern="dd/MM/yyyy" /></th>
									<th>${s.KETERANGAN}</th>
									<th>${s.LUS_LOGIN_NAME}</th>
									<th>${s.UPLOAD_ID}</th>
									<td align="center">
										<a style="color: blue;" href="bankdata.htm?download=download&uploadid=${s.UPLOAD_ID}&code_id=${s.CODE_ID}&kode=${s.UPLOAD_JENIS}&jenis=${jenis}&level=${level}&tglawal=${tglDown1}&tglakhir=${tglDown2}" name="download" id="download" >Download</a>
										<input type="hidden" name="uploadid" id="uploadid" value="${s.UPLOAD_ID}"> 
										<input type="hidden" name="kode" id="kode" value="${s.UPLOAD_JENIS}"> 
									</td>
									
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</th>
			</tr>
			</c:if>
		</table>
		<table class="entry2">
			<c:if test="${not empty errors}">
				<tr>
					<td>
						<div id="error" style="text-transform: none;">ERROR:<br>
							<c:forEach var="err" items="${errors}">
								- <c:out value="${err}" escapeXml="false" />
								<br />
								<script>
									pesan += '${err}\n';
								</script>
							</c:forEach>
						</div>
					</td>
				</tr>
			</c:if>
		</table>
	</form>
</div>	
<%-- <div id="tab-3">
				<form method="post2" name="formpost2" style="text-align: center;">								
						<fieldset class="ui-widget ui-widget-content">	
						  	<table>
						  	<c:if test="${sessionScope.currentUser.lde_id eq \"01\" or sessionScope.currentUser.lde_id eq \"17\"}">
								<tr style="background-color: #F5F5F5;">
									<th>Judul Data / Kriteria / Sub Kriteria <input type="text" size="50" name="sumber" ></th>
									<th></th>
								</tr>
								<tr style="background-color: #F5F5F5;">
									<th>Urutan Data nya Ada di : 
								<input type="hidden" name="pil">
									<select name="kd_id" id="kd_id">
										<c:forEach items="${listkategori}" var="x">
										<option value="${x.key}">${x.value}</option></c:forEach></select></th>
										<th><input type="submit" id="update" name="update" value=" Update "></th>
								</tr>
							</div> 	
							</c:if>
							</table>				
						</fieldset>
					</div>	 			
					</form>						              
			</div> --%>
</BODY>
<c:if test="${not empty pesan}">
	<script>alert('${pesan}');</script>
</c:if>

<c:if test="${not empty jenisupload}">
	<script type="text/javascript">
		var a = '${jenisupload}';
		//alert(a);
		document.getElementById('jenisupload').value = a;
	</script>
</c:if>

<%@ include file="/include/page/footer.jsp"%>