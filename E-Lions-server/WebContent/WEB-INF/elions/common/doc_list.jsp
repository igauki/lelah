<%@ include file="/include/page/header.jsp"%>
<script>
	//http://ajsjava/E-Lions/common/util.htm?window=doc&file=asdf&spaj=01200700020
	
	/*function tampil(fil){
		var msag = document.getElementById('msag_id').value;
		
		if(msag !=''){
			v_mid = fil.substring(0, fil.indexOf("~", 0));
			
			v_filename = fil.substring(fil.indexOf("~", 0)+1);
			alert(v_filename);
			if(fil != '') document.getElementById('docFrame').src = '${path}/common/util.htm?window=doc_bp&file='+v_filename+'&mid=' + msag;
		}else{
			v_spaj = fil.substring(0, fil.indexOf("~", 0));
			v_filename = fil.substring(fil.indexOf("~", 0)+1);
			if(fil != '') document.getElementById('docFrame').src = '${path}/common/util.htm?window=doc&file='+v_filename+'&spaj=' + v_spaj;
		}
	}*/
	function tampil(fil){
		var msag = document.getElementById('msag_id').value;
		pos = fil.indexOf("~~", 0);
		
		if(pos>0){
			v_mid = fil.substring(0, fil.indexOf("~", 0));
			pos2 = fil.indexOf("~", 0);
			v_filename = fil.substring(pos2+1,fil.indexOf("~~", 0));			
			//if(fil != '') document.getElementById('docFrame').src = '${path}/common/util.htm?window=doc_bp&file='+v_filename+'&mid=' + msag;
			if(fil != '') document.getElementById('docFrame').src = 'https://ews.sinarmasmsiglife.co.id:8443/E-Lions/common/util.htm?window=doc_bp&file='+v_filename+'&mid=' + msag;
		}else{
			v_spaj = fil.substring(0, fil.indexOf("~", 0));
			v_filename = fil.substring(fil.indexOf("~", 0)+1);
			//if(fil != '') document.getElementById('docFrame').src = '${path}/common/util.htm?window=doc&file='+v_filename+'&spaj=' + v_spaj;
			if(fil != '') document.getElementById('docFrame').src = 'https://ews.sinarmasmsiglife.co.id:8443/E-Lions/common/util.htm?window=doc&file='+v_filename+'&spaj=' + v_spaj;
		}
	}
	
	function konfirmasi(){
		if(confirm('Hapus File '+document.formpost.dok.value+' ?')) {
			document.formpost.pass.value = prompt('Untuk menghapus file '+document.formpost.dok.value+' , harap masukkan password untuk menghapus!');
			return true;
		}else {
			return false;
		}
	}
	
	function PopMultiDoc(){
		var spaj = '${spaj}';
		popWin('${path}/common/util.htm?window=multi_doc_list&spaj='+spaj, 600, 900);
	}
</script>
<body onload="setFrameSize('docFrame', 45);" onresize="setFrameSize('docFrame', 45);" style="height: 100%;">
<form name="formpost" method="post">
<div class="tabcontent">
<table class="entry2" style="width: 100%;">
	<tr>
		<th>Pilih Dokumen : </th>
		<td>
			<select name="dok" id="dok" onChange="tampil(this.value);">
				<option value="">---=-=-=-=-=-= [ Pilih Dokumen ] =-=-=-=-=-=---</option>
				<optgroup label="Polis Utama (${spaj})">
				<c:forEach var="s" items="${daftarFile}">
					<option value="${spaj}~${s.key}">${s.key} [${s.value}]</option>
				</c:forEach>
				</optgroup>
				<c:if test="${not empty daftarFile2}">
					<optgroup label="Polis Surrender Endorsement (${spaj2})">
					<c:forEach var="s" items="${daftarFile2}">
						<option value="${spaj2}~${s.key}">${s.key} [${s.value}]</option>
					</c:forEach>
					</optgroup>
				</c:if>
				<c:if test="${not empty daftarFile3}">
					<optgroup label="Polis BP (${spaj3})">
					<c:forEach var="s" items="${daftarFile3}">
						<option value="${spaj3}~${s.key}~~${msag_id}">${s.key} [${s.value}]</option>
					</c:forEach>
					</optgroup>
				</c:if>
				<c:if test="${not empty daftarFile4}">
					<optgroup label="Surat Batal/Refund (${spaj4})">
					<c:forEach var="s" items="${daftarFile4}">
						<option value="${spaj4}~${s.key}">${s.key} [${s.value}]</option>
					</c:forEach>
					</optgroup>
				</c:if>
				<c:if test="${not empty daftarFile5}">
					<optgroup label="Perpanjangan Polis (${spaj5})">
					<c:forEach var="s" items="${daftarFile5}">
						<option value="${spaj5}~${s.key}">${s.key} [${s.value}]</option>
					</c:forEach>
					</optgroup>
				</c:if>
				<c:if test="${not empty daftarFile6}">
					<optgroup label="Surat Reinstatement Polis (${spaj6})">
					<c:forEach var="s" items="${daftarFile6}">
						<option value="${spaj6}~${s.key}">${s.key} [${s.value}]</option>
					</c:forEach>
					</optgroup>
				</c:if>
				<c:if test="${not empty daftarFile9}">
					<optgroup label="File Endorsment (${spaj9})">
					<c:forEach var="s" items="${daftarFile9}">
						<option value="${spaj9}~${s.key}">${s.key} [${s.value}]</option>
					</c:forEach>
					</optgroup>
				</c:if>
				
				<c:if test="${not empty daftarFileR1}">
					<optgroup label="File Worksheet (${spajR1})">
						<option value="${spajR1}~${daftarFileR1[0].key}">${daftarFileR1[0].key} [${daftarFileR1[0].value}]</option>
					</optgroup>
				</c:if>
				
			</select>
			<input type="hidden" name="msag_id" id="msag_id" value="${msag_id}">
		    <input type="submit" name="delete" value="X" onclick="return konfirmasi();">
		    <input type="button" name="multi_doc" id="multi_doc" value="Multi View" onclick="return PopMultiDoc();">
			<input type="hidden" name="pass">
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<iframe name="docFrame" src="" id="docFrame" width="100%"  height="100%"> Please Wait... </iframe>
		</td>
	</tr>
</table>
</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>