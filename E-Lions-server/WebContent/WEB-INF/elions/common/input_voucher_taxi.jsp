<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
	    <title>PT. Asuransi Jiwa Sinarmas MSIG</title>
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	    <meta name="Description" content="Ekalife">
	    <link rel="Stylesheet" type="text/css" href="${path}/include/css/default.css" media="screen">
	    <link rel="shortcut icon" href="${path}/include/image/eas.ico">
	    <!--[if IE]>
        <style>
            #voucherList {
	            height: expression( this.scrollHeight > 430 ? "430px" : "auto" ); /* sets max-height for IE */
	        }
        </style>
        <![endif]-->
	    <style>
	        body,legend, table.entry th,
	        table.entry td, .button {
	            font-size: 12px;
	        }
	        
	        input {
	            font-size: 10px;
	            padding-left: 10px;
	            height: 20px;
	        }
	        
	        .button {
	            padding-left: 10px;
	            padding-right: 10px;
	            cursor: pointer;
	        }
	        
	        .info {
	            font-size: 9px;
	        }
	        
	        .errBox {
	            border-color: #FF0000;
	        }
	        
	        .readOnlyErrBox {
	            background-color: #D0D0D0;
	            color: #FF0000;
	        }
	        
	        .errorDiv {
	            color: #FF0000;
	            font-size: 10px;
	            text-transform: capitalize;
	            height: 15px;
	        }
	        
	        table.entry th {
	            text-align: center;
	            vertical-align: middle;
	            padding-top: 5px;
	            padding-bottom: 5px;
	        }
	        
	        table#inputTbl.entry td {
	            padding-left: 10px;
	        }
	        
	        #voucherContainer {
	            height: 480px;
	            overflow: auto;
	        }
	        
	        #voucherList {
	            border-top: 1px solid #8A867A;
	            height: 425px;
	            overflow: auto;
	            text-align: center;
	            font-size: 11px;
	        }
	        
	        #voucherList ul {
	            list-style: none outside none;
	            margin: 0;
	            padding: 0;
	        }
	        
	        #voucherList ul li {
	            line-height: 20px;
	            border-right: 1px solid #8A867A;
	            border-left: 1px solid #8A867A;
	            border-bottom: 1px solid #8A867A;
	        }
	        
	        #voucherList ul li a {
	            text-decoration: none;
	            background-color: #ECE9D8;
	            color: #000000;
	            display: block;
                padding-top: 2px;
                padding-bottom: 2px;
	        }
	        
	        #voucherList ul li a:hover,
	        #voucherList ul li a:visited {
	            background-color: #C1D2EE;
	        }
	    </style>
	    <!-- DatePicker Script (jscalendar) -->
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
	    <script type="text/javascript" src="${path}/include/js/default.js"></script>
	    <script type="text/javascript">
	        hideLoadingMessage();
	        
	        function editVoucher(no) {
	            window.location = '${path}/common/voucher_taxi.htm?window=input&action=edit&no=' + no;
	        }
	        
	        function cariMarketing() {
	           var nama = document.getElementById('msvt_user_name').value;
	           var dept = document.getElementById('msvt_user_dept').value;
	           
	           popWin('${path}/common/voucher_taxi.htm?window=cariMarketing&nama=' + nama + '&dept=' + dept + '&filter=nama',300,510,'yes','no','no');
	        }
	        
	        function formatCurrency(num)
			{
			    num = num.toString().replace(/\,/g,'');
			    if(isNaN(num))
			        num = "0";
			    sign = (num == (num = Math.abs(num)));
			    num = Math.floor(num*100+0.50000000001);
			    cents = num%100;
			    num = Math.floor(num/100).toString();
			    if(cents<10)
			        cents = "0" + cents;
			    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
			        num = num.substring(0,num.length-(4*i+3))+','+
			              num.substring(num.length-(4*i+3));
			    return (((sign)?'':'-') + num + '.' + cents);
			}
			
			function changeFormat(elem) {
			    var val = document.getElementById(elem).value;
			    document.getElementById(elem).value = formatCurrency(val);
			}
			
			function checkStatus() {
			    var status = document.getElementById('msvt_status_flag').value;
			    var rowKet = document.getElementById('rowKeterangan');
			    if(status == 1)
		            rowKet.style.display = 'block';
			    else
			        rowKet.style.display = 'none';
			}
			
			function checkCharLength(elem) {
			    if(elem.value.length > 200) {
			        elem.className = 'errBox';
			        alert('Kolom keterangan tidak boleh lebih dari 200 karakter');
			    }
			}
	    </script>
	</head>
	<body>
	    <c:choose>
	        <c:when test="${empty action}">
	            <c:set var="postUrl" value="${path}/common/voucher_taxi.htm?window=input"/>
	        </c:when>
	        <c:otherwise>
	            <c:set var="postUrl" value="${path}/common/voucher_taxi.htm?window=input&action=edit"/>
	        </c:otherwise>
	    </c:choose>
	    <div id="contents">
	        <fieldset>
	            <legend>Input Voucher Taxi Marketing</legend>
	            <form:form id="inputForm" name="inputForm" commandName="command" method="POST" action="${postUrl}">
	                <table class="entry" style="width:100%;">
	                    <tr>
	                        <td style="width: 200px; vertical-align: top;">
	                            <div id="voucherContainer">
	                                No. Voucher:<br>
	                                <span class="info">* Klik pada no voucher untuk edit</span>
	                                <div id="voucherList">
	                                    <ul>
	                                        <c:if test="${empty listNoVoucher}">
	                                            <li>
	                                                <a href="javascript:void(0);"><span style="font-size: 10px;">Tidak ada.</span></a>
	                                            </li>
	                                        </c:if>
	                                        <c:forEach items="${listNoVoucher}" var="no">
	                                            <li>
	                                                <a href="javascript:void(0);" title="Edit" onclick="editVoucher('${no}');"><strong>${no}</strong></a>
	                                            </li>
	                                        </c:forEach>
	                                    </ul>
	                                    <div style="clear: both;"></div>
	                                </div>
	                                <div style="margin-top: 5px;">
	                                    <input type="button" id="reportBtn" class="button" value="Report" title="View Report Voucher Taxi"
	                                               onclick="popWin('${path}/common/voucher_taxi.htm?window=view',600,800,'no','no','no')">
	                                </div>
	                            </div>
	                        </td>
	                        <td style="vertical-align: top; padding-left: 20px;">
	                            <table id="inputTbl" class="entry" style="width:100%;">
	                                <tr>
	                                    <th style="width: 30%;">No. Voucher</th>
	                                    <td>
	                                        <c:choose>
	                                            <c:when test="${action eq 'edit'}">
	                                                <form:input path="msvt_no" size="40" readonly="readonly" cssClass="readOnly"/>
	                                            </c:when>
	                                            <c:otherwise>
	                                                <form:input path="msvt_no" size="40" cssErrorClass="errBox"/>
	                                            </c:otherwise>
	                                        </c:choose>
	                                        <div class="errorDiv">
	                                            <form:errors path="msvt_no"/>
	                                        </div>
	                                    </td>
	                                </tr>
	                                <tr>
                                        <th>Departemen</th>
                                        <td>
                                            <%-- <form:select path="msvt_user_dept" items="${deptList}" itemLabel="key" itemValue="value" 
                                                cssErrorClass="errBox" cssStyle="width: 250px"/> --%>
                                            <form:select path="msvt_user_dept" cssErrorClass="errBox" cssStyle="width: 250px">
                                                <c:forEach items="${deptList}" var="d">
                                                    <option value="${d.value}">${d.key}</option>
                                                </c:forEach>
                                            </form:select>
                                            <div class="errorDiv">
                                                <form:errors path="msvt_user_dept"/>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
	                                    <th>Kode Agen</th>
	                                    <td>
	                                        <form:input path="msvt_msag_id" size="30" readonly="readonly" cssClass="readOnly" cssErrorClass="readOnlyErrBox"/>
	                                        <div class="errorDiv">
	                                            <form:errors path="msvt_msag_id"/>
	                                        </div>
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <th>Nama Marketing</th>
	                                    <td>
	                                        <form:input path="msvt_user_name" size="50" cssErrorClass="errBox"
	                                            onkeypress="if(event.keyCode==13) {btnCari.click(); return false;}"/>
                                            <span style="margin-left: 10px;">
                                                <input type="button" title="Cari Marketing" id="btnCari" class="button" 
                                                    value="Cari" onclick="cariMarketing();">
                                            </span>
                                            <div class="errorDiv">
                                                <form:errors path="msvt_user_name"/>
                                            </div>
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <th>Status Voucher</th>
	                                    <td>
	                                        <form:select path="msvt_status_flag" items="${statusList}" itemLabel="value" itemValue="key"
	                                             cssStyle="width: 120px;" onchange="checkStatus();"/>
	                                        <div class="errorDiv">
	                                            <form:errors path="msvt_status_flag"/>
	                                        </div>
	                                    </td>
	                                </tr>
	                                <tr id="rowKeterangan"<c:if test="${command.msvt_status_flag eq 0}"> style="display: none;"</c:if>>
	                                    <th>Keterangan</th>
	                                    <td>
	                                        <form:textarea path="msvt_status_desc" cssStyle="width: 400px; height: 100px;" onblur="checkCharLength(this);" cssErrorClass="errBox"/>
	                                        <div class="errorDiv">
	                                            <form:errors path="msvt_status_desc"/>
	                                        </div>
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <th>Dari</th>
	                                    <td>
	                                        <form:input path="msvt_from" size="50" cssErrorClass="errBox"/>
	                                        <div class="errorDiv">
	                                            <form:errors path="msvt_from"/>
	                                        </div>
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <th>Ke</th>
	                                    <td>
	                                        <form:input path="msvt_to" size="50" cssErrorClass="errBox"/>
	                                        <div class="errorDiv">
	                                            <form:errors path="msvt_to"/>
	                                        </div>
	                                    </td>
	                                </tr>
                                    <tr>
                                        <th>Tanggal Berangkat</th>
                                        <td>
                                            <%-- <input type="text" size="10" name="msvt_release_date" 
                                                value="<fmt:formatDate pattern="dd/MM/yyyy" value="${voucherTaxi.msvt_release_date}"/>"> --%>
                                            <script>
                                                inputDate(
                                                    'msvt_release_date',
                                                    '<fmt:formatDate pattern="dd/MM/yyyy" value="${command.msvt_release_date}"/>',
                                                    false
                                                );
                                            </script>
                                            <div class="errorDiv">
                                                <form:errors path="msvt_release_date"/>
                                            </div>
                                        </td>
                                    </tr>
	                                <tr>
	                                    <th>Tanggal Kembali</th>
	                                    <td>
	                                        <%-- <input type="text" size="10" name="msvt_return_date" 
	                                            value="<fmt:formatDate pattern="dd/MM/yyyy" value="${voucherTaxi.msvt_return_date}"/>"> --%>
	                                        <script>
	                                            inputDate(
	                                                'msvt_return_date',
	                                                '<fmt:formatDate pattern="dd/MM/yyyy" value="${command.msvt_return_date}"/>',
	                                                false
	                                            );
	                                        </script>
	                                        <div class="errorDiv">
	                                            <form:errors path="msvt_return_date"/>
	                                        </div>
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <th>Total Biaya</th>
	                                    <td>
	                                        <!-- <form:input path="msvt_cost_formatted" size="50" cssErrorClass="errBox" onblur="changeFormat('msvt_cost_formatted');"/> -->
	                                        <input id="msvt_cost_formatted" name="msvt_cost_formatted" size="50" value="<fmt:formatNumber type="currency" currencySymbol="" maxFractionDigits="0" value="${command.msvt_cost}"/>">
	                                        <div class="errorDiv">
	                                            <form:errors path="msvt_cost_formatted"/>
	                                        </div>
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td></td>
	                                    <td>
	                                        <input type="submit" title="Save" class="button" name="submit" value="Save" onclick="return confirm('Save?');">
	                                        <c:if test="${action eq 'edit'}">
	                                            <input type="button" title="Cancel" class="button" value="Cancel" onclick="window.location='${path}/common/voucher_taxi.htm?window=input'">
	                                        </c:if>
	                                        <c:if test="${not empty err}">
		                                        <div id="error" style="font-size: 10px;">
                                                    ERROR:<br>
                                                    <c:forEach items="${err.allErrors}" var="e">
                                                        <c:if test="${e.code ne 'typeMismatch'}">
                                                            - ${e.defaultMessage}<br>
                                                        </c:if>
                                                    </c:forEach>
                                                </div>
		                                    </c:if>
		                                    <c:if test="${not empty success}">
		                                        <div id="success" style="font-size:10px;">
                                                    INFO:<br>
                                                    ${success}
                                                </div>
		                                    </c:if>
	                                    </td>
	                                </tr>
	                            </table>
	                        </td>
	                    </tr>
	                </table>
	            </form:form>
	        </fieldset>
	    </div>
	</body>
</html>