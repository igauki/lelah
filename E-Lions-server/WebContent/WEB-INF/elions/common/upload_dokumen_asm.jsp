<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path}/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path}/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script>
	hideLoadingMessage();
	
	function pesan(){
		var pesan = '${pesan}';
		if(pesan != '') alert(pesan);
	}
</script>
</head>
<BODY onload="setupPanes('container1', 'tab1'); pesan();" style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Upload File</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" style="text-align: center;" enctype="multipart/form-data">
			
					<table class="entry2">
						<tr>
							<td class="left">
								<fieldset>
									<legend>Blanko</legend>
									Cari No.Blanko : &nbsp; 
									<font style="font: bold;"> ASM </font>
									<input type="text" name="no_blanko" value="${no_blanko}">&nbsp;<input type="submit" name="show" value="Show">
									<c:if test="${not empty errorMessage}">
										<div id="error">
											Pesan:<br>
											- ${errorMessage}
										</div>
									</c:if>
								</fieldset>
							</td>
						</tr>
						<c:if test="${not empty dataBlanko }">
							<tr>
								<td>
									<fieldset>
										<legend>Daftar File yang akan Di-Upload</legend>
										<table class="displaytag" style="width: auto;" id="uploadTable">
										<thead>
											<tr>
												<th style="width: 20px;">No.</th>
												<th style="width: 200px;">Tipe Dokumen</th>
												<th style="width: 500px;">File yang di-Upload</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="i" begin="0" end="9" step="1" varStatus="st">
												<tr>
													<td>${st.count}.</td>
													<td>
														<select name="jns${i}">
															<option value=""></option>
															<c:forEach items="${daftarJenis}" var="c">
																<option value="${c.key}">[${c.desc}] ${c.value}</option>
															</c:forEach>
														</select>
													</td>
													<td><input type="file" name="daftarFile[${i}]" size="80"></td>
												</tr>
											</c:forEach>
										</tbody>
										</table>
										
										<div class="info">
											<br/>Catatan<br/>
										    - File yang diupload harus dalam bentuk pdf (.pdf)<br/>
											- Maksimum sekali upload = 10 file<br/>
											- Maksimum ukuran file = 500 KB per file<br/>
										</div>
										
									</fieldset>
									<fieldset>
										<legend>UPLOAD</legend>
										<input type="submit" name="upload" value="Upload" onclick="return confirm('Apakah anda sudah memastikan hal berikut?\n- Ukuran setiap file tidak melebihi 500 kb\n- Tipe Dokumen sudah dipilih untuk setiap file yang diupload');">
									</fieldset>							
								</td>
							</tr>
						</c:if>
					</table>			

				</form>
			</div>
		</div>
	</div>

</form>
</body>
<%@ include file="/include/page/footer.jsp"%>