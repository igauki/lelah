<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
    <head>
        <title>PT. Asuransi Jiwa Sinarmas MSIG</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <meta name="Description" content="Ekalife">
        <link rel="Stylesheet" type="text/css" href="${path}/include/css/default.css" media="screen">
        <link rel="shortcut icon" href="${path}/include/image/eas.ico">
        <style>
            #list td {
                background-color: transparent;
            }
        </style>
        <script type="text/javascript" src="${path}/include/js/default.js"></script>
        <script type="text/javascript">
            hideLoadingMessage();
            
            function backToParent(msag_id, nama) {
                var parentForm = self.opener.document.forms['inputForm'];
                if(parentForm) {
                    parentForm.elements['msvt_msag_id'].value = msag_id;
                    parentForm.elements['msvt_user_name'].value = nama;
                }
                window.close();
            }
            
            function highlightRows(tableId) {
                var table = document.getElementById(tableId);
                var tbody = table.getElementsByTagName('tbody')[0];
                var rows = tbody.getElementsByTagName('tr');
                var oldBg = '#FFFFFF';
                for(var i = 0; i < rows.length; i++) {
                    rows[i].onmouseover = function() {
                        oldBg = this.style.backgroundColor;
                        this.style.backgroundColor = '#EEEEEE';
                        this.style.cursor = 'pointer';
                    };
                    
                    rows[i].onmouseout = function() {
                        this.style.backgroundColor = oldBg;
                        this.style.cursor = '';
                    };
                    
                    rows[i].onclick = function() {
                        var msag_id = this.getElementsByTagName('td')[1].innerHTML;
                        var nama = this.getElementsByTagName('td')[2].innerHTML;
                        backToParent(msag_id,nama);
                    };
                }
            }
        </script>
    </head>
    <body onload="document.formCari.nama.focus();">
        <div id="contents">
            <fieldset>
                <legend>Cari Marketing</legend>
                <form name="formCari" method="POST" action="${path}/common/voucher_taxi.htm?window=cariMarketing">
                    <table class="entry" style="width:100%;">
                        <tr>
                            <th>
                                <select name="dept">
                                    <c:forEach items="${deptList}" var="d">
                                        <option value="${d.value}"<c:if test="${dept eq d.value}"> selected</c:if>>${d.key}</option>
                                    </c:forEach>
                                </select>
                                <select name="filter">
                                    <option value="nama"<c:if test="${filter eq 'nama'}"> selected</c:if>>NAMA</option>
                                    <option value="kode"<c:if test="${filter eq 'kode'}"> selected</c:if>>KODE AGEN</option>
                                </select>
                                <input type="text" name="nama" value="${nama}" onkeypress="if(event.keyCode==13) {btnCari.click(); return false;} else if(event.keyCode==27) {window.close();}">
		                        <input type="submit" title="Cari" class="button" value="Cari" name="btnCari">
		                        
		                        <input type="button" title="Tutup" class="button" value="Tutup" onclick="window.close()">
                            </th>
                        </tr>
                        <tr>
		                    <td>
		                        <div>
		                            <table id="list" class="entry" style="width: 100%;">
	                                    <tr>
	                                        <th>No.</th>
	                                        <th>Kode Agen</th>
	                                        <th>Nama Marketing</th>
	                                    </tr>
	                                    <c:set var="bgColor" value="#FFFFCC"/>
	                                    <c:forEach items="${listMarketing}" var="lm" varStatus="st">
	                                        <c:choose>
	                                            <c:when test="${bgColor eq '#FFFFCC'}">
	                                                <c:set var="bgColor" value="#FFFFFF"/>
	                                            </c:when>
	                                            <c:otherwise>
	                                                <c:set var="bgColor" value="#FFFFCC"/>
	                                            </c:otherwise>
	                                        </c:choose>
	                                        <tr style="background-color: ${bgColor};" title="${lm.MCL_FIRST}">
	                                            <td>${st.count}.</td>
	                                            <td>${lm.MSAG_ID}</td>
	                                            <td>${lm.MCL_FIRST}</td>
	                                        </tr>
	                                    </c:forEach>
	                                </table>
		                        </div>
                                <div class="info">
                                    * Klik untuk memilih agen<br/>
                                </div>
		                    </td>
		                </tr>
                    </table>
                    <script>highlightRows('list');</script>
                </form>
            </fieldset>
        </div>
    </body>
</html>