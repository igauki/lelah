<%@ include file="/include/page/header.jsp"%>
<style type="text/css">
	html,body{
	     overflow: hidden;
	}
</style>
<script>
	function apus(hal){
		if(document.formpost.passs){
			var pass = document.formpost.passs.value;
			if(pass){
				if(pass != ''){
					window.location = hal + '&pass=' + pass;
					return false;
				}
			}
		}
		alert('Maaf tetapi anda tidak mempunyai otorisasi untuk hal ini. Terima kasih.');
	}
</script>
<body style="height: 100%;" onload="setupPanes('container1', 'tab1'); ">
	<div class="tab-container" id="container1">
		
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Daftar Dokumen SPAJ ${spaj}</a>
			</li>
		</ul>

		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form method="post" name="formpost">
					<table class="entry2">
						<c:choose>
							<c:when test="${empty error}">
								<tr>
									<th>SPAJ</th>
									<td>${spaj}</td>
								</tr>
								<c:if test="${from eq 'java'}">
								<tr>
									<th>Password</th>
									<td><input type="password" name="passs" /></td>
								</tr>
								</c:if>
								<tr>
									<th>Dokumen</th>
									<td>
										<table class="displaytag" style="width: auto;">
											<tr>
												<th>Dokumen</th>
												<th>Tanggal</th>
												<th></th>
											</tr>
											<c:forEach var="dok" items="${daftarFile}">
												<tr>
													<td style="text-align: left;"><a href="${path}/common/util.htm?window=dokumen&spaj=${spaj}&file=${dok.key}">${dok.key}</a></td>
													<td style="text-align: center;">${dok.value}</td>
													<td>
														<c:if test="${from eq 'java'}">
															<c:if test="${dok.key eq \"tanda_terima.pdf\"}">
																<input type="button" value="Delete" onclick="apus('${path}/common/util.htm?window=dokumen&spaj=${spaj}&delete=${dok.key}');">
															</c:if>
															<c:if test="${dok.key eq \"sertifikat_powersave.pdf\"}">
																<input type="button" value="Delete" onclick="apus('${path}/common/util.htm?window=dokumen&spaj=${spaj}&delete=${dok.key}');">
															</c:if>
														</c:if>
													</td>
												</tr>
											</c:forEach>
										</table>
									</td>
								</tr>
							</c:when>
							<c:otherwise>
								<tr>
									<th>Error:</th>
									<td><div id="error">${error}</div></td>
								</tr>
							</c:otherwise>
						</c:choose>
					</table>
				</form>			
			</div>
		</div>
		
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>