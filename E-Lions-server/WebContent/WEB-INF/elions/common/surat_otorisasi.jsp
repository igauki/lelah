<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>

<body onload="setupPanes('container1','tab1');" style="height: 50%;width: 50%;" >
<script>
		function eventController(event){
			if(event == 'gotoPage'){
				document.getElementById('theEvent').value = 'gotoPage';
				document.getElementById('search').click();
			}else if(event == 'first'){
				document.getElementById('theEvent').value = 'first';
				document.getElementById('search').click();
			}else if(event == 'prev'){
				document.getElementById('theEvent').value = 'prev';
				document.getElementById('search').click();
			}else if(event == 'next'){
				document.getElementById('theEvent').value = 'next';
				document.getElementById('search').click();
			}else if(event == 'last'){
				document.getElementById('theEvent').value = 'last';
				document.getElementById('search').click();
			}
		}
		function change_bgcolor( obj, color )
        {
            obj.style.backgroundColor = color;
        }
	</script>
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Otorisasi menu SE/IM/SK </a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
<form:form id="formpost" commandName="jobstreetCompanyForm">
		<fieldset style="margin: 0px 7px 7px 7px; padding: 7px 7px 7px 7px; text-align: left;">
			<legend>
				List Surat
            </legend>
		<div id="pane1" class="entry2">
            <table style="width: 100%">
            	<tr>
                    <td style="text-align: right; font-family: sans-serif;">
                    	Jenis
                    </td>
                    <td>
                    	<select name="jenisSurat" id="jenisSurat"">
							<option value="" selected>ALL</option>
							<option value="1" >SK</option>
							<option value="2" >SE</option>
							<option value="3" >IM</option>
						</select> 
						<script type="text/javascript">document.getElementById('jenisSurat').value = '${jenisSurat}';</script>
                    </td>
                    <td style="text-align: right; font-family: sans-serif;">
                        Th.Update
                    </td>
                    <td >
                    	<input type="text" name="thUpdate" value="${thUpdate}" style="width: 35px;" maxlength="4"/>
                    	bln.
                    	<select name="lstBln" id="lstBln"">
							<option value="" selected>All</option>
							<option value="9" >9 bln. terakhir</option>
							<option value="6" >6 bln. terakhir</option>
							<option value="3" >3 bln. terakhir</option>
							<option value="1" >1 bln. terakhir</option>
						</select> 
						<script type="text/javascript">document.getElementById('lstBln').value = '${lstBln}';</script>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right; font-family: sans-serif;">
                    	No.
                    </td>
                    <td>
                    	<input type="text" name="noSurat" value="${noSurat}"/>
                    </td>
                    <td style="text-align: right; font-family: sans-serif;">
                        Judul
                    </td>
                    <td colspan="2">
                    	<input type="text" name="judulSurat" value="${judulSurat}" style="width: 200"/>
                    </td>
                </tr>
               <tr>
                    <td style="text-align: right; font-family: sans-serif;">
                    	Jml. Baris
                    </td>
                    <td>
                    	<input type="text" name="noRow" value="${noRow}" style="width: 30;" maxlength="3"/>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td style="text-align: left font-family: sans-serif;">
                    	<input type="submit" name="search" id="search" value="search"/>
                    	<input type="submit" name="save" id="save" value="save"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="7">
                        <hr/>
                    </td>
               </tr>
            </table>
            <table style="width: 100%">
                <tr style="background-color: burlywood;">
					<td style="width: 20%">Nomor</td>
					<td style="width: 50%">Judul</td>
					<td style="width: 10%">Last Update</td>
					<td style="width: 10%">Action</td>
					<td style="width: 10%">Akses AKM</td>
                </tr>
                <c:set var="backGroundColor" value="#EAEAEA"/>
                <c:forEach items="${daftarSurat}" var="x" varStatus="xt">

                    <c:choose>
                        <c:when test='${"#EAEAEA" == backGroundColor}'>
                            <c:set var="backGroundColor" value="#FFFFFF"/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="backGroundColor" value="#EAEAEA"/>
                        </c:otherwise>
                    </c:choose>
                    
                    <tr style="background-color: ${backGroundColor};"
                        onMouseOver="change_bgcolor( this, '#FFD6CA' );"
                        onMouseOut="change_bgcolor(this, '${backGroundColor}');">
                        <c:set var="disabledAddition" value=""/>
                        <td style="text-align: left; vertical-align: text-top;">
                                ${x.no_surat}
                                <input type="hidden" name="surat_id${xt.index}" value="${x.surat_id}"/>
                        </td>
                        <td style="text-align: left; vertical-align: text-top;">
                                ${x.jdl_surat}
                        </td>
                        <td style="text-align: center; vertical-align: text-top;">
                                <fmt:formatDate value="${x.tgl_edit}" pattern="dd/MM/yyyy hh:mm:ss"/>
                        </td>
                        <td style="text-align: center; vertical-align: text-top;">
                              <a href="${setUrlForPdf}${x.nm_file}" target="_blank">Download</a>
                        </td>
                        <td style="text-align: center; vertical-align: text-top;">
                        	<c:choose>
                        		<c:when test="${x.akm_flag eq '1'}">
                        			<input type="checkbox" name="yncheck_akm${xt.index}" checked="checked" value="1"/>
                        		</c:when>
                        		<c:otherwise>
                        			<input type="checkbox" name="yncheck_akm${xt.index}" value="1"/>
                        		</c:otherwise>
                        	</c:choose>
                        </td>
                    </tr>
                </c:forEach>
                <tr style="background-color: tan;">
                    <td>page ${currPage} of ${lastPage}</td>
                    <td colspan="1" style="text-align: right;">
                    	<a href="#" onclick="eventController('gotoPage');">goto page</a> <input type="text" name="goTo" style="width: 35"value="${currPage}" /> 
                    </td>
                    <td colspan="2" style="text-align: right;">
                        <a href="#" onclick="eventController('first');">first</a>
                        <a href="#" onclick="eventController('prev');">prev</a>
                        <a href="#" onclick="eventController('next');">next</a>
                        <a href="#" onclick="eventController('last');">last</a>
                    </td>
                    <td colspan="2" style="text-align: center;">
                    	<input type="submit" name="save" id="save" value="save"/>
                    </td>
                </tr>
            </table>
            </div>

            <spring:hasBindErrors name="command">
				<div class="errorBox">
					Harap lengkapi informasi berikut:<br/>
					<form:errors path="*" />
				</div>
			</spring:hasBindErrors>

            <br/>
            <c:forEach var="msg" items="${pageMessageList}" varStatus="xt">
                    <span style="vertical-align: text-top; color:brown;">
                            ${msg}
                    </span>
                <br/>
            </c:forEach>

        </fieldset>
        <input type="hidden" id="targetParam"/>
        <input type="hidden" name="currPage" id="currPage"/>
        <input type="hidden" name="lastPage" id="lastPage"/>
        <input type="hidden" name="theEvent" id="theEvent"/>
		<input type="hidden" name="noSuratTemp" id="noSuratTemp" value="${noSuratTemp}"/>
        <input type="hidden" name="judulSuratTemp" id="judulSuratTemp" value="${judulSuratTemp}"/>
        <input type="hidden" name="jenisSuratTemp" id="jenisSuratTemp" value="${jenisSuratTemp}"/>
        <input type="hidden" name="thUpdateTemp" id="thUpdateTemp" value="${thUpdateTemp}"/>
        <input type="hidden" name="lstBlnTemp" id="lstBlnTemp" value="${lstBlnTemp}"/>
    </form:form>

<script type="text/javascript">
	document.getElementById('currPage').value = '${currPage}';
	document.getElementById('lastPage').value = '${lastPage}';
    if( '${pageMessage}' != '' )
    {
        alert( '${pageMessage}' );
    }
</script>
<script type="text/javascript">
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		</script>
		<script type="text/javascript">
		try {
		var pageTracker = _gat._getTracker("UA-8107264-3");
		pageTracker._trackPageview();
		} catch(err) {}</script>	
</div>
		</div>
	</div>
	
</body>

<%@ include file="/include/page/footer.jsp"%>