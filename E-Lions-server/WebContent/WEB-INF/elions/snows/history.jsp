<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />

<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->

<body>
<input type="hidden" id="mi_id" name="mi_id" value="${mi_id}">
	<table style="width: 100%;">
		<tr>
			<th>Created Date</th>
			<th>Pre Position</th>
			<th>Current Position</th>
			<th>DESCRIPTION</th>
		</tr>
		<c:forEach items="${hist}" var="h">
			<tr style="text-align: center;">
				<td>${h.CREATE_DATE }</td>
				<td>${h.BEFORE}</td>
				<td>${h.AFTER}</td>
				<td style="text-align: left;">${h.KET}</td>
			</tr>
		</c:forEach>
	</table>
</body>
<%@ include file="/include/page/footer.jsp"%>
