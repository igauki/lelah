<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />

<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->

<script type="text/javascript">
$().ready(function() {
	$('#dok').change(function(){
		var file = $(this).val();
		if(file!='') $('#docFrame').attr('src','${path}/snows/snows.htm?window=load_viewer&file='+file+'&lca_id='+$('#lca_id').val()+'&spaj='+$('#spaj').val()+'&lstb_id='+$('#lstb_id').val()+'&mi_id='+$('#mi_id').val());
	});
});

	/* function tampil(fil,frame){
		var msag = document.getElementById('msag_id').value;
		pos = fil.indexOf("~~", 0);
		
		if(pos>0){
			v_mid = fil.substring(0, fil.indexOf("~", 0));
			pos2 = fil.indexOf("~", 0);
			v_filename = fil.substring(pos2+1,fil.indexOf("~~", 0));		
			if(fil != '') document.getElementById('docFrame'+frame).src = '${path}/snows/snows.htm?window=doc_bp&file='+v_filename+'&mid=' + msag;
		}else{
			v_spaj = fil.substring(0, fil.indexOf("~", 0));
			v_filename = fil.substring(fil.indexOf("~", 0)+1);
			if(fil != '') document.getElementById('docFrame'+frame).src = '${path}/snows/snows.htm?window=doc&file='+v_filename+'&spaj=' + v_spaj;
		}
	} */
// hideLoadingMessage();
</script>
<body>
<form name="formpost" method="post">
	<div class="tabcontent">
	<input type="hidden" name="lca_id" id="lca_id" value="${lca_id}">
	<input type="hidden" name="spaj" id="spaj" value="${spaj}">
	<input type="hidden" name="mi_id" id="mi_id" value="${mi_id}">
	<input type="hidden" name="lstb_id" id="lstb_id" value="${lstb_id}">
	<table class="entry2" style="width:100%;">
		<tr>
			<th>
				<table style="width:100%;">
					<tr>
						<th>Pilih Dokumen : </th>
						<td>
							<select name="dok" id="dok">
								<option value="">---=-=-=-=-=-= [ Pilih Dokumen ] =-=-=-=-=-=---</option>
								<c:forEach var="s" items="${daftarFile}">
									<option value="${s.key}">${s.key} [${s.value}]</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<iframe name="docFrame" src="" id="docFrame" width="100%" height="600px"> Please Wait... </iframe>
						</td>
					</tr>
				</table>
			</th>
			<%-- <c:forEach var="s"  items="${perulangan}" varStatus="xt">
				<th>
					<table>
						<tr>
							<th>Pilih Dokumen : </th>
							<td>
								<select name="dok" id="dok" onChange="tampil(this.value,${xt.index+1});">
									<option value="">---=-=-=-=-=-= [ Pilih Dokumen ] =-=-=-=-=-=---</option>
									<c:forEach var="s" items="${daftarFile}">
										<option value="${s.key}">${s.key} [${s.value}]</option>
									</c:forEach>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<iframe name="docFrame${xt.index+1}" src="" id="docFrame${xt.index+1}" width="100%"  height="100%"> Please Wait... </iframe>
							</td>
						</tr>
					</table>
				</th>
			</c:forEach> --%>
		</tr>
	</table>
	</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>