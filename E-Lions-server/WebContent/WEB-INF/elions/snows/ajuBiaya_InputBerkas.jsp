<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>
<%@ page import="com.ekalife.elions.model.User"%>
<%User currentUser = (User) session.getAttribute("currentUser");%>
<c:set var="path" value="${pageContext.request.contextPath}" />

<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->

<script type="text/javascript">



$().ready(function() {
	var pesan = '${pesan}';
	if(pesan != '') alert(pesan);
		
	$("#tabs").tabs();

	
	$('#btnNew').click(function(){	
		
			$("#cabanglist1").removeAttr('disabled');
			$("#reimbes").removeAttr('disabled');
			$("#ca").removeAttr('disabled');
			$("#sdp").removeAttr('disabled');
			$("#atk").removeAttr('disabled');
			
			$("#ca").attr('checked','checked');	
			
			document.getElementById('tbTrans').style.visibility = 'visible';
			document.getElementById('divTrans').style.visibility = 'visible';
			
			document.getElementById('btnCancel').style.visibility = 'visible';
			document.getElementById('btnSave').style.visibility = 'hidden';
			document.getElementById('btnCheck').style.visibility = 'visible';
			
			$('.listdetail_1' ).remove();
			$('.listdetail_2' ).remove();
			$('.listdetail_3' ).remove();
			$("#countnumber").val(0);
			$("#tglKirimBerkas").val("");
			
			
			var url = '${path}/snows/snows.htm?window=ajax&pages=inputBerkasAjuBiaya&jn=newIdBerkas';
		//	window.location.href = '${path}/snows/snows.htm?window=inputBerkasAjuBiaya';	
			
			$.getJSON(url, function(result) {
				
				$("#NoBerkas").val(result.mi_id);
				$("#tgl_berkas_masuk").val(result.hariIni);
			});
		
			$("#btnNew").attr('disabled','disabled');		
			$("#btnEdit").attr('disabled','disabled');
			$("#btnDel").attr('disabled','disabled');		
			$("#btnUpl").attr('disabled','disabled');
			$("#btnTrans2").attr('disabled','disabled');
			$("#btnPrint").attr('disabled','disabled');
			$("#btnView").attr('disabled','disabled');
			$("#btnTglKirim").attr('disabled','disabled');
			
			
			$("#namaBank").removeAttr('readonly');
			$("#noRek").removeAttr('readonly');
			$("#atasNamaRek").removeAttr('readonly');
			
			$("#btnadd").removeAttr('disabled');
			$("#btndel1").removeAttr('disabled');
			
			
			$('#tgl_berkas_masuk').val("");
			$('#totalBiaya').val("");
			$('#namaBank').val("");
			$('#noRek').val("");
			$('#atasNamaRek').val("");
			
			
			
			$('.check2_' ).remove();
			document.getElementById('btnadd').style.visibility = 'visible';
			document.getElementById('btndel1').style.visibility = 'visible';
						
		
					
	});

	
	$('#btnEdit').click(function (){
		
		var NO_BERKAS = document.getElementById("NoBerkas").value ;
		
		$('.listdetail_1' ).remove();
		$('.listdetail_2' ).remove();
		$('.listdetail_3' ).remove();
		
		document.getElementById('tbTrans').style.visibility = 'visible';
		document.getElementById('divTrans').style.visibility = 'visible';
		
		document.getElementById('btnCancel2').style.visibility = 'visible';
		document.getElementById('btnSave2').style.visibility = 'hidden';
		document.getElementById('btnCheck2').style.visibility = 'visible';
		
		$("#btnNew").attr('disabled','disabled');
		$("#btnEdit").attr('disabled','disabled');
		$("#btnDel").attr('disabled','disabled');		
		$("#btnUpl").attr('disabled','disabled');
		$("#btnTrans2").attr('disabled','disabled');
		$("#btnPrint").attr('disabled','disabled');
		$("#btnView").attr('disabled','disabled');
		$("#btnTglKirim").attr('disabled','disabled');
		
		
		document.getElementById('btnadd2').style.visibility = 'visible';
		document.getElementById('btndel2').style.visibility = 'visible';
		
		var url = '${path}/snows/snows.htm?window=ajax&pages=inputBerkasAjuBiaya&jn=cekIDBerkas&mi_id='+NO_BERKAS;
		
		$('.tgl').attr('enabled : enabled');
		
		$.getJSON(url, function(result) {
						
			$("#countnumber").val(result.countnumber);					
						
			$("#reimbes").attr('disabled','disabled');
			$("#ca").attr('disabled','disabled');
			$("#sdp").attr('disabled','disabled');
			$("#atk").attr('disabled','disabled');
			
			$("#namaBank").removeAttr('readonly');
			$("#noRek").removeAttr('readonly');
			$("#atasNamaRek").removeAttr('readonly');						
									
			$.each(result.dataDetailItem,function(id,val){
			$("#totalBiaya").val(result.JumlahTtlBiaya);

				$('<tr class="listdetail_2" >').append(				
				
					$('<td>').html('<input type="text" value='+val.no_urut+' name="noUrut2'+val.no_urut+'" id="noUrut2'+val.no_urut+'" readonly="readonly" style="width: 20px;" size="2" maxlength="2" readOnly>'),
					$('<td>').html('<input type="text" value="'+val.MID_DESC+'" name="name_item2'+val.no_urut+'" id="name_item2'+val.no_urut+'"  style="width: 300px;">'),
					$('<td>').html('<input type="text" value='+val.KUANTITAS+' name="qty2'+val.no_urut+'" id="qty2'+val.no_urut+'" class="qty3" onkeypress="return isNumberKey(event)" style="width: 60px;">'),
				//	$('<td>').html('<input type="text" value="'+val.AB_TYPE+'" name="jenis${status.index +1}" readonly="readonly" style="width: 200px;">'),
					$('<td>').html('<select class="jenis2"   name="jenis2'+val.no_urut+'"  id="jenis2'+val.no_urut+'" title="Pilih Salah satu"  style="width: 200px;"><option value="'+val.AB_TYPE+'">'+val.AB_TYPE+'</option></select>'),
					$('<td>').html('<input type="text" value="'+val.TGL_GUNA+'" name="tgl_pemakaian2'+val.no_urut+'"  id="tgl_pemakaian2'+val.no_urut+'" class="tgl1"  style="width: 120px;">'),
					$('<td>').html('<input type="text" value='+val.MID_NOMINAL+'  name="biaya2'+val.no_urut+'" id="biaya2'+val.no_urut+'" class="biaya3" onkeypress="return isNumberKey(event)" style="width: 100px;">'),
					$('<td>').html('<input type="textarea" value="'+val.MID_KET+'" name="ket2'+val.no_urut+'" id="ket2'+val.no_urut+'"   style="width: 300px;">')
				).appendTo('#tableProd');
			});
			
			if(result.dataMstInbox[0].ALOKASI_KODE == 59){
				$("#reimbes").attr('checked', 'checked');	
				$(".qty3").attr('disabled','disabled');								
				$(".biaya3").removeAttr('disabled');
				$("#namaBank").removeAttr('disabled');
				$("#noRek").removeAttr('disabled');
				$("#atasNamaRek").removeAttr('disabled');
				$.each(result.alokasiReimburse,function(id, val){					
				$('select[class="jenis2"]').append('<option id="'+val+'" value="'+val+'">'+val+'</option>');
				});				
			}else if (result.dataMstInbox[0].ALOKASI_KODE == 60) {
				$("#ca").attr('checked', 'checked');
				$(".qty3").attr('disabled','disabled');								
				$(".biaya3").removeAttr('disabled');
				$("#namaBank").removeAttr('disabled');
				$("#noRek").removeAttr('disabled');
				$("#atasNamaRek").removeAttr('disabled');
				$.each(result.alokasiCA,function(id, val){					
				$('select[class="jenis2"]').append('<option id="'+val+'" value="'+val+'">'+val+'</option>');
				});
			}else if (result.dataMstInbox[0].ALOKASI_KODE == 69) {
				$("#sdp").attr('checked', 'checked');
				$(".qty3").attr('disabled','disabled');								
				$(".biaya3").removeAttr('disabled');
				$("#namaBank").removeAttr('disabled');
				$("#noRek").removeAttr('disabled');
				$("#atasNamaRek").removeAttr('disabled');
				$.each(result.alokasiSDP,function(id, val){					
				$('select[class="jenis2"]').append('<option id="'+val+'" value="'+val+'">'+val+'</option>');
				});
			}else if (result.dataMstInbox[0].ALOKASI_KODE == 68) {
				$("#atk").attr('checked', 'checked');	
				$(".biaya3").attr('disabled','disabled');								
				$(".qty3").removeAttr('disabled');
				$("#namaBank").attr('disabled','disabled');	
				$("#noRek").attr('disabled','disabled');	
				$("#atasNamaRek").attr('disabled','disabled');	
				$.each(result.alokasiATK,function(id, val){					
				$('select[class="jenis2"]').append('<option id="'+val+'" value="'+val+'">'+val+'</option>');
				});
			}
			
			$(".tgl1").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: "dd/mm/yy" 
			});	
			
		});
		
		
		
	});
	
	$('#btnSave').click(function(){
		var nu = document.getElementById("countnumber").value;	
		var piluk = document.getElementById("pilihalokasi").value;	
		var cabang = document.getElementById("cabanglist1").value;
		
		//var cabang = document.getElementById("cabanglist1").value;	
		//var tr = $('#tableProd tbody tr:last-child');		
		idx =  parseInt(nu);
		pil =  parseInt(piluk);
		var ni = 0;
		var gagal = "";
		
		if(nu == 0){
			alert("Mohon Mengisi Data Detail Keperluan Terlebih Dahulu");
			alert(cabang);
			gagal = 1;
		}
		
	//	alert("Please wait until current program is finished saving or being changed !");
		alert("Mohon menunggu sampai proses data berhasil di tambahkan atau di perbaharui!");
				
		if (idx > 0){			
					for (var i = 1; i <= idx; i++) { 
							var nama_item = document.getElementById("name_item2"+i).value;
							var qty = document.getElementById("qty2"+i).value;
							var jenis = document.getElementById("jenis2"+i).value;
							var tg = document.getElementById("tg2"+i).value;
							var bl = document.getElementById("bl2"+i).value;
							var th = document.getElementById("th2"+i).value;
							var biaya = document.getElementById("biaya2"+i).value;
							
							
							if(nama_item == ""){
								alert("Detail Per item no "+ i+", tidak boleh kosong ");  				 		
								gagal = 1;
							}	
							if(tg == "" || bl == ""  || th == ""){
								alert("Tanggal awal pemakaian no "+ i+", tidak boleh kosong ");  	
				    			gagal = 1;
							}
							if(bl > 12){
								alert("[ Tanggal awal pemakaian no "+ i+" ] - Mohon periksa kembali penginputan bulan");  	
				    			gagal = 1;
							}
							if(jenis == ""){
								alert("Jenis Keperluan belum di pilih");  	
				    			gagal = 1;
							}
							
							if(pil == 68 && qty == ""){
								alert("[Kuantitas no "+ i+" ]  - Untuk Alokasi ATK Kantor harus menginput Kuantitas");  	
				    			gagal = 1;
							}	
							
							if(pil != 68 && biaya == ""){
								alert("[Biaya no "+ i+" ]  - Untuk ALokasi Cash Advance / Sewa dan Pembayaran PO / Lain-lain harus menginput Biaya");  	
				    			gagal = 1;
							}
						
					}
			
			if (gagal == 1){				
				return false;
				}
			
		}

		
	
	});
	
	$('#btnSave2').click(function(){
		//alert("Please wait until current program is finished saving or being changed !");
		alert("Mohon menunggu sampai proses data berhasil di tambahkan atau di perbaharui!");
	});
			
	$('#btnUpl').click(function(){
		var NO_BERKAS = document.getElementById("NoBerkas").value ;

		popWin('${path}/snows/snows.htm?window=upload_ajuanBiaya&NoBerkas='+NO_BERKAS);
	
	});
	

	$('#btnView').click(function(){
		window.open('${path}/snows/snows.htm?window=viewer_ajuanBiaya&NoBerkas='+$('#NoBerkas').val(),'winView','width=1200, height=650');
	});
	
	// (jQueryUI datepicker) init semua field datepicker
	$(".tgl").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: "dd/mm/yy" 
	});
	
	
	$("#atk").click(function(){			  					
			document.getElementById('tbTrans').style.visibility = 'hidden';
			document.getElementById('divTrans').style.visibility = 'hidden';
			$('.listdetail_1' ).remove();
			$('.listdetail_2' ).remove();
			$('.listdetail_3' ).remove();
			document.getElementById('btnSave').style.visibility = 'hidden';
			$("#countnumber").val(0);
	});
			   
   $("#ca").click(function(){
 		 	document.getElementById('tbTrans').style.visibility = 'visible';
			document.getElementById('divTrans').style.visibility = 'visible';
			$('.listdetail_1' ).remove();
			$('.listdetail_2' ).remove();
			$('.listdetail_3' ).remove();
			document.getElementById('btnSave').style.visibility = 'hidden';
			$("#countnumber").val(0);
   });
   
   $("#sdp").click(function(){
 		 	document.getElementById('tbTrans').style.visibility = 'visible';
			document.getElementById('divTrans').style.visibility = 'visible';
			$('.listdetail_1' ).remove();
			$('.listdetail_2' ).remove();
			$('.listdetail_3' ).remove();
			document.getElementById('btnSave').style.visibility = 'hidden';
			$("#countnumber").val(0);
   });
   
   $("#reimbes").click(function(){
 		 	document.getElementById('tbTrans').style.visibility = 'visible';
			document.getElementById('divTrans').style.visibility = 'visible';
			$('.listdetail_1' ).remove();
			$('.listdetail_2' ).remove();
			$('.listdetail_3' ).remove();
			document.getElementById('btnSave').style.visibility = 'hidden';
			$("#countnumber").val(0);
   });

// 	$("form").submit(function(e){
// 	  e.preventDefault();
// 	});
	
	$(".dataInbox").click(function(){
		
		
		$( "#success" ).empty();
		var NO_BERKAS = $(this).attr('id');
		var url = '${path}/snows/snows.htm?window=ajax&pages=inputBerkasAjuBiaya&jn=cekIDBerkas&mi_id='+NO_BERKAS;
			
		document.getElementById('btnadd').style.visibility = 'hidden';
		document.getElementById('btndel1').style.visibility = 'hidden';
		document.getElementById('btnadd2').style.visibility = 'hidden';
		document.getElementById('btndel2').style.visibility = 'hidden';
		document.getElementById('tgl_berkas_masuk').disabled = true;
		document.getElementById('namaBank').readonly = 'readonly';
		document.getElementById('noRek').readonly = 'readonly';
		document.getElementById('atasNamaRek').readonly = 'readonly';
		
		document.getElementById('tbTrans').style.visibility = 'visible';
		document.getElementById('divTrans').style.visibility = 'visible';
		
		document.getElementById('btnCancel').style.visibility = 'hidden';
		document.getElementById('btnSave').style.visibility = 'hidden';
		document.getElementById('btnCheck').style.visibility = 'hidden';
		
		document.getElementById('btnCancel2').style.visibility = 'hidden';
		document.getElementById('btnSave2').style.visibility = 'hidden';
		document.getElementById('btnCheck2').style.visibility = 'hidden';
		
		$("#reimbes").attr('disabled','disabled');
		$("#ca").attr('disabled','disabled');
		$("#sdp").attr('disabled','disabled');
		$("#atk").attr('disabled','disabled');
						
		$("#btnNew").removeAttr('disabled');
		$("#btnEdit").removeAttr('disabled');
		$("#btnDel").removeAttr('disabled');
		$("#btnUpl").removeAttr('disabled');
		$("#btnTrans2").removeAttr('disabled');
		$("#btnPrint").removeAttr('disabled');
		$("#btnView").removeAttr('disabled');
		$("#btnTglKirim").removeAttr('disabled');
		$("#txtCari").val('');
		
		
		$('.listdetail_1' ).remove();
		$('.listdetail_2' ).remove();
		$('.listdetail_3' ).remove();
		//$('#tbChecklist').empty();
		$.getJSON(url, function(result) {
			
			$("#NoBerkas").val(result.dataMstInbox[0].NO_BERKAS);
			$("#tgl_berkas_masuk").val(result.dataMstInbox[0].TGL_AJUAN);								
			$("#namaBank").val(result.dataMstInbox[0].NAMA_BANK);	
			$("#noRek").val(result.dataMstInbox[0].MRC_NO_AC);	
			$("#atasNamaRek").val(result.dataMstInbox[0].MRC_ATAS_NAMA);	
			$("#alokasiBiaya").val(result.dataMstInbox[0].ALOKASI_KODE);
			$("#cabanglist1").val(result.dataMstInbox[0].MRC_KOTA);
			$("#tglKirimBerkas").val(result.dataMstInbox[0].TGL_KIRIM_BERKAS);
			$("#pilihalokasi").val(result.dataMstInbox[0].ALOKASI_KODE);
			
			if(result.dataMstInbox[0].ALOKASI_KODE == 59){
			$("#reimbes").attr('checked', 'checked');
			}else if (result.dataMstInbox[0].ALOKASI_KODE == 60) {
			$("#ca").attr('checked', 'checked');
			}else if (result.dataMstInbox[0].ALOKASI_KODE == 69) {
			$("#sdp").attr('checked', 'checked');
			}else if (result.dataMstInbox[0].ALOKASI_KODE == 68) {
			$("#atk").attr('checked', 'checked');
			}
			
			
			$.each(result.dataDetailItem,function(id,val){
			$("#totalBiaya").val(result.JumlahTtlBiaya);

				$('<tr class="listdetail_1" >').append(
				
					$('<td>').html('<input type="text" value='+val.no_urut+' id="noUrut" name="noUrut${status.index +1}" readonly="readonly" style="width: 20px;" size="2" maxlength="2" readOnly>'),
					$('<td>').html('<input type="text" value="'+val.MID_DESC+'" id="name_item" name="name_item${status.index +1}" readonly="readonly" style="width: 300px;">'),
					$('<td>').html('<input type="text" value='+val.KUANTITAS+' id="qty" name="qty${status.index +1}" readonly="readonly" style="width: 60px;">'),
					$('<td>').html('<input type="text" value="'+val.AB_TYPE+'" id="jenis" name="jenis${status.index +1}" readonly="readonly" style="width: 200px;">'),
					$('<td>').html('<input type="text" value="'+val.TGL_GUNA+'" id="tgl_pemakaian"  name="tgl_pemakaian${status.index +1}" class="tgl" readonly="readonly" style="width: 120px;">'),
					$('<td>').html('<input type="text" onkeypress="return isNumberKey(event)" value='+val.MID_NOMINAL+' id="biaya" name="biaya${status.index +1}" readonly="readonly" style="width: 100px;">'),
					$('<td>').html('<input type="textarea" value="'+val.MID_KET+'" id="ket"  name="ket${status.index +1}" readonly="readonly" style="width: 300px;">')
				).appendTo('#tableProd');
			});
			
		});
		
		$("#cabanglist1").attr('disabled','disabled');
		
		
	});
	
	$('#btnSearc').click(function (){
		
		var noReg = document.getElementById("noReg").value ;
		
		$('.listBiaya' ).remove();
		$('.listBiayaS' ).remove();
		//alert(noReg);
		
		var url = '${path}/snows/snows.htm?window=ajax&pages=inputBerkasAjuBiaya&jn=searchIDBerkas&mi_id='+noReg;
		
			$.getJSON(url, function(result) {
			
			$.each(result.dataHistory,function(id,val){			
	
				$('<tr class="listBiayaS" id="listBiayaS">').append(				
					$('<td style="background-color: #E0E0E0;">').html('<input type="text" id="noms" name="noms}"size="4" maxlength="4" readOnly style="width: 20px;">'),
					$('<td style="background-color: #E0E0E0;">').html('<input type="text" name="NoBels" id="NoBels" value="'+val.MI_ID+'" readOnly style="width: 100px;">'),
					$('<td style="background-color: #E0E0E0;">').html('<input type="text" name="tglTerbentukls" id="tglTerbentukls" value="'+val.CREATE_DATE+'" readOnly style="width: 150px;" >'),
					$('<td style="background-color: #E0E0E0;">').html('<input type="text" name="posDokLs" id="posDokLs" value="'+val.LSPD_POSITION+'" readOnly style="width: 150px;">'),
					$('<td style="background-color: #E0E0E0;">').html('<input type="text" name="userls" id="userls" value="'+val.LUS_FULL_NAME+'" readOnly style="width: 150px;">'),
					$('<td style="background-color: #E0E0E0;">').html('<input type="text" name="descls" id="descls" value="'+val.MI_DESC+'" readOnly style="width: 600px; display:inline-block;" >')
					).appendTo('#tableHist');
			});
			
		});
		
		
	});
	
});

function dialog(path){
	$(function(){
		$('.dialog').empty();
		var img = $('<img src="'+path+'" width="600px" />').appendTo('.dialog');
		$(".dialog").dialog({
			width: 640,
			modal: true
		});
	});
}

function checkTgl(target, tgl){
// 	$("#"+target+"").datepicker({ dateFormat: 'd/M/yy' });
	if(tgl == undefined){
		$("#"+target+"").datepicker( "setDate", "");
	}else{
		$("#"+target+"").datepicker( "setDate", tgl);
	}
}

function jumlahbiaya(){
	var a = document.getElementById("biaya2").value ;
	document.getElementById("totalBiaya").value = a ;
}

function copyvalue(asal,target){
	alert(asal);
	$(target).val(asal);
}

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}


function addRowDOM1x (tableID, jml) {
		//untuk mengambil value radiobutton yang di checked	
		var selectedVal = "";
		var selected = $("input[type='radio'][name='alokasiBiaya']:checked");
	
		if (selected.length > 0) {
		    selectedVal = selected.val();
		}
		
		var nu = document.getElementById("countnumber").value;	
		var piluk = document.getElementById("pilihalokasi").value;	
		//var tr = $('#tableProd tbody tr:last-child');		
		idx =  parseInt(nu)+ 1;
		pil =  parseInt(piluk);
	//	alert(selectedVal);
		
			
		//samakan apakah pilihan alokasi sebelumnya sama dengan field yang mau di tambahkan jika tidak hapus data list itemnya dan set number dari 1
		if (pil != selectedVal ){
			$('.listdetail_1' ).remove();
			$('.listdetail_2' ).remove();
			$('.listdetail_3' ).remove();
			idx = 1;
		}
		
		
			var url = '${path}/snows/snows.htm?window=ajax&pages=inputBerkasAjuBiaya&jn=addRowtabel&alokasi='+selectedVal;
		
			$.getJSON(url, function(result) {
			
					if (selectedVal ==  59){
							$.each(result.alokasiReimburse,function(id, val){					
						 		$('select[id="jenis2'+idx+'"]').append('<option id="'+val+'" value="'+val+'">'+val+'</option>');
								});
							$(".qty2").attr('disabled','disabled');	
							$(".biaya2").removeAttr('disabled');						
					}else if (selectedVal ==  60){
							$.each(result.alokasiCA,function(id, val){					
						 		$('select[id="jenis2'+idx+'"]').append('<option id="'+val+'" value="'+val+'">'+val+'</option>');
								});
							$(".qty2").attr('disabled','disabled');	
							$(".biaya2").removeAttr('disabled');
					}else if (selectedVal ==  69){
							$.each(result.alokasiSDP,function(id, val){					
						 		$('select[id="jenis2'+idx+'"]').append('<option id="'+val+'" value="'+val+'">'+val+'</option>');
								});
							$(".qty2").attr('disabled','disabled');	
							$(".biaya2").removeAttr('disabled');
					}else if (selectedVal ==  68){
							$.each(result.alokasiATK,function(id, val){					
						 		$('select[id="jenis2'+idx+'"]').append('<option id="'+val+'" value="'+val+'">'+val+'</option>');
								});							
							$(".biaya2").attr('disabled','disabled');								
							$(".qty2").removeAttr('disabled');
					}
			
		
			});
		
		
		
 	  		$('<tr class="listdetail_2" id="check2_">').append(				
					$('<td>').html('<input type="text" value="'+idx+'" name="noUrut2'+idx+'"  id="noUrut2'+idx+'" style="width: 20px;" size="2" maxlength="2" readOnly>'),
					$('<td>').html('<input type="text"  name="name_item2'+idx+'" id="name_item2'+idx+'"   style="width: 300px;">'),
					$('<td>').html('<input type="number" onkeypress="return isNumberKey(event)" class="qty2"  name="qty2'+idx+'" id="qty2'+idx+'"  style="width: 60px;">'),
					$('<td>').html('<select name="jenis2'+idx+'" id="jenis2'+idx+'" title="Pilih Salah satu"  style="width: 200px;"><option value="">Pilih Salah satu</option></select>'),
					$('<td>').html('<input type="text"  name="tgl_pemakaian2'+idx+'" id="tgl_pemakaian2'+idx+'" class="tgl2"  style="width: 120px;">'),
				//	$('<td nowrap="nowrap">').html('<input type="number" onkeypress="return isNumberKey(event)"   name="tg2'+idx+'" id="tg2'+idx+'"  maxlength="2"  style="width: 20px;"><input type="number" onkeypress="return isNumberKey(event)"   name="bl2'+idx+'" id="bl2'+idx+'" maxlength="2" style="width: 20px;"><input type="number" onkeypress="return isNumberKey(event)"  name="th2'+idx+'" id="th2'+idx+'" maxlength="4" style="width: 40px;">'),
					$('<td>').html('<input type="number" onkeypress="return isNumberKey(event)" class="biaya2"  name="biaya2'+idx+'" id="biaya2'+idx+'" style="width: 100px;" >'),
					$('<td>').html('<input type="text"  name="ket2'+idx+'" id="ket2'+idx+'"  style="width: 300px;">')						
				).appendTo('#tableProd');
		
		$(".tgl2").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: "dd/mm/yy" 
		});
				
		$("#countnumber").val(idx);
		$("#pilihalokasi").val(selectedVal);
		
 	}

function addRowDOM2x (tableID, jml) {
		//untuk mengambil value radiobutton yang di checked	
		var selectedVal = "";
		var selected = $("input[type='radio'][name='alokasiBiaya']:checked");
	
		if (selected.length > 0) {
		    selectedVal = selected.val();
		}
		
		var nu = document.getElementById("countnumber").value;	
		var piluk = document.getElementById("pilihalokasi").value;	
		//var tr = $('#tableProd tbody tr:last-child');		
		idx =  parseInt(nu)+ 1;
		pil =  parseInt(piluk);
	//	alert(selectedVal);
		
			
		//samakan apakah pilihan alokasi sebelumnya sama dengan field yang mau di tambahkan jika tidak hapus data list itemnya dan set number dari 1
		if (pil != selectedVal ){
			$('.listdetail_1' ).remove();
			$('.listdetail_2' ).remove();
			$('.listdetail_3' ).remove();
			idx = 1;
		}
		
		
			var url = '${path}/snows/snows.htm?window=ajax&pages=inputBerkasAjuBiaya&jn=addRowtabel&alokasi='+selectedVal;
		
			$.getJSON(url, function(result) {
			
					if (selectedVal ==  59){
							$.each(result.alokasiReimburse,function(id, val){					
						 		$('select[id="jenis2'+idx+'"]').append('<option id="'+val+'" value="'+val+'">'+val+'</option>');
								});
							$(".qty2").attr('disabled','disabled');	
							$(".biaya2").removeAttr('disabled');						
					}else if (selectedVal ==  60){
							$.each(result.alokasiCA,function(id, val){					
						 		$('select[id="jenis2'+idx+'"]').append('<option id="'+val+'" value="'+val+'">'+val+'</option>');
								});
							$(".qty2").attr('disabled','disabled');	
							$(".biaya2").removeAttr('disabled');
					}else if (selectedVal ==  69){
							$.each(result.alokasiSDP,function(id, val){					
						 		$('select[id="jenis2'+idx+'"]').append('<option id="'+val+'" value="'+val+'">'+val+'</option>');
								});
							$(".qty2").attr('disabled','disabled');	
							$(".biaya2").removeAttr('disabled');
					}else if (selectedVal ==  68){
							$.each(result.alokasiATK,function(id, val){					
						 		$('select[id="jenis2'+idx+'"]').append('<option id="'+val+'" value="'+val+'">'+val+'</option>');
								});							
							$(".biaya2").attr('disabled','disabled');								
							$(".qty2").removeAttr('disabled');
					}
			
		
			});
		
		
		
 	  		$('<tr class="listdetail_2" id="check2_">').append(				
					$('<td>').html('<input type="text" value="'+idx+'" name="noUrut2'+idx+'"  id="noUrut2'+idx+'" style="width: 20px;" size="2" maxlength="2" readOnly>'),
					$('<td>').html('<input type="text"  name="name_item2'+idx+'" id="name_item2'+idx+'"   style="width: 300px;">'),
					$('<td>').html('<input type="number" onkeypress="return isNumberKey(event)" class="qty2"  name="qty2'+idx+'" id="qty2'+idx+'"  style="width: 60px;">'),
					$('<td>').html('<select name="jenis2'+idx+'" id="jenis2'+idx+'" title="Pilih Salah satu"  style="width: 200px;"><option value="">Pilih Salah satu</option></select>'),
					$('<td>').html('<input type="text"  name="tgl_pemakaian2'+idx+'" id="tgl_pemakaian2'+idx+'" class="tgl2" style="width: 120px;">'),
				//	$('<td nowrap="nowrap">').html('<input type="number" onkeypress="return isNumberKey(event)"   name="tg2'+idx+'" id="tg2'+idx+'"  maxlength="2"  style="width: 20px;"><input type="number" onkeypress="return isNumberKey(event)"   name="bl2'+idx+'" id="bl2'+idx+'" maxlength="2" style="width: 20px;"><input type="number" onkeypress="return isNumberKey(event)"  name="th2'+idx+'" id="th2'+idx+'" maxlength="4" style="width: 40px;">'),
					$('<td>').html('<input type="number" onkeypress="return isNumberKey(event)" class="biaya2"  name="biaya2'+idx+'" id="biaya2'+idx+'" style="width: 100px;" >'),
					$('<td>').html('<input type="text"  name="ket2'+idx+'" id="ket2'+idx+'"  style="width: 300px;">')						
				).appendTo('#tableProd');
				
				
				
		$(".tgl2").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: "dd/mm/yy" 
		});
		
		$("#countnumber").val(idx);
		$("#pilihalokasi").val(selectedVal);
		
 	}

 	
function delRow1(){
		
		var nu = document.getElementById("countnumber").value;			
		//alert(idx);
		
		if (idx >0){
		$('#tableProd tr:last').remove();
		idx =  parseInt(nu) - 1;
		}
	   $("#countnumber").val(idx);
	} 
	
function checking(){
		
	var nu = document.getElementById("countnumber").value;	
		var piluk = document.getElementById("pilihalokasi").value;	
		var cabang = document.getElementById("cabanglist1").value;
		
		var namaBank = document.getElementById("namaBank").value;
		var noRek = document.getElementById("noRek").value;
		var atasNamaRek = document.getElementById("atasNamaRek").value;
		//var cabang = document.getElementById("cabanglist1").value;	
		//var tr = $('#tableProd tbody tr:last-child');		
		idx =  parseInt(nu);
		pil =  parseInt(piluk);
		var ni = 0;
		var gagal = "";
		
		if(nu == 0){
			alert("Mohon Mengisi Data Detail Keperluan Terlebih Dahulu");			
			gagal = 1;
		}
		
		
		
		if (idx > 0){	
		//save akan visible jika pada saat checking , lolos dari semua role
		document.getElementById('btnSave').style.visibility = 'visible';	
			
					for (var i = 1; i <= idx; i++) { 
							var nama_item = document.getElementById("name_item2"+i).value;
							var qty = document.getElementById("qty2"+i).value;
							var jenis = document.getElementById("jenis2"+i).value;
// 							var tg = document.getElementById("tg2"+i).value;
// 							var bl = document.getElementById("bl2"+i).value;
// 							var th = document.getElementById("th2"+i).value;
							var tgl_pemakaian = document.getElementById("tgl_pemakaian2"+i).value;
							var biaya = document.getElementById("biaya2"+i).value;
							
							
							if(nama_item == ""){
								alert("Detail Per item no "+ i+", tidak boleh kosong ");  				 		
								gagal = 1;
							}	
							if(tgl_pemakaian == ""){
								alert("Tanggal awal pemakaian no "+ i+", tidak boleh kosong ");  	
				    			gagal = 1;
							}							
							if(jenis == ""){
								alert("Jenis Keperluan belum di pilih");  	
				    			gagal = 1;
							}
							
							if(pil == 68 && qty == ""){
								alert("[Kuantitas no "+ i+" ]  - Untuk Alokasi ATK Kantor harus menginput Kuantitas");  	
				    			gagal = 1;
							}	
							
							if(pil != 68 && biaya == ""){
								alert("[Biaya no "+ i+" ]  - Untuk ALokasi Cash Advance / Sewa dan Pembayaran PO / Lain-lain harus menginput Biaya");  	
				    			gagal = 1;
							}
							
							
						var url = '${path}/snows/snows.htm?window=ajax&pages=inputBerkasAjuBiaya&jn=cekPermintaan&cek=cekNew&jenis='+jenis+'&tgl_pemakaian='+tgl_pemakaian+'&cabang='+cabang;
						var psn = "";
						
						
						$.getJSON(url, function(result) {
								psan = result.psn;								
								if(psan != ""){
									alert(psan);
									document.getElementById('btnSave').style.visibility = 'hidden';
								}
								
						});
					
					};
					
			if(pil != 68 && namaBank == ""){
				alert("[Transfer ]  - Untuk ALokasi Cash Advance / Sewa dan Pembayaran PO / Lain-lain harus menginput Nama Bank");  	
    			gagal = 1;
			}
			if(pil != 68 && noRek == ""){
				alert("[Transfer ]  - Untuk ALokasi Cash Advance / Sewa dan Pembayaran PO / Lain-lain harus menginput No Rek Bank");  	
    			gagal = 1;
			}
			if(pil != 68 && atasNamaRek == ""){
				alert("[Transfer ]  - Untuk ALokasi Cash Advance / Sewa dan Pembayaran PO / Lain-lain harus menginput Atas Nama");  	
    			gagal = 1;
			}
									
							
			if( gagal == 1){
				document.getElementById('btnSave').style.visibility = 'hidden';
			};
			
		};



//	   $("#statSave").val(idx);
	} 

function checking2(){
		var NO_BERKAS = document.getElementById("NoBerkas").value ;
		var nu = document.getElementById("countnumber").value;	
		var piluk = document.getElementById("pilihalokasi").value;	
		var namaBank = document.getElementById("namaBank").value;
		var noRek = document.getElementById("noRek").value;
		var atasNamaRek = document.getElementById("atasNamaRek").value;
		pil =  parseInt(piluk);
		idx =  parseInt(nu);
	//	alert (idx);
		
		if(nu == 0){
			alert("Mohon Mengisi Data Detail Keperluan Terlebih Dahulu");
			gagal = 1;
		}
		
		if (idx > 0){	
		//save akan visible jika pada saat checking , lolos dari semua role
		document.getElementById('btnSave2').style.visibility = 'visible';	
			
					for (var i = 1; i <= idx; i++) { 
							var nama_item = document.getElementById("name_item2"+i).value;
							var qty = document.getElementById("qty2"+i).value;
 							var jenis = document.getElementById("jenis2"+i).value;
 							var tgl_pemakaian = document.getElementById("tgl_pemakaian2"+i).value;
 							var biaya = document.getElementById("biaya2"+i).value;
 							var ket = document.getElementById("ket2"+i).value;
								
							
							if(nama_item == ""){
								alert("Detail Per item no "+ i+", tidak boleh kosong ");  				 		
								gagal = 1;
							}	
							
							if(tgl_pemakaian == ""){
								alert("Tanggal awal pemakaian no "+ i+", tidak boleh kosong ");  	
				    			gagal = 1;
							}							
							if(jenis == ""){
								alert("Jenis Keperluan belum di pilih");  	
				    			gagal = 1;
							}
							
							if(pil == 68 && qty == ""){
								alert("[Kuantitas no "+ i+" ]  - Untuk Alokasi ATK Kantor harus menginput Kuantitas");  	
				    			gagal = 1;
							}	
							
							if(pil != 68 && biaya == ""){
								alert("[Biaya no "+ i+" ]  - Untuk ALokasi Cash Advance / Sewa dan Pembayaran PO / Lain-lain harus menginput Biaya");  	
				    			gagal = 1;
							}
							
							
						var url = '${path}/snows/snows.htm?window=ajax&pages=inputBerkasAjuBiaya&jn=cekPermintaan&cek=cekUpdate&NO_BERKAS='+NO_BERKAS+'&nama_item='+nama_item+'&tgl_pemakaian='+tgl_pemakaian+'&jenis='+jenis+'&qty='+qty+'&biaya='+biaya;
						var psn = "";
						
						
						$.getJSON(url, function(result) {
								psan = result.psn;								
								if(psan != ""){
									alert(psan);
									document.getElementById('btnSave2').style.visibility = 'hidden';
								}
								
						});
					
					};
					
			if(pil != 68 && namaBank == ""){
				alert("[Transfer ]  - Untuk ALokasi Cash Advance / Sewa dan Pembayaran PO / Lain-lain harus menginput Nama Bank");  	
    			gagal = 1;
			}
			if(pil != 68 && noRek == ""){
				alert("[Transfer ]  - Untuk ALokasi Cash Advance / Sewa dan Pembayaran PO / Lain-lain harus menginput No Rek Bank");  	
    			gagal = 1;
			}
			if(pil != 68 && atasNamaRek == ""){
				alert("[Transfer ]  - Untuk ALokasi Cash Advance / Sewa dan Pembayaran PO / Lain-lain harus menginput Atas Nama");  	
    			gagal = 1;
			}
									
			
							
			if( gagal == 1){
				document.getElementById('btnSave2').style.visibility = 'hidden';
			};
			
		};
		
		
	} 


function ubah(x){
		var url = '${path}/snows/snows.htm?window=ajax&pages=inputBerkasAjuBiaya&jn=databycabang&cabangpil='+x;
	//	window.location.href = '${path}/snows/snows.htm?window=inputBerkasAjuBiaya';	
		
		$.getJSON(url, function(result) {
			alert(result.dataInbox);
			$(".dataInbox").val(result.dataInbox);  
		});
		
		//alert (x);
	} 
	

function inputTglBerkas(){
			
		var NO_BERKAS = document.getElementById("NoBerkas").value ;
		var tbk = prompt("Silahkan Input Tanggal Pengiriman Berkas", "dd/mm/yyyy");

		if (tbk != null) {		
				var url = '${path}/snows/snows.htm?window=ajax&pages=inputBerkasAjuBiaya&jn=inputTglBerkas&mi_id='+NO_BERKAS+'&tbk='+tbk;
				var psan = "";
								
				
				$.getJSON(url, function(result) {
						psan = result.pesan;
						tgbk = result.tglbk;			
						alert(psan);
						$("#tglKirimBerkas").val(tgbk);
				});
				
		};
		
	} 


    
hideLoadingMessage();
</script>
<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; background-color: #E0E0E0; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/*input[type="text"]{width: 200px;}*/
	button{width:125px;height: 30px;}
	
	input[type="submit"]{
		width:125px;
		height: 20px;
		border:1px solid purple;
    	text-decoration:none;
	}
	
	.dataInbox{
		cursor:pointer;
		cursor: hand;
	}
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }
	
	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }
</style>
<body >
	<table bgcolor="#E0E0E0"><tr><td>
	<div id="tabs" >
	<table><tr><td>
		<ul>
			<li><a href="#tab-1">INPUT BERKAS</a></li>		
			<li><a href="#tab-2">STATUS BERKAS</a></li>				
		</ul>
		</td></tr>
		<tr><td>
		<div id="tab-1">
			<form name="formpost" method="post" action="${path}/snows/snows.htm?window=inputBerkasAjuBiaya" id="formpost" >
				<table class="entry2">
					
					<tr>
						<th  valign="top" nowrap="nowrap">
							<label>No. Registrasi</label><br/><hr>
							
							<c:forEach items="${dataInbox}" var="d">
								<div class="dataInbox" id="${d.NO_BERKAS}">		
								<c:if test="${d.POSISI_BERKAS eq '207'}">	
								<table align ="center" width= 100%><tr>
									<c:if test="${d.LSPD_ID_FROM eq null}">	
									<td style="border:1px solid purple;background:purple;color: white;font-weight: bold;font-size:100%;">
									${d.NO_BERKAS}<br/></td>
									</c:if>	
									<c:if test="${d.LSPD_ID_FROM ne null}">	
									<td style="border:1px solid purple;background:red;color: white;font-weight: bold;font-size:100%;">
									${d.NO_BERKAS}<br/></td>
									</c:if>	
								</tr></table>					
								</c:if>								
								</div>
							</c:forEach>
						</th>
						<td valign="top">
							<fieldset>
<!-- 							<c:if test="${not empty pesan }"> -->
<!-- 							<div id="success"> -->
<!-- 								${pesan } -->
<!-- 							</div> -->
<!-- 							</c:if> -->
							<!-- untuk info polis -->
							<div id="f_input">
								<table>
										<TR>
										<td nowrap="nowrap" >
										<fieldset>
											<input type="submit" id="btnNew" name="btnNew" value="New Inbox"> &nbsp;&nbsp; 
											<input type="button" id="btnEdit" name="btnEdit" value="Edit" disabled = "disabled" style="width: 130px; height: 20px; border:1.5px solid purple;text-decoration:none;"> &nbsp;&nbsp;
											<input type="submit" id="btnDel" name="btnDel" value="Batal" disabled = "disabled"> &nbsp;&nbsp;
											<input type="submit" id="btnPrint" name="btnPrint" value="Print Formulir" disabled = "disabled">	&nbsp;&nbsp;
											<input type="button" id="btnUpl" name="btnUpl" value="Upload" disabled = "disabled" style="width: 130px; height: 20px; border:1.5px solid purple;text-decoration:none;"> &nbsp;&nbsp;
											<input type="button" id="btnView" name="btnView" value="Viewer" disabled = "disabled" style="width: 130px; height: 20px; border:1.5px solid purple;text-decoration:none;"> &nbsp;&nbsp;
											<input type="button" id="btnTglKirim" name="btnTglKirim" value="Tgl Kirim Berkas" onClick="inputTglBerkas()" disabled = "disabled" style="width: 130px; height: 20px; border:1.5px solid purple;text-decoration:none;">&nbsp;&nbsp; &nbsp; 
											<input type="submit" id="btnTrans2" name="btnTrans2" value="Transfer"  disabled = "disabled"> 
											
										</fieldset>
										</td>
										</TR>
										<tr>
										<td>
											<legend>Pengajuan Biaya</legend>
											<br>
										<table>
										
											<tr>
												<th style="width: 100px;">NO REGISTRASI</th>
												<td>:</td>
												<td><input type="text" id="NoBerkas" name="NoBerkas" value ="${NoBerkas}" style="background-color: #E0E0E0; width: 150px;" readonly="readonly"   ></td>
											</tr>
											<tr>
												<th style="width: 100px;" nowrap="nowrap">TANGGAL PENGAJUAN</th>
												<td>:</td>
												<td><input type="text" class="tgl" id="tgl_berkas_masuk" name="tgl_berkas_masuk" value ="${tgl_berkas_masuk}" disabled = "disabled" style="width: 150px;"></td>
											</tr>
											<tr>
												<th style="width: 100px;">CABANG</th>
												<td>:</td>
												<td colspan="3">
<!-- 												<c:if test="${statBtn eq '1'}"> -->
<!-- 														<input type="text" name="cabang" id="cabang" readonly="readonly" value="${cabang}" >	 -->
<!-- 												</c:if> -->
<!-- 												<c:if test="${statBtn eq '0'}"> -->
																<select name="cabanglist1" id="cabanglist1" >																	
																	<c:forEach var="x" items="${cabangAll}">
																		<option value="${x.VALUE}"
																		<c:if test="${namaCabang eq  x.VALUE }">Selected</c:if>>
																				${x.VALUE}
																		</option>
																	</c:forEach>
																</select>
													
<!-- 												</c:if> -->
												</td>
											</tr>
											<tr>
												<th style="width: 100px;">ALOKASI BIAYA</th>
												<td>:</td>
												<td><input type="radio" name="alokasiBiaya" id="ca" value="60" <c:if test="${pilihalokasi eq '60'}"> checked="checked" </c:if> <c:if test="${statSave eq '0'}">disabled = "disabled"</c:if>
																/> <b>CASH ADVANCE</b> 																
													<input type="radio" name="alokasiBiaya" id="sdp" value="69" <c:if test="${pilihalokasi eq '69'}"> checked="checked" </c:if> <c:if test="${statSave eq '0'}">disabled = "disabled"</c:if>
																/> <b>SEWA DAN PEMBAYARAN PO</b>
													<input type="radio" name="alokasiBiaya" id="atk" value="68" <c:if test="${pilihalokasi eq '68'}"> checked="checked" </c:if> <c:if test="${statSave eq '0'}">disabled = "disabled"</c:if>
																/> <b>ATK KANTOR</b>
													<input type="radio" name="alokasiBiaya" id="reimbes" value="59" <c:if test="${pilihalokasi eq '59'}"> checked="checked" </c:if> <c:if test="${statSave eq '0'}">disabled = "disabled"</c:if>
																/> <b>LAINNYA</b>
												</td>
											</tr>
											<tr>
												<th style="width: 100px;">KEPERLUAN</th>
												<td>:</td>
												<td colspan="3">
													
													<table>
													<tr>
													<td>
														<div id="listDet" >	
														<table border=2 id="tableProd">
															<tr>
															<th>No.</th>
															<th>Detail Per Item (NamaBarang & type)</th>
															<th>Kuantitas</th>
															<th>Jenis Keperluan</th>
															<th>Tgl Awal Pemakaian<br> <small>(dd/mm/yyyy)</small></th>
															<th>Biaya</th>
															<th>Keterangan</th>
															
															</tr>
														
<!-- 														<c:forEach items="${dataDetailItem}" var="ptx" varStatus="status">  -->
<!-- 														<tr class="listdetail_2" id="check2_" > -->
<!-- 															<td><input type="text" name='no_urut2${status.index +1}' id="no_urut2" value ="${ptx.NO_URUT}" size="2" maxlength="2" readOnly style="width: 20px;"></td> -->
<!-- 															<td><input type="text" name="name_item2${status.index +1}" id="name_item2" value="${ptx.MID_DESC}"  style="width: 300px;" ></td> -->
<!-- 															<td><input type="text" name="qty2${status.index +1}" id="qty2" value="${ptx.KUANTITAS}" style="width: 60px;" ></td> -->
<!-- 															<td><input type="text" name="jenis2${status.index +1}" id="jenis2" value="${ptx.AB_TYPE}" style="width: 100px;" ></td> -->
<!-- 															<td><input type="text" name="tgl_pemakaian2${status.index +1}" id="tgl_pemakaian2" value="${ptx.TGL_GUNA}" class="tgl" style="width: 80px;"></td> -->
<!-- 															<td nowrap ="nowrap"><input type="text"  name="tg2${status.index +1}" id="tg2" value="${ptx.tg2}"  maxlength="2"  style="width: 20px;"> -->
<!-- 																<input type="text"  name="bl2${status.index +1}" id="bl2" value="${ptx.bl2}"  maxlength="2" style="width: 20px;"> -->
<!-- 																<input type="text"  name="th2${status.index +1}" id="th2" value="${ptx.th2}"  maxlength="4" style="width: 40px;"></td> -->
<!-- 															<td><input type="text" name="biaya2${status.index +1}" id="biaya2" value="${ptx.MID_NOMINAL}"   style="width: 100px;" onchange="jumlahbiaya()" ></td> -->
<!-- 															<td><input type="text" name="ket2${status.index +1}" id="ket2" value="${ptx.MID_KET}"  style="width: 300px;"></td> -->
<!-- 															</tr> -->
															
<!-- 														</c:forEach>	 -->
																																			
													</table>
													</div>	
													
													</td>													
													</tr>
													
													<tr>
													<td colspan = 7 align="center">
														
														<input type="button" id="btnadd" name="btnadd" value="+" tabindex="15" onClick = "addRowDOM1x('tableProd','1')" <c:if test="${statSave eq '0'}">disabled = "disabled"</c:if>  style="border:1px solid purple;text-decoration:none; background:gray;color: white;font-weight: bold;visibility:hidden" >
														<input type="button" id="btndel1" name="btndel1" value="-" onClick="delRow1()" <c:if test="${statSave eq '0'}">disabled = "disabled"</c:if> style="border:1px solid purple;text-decoration:none; background:gray;color: white;font-weight: bold;visibility:hidden" >
														<input type="button" id="btnadd2" name="btnadd2" value="+" tabindex="15" onClick = "addRowDOM2x('tableProd','1')"  style="border:1px solid purple;text-decoration:none; background:gray;color: white;font-weight: bold;visibility:hidden" >
														<input type="button" id="btndel2" name="btndel2" value="-" onClick="delRow1()"  style="border:1px solid purple;text-decoration:none; background:gray;color: white;font-weight: bold;visibility:hidden" >
														<input type ="hidden" id = "countnumber" name = "countnumber" value ="${countnumber}"  >
														<input type ="hidden" id = "pilihalokasi" name = "pilihalokasi" value ="${pilihalokasi}"  >
														<input type ="hidden" id = "statBtn" name = "statBtn" value ="${statBtn}"  >
														<input type ="hidden" id = "namaCabang" name = "namaCabang" value ="${namaCabang}"  >
														<input type ="hidden" id = "statSave" name = "statSave" value ="${statSave}"  >
													</td>
													</tr>
													</table>
													
													
																										
												</td>
											</tr>
											<tr>
												<th style="width: 100px;">Total Biaya</th>
												<td>:</td>
												<td>&nbsp;<input type="text" id="totalBiaya" name="totalBiaya" value ="${totalBiaya}" readonly="readonly" style="background-color: #E0E0E0; width: 150px;" ></td>
											</tr>
										</table> <br/>
									
										</td>
										
									</tr>
								</table><br>
								
								<legend id = "divTrans" >Transfer</legend><br>
										<table id = "tbTrans">
											<tr >
												<th style="width: 100px;">NAMA. BANK</th><td>:</td>
												<td><input type="text" name="namaBank" id="namaBank" value ="${namaBank}" 
													<c:if test="${statSave eq '0'}">readonly = "readonly"</c:if> ></td>
											</tr>
											<tr>
												<th style="width: 100px;">NO. REK</th><td>:</td>
												<td><input type="number" onkeypress="return isNumberKey(event)"  name="noRek" id="noRek" value ="${noRek}" 
													<c:if test="${statSave eq '0'}">readonly = "readonly"</c:if> ></td>
											</tr>	
											<tr>
												<th style="width: 100px;">ATAS NAMA</th><td>:</td>
												<td><input type="text" name="atasNamaRek" id="atasNamaRek" value ="${atasNamaRek}" 
												 	<c:if test="${statSave eq '0'}">readonly = "readonly"</c:if> ></td>
											</tr>	
																		
										</table> <br/>
										
									<legend id = "divTglKirim" >TANGGAL KIRIM BERKAS</legend><br>
										<table id = "tbKirim">
											<tr >
												<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													&nbsp;&nbsp;</td>
												<th>
												<input type="text" id="tglKirimBerkas" name="tglKirimBerkas" value ="${tglKirimBerkas}" style="background-color: #E0E0E0; width: 100px;" readonly="readonly"   ></th>
											</tr>
																												
										</table> <br/>

								
							</div>
									<br>
									
									 &nbsp;&nbsp;&nbsp;&nbsp;
									 <input type="button" id="btnCheck" name="btnCheck" value="Check" onClick="checking()" 
									<c:if test="${statSave eq '0'}">style="width: 80px; height: 18px; border:1px solid white;text-decoration:none;background-color: purple; color: white; visibility:hidden;"</c:if>
									<c:if test="${statSave eq '1'}">style="width: 80px; height: 18px; border:1px solid white;text-decoration:none;background-color: purple; color: white; visibility:visible;"</c:if>> &nbsp;&nbsp;
						
								
									<input type="submit" id="btnCancel" name="btnCancel" value="Cancel"  
									<c:if test="${statSave eq '0'}">style="width: 80px; height: 18px; border:1px solid white;text-decoration:none;background-color: purple; color: white; visibility:hidden;"</c:if>
									<c:if test="${statSave eq '1'}">style="width: 80px; height: 18px; border:1px solid white;text-decoration:none;background-color: purple; color: white; visibility:visible;"</c:if>> &nbsp;&nbsp;
									
										<input type="submit" id="btnSave" name="btnSave" value="Save"
									<c:if test="${statSave eq '0'}">style="width: 80px; height: 18px; border:1px solid white;text-decoration:none;background-color: purple; color: white; visibility:hidden;"</c:if>
									<c:if test="${statSave eq '2'}">style="width: 80px; height: 18px; border:1px solid white;text-decoration:none;background-color: purple; color: white; visibility:visible;"</c:if>> &nbsp;&nbsp;
									
									<br>
									
										 &nbsp;&nbsp;&nbsp;&nbsp;
									 <input type="button" id="btnCheck2" name="btnCheck2" value="Check Update" onClick="checking2()" 
									<c:if test="${statSave eq '0'}">style="width: 120px; height: 18px; border:1px solid white;text-decoration:none;background-color: purple; color: white; visibility:hidden;"</c:if>
									<c:if test="${statSave eq '1'}">style="width: 120px; height: 18px; border:1px solid white;text-decoration:none;background-color: purple; color: white; visibility:visible;"</c:if>> &nbsp;&nbsp;
						
								
									<input type="submit" id="btnCancel2" name="btnCancel2" value="Cancel"  
									<c:if test="${statSave eq '0'}">style="width: 80px; height: 18px; border:1px solid white;text-decoration:none;background-color: purple; color: white; visibility:hidden;"</c:if>
									<c:if test="${statSave eq '1'}">style="width: 80px; height: 18px; border:1px solid white;text-decoration:none;background-color: purple; color: white; visibility:visible;"</c:if>> &nbsp;&nbsp;
									
										<input type="submit" id="btnSave2" name="btnSave2" value="Update"
									<c:if test="${statSave eq '0'}">style="width: 80px; height: 18px; border:1px solid white;text-decoration:none;background-color: purple; color: white; visibility:hidden;"</c:if>
									<c:if test="${statSave eq '2'}">style="width: 80px; height: 18px; border:1px solid white;text-decoration:none;background-color: purple; color: white; visibility:visible;"</c:if>> &nbsp;&nbsp;
									
						</fieldset>		
						</td>
						
					</tr>
					<tr><td colspan=2>
					<font color= purple>Ungu </font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Inputan Awal <br>
					<font color= red>Merah </font>&nbsp;&nbsp;: Back to Admin<br>
					</td></tr>
				</table>
			</form>
		</div>
		
		<div id="tab-2" style="color:purple; background-color: #E0E0E0;  ">
		
		<table cellpadding="5" cellspacing=0>
		<tr style="background-color: purple; color:white;"  >
			<th style="width: 100px;" nowrap="nowrap">No. Regristration</th><td>:</td>
			<td><input type="text" name="noReg" id="noReg" value ="${noReg}" ></td>
			<td> <input type="button" class ="btnSearc" id="btnSearc" name="btnSearc" value="Search" ></td>
		</tr>
		<tr><td><br><br></td></tr>
		</table>
		<table bgcolor="#BDBDBD"  id="tableHist">
				<tr >
					<th style="width: 20px;" >No</th>
					<th style="width: 100px;">No. Regristration</th>				
					<th style="width: 150px;">Tanggal</th>
					<th style="width: 150px;">Posisi Document</th>
					<th style="width: 150px;">User</th>				
					<th style="width: 600px;">Keterangan</th>
				</tr>
			<pg:paging url="${path}/snows/snows.htm?window=inputBerkasAjuBiaya" pageSize="50">
				<c:forEach items="${dataHistory}" var="d" varStatus="status"> 
			<pg:item>
				<tr class="listBiaya" id="listBiaya" >
					<td  style="background-color: #E0E0E0;"><input type="text" name='nom' id="nom" value ="${status.index +1}" size="4" maxlength="4" readOnly style="width: 20px;"></td>
					<td  style="background-color: #E0E0E0;"><input type="text" name="NoBel" id="NoBel" value="${d.MI_ID}" readOnly style="width: 100px;" ></td>
					<td  style="background-color: #E0E0E0;"><input type="text" name="tglTerbentukl" id="tglTerbentukl" value="${d.CREATE_DATE}" readOnly style="width: 150px;" ></td>
					<td  style="background-color: #E0E0E0;"><input type="text" name="posDokL" id="posDokL" value="${d.LSPD_POSITION}" readOnly style="width: 150px;" ></td>
					<td  style="background-color: #E0E0E0;"><input type="text" name="userl" id="userl" value="${d.LUS_FULL_NAME}" readOnly style="width: 150px;" ></td>
					<td  style="background-color: #E0E0E0;" ><input type="text" name="descl" id="descl" value="${d.MI_DESC}" readOnly style="width: 600px; display:inline-block;" ></td>
	 			</tr>
			</pg:item>									
				</c:forEach>	
			<pg:index>
	  		<pg:page><%=thisPage%>	  		
			</pg:page>			  		
			</pg:index>
			<p style="color:red;">Note : Pages hanya di gunakan untuk list semua data. Abaikan pages untuk list data pencarian !</p>
			
	  	</pg:paging>						
		</table>
		
		</div>
		</td>
		</tr>
		</table>
	</div>
	</td></tr></table>
</body>
<%@ include file="/include/page/footer.jsp"%>