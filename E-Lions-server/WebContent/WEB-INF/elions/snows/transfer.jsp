<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />

<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->

<body>
<form action="${path}/snows/snows.htm?window=transfer" method="post">
<input type="hidden" id="mi_id" name="mi_id" value="${mi_id}">
	<c:if test="${not empty pesan }">
	<div id="success">
		${pesan }
	</div>
	</c:if>
	<table>
		<tr>
			<th>Asal Pekerjaan</th>
			<td>:</td>
			<td>New Input</td>
		</tr>
		<!-- <tr>
			<th>Pendingan Dari</th>
			<td>:</td>
			<td>New Input</td>
		</tr> -->
		<tr>
			<th>Transfer Ke</th>
			<td>:</td>
			<td>
				<select id="transKe" name="transKe">
					<option value="203">POLICY SERVICE</option>
					<option value="204">CUSTOMER SERVICE</option>
					<%-- <c:forEach items="${transferKe}" var="t">
						<option value="${t.key}">${t.value}</option>
					</c:forEach> --%>
				</select>
			</td>
		</tr>
		<tr>
			<th>Description</th>
			<td>:</td>
			<td><textarea rows="3" cols="50" id="desc" name="desc"></textarea></td>
		</tr>
		<tr>
			<th>&nbsp;</th>
			<td>&nbsp;</td>
			<td>
				<input type="submit" id="trans" name="trans" value="Transfer" <c:if test="${masuk == 2}">disabled</c:if>>
				<input type="button" id="cancel" name="cancel" value="Cancel" onclick="self.close();">
			</td>
		</tr>
	</table>
</form>
<script type="text/javascript">
	window.onunload = refreshParent;
    function refreshParent() {
        window.opener.location.reload('${path}/snows/snows.htm?window=inputSnows');
    }
hideLoadingMessage();
</script>
</body>
<%@ include file="/include/page/footer.jsp"%>