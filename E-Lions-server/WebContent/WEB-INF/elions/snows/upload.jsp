<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />

<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->

<script type="text/javascript">
$().ready(function() {

});
hideLoadingMessage();
</script>
<body>
<form action="${path}/snows/snows.htm?window=upload" method="post" enctype="multipart/form-data">
	<input type="hidden" id="spaj" name=spaj value="${spaj}">
	<input type="hidden" id="mi_id" name="mi_id" value="${mi_id}">
	<input type="hidden" id="lca_id" name="lca_id" value="${lca_id}">
	<input type="hidden" id="ljj_id" name="ljj_id" value="${ljj_id}">
	<input type="hidden" id="lstb_id" name="lstb_id" value="${lstb_id}">
	<c:if test="${not empty pesan }">
	<div id="success">
		${pesan }
	</div>
	</c:if>
	<legend>Daftar file yang harus di upload :</legend>
	<table>
		<c:forEach items="${dataCheckList}" var="d" varStatus="s">
			<tr>
				<th>${d.LC_NAMA}</th>
				<td><input type="hidden" value="${d.LC_ID}" id="lc_id${s.index}" name="lc_id${s.index}" >&nbsp;</td>
				<td><input type="file" name="daftarFile[${s.index}]" id="daftarFile[${s.index}]"></td>
			</tr>
		</c:forEach>
	</table>
	<em style="color: red;">
		<ul>
			<li>* Format file pdf</li>
			<li>Seluruh file sudah di pilih</li>
		</ul>
	</em>
	<fieldset>
		<legend>Action</legend>
		<input type="submit" name="upload" id="upload" value="Upload">
	</fieldset>
</form>

</body>
<%@ include file="/include/page/footer.jsp"%>