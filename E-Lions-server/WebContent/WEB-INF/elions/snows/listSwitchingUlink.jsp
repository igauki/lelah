<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />

<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->

<script type="text/javascript">
$().ready(function() {

});
hideLoadingMessage();
</script>
<body>
<form action="" method="post">
	<input type="hidden" id="mi_id" name="mi_id" disabled="disabled">
	<table>
		<tr></tr>
		<c:forEach items="${dataList}" var="d">
			<tr>
				<td></td>
			</tr>
		</c:forEach>
	</table>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>