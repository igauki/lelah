<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />

<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->

<script type="text/javascript">
$().ready(function() {
	$("#no_reg",opener.document).val();
});
function ambil(no_reg, edate){
	$("#reff",opener.document).val(no_reg);
	$("#tgl_jt_tempo",opener.document).val(edate);
	self.close();
}
hideLoadingMessage();
</script>
<style type="text/css">
	tr:hover td {
		background:#3CB4F0;
		cursor: hand;
		cursor: pointer;
	}
</style>
<body>
<form action="" method="post">
	<input type="hidden" id="mi_id" name="mi_id" disabled="disabled">
	<br/><center>Silahkan pilih salah satu dengan click baris yang diinginkan.</center> <br/>
	<table border="1">
		<tr>
			<th>Jenis Investasi</th>
			<th>Premi Ke</th>
			<th>TU Ke</th>
			<th>Periode</th>
			<th>Jenis Roll Over</th>
			<th>MTI</th>
			<th>Jumlah</th>
			<th>Tgl NAB</th>
			<th>NAB</th>
			<th>Jumlah Unit</th>
			<th>No Register</th>
		</tr>
		<c:forEach items="${dataList}" var="d">
			<tr onclick="ambil('${d.NO_REG}','<fmt:formatDate value="${d.MSL_EDATE}" pattern="d/M/yyyy"/>');">
				<td>${d.LJI_INVEST}</td>
				<td>${d.MSL_PREMI_KE}</td>
				<td>${d.MSL_TU_KE }</td>
				<td><fmt:formatDate value="${d.MSL_BDATE}" pattern="d/M/yyyy"/> - <fmt:formatDate value="${d.MSL_EDATE}" pattern="d/M/yyyy"/></td>
				<td>${d.JN_RO }</td>
				<td>${d.MSL_MGI}</td> 
				<td>${d.MSL_PREMI}</td>
				<td>${d.MSL_TGL_NAB}</td>
				<td>${d.MSL_NAB}</td>
				<td>${d.MSL_UNIT }</td>
				<td>${d.NO_REG}</td>
			</tr>
		</c:forEach>
<!-- 		<tr> -->
<!-- 			<td><input type="submit" id="set" name="set" value="OK"> </td> -->
<!-- 			<td><input type="button" id="close" name="close" value="close" onclick="self.close();" > </td> -->
<!-- 		</tr> -->
	</table>
</form>

</body>
<%@ include file="/include/page/footer.jsp"%>