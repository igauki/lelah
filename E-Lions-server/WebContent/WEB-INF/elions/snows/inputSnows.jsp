<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>
<!DOCTYPE html>
<meta http-equiv="X-UA-Compatible" content="IE=9" >
<c:set var="path" value="${pageContext.request.contextPath}" />

<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->

<script type="text/javascript">
$(document).ready(function() {
	var pesan = '${pesan}';
	if(pesan != '') alert(pesan);
		
	$("#tabs").tabs();
	
	$("#btnEdit").attr('disabled','disabled');
	$("#btnSave").attr('disabled','disabled');
	$("#btnDel").attr('disabled','disabled');
	$("#btnUpl").attr('disabled','disabled');
	$("#btnTrans").attr('disabled','disabled');
	$("#btnView").attr('disabled','disabled');
	$("#btnHist").attr('disabled','disabled');
	$("#btnHist").attr('disabled','disabled');
	
	$('#btnNew').click(function(){
		window.location.href = '${path}/snows/snows.htm?window=inputSnows';
	});
		
// 	$('#btnDel').click(function(){
// 		var mi_id = $('#hidNoInbox').val();
// 		var desc = $('#desc').val();
// 		url = '${path}/snows/snows.htm?window=ajax&pages=inputSnows&jn=delData&hidNoInbox='+mi_id+'&desc='+desc;
// 		$.getJSON(url, function(result){
// 			alert("Berhasil di delete");
// 			location.reload();
// 		});
// 	});
	
	$('#btnEdit').click(function (){
		$('.tgl').attr('enabled : enabled');
	});
		
	$('#btnUpl').click(function(){
		var ljj_id = $('#jnsInbox').val();
		var spaj = $('#noSPAJ').val();
		var lstb_id = $('#lstb_id').val();
		popWin('${path}/snows/snows.htm?window=upload&mi_id='+$('#hidNoInbox').val()+'&lca_id='+$('#lca_id').val()+'&ljj_id='+ljj_id+'&spaj='+spaj+'&lstb_id='+lstb_id, 300, 800);
	});
	
	$('#btnTrans').click(function(){
// 		popWin('${path}/snows/snows.htm?window=transfer&mi_id='+$('#hidNoInbox').val(), 220, 400);
		window.open('${path}/snows/snows.htm?window=transfer&mi_id='+$('#hidNoInbox').val(),'winTran','width=400, height=220');
	});
	
	$('#btnView').click(function(){
		var spaj = $('#noSPAJ').val();
		var lstb_id = $('#lstb_id').val();
		window.open('${path}/snows/snows.htm?window=viewer&mi_id='+$('#hidNoInbox').val()+'&lca_id='+$('#lca_id').val()+'&spaj='+spaj+'&lstb_id='+lstb_id,'winView','width=1200, height=600');
	});
	
	$('#btnHist').click(function(){
		window.open('${path}/snows/snows.htm?window=hist&mi_id='+$('#hidNoInbox').val(),'winView','width=900, height=400');
	});
	
	// (jQueryUI datepicker) init semua field datepicker
	$(".tgl").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: "dd/mm/yy" 
	});
	
	$(".ajax-input").change(function(){
		var a = $(this).val();
		var id = $(this).attr('id');
		var tipe = $("#lstb_id").val();
		var url = '${path}/snows/snows.htm?window=ajax&pages=inputSnows&jn='+id+'&val='+a+'&lstb_id='+tipe;
		$.getJSON(url, function(result) {
			if(result.dataInputSnows){
				$('#lstb_id option[value='+result.dataInputSnows[0].LSTB_ID+']').attr('selected','selected');
				$("#noPolis").val(result.dataInputSnows[0].POLICY_FORMATTED);
		        $("#noSPAJ").val(result.dataInputSnows[0].SPAJ_FORMATTED);	
// 		        if(tipe==11){
// 		        	$("#ttg").val(result.dataInputSnows[0].TERTANGGUNG);
// 					$("#pp").val(result.dataInputSnows[0].PEMEGANG);		        
// 		        }else{
		        	$("#ttg").val(result.dataInputSnows[0].Tertanggung);
					$("#pp").val(result.dataInputSnows[0].Pemegang);
// 		        }
				$("#plan").val(result.dataInputSnows[0].LSDBS_NAME);
				$("#lsbs_id").val(result.dataInputSnows[0].LSBS_ID);
				$("#lca_id").val(result.dataInputSnows[0].LCA_ID);	
				$("#tgl_input").datepicker("setDate", new Date());	
// 				$("#desc").val(result.dataMstInbox[0].NOW);

				$('#jnsInbox option[value=""]').attr('selected','selected');
				$("#tgl_jt_tempo").val("");
				$("#reff").val("");
				$("#tgl_jt_tempo").val("");
				$("#tgl_jt_tempo").attr("readonly", false);
				$("#tgl_jt_tempo").attr("disabled", false);
				$("#tgl_jt_tempo").css("background-color", "#FFFFFF"); 
				$("#tgl_jt_tempo1").css("display", "none");
				$("#cekidot").css("display", "none");
				$("#warningjt").css("display", "none");
				
// 				$("#btnEdit").removeAttr('disabled');
				$("#btnSave").removeAttr('disabled');
// 				$("#btnDel").removeAttr('disabled');
// 				$("#btnUpl").removeAttr('disabled');
// 				$("#btnTrans").removeAttr('disabled');
// 				$("#btnView").removeAttr('disabled');
// 				$("#btnHist").removeAttr('disabled');
				if(result.pesan) alert(result.pesan);
			}else if(result.pesan){
				alert(result.pesan);
				$("#noPolis").val('');
		        $("#noSPAJ").val('');	
				$("#ttg").val('');
				$("#pp").val('');
				$("#plan").val('');
				$("#lsbs_id").val('');
				$("#lca_id").val('');	
				$("#txtCari").val('');
			}
		});				
	});
	
	$(".ajax-select").change(function(){
		$('#checklist td').parent().remove();
		var a = $(this).find('option:selected').val();
		var noSPAJ = $("#noSPAJ").val();
		var id = $(this).attr('id');
		
		if(noSPAJ==null || noSPAJ==""){
			alert("Harap input NO POLIS atau NO SPAJ terlebih dahulu");
			return;
		}
		
		$("#tgl_jt_tempo").val("");
		$("#reff").val("");
		$("#tgl_jt_tempo").attr("readonly", false);
		$("#tgl_jt_tempo").attr("disabled", false);
		$("#tgl_jt_tempo").css("background-color", "#FFFFFF"); 
		$("#tgl_jt_tempo1").css("display", "none");
		$("#cekidot").css("display", "none");
		$("#warningjt").css("display", "none");
				
		if(a==27){
			popWin('${path}/snows/snows.htm?window=listWithdrawSlink&noSPAJ='+noSPAJ, 500, 700);
		}else if(a==26){
			alert("Anda yakin ingin melanjutkan proses pencarian seluruhnya ?");
		}/* else if(a==33){
			popWin('${path}/snows/snows.htm?window=listReturSlink&noSPAJ='+noSPAJ, 500, 700);
		}else if(a==28){
			popWin('${path}/snows/snows.htm?window=listEndorsSlink&noSPAJ='+noSPAJ, 500, 700);
		} */
		var ljj_id = $("#jnsInbox").val();
		var url = '${path}/snows/snows.htm?window=ajax&pages=inputSnows&jn=jnsInbox&ljj_id='+a+'&noSPAJ='+noSPAJ;
	    $.getJSON(url, function(result) {
			$.each(result.checkList, function(id,val) {
				$('<tr>').append(
		        $('<td>').html('<input type="checkbox" value='+val.LC_ID+' name="chkBox" class="chBox" style="width:30px;">'), // '+ (val.PRIORITY==2?'checked':'') +' // auto checked
		        $('<td>').html(val.LC_NAMA + ' ' + (val.PRIORITY==2?'<font color=red>*</font>':'')),
		        $('<td>').html('<input type="text" class="descBox" id=desc_'+val.LC_ID+' name=desc_'+val.LC_ID+'>')).appendTo('#checklist');
			});
			$.each(result.jenisCase, function(id,val) {
				if(val.peringatan.length>0){ 
					if(ljj_id!=23)
					alert(val.peringatan);
				}else{
				
				if((ljj_id>=5 && ljj_id<=10) || (ljj_id>=13 && ljj_id<=18)){ //tahapan & simpanan
					$("#tgl_jt_tempo").attr("readonly", true);
					$("#tgl_jt_tempo").attr("disabled", true);
					$("#tgl_jt_tempo").css("background-color", "#E0E0E0"); 
					
					var text = '<select name="tgl_jt_tempo1" onchange="test(1)" id="tgl_jt_tempo1"';
					text+='>';
					text += '<option value=""';
					text += '></option>';		
					for(i = 0; i < val.pilihan.length; i++) {
						text += '<option value="'+val.pilihan[i].JT_TEMPO+'"';
						text += '>'+val.pilihan[i].JT_TEMPO+'</option>';
					}
					text += '</select> <span id="warningjt"><font color="red"> *harap pilih tgl jatuh tempo</font></span>';
					
					if(document.getElementById("jtempo")){
						document.getElementById("jtempo").innerHTML=text;
					}
				}else if(ljj_id==23){ //Withdraw Unit Link
					$("#cekidot").css("display", "block");
										
					var text = '<select name="mu_ke" onchange="test(2)" id="mu_ke"';
					text+='>';
					text += '<option value=""';
					text += '></option>';	
					for(i = 0; i < val.pilihanWU.length; i++) {
						text += '<option value="'+val.pilihanWU[i].MU_KE+'"';
						text += '>'+val.pilihanWU[i].MU_KE+'</option>';
					}
					text += '</select> <span id="warningjt"><font color="red"> *pilih unit link yang ke berapa</font></span>';
					
					if(document.getElementById("mu_ke_wul")){
						document.getElementById("mu_ke_wul").innerHTML=text;
					}
				}else{ //lainnya
					$("#tgl_jt_tempo").val(val.jt_tempo);
					$("#reff").val(val.no_reff);
				}
				}
			});
			
		});	
	});
	
// 	$("form").submit(function(e){
// 	  e.preventDefault();
// 	});
	
	$(".dataInbox").click(function(){
		var mi_id = $(this).attr('id');
		var url = '${path}/snows/snows.htm?window=ajax&pages=inputSnows&jn=showData&mi_id='+mi_id;
		$('#tbChecklist').empty();
		$.getJSON(url, function(result) {
			$('#lstb_id option[value='+result.dataMstInbox[0].lstb_id+']').attr('selected','selected');
			$('#jnsInbox option[value='+result.dataMstInbox[0].LJJ_ID+']').attr('selected','selected');
			$("#noPolis").val(result.dataMstInbox[0].polis);
	        $("#noSPAJ").val(result.dataMstInbox[0].SPAJ_FORMATTED);	
			$("#ttg").val(result.dataMstInbox[0].Tertanggung);
			$("#pp").val(result.dataMstInbox[0].Pemegang);
// 	        if(tipe==11){
// 				$("#ttg").val(result.dataMstInbox[0].TERTANGGUNG);
// 				$("#pp").val(result.dataMstInbox[0].PEMEGANG);		        
// 	        }else{
				$("#ttg").val(result.dataMstInbox[0].Tertanggung);
				$("#pp").val(result.dataMstInbox[0].Pemegang);
// 	        }
			$("#plan").val(result.dataMstInbox[0].LSDBS_NAME);
			$("#lsbs_id").val(result.dataMstInbox[0].LSBS_ID);
			$("#lca_id").val(result.dataMstInbox[0].LCA_ID);	
			$("#noInbox").val(result.dataMstInbox[0].MI_ID);	
			$("#hidNoInbox").val(result.dataMstInbox[0].MI_ID);
			checkTgl('tgl_berkas_masuk',result.dataMstInbox[0].TGL_BERKAS_MASUK);
			checkTgl('tgl_berkas_lengkap',result.dataMstInbox[0].TGL_BERKAS_LENGKAP);
			checkTgl('tgl_jt_tempo',result.dataMstInbox[0].TGL_JT_TEMPO);
			checkTgl('tgl_konfirmasi',result.dataMstInbox[0].TGL_KONFIRMASI);
			checkTgl('tgl_admin_terima',result.dataMstInbox[0].TGL_ADMIN_TERIMA);
			checkTgl('tgl_input',result.dataMstInbox[0].CREATE_DATE);
			$("#desc").val(result.dataMstInbox[0].NOW +' '+ result.dataMstInbox[0].MI_DESC);
			$("#pos").val("ADMIN");
			$("#reff").val(result.dataMstInbox[0].NO_REFF);
			$("#user").val(result.dataMstInbox[0].nama);
			$("#ljj_file").val(result.dataMstInbox[0].LJJ_FILE);
			$("#ljj_folder").val(result.dataMstInbox[0].LJJ_FOLDER);
		
			$("#btnEdit").removeAttr('disabled');
			$("#btnSave").attr('disabled','disabled');
			$("#btnDel").removeAttr('disabled');
			$("#btnUpl").removeAttr('disabled');
			$("#btnTrans").removeAttr('disabled');
			$("#btnView").removeAttr('disabled');
			$("#btnHist").removeAttr('disabled');
			$("#txtCari").val('');
			
			$.each(result.dataCheckList,function(id,val){
				if(val.chk){
					var ch = 'checked';
				}else{
					var ch = '';
				}
				$('<tr>').append(
					$('<td>').html('<input type="checkbox" value='+val.lc_id+' name="chkBox" '+ch+' class="chBox" style="width:30px;">'),
					$('<td>').text(val.lc_nama),
					$('<td>').html('<input type="text" class="descBox" id=desc_'+val.lc_id+' name=desc_'+val.lc_id+' value='+val.desc+' >')
				).appendTo('#tbChecklist');
			});
		});
	});
	
	$("#btnSearch").click(function(){
		var selC = $("#selCari").val();
		var txt = $("#txtCari").val().trim();
		var url = '${path}/snows/snows.htm?window=ajax&pages=inputSnows&jn=cariData&txt='+txt+'&selC='+selC;
		$('#tbChecklist').empty();
		$.getJSON(url,function(result){
			if(result.dataMstInbox){
// 				$( ".tgl" ).datepicker( "setDate", "01/01/1900" );
				$('#lstb_id option[value='+result.dataMstInbox[0].LSTB_ID+']').attr('selected','selected');
				$('#jnsInbox option[value='+result.dataMstInbox[0].LJJ_ID+']').attr('selected','selected');
				$("#noPolis").val(result.dataMstInbox[0].polis);
		        $("#noSPAJ").val(result.dataMstInbox[0].SPAJ_FORMATTED);	
// 		        if(tipe==11){
// 					$("#ttg").val(result.dataMstInbox[0].TERTANGGUNG);
// 					$("#pp").val(result.dataMstInbox[0].PEMEGANG);		        
// 		        }else{
					$("#ttg").val(result.dataMstInbox[0].Tertanggung);
					$("#pp").val(result.dataMstInbox[0].Pemegang);
// 		        }
				$("#plan").val(result.dataMstInbox[0].LSDBS_NAME);
				$("#lsbs_id").val(result.dataMstInbox[0].LSBS_ID);
				$("#lca_id").val(result.dataMstInbox[0].LCA_ID);	
				$("#noInbox").val(result.dataMstInbox[0].MI_ID);	
				$("#hidNoInbox").val(result.dataMstInbox[0].MI_ID);
				checkTgl('tgl_berkas_masuk',result.dataMstInbox[0].TGL_BERKAS_MASUK);
				checkTgl('tgl_berkas_lengkap',result.dataMstInbox[0].TGL_BERKAS_LENGKAP);
				checkTgl('tgl_jt_tempo',result.dataMstInbox[0].TGL_JT_TEMPO);
				checkTgl('tgl_konfirmasi',result.dataMstInbox[0].TGL_KONFIRMASI);
				checkTgl('tgl_admin_terima',result.dataMstInbox[0].TGL_ADMIN_TERIMA);
				checkTgl('tgl_input',result.dataMstInbox[0].CREATE_DATE);
				$("#desc").val(result.dataMstInbox[0].MI_DESC);
				$("#pos").val("ADMIN");
				$("#reff").val(result.dataMstInbox[0].NO_REFF);
				$("#user").val(result.dataMstInbox[0].nama);
				$("#ljj_file").val(result.dataMstInbox[0].LJJ_FILE);
				$("#ljj_folder").val(result.dataMstInbox[0].LJJ_FOLDER);
				
				$("#btnDel").removeAttr('disabled');
				$("#btnUpl").removeAttr('disabled');
				$("#btnTrans").removeAttr('disabled');
				$("#btnView").removeAttr('disabled');
				$("#btnHist").removeAttr('disabled');
				$("#btnSave").attr('disabled','disabled');
				$("#txtCari").val('');
				
				$.each(result.dataCheckList,function(id,val){
					if(val.chk){
						var ch = 'checked';
					}else{
						var ch = '';
					}
					$('<tr>').append(
						$('<td>').html('<input type="checkbox" value='+val.lc_id+' name="chkBox" '+ch+' class="chBox" style="width:30px;">'),
						$('<td>').text(val.lc_nama),
						$('<td>').html('<input type="text" class="descBox" id=desc_'+val.lc_id+' name=desc_'+val.lc_id+' >')
					).appendTo('#tbChecklist');
				});
			}else if(result.pesan){
				$('#formpost').trigger("reset");
				alert(result.pesan);
			}
		});
	});
});

	
function test(job){ 
	//job 1=tahapan/simpanan, 2=Withdrat Unit Link
	var noSPAJ = $("#noSPAJ").val();
	var a = $("#jnsInbox").val();
	if (job==1){
		var tgl = $("#tgl_jt_tempo1").val();
		var url = '${path}/snows/snows.htm?window=ajax&pages=inputSnows&jn=jnsInbox&ljj_id='+a+'&noSPAJ='+noSPAJ+'&tgl_jt_tempo='+tgl;
    }else if(job==2){
    	var mu_ke = $("#mu_ke").val();
    	var url = '${path}/snows/snows.htm?window=ajax&pages=inputSnows&jn=jnsInbox&ljj_id='+a+'&noSPAJ='+noSPAJ+'&mu_ke='+mu_ke;
    }
    $.getJSON(url, function(result) {
		$.each(result.jenisCase, function(id,val) {
			$("#reff").val(val.no_reff);
			$("#tgl_jt_tempo").val(val.jt_tempo);
		});
	});	
}	

function dialog(path){
	$(function(){
		$('.dialog').empty();
		var img = $('<img src="'+path+'" width="600px" />').appendTo('.dialog');
		$(".dialog").dialog({
			width: 640,
			modal: true
		});
	});
}

function checkTgl(target, tgl){
// 	$("#"+target+"").datepicker({ dateFormat: 'd/M/yy' });
	if(tgl == undefined){
		$("#"+target+"").datepicker( "setDate", "");
	}else{
		$("#"+target+"").datepicker( "setDate", tgl);
	}
}

function cek_tgl(){
	var ljj_id = document.getElementById("jnsInbox").value;
	
	if((ljj_id>=5 && ljj_id<=10) || (ljj_id>=13 && ljj_id<=18)){
		$("#tgl_jt_tempo").attr("disabled", false);
		if($("#reff").val()==""){
			alert("Tidak ada Nomor Reff untuk data ini, tidak dapat diinput.");
			return false;
		}
	}
	
	if(document.getElementById("tgl_berkas_masuk").value == ""){
			alert("Tanggal Berkas Masuk harus di isi !");
			return false;
	}
	
	if(document.getElementById("tgl_berkas_lengkap").value == ""){
			alert("Tanggal Berkas Lengkap harus di isi !");
			return false;
	}
		
			
	if(document.getElementById("tgl_konfirmasi").value == ""){
			alert("Tanggal Konfirmasi harus di isi !");
			return false;
	}
	
	if(document.getElementById("desc").value == ""){
				alert("Deskripsi harus di isi !");
				return false;
	}
		
}

function konfirm(){
	if(confirm("Yakin untuk DELETE Inbox ini?")){
		return true;
	}else{
		return false;
	}
}

hideLoadingMessage();
</script>
<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	input{width: 200px;}
	button{width:125px;height: 30px;}
	
	input[type="submit"], input[type="button"]{
		width:125px;
		height: 20px;
		border:1px solid red;
    	text-decoration:none;
	}
	
	.dataInbox{
		cursor:pointer;
		cursor: hand;
	}
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }
	
	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }
</style>
<body>
	<div id="tabs">
		<ul>
			<li><a href="#tab-1">INPUT</a></li>					
		</ul>
		<div id="tab-1">
			<form name="formpost" method="post" action="${path}/snows/snows.htm?window=inputSnows" id="formpost">
				<table class="entry2">
					<tr>
						<th style="width: 120px;" valign="top">
							<label>No. Inbox</label><br/>
							<label>No. Polis</label><br/><hr>
							<c:forEach items="${dataInbox}" var="d">
								<div class="dataInbox" id="${d.MI_ID}">
									<c:choose>
										<c:when test="${d.LSPD_ID_FROM ne null }">
											<span style="background-color: red; color: white;">
											${d.MI_ID} <br/>
											${d.polis } <br/><hr> </span>
										</c:when>
										<c:otherwise>
											${d.MI_ID} <br/>
											${d.polis } <br/><hr>
										</c:otherwise>
									</c:choose>
								</div>
							</c:forEach>
						</th>
						<td valign="top">
							<fieldset>
							<c:if test="${not empty pesan }">
							<div id="success">
								${pesan }
							</div>
							</c:if>
							<!-- untuk info polis -->
							<div id="f_input">
								<table>
									<tr>
										<td>
											<select id="selCari" name="selCari">
												<option value="MI_ID">NO INBOX</option>
												<option value="POLIS">NO POLIS</option>
											</select> &nbsp; &nbsp;
											<input type="text" id="txtCari" name="txtCari"> &nbsp; &nbsp;
											<input type="button" id="btnSearch" name="btnSearch" value="Search">
										</td>
<!-- 										<td><input type="text" id="txtCari" name="txtCari"></td> -->
<!-- 										<td><button id="btnSearch" name="btnSearch">Search</button></td> -->
									</tr>
									<tr>
										<td>
											<legend>Detail Polis</legend>
											<table>
											<tr>
												<th>Tipe Bisnis</th><td>:</td>
												<td>
													<select id="lstb_id" name="lstb_id">
														<option value="1">Individu</option>
														<option value="2">MRI</option>
														<option value="11">DMTM</option>
													</select>
		<!-- 											<input type="text" id="tipe" name="tipe" disabled="disabled"> -->
													<input type="hidden" id="lca_id" name="lca_id" disabled="disabled">
													<input type="hidden" id="mi_id" name="mi_id" disabled="disabled">
													<input type="hidden" id="ljj_file" name="ljj_file">
													<input type="hidden" id="ljj_folder" name="ljj_folder">
												</td>
											</tr>
											<tr>
												<th>No Polis</th><td>:</td><td><input type="text" id="noPolis" name="noPolis" class="ajax-input"></td>
												<th>No SPAJ</th><td>:</td><td><input type="text" id="noSPAJ" name="noSPAJ" class="ajax-input"></td>
											</tr>
											<tr>
												<th>Nama Tertanggung</th><td>:</td><td><input type="text" id="ttg" name="ttg" readonly="readonly" style="background-color: #E0E0E0;"></td>
											</tr>
											<tr>
												<th>Nama Pemegang</th>
												<td>:</td>
												<td colspan="3"><input type="text" id="pp" name="pp" readonly="readonly" style="background-color: #E0E0E0;"></td>
											</tr>
											<tr>
												<th>Plan/Bisnis</th>
												<td>:</td>
												<td><input type="text" id="plan" name="plan" readonly="readonly" style="background-color: #E0E0E0;"> <input type="text" id="lsbs_id" name="lsbs_id" readonly="readonly" style="width:30px;background-color: #E0E0E0;"></td>
											</tr>
										</table> <br/>
										<legend>Entry Task</legend>
										<table>
											<tr>
												<th>No. Inbox</th>
												<td>:</td>
												<td>
													<input type="text" id="noInbox" name="noInbox" readonly="readonly" style="background-color: #E0E0E0;">
													<input type="hidden" id="hidNoInbox" name="hidNoInbox">
												</td>
											</tr>
											<tr>
												<th>Jenis Inbox</th>
												<td>:</td>
												<td>
													<select id="jnsInbox" name="jnsInbox" class="ajax-select">
														<option value="" selected="selected"></option>
														<c:forEach items="${jnsInput}" var="i">
															<option value="${i.key}">${i.value }</option>
														</c:forEach>
													</select>
												</td>
											</tr>
											<tr id="cekidot" style="display:none;"> 	
												<th>UNIT LINK KE</th>
												<td>:</td>
												<td><span id="mu_ke_wul"></span></td>
											</tr>
											<tr>
												<th>Tgl Berkas Masuk</th><td>:</td><td><input type="text" class="tgl" id="tgl_berkas_masuk" name="tgl_berkas_masuk"></td>
											</tr>
											<tr>
												<th>Tgl Berkas Lengkap</th><td>:</td><td><input type="text" class="tgl" id="tgl_berkas_lengkap" name="tgl_berkas_lengkap" ></td> 
											</tr>
											<tr>
												<th>Tgl Jt Tempo</th><td>:</td>
											    <td><input type="text" class="tgl" id="tgl_jt_tempo" name="tgl_jt_tempo" readonly="readonly" style="width:104px; background-color: #E0E0E0;">&nbsp;&nbsp; 
											    <span id="jtempo"></span></td>
											</tr>
											<tr>
												<th>Tgl konfirmasi</th><td>:</td><td><input type="text" class="tgl" id="tgl_konfirmasi" name="tgl_konfirmasi"></td>
											</tr>
											<tr>
												<th>Tgl Terima Admin</th>
												<td>:</td>
												<td><input type="text" class="tgl" id="tgl_admin_terima" name="tgl_admin_terima"></td>
											</tr>
											<tr>
												<th>Description</th>
												<td>:</td>
												<td><textarea name="desc" id="desc" cols="50" rows="3"></textarea></td>
											</tr>
											<tr>
												<th>Current Position</th>
												<td>:</td>
												<td><input type="text" name="pos" id="pos" readonly="readonly" style="background-color: #E0E0E0;"></td>
											</tr>
											<tr>
												<th>Tgl Input</th>
												<td>:</td>
												<td><input type="text" class="tgl" name="tgl_input" id="tgl_input" disabled="disabled" style="background-color: #E0E0E0;"></td>
											</tr>
											<tr>
												<th>No. Reff</th><td>:</td><td><input type="text" name="reff" id="reff" readonly="readonly" style="background-color: #E0E0E0;"></td>
											</tr>	
											<tr>
												<th>User</th><td>:</td><td><input type="text" name="user" id="user" readonly="readonly" value="${sessionScope.currentUser.lus_full_name}" style="background-color: #E0E0E0;"></td>
											</tr>							
										</table> <br/>
										<legend>Checklist</legend>
										<table id="checklist">
											<tr>
												<th>&nbsp;</th>
												<th>Checklist</th>
												<th>Description</th>
											</tr>
											<tbody id="tbChecklist"></tbody>
										</table>
										</td>
										<td>
										<fieldset>
											<input type="submit" id="btnNew" name="btnNew" value="New / Refresh"> &nbsp;&nbsp;
											<input type="submit" id="btnEdit" name="btnEdit" value="Update" onClick="return cek_tgl();"><br/><br/>
											<input type="submit" id="btnSave" name="btnSave" value="Save" onClick="return cek_tgl();"> &nbsp;&nbsp;
											<input type="submit" id="btnDel" name="btnDel" value="Delete" onClick="return konfirm();"><br/><br/>
											<input type="button" id="btnUpl" name="btnUpl" value="Upload"> &nbsp;&nbsp;
											<input type="button" id="btnTrans" name="btnTrans" value="Transfer"><br/><br/>
											<input type="button" id="btnView" name="btnView" value="Viewer"> &nbsp;&nbsp;
											<input type="button" id="btnHist" name="btnHist" value="History"><br/><br/>
										</fieldset>
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>