<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen"> <!-- CSS untuk DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script><!-- DatePicker Script (jscalendar) -->
	</head>
	<script>
		function cari(){
			tgl1=formpost.tanggalAwal.value;
			tgl2=formpost.tanggalAkhir.value;
			jenisReport=formpost.jenisReport.value;
			
			//if(jenisReport == "tanggal_prod"){
			//	alert("Mohon tidak menekan tombol cari lebih dari satu kali.\nReport Berdasarkan Tanggal Produksi membutuhkan waktu 5-10 menit PER BULAN untuk proses retrieve data.\nTerima kasih");
			//}
			document.getElementById('infoFrame').src='${path}/common/util.htm?window=data_nasabah&jenisReport='+jenisReport+'&tanggalAwal='+tgl1+'&tanggalAkhir='+tgl2;
		}
		
		function awal(){
			hideLoadingMessage();
			setFrameSize('infoFrame', 110);
		}
	</script>
<body onload="awal();setupPanes('container1','tab1');" onresize="setFrameSize('infoFrame', 110);" style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Report Khusus Sinarmas Sekuritas</a>
				</li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form id="formpost" method="post" style="text-align: left;">
						<table class="entry2" style="width: 100%;">
							<tr>
								<th style="width: 150px;"> Tanggal : </th>
								<td>
									<script>inputDate('tanggalAwal', '${tglAwal}', false);</script>
									 s/d 
									 <script>inputDate('tanggalAkhir', '${tglAkhir}', false);</script>
									 <input type="hidden" name="Cari" value="Cari"><input type="button" name="btnCari" value="Cari" onClick="cari();">
								</td>
							</tr>
							<tr></tr>
							<tr>
								<th> Jenis Report : </th>
								<td>
									<select name="jenisReport">
										<option value="tanggal_input">Data Nasabah berdasarkan Tanggal Input</option>
										<option value="tanggal_efektif">Data Nasabah berdasarkan Tanggal Efektif Penempatan</option>
										<option value="tanggal_prod">Data Nasabah berdasarkan Tanggal Produksi</option>
										<option value="jatuh_tempo">Jatuh Tempo</option>
										<option value="roll_over">Rollover berdasarkan Tanggal Produksi</option>
										<option value="cair">Sudah pencairan</option>
									</select>
								</td>
							</tr>
							<tr width="100%">
								<td colspan="5">
									<iframe src="" name="infoFrame" id="infoFrame"
									width="100%" > Please Wait... </iframe>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>