<%@ include file="/include/page/header_mall.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<%User currentUser = (User) session.getAttribute("currentUser");%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>

 
<script type="text/javascript">
	hideLoadingMessage();
	
	function update(){
		var msp_id = document.getElementById('msp_id').value;
		document.getElementById('refreshPage').href='input_fire.htm?msp_id='+msp_id;
		document.getElementById('refreshPage').click();
		
	}
	
	function insert(){
		document.frmParam.kata.value = 'insert';
		if(trim(document.frmParam.kata.value)=='') return false;
		else createLoadingMessage();	
	}
	
	function cekElseEnvir(){
		if(document.getElementById('msp_fire_insured_addr_envir').value == '5'){
			document.getElementById('msp_fire_ins_addr_envir_else').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_ins_addr_envir_else').style.visibility = "hidden";
		}
	}
	
	function cekElseOkupasi(){
		if(document.getElementById('msp_fire_okupasi').value == 'L'){
			document.getElementById('msp_fire_okupasi_else').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_okupasi_else').style.visibility = "hidden";
		}
	}
	
	function bodyOnLoad(){
		cekElseEnvir();
		cekElseOkupasi();
	}
	
</script>
</head>

<body onload="resizeCenter(750,600);bodyOnLoad();" style="height: 100%;">
<XML ID=xmlData></XML>


			<div id="pane1" class="panes">
<form:form commandName="cmd" name="frmParam" id="frmParam" method="post">
					<fieldset>
					   <legend>ACTION</legend>
					   <table class="result_table2">
					   <tr><td colspan="2" style="border: 0px;">
					   <spring:bind path="cmd.*">
			<c:if test="${not empty status.errorMessages}">
				<div id="error">ERROR:<br>
				<c:forEach var="error" items="${status.errorMessages}">
	- <c:out value="${error}" escapeXml="false" />
					<br />
				</c:forEach></div>
			</c:if>
		</spring:bind>	</td></tr>
						<tr>
							<td colspan="2" style="text-align: center;">
								<input type="hidden" name="msp_id" id="msp_id" value="${msp_id}" />
								<input type="button" name="edit" value="Edit" style="visibility: hidden;width: 1px" onclick="return update();"/>
								<input type="submit" name="save" value="Save"  onclick="return insert();" accesskey="S" />
								<a href="input_fire.htm" id="refreshPage"></a>
							</td>
						</tr>
					</table>
					</fieldset>
					<fieldset>
	   <legend>ASURANSI KEBAKARAN</legend>
	   <table class="result_table2" cellspacing="3">
	   <c:if test="${currentUser.lca_id eq '58'}">
	   					<tr>
							<td>Appointment Id:</td>
							<td colspan="3"><form:input path="mspo_plan_provider" size="50" cssErrorClass="inpError" readonly="readonly"/><font class="error">*</font></td>
						</tr>
						</c:if>
	   					<tr>
							<th colspan="4">INFORMASI UTAMA</th>
						</tr>
						<tr>
							<td>Nama Tertanggung:</td>
							<td colspan="3">
								<select name="msp_fire_code_name">
									<c:forEach var="kn" items="${kode_nama_list}"> 
										<option <c:if test="${cmd.msp_fire_code_name eq kn.ID}"> SELECTED </c:if> value="${kn.ID}">${kn.NAMA}
										</option>
									</c:forEach> 
								</select>
								<form:input path="msp_fire_name" size="50" cssErrorClass="inpError"/><font class="error">*</font>
							</td>
						</tr>
						<tr>
							<td>Nomor KTP:</td>
							<td colspan="3"><form:input path="msp_fire_identity" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td>Tanggal Lahir:</td>
							<td colspan="3">
								<spring:bind path="cmd.msp_fire_date_of_birth2" >
        		                    <script>
                                        var isDisabled;
                                        var status = '${statusForm}';
                                        if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
                                <font color="#CC3300">*</font></spring:bind>
							</td>
						</tr>
						<tr>
							<td>Jenis Pekerjaan:</td>
							<td>
								<select name="msp_fire_occupation2" onchange="cekElseOccupation();">
									<c:forEach var="pkj" items="${pekerjaan}"> 
										<option <c:if test="${cmd.msp_fire_occupation2 eq pkj.ID}"> SELECTED </c:if> value="${pkj.ID}">${pkj.PEKERJAAN}
										</option>
									</c:forEach> 
								</select><font class="error">*</font>	
							<form:input path="msp_fire_occupation" cssErrorClass="inpError"/><font class="error">*</font></td>
							<td>Bidang Usaha:</td>
							<td>
								<select name="msp_fire_type_business2" onchange="cekElseBusiness();">
									<c:forEach var="bin" items="${bidang_industri}"> 
										<option <c:if test="${cmd.msp_fire_type_business2 eq bin.ID}"> SELECTED </c:if> value="${bin.ID}">${bin.BIDANG}
										</option>
									</c:forEach> 
								</select><font class="error">*</font>
							<form:input path="msp_fire_type_business" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td>Sumber Dana:</td>
							<td colspan="3">
							<select name="msp_fire_source_fund2" onchange="cekElseSourceFund();">
									<c:forEach var="sd" items="${sumber_pendanaan}"> 
										<option <c:if test="${cmd.msp_fire_source_fund2 eq sd.ID}"> SELECTED </c:if> value="${sd.ID}">${sd.DANA}
										</option>
									</c:forEach> 
								</select><font class="error">*</font>
							<form:input path="msp_fire_source_fund" cssErrorClass="inpError" /></td>
						</tr>
						<tr>
							<td>Alamat Tertanggung:</td>
							<td colspan="3">
							<select name="msp_fire_addr_code">
									<c:forEach var="ka" items="${kode_alamat_list}"> 
										<option <c:if test="${cmd.msp_fire_addr_code eq ka.ID}"> SELECTED </c:if> value="${ka.ID}">${ka.ALAMAT}
										</option>
									</c:forEach> 
							</select>
							<form:input path="msp_fire_address_1" size="70" cssErrorClass="inpError"/><font class="error">*</font>
							</td>
						</tr>
						<tr>
							<td>kode pos:</td>
							<td colspan="3"><form:input path="msp_fire_postal_code" maxlength="5" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td>Telp:</td>
							<td><form:input path="msp_fire_phone_number" cssErrorClass="inpError"/><font class="error">*</font></td>
							<td>Hp:</td>
							<td><form:input path="msp_fire_mobile" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td>Alamat Email:</td>
							<td colspan="3"><form:input path="msp_fire_email" size="60" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th colspan="4">INFORMASI OBYEK PERTANGGUNGAN</th>
						</tr>
						<tr>
							<td>Alamat Pertanggungan:</td>
							<td colspan="3">
							<select name="msp_fire_insured_addr_code">
									<c:forEach var="ka" items="${kode_alamat_list}"> 
										<option <c:if test="${cmd.msp_fire_insured_addr_code eq ka.ID}"> SELECTED </c:if> value="${ka.ID}">${ka.ALAMAT}
										</option>
									</c:forEach> 
							</select>
							<form:input path="msp_fire_insured_addr" size="60" cssErrorClass="inpError"/>
							No.<form:input path="msp_fire_insured_addr_no" maxlength="5" size="5" cssErrorClass="inpError"/>
							</td>
						</tr>
						<tr>
							<td>Kode Pos:</td>
							<td><form:input path="msp_fire_insured_postal_code" maxlength="5" cssErrorClass="inpError"/><font class="error">*</font></td>
							<td>Kota:</td>
							<td><form:input path="msp_fire_insured_city" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td>Telp:</td>
							<td colspan="3"><form:input path="msp_fire_insured_phone_number" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td>Jenis Bangunan Disekeliling Pertanggungan:</td>
							<td colspan="3">
								<select name="msp_fire_insured_addr_envir" onchange="cekElseEnvir();">
									<c:forEach var="kos" items="${kode_obyek_sekitar_list}"> 
										<option <c:if test="${cmd.msp_fire_insured_addr_envir eq kos.ID}"> SELECTED </c:if> value="${kos.ID}">${kos.OBYEK}
										</option>
									</c:forEach> 
								</select><br/>
								<form:input path="msp_fire_ins_addr_envir_else" size="80" cssErrorClass="inpError"/>
							</td>
						</tr>
						<tr>
							<td>Penggunaan Bangunan:</td>
							<td colspan="3">
								<select name="msp_fire_okupasi" onchange="cekElseOkupasi();">
									<c:forEach var="ko" items="${kode_okupasi_list}"> 
										<option <c:if test="${cmd.msp_fire_okupasi eq ko.ID}"> SELECTED </c:if> value="${ko.ID}">${ko.OKUPASI}
										</option>
									</c:forEach> 
								</select><br/>
								<form:input path="msp_fire_okupasi_else" size="80" cssErrorClass="inpError"/>
							</td>
						</tr>
						</table>
						</fieldset>
						<fieldset>
					   <legend>-</legend>
					   <table class="result_table2">
						<tr>
							<td colspan="2" align="left">
								<font class="error">* HARUS DIISI</font>
							</td>
						</tr>
						<tr>
							<td colspan="2" style="text-align: center;">
							<input type="hidden" name="kata" size="1" value="${kata}" />
								<input type="submit" name="save" value="Save"  onclick="return insert();" accesskey="S" />
						<input type="hidden" name="result" size="1" value="${result}" />
							</td>
						</tr>
					</table>
					</fieldset>
					</div>
</form:form>
	<c:if  test="${not empty successMessage}">	
			<script type="text/javascript">													
				alert('${successMessage}');	
				document.getElementById('refreshPage').click();
			</script>
	</c:if>
</body>
</html>