<%@ include file="/include/page/header.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />

<html>
<head>
<script language="JavaScript">
	var pesan = '${pesan}';
	if(pesan!=null && pesan!=''){
		alert(pesan);
	}
	
	var jmlhadiah=parseInt('${jml}');
	var flag_add1=0;
	
	function addRowDOM1 (tableID) {
		flag_add1=1;
   		var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
   		var tmp = 0;
   		var idx = null;
   		
		tmp = parseInt(document.getElementById("tableHadiah").rows.length);
		idx = jmlhadiah + 1;
   			
		var oRow = oTable.insertRow(oTable.rows.length);
        var oCells = oRow.cells;
		var i=tmp-1;
		var harga = document.getElementById('lh_harga').value;
		if (document.all) {
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td >"+idx+"</td>";
			
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td ><div id=hadiahmenu"+idx+"><input type=text name='hadiahharga"+idx+"' value='1' size='20'></div></td>";
			//<select name='hadiah.mh_no"+idx+"'><option>ALL</option></select>
			
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td ><input type=text name='hadiah.mh_quantity"+idx+"' value='1' size='3'></td>";         
			
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML ="<td ><input type=checkbox name=cek"+idx+" id=ck"+idx+"' class=\"noBorder\" ></td></tr>";
					
			var cell = oRow.insertCell(oCells.length);	
			jmlhadiah = idx;
			
			document.getElementById( 'jmlhadiah' ).value = jmlhadiah;
		};
		ajaxSelectHadiah(harga,'select_hadiah_ps_spesial', 'hadiahmenu'+idx, 'hadiah.mh_no'+idx,'hadiah.mh_no'+idx);
 	}
 	
 	function cancel1()
	{		
		var idx= null;
		idx = jmlhadiah;
		
		if  ( (idx)!="")
		{
			if (idx >0)
				flag_add1=1;
		}
		if(flag_add1==1)
		{
			deleteRowDOM1('tableHadiah');
		};
	}		
	
	function deleteRowDOM1 (tableID)
	 { 
		var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
	    var row=parseInt(document.getElementById("tableHadiah").rows.length);
		var flag_row=0;
		var jumlah_cek=0;
		var idx = jmlhadiah;
	
		if(row>0)
		{
			flag_row=0;
			for (var i=1;i<((parseInt(row)));i++)
			{
				if (eval("document.frmParam.elements['cek"+i+"'].checked"))
				{
					idx=idx-1;
					for (var k =i ; k<((parseInt(row))-1);k++)
					{
						eval(" document.frmParam.elements['hadiah.mh_no"+k+"'].value =document.frmParam.elements['hadiah.mh_no"+(k+1)+"'].value;");   
						eval(" document.frmParam.elements['hadiah.mh_quantity"+k+"'].value =document.frmParam.elements['hadiah.mh_quantity"+(k+1)+"'].value;"); 
						eval(" document.frmParam.elements['cek"+k+"'].checked = document.frmParam.elements['cek"+(k+1)+"'].checked;");
					}
					oTable.deleteRow(parseInt(document.getElementById("tableHadiah").rows.length)-1);
					row=row-1;
					//alert(mn);
					jmlhadiah = idx;
					//alert(mn);
					i=i-1;
					
					document.getElementById( 'jmlhadiah' ).value = jmlhadiah;
				}
			}
	
			row=parseInt(document.getElementById("tableHadiah").rows.length);
				if(row==1)	
					flag_add=0;
			
		}
	}
	
	function ajaxSelectHadiah(lh_harga,selectType, selectLocation, selectName, selectValue){
		ajaxManager.select_hadiah_ps_spesial(lh_harga,
			{callback:function(map) {
				DWRUtil.useLoadingMessage();
				if (map=='' || map==null){
					alert('Tidak ada data');
				}else{
					var text = '<select name="'+selectName+'">';
					for(var i=0 ; i<map.length ;i++){
						text += '<option value="'+map[i].LH_ID+'~'+map[i].LH_HARGA+'"';
						if(selectValue==map[i].LH_ID) text += ' selected ';
						text += '>'+map[i].LH_NAMA+' ('+(map[i].LH_HARGA).formatMoney(0, '.', ',')+')</option>';
					}
					text += '</select>';
					
					if(document.getElementById(selectLocation)){
						document.getElementById(selectLocation).innerHTML=text;
					}
				}
		   },
		  	timeout:60000,
		  	errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
		
	}
	
	Number.prototype.formatMoney = function(c, d, t){ 
		var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0; 
	   	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : ""); 
	 }; 
	 
	 function konfirm(){
		if(confirm("Pastikan sudah ada approval dari aktuari!")){
			return true;
		}else{
			return false;
		}
	}
	 
</script>
</head>

<body>
	<form name="frmParam" id="frmParam" method="post">
	<c:if test="${mode eq \"1\" }">
	<div id="ps_hadiah">
       		<table width="100%">
       			<tr > 
		            <th colspan="7" class="subtitle"  height="20"> <p align="center"><b> 
		                <font face="Verdana" size="1" color="#FFFFFF">KHUSUS STABLE SAVE BERHADIAH</font></b> </p>
		            </th>
		        </tr>
		        <tr> 
		            <th class="subtitle" align="center" colspan="7"> 
		             	<input name="btnadd1" type="button" id="btnadd1" value="ADD" onClick="addRowDOM1('tableHadiah')"> &nbsp;&nbsp;&nbsp;&nbsp; 
		             	<input name="btn_cancel1" type="button" id="btn_cancel1" value="DELETE" onClick="cancel1()">			
		            	<input type="hidden" name="jmlhadiah" id="jmlhadiah"  value="${jml}" >
		            	<input type="hidden" name="lh_harga" id="lh_harga"  value="${premi }" >
		            </th>
		        </tr>
		        <tr>
       				<th colspan="7">
		            	<div id="hadiah_2" style="display: block;">
       					<table ID="tableHadiah" width="100%" border="0" cellspacing="1" cellpadding="1" class="entry">
			       			<tr > 
			         			<th class="subtitle2" style="text-align: center;"><font size="1"><b><font face="Verdana" color="#FFFFFF">No</font></b></font></th>
			       				<th class="subtitle2" style="text-align: center;"><font size="1"><b><font face="Verdana" color="#FFFFFF">Hadiah</font></b></font><font color="#CC3300">*</font></th>
			         			<th class="subtitle2" style="text-align: center;"> <font size="1"><b><font face="Verdana" color="#FFFFFF">Unit</font><font color="#CC3300">*</font></b></font></th>
								<th class="subtitle2" style="text-align: center;">Cek</th>
							</tr>
			    			<c:forEach items="${hadiah}" var="hadiah" varStatus="status">
							<tr> 
				  				<th> ${status.count}</th>
				        		<th>
									<select name="hadiah.mh_no${status.index +1}">
										<c:forEach var="hdh" items="${select_hadiah}">
											<option 
												<c:if test="${hadiah.lh_id eq hdh.LH_ID}"> SELECTED </c:if>
												value="${hdh.LH_ID}~${hdh.LH_HARGA}">${hdh.LH_NAMA} (<fmt:formatNumber>${hdh.LH_HARGA}</fmt:formatNumber>)</option>
										</c:forEach>
									</select>
				   				</th>
								<th> 
									<input type="text" name="hadiah.mh_quantity${status.index +1}" value ="${hadiah.mh_quantity}" size="3" maxlength="3">
				        		</th>
				  				<th><input type=checkbox name="cek${status.index +1}" id= "ck${status.index +1}" class="noBorder" ></th>
				
							</tr>
							</c:forEach>		 
			  			</table>
			  			<input type="submit" id="btnSimpan" name="btnSimpan" title="Edit Hadiah" value="Edit" onclick="konfirm();">
			  			</div>
  				</th>
   			</tr>
   		</table>
	</div>
	</c:if>
	<c:if test="${mode eq \"0\" }">
		Tidak ada data.
	</c:if>
	</form>
</body>
</html>