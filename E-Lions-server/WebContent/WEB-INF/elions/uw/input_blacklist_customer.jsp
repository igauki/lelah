<%@ include file="/include/page/header.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<%User currentUser = (User) session.getAttribute("currentUser");%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>

<script type="text/javascript">

	function Body_onload() {
		var newStatus = document.getElementById('newStatus').value;
		var searchExistFlag = document.getElementById('searchExistFlag').value;
		if( newStatus == 'exist' ){
			objElems = formpost.elements;
  			for(i=0;i<objElems.length;i++){
  				//alert(objElems[i].name);
    			objElems[i].disabled = true;
    		}
    	}
    	
    	if( searchExistFlag == 'EXIST' ){
			objElems = formpost.elements;
  			for(i=0;i<objElems.length;i++){
  				//alert(objElems[i].name);
    			objElems[i].disabled = false;
    		}
    		fixedControl();
    	}
    	
    	if( newStatus != 'exist' && newStatus != 'new'){
    		fixedControl();
    	}
    	
    	formpost.elements['edit'].disabled = '';
    	if(newStatus == 'new'){
    		editControl();
    	}
    	if(document.getElementById('blacklist.lbl_alasan').disabled == true){
			formpost.elements['edit'].value = 'EDIT : OFF';
		}else if(document.getElementById('blacklist.lbl_alasan').disabled == false){
			formpost.elements['edit'].value = 'EDIT : ON';
		}
		
		enableDoc_Att_List();	
	
  	}
  	
  	function enableCariExisting(){
  		var statusValue = document.getElementById('blacklist.lbl_sts_cust').value;
  		if(statusValue == 2){
  			formpost.elements['searchExist'].style.visibility = 'visible';
  		}else{
  			formpost.elements['searchExist'].style.visibility = 'hidden';
  		}
  	}
		
	hideLoadingMessage();
	function awal(){
		<c:if test="${not empty param.m}">
			alert('Informasi: ${param.m}');
		</c:if>
		<c:if test="${s eq 1}">
			window.location='${path }/uw/input_blacklist_customer.htm?kopiMclId=${kopiMclId}&regSpaj=${regSpaj}&noPolis=${noPolis}&flagRefresh=refresh&searchExistFlag=${searchExistFlag}';
			alert('Informasi Data Attention List Berhasil Disimpan.');
		</c:if>
		<c:if test="${s eq 2}">
			window.location='${path }/uw/input_blacklist_customer.htm?kopiMclId=${kopiMclId}&regSpaj=${regSpaj}&noPolis=${noPolis}&flagRefresh=refresh&searchExistFlag=${searchExistFlag}';
			alert('Informasi Data Attention List Berhasil Diupdate.');
		</c:if>
		<c:if test="${s eq 3}">
			window.location='${path }/uw/input_blacklist_customer.htm?kopiMclId=${kopiMclId}&regSpaj=${regSpaj}&noPolis=${noPolis}&flagRefresh=refresh&searchExistFlag=${searchExistFlag}';
		</c:if>
		<c:if test="${s eq 4}">
			window.location='${path }/uw/input_blacklist_customer.htm?kopiMclId=${kopiMclId}&regSpaj=${regSpaj}&noPolis=${noPolis}&flagRefresh=refresh&searchExistFlag=${searchExistFlag}';
			alert('Informasi Data Attention List Gagal Disimpan.');
		</c:if>
		<c:if test="${s eq 5}">
			window.location='${path }/uw/input_blacklist_customer.htm?kopiMclId=${kopiMclId}&regSpaj=${regSpaj}&noPolis=${noPolis}&flagRefresh=refresh&searchExistFlag=${searchExistFlag}';
			alert('Informasi Data Attention List Gagal Diupdate.');
		</c:if>
		<c:if test="${s eq 6}">
			window.location='${path }/uw/input_blacklist_customer.htm?kopiMclId=${kopiMclId}&regSpaj=${regSpaj}&noPolis=${noPolis}&flagRefresh=refresh&searchExistFlag=${searchExistFlag}';
			alert('Upload dokumen PDF Gagal.');
		</c:if>		
		add_error();
	}
	
	function fixedControl(){
		var statusForm = document.getElementById('statusForm').value;
    	document.getElementById('blacklist.reg_spaj').disabled = true;
    	document.getElementById('blacklist.no_policy').disabled = true;
    	document.getElementById('blacklist.lbl_sts_cust').disabled = statusForm;
		document.getElementById('client.mcl_first').disabled = statusForm;
		document.getElementById('client.mspe_place_birth').disabled = statusForm;
		document.getElementById('_client.mspe_date_birth2').disabled = statusForm;
		document.getElementById('cowok').disabled = statusForm;
		document.getElementById('cewek').disabled = statusForm;
		//document.getElementById('client.lside_id').disabled = statusForm;
		if(document.getElementById('client.lside_id').value == 1 && document.getElementById('blacklist.lbl_no_identity') != null){
			document.getElementById('blacklist.lbl_no_identity').disabled = statusForm;
		}else if(document.getElementById('client.lside_id').value == 2 && document.getElementById('blacklist.lbl_no_identity_sim') != null){
			document.getElementById('blacklist.lbl_no_identity_sim').disabled = statusForm;
		}
		else if(document.getElementById('client.lside_id').value == 3 && document.getElementById('blacklist.lbl_no_identity_paspor') != null){
			document.getElementById('blacklist.lbl_no_identity_paspor').disabled = statusForm;
		}
		else if(document.getElementById('client.lside_id').value == 4 && document.getElementById('blacklist.lbl_no_identity_kk') != null){
			document.getElementById('blacklist.lbl_no_identity_kk').disabled = statusForm;
		}
		else if(document.getElementById('client.lside_id').value == 5 && document.getElementById('blacklist.lbl_no_identity_akte_lahir') != null){
			document.getElementById('blacklist.lbl_no_identity_akte_lahir').disabled = statusForm;
		}
		else if(document.getElementById('client.lside_id').value == 0 && document.getElementById('blacklist.lbl_no_identity_kims_kitas') != null){
			document.getElementById('blacklist.lbl_no_identity_kims_kitas').disabled = statusForm;
		}
		document.getElementById('client.mspe_sts_mrt').disabled = statusForm;
		document.getElementById('blacklistfamily.lblf_tgllhr_sis').disabled = statusForm;
		document.getElementById('client.mkl_kerja').disabled = statusForm;
		document.getElementById('addressNew.alamat_rumah').disabled = statusForm;
		document.getElementById('addressNew.area_code_rumah').disabled = statusForm;
		document.getElementById('addressNew.telpon_rumah').disabled = statusForm;
		document.getElementById('addressNew.no_hp').disabled = statusForm;
		document.getElementById('client.mspe_email').disabled = statusForm;
		document.getElementById('addressNew.alamat_kantor').disabled = statusForm;
		document.getElementById('addressNew.area_code_kantor').disabled = statusForm;
		document.getElementById('addressNew.telpon_kantor').disabled = statusForm;
		formpost.elements['searchExist'].style.disabled = statusForm;
		
		enableDoc_Att_List();
	}
	
	function editControl(){
    	if(formpost.elements['edit'].value == 'EDIT : OFF'){
    		objElems = formpost.elements;
  			for(i=0;i<objElems.length;i++){
    			objElems[i].disabled = false;
    		}
    		fixedControl();
    		formpost.elements['save'].disabled = '';
    		formpost.elements['edit'].value = 'EDIT : ON';
    		document.getElementById('dok_att_list').disabled = false;    		
    		document.getElementById('lbl_save_attentionlist').disabled = false;
    		document.getElementById('doc_att_list').disabled = false;
    		document.getElementById('downloadbutton').disabled = false;
    	}else if(formpost.elements['edit'].value == 'EDIT : ON'){
    		objElems = formpost.elements;
  			for(i=0;i<objElems.length;i++){
    			objElems[i].disabled = true;
    		}
    		formpost.elements['edit'].disabled = '';
    		formpost.elements['edit'].value = 'EDIT : OFF';
    		document.getElementById('dok_att_list').disabled = false;    		
    		document.getElementById('lbl_save_attentionlist').disabled = false;
    		document.getElementById('doc_att_list').disabled = false;
    		document.getElementById('downloadbutton').disabled = false;
    	}
	}
	
	function error_conf(){
		if(confirm("ADA INPUTAN YANG MASIH KOSONG!\n\nApakah Masih Ingin Lanjutkan Proses Save ?")){
			document.getElementById('flagConfSave').value = 'YES';
			document.getElementById('save').click();
		}
	}
	
	function add_error(){
		<c:if test="${addDuplicateError eq 'addDuplicateError'}">
			alert('ALERT!\nPROSES GAGAL DILAKUKAN\nInformasi Data Attention List Telah Ada Sebelumnya');
		</c:if>
	}
	
	function download(){
	 	var pdf =document.getElementById('dok_att_list').value;
	 	if(!pdf==''){
	 		//alert(pdf);		
    		objElems = formpost.elements;
  			for(i=0;i<objElems.length;i++){
    			objElems[i].disabled = false;
    		}    		
    		   	    	    		    
	 		document.getElementById('doc_att_list').value=pdf;
	 		document.getElementById('downloadbutton').click();
	 			 		
  			for(i=0;i<objElems.length;i++){
  				//alert(objElems[i].name);
    			objElems[i].disabled = true;
    		}    	   		
    		formpost.elements['kopiMclId'].disabled = false;
    		enableDoc_Att_List();
	 	}else alert('Tidak ada berkas PDF Attention List!');
	
	
	}

	function enableDoc_Att_List(){
		document.getElementById('dok_att_list').disabled = false;    		
    	document.getElementById('lbl_save_attentionlist').disabled = false;
    	document.getElementById('doc_att_list').disabled = false;
    	document.getElementById('downloadbutton').disabled = false;
    	formpost.elements['edit'].disabled = false;
    	}
	
//<!--
// -->
</script>
</head>

<body onload="Body_onload();awal();enableCariExisting();">
<XML ID=xmlData></XML>
<form:form commandName="cmd" name="formpost" id="formpost" method="post" enctype="multipart/form-data">
		<div id="contents">
		<c:if test="${addError != 'addError'}">
		<spring:bind path="cmd.*">
			<c:if test="${not empty status.errorMessages}">
				<div id="error">ERROR:<br>
				<c:forEach var="error" items="${status.errorMessages}">
					- <c:out value="${error}" escapeXml="false" />
					<br />
				</c:forEach></div>
			</c:if>
		</spring:bind>
		</c:if>
					<table class="entry">
						<tr>
							<th colspan=5 class="subtitle">DATA ATTENTION LIST CUSTOMER
							<div style="visibility: hidden">
								<input type="text" id="maxAlias" name="maxAlias" size="1" value="0"/>
								<input type="text" id="i_ktp" name="i_ktp" size="1" value="0"/>
								<input type="text" id="i_sim" name="i_sim" size="1" value="0"/>
								<input type="text" id="i_kk" name="i_kk" size="1" value="0"/>
								<input type="text" id="i_paspor" name="i_paspor" size="1" value="0"/>
								<input type="text" id="i_akte_lahir" name="i_akte_lahir" size="1" value="0"/>
								<input type="text" id="i_kims_kitas" name="i_kims_kitas" size="1" value="0"/>
								<input type="text" id="maxAlias" name="maxAlias" size="1" value="0"/>
								<input type="text" id="alias_1" name="alias_1" size="1" value="0"/>
								<input type="text" id="alias_2" name="alias_2" size="1" value="0"/>
								<input type="text" id="alias_3" name="alias_3" size="1" value="0"/>
								<input type="text" id="alias_4" name="alias_4" size="1" value="0"/>
								<input type="text" id="alias_5" name="alias_5" size="1" value="0"/>
								<input type="text" id="kopiMclId" name="kopiMclId" size="1" value="${kopiMclId}"/>
								<input type="text" id="regSpaj" name="regSpaj" size="1" value="${regSpaj}"/>
								<input type="text" id="noPolis" name="noPolis" size="1" value="${noPolis}"/>
								<input type="text" id="newStatus" name="newStatus" size="1" value="${newStatus}"/>
								<input type="text" id="searchExistFlag" name="searchExistFlag" size="1" value="${searchExistFlag}"/>
								<input type="text" id="statusForm" name="statusForm" size="1" value="${statusForm}"/>
								<input type="text" id="delIndex" name="delIndex" size="1" value=""/>
								<input type="text" id="flagSave" name="flagSave" size="1" value="${flagSave}"/>
								<input type="text" id="flagConfSave" name="flagConfSave" size="1" value=""/>
								<form:input path="client.mcl_id" size="1" maxlength="10" />
								<form:input path="blacklist.lbl_id" size="1" maxlength="10" />
								<form:input path="client.lside_id" size="1"/>
								<input type="submit" id="submitKopiMclId" name="submitKopiMclId" size="1" value="CARI"/>
							</div>
							</th>
						</tr>
						<c:if test="${cmd.blacklist.lbl_id != null}">
							<tr>
								<th width="18">*</th>
								<th width="287">Tgl. Input</th>
								<th colspan="3">
									<fmt:formatDate value="${cmd.blacklist.lbl_tgl_input}" pattern="dd/MM/yyyy" />
								</th>
							</tr>
							<tr>
								<th width="18">*</th>
								<th width="287">User</th>
								<th colspan="3">
									${cmd.blacklist.lus_login_name}
								</th>
							</tr>
						</c:if>
						<tr>
							<th width="18">1.</th>
							<th width="287">No. Attention List</th>
							<th colspan="3">
								<c:choose>
									<c:when test="${cmd.blacklist.lbl_id != null}">${cmd.blacklist.lbl_id}</c:when>
									<c:otherwise>-</c:otherwise>
								</c:choose>
							</th>
						</tr>
						<tr>
							<th width="18">2.</th>
							<th width="287">Sumber Informasi</th>
							<th colspan="3">
								<form:select path="blacklist.lbl_sumber_info" cssStyle="width: 100px;" >
									<form:option value="" label=""/>
									<form:option value="1" label="INTERNAL"/>
									<form:option value="2" label="EXTERNAL"/>
								</form:select>
								<font class="error">*</font>
								<form:select path="blacklist.lbl_sumber_info2" cssStyle="width: 100px;" >
									<form:option value="" label=""/>
									<form:option value="1" label="DEPT."/>
									<form:option value="2" label="PERORANGAN"/>
									<form:option value="3" label="PERUSAHAAN"/>
									<form:option value="4" label="PPATK"/>
									<form:option value="5" label="KPK"/>
									<form:option value="6" label="OJK"/>
									<form:option value="7" label="POLISI"/>
									<form:option value="8" label="LAIN-LAIN"/>																		
								</form:select>
								<font class="error">*</font>
								<form:input path="blacklist.lbl_sumber_informasi" size="42" maxlength="30" cssErrorClass="inpError" cssStyle="color : ${fontColor}"/>
								<font class="error">*</font>
							</th>
						</tr>
						<tr>
							<th>3.</th>
							<th>New Bisnis Dapat Diproses</th>
							<th width="400">
								<form:select path="blacklist.lbl_nb_process" cssStyle="width: 160px;">
									<form:option value="0" label="TIDAK"/>
									<form:option value="1" label="YA"/>
								</form:select>
								<font class="error">*</font>
							</th>
							<th width="210">Status Customer</th>
							<th width="307">
								<form:select path="blacklist.lbl_sts_cust" cssStyle="width: 160px;" disabled="${statusForm}" onchange="enableCariExisting();">
									<form:option value="" label=""/>
									<form:option value="1" label="CALON CUSTOMER"/>
									<form:option value="2" label="EXISTING CUSTOMER"/>
									<form:option value="3" label="LAIN-LAIN"/>
								</form:select>
								<font class="error">*</font>
								<input type="button" value="CARI" name="searchExist"
									onclick="popWin('${path}/uw/blacklistgutri.htm?posisi=1&win=edit&cari=exca', 350, 450); "
									accesskey="C" onmouseover="return overlib('Cari Existing Customer', AUTOSTATUS, WRAP);" onmouseout="nd();">
							</th>
						</tr>
						<tr>
							<th>4.</th>
							<th>No.Polis/No. SPAJ (jika ada)</th>
							<th colspan="3">
								<form:input path="blacklist.no_policy" size="24" maxlength="10" cssErrorClass="inpError" cssStyle="color : ${fontColor}" disabled="disabled"/> /
								<form:input path="blacklist.reg_spaj" size="24" maxlength="10" cssErrorClass="inpError" cssStyle="color : ${fontColor}" disabled="disabled"/>
							</th>
						</tr>
						<tr>
							<th>5.</th>
							<th>Nama lengkap</th>
							<th colspan="3">
								<form:input path="client.mcl_first" size="72" maxlength="60" cssErrorClass="inpError" cssStyle="color : ${fontColor}" disabled="${statusForm}"/>
								<font class="error">*</font>
							</th>
						</tr>
						<tr>
						<th>6.</th>
						<th colspan="4">
							<input type="submit" name="addnewalias" value="ADD"/> Nama Alias 
							<c:if test="${errAlias != ''}"><font color='${fontColor}'>${errAlias}</font></c:if>
						</th>
						</tr>
						<c:if test="${cmd.blacklist.lbl_nama_alias_1 != '' || alias_1 eq '1'}">
						<tr>
							<th>6.a.</th>
							<th>Nama Alias 1</th>
							<th colspan="3">
								<form:input path="blacklist.lbl_nama_alias_1" size="72" maxlength="60" cssErrorClass="inpError" cssStyle="color : ${fontColor}" />
								<font class="error"></font>
								<script>if(document.getElementById('blacklist.lbl_nama_alias_1').value == ' ')document.getElementById('blacklist.lbl_nama_alias_1').value='';
								document.getElementById('maxAlias').value='1';document.getElementById('alias_1').value='1';</script>
							</th>
						</tr>
						</c:if>
						<c:if test="${cmd.blacklist.lbl_nama_alias_2 != '' || alias_2 eq '1'}">
						<tr>
							<th>6.b.</th>
							<th>Nama Alias 2</th>
							<th colspan="3">
								<form:input path="blacklist.lbl_nama_alias_2" size="72" maxlength="60" cssErrorClass="inpError" cssStyle="color : ${fontColor}" />
								<font class="error"></font>
								<script>if(document.getElementById('blacklist.lbl_nama_alias_2').value == ' ')document.getElementById('blacklist.lbl_nama_alias_2').value='';
								document.getElementById('maxAlias').value='2';document.getElementById('alias_2').value='1';</script>
							</th>
						</tr>
						</c:if>
						<c:if test="${cmd.blacklist.lbl_nama_alias_3 != '' || alias_3 eq '1'}">
						<tr>
							<th>6.c.</th>
							<th>Nama Alias 3</th>
							<th colspan="3">
								<form:input path="blacklist.lbl_nama_alias_3" size="72" maxlength="60" cssErrorClass="inpError" cssStyle="color : ${fontColor}" />
								<font class="error"></font>
								<script>if(document.getElementById('blacklist.lbl_nama_alias_3').value == ' ')document.getElementById('blacklist.lbl_nama_alias_3').value='';
								document.getElementById('maxAlias').value='3';document.getElementById('alias_3').value='1';</script>
							</th>
						</tr>
						</c:if>
						<c:if test="${cmd.blacklist.lbl_nama_alias_4 != '' || alias_4 eq '1'}">
						<tr>
							<th>6.d.</th>
							<th>Nama Alias 4</th>
							<th colspan="3">
								<form:input path="blacklist.lbl_nama_alias_4" size="72" maxlength="60" cssErrorClass="inpError" cssStyle="color : ${fontColor}" />
								<font class="error"></font>
								<script>if(document.getElementById('blacklist.lbl_nama_alias_4').value == ' ')document.getElementById('blacklist.lbl_nama_alias_4').value='';
								document.getElementById('maxAlias').value='4';document.getElementById('alias_4').value='1';</script>
							</th>
						</tr>
						</c:if>
						<c:if test="${cmd.blacklist.lbl_nama_alias_5 != '' || alias_5 eq '1'}">
						<tr>
							<th>6.e.</th>
							<th>Nama Alias 5</th>
							<th colspan="3">
								<form:input path="blacklist.lbl_nama_alias_5" size="72" maxlength="60" cssErrorClass="inpError" cssStyle="color : ${fontColor}" />
								<font class="error"></font>
								<script>if(document.getElementById('blacklist.lbl_nama_alias_5').value == ' ')document.getElementById('blacklist.lbl_nama_alias_5').value='';
								document.getElementById('alias_5').value='1';</script>
							</th>
						</tr>
						</c:if>
						<tr>
							<th>7.</th>
							<th>Tempat lahir</th>
							<th>
								<form:input path="client.mspe_place_birth" size="42" maxlength="30" cssErrorClass="inpError" cssStyle="color : ${fontColor}" disabled="${statusForm}"/>
								<font class="error">*</font>
							</th>
							<th>Tanggal lahir</th>
							<th>
								<spring:bind path="cmd.client.mspe_date_birth2" >
        		                    <script>
                                        var isDisabled;
                                        var status = '${statusForm}';
                                        if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
                                <font color="#CC3300">*</font></spring:bind>
							</th>
						</tr>
						<tr>
							<th>8.</th>
							<th>Jenis kelamin</th>
							<th colspan="3">
								<label for="cowok">
									<form:radiobutton path="client.mspe_sex" id="cowok" value="1" cssClass="noBorder" disabled="${statusForm}"/> Pria
								</label>
								<label for="cewek">
									<form:radiobutton path="client.mspe_sex" id="cewek" value="0" cssClass="noBorder" disabled="${statusForm}"/> Wanita
								</label>
								<font class="error">*</font>
							</th>
						</tr>
						<tr>
						<th>9.</th>
						<th>
							<input type="submit" name="addnewidentitas" value="ADD"/> Bukti Identitas 
						</th>
						<th colspan="3">
							<select name="lside_id" id="lside_id">
								<option value=""></option>
								<option value="1">KTP</option>
								<option value="2">SIM</option>
								<option value="3">PASPOR</option>
								<option value="4">KK</option>
								<option value="5">AKTE LAHIR</option>
								<option value="7">KIMS/KITAS</option>
							</select>
							<c:if test="${errIdentitas != ''}"><font color='${fontColor}'>${errIdentitas}</font></c:if>
						</th>
						</tr>
						<c:if test="${cmd.client.lside_id eq '1' || cmd.blacklist.lbl_no_identity != '' || i_ktp eq '1'}">
						<tr>
							<th>9.-.</th>
							<th>KTP</th>
							<th colspan="3">No. 
								<form:input path="blacklist.lbl_no_identity" size="42" maxlength="30" cssErrorClass="inpError" cssStyle="color : ${fontColor}" />
								<font class="error"></font>
								<script>if(document.getElementById('blacklist.lbl_no_identity').value == ' ')document.getElementById('blacklist.lbl_no_identity').value='';
								document.getElementById('i_ktp').value='1';</script>
							</th>
						</tr>
						</c:if>
						<c:if test="${cmd.client.lside_id eq '2' || cmd.blacklist.lbl_no_identity_sim != '' || i_sim eq '1'}">
						<tr>
							<th>9.-.</th>
							<th>SIM</th>
							<th colspan="3">No. 
								<form:input path="blacklist.lbl_no_identity_sim" size="42" maxlength="30" cssErrorClass="inpError" cssStyle="color : ${fontColor}" />
								<font class="error"></font>
								<script>if(document.getElementById('blacklist.lbl_no_identity_sim').value == ' ')document.getElementById('blacklist.lbl_no_identity_sim').value='';
								document.getElementById('i_sim').value='1';</script>
							</th>
						</tr>
						</c:if>
						<c:if test="${cmd.client.lside_id eq '3' || cmd.blacklist.lbl_no_identity_paspor != '' || i_paspor eq '1'}">
						<tr>
							<th>9.-.</th>
							<th>PASPOR</th>
							<th colspan="3">No. 
								<form:input path="blacklist.lbl_no_identity_paspor" size="42" maxlength="30" cssErrorClass="inpError" cssStyle="color : ${fontColor}" />
								<font class="error"></font>
								<script>if(document.getElementById('blacklist.lbl_no_identity_paspor').value == ' ')document.getElementById('blacklist.lbl_no_identity_paspor').value='';
								document.getElementById('i_paspor').value='1';</script>
							</th>
						</tr>
						</c:if>
						<c:if test="${cmd.client.lside_id eq '4' || cmd.blacklist.lbl_no_identity_kk != '' || i_kk eq '1'}">
						<tr>
							<th>9.-.</th>
							<th>KK</th>
							<th colspan="3">No. 
								<form:input path="blacklist.lbl_no_identity_kk" size="42" maxlength="30" cssErrorClass="inpError" cssStyle="color : ${fontColor}" />
								<font class="error"></font>
								<script>if(document.getElementById('blacklist.lbl_no_identity_kk').value == ' ')document.getElementById('blacklist.lbl_no_identity_kk').value='';
								document.getElementById('i_kk').value='1';</script>
							</th>
						</tr>
						</c:if>
						<c:if test="${cmd.client.lside_id eq '5' || cmd.blacklist.lbl_no_identity_akte_lahir != '' || i_akte_lahir eq '1'}">
						<tr>
							<th>9.-.</th>
							<th>AKTE LAHIR</th>
							<th colspan="3">No. 
								<form:input path="blacklist.lbl_no_identity_akte_lahir" size="42" maxlength="30" cssErrorClass="inpError" cssStyle="color : ${fontColor}" />
								<font class="error"></font>
								<script>if(document.getElementById('blacklist.lbl_no_identity_akte_lahir').value == ' ')document.getElementById('blacklist.lbl_no_identity_akte_lahir').value='';
								document.getElementById('i_akte_lahir').value='1';</script>
							</th>
						</tr>
						</c:if>
						<c:if test="${cmd.client.lside_id eq '7' || cmd.blacklist.lbl_no_identity_kims_kitas != '' || i_kims_kitas eq '1'}">
						<tr>
							<th>9.-.</th>
							<th>KIMS/KITAS</th>
							<th colspan="3">No. 
								<form:input path="blacklist.lbl_no_identity_kims_kitas" size="42" maxlength="30" cssErrorClass="inpError" cssStyle="color : ${fontColor}" />
								<font class="error"></font>
								<script>if(document.getElementById('blacklist.lbl_no_identity_kims_kitas').value == ' ')document.getElementById('blacklist.lbl_no_identity_kims_kitas').value='';
								document.getElementById('i_kims_kitas').value='1';</script>
							</th>
						</tr>
						</c:if>
						
						<tr>
							<th>10.</th>
							<th>Status</th>
							<th colspan="3">						
								<form:select path="client.mspe_sts_mrt" cssStyle="width: 130px;" disabled="${statusForm}">
									<form:option value="" label=""/>
									<form:option value="1" label="BELUM MENIKAH"/>
									<form:option value="2" label="MENIKAH"/>
									<form:option value="3" label="CERAI"/>
								</form:select>
								<font class="error">*</font>
							</th>
						</tr>
						<tr>
							<th>a.</th>
							<th>Nama Suami/Istri</th>
							<th>
								<form:input path="blacklistfamily.lblf_nama_si" size="42" maxlength="30" cssErrorClass="inpError" cssStyle="color : ${fontColor}" />
							</th>
							<th>Tgl Lahir</th>
							<th>
								<spring:bind path="cmd.blacklistfamily.lblf_tgllhr_sis" >
									<script>
                                        var isDisabled;
                                        var status = '${statusForm}';
                                        //if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
								</spring:bind>
							</th>
						</tr>
						<tr>
							<th>b.</th>
							<th>Nama Anak 1</th>
							<th>
								<form:input path="blacklistfamily.lblf_nama_anak1" size="42" maxlength="30" cssErrorClass="inpError" cssStyle="color : ${fontColor}" />
							</th>
							<th>Tgl Lahir</th>
							<th>
								<spring:bind path="cmd.blacklistfamily.lblf_tgllhr_anak1s" >
									<script>
                                        var isDisabled;
                                        var status = '${statusForm}';
                                        //if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
								</spring:bind>
							</th>
						</tr>
						<tr>
							<th>c.</th>
							<th>Nama Anak 2</th>
							<th>
								<form:input path="blacklistfamily.lblf_nama_anak2" size="42" maxlength="30" cssErrorClass="inpError" cssStyle="color : ${fontColor}" />
							</th>
							<th>Tgl Lahir</th>
							<th>
								<spring:bind path="cmd.blacklistfamily.lblf_tgllhr_anak2s" >
									<script>
                                        var isDisabled;
                                        var status = '${statusForm}';
                                        //if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
								</spring:bind>
							</th>
						</tr>
						<tr>
							<th>d.</th>
							<th>Nama Anak 3</th>
							<th>
								<form:input path="blacklistfamily.lblf_nama_anak3" size="42" maxlength="30" cssErrorClass="inpError" cssStyle="color : ${fontColor}" />
							</th>
							<th>Tgl Lahir</th>
							<th>
								<spring:bind path="cmd.blacklistfamily.lblf_tgllhr_anak3s" >
									<script>
                                        var isDisabled;
                                        var status = '${statusForm}';
                                        //if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
								</spring:bind>
							</th>
						</tr>
						<tr>
							<th>11.</th>
							<th>Pekerjaan</th>
							<th colspan="3">
								<form:input path="client.mkl_kerja" size="66" maxlength="100" cssErrorClass="inpError" cssStyle="color : ${fontColor}" disabled="${statusForm}"/>
							</th>
						</tr>
						<tr>
							<th rowspan="3">12. a.</th>
							<th rowspan="3">Alamat rumah</th>
							<th rowspan="3">
								<form:textarea path="addressNew.alamat_rumah" cols="40" rows="5" onkeyup="textCounter(this, 200);" onkeydown="textCounter(this, 200);" cssErrorClass="inpError" cssStyle="color : ${fontColor}" disabled="${statusForm}"/>
								<font class="error">*</font>
							</th>
							<th>Telpon rumah</th>
							<th>
								<form:input path="addressNew.area_code_rumah" size="5" maxlength="4" cssErrorClass="inpError" cssStyle="color : ${fontColor}" disabled="${statusForm}"/>
								<form:input path="addressNew.telpon_rumah" size="16" maxlength="24" cssErrorClass="inpError" cssStyle="color : ${fontColor}" disabled="${statusForm}"/>
								<font class="error">*</font>
							</th>
						</tr>
						<tr>
							<th>HP</th>
							<th>
								<form:input path="addressNew.no_hp" size="24" maxlength="20" cssErrorClass="inpError" cssStyle="color : ${fontColor}" disabled="${statusForm}"/>
								<font class="error">*</font>
							</th>
						</tr>
						<tr>
							<th>Email</th>
							<th>
								<form:input path="client.mspe_email" size="42" maxlength="50" cssErrorClass="inpError" cssStyle="color : ${fontColor}" disabled="${statusForm}"/>
							</th>
						</tr>
						<tr>
							<th>12. b.</th>
							<th>Alamat kantor</th>
							<th>
								<form:textarea path="addressNew.alamat_kantor" cols="40" rows="5" onkeyup="textCounter(this, 200);" onkeydown="textCounter(this, 200);" cssErrorClass="inpError" cssStyle="color : ${fontColor}" disabled="${statusForm}"/>
							</th>
							<th>Telpon kantor</th>
							<th>
								<form:input path="addressNew.area_code_kantor" size="5" maxlength="4" cssErrorClass="inpError" cssStyle="color : ${fontColor}" disabled="${statusForm}"/>
								<form:input path="addressNew.telpon_kantor" size="16" maxlength="24" cssErrorClass="inpError" cssStyle="color : ${fontColor}" disabled="${statusForm}"/>
							</th>
						</tr>
						<tr>
						<th>13.</th>
						<th colspan="4">
							<input type="submit" name="addnewrow" value="ADD"/> Kejadian dan Diagnosa
						</th>
						</tr>
						<c:forEach var="detBlackList" items="${cmd.detBlackListAll}" varStatus="xt">
                                <tr>
							<th>13. ${xt.index + 1}.a.</th>
							<th>Tgl.kejadian/rawat</th>
							<th colspan="3">
								<spring:bind path="cmd.detBlackListAll[${xt.index}].ldbl_tgl_kejadian2" >
									<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
								<font color="#CC3300">*</font></spring:bind>
								s/d
								<spring:bind path="cmd.detBlackListAll[${xt.index}].ldbl_tgl_kejadian_to2" >
									<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
								<font color="#CC3300">*</font></spring:bind>
							</th>
						</tr>
						<tr>
							<th>13. ${xt.index + 1}.b.</th>
							<th>Diagnosa<br>
							<!-- <input type="submit" name="deloldrow" value="DELETE" onclick="document.getElementById('delIndex').value = '${xt.index}';"/> -->
							</th>
							<th colspan="3">
								<form:textarea path="detBlackListAll[${xt.index}].ldbl_diagnosa" cols="140" rows="5" onkeyup="textCounter(this, 600);" onkeydown="textCounter(this, 600);" cssErrorClass="inpError" cssStyle="color : ${fontColor}" />
								<font class="error">*</font>
							</th>
							</tr>
                        </c:forEach>
						<tr>
							<th>14.</th>
							<th>Keterangan / Alasan</th>
							<th colspan="3">
								<form:select path="blacklist.lbl_flag_alasan" cssStyle="width: 300px;" >
									<form:option value="" label=""/>
									<form:option value="1" label="OVERINSURED"/>
									<form:option value="2" label="CLAIM FRAUD"/>
									<form:option value="3" label="PEMALSUAN KUITANSI"/>
									<form:option value="4" label="DUGAAN TP (TINDAK PIDANA) KORUPSI"/>
									<form:option value="5" label="DUGAAN TP NARKOTIKA"/>
									<form:option value="6" label="ADA DI LIST TERORIS"/>									
									<form:option value="7" label="LAINNYA"/>
									<form:option value="8" label="SUAP"/>
								</form:select>
								<font class="error">*</font><br/>
								<form:textarea path="blacklist.lbl_alasan" cols="140" rows="20" onkeyup="textCounter(this, 1200);" onkeydown="textCounter(this, 1200);" cssErrorClass="inpError" cssStyle="color : ${fontColor}" />
								<font class="error">*</font>
							</th>
						</tr>
						<c:if test="${cmd.blacklist.lbl_id != null}">
						<tr>
							<th>15.</th>
							<th>Dokumen PDF Attention List</th>
							<th colspan="3">				
									<select name="dok_att_list" id="dok_att_list" >
									<c:forEach var="f" items="${daftarAda}"> 
										<option value="${f.key}">${f.key} [${f.value}]
										</option>
									</c:forEach> 
								</select>	
							 &nbsp; <img src="${path}/include/image/save.gif" id="lbl_save_attentionlist" style="cursor: pointer;" width="21" height="19"  border="0" title="DOWNLOAD PDF" onclick="download();"/>				
							 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;							
							 File Upload: <input type="file" name="file1" id="file1" value="${file1}" size="70" />
							 <input type="submit" name="upload" id="upload" value="Upload"  onclick="document.getElementById('doc_att_list').value=document.getElementById('file1').value"/>						  
							</th>
						</tr>
						</c:if>
						<tr>
				<th class="subtitle" colspan="5"></th>
			</tr>		
            <tr> 
            <td colspan="9"> 
			 <p align="left"><b> <font face="Verdana" size="2" color="#CC3300">Note : * Wajib diisi</font></b></p>
			</td>
          </tr>	
			<tr>
				<td colspan="5" style="text-align: center;">
					<div id="buttons">
						<input type="submit" name="save" value="SAVE"  style="width: 80; height:20"/>
						<input type="button" name="edit" value="EDIT : OFF" style="width: 80; height:20" onclick="editControl()"/>
						<input type="hidden" name="doc_att_list" id="doc_att_list" value="${doc_att_list}" />						
					</div>
					<div id="div_d" style="visibility: hidden;"><input type="submit" name="downloadbutton" id="downloadbutton" value="DOWNLOAD"></div>	
				 </td>
			</tr>
			<c:if test="${addError != 'addError'}">
			<spring:bind path="cmd.*">
			<c:if test="${not empty status.errorMessages}">
				<script>
				error_conf();		
				</script>
			</c:if>
		</spring:bind>	
		</c:if>
	
					</table>
					</div>	
			
</form:form>
</body>
</html>