<%@ include file="/include/page/header.jsp"%>
<script>
 function cek(spaj){
		if(spaj == ''){
			alert('Harap pilih SPAJ terlebih dahulu!');
			return false;
		}else{
			return true;
		}
	}
	
	function buttonLink(b,spaj){
		 if(b=='medis'){
			document.getElementById('infoFrame').src='${path }/uw/endorsenonmaterial.htm?window=infomedis&spaj='+spaj;
	
		}
	}
	
	function awal(){
		setFrameSize('infoFrame', 45);
		setFrameSize('docFrame', 45);
	}
	   
 /*  function buttonLink(b){
        if(b=='editblanko'){
            document.getElementById('infoFrame').src='${path}/uw/uw.htm?window=editblanko&spaj='+spaj;
         }else if(b=='addressbilling'){
                if(cek(spaj))get('infoFrame').src=document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=addressbilling&spaj='+spaj;
        }else if(b=='y'){
                document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=addressbilling&spaj='+spaj;
        }else if(b=='aw'){
                document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=addressbilling&spaj='+spaj;
        }else if(b=='i'){
            if(cek(spaj))
            if(document.getElementById('infoFrame')){
                document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj;
                document.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+spaj;
            }
         }
    }
    
    function awal(){
        setFrameSize('infoFrame', 45);
        setFrameSize('docFrame', 45);
    }
     */
    
</script>
<body onload="awal();" onresize="setFrameSize('infoFrame', 45); setFrameSize('docFrame', 45);" style="height: 100%;">
<c:choose>    
    <c:when test="${not empty main}">
        <div id="error">
            Maaf, Anda tidak memiliki akses untuk menu ini
        </div>
    </c:when>
    <c:when test="${not empty product}">
        <div id="error">
            Menu Hanya Bisa Di gunakan Untuk Product Bancassurance<br>
        
        </div>
    </c:when>
    <c:otherwise>
        <form:form id="formpost" name="formpost" commandName="cmd" method="post">
             <fieldset>
                 <table class="entry2">
                    <tr>
                            <th>No. Polis</th>
                            <th><input readonly type=text size=25 value="<elions:spaj nomor="${polis}"/>"></th>
                            <th>No. SPAJ</th>
                            <th><input readonly type=text size=20 value="<elions:spaj nomor="${param.spaj}"/>"></th>
                            <th>Posisi Document</th>
                            <th><input readonly type=text size=35 value="<elions:spaj nomor="${posisi}"/>"></th>
                            <th>Pemegang</th>
                            <th><input type="text" value="${pemegang.mcl_first}" size="33" readonly>
                                <img style="border:none;" alt="Refresh" src="${path}/include/image/action_refresh.gif" onclick="history.go(); return false;"></th>
                    </tr>
            </table>
        </fieldset>
  </form:form>
    <form name="formpost" method="post">
        <fieldset>
        <div class="">
            <table class="entry2" style="width: 98%;">
                <tr  align = "left">
                <th>Pilih Jenis Proses Endorse :
                    <th>
                        <input type="hidden" name="koderegion">
                        <input type="hidden" name="kodebisnis">
                        <input type="hidden" name="numberbisnis">
                        <input type="hidden" name="jml_peserta">
                       <!--<select name="endorse" onchange="tampol(this.value);">
                                <option value=""> [- Silakan Pilih Jenis Proses Endorse -] </option>
                                <c:forEach items="${lsEndorse}" var="d">
                                    <option value="${d.key}" 
                                        <c:if test="${d.key eq jenis}">selected</c:if>
                                    >${d.value}</option>
                                </c:forEach>
                            </select> -->
                        <input type="button" value="Blanko" name="blanko" disable="true"
                        onclick="popWin('${path}/uw/endorsenonmaterial.htm?window=editblanko&spaj=${param.spaj}', 400, 600); " accesskey="C"
                        onmouseover="return overlib('Edit No Blanko', AUTOSTATUS, WRAP);" onmouseout="nd();">
                        
                        <input type="button" value="Nama P.Polis" name="pemegang"
                        onclick="popWin('${path}/uw/endorsenonmaterial.htm?window=editnamapemegang&spaj=${param.spaj}', 400, 600); "accesskey="P"
                        onmouseover="return overlib('Edit Nama Pemegang Polis', AUTOSTATUS, WRAP);" onmouseout="nd();">
                        
                        <input type="button" value="Alamat Penagihan" name="penagihan"
                        onclick="popWin('${path}/uw/endorsenonmaterial.htm?window=addressbilling&spaj=${param.spaj}', 600, 750); "accesskey="N"
                        onmouseover="return overlib('Edit Alamat Penagihan', AUTOSTATUS, WRAP);" onmouseout="nd();">
                        
                        <input type="button" value="Alamat Pemegang " name="rumah"
                        onclick="popWin('${path}/uw/addressnew.htm?reg_spaj=${param.spaj};', 700, 750); "accesskey="H"
                        onmouseover="return overlib('Edit Alamat Rumah Pemegang', AUTOSTATUS, WRAP);" onmouseout="nd();">&nbsp;
                        
                        <input type="button" value="Ahli Waris" name="waris" 
                        onclick="popWin('${path}/uw/benefeciary.htm?reg_spaj=${param.spaj}',500, 850); "accesskey="K"
                        onmouseover="return overlib('Ahli Waris', AUTOSTATUS, WRAP);" onmouseout="nd();" disabled ="disabled">
                        
                        <input type="button" value="DOB Pemegang" name="tglpemegang"
                        onclick="popWin('${path}/uw/endorsenonmaterial.htm?window=edittglpemegang&spaj=${param.spaj}', 450, 600); "accesskey="L"
                        onmouseover="return overlib('Edit Tgl Lahir Pemegang', AUTOSTATUS, WRAP);" onmouseout="nd();">
                        
                        <input type="button" value="Info Medis" name="medis"
                        onclick="popWin('${path}/uw/endorsenonmaterial.htm?window=infomedis&spaj=${param.spaj}', 450, 600); "accesskey="L"
                        onmouseover="return overlib('Edit Info Medis', AUTOSTATUS, WRAP);" onmouseout="nd();"> 
                        
                        <input type="button" value="History" name="History"
                        onclick="popWin('${path}/uw/viewer/ulangan.htm?spaj=${param.spaj}', 300, 600); " accesskey="C"
                        onmouseover="return overlib('History', AUTOSTATUS, WRAP);" onmouseout="nd();">
                        
                        <input type="button" value="Report" name="report"
                        onclick="popWin('${path}/uw/endorsenonmaterial.htm?window=endorse&spaj=${param.spaj}', 700,850); "accesskey="T"
                        onmouseover="return overlib('Print Report Endorse', AUTOSTATUS, WRAP);" onmouseout="nd();"> 
                        
                         <c:if test="${pemegang.lsre_id ne 1}" > 
                          <th>Untuk Tertanggung(Jika Pemegang Bukan Tertanggung)<br>
                        <input type="button" value="Nama Tertanggung" name="tertanggung"
                        onclick="popWin('${path}/uw/endorsenonmaterial.htm?window=editnamatertanggung&spaj=${param.spaj}', 400,600); "accesskey="T"
                        onmouseover="return overlib('Edit Nama Tertanggung', AUTOSTATUS, WRAP);" onmouseout="nd();">
                        
                        <input type="button" value="Alamat Tertanggung " name="rumah"
                        onclick="popWin('${path}/uw/endorsenonmaterial.htm?window=editalamattertanggung&spaj=${param.spaj}', 700, 750); "accesskey="H"
                        onmouseover="return overlib('Edit Alamat Rumah Tertanggung', AUTOSTATUS, WRAP);" onmouseout="nd();">&nbsp;
                        
                        <input type="button" value="DOB Tertanggung" name="tgltertanggung"
                        onclick="popWin('${path}/uw/endorsenonmaterial.htm?window=edittgltanggung&spaj=${param.spaj}', 450, 600); "accesskey="M"
                        onmouseover="return overlib('Edit Tgl Lahir Tertanggung', AUTOSTATUS, WRAP);" onmouseout="nd();">
                        </c:if></th>
                </th>
            </tr>
               <tr width="100%">
                     <td colspan="5">
                        <iframe src="" name="infoFrame" id="infoFrame"
                           width="100%" > Please Wait... </iframe>
                           </td>
                       </tr>
                    </td>
                </tr>
            </table>
        </div>
    </form>
 </c:otherwise>
 </c:choose>
</body>
<%@ include file="/include/page/footer.jsp"%>