<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript">
	var sukses = '${sukses}';
	if(sukses==1){
		alert('${successMessage}');
		parent.location.href='${path}/uw/uw.htm?window=collection';
	}
	
	function toSubmit(){
      	var result = confirm("Yakin ingin memperbarui status untuk SPAJ ini?");
      	var detail = document.getElementById("detail").value;
      	var flagStatusFR = document.getElementById("flagStatusFR").value;
      
		if(result==true){
			if (flagStatusFR == "F2"){
				if (detail == ""){
					alert("Mohon berikan informasi mengenai detail Further!");
					return false; 
				}else{
					return true;
				}
			}else{
				return true;
			}			
			
		}else{
			return false; 
		}
	
		
   }
   
   function change(obj) {
			
			var selectBox = obj;
    		var selected = selectBox.options[selectBox.selectedIndex].value;
   			var tujuan = document.getElementById("tujuan");
   			var tambahan = document.getElementById("tambahan");
   			var subjek = document.getElementById("subjek");
   			var keterangan = document.getElementById("keterangan");
   			var btnKirimAndSave = document.getElementById("btnKirimAndSave");
   			var btnSave = document.getElementById("btnSave");
   		//	var buttonStat = document.getElementById("tombol").value;
   		//	alert(buttonStat);
			
			    if(selected == '1') {
			  		tujuan.style.display = "none";
			        tambahan.style.display = "none";
			        keterangan.style.display = "none";
			        btnKirimAndSave.style.display = "none";
			        StatusFR.style.display = "none";
			        btnSave.style.display = "block";
			    }else  if(selected == '2') {
			  		tujuan.style.display = "block";
			        tambahan.style.display = "block";
			        keterangan.style.display = "none";
			        btnKirimAndSave.style.display = "block";
			        StatusFR.style.display = "block";
			        btnSave.style.display = "none";
			    }
			    else if(selected == 'F1') {
					tujuan.style.display = "block";
			        tambahan.style.display = "block";
			        keterangan.style.display = "none";
			        btnKirimAndSave.style.display = "block";
			        btnSave.style.display = "none";
			    }else if(selected == 'F2'){
			   		 tujuan.style.display = "block";
			        tambahan.style.display = "block";
			        keterangan.style.display = "block";
			        btnKirimAndSave.style.display = "block";
			        btnSave.style.display = "none";
			    }
			}
   
</script>
<body style="height: 100%;">
	<c:choose>
		<c:when test="${speedy  eq 1 }" >
			<div id="success">${successMessage }</div>
		</c:when>
		<c:otherwise>
			<form method="post" name="formpost" action="" onsubmit="return toSubmit();">
				<fieldset>
					<legend>Status Further Collection</legend>
					<table class="entry2">
					<tr><td></td><td></td></tr>
					<tr><td></td><td></td></tr>
					<tr>
							<th style=" line-height: 1;font-size: 10px; font-color: black">STATUS</th>
							<td>
							<select name="flagStatus" id="flagStatus" title="Pilih Salah satu" style="width: 420px; line-height: 1;font-size: 12px;background: #aaccdd;border: 2px solid #dcc;" onchange="change(this)">	
																						
												<option value="1" <c:if test="${flagStatus eq '1'}">selected="selected"</c:if>>COMPLETED COLLECTION (ACCEPTED)</option>
												<option value="2" <c:if test="${flagStatus eq '3'}">selected="selected"</c:if>>FURTHER COLLECTION</option>
										</select>
							</td>
						</tr>
						<tr  id = "StatusFR" style="display: none;">
							<th >Jenis Further</th>
							<td>
							<select name="flagStatusFR" id="flagStatusFR" title="Pilih Salah satu" style="width: 420px; line-height: 1;font-size: 11px;border: 2px solid #dcc;" onchange="change(this)">	
																						
												<option value="F1" <c:if test="${flagStatus eq '1'}">selected="selected"</c:if>>PERMINTAAN BSB</option>
												<option value="F2" <c:if test="${flagStatus eq '2'}">selected="selected"</c:if>>LAIN LAIN</option>
												
										</select>
							</td>
						</tr>
						<tr  id = "keterangan" style="display: none;">
							<th>Detail *</th>
							<td><textarea rows="4" cols="80" name="detail" id="detail">${message}</textarea></td>
						</tr>
						<tr id = "tujuan" style="display: none;">
							<th>To</th>
							<td><input type="text" value="${to }" name="to" id="to"  style="width: 420px;">
							<em>*pemisah e-mail gunakan tanda ';'</em></td>
						</tr>
						<tr id = "tambahan" style="display: none;">
							<th>Cc</th>
							<td><input type="text" value="${cc }" value="" name="cc" id="cc" style="width: 420px;">
							<em>*pemisah e-mail gunakan tanda ';'</em></td>
						</tr>
												
						<tr>
							<th></th>
							<td>
								<input type="submit" name="btnKirimAndSave" id="btnKirimAndSave" value="Update Status & Kirim Email" style="display: none;" >
								<input type="submit" name="btnSave" id="btnSave" value="Update Status">
							</td>	
						</tr>
					</table>
				</fieldset>
			</form>
		</c:otherwise>
	</c:choose>
</body>
<%@ include file="/include/page/footer.jsp"%>