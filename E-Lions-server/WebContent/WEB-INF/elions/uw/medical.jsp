<%@ include file="/include/page/header.jsp"%>
<script>
	function tambah(){
		formpost.flag.value="1";
		formpost.submit();
	}

	function ubah(){
		formpost.flagAdd.value="2";
		if(formpost.desc!=null)
			i=formpost.desc.length;
		else
			i=0;
				
		if(i>0){
			for(a=0;a<i;a++){
				formpost.jenis[a].disabled=false;
				formpost.elements['hasil'+a][0].disabled=false;
				formpost.elements['hasil'+a][1].disabled=false;
				formpost.status[a].disabled=false;
				formpost.desc[a].disabled=false;
			}
		}else if(i=='0'){
			alert("TIdak BIsa Ubah");
			return 0;
		}else{
			formpost.jenis.disabled=false;
			formpost.elements['hasil0'][0].disabled=false;
			formpost.elements['hasil0'][1].disabled=false;
			formpost.status.disabled=false;
			formpost.desc.disabled=false;
		}
		formpost.btn_add.disabled=true;
		formpost.btn_edit.disabled=true;
		formpost.btn_delete.disabled=true;
		formpost.btn_save.disabled=false;
		formpost.btn_cancel.disabled=false;
		
		
	}

	function batal(){
		var spaj='${cmd.spaj}';
		if(spaj!=null)
			window.location='${path }/uw/medical.htm?spaj='+spaj;
	}

	function simpan(){
		formpost.flag.value="2";
		proses=formpost.flagAdd.value;
		
		if(proses=='1'){//add
			if(formpost.row.value=='0'){
				if(formpost.elements['hasil0'][0].checked)
					formpost.status.value='1';
				else
					formpost.status.value='0';
				formpost.submit();		
			}else{	
				i=formpost.jenis.length-1;
				if(formpost.elements['hasil'+i][0].checked)
					formpost.status[i].value='1';
				else
					formpost.status[i].value='0';
					
				formpost.submit();		
			}
		}else if(proses=='2'){
			formpost.submit();
		}
			
	}

	function hapus(){
		formpost.flag.value="2";
		formpost.flagAdd.value="3";		
		if(formpost.row.value=='1'){	//hanya 1 row
			formpost.brs.value=1;
			formpost.submit();
		}else if(formpost.row.value>1){
			brs=prompt('Masukan Baris yang akan dihapus',""); 
			if(brs!=null)
			if(brs=='0')
				alert("Minimal baris adalah 1");
			else if(brs>formpost.jenis.length)
				alert("Baris ke-"+brs+" tidak ada");
			else if(cekBrs(brs)){
				formpost.brs.value=brs;
				formpost.submit();
			}			
		
		}else{
			alert("Tidak ada data");
		}	
		
		
	}
	
	function cekBrs(name){
		flag=true;
		for(i=0;i< name.length;i++)
			if(isNaN(name.charAt(i))==true)
				flag=false;
		if(flag==false){
			 alert("Silahkan Masukan Baris Dengan Benar!");
			return 0;
		}
		
		return 1;
		
	}
	
	function awal(){
		setFrameSize('infoFrame', 45);
		if(formpost.flagAdd.value=='1'){//add
			formpost.btn_add.disabled=true;
			formpost.btn_edit.disabled=true;
			formpost.btn_delete.disabled=true;
			formpost.btn_save.disabled=false;
			formpost.btn_cancel.disabled=false;
			
			if(formpost.row.value=='0'){
				formpost.jenis.disabled=false;			
				formpost.desc.disabled=false;
				formpost.elements['hasil0'][0].disabled=false;
				formpost.elements['hasil0'][1].disabled=false;
				formpost.status.disabled=false;
			}else{
				i=formpost.jenis.length-1;
				formpost.jenis[i].disabled=false;
				formpost.elements['hasil'+i][0].disabled=false;
				formpost.elements['hasil'+i][1].disabled=false;
				formpost.status[i].disabled=false;
				formpost.desc[i].disabled=false;
			}
			
		}else{
			formpost.btn_add.disabled=false;
			formpost.btn_edit.disabled=false;
			formpost.btn_delete.disabled=false;
			formpost.btn_save.disabled=true;
			formpost.btn_cancel.disabled=false;
		}
		
		/*if(formpost.row.value=='0'){//data kosong
			formpost.btn_add.disabled=false;
			formpost.btn_edit.disabled=true;
			formpost.btn_delete.disabled=true;
			formpost.btn_save.disabled=true;
			formpost.btn_cancel.disabled=false;
		}	*/
	
	}
	
</script>

<body onload="awal();" onresize="setFrameSize('infoFrame', 45);" style="height: 100%;">
<form name="formpost" method="post">
	<div class="tabcontent">
	<fieldset>
	<legend>Medis</legend>
	<table class="entry2" width="60%">
		<tr>
			<th>Jenis</th>
			<th>Hasil</th>
			<th>Keterangan</th>
		</tr>
		<c:forEach var="s" items="${cmd.lsMedical}" varStatus="xt">
			<tr>
				<td>
					<select name="jenis" disabled>
						<c:forEach var="t" items="${lsJenis}">
							<option value="${t.LSMC_ID}"  
								<c:if test="${s.lsmc_id eq t.LSMC_ID }">selected</c:if>>
								${t.LSMC_NAME}
							</option>
						</c:forEach>
					</select>
				</td>	
				<td>
					<c:choose>
						<c:when test="${s.msdm_status eq 1 or s.msdm_status eq null}">
							<input type="hidden" disabled name="status" value="${s.msdm_status }" >
							<input type="radio" disabled name="hasil${xt.index}" checked>Normal
							<input type="radio" disabled name="hasil${xt.index}" >Tidak Normal
						</c:when>
						<c:when test="${s.msdm_status eq 0}"> 
							<input type="hidden" disabled name="status" value="${s.msdm_status }" >
							<input type="radio" disabled name="hasil${xt.index}" >Normal
							<input type="radio" disabled name="hasil${xt.index}" checked>Tidak Normal
						</c:when>
					</c:choose>					
				</td>
				<td>
					<textarea rows="5" cols="30" name="desc" disabled>${s.msdm_desc}</textarea>
				</td>
			</tr>
		</c:forEach>
	</table>
	<table width="60%">
		<tr>
			<td align="center">
				<input type="hidden" name="flag" value="0" >
				<input type="hidden" name="flagAdd" value="${cmd.flagAdd}" >
				<input type="hidden" name="row" value="${cmd.row}" >
				<input type="hidden" name="brs" value="0" >
				<input type="button" name="btn_save" value="Save" onclick="simpan();">
				<input type="button" name="btn_edit" value="Edit" onclick="ubah();">
				<input type="button" name="btn_add" value="Add" onclick="tambah();">
				<input type="button" name="btn_delete" value="Delete" onclick="hapus();">
				<input type="button" name="btn_cancel" value="Cancel" onclick="batal();">
			</td>
		</tr>
	</table>
	</fieldset>
	<fieldset>
	<legend>Email Cabang</legend>
	<table class="entry2" >
		<tr>
			<th>
				Keterangan Email 
			</th>
			<th>
				<textarea rows="5" cols="50" name="desc" ></textarea>
			</th>
			<th>
				<input type="button" name="sendEmail" value="Send Email" >
			</th>
		</tr>
	</table>
	</fieldset>
	<table width="50%">
	<tr>
		<td colspan="2">
			<c:if test="${not empty submitSuccess }">
	        	<div id="success">
		        	Berhasil
	        	</div>
	        </c:if>	
  			<spring:bind path="cmd.*">
				<c:if test="${not empty status.errorMessages}">
					<div id="error">
						ERROR:<br>
							<c:forEach var="error" items="${status.errorMessages}">
										- <c:out value="${error}" escapeXml="false" />
								<br/>
							</c:forEach>
					</div>
				</c:if>									
			</spring:bind>
          
		</td>
	</tr>
</table>
	</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>