<%@ include file="/include/page/header.jsp"%>
<style type="text/css">
html,body {
	overflow: hidden;
}
</style>
<script>
	function buttonLinks(str){
		
		var spaj = document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value;
		
		if(str=='refresh'){
			history.go();
			return false;
		}else if(str=='summary'){
			document.getElementById('infoFrame').src='${path}/report/uw.htm?window=proses30hari';
		}else if(str=='surat'){ 
			document.getElementById('infoFrame').src='${path}/report/uw.htm?window=pengantar_ttp';
			return false;
		}else if(spaj == '' && str != 'cari') {
			alert('Maaf tetapi tidak ada nomor registrasi SPAJ yang dipilih');
			return false;
		}
		
		if(str=='cari'){
			popWin('${path}/uw/spaj.htm?posisi=7&win=ttp', 350, 450);
		}else if(str=='30hari'){
			if(confirm('Anda yakin untuk melakukan proses 30 hari dan transfer ke Print Komisi?\nProses ini akan memakan waktu kurang lebih 5 menit.')){
				createLoadingMessage();
				return true;
			}
		}else if(str=='deduct'){
			popWin('${path}/finance/komisi.htm?window=deduct&disabled=true&msco_id='+
				document.formpost.v1.value+'&spaj='+document.formpost.v2.value+'&nama='+
				document.formpost.v3.value+'&lev_comm='+document.formpost.v4.value, 400, 720);
		}else if(str=='info'){
			tampilkan(spaj);
		}else if(str=='uwinfo'){
            document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=uwinfo&spaj='+spaj;
		}else if(str=='batal'){
			document.getElementById('infoFrame').src='${path }/uw/cancel.htm?from=tandaterima&spaj='+spaj;
		}else if(str=='checklist'){
			document.getElementById('infoFrame').src='${path }/checklist.htm?lspd_id=7&reg_spaj='+spaj;
		}else if(str=='transfer'){ 
			document.getElementById('infoFrame').src='${path }/uw/ttp.htm?window=transfer&spaj='+spaj;
		}else if(str=='transferNew'){ 
			ajaxInfoRek(spaj,'infoRek','cekRekAgen');
			document.getElementById('infoFrame').src='${path }/uw/transfer_ttp.htm?spaj='+spaj;
		}else if(str=='transferToAs'){ 
			if(confirm("Anda Yakin Untuk transfer langsung polis ini \nke Agent Commision (Agency Support) ?"))
				document.getElementById('infoFrame').src='${path }/uw/ttp.htm?window=transferToAs&spaj='+spaj;
		}else if(str=='komisi'){ 
			document.getElementById('infoFrame').src='${path }/report/uw.htm?window=view_list_pem_komisi';
		}else if(str=='list_pending'){ 
			document.getElementById('infoFrame').src='${path }/report/uw.htm?window=list_pending';
		}else if(str=='ttpket'){ 
			document.getElementById('infoFrame').src='${path }/uw/viewer.htm?window=editKetTTp&spaj='+spaj+'&show=2';
		}
		else if(str=='editTglTrmTtp'){
			popWin('${path }/uw/viewer.htm?window=editTglTrmTtp&spaj='+spaj+'&show=4', 150, 350);
		}
	}
	
	function tampilkan(asdf){
		ajaxInfoTtp(asdf,'infoTtp','selectMstInsuredMsteFlagKomisi');
		if(document.getElementById('error')) document.getElementById('error').style.display='none';
		if(document.getElementById('infoFrame')){
			document.getElementById('infoFrame').style.display='block';
			document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+asdf;	
			document.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+asdf;
			pesanTTP(asdf);
		}
	
	}
	
	function mulai(){
		if('${snow_spaj}'!=''){
			document.formpost.spaj.value='${snow_spaj}';
			document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value='${snow_spaj}';
		}
	}
</script>
<body
	onload="setFrameSize('infoFrame', 45); setFocus('spaj'); mulai();setFrameSize('docFrame', 45);"
	onresize="setFrameSize('infoFrame', 45);setFrameSize('docFrame', 45);" style="height: 100%;">

	<form name="formpost" method="post">
		<input type="hidden" name="v1">
		<input type="hidden" name="v2">
		<input type="hidden" name="v3">
		<input type="hidden" name="v4">
		<div class="tabcontent">
			<table class="entry2" style="width: 98%">
				<tr>
					<th>
						SPAJ
					</th>
					<td>
						<select name="spaj"
							onchange="tampilkan(this.options[this.selectedIndex].value);">
							<option value="">
								[--- Silahkan Pilih/Cari SPAJ ---]
							</option>
							<c:forEach var="s" items="${daftarSPAJ}">
								<option value="${s.REG_SPAJ }"
									style="background-color: ${s.BG}">
									${s.SPAJ_FORMATTED} - ${s.POLICY_FORMATTED }
								</option>
							</c:forEach>
						</select>

						<input type="button" value="Info" name="info" 				onclick="return buttonLinks('info');">
						<input type="button" value="U/W Info" name="uwinfo"         onclick="return buttonLinks('uwinfo');">
						<input type="button" value="Cari" name="search" 			onclick="return buttonLinks('cari');">
						<input type="button" value="Refresh" name="refresh" 		onclick="return buttonLinks('refresh');">
						<input type="button" value="Summary 30 hari" name="summary" onclick="return buttonLinks('summary');">
						<input type="button" value="Checklist" name="checklist" 	onclick="return buttonLinks('checklist');">
					</td>
					<td rowspan=2>
						<div id="prePostError" style="display: none;"></div>
						<div id="infoTtp" style="font: bold; color: #FF0000;"
							align="right">
						</div>
						<div id="infoRek" style="font: bold; color: #FF0000;"
							align="right">
						</div>
					</td>
				</tr>
				<tr>
					<th>
						Proses
					</th>
					<td>
						<input type="submit" value="Proses 30 Hari" name="30hari"				onclick="return buttonLinks('30hari');">
						
						<c:if test="${sessionScope.currentUser.lus_id eq '574'}">
							<input type="button" value="Batal" name="batal"							onclick="return buttonLinks('batal');">
						</c:if>
						
						<input type="hidden" disabled value="Transfer" name="transfer"			onclick="return buttonLinks('transfer');">
						<input type="button" value="Transfer" name="transfer"					onclick="return buttonLinks('transferNew');">
						<input type="button" value="Transfer to AS" name="transfer"				onclick="return buttonLinks('transferToAs');">
						<input type="hidden" value="Surat Pengantar" name="surat"				onclick="return buttonLinks('surat');">
						<input type="button" value="View List Pembayaran Komisi" name="komisi" 	onclick="return buttonLinks('komisi');">
						<input type="button" value="List Pending TTP" name="list_pending"		onclick="return buttonLinks('list_pending');">
						<input type="button" value="Edit Keterangan TTP" name="ttp" 	onclick="return buttonLinks('ttpket');">
						<input type="button" value="Input Tanggal Terima TTP" name="editTglTrmTtp" onclick="return buttonLinks('editTglTrmTtp');">
					</td>
				</tr>

				<c:forEach items="${err}" var="x">
					<tr>
						<td colspan="3">
							<div style="font: bold; color: #FF0000;" align="left">
								- ${x.value }
							</div>
						</td>
					</tr>
				</c:forEach>
<!-- 				<tr> -->
<!-- 					<td colspan="3"> -->
<!-- 						<c:if test="${not empty pesan}"> -->
<!-- 							<div id="success"> -->
<!-- 								${pesan } -->
<!-- 							</div> -->
<!-- 						</c:if> -->
<!-- 						<iframe name="infoFrame" id="infoFrame" width="100%" -->
<!-- 							frameborder="YES"> -->
<!-- 							- -->
<!-- 						</iframe> -->
						<!--	<script>tampilkan('${daftarSPAJ[0].REG_SPAJ}');</script>-->
<!-- 					</td> -->
<!-- 				</tr> -->
				<tr>
					<c:if test="${not empty pesan}">
						<div id="success">
							${pesan }
						</div>
					</c:if>
					<td colspan="3">
						<table style="margin: 0 0 0 0; padding: 0 0 0 0; width: 100%;">
							<c:choose>
								<c:when test="${not empty wideScreen}">
									<td style="width: 60%;">
										<iframe name="infoFrame" id="infoFrame" width="100%"frameborder="YES">-</iframe>
									</td>
									<td style="width: 40%;">
										<iframe src="" name="docFrame" id="docFrame" width="100%">E-Lions</iframe>
									</td>
								</c:when>
								<c:otherwise>
									<td>
										<iframe name="infoFrame" id="infoFrame" width="100%"frameborder="YES">-</iframe>
									</td>
								</c:otherwise>
							</c:choose>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>