<%--
/**********************************************************************
 * Program History
 *
 * Project Name      	: E-Proposal
 * Function Id         	:
 * Program Name   		: ${NAME}
 * Description         	:
 * Environment      	: Java  1.5.0_06
 * Author               : samuel
 * Version              : 1.0
 * Creation Date    	: Jul 19, 2007 1:49:51 PM
 *
 * Update history   Re-fix date      Person in charge      Description
 *
 *
 *
 * Copyright(C) 2007-Asuransi Jiwa Sinarmas. All Rights Reserved.
 ***********************************************************************/
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
</head>

<body>
<br/>
<br/>
Generating Document, please wait...

<form:form id="formdownload" commandName="cepr01030000Command">
</form:form>

<script type="text/javascript">
    alert( 'session downloadUrlSession = ${downloadUrlSession}')
    document.getElementById('formdownload').action='${path }/${downloadUrlSession}';
    document.getElementById('formdownload').submit();
</script>


</body>

</html>