<%@ include file="/include/page/header.jsp"%>
<script>
function buttonLinks(str){
	var spaj = document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value;
	if(str=='cari'){
		popWin('${path}/uw/spaj.htm?posisi=9&win=ttp', 350, 450);  //&search=yes
	}else if(str=='info'){
		tampilkan(spaj);
	}else if(str=='transfer'){
		if(confirm('Ada Memo,Anda Yakin untuk transfer balik ke TTP?'))
			document.getElementById('infoFrame').src='${path }/uw/ttp.htm?window=checking_ttp_transfer&spaj='+spaj;
	}else if(str=='deduct'){
		if(confirm('Apakah anda yakin untuk dilanjutkan?'))
			document.getElementById('infoFrame').src='${path }/uw/slip_potongan_komisi.htm?spaj='+spaj;
	}else if(str=='komisi'){
		if(confirm('Apakah anda yakin untuk dilanjutkan?'))
			document.getElementById('infoFrame').src='${path }/report/uw.htm?window=view_list_pot_komisi';
	}
}	
function tampilkan(asdf){
	if(document.getElementById('error')) document.getElementById('error').style.display='none';
	if(document.getElementById('infoFrame')){
		document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+asdf;	
	}
}
	
</script>
<body onload="setFrameSize('infoFrame', 65); setFocus('spaj');"
	onresize="setFrameSize('infoFrame', 65);" style="height: 100%;">

<form name="formpost" method="post">
<table class="entry2" style="width: 98%">
<tr>
<th>SPAJ</th>
<td>	
	<select name="spaj" 
	onchange="tampilkan(this.options[this.selectedIndex].value);">
		<c:forEach var="s" items="${daftarSPAJ}">
			<option value="${s.REG_SPAJ }" style="background-color: ${s.WARNA};" 
								<c:if test="${s.REG_SPAJ eq param.spaj }">selected<c:set var="ada" value="ada" /></c:if>>${s.SPAJ_FORMATTED}
							- ${s.POLICY_FORMATTED }</option>
		</c:forEach>
	</select>
	<input type="button" value="Info" name="info" onclick="return buttonLinks('info');">
	<input	type="button" value="Cari" name="search"	onclick="return buttonLinks('cari');"> 
	<input	type="button" value="Transfer" name="transfer"	onclick="return buttonLinks('transfer');"> 
	<input	type="button" value="Deduct" name="deduct"	onclick="return buttonLinks('deduct');"> 
	<input	type="button" value="View List Potongan Komisi" name="komisi"	onclick="return buttonLinks('komisi');"> 
</td>

<th>
	<table>
	<th style="border-right: 1px #e0e0e0 solid; border-bottom: 1px #e0e0e0 solid; color: black  font-size: 5px;">Keterangan warna NO.SPAJ/No.POLIS :</th>
	<th>
		<tr>
			<td TEXT="afeeee" style="border-right: 1px #e0e0e0 solid; border-bottom: 1px #e0e0e0 solid; color: blue;  font-size: 9px;">REK BKN BSM/REK KOSONG</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td TEXT="CC99FF" style="border-right: 1px #e0e0e0 solid; border-bottom: 1px #e0e0e0 solid; color: CC99FF;  font-size: 9px;">DEDUCT</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td TEXT="ffb6c1" style="border-right: 1px #e0e0e0 solid; border-bottom: 1px #e0e0e0 solid; color: ffb6c1;  font-size: 9px;">SERTIFIKAT TDK BERLISENSI</td>
			<td>&nbsp;</td>
		</tr></th>
	</table>
<th>
<td rowspan=2><div id="prePostError" style="display:none;"></div></td>
</tr> 
<tr>
<th>Proses</th>
<td>
</td>
</tr>
<tr><td colspan="3">
<c:if test="${not empty pesan}">
	<div id="success">${pesan }</div>
</c:if>
	<iframe name="infoFrame" id="infoFrame"
		width="100%" frameborder="YES">-</iframe>
</td></tr></table></div>

</form>
</body>
<%@ include file="/include/page/footer.jsp"%>