<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<meta HTTP-EQUIV="Expires" CONTENT="-1">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link href="${path}/include/image/eas.ico" rel="shortcut icon">
		<script type="text/javascript" src="${path }/include/js/default.js"></script> 
		<script type="text/javascript" src="${path }/include/js/jquery-1.3.2.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				hideLoadingMessage();
				<c:if test="${not empty param.success}">
					alert('${param.success}');
				</c:if>
				
				<c:if test="${cmd.type_reinsurer eq 0}">
					$('#lsrei_id').animate({ opacity: 'hide' }, 'fast');
				</c:if>
				
				$('.noBorder').click(function(e) {
					if(this.id == "new") {
						$('#lsrei_id').animate({ opacity: 'hide' }, 'fast');
						//window.location='${path}/uw/reinsurer.htm';
					}
					else if(this.id == "exist") {
						$('#lsrei_id').animate({ opacity: 'show' }, 'fast'); 
					}		
				});
				
				$('.button').click(function(e) {
					e.preventDefault();
					if(confirm("simpan perubahan ?")) {
						$('#submitMode').val(this.id);
						this.disabled=true;
						$('#formpost').submit();
					}	
				});
				
				$('.select').change(function(e) {
					if(this.id == "lsrei_id" && this.value != '') {
						$('#submitMode').val(this.id);
						$('#formpost').submit();
						
					}
				});
			});
		</script>
	</head>		
	<body style="height: 100%;">
		<div id="contents">
			<fieldset>
				<legend>Master Reinsurer</legend>
				<form:form commandName="cmd" id="formpost" method="post">
			    	<table class="entry2" border="0">
			    		<tr>
			    			<td colspan="2">
								<label for="new"><form:radiobutton path="type_reinsurer" cssClass="noBorder" value="0" id="new" cssStyle="margin-right: 3px" />New</label>
								<label for="exist"><form:radiobutton path="type_reinsurer" cssClass="noBorder" value="1" id="exist" cssStyle="margin-right: 3px" />Existing</label>    			
								<form:select path="lsrei_id" id="lsrei_id" cssClass="select">
									<form:option label="" value="-1"/>
									<form:options items="${lsHslReas}" itemLabel="value" itemValue="key" />
								</form:select> 			    			
			    			</td>
			    		</tr>
			    		<tr>
			    			<th>Reinsurer Info</th>
			    			<td width="85%">
			    				<table class="entry" width="100%" border="0">
			    					<tr>
			    						<td width="100px" style="text-align: center">Nama</td>
			    						<td><form:input id="lsre_nama" path="lsre_nama" size="40" cssErrorClass="errField" onkeyup="textCounter(this, 30); " onkeydown="textCounter(this, 30);"/></td>
			    					</tr>
			    					<tr>
			    						<td style="text-align: center">Alamat</td>
			    						<td>
			    							<form:textarea id="lsre_alamat" tabindex="6" cols="65" rows="3" cssErrorClass="errField"
								               onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200);" 
								               path="lsre_alamat"/>
			    						</td>
			    					</tr>
			    					<tr>
			    						<td style="text-align: center">Telepon</td>
			    						<td>
			    							<form:input id="lsre_telpon" path="lsre_telpon" size="40" cssErrorClass="errField" onkeyup="textCounter(this, 20); " onkeydown="textCounter(this, 20);"/>
			    							<span style="color: #61A058">*) 392 1826  -  314 0267</span>
			    						</td>
			    					</tr>
			    					<tr>
			    						<td style="text-align: center">Fax</td>
			    						<td>
			    							<form:input id="lsre_fax" path="lsre_fax" size="40" cssErrorClass="errField" onkeyup="textCounter(this, 20); " onkeydown="textCounter(this, 20);"/>
			    							<span style="color: #61A058">*) 392 1826  -  314 0267</span>
			    						</td>
			    					</tr>
			    					<tr>
			    						<td style="text-align: center">Contact</td>
			    						<td>
			    							<form:input id="lsre_contact" path="lsre_contact" size="40" cssErrorClass="errField" onkeyup="textCounter(this, 30); " onkeydown="textCounter(this, 30);"/>
			    							<span style="color: #61A058">*) Bpk Edy / Ibu Siti</span>
			    						</td>
			    					</tr>
			    					<!--<tr>
			    						<td style="text-align: center">Account GL</td>
			    						<td><form:input id="lsre_account_gl" path="lsre_account_gl" size="40" cssErrorClass="errField" onkeyup="textCounter(this, 10); " onkeydown="textCounter(this, 10);"/></td>
			    					</tr>-->
			    					<tr>
			    						<td style="text-align: center">Email</td>
			    						<td>
			    							<form:input id="lsre_email" path="lsre_email" size="40" cssErrorClass="errField" onkeyup="textCounter(this, 50); " onkeydown="textCounter(this, 50);"/>
			    							<span style="color: #61A058">*) edy@yahoo.com - siti@gmail.com</span>
			    						</td>
			    					</tr>
			    				</table>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td colspan="2" align="center">
			    				<input type="hidden" name="submitMode" id="submitMode">
			    				<input type="button" class="button" id="saveReasdur" value="Save" style="width:100px;">
			    			</td>
			    		</tr>
			    	</table>
			    </form:form>
			</fieldset>
		</div>	    	
	</body>
</html>