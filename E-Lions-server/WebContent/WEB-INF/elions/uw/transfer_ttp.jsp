<%@ include file="/include/page/header.jsp"%>
<script>
	function awal(){
		if('${cmd.simpan}'=='false'){
			if(''!='${cmd.info}' && confirm("${cmd.info}")){
				formpost.submit();				
			}
		}else if('${cmd.posisi}'!=''){
			alert('${cmd.posisi}');
		}
	}
	function pencet(){
		//alert("diblock sementara");
		popWin('/ReportServer/?new=true&rs=UW%5CAgenTakBerpolis&1kode=003702&1userName=${sessionScope.currentUser.name}', 600, 800); 	
	}
	function trans(){
		var result = confirm("Apakah TTD Spaj "+'${cmd.spaj}'+" Sudah Sesuai Dengan Bukti Identitas ?", "ttp");
		flagKom='${cmd.flagKomisi}';
		flagWC='${cmd.flagWC}';
		proses='${cmd.proses}';
		if(result==true){
			if(flagWC==1){
				var pass = prompt("Total Premi Diatas >=50 Jt , Silahkan masukkan PASSWORD Validasi Welcome Call : ", "");
				if(pass=="sinus"){
					if(proses=='0'&&flagKom=='0'){
						formpost.submit();
					}else{
						formpost.submit();
					 }
				}else{
				alert('Password Tidak Valid')
				}
			}else{
				if(proses=='0'&&flagKom=='0'){
					formpost.submit();
				}else{
					formpost.submit();
				}
			}
	}
}
</script>
<body style="height: 100%;" onload="awal();"> 
<form:form id="formpost" name="formpost" commandName="cmd">
	<fieldset style="text-align: left; height: 100%;">
		<legend>Transfer ke Komisi</legend>
		<table class="entry2">
			<tr>
				<td>Tanggal Kembali: 
					<script>inputDate('tanggal', '<fmt:formatDate value="${cmd.tgl}" pattern="dd/MM/yyyy"/>', false);</script>
				</td>
			</tr>
			<tr>
				<td>
					<input <c:if test="${cmd.info ne null || submitSuccess eq 1}">disabled</c:if> type="button" name="transfer" 
						<c:if test="${cmd.proses ne 7}">
							value="Transfer"
						</c:if>
						<c:if test="${cmd.proses eq 7}">
							value="Accept Fax" 
						</c:if>
						onclick="trans();">
					<input type="button" name="info" value="Info Agen tak Berpolis" onclick="pencet();">
					<form:hidden path="fax"/>
				</td>
			</tr>
			<tr> 
	        <td nowrap="nowrap" >
				<c:if test="${submitSuccess eq 1}">
		        	<div id="success">
			        	Berhasil ${hsl}
		        	</div>
		        </c:if>	
	  			<spring:bind path="cmd.*">
					<c:if test="${not empty status.errorMessages}">
						<div id="error">
			        		Pesan:<br>
								<c:forEach var="error" items="${status.errorMessages}">
											- <c:out value="${error}" escapeXml="false" />
									<br/>
								</c:forEach>
						</div>
					</c:if>									
				</spring:bind>
			</td>
   		  </tr>
		</table>
	</fieldset>
	
</form:form>
</body>

<%@ include file="/include/page/footer.jsp"%>