<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path}/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path}/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path}/include/js/ajaxtags/overlibmws.js"></script>
<script>
	hideLoadingMessage();
</script>
</head>
<BODY onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Upload Hasil Scan Dokumen PAS</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" style="text-align: center;" enctype="multipart/form-data">
			
					<table class="entry2">
						<tr>
							<th style="vertical-align: top;">
								
								<fieldset>
									<legend>Daftar File Saat ini</legend>
									<table class="displaytag">
										<thead>
											<tr>
												<th>File</th>
												<th>Last Modified</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="s" items="${daftarAda}">
												<tr>
													<td style="background-color: white; text-align: left;">${s.key}</td>
													<td style="background-color: white; text-align: center;">${s.value}</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</fieldset>
								
								<fieldset>
									<legend>Upload File untuk PAS [${no_kartu}]</legend>
																		
									<table class="entry2">
										<c:forEach items="${daftarFile}" var="u" varStatus="s">
											<tr>
												<th>${u.value}<%-- <c:if test="${u.desc eq 1}"><span class="error"> *</span></c:if> --%></th>
												<td>
													<input type="file" name="daftarFile[${s.index}]" size="60">
												</td>
											</tr>
										</c:forEach>
										<!--
										<tr>
											<td colspan="2" class="error">
												* Syarat untuk dapat mencetak Polis
											</td>
										</tr>
										-->
									</table>
								</fieldset>
								
								<fieldset>
									<legend>Action</legend>
									<input type="submit" name="upload" value="Upload" onclick="return confirm('Sudah cek kebenaran datanya?');">
								</fieldset>

							</th>
						</tr>
					</table>			

				</form>
			</div>
		</div>
	</div>

</form>
</body>
<c:if test="${not empty pesan}">
	<script>alert('${pesan}');</script>
</c:if>
<%@ include file="/include/page/footer.jsp"%>