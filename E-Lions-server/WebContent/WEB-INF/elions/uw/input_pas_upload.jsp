<%@ include file="/include/page/header.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<%User currentUser = (User) session.getAttribute("currentUser");%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>

<script type="text/javascript">
	hideLoadingMessage();
	
	function agen_cek()
	{
		if (document.frmParam.elements['msag_id'].value != "")
		{
			listdataagen2(document.frmParam.elements['msag_id'].value);
		}
	}
	
	function pin_cek()
	{
		if (document.frmParam.elements['pin'].value != "")
		{
			//alert(document.frmParam.elements['pin'].value);
			cekPin(document.frmParam.elements['pin'].value);
		}
	}
	
	function update(){
		if(trim(document.frmParam.kata.value)=='') return false;
		else createLoadingMessage();	
	}
	
	function cekElseEnvir(){
		if(document.getElementById('msp_fire_insured_addr_envir').value == '5'){
			document.getElementById('msp_fire_ins_addr_envir_else').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_ins_addr_envir_else').style.visibility = "hidden";
		}
	}
	
	function cekElseSourceFund(){
		if(document.getElementById('msp_fire_source_fund2').value == 'LAINNYA'){
			document.getElementById('msp_fire_source_fund').style.visibility = "visible";
			document.getElementById('msp_fire_source_fund').value='';
		}else{
			document.getElementById('msp_fire_source_fund').style.visibility = "hidden";
		}
	}
	
	function cekElseBusiness(){
		if(document.getElementById('msp_fire_type_business2').value == 'LAINNYA'){
			document.getElementById('msp_fire_type_business').style.visibility = "visible";
			document.getElementById('msp_fire_type_business').value='';
		}else{
			document.getElementById('msp_fire_type_business').style.visibility = "hidden";
		}
	}
	
	function cekElseOccupation(){
		if(document.getElementById('msp_fire_occupation2').value == 'LAIN-LAIN'){
			document.getElementById('msp_fire_occupation').style.visibility = "visible";
			document.getElementById('msp_fire_occupation').value='';
		}else{
			document.getElementById('msp_fire_occupation').style.visibility = "hidden";
		}
	}
	
	function cekElseTrio(){
		if(document.getElementById('msp_fire_source_fund2').value == 'LAINNYA'){
			document.getElementById('msp_fire_source_fund').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_source_fund').style.visibility = "hidden";
		}
		if(document.getElementById('msp_fire_type_business2').value == 'LAINNYA'){
			document.getElementById('msp_fire_type_business').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_type_business').style.visibility = "hidden";
		}
		if(document.getElementById('msp_fire_occupation2').value == 'LAIN-LAIN'){
			document.getElementById('msp_fire_occupation').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_occupation').style.visibility = "hidden";
		}
	}
	
	function cekElseOkupasi(){
		if(document.getElementById('msp_fire_okupasi').value == 'L'){
			document.getElementById('msp_fire_okupasi_else').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_okupasi_else').style.visibility = "hidden";
		}
	}
	
	function dofo(index, flag) {
		if(flag==1){
			var a="rekening["+(index+1)+"]";
			var b="rekening["+(index)+"]";		
			document.frmParam.elements[a].focus();
			
		}else{
			var a="rekening["+(index+1)+"]";		
			document.frmParam.elements[a].focus();
		}
	}
	
	function dofo_autodebet(index, flag) {
		if(flag==1){
			var a="rekening_autodebet["+(index+1)+"]";
			var b="rekening_autodebet["+(index)+"]";		
			document.frmParam.elements[a].focus();
			
		}else{
			var a="rekening_autodebet["+(index+1)+"]";		
			document.frmParam.elements[a].focus();
		}
	}
	
	function doRek() {
		document.getElementById('msp_no_rekening').value = '';
		for(i = 0 ; i < 21 ; i++){
			document.getElementById('msp_no_rekening').value = document.getElementById('msp_no_rekening').value + document.frmParam.elements["rekening["+i+"]"].value;
		}
	}
	
	function doRek_autodebet() {
		document.getElementById('msp_no_rekening_autodebet').value = '';
		for(i = 0 ; i < 21 ; i++){
			document.getElementById('msp_no_rekening_autodebet').value = document.getElementById('msp_no_rekening_autodebet').value + document.frmParam.elements["rekening_autodebet["+i+"]"].value;
		}
	}
	
	function doEnableDisableRek() {
		if(document.getElementById('msp_flag_cc').value == '1'){// kartu kredit
			//rekening
			document.getElementById('caribank1').disabled = 'disabled';
			document.getElementById('btncari1').disabled = 'disabled';
			document.getElementById('lsbp_id').disabled = 'disabled';
			document.getElementById('msp_rek_cabang').disabled = 'disabled';
			document.getElementById('msp_rek_kota').disabled = 'disabled';
			document.getElementById('msp_rek_nama').disabled = 'disabled';
			for(i = 0 ; i < 21 ; i++){
				document.getElementById('rekening['+i+']').disabled = 'disabled';
			}
			//autodebet
			document.getElementById('caribank2').disabled = '';
			document.getElementById('btncari2').disabled = '';
			document.getElementById('lsbp_id_autodebet').disabled = '';
			document.getElementById('msp_rek_nama_autodebet').disabled = '';
			document.getElementById('msp_tgl_debet_div').disabled = '';
			document.getElementById('msp_tgl_debet_div').style.visibility = 'visible';
			document.getElementById('msp_tgl_valid_div').disabled = '';
			document.getElementById('msp_tgl_valid_div').style.visibility = 'visible';
			for(i = 0 ; i < 21 ; i++){
				document.getElementById('rekening_autodebet['+i+']').disabled = '';
			}
		}else if(document.getElementById('msp_flag_cc').value == '2'){//tabungan
			//rekening
			document.getElementById('caribank1').disabled = '';
			document.getElementById('btncari1').disabled = '';
			document.getElementById('lsbp_id').disabled = '';
			document.getElementById('msp_rek_cabang').disabled = '';
			document.getElementById('msp_rek_kota').disabled = '';
			document.getElementById('msp_rek_nama').disabled = '';
			for(i = 0 ; i < 21 ; i++){
				document.getElementById('rekening['+i+']').disabled = '';
			}
			//autodebet
			document.getElementById('caribank2').disabled = '';
			document.getElementById('btncari2').disabled = '';
			document.getElementById('lsbp_id_autodebet').disabled = '';
			document.getElementById('msp_rek_nama_autodebet').disabled = '';
			document.getElementById('msp_tgl_debet_div').disabled = '';
			document.getElementById('msp_tgl_debet_div').style.visibility = 'visible';
			document.getElementById('msp_tgl_valid_div').disabled = 'disabled';
			document.getElementById('msp_tgl_valid_div').style.visibility = 'hidden';
			for(i = 0 ; i < 21 ; i++){
				document.getElementById('rekening_autodebet['+i+']').disabled = '';
			}
		}else{
			//rekening
			document.getElementById('caribank1').disabled = 'disabled';
			document.getElementById('btncari1').disabled = 'disabled';
			document.getElementById('lsbp_id').disabled = 'disabled';
			document.getElementById('msp_rek_cabang').disabled = 'disabled';
			document.getElementById('msp_rek_kota').disabled = 'disabled';
			document.getElementById('msp_rek_nama').disabled = 'disabled';
			for(i = 0 ; i < 21 ; i++){
				document.getElementById('rekening['+i+']').disabled = 'disabled';
			}
			//autodebet
			document.getElementById('caribank2').disabled = 'disabled';
			document.getElementById('btncari2').disabled = 'disabled';
			document.getElementById('lsbp_id_autodebet').disabled = 'disabled';
			document.getElementById('msp_rek_nama_autodebet').disabled = 'disabled';
			document.getElementById('msp_tgl_debet_div').disabled = 'disabled';
			document.getElementById('msp_tgl_debet_div').style.visibility = 'hidden';
			document.getElementById('msp_tgl_valid_div').disabled = 'disabled';
			document.getElementById('msp_tgl_valid_div').style.visibility = 'hidden';
			for(i = 0 ; i < 21 ; i++){
				document.getElementById('rekening_autodebet['+i+']').disabled = 'disabled';
			}
		}
	}
	
	function doPp() {
		if(document.getElementById('msp_rek_nama').value == ''){
			document.getElementById('msp_rek_nama').value = document.getElementById('nama_pp').value;
		}
		document.getElementById('msp_pas_nama_pp').value = document.getElementById('nama_pp').value;
		
	}
	
	function inPp() {
		document.getElementById('nama_pp').value = document.getElementById('msp_pas_nama_pp').value;
	}
	
	function inRek() {
		var rek = document.getElementById('msp_no_rekening').value
		var size = rek.length;
		for(i = 0 ; i < size ; i++){
			document.frmParam.elements["rekening["+i+"]"].value = rek.charAt(i);
		}
	}
	
	function inRek_autodebet() {
		var rek = document.getElementById('msp_no_rekening_autodebet').value
		var size = rek.length;
		for(i = 0 ; i < size ; i++){
			document.frmParam.elements["rekening_autodebet["+i+"]"].value = rek.charAt(i);
		}
	}
	
	function doPremi(){
		var lsdbs_number = document.getElementById('produk').value;
		var lscb_id = document.getElementById('lscb_id').value;
		var prm = 0;
		
		if(lsdbs_number == '1' && lscb_id == 3){
				document.getElementById('premi').value = '150000';
			}else if(lsdbs_number == '1' && lscb_id == 6){
				document.getElementById('premi').value = '15000';
			}else if(lsdbs_number == '1' && lscb_id == 1){
				prm = 150000 * 0.27;
				document.getElementById('premi').value = prm;
			}else if(lsdbs_number == '1' && lscb_id == 2){
				prm = 150000 * 0.525;
				document.getElementById('premi').value = prm;
			}else if(lsdbs_number == '2' && lscb_id == 3){
				document.getElementById('premi').value = '300000';
			}else if(lsdbs_number == '2' && lscb_id == 6){
				document.getElementById('premi').value = '30000';
			}else if(lsdbs_number == '2' && lscb_id == 1){
				prm = 300000 * 0.27;
				document.getElementById('premi').value = prm;
			}else if(lsdbs_number == '2' && lscb_id == 2){
				prm = 300000 * 0.525;
				document.getElementById('premi').value = prm;
			}else if(lsdbs_number == '3' && lscb_id == 3){
				document.getElementById('premi').value = '500000';
			}else if(lsdbs_number == '3' && lscb_id == 6){
				document.getElementById('premi').value = '50000';
			}else if(lsdbs_number == '3' && lscb_id == 1){
				prm = 500000 * 0.27;
				document.getElementById('premi').value = prm;
			}else if(lsdbs_number == '3' && lscb_id == 2){
				prm = 500000 * 0.525;
				document.getElementById('premi').value = prm;
			}else if(lsdbs_number == '4' && lscb_id == 3){
				document.getElementById('premi').value = '900000';
			}else if(lsdbs_number == '4' && lscb_id == 6){
				document.getElementById('premi').value = '90000';
			}else if(lsdbs_number == '4' && lscb_id == 1){
				prm = 900000 * 0.27;
				document.getElementById('premi').value = prm;
			}else if(lsdbs_number == '4' && lscb_id == 2){
				prm = 900000 * 0.525;
				document.getElementById('premi').value = prm;
			}else{
				document.getElementById('premi').value = '0';
			}
	} 
	
	function changeInputAsuransi(){
		var flag = document.getElementById('tambah_input_asuransi').value;
		// kalau pilih tidak, semua inputan fire dikosongin dan didisable otomatis
		if(flag == '0'){
			document.getElementById('msp_fire_code_name').disabled = 'disabled';document.getElementById('msp_fire_phone_number').disabled = 'disabled';
			document.getElementById('msp_fire_name').disabled = 'disabled';document.getElementById('msp_fire_mobile').disabled = 'disabled';
			document.getElementById('msp_fire_identity').disabled = 'disabled';document.getElementById('msp_fire_email').disabled = 'disabled';
			document.getElementById('msp_fire_insured_addr_code').disabled = 'disabled';
			document.getElementById('msp_fire_occupation2').disabled = 'disabled';document.getElementById('msp_fire_insured_addr').disabled = 'disabled';
			document.getElementById('msp_fire_occupation').disabled = 'disabled';document.getElementById('msp_fire_insured_addr_no').disabled = 'disabled';
			document.getElementById('msp_fire_type_business2').disabled = 'disabled';document.getElementById('msp_fire_insured_postal_code').disabled = 'disabled';
			document.getElementById('msp_fire_type_business').disabled = 'disabled';document.getElementById('msp_fire_insured_city').disabled = 'disabled';
			document.getElementById('msp_fire_source_fund2').disabled = 'disabled';document.getElementById('msp_fire_insured_phone_number').disabled = 'disabled';
			document.getElementById('msp_fire_source_fund').disabled = 'disabled';document.getElementById('msp_fire_insured_addr_envir').disabled = 'disabled';
			document.getElementById('msp_fire_addr_code').disabled = 'disabled';document.getElementById('msp_fire_ins_addr_envir_else').disabled = 'disabled';
			document.getElementById('msp_fire_address_1').disabled = 'disabled';document.getElementById('msp_fire_okupasi').disabled = 'disabled';
			document.getElementById('msp_fire_postal_code').disabled = 'disabled';document.getElementById('msp_fire_okupasi_else').disabled = 'disabled';
		}else{// kalau pilih ya, semua inputan fire dibuka otomatis
			document.getElementById('msp_fire_code_name').disabled = '';document.getElementById('msp_fire_phone_number').disabled = '';
			document.getElementById('msp_fire_name').disabled = '';document.getElementById('msp_fire_mobile').disabled = '';
			document.getElementById('msp_fire_identity').disabled = '';document.getElementById('msp_fire_email').disabled = '';
			document.getElementById('msp_fire_insured_addr_code').disabled = '';
			document.getElementById('msp_fire_occupation2').disabled = '';document.getElementById('msp_fire_insured_addr').disabled = '';
			document.getElementById('msp_fire_occupation').disabled = '';document.getElementById('msp_fire_insured_addr_no').disabled = '';
			document.getElementById('msp_fire_type_business2').disabled = '';document.getElementById('msp_fire_insured_postal_code').disabled = '';
			document.getElementById('msp_fire_type_business').disabled = '';document.getElementById('msp_fire_insured_city').disabled = '';
			document.getElementById('msp_fire_source_fund2').disabled = '';document.getElementById('msp_fire_insured_phone_number').disabled = '';
			document.getElementById('msp_fire_source_fund').disabled = '';document.getElementById('msp_fire_insured_addr_envir').disabled = '';
			document.getElementById('msp_fire_addr_code').disabled = '';document.getElementById('msp_fire_ins_addr_envir_else').disabled = '';
			document.getElementById('msp_fire_address_1').disabled = '';document.getElementById('msp_fire_okupasi').disabled = '';
			document.getElementById('msp_fire_postal_code').disabled = '';document.getElementById('msp_fire_okupasi_else').disabled = '';
		}
	}
	
	function editNamaTertanggung(){
		if(document.getElementById('button_edit_nama_tertanggung').value == 'EDIT'){
			document.getElementById('msp_full_name').disabled = ''
			document.getElementById('button_edit_nama_tertanggung').value = 'CANCEL EDIT';
		}else if(document.getElementById('button_edit_nama_tertanggung').value == 'CANCEL EDIT'){
			document.getElementById('msp_full_name').value = document.getElementById('msp_full_name_temp').value;
			document.getElementById('msp_full_name').disabled = 'disabled'
			document.getElementById('button_edit_nama_tertanggung').value = 'EDIT';
		}else{
			document.getElementById('msp_full_name').disabled = 'disabled'
			document.getElementById('button_edit_nama_tertanggung').value = 'EDIT';
		}
	}
	
	function bodyOnLoad(){
		cekElseEnvir();
		cekElseOkupasi();
		cekElseTrio();
		inRek();
		inRek_autodebet();
		inPp();
		doPremi();
		agen_cek();
		changeInputAsuransi();
		doEnableDisableRek();
	}
	
</script>

</head>

<body onload="" style="height: 100%;">
<div id="contents">
<div class="tab-container" id="container1">

		<div class="tab-panes">

			<div id="pane1" class="panes">
<form method="post" name="formpost" style="text-align: center;" enctype="multipart/form-data">
<fieldset>
<legend>Upload File</legend>
									<table class="entry2">
										<tr>
											<th>File</th>
								<td>
												<input type="file" name="file1" size="70" />
											</td>
										</tr>
									</table>
						</fieldset>
						<fieldset>
					   <legend>-</legend>
					   <table class="entry2">
						<tr>
							<th colspan="2" align="left">
								<font class="error">* FILE YANG DIUPLOAD ADALAH FILE EXCEL</font>
							</th>
						</tr>
						<tr>
							<th colspan="2">
							<input type="hidden" name="flag_posisi" size="1" id="flag_posisi" value="${flag_posisi}"/>
							<input type="hidden" name="kata" size="1" value="edit" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
								<input type="submit" name="upload" value="Upload" />
					<input type="button" name="close" value="Close" onclick="window.close();"
						accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">
						<input type="hidden" name="result" size="1" value="${result}" />
							</th>
						</tr>
					</table>
					</fieldset>
					<fieldset style="text-align: left;">
<legend>Status Upload</legend>
<c:forEach items="${successMessage}" var="d" varStatus="st">
<c:choose>
<c:when test="${d eq 'proses insert gagal'}">
<font color="red">${st.index+1}. ${d}</font>
</c:when>
<c:otherwise>
<font color="blue">${st.index+1}. ${d}</font>
</c:otherwise>
</c:choose>

<br/>
</c:forEach>
</fieldset>
					</div>
		</div>
	</div>	
</form>
	</div>
</body>
</html>