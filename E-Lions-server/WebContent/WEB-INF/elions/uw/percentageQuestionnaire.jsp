<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen"> <!-- CSS untuk DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script><!-- DatePicker Script (jscalendar) -->
	</head>
	<script>
		function cari(){
			tipe_1 = document.getElementById('tipe_1').checked
			tipe_2 = document.getElementById('tipe_2').checked
			jenisReport=formpost.jenisReport.value;
			if( tipe_1 == false && tipe_2 == false){
				alert('Silakan Pilih Jenis Chart Terlebih Dahulu');
			}else{
				if( tipe_1 == true ){
						document.getElementById('barChart').style.display = '';
						document.getElementById('pieChart_2').style.display = 'none';
						document.getElementById('pieChart_1').style.display = 'none';
						document.getElementById('infoFrameBar').src='${path}/uw/uw.htm?window=percentageQuestionnaireMultiBarChart&jenisReport='+jenisReport;
				}else if( tipe_2 == true ){
					if('1' == jenisReport ){
						document.getElementById('pieChart_2').style.display = 'none';
						document.getElementById('barChart').style.display = 'none';
						document.getElementById('pieChart_1').style.display = '';
						document.getElementById('infoFramePie_1').src='${path}/uw/uw.htm?window=percentageQuestionnaireMultiPieChart&jenisReport='+jenisReport+'&pieKe=1';
						document.getElementById('infoFramePie_2').src='${path}/uw/uw.htm?window=percentageQuestionnaireMultiPieChart&jenisReport='+jenisReport+'&pieKe=2';
						document.getElementById('infoFramePie_3').src='${path}/uw/uw.htm?window=percentageQuestionnaireMultiPieChart&jenisReport='+jenisReport+'&pieKe=3';
						document.getElementById('infoFramePie_4').src='${path}/uw/uw.htm?window=percentageQuestionnaireMultiPieChart&jenisReport='+jenisReport+'&pieKe=4';
						document.getElementById('infoFramePie_5').src='${path}/uw/uw.htm?window=percentageQuestionnaireMultiPieChart&jenisReport='+jenisReport+'&pieKe=5';
						document.getElementById('infoFramePie_6').src='${path}/uw/uw.htm?window=percentageQuestionnaireMultiPieChart&jenisReport='+jenisReport+'&pieKe=6';
						document.getElementById('infoFramePie_7').src='${path}/uw/uw.htm?window=percentageQuestionnaireMultiPieChart&jenisReport='+jenisReport+'&pieKe=7';
						document.getElementById('infoFramePie_8').src='${path}/uw/uw.htm?window=percentageQuestionnaireMultiPieChart&jenisReport='+jenisReport+'&pieKe=8';
						document.getElementById('infoFramePie_9').src='${path}/uw/uw.htm?window=percentageQuestionnaireMultiPieChart&jenisReport='+jenisReport+'&pieKe=9';
						document.getElementById('infoFramePie_10').src='${path}/uw/uw.htm?window=percentageQuestionnaireMultiPieChart&jenisReport='+jenisReport+'&pieKe=10';
						document.getElementById('infoFramePie_11').src='${path}/uw/uw.htm?window=percentageQuestionnaireMultiPieChart&jenisReport='+jenisReport+'&pieKe=11';
						document.getElementById('infoFramePie_12').src='${path}/uw/uw.htm?window=percentageQuestionnaireMultiPieChart&jenisReport='+jenisReport+'&pieKe=12';
						document.getElementById('infoFramePie_13').src='${path}/uw/uw.htm?window=percentageQuestionnaireMultiPieChart&jenisReport='+jenisReport+'&pieKe=13';
						document.getElementById('infoFramePie_14').src='${path}/uw/uw.htm?window=percentageQuestionnaireMultiPieChart&jenisReport='+jenisReport+'&pieKe=14';
					}else if('2' == jenisReport ){
						document.getElementById('barChart').style.display = 'none';
						document.getElementById('pieChart_2').style.display = '';
						document.getElementById('pieChart_1').style.display = 'none';
						document.getElementById('infoFramePie2_1').src='${path}/uw/uw.htm?window=percentageQuestionnaireMultiPieChart&jenisReport='+jenisReport+'&pieKe=1';
						document.getElementById('infoFramePie2_2').src='${path}/uw/uw.htm?window=percentageQuestionnaireMultiPieChart&jenisReport='+jenisReport+'&pieKe=2';
						document.getElementById('infoFramePie2_3').src='${path}/uw/uw.htm?window=percentageQuestionnaireMultiPieChart&jenisReport='+jenisReport+'&pieKe=3';
					}else if( '3' == jenisReport ){
						document.getElementById('barChart').style.display = '';
						document.getElementById('pieChart_2').style.display = 'none';
						document.getElementById('pieChart_1').style.display = 'none';
						document.getElementById('infoFrameBar').src='${path}/uw/uw.htm?window=percentageQuestionnaireMultiPieChart&jenisReport='+jenisReport+'&pieKe=1';
					
					}
				}
			}
		}
		
		function tampil(pormat){
			if(document.getElementById('startDate').value != '' && document.getElementById('endDate').value != '' ){
				var a = '${path}/report/bac.'+pormat+'?window=kuesioner_pelayanan_report' + 
					'&startDate=' + document.getElementById('startDate').value +
					'&endDate=' + document.getElementById('endDate').value;
				popWin(a, 768, 1024);
			}else if(document.getElementById('startDate').value == '' || document.getElementById('endDate').value == ''){
				alert('harap isi dgn lengkap tanggal');
			}
		}
			
		function awal(){
			hideLoadingMessage();
			setFrameSize('infoFrameBar', 60);
		}
		
function popWin(href, height, width,scrollbar,stat, resize) {
	var vWin;
	if(scrollbar!='no')scrollbar='yes';
	if(stat!='yes')stat='no';
	if(resize!='no')resize='yes';

	vWin = window.open(href,'','height='+height+',width='+width+
		',toolbar=no,directories=no,status=no,menubar=no,scrollbars='+scrollbar+',resizable='+resize+',modal=yes,status='+stat+
		',left='+((screen.availWidth-width)/2)+
		',top='+((screen.availHeight-height)/2));
	vWin.opener = self;
} 
	</script>
<body onload="awal();setupPanes('container1','tab1');" onresize="setFrameSize('infoFrameBar', 60)" style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Customer Service Survey Percentage</a>
				</li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form id="formpost" method="post" style="text-align: left;">
											<fieldset style="text-align: left;">
							<legend>Show Report</legend>
							<table class="entry2" style="width: auto;">
								<tr>
								<th nowrap="nowrap">Tanggal</th>
							<th class="left">
										<script>inputDate('startDate', '${startDate}', false);</script> s/d
										<script>inputDate('endDate', '${endDate}', false);</script>		
										&nbsp;&nbsp;
									</th>
								</tr>
									<tr>
									<th nowrap="nowrap"></th>
									<td colspan="2">
									</br>
										<input type="button" name="pdf" value="Show PDF" onclick="tampil('pdf');">
										<input type="button" name="xls" value="Show XLS" onclick="tampil('xls');">
									</td>
								</tr>
								</table>
								</fieldset>
				<fieldset style="text-align: left;">
						<br>	<legend>Persentase</legend>
						<table class="entry2" style="width: 100%;">
													<tr>
									<th style="width: 200px;" > Jenis Chart :</th>
									<td>
									<input class="noBorder" type="radio" name="tipeChart" id="tipe_1" value="1" /> Bar Chart
									&nbsp; &nbsp;
									<input class="noBorder" type="radio" name="tipeChart" id="tipe_2" value="2" /> Pie Chart
									</td>
							</tr>
							<tr>
									<th style="width: 200px;" > Jenis Persentase :</th>
									<td>
								<select name="jenisReport">
										<option value="1">By Question</option>
										<option value="2">By Group</option>
										<option value="3">All</option>
									</select>
									&nbsp;&nbsp;
									<input type="hidden" name="Cari" value="Cari"><input type="button" name="btnCari" value="Show" onClick="cari();">
									&nbsp; ( ${jmlInput} orang )
									</td>
							</tr>	
								
						<tr>
								<td>
								</td>
							</tr>		

</table>
						<table class="entry2" style="width: 100%;">			
							<tr width="100%">
								<td colspan="5">
								<div id="barChart" style="display: none;" >
									<iframe src="" name="infoFrameBar" id="infoFrameBar"
									width="100%" > Please Wait... </iframe>
								</div>
								
								<div id="pieChart_1" style="display: none;">
									<iframe src="" name="infoFramePie_1" id="infoFramePie_1"
									width="400" height="350"> Please Wait... </iframe>
									<iframe src="" name="infoFramePie_2" id="infoFramePie_2"
									width="400" height="350"> Please Wait... </iframe>
									<iframe src="" name="infoFramePie_3" id="infoFramePie_3"
									width="400" height="350"> Please Wait... </iframe>
									<iframe src="" name="infoFramePie_4" id="infoFramePie_4"
									width="400" height="350"> Please Wait... </iframe>
									<iframe src="" name="infoFramePie_5" id="infoFramePie_5"
									width="400" height="350"> Please Wait... </iframe>
									<iframe src="" name="infoFramePie_6" id="infoFramePie_6"
									width="400" height="350"> Please Wait... </iframe>
									<iframe src="" name="infoFramePie_7" id="infoFramePie_7"
									width="400" height="350"> Please Wait... </iframe>
									<iframe src="" name="infoFramePie_8" id="infoFramePie_8"
									width="400" height="350"> Please Wait... </iframe>
									<iframe src="" name="infoFramePie_9" id="infoFramePie_9"
									width="400" height="350"> Please Wait... </iframe>
									<iframe src="" name="infoFramePie_10" id="infoFramePie_10"
									width="400" height="350"> Please Wait... </iframe>
									<iframe src="" name="infoFramePie_11" id="infoFramePie_11"
									width="400" height="350"> Please Wait... </iframe>
									<iframe src="" name="infoFramePie_12" id="infoFramePie_12"
									width="400" height="350"> Please Wait... </iframe>
									<iframe src="" name="infoFramePie_13" id="infoFramePie_13"
									width="400" height="350"> Please Wait... </iframe>
									<iframe src="" name="infoFramePie_14" id="infoFramePie_14"
									width="400" height="350"> Please Wait... </iframe>
								</div>
								<div id="pieChart_2" style="display: none;">
									<iframe src="" name="infoFramePie2_1" id="infoFramePie2_1"
									width="400" height="350"> Please Wait... </iframe>
									<iframe src="" name="infoFramePie2_2" id="infoFramePie2_2"
									width="400" height="350"> Please Wait... </iframe>
									<iframe src="" name="infoFramePie2_3" id="infoFramePie2_3"
									width="400" height="350"> Please Wait... </iframe>
								</div>
								</td>
								
							</tr>
						</table>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>