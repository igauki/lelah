<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	    $().ready(function() {
		$("#tabs").tabs();
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol	 
		
		$("#btnSearch").click(function() {
			var z=$("#spaj").val();
			
					
			if(z=="" || z==null){	
					
				alert("Masukan spaj terlebih dahulu");
				
			}else{
				var url = "${path}/uw/uw.htm?window=npw_aktif&json=1&spaj="+z;
				 			
				  $.getJSON(url, function(result) {
				  $.each(result, function() {
				 		 $("#ppttg").val(this.PPTG);
						 $("#position").val(this.NAMA_POSITION);
						 $("#lspd_id").val(this.LSPID_BEFORE);
						 $("#status").val(this.STATUS);
						
				  });	
				});	
			
			
			}			
	
		});
		$("#aktif").click(function() {
			var ppttg = $("#ppttg").val();
			var pass = $("#pass").val();
			var ket = $("#keterangan").val();
			
			if(pass ==null || pass=="" || ket ==null || ket==""){
			   	alert("Masukan Password/Alasan Terlebih Dahulu!");			   
				return false;
			};		
			
			if(ppttg ==null || ppttg==""){
			   	alert("Data tidak ada , silahkan cari nomor spaj lagi");
				return false;
			}else{
			
				return true;
			};
		
		});
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			};
		};
	});	
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }
</style>

<body>

	<form method="post" name="formpost" enctype="multipart/form-data">
							<fieldset class="ui-widget ui-widget-content">	
								<legend class="ui-widget-header ui-corner-all"><div><h5>Menu Pengaktifkan Polis NPW</h5></div></legend>
									
									<table class = "some" >
									<tr>
									<th>User</th>
									<td> :</td>								
									  <td>									  	
									     <input type="text" name="date" id="date" size="30" value="${date}" readonly="readonly">
									     <input type="text" name="user" id="user" size="20" value="${namaUSer}" readonly="readonly">
					 					 
									  </td>
									  
									</tr>
									<tr>
									  <th>  Masukan SPAJ </th>
									  <td> :</td>
									  <td>
									     <input type="text" name="spaj" id="spaj" size="20" <c:if test="${spaj ne ''}">value="${spaj}" </c:if>>
					 					 <input type="button" value="Search" name="btnSearch" id="btnSearch">
									  </td>
									  
									</tr>
									<tr>
									  <th>  Pemegang/Tertanggung </th>
									  <td> :</td>
									  <td>
									     <input type="text" name="ppttg" id="ppttg" size="50" value="${ppttg}" readonly="readonly"></span>
									  </td>
									  
									</tr>
									<tr>
									  <th>  Posisi Sebelumnya </th>
									  <td> :</td>
									  <td>
									     <input type="text" name="position" id="position" size="50" value="${position}" readonly="readonly">
									     <input type="hidden" name="lspd_id" id="lspd_id" value="${lspd_id} }"> 
									  </td>
									  
									</tr>
									
									<tr>
									  <th> Jenis </th>
									  <td> :</td>
									  <td>
									    <input type="radio" id="type" name="type" value="1" checked="checked">Aktif
									    <input type="radio" id="type2" name="type" value="0" >NPW
									  </td>
									  
									</tr>
									
									<tr>
									  <th> Status Polis </th>
									  <td> :</td>
									  <td>
									  	 <input type="text" id="status" name="status" value="${status}" readonly="readonly">	
									  	
									  </td>									  
									</tr>
									
									<tr>
									  <th> Password </th>
									  <td> :</td>
									  <td>
									  	 <input type="password" id="pass" name="pass" value="${pass}">	
									  </td>									  
									</tr>
									
									<tr>
									  <th> Alasan/Keterangan </th>
									  <td> :</td>
									  <td>
									  	 <textarea rows="3" cols="50" id="keterangan" name="keterangan">${keterangan}</textarea>
									  </td>									  
									</tr>
									</table>
									<div class="rowElem" style="margin-left: 500px;">
										<span><input type="submit" value="Update" name="aktif" id="aktif">
										<input type="hidden" name="app" value="1"> 										
									</div>
									
									<div class="rowElem"> 
									<c:choose>
												<c:when test="${success ne 'BERHASIL'}">
												<font color="red">${success}</font>
												</c:when>
												<c:otherwise>
												<font color="blue">${success}</font>
												</c:otherwise>
									</c:choose>			
									</div>
							</fieldset>
						</form>




</body>