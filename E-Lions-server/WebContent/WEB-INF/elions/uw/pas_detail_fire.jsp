<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<!-- DatePicker Script (jscalendar) -->
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	function cari(){
		if(trim(document.getElementById('kata').value)=='') return false;
		else createLoadingMessage();	
	}
	
</script>
</head>
<BODY onload="resizeCenter(650,400); document.title='PopUp :: Cari Data Pas'; setupPanes('container1', 'tab1'); document.formpost.kata.select(); " style="height: 100%;">

	<div class="tab-container" id="container1" style="width: 1500px;">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">
				Fire Detail
				</a>
			</li>
		</ul>

		<div class="tab-panes" style="width: 1500px;">

			<div id="pane1" class="panes" style="width: 100%;">
				<form method="post" name="formpost" action="#" style="text-align: center;">
					<input type="hidden" name="action" id="action" value="" />
					<input type="hidden" name="msp_id" id="msp_id" value="" />
					<input type="hidden" name="win" value="${param.win}">
					<input type="hidden" name="cari" value="${param.cari}">
					<input type="hidden" name="posisi" value="${param.posisi}">
					<table class="entry2" width="100%">
						<tr>
							<th rowspan="1">Cari:</th>
							<th class="left">
								<select name="tipe">
									<option value="1" <c:if test="${param.tipe eq \"1\" }">selected</c:if>>Nama</option>
									<option value="2" <c:if test="${param.tipe eq \"2\" }">selected</c:if>>No.Bukti Identitas</option>
									<option value="7" <c:if test="${param.tipe eq \"7\" }">selected</c:if>>No.HP</option>
									<option value="3" <c:if test="${param.tipe eq \"3\" }">selected</c:if>>Fire Id</option>
									<option value="4" <c:if test="${param.tipe eq \"4\" }">selected</c:if>>No.Kartu</option>
								</select>
								<select name="pilter">
									<option <c:if test="${\"LIKE\" eq param.pilter}">selected</c:if> value="LIKE">LIKE</option>
									<option <c:if test="${\"LT\" eq param.pilter}">selected</c:if> value="LT"><</option>
									<option <c:if test="${\"LE\" eq param.pilter}">selected</c:if> value="LE"><=</option>
									<option <c:if test="${\"EQ\" eq param.pilter}">selected</c:if> value="EQ">=</option>
									<option <c:if test="${\"GE\" eq param.pilter}">selected</c:if> value="GE">>=</option>
									<option <c:if test="${\"GT\" eq param.pilter}">selected</c:if> value="GT">></option>
								</select>					
								<input type="text" name="kata" id="kata" size="34" value="${param.kata }" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
								<input type="submit" name="search" id="search" value="Search" onclick="return cari();"
									accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
								<input type="button" name="refreshButton" onclick="document.getElementById('search').click();" style="visibility: hidden;"/>
							</th>
						</tr>
					</table>
					<table class="simple">
						<thead>
							<tr>
								<th style="text-align: left">Action</th>
								<th style="text-align: left">Dist.</th>
								<th style="text-align: left">Nama PAS</th>
								<th style="text-align: left">Nama Fire</th>
								<th style="text-align: left">Status SMS</th>
								<th style="text-align: left">Status PAS</th>
								<th style="text-align: left">Tgl. Aksep PAS</th>
								<th style="text-align: left">Status Fire</th>
								<th style="text-align: left">Tgl. Aksep Fire</th>
								<th style="text-align: left">Tgl. Terima SMS</th>
								<th style="text-align: left">Text SMS</th>
								<th style="text-align: left">No. HP PAS</th>
								<th style="text-align: left">No. HP Fire</th>
								<th style="text-align: left">No. Kartu</th>
								<th style="text-align: left">PIN</th>
								<th style="text-align: left">Fire Id</th>
								<th style="text-align: left">SPAJ</th>
								<th style="text-align: left">No. Polis PAS</th>
								<th style="text-align: left">No. Polis Fire</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="pas" items="${cmd.listPas}" varStatus="stat">
								<c:choose>
									<c:when test="${pas.msp_kode_sts_sms eq '00'}">
										<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='arrow';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
								 			>
									</c:when>
									<c:when test="${not empty pas.msp_full_name}">
										<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='arrow';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
								 			>
									</c:when>
									<c:otherwise>
										<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';return true;" onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;">
									</c:otherwise>
								</c:choose>
										<td>
										<input type="button" value="input fire" name="inputDetail"
																onclick="popWin('${path}/uw/input_fire_detail.htm?posisi=1&win=edit&msp_id='+${pas.msp_id}, 350, 450); "
																onmouseover="return overlib('Input PAS Detail', AUTOSTATUS, WRAP);" onmouseout="nd();" />
										</td>
								 		<td>
											<c:choose>
												<c:when test="${pas.dist eq '05'}">
													individu
												</c:when>
												<c:when test="${pas.dist eq '01'}">
													dmtm
												</c:when>
												<c:otherwise>
													-
												</c:otherwise>
											</c:choose>
										</td>
										<td>${pas.msp_full_name}</td>
										<td>${pas.msp_fire_name}</td>
										<td>${pas.msp_desc_sts_sms}</td>
										<td>
											<c:choose>
												<c:when test="${pas.msp_kode_sts_sms eq '00' && pas.lspd_id eq '1'}">
													input detail
												</c:when>
												<c:when test="${pas.lssp_id eq '1' && pas.lspd_id eq '2'}">
													transfer ke uw
												</c:when>
												<c:when test="${pas.lssp_id eq '5' && pas.lspd_id eq '2'}">
													aksep uw
												</c:when>
												<c:when test="${pas.lssp_id eq '5' && pas.lspd_id eq '99' && pas.msp_fire_export_flag == '5'}">
													transfer print polis
												</c:when>
												<c:when test="${pas.lssp_id eq '2' && pas.lspd_id eq '95'}">
													dibatalkan
												</c:when>
												<c:otherwise>
													-
												</c:otherwise>
											</c:choose>
										</td>
										<td><fmt:formatDate value="${pas.msp_pas_accept_date}" pattern="dd/MM/yyyy" /></td>
										<td>
											<c:choose>
												<c:when test="${empty pas.msp_fire_export_flag}">
													belum diaksep uw
												</c:when>
												<c:when test="${pas.msp_fire_export_flag == '0'}">
													belum dikirim
												</c:when>
												<c:when test="${pas.msp_fire_export_flag == '1'}">
													sudah dikirim
												</c:when>
												<c:when test="${pas.msp_fire_export_flag == '2'}">
													resend
												</c:when>
												<c:when test="${pas.msp_fire_export_flag == '3'}">
													resent dikirim
												</c:when>
												<c:when test="${pas.msp_fire_export_flag == '4'}">
													cek resend
												</c:when>
												<c:when test="${pas.msp_fire_export_flag == '5'}">
													diterima
												</c:when>
												<c:when test="${pas.msp_fire_export_flag == '6'}">
													ditolak (${pas.msp_fire_fail_desc})
												</c:when>
												<c:otherwise>
													-
												</c:otherwise>
											</c:choose>
											
										</td>
										<td><fmt:formatDate value="${pas.msp_fire_accept_date}" pattern="dd/MM/yyyy" /></td>
										<td><fmt:formatDate value="${pas.msp_message_date}" pattern="dd/MM/yyyy HH:mm:ss" /></td>
										<td>${pas.msp_text}</td>
										<td>${pas.msp_mobile}</td>
										<td>${pas.msp_fire_mobile}</td>
										<td>${pas.no_kartu}</td>
										<td>${pas.pin}</td>
										<td>${pas.msp_fire_id}</td>
										<td>${pas.reg_spaj}</td>
										<td>${pas.mspo_policy_no}</td>
										<td>${pas.msp_fire_policy_no}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<br>
				</form>
			</div>
		</div>
	</div>

<c:if test="${jml eq 1}">
<script>backToParent1('${v1}', '${v2}');</script>
</c:if>
<c:if  test="${not empty successMessage}">	
			<script type="text/javascript">													
				alert('${successMessage}');	
				document.getElementById('refreshPage').click();
			</script>
	</c:if>
</body>
<%@ include file="/include/page/footer.jsp"%>