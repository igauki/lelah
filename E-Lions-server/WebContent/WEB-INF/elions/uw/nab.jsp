<%@ include file="/include/page/header.jsp"%>
<script>
function getJumlahNab(pos, startDate, endDate){
ajaxManager.selectJumlahNAB(pos, startDate, endDate,
	{callback:function(jml) {
		DWRUtil.useLoadingMessage();
		if(jml==0){
			alert('Maaf, tetapi tidak ada transaksi pada tanggal tersebut.');
			document.formpost.proses.disabled=false;
		}else if(confirm('Terdapat '+jml+ ' transaksi. Lanjutkan proses perhitungan NAB?')){
			document.formpost.jumlah.value=jml;
			document.formpost.submit();
		}else{ 
			document.formpost.proses.disabled=false;
		}
	   },
	  timeout:30000,
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});	
}

</script>
<body style="height: 100%;">
	<div id="contents">
		<fieldset>
		<form method="post" name="formpost">
			<input type="hidden" value="${param.pos}" name="pos">
			<input type="hidden" value="0" name="jumlah">
			<legend>Proses HITUNG UNIT</legend>
			<table class="entry">
				<tr>
					<th>Tanggal Aksep</th>
					<td>
						<input type="text" readonly name="startDate" value="${startDate}" id="startDate" size="12">
						<img src="${path}/include/image/calendar.jpg" align="baseline" title="Calendar" border="0" id="_startDate">
						<script type="text/javascript">
						    Calendar.setup({
						        inputField     :    "startDate",
						        ifFormat       :    "%d/%m/%Y",
						        button         :    "_startDate",
						        align          :    "Tl"
						    });
						</script>														
						s/d 
						<input type="text" readonly name="endDate" value="${endDate}" id="endDate" size="12">
						<img src="${path}/include/image/calendar.jpg" align="baseline" title="Calendar" border="0" id="_endDate">
						<script type="text/javascript">
						    Calendar.setup({
						        inputField     :    "endDate",
						        ifFormat       :    "%d/%m/%Y",
						        button         :    "_endDate",
						        align          :    "Tl"
						    });
						</script>														
					</td>
				</tr>
				<tr>
					<th>Tanggal NAB</th>
					<td>
						<input type="text" readonly name="nabDate" value="${nabDate}" id="nabDate" size="12">
						<img src="${path}/include/image/calendar.jpg" align="baseline" title="Calendar" border="0" id="_nabDate">
						<script type="text/javascript">
						    Calendar.setup({
						        inputField     :    "nabDate",
						        ifFormat       :    "%d/%m/%Y",
						        button         :    "_nabDate",
						        align          :    "Tl"
						    });
						</script>														
					</td>
				</tr>
				<tr>
					<th></th>
					<td>
						<input type="button" name="proses" value="Proses" onclick="this.disabled=true; getJumlahNab(document.formpost.pos.value,document.formpost.startDate.value,document.formpost.endDate.value);">
						<c:choose>
							<c:when test="${not empty sukses}">
								<div id="success"><spring:message code="nab.prosesSuccess" /></div>
							</c:when>
							<c:when test="${not empty gagal}">
								<div id="error">ERROR:<br>
									<c:forEach var="error" items="${gagal}">
										- <c:out value="${error}" escapeXml="false" />
										<br />
									</c:forEach>
								</div>
							</c:when>
						</c:choose>
					</td>	
				</tr>
			</table>
		</form>
		</fieldset>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>