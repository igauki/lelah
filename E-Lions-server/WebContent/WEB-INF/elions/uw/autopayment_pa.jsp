<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		$("#tabs").tabs();
		
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		$("#load").hide();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		
		$("input:submit, input:button, button, input:reset, input:file").button(); //styling untuk semua tombol
		
	    $("#selecctall").click(function(event) {  //on click
	        if(this.checked) { // check select status
	            $(".noBorder").each(function() { //loop through each checkbox
	                this.checked = true;  //select all checkboxes with class "checkbox1"              
	            });
	        }else{
	            $(".noBorder").each(function() { //loop through each checkbox
	                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
	            });        
	        }
	    });
	   
		$("#btnproses").click(function() {
			$("#btnproses").val("Loading....Please Wait...!");
			$("#btnproses").hide();
			$("#tab-1").fadeOut();
			$("#load").fadeIn();
			return true;
		});
	});
</script>


<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

</style>

	<body>
		<div id="load">   
			<img  src="${path}/include/image/loading.gif" alt="loading" />
	        <br /><br />
			<h3>Loading Please Wait....</h3>       
	    </div>
			
		<div id="tab-1">				
			<form id="formPost2" name="formPost2" method="post" target=""  >	
				<table class="displaytag" cellpadding='0' cellspacing='0' border="1" align="center" style="font-size: 11px">
					<tr>															
						<th nowrap align="center" bgcolor="#b7b7b7"><input type="checkbox" id="selecctall"/>Check</th>
						<th nowrap align="justify" width="50" bgcolor="#b7b7b7">No.SPAJ</th>
						<th nowrap align="justify" width="50" bgcolor="#b7b7b7">No.POLIS</th>															
						<th nowrap align="justify" width="150" bgcolor="#b7b7b7">PRODUK</th>
						<th nowrap align="justify" width="150" bgcolor="#b7b7b7">PEMEGANG</th>
						<th nowrap align="justify" width="50" bgcolor="#b7b7b7">PREMI</th>
					</tr>
					<pg:paging url="${path}/uw/uw.htm?window=autopayment_pa" pageSize="15">
						<c:forEach items="${dataSpaj}" var="d">	
							<pg:item>												
								<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';return true;" onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;">															
									<td align="center"><input type="checkbox" class="noBorder" name="chbox" value="${d.SPAJ}" id="chbox"></td>
									<td align="justify" width="50" style="font-size: 10px;">${d.SPAJ}</td>
									<td align="justify" width="50" style="font-size: 10px;">${d.NO_POLIS}</td>
									<td align="justify" width="150" style="font-size: 10px;">${d.NAMA_PRODUK}</td>
									<td align="justify" width="150" style="font-size: 10px;">${d.NAMA_PEMEGANG}</td>															
									<td align="justify" width="50" style="font-size: 10px;">${d.PREMIUM}</td>
								</tr>
							</pg:item>
						</c:forEach>
						<pg:index>
			  				<pg:page><%=thisPage%></pg:page>
						</pg:index>
					</pg:paging>		
				</table>
				<div class="rowElem" style="margin: 50px 0px"> 											   
					<input type="submit" name="btnproses" id="btnproses" value="Proses Payment">																						
					<input type="hidden" name="proses" value="1"> 
				</div> 			
			</form>			
		</div> 
	</body>
</html>


