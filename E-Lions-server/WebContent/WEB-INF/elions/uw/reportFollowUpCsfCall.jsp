<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen"> <!-- CSS untuk DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script><!-- DatePicker Script (jscalendar) -->
	</head>
	<script>
		function awal(){
			setFrameSize('infoFrame', 45);
		}
	</script>
	<body onload="awal();" onresize="setFrameSize('infoFrame', 45);" style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Report Gagal Validasi</a>
				</li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form id="formpost" method="post">
						<table width="100%"class="entry2">
							<tr>
								<th width="10%">Tanggal Validasi:</th>
								<td width="20%">
									<script>inputDate('tanggalAwal', '${tanggalAwal}', false);</script>
									 s/d 
									 <script>inputDate('tanggalAkhir', '${tanggalAkhir}', false);</script>
								</td>
								<td align="left" rowSpan="3"><input type="submit" name="btnShow" value="Show"></td>
							</tr>
							<tr></tr>
							<tr>
								<th></th>
								<th>
									<td align="left" rowSpan="3"></td>
								</th>
							</tr>
							<tr width="100%">
								<td colspan="5">
									<iframe src="" name="infoFrame" id="infoFrame"
									width="100%" > Please Wait... </iframe>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>