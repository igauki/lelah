<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<!-- DatePicker Script (jscalendar) -->
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	function cariData(){
		var pilter = document.getElementById('pilter').value;
		var tipe = document.getElementById('tipe').value;
		var kata = document.getElementById('kata').value;
		
		//document.getElementById('refreshPage').href = 'pas_detail_partner.htm?pilter='+pilter+'&tipe='+tipe+'&kata='+kata;
		window.open('pas_sms_detail_partner.htm?pilter='+pilter+'&tipe='+tipe+'&kata='+kata, '_self');
		//document.getElementById('refreshPage').click();
		
	}
	
	function update(msp_id, action){
		document.getElementById('msp_id').value = msp_id;
		document.getElementById('action').value = action;
		if(trim(document.getElementById('action').value)=='') return false;
		else createLoadingMessage();	
	}
	
	function transfer_process(nama, msp_id){
		if(confirm('Apakah anda yakin untuk TRANSFER PAS BP ('+nama+')?')){
					update(msp_id, 'transfer');
					document.getElementById('transfer').click();
				}else{
					document.getElementById('search').click();
				}
	}
	
	function verify_process(nama, msp_id){
		if(confirm('Apakah anda yakin untuk VERIFIKASI PAS BP ('+nama+')?')){
					update(msp_id, 'verify');
					document.getElementById('verify').click();
				}else{
					document.getElementById('search').click();
				}
	}
	
	function backToParent1(mcl_id, reg_spaj)
	{
			parentForm = self.opener.document.forms['frmParam'];
			if(parentForm) {
				if(parentForm.elements['kopiMclId']){
					parentForm.elements['kopiMclId'].value = mcl_id; 
					parentForm.elements['regSpaj'].value = reg_spaj; 
					parentForm.elements['submitKopiMclId'].click();
					window.close();
				}
			}
			backToParent(mcl_id, reg_spaj);
	}
	function backToParent(mcl_id, reg_spaj){
		parentForm = self.opener.document.forms['formpost'];
		if(parentForm.elements['kopiMclId']){
			parentForm.elements['kopiMclId'].value = mcl_id; 
			parentForm.elements['regSpaj'].value = reg_spaj; 
			
			if(parentForm.elements['flagRefresh']){
				parentForm.elements['flagRefresh'].value = 'refresh'; 
			}
			if(parentForm.elements['searchExistFlag']){
				parentForm.elements['searchExistFlag'].value = 'EXIST';
			}
			if(parentForm.elements['btn_input_blacklist']){
				parentForm.elements['btn_input_blacklist'].value = 'Input Attention List Cari';
			}
			if(parentForm.elements['btn_input_blacklist']){
				parentForm.elements['btn_input_blacklist'].click();
			}
			if(parentForm.elements['submitKopiMclId']){
				parentForm.elements['submitKopiMclId'].click();
			}
			
			window.close();
		}
	}
	
	function bodyEndLoad(){
		var successMessage = '${successMessage}';
		if(successMessage != ''){
				alert(successMessage);	
				document.getElementById('search').click();
				}
	}

</script>
</head>
<BODY onload="resizeCenter(650,400); setupPanes('container1', 'tab1'); document.formpost.kata.select();bodyEndLoad(); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">
				Input PAS BP SMS
				</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" action="#" style="text-align: center;">
					<input type="button" value="REPORT BP" name="rPasSmsPartner" id="rPasSmsPartner"
					onclick="popWin('${path}/uw/viewer/agen.htm?window=report_pas_sms_partner', 350, 450); "
					onmouseover="return overlib('REPORT PAS SMS PARTNER', AUTOSTATUS, WRAP);" onmouseout="nd();" />
					<c:if test="${sessionScope.currentUser.lde_id eq '11' || sessionScope.currentUser.lde_id eq '01'}">
					<input type="button" value="REPORT UW" name="rPasSmsUW" id="rPasSmsUW"
					onclick="popWin('${path}/uw/viewer/agen.htm?window=report_pas_sms_uw', 350, 450); "
					onmouseover="return overlib('REPORT PAS SMS UW', AUTOSTATUS, WRAP);" onmouseout="nd();" />
					</c:if>
					<a href="pas_detail.htm" id="refreshPage" name="refreshPage"></a>
					<input type="button" name="refreshButton" onclick="document.getElementById('search').click();" style="visibility: hidden;"/>
					<input type="submit" name="transfer" id="transfer" value="Transfer" style="visibility: hidden;" onmouseout="nd();"/>
					<input type="submit" name="verify" id="verify" value="Transfer" style="visibility: hidden;" onmouseout="nd();"/>
					<input type="hidden" name="action" id="action" value="" />
					<input type="hidden" name="msp_id" id="msp_id" value="" />
					<input type="hidden" name="win" value="${param.win}">
					<input type="hidden" name="cari" value="${param.cari}">
					<input type="hidden" name="posisi" value="${param.posisi}">
					<table class="entry2">
						<tr>
							<th rowspan="1">Cari:</th>
							<th class="left">
								<select name="tipe">
									<option value="1" <c:if test="${param.tipe eq \"1\" }">selected</c:if>>Nama</option>
									<option value="2" <c:if test="${param.tipe eq \"2\" }">selected</c:if>>No.Bukti Identitas</option>
									<option value="3" <c:if test="${param.tipe eq \"3\" }">selected</c:if>>Fire Id</option>
									<option value="4" <c:if test="${param.tipe eq \"4\" }">selected</c:if>>No.Kartu</option>
								</select>
								<select name="pilter">
									<option <c:if test="${\"LIKE\" eq param.pilter}">selected</c:if> value="LIKE">LIKE</option>
									<option <c:if test="${\"LT\" eq param.pilter}">selected</c:if> value="LT"><</option>
									<option <c:if test="${\"LE\" eq param.pilter}">selected</c:if> value="LE"><=</option>
									<option <c:if test="${\"EQ\" eq param.pilter}">selected</c:if> value="EQ">=</option>
									<option <c:if test="${\"GE\" eq param.pilter}">selected</c:if> value="GE">>=</option>
									<option <c:if test="${\"GT\" eq param.pilter}">selected</c:if> value="GT">></option>
								</select>					
								<input type="text" name="kata" size="34" value="${param.kata }" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
								<input type="button" name="search" id="search" value="Search" onclick="cariData();" />
							</th>
						</tr>
						<tr>
				
						</tr>
					</table>
					<table class="simple">
						<thead>
							<tr>
							    <th style="text-align: left">No. Kartu</th>
								<th style="text-align: left">Nama Tertanggung </th>
								<th style="text-align: left">Tempat</th>
								<th style="text-align: left">Tgl.Lahir</th>
								<th style="text-align: left">No. Identitas</th>
								<th style="text-align: left">No. HP</th>
								<th style="text-align: left">Alamat</th>
								<th style="text-align: left">Kota</th>
								<th style="text-align: left">Kode Pos</th>
								<th style="text-align: left">E-Mail</th>
								<th style="text-align: left">Fire Id</th>
								<th style="text-align: left">Detail</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="pas" items="${cmd}" varStatus="stat">
								<!-- <c:if test="${
									(pas.msp_fire_id == null && sessionScope.currentUser.lde_id ne '29' ) ||
									(pas.msp_fire_id != null && sessionScope.currentUser.lde_id eq '29' ) || sessionScope.currentUser.lde_id eq '01'
									}"> -->
								<tr>
								       <td>${pas.no_kartu}</td>
										<td>${pas.msp_full_name}</td>
										<td>${pas.msp_pas_tmp_lhr_tt}</td>
										<td>${pas.msp_date_of_birth2}</td>
										<td>${pas.msp_identity_no_tt}</td>
										<td>${pas.msp_mobile}</td>
										<td width="200px">${pas.msp_address_1}</td>
										<td>${pas.msp_city}</td>
										<td>${pas.msp_postal_code}</td>
										<td>${pas.msp_pas_email}</td>
										<td>${pas.msp_fire_id}</td>
										<td width="140px">
											<c:choose>
												<c:when test="${pas.msp_fire_id != null}">
													<!--<input type="button" value="INPUT" name="inputDetail"
														onclick="popWin('${path}/uw/input_pas_sms_detail_partner.htm?posisi=1&win=edit&flag_posisi=pas_detail&msp_id='+${pas.msp_id}, 350, 450); "
														onmouseover="return overlib('Input PAS BP SMS Detail', AUTOSTATUS, WRAP);" onmouseout="nd();" />
														<c:choose>
															<c:when test="${pas.msp_pas_nama_pp != null}">
																<input type="button" name="transfer_btn" value="Transfer" onclick="transfer_process('${pas.msp_fire_id}','${pas.msp_id}');"
																onmouseout="nd();"/>
															</c:when>
															<c:otherwise>
																<input type="button" name="transfer_btn" value="Transfer" disabled="disabled"/>
															</c:otherwise>
														</c:choose> -->
												</c:when>
												<c:otherwise>
													<input type="button" name="verify_btn" value="Verify" onclick="verify_process('${pas.msp_fire_id}','${pas.msp_id}');"
													onmouseout="nd();"/>
												</c:otherwise>
											</c:choose>
											
											
										</td>
								</tr>
								<!--</c:if> -->
							</c:forEach>
						</tbody>
					</table>
					<br>
				</form>
			</div>
		</div>
	</div>

</body>
<%@ include file="/include/page/footer.jsp"%>