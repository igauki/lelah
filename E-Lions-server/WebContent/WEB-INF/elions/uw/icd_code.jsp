<%@ include file="/include/page/header.jsp"%>
	<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
	<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
	<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
	<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>
	<script>
		function tambah(){
			formpost.flagAdd.value="1";
			formpost.submit();
		}
	
		function simpan(){
			formpost.flagAdd.value="2";
			formpost.submit();
		}
	
		function hapus(){
			formpost.flagAdd.value="3";
			formpost.submit();		
		}
		
		function keterangan(e,value) {
			var unicode=e.charCode? e.charCode : e.keyCode;
			if(unicode == 13) {
				formpost.flagAdd.value="4";
				formpost.index.value = value;
				formpost.submit();
			}	
		}
		
		function cek(value) {
			alert(value);
		}
	</script>	
	<body onresize="setFrameSize('infoFrame', 45);" style="height: 100%;">
		<form:form id="formpost" name="formpost" commandName="cmd">
			<div class="tabcontent">
				<fieldset>
					<legend>ICD Code [${cmd.spaj}]</legend>
					<table class="entry2">
						<tr>
							<th width="8%">Hapus</th>
							<th width="">ICD Code</th>
							<th>Title</th>	
							<!--<th width="22%">Tanggal</th>-->
							<!--<th width="20%">User Input</th>-->													
						</tr>
						<c:forEach var="s" items="${cmd.lsIcd}" varStatus="xt">
							<tr>
								<td style="text-align: center;vertical-align: bottom;" >
									<c:if test="${sessionScope.currentUser.lus_id eq s.msdi_lus_id}">
										<form:checkbox cssClass="noBorder" path="lsIcd[${xt.index}].cek" id="lsIcd[${xt.index}].cek" value="1"/>
									</c:if>	
								</td>
								<td <c:if test="${!empty s.lic_id}"> style="text-align: center;"</c:if>>
									<c:choose>
										<c:when test="${empty s.lic_id}">
											<spring:bind path="cmd.lsIcd[${xt.index}].lic_id"><br>
												<input type="text" name="${status.expression}" id="${status.expression}" value="${status.value}" style="width: 100%" onfocus="this.select();" onkeypress="keterangan(event,'${xt.index}')"
												<c:if test="${not empty status.errorMessage}">
													style='background-color :#FFE1FD'
												</c:if>>
												<span id="indicator_icd${xt.index}" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
											</spring:bind>
											<ajax:autocomplete
												  source="lsIcd[${xt.index}].lic_id"
												  target="lsIcd[${xt.index}].lic_id"
												  baseUrl="${path}/servlet/autocomplete?s=lsIcd[${xt.index}].lic_id&q=icd"
												  className="autocomplete"
												  indicator="indicator_icd${xt.index}"
												  minimumCharacters="1"
												  parser="new ResponseXmlToHtmlListParser()"/>											
										</c:when>
										<c:otherwise>
												${s.lic_id}
										</c:otherwise>
									</c:choose>								
																		
								</td>
								<td style="vertical-align: bottom;">${s.lic_desc}</td>
								<!--<td style="text-align: center;vertical-align: bottom;"><fmt:formatDate value="${s.msdi_input_date}" pattern="dd/MM/yyyy [h:m:s a]"/></td>-->								
								<!--<td style="text-align: center;vertical-align: bottom;">${s.lus_full_name}</td>-->	
							</tr>
						</c:forEach>
						<tr>
							<td colspan="5" style="text-align: center;">
								<form:hidden path="flagAdd" />
								<input type="button" name="btn_save" value="Save" onclick="simpan();" <c:if test="${cmd.mode eq \'viewer\'}">disabled="disabled"</c:if>>
								<input type="button" name="btn_add" value="Add" onclick="tambah();" <c:if test="${cmd.mode eq \'viewer\'}">disabled="disabled"</c:if>>
								<input type="button" name="btn_delete" value="Delete" onclick="hapus();" <c:if test="${cmd.mode eq \'viewer\'}">disabled="disabled"</c:if>>								
							</td>
						</tr>
					</table>
					<input type="hidden" name="spaj" value="${cmd.spaj}">
					<input type="hidden" name="index" id="index">
				</fieldset>
				<table width="50%">
					<tr>
						<td colspan="2">
							<c:if test="${not empty submitSuccess}">
					        	<div id="success">
						        	Berhasil
					        	</div>
					        </c:if>	
						</td>
					</tr>
				</table>				
			</div>
		</form:form>
	</body>
<%@ include file="/include/page/footer.jsp"%>
