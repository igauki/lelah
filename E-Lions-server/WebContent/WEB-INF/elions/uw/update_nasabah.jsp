<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/header.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />

<html>
	<head>
	
		<script languange="javascript">
			function awal()
			{
			
			//document.formpost.elements['pemegang.mspo_policy_no'].value = formatPolis('${cmd.pemegang.mspo_policy_no}');


		
				if  (((document.formpost.elements['pemegang.mkl_pendanaan'].value).toUpperCase() != ("Lainnya").toUpperCase()) && (document.formpost.elements['pemegang.mkl_pendanaan'].value!=null))
				{
					document.formpost.elements['pemegang.danaa'].value='';
				}else{
					if ((document.formpost.elements['pemegang.danaa'].value).toUpperCase() ==("Lainnya").toUpperCase())
					{
						document.formpost.elements['pemegang.danaa'].value='';
					}
				}

				if  (((document.formpost.elements['pemegang.mkl_kerja'].value).toUpperCase()!=("Lainnya").toUpperCase()) && (document.formpost.elements['pemegang.mkl_kerja'].value!=null))
				{
					document.formpost.elements['pemegang.kerjaa'].value='';
				}else{
					if ((document.formpost.elements['pemegang.kerjaa'].value).toUpperCase()==("Lainnya").toUpperCase())
					{
						document.formpost.elements['pemegang.kerjaa'].value='';
					}
				}
		
				if (((document.formpost.elements['pemegang.mkl_kerja'].value).toUpperCase()!=("Karyawan").toUpperCase()) && (document.formpost.elements['pemegang.mkl_kerja'].value!=null))
				{
					document.formpost.elements['pemegang.kerjab'].value='';
				}else{
					if ((document.formpost.elements['pemegang.kerjab'].value).toUpperCase()==("Karyawan").toUpperCase())
					{
						document.formpost.elements['pemegang.kerjab'].value='';
					} 
				}
						
				if  (((document.formpost.elements['pemegang.mkl_industri'].value).toUpperCase()!=("Lainnya").toUpperCase()) && (document.formpost.elements['pemegang.mkl_industri'].value!=null))
				{
					document.formpost.elements['pemegang.industria'].value='';
				}else{
					if  ((document.formpost.elements['pemegang.industria'].value).toUpperCase()==("Lainnya").toUpperCase())
					{
						document.formpost.elements['pemegang.industria'].value='';
					}
				}
		
		
				
				if  (((document.formpost.elements['tertanggung.mkl_pendanaan'].value).toUpperCase()!=("Lainnya").toUpperCase()) && (document.formpost.elements['tertanggung.mkl_pendanaan'].value!=null))
				{
					document.formpost.elements['tertanggung.danaa'].value='';
				}else{
					if  ((document.formpost.elements['tertanggung.danaa'].value).toUpperCase()==("Lainnya").toUpperCase())
					{
						document.formpost.elements['tertanggung.danaa'].value='';
					}
				}
				
				if  (((document.formpost.elements['tertanggung.mkl_kerja'].value).toUpperCase()!=("Lainnya").toUpperCase()) &&(document.formpost.elements['tertanggung.mkl_kerja'].value!=null))
				{
					document.formpost.elements['tertanggung.kerjaa'].value='';
				}else{
					if  ((document.formpost.elements['tertanggung.kerjaa'].value).toUpperCase()==("Lainnya").toUpperCase())
					{
						document.formpost.elements['tertanggung.kerjaa'].value='';
					}
				}
				
				if (((document.formpost.elements['tertanggung.mkl_kerja'].value).toUpperCase()!=("Karyawan").toUpperCase()) && (document.formpost.elements['tertanggung.mkl_kerja'].value!=null))
				{
					document.formpost.elements['tertanggung.kerjab'].value='';
				}else{
					if ((document.formpost.elements['tertanggung.kerjab'].value).toUpperCase()==("Karyawan").toUpperCase())
					{
						document.formpost.elements['tertanggung.kerjab'].value='';
					}
				}
						
				if  (((document.formpost.elements['tertanggung.mkl_industri'].value).toUpperCase()!=("Lainnya").toUpperCase()) && (document.formpost.elements['tertanggung.mkl_industri'].value!=null))
				{
					document.formpost.elements['tertanggung.industria'].value='';
				}else{
					if ((document.formpost.elements['tertanggung.industria'].value).toUpperCase()==("Lainnya").toUpperCase())
					{
						document.formpost.elements['tertanggung.industria'].value='';
					}
				}
		
			<c:if test="${cmd.pemegang.lsre_id eq  1}">
				pane2.disabled= true;
				pane2.readOnly= true;
				document.formpost.elements['tertanggung.mspe_no_identity'].readOnly = true;
				document.formpost.elements['tertanggung.alamat_rumah'].readOnly = true;
				document.formpost.elements['tertanggung.kota_rumah'].readOnly = true;
				document.formpost.elements['tertanggung.kd_pos_rumah'].readOnly = true;
				document.formpost.elements['tertanggung.area_code_rumah'].readOnly = true;
				document.formpost.elements['tertanggung.telpon_rumah'].readOnly = true;
				document.formpost.elements['tertanggung.area_code_rumah2'].readOnly = true;
				document.formpost.elements['tertanggung.telpon_rumah2'].readOnly = true;
				document.formpost.elements['tertanggung.alamat_kantor'].readOnly = true;
				document.formpost.elements['tertanggung.kota_kantor'].readOnly = true;
				document.formpost.elements['tertanggung.kd_pos_kantor'].readOnly = true;
				document.formpost.elements['tertanggung.area_code_kantor'].readOnly = true;
				document.formpost.elements['tertanggung.telpon_kantor'].readOnly = true;
				document.formpost.elements['tertanggung.area_code_kantor2'].readOnly = true;
				document.formpost.elements['tertanggung.telpon_kantor2'].readOnly = true;
				document.formpost.elements['tertanggung.no_hp'].readOnly = true;
				document.formpost.elements['tertanggung.no_hp2'].readOnly = true;
				document.formpost.elements['tertanggung.email'].readOnly = true;
				
				<c:forEach var="k" items="${cmd.tertanggung.listKeluarga}" varStatus="s">
					document.formpost.elements['tertanggung.listKeluarga[${s.index}].nama'].readOnly = true;
					document.formpost.elements['tertanggung.listKeluarga[${s.index}].tanggal_lahir'].readOnly = true;
				</c:forEach>
				
				document.formpost.elements['tertanggung.mkl_pendanaan'].disabled = true;
				document.formpost.elements['tertanggung.mkl_kerja'].disabled = true;
				document.formpost.elements['tertanggung.mkl_industri'].disabled = true;
				document.formpost.elements['tertanggung.mkl_penghasilan'].disabled = true;
				document.formpost.btnCarikota4.disabled = true;
				document.formpost.btnCarikota5.disabled = true;
				document.formpost.elements['tertanggung.kerjaa'].readOnly = true;
				document.formpost.elements['tertanggung.danaa'].readOnly = true;
				document.formpost.elements['tertanggung.industria'].readOnly = true;
				document.formpost.elements['tertanggung.industria'].readOnly = true;
			</c:if>	
			}
			
		</script>
	
		<title>Form Pengkinian / Pembaharuan Data - PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="Sinarmaslife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path}/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
	</head>
	<body style="height: 100%;text-align: center;"onload="awal();setupPanes('container1', 'tab1');" >
	
	<form:form id="formpost" name="formpost" commandName="cmd" method="post">
	<fieldset>
		<legend>Tanggal BAS Terima Form Pengkinian Data</legend>
		<table class="entry2">
			<tr>
				<tr>						
					<td><script>inputDate('msch_bas_tgl_terima', '${cmd.msch_bas_tgl_terima}', false, '', 9);</script></td>	
				</tr>
			</tr>
		</table>
	</fieldset>
	<fieldset>
	<legend>Update Data</legend>
	<table class="entry2">
		<tr>
			<th colspan="7">
				<h5>Formulir Pengkinian / Pembaharuan Data</h5>
			</th>
		</tr>
		<tr>
			<th rowspan="6" >&nbsp;</th>
			<td>No. Seri</td>
			<td>:</td>
			<td width="25%"><form:input path="pemegang.mspo_no_blanko" cssStyle="width:100%" readonly="true"/></td>
			<td colspan="3">
				<span class="error">Apabila data dalam Formulir ini juga akan digunakan untuk Polis lain, silahkan pilih (centang) No. Polis lain di bawah.</span>
			</td>
		</tr>
		<tr>
			<td>Pemegang Polis</td>
			<td>:</td>
			<td><form:input path="pemegang.mcl_first" cssStyle="width:100%" readonly="true"/></td>
			<c:forEach var="p" items="${cmd.listPolisLain1}" varStatus="s">
				<c:if test="${s.index eq  0  or s.index eq  5 or s.index eq  10}">		
				<td rowspan="5"  width="20%">
					<table class="displaytag">
						<tr>
							<th>No.</th>
							<th></th>
							<th>No. Polis Lain</th>
						</tr>
				</c:if>	
						<tr>
							<td>${s.count}. </td>
							<td>
								<form:checkbox cssClass="noBorder" path="listPolisLain1[${s.index}].cek" value="1"/>
				     			</td>
							<td><form:input path="listPolisLain1[${s.index}].mspo_policy_no" readonly="true"/></td>
						</tr>
			<c:if test="${ s.index eq  4 or s.index eq  9 or s.index eq  14}">		
					
				</table>
			
			</td>
			</c:if>	
			</c:forEach>
		</tr>
		<tr>
			<td>Tertanggung <elions:polis nomor="${cmd.pemegang.mspo_policy_no}"/></td>
			<td>:</td>
			<td><form:input path="tertanggung.mcl_first" cssStyle="width:100%" readonly="true"/></td>
		</tr>
		<tr>
			<td>Produk</td>
			<td>:</td>
			<td><form:input path="datausulan.lsdbs_name" cssStyle="width:100%" readonly="true"/></td>
		</tr>
		<tr>
			<td>Cabang</td>
			<td>:</td>
			<td><form:input path="agen.lsrg_nama" cssStyle="width:100%" readonly="true"/></td>
		</tr>
			<tr ><td colspan="9">
	   <c:if test="${cmd.datausulan.status_submit eq  \"gagal\"}">
           <script type="text/javascript">
          	 	var kar = "Updating data untuk polis - polis ini tidak berhasil "+"\n";
        	   	kar = kar + '${cmd.datausulan.keterangan}'+"\n";
           		alert(kar);
           </script>
            <!--   <p align="center"><b> <font face="Verdana" size="1" > 
                Updating data untuk polis - polis ini tidak berhasil.</font></b> 
				<p align="center"><b> <font face="Verdana" size="1" > 
                ${cmd.datausulan.keterangan}</font></b> -->
          </c:if>
           <c:if test="${cmd.datausulan.status_submit eq  \"berhasil\"}">
        <table border="0" width="100%"  cellspacing="0" cellpadding="0" >
          <tr > 
            <th  colspan="4" height="20" >	
           <script type="text/javascript">
           	alert("Terima Kasih anda telah melakukan pengkinian / pembaharuan data polis.");
           	<c:if test="${cmd.jumlah_sisa_polis gt 0}">
           	var kar = "Berikut polis lain yang belum anda perbaharui datanya. "+"\n";
   	               <c:forEach var="tab" items="${cmd.listPolis2}"> 
							kar= kar + formatPolis('${tab.mspo_policy_no}') +"\n"; 
					</c:forEach> 
					alert(kar);
			</c:if>
           </script>
             <!-- <p align="center"><b> <font face="Verdana" size="1" > 
                Terima Kasih anda telah melakukan pengkinian / pembaharuan data polis.</font></b></p> 
                 <c:if test="${cmd.jumlah_sisa_polis gt 0}">
                	<b> <font face="Verdana" size="1" > 
   	             Berikut polis lain yang belum anda perbaharui datanya.<br>
   	               <c:forEach var="tab" items="${cmd.listPolis2}"> 
							${tab.mspo_policy_no} 
					</c:forEach> 
              </select>
   	             </font></b> 
    	          </c:if>-->
            </th>
          </tr>
        </table>        
          </c:if>
          </td></tr>	
          
          	<tr ><td colspan="9">
          	<table border="0" width="100%"  cellspacing="0" cellpadding="0" >
          <tr > 
            <th  colspan="9" height="20" >
	<spring:bind path="cmd.*"> <c:if test="${not empty status.errorMessages}"> 
	      <script>
	      	var psn='Informasi : \n';
	      	<c:forEach items="${cmd.alert}" var="x" varStatus="xt">
	      		psn+='${xt.index+1}. ${x}\n'
	      	</c:forEach>
	      	psn+='\nJika Data Tidak Lengkap dan Ingin Melanjutkan Proses,\nMaka Isi alasan dan centang checkboxnya.'
	      	alert(psn);
	      </script>
	      <table class="entry2">
	         <tr>
	          	<th>
		          	<form:checkbox cssClass="noBorder" path="cekUpdate" value="1"/>
	          		Lanjut Update Nasabah, Tetapi Data Masih ada Kekurangan.
	          	</th>
	          	<td>
	          		<form:textarea path="alasan" rows="7" cols="60"/>
	          	</td>
	          </tr>
          </table>
          <div id="error"> ERROR:<br>
            <c:forEach var="error" items="${status.errorMessages}"> - <c:out value="${error}" escapeXml="false" /> 
            <br />
            </c:forEach></div>
          </c:if> </spring:bind>
          </th>
          </tr>
        </table> 
          </td></tr>		
		<tr>
			<th></th>
			<td colspan="6">
				<div class="tab-container" id="container1">
				
					<ul class="tabs">
						<li>
							<a href="#" onClick="return showPane('pane1', this)" id="tab1">A. Data Pemegang Polis</a>
							<a href="#" onClick="return showPane('pane2', this)" id="tab2">B. Data Tertanggung</a>
						</li>
					</ul>
			
					<div class="tab-panes">
						<div id="pane1" class="panes">
							<table class="entry2" style="width: auto;">
								<tr>
									<th>1.</th>
									<th>Bukti Identitas</th>
									<td>
										<label for="lside_id_1">
											<form:radiobutton id="lside_id_1" cssClass="noBorder" path="pemegang.lside_id" value="1" />KTP
										</label>
										<br>
										<label for="lside_id_3">
											<form:radiobutton id="lside_id_3" cssClass="noBorder" path="pemegang.lside_id" value="3" />Paspor
										</label>
<!--										<span class="header">No.</span>-->
									</td>
									<th>No.</th>
									<td colspan="2"><form:input path="pemegang.mspe_no_identity" maxlength="50" cssStyle="width:200"/></td>
								</tr>
								<tr>
									<th>2.</th>
									<th>Status</th>
									<td>
										<c:forEach var="sm" items="${select_marital}" varStatus="st">
											<label for="mspe_sts_mrt_${st.count}">
												<form:radiobutton id="mspe_sts_mrt_${st.count}" cssClass="noBorder" path="pemegang.mspe_sts_mrt" value="${sm.ID}" />${sm.MARITAL}
											</label><br/>
										</c:forEach>
									</td>
									<td colspan="3">
										<table class="displaytag" style="width: auto;">
											<tr>
												<th>No.</th>
												<th colspan="2">Nama</th>
												<th>Tanggal Lahir <br>(DD/MM/YYYY)</th>
											</tr>
											<c:forEach var="k" items="${cmd.pemegang.listKeluarga}" varStatus="s">
												<tr>
													<td><form:input path="pemegang.listKeluarga[${s.index}].no"  cssStyle="width:100%" readonly="true"/> </td>
													<td><form:input path="pemegang.listKeluarga[${s.index}].lsre_relation" cssStyle="width:100%" readonly="true"/> </td>
													<td><form:input path="pemegang.listKeluarga[${s.index}].nama"  maxlength="200"/></td>
													<td><form:input path="pemegang.listKeluarga[${s.index}].tanggal_lahir"   />
													</td>
												</tr>
											</c:forEach>
										</table>
									</td>
								</tr>
								<tr>
									<th rowspan="5">3.</th>
									<th rowspan="5">Alamat Rumah</th>
									<td colspan="2" rowspan= "5">
										<form:textarea path="pemegang.alamat_rumah" rows="7" cols="60"/>
									</td>
								</tr>
								<tr><th>Kode Pos</th>
									<td>
										<form:input path="pemegang.kd_pos_rumah" cssStyle="width:70"  maxlength="10"/>
									</td>
								</tr>
								<tr>
									<th>Kota</th>
									<td>
										<form:input path="pemegang.kota_rumah" tabindex="1" size="40" onkeypress="return tampil(document.formpost.btnCarikota);" cssStyle="width:110" maxlength="30"/>
										  	<span id="indicator" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
												<input type="button" class="button" value="Cari" name="btnCarikota1" 
													onclick="popWin('${path}/cari.htm?window=carikota&nama='+document.formpost.elements['pemegang.kota_rumah'].value+'&nama_param=pemegang.kota_rumah', 320, 480); ">
				
									</td>
								</tr>
								<tr>
									<th>Telp 1</th>
									<td>
										<form:input path="pemegang.area_code_rumah"  cssStyle="width:40" maxlength ="4"/>
										<form:input path="pemegang.telpon_rumah" maxlength="20"/>
									</td>
												</tr>
								<tr>
									<th>Telp 2</th>
									<td>
										<form:input path="pemegang.area_code_rumah2"   cssStyle="width:40" maxlength ="4"/>
										<form:input path="pemegang.telpon_rumah2" maxlength="20"/>
									</td>
								</tr>
								<tr>
									<th rowspan="5">4.</th>
									<th rowspan="5">Alamat Kantor</th>
									<td colspan="2" rowspan="5">
										<form:textarea path="pemegang.alamat_kantor" rows="7" cols="60"/>
									</td>
								</tr>
								<tr>
									<th>Kode Pos</th>
									<td>
										<form:input path="pemegang.kd_pos_kantor"  maxlength="10" cssStyle="width:70"/>
									</td>
								</tr>
								<tr>
									<th>Kota</th>
									<td>
										<form:input path="pemegang.kota_kantor" tabindex="1" size="40" onkeypress="return tampil(document.formpost.btnCarikota);" cssStyle="width:110" maxlength="30"/>
										  	<span id="indicator" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
												<input type="button" class="button" value="Cari" name="btnCarikota2" 
													onclick="popWin('${path}/cari.htm?window=carikota&nama='+document.formpost.elements['pemegang.kota_kantor'].value+'&nama_param=pemegang.kota_kantor', 320, 480); ">
			
									</td>
									
								</tr>
								<tr>
									<th>Telp 1</th>
									<td>
										<form:input path="pemegang.area_code_kantor"  maxlength="4"  cssStyle="width:40"/>
										<form:input path="pemegang.telpon_kantor" maxlength="20"/>
									</td>
								</tr>
								<tr>
									<th>Telp 2</th>
									<td>
										<form:input path="pemegang.area_code_kantor2"  maxlength="4"  cssStyle="width:40"/>
										<form:input path="pemegang.telpon_kantor2" maxlength="20"/>
									</td>
								</tr>
								<tr>
									<th rowspan="5">5.</th>
									<th rowspan="5">Alamat Penagihan</th>
									<td colspan="2" rowspan="5">
										<form:textarea path="addressBilling.msap_address" rows="7" cols="60"/>
									</td>
								</tr>
								<tr>
									<th>Kode Pos</th>
									<td>
										<form:input path="addressBilling.msap_zip_code" />
									</td>
								</tr>
								<tr>
									<th>Kota</th>
									<td>
										<form:input path="addressBilling.kota" tabindex="1" size="40" onkeypress="return tampil(document.formpost.btnCarikota);" cssStyle="width:110" maxlength="30"/>
										  	<span id="indicator" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
												<input type="button" class="button" value="Cari" name="btnCarikota3" 
													onclick="popWin('${path}/cari.htm?window=carikota&nama='+document.formpost.elements['addressBilling.kota'].value+'&nama_param=addressBilling.kota', 320, 480); ">
				
	
									</td>
								</tr>
								<tr>
									<th>Telp 1</th>
									<td>
										<form:input path="addressBilling.msap_area_code1"  maxlength="4"  cssStyle="width:40"/>
										<form:input path="addressBilling.msap_phone1" maxlength="20"/>
									</td>
								</tr>
								<tr>
									<th>Telp 2</th>
									<td>
										<form:input path="addressBilling.msap_area_code2"  maxlength="4"  cssStyle="width:40"/>
										<form:input path="addressBilling.msap_phone2" maxlength="20"/>
									</td>
								</tr>
								<tr>
									<th>6.</th>
									<th>No. HP 1</th>
									<td colspan="2">
										<form:input path="pemegang.no_hp" maxlength="20"/>
									</td>
									<th>No. HP 2</th>
									<td >
										<form:input path="pemegang.no_hp2" maxlength="20"/>
									</td>
								</tr>
								<tr>
									<th>7.</th>
									<th>Alamat E-mail 1</th>
									<td colspan="2">
										<form:input path="pemegang.email" maxlength="50" cssStyle="width:200"/>
									</td>
									<th>Alamat E-mail 2</th>
									<td >
										<form:input path="addressBilling.e_mail" maxlength="50" cssStyle="width:200"/>
									</td>
								</tr>
								<tr>
									<th>8.</th>
									<th>Pekerjaan</th>
									<td>
										<select name="pemegang.mkl_kerja">
											<c:forEach var="kerja" items="${select_pekerjaan}">
												<option
													<c:if test="${cmd.pemegang.mkl_kerja eq kerja.ID}"> SELECTED </c:if>
													value="${kerja.ID}">${kerja.KLASIFIKASI}</option>
											</c:forEach>
										</select>
									</td><td></td>
									<th >Lain-Lain, Sebutkan</th>
									<td><form:input path="pemegang.kerjaa" maxlength="100" cssStyle="width:200"/></td>
									
								</tr>
								<tr>
								<th></th>
											<th>Jabatan</th>
											<td><form:input path="pemegang.kerjab" maxlength="100" 	cssStyle="width:200" />
											</td><td></td>
											<td></td>
										<td></td>
								</tr>
								<tr>
									<th>9.</th>
									<th>Bidang Usaha</th>
									<td><select name="pemegang.mkl_industri">
										<c:forEach var="industri" items="${select_industri}">
											<option
												<c:if test="${cmd.pemegang.mkl_industri eq industri.ID}"> SELECTED </c:if>
												value="${industri.ID}">${industri.BIDANG}</option>
										</c:forEach></td><td></td>
									<th>Lainnya, Sebutkan</th>
									<td><form:input path="pemegang.industria" maxlength="100" cssStyle="width:200"/></td>
								
								</tr>
								<tr>
									<th>10.</th>
									<th>Penghasilan per Tahun</th>
									<td><select name="pemegang.mkl_penghasilan">
											<c:forEach var="penghasilan" items="${select_penghasilan}">
												<option
													<c:if test="${cmd.pemegang.mkl_penghasilan eq penghasilan.ID}"> SELECTED </c:if>
													value="${penghasilan.ID}">${penghasilan.PENGHASILAN}</option>
											</c:forEach>
										</select></td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<th>11.</th>
									<th>Sumber Penghasilan</th>
									<td><select name="pemegang.mkl_pendanaan">
										<c:forEach var="dana" items="${select_dana}">
											<option
												<c:if test="${cmd.pemegang.mkl_pendanaan eq dana.ID}"> SELECTED </c:if>
												value="${dana.ID}">${dana.DANA}</option>
										</c:forEach>
									</select></td><td></td>
									<th>Lainnya, Jelaskan</th>
									<td><form:input path="pemegang.danaa" maxlength="100" cssStyle="width:200"/></td>
						
								</tr>
							</table>
	
						</div>
						<div id="pane2" class="panes">
							<table class="entry2" style="width: auto;">
								<tr>
									<th>1.</th>
									<th>Bukti Identitas</th>
									<td>
										<label for="lside_id_1">
											<form:radiobutton id="lside_id_1" cssClass="noBorder" path="tertanggung.lside_id" value="1" />KTP
										</label>
										<br>
										<label for="lside_id_3">
											<form:radiobutton id="lside_id_3" cssClass="noBorder" path="tertanggung.lside_id" value="3" />Paspor
										</label>
										<br>
										<label for="lside_id_2">
											<form:radiobutton id="lside_id_2" cssClass="noBorder" path="tertanggung.lside_id" value="3" />Akte Lahir (Untuk Anak)
										</label>
<!--										<span class="header">No.</span>-->
									</td>
									<th >No.</th>
									<td colspan="2"><form:input path="tertanggung.mspe_no_identity"  maxlength="50" cssStyle="width:200"/></td>
								</tr>
								<tr>
									<th>2.</th>
									<th>Status</th>
									<td>
										<c:forEach var="sm" items="${select_marital}" varStatus="st">
											<label for="mspe_sts_mrt_${st.count}">
												<form:radiobutton id="mspe_sts_mrt_${st.count}" cssClass="noBorder" path="tertanggung.mspe_sts_mrt" value="${sm.ID}" />${sm.MARITAL}
											</label><br/>
										</c:forEach>
									</td>
									<td colspan="3">
										<table class="displaytag" style="width: auto;">
											<tr>
												<th>No.</th>
												<th colspan="2">Nama</th>
												<th>Tanggal Lahir <br> (DD/MM/YYYY)</th>
											</tr>
											<c:forEach var="k" items="${cmd.tertanggung.listKeluarga}" varStatus="s">
												<tr>
													<td><form:input path="tertanggung.listKeluarga[${s.index}].no"  cssStyle="width:100%" readonly="true"/> </td>
													<td><form:input path="tertanggung.listKeluarga[${s.index}].lsre_relation" cssStyle="width:100%" readonly="true"/> </td>
													<td><form:input path="tertanggung.listKeluarga[${s.index}].nama" /></td>
													<td><form:input path="tertanggung.listKeluarga[${s.index}].tanggal_lahir" /></td>
												</tr>
											</c:forEach>
										</table>
									</td>
								</tr>
								<tr>
									<th rowspan="5">3.</th>
									<th rowspan="5">Alamat Rumah</th>
									<td colspan="2" rowspan ="5">
										<form:textarea path="tertanggung.alamat_rumah" rows="7" cols="60"/>
									</td>
								</tr>
								<tr>
									<th>Kode Pos</th>
									<td>
										<form:input path="tertanggung.kd_pos_rumah"  maxlength="10" cssStyle="width:70"/>
									</td>
								</tr>
								<tr>
									<th>Kota</th>
									<td>
										<form:input path="tertanggung.kota_rumah" tabindex="1" size="40" onkeypress="return tampil(document.formpost.btnCarikota);" cssStyle="width:110" maxlength="30"/>
										  	<span id="indicator" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
												<input type="button" class="button" value="Cari" name="btnCarikota4" 
													onclick="popWin('${path}/cari.htm?window=carikota&nama='+document.formpost.elements['tertanggung.kota_rumah'].value+'&nama_param=tertanggung.kota_rumah', 320, 480); ">
										</td>
								</tr>
								<tr>
									<th>Telp 1</th>
									<td>
										<form:input path="tertanggung.area_code_rumah"  maxlength="4"  cssStyle="width:40"/>
										<form:input path="tertanggung.telpon_rumah" maxlength="20"/>
									</td>
								</tr>
								<tr>
									<th>Telp 2</th>
									<td>
										<form:input path="tertanggung.area_code_rumah2"  maxlength="4"  cssStyle="width:40"/>
										<form:input path="tertanggung.telpon_rumah2" maxlength="20"/>
									</td>
								</tr>
								<tr>
									<th rowspan="5">4.</th>
									<th rowspan="5">Alamat Kantor</th>
									<td colspan="2" rowspan="5">
										<form:textarea path="tertanggung.alamat_kantor" rows="7" cols="60"/>
									</td>
								</tr>
								<tr>
									<th>Kode Pos</th>
									<td>
										<form:input path="tertanggung.kd_pos_kantor"  maxlength="10" cssStyle="width:70"/>
									</td>
								</tr>
								<tr>
									<th>Kota</th>
									<td>
									<form:input path="tertanggung.kota_kantor" tabindex="1" size="40" onkeypress="return tampil(document.formpost.btnCarikota);" cssStyle="width:110" maxlength="30"/>
										  	<span id="indicator" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
												<input type="button" class="button" value="Cari" name="btnCarikota5" 
													onclick="popWin('${path}/cari.htm?window=carikota&nama='+document.formpost.elements['tertanggung.kota_kantor'].value+'&nama_param=tertanggung.kota_kantor', 320, 480); ">
										</td>
								</tr>
								<tr>
									<th>Telp 1</th>
									<td>
										<form:input path="tertanggung.area_code_kantor"  maxlength="4"  cssStyle="width:40"/>
										<form:input path="tertanggung.telpon_kantor" maxlength="20"/>
									</td>
								</tr>
								<tr>
									<th>Telp 2</th>
									<td>
										<form:input path="tertanggung.area_code_kantor2"  maxlength="4"  cssStyle="width:40"/>
										<form:input path="tertanggung.telpon_kantor2" maxlength="20"/>
									</td>
								</tr>
								<tr>
									<th rowspan="5">5.</th>
									<th rowspan="5">Alamat Penagihan</th>
									<td colspan="2" rowspan="5">
										<textarea  name="alamat_tagih" readOnly cols="60" rows="7"> ${cmd.addressBilling.msap_address} </textarea>
									</td>
								</tr>
								<tr>
									<th>Kode Pos</th>
									<td>
										<input type="text"  name="kodepos_tagih" value="${cmd.addressBilling.msap_zip_code}" readOnly >
									</td>
								</tr>
								<tr>
									<th>Kota</th>
									<td>
										<input type="text"  name="kota_tagih" value="${cmd.addressBilling.kota}" readOnly >
									</td>
								</tr>
								<tr>
									<th>Telp 1</th>
									<td>
										<input type="text"  name="area_tagih1" value="${cmd.addressBilling.msap_area_code1}" readOnly  maxlength="4" size="6">
										<input type="text"  name="telp_tagih1" value="${cmd.addressBilling.msap_phone1}" readOnly  maxlength="20">
										
									</td>
								</tr>
								<tr>
									<th>Telp 2</th>
									<td>
										<input type="text"  name="area_tagih2" value="${cmd.addressBilling.msap_area_code2}" readOnly   maxlength="4" size="6">
										<input type="text"  name="telp_tagih2" value="${cmd.addressBilling.msap_phone2}" readOnly  maxlength="20">
										
									</td>
								</tr>
								<tr>
									<th>6.</th>
									<th>No. HP 1</th>
									<td colspan="2">
										<form:input path="tertanggung.no_hp" maxlength="20"/>
									</td>
									<th>No. HP 2</th>
									<td >
										<form:input path="tertanggung.no_hp2" maxlength="20"/>
									</td>
								</tr>
								<tr>
									<th>7.</th>
									<th>Alamat E-mail 1</th>
									<td colspan="2">
										<form:input path="tertanggung.email" maxlength="50" cssStyle="width:200"/>
									</td>
									<th>Alamat E-mail 2</th>
									<td>
										<input type="text"  name="email_tagih" value="${cmd.addressBilling.e_mail}" readOnly maxlength="50" size="40">
										
									</td>
								</tr>
								<tr>
									<th>8.</th>
											<th>Pekerjaan</th>
											<td>
												<select name="tertanggung.mkl_kerja">
													<c:forEach var="kerja" items="${select_pekerjaan}">
														<option
															<c:if test="${cmd.tertanggung.mkl_kerja eq kerja.ID}"> SELECTED </c:if>
															value="${kerja.ID}">
															${kerja.KLASIFIKASI}
														</option>
													</c:forEach>
												</select>
											</td><td></td>
											<th>
												Lain-Lain, Sebutkan
											</th>
											<td>
												<form:input path="tertanggung.kerjaa" maxlength="100" 	cssStyle="width:200" />
											</td>
										<td></td>
								</tr>
								<tr>
								<th></th>
											<th>Jabatan</th>
											<td><form:input path="tertanggung.kerjab" maxlength="100" 	cssStyle="width:200" />
											</td><td></td>
											<td></td>
											<td></td>
										<td></td>
								</tr>
								<tr>
									<th>9.</th>
									<th>Bidang Usaha</th>
									<td><select name="tertanggung.mkl_industri">
										<c:forEach var="industri" items="${select_industri}">
											<option
												<c:if test="${cmd.tertanggung.mkl_industri eq industri.ID}"> SELECTED </c:if>
												value="${industri.ID}">${industri.BIDANG}</option>
										</c:forEach></td><td></td>
									<th>
												Lain-Lain, Sebutkan
											</th>
											<td>
												<form:input path="tertanggung.industria" maxlength="100"	cssStyle="width:200" />
											</td>
										<td></td>
								</tr>
								<tr>
									<th>10.</th>
									<th>Penghasilan per Tahun</th>
									<td><select name="tertanggung.mkl_penghasilan">
											<c:forEach var="penghasilan" items="${select_penghasilan}">
												<option
													<c:if test="${cmd.tertanggung.mkl_penghasilan eq penghasilan.ID}"> SELECTED </c:if>
													value="${penghasilan.ID}">${penghasilan.PENGHASILAN}</option>
											</c:forEach>
										</select></td>
																		<td></td>
									<td></td><td></td>
								</tr>
								<tr>
									<th>11.</th>
									<th>Sumber Penghasilan</th>
									<td><select name="tertanggung.mkl_pendanaan">
										<c:forEach var="dana" items="${select_dana}">
											<option
												<c:if test="${cmd.tertanggung.mkl_pendanaan eq dana.ID}"> SELECTED </c:if>
												value="${dana.ID}">${dana.DANA}</option>
										</c:forEach>
									</select></td>
									<td></td>
									<th>
												Lain-Lain, Sebutkan
											</th>
											<td>
												<form:input path="tertanggung.danaa" maxlength="100"	cssStyle="width:200" />
											</td>
									<td></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
	<table>
		<tr>
			<td>
					<input type="submit" value="Save" >
				<br>
			</td>
		</tr>
	</table>	
</fieldset>
         
	</form:form>
				
				  
	</body>
</html>