<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript" src="${path}/include/js/jquery-1.3.2.min.js"></script>
<script>
$(document).ready(function() {
        <c:if test="${not empty pesanError}">
            alert('${pesanError}');
        </c:if>
    });
</script>
<c:choose>    
    <c:when test="${not empty hak}">
        <div id="error">
            Maaf, No SPAJ ini sudah proses REAS, tidak bisa diedit.
        </div>
    </c:when>
    <c:otherwise>
   <body onload="document.title='PopUp :: Edit Polis';setupPanes('container1','tab1');" style="height: 100%;">
     <div class="tab-container" id="container1">
              <ul class="tabs">
                <li>
                    <a href="#" onClick="return showPane('pane1', this)" id="tab1">Proses Akseptasi Endorsment</a>
                </li>
            </ul>
            <div class="tab-panes">
                <div id="pane1" class="panes">
                     <form id="formpost" name="formpost" method="post">
           <fieldset>
              <legend>Info User</legend>
                 <table class="entry2">
                   <tr>
                       <th nowrap="nowrap">User</th>
                       <th align ="left">&nbsp;[${infoDetailUser.LUS_ID}]&nbsp;&nbsp;${infoDetailUser.LUS_LOGIN_NAME}</th>
                   </tr>
                   <tr>
                       <th nowrap="nowrap">Nama<br></th>
                       <th align ="left">&nbsp;${infoDetailUser.LUS_FULL_NAME}&nbsp;[${infoDetailUser.LDE_DEPT}]</th>
                   </tr>
                </table>
         </fieldset>
         <fieldset>
         <legend>
			Akseptasi Endorsment 
	     </legend>
                <table class="entry2">
                   <tr>
                       <th><b>Status Akseptasi Endors</b><br></th>
<!--                        <th align ="left">&nbsp;${infoEndorsNew.KET_AKSEP_UW}</th>  -->
                       <td align ="left"><font color="green">&nbsp;${infoEndorsNew.KET_AKSEP_UW}</font></td>                  
                   </tr>                
<!--                    <tr> -->
<!--                        <th colspan="2" nowrap="nowrap">&nbsp;</th>                    -->
<!--                    </tr> -->
               	   <tr>
					   <th nowrap="nowrap">Pilih Akseptasi </th>
					   <th nowrap="nowrap" class="left">
						<select name="pilaksepuw">
							<c:forEach var="pil" items="${select_aksep_endors}">
							<option
								<c:if test="${endors.msen_aksep_uw eq pil.ID}"> SELECTED </c:if>
								value="${pil.ID}">${pil.AKSEP}
							</option>
							</c:forEach>
						</select>
					   </th>
				   </tr>
				   <tr>
					   <th nowrap="nowrap">Keterangan</th>
					   <th nowrap = "nowrap" align ="left">
						   <input type ="text" size = 40 name="ket_aksep_uw"/><font class="error">*</font>
					   </th>       		
	          	   </tr>
	               <tr>
                       <th colspan="2" nowrap="nowrap">&nbsp;</th>                
                   </tr>
          	       <tr align = "left">
                       <th nowrap="nowrap">&nbsp;</th>          	   
                       <th nowrap="nowrap"> 
                           <input type="hidden" name="save" id="save">  
                           <input type="submit" name="save" value="Save" onclick="return confirm('Apakah Anda Yakin Untuk Melanjutkan Proses Akseptasi Endors ?');"> 
                       </th>
                   </tr>                           	   
               </table>
        </fieldset>
               
               <tr><td></td></tr>
                <tr align = "left">
                        <th>&nbsp;</th>
                </tr>
                <tr>
                        <td><font color="#ff0000"></font></td>
                </tr>
        </div>
      </div>
   </div>
    </c:otherwise>
 </c:choose>
</body>
<%@ include file="/include/page/footer.jsp"%>