<%@ include file="/include/page/header.jsp"%>
<script>

	
	function awal(){
		setFrameSize('infoFrame', 45);
	}
	
	function tekan(btn){
		var nospaj='';
		if(document.formpost.spaj.options.length>0)
			nospaj=document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value
		
		if(btn=='c'){
			popWin('${path}/uw/spaj.htm?posisi=-1&win=uw', 350, 450); 
		}else if(btn=='u'){
			if(nospaj!='')
				document.getElementById('infoFrame').src='${path}/uw/update_nasabah.htm?spaj='+nospaj;
			else{
				alert("Silahkan cari s Terlebih Dahulu..");
				popWin('${path}/uw/spaj.htm?posisi=-1&win=uw', 350, 450); 
			}	
		}
	}
	
</script>
<body  onload="awal();" onresize="setFrameSize('infoFrame', 45);" style="height: 100%;">
<form name="formpost" method="post">
<div class="tabcontent">
<fieldset>
<legend>Update Data Nasabah (Pengkinian Data Nasabah)</legend>
<table class="entry2" style="width: 98%;">
	<tr>
		<th>Cari SPAJ</th>
		<td> 
			<select name="spaj" onChange="cariregion(this.options[this.selectedIndex].value,'region');">
				<c:forEach var="s" items="${daftarSPAJ}">
					<option value="${s.REG_SPAJ }" style="background-color: ${s.BG};" 
						<c:if test="${s.REG_SPAJ eq param.spaj }">selected<c:set var="ada" value="ada" /></c:if>>${s.SPAJ_FORMATTED}
					- ${s.POLICY_FORMATTED }</option>
				</c:forEach>
			</select>

			<input type="button" value="Info" name="info"
				onclick="tampil(document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);"
				accesskey="I" onmouseover="return overlib('Alt-I', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Cari" name="search"
				onclick="tekan('c')"
				accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Update" name="update"
				onclick="tekan('u');"
				accesskey="U" onmouseover="return overlib('Alt-U', AUTOSTATUS, WRAP);" onmouseout="nd();">
				
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
				width="100%"  height="100%"> Please Wait... </iframe>
		</td>
	</tr>
</table>
</fieldset>
</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>