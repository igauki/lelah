<%@ include file="/include/page/header.jsp"%>
<script>
	function transper(){
		if(confirm('Transfer ke Filling?')){
			window.location = '${path }/uw/transfer_ttp.htm?spaj=${param.spaj}';
		}
	}
</script>
<c:choose>
<c:when test="${cmd.block  eq 1 }" >
	<div id="error" style="text-align:center; text-transform:uppercase;">SPAJ ini sedang diajukan redeem, tidak dapat proses lebih lanjut!</div>
</c:when>
<c:otherwise>
<body style="height: 100%;">
	<div id="contents">
		<fieldset>
			<form method="post" name="formpost">
				<input type="hidden" value="${param.spaj}" name="spaj">
				<legend>Transfer ke Print Polis</legend>
				<table class="entry">
					<tr>
						<td>
							<c:choose>
								<c:when test="${not empty param.submitSuccess}">
									<div id="success">
										${param.submitSuccess}
										${param.pesanDirectPrint}
										<c:if test="${not empty param.transferTTP}">
											<br/><br/>
											<input type="button" value="Transfer to Komisi/Filling" onclick="transper();">
										</c:if>
									</div>
								</c:when>
								<c:otherwise>
									<div id="success">
									<ul>
										<c:if test="${not empty cmd.kuranglebih}">
											<li>
												${cmd.kuranglebih}
												<br>Alokasi Sisa : 
												<select name="selectKurang">
													<option value="1">Selisih Kurs</option>
													<option value="2">Potong Komisi</option>
												</select>
												Password : <input type="password" id="inpPass" name="password" value="">
												<br><br>
											</li>
										</c:if>
										<c:if test="${not empty cmd.kuranglebihTopUp}">
											<li>
												${cmd.kuranglebihTopUp}
												<br>Alokasi Sisa : 
												<select name="selectKurangTopUp">
													<option value="1">Selisih Kurs</option>
													<option value="2">Potong Komisi</option>
												</select>
												Password : <input type="password" id="inpPass" name="password" value="">
												<br><br>
											</li>
										</c:if>
										<c:if test="${not empty cmd.konfirmasiRK}">
											<li>
												${cmd.konfirmasiRK}
												<br>- Bulan Produksi:
												<select name="selectBulan">
													<option value="1">Produksi Bulan Sekarang</option>
													<option value="2">Produksi Bulan Berikutnya</option>
												</select>
												<br><br>
											</li>
										</c:if>
										<li>
											<input type="submit" name="transfer" value="Transfer [<elions:spaj nomor="${param.spaj}"/>] ke Print Polis!"
												accesskey="A" onmouseover="overlib('Alt-A', AUTOSTATUS, WRAP);" onmouseout="nd();">
											
										</li>	
									</ul>
									</div>
								</c:otherwise>
							</c:choose>
							
							<spring:bind path="cmd.*">
								<c:if test="${not empty status.errorMessages}">
									<div id="error" align="left">
									ERROR:<br>
									<c:forEach var="error" items="${status.errorMessages}">
												- <c:out value="${error}" escapeXml="false" />
										<br />
									</c:forEach>
									</div>
								</c:if>
							</spring:bind>

						</td>
					</tr>
					
					<tr>
					<td>
						<c:if test="${not empty confirmMessage}">
						<div class="successBox">
							${confirmMessage}
							<script>alert('${confirmMessage}');</script>
						</div>
						</c:if>
					</td>
					</tr>
				</table>
			</form>
		</fieldset>
	</div>
</body>
</c:otherwise>
</c:choose>
<%@ include file="/include/page/footer.jsp"%>