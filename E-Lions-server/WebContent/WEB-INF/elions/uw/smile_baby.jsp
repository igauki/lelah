<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}    
        
        $('input[name=type]').change(function() {
			if($("input[name=type]:checked").val()=='0')$("#prod").show();
        	else $("#prod").hide();
		});
	});
	
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; padding:5px;}
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 8px; border-bottom:1px solid #ccc; }
	fieldset .rowElem label { margin-right: 1em; width: 16em; display: inline-block; text-align:right; font-weight:bold;}
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	.error1 { text-align:center; text-transform:uppercase; 	color: red;
				background: #ffc; border: 1px solid #4682b4; padding: 2px; padding-left: 4px;
				margin: 5px 0px 5px 0px; font-family: verdana, geneva, arial, helvetica, sans-serif;
				font-size: 1.6em;}

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }
	
	#mggu {width: 30px;;}
</style>

<c:choose>
	<c:when test="${empty err}">
		<body>
			<form name="formPost" method="post">
				<fieldset class="ui-widget ui-widget-content">
					<legend class="ui-widget-header ui-corner-all">SMiLe Baby</legend>
					<div class="rowElem">
						<label>No. SPAJ</label> <span>${spaj}</span>
					</div>
					<div class="rowElem">
						<label>Nama Pemegang Polis</label> <span>${namapp}</span>
					</div>
					<div class="rowElem">
						<label>Nama Tertanggung</label> <span>${namatt}</span>
					</div>
					<div class="rowElem">
						<label>Perkiraan Tanggal Lahir</label> 
						<input name="bdate"	id="bdate" type="text" class="datepicker" value="${perkiraan}" >
					</div>
					<div class="rowElem">
						<label>Umur Kandungan</label> 
						<input name="mggu"	id="mggu" type="text" value="${mggu}"> Minggu
					</div>
					<div class="rowElem">
						<label></label> 
						<input type="submit" name="save" id="save" value="Submit" onclick="cek()"> 
						<input type="hidden" name="xaxa" value="1">
					</div>

				</fieldset>
			</form>
		</body>
	</c:when>
	<c:otherwise>
		<div class="error1">${err}</div>
	</c:otherwise>
</c:choose>

<script>
	function cek(){
		var spaj = '${spaj}';
		var x = document.getElementById("bdate").value; 
		var y = document.getElementById("mggu").value; 
		var result = true;
		if (x == "" || y==""){
			alert("Harap menigisi semua kolom terlebih dahulu");
// 			document.getElementById("save").disabled = true;
			result = false;
		}
		
		if (y<20 || y>32){
				alert("Usia Kanduangan tidak sesuai dengan Kententuan Produk");
				result = false;
		}
		
		if (result==true){
			alert("Berhasil Input \n Tanggal Perkiraan Lahir: " +x+"\n Umur Kehamilan: "+y+" minggu.");	
		}
		
	}
	
</script>

<%@ include file="/include/page/footer.jsp"%>