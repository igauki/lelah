<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen"> <!-- CSS untuk DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script><!-- DatePicker Script (jscalendar) -->
	</head>
	<script>
		function tampil(){
			var cek = formpost.email.checked ;
			var to = formpost.to.value;
			var cc = formpost.cc.value;
			var pdf = formpost.pdfAtt.checked ;
			var xls = formpost.excelAtt.checked ;
			
			lca_id=formpost.lca_id.value;
			tgl1=formpost.tanggalAwal.value;
			tgl2=formpost.tanggalAkhir.value;
			
			window.location='${path }/report/uw.htm?window=report_akseptasi_khusus&tanggalAwal='+tgl1+'&tanggalAkhir='+tgl2+'&lca_id='+lca_id+'&cekMail='+cek+'&to='+to+'&isPDF='+pdf+'&isXls='+xls;
			
		}
	
		function show() {
			var cek = formpost.email.checked ;
		
			if(cek == true) {
				document.getElementById("showMail").style.visibility = "visible";
				document.getElementById("btnShow").value = "SEND";
			}
			else {
				document.getElementById("showMail").style.visibility = "hidden"; mailto:
				document.getElementById("btnShow").value = "SHOW"
			}
		}
		
		function kuis1(idx){
		
		}
	</script>
	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Report Akseptasi Khusus</a>
				</li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form id="formpost" method="post">  &nbsp;<table class="entry2">
							<tr>
								<th width="10%">Tanggal :</th>
								<td width="20%">
									<script>inputDate('tanggalAwal', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>
									 s/d 
									 <script>inputDate('tanggalAkhir', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script> </td>
								<td align="left" rowspan="3"> <input type="button" name="btnShow" value="SHOW" id="btnShow" onclick="tampil();"></td><th></th></tr>
							<tr>
								<th>Nama Cabang</th>
								<td>
									<select name="lca_id" >
										<option value="0">---Pilih---</option>
										<c:forEach items="${lsCabang}" var="x" varStatus="xt">
											<option value="${x.key}" 
													<c:if test="${x.key eq lca_id}">selected</c:if> >
													${x.value}
											</option>
										</c:forEach>		
								</td>			
							</tr>
							<tr>
								<th></th>
								<td><input type="checkbox" id="email" onclick="show()">Email Report</td>
							</tr>
							<tr>
								<th></th>
								<td>
									<div id="pane3" class="panes">
										<table class="entry2">
											<tr>
												<td>
													<div id="showMail" style="visibility: hidden">
														
														To: <input type="text" readonly value="Sugeng@ekalife2000.com;" name="to" id="to" size="60" >
														CC: <input type="hidden" value="yusuf@sinarmasmsiglife.co.id;" name="cc" id="cc" size="60">
														<div><input type="checkbox" id="excelAtt" >Email Report dalam bentuk Excel</div>
														<div><input type="checkbox" id="pdfAtt" >Email Report dalam bentuk PDF</div>
														
													</div>
												</td>
											</tr>	
										</table>
									</div>
								</td>
							</tr>
							
							</tr>
							<tr>
								<td colspan="2">
									<div id="error">
						        		Daftar Cabang yang terakseptasi khusus hari ini :<br>
						        			<c:if test="${flagCabang eq 1}">Tidak Ada</c:if>
						        			<c:forEach var="error" items="${lsCabangToday}" >
												- <c:out value="${error.value}" escapeXml="false" />
												<br/>
											</c:forEach>
									</div>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>

								