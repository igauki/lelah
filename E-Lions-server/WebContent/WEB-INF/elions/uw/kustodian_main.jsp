<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript">

	var current = 'LaporanKustodianNewBusiness';
	
	ajaxSelectMultipleWithParam(42, 'selectDaftarSPAJUnitLink', '_pilihSpaj', 'pilihSpaj', 'REG_SPAJ', 'SPAJPOLIS', 24);

	function tampilkan(forminput, v){
		document.getElementById('formatReport').value = "." + v;
		document.getElementById(forminput).submit();
		return false;
	}

	function tampilkan2(forminput, report, format){
//		if(forminput!='formSuratPermintaanAlokasiInvestasi') if(!cekSpaj2()) return false;
		document.getElementById(forminput).action='${path}/report/kustodian.'+format+'?window='+report;
		document.getElementById(forminput).submit();
		return false;
	}

	function showww(id){
		document.getElementById('transfer').style.display = 'none';
		document.getElementById(trimWhitespace(id)).style.display = 'block';
		document.getElementById(current).style.display = 'none';
		current = trimWhitespace(id);		
		if(current=='LaporanKustodianNewBusiness'){
			document.getElementById('transfer').style.display = 'block';
			ajaxSelectMultipleWithParam(42, 'selectDaftarSPAJUnitLink', '_pilihSpaj', 'pilihSpaj', 'REG_SPAJ', 'SPAJPOLIS', 24);
		}
	}

	function cekSpaj2(){
		var pilih = document.getElementById('pilihSpaj');
		if(pilih){
			if(!pilih.value){ 
				alert('Harap pilih minimal 1 nomor Polis / SPAJ.');
				return false;
			}
		}
		return true;
	}
	
	function cekSpaj(posisi){
		var pilih = document.getElementById('pilihSpaj');
		if(pilih){
			if(!pilih.value){ 
				alert('Harap pilih minimal 1 nomor Polis / SPAJ.');
				return false;
			}
		}
		
		if(posisi==42 || posisi==46 || posisi==51 || posisi==61){
			return confirm('Harap pastikan anda sudah mencetak LAPORAN KUSTODIAN sebelum transfer ke PROSES NAB.');
		}else if(posisi==48 || posisi==53 || posisi==63){
			return confirm('Harap pastikan anda sudah mencetak SURAT TRANSAKSI sebelum transfer ke FILLING.');
		}

		return false;
	}
	
	function searchLap(flag){
		if(flag=="1"){
			var tgl = (document.getElementById('tanggalFile').value).replace("/", "").replace("/", "");
			var jen=document.getElementById('jenisnya').value;
			var jam=document.getElementById('jamduanya').value;
			var dk=document.getElementById('dk').value;
			if(dk=="D")dk="debet";else dk="kredit"
			popWin('${path}/common/util.htm?window=download&file='+tgl+'.pdf&dir=${cmd.dirLaporan}'+jen+'\\'+jam+'\\'+dk, 600, 800);
		}else if(flag=="2"){
			var tgl = (document.getElementById('tanggalAwal').value).replace("/", "").replace("/", "");
			var jam=document.getElementById('jamduanya').value;
			var dk=document.getElementById('dk').value;
			var rep=document.getElementById('repotnya').value;
			if(dk=="D")dk="debet";else dk="kredit"
			popWin('${path}/common/util.htm?window=download&file='+tgl+'.pdf&dir=${cmd.dirSurat}'+dk+'\\'+jam+'\\'+rep, 600, 800);
		}	
	}
</script>
<body style="height: 100%;">
	<div id="contents">
		<fieldset>
			<legend>Laporan Kustodian</legend>
			<table class="entry2">
				<tr>
					<th style="width: 60px;">Laporan</th>
					<td>
						<SELECT name="pilih" onchange="showww(this.value);">
							<c:forEach var="menu" items="${cmd.daftarMenu}">
								<option value="${menu}">${menu}</option>
							</c:forEach>
						</SELECT>
					</td>
				</tr>
				<tr>
					<th></th>
					<td>
					
						<!-- ====== -->
						<form name="formpost" method="post" action="kustodian.htm" target="_blank" id="formLaporanKustodianNewBusiness">

							<fieldset id="LaporanKustodianNewBusiness">
								<legend>Cetak Laporan</legend>
								<input type="hidden" name="laporan" value="true">
								<TABLE class="entry2">							
									<tr>
										<th style="width: 150px; ">Jenis Report</th>
										<td>
											<label for="radio1"><input id="radio1" type="radio" class="noBorder" onclick="document.getElementById('jenisnya').value='kustodi';" name="jenis" value="kustodi" checked="checked">Bank Kustodi</label>
											<label for="radio2"><input id="radio2" type="radio" class="noBorder" name="jenis" onclick="document.getElementById('jenisnya').value='finance';" value="finance">Finance</label>
											<input type="hidden" value="kustodi" id="jenisnya" name="jenisnya">
										</td>
									</tr>
									<tr>
										<th>Waktu Transaksi</th>
										<td>
											<label for="sebelum"><input class="noBorder" type="radio" id="sebelum" onclick="document.getElementById('jamduanya').value='before';" name="jamdua" value="0" checked="checked">Sebelum ${cmd.cutoff}</label>
											<label for="sesudah"><input class="noBorder" type="radio" id="sesudah"  onclick="document.getElementById('jamduanya').value='after';" name="jamdua" value="1">Sesudah ${cmd.cutoff}</label>
											<input type="hidden" value="before" id="jamduanya" name="jamduanya">
										</td>
									</tr>
									<tr>
										<th>Jenis Transaksi</th>
										<td>
											<select name="dk" id="dk">
												<option value="D">Pemasukkan</option>
												<option value="K">Pengeluaran</option>
											</select>
										</td>
									</tr>
									<tr>
										<th>Tanggal Kirim Berkas</th>
										<td>
											<script>inputDate('tanggalKirim', '<fmt:formatDate value="${cmd.now}" pattern="dd/MM/yyyy"/>', false);</script>
										</td>
									</tr>
									<tr>
										<th>Catatan Kaki</th>
										<td>
											<select name="kaki">
												<option value="">-</option>
												<option value="Pemindahan Dana Investasi Dynamic Fund ke Fixed Income">Pemindahan Dana Investasi Dynamic Fund ke Fixed Income</option>
												<option value="Pemindahan Dana Investasi Fixed Income ke Dynamic Fund">Pemindahan Dana Investasi Fixed Income ke Dynamic Fund</option>
											</select>
										</td>
									</tr>
									<tr>
										<th></th>
										<td>
											<a href="#" onclick="return tampilkan2('formLaporanKustodianNewBusiness', 'LaporanKustodianNewBusiness', 'html');"
												onmouseover="return overlib('VIEW', AUTOSTATUS, WRAP);" onmouseout="nd();">
												<img style="border:none;" alt="HTML" src="${path}/include/image/page_html.gif"/></a>

											<a href="#" onclick="return tampilkan2('formLaporanKustodianNewBusiness', 'LaporanKustodianNewBusiness', 'pdf');"
												onmouseover="return overlib('PRINT', AUTOSTATUS, WRAP);" onmouseout="nd();">
												<img style="border:none;" alt="PDF" src="${path}/include/image/pdf.gif"/></a>

											<a href="#" onclick="return tampilkan2('formLaporanKustodianNewBusiness', 'LaporanKustodianNewBusiness', 'xls');"
												onmouseover="return overlib('PRINT', AUTOSTATUS, WRAP);" onmouseout="nd();">
												<img style="border:none;" alt="EXCEL" src="${path}/include/image/excel.gif"/></a>
										</td>
									</tr>
									<tr>
										<th colspan="2">
											<fieldset id="search">
												<legend>Search</legend>
												<span>Pilih file yang dicari berdasarkan kategori di atas, dan masukkan tanggal laporan yang ingin dicari. <br/> Kemudian tekan tombol cari file untuk mulai mencari.</span>
												<table class="entry2" width="100%">
													
													<tr>
														<th style="width: 150px; ">Tanggal Laporan</th>
														<td>
															 <script>inputDate('tanggalFile', '<fmt:formatDate value="${cmd.now}" pattern="dd/MM/yyyy"/>', false);</script>
														</td>
													</tr>
													<tr>
														<th>
															
														</th>
														<td>
															<input type="button" name="srcLap" value="cari file" onclick="searchLap('1');">
														</td>
													</tr>
												</TABLE>
											</fieldset>
										</th>
									</tr>
								</TABLE>	
							</fieldset>
							

							<fieldset id="Transfer">
								<legend>Transfer Polis</legend>
								<INPUT type="hidden" name="window" value="LaporanKustodianNewBusiness">
								<INPUT type="hidden" name="lspd_id" value="42">
								
								<TABLE class="entry2">
									<tr>
										<th style="width: 150px; ">Pilih Polis / SPAJ</th>
										<td>
											<div id="_pilihSpaj"></div>
											<span class="info">Gunakan tombol CTRL + Click untuk memilih lebih dari satu SPAJ</span><br/>
											<INPUT type="submit" name="transfer" value="Transfer" onclick="return cekSpaj(42);">
										</td>
									</tr>
								</TABLE>
							</fieldset>
							
						
							
						</form>

						<!-- ====== -->
						<fieldset id="SummaryKustodianNewBusiness" style="display:none;">
							<form name="formpost" method="post" target="_blank" id="formSummaryKustodianNewBusiness">
								<INPUT type="hidden" name="lspd_id" value="43">
								<TABLE class="entry2">
									<tr>
										<th style="width: 150px; ">Jenis Transaksi</th>
										<td>
											<select name="dk">
												<option value="D">Pemasukkan</option>
												<option value="K">Pengeluaran</option>
											</select>
										</td>
									</tr>
									<tr>
										<th>Tanggal Transaksi</th>
										<td>
											<input type="hidden" name="1LSPD" value="43">
											<script>inputDate('tglAwal', '<fmt:formatDate value="${cmd.now}" pattern="dd/MM/yyyy"/>', false);</script>
											s/d
											<script>inputDate('tglAkhir', '<fmt:formatDate value="${cmd.now}" pattern="dd/MM/yyyy"/>', false);</script>
										</td>
									</tr>
									<tr>
										<th></th>
										<td>
											<a href="#" onclick="return tampilkan2('formSummaryKustodianNewBusiness', 'laporan_kustodian', 'html');"
												onmouseover="return overlib('VIEW', AUTOSTATUS, WRAP);" onmouseout="nd();">
												<img style="border:none;" alt="HTML" src="${path}/include/image/page_html.gif"/></a>

											<a href="#" onclick="return tampilkan2('formSummaryKustodianNewBusiness', 'laporan_kustodian', 'pdf');"
												onmouseover="return overlib('PRINT', AUTOSTATUS, WRAP);" onmouseout="nd();">
												<img style="border:none;" alt="PDF" src="${path}/include/image/pdf.gif"/></a>
										</td>
									</tr>
								</TABLE>
							</form>
						</fieldset>

						<!-- ====== -->
						<fieldset id="ExcellinkKaryawan" style="display:none;">
							<form name="formpost" method="post" action="kustodian.htm" target="_blank" id="formExcellinkKaryawan">
								<TABLE class="entry2">
									<tr>
										<th style="width: 150px; ">Periode</th>
										<td>
											<SELECT name="tgl_mm">
												<option value="01">Januari</option>
												<option value="02">Februari</option>
												<option value="03">Maret</option>
												<option value="04">April</option>
												<option value="05">Mei</option>
												<option value="06">Juni</option>
												<option value="07">Juli</option>
												<option value="08">Agustus</option>
												<option value="09">September</option>
												<option value="10">Oktober</option>
												<option value="11">November</option>
												<option value="12">Desember</option>
											</SELECT>
											<input type="text" name="tgl_yyyy" value="<fmt:formatDate value="${cmd.now}" pattern="yyyy"/>" size="5">
										</td>
									</tr>
									<tr>
										<th></th>
										<td>
											<select name="reportPath">
												<c:forEach var="ex" items="${cmd.daftarExcellinkKaryawan}">
													<option value="${ex.value}">${ex.key}</option>
												</c:forEach>
											</select>
										</td>
									</tr>
									<tr>
										<th></th>
										<td>
											<a href="#" onclick="return tampilkan2('formExcellinkKaryawan', 'excellink_karyawan', 'html');"
												onmouseover="return overlib('VIEW', AUTOSTATUS, WRAP);" onmouseout="nd();">
												<img style="border:none;" alt="HTML" src="${path}/include/image/page_html.gif"/></a>

											<a href="#" onclick="return tampilkan2('formExcellinkKaryawan', 'excellink_karyawan', 'pdf');"
												onmouseover="return overlib('PRINT', AUTOSTATUS, WRAP);" onmouseout="nd();">
												<img style="border:none;" alt="PDF" src="${path}/include/image/pdf.gif"/></a>
										</td>
									</tr>
								</TABLE>
							</form>
						</fieldset>
						
						<!-- ====== -->
						<fieldset id="SuratPermintaanAlokasiInvestasi" style="display:none;">
							<form name="formpost" method="post" action="kustodian.htm" target="_blank" id="formSuratPermintaanAlokasiInvestasi">
								<TABLE class="entry2">
									<tr>
										<th style="width: 150px; ">Jenis Transaksi</th>
										<td>
											<select name="dk">
												<option value="D">Pemasukkan</option>
												<option value="K">Pengeluaran</option>
											</select>
										</td>
									</tr>
									<tr>
										<th>Tanggal Transaksi</th>
										<td>
											<script>inputDate('tanggalAwal', '<fmt:formatDate value="${cmd.now}" pattern="dd/MM/yyyy"/>', false);</script>
										</td>
									</tr>
									<tr>
										<th>Waktu Transaksi</th>
										<td>
											<label for="sebelum"><input class="noBorder" type="radio" id="sebelum" onclick="document.getElementById('jamduanya').value='before';" name="jamdua" value="0" checked="checked">Sebelum ${cmd.cutoff}</label>
											<label for="sesudah"><input class="noBorder" type="radio" id="sesudah" onclick="document.getElementById('jamduanya').value='after';" name="jamdua" value="1">Sesudah ${cmd.cutoff}</label>
											<input type="hidden" value="before" id="jamduanya" name="jamduanya">
										</td>
									</tr>
									<tr>
										<th>Jenis Report</th>
										<td>
											<label for="biasa"><input class="noBorder" type="radio" id="biasa" onclick="document.getElementById('repotnya').value='biasa';" name="repot" value="0" checked="checked">Biasa</label>
											<label for="ekalink"><input class="noBorder" type="radio" id="ekalink" onclick="document.getElementById('repotnya').value='ekalink';" name="repot" value="1">Ekalink 88</label>
											<input type="hidden" value="biasa" id="repotnya" name="repotnya">
										</td>
									</tr>
									<tr>
										<th>Distribusi</th>
										<td>
											<label for="other"><input class="noBorder" type="radio" id="other" onclick="document.getElementById('distribusi').value='other';" name="dist" value="0">Others</label>
											<label for="sms"><input class="noBorder" type="radio" id="sms" onclick="document.getElementById('distribusi').value='sms';" name="dist" value="1" checked="checked">Sekuritas</label>
											<input type="hidden" value="sms" id="distribusi" name="distribusi">
										</td>
									</tr>
									<tr>
										<th>Mengetahui</th>
										<td>
											<input type="text" name="mengetahui" value="${cmd.mengetahui}" size="50" style="text-transform: none;">
										</td>
									</tr>
									<tr>
										<th></th> 
										<td>
											<a href="#" onclick="return tampilkan2('formSuratPermintaanAlokasiInvestasi', 'alokasi_investasi', 'html');"
												onmouseover="return overlib('VIEW', AUTOSTATUS, WRAP);" onmouseout="nd();">
												<img style="border:none;" alt="HTML" src="${path}/include/image/page_html.gif"/></a>

											<a href="#" onclick="return tampilkan2('formSuratPermintaanAlokasiInvestasi', 'alokasi_investasi', 'pdf');"
												onmouseover="return overlib('PRINT', AUTOSTATUS, WRAP);" onmouseout="nd();">
												<img style="border:none;" alt="PDF" src="${path}/include/image/pdf.gif"/></a>
										</td>
									</tr>
									<tr>
										<th>Search File</th>
										<td>
<!--										<script>inputDate('tanggalFile', '<fmt:formatDate value="${cmd.now}" pattern="ddMMyyyy"/>', false);</script>-->
											
											<span>Pilih file yang dicari berdasarkan kategori di atas.  Kemudian tekan tombol cari file untuk mulai mencari.</span>
											<br/>
											<input type="button" name="srcLap" value="cari file" onclick="searchLap('2');">
											
										</td>
									</tr>
									
								</TABLE>
							</form>
						</fieldset>
						
					</td>
				</tr>
			</table>
		</fieldset>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>