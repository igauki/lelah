<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">

$().ready(function() {
		
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("#tabs").tabs();
		$("input[title], select[title], textarea[title], button[title], a[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		
		$(".checkall").click(function () {
			tot = $(this).parent().find(":hidden");
			if(tot.val() == "true") tot.val(""); else tot.val("true");
			$(this).parent().parent().find(":checkbox").attr("checked", tot.val());
			return false;
		});	
		
		$("#selectjenis").change(function() {
			$("#selectNamaBank").empty();
			var url = "${path}/uw/uw.htm?window=picrekeningbsm&json=1&jenis=" +$("#selectjenis").val(); ///uw/ubah_reas.htm?
			$.getJSON(url, function(result) {
				$("<option/>").val(" ").html("").appendTo("#selectNamaBank");
				$.each(result, function() {
					$("<option/>").val(this.LCB_NO).html(this.NAMA_CABANG).appendTo("#selectNamaBank");
				});
				$("#select_kriteria option:first").attr('selected','selected');
			});
	
		});
		
	$("#selectNamaBank").change(function() {
			var url = "${path}/uw/uw.htm?window=picrekeningbsm&json=2&jenis=" +$("#selectjenis").val()+"&lcb_no=" +$("#selectNamaBank").val(); ///uw/ubah_reas.htm?
			  $.getJSON(url, function(result) {
				  $.each(result, function() {
				 		 $("#email").val(this.EMAIL_CAB);
				 		 $("#cabang").val(this.NAMA_CABANG);
				 		 $("#lcb_no").val(this.LCB_NO);
				 		 $("#wil").val(this.WIL_AJS);
				 		 $("#alamat").val(this.ALAMAT);
				 		 $("#rekening").val(this.NO_REK_PIC);
				 		 $("#pic").val(this.NAMA_REK_PIC);
				 		 $("#e_pic").val(this.EMAIL_PIC);
				  });	
				});	
	
		});
			
	});

</script>
<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.6em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: left; width: 7em; }

	/* lebar untuk form elements */
	.lebar { width: 44em; }
	
	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	table tr td label { display: inline-block; width: 60em;}
	
	/* styling untuk table th */
	table tr th { padding: 1em;}
	
</style>

<BODY style="height: 100%;">
	<c:choose>
		<c:when test="${flag eq \"1\" }">
			<div id="tabs">
				<ul>
					<li><a href="#tab-1">Data Bank Bank Sinarmas / Sekuritas /
							Bank Sinarmas Syariah</a>
					</li>
				</ul>
				<div id="tab-1">
					<form method="post" name="formpost" style="text-align: left;"
						enctype="multipart/form-data">
						<table class="entry2" style="width: 100%" align="left">
							<tr>
							<tr style="background-color: #F5F5F5;" align="left">
								<th>Jenis Bank</th>
								<th class="left" style="white-space: nowrap;"><select
									name="selectjenis" id="selectjenis">
										<option value="">Pilih Jenis</option>
										<option value=2>Bank Sinarmas</option>
										<option value="3">Sinarmas Sekuritas</option>
										<option value="16">Bank Sinarmas Syariah</option>
								</select></th>
							</tr>
							<tr style="background-color: #F5F5F5;" align="left">
								<th>Nama Bank</th>
								<th class="left" style="white-space: nowrap;"><select
									name="selectNamaBank" id="selectNamaBank">
										<option value="">Pilih Cabang Bank</option>
										<c:forEach var="x" items="${selectNamaBank}">
											<option value="${x.LCB_NO}">${x.NAMA_CABANG}</option>
										</c:forEach>
								</select></th>
							</tr>
							<tr style="background-color: #F5F5F5;" align="left">
								<th>Kode Cabang</th>
								<th class="left" style="white-space: nowrap;"><input
									type="text" name="lcb_no" id="lcb_no" size="10"
									value="${lcb_no}" readonly="readonly"></th>
							</tr>
							<tr style="background-color: #F5F5F5;" align="left">
								<th>Nama Cabang Bank</th>
								<th class="left" style="white-space: nowrap;"><input
									type="text" name="cabang" id="cabang" size="100"
									value="${cabang}" readonly="readonly"></th>
							</tr>
							<tr style="background-color: #F5F5F5;" align="left">
								<th>Email Cabang Bank</th>
								<th class="left" style="white-space: nowrap;"><input
									type="text" name="email" id="email" size="50" value="${email}"
									readonly="readonly"></th>
							</tr>
							<tr style="background-color: #F5F5F5;" align="left">
								<th>Alamat Cabang Bank</th>
								<th class="left" style="white-space: nowrap;"><textarea
										rows="3" cols="80" name="alamat" id="alamat" readonly>${alamat}</textarea>
								</th>
							</tr>
							<tr style="background-color: #F5F5F5;" align="left">
								<th>No Rekening PIC</th>
								<th class="left" style="white-space: nowrap;"><input
									type="text" name="rekening" id="rekening" value="${rekening}"
									size="50"></th>
							</tr>
							<tr style="background-color: #F5F5F5;" align="left">
								<th>Rekening PIC A/n</th>
								<th class="left" style="white-space: nowrap;"><input
									type="text" name="pic" id="pic" value="${pic}" size="50">
								</th>
							</tr>
							<tr style="background-color: #F5F5F5;" align="left">
								<th>Alamat Email PIC</th>
								<th class="left" style="white-space: nowrap;"><input
									type="text" name="e_pic" id="e_pic" value="${e_pic}"
									size="50"></th>
							</tr>
							<tr style="background-color: #F5F5F5;" align="left">
								<th class="left" style="white-space: nowrap;"><input
									type="submit" value="Update" name="update" id="update">
								</th>
							</tr>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</c:when>
		<c:otherwise>
			<div id="tabs">
				<ul>
					<li><a href="#tab-1">Summary Per Cabang</a>
					</li>
				</ul>
				<div id="tab-1">
					<form method="post" name="formpost" style="text-align: left;"
						enctype="multipart/form-data">
						<table class="entry" style="width: 100%" align="left">
							<tr style="background-color: #F5F5F5;" align="left">
								<th>Jenis Bank</th>
								<th class="left" style="white-space: nowrap;"><select
									name="selectjenis" id="selectjenis">
										<option value="">Pilih Jenis</option>
										<option value=2>Bank Sinarmas</option>
										<option value="3">Sinarmas Sekuritas</option>
										<option value="16">Bank Sinarmas Syariah</option>
								</select></th>
							</tr>
							<tr style="background-color: #F5F5F5;" align="left">
								<th>Nama Bank</th>
								<th class="left" style="white-space: nowrap;"><select
									name="selectNamaBank" id="selectNamaBank">
										<option value="">Pilih Cabang Bank</option>
										<c:forEach var="x" items="${selectNamaBank}">
											<option value="${x.LCB_NO}">${x.NAMA_CABANG}</option>
										</c:forEach>
								</select>
								<input type="hidden" name="cabang" id="cabang" size="50" value="${cabang}" readonly="readonly"></th>
								<input type="hidden" name="lcb_no" id="lcb_no" size="50" value="${lcb_no}" readonly="readonly"></th>
							</tr>
							<tr style="background-color: #F5F5F5;" align="left">
								<th style="width: 200px;" align="left">Periode Pengajuan</th>
								<th><input name="bdate" id="bdate" type="text"
									class="datepicker" title="Tanggal Awal" value="${bdate}"> s / d <input name="edate" id=""edate"" type="text"
									class="datepicker" title="Tanggal Akhir" value="${edate}"></th>
							</tr>
							<tr style="background-color: #F5F5F5;" align="left">
								<th class="left" style="white-space: nowrap;"><input type="submit" value="pdf" name="pdf" id="pdf">
									<input type="submit" value="excel" name="excel" id="excel">
								</th>
							</tr>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</c:otherwise>
	</c:choose>
</BODY>
<c:if test="${not empty pesan}">
	<script>alert('${pesan}');</script>
</c:if>

<%@ include file="/include/page/footer.jsp"%>