<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript">
	var sukses = '${sukses}';
	if(sukses==1){
		alert('${successMessage}');
		parent.location.href='${path}/uw/uw.htm?window=main&posisi_uw=27';
	}
	
	function toSubmit(){
      	var result = confirm("Yakin akan di transfer?", "SPEEDY");
		if(result==true){
			return true;
		}else{
			return false; 
		}
		
		return false;
   }
</script>
<body style="height: 100%;">	
	
	<c:choose>
		<c:when test="${spajrecur  eq 1 }" >
			<div id="error" style="text-align:center;">${block_r }</div>
		</c:when>
		<c:otherwise>
	
		<c:choose>
			<c:when test="${speedy  eq 1 }" >
				<div id="success">${successMessage }</div>
			</c:when>
			<c:otherwise>
				<form method="post" name="formpost" action="" onsubmit="return toSubmit();">
					<fieldset>
						<legend>Back To BAS</legend>
						<table class="entry2">
							<tr>
								<th>To:</th>
								<td><input type="text" value="${to }" name="to" id="to" style="width: 400px;"></td>
							</tr>
							<tr>
								<th>Cc:</th>
								<td><input type="text" value="${cc }" value="" name="cc" id="cc" style="width: 400px;"></td>
							</tr>
							<tr>
								<th>Subject:</th>
								<td><input type="text" value="${subject }" name="subject" id="subject" style="width: 400px;"></td>
							</tr>
							<tr>
								<th>Isi</th>
								<td><textarea rows="4" cols="80" name="isi" id="isi">${message}</textarea></td>
							</tr>
							<tr>
								<th></th>
								<td>
									<input type="submit" name="btnKirim" id="btnKirim" value="Kirim & Transfer">
								</td>	
							</tr>
						</table>
					</fieldset>
				</form>
			</c:otherwise>
		</c:choose>
		
		</c:otherwise>
		</c:choose>
</body>
<%@ include file="/include/page/footer.jsp"%>