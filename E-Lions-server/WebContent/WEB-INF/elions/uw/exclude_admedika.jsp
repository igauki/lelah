<%@ include file="/include/page/header.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>
	<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
	<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
	<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
	<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

	<script>
		function tambah(){
			formpost.flagAdd.value="1";
			formpost.submit();
		}
	
		function simpan(){
			formpost.flagAdd.value="2";
			formpost.submit();
		}
	
		function hapus(){
			formpost.flagAdd.value="3";
			formpost.submit();		
		}
		
		function keterangan(e,value) {
			var unicode=e.charCode? e.charCode : e.keyCode;
			if(unicode == 13) {
				formpost.flagAdd.value="4";
				formpost.index.value = value;
				formpost.submit();
			}	
		}
		
		function cek(value) {
			alert(value);
		}
	</script>	
	<body onresize="setFrameSize('infoFrame', 45);" style="height: 100%;">
		<form:form id="formpost" name="formpost" commandName="cmd">
			<div class="tabcontent">
				<fieldset>
					<legend>ICD Code [${cmd.spaj}]</legend>					
					
					<table class="entry2">
					<tr>
							<td colspan="5" style="text-align: center;">
								<form:hidden path="flagAdd" />								
								<input type="button" name="btn_add" value="Add" onclick="tambah();" <c:if test="${cmd.mode eq \'viewer\'}">disabled="disabled"</c:if>>
								<input type="button" name="btn_delete" value="Delete" onclick="hapus();" <c:if test="${cmd.mode eq \'viewer\'}">disabled="disabled"</c:if>>								
							</td>
						</tr>
						<tr>
							<th width="8%">Hapus</th>
							<th width="">ICD Code</th>
							
							<!--<th width="22%">Tanggal</th>-->
							<!--<th width="20%">User Input</th>-->													
						</tr>
						<c:forEach var="s" items="${cmd.lsIcd}" varStatus="xt">
							<tr>
							
							
								<td style="text-align: center;vertical-align: bottom;" >
									<c:if test="${sessionScope.currentUser.lus_id eq s.msdi_lus_id}">
										<form:checkbox cssClass="noBorder" path="lsIcd[${xt.index}].cek" id="lsIcd[${xt.index}].cek" value="1"/>
									</c:if>	
								</td>
								<td <c:if test="${!empty s.lic_id}"> style="text-align: center;"</c:if>>
									<c:choose>
										<c:when test="${empty s.lic_id}">
											<spring:bind path="cmd.lsIcd[${xt.index}].lic_id"><br>
												<input type="text" name="${status.expression}" id="${status.expression}" value="${status.value}" style="width: 50%" onfocus="this.select();" onkeypress="keterangan(event,'${xt.index}')"
												<c:if test="${not empty status.errorMessage}">
													<c:out value="${err}" escapeXml="false" />
												</c:if>>
												<span id="indicator_icd${xt.index}" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
											</spring:bind>
											<ajax:autocomplete
												  source="lsIcd[${xt.index}].lic_id"
												  target="lsIcd[${xt.index}].lic_id"
												  baseUrl="${path}/servlet/autocomplete?s=lsIcd[${xt.index}].lic_id&q=icd"
												  className="autocomplete"
												  indicator="indicator_icd${xt.index}"
												  minimumCharacters="1"
												  parser="new ResponseXmlToHtmlListParser()"/>											
										</c:when>
										<c:otherwise>
												${s.lic_id}
										</c:otherwise>
									</c:choose>								
																		
								</td>
								
								
							</tr>
						</c:forEach>
						
					</table>
					<table class="entry1">
						<tr>
							<td>
							   Daftar Peserta :
							</td>
							<td>
							<select  name="dp" id="dp" title="Silahkan pilih jenis peserta">
																			
								<c:forEach var="c" items="${daftarPeserta}" varStatus="s">
									<option value="${c.key}">${c.value}</option>
					        	</c:forEach> 
				   			</select> 	
							</td>
						</tr>
						
						<tr>
							<td>
							   Keputusan Pengecualian :
							</td>
							<td>
							
							<textarea rows="3" cols="50" id="description" name="description"></textarea>	
				   				
							</td>
						</tr>
						
						<tr>
							<td>
							   Payor ID Number :
							</td>
							<td>
							
							<input type="text" name="payorID" id="payorID"  style="width: 50%">
				   				
							</td>
						</tr>
						
						<tr>
							<td>
							   Card Member :
							</td>
							<td>
							
							<input type="text" name="card" id="card"  style="width: 50%">
				   				
							</td>
						</tr>
						
					
					</table>
					<input type="button" name="btn_save" value="Send/Save" onclick="simpan();" <c:if test="${cmd.mode eq \'viewer\'}">disabled="disabled"</c:if>>
					<input type="hidden" name="spaj" value="${cmd.spaj}">
					<input type="hidden" name="index" id="index">
				</fieldset>
				<table width="50%">
					<tr>
						<td colspan="2">
						<spring:hasBindErrors name="cmd">
		    				<tr>
		    					<td colspan="10">
									<div id="error" style="margin: 5px 5px 5px 5px;">
										- <form:errors path="*" delimiter="<br>- "/>
									</div>
								</td>
		    				</tr>
	    				</spring:hasBindErrors>		
							<c:if test="${not empty submitSuccess}">
					        	<div id="success">
						        	Berhasil
					        	</div>
					        </c:if>	
						</td>
					</tr>
				</table>				
			</div>
			<div>
				<fieldset>
					<legend>Data Exclude [${cmd.spaj}]</legend>	
					 <table class="displaytag" cellpadding='0' cellspacing='0' border="1" align="center" style="font-size: 11px">
														<tr>														
															<th nowrap align="justify" width="20" bgcolor="#b7b7b7">No.SPAJ</th>																													
															<th nowrap align="justify" width="20" bgcolor="#b7b7b7">KODE ICD</th>
															<th nowrap align="justify" width="300" bgcolor="#b7b7b7">NAMA ICD</th>
															<th nowrap align="justify" width="200" bgcolor="#b7b7b7">KATEGORI</th>
															<th nowrap align="justify" width="200" bgcolor="#b7b7b7">JENIS TERTANGGUNG</th>														
																							
														</tr>
														<pg:paging url="${path}/uw/exclude_admedika.htm?spaj=${cmd.spaj}" pageSize="15">
														<c:forEach items="${dataICD}" var="d">	
														<pg:item>												
															<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';return true;" onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;">															
																
																<td align="justify" width="20" style="font-size: 10px;">${d.SPAJ}</td>
																<td align="justify" width="20" style="font-size: 10px;">${d.KODE}</td>
																<td align="justify" width="300" style="font-size: 10px;">${d.NAMA_ICD}</td>
																<td align="justify" width="200" style="font-size: 10px;">${d.KATEGORI}</td>
																<td align="justify" width="200" style="font-size: 10px;">${d.JENIS}</td>															
																											
																																
															</tr>
														</pg:item>
													</c:forEach>
													<pg:index>
												  		<pg:page><%=thisPage%></pg:page>
													</pg:index>
													</pg:paging>		
											</table>
				</fieldset>	
			
			</div>
		</form:form>
	</body>
<%@ include file="/include/page/footer.jsp"%>
