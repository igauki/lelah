<%@ include file="/include/page/header_mall.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<%User currentUser = (User) session.getAttribute("currentUser");%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>

 
<script type="text/javascript">
	hideLoadingMessage();
	
	function agen_cek()
	{
		if (document.getElementById('msag_id').value != "")
		{
			ajaxManager.listagenpas(document.getElementById('msag_id').value,
			{callback:function(map) {
		
				DWRUtil.useLoadingMessage();
				document.getElementById('nama_agen').value = map.nama_penutup;
				document.getElementById('cabang_agen').value = map.nama_regional;
				
			   },
			  timeout:180000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
			});
		}
	}
	
	function premi_cek()
	{
		if (document.getElementById('produk').value != "" && document.getElementById('msp_date_of_birth').value != "")
		{
			ajaxManager.cekPremiHcp(document.getElementById('produk').value, document.getElementById('msp_date_of_birth').value,
		{callback:function(map) {
		
			DWRUtil.useLoadingMessage();
			if(map!=null){
				document.getElementById('msp_premi').value = map.premi;
				doPremi(map.premi);
				doUp();
			}
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
		}else{
			document.getElementById('msp_premi').value = "0";
			document.getElementById('premi').value = "0";
			document.getElementById('msp_up').value = "0";
		}
	}
	
	function kartu_cek_key()
	{
		if(document.getElementById('no_kartu').value.length == 16)
		{
			ajaxManager.cekKartuPas(document.getElementById('no_kartu').value,
		{callback:function(map) {
			DWRUtil.useLoadingMessage();
			if(map!=null){
				jenis_paket(map.produk);
			}
		   },
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
		}
	}
	
	function jenis_paket(premi_tahunan){
		var paket = 'Paket : HCP';
		var plan = 'Mohon Isi Data HCP';
		alert( paket + '\n' + plan);
	}
	
	function insert(){
		document.getElementById('kata').value = 'insert';
		if(trim(document.getElementById('kata').value)=='') return false;
		else createLoadingMessage();	
	}
	
	function dofo(index, flag) {
		if(flag==1){
			var a="rekening["+(index+1)+"]";
			var b="rekening["+(index)+"]";		
			document.getElementById(a).focus();
		}else{
			var a="rekening["+(index+1)+"]";		
			document.getElementById(a).focus();
		}
	}
	
	function dofo_autodebet(index, flag) {
		if(flag==1){
			var a="rekening_autodebet["+(index+1)+"]";
			var b="rekening_autodebet["+(index)+"]";		
			document.getElementById(a).focus();
		}else{
			var a="rekening_autodebet["+(index+1)+"]";		
			document.getElementById(a).focus();
		}
	}
	
	function doRek() {
		document.getElementById('msp_no_rekening').value = '';
		for(i = 0 ; i < 21 ; i++){
			document.getElementById('msp_no_rekening').value = document.getElementById('msp_no_rekening').value + document.getElementById("rekening["+i+"]").value;
		}
	}
	
	function doRek_autodebet() {
		document.getElementById('msp_no_rekening_autodebet').value = '';
		for(i = 0 ; i < 21 ; i++){
			document.getElementById('msp_no_rekening_autodebet').value = document.getElementById('msp_no_rekening_autodebet').value + document.getElementById("rekening_autodebet["+i+"]").value;
		}
	}
	
	function doPp() {
		if(document.getElementById('msp_rek_nama').value == ''){
			document.getElementById('msp_rek_nama').value = document.getElementById('nama_pp').value;
		}
		document.getElementById('msp_pas_nama_pp').value = document.getElementById('nama_pp').value;
	}
	
	function inPp() {
		document.getElementById('nama_pp').value = document.getElementById('msp_pas_nama_pp').value;
	}
	
	function inRek() {
		var rek = document.getElementById('msp_no_rekening').value
		var size = rek.length;
		for(i = 0 ; i < size ; i++){
			document.getElementById('rekening['+i+']').value = rek.charAt(i);
		}
	}
	
	function inRek_autodebet() {
		var rek = document.getElementById('msp_no_rekening_autodebet').value
		var size = rek.length;
		for(i = 0 ; i < size ; i++){
			document.getElementById('rekening_autodebet['+i+']').value = rek.charAt(i);
		}
	}
	
	function doPremi(){
		var lscb_id = document.getElementById('lscb_id').value;
		var premi_tahunan = document.getElementById('msp_premi').value;
		var prm = 0;
		if(lscb_id == 3){
				document.getElementById('premi').value = premi_tahunan;
			}else if(lscb_id == 6){
				prm = premi_tahunan * 0.10;
				document.getElementById('premi').value = Math.round(prm);
			}else if(lscb_id == 1){
				prm = premi_tahunan * 0.27;
				document.getElementById('premi').value = Math.round(prm);
			}else if(lscb_id == 2){
				prm = premi_tahunan * 0.525;
				document.getElementById('premi').value = Math.round(prm);
			}else{
				document.getElementById('premi').value = '0';
			}
	} 
	
	function doUp(){
		if(document.getElementById('produk').value != ""){
			document.getElementById('msp_up').value = document.getElementById('produk').value * 100000;
		}else{
			document.getElementById('msp_up').value = "0";
		}
	} 
	
	function doEnableDisableRek() {
		if(document.getElementById('msp_flag_cc').value == '1'){// kartu kredit
			//rekening
			document.getElementById('caribank1').value = '';document.getElementById('caribank1').disabled = 'disabled';
			document.getElementById('btncari1').disabled = 'disabled';
			document.getElementById('lsbp_id').value = '';document.getElementById('lsbp_id').disabled = 'disabled';
			document.getElementById('msp_rek_cabang').value = '';document.getElementById('msp_rek_cabang').disabled = 'disabled';
			document.getElementById('msp_rek_kota').value = '';document.getElementById('msp_rek_kota').disabled = 'disabled';
			document.getElementById('msp_rek_nama').value = '';document.getElementById('msp_rek_nama').disabled = 'disabled';
			for(i = 0 ; i < 21 ; i++){
				document.getElementById('rekening['+i+']').value = '';
				document.getElementById('rekening['+i+']').disabled = 'disabled';
			}
			document.getElementById('msp_no_rekening').value = '';
			//autodebet
			document.getElementById('caribank2').value = '';document.getElementById('caribank2').disabled = '';
			document.getElementById('btncari2').disabled = '';
			document.getElementById('lsbp_id_autodebet').value = '';document.getElementById('lsbp_id_autodebet').disabled = '';
			document.getElementById('msp_rek_nama_autodebet').value = '';document.getElementById('msp_rek_nama_autodebet').disabled = '';
			document.getElementById('msp_tgl_debet_div').disabled = '';
			document.getElementById('msp_tgl_debet_div').style.visibility = 'visible';
			document.getElementById('msp_tgl_valid_div').disabled = '';
			document.getElementById('msp_tgl_valid_div').style.visibility = 'visible';
			for(i = 0 ; i < 21 ; i++){
				document.getElementById('rekening_autodebet['+i+']').value = '';
				document.getElementById('rekening_autodebet['+i+']').disabled = '';
			}
			document.getElementById('msp_no_rekening_autodebet').value = '';
		}else if(document.getElementById('msp_flag_cc').value == '2'){//tabungan
			//rekening
			document.getElementById('caribank1').value = '';document.getElementById('caribank1').disabled = '';
			document.getElementById('btncari1').disabled = '';
			document.getElementById('lsbp_id').value = '';document.getElementById('lsbp_id').disabled = '';
			document.getElementById('msp_rek_cabang').value = '';document.getElementById('msp_rek_cabang').disabled = '';
			document.getElementById('msp_rek_kota').value = '';document.getElementById('msp_rek_kota').disabled = '';
			document.getElementById('msp_rek_nama').value = '';document.getElementById('msp_rek_nama').disabled = '';
			for(i = 0 ; i < 21 ; i++){
				document.getElementById('rekening['+i+']').value = '';
				document.getElementById('rekening['+i+']').disabled = '';
			}
			document.getElementById('msp_no_rekening').value = '';
			//autodebet
			document.getElementById('caribank2').value = '';document.getElementById('caribank2').disabled = '';
			document.getElementById('btncari2').disabled = '';
			document.getElementById('lsbp_id_autodebet').value = '';document.getElementById('lsbp_id_autodebet').disabled = '';
			document.getElementById('msp_rek_nama_autodebet').value = '';document.getElementById('msp_rek_nama_autodebet').disabled = '';
			document.getElementById('msp_tgl_debet_div').disabled = '';
			document.getElementById('msp_tgl_debet_div').style.visibility = 'visible';
			document.getElementById('msp_tgl_valid_div').disabled = 'disabled';
			document.getElementById('msp_tgl_valid_div').style.visibility = 'hidden';
			for(i = 0 ; i < 21 ; i++){
				document.getElementById('rekening_autodebet['+i+']').value = '';
				document.getElementById('rekening_autodebet['+i+']').disabled = '';
			}
			document.getElementById('msp_no_rekening_autodebet').value = '';
		}else{
			//rekening
			document.getElementById('caribank1').value = '';document.getElementById('caribank1').disabled = 'disabled';
			document.getElementById('btncari1').disabled = 'disabled';
			document.getElementById('lsbp_id').value = '';document.getElementById('lsbp_id').disabled = 'disabled';
			document.getElementById('msp_rek_cabang').value = '';document.getElementById('msp_rek_cabang').disabled = 'disabled';
			document.getElementById('msp_rek_kota').value = '';document.getElementById('msp_rek_kota').disabled = 'disabled';
			document.getElementById('msp_rek_nama').value = '';document.getElementById('msp_rek_nama').disabled = 'disabled';
			for(i = 0 ; i < 21 ; i++){
				document.getElementById('rekening['+i+']').value = '';
				document.getElementById('rekening['+i+']').disabled = 'disabled';
			}
			document.getElementById('msp_no_rekening').value = '';
			//autodebet
			document.getElementById('caribank2').value = '';document.getElementById('caribank2').disabled = 'disabled';
			document.getElementById('btncari2').disabled = 'disabled';
			document.getElementById('lsbp_id_autodebet').value = '';document.getElementById('lsbp_id_autodebet').disabled = 'disabled';
			document.getElementById('msp_rek_nama_autodebet').value = '';document.getElementById('msp_rek_nama_autodebet').disabled = 'disabled';
			document.getElementById('msp_tgl_debet_div').disabled = 'disabled';
			document.getElementById('msp_tgl_debet_div').style.visibility = 'hidden';
			document.getElementById('msp_tgl_valid_div').disabled = 'disabled';
			document.getElementById('msp_tgl_valid_div').style.visibility = 'hidden';
			for(i = 0 ; i < 21 ; i++){
				document.getElementById('rekening_autodebet['+i+']').value = '';
				document.getElementById('rekening_autodebet['+i+']').disabled = 'disabled';
			}
			document.getElementById('msp_no_rekening_autodebet').value = '';
		}
	}
	
	function bodyOnLoad(){
		inRek();
		inRek_autodebet();
		inPp();
		agen_cek();
		doEnableDisableRek();
	}
	
</script>

</head>

<body onload="resizeCenter(750,600);bodyOnLoad();" style="height: 100%;">
<XML ID=xmlData></XML>
<div class="tab-container" id="container1">

		<div class="tab-panes">

			<div id="pane1" class="panes">
<form:form commandName="cmd" name="frmParam" id="frmParam" method="post">
<fieldset>
					   <legend>ACTION</legend>
					   <table class="result_table2">
						<tr>
							<td colspan="2">
								<input type="hidden" name="msp_id" id="msp_id" value="${msp_id}" />
								<input type="button" name="edit" id="edit" value="Edit" style="visibility: hidden;width: 1px" onclick="return update();"/>
								<input type="submit" name="save" id="save" value="Save"  onclick="return insert();"/>
								<input type="button" name="close" id="close" value="Close" onclick="window.close();" />
								<a href="input_fire.htm" id="refreshPage"></a>
							</td>
						</tr>
					</table>
					</fieldset>
						<fieldset>
					  <legend>HCP</legend>
					   <table class="result_table2" >
					   <tr><td colspan="2" style="border: 0px;">
					   <spring:bind path="cmd.*">
			<c:if test="${not empty status.errorMessages}">
				<div id="error">ERROR:<br>
				<c:forEach var="error" items="${status.errorMessages}">
	- <c:out value="${error}" escapeXml="false" />
					<br />
				</c:forEach></div>
			</c:if>
		</spring:bind>	</td></tr>
					   	<tr>
							<td align="left">Tanggal Aktivasi (efektif Polis):</td>
							<td><spring:bind path="cmd.msp_pas_beg_date" >
        		                    <script>
                                        isDisabled = true;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
                                </spring:bind></td>
						</tr>
						<tr>
							<td align="left">No. Kartu:</td>
							<td>
							<spring:bind path="cmd.no_kartu" >
							<input type="text" id="no_kartu" name="${status.expression}" value="${status.value }" onkeyup="kartu_cek_key();" />
							</spring:bind>
							<font class="error">* tanpa titik (.)</font></td>
						</tr>
						<tr>
							<th colspan="2">Data Pemegang Polis</th>
						</tr>
						<tr>
							<td align="left">Nama (sesuai KTP):</td>
							<td><input type="text" id="nama_pp" name="nama_pp" id="nama_pp" onblur="doPp();" /><font class="error">*</font>
							<span style="visibility: hidden"><form:input path="msp_pas_nama_pp" cssErrorClass="inpError"/></span></td>
						</tr>
						<tr>
							<td align="left">Tempat, Tgl. lahir:</td>
							<td><form:input path="msp_pas_tmp_lhr_pp" cssErrorClass="inpError"/>
								<spring:bind path="cmd.msp_pas_dob_pp" >
        		                    <script>
                                        var isDisabled;
                                        var status = '${statusForm}';
                                        if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
                                <font color="#CC3300">*</font>
                                </spring:bind>
							</td>
						</tr>
						<tr>
							<td align="left">Jenis Kelamin:</td>
							<td>
							<spring:bind path="cmd.msp_sex_pp">
								<label for="cowok"> <input type="radio" class=noBorder
									name="${status.expression}" value="1"
									<c:if test="${cmd.msp_sex_pp eq 1 or cmd.msp_sex_pp eq null}"> 
												checked</c:if>
									id="cowok">Pria </label>
								<label for="cewek"> <input type="radio" class=noBorder
									name="${status.expression}" value="0"
									<c:if test="${cmd.msp_sex_pp eq 0}"> 
												checked</c:if>
									id="cewek">Wanita </label>
							</spring:bind>
							<font class="error">*</font>
							</td>
						</tr>
						<tr>
							<td align="left">No Identitas:</td>
							<td><form:input path="msp_identity_no" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">Alamat:</td>
							<td><form:input path="msp_address_1" size="80" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">Kota:</td>
							<td><form:input path="msp_city" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">Kode Pos:</td>
							<td><form:input path="msp_postal_code" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">Telp:</td>
							<td><form:input path="msp_pas_phone_number" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">No. HP:</td>
							<td><form:input path="msp_mobile" cssErrorClass="inpError" /><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">No. HP2:</td>
							<td><form:input path="msp_mobile2" cssErrorClass="inpError" /><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">Alamat Email:</td>
							<td><form:input path="msp_pas_email" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">Hubungan dengan Tertanggung:</td>
							<td>
								<select name="lsre_id" id="lsre_id" >
									<c:forEach var="rp" items="${relasi_pas}"> 
										<option <c:if test="${cmd.lsre_id eq rp.ID}"> SELECTED </c:if> value="${rp.ID}">${rp.RELATION}
										</option>
									</c:forEach> 
								</select>
							<font class="error">*</font></td>
						</tr>
						<tr>
							<th colspan="2">Data Tertanggung</th>
						</tr>
						<tr>
							<td align="left">Nama Tertanggung:</td>
							<td><form:input path="msp_full_name" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">Tempat, Tgl. lahir:</td>
							<td><form:input path="msp_pas_tmp_lhr_tt" cssErrorClass="inpError"/>
								<spring:bind path="cmd.msp_date_of_birth" >
        		                    <script>
                                        var isDisabled;
                                        var status = '${statusForm}';
                                        if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
                                <font color="#CC3300">*</font>
                                </spring:bind>
							</td>
						</tr>
						<tr>
							<td align="left">No Identitas:</td>
							<td><form:input path="msp_identity_no_tt" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th colspan="2">Data HCP</th>
						</tr>
						<tr>
							<td align="left">Paket:</td>
							<td>
								<select name="produk" id="produk" onchange="premi_cek();">
									<c:forEach var="paket_p" items="${produk_hcp}"> 
										<option <c:if test="${cmd.produk eq paket_p.ID}"> SELECTED </c:if> value="${paket_p.ID}">${paket_p.PRODUK}
										</option>
									</c:forEach> 
								</select>
							</td>
						</tr>
						<tr>
							<td align="left">premi tahunan:</td>
							<td>
								<spring:bind path="cmd.msp_premi" >
								<input type="text" name="${status.expression}" value="${status.value }" id="msp_premi" readonly="readonly"/>
								</spring:bind>
							</td>
						</tr>
						<tr>
							<td align="left">jumlah premi:</td>
							<td>
								<input type="text" name="premi" id="premi" disabled="disabled"/>
							</td>
						</tr>
						<tr>
							<td align="left">UP:</td>
							<td>
								<spring:bind path="cmd.msp_up" >
								<input type="text" name="${status.expression}" value="${status.value }" id="msp_up" readonly="readonly"/>
								</spring:bind>
							</td>
						</tr>
						<tr>
							<td align="left">cara pembayaran:</td>
							<td>
								<select name="msp_flag_cc" id="msp_flag_cc" onchange="doEnableDisableRek();">
									<c:forEach var="ap" items="${autodebet_pas}"> 
										<option <c:if test="${cmd.msp_flag_cc eq ap.ID}"> SELECTED </c:if> value="${ap.ID}">${ap.AUTODEBET}
										</option>
									</c:forEach> 
								</select>
							<font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">bentuk pembayaran:</td>
							<td>
								<select name="lscb_id" id="lscb_id" onchange="doPremi();">
									<c:forEach var="cp" items="${carabayar_pas}"> 
										<option <c:if test="${cmd.lscb_id eq cp.ID}"> SELECTED </c:if> value="${cp.ID}">${cp.PAYMODE}
										</option>
									</c:forEach> 
								</select>
							<font class="error">*</font></td>
						</tr>
						<tr>
							<th colspan="2">Data Agen</th>
						</tr>
						<tr>
							<td align="left">kode agen:</td>
							<td>
								<spring:bind path="cmd.msag_id" >
									<input type="text" id="msag_id" name="${status.expression}" value="${status.value }" size="8" onblur="agen_cek();"/>
								</spring:bind>
								<input type="text" name="nama_agen" id="nama_agen" size="50" disabled="disabled"/>
							<font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">kode_ao:</td>
							<td>
								<form:input path="kode_ao" cssErrorClass="inpError"/>
							</td>
						</tr>
						<tr>
							<td align="left">pribadi:</td>
							<td>
								<form:checkbox id="pribadi" cssClass="noBorder" path="pribadi" value="1"/>
							</td>
						</tr>
							<td align="left">cabang:</td>
							<td>
								<input type="text" name="cabang_agen" id="cabang_agen" size="30" disabled="disabled"/>
							</td>
						</tr>
						<tr>
							<th colspan="2">Data Rekening Pemegang Polis</th>
						</tr>
						<tr>
							<td align="left">cari bank:</td>
							<td><input type="text" name="caribank1" id="caribank1" onkeypress="if(event.keyCode==13){ document.frmParam.btncari1.click(); return false;}"> 
	              				<input type="button" name="btncari1" id="btncari1" value="Cari" onclick="ajaxSelectWithParam1a(document.getElementById('caribank1').value,'select_bank1','bank1','lsbp_id','', 'BANK_ID', 'BANK_NAMA', '','Silahkan pilih BANK','3');"></td>
						</tr>
						<tr>
							<td align="left">bank:</td>
							<td><div id="bank1"> 
								<select name="lsbp_id" id="lsbp_id" >
				                  <option value="${cmd.lsbp_id}">${cmd.lsbp_nama}</option>
				                </select>
				                </div>
							</td>
						</tr>
						<tr>
							<td align="left">no rekening:</td>
							<td>
							<input type="text" name="rekening[0]" id="rekening[0]" size="1"    maxlength="1" onkeyup="dofo(0, 1);doRek();" />
							<input type="text" name="rekening[1]" id="rekening[1]" size="1"    maxlength="1" onkeyup="dofo(1, 1);doRek();" />
							<input type="text" name="rekening[2]" id="rekening[2]" size="1"    maxlength="1" onkeyup="dofo(2, 1);doRek();" />
							<input type="text" name="rekening[3]" id="rekening[3]" size="1"    maxlength="1" onkeyup="dofo(3, 1);doRek();" />
							<input type="text" name="rekening[4]" id="rekening[4]" size="1"    maxlength="1" onkeyup="dofo(4, 1);doRek();" />
							<input type="text" name="rekening[5]" id="rekening[5]" size="1"    maxlength="1" onkeyup="dofo(5, 1);doRek();" />
							<input type="text" name="rekening[6]" id="rekening[6]" size="1"    maxlength="1" onkeyup="dofo(6, 1);doRek();" />
							<input type="text" name="rekening[7]" id="rekening[7]" size="1"    maxlength="1" onkeyup="dofo(7, 1);doRek();" />
							<input type="text" name="rekening[8]" id="rekening[8]" size="1"    maxlength="1" onkeyup="dofo(8, 1);doRek();" />
							<input type="text" name="rekening[9]" id="rekening[9]" size="1"    maxlength="1" onkeyup="dofo(9, 1);doRek();" />
							<input type="text" name="rekening[10]" id="rekening[10]" size="1"    maxlength="1" onkeyup="dofo(10, 1);doRek();" />
							<input type="text" name="rekening[11]" id="rekening[11]" size="1"    maxlength="1" onkeyup="dofo(11, 1);doRek();" />
							<input type="text" name="rekening[12]" id="rekening[12]" size="1"    maxlength="1" onkeyup="dofo(12, 1);doRek();" />
							<input type="text" name="rekening[13]" id="rekening[13]" size="1"    maxlength="1" onkeyup="dofo(13, 1);doRek();" />
							<input type="text" name="rekening[14]" id="rekening[14]" size="1"    maxlength="1" onkeyup="dofo(14, 1);doRek();" />
							<input type="text" name="rekening[15]" id="rekening[15]" size="1"    maxlength="1" onkeyup="dofo(15, 1);doRek();" />
							<input type="text" name="rekening[16]" id="rekening[16]" size="1"    maxlength="1" onkeyup="dofo(16, 1);doRek();" />
							<input type="text" name="rekening[17]" id="rekening[17]" size="1"    maxlength="1" onkeyup="dofo(17, 1);doRek();" />
							<input type="text" name="rekening[18]" id="rekening[18]" size="1"    maxlength="1" onkeyup="dofo(18, 1);doRek();" />
							<input type="text" name="rekening[19]" id="rekening[19]" size="1"    maxlength="1" onkeyup="dofo(19, 1);doRek();" />
							<input type="text" name="rekening[20]" id="rekening[20]" size="1"    maxlength="1" onkeyup="dofo(20, 1);doRek();" /><font class="error">*</font>
							<span style="visibility: hidden;"><form:input path="msp_no_rekening" id="msp_no_rekening" cssErrorClass="inpError" size="1"/></span></td>
						</tr>
						<tr>
							<td align="left">cabang:</td>
							<td><form:input path="msp_rek_cabang" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">kota:</td>
							<td><form:input path="msp_rek_kota" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">atas nama:</td>
							<td><form:input path="msp_rek_nama" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th colspan="2">Data Rekening Pemegang Polis (AUTODEBET)</th>
						</tr>
						<tr>
							<td align="left">cari bank:</td>
							<td><input type="text" name="caribank2" id="caribank2" onkeypress="if(event.keyCode==13){ document.frmParam.btncari2.click(); return false;}"> 
	              				<input type="button" name="btncari2" id="btncari2" value="Cari" onclick="ajaxSelectWithParam1a(document.getElementById('caribank2').value,'select_bank2','bank2','lsbp_id_autodebet','', 'BANK_ID', 'BANK_NAMA', '','Silahkan pilih BANK','3');"></td>
						</tr>
						<tr>
							<td align="left">bank:</td>
							<td><div id="bank2"> 
								<select name="lsbp_id_autodebet" id="lsbp_id_autodebet">
				                  <option value="${cmd.lsbp_id_autodebet}">${cmd.lsbp_nama_autodebet}</option>
				                </select>
				                </div>
							</td>
						</tr>
						<tr>
							<td align="left">no rekening:</td>
							<td>
							<input type="text" name="rekening_autodebet[0]" id="rekening_autodebet[0]" size="1"    maxlength="1" onkeyup="dofo_autodebet(0, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[1]" id="rekening_autodebet[1]" size="1"    maxlength="1" onkeyup="dofo_autodebet(1, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[2]" id="rekening_autodebet[2]" size="1"    maxlength="1" onkeyup="dofo_autodebet(2, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[3]" id="rekening_autodebet[3]" size="1"    maxlength="1" onkeyup="dofo_autodebet(3, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[4]" id="rekening_autodebet[4]" size="1"    maxlength="1" onkeyup="dofo_autodebet(4, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[5]" id="rekening_autodebet[5]" size="1"    maxlength="1" onkeyup="dofo_autodebet(5, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[6]" id="rekening_autodebet[6]" size="1"    maxlength="1" onkeyup="dofo_autodebet(6, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[7]" id="rekening_autodebet[7]" size="1"    maxlength="1" onkeyup="dofo_autodebet(7, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[8]" id="rekening_autodebet[8]" size="1"    maxlength="1" onkeyup="dofo_autodebet(8, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[9]" id="rekening_autodebet[9]" size="1"    maxlength="1" onkeyup="dofo_autodebet(9, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[10]" id="rekening_autodebet[10]" size="1"    maxlength="1" onkeyup="dofo_autodebet(10, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[11]" id="rekening_autodebet[11]" size="1"    maxlength="1" onkeyup="dofo_autodebet(11, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[12]" id="rekening_autodebet[12]" size="1"    maxlength="1" onkeyup="dofo_autodebet(12, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[13]" id="rekening_autodebet[13]" size="1"    maxlength="1" onkeyup="dofo_autodebet(13, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[14]" id="rekening_autodebet[14]" size="1"    maxlength="1" onkeyup="dofo_autodebet(14, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[15]" id="rekening_autodebet[15]" size="1"    maxlength="1" onkeyup="dofo_autodebet(15, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[16]" id="rekening_autodebet[16]" size="1"    maxlength="1" onkeyup="dofo_autodebet(16, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[17]" id="rekening_autodebet[17]" size="1"    maxlength="1" onkeyup="dofo_autodebet(17, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[18]" id="rekening_autodebet[18]" size="1"    maxlength="1" onkeyup="dofo_autodebet(18, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[19]" id="rekening_autodebet[19]" size="1"    maxlength="1" onkeyup="dofo_autodebet(19, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[20]" id="rekening_autodebet[20]" size="1"    maxlength="1" onkeyup="dofo_autodebet(20, 1);doRek_autodebet();" /><font class="error">*</font>
							<span style="visibility: hidden;"><form:input path="msp_no_rekening_autodebet" id="msp_no_rekening_autodebet" cssErrorClass="inpError" size="1"/></span></td>
						</tr>
						<tr>
							<td align="left">atas nama:</td>
							<td><form:input path="msp_rek_nama_autodebet" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">tanggal debet:</td>
							<td><div id="msp_tgl_debet_div" >
								<spring:bind path="cmd.msp_tgl_debet" >
        		                    <script>
                                        var isDisabled;
                                        var status = '${statusForm}';
                                        if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
                                </spring:bind>
							<font class="error">*</font></div></td>
						</tr>
						<tr>
							<td align="left">tanggal valid:</td>
							<td><div id="msp_tgl_valid_div" >
								<spring:bind path="cmd.msp_tgl_valid" >
        		                    <script>
                                        var isDisabled;
                                        var status = '${statusForm}';
                                        if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
                                </spring:bind>
							<font class="error">*</font></div></td>
						</tr>
						<tr>
							<th colspan="2" class="subtitle">Dokumen yang dilengkapi</th>
						</tr>
						<tr>
							<th align="left">Tanggal Terima Admin:</th>
							<td><spring:bind path="cmd.msp_admin_date" >
        		                    <script>
                                        isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
                                </spring:bind></td>
						</tr>
						<tr>
							<th align="left" rowspan="2">ADMIN</th>
							<td>
								<form:checkbox id="msp_cek_ktp" cssClass="noBorder" path="msp_cek_ktp" value="1"/>Fotokopi KTP
								<form:checkbox id="msp_cek_kk" cssClass="noBorder" path="msp_cek_kk" value="1"/>Fotokopi Kartu Keluarga
								<form:checkbox id="msp_cek_npwp" cssClass="noBorder" path="msp_cek_npwp" value="1"/>Fotokopi NPWP
							</td>
						</tr>
						<tr>
							<td>
								<form:checkbox id="msp_cek_bukti_bayar" cssClass="noBorder" path="msp_cek_bukti_bayar" value="1"/>
								Bukti pembayaran
							</td>
						</tr>
						</table></fieldset>
						<fieldset>
					   <legend>ACTION</legend>
					   <table class="result_table2">
						<tr>
							<td colspan="2">
								<input type="hidden" name="msp_id" id="msp_id" value="${msp_id}" />
								<input type="hidden" name="kata" id="kata" size="1" value="insert" />
								<input type="button" name="edit" id="edit" value="Edit" style="visibility: hidden;width: 1px" onclick="return update();"/>
								<input type="submit" name="save" id="save" value="Save"  onclick="return insert();" />
								<input type="button" name="close" id="save" value="Close" onclick="window.close();" />
								<a href="input_fire.htm" id="refreshPage"></a>
							</td>
						</tr>
					</table>
					</fieldset>
					</div>
		</div>
	</div>	
</form:form>
	<c:if  test="${not empty successMessage}">	
			<script type="text/javascript">													
				alert('${successMessage}');	
				parentForm = self.opener.document.forms['formpost'];
				window.opener.transfer_process('${cmd.msp_fire_id}','${msp_id}');
				//parentForm.elements['refreshButton'].click();
				window.close();
			</script>
	</c:if>
</body>
</html>