<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
</script>
</head>
<BODY onload="resizeCenter(900,600); document.title='PopUp :: Daftar Polis MallAssurance'; setupPanes('container1', 'tab1'); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Mohon Agar Polis-polis Berikut Dapat di Scan, Upload, dan Transfer ke Underwriting </a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<table class="simple">
					<caption>Daftar Polis MallAssurance yang Belum Ditransfer ke Underwriting.</caption>
					<thead>
						<tr>
							<th style="text-align: center">No</th>
							<th style="text-align: center">No.SPAJ</th>
							<th style="text-align: center">No.Polis</th>
							<th style="text-align: center">Nama Produk</th>
							<th style="text-align: center">Cara Bayar</th>
							<th style="text-align: center">Premi</th>
							<th style="text-align: center">Tgl. Print</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="l" items="${sessionScope.listSpajMall}">
							<tr>
								<td style="text-align: center">&nbsp; ${l.NO} &nbsp;</td>
								<td>${l.REG_SPAJ}</td>
								<td>${l.MSPO_POLICY_NO_FORMAT}</td>
								<td>${l.LSDBS_NAME}</td>
								<td style="text-align: right">${l.LSCB_PAY_MODE}</td>
								<td style="text-align: right">${l.MSPR_PREMIUM}</td>
								<td style="text-align: center">${l.TGL_PRINT}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<br>
				<input type="button" name="close" value="Close" onclick="window.close();">
			</div>
		</div>
	</div>
</form>

<c:if test="${jml eq 1}">
<script>backToParent('${v1}');</script>
</c:if>
</body>
<%@ include file="/include/page/footer.jsp"%>