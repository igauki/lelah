<%@ include file="/include/page/header.jsp"%>
<script>

	function showPage(hal){
		var spaj = document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value;
		var lsbs = document.formpost.kodebisnis.value;
		var lsdbs = document.formpost.numberbisnis.value;
		if(spaj == ''){
			alert('Harap pilih SPAJ terlebih dahulu!');
		}else{
			createLoadingMessage();
			if('tampil' == hal){
				document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj+'&halForBlacklist=uw';
			}else if('endors' == hal){
				document.getElementById('infoFrame').src='${path}/bac/endors.htm?window=main&spaj='+spaj;
			}else if('edit' == hal){
				document.getElementById('infoFrame').src='${path}/bac/edit.htm?showSPAJ='+spaj;
			}else if('editagen' == hal){
				document.getElementById('infoFrame').src='${path}/bac/editagenpenutup.htm?spaj='+spaj;			
			}else if('uwinfo' == hal){
				document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=uwinfo&spaj='+spaj;			
			}else if('checklist' == hal){
				document.getElementById('infoFrame').src='${path}/checklist.htm?lspd_id=2&reg_spaj='+spaj;
			}else if('sblink' == hal){
				popWin('${path}/uw/inputsblink.htm?reg_spaj='+spaj, 550, 875); 
			}else if('questionnaire' == hal){
				document.getElementById('infoFrame').src='${path}/uw/questionnaire.htm?reg_spaj='+spaj; 
			}else if('batal' == hal){
				document.getElementById('infoFrame').src='${path }/uw/cancelUw.htm?spaj='+spaj;
			}else if('status' == hal){
				document.getElementById('infoFrame').src='${path }/uw/status.htm?spaj='+spaj;
			}else if('premi' == hal){
				document.getElementById('infoFrame').src='${path }/uw/premi.htm?spaj='+spaj;
			}else if('medis' == hal){
				document.getElementById('infoFrame').src='${path }/uw/medical_new.htm?spaj='+spaj;			
			}else if('kyc' == hal){
				var result = confirm("Apakah termasuk Transaksi Keuangan Mencurigakan? Tekan YES untuk YA, dan CANCEL untuk TIDAK.", "U/W");
				proses = 0;
				if(result == true) proses=1;
				document.getElementById('infoFrame').src='${path }/uw/uw.htm?window=kyc_proses&spaj='+spaj+'&proses='+proses;
			}else if('simultan' == hal){
				document.getElementById('infoFrame').src='${path }/uw/simultan_new.htm?spaj='+spaj;
			}else if('reas' == hal){
				var result=confirm("Apakah Data Anda Sudah Benar ?\nCek Extra Premi, Medis dll..!", "U/W");
				if(result==true) document.getElementById('infoFrame').src='${path }/uw/reas_new.htm?spaj='+spaj;
			}else if('reasguthrie' == hal){
				var result=confirm("SPAJ GUTHRIE! Lanjutkan Proses REAS?", "U/W");
				if(result==true) document.getElementById('infoFrame').src='${path }/uw/viewer.htm?window=reas_guthrie&spaj='+spaj;
			}else if('aksep' == hal){
				if(confirm("Apakah Anda Ingin Aksep/Fund Polis Ini?")){
					document.getElementById('infoFrame').src='${path }/uw/akseptasi.htm?spaj='+spaj;
				}
			}else if('transfer' == hal){
				if(confirm("Apakah Anda Ingin Transfer Polis Ini?")){
					document.getElementById('infoFrame').src='${path }/uw/transfer.htm?spaj='+spaj+'&flagNew='+1;
				}	
			}else if('ubahreas' == hal){
				document.getElementById('infoFrame').src='${path }/uw/ubah_reas.htm?spaj='+spaj;
			}else if('nik' == hal){
				if  (lsbs == 138 || lsbs == 140 || lsbs == 141 || lsbs == 148 || lsbs == 149 || lsbs == 156 || lsbs == 139 || 
						(lsbs == 142 && lsdbs == 4) || (lsbs == 143 && lsdbs == 2) || (lsbs == 158 && lsdbs == 4) || (lsbs == 158 && lsdbs == 7)){
					popWin('${path}/bac/nik.htm?sts=insert&spaj='+spaj, 500, 800);
				}else{
					alert('Nik tidak dapat diisi untuk polis ini');					
				}
			}else if('tglspaj' == hal){
				document.getElementById('infoFrame').src='${path }/uw/viewer.htm?window=editTglTrmKrmSpaj&spaj='+spaj+'&show=2';
			}else if('viewlimit' == hal){
				popWin('${path}/common/menu.htm?frame=view_limit_uw', 600, 800);
			}else if('viewsimultan' == hal){
				popWin('${path }/uw/viewer.htm?window=view_simultan', 600, 800);
			}else if('viewkyc' == hal){
				popWin('${path }/uw.htm?window=kyc_proses', 600, 800);
			}else if('hcp' == hal){
				if (lsbs != 161){
					if(lsbs==183 || lsbs==189 || lsbs==193){
							popWin('${path}/bac/ttghcp.htm?sts=insert&showSPAJ='+spaj, 500, 800);
						}else {
							if (eval(document.formpost.jml_peserta.value) > 0){
								popWin('${path}/bac/ttghcp.htm?sts=insert&showSPAJ='+spaj, 500, 800);
							}else{
								alert("Tidak bisa mengisi data SM/Eka Sehat/HCP Family karena belum mengambil plan BASIC, Silahkan melakukan penginputan plan BASIC pada halaman penginputan spaj utama terlebih dahulu.");
							}
						}
				}else{
						alert("Produk ini tidak bisa mengisi data SM/Eka Sehat/HCP Family");
				}
			}else if('reffbii' == hal){
				popWin('${path}/bac/reff_bank.htm?window=main&spaj='+spaj, 400, 700);
			}else if('claim' == hal){
				popWin('${path}/uw/uw.htm?window=healthClaim&spaj='+spaj, 400, 900);
			}
		}
	}

	/*
	function simultan(){
		createLoadingMessage();
		var spaj=document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value
		document.getElementById('infoFrame').src='${path }/uw/simultan.htm?spaj='+spaj;
	}
	function reas(){
		createLoadingMessage();
		var spaj=document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value
		var result=confirm("Apakah Data Anda Sudah Benar ?\nCek Extra Premi, Medis dll..!", "U/W");
		if(result==true)
			document.getElementById('infoFrame').src='${path }/uw/reas.htm?spaj='+spaj;
	}
	*/
	
	function cariregion(spaj,nama){
		if(spaj != ''){
			if(document.getElementById('docFrame')) document.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+spaj;
			if(document.getElementById('infoFrame')) document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj+'&halForBlacklist=uw';
			ajaxManager.listcollect(spaj , nama,
			{callback:function(map) {
				DWRUtil.useLoadingMessage();
				document.formpost.koderegion.value=map.LSRG_NAMA;
				document.formpost.kodebisnis.value = map.LSBS_ID;
				document.formpost.numberbisnis.value = map.LSDBS_NUMBER;
				document.formpost.jml_peserta.value = map.jml_peserta;
			   },
			  timeout:15000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
			});
			
		}	
	}
	
	function awal(){
		cariregion(document.formpost.spaj.value,'region');
		setFrameSize('infoFrame', 90);
		setFrameSize('docFrame', 90);
	}
	
	function copyToClipboard(txt){
	
		if( window.clipboardData && clipboardData.setData ){
		
			clipboardData.setData("Text", txt);
		}
		else{
		 alert("Internet Explorer required"); 
		}
	}
	
	function run(){
		var shell = new ActiveXObject("WScript.Shell"); 
		var commandtoRun = "\\\\aplikasi\\lions\\secan4400f.exe";
	   
		if (shell){		
		 shell.run(commandtoRun); 
		} 
		else{ 
			alert("program or file doesn't exist on your system."); 
		}
	}
	
	function copyAndRun(){ 
	 	txt = document.getElementById("spaj").value;
	 	copyToClipboard(txt);
	 	run();		
	}

</script>
<body  onload="awal();" onresize="setFrameSize('infoFrame', 90); setFrameSize('docFrame', 90);" style="height: 100%;">
<form name="formpost" method="post">
<div class="tabcontent">
<table class="entry2" style="width: 98%;">
	<tr>
		<th>Cari SPAJ</th>
		<td> <input type="hidden" name="halForBlacklist" value="uw"> 
		  <input type="hidden" name="koderegion" > 
          <input type="hidden" name="kodebisnis">
          <input type="hidden" name="numberbisnis">
          <input type="hidden" name="jml_peserta">
			<select name="spaj" id="spaj" onChange="cariregion(this.options[this.selectedIndex].value,'region');">
				<option value="">[--- Silahkan Pilih/Cari SPAJ ---]</option>
				<c:forEach var="s" items="${daftarSPAJ}">
					<option value="${s.REG_SPAJ }" style="background-color: ${s.BG};" 
						<c:if test="${s.REG_SPAJ eq param.spaj }">selected<c:set var="ada" value="ada" /></c:if>>${s.SPAJ_FORMATTED}
					- ${s.POLICY_FORMATTED }</option>
				</c:forEach>
			</select>

	<tr>
		<th>Proses Utama</th>
		<td><%--<input name="btn_batal" type="button" value="Batalkan" 			onClick="showPage('batal');" accesskey="B" onmouseover="return overlib('Alt-B	', AUTOSTATUS, WRAP);" onmouseout="nd();">--%><input type="button" value="View Simultan" name="view simultan" onclick="showPage('viewsimultan');" onmouseover="return overlib('View Simultan Polis', AUTOSTATUS, WRAP);" onmouseout="nd();">       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;          
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<table style="margin: 0 0 0 0; padding: 0 0 0 0; width: 100%;">
				<tr>
					<c:choose>
						<c:when test="${sessionScope.currentUser.wideScreen}">
							<td style="width: 60%;">
								<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
									width="100%"  height="100%"> Please Wait... </iframe>
							</td>
							<td style="width: 40%;">
								<iframe src="" name="docFrame" id="docFrame" width="100%">E-Lions</iframe>
							</td>
						</c:when>
						<c:otherwise>
							<td>
								<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
									width="100%"  height="100%"> Please Wait... </iframe>
							</td>
						</c:otherwise>
					</c:choose>
				</tr>
			</table>		
		</td>	
	</tr>
</table>
</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>