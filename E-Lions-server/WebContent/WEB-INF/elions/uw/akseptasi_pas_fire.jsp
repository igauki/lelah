<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<!-- DatePicker Script (jscalendar) -->
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	function onLoad(){
		if(trim(document.formpost.result.value)!=''){
			alert(document.formpost.result.value);
		}
	}
	
	function cariData(){
		var pilter = document.getElementById('pilter').value;
		var tipe = document.getElementById('tipe').value;
		var kata = document.getElementById('kata').value;
		
		document.getElementById('refreshPage').href = 'akseptasi_pas.htm?tipe='+tipe+'&kata='+kata+'&pilter='+pilter;
		document.getElementById('refreshPage').click();
	
	}
	
	function update(msp_id, action){
		document.getElementById('msp_id').value = msp_id;
		document.getElementById('action').value = action;
		if(trim(document.getElementById('action').value)=='') return false;
		else createLoadingMessage();	
	}
	
	function openDocPdf(mid){
		document.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list_bp&mid='+mid;
	}

</script>
</head>
<BODY onload="" style="height: 100%;width: 100%;">
	<table class="simple" style="width: 100%; height: 100%;">
	<tr>
	<td style="width: 60%; height: 100%">
	<table class="simple" style="width: 100%; height: 100%;">
		<tr >
							<th style="text-align:left ;width: 20px;">Cari Pas :</th>
							<th style="text-align:left ;height: 30px;">
								<select name="tipe_pas" id="tipe_pas">
									<option value="1" >Nama</option>
									<option value="2" >No.Bukti Identitas</option>
									<option value="7" >No.HP</option>
									<option value="3" >Fire Id</option>
									<option value="4" >No.Kartu</option>
								</select>
								<select name="pilter_pas" id="pilter_pas">
									<option  value="LIKE">LIKE</option>
									<option  value="LT"><</option>
									<option  value="LE"><=</option>
									<option  value="EQ">=</option>
									<option  value="GE">>=</option>
									<option  value="GT">></option>
								</select>		
								<input type="text" name="kata_pas" id="kata_pas" size="34" value="" 
								onkeypress="if(event.keyCode==13) {document.frames('infoFrame1').document.forms('formpost').elements('kata').value=this.value;document.frames('infoFrame1').document.forms('formpost').elements('search').click(); return false;}" />
								<input type="button" name="search_pas" id="search_pas" value="Search" 
								onclick="document.frames('infoFrame1').document.forms('formpost').elements('pilter').value=document.getElementById('pilter_pas').value;
								document.frames('infoFrame1').document.forms('formpost').elements('tipe').value=document.getElementById('tipe_pas').value;
								document.frames('infoFrame1').document.forms('formpost').elements('kata').value=document.getElementById('kata_pas').value;document.frames('infoFrame1').cariData();" onmouseout="nd();" />
							</th>
		</tr>
		<tr>
			<td style="width: 100%;" colspan="2">
				<iframe src="${path}/uw/akseptasi_pas.htm" name="infoFrame1" id="infoFrame1"
					width="100%"  height="100%"> Please Wait... </iframe>
			</td>
		</tr>
		<tr >
							<th style="text-align:left ;width: 20px;">Cari Fire :</th>
							<th style="text-align:left ;height: 30px;">
								<select name="tipe_fire" id="tipe_fire">
									<option value="5" >Nama</option>
									<option value="6" >No.Bukti Identitas</option>
									<option value="8" >No.HP</option>
									<option value="3" >Fire Id</option>
									<option value="4" >No.Kartu</option>
								</select>
								<select name="pilter_fire" id="pilter_fire">
									<option  value="LIKE">LIKE</option>
									<option  value="LT"><</option>
									<option  value="LE"><=</option>
									<option  value="EQ">=</option>
									<option  value="GE">>=</option>
									<option  value="GT">></option>
								</select>		
								<input type="text" name="kata_fire" id="kata_fire" size="34" value="" 
								onkeypress="if(event.keyCode==13) {document.frames('infoFrame2').document.forms('formpost').elements('kata').value=this.value;document.frames('infoFrame2').document.forms('formpost').elements('search').click(); return false;}" />
								<input type="button" name="search_fire" id="search_fire" value="Search" 
								onclick="document.frames('infoFrame2').document.forms('formpost').elements('pilter').value=document.getElementById('pilter_fire').value;
								document.frames('infoFrame2').document.forms('formpost').elements('tipe').value=document.getElementById('tipe_fire').value;
								document.frames('infoFrame2').document.forms('formpost').elements('kata').value=document.getElementById('kata_fire').value;document.frames('infoFrame2').cariData();" onmouseout="nd();" />
								<input type="radio" name="jenisDist_fire" id="jenisDist_all_fire" value="" onclick="document.frames('infoFrame2').document.forms('formpost').document.getElementById('jenisDist_all').click();document.frames('infoFrame2').cariData();" checked/>			
								ALL
								<input type="radio" name="jenisDist_fire" id="jenisDist_idv_fire" value="individu" onclick="document.frames('infoFrame2').document.forms('formpost').document.getElementById('jenisDist_idv').click();document.frames('infoFrame2').cariData();"/>			
								INDIVIDU
								<input type="radio" name="jenisDist_fire" id="jenisDist_dmtm_fire" value="dmtm" onclick="document.frames('infoFrame2').document.forms('formpost').document.getElementById('jenisDist_dmtm').click();document.frames('infoFrame2').cariData();"/>
								DMTM
							</th>
		</tr>
		<tr>
			<td style="width: 100%;" colspan="2">
				<iframe src="${path}/uw/akseptasi_fire.htm" name="infoFrame2" id="infoFrame2"
					width="100%"  height="100%"> Please Wait... </iframe>
			</td>
		</tr>
	</table>
	</td>
	<td style="width: 40%; height: 100%">
		<iframe name="docFrame" id="docFrame" style="height: 100%;" src="" width="100%"> Please Wait... </iframe>
	</td>
</tr>
</table>
</body>
<%@ include file="/include/page/footer.jsp"%>