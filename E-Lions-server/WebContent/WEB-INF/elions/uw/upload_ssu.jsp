<%@ include file="/include/page/header.jsp"%>


<script type="text/javascript">
	
$().ready(function() {
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		$("#tabs").tabs();
		
		if('${showTab}' !== '') {
		    $('a[href=#${showTab}]').click();
		}

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
});
	
	
	function onload(page) {
// 		document.getElementById("tab-2").style.display = 'none';
// 		document.getElementById("tab-3").style.display = 'none';
	}
	
	function buttontab(tab_ke) {
// 		alert(tab_ke);
		if (tab_ke==1) {
			document.getElementById("tab-1").style.display = 'block';
			document.getElementById("tab-2").style.display = 'none';
			document.getElementById("tab-3").style.display = 'none';
		}else if (tab_ke==2) {
			document.getElementById("tab-1").style.display = 'none';
			document.getElementById("tab-2").style.display = 'block';
			document.getElementById("tab-3").style.display = 'none';
		}else {
			document.getElementById("tab-1").style.display = 'none';
			document.getElementById("tab-2").style.display = 'none';
			document.getElementById("tab-3").style.display = 'block';
		}
	}
	
	function tampil(fil){
		v_filename = fil.substring(fil.indexOf("~", 0)+1);
		if(fil != '') document.getElementById('docFrame').src = '${path}/common/util.htm?window=doc_ssu&file='+v_filename;
	}
	
	function test(ul) {
		if(ul==1){
			document.getElementById('docFrame').style.visibility = "hidden";
		}else if(ul==2){
			document.getElementById('docFrame').style.visibility = "visible";
		}else {
			document.getElementById('docFrame').style.visibility = "hidden";
		}
	}
	
	function addRowDOM1 (tableID, jml) {
		if(parseInt(document.getElementById("tableProd").rows.length)<=10){
	
		tmp = 1;
		if (document.all) {
			for (var i = 1; i <= parseInt(jml); i++) { 
				flag_add1=1;
		   		var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
		   		var idx = null;
		   
				tmp = parseInt(document.getElementById("tableProd").rows.length);
				idx = tmp;
				var pesan = document.getElementById('scc'+idx).value;
				
				var oRow = oTable.insertRow(oTable.rows.length);
		        var oCells = oRow.cells;
		        
		// 			if(jml=='1'){
						var cell = oRow.insertCell(oCells.length);
						cell.innerHTML = "<tr><td><input type=file id='file"+idx+"' name='file"+idx+"' size=60 ></td>";
						var cell = oRow.insertCell(oCells.length);
						cell.innerHTML = "<td ><div id='prod"+idx+"'></div><input type=hidden name='nameprod"+idx+"'>"+
											"<input type=hidden name='pdfname"+idx+"' id='pdfname"+idx+"'><input type=hidden name='putable"+idx+"'> </td>";
						var cell = oRow.insertCell(oCells.length);
						cell.innerHTML = "<td ><div id='subt"+idx+"'><select disabled><option></option></select></div>"+
											"<div id='plan"+idx+"'> </div><input type=hidden name='namesub"+idx+"'></td>";
						var cell = oRow.insertCell(oCells.length);
						cell.innerHTML = "<td><span class=error id='error"+idx+"'></span><span class=scc id='scc"+idx+"'>"+pesan+"</span>"+
											"<span class=nama_prod id='nama_prod"+idx+"'></span></td></tr>";
											
						listProd('selectLsbsSsu', 'nameprod'+idx, 'idprod'+idx, 'LSBS_ID', 'prod'+idx, idx);
		// 			}
			}
		}
		idx = tmp;
		document.getElementById('fl').value = idx;
// 		alert(idx);
		if (idx>1){
			document.getElementById('btndel1').disabled = false;
		}else{
			document.getElementById('btndel1').disabled = true;
		}
	}else{
		alert("Maksimum 10 File Upload Sekaligus.")
	}
	
 	}
	function awal(){
		listProd('selectLsbsSsu', 'nameprod1', 'idprod1', 'LSBS_ID', 'prod1', 1);
	} //onload awal id=1
	
	function listProd(selectType, selectName, selectValue, col1, selectLocation,kex){
		var text = '<input type="text" name="'+selectName+'" id="'+selectValue+'" onkeypress="return enterUpload('+kex+');" /> &nbsp;';
		text += '<input type="button" name="btnUploadCari'+kex+'" id="btnUploadCari'+kex+'" value="SEARCH" onclick="kotak2('+kex+');"/>';
		
		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
	}
	
	function enterUpload(kex){
	    if (event.keyCode == 13) {
	         document.getElementById('btnUploadCari'+kex).click();
	         return false;
	    }
	}
	
	function kotak2(kex)
	{	
// 		document.getElementById('subt'+kex).style.display = 'none';
// 		document.getElementById('nama_prod'+kex).innerHTML = "";
// 		var id = document.getElementById('idprod'+kex).value;
		
// 		if(id!=""){
// 			listDetProd(id,'selectLsdbsSsu', 'namesub'+kex, 'idsub'+kex, 'LSDBS_NUMBER', 'plan'+kex, kex);			
// 		}
// 		
		var a  = document.getElementById('idprod'+kex).value;
		document.getElementById('subt'+kex).style.display = 'none';
// 		alert(a+"\n"+kex);
// 		document.getElementById('subc3').style.display = 'none';
		listDetUpload(a,'selectLsdbsSrc2', 'namesub'+kex, 'idsub'+kex, 'NAMA_LSDBS', 'plan'+kex, kex);
	}
	
	function listDetUpload(p, selectType, selectName, selectValue, col1, selectLocation, kex){
	eval('ajaxManager.'+selectType)(p, {
	  callback:function(list) {

		DWRUtil.useLoadingMessage();
		var text = '<select name="'+selectName+'" id="'+selectValue+'" onchange="cari4Upload('+kex+');"';
		text+='>';
		text += '<option value=""';
		text += '></option>';		
		for(i = 0; i < list.length; i++) {
			text += '<option value="'+eval('list[i].KODE_FILE')+'"';
// 			if(selectValue==eval('list[i].'+col1)) text += ' selected ';
			text += '>'+eval('list[i].'+col1)+'</option>';
		}
		text += '</select>';
		
		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
	   },
	  timeout:180000,	
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
	
	}
	
	function cari4Upload(kex) {
		var fil = (document.getElementById('idsub'+kex).value);
// 		v_filename = fil.substring(fil.indexOf("~", 0)+1);
		var bisnis = fil.substring(3,0);
		var det = fil.substring(7,4); 
		
		var produkuntuktable = fil;
		document.getElementById('putable'+kex).value= produkuntuktable;
		
		
		if(fil != '') {
			specialCaseUpload(bisnis, det, kex);
			
		}
	}
	
	function specialCaseUpload(bisnis ,det, kex)
	{
		ajaxManager.productFileSSU(bisnis,det,
			{callback:function(result) {
	
				DWRUtil.useLoadingMessage();
					v_filename=result;
// 					document.getElementById('docFrame').src = '${path}/common/util.htm?window=doc_ssu&file='+v_filename;
// 					document.getElementById('hasilcari2').innerHTML=v_filename;
					document.getElementById('nama_prod'+kex).innerHTML = v_filename;
					document.getElementById('pdfname'+kex).value= v_filename;
			   },
			  timeout:180000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
			});
			
			var text = '<input type="text" name="sprod1" id="sprod1" style="display:none; text-align:right; text-transform:none;" value=".pdf"/>';
			document.getElementById('sprodplace1').innerHTML=text;
					
			document.getElementById('spesial1').style.display = 'block';
	}
	
	function listDetProd(p, selectType, selectName, selectValue, col1, selectLocation, kex){
	eval('ajaxManager.'+selectType)(p, {
	  callback:function(list) {

		DWRUtil.useLoadingMessage();

		var text = '<select name="'+selectName+'" id="'+selectValue+'" onchange="kotak1('+kex+');"';
		text+='>';
		text += '<option value=""';
		text += '></option>';		
		for(i = 0; i < list.length; i++) {
			text += '<option value="'+eval('list[i].'+col1)+'"';
			if(selectValue==eval('list[i].'+col1)) text += ' selected ';
			text += '>'+eval('list[i].'+col1)+'</option>';
		}
		text += '</select>';
		
		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
	   },
	  timeout:180000,	
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
	
	}
	
	 function kotak1(kex)
	{
		var id = document.getElementById('idprod'+kex).value;
		var sub = document.getElementById('idsub'+kex).value;
// 		if(document.getElementById("error1").innerHTML != ""){
// 			document.getElementById("error1").style.display = 'none';
// 		}
// 		alert(id+"-"+sub);
		if(id!="" && sub!=""){
			listProdSSU(id,sub,kex);
		}
	}
	
	function listProdSSU(lsbsId,lsdbsNumber,kex)
	{
		ajaxManager.listProdSSU(lsbsId,lsdbsNumber,
			{callback:function(map) {
	
				DWRUtil.useLoadingMessage();
					document.getElementById('nama_prod'+kex).innerHTML = map.nama_prod;

			   },
			  timeout:180000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
			});
	}
	
	function cek(){
		var fl = parseInt(document.getElementById('fl').value);
		var validasi = 0;
		var tidakbisa = 0;

		for (var i = 1; i <= fl; i++) { 
			if(document.getElementById("idprod"+i).value=="" || document.getElementById("idsub"+i).value=="" || document.getElementById("file"+i).value=="") {
				validasi += 1;
			}
		}
		if(validasi!=0){
			alert("Harap mengisi semua field dengan lengkap.");
			return false;
		}
		
		for (var i = 1; i <= fl; i++) { 
			if(document.getElementById("nama_prod"+i).innerHTML=="TIDAK BISA DIUPLOAD") {
				tidakbisa += 1;
			}
		}
		
		
		if(tidakbisa!=0){
			if(document.getElementById("spesial1").checked){
				return true;
			}else{
				alert("Ada yang tidak bisa diupload!");
				return false;
				
			}
		}else{
			return true;
		}
	}
	
	function delRow1(tableID, jml){
		tmp = parseInt(document.getElementById("tableProd").rows.length);
		
		if(tmp>2){
			document.getElementById('btndel1').disabled = false;
		  	var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
 			oTable.deleteRow(parseInt(document.getElementById("tableProd").rows.length)-1);
 			if(tmp==3){
 				document.getElementById('btndel1').disabled = true;
 			}
 		}
	} 

	function untukCari(lsbs){
		document.getElementById('subc2').style.display = 'none';
		listDetProdSrc(lsbs,'selectLsdbsSrc', 'cari_plan', 'cari_plan', 'NAMA_LSDBS', 'cari2');
	}
	
	function listDetProdSrc(p, selectType, selectName, selectValue, col1, selectLocation){
	eval('ajaxManager.'+selectType)(p, {
	  callback:function(list) {

		DWRUtil.useLoadingMessage();

		var text = '<select name="'+selectName+'" id="'+selectValue+'" onchange="cari2()"';
		text+='>';
		text += '<option value=""';
		text += '></option>';		
		for(i = 0; i < list.length; i++) {
			text += '<option value="'+eval('list[i].LSDBS_NUMBER')+'"';
// 			if(selectValue==eval('list[i].'+col1)) text += ' selected ';
			text += '>'+eval('list[i].'+col1)+'</option>';
		}
		text += '</select>';
		
		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
	   },
	  timeout:180000,	
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
	
	}
	
	function cari2(){
		document.getElementById('btncari').disabled = false;
	}
	
	function tombolcari(){
// 		var a  = document.getElementById('cari_prod').value;
// 		var b  = document.getElementById('cari_plan').value;
// 		var fil = (a+"-"+b+".pdf");
// 		v_filename = fil.substring(fil.indexOf("~", 0)+1);
// 		if(fil != '') {
// 			document.getElementById('docFrame').src = '${path}/common/util.htm?window=doc_ssu&file='+v_filename;
// 			document.getElementById('hasilcari').innerHTML=v_filename;
// 		}
		
// 		var fil = (document.getElementById('cari_plan2').value);
		var bisnis = document.getElementById('cari_prod').value;
		var det = document.getElementById('cari_plan').value;
		
		if(bisnis != '' && det != '') {
			specialCase(bisnis, det);
		}
		
	}
	
	function tombolcari2(){
		var a  = document.getElementById('fieldcari').value;
		document.getElementById('subc3').style.display = 'none';
		listDetProdSrc2(a,'selectLsdbsSrc2', 'cari_plan2', 'cari_plan2', 'NAMA_LSDBS', 'caribyname');
	}
	
	function listDetProdSrc2(p, selectType, selectName, selectValue, col1, selectLocation){
	eval('ajaxManager.'+selectType)(p, {
	  callback:function(list) {

		DWRUtil.useLoadingMessage();

		var text = '<select name="'+selectName+'" id="'+selectValue+'" onchange="cari3()"';
		text+='>';
		text += '<option value=""';
		text += '></option>';		
		for(i = 0; i < list.length; i++) {
			text += '<option value="'+eval('list[i].KODE_FILE')+'"';
// 			if(selectValue==eval('list[i].'+col1)) text += ' selected ';
			text += '>'+eval('list[i].'+col1)+'</option>';
		}
		text += '</select>';
		
		if(document.getElementById(selectLocation)){
			document.getElementById(selectLocation).innerHTML=text;
		}
	   },
	  timeout:180000,	
	  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
	});
	
	}
	 
	function cari3() {
		var fil = (document.getElementById('cari_plan2').value);
// 		v_filename = fil.substring(fil.indexOf("~", 0)+1);
		var bisnis = fil.substring(3,0);
		var det = fil.substring(7,4); 
		

		
		if(fil != '') {
			specialCase(bisnis, det);
// 			document.getElementById('docFrame').src = '${path}/common/util.htm?window=doc_ssu&file='+v_filename;
// 			document.getElementById('hasilcari2').innerHTML=v_filename;
		}
	}
	
	function specialCase(bisnis ,det)
	{
		ajaxManager.productFileSSU(bisnis,det,
			{callback:function(result) {
	
				DWRUtil.useLoadingMessage();
					v_filename=result;
					document.getElementById('docFrame').src = '${path}/common/util.htm?window=doc_ssu&file='+v_filename;
					document.getElementById('hasilcari2').innerHTML=v_filename;
			   },
			  timeout:180000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
			});
	}
	
	function enter(){
	    if (event.keyCode == 13) {
	        document.getElementById("btncari2").click();
	    }
	}
	
	function scase(){
		var cek = "";
		
		if(document.getElementById("spesial1").checked){
			cek="1";
			document.getElementById("nama_prod1").style.display = 'none';
			document.getElementById("sprod1").style.display = 'block';
		}else{
			cek="0";
			document.getElementById("nama_prod1").style.display = 'block';
			document.getElementById("sprod1").style.display = 'none';
		}
		
// 		alert(cek);
	}
	
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Verdana, Arial, sans-serif; font-size: 1em; }

	/* fieldset */
	.fieldset { margin-bottom: 1em; padding: 0.5em; border:none; }
	.fieldset .rowElem { padding: 8px 8px 8px 0px; border-bottom:1px solid #ccc; }
	.fieldset .rowElem label {display: inline-block; text-align:right; font-weight:bold;}
	.fieldset .rowElem td { padding:5px 15px 5px 15px; }
	.fieldset .rowElem th { padding:5px 15px 5px 15px; font-size: 13px;}

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	.error { text-align:center; text-transform:uppercase; color: red;
				margin-left:15px; font-family: verdana, geneva, arial, helvetica, sans-serif;
				font-size: 1.2em;}
	.scc { text-align:center; text-transform:uppercase; color: green;
				margin-left:15px; font-family: verdana, geneva, arial, helvetica, sans-serif;
				font-size: 1.2em;}
	.nama_prod { text-align:center; text-transform:uppercase; 	color: blue;
				margin-left:15px; font-family: verdana, geneva, arial, helvetica, sans-serif;
				font-size: 1.2em;}
	.tombols {padding:22px; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	.fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }
	.buttons { padding: 0px 22px;}
	
	#mggu {width: 30px;;}
	.tablehist {margin:auto;}
	#tab-2 {margin:auto; display:table;}
	#tab-3 {padding:0px 0px 0px 15px;}
	.tbl {
	    color: #555555;
	    font-weight: normal; 
	    padding: 0.4em 1em;
		border-radius: 4px;
	}
	
</style>


<body onLoad="buttontab(${page}); awal(); addRowDOM1('tableProd', ${jumlah});">
<div id="tabs">		
	<!-- <ul id="bagian">
		<li><a href="#tab-1" onClick="test(1);">UPLOAD SSU</a></li>
		<li><a href="#tab-2" onClick="test(2);"></a></li>	
		<li><a href="#tab-3" onClick="test(3);"></a></li>
	</ul> -->
	<div class="tombols">
	<form name="formPost" method="post">
		<input type="submit" value="UPLOAD SSU" name="upload" 
			onclick="buttontab(1);" class="tbl">
		<input type="submit" value="VIEW DOCUMENT" name="view" 
			onclick="buttontab(2);" class="tbl">
		<input type="submit" value="HISTORY UPLOAD" name="history" 
			onclick="buttontab(3);" class="tbl">
	</form>
	</div>
	
	<div id="tab-1">
	<form name="formPost" method="post" enctype="multipart/form-data">
		<div class="ui-widget ui-widget-content fieldset">

			<table class="rowElem" id="tableProd"  border="0" cellspacing="1" cellpadding="1">
				<tr>
					<th align="left">Pilih File (.PDF)</th>
					<th align="left">Produk</th>
					<th align="left">Nama Produk</th>
					<th></th>
					<th></th>
				</tr>
				<tr>
					<td>
						<input type="file" id="file1" name="file1" size="60" >
					</td>
					<td>
						<div id="prod1"> </div>
					</td>
					<td>
						<div id="subt1"><select disabled><option></option></select></div>
						<div id="plan1"> </div>
					</td>		
					<td>
						<span class="error" id="error1">${error1}</span>	
						<span class="scc" id="scc1">${scc1}</span>
						<span class="nama_prod" id="nama_prod1"></span>
						<span id="sprodplace1"></span>
					</td>
					<td>
						<c:if test="${sessionScope.currentUser.lde_id eq '01'}">
							<input type="checkbox" name="spesial1" id="spesial1" onclick="scase();" style="display: none;"/>
						</c:if>
					</td>
				</tr>
			</table>													
		</div>
		<div class="rowElem buttons">
			<input name="btnadd1" type="button" id="btnadd1" value="ADD" onClick="addRowDOM1('tableProd', '1')" class="tbl" >
			<input name="btndel1" type="button" id="btndel1" value="DEL" onClick="delRow1('tableProd', '1')" class="tbl" disabled >
			<input type="submit" name="save" id="save" value="Upload" onclick="return cek()" class="tbl" style="font-weight: bold"> 
			<input type="hidden" name="fl" id="fl" value="1"> 
			<input type="hidden" id="scc2" value="${scc2}"> 
			<input type="hidden" id="scc3" value="${scc3}"> 
			<input type="hidden" id="scc4" value="${scc4}"> 
			<input type="hidden" id="scc5" value="${scc5}"> 
			<input type="hidden" id="scc6" value="${scc6}">
			<input type="hidden" id="scc7" value="${scc7}"> 
			<input type="hidden" id="scc8" value="${scc8}"> 
			<input type="hidden" id="scc9" value="${scc9}"> 
			<input type="hidden" id="scc10" value="${scc10}"> 
			<input type="hidden" name="pdfname1" id="pdfname1" value=""> 
			<input type="hidden" name="putable1" id="putable1" value=""> 
		</div>
	</form>
	</div>
	
	<div id="tab-2">
	<div class="ui-widget ui-widget-content fieldset">
		<table class="rowElem " style="width: 100%; " cellspacing="10" >
			<tr>
				<th align="right" style="width: 25%;">Pilih Dokumen PDF : </th>
				<td align="left" style="width: 75%;">
					<select name="dok" id="dok" onChange="tampil(this.value);">
						<option value="">========= [ Pilih Dokumen ] =========</option>
						<optgroup label="SYARAT-SYARAT UMUM">
						<c:forEach var="s" items="${daftarFileSSU}">
							<option value="${s.key}">${s.key}</option>
						</c:forEach>
						</optgroup>
						<c:if test="${not empty daftarFileSSK}">
							<optgroup label="SYARAT-SYARAT KHUSUS">
							<c:forEach var="s" items="${daftarFileSSK}">
								<option value="${s.key}">${s.key}</option>
							</c:forEach>
							</optgroup>
						</c:if>
					</select>
				</td>
			</tr>
			<tr>
				<th align="right" style="width: 25%;">Pilih Produk : </th>
				<td align="left" style="width: 75%;">
					<select name="cari_prod" id="cari_prod" onChange="untukCari(this.value)">
						<option value=""> </option>
						<c:forEach var="s" items="${cari_lsbs}">
							<option value="${s.LSBS_ID}">${s.NAMA_LSBS}</option>
						</c:forEach>
					</select>
					<span id="subc2"><select disabled><option></option></select></span>
					<span id="cari2"> </span>
					&nbsp;
					<input type="button" name="btncari" id="btncari" value="SEARCH" onclick="tombolcari();" disabled> 
					<!-- <span class="scc" id="hasilcari"></span> -->
				</td>
			</tr>
			<tr>
				<th align="right" style="width: 25%;">Search by Name : </th>
				<td align="left" style="width: 75%;">
					<input type="text" name="fieldcari" id="fieldcari" onkeypress="enter();"/> 
					<input type="button" name="btncari2" id="btncari2" value="SEARCH" onclick="tombolcari2();" > 
					<span class="scc" id="caribyname"></span>
					<span id="subc3"><select disabled><option></option></select></span>
					<!-- <span class="scc" id="hasilcari2"></span> -->
					<input type="hidden" id="kodespecial"/>				
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" style="width: 100%;"><span class="scc" id="hasilcari2"></span></td>
			</tr>
			<tr>
				<td colspan="2">
					<iframe name="docFrame" src="" id="docFrame" width="80%" height="490px" align="center" style="margin:auto; display:block;">  Please Wait... </iframe>
				</td>
			</tr>
		</table>
	</div>
	</div>
	
	<div id="tab-3">
		<div class="ui-widget ui-widget-content fieldset">
			<table class="rowElem tablehist" border="0" cellspacing="1" cellpadding="5" >
				<tr style="background-color: #dcdcdc;">
					<th nowrap align="center" >ID</th>
					<th nowrap align="center" >JENIS</th>
					<th nowrap align="center" >TANGGAL UPLOAD</th>
					<th nowrap align="center" >KODE PRODUK</th>
					<th nowrap align="center" >NAMA PRODUK</th>
					<th nowrap align="center" >NAMA FILE</th>
					<th nowrap align="center" >USER UPLOAD</th>
				</tr>
				<c:set var="xx" value="1" />
				<c:forEach items="${hist_upload}" var="d" varStatus="e">		
					<tr <c:if test="${(e.index+2)%2==0}">style="background-color: #eee;"</c:if> >
						<td nowrap align="center">${d.UPLOAD_ID}</td>
						<td nowrap align="center">${d.UPLOAD_JENIS}</td>
						<td nowrap align="center">${d.UPLOAD_DATE}</td>
						<td nowrap align="center">${d.TEMP_FILENAME}</td>
						<td nowrap align="center">${d.KETENTUAN}</td>
						<td nowrap align="center">${d.FILENAME}</td>
						<td nowrap align="center">${d.LUS_FULL_NAME}</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
</div>
</body>

<%@ include file="/include/page/footer.jsp"%>