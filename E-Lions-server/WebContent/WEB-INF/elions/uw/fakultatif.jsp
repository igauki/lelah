<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript" src="${path }/include/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		function addRow() {
			var no  = $("#row").val();
			if(no == '') no = 1
			else no = parseInt($("#row").val()) + 1;
			var row = '<tr id="row'+no+'">'+
						   '<td><input type="text" name="noPol'+no+'" id="noPol'+no+'" style="text-align: right;width: 100%;"></td>'+
						   '<td><input type="text" name="prod'+no+'" id="prod'+no+'" style="text-align: right;width: 100%;"></td>'+
						   '<td>'+
						   		'<select name="kurs'+no+'" style="width: 100%">'+
						   			'<option value="Rp">Rp</option>'+
						   			'<option value="$US">$US</option>'+						   			
						   		'</select>'+
						   '</td>'+
						   '<td><input type="text" name="up'+no+'" id="up'+no+'" style="text-align: right;width: 100%;"></td>'+
						   '<td><input type="text" name="ret'+no+'" id="ret'+no+'" style="text-align: right;width: 100%;"></td>'+
				      '</tr>';
			$("#tab > tbody:last").append(row);	
			$("#row").val(no);
		}
		
		/*$(function() {
			addRow();
		});*/
		
		$("#add").click(function() { addRow();});
		
	});
</script>	
<script type="text/javascript">
	function tampilkan2(forminput, report, format,flag){
		document.getElementById('show').value = format;
		document.getElementById(forminput).action='${path}/report/fakultatif.'+format+'?window='+report;
		
		if(flag != null) document.getElementById('send').value = flag;
		document.getElementById(forminput).submit();
		return false;
	}
	
	function buka(elemen){
		if(document.formCetakFak.elements[elemen]){
			document.formCetakFak.elements[elemen].readOnly = false;
			document.formCetakFak.elements[elemen].setAttribute('class', '');
			document.formCetakFak.elements[elemen].setAttribute('className', '');
		}
	}
		
	function tutup(elemen){
		if(document.formCetakFak.elements[elemen]){
			document.formCetakFak.elements[elemen].readOnly = true;
			document.formCetakFak.elements[elemen].setAttribute('class', 'readOnly');
			document.formCetakFak.elements[elemen].setAttribute('className', 'readOnly');
		}
	}
	function showElse(nilai){
		
		if(nilai=="0"){
			document.getElementById("reasurderElse").style.display = 'block';
		}else{
			document.getElementById("reasurderElse").style.display = 'none';
		}
	}
	function bukaTutup(){
		if(document.getElementById("L3").checked){
			buka("L3value");
			document.getElementById("L3value").disabled = '';
		}else{
			tutup("L3value");
			document.getElementById("L3value").disabled = 'true';
			document.getElementById("L3value").value = '';
		}
	}
	
</script>
<body style="height: 100%;">
	<div id="contents">
		<fieldset>
			<legend>Surat Fakultatif</legend>
			<form name="formpost" method="post" action="fakultatif.htm" target="_blank" id="formCetakFak">
				<input type="hidden" name="laporan" value="true">
				<table class="entry2">							
					<tr>
						<th style="width: 150px; " valign="top">No.Polis / Reg. SPAJ</th>
						<td>
							<input type="text" name="spaj" value="${spaj}" id="spaj">
						</td>
					</tr>
					<tr>
						<th style="width: 150px; " valign="top">Nilai Retensi</th>
						<td>
							<input type="text" name="retensi" value="${retensi}" id="retensi">
						</td>
					</tr>
					<!--<tr>
						<th style="width: 150px; " valign="top">Nama Rider</th>
						<td>
							<input type="text" name="rider" value="${rider}" id="rider">
						</td>
					</tr>					
					--><tr>
						<th style="width: 150px; " valign="top">Polis Simultan</th>
						<td>
							<table id="tab" class="entry2">
								<thead>
									<tr>
										<th width="200px">No. Polis</th>
										<th width="200px">Produk</th>
										<th width="50px">Kurs</th>
										<th width="30%">UP</th>
										<th width="30%">Retensi</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
							<input type="button" value="add row" id="add">
							<input type="hidden" id="row" name="row">
						</td>
					</tr>										
					<tr>
						<th valign="top">Reasurder</th>
						<td>
							<select name="reasurderSelect" id="reasurderSelect" onchange="showElse(this.value)">
								<c:forEach var="r" items="${cmd.cp_reas}">
									<option value="${r.key}">${r.value}</option>
								</c:forEach>
							</select>
							<div id="reasurderElse" style="display: none;">
								
								<table class="entry">
									<tr>
										<th colspan="2">
											Silahkan isi contact person untuk reasurder lainnya : 
										</th>
									</tr>
									<tr>
										<th>Nama Reasurder</th>
										<td><input type="text" name="reasurder" id="reasurder" size="75"></td>
									</tr>
									<tr>
										<th>Contact Person</th>
										<td>
											<input type="text" name="cp" id="cp" size="75"> <br/>
											<font class="info">info: lebih dari 1 contact person pisahkan dengan tanda slash "/" </font>
										</td>
									</tr>
									<tr>
										<th>Email</th>
										<td>
											<input type="text" name="email" id="email" size="75"> <br/>
											<font class="info">info: lebih dari 1 email pisahkan dengan tanda dash "-" </font>
										</td>
									</tr>
									<tr>
										<th>Fax</th>
										<td>
											<input type="text" name="fax" id="fax" size="75"> <br/>
											<font class="info">info: kode area input dalam tanda kurung ex :(021).  lebih dari 1 fax pisahkan dengan tanda dash "-" </font>
										</td>
									</tr>
									<tr>
										<th>Telepon</th>
										<td>
											<input type="text" name="telepon" id="telepon" size="75"> <br/>
											<font class="info">info: kode area input dalam tanda kurung ex :(021). lebih dari 1 telepon pisahkan dengan tanda dash "-" </font>
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<th valign="top">Lampiran</th>	
						<td>
							<label for="L1"><input type="checkbox" name="L1" id="L1" value="Surat Permintaan Asuransi"> Surat Permintaan Asuransi</label><br/>
							<label for="L2"><input type="checkbox" name="L2" id="L2" value="Data Medis"> Data Medis</label><br/>
							<label for="L3"><input type="checkbox" name="L3" id="L3" onclick="bukaTutup();" value=""> Lain-lain bila ada : keterangan </label>
							<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="text" name="L3value"  id="L3value" size="100" onchange="document.getElementById('L3').value = this.value;" class="readOnly" disabled="disabled">
							<font class="info">info : lebih dari 1 lampiran pisahkan dengan tanda dash "-" </font>
<!--							 <textarea rows="4" cols="20" name="L3value"  id="L3value" onchange="document.getElementById('L3').value = this.value;" class="readOnly" disabled="disabled"></textarea>-->
						</td>
					</tr>
					<tr>
						<th>&nbsp;</th>
						<td><label for="AL"><input type="checkbox" name="AL" id="AL" value="Ada Attachment Lain">Ada Attachment Lain</label><br/></td>
					</tr>
					<tr>
						<th>&nbsp;</th>
						<td>
							<input type="hidden" name="lampiran" id="lampiran">
							<a href="#" onclick="return tampilkan2('formCetakFak', 'cetak_surat', 'html');"
								onmouseover="return overlib('VIEW', AUTOSTATUS, WRAP);" onmouseout="nd();">
								<img style="border:none;" alt="HTML" src="${path}/include/image/page_html.gif"/></a>

							<a href="#" onclick="return tampilkan2('formCetakFak', 'cetak_surat', 'pdf');"
								onmouseover="return overlib('PRINT', AUTOSTATUS, WRAP);" onmouseout="nd();">
								<img style="border:none;" alt="PDF" src="${path}/include/image/pdf.gif"/></a>
							<input type="button" name="sendPDF" value="send pdf" onclick="return tampilkan2('formCetakFak', 'cetak_surat', 'pdf', 'sendPDF');">	
							<input type="hidden" name="show" id="show">
							<input type="hidden" name="send" id="send">
						</td>
					</tr>
					
				</TABLE>	
			
			</form>
		</fieldset>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>
