<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<!-- DatePicker Script (jscalendar) -->
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	function awal(){
		<c:if test="${not empty param.m}">
			alert('Informasi: ${param.m}');
		</c:if>
		<c:if test="${param.s eq 1}">
			alert('Informasi Save Bayar Link Berhasil Disimpan. Apabila data dan urutan sudah dipastikan benar, harap lakukan proses transfer.');
		</c:if>
		<c:if test="${param.s eq 2}">
			alert('Informasi Save Bayar Link Berhasil Diupdate.');
		</c:if>
	}
</script>
</head>
<BODY onload="setupPanes('container1', 'tab1'); awal(); resizeCenter(875, 550);" style="height: 100%;">
 
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Input Save Bayar Link</a>
			</li>
		</ul>

		<div class="tab-panes">
		
			<div id="pane1" class="panes">
				<form:form name="formpost" id="formpost" commandName="cmd">
					<spring:hasBindErrors name="cmd">
						<div id="error">
							<strong>Data yang dimasukkan tidak lengkap. Mohon lengkapi data-data berikut:</strong>
							<br />
							<form:errors path="*" delimiter="<br>" />
						</div>
					</spring:hasBindErrors>

					<fieldset style="text-align: left;">
						<legend>Produk Link</legend>
						<table class="entry2" style="width: auto;">
							<tr>
								<th>SPAJ</th>
								<th>Polis</th>
								<th>Pemegang</th>
								<th>Produk</th>
								<th>User Input</th>
							</tr>
							<tr>
								<th><input type="text" class="readOnly" size="16" readOnly="true" value="${cmd.powersave.reg_spaj}"></th>
								<th><input type="text" class="readOnly" size="22" readOnly="true" value="${cmd.powersave.mspo_policy_no_format}"></th>
								<th><input type="text" class="readOnly" size="40" readOnly="true" value="${cmd.powersave.mcl_first}"></th>
								<th><input type="text" class="readOnly" size="40" readOnly="true" value="${cmd.powersave.lsdbs_name}"></th>
								<th><input type="text" class="readOnly" size="30" readOnly="true" value="${cmd.powersave.lus_login_name}"></th>
							</tr>
						</table>
					</fieldset>
					<c:forEach items="${cmd.daftarPbp}" var="d" varStatus="s">
						<fieldset>
							<legend>Produk Save Ke-${s.count+1}</legend>
							<table class="entry2">
								<tr>
									<th style="width: 100px;">SPAJ</th>
									<th style="width: 120px;">Polis</th>
									<th>Pemegang</th>
									<th>Produk</th>
									<th>Tgl Input SPAJ</th>
									<th>User</th>
								</tr>
								<tr>
									<td style="text-align: center;">
										<form:input size="16" path="daftarPbp[${s.index}].reg_spaj"/>
									</td>
									<td>${d.powerSave.mspo_policy_no_format}</td>
									<td>${d.powerSave.mcl_first}</td>
									<td>${d.powerSave.lsdbs_name}</td>
									<td><fmt:formatDate value="${d.powerSave.mps_input_date}" pattern="dd/MM/yyyy (HH:mm)"/></td>
									<td>[${d.powerSave.lus_id}] ${d.powerSave.lus_login_name}</td>
								</tr>
								<tr>
									<th>Beg Date</th>
									<th>Rollover</th>
									<th>MGI</th>
									<th>Rate</th>
									<th>Premi</th>
									<th>Bunga</th>
								</tr>
								<tr>
									<td><fmt:formatDate value="${d.powerSave.mps_deposit_date}" pattern="dd/MM/yyyy"/></td>
									<td>
										<c:forEach items="${select_rollover}" var="s">
											<c:if test="${d.powerSave.mps_roll_over eq s.ID}">${s.ROLLOVER}</c:if>
										</c:forEach>
									</td>
									<td>${d.powerSave.mps_jangka_inv}</td>
									<td><fmt:formatNumber value="${d.powerSave.mps_rate}"/>%</td>
									<td><fmt:formatNumber value="${d.powerSave.mps_prm_deposit}"/></td>
									<td><fmt:formatNumber value="${d.powerSave.mps_prm_interest}"/></td>
								</tr>
							</table>
						</fieldset>
					</c:forEach>
					<input type="submit" name="save" value="Simpan" onclick="return confirm('Simpan?');">
					<input type="submit" name="transfer" value="Transfer" onclick="return confirm('Transfer?');">
				</form:form>
			</div>
			
		</div>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>