<%@ include file="/include/page/header.jsp"%>
<script>
	
	function cek(spaj){
		if(spaj == ''){
			alert('Harap pilih SPAJ terlebih dahulu!');
			return false;
		}else{
			if(spaj != ''){
				document.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+spaj;
			}
			return true;
		}
	}
	
	function buttonLink(b,spaj){
		if(b=='u'){
			if(cek(spaj))
			if(confirm("Anda Yakin ingin Update Status Polis ini ("+spaj+") Menjadi ACCEPTED?")){
				desc=prompt("Masukan Keterangan Untuk Akseptasi Khusus..","");
				if(desc == null){
				alert("Update Status dibatalkan");
				}else if(desc != ''){
					document.getElementById('infoFrame').src='${path }/uw/uw.htm?window=update_akseptasi&spaj='+spaj+'&desc='+desc;
				}else{
					alert("Update Status dibatalkan");
				}
			}
			
		}else if(b=='r'){
			document.getElementById('infoFrame').src='${path }/uw/uw.htm?window=report_akseptasi_khusus';
		}else if(b=='k'){
			document.getElementById('infoFrame').src='${path }/report/uw.htm?window=akseptasiPendingKomisi';
		}else if(b=='f'){
			document.getElementById('infoFrame').src='${path }/report/uw.htm?window=reportFurtherRequirement';
		}else if(b=='p'){
			document.getElementById('infoFrame').src='${path }/report/uw.htm?window=postponed';
		}else if(b=='a'){
			document.getElementById('infoFrame').src='${path }/report/uw.htm?window=reportFurtherRBanca';
		}else if(b=='e'){
		    if (cek(spaj))
		   // document.getElementById('infoFrame').src='${path}/uw/uw.htm?window=editblanko&spaj='+spaj;
		    //document.getElementById('infoFrame').src='${path}/uw/endorseNonMaterial.htm?spaj='+spaj;
		    document.getElementById('infoFrame').src='${path}/uw/endorsenonmaterial.htm?window=endorseNonMaterial&spaj='+spaj;
		}else if(b=='d'){
			document.getElementById('infoFrame').src='${path }/report/uw.htm?window=reportDecline';
		}else if(b=='c'){
			if(cek(spaj))
			document.getElementById('infoFrame').src='${path}/checklist.htm?reg_spaj='+spaj;
		}else if(b=='s') {
			if(cek(spaj))
			document.getElementById('infoFrame').src='${path}/uw/status.htm?spaj='+spaj;
		}else if(b=='call') {
			if(cek(spaj))popWin('${path}/uw/viewer.htm?window=csfcall&spaj='+spaj, 480, 640); 
		}else if(b=='summary') {
			if(cek(spaj))popWin('${path}/report/bas.htm?window=csf_summary&spaj='+spaj, 700, 900); 
		}else if(b=='agen'){
		    if(cek(spaj))popWin('${path}/bac/editagenpenutup.htm?spaj='+spaj, 360, 480);
		}else if(b=='i'){
			if(cek(spaj))
			if(document.getElementById('infoFrame')){
				document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj;
				document.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+spaj;
			}
		}else if(b=='y') {
			if(cek(spaj))
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=uwinfo&spaj='+spaj;
		}else if(b=='akum') {
			if(cek(spaj))
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=akum_new';
		}else if(b=='en') {
			if(cek(spaj))
			document.getElementById('infoFrame').src='${path}/uw/akseptasi_endorsement_controller.htm?window=aksependors&spaj='+spaj;
		}else if(b=='claim'){
			var spaj=document.formpost.spaj.value;
			popWin('${path}/uw/uw.htm?window=healthClaim&cab=true&regspajClaimDetail='+spaj+'&spaj='+spaj, 400, 900,'yes','yes','yes');
		}else if(b=='upload') {
			if(cek(spaj))
			document.getElementById('infoFrame').src='${path}/uw/upload_nb.htm?reg_spaj='+spaj;
		}else if(b=='list_rk'){
			document.getElementById('infoFrame').src= '${path}/uw/uw.htm?window=drek&piyu=1&startDate=${startDate}&endDate=${endDate}';
		// 		hattori2
		}else if(b=='print'){
			if(cek(spaj))
				if(spaj == ''){
				}else{
				popWin('${path}/report/uw.pdf?window=permohonanaksep&show=pdf&spaj='+spaj);
				}
		}else if(b=='rem'){
			if(cek(spaj))
				if(spaj == ''){
				}else{
					popWin('${path}/report/uw.htm?window=reportEndorsmentMaterial&spaj='+spaj);
				}
		}else if(b=='tolak_med'){
			if(cek(spaj))
				if(spaj == ''){
				}else{
				  
					//popWin('${path}/uw/uw.htm?window=penolakan_medical&spaj='+spaj);
					popWin('${path}/report/uw.htm?window=penolakan_medical&spaj='+spaj);
				}
		}else if(b=='back_to_lb') {
			document.getElementById('infoFrame').src='${path}/uw/uw.htm?window=back_to_lb&flag=2';
		}else if(b=='blacklist'){
			document.getElementById('infoFrame').src='${path}/uw/blacklist.htm';
		}
	}
	
	function awal(){
		setFrameSize('infoFrame', 45);
		setFrameSize('docFrame', 45);
	}
	 //onload="awal();"
	
</script>
<body onload="setFrameSize('infoFrame', 90); setFrameSize('docFrame', 90);" 
	onresize="setFrameSize('infoFrame', 90); setFrameSize('docFrame', 90);" style="height: 100%;">
	
	<form name="formpost" method="post">
		<div class="tabcontent">
			<table class="entry2" style="width: 98%;">
				<tr>
					<th>SPAJ</th>
					<td>
						<input type="hidden" name="koderegion">
						<input type="hidden" name="kodebisnis">
						<input type="hidden" name="numberbisnis">
						<input type="hidden" name="jml_peserta">
						<!--
						<select name="spaj" onchange="ajaxPesan(this.value, 6); tampilkan('info', this.value);">
						-->
						<select name="spaj" id="spaj" onChange="cariregion(this.options[this.selectedIndex].value,'region');">
							<option value="">[--- Silahkan Pilih/Cari SPAJ ---]</option>
							<c:forEach var="s" items="${daftarSPAJ}">
								<option value="${s.REG_SPAJ }"
									style="background-color: ${s.BG}"
									<c:if test="${s.REG_SPAJ eq param.spaj }">selected<c:set var="ada" value="ada" /></c:if>>
									${s.SPAJ_FORMATTED} - ${s.POLICY_FORMATTED }
								</option>
							</c:forEach>
						</select>

						<input type="button" value="Search SPAJ" name="search"
							onclick="popWin('${path}/uw/spaj_aksependorse.htm?posisi=-1&msenaksep=1', 350, 150); "
							accesskey="C"
							onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);"
							onmouseout="nd();">
						<input type="button" value="View Document" name="info"
							accesskey="I"
							onmouseover="return overlib('Alt-I', AUTOSTATUS, WRAP);"
							onmouseout="nd();"
							onclick="buttonLink('i',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
					</td>
				</tr>
				<tr>
					<th>Summary</th>
					<td>
						<input type="hidden" name="koderegion">
						<input type="hidden" name="koderegion">
						<input type="hidden" name="koderegion">
						<input type="hidden" name="koderegion">
						
						<input type="button" value="U/W Info" name="uwinfo" 
							onclick="buttonLink('y',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
						
						<input type="button" value="Akum New" name="akum"
							onclick="buttonLink('akum',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">						
						
						<input type="button" value="View Kesehatan" name="claim" 
							onclick="buttonLink('claim',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);" style="width: 120px;">					
					</td>
				</tr>
				<tr>
					<th>Report</th>
					<td>
						<input type="button" value="Permohonan Underwriting Worksheet Endorsement" name="print"	
          					onclick="buttonLink('print',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);" style="width: 360px;">
          				
          				<input type="button" value="Endorsment Material" name="rem"	
          					onclick="buttonLink('rem',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);" style="width: 170px;"> 
          					<input type="button" value="Print Surat Penolakan SMILE Medical" name="rem"	
          					onclick="buttonLink('tolak_med',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);" style="width: 360px;">       
					</td>
				</tr>
				<tr>
					<th>Process</th>
					<td>
						<input type="button" value="Akseptasi Endorsment" name="endorsement"
							onclick="buttonLink('en',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">	
						
						<input type="button" value="Back To Life Benefit" name="back_to_lb" id = "back_to_lb"
							onclick="buttonLink('back_to_lb',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">		

						<input type="button" value="Attention List" name="blacklist"
							onclick="buttonLink('blacklist', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);" >
					</td>
				</tr>
				<tr>
					<td colspan="2">
					<table style="margin: 0 0 0 0; padding: 0 0 0 0; width: 100%;">
						<tr>
					
					<c:choose>
						<c:when test="${not empty wideScreen}">
							<td style="width: 60%;">
								<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
									width="100%" height="100%">
									Please Wait...
								</iframe>
							</td>
							<td style="width: 40%;">
								<iframe src="" name="docFrame" id="docFrame" width="100%">E-Lions</iframe>
							</td>
						</c:when>
						<c:otherwise>
							<td>
								<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
									width="100%" height="100%">
									Please Wait...
								</iframe>
							</td>
						</c:otherwise>
					</c:choose>
						</tr>
					</table>
					</td>
				</tr>
			</table>
		</div>
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>