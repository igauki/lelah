<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript" src="${path}/include/js/jquery-1.3.2.min.js"></script>
<script>
$(document).ready(function() {
        <c:if test="${not empty pesanError}">
            alert('${pesanError}');
        </c:if>
    });
</script>
<c:choose>    
    <c:when test="${not empty hak}">
        <div id="error">
            Maaf, No SPAJ ini sudah proses REAS, tidak bisa diedit.
        </div>
    </c:when>
    <c:otherwise>
   <body onload="document.title='PopUp :: Edit Polis';setupPanes('container1','tab1');" style="height: 100%;">
     <div class="tab-container" id="container1">
              <ul class="tabs">
                <li>
                    <a href="#" onClick="return showPane('pane1', this)" id="tab1">Edit Polis</a>
                </li>
            </ul>
            <div class="tab-panes">
                <div id="pane1" class="panes">
                     <form id="formpost" name="formpost" method="post">
                    <fieldset>
                      <legend>Info User</legend>
                         <table class="entry2">
                   <tr>
                       <th nowrap="nowrap">User</th><th align ="left">&nbsp;[${infoDetailUser.LUS_ID}]&nbsp;&nbsp;${infoDetailUser.LUS_LOGIN_NAME}</th>
                  </tr>
                  <tr>
                       <th nowrap="nowrap">Nama<br></th><th align ="left">&nbsp;${infoDetailUser.LUS_FULL_NAME}&nbsp;[${infoDetailUser.LDE_DEPT}]</th>
                 </tr>
                    </table>
                         </fieldset>
         <fieldset>
         <legend>
			<c:if test="${pemegang.lsre_id ne 1}" >	
					Edit data pemegang
			</c:if>	
			<c:if test="${pemegang.lsre_id eq 1}" >	
			Edit data pemegang dan tertanggung
		</c:if>	
	   </legend>
                <table class="entry2">
               <tr>
				<th nowrap="nowrap">Perkiraan Penghasilan Kotor Per Tahun</th>
				<th nowrap="nowrap" class="left" colspan="2">
					<select name = "penghasilan" width: 200px;">
						<c:forEach var="penghasilan" items="${select_penghasilan}">
						<option
							<c:if test="${pemegang.mkl_penghasilan eq penghasilan.ID}"> SELECTED </c:if>
							value="${penghasilan.ID}">${penghasilan.PENGHASILAN}</option>
					</c:forEach>
				</select>
			</th>
		</tr>
		<tr>
			<th nowrap="nowrap">Nama Ibu Kandung</th>
             <th colspan = "2" nowrap = "nowrap" align ="left"><input name="mamah" type=text value="${pemegang.mspe_mother}"/></th>
          </tr>
                     </table>
               </fieldset>
               <c:if test="${pemegang.lsre_id ne 1}" >	
               <fieldset>
					<legend>Edit data Tertanggung</legend>
						<table class ="entry2">
						  <tr>
							<th nowrap="nowrap">Perkiraan Penghasilan Kotor Per Tahun</th>
							<th nowrap="nowrap" class="left" colspan="2">
							<select name = "penghasilanT" width: 200px;">
								<c:forEach var="penghasilan" items="${select_penghasilan}">
								<option
								<c:if test="${tertanggung.mkl_penghasilan eq penghasilan.ID}"> SELECTED </c:if>
								value="${penghasilan.ID}">${penghasilan.PENGHASILAN}</option>
						</c:forEach>
					</select>
				</th>
		</tr>
		<tr>
			<th nowrap="nowrap">Nama Ibu Kandung</th>
             <th colspan = "2" nowrap = "nowrap" align ="left"><input name="mamahT" type=text value="${tertanggung.mspe_mother}"/></th>
          </tr>
				</table>
				</fieldset>
               </c:if>
               <tr><td></td></tr>
                <tr align = "left">
                        <th> 
                            <input type="hidden" name="save" id="save">  
                            <input type="submit" name="save" value="Save" onclick="return confirm('Apakah Anda Yakin Untuk Melanjutkan Proses ?');"> 
                           </th>
                         </tr>
                         <tr>
                            <td><font color="#ff0000">* Proses ini akan masuk ke ulangan(History)</font></td>
                        </tr>
        </div>
      </div>
   </div>
    </c:otherwise>
 </c:choose>
</body>
<%@ include file="/include/page/footer.jsp"%>