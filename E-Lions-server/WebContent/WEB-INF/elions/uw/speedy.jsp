<%@ include file="/include/page/header.jsp"%>

<script>

	function showPage(hal){
		var spaj = document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value;
		var lsbs = document.formpost.kodebisnis.value;
		var lsdbs = document.formpost.numberbisnis.value;
		var copy_reg_spaj = document.formpost.copy_reg_spaj.value;
		if(spaj == ''){
			alert('Harap pilih SPAJ terlebih dahulu!');
		}else{
			createLoadingMessage();
			if('tampil' == hal){
				document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj+'&halForBlacklist=uw';
			}else if('edit' == hal){
				document.getElementById('infoFrame').src='${path}/bac/edit.htm?showSPAJ='+spaj;
			}else if('editagen' == hal){
				document.getElementById('infoFrame').src='${path}/bac/editagenpenutup.htm?spaj='+spaj;			
			}else if('uwinfo' == hal){
				document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=uwinfo&spaj='+spaj;			
			}else if('reffbii' == hal){
				popWin('${path}/bac/reff_bank.htm?window=main&spaj='+spaj, 400, 700);
 			}else if('upload_nb' == hal){
 				document.getElementById('infoFrame').src='${path}/uw/upload_nb.htm?reg_spaj='+document.formpost.spaj.value;
			}else if('speedy' == hal){
				var owner = check('owner_sign');
				var parent = check('parent_sign');
				var agent = check('agent_sign');
				var reviewed = check('reviewed');
				
				if(owner==null){
					alert("Harap isi Owner's & Insured's Signature");
				}else if(agent==null){
					alert("Harap isi Agent's Signature");
				}else if(reviewed==null){
					alert("Harap isi Reviewed by User");
				}else{
					document.getElementById('infoFrame').src='${path}/uw/uw.htm?window=prosesSpeedy&spaj='+spaj+'&owner='+owner+'&parent='+parent+'&agent='+agent+'&reviewed='+reviewed;
				}
			}
		}
	}
	
	function cariregion(spaj,nama){
		if(spaj != ''){
			if(document.getElementById('docFrame')) document.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+spaj;
			if(document.getElementById('infoFrame')) document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj+'&halForBlacklist=uw';
			ajaxManager.listcollect(spaj , nama,
			{callback:function(map) {
				DWRUtil.useLoadingMessage();
				document.formpost.koderegion.value=map.LSRG_NAMA;
				document.formpost.kodebisnis.value = map.LSBS_ID;
				document.formpost.numberbisnis.value = map.LSDBS_NUMBER;
				document.formpost.jml_peserta.value = map.jml_peserta;
			   },
			  timeout:15000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
			});
			
		}	
	}
	
	function awal(){
		if('${snow_spaj}'!=''){
			document.formpost.spaj.value='${snow_spaj}';
			document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value='${snow_spaj}';
			cariregion('${snow_spaj}','region');
		}else{
			cariregion(document.formpost.spaj.value,'region');
		}
		setFrameSize('infoFrame', 90);
		setFrameSize('docFrame', 90);
	}
	
	function copyToClipboard(txt){
	
		if( window.clipboardData && clipboardData.setData ){
		
			clipboardData.setData("Text", txt);
		}
		else{
		 alert("Internet Explorer required"); 
		}
	}
	
	function run(){
		var shell = new ActiveXObject("WScript.Shell"); 
		var commandtoRun = "\\\\aplikasi\\lions\\secan4400f.exe";
	   
		if (shell){		
		 shell.run(commandtoRun); 
		} 
		else{ 
			alert("program or file doesn't exist on your system."); 
		}
	}
	
	function copyAndRun(){ 
	 	txt = document.getElementById("spaj").value;
	 	copyToClipboard(txt);
	 	run();		
	}
	
	function check(name){
	    var res = null;
	    var currObj = document.formpost[name];
	    
	    for (var i=0;i<currObj.length;i++) {
			if (currObj[i].checked) {
				res = currObj[i].value;
			}
		}
	    
	    return res;
	}

</script>
<body  onload="awal();" onresize="setFrameSize('infoFrame', 90); setFrameSize('docFrame', 90);" style="height: 100%;">
<form name="formpost" method="post">
<div class="tabcontent">
<table class="entry2" style="width: 98%;">
	<tr>
		<th>Cari SPAJ</th>
		<td> <input type="hidden" name="halForBlacklist" value="uw"> 
		  <input type="hidden" name="koderegion" > 
          <input type="hidden" name="kodebisnis">
          <input type="hidden" name="numberbisnis">
          <input type="hidden" name="jml_peserta">
          <input type="hidden" name="copy_reg_spaj" value="" />
			<select name="spaj" id="spaj" onChange="cariregion(this.options[this.selectedIndex].value,'region');">
				<option value="">[--- Silahkan Pilih/Cari SPAJ ---]</option>
				<c:forEach var="s" items="${daftarSPAJ}">
					<option value="${s.REG_SPAJ }" style="background-color: ${s.BG};" 
						<c:if test="${s.REG_SPAJ eq param.spaj }">selected<c:set var="ada" value="ada" /></c:if>>${s.SPAJ_FORMATTED}
					- ${s.POLICY_FORMATTED }</option>
				</c:forEach>
			</select>
			
			<input type="button" value="Info" name="info" 				onclick="showPage('tampil');" accesskey="I" onmouseover="return overlib('Alt-I', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Cari" name="search" 			onclick="popWin('${path}/uw/spaj.htm?posisi=2&win=uw', 600, 800); " accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Edit" name="info" 				onclick="showPage('edit');" accesskey="J" onmouseover="return overlib('Alt-J', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Edit Agen" name="agen" 			onclick="showPage('editagen');" accesskey="F">
			<input type="button" value="U/W Info" name="uwinfo" 		onclick="showPage('uwinfo');" accesskey="W" onmouseover="return overlib('Alt-W', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Reff Bank" name="reffbiiiiiii"	onclick="showPage('reffbii');">
			<input type="button" value="Scan" name="copySpaj"			onclick="copyAndRun();">
			<input type="button" value="List Powersave By Name" 		onclick="document.getElementById('infoFrame').src='${path}/report/uw.htm?window=list_polis_powersave';" name="list_powersave">
			<input type="button" value="Upload Scan" name="upload_nb"	onclick="showPage('upload_nb');">	
		</td>
	</tr>
	<tr>
		<th>Speedy</th>
		<td>
			<table class="entry2" style="width: 30%;">
				<tr>
					<th>Owner's & Insured's Signature</th>
					<td>
						<input name="owner_sign" id="owner_sign_yes" type="radio" class="radio" value="1" style="border: none;" >Ada &nbsp;	   
	    				<input name="owner_sign" id="owner_sign_no" type="radio" class="radio" value="0" style="border: none;" >Tidak
					</td>
				</tr>
				<tr>
					<th>Parent's Signature if Insured age < 17 yo</th>
					<td>
						<input name="parent_sign" id="parent_sign_yes" type="radio" class="radio" value="1" style="border: none;" >Ada &nbsp;	   
	    				<input name="parent_sign" id="parent_sign_no" type="radio" class="radio" value="0" style="border: none;" >Tidak
					</td>
				</tr>
				<tr>
					<th>Agent's Signature</th>
					<td>
						<input name="agent_sign" id="agent_sign_yes" type="radio" class="radio" value="1" style="border: none;" >Ada &nbsp;	   
	    				<input name="agent_sign" id="agent_sign_no" type="radio" class="radio" value="0" style="border: none;" >Tidak
					</td>
				</tr>
				<tr>
					<th>Reviewed by User</th>
					<td>
						<input name="reviewed" id="reviewed_yes" type="radio" class="radio" value="1" style="border: none;" >Ya &nbsp;	   
	    				<input name="reviewed" id="reviewed_no" type="radio" class="radio" value="0" style="border: none;" >Tidak
					</td>
				</tr>
				<tr>
					<td colspan="2"><input type="button" value="Speedy" name="speedy" onclick="showPage('speedy');"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table style="margin: 0 0 0 0; padding: 0 0 0 0; width: 100%;">
				<tr>
					<c:choose>
						<c:when test="${sessionScope.currentUser.wideScreen}">
							<td style="width: 60%;">
								<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
									width="100%" height="100%"> Please Wait... </iframe>
							</td>
							<td style="width: 40%;">
								<iframe src="" name="docFrame" id="docFrame" width="100%">E-Lions</iframe>
							</td>
						</c:when>
						<c:otherwise>
							<td>
								<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
									width="100%"  height="100%"> Please Wait... </iframe>
							</td>
						</c:otherwise>
					</c:choose>
				</tr>
			</table>		
		</td>	
	</tr>
</table>
</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>