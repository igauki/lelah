<%@ include file="/include/page/header.jsp"%>
<body>
	<form name="form1" method="post">
	 <div id="pane1" class="tabcontent">
	   <fieldset>
	   <legend>${data } Detail</legend>
	   <table class="entry">
		   <tr>
		   		<th>Nama Lengkap</th>
		   		<td>:</td>
		   		<td width=250>${clientNew.mcl_first }</td>
		 		<th>Pekerjaan </th>
		 		<td>:</td>
				<td>${clientNew.mpn_job_desc }</td>
		   </tr>
		   <tr>
		   		<th>Nama Ibu Kandung</th>
		   		<td>:</td>
		   		<td width=250>${clientNew.mspe_mother }</td>
		   		<th>Jenis ID</th>
		   		<td>:</td>
		   		<td width=30>
		   			<c:forEach var="s" items="${lsJnsId}" varStatus="xt">
						<c:if test="${s.LSIDE_ID eq clientNew.lside_id }">${s.LSIDE_NAME}</c:if>
		   			</c:forEach>
		   		</td>
			</tr>
			<tr>
		   		<th>Kewarganegaraan</th>
		   		<td>:</td>
		   		<td>
		   			<c:forEach var="s" items="${lsWargaNegara}" varStatus="xt">
						<c:if test="${s.LSNE_ID eq clientNew.lsne_id }">${s.LSNE_NOTE}</c:if>
		   			</c:forEach>
		   		</td>
		   		<th>Nomor KTP</th>
		   		<td>:</td>
		   		<td>${clientNew.mspe_no_identity }</td>
		   	</tr>
		   <tr>
		   		<th>Tempat Lahir</th>
		   		<td>:</td>
		   		<td>${clientNew.mspe_place_birth}</td>
		   		<th>Tanggal Lahir (dd-mm-yy)</th>
		   		<td>:</td>
		   		<td>
		   			<fmt:formatDate value="${clientNew.mspe_date_birth }" pattern="dd-MM-yyyy"/>
		   		</td>
	   			<c:if test="${umurBeasiswa ne null }">
			   		<th>Usia Beasiswa</th>
			   		<td>:</td>
			   		<td>${umurBeasiswa}</td>
   				</c:if>
		   	
		   	</tr>
		   
		   <tr>
		   		<th>Jenis Kelamin</th>
		   		<td>:</td>
		   		<td>
		   			<c:forEach var="s" items="${lsSex}" varStatus="xt">
						<c:if test="${s.ID eq clientNew.mspe_sex }">${s.VALUE}</c:if>
		   			</c:forEach>
		   		</td>
		   		<th>Status</th>
		   		<td>:</td>
		   		<td>
		   			<c:forEach var="s" items="${lsStsMrt}" varStatus="xt">
						<c:if test="${s.ID eq clientNew.mspe_sts_mrt }">${s.VALUE}</c:if>
		   			</c:forEach>
		   		</td>
		   	</tr>
		   
		   <tr>
		   		<th>Agama</th>
		   		<td>:</td>
		   		<td>
		   			<c:forEach var="s" items="${lsAgama}" varStatus="xt">
						<c:if test="${s.LSAG_ID eq clientNew.lsag_id }">${s.LSAG_NAME}</c:if>
		   			</c:forEach>
		   		</td>
		   		<th>Pendidikan</th>
		   		<td>:</td>
		   		<td>
		   			<c:forEach var="s" items="${lsPendidikan}" varStatus="xt">
						<c:if test="${s.LSED_ID eq clientNew.lsed_id }">${s.LSED_NAME}</c:if>
		   			</c:forEach>
		   		</td>
		   		
		   	</tr>
		   
		   <tr>
		   		<th>Alamat Rumah</th>
		   		<td>:</td>
		   		<td>${addressNew.alamat_rumah}</td>
		   		<th>Kota</th>
		   		<td>:</td>
		   		<td>${addressNew.kota_rumah}</td>
		   		<th>No Telepon</th>
		   		<td>:</td>
		   		<td width="100">(${addressNew.area_code_rumah}) ${addressNew.telpon_rumah}</td>
		   		<th>Kode Pos</th>
		   		<td>:</td>
		   		<td>${addressNew.kd_pos_rumah}</td>
		   	</tr>
		   <tr><td colspan="6" bgcolor="0099FF"></td></tr>	   
		   <tr>
		   		<th>Alamat Kantor</th>
		   		<td>:</td>
		   		<td>${addressNew.alamat_kantor}</td>
		   		<th>Kota</th>
		   		<td>:</td>
		   		<td>${addressNew.kota_kantor}</td>
		   		<th>No Telepon</th>
		   		<td>:</td>
		   		<td width="100">(${addressNew.area_code_kantor}) ${addressNew.telpon_kantor}</td>
		   		<th>Kode Pos</th>
		   		<td>:</td>
		   		<td>${addressNew.kd_pos_kantor}</td>
		   	</tr>
	   		<tr>
		   		<td colspan="6"></td>
		   		<th>Email</th>
		   		<td>:</td>
		   		<td>${addressNew.email}</td>
		   		<th>No HandPhone</th>
		   		<td>:</td>
		   		<td>${addressNew.no_hp}</td>
	   		</tr>
		 	<tr>
		   		<td colspan="11"></td>
		   		<td></td>
		 	</tr>
		 	<tr>
		 		<th>Tujuan</th>
		 		<td>:</td>
				<td>${clientNew.mkl_tujuan}</td>
		 		<th>Penghasilan</th>
		 		<td>:</td>
				<td >${clientNew.mkl_penghasilan}</td>
		 	</tr>
			<tr>
				<th>Pendanaan</th>
		 		<td>:</td>
				<td >${clientNew.mkl_pendanaan}</td>
		 		<th>Industri</th>
		 		<td>:</td>
				<td >${clientNew.mkl_industri}</td>

			</tr>
			<c:if test="${relasi ne null }">
				<tr>
					<th>Hubungan Dengan Calon Pemegang Polis</th>
					<td>:</td>
					<td>${relasi}</td>
			  	</tr>
		  	</c:if>
		   	
	   </table>
	  </fieldset>
    <table width="100%">
			<tr>
        		<td width="100%">
				<div align="center">
            		<input type="Button" name="btnClose" value="Close" title="Close" onClick="javascript:window.close();">
          		</div>
          		</td>
          	</tr>
	  </table>
	  </div>
	</form>
</body>

<%@ include file="/include/page/footer.jsp"%>