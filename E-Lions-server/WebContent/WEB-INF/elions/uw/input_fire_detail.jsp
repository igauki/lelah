<%@ include file="/include/page/header.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<%User currentUser = (User) session.getAttribute("currentUser");%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>

<script type="text/javascript">
	hideLoadingMessage();
	
	function agen_cek()
	{
		if (document.frmParam.elements['msag_id'].value != "")
		{
			listdataagen2(document.frmParam.elements['msag_id'].value);
		}
	}
	
	function pin_cek()
	{
		if (document.frmParam.elements['pin'].value != "")
		{
			//alert(document.frmParam.elements['pin'].value);
			cekPin(document.frmParam.elements['pin'].value);
		}
	}
	
	function update(){
		if(trim(document.frmParam.kata.value)=='') return false;
		else createLoadingMessage();	
	}
	
	function cekElseEnvir(){
		if(document.getElementById('msp_fire_insured_addr_envir').value == '5'){
			document.getElementById('msp_fire_ins_addr_envir_else').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_ins_addr_envir_else').style.visibility = "hidden";
		}
	}
	
	function cekElseSourceFund(){
		if(document.getElementById('msp_fire_source_fund2').value == 'LAINNYA'){
			document.getElementById('msp_fire_source_fund').style.visibility = "visible";
			document.getElementById('msp_fire_source_fund').value='';
		}else{
			document.getElementById('msp_fire_source_fund').style.visibility = "hidden";
		}
	}
	
	function cekElseBusiness(){
		if(document.getElementById('msp_fire_type_business2').value == 'LAINNYA'){
			document.getElementById('msp_fire_type_business').style.visibility = "visible";
			document.getElementById('msp_fire_type_business').value='';
		}else{
			document.getElementById('msp_fire_type_business').style.visibility = "hidden";
		}
	}
	
	function cekElseOccupation(){
		if(document.getElementById('msp_fire_occupation2').value == 'LAIN-LAIN'){
			document.getElementById('msp_fire_occupation').style.visibility = "visible";
			document.getElementById('msp_fire_occupation').value='';
		}else{
			document.getElementById('msp_fire_occupation').style.visibility = "hidden";
		}
	}
	
	function cekElseTrio(){
		if(document.getElementById('msp_fire_source_fund2').value == 'LAINNYA'){
			document.getElementById('msp_fire_source_fund').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_source_fund').style.visibility = "hidden";
		}
		if(document.getElementById('msp_fire_type_business2').value == 'LAINNYA'){
			document.getElementById('msp_fire_type_business').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_type_business').style.visibility = "hidden";
		}
		if(document.getElementById('msp_fire_occupation2').value == 'LAIN-LAIN'){
			document.getElementById('msp_fire_occupation').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_occupation').style.visibility = "hidden";
		}
	}
	
	function cekElseOkupasi(){
		if(document.getElementById('msp_fire_okupasi').value == 'L'){
			document.getElementById('msp_fire_okupasi_else').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_okupasi_else').style.visibility = "hidden";
		}
	}
	
	function dofo(index, flag) {
		if(flag==1){
			var a="rekening["+(index+1)+"]";
			var b="rekening["+(index)+"]";		
			document.frmParam.elements[a].focus();
			
		}else{
			var a="rekening["+(index+1)+"]";		
			document.frmParam.elements[a].focus();
		}
	}
	
	function doRek() {
		document.getElementById('msp_no_rekening').value = '';
		for(i = 0 ; i < 21 ; i++){
			document.getElementById('msp_no_rekening').value = document.getElementById('msp_no_rekening').value + document.frmParam.elements["rekening["+i+"]"].value;
		}
	}
	
	function doPp() {
		if(document.getElementById('msp_rek_nama').value == ''){
			document.getElementById('msp_rek_nama').value = document.getElementById('nama_pp').value;
		}
		document.getElementById('msp_pas_nama_pp').value = document.getElementById('nama_pp').value;
		
	}
	
	function inPp() {
		document.getElementById('nama_pp').value = document.getElementById('msp_pas_nama_pp').value;
	}
	
	function inRek() {
		var rek = document.getElementById('msp_no_rekening').value
		var size = rek.length;
		for(i = 0 ; i < size ; i++){
			document.frmParam.elements["rekening["+i+"]"].value = rek.charAt(i);
		}
	}
	
	function doPremi(){
		var lsdbs_number = document.getElementById('produk').value;
		var lscb_id = document.getElementById('lscb_id').value;
		var prm = 0;
		
		if(lsdbs_number == '1' && lscb_id == 3){
				document.getElementById('premi').value = '150000';
			}else if(lsdbs_number == '1' && lscb_id == 6){
				document.getElementById('premi').value = '15000';
			}else if(lsdbs_number == '1' && lscb_id == 1){
				prm = 150000 * 0.27;
				document.getElementById('premi').value = prm;
			}else if(lsdbs_number == '1' && lscb_id == 2){
				prm = 150000 * 0.52;
				document.getElementById('premi').value = prm;
			}else if(lsdbs_number == '2' && lscb_id == 3){
				document.getElementById('premi').value = '300000';
			}else if(lsdbs_number == '2' && lscb_id == 6){
				document.getElementById('premi').value = '30000';
			}else if(lsdbs_number == '2' && lscb_id == 1){
				prm = 300000 * 0.27;
				document.getElementById('premi').value = prm;
			}else if(lsdbs_number == '2' && lscb_id == 2){
				prm = 300000 * 0.52;
				document.getElementById('premi').value = prm;
			}else if(lsdbs_number == '3' && lscb_id == 3){
				document.getElementById('premi').value = '500000';
			}else if(lsdbs_number == '3' && lscb_id == 6){
				document.getElementById('premi').value = '50000';
			}else if(lsdbs_number == '3' && lscb_id == 1){
				prm = 500000 * 0.27;
				document.getElementById('premi').value = prm;
			}else if(lsdbs_number == '3' && lscb_id == 2){
				prm = 500000 * 0.52;
				document.getElementById('premi').value = prm;
			}else if(lsdbs_number == '4' && lscb_id == 3){
				document.getElementById('premi').value = '900000';
			}else if(lsdbs_number == '4' && lscb_id == 6){
				document.getElementById('premi').value = '90000';
			}else if(lsdbs_number == '4' && lscb_id == 1){
				prm = 900000 * 0.27;
				document.getElementById('premi').value = prm;
			}else if(lsdbs_number == '4' && lscb_id == 2){
				prm = 900000 * 0.52;
				document.getElementById('premi').value = prm;
			}else{
				document.getElementById('premi').value = '0';
			}
	} 
	
	function bodyOnLoad(){
		cekElseEnvir();
		cekElseOkupasi();
		cekElseTrio();
		inRek();
		inPp();
		doPremi();
		agen_cek();
	}
	
</script>

</head>

<body onload="resizeCenter(750,600);bodyOnLoad();" style="height: 100%;">
<XML ID=xmlData></XML>
<div class="tab-container" id="container1">

		<div class="tab-panes">

			<div id="pane1" class="panes">
<form:form commandName="cmd" name="frmParam" id="frmParam" method="post">
						<fieldset>
	   <legend>ASURANSI KEBAKARAN</legend>
	   <table class="entry2">
	   <tr><td colspan="4">
					   <spring:bind path="cmd.*">
			<c:if test="${not empty status.errorMessages}">
				<div id="error">ERROR:<br>
				<c:forEach var="error" items="${status.errorMessages}">
	- <c:out value="${error}" escapeXml="false" />
					<br />
				</c:forEach></div>
			</c:if>
		</spring:bind>	</td></tr>
	   					<tr>
							<th colspan="4">INFORMASI UTAMA</th>
						</tr>
						<tr>
							<th align="left">Nama Tertanggung:</th>
							<td colspan="3">
								<select name="msp_fire_code_name">
									<c:forEach var="kn" items="${kode_nama_list}"> 
										<option <c:if test="${cmd.msp_fire_code_name eq kn.ID}"> SELECTED </c:if> value="${kn.ID}">${kn.NAMA}
										</option>
									</c:forEach> 
								</select>
								<form:input path="msp_fire_name" size="50" cssErrorClass="inpError"/><font class="error">*</font>
							</td>
						</tr>
						<tr>
							<th align="left">Nomor KTP:</th>
							<td colspan="3"><form:input path="msp_fire_identity" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th align="left">Tanggal Lahir:</th>
							<td colspan="3">
								<spring:bind path="cmd.msp_fire_date_of_birth2" >
        		                    <script>
                                        var isDisabled;
                                        var status = '${statusForm}';
                                        if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
                                <font color="#CC3300">*</font></spring:bind>
							</td>
						</tr>
						<tr>
							<th align="left">Jenis Pekerjaan:</th>
							<td>
								<select name="msp_fire_occupation2" onchange="cekElseOccupation();">
									<c:forEach var="pkj" items="${pekerjaan}"> 
										<option <c:if test="${cmd.msp_fire_occupation2 eq pkj.ID}"> SELECTED </c:if> value="${pkj.ID}">${pkj.PEKERJAAN}
										</option>
									</c:forEach> 
								</select><font class="error">*</font>	
							<form:input path="msp_fire_occupation" cssErrorClass="inpError"/><font class="error">*</font></td>
							<th align="left">Bidang Usaha:</th>
							<td>
								<select name="msp_fire_type_business2" onchange="cekElseBusiness();">
									<c:forEach var="bin" items="${bidang_industri}"> 
										<option <c:if test="${cmd.msp_fire_type_business2 eq bin.ID}"> SELECTED </c:if> value="${bin.ID}">${bin.BIDANG}
										</option>
									</c:forEach> 
								</select><font class="error">*</font>
							<form:input path="msp_fire_type_business" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th align="left">Sumber Dana:</th>
							<td colspan="3">
							<select name="msp_fire_source_fund2" onchange="cekElseSourceFund();">
									<c:forEach var="sd" items="${sumber_pendanaan}"> 
										<option <c:if test="${cmd.msp_fire_source_fund2 eq sd.ID}"> SELECTED </c:if> value="${sd.ID}">${sd.DANA}
										</option>
									</c:forEach> 
								</select><font class="error">*</font>
							<form:input path="msp_fire_source_fund" cssErrorClass="inpError" /></td>
						</tr>
						<tr>
							<th align="left">Alamat Tertanggung:</th>
							<td colspan="3">
							<select name="msp_fire_addr_code">
									<c:forEach var="ka" items="${kode_alamat_list}"> 
										<option <c:if test="${cmd.msp_fire_addr_code eq ka.ID}"> SELECTED </c:if> value="${ka.ID}">${ka.ALAMAT}
										</option>
									</c:forEach> 
							</select>
							<form:input path="msp_fire_address_1" size="70" cssErrorClass="inpError"/><font class="error">*</font>
							</td>
						</tr>
						<tr>
							<th align="left">kode pos:</th>
							<td colspan="3"><form:input path="msp_fire_postal_code" maxlength="5" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th align="left">Telp:</th>
							<td><form:input path="msp_fire_phone_number" cssErrorClass="inpError"/><font class="error">*</font></td>
							<th align="left">Hp:</th>
							<td><form:input path="msp_fire_mobile" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th align="left">Alamat Email:</th>
							<td colspan="3"><form:input path="msp_fire_email" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th colspan="4">INFORMASI OBYEK PERTANGGUNGAN</th>
						</tr>
						<tr>
							<th align="left">Alamat Pertanggungan:</th>
							<td colspan="3">
							<select name="msp_fire_insured_addr_code">
									<c:forEach var="ka" items="${kode_alamat_list}"> 
										<option <c:if test="${cmd.msp_fire_insured_addr_code eq ka.ID}"> SELECTED </c:if> value="${ka.ID}">${ka.ALAMAT}
										</option>
									</c:forEach> 
							</select>
							<form:input path="msp_fire_insured_addr" size="60" cssErrorClass="inpError"/>
							No.<form:input path="msp_fire_insured_addr_no" maxlength="5" size="5" cssErrorClass="inpError"/>
							</td>
						</tr>
						<tr>
							<th align="left">Kode Pos:</th>
							<td><form:input path="msp_fire_insured_postal_code" maxlength="5" cssErrorClass="inpError"/><font class="error">*</font></td>
							<th align="left">Kota:</th>
							<td><form:input path="msp_fire_insured_city" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th align="left">Telp:</th>
							<td colspan="3"><form:input path="msp_fire_insured_phone_number" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th align="left">Jenis Bangunan Disekeliling Pertanggungan:</th>
							<td colspan="3">
								<select name="msp_fire_insured_addr_envir" onchange="cekElseEnvir();">
									<c:forEach var="kos" items="${kode_obyek_sekitar_list}"> 
										<option <c:if test="${cmd.msp_fire_insured_addr_envir eq kos.ID}"> SELECTED </c:if> value="${kos.ID}">${kos.OBYEK}
										</option>
									</c:forEach> 
								</select><br/>
								<form:input path="msp_fire_ins_addr_envir_else" size="80" cssErrorClass="inpError"/>
							</td>
						</tr>
						<tr>
							<th align="left">Penggunaan Bangunan:</th>
							<td colspan="3">
								<select name="msp_fire_okupasi" onchange="cekElseOkupasi();">
									<c:forEach var="ko" items="${kode_okupasi_list}"> 
										<option <c:if test="${cmd.msp_fire_okupasi eq ko.ID}"> SELECTED </c:if> value="${ko.ID}">${ko.OKUPASI}
										</option>
									</c:forEach> 
								</select><br/>
								<form:input path="msp_fire_okupasi_else" size="80" cssErrorClass="inpError"/>
							</td>
						</tr>
						</table>
						</fieldset>
						<fieldset>
					   <legend>-</legend>
					   <table class="entry2">
						<tr>
							<th colspan="2" align="left">
								<font class="error">* HARUS DIISI</font>
							</th>
						</tr>
						<tr>
							<th colspan="2">
							<input type="hidden" name="kata" size="1" value="edit" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
								<input type="submit" name="save" value="Save"  onclick="return update();"
									accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();"/>
					<input type="button" name="close" value="Close" onclick="window.close();"
						accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">
						<input type="hidden" name="result" size="1" value="${result}" />
							</th>
						</tr>
					</table>
					</fieldset>
					</div>
		</div>
	</div>	
</form:form>
	<c:if  test="${not empty successMessage}">	
			<script type="text/javascript">													
				alert('${successMessage}');	
				parentForm = self.opener.document.forms['formpost'];
				parentForm.elements['refreshButton'].click();
				window.close();
			</script>
	</c:if>
</body>
</html>