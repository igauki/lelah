<%@ include file="/include/page/header.jsp"%>
	<script>
		function tambah(){
			formpost.flagAdd.value="1";
			formpost.submit();
		}
	
		function simpan(){
			formpost.flagAdd.value="2";
			formpost.submit();
		}
	
		function hapus(){
			formpost.flagAdd.value="3";
			formpost.submit();		
		}
		
		function count() {
			formpost.flagAdd.value="4";
			formpost.submit();			
		}
		
		function lock(value,id) {
			if(value != 2) {
				document.getElementById('msdhr_em'+id).value = 0;
				document.getElementById('msdhr_em'+id).style.visibility = "hidden";
			}else {
				document.getElementById('msdhr_em'+id).value = '';
				document.getElementById('msdhr_em'+id).style.visibility = "visible";
			}
		}
	</script>
	<body>
		<form:form id="formpost" name="formpost" commandName="cmd">
			<div class="tabcontent">
				<fieldset>
					<legend>Hasil Reas [${cmd.spaj}]</legend>
					<table class="entry2">
						<tr>
							<th width="30">#</th>
							<th>Nama Co.Reas</th>
							<th width="120">Tanggal Akseptasi Reas</th>	
							<th width="30">H+</th>
							<th width="90">Expired Date</th>
							<th>Keputusan Reas</th>
							<th width="30">Rating</th>
							<th width="155">Kelainan</th>
							<th width="155">Catatan</th>
							<th width="100">User Input</th>		
						</tr>
						<c:forEach var="s" items="${cmd.lsHslReas}" varStatus="xt">
							<tr>
								<td style="text-align: center;">
									<c:if test="${sessionScope.currentUser.lus_id eq s.msdhr_lus_id}">
										<form:checkbox cssClass="noBorder" path="lsHslReas[${xt.index}].cek" id="lsHslReas[${xt.index}].cek" value="1"/>
									</c:if>	
								</td>
								<td>
									<c:choose>
										<c:when test="${empty s.lsrei_id}">
											<form:select path="lsHslReas[${xt.index}].lsrei_id" cssStyle="width:100%">
												<form:option label="" value=""/>
												<form:options items="${lsHslReas}" itemLabel="value" itemValue="key" />
											</form:select> 											
										</c:when>
										<c:otherwise>
											<select name="lus_id" disabled>
												<option value=""></option>
												<c:forEach items="${lsHslReas}" var="x">
													<option value="${x.key}" <c:if test="${x.key eq s.lsrei_id}">selected</c:if>>${x.value}</option>
												</c:forEach>
											</select>												
										</c:otherwise>
									</c:choose>								
								</td>
								<td style="text-align: center;">
									<spring:bind path="cmd.lsHslReas[${xt.index}].msdhr_input_date">
										<script>inputDate('${status.expression}', '${status.value}', false);</script>
									</spring:bind>									
								</td>
								<td style="text-align: center;">
									<c:choose>
										<c:when test="${empty s.msdhr_expired_day}">
											<form:input id="msdhr_expired_day${xt.index}" size="4" maxlength="3" path="lsHslReas[${xt.index}].msdhr_expired_day"/>												
										</c:when>
										<c:otherwise>
											<form:input id="msdhr_expired_day${xt.index}" size="4" readonly="readonly" cssClass="readOnly" maxlength="3" path="lsHslReas[${xt.index}].msdhr_expired_day"/>																								
										</c:otherwise>
									</c:choose>									
								</td>
								<td style="text-align: center;">
									<form:input id="msdhr_expired_date${xt.index}" size="11" path="lsHslReas[${xt.index}].msdhr_expired_date" readonly="readonly" cssClass="readOnly"/>							
								</td>								
								<td>
									<c:choose>
										<c:when test="${empty s.msdhr_keputusan}">
											<form:select path="lsHslReas[${xt.index}].msdhr_keputusan" id="msdhr_keputusan${xt.index}" cssStyle="width:100%"  onchange="lock(this.value,'${xt.index}')">
												<form:option label="" value=""/>
												<form:options items="${lsKeputusanReas}" itemLabel="value" itemValue="key" />
											</form:select> 											
										</c:when>
										<c:otherwise>
											<select name="lus_id" disabled>
												<option value=""></option>
												<c:forEach items="${lsKeputusanReas}" var="x">
													<option value="${x.key}" <c:if test="${x.key eq s.msdhr_keputusan}">selected</c:if>>${x.value}</option>
												</c:forEach>
											</select>												
										</c:otherwise>
									</c:choose>									
								</td>
								<td style="text-align: center;">
									<!--<form:input id="msdhr_em${xt.index}" size="4" maxlength="3" path="lsHslReas[${xt.index}].msdhr_em"/>-->										
									<c:choose>
										<c:when test="${empty s.msdhr_em}">
											<form:input id="msdhr_em${xt.index}" size="4" maxlength="3" path="lsHslReas[${xt.index}].msdhr_em"/>										
										</c:when>
										<c:otherwise>
											<!--<input type="text" value="lsHslReas[${xt.index}].msdhr_em" id="msdhr_em${xt.index}" readonly="readonly" class="readOnly" maxlength="3">-->
											<form:input id="msdhr_em${xt.index}" size="4" readonly="readonly" cssClass="readOnly" maxlength="3" path="lsHslReas[${xt.index}].msdhr_em"/>											
										</c:otherwise>
									</c:choose>									
								</td>								
								<td>
									<c:choose>
										<c:when test="${empty s.msdhr_alasan}">
											<form:textarea rows="3" cols="60" path="lsHslReas[${xt.index}].msdhr_alasan" cssStyle="width: 100%;text-transform: none;"/>
										</c:when>
										<c:otherwise>
											<textarea rows="3" cols="60" style="width: 100%;text-transform: none;" readonly="readonly" class="readOnly"/>${s.msdhr_alasan}</textarea>
										</c:otherwise>
									</c:choose>	
								</td>
								<td>
									<c:choose>
										<c:when test="${empty s.msdhr_catatan}">
											<form:textarea rows="3" cols="60" path="lsHslReas[${xt.index}].msdhr_catatan" cssStyle="width: 100%;text-transform: none;"/>
										</c:when>
										<c:otherwise>
											<textarea rows="3" cols="60" style="width: 100%;text-transform: none;" readonly="readonly" class="readOnly"/>${s.msdhr_catatan}</textarea>
										</c:otherwise>
									</c:choose>	
								</td>
								<td style="text-align: center;">${s.lus_full_name}</td>	
							</tr>
						</c:forEach>	
	    				<spring:hasBindErrors name="cmd">
		    				<tr>
		    					<td colspan="10">
									<div id="error" style="margin: 5px 5px 5px 5px;">
										- <form:errors path="*" delimiter="<br>- "/>
									</div>
								</td>
		    				</tr>
	    				</spring:hasBindErrors>							
						<tr>
							<td colspan="10" style="text-align: center;">
								<form:hidden path="flagAdd" />
								<input type="button" name="btn_save" value="Save" onclick="simpan();" <c:if test="${cmd.mode eq \'viewer\'}">disabled="disabled"</c:if>>
								<input type="button" name="btn_add" value="Add" onclick="tambah();" <c:if test="${fn:length(cmd.lsHslReas) gt 3}">disabled="disabled"</c:if>>
								<!--<input type="button" name="btn_add" value="Add" onclick="tambah();" <c:if test="${!empty cmd.lsHslReas or cmd.mode eq \'viewer\'}">disabled="disabled"</c:if>>-->
								<input type="button" name="btn_delete" value="Delete" onclick="hapus();" <c:if test="${cmd.mode eq \'viewer\'}">disabled="disabled"</c:if>>								
								<input type="button" name="btn_count" value="Count Exp Date" onclick="count();">								
							</td>

						</tr>																			
					</table>
					<input type="hidden" name="spaj" value="${cmd.spaj}">
				</fieldset>
				<table width="50%">
					<tr>
						<td colspan="2">
							<c:if test="${not empty submitSuccess}">
					        	<div id="success">
						        	Berhasil
					        	</div>
					        </c:if>	
						</td>
					</tr>
				</table>									
			</div>
		</form:form>
	</body>
<%@ include file="/include/page/footer.jsp"%>
