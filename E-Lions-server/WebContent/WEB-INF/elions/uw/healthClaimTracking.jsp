<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<script type="text/javascript" src="${path }/include/js/default.js"></script>

<BODY style="height: 100%; text-align: center;" onload="loadImages()">
<div id="hidepage" style="position: absolute; left:5px; top:5px; background-color: #FFFFCC; layer-background-color: #FFFFCC; height: 100%; width: 100%;"> 
	<table width=100%><tr><td>Page loading ... Please wait.</td></tr></table>
</div> 
	
		
		
		<c:forEach items="${claimTracking}" var="c" varStatus="s">
			<c:if test="${s.count eq 1}">
				<fieldset style="text-align: center;">
					<legend>Tracking Klaim Kesehatan [Polis : ${c.MSPO_POLICY_NO} | REG CLAIM : ${c.REGCLAIM}]</legend>
					<table class="displaytag" width="100%">
					<thead>
						<tr>
							<th>TANGGAL</th><th>KETERANGAN</th><th>POSISI DOKUMEN</th><th>STATUS KLAIM</th>
						</tr>
						</thead>
						<tbody>
						
							<tr class="odd">
								<td><fmt:formatDate value="${c.REGAPLDATE}" pattern="dd/MM/yyyy"/></td>
								<td>TGL PENGAJUAN KLAIM</td>
								<td>PENGAJUAN KLAIM</td>
								<td>Claim Being Processed</td>	
							</tr>
							<tr class="even">
								<td><fmt:formatDate value="${c.TGLDOKUMEN}" pattern="dd/MM/yyyy"/></td>
								<td>TGL DOKUMEN</td>
								<td>TERIMA DOKUMEN LENGKAP</td>
								<td>Claim Being Processed</td>		
							</tr>
							<tr class="odd">
								<td><fmt:formatDate value="${c.REGDATEINPUT}" pattern="dd/MM/yyyy"/></td>
								<td>TGL INPUT</td>
								<td>INPUT KLAIM</td>
								<td>Claim Being Processed</td>		
							</tr>
							<tr class="even">
								<td><fmt:formatDate value="${c.ACCEPTDATE}" pattern="dd/MM/yyyy"/></td>
								<td>TGL AKSEPTASI</td>
								<td>AKSEPTASI</td>
								<td>${c.ST_EXPL}</td>		
							</tr>
						
							<tr class="odd">
								<td><fmt:formatDate value="${c.TGLFINANCE}" pattern="dd/MM/yyyy"/></td>
								<td>TGL FINANCE</td>
								<td>Transfer Ke Finance</td>
								<td>Proses Pembayaran</td>		
							</tr>
							<tr class="even">
								<td><fmt:formatDate value="${c.CLM_PAID_DATE}" pattern="dd/MM/yyyy"/></td>
								<td>TGL Bayar</td>
								<td>Pembayaran</td>
								<td>Pembayaran</td>		
							</tr>
						</tbody>
					</table>
				</fieldset>
			</c:if>
		</c:forEach>
			
		
	
</body>
<%@ include file="/include/page/footer.jsp"%>