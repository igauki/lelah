<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>
		<script>
			
		</script>
</head>
<body>		
	<form:form commandName="cmd" id="formpost" method="post" action="${path}/uw/input_comment.htm?window=comment">
	<table  class="entry2" align="center" cellpadding="2" >  
				<c:if test="${empty disabled}">     				
			        <tr>
			        	<td>INPUT COMMENT</td>
			            <td><textarea id="comment" name="comment" rows="4" cols="50">${comment}</textarea>			            
			            	<input type="hidden" name="add_num" id="add_num" value="${add_num}" />
			            	<input type="hidden" name="mofs_id" id="mofs_id" value="${mofs_id}" />
			            	<input type="hidden" name="type" id="type" value="${type}" />
			            	<input type="hidden" name="spaj" id="spaj" value="${spaj}" />
			            	<input type="hidden" name="nama" id="nama" value="${nama}" />
			            </td>
      				</tr>
      				<tr>
      					<td align ="center" colspan ="5"><input type="submit" name="search" value="Submit" id="Submit" /></td>
      				</tr>
      				<tr>
      					<td align ="center" colspan ="5"><font color="blue">${message}</font><font color="red">${warning}</font></td>
      				</tr>
      			</c:if>
      			<c:if test="${not empty disabled}">
      				<tr>
			        	<td>INPUT COMMENT</td>
			            <td><textarea id="comment" disabled  name="comment" rows="4" cols="50">${comment}</textarea></td>			            
			            <td>	<input type="hidden" name="mofs_id" id="mofs_id" value="${mofs_id}" /></td>
			            <td>	<input type="hidden" name="type" id="type" value="${type}" /></td>
			            <td>	<input type="hidden" name="spaj" id="spaj" value="${spaj}" /></td>
			            
      				</tr>
      				<tr>
      					<td align ="center" colspan ="5"><input type="submit" disabled name="search" value="Submit" id="Submit" /></td>
      				</tr>
      				<tr>
      					<td align ="center" colspan ="5"><font color="blue">${message}</font></td>
      				</tr>
      			</c:if>
    </table>
</form:form>
</body>
<%@ include file="/include/page/footer.jsp"%>