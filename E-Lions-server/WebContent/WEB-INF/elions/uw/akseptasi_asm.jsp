<%@ include file="/include/page/header.jsp"%>
<script>
	function buttonClick(val){
		if(val=='ganti'){
			formpost.flag.value="1";
			formpost.submit();
		}else if(val=='simpan'){
			formpost.flag.value="2";
			formpost.submit();
		}
	}
	
	function awal(){
		if('${err}'=='1'){
			alert("Polis sudah di aksep");
			window.location='${path }/uw/view.htm?showSPAJ=${spaj}';
		}
		
		if('${info}'=='51')
			alert("No Polis Null ");
	 	else if('${info}'=='52')
			alert("No Polis Kembar Silahkan Transfer Ulang");
	 
		
	}
	
</script>

<body onload="awal();">
<c:if test="${err eq null}">
	<form name="formpost"  method="post">
	<table class="entry2" width="60%">
 		<tr>
 			<td>Begin Date: 
	 				<script>inputDate('begDate', '${begDate}', false);</script>
 					<input type="button" value="change" <c:if test="${info eq 1}">disabled</c:if> onClick="buttonClick('ganti');">
				End Date :
					<script>inputDate('endDate', '${endDate}', false);</script>
 			</td>
		</tr>	
		<tr>
			<td>
				No Polis ASM :
				<input type="text" <c:if test="${info eq 1}">disabled</c:if> name="nopolis_asm" size="30">
				<input type="button" <c:if test="${info eq 1}">disabled</c:if> name="simpan" value="Save" onClick="buttonClick('simpan');">
				<input type="hidden" name="flag" value="0">
			</td>
		</tr>
		<c:if test="${info eq 1}">
			<tr>
				<td>
					<div id="success">
						Berhasil Simpan. Silahkan Transfer Polis ini.
					</div>
				</td>
			</tr>
		</c:if>
	</table>
	</form>  
</c:if>
</body>                                                                                   
<%@ include file="/include/page/footer.jsp"%>