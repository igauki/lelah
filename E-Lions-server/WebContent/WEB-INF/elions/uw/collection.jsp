<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<script>

	function showPage(hal){
		var spaj = document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value;
		var lsbs = document.formpost.kodebisnis.value;
		var lsdbs = document.formpost.numberbisnis.value;
		var copy_reg_spaj = document.formpost.copy_reg_spaj.value;
		if(spaj == ''){
			alert('Harap pilih SPAJ terlebih dahulu!');
		}else{
			createLoadingMessage();
			if('tampil' == hal){
				document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj+'&halForBlacklist=uw';
			}else if('editagen' == hal){
				document.getElementById('infoFrame').src='${path}/bac/editagenpenutup.htm?spaj='+spaj;			
			}else if('uwinfo' == hal){
				document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=uwinfo&spaj='+spaj;			
			}else if('approve' == hal){
				var result=confirm("Apakah Semua Proses Di Collection Sudah OK ?");
				if(result==true){ 
					//approved(trim(document.formpost.spaj.value),'1','${sessionScope.currentUser.lus_id}','2','APPROVE BY COLLECTION');
			   	    document.getElementById('infoFrame').src='${path}/uw/uw.htm?window=prosesSpeedy&flag=2&spaj='+spaj;
			   	   //document.getElementById('infoFrame').src='${path}/uw/transferparalel.htm?flag=2&spaj='+spaj;
				}else{
				//approved(trim(document.formpost.spaj.value),'0','${sessionScope.currentUser.lus_id}','2','NOT APPROVE BY COLLECTION');
				alert("Proses DiCancel");
				} 
			}else if('sblink' == hal){
				popWin('${path}/uw/inputsblink.htm?reg_spaj='+spaj, 550, 875); 
			}else if('questionnaire' == hal){
				document.getElementById('infoFrame').src='${path}/uw/questionnaire.htm?reg_spaj='+spaj; 
			}else if('aksepManual' == hal){
				popWin('${path}/bac/pending.htm?spaj='+spaj, 250, 375);
			}else if('emailexpired' == hal){
				var result=confirm("Apakah Anda ingin mengirim email pemberitahuan pengembalian premi ?", "U/W");
				if(result==true) document.getElementById('infoFrame').src='${path }/uw/viewer.htm?window=emailexpired&spaj='+spaj;
			}else if('worksheet' == hal){
				document.getElementById('infoFrame').src='${path }/uw/worksheet.htm?spaj='+spaj+'&copy_reg_spaj='+copy_reg_spaj;
			}else if('reasguthrie' == hal){
				var result=confirm("SPAJ GUTHRIE! Lanjutkan Proses REAS?", "U/W");
				if(result==true) document.getElementById('infoFrame').src='${path }/uw/viewer.htm?window=reas_guthrie&spaj='+spaj;
			}else if('aksep' == hal){
				if(confirm("Apakah Anda Ingin Aksep/Fund Polis Ini?")){
					document.getElementById('infoFrame').src='${path }/uw/akseptasi.htm?spaj='+spaj;
				}
			}else if('transfer' == hal){
				if(confirm("Apakah Anda Ingin Transfer Polis Ini?")){
					document.getElementById('infoFrame').src='${path }/uw/transfer.htm?spaj='+spaj+'&flagNew='+1;
				}	
			}else if('ubahreas' == hal){
				document.getElementById('infoFrame').src='${path }/uw/ubah_reas.htm?spaj='+spaj;
			}else if('nik' == hal){
				if  (lsbs == 138 || lsbs == 140 || lsbs == 141 || lsbs == 148 || lsbs == 149 || lsbs == 156 || lsbs == 139 || 
						(lsbs == 142 && lsdbs == 4) || (lsbs == 143 && lsdbs == 2) || (lsbs == 158 && lsdbs == 4) || (lsbs == 158 && lsdbs == 7)){
					popWin('${path}/bac/nik.htm?sts=insert&spaj='+spaj, 500, 800);
				}else{
					alert('Nik tidak dapat diisi untuk polis ini');					
				}
			}else if('tglspaj' == hal){
				document.getElementById('infoFrame').src='${path }/uw/viewer.htm?window=editTglTrmKrmSpaj&spaj='+spaj+'&show=2';
			}else if('reffbii' == hal){
				popWin('${path}/bac/reff_bank.htm?window=main&spaj='+spaj, 400, 700);
			}else if('claim' == hal){
				popWin('${path}/uw/uw.htm?window=healthClaim&spaj='+spaj, 400, 900);
			}else if('tglTerimaSpaj' == hal){
				popWin('${path}/uw/viewer.htm?window=editTglTrmKrmSpaj&spaj='+spaj+'&show=0', 150, 350);
 			}else if('titipanpremi' == hal){
 				popWin('${path}/ttppremi/titipan_premi.htm?sts=insert&editSPAJ='+spaj, 600, 800);
 			}else if('upload_nb' == hal){
 				document.getElementById('infoFrame').src='${path}/uw/upload_nb.htm?reg_spaj='+document.formpost.spaj.value;
 			}else if('spesial_hadiah' == hal){
 				//alert('${path}/uw/uw.htm?window=spesial_hadiah&reg_spaj='+spaj);
 				//popWin('${path}/uw/uw.htm?window=spesial_hadiah&reg_spaj='+spaj, 400, 900);
 				document.getElementById('infoFrame').src='${path}/uw/uw.htm?window=spesial_hadiah&reg_spaj='+document.formpost.spaj.value;
 			}else if('permintaanBSB' == hal){
					if(confirm('menu ini hanya untuk H+1, Anda yakin untuk memproses?')){
					document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=permintaanBSB&spaj='+spaj;
					}			
			}else if('statusFurtherCollection' == hal){
					document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=furtherCollection&spaj='+spaj;
			}
		}
	}
	
	function jalurkhusus(hal){
	 if('autoproses' == hal){
					document.getElementById('infoFrame').src='${path}/uw/uw.htm?window=autoprosesUwSimas';
			}
	}
	
	function cariregion(spaj,nama){
		if(spaj != ''){
			if(document.getElementById('docFrame')) document.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+spaj;
			if(document.getElementById('infoFrame')) document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj+'&halForBlacklist=uw';
			ajaxManager.listcollect(spaj , nama,
			{callback:function(map) {
				DWRUtil.useLoadingMessage();
				document.formpost.koderegion.value=map.LSRG_NAMA;
				document.formpost.kodebisnis.value = map.LSBS_ID;
				document.formpost.numberbisnis.value = map.LSDBS_NUMBER;
				document.formpost.jml_peserta.value = map.jml_peserta;
			   },
			  timeout:15000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
			});
			
		}	
	}
	
	function awal(){
		if('${snow_spaj}'!=''){
			document.formpost.spaj.value='${snow_spaj}';
			document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value='${snow_spaj}';
			cariregion('${snow_spaj}','region');
		}else{
			cariregion(document.formpost.spaj.value,'region');
		}
		setFrameSize('infoFrame', 90);
		setFrameSize('docFrame', 90);
	}
	
	function copyToClipboard(txt){
	
		if( window.clipboardData && clipboardData.setData ){
		
			clipboardData.setData("Text", txt);
		}
		else{
		 alert("Internet Explorer required"); 
		}
	}
	
	function run(){
		var shell = new ActiveXObject("WScript.Shell"); 
		var commandtoRun = "\\\\aplikasi\\lions\\secan4400f.exe";
	   
		if (shell){		
		 shell.run(commandtoRun); 
		} 
		else{ 
			alert("program or file doesn't exist on your system."); 
		}
	}
	
	function copyAndRun(){ 
	 	txt = document.getElementById("spaj").value;
	 	copyToClipboard(txt);
	 	run();		
	}
	
	function check(name){
	    var res = null;
	    var currObj = document.formpost[name];
	    
	    for (var i=0;i<currObj.length;i++) {
			if (currObj[i].checked) {
				res = currObj[i].value;
			}
		}
	    
	    return res;
	}

</script>
<body  onload="awal();" onresize="setFrameSize('infoFrame', 90); setFrameSize('docFrame', 90);" style="height: 100%;">
<form name="formpost" method="post">
<div class="tabcontent">
<table class="entry2" style="width: 98%;">
	<tr>
		<th>Cari SPAJ</th>
		<td> <input type="hidden" name="halForBlacklist" value="uw"> 
		  <input type="hidden" name="koderegion" > 
          <input type="hidden" name="kodebisnis">
          <input type="hidden" name="numberbisnis">
          <input type="hidden" name="jml_peserta">
          <input type="hidden" name="copy_reg_spaj" value="" />
			<select name="spaj" id="spaj" onChange="cariregion(this.options[this.selectedIndex].value,'region');">
				<option value="">[--- Silahkan Pilih/Cari SPAJ ---]</option>
				<c:forEach var="s" items="${daftarSPAJ}">
					<option value="${s.REG_SPAJ }" style="background-color: ${s.BG};" 
						<c:if test="${s.REG_SPAJ eq param.spaj }">selected<c:set var="ada" value="ada" /></c:if>>${s.SPAJ_FORMATTED}
					- ${s.POLICY_FORMATTED }</option>
				</c:forEach>
			</select>
			
			<input type="button" value="Info" name="info" onclick="showPage('tampil');" accesskey="I" onmouseover="return overlib('Alt-I', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Cari SPAJ" name="search" onclick="popWin('${path}/uw/spaj.htm?posisi=2,27,251,209&win=uw', 600, 800); " accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="U/W Info" name="uwinfo" onclick="showPage('uwinfo');" accesskey="W" onmouseover="return overlib('Alt-W', AUTOSTATUS, WRAP);" onmouseout="nd();">
		</td>
	</tr>
	
	<tr>
		<th>Proses Utama</th>
		<td>
			<input type="button" value="Titipan Premi" name="ttp" onclick="showPage('titipanpremi')" accesskey="Z" onmouseover="return overlib('Titipan Premi', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Approve(Proses NB)" name="approve" onclick="showPage('approve');" style="width: 200px;">
<!-- 			<input type="button" value="permintaan BSB" name="permintaanBSB" onclick="showPage('permintaanBSB');" style="width: 200px;"> -->
<!-- 			<input type="button" value="Status Further Collection" name="statusFurtherCollection"	onclick="showPage('statusFurtherCollection');" style="width: 200px;"> -->
			<input type="button" value="Status Further Collection" name="statusFurtherCollection"	onclick="showPage('statusFurtherCollection');" style="width: 200px;">      
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<table style="margin: 0 0 0 0; padding: 0 0 0 0; width: 100%;">
				<tr>
					<c:choose>
						<c:when test="${sessionScope.currentUser.wideScreen}">
							<td style="width: 60%;">
								<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
									width="100%"  height="100%"> Please Wait... </iframe>
							</td>
							<td style="width: 40%;">
								<iframe src="" name="docFrame" id="docFrame" width="100%">E-Lions</iframe>
							</td>
						</c:when>
						<c:otherwise>
							<td>
								<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
									width="100%"  height="100%"> Please Wait... </iframe>
							</td>
						</c:otherwise>
					</c:choose>
				</tr>
			</table>		
		</td>	
	</tr>
</table>
</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>