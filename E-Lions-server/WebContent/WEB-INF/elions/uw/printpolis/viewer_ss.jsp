<%@ include file="/include/page/header.jsp"%>
<style type="text/css">
	html,body{
	     overflow: hidden;
	}
</style>
<body style="height: 100%;" onload="setupPanes('container1', 'tab1'); ">
	<div class="tab-container" id="container1">
		
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Daftar Syarat-Syarat Umum / Khusus</a>
			</li>
		</ul>

		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form method="post" name="formpost">
					<table class="entry2">
						<c:choose>
							<c:when test="${empty error}">
								<tr>
									<th>Dokumen</th>
									<td>
										<table class="displaytag" style="width: auto;">
											<tr>
												<th>Dokumen</th>
												<th>Tanggal</th>
											</tr>
											<c:forEach var="dok" items="${daftarFile}">
												<tr>
													<td style="text-align: left;"><a href="${path}/uw/printpolis.htm?window=viewer_ss&file=${dok.key}">${dok.key}</a></td>
													<td style="text-align: center;">${dok.value}</td>
												</tr>
											</c:forEach>
										</table>
									</td>
								</tr>
							</c:when>
							<c:otherwise>
								<tr>
									<th>Error:</th>
									<td><div id="error">${error}</div></td>
								</tr>
							</c:otherwise>
						</c:choose>
					</table>
				</form>			
			</div>
		</div>
		
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>