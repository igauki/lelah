<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path}/include/js/disableKeys.js"></script>
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script>
	hideLoadingMessage();
</script>
</head>
<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Alasan Pending Print Polis</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form:form id="formpost" name="formpost" method="post" commandName="command" action="${path}/uw/printpolis.htm?window=alasan">
					<form:hidden path="spaj"/>
					<table class="entry2" style="width: auto;">
						<tr>
							<th>Tanggal</th>
							<th>User</th>
							<th>Keterangan</th>
						</tr>
						<c:forEach items="${command.daftarPosisi}" var="a" varStatus="st">
							<tr>
								<td>
									<fmt:formatDate value="${a.msps_date}" pattern="dd/MM/yyyy (hh:mm)"/>
								</td>
								<td>
									${a.lus_login_name}
								</td>
								<td>
									${a.msps_desc}
								</td>
							</tr>
						</c:forEach>
						<tr>
							<td>
								<fmt:formatDate value="${command.position.msps_date}" pattern="dd/MM/yyyy (hh:mm)"/>
							</td>
							<td>
								${command.position.lus_login_name}
							</td>
							<td>
								<form:textarea path="position.msps_desc" rows="3" cols="50" />
							</td>
						</tr>
					</table>
					<input type="submit" name="save" value="Save">
				</form:form>
			</div>
			
		</div>
	</div>

</body>
</html>