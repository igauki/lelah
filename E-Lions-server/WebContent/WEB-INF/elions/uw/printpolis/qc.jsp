<%@ include file="/include/page/header_jquery.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>    
    <title>PT. Asuransi Jiwa Sinarmas MSIG</title>
    <link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">    
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<c:set var="pesan" value="${error}"/>
	<script type="text/javascript">

	$(document).ready(function() {

	 });
	 

	</script>
	<style type="text/css">
	body
	{
		font-size: 1px;
		color: #996600;
		background-color: #F2F2F2;
	}
	</style>

  </head>
  
  <body bgcolor="ffffff" onLoad="Body_onload();" marginwidth="100%" marginheight="100%">
  	<c:if test="${flag eq null}">							
	  <form name="formpost" method="post">
	    <table width="70%" cellspacing="2" cellpadding="2" border = "1">
	    	<tr>
	    		<td colspan="2" align="center" bgcolor="#B40404" style="color: white;"><h1><b>QA 1</b></h1>
	    		<input type="submit" name="btnQc1" id="btnQc1" value="ok" style="width: 50px">
	    		</td>
	    	</tr>
	    	<tr>
	    		<td style="width: 30%">Cek Fisik</td>
	    		<td style="width: 70%">Cek Inputan</td>
	    	</tr>
	    	<tr>
	    		<td style="width: 30%">Kop Surat</td>
	    		<td style="width: 70%">${pemegang}</td>
	    	</tr>
	    	<tr>
	    		<td style="width: 30%">Standart Cetak dan Kertas</td>
	    		<td style="width: 70%">${tertanggung}</td>		
	    	</tr>
	    		<td style="width: 30%">Kelengkapan Polis</td>
	    		<td style="width: 70%">
	    		<c:forEach items="${select_ahliwaris}" var="x">
    					${x.MSAW_FIRST} <br>
	    		</c:forEach>
	    		</td>		
	    	</tr>
			<tr>
	    		<td style="width: 30%">Tambahan Dokumen</td>
    			<td style="width: 70%">
				<c:forEach items="${select_peserta}" var="y">
    					${y.NAMA} <br>
	    		</c:forEach>
	    		</td>		
	    	</tr>
			<tr>
	    		<td style="width: 30%">Simas, PAS, admedika</td>
	    		<td style="width: 70%">&nbsp; </td>		
	    	</tr>
	    	<tr>
	    		<td colspan="2" bgcolor="#B40404">&nbsp;</td>
	    	</tr>
	    </table>
	  </form>
	</c:if> 
	  <br><br>
	 <c:if test="${flag eq null || flag ne null}">		 
	  <form name="formpost2" method="post">
	    <table width="70%" cellspacing="2" cellpadding="2" border = "1">
	    	<tr>
	    		<td colspan="2" align="center" bgcolor="#B40404" style="color: white;"><h1><b>QA 2</b></h1>
	    		<input type="submit" name="btnQc2" id="btnQc2" value="ok" style="width: 50px">
	    		</td>
	    	</tr>
	    	<tr>
	    		<td style="width: 30%">Cek Fisik</td>
	    		<td style="width: 70%">Cek Inputan</td>
	    	</tr>
	    	<tr>
	    		<td style="width: 30%">Sampul Fisik</td>
	    		<td style="width: 70%">&nbsp; </td>
	    	</tr>
	    	<tr>
	    		<td style="width: 30%">Cover Polis</td>
	    		<td style="width: 70%">&nbsp; </td>		
	    	</tr>
	    		<td style="width: 30%">Standart Bending</td>
	    		<td style="width: 70%">&nbsp; </td>		
	    	</tr>
			<tr>
	    		<td style="width: 30%">Tambahan Dokumen</td>
	    		<td style="width: 70%">&nbsp; </td>		
	    	</tr>
			<tr>
	    		<td style="width: 30%">Simas, PAS, admedika</td>
	    		<td style="width: 70%">&nbsp; </td>		
	    	</tr>
	    	<tr>
	    		<td colspan="2" bgcolor="#B40404">&nbsp;</td>
	    	</tr>
	    </table>
	  </form>
	  </c:if> 
  </body>
</html>
<%@ include file="/include/page/footer.jsp"%>