<%@ include file="/include/page/header.jsp"%>
<style type="text/css">
	html,body{
	     overflow: hidden;
	}
</style>
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript">
	hideLoadingMessage();
	
	function tampilkan(kode, espeaje, dokumen){
		//20181213 Mark create loading message
		createLoadingMessage();	

		//alert(dokumen+kode);
		if(dokumen==-1){
			alert('Silahkan pilih terlebih dahulu jenis dokumen yang ingin ditampilkan');
		}else if(document.getElementById('infoFrame')){
			var eek;

			if(dokumen==12){ //surat perjanjian ngutang
				eek='${path}/report/bac.pdf?window=sph&spaj='+espeaje;	
			}else if(dokumen==9){
				eek = '${path }/uw/printpolis.htm?isviewer=true&window=print&spaj='+espeaje+
				'&printType='+dokumen+'&format=file.pdf';
			}else if(kode==0){
				eek = '${path }/uw/printpolis.htm?isviewer=true&window=print&spaj='+espeaje+
				'&printType='+dokumen+'&format=file.html';
			}else if(kode==1){
				eek = '${path }/uw/printpolis.htm?isviewer=true&window=print&spaj='+espeaje+
					'&printType='+dokumen+'&format=file.jasper';
			}else if(kode==2){
				if(dokumen==2 || dokumen==6)
					eek = '${path }/uw/printpolis.htm?window=printulangpolis&jenis=1&spaj='+espeaje+
					'&printType='+dokumen+'&format=file.pdf';
				else
					eek = '${path }/uw/printpolis.htm?isviewer=true&window=print&jenis=1&spaj='+espeaje+
					'&printType='+dokumen+'&format=file.pdf';
			}else if(kode==3){
				eek = '${path }/uw/printpolis.htm?isviewer=true&e=w&window=print&jenis=1&spaj='+espeaje+
				'&printType='+dokumen+'&format=file.pdf';
			}else if(kode==4){
				eek = '${path }/uw/printpolis.htm?isviewer=true&isbancass=true&window=print&jenis=2&spaj='+espeaje+
				'&printType='+dokumen+'&format=file.pdf';
			}
	
			if(eek!='')
				document.getElementById('infoFrame').src = eek;
			
			return false;
		}
	}
</script>
<body onload="setupPanes('container1', 'tab1'); setFrameSize('infoFrame', 55);" onresize="setFrameSize('infoFrame', 55);" style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">View Print Polis untuk SPAJ [${spaj}]</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">

				<form name="formpost" method="post">
				<table class="entry2" width="98%">
					<tr>
						<th>Dokumen</th>
						<td>
							<select name="printType">
								<c:set var="grup" value=""/>
								<c:forEach var="r" items="${reportList}" varStatus="st">
									<c:choose>
										<c:when test="${r.g ne grup}">
											<c:set var="grup" value="${r.g}"/>				 
											<OPTGROUP label="${r.g}">
												<option value="${r.kode}">${r.judul}</option>
										</c:when>
										<c:otherwise>
											<option value="${r.kode}">${r.judul}</option>						
										</c:otherwise>
									</c:choose>
									<c:if test="${r.g ne grup}">
										<c:set var="grup" value="${r.g}"/>				
										</OPTGROUP>
									</c:if>
								</c:forEach>
							</select> 
				
							&nbsp;
							<a href="#" onclick="return tampilkan(0, '${spaj}', document.formpost.printType.options[document.formpost.printType.selectedIndex].value);"
								onmouseover="return overlib('Alt-H > View (HTML)', AUTOSTATUS, WRAP);" onmouseout="nd();" accesskey="H">
								<img style="border:none;" alt="HTML" src="${path}/include/image/page_html.gif"/></a>
							&nbsp;
							<a href="#" onclick="return tampilkan(1, '${spaj}', document.formpost.printType.options[document.formpost.printType.selectedIndex].value);"
								onmouseover="return overlib('Alt-J > View (JAVA)', AUTOSTATUS, WRAP);" onmouseout="nd();" accesskey="J">
								<img style="border:none;" alt="JAVA" src="${path}/include/image/page_java.gif"/></a>

							<c:choose>
								<c:when test="${not empty orangBancass}">
									&nbsp;
									<a href="#" onclick="return tampilkan(4, '${spaj}', document.formpost.printType.options[document.formpost.printType.selectedIndex].value);"
										onmouseover="return overlib('Alt-R > Print Polis', AUTOSTATUS, WRAP);" onmouseout="nd();" accesskey="R">
										<img style="border:none;" alt="PRINT POLIS" src="${path}/include/image/action_print.gif"/></a>
								</c:when>
								<c:when test="${not empty orangUnderwriting}">
									&nbsp;
									<a href="#" onclick="return tampilkan(2, '${spaj}', document.formpost.printType.options[document.formpost.printType.selectedIndex].value);"
										onmouseover="return overlib('Alt-R > Print Ulang Polis', AUTOSTATUS, WRAP);" onmouseout="nd();" accesskey="R">
										<img style="border:none;" alt="PRINT ULANG POLIS" src="${path}/include/image/action_print.gif"/></a>
								</c:when>
							</c:choose>
							
							<c:if test="${bolehCetakSeenakJidat eq 'true'}">
								&nbsp;
								<a href="#" onclick="return tampilkan(3, '${spaj}', document.formpost.printType.options[document.formpost.printType.selectedIndex].value);"
									onmouseover="return overlib('Alt-P > Print Polis', AUTOSTATUS, WRAP);" onmouseout="nd();" accesskey="P">
									<img style="border:none;" alt="PRINT POLIS" src="${path}/include/image/icon_monitor_pc.gif"/></a>
							</c:if>
							
						</td>
					</tr>
					<tr>
						<th colspan="2">
							<iframe name="infoFrame" id="infoFrame" width="100%" src="${path}/uw/printpolis.htm?window=viewer_info&spaj=${spaj}">
								Please Wait...
							</iframe>
						</th>
					</tr>
				</table>
				</form>
			</div>
		</div>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>