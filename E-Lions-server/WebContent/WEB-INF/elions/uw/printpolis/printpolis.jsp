<%@ include file="/include/page/header.jsp"%>
<style type="text/css">
	html,body{
	     overflow: hidden;
	}
</style>
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript">
	hideLoadingMessage();
	
	function mulai(){
		if('${snow_spaj}'!=''){
			document.formpost.spaj.value='${snow_spaj}';
			document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value='${snow_spaj}';
		}
		<c:if test="${not empty daftarPesan}">
			<c:forEach items="${daftarPesan}" var="d">
				alert('${d}');
			</c:forEach>
		</c:if>
		
		//tampilkan('info',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);
	}
    
	function get(id){
		return document.getElementById(id);
	}

	function cek(asdf){
		if(asdf == '') {
			alert('Harap pilih SPAJ terlebih dahulu!');
			return false;
		}else{
			return true;
		}
	}
	
	function tampilkan(kode, asdf){
		//20181213 Mark create loading message
		createLoadingMessage();
			
		var dipilih = document.formpost.printType.options[document.formpost.printType.selectedIndex].value;
		var copy_reg_spaj = document.formpost.copy_reg_spaj.value;
		var referal = 0;
		
		//untuk simas prima referal
		if(document.getElementById('banyakMaunya')){
			var banyakMaunya = document.getElementById('banyakMaunya').checked;
			if(banyakMaunya){
				referal = 1;
			}		
		}
		
		if(get('error')) get('error').style.display='none';
		if(get('success')) get('success').style.display='none';
		
		if(get('infoFrame')){
			get('infoFrame').style.display='block';
			if(dipilih==12){ //surat perjanjian hutang
				if(cek(asdf)) get('infoFrame').src='${path}/report/bac.pdf?window=sph&spaj='+asdf;	
			}else if(kode=='info'){
				if(cek(asdf)) {
					get('infoFrame').src='${path }/uw/view.htm?showSPAJ='+asdf;	
					if(document.getElementById('docFrame')) document.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+asdf;
				}
			}else if(kode=='otorisasi'){ //khusus pincab / spv bank sinarmas
				get('infoFrame').src='${path }/uw/printpolis.htm?window=otorisasi';	
			}else if(kode=='vmaterai'){ //khusus pincab / spv bank sinarmas
				get('infoFrame').src='${path}/uw/uw.htm?window=validasimaterai';	
			}else if(kode=='inputEndorsment'){
				if(cek(asdf)) get('infoFrame').src='${path}/uw/printpolis.htm?window=suratendorsment&spaj='+asdf;
			}else if(kode=='transfer'){
				if(cek(asdf)) if(confirm('Anda yakin mentransfer polis ini?')){
					if(confirm('Apakah anda sudah mengirim SOFTCOPY POLIS ini (apabila nasabah mempunyai e-mail)?')){
						get('infoFrame').src='${path }/uw/printpolis.htm?window=transfer&spaj='+asdf;
					}
				}
			}else if(kode=='addressbilling'){
				if(cek(asdf))
					get('infoFrame').src=document.getElementById('infoFrame').src='${path}/uw/addressbilling.htm?reg_spaj='+asdf;
			}else if(kode=='tglkirim'){
				if(cek(asdf)) if(confirm("E-mail pemberitahuan pengiriman polis akan dikirimkan ke cabang. Lanjutkan?")){
							  	var flag = cektglkirimpolis(asdf);
							  	if(document.getElementById('cekkirimp').value==1){ 				
									if(confirm("Kartu belum diterima, Lanjutkan Proses?Klik OK untuk mengisi Tgl Terima Admedika, Klik Cancel untuk mengisi Tgl Kirim Polis.")){
										get('infoFrame').src='${path }/uw/viewer.htm?window=editTglTrmKrmSpaj&spaj='+asdf+'&show=1';
									}else{
										get('infoFrame').src='${path }/uw/viewer.htm?window=editTglTrmKrmSpaj&spaj='+asdf+'&show=1';
									}
								}else{
									get('infoFrame').src='${path }/uw/viewer.htm?window=editTglTrmKrmSpaj&spaj='+asdf+'&show=1';
								}
							  }else{
							  	get('infoFrame').src='${path }/uw/viewer.htm?window=editTglTrmKrmSpaj&spaj='+asdf+'&show=1';
							  }							
			}else if(kode=='tglterimaAdmedika'){
				if(cek(asdf)) get('infoFrame').src='${path }/uw/viewer.htm?window=editTglTrmKrmSpaj&spaj='+asdf+'&show=3';
			}else if(kode=='kirimlagi'){
				if(cek(asdf)) get('infoFrame').src='${path }/uw/viewer.htm?window=kirimUlangSoftcopy&spaj='+asdf;
			}else if(kode=='simasCard'){
				if(cek(asdf)) popWin('${path }/uw/viewer.htm?window=simasCard&spaj='+asdf, 480, 640);
			}else if(kode=='email'){
				if(cek(asdf)) get('infoFrame').src='${path }/uw/printpolis.htm?window=email&spaj='+asdf;
			}else if(kode=='uwinfo'){
				if(cek(asdf)) get('infoFrame').src='${path}/uw/viewer.htm?window=uwinfo&spaj='+asdf;
			}else if(kode=='validforprint'){
				if(cek(asdf)) get('infoFrame').src='${path }/uw/printpolis.htm?window=validforprint&spaj='+asdf;
			}else if(kode=='checklist'){
				if(cek(asdf)) get('infoFrame').src='${path }/checklist.htm?lspd_id=6&reg_spaj='+asdf;
			}else if(kode=='alasan'){
				if(cek(asdf)) get('infoFrame').src='${path }/uw/printpolis.htm?window=alasan&spaj='+asdf;
			}else if(kode=='upload'){
				if(cek(asdf)) get('infoFrame').src='${path }/uw/upload_nb.htm?reg_spaj='+asdf;
			}else if(kode=='uw_worksheet'){
				if(cek(asdf)) get('infoFrame').src='${path }/uw/worksheet.htm?spaj='+asdf+'&copy_reg_spaj='+copy_reg_spaj;
			}else if(kode=='endors'){
				if(cek(asdf)) get('infoFrame').src='${path }/uw/endorsenonmaterial.htm?window=endorseNonMaterial&spaj='+asdf;
			}else if(kode=='hcp'){
				if(cek(asdf)) popWin('${path}/bac/ttghcp.htm?sts=view&showSPAJ='+asdf, 500, 800); 
				//get('infoFrame').src='${path }/bac/ttghcp.htm?sts=view&showSPAJ='+asdf;
			}else if(kode=='generatepdf'){
				if(cek(asdf)) if(confirm('Generate semua dokumen polis (termasuk wording)?')){
					get('infoFrame').src='${path }/uw/printpolis.htm?window=wording&spaj='+asdf;
				}
			}else if(kode=='cari'){
				popWin('${path}/uw/spaj.htm?posisi=6&win=printpolis', 350, 450); 
				
			//cover letter
			}else if(kode=='cover'){
				if(confirm('Tekan OK untuk jenis produk banccassurance\nTekan CANCEL untuk jenis produk worksite.')){
					popWin('${path}/report/uw.htm?window=cover_letter', 480, 640);
				}else{
					popWin('${path}/report/uw.htm?window=cover_letter_worksite', 480, 640);
				}
				
			//khusus ssu/ssk, gak bisa preview dlm format lain, karena langsung file pdf
			}else if(dipilih=='9'){
				if(cek(asdf)) get('infoFrame').src='${path }/uw/printpolis.htm?window=print&spaj='+asdf+'&printType='+dipilih+'&format=file.pdf';
			
			//cetak polis
			}else if(kode=='print'){
				if(cek(asdf)){
					
					konfirm = false;
					if(dipilih == '2' || dipilih == '6'){
						konfirm = true;
					}
					
					if(dipilih == '6'){
						alert('Untuk Produk Powersave,Stable Save dan Stable Link, apabila memiliki Asuransi Tambahan, silakan memilih Endorsemen untuk Info Asuransi Tambahannya.');
					}
					
					if(dipilih == '18'){
						alert('Untuk pencetakan Surat Endorse Swine Flu Rider, harap gunakan kertas Concorde');
					}
					
					konfirm2 = true;
					if(konfirm){
						konfirm2 = confirm('Dokumen hanya dapat dicetak sekali. Lanjutkan dengan pencetakan dokumen?');
					}
				
					if(konfirm2){
						get('infoFrame').src='${path }/uw/printpolis.htm?window=print&spaj='+asdf+'&printType='+dipilih+'&referal='+referal+'&format=file.pdf';
					}
				
				} 
				
			//preview html
			}else if(kode=='printhtml'){
				if(cek(asdf)) get('infoFrame').src='${path }/uw/printpolis.htm?isviewer=true&window=print&spaj='+asdf+'&printType='+dipilih+'&referal='+referal+'&format=file.html';
				
			//preview java applet
			}else if(kode=='printjava'){
				if(cek(asdf)) get('infoFrame').src='${path }/uw/printpolis.htm?isviewer=true&window=print&spaj='+asdf+'&printType='+dipilih+'&referal='+referal+'&format=file.jasper';
			}else if(kode=='sblink'){
				if(cek(asdf)) popWin('${path}/uw/inputsblink.htm?reg_spaj='+asdf, 480, 640);
			}else if(kode=='editSpaj'){
			  if(cek(asdf)) popWin('${path}/bac/edit.htm?data_baru=true&showSPAJ='+asdf, 600, 800);
				//if(cek(asdf))
				//popWin('${path}/uw/printpolis.htm?window=cek_spv&spaj='+asdf, 600, 800)
				 /*popWin('${path}/bac/edit.htm?data_baru=true&showSPAJ='+asdf, 600, 800);
				 var user =;
                 value = prompt("Masukkan Username SPV/Pincab",user);*/
			}else if(kode=='espajdmtm'){
				if(cek(asdf)) get('infoFrame').src='${path}/reports/espajdmtm2.pdf?spaj='+asdf;
			}else if(kode=='espaj'){
				eval('ajaxManager.mallnonESPAJ')(asdf, {
				  callback:function(output) {
					DWRUtil.useLoadingMessage();
					var text = output;
				 	if(output==1){
						alert("Untuk produk stable link, powersave tidak perlu print e-spaj");
					}else{
						if(cek(asdf)) get('infoFrame').src='${path}/reports/espaj.pdf?spaj='+asdf;
					} 
				   },
				  timeout:180000,
				  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
				});
			}else if(kode=='espajgadget'){
				eval('ajaxManager.eSpajGadget')(asdf, {
					callback:function(output) {
						DWRUtil.useLoadingMessage();
					 	if(output == 0){
							alert("Hanya dapat dijalankan untuk SPAJ Gadget");
						}else{
							if(cek(asdf)) get('infoFrame').src='${path}/reports/espajonlinegadget.pdf?spaj='+asdf;
						} 
					},
					timeout:180000,
					errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
				});
			}else if(kode=='tglTerimaSpaj'){
				if(cek(asdf)) popWin('${path}/uw/viewer.htm?window=editTglTrmKrmSpaj&spaj='+asdf+'&show=0', 150, 350);
 			}else if(kode=='qcok'){
 				if(cek(asdf)) get('infoFrame').src='${path }/uw/printpolis.htm?window=qcControl&spaj='+asdf;
 			}else if(kode=='skcerdas'){
				if(cek(asdf)) get('infoFrame').src='${path}/reports/suratKonfirmasiCerdas.pdf?spaj='+asdf;
			}
			
			return false;
		}
	
	}
</script>
<body onload="setFrameSize('infoFrame', 100); setFrameSize('docFrame', 100); mulai();" onresize="setFrameSize('infoFrame', 100); setFrameSize('docFrame', 100); " style="height: 100%;">
<c:choose>
	<c:when test="${not empty transfer}">
		<div class="tabcontent">
			<c:choose>
				<c:when test="${not empty error}">
					<div id="error" style="text-transform: none;">
						${error}
					</div>
				</c:when>
				<c:when test="${not empty success}">
					<div id="success" style="text-transform: none;">
						${success}
					</div>
				</c:when>
			</c:choose>
		</div>
	</c:when>
	<c:otherwise>

		<form name="formpost" method="post">
		<div class="tabcontent">
		<table class="entry2" style="width: 98%">
			<tr>
				<th>SPAJ</th>
				<td>
		
					<select name="spaj" onchange="ajaxPesan(this.value, 6); tampilkan('info', this.value);">
 						<option value="">[--- Silahkan Pilih/Cari SPAJ ---]</option>
						<c:forEach var="s" items="${daftarSPAJ}">
							<option value="${s.REG_SPAJ }" style="background-color: ${s.BG};" 
								<c:if test="${s.REG_SPAJ eq param.spaj }">selected<c:set var="ada" value="ada" /></c:if>>${s.SPAJ_FORMATTED}
							- ${s.POLICY_FORMATTED }</option>
						</c:forEach>
					</select>
					<input type="hidden" name="testingaja" id="testingaja" value=""/>
					<input type="hidden" name="cekkirimp" id="cekkirimp" value="">
					<input type="hidden" name="copy_reg_spaj" value="">
					<input type="button" value="Cari" name="search" accesskey="C" style="width: 45px;"
						onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();"	
						onclick="tampilkan('cari', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
						<!-- iga 03/03/2020 simas card dinonaktifkan
						<input type="button" value="Simas Card" name="simasCard" accesskey="S" style="width: 80px;"
							onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();"	
							onclick="tampilkan('simasCard', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);"> 
							-->
					
				<c:choose>
					<c:when test="${sessionScope.currentUser.lca_id eq '58'}">
						<input type="button" value="Tgl Kirim" name="tglkirim" accesskey="G" style="width: 70px;"
							onmouseover="return overlib('Alt-G', AUTOSTATUS, WRAP);" onmouseout="nd();"	
							onclick="tampilkan('tglkirim', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
						<input type="button" value="Info" name="info" accesskey="I" style="width: 45px;"
						onmouseover="return overlib('Alt-I', AUTOSTATUS, WRAP);" onmouseout="nd();"
						onclick="tampilkan('info', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
						<input type="button" value="Upload" name="upload" accesskey="U"
							onmouseover="return overlib('Alt-U', AUTOSTATUS, WRAP);" onmouseout="nd();"
							onclick="tampilkan('upload', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
						<input type="button" value="edit spaj" id="editSpajController" name="editSpajController" onclick="tampilkan('editSpaj', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);"/>
						<input name="btn_sblink" type="button" value="SB Link" accesskey="S"
							onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();"
						 	onClick="tampilkan('sblink', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">			
					</c:when>
					<c:otherwise>
						<c:if test="${sessionScope.currentUser.lca_id ne '55'}">
							<c:if test="${sessionScope.currentUser.flag_approve ne 1}">
								<input type="button" value="Tgl Kirim" name="tglkirim" accesskey="G" style="width: 70px;"
									onmouseover="return overlib('Alt-G', AUTOSTATUS, WRAP);" onmouseout="nd();"	
									onclick="tampilkan('tglkirim', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
							</c:if>
							
							<input type="button" value="Info" name="info" accesskey="I" style="width: 45px;"
								onmouseover="return overlib('Alt-I', AUTOSTATUS, WRAP);" onmouseout="nd();"
								onclick="tampilkan('info', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
								
							<c:if test="${sessionScope.currentUser.lus_id eq 847}">
								<input type="button" value="Upload" name="upload" accesskey="U"
									onmouseover="return overlib('Alt-U', AUTOSTATUS, WRAP);" onmouseout="nd();"
									onclick="tampilkan('upload', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
							</c:if>
								
							<c:if test="${empty sessionScope.currentUser.cab_bank}">
								<input type="button" value="Tgl Terima Admedika" name="tglTerima" accesskey="A" style="width: 140px;"
									onmouseover="return overlib('Alt-A', AUTOSTATUS, WRAP);" onmouseout="nd();"	
									onclick="tampilkan('tglterimaAdmedika', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
								
								<input type="button" value="C. Letter" name="printcoverletter" accesskey="L"
									onmouseover="return overlib('Alt-L', AUTOSTATUS, WRAP);" onmouseout="nd();" style="width: 70px;"
									onclick="tampilkan('cover', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
									<!-- Iga 03/04/2020 penutupan valid print -->
								<!-- <input type="button" value="Valid" name="validforprint" accesskey="V"
									onmouseover="return overlib('Alt-V', AUTOSTATUS, WRAP);" onmouseout="nd();"
									onclick="tampilkan('validforprint', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);"> -->
									
								<input type="button" value="Softcopy" name="kirimlagi" accesskey="A" style="width: 90px;"
									onmouseover="return overlib('KIRIM ULANG SOFTCOPY', AUTOSTATUS, WRAP);" onmouseout="nd();"	
									onclick="tampilkan('kirimlagi', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
									
								<input type="button" value="Alasan" name="alasan" accesskey="N"
									onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();"
									onclick="tampilkan('alasan', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
		
								<input type="button" value="Upload" name="upload" accesskey="U"
									onmouseover="return overlib('Alt-U', AUTOSTATUS, WRAP);" onmouseout="nd();"
									onclick="tampilkan('upload', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
									
<!-- 								<input type="button" value="SM/Eka Sehat/HCP FAMILY" name="hcp"	accesskey="H" style="width: 160px;"
									onmouseover="return overlib('Alt-H', AUTOSTATUS, WRAP);" onmouseout="nd();" 
									onclick="tampilkan('hcp', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);"> -->
									
								<input type="button" value="Worksheet" name="uw_worksheet" 
									onmouseover="return overlib('UW WORKSHEET', AUTOSTATUS, WRAP);" onmouseout="nd();"
									onclick="tampilkan('uw_worksheet', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
									
								<input type="button" value="Endorse Non Material" name="Endorse Non Material" 
									onmouseover="return overlib('Endorse Non Materia', AUTOSTATUS, WRAP);" onmouseout="nd();"
									onclick="tampilkan('endors', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">	
							</c:if>
							<c:if test="${sessionScope.currentUser.lus_id eq 519 || sessionScope.currentUser.lus_id eq 717 || sessionScope.currentUser.lus_id eq 1097 || sessionScope.currentUser.lus_id eq 1504 
										|| sessionScope.currentUser.lus_id eq 555 || sessionScope.currentUser.lus_id eq 3676 || sessionScope.currentUser.lus_id eq 3813 || sessionScope.currentUser.lus_id eq 495
										|| sessionScope.currentUser.lus_id eq 795 || sessionScope.currentUser.lus_id eq 3376}">	
								<input type="button" value="Endorsement Polis" name="endorsment" 
									onclick="tampilkan('inputEndorsment', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);" 
									accesskey="D" onmouseover="return overlib('Alt-D',AUTOSTATUS,WRAP);" onmouseout="nd();"> 
							</c:if>
						</c:if>	
					</c:otherwise>
				</c:choose>
				</td>
				<td rowspan=2><div id="prePostError" style="display:none;"></div></td>
			</tr>
		
			<tr>
				<th>Print</th>
				<td>
					<select name="printType">
						<c:set var="grup" value=""/>
						<c:forEach var="r" items="${reportList}" varStatus="st">
							<c:choose>
								<c:when test="${r.g ne grup}">
									<c:set var="grup" value="${r.g}"/>				
									<OPTGROUP label="${r.g}">
										<option value="${r.kode}">${r.judul}</option>
								</c:when>
								<c:otherwise>
									<option value="${r.kode}">${r.judul}</option>						
								</c:otherwise>
							</c:choose>
							<c:if test="${r.g ne grup}">
								<c:set var="grup" value="${r.g}"/>				
								</OPTGROUP>
							</c:if>
						</c:forEach>
					</select> 
					
					<a href="#" onclick="return tampilkan('printhtml', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);"
						onmouseover="return overlib('Alt-H > View (HTML)', AUTOSTATUS, WRAP);" onmouseout="nd();" accesskey="H">
						<img style="border:none;" alt="HTML" src="${path}/include/image/page_html.gif"/></a>
						
					<a href="#" onclick="return tampilkan('printjava', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);"
						onmouseover="return overlib('Alt-J > View (JAVA)', AUTOSTATUS, WRAP);" onmouseout="nd();" accesskey="J">
						<img style="border:none;" alt="JAVA" src="${path}/include/image/page_java.gif"/></a>
						
					<input type="hidden" name="window" value="transfer">
					<c:choose>
						<c:when test="${sessionScope.currentUser.jn_bank eq 3}">
							<c:if test="${sessionScope.currentUser.flag_approve eq 0 and sessionScope.currentUser.cab_bank eq 'M35' }">
								<input type="button" value="Print" name="print" accesskey="P" style="width: 40px;"
								onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();"
								onclick="tampilkan('print', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
							</c:if>
						</c:when>
						<c:otherwise>
							<c:if test="${sessionScope.currentUser.flag_approve ne 1}">
							<input type="button" value="Print" name="print" accesskey="P" style="width: 40px;"
								onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();"
								onclick="tampilkan('print', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
							</c:if>
						</c:otherwise>
					</c:choose>
				
				<c:choose>
					<c:when test="${sessionScope.currentUser.lca_id eq '58'}">
						<input type="button" value="Checklist" name="checklist" accesskey="C" style="width: 70px;" 
							onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();"
							onclick="return tampilkan('checklist', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
						<input type="button" value="Transfer" name="transferTandaTerimaPolis" accesskey="T" style="width: 67px;"
							onmouseover="return overlib('Alt-T', AUTOSTATUS, WRAP);" onmouseout="nd();"
							onclick="return tampilkan('transfer', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">	
						<input type="button" value="Valid" name="validforprint" accesskey="V"
									onmouseover="return overlib('Alt-V', AUTOSTATUS, WRAP);" onmouseout="nd();"
									onclick="tampilkan('validforprint', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
						<input name="btn_espaj" type="button" value="E-SPAJ" onClick="return tampilkan('espaj', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);" accesskey="S" 
							 onmouseover="return overlib('E-SPAJ', AUTOSTATUS, WRAP);" onmouseout="nd();" style="margin-top: 3px">
						<input name="btn_espaj_gadget" type="button" value="E-SPAJ GADGET" onClick="return tampilkan('espajgadget', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);" accesskey="S" 
							 onmouseout="nd();" style="margin-top: 3px">
					</c:when> 
					<c:otherwise>
						<c:if test="${sessionScope.currentUser.lca_id ne '55'}">
							<c:if test="${empty sessionScope.currentUser.cab_bank}">
								<input type="button" value="U/W Info" name="uwinfo" 
									onclick="tampilkan('uwinfo', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);" style="width: 60px;"
									accesskey="U" onmouseover="return overlib('Alt-U', AUTOSTATUS, WRAP);" onmouseout="nd();">
								
								<input type="button" value="QC OK" name="qcok" accesskey="Q"
									onmouseover="return overlib('Alt-Q', AUTOSTATUS, WRAP);" onmouseout="nd();" 
                                     onclick="tampilkan('qcok', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);" style="width: 60px;">
                                     
								<input type="button" value="Email" name="email" accesskey="E" style="width: 42px;" 
									onmouseover="return overlib('Alt-E', AUTOSTATUS, WRAP);" onmouseout="nd();"
									onclick="tampilkan('email', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
					
								<input type="button" value="Transfer" name="transferTandaTerimaPolis" accesskey="T" style="width: 67px;"
									onmouseover="return overlib('Alt-T', AUTOSTATUS, WRAP);" onmouseout="nd();"
									onclick="return tampilkan('transfer', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
								
								<input type="button" value="Generate" name="generatePDF" accesskey="F" style="width: 70px;" 
									onmouseover="return overlib('Alt-F', AUTOSTATUS, WRAP);" onmouseout="nd();"
									onclick="return tampilkan('generatepdf', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
								
								<input name="btn_espaj" type="button" value="E-SPAJ" onClick="return tampilkan('espaj', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);" accesskey="S" 
							 		onmouseover="return overlib('E-SPAJ', AUTOSTATUS, WRAP);" onmouseout="nd();" style="margin-top: 3px">
							 		
							 	<input name="btn_espaj_dmtm" type="button" value="E-SPAJ DMTM" onClick="return tampilkan('espajdmtm', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);" accesskey="S" 
							 		onmouseover="return overlib('E-SPAJ', AUTOSTATUS, WRAP);" onmouseout="nd();" style="margin-top: 3px">
							 
							 	<input name="btn_espaj_gadget" type="button" value="E-SPAJ GADGET" onClick="return tampilkan('espajgadget', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);" accesskey="S" 
							 		onmouseout="nd();" style="margin-top: 3px">
							 		
							 	<input name="btn_espaj" type="button" value="SURAT KONFIRMASI CERDAS" onClick="return tampilkan('skcerdas', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);" accesskey="S" 
							 		onmouseover="return overlib('SURAT KONFIRMASI', AUTOSTATUS, WRAP);" onmouseout="nd();" style="margin-top: 3px">	
							 	
							 	<input type="button" value="Tgl Terima Spaj" name="tglTerima"
									onclick="tampilkan('tglTerimaSpaj', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);"
									accesskey="Z" onmouseover="overlib('Edit Tanggal Terima Spaj', AUTOSTATUS, WRAP);" onmouseout="nd();">
							</c:if>
							
							<c:if test="${sessionScope.currentUser.jn_bank eq 2}">
								<input type="button" value="Transfer" name="transferTandaTerimaPolis" accesskey="T" style="width: 67px;"
								onmouseover="return overlib('Alt-T', AUTOSTATUS, WRAP);" onmouseout="nd();"
								onclick="return tampilkan('transfer', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">	
							</c:if>
							
							<c:if test="${sessionScope.currentUser.flag_approve ne 1}">
								<input type="button" value="Checklist" name="checklist" accesskey="C" style="width: 70px;" 
									onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();"
									onclick="return tampilkan('checklist', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
									<input type="button" value="Validasi Biaya Materai" name="vmaterai" accesskey="O" style="width: 180px;" 
									onmouseover="return overlib('Alt-M', AUTOSTATUS, WRAP);" onmouseout="nd();"
									onclick="return tampilkan('vmaterai');">
							</c:if>
												
							<c:if test="${isSupervisorOrPincabOrUw}">
								<input type="button" value="Otorisasi Print Ulang" name="otorisasiPrintUlang" accesskey="O" style="width: 150px;" 
									onmouseover="return overlib('Alt-O', AUTOSTATUS, WRAP);" onmouseout="nd();"
									onclick="return tampilkan('otorisasi');">
							</c:if>					
							
							<c:if test="${sessionScope.currentUser.lde_id eq '01' or sessionScope.currentUser.lde_id eq '11'}">
							<input type="button" value="Edit Alamat Penagihan" style="width: 160px;"
									onclick="return tampilkan('addressbilling', document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
								<label for="banyakMaunya"><input type="checkbox" class="noBorder" id="banyakMaunya" name="banyakMaunya" value="1">Simas Prima Referal</label>
							</c:if>
						</c:if>	
					</c:otherwise>
				</c:choose>	
				</td>
			</tr>
			<tr>
				<td colspan="3">
		
					<table style="margin: 0 0 0 0; padding: 0 0 0 0; width: 100%;">
						<tr>
							<c:choose>
								<c:when test="${sessionScope.currentUser.wideScreen}">
									<td style="width: 60%;">
										<c:choose>
											<c:when test="${not empty error}">
												<div id="error" style="text-transform: none;">
													${error}
												</div>
											</c:when>
											<c:when test="${not empty success}">
												<div id="success" style="text-transform: none;">
													${success}
												</div>
											</c:when>
										</c:choose>
										
										<iframe name="infoFrame" id="infoFrame" width="100%" src="${path}/uw/printpolis.htm?window=info">
											Please Wait...
										</iframe>
									</td>
									<td style="width: 40%;">
										<iframe src="" name="docFrame" id="docFrame" width="100%">E-Lions</iframe>
									</td>
								</c:when>
								<c:otherwise>
									<td>
										<c:choose>
											<c:when test="${not empty error}">
												<div id="error" style="text-transform: none;">
													${error}
												</div>
											</c:when>
											<c:when test="${not empty success}">
												<div id="success" style="text-transform: none;">
													${success}
												</div>
											</c:when>
										</c:choose>
										
										<iframe name="infoFrame" id="infoFrame" width="100%" src="${path}/uw/printpolis.htm?window=info">
											Please Wait...
										</iframe>
									</td>
								</c:otherwise>
							</c:choose>
						</tr>
					</table>		
				</td>
			</tr>
		</table>
		</div>
		</form>

	</c:otherwise>
</c:choose>
<c:if test="${not empty warning}"><script>alert('${warning}');</script></c:if>
</body>
<%@ include file="/include/page/footer.jsp"%>