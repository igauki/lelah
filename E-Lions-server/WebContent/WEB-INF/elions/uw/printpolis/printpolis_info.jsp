<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
	</head>
	<body style="height: 100%;">
		<c:choose>
			<c:when test="${sessionScope.currentUser.jn_bank eq -1}"> <!-- inputan biasa -->
				<div id="success">
					INFO:
					<br/>- Untuk Produk POWERSAVE BANK SINARMAS, harap cek penggunaan kertas khusus Bank SINARMAS 
					<br/>- Untuk Produk PRIVILEGE SAVE - UOB, harap cek penggunaan kertas khusus Bank UOB
					<br/>- Untuk Produk PA : SUPER PROTECTION, harap diperhatikan bahwa SSU / SSK sudah PRE-PRINTED (tidak perlu dicetak ulang) 
				</div>
			</c:when>
			<c:when test="${sessionScope.currentUser.jn_bank eq 0 or sessionScope.currentUser.jn_bank eq 1}"> <!-- inputan BII -->
				<div id="success">
					INFO:
					<br/>- Silahkan Melakukan Pencetakan Terhadap Dokumen-Dokumen Berikut:
					<br/>&nbsp;&nbsp;&nbsp;1. Polis Asuransi
					<br/>&nbsp;&nbsp;&nbsp;2. Syarat-Syarat Umum Polis
					<br/>&nbsp;&nbsp;&nbsp;3. Tanda Terima Polis
				</div>
			</c:when>
			<c:when test="${sessionScope.currentUser.jn_bank eq 2}"> <!-- inputan bank sinarmas -->
				<div id="success">
					INFO:
					<br/>- Silahkan Melakukan Pencetakan Terhadap Dokumen-Dokumen Berikut:
					<br/>&nbsp;&nbsp;&nbsp;1. Sertifikat Asuransi
					<br/>&nbsp;&nbsp;&nbsp;2. Tanda Terima Sertifikat
					<br/>&nbsp;&nbsp;&nbsp;3. Surat Perjanjian Pinjaman Polis
				</div>
			</c:when>
			<c:when test="${sessionScope.currentUser.jn_bank eq 3}"> <!-- inputan sinarmas sekuritas -->
				<div id="success">
					INFO:
					<br/>- Silahkan Melakukan Pencetakan Terhadap Dokumen-Dokumen Berikut:
					<br/>&nbsp;&nbsp;&nbsp;1. Sertifikat Asuransi
					<br/>&nbsp;&nbsp;&nbsp;2. Tanda Terima Sertifikat
					<br/>&nbsp;&nbsp;&nbsp;3. Surat Perjanjian Pinjaman Polis
				</div>
			</c:when>
			<c:otherwise>-
			</c:otherwise>
		</c:choose>

		<div id="success">
			INFO WARNA PADA PILIHAN REG SPAJ / NO POLIS:
			<br/>&nbsp;&nbsp;&nbsp;1. PUTIH  : Menandakan Polis/Sertifikat belum pernah dicetak
			<br/>&nbsp;&nbsp;&nbsp;2. KUNING : Menandakan Polis/Sertifikat sudah pernah dicetak dan harus melalui otorisasi pencetakan ulang terlebih dahulu
		</div>

		<c:if test="${sessionScope.currentUser.jn_bank eq 2 or sessionScope.currentUser.jn_bank eq 16}">
			<div id="success" class="panes">
				DAFTAR POLIS YANG BELUM DI PRINT
				<table class="entry2" style="width: auto;">
					<tr>
						<th>No.</th>
						<th>SPAJ</th>
						<th>Polis</th>
						<th>User Input</th>
						<th>Tgl Transfer Ke Print Polis</th>
					</tr>
					<c:forEach items="${datapolis}" var="a" varStatus="st">
						<tr>
							<td>
								${st.index+1}
							</td>
							<td>
								${a.SPAJ}
							</td>
							<td>
								${a.POLIS}
							</td>
							<td>
								${a.USER_INPUT}
							</td>
							<td>
								<fmt:formatDate value="${a.TGL_TRANSFER_PRINTPOLIS}" pattern="dd/MM/yyyy"/>
							</td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</c:if>
	</body>
	<%@ include file="/include/page/footer.jsp"%>