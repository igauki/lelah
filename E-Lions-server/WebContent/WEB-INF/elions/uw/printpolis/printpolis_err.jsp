<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript">
	var pesan='';
</script>
<body style="height: 100%;">
<div id="contents">  
	<div id="error" style="text-transform: none;">ERROR:<br>
		<c:forEach var="err" items="${error}">
			- <c:out value="${err}" escapeXml="false" />
			<br />
			<script>
				pesan += '${err}\n';
			</script>
		</c:forEach>
	</div>
</div>
<script>
	if(pesan != '') alert(pesan);
	<c:if test="${not empty printUlang}">
		if(parent.document.getElementById('infoFrame'))
			parent.document.getElementById('infoFrame').src='${path }/uw/printpolis.htm?window=printulangpolis&jenis=${jenis}&spaj=${printUlang}&printType=2&format=file.pdf';
	</c:if>
	<c:if test="${not empty printNeedAuthorization}">
		if(parent.document.getElementById('infoFrame'))
			parent.document.getElementById('infoFrame').src='${path }/uw/printpolis.htm?window=printotorisasipolis&jenis=1&spaj=${printNeedAuthorization}&printType=6&format=file.pdf';
	</c:if>
</script>
</body>
<%@ include file="/include/page/footer.jsp"%>
