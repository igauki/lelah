<%@ include file="/include/page/header.jsp"%>
<style type="text/css">
	html,body{
	     overflow: hidden;
	}
</style>
<script>
	function generateDokumen(){
		var elemen = document.getElementsByName('report');
		for(i=0; i<elemen.length; i++){
			if(elemen[i].checked){
				var url = '${path}/uw/printpolis.htm?window=print&spaj=${spaj}&printType='+elemen[i].value+'&format=file.pdf&generate=true';
				document.getElementById('hiddenPrintFrame'+i).src = url;
			}
		}	
	}	
</script>
<body style="height: 100%;">
	<fieldset>
		<form name="formpost">
			<c:set var="checkAll">
			    <input class="noBorder" type="checkbox" onclick="checkAll('report')" checked="checked">
			</c:set>		
			<display:table id="r" name="reportList" class="displaytag" style="width: auto;">
				<display:column title="${checkAll}" style="text-align: center;">
					<input class="noBorder" type="checkbox" name="report" value="${r.kode}" checked="checked">
					<iframe id="hiddenPrintFrame${r_rowNum-1}" style="visibility: hidden; display: none;">
						Please Wait...
					</iframe>
				</display:column>
				<display:column title="Dokumen" style="text-align: left;" property="judul" />
				<display:column title="Tanggal Cetak"/>
			</display:table>
		
			<br>

			<input type="button" value="Generate" onclick="generateDokumen()"
				accesskey="G" onmouseover="return overlib('Alt-G', AUTOSTATUS, WRAP);" onmouseout="nd();">

		</form>
	</fieldset>
</body>
<%@ include file="/include/page/footer.jsp"%>