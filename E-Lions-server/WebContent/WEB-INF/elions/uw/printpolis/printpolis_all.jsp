<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript">
	function konfirmasi(){
		if(confirm('Anda yakin ingin mencetak ulang polis?\nHarap pastikan anda mempunyai Adobe Reader sebelum melanjutkan.')){
			if(popWinModal('${path}/common/menu.htm?frame=password&tipe=1&caller=viewer', 120, 300, window)){
				if(popWinModal('${path}/common/menu.htm?frame=ulanganframe&jenis=1&status=1&spaj=${param.spaj }', 220, 440, window)){
					return true;
				}
			}else{
				alert('Maaf, tetapi anda tidak mempunyai otorisasi');
			}
		}	
		return false;
	}
</script>
<body style="height: 100%;">
<div id="contents">  
	<form name="formpost" method="post">
		<input type="hidden" name="window" value="print">
		<input type="hidden" name="printType" value="all">
		<input type="hidden" name="spaj" value="${param.spaj}">
		<input type="hidden" name="printProgress" value="${printProgress}">
		<fieldset>
			<legend>Proses Print Polis untuk SPAJ <elions:spaj nomor="${param.spaj}"/></legend>
			<c:set var="grup" value="-"/>				
			<c:set var="ketemu" value="false"/>
			<table class="simple" style="width: 500px; ">
				<c:forEach var="r" items="${reportList}" varStatus="st">
					<c:if test="${not empty r.g}">
						<c:set var="hitung" value="${r.kode}"/>
						<c:choose>
							<c:when test="${r.g ne grup}">
								<tr style="height: 23px;">
									<th width="30%">${r.g}</th>
									<td class="subheader" width="40%">${r.judul}</td>
									<td nowrap="nowrap">
										<c:choose>
											<c:when test="${r.kode eq printProgress}">
												<c:set var="ketemu" value="true"/>
												<img src="${path }/include/image/prev.gif" alt="Ekalife"/> <span class="error">PRINTING. Please Wait...</span>
											</c:when>
											<c:when test="${ketemu eq \"false\" and (printProgress ne \"-1\" and printProgress ne \"all\") }">
												<img src="${path }/include/image/18_filetick.gif" alt="Ekalife"/>
											</c:when>
										</c:choose>
									</td>
								</tr>
								<c:set var="grup" value="${r.g}"/>				
							</c:when>
							<c:otherwise>
								<tr style="height: 23px;">
									<th width="30%"></th>
									<td class="subheader" width="40%">${r.judul}</td>
									<td nowrap="nowrap">
										<c:choose>
											<c:when test="${r.kode eq printProgress}">
												<c:set var="ketemu" value="true"/>
												<img src="${path }/include/image/prev.gif" alt="Ekalife"/> <span class="error">PRINTING. Please Wait...</span>
											</c:when>
											<c:when test="${ketemu eq \"false\" and (printProgress ne \"-1\" and printProgress ne \"all\") }">
												<img src="${path }/include/image/18_filetick.gif" alt="Ekalife"/>
											</c:when>
										</c:choose>
									</td>
								</tr>
							</c:otherwise>
						</c:choose>
					</c:if>
				</c:forEach>

				<tr>
					<td style="text-align: center;" colspan="3">
					
						<c:if test="${not empty errors}">
							<div id="error" style="text-align: center;">ERROR:<br>
								<c:forEach var="error" items="${errors}">
									- <c:out value="${error}" escapeXml="false" />
									<c:if test="${error eq \"Maaf, tetapi polis ini sudah pernah dicetak.\"}">
										<c:set var="sudahPernahDicetak" value="true"/>
									</c:if>
								<br />
								</c:forEach>
							</div>
						</c:if>

						<c:choose>
							<c:when test="${sudahPernahDicetak eq \"true\"}">
								<input type="hidden" name="authorized" value="true">
								<input type="submit" value="Cetak Ulang Polis" name="ulang" onclick="return konfirmasi();">
							</c:when>
							<c:when test="${printProgress eq \"-1\"}">
								<input type="submit" value="Start Printing" name="start">
								<img src="${path }/include/image/prev.gif" alt="Ekalife"/> <span class="error">Tekan untuk memulai Pencetakkan</span>
							</c:when>
							<c:when test="${not empty finish}">
								<div id="success" style="text-transform: none;">
									Proses Print Polis Selesai
									<br>Harap gunakan menu Print diatas apabila ada dokumen yang belum tercetak.
								</div>
							</c:when>
							<c:when test="${printProgress eq hitung}">
								<input type="submit" value="Finish" name="finish">
							</c:when>
							<c:otherwise>
								<input type="submit" value="Next" name="next">
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</table>
			<c:if test="${not empty param.ulang}">
				<c:set var="authorizedToPrint" value="&authorized=true" />
			</c:if>
			<c:if test="${printProgress ne \"finish\" and printProgress ne \"all\" and printProgress ne \"-1\"}">
				<iframe name="infoFrame" id="infoFrame" width="5" height="5"
						src="${path }/uw/printpolis.htm?window=print&spaj=${param.spaj}&printType=${printProgress}&format=file.pdf${authorizedToPrint}">
					Please Wait...
				</iframe>
			</c:if>
		</fieldset>
	</form>
</div>
</body>
<%@ include file="/include/page/footer.jsp"%>
