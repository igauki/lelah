<%@ include file="/include/page/header.jsp"%>
<script>
	function go_to(jenis, path){
		var spaj = document.formpost.spaj.value;
		if(spaj=='' && 'cover_letter'!=jenis){
			alert('Silahkan pilih salah satu nomor SPAJ terlebih dahulu');
		}else{
			if('cari' == jenis){
				popWin('${path}/'+path, 350, 450); 
			}else if('polis' == jenis){
				var format = document.formpost.f_ormat;
				format = format[0].checked?'pdf':format[1].checked?'html':'';
				document.getElementById('infoFrame').src='${path}/report/polis.'+format+'?window=view&jenis='+path+'&spaj='+spaj;
			}else if('info' == jenis){
				document.getElementById('infoFrame').src='${path}/'+path+'?showSPAJ='+spaj;
			}else if('cover_letter' == jenis){
				document.getElementById('infoFrame').src='${path}/'+path;
			}else if('transfer' == jenis){
				if(confirm('Anda sudah melakukan cek terhadap seluruh dokumen polis?\nPolis akan ditransfer ke PRINT POLIS.'))
					document.getElementById('infoFrame').src='${path}/'+path+'&spaj='+spaj;
			}
		}
	}
</script>
<body style="height: 100%;" onload="setFrameSize('infoFrame', 68);" onresize="setFrameSize('infoFrame', 68);">
	<form method="post" name="formpost">
		<table class="entry2">
			<tr>
				<th style="width: 115px;" rowspan="2">
					<select name="spaj" size="30" style="width: 110px;" id="comboSpaj">
						<c:forEach var="s" items="${daftarSpaj}" varStatus="st">
							<option value="${s.REG_SPAJ }" style="background-color: ${s.BG};"
								<c:if test="${st.index eq 0}">selected</c:if>>${s.SPAJ_FORMATTED}
							</option>
						</c:forEach>
					</select>
					<br/>
					<input type="button" value="Cari" onclick="go_to('cari', 'uw/spaj.htm?posisi=9&win=printpolis');" style="width: 110px;">
				</th>
				<td>
						<table class="entry2">
							<tr>
								<th>Dokumen</th>
								<td colspan="3">
									<c:forEach var="r" items="${reportList}" varStatus="st">
										<input type="button" value="${r.key}" onclick="go_to('polis', '${r.key}');"
												onmouseover="return overlib('${r.value}', AUTOSTATUS, WRAP);" onmouseout="nd();">
									</c:forEach>
								</td>
							</tr>
							<tr>
								<th>Format</th>
								<td>
									<label for="pdf">
										<input type="radio" id="pdf" name="f_ormat" class="noBorder" onClick="createCookie('elions.formatPolis', 'pdf', 30);"
											<c:if test="${formatPolis eq \"pdf\"}">checked="checked"</c:if>>PDF
									</label>
									<label for="html">
										<input type="radio" id="html" name="f_ormat" class="noBorder" onClick="createCookie('elions.formatPolis', 'html', 30);"
											<c:if test="${formatPolis eq \"html\"}">checked="checked"</c:if>>HTML
									</label>
								</td>
								<th>Proses</th>
								<td>
									<input type="button" value="Info" 				onclick="go_to('info', 'uw/view.htm');"
										onmouseover="return overlib('Informasi Polis', AUTOSTATUS, WRAP);" onmouseout="nd();">
									<input type="button" value="Cover Letter" 	onclick="go_to('cover_letter', 'report/polis.htm?window=cover_letter');"
										onmouseover="return overlib('Cetak Cover Letter', AUTOSTATUS, WRAP);" onmouseout="nd();">
									<input type="button" value="Transfer" 		onclick="go_to('transfer', 'report/polis.htm?window=transfer_printpolis');"
										onmouseover="return overlib('Transfer ke PRINT POLIS', AUTOSTATUS, WRAP);" onmouseout="nd();">
								</td>
							</tr>
						</table>
				</td>
			</tr>
			<tr>
				<th>
					<iframe name="infoFrame" id="infoFrame" width="100%" src="${path}/uw/spaj.htm?posisi=9&win=printpolis">
						Please Wait...
					</iframe>
				</th>
			</tr>
		</table>
	</form>
</body>
<script>
	document.formpost.spaj.size=${comboSize};
</script>
<%@ include file="/include/page/footer.jsp"%>