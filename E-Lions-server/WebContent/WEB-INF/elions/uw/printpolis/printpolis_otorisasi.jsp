<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path}/include/js/disableKeys.js"></script>
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script>
	hideLoadingMessage();
	
	function cari(){
		if(trim(document.formpost.kata.value)=='') return false;
		else createLoadingMessage();	
	}
</script>
</head>
<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Otorisasi Print Ulang Sertifikat</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form id="formpost" name="formpost" action="${path}/uw/printpolis.htm?window=otorisasi" method="post">
					<table class="entry1">
						<tr>
							<th>Cari:</th>
							<th class="left">
								<select name="tipe">
									<option value="0" <c:if test="${param.tipe eq \"0\" }">selected</c:if>>No. SPAJ</option>
									<option value="1" <c:if test="${param.tipe eq \"1\" }">selected</c:if>>No. Polis</option>
									</select>
								<select name="pilter">
									<option <c:if test="${\"LIKE%\" eq param.pilter}">selected</c:if> value="LIKE%">LIKE%</option>
									<option <c:if test="${\"%LIKE\" eq param.pilter}">selected</c:if> value="%LIKE">%LIKE</option>
									<option <c:if test="${\"%LIKE%\" eq param.pilter}">selected</c:if> value="%LIKE%">%LIKE%</option>
									<option <c:if test="${\"LT\" eq param.pilter}">selected</c:if> value="LT"><</option>
									<option <c:if test="${\"LE\" eq param.pilter}">selected</c:if> value="LE"><=</option>
									<option <c:if test="${\"EQ\" eq param.pilter}">selected</c:if> value="EQ">=</option>
									<option <c:if test="${\"GE\" eq param.pilter}">selected</c:if> value="GE">>=</option>
									<option <c:if test="${\"GT\" eq param.pilter}">selected</c:if> value="GT">></option>
								</select>					
								<input type="text" name="kata" size="35" value="${param.kata }" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
								<input type="submit" name="search" value="Search" onclick="return cari();"
									accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
							</th>
						</tr>
					</table>
					<table>
					<tr>
					<td align="left">
					<a href="${path}/uw/printpolis.htm?window=otorisasi&action=prev&dari=${dari}&sampai=${sampai}"> << Previous</a> |
					<a href="${path}/uw/printpolis.htm?window=otorisasi&action=next&dari=${dari}&sampai=${sampai}">Next >> </a>
					<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					<td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					<td align="right"></td>
					<td>Page ${myself} of ${page}</td>
					</tr>
				    </table>
					
					
					<table class="entry2" style="width: auto;">
						<tr>
							<th>Daftar Polis</th>
							<td>
								<table class="displaytag" style="width: auto;">
									<tr>
										<th>Nomor Register</th>
										<th>Nomor Polis</th>
										<th>Tanggal Cetak Terakhir</th>
										<th>Pilih</th>
									</tr>
									<c:forEach items="${daftarPolis}" var="a" varStatus="st">
										<tr>
											<td>
												${a.reg_spaj}
											</td>
											<td>
												${a.mspo_policy_no_format}
											</td>
											<td>
												<fmt:formatDate value="${a.mspo_date_print}" pattern="dd/MM/yyyy (hh:mm)"/>
											</td>
											<td>
												<input type="button" value="Pilih" onclick="document.getElementById('spaj').value = '${a.reg_spaj}';">
											</td>
										</tr>
									</c:forEach>
								</table>
							</td>
						</tr>
						<tr>
							<th>Alasan Print Ulang</th>
							<td>
								<input type="text" id="spaj" name="spaj" value="" class="readOnly" size="15"><br/>
								<textarea rows="3" cols="50" name="alasan"></textarea><br/>
								<input type="submit" name="save" value="Save" onclick="return confirm('Otorisasi ulang pencetakan sertifikat ini?');">
								<c:if test="${not empty pesanError}">
									<br/>
									<div id="error">${pesanError}</div>
								</c:if>
							</td>
						</tr>
					</table>
				</form>
			</div>
			
		</div>
	</div>

</body>
</html>
<c:if test="${not empty pesanError}">
<script>alert('${pesanError}');</script>
</c:if>