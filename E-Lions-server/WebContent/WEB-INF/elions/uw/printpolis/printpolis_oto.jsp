<%@ include file="/include/page/header.jsp"%>
<script>

</script>
<body style="height: 100%;" onload="document.formpost.password.focus();">
	<form method="post" name="formpost">
		<table class="entry2">
			<tr>
				<th style="width: 150px;">Tanggal</th>
				<td>
					<input name="spaj" type="hidden" value="${spaj}">
					<input size="20" style="text-align: center;" class="readOnly" name="tgl" type="text" value="<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy hh:mm"/>" readonly="readonly">
					<input size="30" class="readOnly" name="status" type="text" value="${sessionScope.currentUser.name}" readonly="readonly">
				</td>
			</tr>
			<tr>
				<th>User Otorisasi</th>
				<td>
					<select name="otorotor">
						<option value="">-</option>
						<c:forEach var="d" items="${daftarOtor}">
							<option value="${d.key}">${d.value}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<th>Silahkan Masukkan Password Otorisasi Atasan / SPV Anda</th>
				<td>
					<input size="53" name="password" type="password" value="">
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<input type="submit" name="save" value="Print Sertifikat">

					<c:if test="${not empty err}">
						<div id="error">${err}</div>
					</c:if>
				</td>
			</tr>
		</table>
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>