<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path}/include/js/disableKeys.js"></script>
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script>
	hideLoadingMessage();
	
	function konfirmasi(jenis){
		if(jenis=='kirim'){
			var pesan = 'Apakah anda sudah mengecek semua dokumen?\nApabila anda melanjutkan, softcopy polis akan dikirimkan ke nasabah.';
			pesan += '\n\nKirim E-mail Softcopy?';
		}else if(jenis=='test'){
			var pesan = 'Softcopy Polis hanya akan dikirimkan ke e-mail yang tercantum di kolom CC. (Untuk validasi saja)';
			pesan += '\n\nKirim E-mail Softcopy?';
		}
		
		return confirm(pesan);	
	}
	
</script>
</head>
<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">E-mail Softcopy</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<c:if test="${not empty cmd.errorMessages}">
					<div id="error" style="text-align: left;">
						ERROR:<br>
						<c:forEach var="error" items="${cmd.errorMessages}">
							- <c:out value="${error}" escapeXml="false" />
							<br />
						</c:forEach>
					</div>
				</c:if>
				<c:if test="${not empty success}">
					<div id="success" style="text-align: left;">
						${success}
					</div>
					<script>alert('${success}');</script>
				</c:if>
				<form id="formpost" name="formpost" method="post" enctype="multipart/form-data">
					<fieldset>
						<legend>E-mail dikirim ke</legend>
						<table class="entry2">
							<tr>
								<th>
									SPAJ
								</th>
								<td>
									<input type="text" name="spaj" size="20" value="${cmd.spaj}" style="text-transform: none;" readonly="readonly" class="readOnly">
								</td>
							</tr>
							<tr>
								<th>
									To:
								</th>
								<td>
									<input type="text" name="emailto" size="100" value="${cmd.emailto}" style="text-transform: none;">
								</td>
							</tr>
							<tr>
								<th>
									E-mail Agen:
								</th>
								<td>
									<input type="text" name="emailcc" size="100" value="${cmd.emailcc}" style="text-transform: none;">
									<br/><span class="info" style="text-transform: none">* Untuk > 1 penerima, pisahkan dengan tanda (;)</span>
								</td>
							</tr>
<!--							<tr>-->
<!--								<th>-->
<!--									E-mail Agen:-->
<!--								</th>-->
<!--								<td>-->
<!--									<input type="text" name="emailcc2" size="100" value="${emailcc2}" style="text-transform: none;">-->
<!--								</td>-->
<!--							</tr>-->
							<tr>
								<th>
									Subject:
								</th>
								<td>
									<input type="hidden" name="emailsubject" value="${cmd.emailsubject}">
									${cmd.emailsubject}
<!--									<input type="text" name="emailsubject" size="120" value="${cmd.emailsubject}" style="text-transform: none;">-->
								</td>
							</tr>
							<tr>
								<th>
									Message:
								</th>
								<td>
									<input type="hidden" name="emailmessage" value="${cmd.emailmessage}">
									${cmd.emailmessage}
<!--									<textarea rows="10" cols="120" name="emailmessage" style="text-transform: none;">${cmd.emailmessage}</textarea>-->
								</td>
							</tr>
							<tr>
								<th>File SPAJ : </th>
								<td>
									<input type="file" name="file1" size="50" /> Judul: <input type="text" name="namafile1" value="Dokumen SPAJ" size="40"><br/>
									<input type="file" name="file2" size="50" /> Judul: <input type="text" name="namafile2" value="Proposal Unit-Link" size="40"><br/>
									<input type="file" name="file3" size="50" /> Judul: <input type="text" name="namafile3" value="Dokumen Tambahan" size="40">
								</td>
							</tr>
							<tr>
								<th></th>
								<td>
									<c:if test="${not empty errors}">
										<div id="error">${errors}</div>
									</c:if>
									<div id="success" style="text-transform: none;">
										INFO:
										<br/>- Silahkan tekan tombol Browse diatas untuk menambah max. 3 dokumen tambahan untuk dikirim ke nasabah
										<br/>- Kolom JUDUL disebelah kanan merupakan tulisan yang tertulis di DAFTAR ISI softcopy
									</div>
									<input type="submit" name="kirim" value="Kirim" onclick="return konfirmasi('kirim');"
										accesskey="V" onmouseover="return overlib('Alt-V', AUTOSTATUS, WRAP);" onmouseout="nd();">
								</td>
							</tr>
						</table>
					</fieldset>				
				</form>
			</div>
			
		</div>
	</div>

	<script>
		<c:if test="${not empty param.sukses}">
			alert('Kurs berhasil diinput');
		</c:if>
		<c:if test="${not empty kursBelomAda}">
			alert('${kursBelomAda}');
		</c:if>
	</script>
	
</body>
</html>