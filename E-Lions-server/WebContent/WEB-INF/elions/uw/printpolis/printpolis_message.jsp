<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript">
	var pesan='';
</script>
<body style="height: 100%;">
<div id="contents">  
	<c:choose>
		<c:when test="${not empty error}">
			<div id="error" style="text-transform: none;"></div>
			<script>
				pesan += 'PESAN:<br>';
				<c:forEach var="err" items="${error}">
					pesan += '- ${err}<br>';
				</c:forEach>
				document.getElementById('error').innerHTML=pesan;
			</script>
		</c:when>
		<c:otherwise>
			<div id="success" style="text-transform: none;"></div>
			<script>
				<c:choose>
					<c:when test="${success.jenis eq \"TRANSFER KE PRINT POLIS\"}">
							parent.removeOptionFromSelect('comboSpaj');
							pesan += 'SPAJ nomor ${success.spaj} berhasil ditransfer ke PRINT POLIS.<br>';
							pesan += '<br>Daftar Dokumen yang dihasilkan:<br>';
							<c:forEach var="suc" items="${success.successList}">
								<c:if test="${suc.key eq \"true\"}">
									pesan += '- ${suc.value}<br>';
								</c:if>
							</c:forEach>
							pesan += '<br>Daftar Dokumen yang tidak dihasilkan:<br>';
							<c:forEach var="suc" items="${success.successList}">
								<c:if test="${suc.key eq \"false\"}">
									pesan += '- ${suc.value}<br>';
								</c:if>
							</c:forEach>
					</c:when>
					<c:when test="${success.jenis eq \"TRANSFER KE FILLING\"}">
							parent.removeOptionFromSelect('comboSpaj');
							pesan += '${success.successMessage}';
					</c:when>
					<c:when test="${success.jenis eq \"E-MAIL SOFTCOPY\"}">
							pesan += '${success.successMessage}';
					</c:when>
					<c:otherwise></c:otherwise>
				</c:choose>
				document.getElementById('success').innerHTML=pesan;
				pesan=''; //kalau mau di-alert, delete baris ini;
			</script>
		</c:otherwise>
	</c:choose>
</div>
<script>
	if(pesan != '') {
		pesan = pesan.replace(/<br>/g,"\n");
		alert(pesan);
	}
	<c:if test="${not empty redirectTo}">
		window.location='${redirectTo}';
	</c:if>
</script>
</body>
<%@ include file="/include/page/footer.jsp"%>
