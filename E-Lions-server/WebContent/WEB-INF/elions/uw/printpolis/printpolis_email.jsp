<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script>
	function konfirmasi(elemen){
		if(trim(elemen.value)=='') return confirm('Kirim E-mail tanpa Attachment SPAJ?');
		return confirm('Kirim E-mail dengan Attachment SPAJ?');
	}
</script>
</head>
<BODY onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">E-mail Softcopy</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" style="text-align: center;" enctype="multipart/form-data">
					<table class="entry2">
						<tr>
							<th>Silahkan upload file SPAJ : </th>
							<td>
								<input type="file" name="file" />
							</td>
						</tr>
						<tr>
							<th></th>
							<td>
								<input type="submit" name="email" value="Kirim" onclick="return konfirmasi(document.formpost.file);">
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>

</form>
</body>
<%@ include file="/include/page/footer.jsp"%>