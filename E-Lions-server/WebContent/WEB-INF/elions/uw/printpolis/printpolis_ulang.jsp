<%@ include file="/include/page/header.jsp"%>
<script>
	function cek(pw, inp){
		if(pw.value.toUpperCase()==inp.value.toUpperCase()){
			if('${param.caller}'=='viewer'){
				window.returnValue = true;
			}
		}else{
			window.returnValue = false;
		}
		window.close();
	}
	function tampol(palelu){
		document.getElementById('alasan1').style.display = 'none';
		document.getElementById('alasan2').style.display = 'none';
		if(palelu){
			if(palelu == 1){ //print ulang polis
				document.getElementById('alasan1').style.display = 'block';
				document.getElementById('alasan2').style.display = 'none';
			}else if(palelu == 3){ //print endors polis
				document.getElementById('alasan1').style.display = 'none';
				document.getElementById('alasan2').style.display = 'block';
			}else if (palelu==4){
				if(cek(asdf)) get('infoFrame').src='${path }/uw/printpolis.htm?window=print&spaj='+asdf+'&printType='+9+'&format=file.pdf';
			}
		}
	}
</script>
<body style="height: 100%;" onload="document.formpost.password.focus(); tampol(${jenis});">
	<form method="post" name="formpost">
		<table class="entry2">
			<tr>
				<th style="width: 150px;">Tanggal</th>
				<td>
					<input name="spaj" type="hidden" value="${spaj}">
					<input size="20" style="text-align: center;" class="readOnly" name="tgl" type="text" value="<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy hh:mm"/>" readonly="readonly">
					<input size="30" class="readOnly" name="status" type="text" value="${sessionScope.currentUser.name}" readonly="readonly">
				</td>
			</tr>
			<tr>
				<th>Jenis</th>
				<td>
					<c:choose>
						<c:when test="${sessionScope.currentUser.lde_id eq '01' or sessionScope.currentUser.lde_id eq '11'}">
							<select name="penis" onchange="tampol(this.value);">
								<option value="">-</option>
								<c:forEach items="${daftarJenis}" var="d">
									<option value="${d.key}" 
										<c:if test="${d.key eq jenis}">selected</c:if>
									>${d.value}</option>
								</c:forEach>
							</select>
						</c:when>
						<c:otherwise>
							<c:forEach items="${daftarJenis}" var="d">
								<c:if test="${d.key eq jenis or d.key eq penis}">
									<input class="readOnly" size="53" name="jenis" type="text" value="${d.value}" readonly="readonly">
									<input type="hidden" name="penis" value="${d.key}">
								</c:if>
							</c:forEach>
						</c:otherwise>
					</c:choose>
					
				</td>
			</tr>
			<tr>
				<th>Status Polis</th>
				<td>
					<input name="_status" type="hidden" value="${status.LSSP_ID}">
					<input class="readOnly" size="53" name="status" type="text" value="${status.LSSP_STATUS}" readonly="readonly">
				</td>
			</tr>
			<tr>
				<th>Password Print Ulang</th>
				<td>
					<input size="53" name="password" type="password" value="">
				</td>
			</tr>
			<tr>
				<th>Alasan</th>
				<td>
					<select name="alasan1" id="alasan1" style="display: none;">
						<c:forEach items="${daftarAlasanPrintUlang}" var="d">
							<option value="${d.key}" 
								<c:if test="${d.key eq alasan1}">selected</c:if>
							>${d.value}</option>
						</c:forEach>
					</select>
					<select name="alasan2" id="alasan2" style="display: none;">
						<c:forEach items="${daftarAlasanPrintEndors}" var="d">
							<option value="${d.key}" 
								<c:if test="${d.key eq alasan2}">selected</c:if>
							>${d.value}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<th>Keterangan</th>
				<td>
					<textarea rows="4" cols="51" name="ket">${ket }</textarea>
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<input type="submit" name="save" value="Print Ulang Polis">

					<c:choose>
						<c:when test="${not empty saveSuccessfull}">
							<div id="success">sukses</div>
						</c:when>
						<c:when test="${not empty err}">
							<div id="error">${err}</div>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>					
				</td>
			</tr>
		</table>
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>