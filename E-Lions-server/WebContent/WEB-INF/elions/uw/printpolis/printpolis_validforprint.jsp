<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path}/include/js/disableKeys.js"></script>
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script>
	hideLoadingMessage();
	
	function cari(){
		lca_id=formpost.lca_id.value;
	}
	
	function awal(){
		tanyaEmail = '${tanyaEmail}';
		if(tanyaEmail == 'true'){
			if(confirm('Apakah anda ingin melanjutkan ke proses email softcopy?')){
				window.location='${path }/uw/printpolis.htm?window=email&spaj=${spaj}';
			}
		}
	}
	
</script>
</head>
<body onload="setupPanes('container1', 'tab1'); awal();" style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Validasi</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form id="formpost1" name="formpost1" method="post">
				<input type="hidden" name="validasi" id="validasi1" value="${validasi}">
				<input type="hidden" name="validasi" id="validasi2" value="${validasi}">
				<fieldset >
					<legend>Cek Kelengkapan</legend>
					<table>
					<tr>
					<input type="hidden" nama="spaj" value="${spaj}">
					</tr>
					<tr>
						<th>
						
						<input type="checkbox" name="polut" class="noBorder" 
							value="1"  size="30"  tabindex="5">
						</th>
						<td>Polis Utama</td>
					</tr>
					<tr>
						<th>
						<input type="checkbox" name="manfaat" class="noBorder" 
							value="1"  size="30" tabindex="5"> 	
						</th>
						<td>Manfaat Polis</td>		
					</tr>	
					<tr>
						<th>
						<input type="checkbox" name="lapadan" class="noBorder" 
							value="1"  size="30"  tabindex="5">
						</th>
						<td>Laporan alokasi dana</td>
					</tr>
					
					<tr>
						<th>
						<input type="checkbox" name="ttp" class="noBorder" 
							value="1"  size="30"  tabindex="5">
						</th>
						<td>Tanda Terima Polis</td>
					</tr>
					<tr>
						<th>
						<input type="checkbox" name="ssu" class="noBorder" 
							value="1"  size="30"  tabindex="5">
						</th>
						<td>Syarat-syarat Umum</td>
					</tr>
					<tr>
						<th>
						<input type="checkbox" name="sppp" class="noBorder" 
							value="1"  size="30"  tabindex="5">
						</th>
						<td>Surat Perjanjian Pinjaman Polis</td>
					</tr>
					<tr>
						<th>
						<input type="checkbox" name="kp" class="noBorder" 
							value="1"  size="30"  tabindex="5">
						</th>
						<td>Kartu Polis</td>
					</tr>
					<tr>
						<th>
							
							<input type="submit" name="ok" value="ok" onclick="return confirm('Apakah anda sudah mengecek semua dokumen?');"
										accesskey="O" onmouseover="return overlib('Alt-O', AUTOSTATUS, WRAP);" onmouseout="nd();">
						</th>						
					</tr>
					</table>
					<table>
						<tr>
							<th></th>
							<td>
								<c:if test="${not empty errors1}">
											<div id="error">${errors1}</div>
								</c:if>
								<c:if test="${empty errors1}">
											<div id="error">${errors1}</div>
								</c:if>
							</td>
						</tr>
					</table>
				</fieldset>
				</form>
				<form id="formpost" name="formpost" method="post">
					<fieldset>
						<legend>Proses Validasi</legend>
						<table class="entry2">
							<tr>
								<td>
									<label for="validasi1"><input type="radio" style="border: none;" value="1" name="validasi" id="validasi1">Print Polis Cabang</label>
									<label for="validasi2"><input type="radio" style="border: none;" value="2" name="validasi" id="validasi2">SoftCopy Cabang</label>
								</td>
							</tr>
						</table>
					</fieldset>
					<fieldset>
						<legend>E-mail notifikasi akan dikirim ke</legend>
						<table class="entry2">
							<tr>
								<input type="hidden" id="validasi1" name="validasi" value="">
								<input type="hidden" id="validasi2" name="validasi">
								<th>
									To: 
								</th>
								<td>
									<c:if test="${not empty emailCab }">
										<input type="text"  name="emailto" size="100" value="${emailCab}" style="text-transform: none;" <c:if test="${lock eq 1 }">disabled</c:if > />
									</c:if>
									<c:if test="${empty emailCab }">
										<input type="text" name="emailto" size="100" value="${emailto}" style="text-transform: none;" <c:if test="${lock eq 1 }">disabled</c:if > />
									</c:if>
									Nama Cabang
									<select name="lca_id" onChange="javascript:formpost.submit();" <c:if test="${lock eq 1 }">disabled</c:if>>
										 <c:choose>
										 	<c:when test="${team eq 1}">
										 	</c:when>
										 	<c:otherwise>
										 		<option value="0">---Pilih---</option>
										 	</c:otherwise>
										 </c:choose>
										
										<c:forEach items="${lsRegion}" var="x" varStatus="xt">
											<option value="${x.KEY}" 
													<c:if test="${x.KEY eq lca_id}">selected</c:if> >
													${x.VALUE}
											</option>
										</c:forEach>
									</select>
									<input type="submit" name="cari" value="Cari" accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();" <c:if test="${lock eq 1 }">disabled</c:if> >
									<br/><span class="info" style="text-transform: none">* Untuk > 1 penerima, pisahkan dengan tanda (;)</span>
								</td>
							</tr>
							<tr>
								<th>
									Cc:
								</th>
								<td>
									<input type="text" name="emailcc" size="100" value="${emailcc}" style="text-transform: none;" <c:if test="${lock eq 1 }">disabled</c:if >/>
								</td>
							</tr>
							<tr>
								<th>
									Subject:
								</th>
								<td>
									<input type="text" name="emailsubject" size="125" value="${emailsubject}" style="text-transform: none;">
								</td>
							</tr>
							<tr>
								<th>
									Message:
								</th>
								<td>
									<textarea rows="12" cols="125" name="emailmessage" style="text-transform: none;">${emailmessage}</textarea>
								</td>
							</tr>
							<tr>
								<th>
									Keterangan:
								</th>
								<td>
									<textarea rows="3" cols="125" name="keterangan" style="text-transform: none;">${keterangan}</textarea>
									<span>* Wajib Diisi</span>
								</td>
							</tr>
							<c:if test="${lsbs_id}">
							<tr>
								<th>
									File SPAJ:
								</th>
								<td><b>Dokumen Tambahan</b> &nbsp;
									<input type="file" rows="3" cols="200" name="file_tambahan" id = "file_tambahan" size="60"></input>	
								</td>
							</tr>
							</c:if>
							<tr>
								<th></th>
								<td>
									<c:if test="${not empty errors}">
										<div id="error">${errors}</div>
									</c:if>
									<input type="submit" name="kirim" value="Kirim" onclick="return confirm('Apakah anda sudah mengecek semua dokumen?\nApabila anda melanjutkan, email notifikasi akan dikirimkan dan polis ini dapat dicetak di cabang.');"
										accesskey="V" onmouseover="return overlib('Alt-V', AUTOSTATUS, WRAP);" onmouseout="nd();">
								</td>
							</tr>
						</table>
					</fieldset>				
				</form>
			</div>
			
		</div>
	</div>

	<script>
		<c:if test="${not empty param.sukses}">
			alert('Kurs berhasil diinput');
		</c:if>
		<c:if test="${not empty kursBelomAda}">
			alert('${kursBelomAda}');
		</c:if>
	</script>
	
</body>
</html>