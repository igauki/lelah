<%@ include file="/include/page/header.jsp"%>
<script>
	function awal(){
		if('${cmd.flag}'=='1'){
			//alert("Deduct telah Berhasil Di simpan \nSilahkan cetak slip potongan komisi / transfer ke ttp");
			window.location='${path }/uw/slip_potongan_komisi.htm?spaj=${cmd.spaj}';
		}else if('${cmd.flag}'=='2'){
			alert("Polis Berhasil di Transfer ke TTP");
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';
		}else if('${cmd.flag}'=='5'){
			alert("Deduct telah di isi silahkan cetak slip potongan komisi \n atau transfer ke ttp.");
		}
		
		if('${cmd.posisi}'!=''){
			alert("${cmd.posisi}");
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';
		}
		
	}

	function rubah(banyak_maunya){
		if(banyak_maunya == 9){
			document.getElementById('asdf').style.display = "none";
			document.getElementById('t1').style.display = "block";
			document.getElementById('t2').style.display = "block";
			document.getElementById('t3').style.display = "none";
			document.getElementById('t4').style.display = "none";
			document.getElementById('t5').style.display = "none";
			document.getElementById('t6').style.display = "none";
			
		}else{
			document.getElementById('asdf').style.display = "block";
			document.getElementById('t1').style.display ="none";
			document.getElementById('t2').style.display = "none";
			document.getElementById('t3').style.display = "block";
			document.getElementById('t4').style.display = "block";
			document.getElementById('t5').style.display = "block";
			document.getElementById('t6').style.display = "block";
		}
	}

	function kelapkelip(banyak_maunya){
		if (banyak_maunya== 9){
		document.getElementById('asdf').style.display = 'none';
		}else {
		
		}
	}
	function buttonLinks(flag){
		if(flag=='print'){
			popWin('${path}/report/uw.pdf?window=slip_pot_komisi&spaj=${cmd.spaj}',600,800)
		}else if(flag=='ubah'){
			formpost.ubah.value="1";
			formpost.submit();
		}else if(flag=='transfer'){
			formpost.ubah.value="2";
			formpost.submit();
		}
	}
</script>
<form:form id="formpost" name="formpost" commandName="cmd">
<body style="height: 100%;" onload="awal();"> 
<c:if test="${cmd.posisi eq null}">
	<table class="entry2">
		<tr>
			<td width="600">
				<fieldset>
					<legend>SLIP PEMOTONGAN KOMISI UNTUK PREMI SPAJ BARU</legend>
					<table class="entry2">
						<tr><input type="hidden" name="ubah" value="0">
							<th id="th1">Jenis</th>
							<td>
								<form:select path="lsjd_id" onchange="rubah(this.value);">
									<form:option label="" value="0"/>
									<form:options items="${lsDeduct}" itemLabel="value" itemValue="key"/>
								</form:select> 
							</td>
						</tr>
						<tr>
							<th>Cabang</th>
							<td><input type="text" size="75" name="cabang" readonly value="${cmd.cabang }"></td>
						</tr>
						<tr>
							<th>Nama Agen</th>
							<td><input type="text" size="75" name="cabang" readonly value="${cmd.nama_agen}"></td>
						</tr>
						<tr>
							<th>Kode Agen</th>
							<td><input type="text" size="7"name="cabang" readonly value="${cmd.kode_agen}"></td>
						</tr>
						<tr>
							<th>Plan yang diambil</th>
							<td> 
								<form:select id="asdf" path="lsbs_id"  onchange="buttonLinks('ubah')">
									<form:option label="NONE" value="0"/>
									<form:options items="${selectLstBisnis}" itemLabel="value" itemValue="key"/>
								</form:select> 
								<c:if test="${cmd.lsbs_id ne null}">
									<form:select path="lsdbs_number" >
										<form:option label="NONE" value="0"/>
										<form:options items="${cmd.lsDetProduk}" itemLabel="value" itemValue="key"/>
									</form:select> 
								</c:if>
							</td>
						</tr>
					</table>
				</fieldset>
				<fieldset>
					<legend>Premi diambil dari komisi Polis No. ${cmd.nopolis} sebesar :</legend>
					<table class="entry2">
						<tr>
						<th>Total *</th>
							
							<td>
								<form:input id="t1" path="total2"  maxlength="14" />
							</td>
						</tr>
							<tr>
							<th>Jenis Lisensi</th>
							<td><select id="t2" name="jns_lisence"  path="jns_lisence">
								<option <c:if test="${\"No\" eq param.pilter}">selected</c:if> value="No">No</option>
								<option <c:if test="${\"Pemutihan\" eq param.pilter}">selected</c:if> value="Pemutihan">Pemutihan</option>
								<option <c:if test="${\"Ujian\" eq param.pilter}">selected</c:if> value="Ujian">Ujian</option>
								<option <c:if test="${\"Semesteran\" eq param.pilter}">selected</c:if> value="Semesteran">Semesteran</option>
								<option <c:if test="${\"Tidak Lulus\" eq param.pilter}">selected</c:if> value="Tidak Lulus">Tidak Lulus</option>
								<option <c:if test="${\"Temporary\" eq param.pilter}">selected</c:if> value="Temporary">Temporary</option>
							</td>
							</select>
							</tr>
						
						
						<tr>
							<th>UPP</th>
							<td>
								<form:input  id="t3" path="pot_upp" maxlength="14" />
							</td>
						</tr>
						<tr>
							<th>Extra Premi</th>
							<td>
								<form:input  id="t4" path="pot_extra" maxlength="14" />
							</td>
						</tr>
						<tr>
							<th>Biaya Polis</th>
							<td>
								<form:input  id="t5" path="pot_biaya" maxlength="14" />
							</td>
						</tr>
						<tr>
							<th>Total</th>
							<td>
								<form:input  id="t6" path="total" readonly='true' maxlength="14" />
							</td>
						</tr>
						<tr>
							<th>SPAJ Baru No.</th>
							<td><form:input path="new_spaj" maxlength="14" /></td>
						</tr>
						
					</table>
					<br>Keterangan  * : Hanya diisi untuk pembayaran lisensi
					
				</fieldset>
			</td>
		</tr>
		<tr>
			<td width="600" align="center">
				<input type="submit" value="Save" name="save"
				<c:if test="${submitSuccess eq 1 }">disabled</c:if>
				>
				<input type="button" value="Print " name=print" onClick="buttonLinks('print')";>
				<input type="button" value="Transfer to Finance" name="transfer" onClick="buttonLinks('transfer')";>
				
			</td>
		</tr>
		
		<tr> 
			<td nowrap="nowrap" colspan="2">
	        	<c:if test="${cmd.flag eq 1 }">
	            	<div id="success">
			        	Berhasil Disimpan
		        	</div>
		        </c:if>	
	        	<c:if test="${cmd.flag eq 2 }">
	            	<div id="success">
			        	Berhasil Transfer ke TTP
		        	</div>
		        </c:if>	
	  			<spring:bind path="cmd.*">
					<c:if test="${not empty status.errorMessages}">
						<div id="error">
			        		Pesan:<br>
								<c:forEach var="error" items="${status.errorMessages}">
											- <c:out value="${error}" escapeXml="false" />
									<br/>
								</c:forEach>
						</div>
					</c:if>									
				</spring:bind>
			</td>
   		  </tr>
		
	</table>
</c:if>
</body>
</form:form>
<%@ include file="/include/page/footer.jsp"%>