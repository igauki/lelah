<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<!-- DatePicker Script (jscalendar) -->
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	function cariData(){
		if(trim(document.formpost.kata.value)=='' && trim(document.formpost.telp.value)=='' && trim(document.formpost.tgl_lahir.value)=='') return false;
		else createLoadingMessage();	
		//createLoadingMessage();	
	}
	
	function backToParent1(mcl_id, reg_spaj, no_polis)
	{
			parentForm = self.opener.document.forms['frmParam'];
			if(parentForm) {
				if(parentForm.elements['kopiMclId']){
					parentForm.elements['kopiMclId'].value = mcl_id; 
					parentForm.elements['regSpaj'].value = reg_spaj; 
					parentForm.elements['noPolis'].value = no_polis; 
					parentForm.elements['submitKopiMclId'].click();
					window.close();
				}
			}
			backToParent(mcl_id, reg_spaj, no_polis);
	}
	function backToParent(mcl_id, reg_spaj, no_polis){
		parentForm = self.opener.document.forms['formpost'];
		if(parentForm.elements['kopiMclId']){
			parentForm.elements['kopiMclId'].value = mcl_id; 
			parentForm.elements['regSpaj'].value = reg_spaj; 
			parentForm.elements['noPolis'].value = no_polis; 
			
			if(parentForm.elements['flagRefresh']){
				parentForm.elements['flagRefresh'].value = 'refresh'; 
			}
			if(parentForm.elements['searchExistFlag']){
				parentForm.elements['searchExistFlag'].value = 'EXIST';
			}
			if(parentForm.elements['btn_input_blacklist']){
				parentForm.elements['btn_input_blacklist'].value = 'Input Attention List Cari';
			}
			if(parentForm.elements['btn_input_blacklist']){
				parentForm.elements['btn_input_blacklist'].click();
			}
			if(parentForm.elements['submitKopiMclId']){
				parentForm.elements['submitKopiMclId'].click();
			}
			
			window.close();
		}
	}

</script>
</head>
<BODY onload="resizeCenter(650,400); document.title='PopUp :: Cari Data Attention List'; setupPanes('container1', 'tab1'); document.formpost.kata.select(); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">
				<c:if test="${param.cari eq 'data'}">Cari Data Attention List</c:if>
				<c:if test="${param.cari eq 'spaj'}">Cari SPAJ</c:if>
				<c:if test="${param.cari eq 'exca'}">Cari Existing Customer</c:if>
				</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" action="${path }/uw/blacklistgutri.htm" style="text-align: center;">
					<input type="hidden" name="win" value="${param.win}">
					<input type="hidden" name="cari" value="${param.cari}">
					<input type="hidden" name="posisi" value="${param.posisi}">
					<table class="entry2">
						<tr>
							<th rowspan="3">Cari:</th>
							<th class="left">
								<select name="tipe">
									<option value="0" <c:if test="${param.tipe eq \"0\" }">selected</c:if>>Nama</option>
									<option value="1" <c:if test="${param.tipe eq \"1\" }">selected</c:if>>No.Attention List</option>
									<option value="2" <c:if test="${param.tipe eq \"2\" }">selected</c:if>>Alias</option>
									<option value="3" <c:if test="${param.tipe eq \"3\" }">selected</c:if>>No.Bukti Identitas</option>
									<option value="4" <c:if test="${param.tipe eq \"4\" }">selected</c:if>>No.SIM</option>
									<option value="5" <c:if test="${param.tipe eq \"5\" }">selected</c:if>>No.PASPOR</option>
									<option value="6" <c:if test="${param.tipe eq \"6\" }">selected</c:if>>No.Akte Lahir</option>
									<option value="7" <c:if test="${param.tipe eq \"7\" }">selected</c:if>>No.KIMS/KITAS</option>
									<option value="8" <c:if test="${param.tipe eq \"8\" }">selected</c:if>>No.Polis</option>
									<option value="9" <c:if test="${param.tipe eq \"9\" }">selected</c:if>>No.SPAJ</option>
								</select>
								<select name="pilter">
									<option <c:if test="${\"LIKE%\" eq param.pilter}">selected</c:if> value="LIKE%">LIKE%</option>
									<option <c:if test="${\"%LIKE\" eq param.pilter}">selected</c:if> value="%LIKE">%LIKE</option>
									<option <c:if test="${\"%LIKE%\" eq param.pilter}">selected</c:if> value="%LIKE%">%LIKE%</option>
									<option <c:if test="${\"LT\" eq param.pilter}">selected</c:if> value="LT"><</option>
									<option <c:if test="${\"LE\" eq param.pilter}">selected</c:if> value="LE"><=</option>
									<option <c:if test="${\"EQ\" eq param.pilter}">selected</c:if> value="EQ">=</option>
									<option <c:if test="${\"GE\" eq param.pilter}">selected</c:if> value="GE">>=</option>
									<option <c:if test="${\"GT\" eq param.pilter}">selected</c:if> value="GT">></option>
								</select>					
								<input type="text" name="kata" size="34" value="${param.kata }" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
								<input type="submit" name="search" value="Search" onclick="return cariData();"
									accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
							</th>
						</tr>
						<tr>
							<th class="left">
								<label for="lahir">
									<input class="noBorder" type="checkbox" name="centang" id="lahir" value="1">
									Tanggal Lahir (untuk pencarian berdasarkan Pemegang/Tertanggung) :
								</label>
								<script>inputDate('tgl_lahir', '', false);</script>
							</th>
						</tr>
						<tr>
							<th class="left">
								sumber
								<select name="sumber">
									<option value="0" <c:if test="${param.sumber eq \"0\" }">selected</c:if>>ALL</option>
									<option value="1" <c:if test="${param.sumber eq \"1\" }">selected</c:if>>INDIVIDU</option>
									<option value="11" <c:if test="${param.sumber eq \"11\" }">selected</c:if>>DMTM</option>
								</select>
								Telp.
								<input type="text" name="telp" size="34" value="${param.telp }" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
							</th>
						</tr>
					</table>
					<table class="simple">
						<thead>
							<tr>
								<th style="text-align: left">No.</th>
								<th style="text-align: left">Nama</th>
								<th style="text-align: left">Tgl.Lahir</th>
								<th style="text-align: left">No.Bukti Identitas</th>
								<th style="text-align: left">No.Polis</th>
								<th style="text-align: left">No.SPAJ</th>
								<th style="text-align: left">Telp</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="mcl" items="${cmd.listMcl}" varStatus="stat">
								<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
									onclick="backToParent1('${mcl.mcl_id}','${mcl.reg_spaj}','${mcl.mspo_policy_no}');">
									<c:if test="${mcl.mcl_blacklist eq 1}">
										<td><font color="blue">${mcl.lbl_id}</font></td>
										<td><font color="blue">${mcl.mcl_first}</font></td>
										<td><font color="blue">${mcl.mspe_date_birth2}</font></td>
										<td><font color="blue">${mcl.mspe_no_identity}</font></td>
										<td><font color="blue">${mcl.mspo_policy_no}</font></td>
										<td><font color="blue">${mcl.reg_spaj}</font></td>
										<td><font color="blue">${mcl.telpon_rumah}</font></td>
									</c:if>
									<c:if test="${mcl.mcl_blacklist eq 0}">
										<td>${mcl.lbl_id}</td>
										<td>${mcl.mcl_first}</td>
										<td>${mcl.mspe_date_birth2}</td>
										<td>${mcl.mspe_no_identity}</td>
										<td>${mcl.mspo_policy_no}</td>
										<td>${mcl.reg_spaj}</td>
										<td>${mcl.telpon_rumah}</td>
									</c:if>
								</tr>
								<c:set var="jml" value="${stat.count}"/>
								<c:if test="${stat.count eq 1}">
									<c:set var="v1" value="${mcl.mcl_id}"/>
									<c:set var="v2" value="${mcl.reg_spaj}"/>
									<c:set var="v3" value="${mcl.mspo_policy_no}"/>
								</c:if>
							</c:forEach>
						</tbody>
					</table>
					<br>
					<input type="hidden" name="flag" value="${cmd.flag }" >
					<input type="button" name="close" value="Close" onclick="window.close();"
						accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">

				</form>
			</div>
		</div>
	</div>

<c:if test="${jml eq 1}">
<script>backToParent1('${v1}', '${v2}', '${v3}');</script>
</c:if>

</body>
<%@ include file="/include/page/footer.jsp"%>