<%@ include file="/include/page/header_mall.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<%User currentUser = (User) session.getAttribute("currentUser");%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>

 
<script type="text/javascript">
	hideLoadingMessage();
	
	function bodyEndLoad(){
		var successMessage = '${successMessage}';
		if(successMessage != ''){
		alert('${successMessage}');	
				//var flag_posisi = document.getElementById('flag_posisi').value;
				parentForm = self.opener.document.forms['formpost'];
				//if(${successMessage} == 'insert sukses'){
					parentForm.elements['tmms_id'].value='${tmms_id}';
					parentForm.elements['refreshButton'].click();
					window.close();
				//}
				}
	}
</script>

</head>

<body onload="resizeCenter(750,600);bodyOnLoad();" style="height: 100%;">
<XML ID=xmlData></XML>
<div class="tab-container" id="container1">

		<div class="tab-panes">

			<div id="pane1" class="panes">
<form:form commandName="cmd" name="frmParam" id="frmParam" method="post">
						<fieldset>
					  <legend>PA</legend>
					   <table class="result_table2" cellspacing="3">
					   <tr><td colspan="2" style="border: 0px;">
					   <spring:bind path="cmd.*">
			<c:if test="${not empty status.errorMessages}">
				<div id="error">ERROR:<br>
				<c:forEach var="error" items="${status.errorMessages}">
	- <c:out value="${error}" escapeXml="false" />
					<br />
				</c:forEach></div>
			</c:if>
		</spring:bind>	</td></tr>
			<c:if test="${currentUser.lca_id eq '58'}">
		<tr>
							<td>Appointment Id:</td>
							<td><form:input path="tmms.mspo_plan_provider" size="50" cssErrorClass="inpError" readonly="readonly"/><font class="error">*</font></td>
						</tr>
						</c:if>
						<tr>
							<th colspan="2">Data Pemegang Polis</th>
						</tr>
						<tr>
							<td align="left">Nama Pemegang:</td>
							<td><form:input path="tmms.holder_name" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">Jenis Kelamin:</td>
							<td>
							<spring:bind path="cmd.tmms.sex">
								<label for="cowok"> <input type="radio" class=noBorder
									name="${status.expression}" value="1"
									<c:if test="${cmd.tmms.sex eq 1 or cmd.tmms.sex eq null}"> 
												checked</c:if>
									id="cowok">Pria </label>
								<label for="cewek"> <input type="radio" class=noBorder
									name="${status.expression}" value="0"
									<c:if test="${cmd.tmms.sex eq 0}"> 
												checked</c:if>
									id="cewek">Wanita </label>
							</spring:bind>
							<font class="error">*</font>
							</td>
						</tr>
						<tr>
							<td align="left">Tempat Lahir:</td>
							<td><form:input path="tmms.bod_tempat" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">Tanggal Lahir:</td>
							<td>
								<spring:bind path="cmd.tmms.bod_holder" >
        		                    <script>
                                        var isDisabled;
                                        var status = '${statusForm}';
                                        if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
                                <font color="#CC3300">*</font>
                                </spring:bind>
							</td>
						</tr>
						<tr>
							<td align="left">No KTP:</td>
							<td><form:input path="tmms.no_identitas" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">Status:</td>
							<td>
								<spring:bind path="cmd.tmms.status">
									<label for="menikah"> <input type="radio" class=noBorder
										name="${status.expression}" value="1"
										<c:if test="${cmd.tmms.status eq 1 or cmd.tmms.sex eq null}"> 
													checked</c:if>
										id="menikah">Menikah </label>
									<label for="belum_menikah"> <input type="radio" class=noBorder
										name="${status.expression}" value="0"
										<c:if test="${cmd.tmms.status eq 0}"> 
													checked</c:if>
										id="belum_menikah">Belum Menikah </label>
								</spring:bind>
								<font class="error">*</font>
							</td>
						</tr>
						<tr>
							<td align="left">Email:</td>
							<td><form:input path="tmms.email" size="60" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">Alamat:</td>
							<td><form:input path="tmms.address1" size="80" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">Kota:</td>
							<td><form:input path="tmms.city" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">Kode Pos:</td>
							<td><form:input path="tmms.postal_code" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">No Telepon:</td>
							<td><form:input path="tmms.home_phone" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">No Hp:</td>
							<td><form:input path="tmms.mobile_no" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						</table>
						</fieldset>
						<fieldset>
					   <legend>-</legend>
					   <table class="result_table2">
						<tr>
							<td colspan="2" align="left">
								<font class="error">* HARUS DIISI</font>
							</td>
						</tr>
						<tr>
							<td colspan="2" style="text-align: center;">
							<input type="hidden" name="flag_posisi" size="1" id="flag_posisi" value="${flag_posisi}"/>
							<input type="hidden" name="kata" size="1" value="edit" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
								<input type="submit" name="save" value="Save"  onclick="return update();" accesskey="S" />
								<input type="button" name="close" value="Close" onclick="window.close();" accesskey="C" />
						<input type="hidden" name="result" size="1" value="${result}" />
							</td>
						</tr>
					</table>
					</fieldset>
					</div>
		</div>
	</div>	
</form:form>

</body>
</html>