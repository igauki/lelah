<%@ include file="/include/page/header.jsp"%>
	<body>
		<fieldset>
		<legend>SAR TEMP</legend>
			<display:table id="baris" name="sarTemp" class="simple" >                                                                                        
				<display:column property="NO_POLIS" title="NO POLIS"  />                                                                                      
				<display:column property="LSDBS_NAME" title="PRODUCT"  />                                                                                  
				<display:column property="LKU_SYMBOL" title="KURS"  />                                                                                  
				<display:column property="SAR" title="SAR" format="{0, number, #,##0.00;(#,##0.00)}"  />                                                      
				<display:column property="LSSP_STATUS" title="STATUS"  />                                                                                
				<display:column property="MEDIS" title="MEDIS"  />                                                                                            
				<display:column property="LSGR_NAME" title="GROUP"  />   
			</display:table>  
		</fieldset>	
		<fieldset>
		<legend>REAS TEMP</legend>	
			<table class="entry">
		 		<tr>
			 		<th>Insured For</th>
			 		<td>
						<c:if test="${reasTemp.PEMEGANG eq 0}">
				 			<input type="pemegang" type="text" value="Insured">
						</c:if> 
						<c:if test="${reasTemp.PEMEGANG eq 1}">
				 			<input type="pemegang" type="text" value="Policy Holder">
						</c:if> 
			 		</td>
					<td width="150"></td>
			    	<th>US$ Conv.</th>
			 		<td>
			 			<input name="nil kurs" type="text" value="${reasTemp.NIL_KURS}">
			 		</td>
		 		</tr>
		 		<tr>
			 		<th>Reins. Type</th>
			 		<td>
			 			<c:if test="${reasTemp.MSTE_REAS eq 0}">
				 			<input type="tipe reas" type="text" value="Non Reas">
						</c:if> 
			 			<c:if test="${reasTemp.MSTE_REAS eq 1}">
				 			<input type="tipe reas" type="text" value="Treaty">
						</c:if> 
			 			<c:if test="${reasTemp.MSTE_REAS eq 2}">
				 			<input type="tipe reas" type="text" value="Facultative">
						</c:if> 
			 		</td>
					<td width="150"></td>
			 		<th>Extra Mort.</th>
			 		<td>
			 			<input name="extra" type="text" value="${reasTemp.EXTRA_MORTALITY}">
			 		</td>
		 		</tr>
			</table>
		    <table class="entry" BORDER="1">
				<tr> 
					<td></td>
		            <td></td>
		            <th>Sum At Risk</th>
		            <td></td>
		            <th>Retensi</th>
		            <td></td>
		            <th>Reassured</th>
				</tr>
				<tr> 
					<th>Term Rider</th>
					<td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.SAR_TR_RD}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
		            <td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.RETENSI_TR_RD}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
					<td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.REAS_TR_RD}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
				</tr>
		        <tr> 
		            <th>Main Product</th>
		            <td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.SAR_LIFE}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
		            <td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.RETENSI_LIFE}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
					<td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.REAS_LIFE}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
		        </tr>
				<tr> 
		            <th>Simas Sp</th>
					<td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.SAR_SSP}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
		            <td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.RETENSI_SSP}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
					<td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.REAS_SSP}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
		        </tr>
	            <tr> 
		            <th>PA Include</th>
					<td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.SAR_PA_IN}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
		            <td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.RETENSI_PA_IN}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
					<td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.REAS_PA_IN}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
				</tr>
	            <tr> 
		            <th>PA Rider</th>
					<td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.SAR_PA_RD}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
		            <td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.RETENSI_PA_RD}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
					<td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.REAS_PA_RD}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
				</tr>
	            <tr> 
		            <th>PK Include</th>
					<td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.SAR_PK_IN}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
		            <td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.RETENSI_PK_IN}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
					<td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.REAS_PK_IN}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
	            </tr>
	            <tr> 
		            <th>PK Rider</th>
					<td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.SAR_PK_RD}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
		            <td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.RETENSI_PK_RD}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
					<td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.REAS_PK_RD}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
	            </tr>
	            <tr> 
		            <th>Super Sehat</th>
					<td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.SAR_SSH}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
		            <td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.RETENSI_SSH}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
					<td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.REAS_SSH}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
	            </tr>
	            <tr> 
		            <th>Cash Plan</th>
					<td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.SAR_CASH}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
		            <td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.RETENSI_CASH}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
					<td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.REAS_CASH}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
	            </tr>
	            <tr> 
		            <th>TPD</th>
					<td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.SAR_TPD}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
		            <td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.RETENSI_TPD}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
					<td width="5"></td>
					<td>
						<fmt:formatNumber value="${reasTemp.REAS_TPD}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
		            </tr>
				<c:forEach items="${reasTemp.lsRiderExcNew}" var="x" >
			        <tr>
			    		<th>${x.lsdbs_name}<th>
			    		<td><fmt:formatNumber value="${x.sar }" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" /></td>
						<td width="5"></td>
			    		<td><fmt:formatNumber value="${x.retensi }" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" /></td>
						<td width="5"></td>
			    		<td><fmt:formatNumber value="${x.reas }" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" /></td>
			        </tr>    
		     	</c:forEach>   
	            <tr>
		          	<td></td> 
		          	<td></td> 
		          	<th colspan="3" ><div align="center">Total Pertanggungan Yang Direasuransikan</div></th> 
					<td></td>
					<td>
						<fmt:formatNumber value="${total}" type="currency" currencySymbol="${reasTemp.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" />
					</td>
					<td>
					
					</td>
	            </tr>
	            <tr> 
		            <td></td>
		            <td></td>
		            <td></td>
		            <td></td>
		            <td></td>
	            </tr>
	        </table>    
		</fieldset>
	</body>	
<%@ include file="/include/page/footer.jsp"%>