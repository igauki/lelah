<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	    $().ready(function() {
		$("#tabs").tabs();
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		$("#company").hide();
		$("#company1").hide();
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		$("#company2").hide();	
		// user
		$("#dist").change(function() {
			var s=$("#dist").val();
			
			
			if(s ==00 || s==05 || s==04 || s==06 || s==07 || s==08 || s==09 || s==10 || s==11 || s==12 || s==13 || s==14 || s==15 || s==16 || s==17 || s==18 || s==19 || s==20 || s==21 || s==22){
			 	$("#company").hide();
				$("#company").empty();				
			}else{
				 $("#company").show();
				 $("#company").empty();
				 var url = "${path}/uw/upload_excel.htm?window=handleRequest&json=1&lca=" +$("#dist").val();
				 alert(url);			
				 $.getJSON(url, function(result) {
					$("<option/>").val("ALL").html("ALL").appendTo("#company");
					$.each(result, function() {
						$("<option/>").val(this.key).html(this.value).appendTo("#company");
					});
					$("#company option:first").attr('selected','selected');
				});	
			
			//kopi untuk disubmit (isinya, bukan valuenya)
			$("#distribusi").val($("#dist option:selected").text());			
			
 			
			    
			}
		});
		
		$("#dist1").change(function() {
			var z=$("#dist1").val();
			
			
			if(z ==00 || z==05 || z==04 || z==06 || z==07 || z==08 || z==09 || z==10 || z==11 || z==15 || z==16 || z==17 || z==18 || z==19 || z==20 || z==21 || z==22){
			 	$("#company1").hide();
				$("#company1").empty();
				
			}else{
				 $("#company1").show();
				 $("#company1").empty();
				 var url = "${path}/uw/upload_excel.htm?window=handleRequest&json1=1&lca1=" +$("#dist1").val();
							
				 $.getJSON(url, function(result) {
					$("<option/>").val("ALL").html("ALL").appendTo("#company1");
					$.each(result, function() {
						$("<option/>").val(this.key).html(this.value).appendTo("#company1");
					});
					$("#company1 option:first").attr('selected','selected');
				});	
			
			//kopi untuk disubmit (isinya, bukan valuenya)
 			$("#distribusi1").val($("#dist1 option:selected").text());		
 	         
 	        }		
	
		});
		
		$("#dist2").change(function() {
			var x=$("#dist2").val();
				
			if(x ==00 || x==05 || x==04 || x==06 || x==07 || x==08 || x==09 || x==10 || x==11 || s==12 || s==13 || s==14 || s==15 || s==16 || s==17 || s==18 || s==19 || s==20 || s==21 || s==22){
			 	$("#company2").hide();
				$("#company2").empty();
				
			}else{
				 $("#company2").show();
				 $("#company2").empty();
				 var url = "${path}/uw/upload_excel.htm?window=handleRequest&json1=1&lca1=" +$("#dist2").val();
							
				 $.getJSON(url, function(result) {
					$("<option/>").val("ALL").html("ALL").appendTo("#company2");
					$.each(result, function() {
						$("<option/>").val(this.key).html(this.value).appendTo("#company2");
					});
					$("#company2 option:first").attr('selected','selected');
				});	
			
			//kopi untuk disubmit (isinya, bukan valuenya)
 			$("#distribusi2").val($("#dist2 option:selected").text());		
 	         
 	        }		
	
		});

		$("#upload").click(function() {
			var file=$("#file1").val();
			var dist=$("#dist").val();
			var cmpny=$("#company").val();
			
		    if(file=="" || file==null){
				alert('Silahkan pilih file terlebih dahulu');
				return false;
			}else if(dist=="00"){
				alert('Silahkan Pilih Distribusi terlebih dahulu');
				return false;	
			}else{
				$("#upload").val("Loading....Please Wait...!");
				return true;
			}
		});
		
		
		$("#showPolis").click(function() {
			//var file=$("#file1").val();
			var dist=$("#dist2").val();
			var cmpny=$("#company2").val();
			var sts=$("#st_polis").val();
			
			if(dist=="00"){
			     alert('Silahkan Pilih Distribusi terlebih dahulu');
				  return false;	
			}else if(sts==""){
				 alert('Silahkan Pilih Status Polis terlebih dahulu');
				  return false;	
			}
		});
		

		$("#company").click(function() {
			//kopi untuk disubmit (isinya, bukan valuenya)
			$("#company2").val($("#company option:selected").text());
		});	
		$("#company1").click(function() {
			//kopi untuk disubmit (isinya, bukan valuenya)
			$("#company3").val($("#company1 option:selected").text());
		});	
		$("#company2").click(function() {
			//kopi untuk disubmit (isinya, bukan valuenya)
			$("#company3").val($("#company2 option:selected").text());
		});	
			var index = $('div#tabs li a').index($('a[href="#tab-${showTab}"]').get(0));
				$('div#tabs').tabs({selected:index});
		
	});
	
	function showBSB(reg_spaj){
		popWin('${path}/uw/upload_nb.htm?reg_spaj='+reg_spaj, 400, 700);
	}
	
	function showSPAJ(reg_spaj){
		popWin('${path }/uw/view.htm?showSPAJ='+reg_spaj, 400, 700);
	}
	
</script>


<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

</style>

	<body>
		<div id="tabs">		
			<ul>
				<li><a href="#tab-1">Upload Data </a></li>
				<li><a href="#tab-2">Report Hasil Upload</a></li>
				<li><a href="#tab-3">Input/Transfer/Delete Data Upload</a></li>
			</ul>
			<div id="tab-1">
				<form id="formPost" name="formPost" method="post" enctype="multipart/form-data">
					<fieldset class="ui-widget ui-widget-content">
						<legend class="ui-widget-header ui-corner-all"><div>Upload Excel File</div></legend>
						<fieldset class="ui-widget ui-widget-content">
							<legend class="ui-widget-header ui-corner-all"><div>PERHATIAN!!</div></legend>
	 						<div class="rowElem">
								<ul>
									<li>Format Tanggal Harus dd/mm/yyy</li>
									<li>Jangan sampai ada kolom/baris yang kosong</li>
								</ul>
	 						 </div>
						</fieldset>
						<div class="rowElem" id="dropdown">
		 					<label>Jenis Distribusi:</label>
		 					<select  name="dist" id="dist" title="Silahkan pilih jenis distribusi">
		 						<c:forEach var="c" items="${listdist}" varStatus="s">
									<option value="${c.key}">${c.value}</option>
								</c:forEach>
		 					</select>
							<select size="10" name="company" id="company" title="Silahkan Pilih Nama Perusahaan">
	 							<c:forEach var="c" items="${listcompany}" varStatus="s">
									<option value="${c.key}">${c.value}</option>
	 							</c:forEach>
							</select>
						</div>
		 				<div class="rowElem">
							<label>File Excel(*.xls) :</label>
		 					<input type="file" name="file1" id="file1" size="70" />
							<input type="hidden" name="file_fp" id="file_fp"/>
		 					<input type="submit" name="upload" id="upload" value="Upload">
		 					<input type="hidden" name="distribusi" id="distribusi" value="ALL">
							<input type="hidden" name="company" id="company" value="ALL">
						</div>
					</fieldset>
					<div class="rowElem">
						<c:forEach items="${pesan}" var="d" varStatus="st">
							<c:choose>
								<c:when test="${d.sts eq 'FAILED'}">
									<font color="red">${st.index+1}. ${d.msg}</font>
								</c:when>
								<c:otherwise>
									<font color="blue">${st.index+1}. ${d.msg}</font>
								</c:otherwise>
							</c:choose>
							<br/>
						</c:forEach>
					</div>
				</form>
			</div>
			<div id="tab-2">
				<form id="formPost1" name="formPost1" method="post" target="_blank">
					<fieldset class="ui-widget ui-widget-content">
						<div class="rowElem" id="dropdown1">
			 				<label>Jenis Distribusi:</label>
		 					<select name="dist1" id="dist1" title="Silahkan pilih jenis distribusi">
		 						<c:forEach var="c" items="${listdist1}" varStatus="s">
									<option value="${c.key}">${c.value}</option>
								</c:forEach>
		 					</select>
							<select size="10" name="company1" id="company1" title="Silahkan Pilih Nama Perusahaan">
								<c:forEach var="c" items="${listcompany1}" varStatus="s">
									<option value="${c.key}">${c.value}</option>
								</c:forEach>
							</select>
						</div>
						<div class="rowElem" id="dropdown2">
							<label>Jenis Distribusi:</label>
							<select name="tipe" id="tipe" title="Silahkan pilih jenis Report">
								<c:forEach var="c" items="${listreport}" varStatus="s">
									<option value="${c.key}">${c.value}</option>
								</c:forEach>
							</select>
						</div>
						<div class="rowElem">
							<label>Periode Tanggal Upload :</label>
							<input name="bdate" id="bdate" type="text" class="datepicker" title="Tanggal Awal" value="${bdate}"> s/d
							<input name="edate" id="edate" type="text" class="datepicker" title="Tanggal Akhir" value="${edate}">
						</div>
					    <div class="rowElem">
							<input type="submit" name="showPDF" id="showPDF" value="Show (PDF)">
							<input type="submit" name="showXLS" id="showXLS" value="Show (Excel)">
							<input type="hidden" name="distribusi1" id="distribusi1" value="ALL">
							<input type="hidden" name="company1" id="company1" value="ALL">
							<input type="hidden" name="showReport" value="1">
						</div>
					</fieldset>
				</form>
			</div>
				
			<div id="tab-3">
				<form id="formPost2" name="formPost2" method="post" target="">
					<fieldset class="ui-widget ui-widget-content">
						<div class="rowElem" id="dropdown2">
				 			<label>Jenis Distribusi:</label>
							<select  name="dist2" id="dist2" title="Silahkan pilih jenis distribusi">
								<c:forEach var="c" items="${listdist2}" varStatus="s">
									<option value="${c.key}">${c.value}</option>
								</c:forEach>
							</select>
							<select size="10" name="company2" id="company2" title="Silahkan Pilih Nama Perusahaan">
								<c:forEach var="c" items="${listcompany2}" varStatus="s">
									<option value="${c.key}">${c.value}</option>
								</c:forEach>
							</select>
						</div>
						<div class="rowElem">
							<label>Periode Tanggal Upload :</label>
							<input name="bdate1" id="bdate1" type="text" class="datepicker" title="Tanggal Awal" value="${bdate}"> s/d
							<input name="edate1" id="edate1" type="text" class="datepicker" title="Tanggal Akhir" value="${edate}">
						</div>
						<div class="rowElem">
							<label>Status Data Upload :</label>
							<input name="st_polis" id="st_polis" type="radio" class="radio" value="0" checked="checked">Data Upload Yang Belum Di Input
					    	<input name="st_polis" id="st_polis" type="radio" class="radio" value="1" >Data Upload yang Sudah Di Input
						</div>
					    <div class="rowElem">
							<input type="submit" name="showPolis" id="showPolis" value="Show Polis">
							<input type="hidden" name="distribusi2" id="distribusi2" value="ALL">
							<input type="hidden" name="company2" id="company3" value="ALL">
							<input type="hidden" name="showPolicy" value="1">
						</div>
					</fieldset>
					<fieldset class="ui-widget ui-widget-content">
						<table class="displaytag" cellpadding='0' cellspacing='0' border="1" align="center" style="font-size: 11px">
							<tr>
								<th nowrap align="center" bgcolor="#b7b7b7">Check</th>
								<th nowrap align="center" width="50" bgcolor="#b7b7b7">No.</th>
								<th nowrap align="center" width="50" bgcolor="#b7b7b7">No.Temp</th>
								<th nowrap align="center" width="50" bgcolor="#b7b7b7">No.SPAJ</th>
								<th nowrap align="center" width="200" bgcolor="#b7b7b7">Pemegang </th>
								<th nowrap align="center" width="200" bgcolor="#b7b7b7">Tertanggung</th>
								<th nowrap align="center" width="200" bgcolor="#b7b7b7">Produk</th>
								<th nowrap align="center" width="100" bgcolor="#b7b7b7">Jumlah Peserta Tambahan</th>
								<th nowrap align="center" width="50" bgcolor="#b7b7b7">Premi</th>
								<th nowrap align="center" width="60" bgcolor="#b7b7b7">Begin Date</th>
								<c:if test="${stpolis eq '1'}">
									<th width="100" bgcolor="#b7b7b7">Action</th>
								</c:if>
							</tr>
							<c:forEach items="${dbpolis}" var="d" varStatus="ss">
								<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';return true;" onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;">
									<td align="center"><input type="checkbox" class="noBorder" name="chbox" value="${d.NO_TEMP}~${d.REG_SPAJ}" id="chbox" <c:if test="${stpolis ne '1'}">checked</c:if>></td>
									<td align="center" width="50">${ss.index+1}</td>
									<td align="center" width="50">${d.NO_TEMP}</td>
									<td align="center" width="50">${d.REG_SPAJ}</td>
									<td align="center" width="200">${d.PP_NAME}</td>
									<td align="center" width="200">${d.TT_NAME}</td>
									<td align="center" width="250">${d.PLAN_UTAMA}</td>
									<td align="center" width="100">${d.JUMLAH_PESERTA}</td>
									<td align="center" width="50">${d.MSPR_PREMIUM}</td>
									<td align="center" width="60">${d.MSPR_BEG_DATE}</td>
									<c:if test="${stpolis eq '1'}">
										<td>
										 	<input type="button" name="checkSpaj" id="checkSpaj" value="Detail" onclick="showSPAJ('${d.REG_SPAJ}');">
										 	<input type="button" name="uploadBSB" id="uploadBSB" value="BSB" onclick="showBSB('${d.REG_SPAJ}');">
										</td>
									</c:if>
								</tr>
							</c:forEach>
						</table>
						<div class="rowElem">
						    <c:if test="${stpolis eq '0'}">
								<input type="submit" name="delete" id="delete" value="Delete">
								<input type="submit" name="input" id="input" value="Input">
							</c:if>
							<c:if test="${stpolis eq '1'}">
								<input type="submit" name="transfer" id="transfer" value="Transfer">
							</c:if>
							<input type="hidden" name="distribusi2" id="distribusi2" value="ALL">
							<input type="hidden" name="company2" id="company3" value="ALL">
							<input type="hidden" name="InputDel" value="1">
						</div>
					</fieldset>
					<div class="rowElem">
						<c:forEach items="${pesan}" var="d" varStatus="st">
							<c:choose>
								<c:when test="${d.sts2 eq 'FAILED'}">
									<font color="red">${st.index+1}. ${d.msg2}</font>
								</c:when>
								<c:otherwise>
									<font color="blue">${st.index+1}. ${d.msg2}</font>
								</c:otherwise>
							</c:choose>
							<br/>
						</c:forEach>
					</div>
					<div class="rowElem">
						<font color="blue">${pk}</font>
					</div>
				</form>
			</div>
		</div>		
	</body>
</html>