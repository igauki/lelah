<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		$("#tabs").tabs();
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		$("#dp").change(function() {
			var z=$("#dp").val();
			var x=$("#spaj").val();		
			var lsbs=$("#lsbs").val();	
					
			if(z!=10){	
					
				var url = "${path}/uw/uw.htm?window=view_kesehatanDMTM&json=1&spaj="+x+"&fjp="+z+"&lsbs="+lsbs;
				 			
				 $.getJSON(url, function(result) {
				  $.each(result, function() {
						 $("#q1").val(this.Q1);
						 $("#q1desc").val(this.Q1DESC);
						 $("#q2").val(this.Q2);
						 $("#q2desc").val(this.Q2DESC);
						 $("#q3a").val(this.Q3A);
						 $("#q3b").val(this.Q3B);
						 $("#q3c").val(this.Q3C);
						 $("#q3d").val(this.Q3D);
						 $("#q3e").val(this.Q3E);
						 $("#q3fa").val(this.Q3FA);
						 $("#q3fb").val(this.Q3FB);
						 $("#q3g").val(this.Q3G);
						 $("#q3h").val(this.Q3H);
						 $("#q3i").val(this.Q3I);
						 $("#q3j").val(this.Q3J);
						 $("#q3k").val(this.Q3K);
						 $("#q3l").val(this.Q3L);
						 $("#q3m").val(this.Q3M);
						 $("#q3n").val(this.Q3N);
						 $("#q3o").val(this.Q3O);
						 $("#q3p").val(this.Q3P);
						 $("#q3q").val(this.Q3Q);
						 $("#q4a").val(this.Q4A);
						 $("#q4b").val(this.Q4B);
						 $("#q5a").val(this.Q5A);
						 $("#q5b").val(this.Q5B);
						 $("#q6a").val(this.Q6A);
						 $("#q6b").val(this.Q6B);
						 $("#q7a").val(this.Q7A);
						 $("#q7b").val(this.Q7B);
				  });	
				});	
				
			}			
	
		});
		
		
	});
	
	
	
</script>


<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

</style>

<body>
		<div id="tabs">		
				<ul>
					<li><a href="#tab-1">View Kesehatan</a></li>					
					
				</ul>
		<div id="tab-1">			
		  <form id="formPost" name="formPost" method="post"   target="">
		      <fieldset class="ui-widget ui-widget-content">
			  <legend class="ui-widget-header ui-corner-all"><div>Questionare Kesehatan DMTM SIO</div></legend>
			  <div class="rowElem" id="dropdown2"> 
					<label>Daftar Peserta:</label> 
					<select  name="dp" id="dp" title="Silahkan pilih jenis peserta">												
						<c:forEach var="c" items="${daftarPeserta}" varStatus="s">
							<option value="${c.key}">${c.value}</option>
				        </c:forEach> 
				    </select> 									
			</div>	
			<div class="rowElem">
		             <label>No. SPAJ</label>
     				<input type="text" size="12" value="${spaj}" id="spaj" name="spaj" style="background: #D4D4D4;" readonly="readonly">
     				<input type="hidden" size="12" value="${lsbs}" id="lsbs" name="lsbs" style="background: #D4D4D4;" readonly="readonly">
			</div>																											
	        <div class="rowElem">		
				    <table  border="1px" >
				    
					      	 <tr align="left"> 
					      	 	<th>1.</th>
					      	 	<td>Apakah dalam 5 tahun terakhir Anda pernah atau direncanakan atau dianjurkan melakukan pemeriksaan kesehatan, pengobatan rutin dalam pengawasan dokter atau menjalani perawatan rumah sakit atau operasi atau pembedahan?Jika YA, jelaskan ...<br><textarea rows="3" cols="150" style="width: 40%; text-transform: uppercase; " id="q1desc" name="q1desc"></textarea></td>
					      	 	<td><input type="text" size="5" id="q1" name="q1"></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<th>2.</th>
					      	 	<td>Berdasarkan pengetahuan, keyakinan dan informasi yang Anda ketahui, apakah ada anggota keluarga kandung (Ayah/Ibu/Kakak/Adik) yang pernah menderita penyakit jantung, diabetes, tekanan darah tinggi, ginjal, hepatitis, kelainan mental, kanker dan stroke sebelum mencapai usia 60 tahun?Jika YA, jelaskan ...<br><textarea rows="3" cols="150" style="width: 40%; text-transform: uppercase;" id="q2desc" name="q2desc"></textarea></td>
					      	 	<td><input type="text" size="5" id="q2" name="q2"></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<th>3.</th>
					      	 	<td>Apakah Anda sedang atau pernah menjalani pengobatan/perawatan atau pernah diberitahu dalam konsultasi/pengawasan medis sehubungan dengan salah satu atau beberapa keadaan/gejala gangguan pada:</td>
					      	 	<td></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<th>3.a.</th>
					      	 	<td>Penyakit Jantung</td>
					      	 	<td><input type="text" size="5" id="q3a" name="q3a"></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<th>3.b.</th>
					      	 	<td>Darah Tinggi</td>
					      	 	<td><input type="text" size="5" id="q3b" name="q3b"></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<th>3.c.</th>
					      	 	<td>Kencing Manis</td>
					      	 	<td><input type="text" size="5" id="q3c" name="q3c"></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<th>3.d.</th>
					      	 	<td>Epilepsi</td>
					      	 	<td><input type="text" size="5" id="q3d" name="q3d"></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<th>3.e.</th>
					      	 	<td>Kanker/Tumor</td>
					      	 	<td><input type="text" size="5" id="q3e" name="q3e"></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<th>3.f.</th>
					      	 	<td>Hamil <input type="text" size="3" id="q3fb" name="q3fb"> bulan</td>
					      	 	<td><input type="text" size="5" id="q3fa" name="q3fa"></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<th>3.g.</th>
					      	 	<td>Stroke/ Kelainan Pembuluh Darah Otak</td>
					      	 	<td><input type="text" size="5" id="q3g" name="q3g"></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<th>3.h.</th>
					      	 	<td>Gangguan Hati/ Empedu/ termasuk Hepatitis</td>
					      	 	<td><input type="text" size="5" id="q3h" name="q3h"></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<th>3.i.</th>
					      	 	<td>Kelainan Ginjal dan/ Saluran Kemih</td>
					      	 	<td><input type="text" size="5" id="q3i" name="q3i"></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<th>3.j.</th>
					      	 	<td>Kelainan Tulang dan/ Sendi</td>
					      	 	<td><input type="text" size="5" id="q3j" name="q3j"></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<th>3.k.</th>
					      	 	<td>Kelainan Darah dan/ Pembuluh Darah</td>
					      	 	<td><input type="text" size="5" id="q3k" name="q3k"></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<th>3.l.</th>
					      	 	<td>Kelainan Hormonal</td>
					      	 	<td><input type="text" size="5" id="q3l" name="q3l"></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<th>3.m.</th>
					      	 	<td>Asthma/ Saluran Pernapasan</td>
					      	 	<td><input type="text" size="5" id="q3m" name="q3m"></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<th>3.n.</th>
					      	 	<td>TBC</td>
					      	 	<td><input type="text" size="5" id="q3n" name="q3n"></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<th>3.o.</th>
					      	 	<td>AIDS dan HIV</td>
					      	 	<td><input type="text" size="5" id="q3o" name="q3o"></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<th rowspan="2">3.p.</th>
					      	 	<td>Lainnya, sebutkan ...</td>
					      	 	<td><input type="text" size="5" id="q3p" name="q3p"></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<td>Jika YA, jelaskan ...<br><textarea rows="3" cols="150" style="width: 40%; text-transform: uppercase;" id="q3q" name="q3q"></textarea></td>
					      	 	<td></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<th>4</th>
					      	 	<td>Apakah Anda mempunyai kebiasaan merokok atau minum minuman beralkohol atau menggunakan narkotika/ obat terlarang atau obat penenang?Jika YA, jelaskan ...<br><textarea rows="3" cols="150" style="width: 40%; text-transform: uppercase;" id="q4b" name="q4b"></textarea></td>
					      	 	<td><input type="text" size="5" id="q4a" name="q4a"></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<th>5</th>
					      	 	<td>Apakah Anda melakukan atau mempunyai kegemaran olahraga yang penuh risiko (misalnya mendaki gunung, layang gantung, olahraga bermotor, menyelam, dll) atau melakukan penerbangan selain penumpang pesawat komersial yang berjadwal?Jika YA, jelaskan ...<br><textarea rows="3" cols="150" style="width: 40%; text-transform: uppercase;" id="q5b" name="q5b"></textarea></td>
					      	 	<td><input type="text" size="5" id="q5a" name="q5a"></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<th>6</th>
					      	 	<td>Apakah permohonan asuransi Anda ini untuk menutup atau memulihkan asuransi jiwa, penyakit kritis, kecelakaan, kecacatan atau asuransi kesehatan pernah ditolak, ditangguhkan atau dibatalkan atau pernahkah polis Anda diubah persyaratannya, dikenakan premi tambahan, dibatalkan atau ditolak pembaharuannya atau selama 3 tahun terakhir pernah mengajukan klaim pada asuransi manapun?Jika YA, jelaskan ...<br><textarea rows="3" cols="150" style="width: 40%; text-transform: uppercase;" id="q6b" name="q6b"></textarea></td>
					      	 	<td><input type="text" size="5" id="q6a" name="q6a"></td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<th rowspan="2">7</th>
					      	 	<td>Tinggi Badan</td>
					      	 	<td><input type="text" size="5" id="q7a" name="q7a">Cm</td>
					      	 </tr>
					      	 <tr align="left"> 
					      	 	<td>Berat Badan</td>
					      	 	<td><input type="text" size="5" id="q7b" name="q7b">Kg</td>
					      	 </tr>
					      	 
				      </table>
				   </div>   
			  </fieldset>					
		   </form>	
						
	     </div>	
	</div>
			
			

			
</body>
</html>


