<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript" src="${path}/include/js/jquery-1.3.2.min.js"></script>
<script>
$(document).ready(function() {
        <c:if test="${not empty pesanError}">
            alert('${pesanError}');
        </c:if>
    });
</script>
<c:choose>    
    <c:when test="${not empty hak}">
        <div id="error">
            Maaf, No SPAJ ini sudah proses REAS, tidak bisa diedit.
        </div>
    </c:when>
    <c:otherwise>
   <body onload="document.title='PopUp :: Edit Info Medis';setupPanes('container1','tab1');" style="height: 100%;">
     <div class="tab-container" id="container1">
              <ul class="tabs">
                <li>
                    <a href="#" onClick="return showPane('pane1', this)" id="tab1">Edit Info Medis</a>
                </li>
            </ul>
            <div class="tab-panes">
                <div id="pane1" class="panes">
                     <form id="formpost" name="formpost" method="post">
                    <fieldset>
                      <legend>Info User</legend>
                         <table class="entry2">
                   <tr>
                       <th nowrap="nowrap">User</th><th align ="left">&nbsp;[${infoDetailUser.LUS_ID}]&nbsp;&nbsp;${infoDetailUser.LUS_LOGIN_NAME}</th>
                  </tr>
                  <tr>
                       <th nowrap="nowrap">Nama<br></th><th align ="left">&nbsp;${infoDetailUser.LUS_FULL_NAME}&nbsp;[${infoDetailUser.LDE_DEPT}]</th>
                 </tr>
                    </table>
                         </fieldset>
         <fieldset>
             <legend>Edit Info Medis&nbsp;</legend>
                <table class="entry2">
                <tr>
                   <th nowrap="nowrap" class="left">
			<select disabled>
				<c:forEach var="medis" items="${select_medis}"><option<c:if test="${dataUsulan.mste_medical eq medis.ID}"> SELECTED </c:if>
				value="${medis.ID}">${medis.MEDIS}</option>
				</c:forEach>
			</select>
			</th>
                </tr>
                <tr>
                     <th colspan = "2" nowrap = "nowrap" align ="left">DIUBAH MENJADI</th>
                 </tr>
                 <tr>
                    <th nowrap="nowrap" class="left">
			<select name="medis">
				<c:forEach var="medis" items="${select_medis}"><option<c:if test="${dataUsulan.mste_medical eq medis.ID}"> SELECTED </c:if>
				value="${medis.ID}">${medis.MEDIS}</option>
				</c:forEach>
			</select>
			</th>
                 </tr>
                 <tr>
                 <th colspan = "2">&nbsp;</th>
                 </tr>
             <th colspan = "2" nowrap = "nowrap" align ="left"> Keterangan(Alasan Melakukan Edit):</font></th> </tr>
                 <tr>
                 <th colspan = "2" nowrap = "nowrap" align ="left"><textarea rows="4" cols="20" style="width: 70%; text-transform: uppercase;" name="alasan"></textarea></font></th>
             </tr>
                 <tr align = "left">
                        <th colspan="2"> 
                            <input type="hidden" name="save" id="save">  
                            <input type="submit" name="save" value="Save" onclick="return confirm('Apakah Anda Yakin Untuk Melanjutkan Proses ?');"> 
                            <input type="button" name="close" value="Close" onclick="window.close();"></font>
                           </th>
                         </tr>
                         <tr>
                            <td><font color="#ff0000">* Proses ini akan masuk ke ulangan(History)</font></td>
                        </tr>
                     </table>
               </fieldset>
            </form>
        </div>
      </div>
   </div>
    </c:otherwise>
 </c:choose>
</body>
<%@ include file="/include/page/footer.jsp"%>