<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/mallinsurance1.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<!-- DatePicker Script (jscalendar) -->
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	function cariData(){
		//var pilter = document.getElementById('pilter').value;
		//var tipe = document.getElementById('tipe').value;
		//var kata = document.getElementById('kata').value;
		var pdfName = document.getElementById('pdfName').value;
		
		//var url = 'pa_free_detail.htm?tmms_id='+tmms_id;
		var url = 'pa_free_detail.htm?pdfName='+pdfName;
		window.open(url, '_self');
		
		//document.getElementById('refreshPage').href = 'pa_free_detail.htm?tmms_id='+tmms_id;
		//document.getElementById('refreshPage').click();
	
	}
	
	function openPdf(){
		//var pdfName = document.getElementById('pdfName').value;
		
		//var link = 'pa_free_detail.htm?pdfName='+pdfName;
		//window.open( link, '_blank' );
		//document.getElementById('refreshPage').href = link;
		document.getElementById('search').click();
	
	}
	
	function sendEmail(){
		var email = document.getElementById('emailName').value;
		if(email == ''){
			alert('E-MAIL GAGAL DIKIRIM. E-MAIL TIDAK DITEMUKAN');
		}else{
			document.getElementById('search').click();
		}
		
	}
	
	function bodyEndLoad(){
		if(document.getElementById('popUpInsert').value == 'open'){
			document.getElementById('inputFreePa').click();
		}
		var err = '${error_email}'
		if(err != ''){
			alert(err);
		}
	}
	
	function referesh(){	
		document.getElementById('refresh').click();
	}
	
</script>
</head>
<BODY onload="bodyEndLoad();" style="height: 100%;">

	<div class="tab-container" id="container1">

		<div class="tab-panes">

			<div id="pane1" class="panes"><br/>
				<form method="post" name="formpost" action="#" style="text-align: center;">
					<input type="button" value="INPUT FREE PA" name="inputFreePa" id="inputFreePa" class="button2"
					onclick="popWin('${path}/uw/pa_free_input.htm', 350, 450); "
					onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
					onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />
					<a href="pa_free_detail.htm" id="refreshPage" name="refreshPage"></a>
					<input type="button" name="refreshButton" onclick="document.getElementById('search').click();" style="visibility: hidden;"/>
					<input type="submit" name="transfer" id="transfer" value="Transfer" style="visibility: hidden;" onmouseout="nd();"/>
					<input type="hidden" name="action" id="action" value="" />
					<input type="hidden" name="popUpInsert" id="popUpInsert" value="${popUpInsert}" />
					<input type="hidden" name="tmms_id" id="tmms_id" value="" />
					<input type="hidden" name="pdfName" id="pdfName"/>
					<input type="hidden" name="emailName" id="emailName"/>
					<input type="hidden" name="nama" id="nama"/>
					<input type="hidden" name="win" value="${param.win}">
					<input type="hidden" name="cari" value="${param.cari}">
					<input type="hidden" name="posisi" value="${param.posisi}">
					<!-- fieldset>
		 					<legend>Pencarian</legend>
					<table class="result_table2">
						<tr>
							<th rowspan="1">Cari:</th>
							<td>
								<select name="tipe">
									<option value="1" <c:if test="${param.tipe eq \"1\" }">selected</c:if>>Nama</option>
									<option value="2" <c:if test="${param.tipe eq \"2\" }">selected</c:if>>No.Bukti Identitas</option>
								</select>
								<select name="pilter">
									<option <c:if test="${\"LIKE\" eq param.pilter}">selected</c:if> value="LIKE">LIKE</option>
									<option <c:if test="${\"LT\" eq param.pilter}">selected</c:if> value="LT"><</option>
									<option <c:if test="${\"LE\" eq param.pilter}">selected</c:if> value="LE"><=</option>
									<option <c:if test="${\"EQ\" eq param.pilter}">selected</c:if> value="EQ">=</option>
									<option <c:if test="${\"GE\" eq param.pilter}">selected</c:if> value="GE">>=</option>
									<option <c:if test="${\"GT\" eq param.pilter}">selected</c:if> value="GT">></option>
								</select>					
								<input type="text" name="kata" size="34" value="${param.kata }" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
								<input type="submit" name="search" id="search" value="Search" onclick="cariData();" class="button2"
								onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
								onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />
							</td>
						</tr>
					</table>
					</fieldset -->
					<div style="visibility: hidden;">
					<input type="submit" name="search" id="search" value="Search" class="button2"
								onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
								onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />
								</div>
					<br/>
					<table  class="" width="1080" align="center" cellpadding="2" >       				
			        <tr>
			            <td width="573" class="">Page ${currPage} of ${lastPage} </td>
			            <td width="493" align="right" class="">Go
			                <input name="cPage" type="text" style="width:45px" maxlength="4" >
			                <input type="button" value="Go" id="go" style="width: 40px;"  onclick="referesh()"/>
			                <input type="submit" name="refresh" value="refresh" id="refresh" style="visibility: hidden;" /> 
			               <label></label>
			                &nbsp;			           
			                
			                <c:choose>
			        			<c:when test="${currPage eq '1'}">
			        			<font color="gray">First | Prev |</font> 
			        			</c:when>
			        			<c:otherwise>
			               			<a href="#" class="" onclick="document.forms[ 0 ].cPage.value='${firstPage}'; referesh()">First</a> | 
			                		<a href="#" class="" onclick="document.forms[ 0 ].cPage.value='${previousPage}'; referesh()">Prev</a> | 
			                	</c:otherwise>
			                </c:choose>
			                
			                <c:choose>
			        			<c:when test="${currPage eq lastPage}">
			        			<font color="gray">Next | Last </font>
			        			</c:when>
			        			<c:otherwise>
			               			<a href="#" class="" onclick="document.forms[ 0 ].cPage.value='${nextPage}'; referesh()">Next</a> | 
			                		 <a href="#" class="" onclick="document.forms[ 0 ].cPage.value='${lastPage}'; referesh()">Last</a> 
			                	</c:otherwise>
			                </c:choose>                
               			 </td>
      				  </tr>
    				</table>		
					<table class="result_table">
						<thead>
							<tr>
								<th style="text-align: left">Nama</th>
								<th style="text-align: left">Tempat</th>
								<th style="text-align: left">Tgl.Lahir</th>
								<th style="text-align: left">No. Identitas</th>
								<th style="text-align: left">Alamat</th>
								<th style="text-align: left">Kota</th>
								<th style="text-align: left">Kode Pos</th>
								<th style="text-align: left">E-Mail</th>
								<th style="text-align: left">Action</th>
							</tr>
						</thead>
						<tbody>
							<c:set var="backGroundColor" value="#FED9D9" />

							<c:forEach var="pa" items="${tmmsList}" varStatus="stat">
											<c:choose>
												<c:when test='${"#FED9D9" == backGroundColor}'>
													<c:set var="backGroundColor" value="#FFF2F2" />
												</c:when>
												<c:otherwise>
													<c:set var="backGroundColor" value="#FED9D9" />
												</c:otherwise>
											</c:choose>
								<tr style="background-color: ${backGroundColor};">
										<td>${pa.holder_name}</td>
										<td>${pa.bod_tempat}</td>
										<td>
											<fmt:formatDate value="${pa.bod_holder}" pattern="dd/MM/yyyy" />
										</td>
										<td>${pa.no_identitas}</td>
										<td width="200px">${pa.address1}</td>
										<td>${pa.city}</td>
										<td>${pa.postal_code}</td>
										<td>${pa.email}</td>
										<td width="140px">
											<input type="button" value="EDIT" name="editPaDetail"
											onclick="popWin('${path}/uw/pa_free_update.htm?tmms_id='+${pa.id}, 350, 450); " class="button2" 
											onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
											onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />
											<input type="button" value="SEND E-MAIL" name="emailPdfPolis"
											onclick="document.getElementById('pdfName').value='${pa.no_sertifikat}';document.getElementById('nama').value='${pa.holder_name}';document.getElementById('emailName').value='${pa.email}';sendEmail();"
											class="button2"
											onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
											onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />
										</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<br>
				</form>
			</div>
		</div>
	</div>

</body>
<%@ include file="/include/page/footer.jsp"%>