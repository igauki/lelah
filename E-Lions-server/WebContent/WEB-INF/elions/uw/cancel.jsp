<%@ include file="/include/page/header.jsp"%>
<body style="height: 100%;">
	<div id="contents">
		<spring:bind path="cmd.*">
			<c:choose>
				<c:when test="${empty hasInitErrors }">
					<form method="post" name="formpost" action="${path }/uw/cancel.htm">
						<input type="hidden" value="${param.from}" name="from">
						<input type="hidden" value="${param.spaj }" name="spaj">
						<fieldset>
							<legend>Pembatalan SPAJ</legend>
							<table class="entry2">
								<tr>
									<th>SPAJ:</th>
									<td><input type="text" value="${param.spaj }" name="spaj" readonly="readonly"></td>
								</tr>
								<tr>
									<th>Password:</th>
									<td>
										<input type="password" name="verify" value="">
									</td>
								</tr>
								<tr>
									<th>Alasan Pembatalan:</th>
									<td>
										<textarea rows="4" cols="50" name="alasan">${param.alasan}</textarea>
									</td>
								</tr>
								<tr>
									<th></th>
									<td><input type="submit" name="cancel" value="Batalkan"></td>	
								</tr>
							</table>
						</fieldset>
					</form>
				</c:when>
				<c:otherwise>
					<input type="button" name="back" value="Back" onclick="window.location='${path }/uw/view.htm?showSPAJ=${param.spaj }';">
				</c:otherwise>
			</c:choose>
			<c:choose>
				<c:when test="${not empty status.errorMessages}">
					<div id="error">
					ERROR:<br>
					<c:forEach var="error" items="${status.errorMessages}">
								- <c:out value="${error}" escapeXml="false" />
						<br />
					</c:forEach></div><br>
				</c:when>
				<c:when test="${not empty submitSuccess}">
					<div id="success">
						<spring:message code="payment.cancelSuccess"/>
					</div>
				</c:when>
			</c:choose>
		</spring:bind>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>