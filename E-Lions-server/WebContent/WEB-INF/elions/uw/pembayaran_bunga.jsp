<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<meta HTTP-EQUIV="Expires" CONTENT="-1">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link href="${path}/include/image/eas.ico" rel="shortcut icon">
		<!--  -->
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<!--  -->
		<script type="text/javascript" src="${path }/include/js/default.js"></script>
		<!-- Ajax Related -->
		<script type="text/javascript" src="${path}/include/js/ajax.js"></script>
		<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script>
		<script type="text/javascript" src="${path }/dwr/engine.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>
		<script type="text/javascript">
			hideLoadingMessage();

			function cek(cekbok){
				var ukuran = ${ukuran};
				for(i=0; i<ukuran; i++){
					document.getElementById('ucup'+i).checked = cekbok.checked;
				}
			}

			function centang(elemen){
				document.getElementById(elemen).checked = true;
			}

			function cetak(){		
				popWin('${path}/report/bac.pdf?window=pembayaran_bunga' + 
					'&lcb_no=' + document.formpost.lcb_no.value +
					'&startDate=' + document.formpost.startDate.value +
					'&endDate=' + document.formpost.endDate.value +
					'&mpb_flag_bs=' + document.formpost.mpb_flag_bs.value, 600, 850);
			}
			
			function cetak2(){		
				popWin('${path}/report/bac.xls?window=pembayaran_bunga' + 
					'&lcb_no=' + document.formpost.lcb_no.value +
					'&startDate=' + document.formpost.startDate.value +
					'&endDate=' + document.formpost.endDate.value +
					'&mpb_flag_bs=' + document.formpost.mpb_flag_bs.value, 600, 850);
			}
			
		</script>
		
	</head>
	<body onload="setupPanes('container1','tab1');" style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Daftar Transfer Bank</a>
				</li>
			</ul>

			<div class="tab-panes">

				<div id="pane1" class="panes">
					<form method="post" name="formpost" action="${path}/uw/uw.htm?window=pembayaran_bunga">
						<fieldset>
							<legend>Parameter</legend>
							<input type="hidden" name="flag" id="flag" value="${flag}">
							<table class="entry2" style="width: auto;">
								<tr>
									<th nowrap="nowrap">Status Pembayaran</th>
									<td>
										<select name="mpb_flag_bs">
											<c:forEach items="${StatusBayar}" var="b">
												<option value="${b.key}"
													<c:if test="${b.key eq mpb_flag_bs}"> selected </c:if>
												>${b.value}</option>
											</c:forEach>
										</select>
									</td>
								</tr>
								<tr>
									<th nowrap="nowrap">Cabang Sekuritas</th>
									<td>
										<select name="lcb_no">
											<c:forEach items="${daftarCab}" var="b">
												<option value="${b.key}"
													<c:if test="${b.key eq lcb_no}"> selected </c:if>
												>${b.value}</option>
											</c:forEach>
										</select>
									</td>
								</tr>
								<tr>
									<th nowrap="nowrap">Tanggal Bayar</th>
									<td>
										<script>inputDate('startDate', '${startDate}', false);</script> s/d 
										<script>inputDate('endDate', '${endDate}', false);</script>									
									</td>
								</tr>
								<tr>
									<th nowrap="nowrap"></th>
									<td>
										<input type="submit" name="cari" value="Cari">
										<input type="button" name="tutup" value="Tutup" onclick="window.close();">
										<input type="button" name="print" value="Print" onclick="cetak();">
										<input type="button" name="excel" value="Excel" onclick="cetak2();">
									</td>
								</tr>
								<c:if test="${flag eq 1}">
								<tr>
									<td colspan="2">
										<table class="displaytag">
											<thead>
												<tr>
													<th style="width: 20px;">
														<input class="noBorder" type="checkbox" onclick="cek(this)">
													</th>
													<th style="width: 100px;">No Polis</th>
													<th style="width: 150px;">Cabang</th>
													<th style="width: 100px;">Tanggal bayar</th>
													<th style="width: 120px;">Nama Pemegang</th>
													<th style="width: 20px;">Mata Uang</th>
													<th style="width: 100px;">Premi</th>
													<th style="width: 100px;">Bunga</th>
													<th style="width: 100px;">Tambahan Bunga</th>
													<th style="width: 100px;">Jumlah Bayar</th>
													<th style="width: 90px;">Status</th>
													<th style="width: 150px;">User yang Proses</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${daftarDrek}" var="d" varStatus="st">
													<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
															<c:if test="${d.mpb_flag_bs eq 2 or d.flag_check eq 1}"> style="background-color: #C0C0C0" </c:if>
															>
														<td class="center"><input class="noBorder" type="checkbox" name="ucup${st.index}" value="1" id="ucup${st.index}" <c:if test="${d.flag_check eq 1}"> checked="checked"</c:if> ></td>
														<td class="center">${d.mspo_policy_no_format}</td>
														<td class="left">${d.nama_cabang}</td>
														<td class="center"><fmt:formatDate pattern="dd/MM/yyyy" value="${d.mpb_paid_date}"/></td>
														<td class="left">${d.pemegang}</td>
														<td class="center">${d.lku_symbol}</td>
														<td class="right"><fmt:formatNumber value="${d.mpb_prm_deposit}" maxFractionDigits="2" minFractionDigits="2" /><input type="hidden" name="flag_bs${st.index}" id="flag_bs${st.index}" value="${d.mpb_flag_bs}"> </td>
														<td class="right"><fmt:formatNumber value="${d.mpb_bunga}" maxFractionDigits="2" minFractionDigits="2" /></td>
														<td class="right"><fmt:formatNumber value="${d.mpb_tambah}" maxFractionDigits="2" minFractionDigits="2" /></td>
														<td class="right"><fmt:formatNumber value="${d.mpb_bunga + d.mpb_tambah}" maxFractionDigits="2" minFractionDigits="2" /></td>
														<td class="center">${d.status}<input type="hidden" name="jenis" id="jenis" value="${d.mpb_jenis}"></td>
														<td class="left">${d.lus_login_name}<input type="hidden" name="bayarid" id="bayarid" value="${d.mpb_bayar_id}"></td>
													</tr>
												</c:forEach>
												
											</tbody>
										</table>
										<c:if test="${flag eq 1}">
											<input type="submit" name="recheck" value="save" onclick="return confirm('Data yang ditandai akan disave.Yakin untuk Save?');">
										</c:if>
									</td>
								</tr>
								</c:if>
							</table>
						</fieldset>					
					</form>
				</div>
			</div>
			
		</div>

	</body>
	
</html>