<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html> 
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/mallinsurance1.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<!-- DatePicker Script (jscalendar) -->
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	function cariData(){
		var pilter = document.getElementById('pilter').value;
		var tipe = document.getElementById('tipe').value;
		var kata = document.getElementById('kata').value;
		var url = 'hcp_detail.htm?pilter='+pilter+'&tipe='+tipe+'&kata='+kata;
		window.open(url, '_self');
		
		//document.getElementById('refreshPage').href = 'pas_detail.htm?pilter='+pilter+'&tipe='+tipe+'&kata='+kata;
		//document.getElementById('refreshPage').click();
	
	}
	
	function update(msp_id, action){
		document.getElementById('msp_id').value = msp_id;
		document.getElementById('action').value = action;
		if(trim(document.getElementById('action').value)=='') return false;
		else createLoadingMessage();	
	}
	
	function transfer_process(nama, msp_id){
		if(confirm('Apakah anda yakin untuk TRANSFER HCP ('+nama+')?')){
					update(msp_id, 'transfer');
					document.getElementById('transfer').click();
				}else{
					document.getElementById('search').click();
				}
	}
	
	function backToParent1(mcl_id, reg_spaj)
	{
			parentForm = self.opener.document.forms['frmParam'];
			if(parentForm) {
				if(parentForm.elements['kopiMclId']){
					parentForm.elements['kopiMclId'].value = mcl_id; 
					parentForm.elements['regSpaj'].value = reg_spaj; 
					parentForm.elements['submitKopiMclId'].click();
					window.close();
				}
			}
			backToParent(mcl_id, reg_spaj);
	}
	
	function bodyEndLoad(){
		if(document.getElementById('popUpInsert').value == 'open'){
			document.getElementById('inputHcp').click();
		}
	}

</script>
</head>
<BODY onload="document.formpost.kata.select(); bodyEndLoad();" style="height: 100%;">

	<div class="tab-container" id="container1">

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" action="#" style="text-align: center;">
							<input type="button" value="INPUT HCP" name="inputHcp" id="inputHcp" class="button2"
							onclick="popWin('${path}/uw/input_hcp.htm', 350, 450); "
							onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
							onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />							
					<a href="hcp_detail.htm" id="refreshPage" name="refreshPage"></a>
					<input type="button" name="refreshButton" onclick="document.getElementById('search').click();" style="visibility: hidden;"/>
					<input type="submit" name="transfer" id="transfer" value="Transfer" style="visibility: hidden;" onmouseout="nd();"/>
					<input type="hidden" name="action" id="action" value="" />
					<input type="hidden" name="popUpInsert" id="popUpInsert" value="${popUpInsert}" />
					<input type="hidden" name="msp_id" id="msp_id" value="" />
					<input type="hidden" name="win" value="${param.win}">
					<input type="hidden" name="cari" value="${param.cari}">
					<input type="hidden" name="posisi" value="${param.posisi}">
					<table class="result_table2">
						<tr>
							<th rowspan="1">Cari:</th>
							<td class="left">
								<select name="tipe" id="tipe">
									<option value="1" <c:if test="${param.tipe eq \"1\" }">selected</c:if>>Nama</option>
									<option value="2" <c:if test="${param.tipe eq \"2\" }">selected</c:if>>No.Bukti Identitas</option>
									<option value="3" <c:if test="${param.tipe eq \"3\" }">selected</c:if>>Reg Id</option>
									<option value="4" <c:if test="${param.tipe eq \"4\" }">selected</c:if>>No.Kartu</option>
								</select>
								<select name="pilter" id="pilter">
									<option <c:if test="${\"LIKE\" eq param.pilter}">selected</c:if> value="LIKE">LIKE</option>
									<option <c:if test="${\"LT\" eq param.pilter}">selected</c:if> value="LT"><</option>
									<option <c:if test="${\"LE\" eq param.pilter}">selected</c:if> value="LE"><=</option>
									<option <c:if test="${\"EQ\" eq param.pilter}">selected</c:if> value="EQ">=</option>
									<option <c:if test="${\"GE\" eq param.pilter}">selected</c:if> value="GE">>=</option>
									<option <c:if test="${\"GT\" eq param.pilter}">selected</c:if> value="GT">></option>
								</select>					
								<input type="text" name="kata" id="kata" size="34" value="${param.kata }" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
								<input type="button" name="search" id="search" value="Search" onclick="cariData();" class="button2" 
								onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
					onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />
							</td>
						</tr>
					</table>
					<table class="result_table">
						<thead>
							<tr>
								<th style="text-align: left">Nama Tertanggung</th>
								<th style="text-align: left">Tempat</th>
								<th style="text-align: left">Tgl.Lahir</th>
								<th style="text-align: left">No. Identitas</th>
								<th style="text-align: left">No. Kartu</th>
								<th style="text-align: left">Alamat</th>
								<th style="text-align: left">Kota</th>
								<th style="text-align: left">Kode Pos</th>
								<th style="text-align: left">E-Mail</th>
								<th style="text-align: left">Reg Id</th>
								<th style="text-align: left">Detail</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="pas" items="${cmd}" varStatus="stat">
								<tr>
										<td>${pas.msp_full_name}</td>
										<td>${pas.msp_pas_tmp_lhr_tt}</td>
										<td>${pas.msp_date_of_birth2}</td>
										<td>${pas.msp_identity_no_tt}</td>
										<td>${pas.no_kartu}</td>
										<td width="200px">${pas.msp_address_1}</td>
										<td>${pas.msp_city}</td>
										<td>${pas.msp_postal_code}</td>
										<td>${pas.msp_pas_email}</td>
										<td>${pas.msp_fire_id}</td>
										<td width="140px">
												<input type="button" value="INPUT" name="inputDetail"
													   onclick="popWin('${path}/uw/input_hcp_detail.htm?posisi=1&win=edit&flag_posisi=hcp_detail&msp_id='+${pas.msp_id}, 350, 450); "
													   class="button2" 
													   onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
													   onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />											
														<input type="button" name="transfer_btn" value="Transfer" onclick="transfer_process('${pas.msp_fire_id}','${pas.msp_id}');"
														class="button2" 
									onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
						onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />
										</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<br>
				</form>
			</div>
		</div>
	</div>

<c:if  test="${not empty successMessage}">	
			<script type="text/javascript">													
				alert('${successMessage}');	
				document.getElementById('search').click();
			</script>
	</c:if>
</body>
<%@ include file="/include/page/footer.jsp"%>