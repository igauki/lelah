<%@ include file="/include/page/header.jsp"%>
<script>
	function awal(){
		var hsl=formpost.hasil.value;
		var info=formpost.info.value;
		if(info=='0'){
			if(confirm("Transfer Ke ${cmd.to}")){
				formpost.proses.value=2;
				formpost.submit();
			}else
				window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';		
		}else if(info=='1'){
			alert("Posisi Polis Ini Ada Di "+formpost.posisi.value);
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
		}else if(info=='2'){
			if(confirm("Transfer Status Decline ke Policy Canceled ?")){
				formpost.proses.value=1;
				formpost.submit();
			}	
		}else if(info=='3'){
			if(confirm("Polis Ini Non Standard Extra Premi Blom Ada, Yakin Lanjutkan ?")){
				formpost.proses.value=2;
				formpost.submit();
			}	
		}
		
		///
		if(hsl=='0'){
			alert("Proses Transfer Gagal");
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
		}else if(hsl=='1'){
			alert("Proses Transfer Ke Pembayaran Berhasil.");
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
		}else if(hsl=='2'){
			alert("Proses Transfer Ke Pembayaran Berhasil dan Payment Lunas\n Silahkan Transfer ke print polis");
			window.location='${path }/uw/transfer_to_print.htm?from=payment&spaj=${cmd.spaj}';	
		}else if(hsl=='3'){
			alert("Proses Transfer Ke Print Polis Berhasil");
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
		}else if(hsl=='4'){
			alert("Transfer Status Decline ke Policy Canceled Berhasil ");
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
		}else if(hsl=='5'){
			alert("Proses Transfer Gagal. Silahkan cek persentase dana DPLK untuk produk ini.");
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
		}
		
		if(hsl != '0'){
			var pesan = '${pesanTambahan}';
			if(pesan != '') alert(pesan);
		}
	}
	
</script>
<c:choose>
<c:when test="${cmd.block  eq 1 }" >
	<div id="error" style="text-align:center; text-transform:uppercase;">SPAJ ini sedang diajukan redeem, tidak dapat proses lebih lanjut!</div>
</c:when>
<c:otherwise>
<body onload="awal()">
	<form name="formpost"  method="post">
		<table class="entry" width="60%">
			<tr>
				<td><div id="success">Silahkan Tunggu... Sedang dilakukan Proses Transfer...</div>
				</td>
			</tr>
			<tr>
				<td>
					<input type="hidden" name="info" value="${cmd.info}">
					<input type="hidden" name="posisi" value="${cmd.lsposDoc}">
					<input type="hidden" name="proses" value="0">
					<input type="hidden" name="hasil" value="${hasil}">
					<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
								ERROR:<br>
									<c:forEach var="error" items="${status.errorMessages}">
												- <c:out value="${error}" escapeXml="false" />
										<br/>
									</c:forEach>
							</div>
						</c:if>									
					</spring:bind>
				</td>
			</tr>
	 	</table>
	</form>	
</body>	
</c:otherwise>
</c:choose>
<%@ include file="/include/page/footer.jsp"%>