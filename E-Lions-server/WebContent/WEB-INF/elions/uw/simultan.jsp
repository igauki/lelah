<%@ include file="/include/page/header.jsp"%>
<script>
	
	function cek_rb_tt(i){
	//alert(i);
		document.formpost.chooseTt.value=i;
	}
	
	function cek_rb_pp(i){
	//alert(i);
		document.formpost.choosePp.value=i;
	}
	function proses(){
		document.formpost.btnProses.disabled=true;
		formpost.submit();
	}
	function awal(){
		//untuk pilihan select
		p=formpost.choosePp.value;
		t=formpost.chooseTt.value;
		//
		val=formpost.flag.value;///1=tertanggung else pemengang dan tertanggung
		if(val=='1'){
			tLength=formpost.rb_tt.length;
			if(tLength!=null){
				if(t!=''){
					formpost.rb_tt[t].checked=true;		
				}
			}	
			
		}else {
			tLength=formpost.rb_tt.length;
			pLength=formpost.rb_pp.length;
			if(tLength!=null){
				if(t!=''){
					formpost.rb_tt[t].checked=true;		
				}
			}
			//
			if(pLength!=null){
				if(p!=''){
					formpost.rb_pp[p].checked=true;		
				}
			}
			
		}
	
		//
		info=${cmd.error};
		//alert(info);
		if(info==100){
			document.formpost.btnProses.disabled=true;
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
			alert("Proses Simultan Berhasil");
		}else if(info==1){
			pos='${cmd.flagId}';
			document.formpost.btnProses.disabled=true;
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
			alert("Posisi Polis ini Ada di"+pos);
		}else if(info==2){
			document.formpost.btnProses.disabled=true;
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
			alert("Proses Simultan Sudah pernah dilakukan untuk Pemegang & Tertanggung Polis");
		}else if(info==3){
			document.formpost.btnProses.disabled=true;
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
			alert("Tidak Bisa proses Simultan dan Reas \npolis ASM");
		}else if(info==103){
			document.formpost.btnProses.disabled=true;
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
			alert("Error Pada saat get counter Silahkan hubungi EDP");
		}else if(info==102){
			document.formpost.btnProses.disabled=true;
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
			alert("Tertanggung ini Memiliki Premi >= Rp 500jt ");
		}else if(info==104){
			document.formpost.btnProses.disabled=true;
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
			alert("Pemegang ini Memiliki Premi >= Rp 500jt ");
		}
		
		
		
	}
	
</script>

<form name="formpost" method="post">
<body onload="awal()";>
<c:choose>
	<c:when test="${cmd.flagAdd eq 1 }">
		<fieldset>
			<legend>Simultan Tertanggung (Hubungan Diri Sendiri)</legend>
			<display:table id="baris" name="cmd.lsSimultanTt" class="displaytag" >                                                                                                 
				<display:column title="Keterangan"  >
				<c:choose>
					<c:when test="${baris.SYMBOL eq \"XX\" || baris.SYMBOL eq \"WW\"  }">
						<a href="javascript:popWin('${path}/uw/detailsimultan.htm?spajAwal=${cmd.spaj }&flag_tt=${baris.BRS }', 400, 800,1,0) ;" >New Client</a>
					</c:when>
					<c:otherwise>
							<a href="javascript:popWin('${path}/uw/detailsimultan.htm?spajAwal=${cmd.spaj }&flag_tt=${baris.BRS }', 400, 800,1,0) ;" >Detail Simultan</a>
					</c:otherwise>
				</c:choose>
				</display:column>
				<display:column property="MCL_FIRST" title="Nama"  />                                                                                    
				<display:column property="MSPE_PLACE_BIRTH" title="Tempat Lahir"  />                                                                      
				<display:column property="MSPE_DATE_BIRTH" title="Tanggal" format="{0, date, dd/MM/yyyy}"  />                                         
				<display:column title="Jenis Identitas"  >
					<c:forEach var="s" items="${lsIdentity}">
						<c:if test="${baris.LSIDE_ID eq s.LSIDE_ID}">${s.LSIDE_NAME }
						</c:if>
					</c:forEach>
				</display:column>                                                                                  
				<display:column property="MSPE_NO_IDENTITY" title="No Identitas"  />                                                                      
				<display:column title="Jns Kelamin"  >
				<c:choose>
					<c:when test="${baris.MSPE_SEX eq 1 }">
					Pria
					</c:when>
					<c:when test="${baris.MSPE_SEX eq 0 }">
					Wanita
					</c:when>
				</c:choose>
				</display:column>                                                                                      
				<display:column property="MSPE_STS_MRT" title="Status"  />                                                                              
				<display:column property="MSPE_MOTHER" title="Nama Ibu Kandung"  />                                                                                
				<display:column property="MCL_ID" title="MCL_ID"  />    
				<display:column  title=""  >    
					<c:choose>
						<c:when test="${baris.BRS eq 0 }">
							<input type="radio" name="rb_tt" onClick="cek_rb_tt(${baris.BRS })" checked>
						</c:when>
						<c:when test="${baris.BRS gt 0}">
							<input type="radio" name="rb_tt" onClick="cek_rb_tt(${baris.BRS })" >
						</c:when>
					</c:choose>	
				</display:column>
			</display:table>                                                 
		</fieldset>	  
	</c:when>	
	<c:when test="${cmd.flagAdd ne 1 }">
		<fieldset>
			<legend>Simultan Tertanggung</legend>
			<display:table id="baris" name="cmd.lsSimultanTt" class="displaytag" >                                                                                                 
				<display:column title="Keterangan"  >
					<c:choose>
						<c:when test="${baris.SYMBOL eq \"XX\" || baris.SYMBOL eq \"WW\"  }">
							<a href="javascript:popWin('${path}/uw/detailsimultan.htm?spajAwal=${cmd.spaj }&flag_tt=${baris.BRS }', 400, 800,1,0) ;" >New Client</a>
						</c:when>
						<c:otherwise>
								<a href="javascript:popWin('${path}/uw/detailsimultan.htm?spajAwal=${cmd.spaj }&flag_tt=${baris.BRS }', 400, 800,1,0) ;" >Detail Simultan</a>
						</c:otherwise>
					</c:choose>
				</display:column>
				<display:column property="MCL_FIRST" title="Nama"  />                                                                                    
				<display:column property="MSPE_PLACE_BIRTH" title="Tempat Lahir"  />                                                                      
				<display:column property="MSPE_DATE_BIRTH" title="Tanggal" format="{0, date, dd/MM/yyyy}"  />                                         
				<display:column title="Jenis Identitas"  >
					<c:forEach var="s" items="${lsIdentity}">
						<c:if test="${baris.LSIDE_ID eq s.LSIDE_ID}">${s.LSIDE_NAME }
						</c:if>
					</c:forEach>
				</display:column>     
				<display:column property="MSPE_NO_IDENTITY" title="No Identitas"  />                                                                      
				<display:column title="Jns Kelamin"  >
				<c:choose>
					<c:when test="${baris.MSPE_SEX eq 1 }">
					Pria
					</c:when>
					<c:when test="${baris.MSPE_SEX eq 0 }">
					Wanita
					</c:when>
				</c:choose>
				</display:column>                                                                                      
				<display:column property="MSPE_STS_MRT" title="Status"  />                                                                              
				<display:column property="MSPE_MOTHER" title="Nama Ibu Kandung"  />                                                                                
				<display:column property="MCL_ID" title="MCL_ID"  />       
				<display:column  title=""  >    
					<c:choose>
						<c:when test="${baris.BRS eq 0 }">
							<input type="radio" name="rb_tt" onClick="cek_rb_tt(${baris.BRS })" checked>
						</c:when>
						<c:when test="${baris.BRS gt 0}">
							<input type="radio" name="rb_tt" onClick="cek_rb_tt(${baris.BRS })" >
						</c:when>
					</c:choose>	
				</display:column>                                                                                   
			</display:table>                                                 
		</fieldset>	  
		<fieldset>
			<legend>Simultan Pemegang</legend>
			<display:table id="baris" name="cmd.lsSimultanPp" class="displaytag" >                                                                                                 
				<display:column title="Keterangan"  >
					<c:choose>
						<c:when test="${baris.SYMBOL eq \"XX\" || baris.SYMBOL eq \"WW\"  }">
							<a href="javascript:popWin('${path}/uw/detailsimultan.htm?spajAwal=${cmd.spaj }&flag_pp=${baris.BRS }', 400, 800,1,0) ;" >New Client</a>
						</c:when>
						<c:otherwise>
								<a href="javascript:popWin('${path}/uw/detailsimultan.htm?spajAwal=${cmd.spaj }&flag_pp=${baris.BRS }', 400, 800,1,0) ;" >Detail Simultan</a>
						</c:otherwise>
					</c:choose>
				</display:column>
				<display:column property="MCL_FIRST" title="Nama"  />                                                                                    
				<display:column property="MSPE_PLACE_BIRTH" title="Tempat Lahir"  />                                                                      
				<display:column property="MSPE_DATE_BIRTH" title="Tanggal" format="{0, date, dd/MM/yyyy}"  />                                         
				<display:column title="Jenis Identitas"  >
					<c:forEach var="s" items="${lsIdentity}">
						<c:if test="${baris.LSIDE_ID eq s.LSIDE_ID}">${s.LSIDE_NAME }
						</c:if>
					</c:forEach>
				</display:column>     				
				<display:column property="MSPE_NO_IDENTITY" title="No Identitas"  />                                                                      
				<display:column title="Jns Kelamin"  >
				<c:choose>
					<c:when test="${baris.MSPE_SEX eq 1 }">
					Pria
					</c:when>
					<c:when test="${baris.MSPE_SEX eq 0 }">
					Wanita
					</c:when>
				</c:choose>
				</display:column>                                                                                      
				<display:column property="MSPE_STS_MRT" title="Status"  />                                                                              
				<display:column property="MSPE_MOTHER" title="Nama Ibu Kandung"  />                                                                                
				<display:column property="MCL_ID" title="MCL_ID"  />              
				<display:column  title=""  >    
					<c:choose>
						<c:when test="${baris.BRS eq 0 }">
							<input type="radio" name="rb_pp" onClick="cek_rb_pp(${baris.BRS })" checked>
						</c:when>
						<c:when test="${baris.BRS gt 0}">
							<input type="radio" name="rb_pp" onClick="cek_rb_pp(${baris.BRS })" >
						</c:when>
					</c:choose>	
				</display:column>                                                                            
			</display:table>                                                 
		</fieldset>	  
	</c:when>
</c:choose>
<table align="center">
	<tr>
		<td>
			<input type="button" value="Proses" onClick="proses()" name="btnProses">
			<input type="hidden" value="${cmd.error }" name="error">
			<input type="hidden" value="${cmd.flagAdd}" name="flag">
			<input type="hidden" value="${t}" name="chooseTt">
			<input type="hidden" value="${p }" name="choosePp">
		</td>
	</tr>
</table>
</body>
</form>
<%@ include file="/include/page/footer.jsp"%>