<%@ include file="/include/page/header.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<script type="text/javascript" src="${path}/include/js/jquery-1.3.2.min.js"></script>
<script>
$(document).ready(function() {
        <c:if test="${not empty param.pesan}">
            alert('${param.pesan}');
        </c:if>
    });
</script>
<body onload="document.title='PopUp :: Edit Alamat ';setupPanes('container1','tab1');" style="height: 100%;">
    <div class="tab-container" id="container1">
        <ul class="tabs"><li><a href="#" onClick="return showPane('pane1', this)" id="tab1">Alamat Rumah Pemegang</a></li></ul>
        <div class="tab-panes">
            <div id="pane1" class="panes">
                <form:form id="formpost" name="formpost" commandName="cmd">
                     <fieldset>
                      <legend>Info User</legend>
                         <table class="entry2">
                   <tr>
                       <th nowrap="nowrap">User</th><th align ="left">&nbsp;[${infoDetailUser.LUS_ID}]&nbsp;&nbsp;${infoDetailUser.LUS_LOGIN_NAME}</th>
                  </tr>
                  <tr>
                       <th nowrap="nowrap">Nama<br></th><th align ="left">&nbsp;${infoDetailUser.LUS_FULL_NAME}&nbsp;[${infoDetailUser.LDE_DEPT}]</th>
                 </tr>
               </table>
            </fieldset>
   <fieldset>
     
             <legend>   Data Alamat Pemegang</legend>
                    <table class="entry2">
                      <tr align = "left"> 
                        <th nowrap="nowrap">Alamat Rumah</th>
                           <th><form:textarea disabled="disabled" path="pemegang.alamat_rumah" cols="80" rows="3" onkeyup="textCounter(this, 200);" onkeydown="textCounter(this, 200);"/></th>
                      </tr>
                      <tr align = "left"> 
                        <th nowrap="nowrap">Kota </th>
                        <th><form:input path="pemegang.kota_rumah" maxlength="10" disabled="disabled"/> Kode Pos <form:input path="pemegang.kd_pos_rumah" size="10" disabled="disabled"/></th>
                      </tr>
                      <tr align = "center"> 
                        <th nowrap="nowrap" colspan='2'>D I U B A H  M E N J A D I</th>
                      </tr>
                      <tr align = "left"> 
                        <th nowrap="nowrap">Alamat Rumah</th>
                           <th><textarea rows="4" cols="20" style="width: 70%; text-transform: uppercase;" name="alamatPemegang"></textarea></th>
                      </tr>
                      <tr align = "left"> 
                        <th nowrap="nowrap">Kota </th>
                        <th><input name="kotaP" maxlength="20"/> Kode Pos <input name="KdPosP" size="10"/></th>
                      </tr>
                   </table>
                </fieldset>
                   <table class="entry2">
                    <tr align = "left">
                     <th colspan="3">Keterangan(Alasan Melakukan Edit):
                      <textarea rows="2" cols="40" style="width:50%;text-transform: uppercase;" name="keterangan"/></textarea></th>
                      </tr>
                      <tr align = "left">
                        <th nowrap="nowrap"></th>
                        <th colspan="1">                        
                            <spring:hasBindErrors name="cmd">
                                <div id="error">
                                    <form:errors path="*" delimiter="<br>" />
                                </div>
                            </spring:hasBindErrors>
                            <input type="hidden" name="submit" id="submit">  
                            <input type="submit" name="submit" value="Save">
                            <input type="button" name="close" value="Close" onclick="window.close();">
                        </th>
                        </tr>
                         <tr>
                      <td colspan ="2"><font color="#ff0000">* Proses ini akan masuk ke ulangan(History)</td>
                      </tr>
                      </table>
                </form:form>
            </div>
        </div>
    </div>
</body>
<%@ include file="/include/page/footer.jsp" %>