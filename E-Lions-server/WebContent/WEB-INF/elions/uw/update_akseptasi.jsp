<%@ include file="/include/page/header.jsp"%>
<script>
	function transper(asdf){
		if(asdf){
			if(asdf!=''){
					if(confirm('Anda yakin mentransfer polis '+asdf+' ke Filling/Tanda Terima Polis?')){
						window.location='${path }/uw/printpolis.htm?window=transfer&spaj='+asdf;
					}
			}
		}
		return false;
	}
</script>
<body style="height: 100%;">
<form name="formpost" method="post">
<div class="tabcontent">
<table class="entry2" style="width: 98%;">
	<tr>
<!--		<c:if test="${msg eq null }">-->
<!--		<td>Silahkan Tunggu Sedang Proses Update Akseptasi...</td>-->
<!--		</c:if>-->
		<c:if test="${msg ne null }">
			<td>
				${msg}
				<c:if test="${lspd_id eq 6}">
				<br/><br/>
					Polis Saat ini ada di posisi PRINT POLIS:
					<input type="button" value="Transfer Ke Tanda Terima / Filling" onclick="return transper('${spaj}');">
				</c:if>
			</td>
		</c:if>
		
		<c:if test="${error ne null}">
		<div id="error">
			ERROR:<br>
			<c:forEach var="error" items="${error}">
					- <c:out value="${error}" escapeXml="false" />
					<br />
			</c:forEach></div>
		</c:if>
	</tr>
</table>
</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>