<%@ include file="/include/page/header_mall.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<%User currentUser = (User) session.getAttribute("currentUser");%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>

<script type="text/javascript">
	hideLoadingMessage();
	
	function agen_cek()
	{
		if (document.getElementById('msag_id').value != "")
		{
			ajaxManager.listagenpas(document.getElementById('msag_id').value,
			{callback:function(map) {
		
				DWRUtil.useLoadingMessage();
				document.getElementById('nama_agen').value = map.nama_penutup;
				document.getElementById('cabang_agen').value = map.nama_regional;
				
			   },
			  timeout:180000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
			});
		}
	}
	
	function agen_cek_pp()
	{
		if (document.getElementById('msag_id_pp').value != "")
		{
			document.getElementById('msag_id').value = document.getElementById('msag_id_pp').value;
			document.getElementById('msag_id_pp_text').value = document.getElementById('msag_id_pp').value;
			agen_cek();
		}
	}
	
	function cari_agen_pp(){
		if (document.getElementById('msag_id_pp').value != "")
		{
			document.getElementById('msag_id_pp_text').value = document.getElementById('msag_id_pp').value;
			//alert('${path }/uw/input_pas_partner.htm?msag_id_pp_cari='+ document.frmParam.elements['msag_id_pp_text'].value);
			window.location='${path }/uw/input_dbd.htm?msag_id_pp_cari='+ document.getElementById('msag_id_pp_text').value;
		}else{
			alert("ISI TERLEBIH DAHULU KODE AGEN");
		}
	}
	
	function pin_cek()
	{
		if (document.getElementById('pin').value != "")
		{
			//alert(document.frmParam.elements['pin'].value);
			cekPin(document.getElementById('pin').value);
		}
	}
	
	function update(){
		if(trim(document.getElementById('kata').value)=='') return false;
		else createLoadingMessage();	
	}
	
	function cekRelasi(){
		if(document.getElementById('lsre_id').value == '1'){		  			
			document.getElementById('msp_full_name').disabled='disabled';
			document.getElementById('msp_pas_tmp_lhr_tt').disabled='disabled';
			document.getElementById('msp_date_of_birth').disabled='disabled';
			document.getElementById('msp_identity_no_tt').disabled='disabled';
			document.getElementById('msp_sex_tt_cowok').disabled='disabled';
			document.getElementById('msp_sex_tt_cewek').disabled='disabled';
		}else{	
			document.getElementById('msp_full_name').disabled='';
			document.getElementById('msp_pas_tmp_lhr_tt').disabled='';
			document.getElementById('msp_date_of_birth').disabled='';
			document.getElementById('msp_identity_no_tt').disabled='';
			document.getElementById('msp_sex_tt_cowok').disabled='';
			document.getElementById('msp_sex_tt_cewek').disabled='';
		}		
	}
	
	function cekElseSourceFund(){
		if(document.getElementById('msp_fire_source_fund2').value == 'LAINNYA'){
			document.getElementById('msp_fire_source_fund').style.visibility = "visible";
			document.getElementById('msp_fire_source_fund').value='';
		}else{
			document.getElementById('msp_fire_source_fund').style.visibility = "hidden";
		}
	}
	
	function cekElseBusiness(){
		if(document.getElementById('msp_fire_type_business2').value == 'LAINNYA'){
			document.getElementById('msp_fire_type_business').style.visibility = "visible";
			document.getElementById('msp_fire_type_business').value='';
		}else{
			document.getElementById('msp_fire_type_business').style.visibility = "hidden";
		}
	}
	
	function cekElseOccupation(){
		if(document.getElementById('msp_fire_occupation2').value == 'LAIN-LAIN'){
			document.getElementById('msp_fire_occupation').style.visibility = "visible";
			document.getElementById('msp_fire_occupation').value='';
		}else{
			document.getElementById('msp_fire_occupation').style.visibility = "hidden";
		}
	}
	
	function cekElseTrio(){
		if(document.getElementById('msp_fire_source_fund2').value == 'LAINNYA'){
			document.getElementById('msp_fire_source_fund').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_source_fund').style.visibility = "hidden";
		}
		if(document.getElementById('msp_fire_type_business2').value == 'LAINNYA'){
			document.getElementById('msp_fire_type_business').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_type_business').style.visibility = "hidden";
		}
		if(document.getElementById('msp_fire_occupation2').value == 'LAIN-LAIN'){
			document.getElementById('msp_fire_occupation').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_occupation').style.visibility = "hidden";
		}
	}
	
	function cekElseOkupasi(){
		if(document.getElementById('msp_fire_okupasi').value == 'L'){
			document.getElementById('msp_fire_okupasi_else').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_okupasi_else').style.visibility = "hidden";
		}
	}
	
	function dofo(index, flag) {
		if(flag==1){
			var a="rekening["+(index+1)+"]";
			var b="rekening["+(index)+"]";		
			document.getElementById(a).focus();
			
		}else{
			var a="rekening["+(index+1)+"]";		
			document.getElementById(a).focus();
		}
	}
	
	function dofo_autodebet(index, flag) {
		if(flag==1){
			var a="rekening_autodebet["+(index+1)+"]";
			var b="rekening_autodebet["+(index)+"]";		
			document.getElementById(a).focus();
			
		}else{
			var a="rekening_autodebet["+(index+1)+"]";		
			document.getElementById(a).focus();
		}
	}
	
	function doRek() {
		document.getElementById('msp_no_rekening').value = '';
		for(i = 0 ; i < 21 ; i++){
			document.getElementById('msp_no_rekening').value = document.getElementById('msp_no_rekening').value + document.getElementById("rekening["+i+"]").value;
		}
	}
	
	function doRek_autodebet() {
		document.getElementById('msp_no_rekening_autodebet').value = '';
		for(i = 0 ; i < 21 ; i++){
			document.getElementById('msp_no_rekening_autodebet').value = document.getElementById('msp_no_rekening_autodebet').value + document.getElementById("rekening_autodebet["+i+"]").value;
		}
	}
	
	function doEnableDisableRek() {
		if(document.getElementById('msp_flag_cc').value == '2' || document.getElementById('msp_flag_cc').value == '1'){//tabungan/kartu kredit
			//autodebet
			//document.getElementById('caribank2').value = '';
			document.getElementById('caribank2').disabled = '';
			document.getElementById('btncari2').disabled = '';
			//document.getElementById('lsbp_id_autodebet').value = '';
			document.getElementById('lsbp_id_autodebet').disabled = '';
			//document.getElementById('msp_rek_nama_autodebet').value = '';
			document.getElementById('msp_rek_nama_autodebet').disabled = '';
			document.getElementById('msp_tgl_debet_div').disabled = '';
			document.getElementById('msp_tgl_debet_div').style.visibility = 'visible';
			if(document.getElementById('msp_flag_cc').value == '1'){
				document.getElementById('msp_tgl_valid_div').disabled = '';
				document.getElementById('msp_tgl_valid_div').style.visibility = 'visible';
			}else{
				document.getElementById('msp_tgl_valid_div').disabled = 'disabled';
				document.getElementById('msp_tgl_valid_div').style.visibility = 'hidden';
			}
			for(i = 0 ; i < 21 ; i++){
				//document.getElementById('rekening_autodebet['+i+']').value = '';
				document.getElementById('rekening_autodebet['+i+']').disabled = '';
			}
			//document.getElementById('msp_no_rekening_autodebet').value = '';
		}else{
			//rekening
			document.getElementById('caribank2').value = '';document.getElementById('caribank2').disabled = 'disabled';
			document.getElementById('btncari2').disabled = 'disabled';
			document.getElementById('lsbp_id_autodebet').value = '';document.getElementById('lsbp_id_autodebet').disabled = 'disabled';
			document.getElementById('msp_rek_nama_autodebet').value = '';document.getElementById('msp_rek_nama_autodebet').disabled = 'disabled';
			document.getElementById('msp_tgl_debet_div').disabled = 'disabled';
			document.getElementById('msp_tgl_debet_div').style.visibility = 'hidden';
			document.getElementById('msp_tgl_valid_div').disabled = 'disabled';
			document.getElementById('msp_tgl_valid_div').style.visibility = 'hidden';
			for(i = 0 ; i < 21 ; i++){
				document.getElementById('rekening_autodebet['+i+']').value = '';
				document.getElementById('rekening_autodebet['+i+']').disabled = 'disabled';
			}
			document.getElementById('msp_no_rekening_autodebet').value = '';
		}
	}
	
	function doPp() {
		if(document.getElementById('msp_rek_nama').value == ''){
			document.getElementById('msp_rek_nama').value = document.getElementById('nama_pp').value;
		}
		document.getElementById('msp_pas_nama_pp').value = document.getElementById('nama_pp').value;
		
	}
	
	function inPp() {
		document.getElementById('nama_pp').value = document.getElementById('msp_pas_nama_pp').value;
	}
	
	function inRek() {
		var rek = document.getElementById('msp_no_rekening').value
		var size = rek.length;
		for(i = 0 ; i < size ; i++){
			document.getElementById('rekening['+i+']').value = rek.charAt(i);
		}
	}
	
	function inRek_autodebet() {
		var rek = document.getElementById('msp_no_rekening_autodebet').value
		var size = rek.length;
		for(i = 0 ; i < size ; i++){
			document.getElementById('rekening_autodebet['+i+']').value = rek.charAt(i);
		}
	}
	
	function doPremi(){
		var lsdbs_number = document.getElementById('produk').value;
		var lscb_id = document.getElementById('lscb_id').value;
		var prm = 0;
		
		if(lsdbs_number == '1' && lscb_id == 3){
				document.getElementById('premi').value = '150000';
			}else if(lsdbs_number == '1' && lscb_id == 6){
				document.getElementById('premi').value = '15000';
			}else if(lsdbs_number == '1' && lscb_id == 1){
				prm = 150000 * 0.27;
				document.getElementById('premi').value = Math.round(prm);
			}else if(lsdbs_number == '1' && lscb_id == 2){
				prm = 150000 * 0.525;
				document.getElementById('premi').value = Math.round(prm);
			}else if(lsdbs_number == '2' && lscb_id == 3){
				document.getElementById('premi').value = '300000';
			}else if(lsdbs_number == '2' && lscb_id == 6){
				document.getElementById('premi').value = '30000';
			}else if(lsdbs_number == '2' && lscb_id == 1){
				prm = 300000 * 0.27;
				document.getElementById('premi').value = Math.round(prm);
			}else if(lsdbs_number == '2' && lscb_id == 2){
				prm = 300000 * 0.525;
				document.getElementById('premi').value = Math.round(prm);
			}else if(lsdbs_number == '3' && lscb_id == 3){
				document.getElementById('premi').value = '500000';
			}else if(lsdbs_number == '3' && lscb_id == 6){
				document.getElementById('premi').value = '50000';
			}else if(lsdbs_number == '3' && lscb_id == 1){
				prm = 500000 * 0.27;
				document.getElementById('premi').value = Math.round(prm);
			}else if(lsdbs_number == '3' && lscb_id == 2){
				prm = 500000 * 0.525;
				document.getElementById('premi').value = Math.round(prm);
			}else if(lsdbs_number == '4' && lscb_id == 3){
				document.getElementById('premi').value = '900000';
			}else if(lsdbs_number == '4' && lscb_id == 6){
				document.getElementById('premi').value = '90000';
			}else if(lsdbs_number == '4' && lscb_id == 1){
				prm = 900000 * 0.27;
				document.getElementById('premi').value = Math.round(prm);
			}else if(lsdbs_number == '4' && lscb_id == 2){
				prm = 900000 * 0.525;
				document.getElementById('premi').value = Math.round(prm);
			}else if(lsdbs_number == '5'){
				document.getElementById('premi').value = '74000';
			}else{
				document.getElementById('premi').value = '0';
			}
	} 
	
	function changeInputAsuransi(){
		var flag = document.getElementById('tambah_input_asuransi').value;
		// kalau pilih tidak, semua inputan fire dikosongin dan didisable otomatis
		if(flag == '0'){
			document.getElementById('msp_fire_code_name').disabled = 'disabled';document.getElementById('msp_fire_phone_number').disabled = 'disabled';
			document.getElementById('msp_fire_name').disabled = 'disabled';document.getElementById('msp_fire_mobile').disabled = 'disabled';
			document.getElementById('msp_fire_identity').disabled = 'disabled';document.getElementById('msp_fire_email').disabled = 'disabled';
			document.getElementById('msp_fire_insured_addr_code').disabled = 'disabled';
			document.getElementById('msp_fire_occupation2').disabled = 'disabled';document.getElementById('msp_fire_insured_addr').disabled = 'disabled';
			document.getElementById('msp_fire_occupation').disabled = 'disabled';document.getElementById('msp_fire_insured_addr_no').disabled = 'disabled';
			document.getElementById('msp_fire_type_business2').disabled = 'disabled';document.getElementById('msp_fire_insured_postal_code').disabled = 'disabled';
			document.getElementById('msp_fire_type_business').disabled = 'disabled';document.getElementById('msp_fire_insured_city').disabled = 'disabled';
			document.getElementById('msp_fire_source_fund2').disabled = 'disabled';document.getElementById('msp_fire_insured_phone_number').disabled = 'disabled';
			document.getElementById('msp_fire_source_fund').disabled = 'disabled';document.getElementById('msp_fire_insured_addr_envir').disabled = 'disabled';
			document.getElementById('msp_fire_addr_code').disabled = 'disabled';document.getElementById('msp_fire_ins_addr_envir_else').disabled = 'disabled';
			document.getElementById('msp_fire_address_1').disabled = 'disabled';document.getElementById('msp_fire_okupasi').disabled = 'disabled';
			document.getElementById('msp_fire_postal_code').disabled = 'disabled';document.getElementById('msp_fire_okupasi_else').disabled = 'disabled';
		}else{// kalau pilih ya, semua inputan fire dibuka otomatis
			document.getElementById('msp_fire_code_name').disabled = '';document.getElementById('msp_fire_phone_number').disabled = '';
			document.getElementById('msp_fire_name').disabled = '';document.getElementById('msp_fire_mobile').disabled = '';
			document.getElementById('msp_fire_identity').disabled = '';document.getElementById('msp_fire_email').disabled = '';
			document.getElementById('msp_fire_insured_addr_code').disabled = '';
			document.getElementById('msp_fire_occupation2').disabled = '';document.getElementById('msp_fire_insured_addr').disabled = '';
			document.getElementById('msp_fire_occupation').disabled = '';document.getElementById('msp_fire_insured_addr_no').disabled = '';
			document.getElementById('msp_fire_type_business2').disabled = '';document.getElementById('msp_fire_insured_postal_code').disabled = '';
			document.getElementById('msp_fire_type_business').disabled = '';document.getElementById('msp_fire_insured_city').disabled = '';
			document.getElementById('msp_fire_source_fund2').disabled = '';document.getElementById('msp_fire_insured_phone_number').disabled = '';
			document.getElementById('msp_fire_source_fund').disabled = '';document.getElementById('msp_fire_insured_addr_envir').disabled = '';
			document.getElementById('msp_fire_addr_code').disabled = '';document.getElementById('msp_fire_ins_addr_envir_else').disabled = '';
			document.getElementById('msp_fire_address_1').disabled = '';document.getElementById('msp_fire_okupasi').disabled = '';
			document.getElementById('msp_fire_postal_code').disabled = '';document.getElementById('msp_fire_okupasi_else').disabled = '';
		}
	}
	
	function editNamaTertanggung(){
		if(document.getElementById('button_edit_nama_tertanggung').value == 'EDIT'){
			document.getElementById('msp_full_name').disabled = ''
			document.getElementById('button_edit_nama_tertanggung').value = 'CANCEL EDIT';
		}else if(document.getElementById('button_edit_nama_tertanggung').value == 'CANCEL EDIT'){
			document.getElementById('msp_full_name').value = document.getElementById('msp_full_name_temp').value;
			document.getElementById('msp_full_name').disabled = 'disabled'
			document.getElementById('button_edit_nama_tertanggung').value = 'EDIT';
		}else{
			document.getElementById('msp_full_name').disabled = 'disabled'
			document.getElementById('button_edit_nama_tertanggung').value = 'EDIT';
		}
	}
	
	function onChangeAgama(){
		if(document.getElementById('lsag_id').value == '6'){
			document.getElementById('msp_agama').disabled = '';
		}else{
			document.getElementById('msp_agama').value = '';
			document.getElementById('msp_agama').disabled = 'disabled';
		}
	}
	
	function onChangePekerjaan(){
		if(document.getElementById('msp_occupation').value == 'LAINNYA'){
			document.getElementById('msp_occupation_else').disabled = '';
		}else{
			document.getElementById('msp_occupation_else').value = '';
			document.getElementById('msp_occupation_else').disabled = 'disabled';
		}
	}
	
	function onChangeExistBp(){
		if(document.getElementById('existya').checked){
			document.getElementById('msp_exist_bp_name').disabled = '';
		}else{
			document.getElementById('msp_exist_bp_name').value = '';
			document.getElementById('msp_exist_bp_name').disabled = 'disabled';
		}
	}
	
	
	function cekUp(){
		var premi = document.getElementById('msp_premi').value;
		var jenis_pas = document.getElementById('jenis_pas').value;
		if(jenis_pas == 'DBD MALL'){
			if(premi == '100000'){
				document.getElementById('msp_up').value = '2000000';
			}
		}else{
			if(premi == '50000'){
				document.getElementById('msp_up').value = '2000000';
			}
			else if(premi == '100000'){
				document.getElementById('msp_up').value = '4000000';
			}
			else if(premi == '150000'){
				document.getElementById('msp_up').value = '6000000';
			}
			else if(premi == '200000'){
				document.getElementById('msp_up').value = '8000000';
			}
			else if(premi == '250000'){
				document.getElementById('msp_up').value = '10000000';
			}
		}
	}
	
	
	function bodyOnLoad(){
		cekRelasi();
		//cekElseOkupasi();
		//cekElseTrio();
		inRek();
		inRek_autodebet();
		inPp();
		//doPremi();
		agen_cek();
		//changeInputAsuransi();
		doEnableDisableRek();
		onChangeAgama();
		onChangePekerjaan();
		cekUp();
	}
	
	function bodyEndLoad(){
		var successMessage = '${successMessage}';
		if(successMessage != ''){
				alert(successMessage);	
				//parentForm = self.opener.document.forms['formpost'];
				//window.opener.transfer_process('${cmd.msp_full_name}','${msp_id}');
				//parentForm.elements['refreshButton'].click();
				window.close();
				}
	}
	
</script>

</head>

<body onload="resizeCenter(650,600);bodyOnLoad();" style="height: 100%;">
<XML ID=xmlData></XML>
<div class="tab-container" id="container1">

		<div class="tab-panes">

			<div id="pane1" class="panes">
<form:form commandName="cmd" name="frmParam" id="frmParam" method="post">
						<fieldset>
					  <legend>DBD Syariah Individu</legend>
					   <table class="result_table2">
					   <tr><td colspan="2">
					   <spring:bind path="cmd.*">
			<c:if test="${not empty status.errorMessages}">
				<div id="error">ERROR:<br>
				<c:forEach var="error" items="${status.errorMessages}">
	- <c:out value="${error}" escapeXml="false" />
					<br />
				</c:forEach></div>
			</c:if>
		</spring:bind>	</td></tr>
						<c:if test="${currentUser.lca_id eq '58'}">
		<tr>
							<td>Appointment Id:</td>
							<td><form:input path="mspo_plan_provider" size="50" cssErrorClass="inpError" readonly="readonly"/><font class="error">*</font></td>
						</tr>
						</c:if>
		<tr>
							<td align="left" bgcolor="orange">Tanggal Aktivasi (efektif Polis):</td>
							<td><spring:bind path="cmd.msp_pas_beg_date" >
        		                    <script>
                                        isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
                                </spring:bind></td>
						</tr>
						<c:if test="${not empty cmd.no_kartu}">
						<tr>
							<td align="left" bgcolor="orange">No. Kartu:</td>
							<td>${cmd.no_kartu}</td>
						</tr>
						</c:if>
						<tr>
							<td align="center" bgcolor="lightgreen" colspan="2" class="subtitle" ><font color="orange">Data Pemegang Polis</font></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">Nama (sesuai KTP):</td>
							<td><input type="text" name="nama_pp" size="65" id="nama_pp" onblur="doPp();" />
							<span style="visibility: hidden"><form:input path="msp_pas_nama_pp" size="1" cssErrorClass="inpError"/></span></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">Tempat, Tgl. lahir:</td>
							<td><form:input path="msp_pas_tmp_lhr_pp" cssErrorClass="inpError"/>
								<spring:bind path="cmd.msp_pas_dob_pp" >
        		                    <script>
                                        var isDisabled;
                                        var status = '${statusForm}';
                                        if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
                                </spring:bind>
							</td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">Jenis Kelamin:</td>
							<td>
							<spring:bind path="cmd.msp_sex_pp">
								<label for="cowok"> <input type="radio" class=noBorder
									name="${status.expression}" value="1"
									<c:if test="${cmd.msp_sex_pp eq 1 or cmd.msp_sex_pp eq null}"> 
												checked</c:if>
									id="cowok">Pria </label>
								<label for="cewek"> <input type="radio" class=noBorder
									name="${status.expression}" value="0"
									<c:if test="${cmd.msp_sex_pp eq 0}"> 
												checked</c:if>
									id="cewek">Wanita </label>
							</spring:bind>
							</td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">Warga Negara:</td>
							<td>
							<spring:bind path="cmd.msp_warga">
									<select name="${status.expression}" >
										<c:forEach var="mw" items="${select_negara}">
											<option
												<c:if test="${status.value eq mw.key}"> SELECTED </c:if>
												value="${mw.key}">${mw.value}</option>
										</c:forEach>
									</select>
								</spring:bind>
							</td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">Status:</td>
							<td>
							<spring:bind path="cmd.msp_status_nikah">
									<select name="${status.expression}" >
										<c:forEach var="sm" items="${select_marital}">
											<option
												<c:if test="${status.value eq sm.ID}"> SELECTED </c:if>
												value="${sm.ID}">${sm.MARITAL}</option>
										</c:forEach>
									</select>
								</spring:bind>
							</td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">Agama:</td>
							<td>
							<spring:bind path="cmd.lsag_id">
								<select name="${status.expression}"  id="lsag_id"  onchange="onChangeAgama();">
										<c:forEach var="d" items="${select_agama}">
											<option
												<c:if test="${status.value eq d.key}"> SELECTED </c:if>
												value="${d.key}">${d.value}</option>
										</c:forEach>
									</select>
										</spring:bind>
								<form:input path="msp_agama" cssErrorClass="inpError"/>
							</td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">Pendidikan:</td>
							<td>
								<spring:bind path="cmd.msp_pendidikan">
									<select name="${status.expression}" >
										<c:forEach var="sp" items="${select_pendidikan}">
											<option
												<c:if test="${status.value eq sp.key}"> SELECTED </c:if>
												value="${sp.key}">${sp.value}</option>
										</c:forEach>
									</select>
								</spring:bind>
							</td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">Pekerjaan:</td>
							<td>
								<spring:bind path="cmd.msp_occupation">
									<select name="${status.expression}" id="msp_occupation" onchange="onChangePekerjaan();">
										<c:forEach var="spk" items="${select_pekerjaan}">
											<option
												<c:if test="${status.value eq spk.ID}"> SELECTED </c:if>
												value="${spk.ID}">${spk.KLASIFIKASI}</option>
										</c:forEach>
									</select>
								</spring:bind>
								<form:input path="msp_occupation_else" cssErrorClass="inpError"/>
							</td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">Bukti Identitas:</td>
							<td>
								<spring:bind path="cmd.lside_id">
									<select name="${status.expression}">
										<c:forEach var="side" items="${select_identitas}">
											<option
												<c:if test="${status.value eq side.key}"> SELECTED </c:if>
												value="${side.key}">${side.value}</option>
										</c:forEach>
									</select>
								</spring:bind>
							</td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">No Identitas:</td>
							<td><form:input path="msp_identity_no" size="65" cssErrorClass="inpError"/></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">Alamat:</td>
							<td><form:input path="msp_address_1" size="85" cssErrorClass="inpError"/></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">Kota:</td>
							<td><form:input path="msp_city" cssErrorClass="inpError"/></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">Kode Pos:</td>
							<td><form:input path="msp_postal_code" cssErrorClass="inpError"/></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">Telp:</td>
							<td><form:input path="msp_area_code_rumah" cssErrorClass="inpError" size="4"/> <form:input path="msp_pas_phone_number" cssErrorClass="inpError"/></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">No. HP:</td>
							<td><form:input path="msp_mobile" cssErrorClass="inpError" disabled="disabled"/></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">No. HP2:</td>
							<td><form:input path="msp_mobile2" cssErrorClass="inpError" disabled="disabled"/></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">Alamat Email:</td>
							<td><form:input path="msp_pas_email" size="65" cssErrorClass="inpError"/></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">Hubungan dengan Tertanggung:</td>
							<td>
								<select name="lsre_id" onchange="cekRelasi();">
									<c:forEach var="rp" items="${relasi_pas}"> 
										<option <c:if test="${cmd.lsre_id eq rp.ID}"> SELECTED </c:if> value="${rp.ID}">${rp.RELATION}
										</option>
									</c:forEach> 
								</select>
							</td>
						</tr>
						<tr>
							<td align="center" bgcolor="lightgreen" colspan="2" class="subtitle" ><font color="orange">Data Tertanggung</font></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">Nama Tertanggung:</td>
							<td><form:input path="msp_full_name" cssErrorClass="inpError" size="65" disabled="disabled"/>
							<input type="button" id="button_edit_nama_tertanggung" value="EDIT" onclick="editNamaTertanggung();"/>
							<input type="hidden" name="msp_full_name_temp" size="1" id="msp_full_name_temp" value="${cmd.msp_full_name}"/>
							</td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">Tempat, Tgl. lahir:</td>
							<td><form:input path="msp_pas_tmp_lhr_tt" cssErrorClass="inpError"/>
								<spring:bind path="cmd.msp_date_of_birth" >
        		                    <script>
                                        var isDisabled;
                                        var status = '${statusForm}';
                                        if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
                                </spring:bind>
							</td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">Jenis Kelamin Tertanggung:</td>
							<td>
							<spring:bind path="cmd.msp_sex_tt">
								<label for="msp_sex_tt_cowok"> <input type="radio" class=noBorder
									name="${status.expression}" value="1"
									<c:if test="${cmd.msp_sex_tt eq 1 or cmd.msp_sex_tt eq null}"> 
												checked</c:if>
									id="msp_sex_tt_cowok">Pria </label>
								<label for="msp_sex_tt_cewek"> <input type="radio" class=noBorder
									name="${status.expression}" value="0"
									<c:if test="${cmd.msp_sex_tt eq 0}"> 
												checked</c:if>
									id="msp_sex_tt_cewek">Wanita </label>
							</spring:bind>
							</td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">No Identitas:</td>
							<td><form:input path="msp_identity_no_tt" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="center" bgcolor="lightgreen" colspan="2" class="subtitle" ><font color="orange">Data DBD Syariah Individu</font></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">Paket:</td>
							<td>
								DBD Syariah
							</td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">jumlah premi:</td>
							<td>
								<select name="msp_premi" id="msp_premi" onchange="cekUp();">
									<c:forEach var="premi_p" items="${premiList}"> 
										<option <c:if test="${cmd.msp_premi eq premi_p.key}"> SELECTED </c:if> value="${premi_p.key}">${premi_p.value}
										</option>
									</c:forEach> 
								</select>
							</td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">UP:</td>
							<td>
								<input type="text" name="msp_up" id="msp_up" readonly="readonly"/>
							</td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">cara pembayaran:</td>
							<td>
								<select name="msp_flag_cc" onchange="doEnableDisableRek();">
									<c:forEach var="ap" items="${autodebet_pas}"> 
										<option <c:if test="${cmd.msp_flag_cc eq ap.ID}"> SELECTED </c:if> value="${ap.ID}">${ap.AUTODEBET}
										</option>
									</c:forEach> 
								</select>
							</td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">bentuk pembayaran:</td>
							<td>
								<select name="lscb_id" id="lscb_id" onchange="doPremi();" disabled="disabled">
									<c:forEach var="cp" items="${carabayar_pas}"> 
										<option <c:if test="${cmd.lscb_id eq cp.ID}"> SELECTED </c:if> value="${cp.ID}">${cp.PAYMODE}
										</option>
									</c:forEach> 
								</select>
							</td>
						</tr>
						<tr>
						    <td align="center" bgcolor="lightgreen" colspan="2" class="subtitle" ><font color="orange">Data Agen</font></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">kode Agen:</td>
							<td>
								<spring:bind path="cmd.msag_id" >
									<input type="text" name="${status.expression}" value="${status.value }" size="8" onblur="agen_cek();" />
								</spring:bind>
								<input type="text" name="nama_agen" id="nama_agen" size="50" disabled="disabled"/>
							</td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">kode ao:</td>
							<td>
								<form:input path="kode_ao" cssErrorClass="inpError"/>
							</td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">pribadi:</td>
							<td>
								<form:checkbox id="pribadi" cssClass="noBorder" path="pribadi" value="1"/>
							</td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">cabang:</td>
							<td>
								<input type="text" name="cabang_agen" id="cabang_agen" size="30" disabled="disabled"/>
							</td>
						</tr>
						<tr>
							<td align="center" bgcolor="lightgreen" colspan="2" class="subtitle" ><font color="orange">Data Rekening Pemegang Polis</font></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">cari bank:</td>
							<td><input type="text" name="caribank1" id="caribank1" onkeypress="if(event.keyCode==13){ document.frmParam.btncari1.click(); return false;}"> 
	              				<input type="button" name="btncari1" id="btncari1" value="Cari" onclick="ajaxSelectWithParam1a(document.getElementById('caribank1').value,'select_bank1','bank1','lsbp_id','', 'BANK_ID', 'BANK_NAMA', '','Silahkan pilih BANK','3');"></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">bank:</td>
							<td><div id="bank1"> 
								<select name="lsbp_id" id="lsbp_id" >
				                  <option value="${cmd.lsbp_id}">${cmd.lsbp_nama}</option>
				                </select>
				                </div>
							</td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">no rekening:</td>
							<td>
							<input type="text" name="rekening[0]" id="rekening[0]" size="1"    maxlength="1" onkeyup="dofo(0, 1);doRek();" />
							<input type="text" name="rekening[1]" id="rekening[1]" size="1"    maxlength="1" onkeyup="dofo(1, 1);doRek();" />
							<input type="text" name="rekening[2]" id="rekening[2]" size="1"    maxlength="1" onkeyup="dofo(2, 1);doRek();" />
							<input type="text" name="rekening[3]" id="rekening[3]" size="1"    maxlength="1" onkeyup="dofo(3, 1);doRek();" />
							<input type="text" name="rekening[4]" id="rekening[4]" size="1"    maxlength="1" onkeyup="dofo(4, 1);doRek();" />
							<input type="text" name="rekening[5]" id="rekening[5]" size="1"    maxlength="1" onkeyup="dofo(5, 1);doRek();" />
							<input type="text" name="rekening[6]" id="rekening[6]" size="1"    maxlength="1" onkeyup="dofo(6, 1);doRek();" />
							<input type="text" name="rekening[7]" id="rekening[7]" size="1"    maxlength="1" onkeyup="dofo(7, 1);doRek();" />
							<input type="text" name="rekening[8]" id="rekening[8]" size="1"    maxlength="1" onkeyup="dofo(8, 1);doRek();" />
							<input type="text" name="rekening[9]" id="rekening[9]" size="1"    maxlength="1" onkeyup="dofo(9, 1);doRek();" />
							<input type="text" name="rekening[10]" id="rekening[10]" size="1"    maxlength="1" onkeyup="dofo(10, 1);doRek();" />
							<input type="text" name="rekening[11]" id="rekening[11]" size="1"    maxlength="1" onkeyup="dofo(11, 1);doRek();" />
							<input type="text" name="rekening[12]" id="rekening[12]" size="1"    maxlength="1" onkeyup="dofo(12, 1);doRek();" />
							<input type="text" name="rekening[13]" id="rekening[13]" size="1"    maxlength="1" onkeyup="dofo(13, 1);doRek();" />
							<input type="text" name="rekening[14]" id="rekening[14]" size="1"    maxlength="1" onkeyup="dofo(14, 1);doRek();" />
							<input type="text" name="rekening[15]" id="rekening[15]" size="1"    maxlength="1" onkeyup="dofo(15, 1);doRek();" />
							<input type="text" name="rekening[16]" id="rekening[16]" size="1"    maxlength="1" onkeyup="dofo(16, 1);doRek();" />
							<input type="text" name="rekening[17]" id="rekening[17]" size="1"    maxlength="1" onkeyup="dofo(17, 1);doRek();" />
							<input type="text" name="rekening[18]" id="rekening[18]" size="1"    maxlength="1" onkeyup="dofo(18, 1);doRek();" />
							<input type="text" name="rekening[19]" id="rekening[19]" size="1"    maxlength="1" onkeyup="dofo(19, 1);doRek();" />
							<input type="text" name="rekening[20]" id="rekening[20]" size="1"    maxlength="1" onkeyup="dofo(20, 1);doRek();" /><font class="error">*</font>
							<span style="visibility: hidden;"><form:input path="msp_no_rekening" id="msp_no_rekening" cssErrorClass="inpError" size="1"/></span></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">cabang:</td>
							<td><form:input path="msp_rek_cabang" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">kota:</td>
							<td><form:input path="msp_rek_kota" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">atas nama:</td>
							<td><form:input path="msp_rek_nama" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="center" bgcolor="lightgreen" colspan="2" class="subtitle" ><font color="orange">Data Rekening Pemegang Polis (AUTODEBET)</font></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">cari bank:</td>
							<td><input type="text" name="caribank2" id="caribank2" onkeypress="if(event.keyCode==13){ document.frmParam.btncari2.click(); return false;}"> 
	              				<input type="button" name="btncari2" id="btncari2" value="Cari" onclick="ajaxSelectWithParam1a(document.getElementById('caribank2').value,'select_bank2','bank2','lsbp_id_autodebet','', 'BANK_ID', 'BANK_NAMA', '','Silahkan pilih BANK','3');"></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">bank:</td>
							<td><div id="bank2"> 
								<select name="lsbp_id_autodebet" id="lsbp_id_autodebet">
				                  <option value="${cmd.lsbp_id_autodebet}">${cmd.lsbp_nama_autodebet}</option>
				                </select>
				                </div>
							</td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">no rekening:</td>
							<td>
							<input type="text" name="rekening_autodebet[0]" id="rekening_autodebet[0]" size="1"    maxlength="1" onkeyup="dofo_autodebet(0, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[1]" id="rekening_autodebet[1]" size="1"    maxlength="1" onkeyup="dofo_autodebet(1, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[2]" id="rekening_autodebet[2]" size="1"    maxlength="1" onkeyup="dofo_autodebet(2, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[3]" id="rekening_autodebet[3]" size="1"    maxlength="1" onkeyup="dofo_autodebet(3, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[4]" id="rekening_autodebet[4]" size="1"    maxlength="1" onkeyup="dofo_autodebet(4, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[5]" id="rekening_autodebet[5]" size="1"    maxlength="1" onkeyup="dofo_autodebet(5, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[6]" id="rekening_autodebet[6]" size="1"    maxlength="1" onkeyup="dofo_autodebet(6, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[7]" id="rekening_autodebet[7]" size="1"    maxlength="1" onkeyup="dofo_autodebet(7, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[8]" id="rekening_autodebet[8]" size="1"    maxlength="1" onkeyup="dofo_autodebet(8, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[9]" id="rekening_autodebet[9]" size="1"    maxlength="1" onkeyup="dofo_autodebet(9, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[10]" id="rekening_autodebet[10]" size="1"    maxlength="1" onkeyup="dofo_autodebet(10, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[11]" id="rekening_autodebet[11]" size="1"    maxlength="1" onkeyup="dofo_autodebet(11, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[12]" id="rekening_autodebet[12]" size="1"    maxlength="1" onkeyup="dofo_autodebet(12, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[13]" id="rekening_autodebet[13]" size="1"    maxlength="1" onkeyup="dofo_autodebet(13, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[14]" id="rekening_autodebet[14]" size="1"    maxlength="1" onkeyup="dofo_autodebet(14, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[15]" id="rekening_autodebet[15]" size="1"    maxlength="1" onkeyup="dofo_autodebet(15, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[16]" id="rekening_autodebet[16]" size="1"    maxlength="1" onkeyup="dofo_autodebet(16, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[17]" id="rekening_autodebet[17]" size="1"    maxlength="1" onkeyup="dofo_autodebet(17, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[18]" id="rekening_autodebet[18]" size="1"    maxlength="1" onkeyup="dofo_autodebet(18, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[19]" id="rekening_autodebet[19]" size="1"    maxlength="1" onkeyup="dofo_autodebet(19, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[20]" id="rekening_autodebet[20]" size="1"    maxlength="1" onkeyup="dofo_autodebet(20, 1);doRek_autodebet();" /><font class="error">*</font>
							<span style="visibility: hidden;"><form:input path="msp_no_rekening_autodebet" id="msp_no_rekening_autodebet" cssErrorClass="inpError" size="1"/></span></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">atas nama:</td>
							<td><form:input path="msp_rek_nama_autodebet" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">tanggal debet:</td>
							<td><div id="msp_tgl_debet_div" >
								<spring:bind path="cmd.msp_tgl_debet" >
        		                    <script>
                                        var isDisabled;
                                        var status = '${statusForm}';
                                        if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
                                </spring:bind>
							<font class="error">*</font></div></td>
						</tr>
						<tr>
							<td align="left" bgcolor="orange">tanggal valid:</td>
							<td><div id="msp_tgl_valid_div" >
								<spring:bind path="cmd.msp_tgl_valid" >
        		                    <script>
                                        var isDisabled;
                                        var status = '${statusForm}';
                                        if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
                                </spring:bind>
							<font class="error">*</font></div></td>
						</tr>
						<tr>
							<td align="center" bgcolor="lightgreen" colspan="2" class="subtitle" ><font color="orange">Dokumen yang Dilengkapi</font></td>
						</tr>
						<tr>
							<td align="left" rowspan="2" bgcolor="orange">ADMIN</td>
							<td>
								<c:choose>
									<c:when test="${cmd.lspd_id != 2}">
									<form:checkbox id="msp_cek_ktp" cssClass="noBorder" path="msp_cek_ktp" value="1"/>Fotokopi KTP
									<form:checkbox id="msp_cek_kk" cssClass="noBorder" path="msp_cek_kk" value="1"/>Fotokopi Kartu Keluarga
									<form:checkbox id="msp_cek_npwp" cssClass="noBorder" path="msp_cek_npwp" value="1"/>Fotokopi NPWP
									<form:checkbox id="msp_cek_rekening" cssClass="noBorder" path="msp_cek_rekening" value="1"/>Fotokopi Rekening
									</c:when>
									<c:otherwise>
									<input type="checkbox" class="noBorder"  <c:if test="${cmd.msp_cek_ktp eq 1}">checked</c:if> disabled>Fotokopi KTP
									<input type="checkbox" class="noBorder"  <c:if test="${cmd.msp_cek_kk eq 1}">checked</c:if> disabled>Fotokopi Kartu Keluarga
									<input type="checkbox" class="noBorder"  <c:if test="${cmd.msp_cek_npwp eq 1}">checked</c:if> disabled>Fotokopi NPWP
									<input type="checkbox" class="noBorder"  <c:if test="${cmd.msp_cek_rekening eq 1}">checked</c:if> disabled>Fotokopi Rekening
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
						<tr>
							<td>
								<c:choose>
									<c:when test="${cmd.lspd_id != 2}">
									<form:checkbox id="msp_cek_bukti_bayar" cssClass="noBorder" path="msp_cek_bukti_bayar" value="1"/>Bukti pembayaran
									<form:checkbox id="msp_cek_srt_keterangan" cssClass="noBorder" path="msp_cek_srt_keterangan" value="1"/>Surat Keterangan
									<form:checkbox id="msp_cek_akte_kelahiran" cssClass="noBorder" path="msp_cek_akte_kelahiran" value="1"/>Akte Kelahiran
									<form:checkbox id="msp_cek_tandatangan" cssClass="noBorder" path="msp_cek_tandatangan" value="1"/>Tanda tangan sudah di Cek									
									</c:when>
									<c:otherwise>
									<input type="checkbox" class="noBorder"  <c:if test="${cmd.msp_cek_bukti_bayar eq 1}">checked</c:if> disabled>Bukti pembayaran
									<input type="checkbox" class="noBorder" <c:if test="${cmd.msp_cek_srt_keterangan eq 1}">checked</c:if> disabled> Surat Keterangan
									<input type="checkbox" class="noBorder" <c:if test="${cmd.msp_cek_akte_kelahiran eq 1}">checked</c:if> disabled> Akte Kelahiran
									<input type="checkbox" class="noBorder" <c:if test="${cmd.msp_cek_tandatangan eq 1}">checked</c:if> disabled> Tanda tangan sudah di Cek
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
						<c:if test="${not empty cmd.lspd_id && cmd.lspd_id != '1'}">
						<tr>
							<td align="left" rowspan="2" bgcolor="orange">UW</td>
							<td>
									<form:checkbox id="msp_cek_ktp_uw" cssClass="noBorder" path="msp_cek_ktp_uw" value="1"/>Fotokopi KTP
									<form:checkbox id="msp_cek_kk_uw" cssClass="noBorder" path="msp_cek_kk_uw" value="1"/>Fotokopi Kartu Keluarga
									<form:checkbox id="msp_cek_npwp_uw" cssClass="noBorder" path="msp_cek_npwp_uw" value="1"/>Fotokopi NPWP
							</td>
						</tr>
						<tr>
							<td>
								<form:checkbox id="msp_cek_bukti_bayar_uw" cssClass="noBorder" path="msp_cek_bukti_bayar_uw" value="1"/>
								Bukti Pembayaran
								<form:checkbox id="msp_cek_rekening_uw" cssClass="noBorder" path="msp_cek_rekening_uw" value="1"/>
								Fotokopi Rekening
								<form:checkbox id="msp_cek_srt_keterangan_uw" cssClass="noBorder" path="msp_cek_srt_keterangan_uw" value="1"/>
								Surat Keterangan UW
								<form:checkbox id="msp_cek_akte_kelahiran_uw" cssClass="noBorder" path="msp_cek_akte_kelahiran_uw" value="1"/>
								Akte Kelahiran UW
								<form:checkbox id="msp_cek_tandatangan_uw" cssClass="noBorder" path="msp_cek_tandatangan_uw" value="1"/>
								Tanda tangan sudah di Cek
							</td>
						</tr>
						</c:if>
						</table></fieldset>
						<fieldset>
					   <legend>-</legend>
					   <table class="result_table2">
						<tr>
							<td colspan="2" align="left">
								<font class="error">* HARUS DIISI</font>
							</td>
						</tr>
						<tr>
							<td colspan="2" align="center">
							<input type="hidden" name="flag_posisi" id="flag_posisi" size="1" id="flag_posisi" value="${flag_posisi}"/>
							<input type="hidden" name="jenis_pas" id="jenis_pas" value="${cmd.jenis_pas}" />
							<input type="hidden" name="kata" id="kata" size="1" value="edit" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
								<input type="submit" id="submit" name="save" value="Save"  onclick="return update();" />
					<input type="button" name="close" value="Close" onclick="window.close();"/>
						<input type="hidden" name="result" size="1" value="${result}" />
							</td>
						</tr>
					</table>
					</fieldset>
					</div>
		</div>
	</div>	
</form:form>
<c:if  test="${not empty successMessage}">	
			<script type="text/javascript">													
				alert('${successMessage}');	
				var flag_posisi = document.getElementById('flag_posisi').value;
				parentForm = self.opener.document.forms['formpost'];
				if(flag_posisi == 'dbd_detail_syariah'){
					window.opener.transfer_process('${cmd.msp_fire_id}','${cmd.msp_id}');
					window.close();
				}else{
					parentForm.elements['refreshButton'].click();
					window.close();
				}
				
			</script>
	</c:if>
</body>
</html>