<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<meta HTTP-EQUIV="Expires" CONTENT="-1">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link href="${path}/include/image/eas.ico" rel="shortcut icon">
		<!--  -->
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<!--  -->
		<script type="text/javascript" src="${path }/include/js/default.js"></script>
		<!-- Ajax Related -->
		<script type="text/javascript" src="${path}/include/js/ajax.js"></script>
		<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script>
		<script type="text/javascript" src="${path }/dwr/engine.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>
		<script type="text/javascript">
			hideLoadingMessage();

			function centang(elemen){
				document.getElementById(elemen).checked = true;
				
			}

			function cek(name) {
	  			xx = document.getElementsByName(name);
	  			if(xx[0].checked == true && xx[1].checked == false) {
	  				 xx[1].disabled = true;
	  			}
	  			else if(xx[0].checked == false && xx[1].checked == false){
					xx[0].disabled = false;	
					xx[1].disabled = false;
	  			}
	  			else if(xx[1].checked == true && xx[0].checked == false) {
	  				xx[0].disabled = true;
	 			}	
			}
			
			function kembali(no_spaj, no_trx, tgl_trx, lku_id, nilai_trx, norek_ajs, ket){
				var dok;
				if(self.opener) dok = self.opener.document;
				else dok = parent.document;
				
				var forminput;
				if(dok.formpost) forminput = dok.formpost;
				else if(dok.frmParam) forminput = dok.frmParam;

				if(document.formpost.piyu.value==1){
					return;
				}else if(no_spaj != ''){
					alert('RK ini sudah digunakan pada nomor SPAJ ' + no_spaj);
				}else{
					//lsrek ekalife				
					var empat = norek_ajs.substr(norek_ajs.length-4);
					forminput.ucup.value = empat;
					forminput.ucup.onkeyup();
					
					//tgl rk
					forminput._mspa_date_book.value = tgl_trx;
					forminput.mspa_date_book.value = tgl_trx;
					
					//lku
					forminput.lku_id.value = lku_id;
					
					//nominal
					forminput.mspa_payment.value = nilai_trx;
					
					//ket
					forminput.no_trx.value = no_trx;
					forminput.mspa_desc.value = 'CEK IBANK (No Transaksi ' + no_trx + ')';
					
					window.close();
				}
			}
			
			function cetak(){		
				popWin('${path}/report/bac.pdf?window=list_rk' + 
					'&lsrek_id=' + document.formpost.lsrek_id.value +
					'&startDate=' + document.formpost.startDate.value +
					'&endDate=' + document.formpost.endDate.value +
					'&lsbp_id=' + document.formpost.lsbp_id.value +
					'&kode=' + document.formpost.kode.value +
					'&startNominal=' + document.formpost.startNominal.value +
					'&endNominal=' + document.formpost.endNominal.value, 600, 850);
			}
			
			function cetak2(){		
				popWin('${path}/report/bac.xls?window=list_rk' + 
					'&lsrek_id=' + document.formpost.lsrek_id.value +
					'&startDate=' + document.formpost.startDate.value +
					'&endDate=' + document.formpost.endDate.value +
					'&lsbp_id=' + document.formpost.lsbp_id.value +
					'&kode=' + document.formpost.kode.value +
					'&startNominal=' + document.formpost.startNominal.value +
					'&endNominal=' + document.formpost.endNominal.value, 600, 850);
			}
			
		</script>
	</head>
	<body onload="setupPanes('container1','tab1');" style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Daftar Transfer Bank</a>
				</li>
			</ul>

			<div class="tab-panes">

				<div id="pane1" class="panes">
					<form method="post" name="formpost" action="${path}/uw/uw.htm?window=drek2">
						<fieldset>
							<legend>Parameter</legend>
							<input type="hidden" name="piyu" value="${piyu}">
							<table class="entry2" style="width: auto;">
								<tr>
									<th nowrap="nowrap">Rekening AJS</th>
									<td>
										<select name="lsrek_id">
											<c:set var="grup" value=""/>
											<c:forEach items="${daftarRekAjs}" var="b">
												<c:choose>
													<c:when test="${b.desc ne grup}">
														<c:set var="grup" value="${b.desc}"/>				 
														<OPTGROUP label="${b.desc}">
															<option value="${b.key}" <c:if test="${b.key eq lsrek_id}"> selected </c:if> >${b.value}</option>
													</c:when>
													<c:otherwise>
														<option value="${b.key}" <c:if test="${b.key eq lsrek_id}"> selected </c:if> >${b.value}</option>
													</c:otherwise>
												</c:choose>
												<c:if test="${b.desc ne grup}">
													<c:set var="grup" value="${b.desc}"/>				
													</OPTGROUP>
												</c:if>
											</c:forEach>
										</select>
									</td>
								</tr>
								<tr>
									<th nowrap="nowrap">Bank Nasabah</th>
									<td>
										<select name="lsbp_id">
											<c:forEach items="${daftarBank}" var="b">
												<option value="${b.key}"
													<c:if test="${b.key eq lsbp_id}"> selected </c:if>
												>${b.value}</option>
											</c:forEach>
										</select>
									</td>
								</tr>
								<tr>
									<th nowrap="nowrap">Cabang Bank Nasabah (Khusus Bank Sinarmas)</th>
									<td>
										<select name="kode">
											<c:forEach items="${daftarCab}" var="b">
												<option value="${b.key}"
													<c:if test="${b.key eq kode}"> selected </c:if>
												>${b.value}</option>
											</c:forEach>
										</select>
									</td>
								</tr>
								<tr>
									<th nowrap="nowrap">Tgl RK</th>
									<td>
										<script>inputDate('startDate', '${startDate}', false);</script> s/d 
										<script>inputDate('endDate', '${endDate}', false);</script>									
									</td>
								</tr>
								<tr>
									<th nowrap="nowrap">Nominal</th>
									<td>
										<input type="text" name="startNominal" value="${startNominal}"> s/d 
										<input type="text" name="endNominal" value="${endNominal}">
									</td>
								</tr>
								<tr>
									<th nowrap="nowrap"></th>
									<td>
										<input type="submit" name="cari" value="Show">
										<input type="button" name="print" value="Show PDF" onclick="cetak();">
										<input type="button" name="excel" value="Show Excel" onclick="cetak2();">
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<c:set var="ajs" value="" />
										<table class="displaytag">
											<thead>
												<tr>
													<th style="width: 20px;">Recheck</th>
													<th style="width: 80px;">Tgl TRX</th>
													<th style="width: 150px;">Cabang</th>
													<th style="width: 100px;">No. SPAJ</th>
													<th style="width: 80px;">Kode Trx</th>
													<th style="width: 120px;">No. TRX</th>
													<th style="width: 40px;">Jenis</th>
													<th style="width: 60px;">Kurs TRX</th>
													<th style="width: 90px;">Nilai TRX</th>
													<th style="width: 150px;">Ket</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${daftarDrek}" var="d" varStatus="st">
													<c:choose>
														<c:when test="${ajs ne d.norek_ajs}">
															<tr>
																<td class="left" colspan="9" style="font-size: 8pt; font-weight: bold; color: #FF0000"><br/>[${d.norek_ajs}] ${d.bank_pusat_ajs} ${d.bank_ajs}</td>
															</tr>
															<c:set var="ajs" value="${d.norek_ajs}" />
														</c:when>
														<c:otherwise>
														</c:otherwise>
													</c:choose>
													<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
															<c:if test="${not empty d.no_spaj}"> style="background-color: #C0C0C0" </c:if>
															>
														<td class="center"><input disabled="disabled" class="noBorder" type="checkbox" name="recheck" value="${d.no_trx}" <c:if test="${d.flag_recheck eq 1}">checked="checked"</c:if>></td>
														<td class="center"><fmt:formatDate pattern="dd/MM/yyyy" value="${d.tgl_trx}" /></td>
														<td class="left">${d.nama_cab}</td>
														<td class="left">${d.no_spaj}</td>
														<td class="left">${d.no_trx}</td> 
														<td><input type="hidden" name="no_trx${st.index}" id="no_trx${st.index}" value="${d.no_trx}" onfocus="centang('buang${st.index}');"></td>
														<td class="center">${d.jenis}</td>
														<td class="center">${d.lku_symbol}</td>
														<td><fmt:formatNumber value="${d.nilai_trx}"/></td>
														<td class="left">${d.ket}</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</td>
								</tr>
							</table>
							
						</fieldset>					
					</form>
				</div>
			</div>
			
		</div>

	</body>
</html>