<%@ include file="/include/page/header_jquery.jsp"%>
<script type="text/javascript" src="${path}/include/js/default.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (jQueryUI Tabs) init tab2 Utama
		$("#tabs").tabs();

		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title], label[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		//Bila tidak ada data maka non aktifkan tombol Simpan
		if($("#pp_bdate").val() != '') {
			$('input[name="pp_bdate"]').attr('enabled','enabled');
			$('input[name="tt_bdate"]').attr('enabled','enabled');
			$('input[name="prod_bdate"]').attr('enabled','enabled');
			$('input[name="old_bdate"]').attr('enabled','enabled');
			$('input[name="new_bdate"]').attr('enabled','enabled');
			$('input[name="ket_bdate"]').attr('enabled','enabled');
			$('input[name="no_helpdesk"]').attr('enabled','enabled');
			$('input[name="btnSave"]').attr('enabled','enabled');
		}else{
			$('input[name="pp_bdate"]').attr('disabled','disabled');
			$('input[name="tt_bdate"]').attr('disabled','disabled');
			$('input[name="prod_bdate"]').attr('disabled','disabled');
			$('input[name="old_bdate"]').attr('disabled','disabled');
			$('input[name="new_bdate"]').attr('disabled','disabled');
			$('input[name="ket_bdate"]').attr('disabled','disabled');
			$('input[name="no_helpdesk"]').attr('disabled','disabled');
			$('input[name="btnSave"]').attr('disabled','disabled');
		}
		
		//Jika klik tombol simpan
		$("#btnSave").click(function(){
		
			var spaj = $('#reg_spaj').val();
			var sv = confirm("Apakah anda sudah yakin untuk menyimpan tanggal Beg Date baru untuk No.SPAJ "+spaj+"?");
			
			if(sv){
				return true;
			}else{
				return false;
			}
		});
		
		//bila ada pesan
		//if($("#pesan").val() != '') alert($("#pesan").val());
	});
</script>
	
<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 7em; }
	
	/* untuk align center */
	.tengah { text-align: center; }
	
	/* untuk warna kolom input */
	.warna { color: grey; }
		
	/* styling untuk table input */
	table.inputTable{
		width: 700px;
	}
	table.inputTable tr th{
		width: 200px;
	}
		
	/* styling untuk server-side validation error */
	.errorField { border: 1px solid red; }
	.errorMessage { color: red; display: block;}
	
</style>
	
	<body>
		<form:form method="post" name="formpost" id="formpost" commandName="cmd">
			<fieldset class="ui-widget ui-widget-content">
				<legend class="ui-widget-header ui-corner-all"><div>BegDate Editor</div></legend>
				
				<table class="displaytag" id="tab1">
					<tr>
						<th style="width: 200px;" align="left">Masukkan No.SPAJ</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="No SPAJ" path="reg_spaj" maxlength="11" cssErrorClass="errorField"/>
						</td>
						<td>
							<form:errors path="reg_spaj" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>
							<input type="submit" name="btnCari" id="btnCari" title="Cari" value="Cari">
						</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>
							<em><form:errors path="pesan" cssClass="errorMessage"/></em>
						</td>
					</tr>
					<tr><td><br></td></tr>
				</table>
				
				<table class="displaytag" id="tab2">
					<tr>
						<th style="width: 200px;" align="left">Pemegang Polis</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="Nama Pemegang Polis" path="pp_bdate" cssClass="warna" size="55" readonly="true"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">Tertanggung</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="Nama Tertanggung" path="tt_bdate" cssClass="warna" size="55" readonly="true"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">Produk Utama</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="Produk Utama" path="prod_bdate" cssClass="warna" size="55" readonly="true"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">BegDate Lama</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="Beg Date lama" path="old_bdate" cssClass="lebar tengah warna" readonly="true"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">BegDate Baru</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="Beg Date Baru" path="new_bdate" cssClass="datepicker" cssErrorClass="datepicker errorField"/>
							<form:errors path="new_bdate" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">User Pemohon</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="Keterangan Edit" path="ket_bdate" cssErrorClass="errorField"/>
						</td>
						<td>
							<form:errors path="ket_bdate" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">No. Helpdesk</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="No Helpdesk" path="no_helpdesk" maxlength="5" cssErrorClass="errorField"/>
						</td>
						<td>
							<form:errors path="no_helpdesk" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left"></th>
						<th>&nbsp;&nbsp;&nbsp;</th>
						<td>
							<input type="submit" name="btnSave" id="btnSave" title="Simpan" value="Simpan">
						</td>
					</tr>
				</table>
			</fieldset>
		</form:form>
	</body>
</html>
