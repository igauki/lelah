<%@ include file="/include/page/header.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />

<html>
  <head>
	<script language="JavaScript">
		function confirm(){
			if(confirm("Apakah no reg_spaj sudah benar?")){
				return true;
			}else{
				return false;
			}
		}
	</script>
  </head>
  <body>
    <form name="frmParam" id="frmParam" method="post">
		<div>
       		<table width="80%">
	   			<tr><td colspan="2"><br/><br/></td></tr>
	   			<tr>
					<td valign="top">
						<span>data lst_json_temp (hari ini)</span>
					</td>
	   				<td>
	   					<table width="100%" border="1" bordercolor="black" style="border-collapse: collapse;">
							<tr>
								<th>
									<span>input_date</span>
								</th>
								<th>
									<span>no_gadget</span>
								</th>
								<th>
									<span>no_va</span>
								</th>
								<th>
									<span>spaj_temp</span>
								</th>
								<th>
									<span>process_status</span>
								</th>
							</tr>
							<c:if test="${fn:length(json_temp) gt 0}">
								<c:forEach items="${json_temp}" var="data">
									<tr>
										<td>
											${data.INPUT_DATE}
										</td>
										<td>
											${data.NO_GADGET}
										</td>
										<td>
											${data.NO_VA}
										</td>
										<td>
											${data.SPAJ_TEMP}
										</td>
										<td>
											${data.PROCESS_STATUS}
										</td>
									</tr>
								</c:forEach>
							</c:if>							
							<c:if test="${fn:length(json_temp) lt 1}">
								<tr>
									<td colspan="5" align="center">Tidak ada data</td>
								</tr>
							</c:if>
	   					</table>
	   				</td>
	   			</tr>
	   			<tr><td colspan="2"><br/></td></tr>
		        <tr>
					<td width="40%" valign="top">
						<span>data yg sudah submit dan bayar, dan belum dijalankan jobs</span>
					</td>
					<td width="60%"> 
						<!-- :&nbsp;<input type="text" value ="${jumlah}" disabled style="border:0px; background-color:inherit; color:black; font-size:10px;"> -->
	        			<table width="100%" border="1" bordercolor="black" style="border-collapse: collapse;">
							<tr>
								<th>
									<span>contract_no</span>
								</th>
								<th>
									<span>no_va</span>
								</th>
							</tr>
							<c:if test="${fn:length(daftar) gt 0}">
								<c:forEach items="${daftar}" var="data">
									<tr>
										<td>
											${data.CONTRACT_NO}
										</td>
										<td>
											${data.NO_VA}
										</td>
									</tr>
								</c:forEach>
							</c:if>						
							<c:if test="${fn:length(daftar) lt 1}">
								<tr>
									<td colspan="2" align="center">Tidak ada data</td>
								</tr>
							</c:if>
						</table>
	        		</td>
	   			</tr>
	   			<tr><td colspan="2"><br/></td></tr>
	   			<tr>
					<td valign="top">
						<span>data yg sudah submit dan bayar, dan belum production (jobs jalan tapi error)</span>
					</td>
	   				<td>
	   					<table width="100%" border="1" bordercolor="black" style="border-collapse: collapse;">
							<tr>
								<th>
									<span>noid</span>
								</th>
								<th>
									<span>fullname</span>
								</th>
								<th>
									<span>no_va</span>
								</th>
								<th>
									<span>input_date</span>
								</th>
								<th>
									<span>reg_spaj</span>
								</th>
							</tr>
							<c:if test="${fn:length(belum_prod) gt 0}">
								<c:forEach items="${belum_prod}" var="data">
									<tr>
										<td>
											${data.NOID}
										</td>
										<td>
											${data.FULLNAME}
										</td>
										<td>
											${data.NO_VA}
										</td>
										<td>
											${data.INPUT_DATE}
										</td>
										<td>
											${data.REG_SPAJ}
										</td>
									</tr>
								</c:forEach>
							</c:if>							
							<c:if test="${fn:length(belum_prod) lt 1}">
								<tr>
									<td colspan="5" align="center">Tidak ada data</td>
								</tr>
							</c:if>
	   					</table>
	   				</td>
	   			</tr>
	   			<tr><td colspan="2"><br/></td></tr>
	   			<tr>
					<td valign="top">
						<span>Job Jalan dan Sudah Production</span>
					</td>
	   				<td>
	   					<table width="100%" border="1" bordercolor="black" style="border-collapse: collapse;">
							<tr>
								<th>
									<span>NOID</span>
								</th>
								<th>
									<span>NO_POLIS</span>
								</th>
								<th>
									<span>REG_SPAJ</span>
								</th>
								<th>
									<span>ALREADY_PRODUCTION | FLAG PRODUCTION</span>
								</th>
								<th>
									<span>ACTION</span>
								</th>
							</tr>
							<c:if test="${fn:length(fail_prod) gt 0}">
								<c:forEach items="${fail_prod}" var="data">
									<tr>
										<td>
											${data.NOID}
										</td>
										<td>
											${data.NO_POLIS}
										</td>
										<td>
											${data.REG_SPAJ}
										</td>
										<td>
											${data.ALREADY_PRODUCTION} | ${data.FLAG_PRODUCTION}
										</td>
										<td>
											<a href="/E-Lions/uw/uw.htm?window=monitor_ws&noid=${data.NOID}&retryProd=1">retry</a>
											
	   									</td>
									</tr>
								</c:forEach>
							</c:if>							
							<c:if test="${fn:length(fail_prod) lt 1}">
								<tr>
									<td colspan="5" align="center">Tidak ada data</td>
								</tr>
							</c:if>
						</table>
	   				</td>
	   			</tr>
	   			
	   			<tr><td colspan="2"><br/></td></tr>
	   			
	   			
	   			<tr>
					<td valign="top">
						<span>Payment yang butuh di retry</span>
					</td>
	   				<td>
	   					<table width="100%" border="1" bordercolor="black" style="border-collapse: collapse;">
							<tr>
								<th>
									<span>NOID</span>
								</th>
								<th>
									<span>NO VA</span>
								</th>
								<th>
									<span>CONTRACT NO</span>
								</th>
								<th>
									<span>FLAG PAYMENT</span>
								</th>
								<th>
									<span>ACTION</span>
								</th>
							</tr>
							<c:if test="${fn:length(fail_payment) gt 0}">
								<c:forEach items="${fail_payment}" var="data">
									<tr>
										<td>
											${data.NOID}
										</td>
										<td>
											${data.NO_VA}
										</td>
										<td>
											${data.AMOUNT}
										</td>
										<td>
											${data.FLAG_PAYMENT}
										</td>
										<td>
											<a href="/E-Lions/uw/uw.htm?window=monitor_ws&noid=${data.NOID}&retryPayment=1">retry</a>
											
	   									</td>
									</tr>
								</c:forEach>
							</c:if>							
							<c:if test="${fn:length(fail_payment) lt 1}">
								<tr>
									<td colspan="5" align="center">Tidak ada data</td>
								</tr>
							</c:if>
						</table>
	   				</td>
	   			</tr>
	   			<tr><td colspan="2"><br/></td></tr>
	   			
	   			<tr>
					<td valign="top">
						<span>Fail to fill deposit</span>
					</td>
	   				<td>
	   					<table width="100%" border="1" bordercolor="black" style="border-collapse: collapse;">
							<tr>
								
								<th>
									<span>REG SPAJ</span>
								</th>
								<th>
									<span>ACTION</span>
								</th>
							</tr>
							<c:if test="${fn:length(fail_to_deposit) gt 0}">
								<c:forEach items="${fail_to_deposit}" var="data">
									<tr>
										<td>
											${data.REG_SPAJ}
										</td>
										
										<td>
											<a href="/E-Lions/uw/uw.htm?window=monitor_ws&spaj=${data.REG_SPAJ}&retryDeposit=1">retry</a>
											
	   									</td>
									</tr>
								</c:forEach>
							</c:if>							
							<c:if test="${fn:length(fail_payment) lt 1}">
								<tr>
									<td colspan="5" align="center">Tidak ada data</td>
								</tr>
							</c:if>
						</table>
	   				</td>
	   			</tr>
	   				
	   			<tr><td colspan="2"><br/></td></tr>
	   			<tr>
	   				<td>
	   					<span>no reg_spaj yang akan di fixing</span>
	   				</td>
	   				<td>
   						<input type="text" name="reg_spaj" maxlength="20" style="width:200px;" <c:if test="${fn:length(belum_prod) lt 1}">disabled</c:if>/>
   						&nbsp;
   						<input type="submit" name="btnFixJob" value="Fix Job" onclick="confirm();" <c:if test="${fn:length(belum_prod) lt 1}">disabled</c:if>/>
	   				</td>
	   			</tr>
	   			
	   			
	   			
	   			
	   			<c:if test="${reg_spaj != null}">
		   			<tr><td colspan="2"><br/></td></tr>
		   			<tr><td colspan="2">fixing no reg_spaj ${reg_spaj} sukses!</td></tr>
	   			</c:if>
   			</table>
		</div>
	</form>
  </body>
</html>
