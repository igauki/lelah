<%@ include file="/include/page/header.jsp"%>
<script>
	function awal(){
		var info=formpost.info.value;
		if(info=='1'){
			alert("Posisi Polis ini Ada di "+formpost.param.value);
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
		}else if(info=='2'){
			alert("Proses Simultan Belum dilakukan");
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
		}else if(info=='3'){
			if(confirm("Proses Reas sudah pernah dilakukan \nType Reas ="+formpost.param.value+
						" \nView Hasil Sebelumnya? ")){
				window.location='${path}/uw/simultanpolis.htm?spaj=${cmd.spaj}';			
			}else
				window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
		}else if(info=='4'){
			if(confirm("Polis ini non-standard, Extra Premi Belum Ada !!!\nAnda Ingin Proses Reas?")){
				formpost.submit();
			}
		}else if(info=='0'){
			if(confirm("Anda Ingin Lakukan Proses Reas ?", "U/W")){
				formpost.submit();
			}
		}else if(info=='5'){
			alert("Tidak Bisa proses Simultan dan Reas\npolis ASM");
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
		}
		
		var suc='${submitSuccess}';
		if(suc=='true'){
			alert("Proses Reas Berhasil. TYpe Reas adalah ${proses}");
			window.location='${path}/uw/simultanpolis.htm?spaj=${cmd.spaj}';			
			if('${medis}'=='1')
				popWin('${path}/uw/medical.htm?spaj=${cmd.spaj}', 500, 800);
		}
		
	}
	
</script>	

<body onload="awal()">
	<form name="formpost" method="post">
		<table>
			<tr>
				<td>
					<c:if test="${empty status.errorMessages}">
						<div id="success">Silahkan Tunggu... Sedang dilakukan Proses Reas...</div>
					</c:if>	
					<input type="hidden" name="info" value="${cmd.info}">
					<input type="hidden" name="param" value="${cmd.lsPosDoc}">
				</td>
			</tr>
			<tr>
			 <td>
		  			<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
								ERROR:<br>
									<c:forEach var="error" items="${status.errorMessages}">
												- <c:out value="${error}" escapeXml="false" />
										<br/>
									</c:forEach>
							</div>
						</c:if>									
					</spring:bind>
				</td>
			</tr>
		</table>
	</form>
</body>

<%@ include file="/include/page/footer.jsp"%>