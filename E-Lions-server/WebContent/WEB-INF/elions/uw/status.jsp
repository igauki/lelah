<%@ include file="/include/page/header.jsp"%>
<script>
function awal(){
	
	var berhasil="${submitSuccess }";
	if(berhasil=="true"){
		frm_batal.Ok.disabled=true;
		frm_batal.Send.disabled=false;
	}
	err='${cmd.error}';
	if(err==1){
	//jika ada edit keterangan
		frm_batal.Ok.disabled=true;
		frm_batal.Send.disabled=true;
	}else if(err==2){
		frm_batal.Ok.disabled=true;
		frm_batal.Send.disabled=true;
		alert("Email Anda Belum Terdaftar.. \nSIlahkan Hubungi (Ferry--EDP) untuk pendaftaran.");
		window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
	}

}


function kirim(){
	len=frm_batal.status.length-1;
	ket=frm_batal.status[len].value;
	bts=ket.indexOf('~');
	stat=ket.substring(bts+1,ket.length);
	ket=ket.substring(0,bts);
	if(frm_batal.status[len].value!=null && frm_batal.status[len].value!=''){
		if (confirm("Anda Ingin kirim Email ke Admin dan CC ke agen? \nKeterangan yang anda kirim adalah "+
			ket+"\nDengan Keterangan :"+frm_batal.textarea[len].value)){
			frm_batal.temp.value=frm_batal.status[len].value;
			frm_batal.flagEmail.value="1";
			frm_batal.submit();
		}
	}else
		alert("Status Tidak Boleh Kosong \nSilahkan Pilih status polis ini?");
}

function ubah(i){
	frm_batal.brs.value=i;
	frm_batal.proses.value=10;
	ket=prompt("Silahkan masukan Keterangan yang baru","");
	if(ket!=''){
		frm_batal.ket.value=ket
		frm_batal.submit();
	}else
		alert("keterangan Tidak BOleh Kosong");
		
}

function simpan(){
	
	//len=frm_batal.status.length-1;
	//ket=frm_batal.status[len].value;
	var lca='${cmd.lcaIdPp}';
	ket=document.getElementById("status").value;	
	t=ket.indexOf('~');
	ket=ket.substr(t+1,ket.length);
	s=ket.indexOf('+');
	ket2=ket.substr(s-1,s);
	
	if(ket2==4){
		periode=prompt("Silahkan Masukan Periode Pembayaran Extra Permi ","setiap tahun");
		if(periode!=null){
			document.getElementById("periode").value=periode;
			frm_batal.submit();
		}	
	}else
	    if(ket2==2 && lca !=40 ){
	     	popWin('${path}/uw/uw.htm?window=inputattlist&spaj='+'${cmd.spaj}', 300, 400);
	     }	   
		 frm_batal.submit();
	
	
}

function cekStatus(){
	var status = document.getElementById("status").value;
	statuslength = status.length;
	
	statusid = status.substring(statuslength-1, statuslength);
	lssa_id = status.substring(statuslength-3,statuslength-2);
	
	if(statusid==1) {		
		ajaxSelectWithParam('3','selectLstStatusAcceptSub','statusacceptsub','substatus','','SUB_ID','SUB_DESC','');
		//window.location= '${path}/uw/status.htm?lssa_id='+statusid;
		frm_batal.substatus.style.visibility = "visible";
	}else 
		frm_batal.substatus.style.visibility = "hidden";
}

</script>

<body onload="awal();cekStatus();" style="height: 100%;">
	<div id="contents">
		<form method="post" name="frm_batal" enctype="multipart/form-data">
		<table class="entry2">
			<tr>
				<input type="hidden" readOnly name="spaj" value="${cmd.spaj}" >
				<th>Tanggal</th>
				<th>Akseptor</th>
				<th>Status</th>
				<th>Keterangan</th>
			</tr>
			<c:forEach var="inv" items="${cmd.lsPosition}" varStatus="xt">
				<tr>
					<td><input type="text" size="22" readOnly name="tanggal" value="${inv.MSPS_DATE}" ></td>
					<td><input type="text" readOnly name="lus id"
						<c:forEach var="user" items="${lsUser}">
							<c:if test="${ (inv.LUS_ID eq user.LUS_ID) }">
							 value="${user.LUS_LOGIN_NAME}"
							 </c:if>
						</c:forEach>	 
						></td>
					<td>

					<c:choose>
						<c:when test="${ xt.index ne (inv.SIZE)}">
							<c:forEach var="status" items="${lsStatusAccept}">
								<c:if test="${ (inv.LSSA_ID eq status.LSSA_ID)}">${status.STATUS_ACCEPT}</c:if>							
							</c:forEach>
							<br>
							${inv.SUB_DESC}
						</c:when>
						<c:otherwise>
							<select onchange="cekStatus();" name="status" id="status">
								<option />
								<c:forEach var="status" items="${lsStatusAccept}">
									<option value="${status.STATUS_ACCEPT}~${status.LSSA_ID}+${status.LSSA_SUB}"
										<c:if test="${ (inv.LSSA_ID eq status.LSSA_ID) 
										 }">selected</c:if>
									> ${status.STATUS_ACCEPT } </option>
								</c:forEach>
							</select>
							<c:if test="${inv.LSSA_ID eq 3 or inv.LSSA_ID eq 5}">	
								<br>
								<div id="statusacceptsub">
									<select name="substatus" id="substatus">
					                  <option value="${substatus.SUB_ID}">${substatus.sub_desc}</option>
					                </select>
				                </div>
							</c:if>
						</c:otherwise>
					</c:choose>
					
					
					</td>
					<td><textarea name="textarea" cols="60" rows="4" onkeyup="textCounter(this, 450); "
						onkeydown="textCounter(this, 450); "
							<c:choose>
	       						<c:when test="${xt.index ne inv.SIZE}" > 
									readOnly
								</c:when>
							</c:choose>
						>${inv.MSPS_DESC}</textarea>
						<c:if test="${ (inv.LSSA_ID eq 3) && xt.index ne inv.SIZE}">
							 <input type="button"  name="btnEdit" value="Edit" title="Ubah keterangan" onClick="ubah(${xt.index} );">
 						</c:if>
					</td>
				</tr>
			</c:forEach>	
			<tr>
				<td colspan="4">
						Attachment : <input type="file" name="unduh" size="70">
				</td>
			</tr>
	</table>
	<table width="60%" class="entry2">
		<spring:bind path="cmd.flag_ut">
			<input type="hidden" name="${status.expression}" value="${status.value}">
		</spring:bind>	

		<c:if test="${cmd.flag_ut eq 1 || cmd.flag_ut eq 3}">
			<tr>
				<th>Email Agen</th>
				<td>
					<spring:bind path="cmd.emailAgen">
						<input size="30" type="text" name="${status.expression}" value="${status.value}">
					</spring:bind>
				</td>
			</tr>
		</c:if>
		<c:if test="${cmd.flag_ut eq 2 || cmd.flag_ut eq 3}">
			<tr>
				<th>Email Admin</th>
				<td>
					<spring:bind path="cmd.emailAdmin">
						<input size="30" type="text" name="${status.expression}" value="${status.value}">
					</spring:bind>
				</td>
			</tr>
		</c:if>
		<tr>
			<td colspan="4"><div align="center">
				   <input type="hidden" name="flagEmail" value="0">
				   <input type="hidden" name="brs" >
				   <input type="hidden" name="ket" >
				   <spring:bind path="cmd.periode">
					 	<input type="hidden" name="${status.expression}" value="${status.value}" id="${status.expression}">
					</spring:bind> 	
				   <input type="hidden" name="proses" value="${cmd.flagAdd}">
				   <input type="hidden" name="temp">
				   <input type="button" name="Ok" value="Save" onClick="simpan();" >
				   <input type="button" disabled name="Send" value="Send Email"  onclick="kirim();" >
			</div></td>
		</tr>
		<tr> 
	        <td nowrap="nowrap" colspan="4">
			    <input type="hidden" name="flag" value="0">
	        	<c:if test="${not empty submitSuccess && cmd.flag_ut ne 0} ">
		        	<div id="success">
			        	<input type="hidden" name="flag" value="0">
			        	Berhasil Disimpan
		        	</div>
		        </c:if>	
	  			<spring:bind path="cmd.*">
					<c:if test="${not empty status.errorMessages}">
						<div id="error">
			        		Pesan:<br>
								<c:forEach var="error" items="${status.errorMessages}">
											- <c:out value="${error}" escapeXml="false" />
									<br/>
								</c:forEach>
						</div>
					</c:if>									
				</spring:bind>
			</td>
   		  </tr>
	</table>
	</form>	
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>