<%@ include file="/include/page/header.jsp"%>
<script>
	function backToParent(spaj){
		if(self.opener.document.getElementById('infoFrame') 
				&& self.opener.document.getElementById('spaj')){
			self.opener.document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj; 
			self.opener.document.getElementById('spaj').value=spaj; 
			if(self.opener.document.getElementById('jenisWindow')){
				if(self.opener.document.getElementById('jenisWindow').value=='viewer')
					self.opener.ajaxPesan(spaj, -1);
				else if(self.opener.document.getElementById('jenisWindow').value=='komisi')
					self.opener.ajaxPesan(spaj, 8);
			}
			
		}
		window.close();	
	}
	function cek(selek, cekbok, tanggalan){
		if(selek && cekbok &&
				document.getElementById(tanggalan+'_Month_ID') &&
				document.getElementById(tanggalan+'_Year_ID') &&
				document.getElementById(tanggalan+'_Day_ID')){
			nilai = selek.options[selek.selectedIndex].value;
			if(nilai==2 || nilai==3) {
				cekbok.disabled=false;
				document.getElementById(tanggalan+'_Month_ID').disabled=false;
				document.getElementById(tanggalan+'_Year_ID').disabled=false;
				document.getElementById(tanggalan+'_Day_ID').disabled=false;
			}else{
				cekbok.disabled=true;
				document.getElementById(tanggalan+'_Month_ID').disabled=true;
				document.getElementById(tanggalan+'_Year_ID').disabled=true;
				document.getElementById(tanggalan+'_Day_ID').disabled=true;
			}
			
			
		}
	}
</script>
<BODY onload="setFocus('kata'); document.title='PopUp :: Cari Nasabah';" style="height: 100%;">
<form method="post" name="formpost" action="${path }/uw/carinasabah.htm">
	<div id="contents">
		<fieldset>
		<legend>Cari Data Nasabah</legend>
		<table class="entry">
<!-- comment this
		<input type="hidden" name="posisi" value="${param.posisi}">
-->
			<tr>
				<th>Cari:</th>
				<td>
					<select name="tipe" onchange="cek(this, document.formpost.cekTglLahir, 'tglLahir'); document.formpost.kata.focus();">
						<c:forEach var="r" items="${cmd.listTipe}" varStatus="st">
							<option value="${st.index}" <c:if test="${st.index eq param.tipe}">selected</c:if>>${r}</option>
						</c:forEach>
					</select>
					<select name="pilter">
						<option <c:if test="${\"LIKE%\" eq param.pilter}">selected</c:if> value="LIKE%">LIKE%</option>
						<option <c:if test="${\"%LIKE\" eq param.pilter}">selected</c:if> value="%LIKE">%LIKE</option>
						<option <c:if test="${\"%LIKE%\" eq param.pilter}">selected</c:if> value="%LIKE%">%LIKE%</option>
						<option <c:if test="${\"LT\" eq param.pilter}">selected</c:if> value="LT"><</option>
						<option <c:if test="${\"LE\" eq param.pilter}">selected</c:if> value="LE"><=</option>
						<option <c:if test="${\"EQ\" eq param.pilter}">selected</c:if> value="EQ">=</option>
						<option <c:if test="${\"GE\" eq param.pilter}">selected</c:if> value="GE">>=</option>
						<option <c:if test="${\"GT\" eq param.pilter}">selected</c:if> value="GT">></option>
					</select>
					<input type="text" autocomplete="off" name="kata" size="40" value="${param.kata }" class="upperCase">
					<label for="cekTglLahir">
			    		<input type="checkbox" class="noBorder" name="cekTglLahir" id="cekTglLahir" value="true" <c:if test="${not empty param.cekTglLahir }">checked</c:if>>DAN Tgl Lahir
					</label>
					<script>inputDate('tglLahir', '', false);</script>
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<input type="submit" name="search" value="Search" onclick="this.disabled=true;this.value='Please Wait...';createLoadingMessage(); document.formpost.submit();">
					<input type="button" name="close" value="Close" onclick="window.close();">
				</td>
			</tr>
		</table>
		<table class="simple">
			<thead>
				<tr>
					<th rowspan="2">No.</th>
					<th colspan="3">Nasabah</th>
					<th rowspan="2">SPAJ</th>
					<th colspan="6">Polis</th>
				</tr>
				<tr>
					<th>Pemegang Polis</th>
					<th>Tertanggung</th>
					<th>Alamat</th>
					<th>No. Polis</th>
					<th>TSI</th>
					<th>Premi Std</th>
					<th>Begin Date</th>
					<th>End Date</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="n" items="${cmd.listNasabah}" varStatus="stat">
					<c:choose>
						<c:when test="${n.AKSES_FLAG eq \"Y\" or empty n.AKSES_FLAG}">
							<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
								onclick="backToParent('${n.REG_SPAJ }');">
								<td>${stat.count }</td>
								<td>${n.PP}</td>
								<td>${n.TT}</td>
								<td>${n.ALAMAT_RUMAH} ${n.KOTA_RUMAH}</td>
								<td><elions:spaj nomor="${n.REG_SPAJ }"/></td>
								<td><elions:polis nomor="${n.MSPO_POLICY_NO}"/></td>
								<td><fmt:formatNumber value="${n.MSPR_TSI}" type="currency" currencySymbol="${n.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" /></td>
								<td><fmt:formatNumber value="${n.MSPR_PREMIUM}" type="currency" currencySymbol="${n.LKU_SYMBOL}" maxFractionDigits="2" minFractionDigits="0" /></td>
								<td><fmt:formatDate value="${n.MSTE_BEG_DATE}" pattern="dd/MM/yyyy"/></td>
								<td><fmt:formatDate value="${n.MSTE_END_DATE}" pattern="dd/MM/yyyy"/></td>
								<td>${n.LSSP_STATUS}</td>
							</tr>
						</c:when>
						<c:otherwise>
							<tr bgcolor="999999"  onclick="alert('Maaf tetapi anda tidak mempunyai otorisasi untuk melihat polis ini!');">
								<td>${stat.count }</td>
								<td colspan=3>-</td>
								<td><elions:spaj nomor="${n.REG_SPAJ }"/></td>
								<td><elions:polis nomor="${n.MSPO_POLICY_NO}"/></td>
								<td colspan=5>Cabang: [${n.LCA_ID}] ${n.LCA_NAMA }</td>
							</tr>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</tbody>
		</table>
		<div class="info">
			* Klik untuk melihat detail<br/>
			* Polis berwarna gelap tidak dapat diakses<br/>
		</div>
	</fieldset>
	</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>