<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path}/include/js/disableKeys.js"></script><!-- Disable Some Keys -->
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript">
	hideLoadingMessage();
</script>
</head>
<body style="height: 100%;" onload='setupPanes("container1", "tab1");'>
<div id="contents">
	<form method="post" name="formpost">
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li><a href="#" onClick="return showPane('pane1', this)" id="tab1">Informasi Komisi</a></li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes" style="text-align: left;">
					<table class="entry2" style="width: 75%;">
						<tr>
							<th>Jalur Distribusi</th>
							<td>
								<select name="dist" onchange="document.formpost.submit();">
									<c:forEach items="${daftarDist}" var="d">
										<option value="${d.key}" 
											<c:if test="${dist eq d.key}">selected</c:if>
										>${d.value}</option>
									</c:forEach>
								</select>
							</td>
							<th>Produk</th>
							<td>
								<c:if test="${not empty daftarProduk}">
									<select name="prod" onchange="document.formpost.show.click();">
										<c:forEach items="${daftarProduk}" var="d">
											<option value="${d.key}"
												<c:if test="${prod eq d.key}">selected</c:if>
											>[${d.key}] ${d.value}</option>
										</c:forEach>
									</select>
								</c:if>
							</td>
							<td><input type="submit" value="Show" name="show"></td>
						</tr>
						<c:if test="${not empty daftarKomisi}">
							<tr>
								<td colspan="5">
									<c:choose>
										<c:when test="${IS_POWERSAVE eq 0}">
											<table class="simple">
												<thead>
													<tr>
														<th style="text-align: center;" rowspan="2">Produk</th>
														<th style="text-align: center;" rowspan="2">Kurs</th>
														<th style="text-align: center;" rowspan="2">Jenis Komisi</th>
														<th style="text-align: center;" rowspan="2">Tanggal Berlaku</th>
														<th style="text-align: center;" rowspan="2">Cara Bayar</th>
														<th style="text-align: center;" colspan="4">(%)</th>
													</tr>
													<tr>
														<th style="text-align: center;">Komisi</th>
														<th style="text-align: center;">Bonus</th>
														<th style="text-align: center;">UPP Evaluasi</th>
														<th style="text-align: center;">UPP Kontes</th>
													</tr>
												</thead>
												<tbody>
													<c:set var="produk" value="" />
											         <c:forEach var="k" items="${daftarKomisi}" varStatus="st">
														<tr>
															<c:if test="${produk ne k.LSDBS_NAME}">
																<td nowrap="nowrap" rowspan="${ukuran}">[${k.LSBS_ID}-${k.LSDBS_NUMBER}] ${k.LSDBS_NAME}</td>
																<c:set var="produk" value="${k.LSDBS_NAME}" />
															</c:if>
															<td nowrap="nowrap">${k.LKU_SYMBOL}</td>
															<td nowrap="nowrap">${k.PREMI}</td>
															<td nowrap="nowrap" style="text-align: center;">${k.LSCO_DATE}</td>
															<td nowrap="nowrap" style="text-align: center;">${k.LSCB_PAY_MODE}</td>
															<td nowrap="nowrap"><fmt:formatNumber value="${k.LSCO_COMM}" />%</td>
															<td nowrap="nowrap"><fmt:formatNumber value="${k.LSCO_BONUS}" />%</td>
															<td nowrap="nowrap"><fmt:formatNumber value="${k.UPP_EVA}" />%</td>
															<td nowrap="nowrap"><fmt:formatNumber value="${k.UPP_KONTES}" />%</td>
														</tr>
											         </c:forEach>
										         </tbody>
											</table>
										</c:when>
										<c:otherwise>
											<table class="simple">
												<thead>
													<tr>
														<th style="text-align: center;" rowspan="3">Produk</th>
														<th style="text-align: center;" rowspan="3">Kurs</th>
														<th style="text-align: center;" rowspan="3">Jenis Komisi</th>
														<th style="text-align: center;" rowspan="3">Tanggal Berlaku</th>
														<th style="text-align: center;" rowspan="3">Cara Bayar</th>
														<th style="text-align: center;" colspan="16">(%)</th>
													</tr>
													<tr>
														<th style="text-align: center;" rowspan="2">Komisi</th>
														<th style="text-align: center;" rowspan="2">Bonus</th>
														<th style="text-align: center;" colspan="7">UPP Evaluasi (MGI)</th>
														<th style="text-align: center;" colspan="7">UPP Kontes (MGI)</th>
													</tr>
													<tr>
														<th style="text-align: center;">1</th>
														<th style="text-align: center;">3</th>
														<th style="text-align: center;">6</th>
														<th style="text-align: center;">9</th>
														<th style="text-align: center;">12</th>
														<th style="text-align: center;">24</th>
														<th style="text-align: center;">36</th>
														<th style="text-align: center;">1</th>
														<th style="text-align: center;">3</th>
														<th style="text-align: center;">6</th>
														<th style="text-align: center;">9</th>
														<th style="text-align: center;">12</th>
														<th style="text-align: center;">24</th>
														<th style="text-align: center;">36</th>
													</tr>
												</thead>
												<tbody>
													<c:set var="produk" value="" />
											         <c:forEach var="k" items="${daftarKomisi}" varStatus="st">
														<tr>
															<c:if test="${produk ne k.LSDBS_NAME}">
																<td nowrap="nowrap" rowspan="${ukuran}">[${k.LSBS_ID}-${k.LSDBS_NUMBER}] ${k.LSDBS_NAME}</td>
																<c:set var="produk" value="${k.LSDBS_NAME}" />
															</c:if>
															<td nowrap="nowrap">${k.LKU_SYMBOL}</td>
															<td nowrap="nowrap">${k.PREMI}</td>
															<td nowrap="nowrap" style="text-align: center;">${k.LSCO_DATE}</td>
															<td nowrap="nowrap" style="text-align: center;">${k.LSCB_PAY_MODE}</td>
															<td nowrap="nowrap"><fmt:formatNumber value="${k.LSCO_COMM}" />%</td>
															<td nowrap="nowrap"><fmt:formatNumber value="${k.LSCO_BONUS}" />%</td>
															<td nowrap="nowrap"><fmt:formatNumber value="${k.UPP_EVA_1}" />%</td>
															<td nowrap="nowrap"><fmt:formatNumber value="${k.UPP_EVA_3}" />%</td>
															<td nowrap="nowrap"><fmt:formatNumber value="${k.UPP_EVA_6}" />%</td>
															<td nowrap="nowrap"><fmt:formatNumber value="${k.UPP_EVA_9}" />%</td>
															<td nowrap="nowrap"><fmt:formatNumber value="${k.UPP_EVA_12}" />%</td>
															<td nowrap="nowrap"><fmt:formatNumber value="${k.UPP_EVA_24}" />%</td>
															<td nowrap="nowrap"><fmt:formatNumber value="${k.UPP_EVA_36}" />%</td>
															<td nowrap="nowrap"><fmt:formatNumber value="${k.UPP_KONTES_1}" />%</td>
															<td nowrap="nowrap"><fmt:formatNumber value="${k.UPP_KONTES_3}" />%</td>
															<td nowrap="nowrap"><fmt:formatNumber value="${k.UPP_KONTES_6}" />%</td>
															<td nowrap="nowrap"><fmt:formatNumber value="${k.UPP_KONTES_9}" />%</td>
															<td nowrap="nowrap"><fmt:formatNumber value="${k.UPP_KONTES_12}" />%</td>
															<td nowrap="nowrap"><fmt:formatNumber value="${k.UPP_KONTES_24}" />%</td>
															<td nowrap="nowrap"><fmt:formatNumber value="${k.UPP_KONTES_36}" />%</td>
														</tr>
											         </c:forEach>
										         </tbody>
											</table>
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
						</c:if>
					</table>
				</div>
			</div>
		</div>
	</form>
</div>
</body>
<%@ include file="/include/page/footer.jsp"%>