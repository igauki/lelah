<%@ include file="/include/page/header.jsp"%>

<script type="text/javascript">
	function add(){
		formpost.flag.value = "1";
		formpost.proses.value = "1";
		formpost.submit();
	}
	
	function ubah(i){
		formpost.flag.value = "2"; 
		formpost.proses.value = "2";
		formpost.brs.value = i;
		formpost.item.value = "1";
		produk = formpost.produk.value;
		bts = produk.indexOf("~");
		formpost.lsbs_id[i].value = produk.substring(0,bts);
		formpost.lsdbs_number[i].value = produk.substring(bts+2,produk.length);
		formpost.submit();
	}
		
	function ekstra(e,i,v){
		var unicode=e.charCode? e.charCode : e.keyCode;
		if(formpost.produk == null){
			formpost.brs.value = i;
			return true;						
		}else{
			if(formpost.produk.value=='' || formpost.produk.value==null){
				alert("Silahkan pilih Produk terlebih dahulu!");
				return false;
			}
			formpost.brs.value = i;
			produk = formpost.produk.value;
			bts = produk.indexOf("~");
			formpost.lsbs_id[i].value = produk.substring(0,bts);
			formpost.lsdbs_number[i].value = produk.substring(bts+2,produk.length);		
			return true;		
		}
	}	
	
	function cekEm() {
		if(formpost.brs.value == ''){
			alert("Silahkan input/edit EM pada baris yg diinginkan!");
		}else{
			var id = "mspr_extra"+formpost.brs.value;
			var v = document.getElementById(id).value;
			proses("2",formpost.brs.value,"2");
		}
	}
		
	function proses(flag,brs,item){
		formpost.flag.value = flag; 
		formpost.brs.value = brs;
		formpost.item.value = item;
		formpost.submit();
	}
		
	function bunga(i){
		if(formpost.produk!=null){
			if(formpost.produk.value == '' || formpost.produk.value == null){
				alert("Silahkan pilih Produk terlebih dahulu!");
			}else{
				proses("2",i,"3");		
			}
		}else{
			proses("2",i,"3");		
		}
	}
		
	function simpan(){
		if(formpost.produk!=null){
			if(formpost.produk.value==''){
				alert("Produk tidak boleh Kosong");
			}else{
				formpost.flag.value="3"; 
				formpost.submit();
			}
		}else{
			formpost.flag.value="3"; 
			formpost.submit();
		}	
	}
	
	function hapus(){
		formpost.flag.value="4";
		formpost.submit();
	}
	
	function awal(){
		var flag = formpost.flag_tombol.value;
		var lspdid = document.getElementById('lspdId').value;
		
		if(flag == 1 || flag == 2){
			document.getElementById('btn_add').disabled = true;
			document.getElementById('btn_hitung').disabled = false;
			document.getElementById('btn_save').disabled = false;
			document.getElementById('btn_delete').disabled = true;
		}else{
			if (lspdid == 2){
			document.getElementById('btn_add').disabled = false;
			document.getElementById('btn_hitung').disabled = true;
			document.getElementById('btn_save').disabled = true;
			document.getElementById('btn_delete').disabled = false;
			}else {
			document.getElementById('btn_add').disabled = true;
			document.getElementById('btn_hitung').disabled = true;
			document.getElementById('btn_save').disabled = true;
			document.getElementById('btn_delete').disabled = true;}
		}
	}
	
</script>

<style type="text/css">
	
</style>

<body onLoad="awal();" onresize="setFrameSize('infoFrame', 45);">
	<form name="formpost" method="post">
		<div class="tabcontent">
			<table class="entry" style="width:98%;">
		 		<span class="subtitle">Premium Calculation</span> 
		   
				<tr>
					<th></th>
					<th>Product</th>
					<th>Sum Insured</th>
					<th>EM</th>
					<th>Satuan</th>
					<th>Rate</th>
					<th>Premium</th>
					<th>Discount</th>
					<th>Total</th>
					<th>%</th>
					<th>Ins. Period</th>
					<th>End Date</th>
				</tr>
				<c:forEach var="s" items="${cmd.lsProduk}" varStatus="xt">
					<tr>
						<td>
							<c:if test="${(s.tambah eq 1) or (s.mspr_extra gt 0)}">
								<input type="checkbox" class="noborder" name="check" id="check" value="${xt.index}">
							</c:if>
						</td>
						<td width="50">
							<c:choose >
								<c:when test="${xt.index >= 0 && s.tambah eq 0}">
									<input type="text" name="prod" value="${s.lsdbs_name}" size="30">
								</c:when>	
								<c:when test="${s.tambah eq 1}">
									<select name="produk" onChange="ubah(${xt.index });">
										<option />
										<c:forEach var="x" items="${lsExtraPremi}">
											<option value="${x.BISNIS_ID}"
												<c:if test="${s.lsbs_id eq x.LSBS_ID }">SELECTED</c:if>>
												${x.LSDBS_NAME}
											</option>
										</c:forEach>
									</select>
								</c:when>
							</c:choose>
							<spring:bind path="cmd.lsProduk[${xt.index }].lsbs_id">
								<input type="hidden" name="lsbs_id" id="lsbs_id" value="${status.value }">
							</spring:bind>	
							<spring:bind path="cmd.lsProduk[${xt.index }].lsdbs_number">
								<input type="hidden" name="lsdbs_number" id="lsdbs_number" value="${status.value}">
							</spring:bind>	
						</td>
						<td>
							<spring:bind path="cmd.lsProduk[${xt.index}].mspr_tsi">
								<input type="text" readonly name="${status.expression }" value="${status.value}" >
							</spring:bind>	
						</td>
						<td>
							<spring:bind path="cmd.lsProduk[${xt.index }].mspr_extra">
								<input type="text" name="${status.expression }" id="mspr_extra${xt.index}" maxlength="5" size="5" onkeypress="return ekstra(event,${xt.index},this.value);"
								<spring:bind path="cmd.lsProduk[${xt.index }].tambah">	
									<c:if test="${xt.index eq 0 }"> readonly</c:if>
								</spring:bind>
								value="${status.value}" >
							</spring:bind>	
						</td>
						<td>
							<select name="selectSatuan" <c:if test="${xt.index >=0 && s.tambah eq 0}"> disabled = "disabled"</c:if> >
								<option <c:if test="${s.mspr_flag_jenis eq 0}">SELECTED</c:if> value="0"> </option> 
								<option <c:if test="${s.mspr_flag_jenis eq 1}">SELECTED</c:if> value="1">Permil</option> 
			           	   		<option <c:if test="${s.mspr_flag_jenis eq 2}">SELECTED</c:if> value="2">Persen</option>
							</select>
							<spring:bind path="cmd.lsProduk[${xt.index }].mspr_flag_jenis">
								<input type="hidden" name="mspr_flag_jenis" id="mspr_flag_jenis" value="${status.value }">
							</spring:bind>
						</td>
						<td>
							<spring:bind path="cmd.lsProduk[${xt.index }].mspr_rate">
								<input type="text"  name="${status.expression}" maxlength="5" size="5" onChange="bunga(${xt.index });"
									<spring:bind path="cmd.lsProduk[${xt.index }].tambah">
										<c:if test="${xt.index eq 0 }"> readonly</c:if>
									</spring:bind>
									value="${status.value}" >
							</spring:bind>	
						</td>
						<td>
							<spring:bind path="cmd.lsProduk[${xt.index }].mspr_premium">
								<input type="text"  name="${status.expression }" value="${status.value}" 
									<spring:bind path="cmd.lsProduk[${xt.index }].tambah">
										<c:if test="${xt.index eq 0 }"> readonly</c:if>
									</spring:bind>
									>
							</spring:bind>	
						</td>
						<td>
							<spring:bind path="cmd.lsProduk[${xt.index }].mspr_discount">
								<input type="text" readonly name="${status.expression }"  size="5" value="${status.value}" >
							</spring:bind>
						</td>
						<td>
							<spring:bind path="cmd.lsProduk[${xt.index }].total">
								<input type="text" readonly name="${status.expression }" value="${status.value}" >
							</spring:bind>	
						</td>
						<td>
							<spring:bind path="cmd.lsProduk[${xt.index }].mspr_persen">
								<input type="text" readonly name="${status.expression }" size="3" value="${status.value}" >
							</spring:bind>	
						</td>
						<td>
							<spring:bind path="cmd.lsProduk[${xt.index }].mspr_ins_period">
								<input size="2" type="text" readonly name="${status.expression }" value="${status.value}" >
								<c:if test="${status.value > 0 }"> 
									<input size="1" type="text" readonly name="year" value="Y" >
								</c:if>
							</spring:bind>	
						</td>
						<td>
							<spring:bind path="cmd.lsProduk[${xt.index }].mspr_end_date">
								<input type="text" readonly name="${status.expression }" value="${status.value}" >
							</spring:bind>	
						</td>
					</tr>
				</c:forEach>
				<tr>
					<td></td>
					<td colspan="4"></td>
					<th>Total</th>
					<td>
						<spring:bind path="cmd.totalPremi">
							<input Readonly type="text" name="${status.expression }" value="${status.value}">
						</spring:bind>
					</td>
					<td></td>
					<td>
						<spring:bind path="cmd.totalTot">
							<input Readonly type="text" name="${status.expression }" value="${status.value}">
						</spring:bind>
					</td>	
				</tr>
			</table>
			<table class="entry" style="width:40%;">
				<tr>
					<th>Keterangan Extra
                     <textarea rows="5" cols="50" style="width: 100%; text-transform: uppercase;" name="keterangan"></textarea></font></th>
				</tr>
			</table>
			<table width="100%">
				<tr>
					<td></td>
					<td align="center">
						<input type="hidden" name="flag" >
						<spring:bind path="cmd.proses">
							<input type="hidden" name="${status.expression}" value="${status.value }">
						</spring:bind>
						<spring:bind path="cmd.lspdId">
							<input type="hidden" name="${status.expression}" value="${status.value }">
						</spring:bind>
						<input type="hidden" name="brs">
						<input type="hidden" name="item">
						<input type="hidden" name="flag_tombol" value="${cmd.tombol}">
						<input type="button" name="btn_add" id="btn_add" value="Add" onClick="add();">
						<input type="button" name="btn_hitung" id="btn_hitung" value="Hitung Premi" onClick="cekEm();">
						<input type="button" name="btn_save" id="btn_save" value="Save" onClick="simpan();">
						<input type="button" name="btn_delete" id="btn_delete" value="Delete" onClick="hapus();">
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<c:if test="${not empty submitSuccess }">
						<script>
							alert("Berhasil");
							window.location="${path }/uw/premi.htm?spaj=${cmd.spaj}";
						</script>
				        	<div id="success">
					        	Berhasil
				        	</div>
				        </c:if>	
			  			<spring:bind path="cmd.*">
							<c:if test="${not empty status.errorMessages}">
								<div id="error">Informasi:
									<br>
										<c:forEach var="error" items="${status.errorMessages}">
											- <c:out value="${error}" escapeXml="false" />
									<br/>
										</c:forEach>
								</div>
							</c:if>
						</spring:bind>
					</td>
				</tr>
			</table>
		</div>
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>