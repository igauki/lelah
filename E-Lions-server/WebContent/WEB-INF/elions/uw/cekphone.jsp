<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	    $().ready(function() {
		$("#tabs").tabs();
		$("#load").hide();
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol	 
		
		
		$("#btnSearch").click(function() {
			var hp = $("#hp").val();
			var seri = $("#seri").val();
			var namapp = $("#namapp").val();
			if(hp ==null || hp==""){
			   	alert("Masukan no hp Terlebih Dahulu!");			   
				return false;
			}else if(seri ==null || seri==""){
				alert("Masukan no seri Terlebih Dahulu!");			   
				return false;
			}else if(namapp ==null || namapp==""){
				alert("Masukan nama pemegang Terlebih Dahulu!");			   
				return false;					
			}else{
			    $("#btnSearch").val("Loading....Please Wait...!");
				$("#btnSearch").hide();
				$("#main").fadeOut();
				$("#load").fadeIn();
				return true;
			};
		
		});
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			};
		};
	});	
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }
</style>

<body>

		<div id="load">   
			<img  src="${path}/include/image/loading.gif" alt="loading" />
	        <br /><br />
			<h3>Loading Please Wait....</h3>       
	  </div>
	 <div id="main">   
	 <form method="post" name="formpost" enctype="multipart/form-data">
							<fieldset class="ui-widget ui-widget-content">	
								<legend class="ui-widget-header ui-corner-all"><div><h5>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; Menu Pengecekkan No Hp</h5></div></legend>
									
									<table class = "some" >	
									<tr>
									  <th>  Masukan No Seri SPAJ </th>
									  <td> :</td>
									  <td>
									     <input type="text" name="seri" id="seri" size="50" <c:if test="${seri ne ''}">value="${seri}" </c:if>>					 					
									  </td>									  
									</tr>
									<tr>
									  <th>  Masukkan Nama Pemegang Polis </th>
									  <td> :</td>
									  <td>
									     <input type="text" name="namapp" id="namapp" size="50" <c:if test="${namapp ne ''}">value="${namapp}" </c:if>>					 					
									  </td>									  
									</tr>
									<tr>
									  <th>  Masukkan Tanggal Lahir Pemegang Polis</th>
									  <td> :</td>
									  <td>
									    <input name="bdate" id="bdate" type="text" class="datepicker" title="Tanggal Lahir" value="${bdate}">			 					
									  </td>									  
									</tr>													
									<tr>
									  <th>  Masukan No Hp </th>
									  <td> :</td>
									  <td>
									     <input type="text" name="hp" id="hp" size=50 <c:if test="${hp ne ''}">value="${hp}" </c:if>>	
									      			 					
									  </td>									  
									</tr>				
									
									</table>
									<div class="rowElem"> 
										<center><input type="submit" value="Search" name="btnSearch" id="btnSearch"></center>	
									</div>	 	
									<div class="rowElem"> 
									 <c:if test="${pesan eq 'YES'}">
									   <font color="blue">
									   		NO HP NASABAH BELUM PERNAH DI GUNAKAN OLEH NASABAH LAIN ATAU AGEN, 
     										SILAHKAN LANJUTKAN INPUT SPAJ								   		
									   
										</font>
									 
									 </c:if>
									 <c:if test="${pesan eq 'NO'}">
									   <font color="red">
									   		<ul>									   				
									   			<li>1. No Hp Sudah digunakan oleh <c:if test="${flag eq'0'}">Nasabah</c:if> <c:if test="${flag eq '1'}">Agen</c:if></li>
									   			<li>2. Harap Follow Up ke Agen Penutup untuk perubahan No Telepon yang benar dari Nasabah</li>
									   			<li>3. Hubungi PIC BAS(Yudi, Iwan, Dina atau Desy) untuk Informasi lebih lanjut!</li>
									   		</ul>						   		
									    <p>Catatan : SPAJ tidak boleh diinput sebelum nomor Telepon dirubah yang benar!!!</p>
										</font>
									 
									 </c:if>
									 <c:if test="${pesan eq 'EXIST'}">
									   <font color="red">
									   		<ul>									   				
									   			<li>1. No Hp Sudah digunakan oleh <c:if test="${flag eq'0'}">Nasabah</c:if> <c:if test="${flag eq '1'}">Agen</c:if></li>
									   			<li>2. Harap Follow Up ke Agen Penutup untuk perubahan No Telepon yang benar dari Nasabah</li>
									   			<li>3. Hubungi PIC BAS(Yudi, Iwan, Dina atau Desy) untuk Informasi lebih lanjut!</li>
									   		</ul>						   		
									    <p>Catatan : SPAJ tidak boleh diinput sebelum nomor Telepon dirubah yang benar!!!(No hp ini sudah pernah dicek)</p>
										</font>
									 
									 </c:if>
									  <c:if test="${pesan eq 'GAGAL'}">
									   <font color="red">
									   		<p>Gagal Cek No Hp! Silahkan Hubungi Administrator</p>
										</font>
									 
									 </c:if>
									</div>																						
							</fieldset>
						</form>
			</div>			
</body>