<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<!-- DatePicker Script (jscalendar) -->
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	function cariData(){
		if(trim(document.formpost.kata.value)=='' && trim(document.formpost.tgl_lahir.value)=='') return false;
		else createLoadingMessage();	
		//createLoadingMessage();	
	}
	
	function backToParent1(msp_id)
	{
			parentForm = self.opener.document.forms['frmParam'];
			if(parentForm) {
				if(parentForm.elements['msp_id']){
					parentForm.elements['msp_id'].value = msp_id; 
					parentForm.elements['edit'].click();
					window.close();
				}
			}
			backToParent(msp_id);
	}
	function backToParent(msp_id){
		parentForm = self.opener.document.forms['formpost'];
		if(parentForm.elements['msp_id']){
			parentForm.elements['msp_id'].value = msp_id; 
			
			if(parentForm.elements['flagRefresh']){
				parentForm.elements['flagRefresh'].value = 'refresh'; 
			}
			if(parentForm.elements['edit']){
				parentForm.elements['edit'].click();
			}
			
			window.close();
		}
	}

</script>
</head>
<BODY onload="resizeCenter(650,400); document.title='PopUp :: Cari Data PAS'; setupPanes('container1', 'tab1'); document.formpost.kata.select(); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">
				Cari Data Pas
				</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" action="${path }/uw/pasgutri.htm" style="text-align: center;">
					<input type="hidden" name="win" value="${param.win}">
					<input type="hidden" name="cari" value="${param.cari}">
					<input type="hidden" name="posisi" value="${param.posisi}">
					<table class="entry2">
						<tr>
							<th rowspan="2">Cari:</th>
							<th class="left">
								<select name="tipe">
									<option value="1" <c:if test="${param.tipe eq \"1\" }">selected</c:if>>Nama</option>
									<option value="2" <c:if test="${param.tipe eq \"2\" }">selected</c:if>>No. Identitas</option>
									<option value="3" <c:if test="${param.tipe eq \"3\" }">selected</c:if>>Fire Id</option>
								</select>
								<select name="pilter">
									<option <c:if test="${\"LIKE%\" eq param.pilter}">selected</c:if> value="LIKE%">LIKE%</option>
									<option <c:if test="${\"%LIKE\" eq param.pilter}">selected</c:if> value="%LIKE">%LIKE</option>
									<option <c:if test="${\"%LIKE%\" eq param.pilter}">selected</c:if> value="%LIKE%">%LIKE%</option>
									<option <c:if test="${\"LT\" eq param.pilter}">selected</c:if> value="LT"><</option>
									<option <c:if test="${\"LE\" eq param.pilter}">selected</c:if> value="LE"><=</option>
									<option <c:if test="${\"EQ\" eq param.pilter}">selected</c:if> value="EQ">=</option>
									<option <c:if test="${\"GE\" eq param.pilter}">selected</c:if> value="GE">>=</option>
									<option <c:if test="${\"GT\" eq param.pilter}">selected</c:if> value="GT">></option>
								</select>					
								<input type="text" name="kata" size="34" value="${param.kata }" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
								<input type="submit" name="search" value="Search" onclick="return cariData();"
									accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
							</th>
						</tr>
						<tr>
							<th class="left">
								<label for="lahir">
									<input class="noBorder" type="checkbox" name="centang" id="lahir" value="1">
									Tanggal Lahir (untuk pencarian berdasarkan Pemegang/Tertanggung) :
								</label>
								<script>inputDate('tgl_lahir', '', false);</script>
							</th>
						</tr>
					</table>
					<table class="simple">
						<thead>
							<tr>
								<th style="text-align: left">Nama Tertanggung</th>
								<th style="text-align: left">No. Identitas</th>
								<th style="text-align: left">Tgl.Lahir</th>
								<th style="text-align: left">Alamat</th>
								<th style="text-align: left">Kota</th>
								<th style="text-align: left">Kode Pos</th>
								<th style="text-align: left">Fire Id</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="pas" items="${cmd.listPas}" varStatus="stat">
								<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
									onclick="backToParent1('${pas.msp_id}');">
										<td>${pas.msp_full_name}</td>
										<td>${pas.msp_identity_no_tt}</td>
										<td>${pas.msp_date_of_birth2}</td>
										<td>${pas.msp_address_1}</td>
										<td>${pas.msp_city}</td>
										<td>${pas.msp_postal_code}</td>
										<td>${pas.msp_fire_id}</td>
								</tr>
								<c:set var="jml" value="${stat.count}"/>
								<c:if test="${stat.count eq 1}">
									<c:set var="v1" value="${pas.msp_id}"/>
								</c:if>
							</c:forEach>
						</tbody>
					</table>
					<br>
					<input type="hidden" name="flag" value="${cmd.flag }" >
					<input type="button" name="close" value="Close" onclick="window.close();"
						accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">

				</form>
			</div>
		</div>
	</div>

<c:if test="${jml eq 1}">
<script>backToParent1('${v1}', '${v2}');</script>
</c:if>

</body>
<%@ include file="/include/page/footer.jsp"%>