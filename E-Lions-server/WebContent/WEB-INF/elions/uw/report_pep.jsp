<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<script type="text/javascript">
	hideLoadingMessage();
	function simpan(){
		formpost.proses.value=1;
		formpost.submit();
	}
	
	function cari(){
		indicator.style.display='';
		formpost.proses.value=0;
		formpost.submit();
	}
	function laporan(){
		var tgl1=formpost.tglAwal.value;
		var tgl2=formpost.tglAkhir.value;
		window.location="${path}/report/uw.htm?window=report_pep&tanggalAwal="+tgl1+"&tanggalAkhir="+tgl2;
	}
	function awal(){
		document.title='PopUp :: Daily Monitoring PEP';
		if('${cmd.sub}'==1){
			cari();
		}	
		
	}
</script>
</head>

<BODY onload="awal();" style="height: 100%;">

<form:form id="formpost" name="formpost" commandName="cmd">
	<fieldset>
		<legend>Daily Monitoring PEP</legend>
		<table class="entry2">
			<tr>
				<td>
					Tanggal Terima SPAJ : dari <script>inputDate('tglAwal', '${cmd.tglAwal}', false, '', 9);</script> s/d <script>inputDate('tglAkhir', '${cmd.tglAkhir}', false, '', 9);</script>
						<span id="indicator" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
						<input type="button" value="Report" onClick="laporan();"
							accesskey="L" onmouseover="return overlib('Alt-L', AUTOSTATUS, WRAP);" onmouseout="nd();">
			
				</td>
			</tr>
		</table>	
	</fieldset>                                                   		
			<table class="entry2">
			<tr>
			 <td>
		  			<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
								Info:<br>
									<c:forEach var="error" items="${status.errorMessages}">
												- <c:out value="${error}" escapeXml="false" />
										<br/>
									</c:forEach>
							</div>
						</c:if>									
					</spring:bind>
				</td>
			</tr>
			</table>
</form:form>
</body>
<%@ include file="/include/page/footer.jsp"%>
