<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<!-- DatePicker Script (jscalendar) -->
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	function onLoad(){
		if(trim(document.formpost.result.value)!=''){
			alert(document.formpost.result.value);
		}
	}
	
	function cariData(){
		var pilter = document.getElementById('pilter').value;
		var tipe = document.getElementById('tipe').value;
		var kata = document.getElementById('kata').value;
		
		document.getElementById('refreshPage').href = 'akseptasi_pas.htm?tipe='+tipe+'&kata='+kata+'&pilter='+pilter;
		document.getElementById('refreshPage').click();
	
	}
	
	function update(msp_id, action){
		document.getElementById('msp_id').value = msp_id;
		document.getElementById('action').value = action;
		if(trim(document.getElementById('action').value)=='') return false;
		else createLoadingMessage();	
	}
	
	function aksep(nama, msp_id, status){
		if(status == '1'){
			if(confirm('Ada status yang belum terselesaikan atas fire id '+nama+'. Lanjut AKSEP PAS?')){
					update(msp_id, 'aksep');
					document.getElementById('save').click();
				}
		}else{
			if(confirm('Apakah anda yakin untuk AKSEP PAS atas fire id '+nama+'?')){
					update(msp_id, 'aksep');
					document.getElementById('save').click();
				}
		}
	}
	
	function transfer_process(nama, msp_id){
		if(confirm('Apakah anda yakin untuk TRANSFER PAS atas fire id '+nama+'?')){
					update(msp_id, 'transfer');
					document.getElementById('transfer').click();
				}
	}
	
	function ubah_agen(nama, msp_id){
		if(confirm('Apakah anda yakin untuk UBAH AGEN PAS atas fire id '+nama+'?')){
					update(msp_id, 'leader');
					document.getElementById('leader').click();
				}
	}
	

</script>
</head>
<BODY onload="resizeCenter(650,400);  setupPanes('container1', 'tab1'); document.formpost.kata.select(); onLoad();" style="height: 100%;">
	<div class="tab-container" id="container1" style="position: absolute;">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">
				PAS
				</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" action="#" style="text-align: center;">
					<table class="simple">
						<thead>
							<tr>
								<th style="text-align: left" width="200px">Action</th>
								<th style="text-align: left">Nama Tertanggung</th>
								<th style="text-align: left">Tempat</th>
								<th style="text-align: left">Tgl.Lahir</th>
								<th style="text-align: left">No. Identitas TT</th>
								<th style="text-align: left">No. Kartu</th>
								<th style="text-align: left">PIN</th>
								<th style="text-align: left">No. HP</th>
								<th style="text-align: left" width="200px">Alamat</th>
								<th style="text-align: left">Kota</th>
								<th style="text-align: left">Kode Pos</th>
								<th style="text-align: left">E-Mail</th>
								<th style="text-align: left">Fire Id</th>
								
							</tr>
						</thead>
						<tbody>
							<c:forEach var="pas" items="${cmd}" varStatus="stat">
								<tr>
										<td>
											<c:choose>
										        	
														<c:when test="${pas.lspd_id == 2 && pas.lssp_id == 10 && pas.msp_flag_aksep == 1}">
															<input type="button" name="save_btn" value="Aksep" disabled="disabled"/>
															<c:choose>
																<c:when test="${pas.dirAgenBp == 0}">
																	<input type="button" name="transfer_btn" value="Transfer" onclick="transfer_process('${pas.msp_fire_id}','${pas.msp_id}');"
																		onmouseout="nd();" title="dokumen tidak ditemukan"/>
																</c:when>
																<c:otherwise>
																	<input type="button" name="transfer_btn" value="Transfer" onclick="transfer_process('${pas.msp_fire_id}','${pas.msp_id}');"
																		onmouseout="nd();"/>
																</c:otherwise>
															</c:choose>
														</c:when>
														<c:when test="${pas.lspd_id == 2 && pas.lssp_id == 10}">
															<c:choose>
																<c:when test="${not empty pas.lrb_id}">
																	<input type="button" value="STATUS" name="inputStatus"
																	onclick="popWin('${path}/uw/status_pas_mall.htm?msp_id='+${pas.msp_id}, 200, 800); "
																	onmouseover="return overlib('Input Status PAS', AUTOSTATUS, WRAP);" onmouseout="nd();" />
																</c:when>
																<c:when test="${pas.jenis_pas == 'INDIVIDU'}">
																	<input type="button" value="STATUS" name="inputStatus"
																	onclick="popWin('${path}/uw/status_pas.htm?msp_id='+${pas.msp_id}, 200, 800); "
																	onmouseover="return overlib('Input Status PAS', AUTOSTATUS, WRAP);" onmouseout="nd();" />
																</c:when>
																<c:when test="${pas.jenis_pas == 'PAS BPD'}">
																	<input type="button" value="STATUS BPD" name="inputStatusBpd" disabled="disabled"
																	onclick="popWin('${path}/uw/status_pas_partner.htm?msp_id='+${pas.msp_id}, 200, 800); "
																	onmouseover="return overlib('Input Status PAS BPD', AUTOSTATUS, WRAP);" onmouseout="nd();" />
																</c:when>	
																<c:when test="${pas.jenis_pas == 'AP/BP'}">
																	<input type="button" value="STATUS BP" name="inputStatusBp"
																	onclick="popWin('${path}/uw/status_pas_partner.htm?msp_id='+${pas.msp_id}, 200, 800); "
																	onmouseover="return overlib('Input Status PAS Partner', AUTOSTATUS, WRAP);" onmouseout="nd();" />
																</c:when>															
															</c:choose>
															<c:choose>
																<%-- <c:when test="${not empty pas.lrb_id}"> Lepas filter buat pas fire MALL - Daru 06 Dec 2013 --%>
																<c:when test="${pas.jenis_pas == 'MALLINSURANCE'}">
																	<input type="button" value="VIEW" name="inputDetail"
																	onclick="popWin('${path}/uw/input_pas_mall_detail.htm?posisi=1&win=edit&msp_id='+${pas.msp_id}, 350, 450); "
																	onmouseover="return overlib('Input PAS MALL Detail', AUTOSTATUS, WRAP);" onmouseout="nd();" />
																</c:when>
																<c:when test="${pas.jenis_pas == 'INDIVIDU'}">
																	<input type="button" value="VIEW" name="inputDetail"
																	onclick="popWin('${path}/uw/input_pas_detail.htm?posisi=1&win=edit&msp_id='+${pas.msp_id}, 350, 450); "
																	onmouseover="return overlib('Input PAS Detail', AUTOSTATUS, WRAP);" onmouseout="nd();" />
																</c:when>
																<c:when test="${pas.jenis_pas == 'PAS BPD'}">
																	<input type="button" value="VIEW BPD" name="inputDetailPartner"
																	onclick="popWin('${path}/uw/input_pas_detail_partner.htm?posisi=1&win=edit&msp_id='+${pas.msp_id}, 350, 450); "
																	onmouseover="return overlib('Input PAS Detail BPD', AUTOSTATUS, WRAP);" onmouseout="nd();" />
																</c:when>	
																<c:when test="${pas.jenis_pas == 'AP/BP'}">
																	<input type="button" value="VIEW" name="inputDetailPartner"
																	onclick="popWin('${path}/uw/input_pas_detail_partner.htm?posisi=1&win=edit&msp_id='+${pas.msp_id}, 350, 450); "
																	onmouseover="return overlib('Input PAS Detail Partner', AUTOSTATUS, WRAP);" onmouseout="nd();" />
																	<input type="button" value="PDF" name="detailPdfPartner"
																	onclick="parent.openDocPdf('${pas.msag_id_pp}');"
																	onmouseover="return overlib('PAS PDF Detail Partner', AUTOSTATUS, WRAP);" onmouseout="nd();" />
																</c:when>
																<c:when test="${pas.jenis_pas == 'AP/BP ONLINE'}">
																	<input type="button" value="VIEW" name="inputDetailPartner"
																	onclick="popWin('${path}/uw/input_bp_online_detail.htm?posisi=1&win=edit&msp_id='+${pas.msp_id}, 350, 450); "
																	onmouseover="return overlib('Input PAS Detail Partner', AUTOSTATUS, WRAP);" onmouseout="nd();" />
																	<input type="button" value="PDF" name="detailPdfPartner"
																	onclick="parent.openDocPdf('${pas.msag_id_pp}');"
																	onmouseover="return overlib('PAS PDF Detail Partner', AUTOSTATUS, WRAP);" onmouseout="nd();" />
																</c:when>																		
															</c:choose>
															<input type="button" name="save_btn" value="Aksep"  onclick="aksep('${pas.msp_fire_id}','${pas.msp_id}','${pas.msp_status}');"
																onmouseout="nd();"/>
															<input type="button" name="transfer_btn" value="Transfer" disabled="disabled"/>
														</c:when>
														<c:otherwise>
															<input type="button" value="STATUS" name="inputStatus" disabled="disabled" />
															<input type="button" value="VIEW" name="inputDetail" disabled="disabled" />
															<input type="button" name="save_btn" value="Aksep" disabled="disabled"/>
															<input type="button" name="transfer_btn" value="Transfer" disabled="disabled"/>
														</c:otherwise>
											</c:choose>
										</td>
										<td>${pas.msp_full_name}</td>
										<td>${pas.msp_pas_tmp_lhr_tt}</td>
										<td>${pas.msp_date_of_birth2}</td>
										<td>${pas.msp_identity_no_tt}</td>
										<td>${pas.no_kartu}</td>
										<td>${pas.pin}</td>
										<td>${pas.msp_mobile}</td>
										<td>${pas.msp_address_1}</td>
										<td>${pas.msp_city}</td>
										<td>${pas.msp_postal_code}</td>
										<td>${pas.msp_pas_email}</td>
										<td>${pas.msp_fire_id}</td>
										
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<br>
					<input type="hidden" name="win" value="${param.win}">
					<input type="hidden" name="cari" value="${param.cari}">
					<input type="hidden" name="posisi" value="${param.posisi}">
					<input type="hidden" name="result" id="result" value="${result}" />
					<a href="akseptasi_pas.htm" id="refreshPage"></a>
					<input type="submit" name="transfer" id="transfer" value="Transfer" style="visibility: hidden;" onmouseout="nd();"/>
					<input type="submit" name="leader" id="leader" value="Leader" style="visibility: hidden;" onmouseout="nd();"/>
					<input type="submit" name="save" id="save" value="Aksep" style="visibility: hidden;" onmouseout="nd();"/>
					<input type="hidden" name="action" value="" />
					<input type="hidden" name="msp_id" id="msp_id" value="" />
					<div style="visibility: hidden;">
					<table class="entry2">
						<tr>
							<th rowspan="1">Cari:</th>
							<th class="left">
								<select name="tipe" id="tipe">
									<option value="1" <c:if test="${param.tipe eq \"1\" }">selected</c:if>>Nama</option>
									<option value="2" <c:if test="${param.tipe eq \"2\" }">selected</c:if>>No.Bukti Identitas</option>
									<option value="7" <c:if test="${param.tipe eq \"7\" }">selected</c:if>>No.HP</option>
									<option value="3" <c:if test="${param.tipe eq \"3\" }">selected</c:if>>Fire Id</option>
									<option value="4" <c:if test="${param.tipe eq \"4\" }">selected</c:if>>No.Kartu</option>
								</select>
								<select name="pilter" id="pilter">
									<option <c:if test="${\"LIKE\" eq param.pilter}">selected</c:if> value="LIKE">LIKE</option>
									<option <c:if test="${\"LT\" eq param.pilter}">selected</c:if> value="LT"><</option>
									<option <c:if test="${\"LE\" eq param.pilter}">selected</c:if> value="LE"><=</option>
									<option <c:if test="${\"EQ\" eq param.pilter}">selected</c:if> value="EQ">=</option>
									<option <c:if test="${\"GE\" eq param.pilter}">selected</c:if> value="GE">>=</option>
									<option <c:if test="${\"GT\" eq param.pilter}">selected</c:if> value="GT">></option>
								</select>					
								<input type="text" name="kata" id="kata" size="34" value="${param.kata }" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
								<input type="button" name="search" id="search" value="Search" onclick="cariData();" onmouseout="nd();">
								<input type="button" name="refreshButton" onclick="document.getElementById('search').click();" style="visibility: hidden;"/>
							</th>
						</tr>
					</table>
					</div>
				</form>
			</div>
		</div>
	</div>

<c:if test="${jml eq 1}">
<script>backToParent1('${v1}', '${v2}');</script>
</c:if>
<c:if  test="${not empty successMessage}">	
			<script type="text/javascript">													
				alert('${successMessage}');	
				if('${successMessage}' == "Agen tidak aktif"){
					ubah_agen('${nama}', '${msp_id}', '${agen_baru}');
				}else{
					document.getElementById('search').click();
				}
			</script>
	</c:if>
</body>
<%@ include file="/include/page/footer.jsp"%>