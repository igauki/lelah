<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript" src="${path}/include/js/jquery-1.3.2.min.js"></script>
<script>
$(document).ready(function() {
        <c:if test="${not empty pesanError}">
            alert('${pesanError}');
        </c:if>
    });
</script>
<body onload="document.title='PopUp :: Edit Tgl Lahir Tertanggung';setupPanes('container1','tab1');" style="height: 100%;">
 <c:choose>    
    <c:when test="${not empty hak}">
        <div id="error">
            Pemegang = Tertanggung, Gunakan Menu Edit Tgl Lahir Pemegang
        </div>
    </c:when>
    <c:otherwise>
         <div class="tab-container" id="container1">
              <ul class="tabs">
                <li>
                    <a href="#" onClick="return showPane('pane1', this)" id="tab1">Tgl Lahir Tertanggung</a>
                </li>
            </ul>
            <div class="tab-panes">
             <div id="pane1" class="panes">
                <form id="formpost" name="formpost" method="post">
                <fieldset>
                      <legend>Info User</legend>
                         <table class="entry2">
                   <tr>
                       <th nowrap="nowrap">User</th><th align ="left">&nbsp;[${infoDetailUser.LUS_ID}]&nbsp;&nbsp;${infoDetailUser.LUS_LOGIN_NAME}</th>
                  </tr>
                  <tr>
                       <th nowrap="nowrap">Nama<br></th><th align ="left">&nbsp;${infoDetailUser.LUS_FULL_NAME}&nbsp;[${infoDetailUser.LDE_DEPT}]</th>
                 </tr>
                    </table>
                         </fieldset>
                  
             <fieldset>
                <legend>Edit Tgl Lahir Tertanggung</legend>
                  <table class="entry2">
                <tr>   
                  <th colspan = "2" nowrap = "nowrap" align ="left">Tanggal Lahir : &nbsp;<script>inputDate('tanggal','<fmt:formatDate value="${tertanggung.mspe_date_birth}" pattern="dd/MM/yyyy"/>',true);</script>
                 &nbsp; Usia : <input disabled="disabled" type ="text" size = 5 name="mste_age" value="${tertanggung.mste_age}"/> Di :&nbsp;<input disabled="disabled" type ="text" size = 20 name="mspe_place_birth" value="${tertanggung.mspe_place_birth}"/></th>
                 </tr>
                  <tr>   
                     <th colspan ="1">D I U B A H &nbsp;&nbsp;M E N J A D I</th>
                </tr>
                <tr>   
                    <th colspan = "2" nowrap = "nowrap" align ="left">Tanggal Lahir : &nbsp;<script >inputDate('tanggalTanggung','<fmt:formatDate value="${tertanggung.mspe_date_birth}" pattern="dd/MM/yyyy"/>',false);</script>&nbsp;
                  Usia : <input disabled="disabled" type ="text" size = 5 name="mste_age" value="${tertanggung.mste_age}" /> Di :&nbsp;<input type ="text" size = 20 name="mspe_place_birth"/></th> 
                </tr>
              <tr>   
                     <th colspan ="2">&nbsp;</th>
                </tr>
                  <tr>
                 <th colspan = "2" nowrap = "nowrap" align ="left"> Keterangan(Alasan Melakukan Edit):</th> </tr>
                 <tr>
                 <th colspan = "2" nowrap = "nowrap" align ="left"><textarea rows="4" cols="20" style="width:70%;text-transform: uppercase;" name="alasan"/></textarea></th>
             </tr>
              <c:if test="${not empty editTglT}">
                 <tr align = "left">
                        <th colspan="2">    
                                 <input type="hidden" name="save" id="save">                        
                                <input type="submit" name="save" value="Save" onclick="return confirm('Apakah Anda Yakin Untuk Melanjutkan Proses ?');">
                                 <input type="button" name="close" value="Close" onclick="window.close();">
                            <%--   <c:if test="${not empty pesanError}"><br/>
                                  <div id="error">${pesanError}</div>
                              </c:if> --%>
                         </th>
                      </tr>
                   <tr>
                      <td><font color="#ff0000">* Proses ini akan masuk ke ulangan(History)</td>
                   </tr>
                   </c:if>
             </table>
           </fieldset>
          </form>
       </c:otherwise>
 </c:choose>
</body>
<%@ include file="/include/page/footer.jsp"%>