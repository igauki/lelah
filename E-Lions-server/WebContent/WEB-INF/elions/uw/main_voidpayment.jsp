<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>
<script>
	function tampil(flag){
		var noRef='';
		no=document.formpost.nomor.value;
		if(flag == '0'){
			document.getElementById('infoFrame').src='${path }/uw/voidpayment.htm?nomor='+no+'&flag='+flag;
		}
	}
	
	function buttonForm(){
		
		var pembayaran = document.getElementById('pembayaran').value;
		var nomor = document.getElementById('nomor').value;
		var jenis1 = document.getElementById('jenis1').checked;
		var jenis2 = document.getElementById('jenis2').checked;
		var jenis3 = document.getElementById('jenis3').checked;
		var account = document.getElementById('account').value;
		var kurs = document.getElementById('kurs').value;
		var jumlah = document.getElementById('jumlah').value;
		var payDate = document.getElementById('payDate').value;
		window.location='${path}/uw/cekpayment.htm?window=proses_voidpayment&flag=1&pembayaran='+pembayaran+'&nomor='+nomor+'&jenis1='+jenis1+'&jenis2='+jenis2+'&jenis3='+jenis3+'&account='+account+'&kurs='+kurs+'&jumlah='+jumlah+'&payDate='+payDate;
	}
	
	function tes(value){//bagian ini untuk merefresh parent saat melakukan submit di iframe(childnya).Yang jadi child ada di halaman input_voidpayment.jsp
		var payment = value.indexOf("-");
		var a = value.substring(0, payment);
		var b = value.length;
		var c = value.substring(payment+1, b);
		window.location='${path}/uw/cekpayment.htm?window=proses_voidpayment&nomor='+c+'&pembayaran='+a;
		
	}
	
	function proses(value){
		alert(value);
		if(value == "NO ACCOUNT") {
		document.getElementById("pilih").innerHTML = "No Account";	
		}else if(value == "MATA UANG"){
		document.getElementById("pilih").innerHTML = "Mata Uang";	
		}else if(value == "JUMLAH PREMI"){
		document.getElementById("pilih").innerHTML = "Jumlah";	
		
		}
	}
	
</script>

<c:if test="${not empty informasi}">
	<script>
		alert('Sending E-Mail berhasil dilakukan');
	</script>
</c:if>

<body onload="setFrameSize('infoFrame', 45); " onresize="setFrameSize('infoFrame', 45);" style="height: 100%;" >

<form:form id="formpost" name="formpost" commandName="cmd">
	<table class="entry2" width="98%">
		<tr>
			<th>Nomor SPAJ ~ Nomor Polis</th>
			<td>
				<input style="border-color:#6893B0;" class="readOnly" readonly="readonly" type="text" id="nomor" name="nomor" value="${nomor}" size="40">
				<c:if test="${nomor eq ''}">
					<script>
						alert("Masukkan No SPAJ/No Polis Terlebih Dahulu");
						popWin('${path}/uw/cekpayment.htm?window=cari_voidpayment', 500, 500);
					</script>
				</c:if>
				<input type="button" value="Cari" onClick="popWin('${path}/uw/cekpayment.htm?window=cari_voidpayment', 500, 500);" />
				<input type="button" value="SHow" onClick="tampil('0');" />
			</td>
		</tr>
		<tr>
			<td><br> </td>
		</tr>
		<tr>
			<td  colspan="2">
				<iframe src="${path}/uw/voidpayment.htm?nomor=${nomor}" name="infoFrame" id="infoFrame"
					width="100%" style="height: 100%;" frameborder="0"  > Please Wait... </iframe>
			</td>
		</tr>
	</table>
	<c:if test="${not empty proses}">
		<table class="entry2">
			<tr>
				<td width="50%" >
					<fieldset style="vertical-align: top;">
						<legend>Informasi Payment</legend>
						<table class="entry2">
							<tr></tr>
							<tr>
								<th>No Spaj</th>
								<td>${spaj}</td>
							</tr>
							<tr>
								<th>No Polis</th>
								<td>${nopolis}</td>
							</tr>
							<c:choose>
								<c:when test="${not empty payment.mspa_no_pre}">
									<tr>
										<th>No Pre</th>
										<td>${payment.mspa_no_pre}</td>
									</tr>
								</c:when>
								<c:otherwise>
									<tr>
										<th>No Pre</th>
										<td>-</td>
									</tr>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${not empty payment.mspa_no_voucher}">
									<tr>
										<th>No Voucher</th>
										<td>${payment.mspa_no_voucher}</td>
									</tr>
								</c:when>
								<c:otherwise>
									<tr>
										<th>No Voucher</th>
										<td>-</td>
									</tr>
								</c:otherwise>
							</c:choose>
							<tr>
								<th>Nama Bank</th>
								<td>${payment.lbn_nama}<input type="hidden" name="pembayaran" id="pembayaran" value="${pembayaran}"> </td>
							</tr>
							<tr>
								<th>Tanggal Pembayaran</th>
								<td><fmt:formatDate value="${payment.mspa_pay_date}" pattern="dd/MM/yyyy"/></td>
							</tr>
							<tr>
								<th>Tanggal Input</th>
								<td><fmt:formatDate value="${payment.mspa_input_date}" pattern="dd/MM/yyyy"/></td>
							</tr>
							<c:choose>
								<c:when test="${not empty urutan}">
									<tr>
										<th>Jumlah</th>
										<td>${payment.lku_symbol} &nbsp; <fmt:formatNumber value="${payment.mspa_payment}" type="number"></fmt:formatNumber></td>
										
									</tr>
								</c:when>
								<c:otherwise>
									<tr>
										<th>Jumlah</th>
										<td><fmt:formatNumber value="${payment.mspa_payment}" type="number"></fmt:formatNumber> &nbsp; ${payment.lku_symbol} </td>
									</tr>
								</c:otherwise>
							</c:choose>
							<tr></tr>
						</table>
					</fieldset>
				</td>
				<td width="50%" valign="top">
					<fieldset style="vertical-align: top;">
						<legend>Void Payment</legend>
						<table class="entry2"> 
							<tr>
								<th>User</th>
								<td><input style="border-color:#6893B0;" type="text" value="${sessionScope.currentUser.name}"
											class="readOnly" readonly="readonly"></td>
							</tr>
							<tr>
								<th width="40%">Revisi</th>
								<td>
									<input type="checkbox" name="jenis1" id="jenis1" class="noBorder" value="NO ACCOUNT" >NO ACCOUNT
									<br><input type="checkbox" name="jenis2" id="jenis2" class="noBorder" value="MATA UANG">MATA UANG
									<br><input type="checkbox" name="jenis3" id="jenis3" class="noBorder" value="JUMLAH PREMI">JUMLAH PREMI
								</td>
							</tr>
							<tr>
								<th>No Account</th>
								<td><input style="border-color:#6893B0;" type="text" id="account" name="account" value="${account}"></td>
							</tr>
							<tr>
								<th>Tanggal Pembayaran</th>
								<td>
									<script>inputDate('payDate', '${payDate}', false, '', 9);</script>
								</td>
							</tr>
							<tr>
								<th>Jumlah</th>
								<td>
								<select onchange="prosesList();" id="kurs" name="kurs" >
									<c:forEach items="${lsKurs}" var="x" varStatus="xt">
										<option value="${x.key}~${x.value}">${x.value}</option>
									</c:forEach>
								</select>
								<input style="border-color:#6893B0;" type="text" id="jumlah" name="jumlah" value="${jumlah}">
								</td>
							</tr>
							<tr>
								<th></th>
								<td>
									<input type="button" name="btnEmail" value="Email" onclick="buttonForm();">
								</td>
							</tr>
						</table>
					</fieldset>
				</td>
			</tr>
		</table>
	</c:if>
	<table class="entry2">
			<c:if test="${not empty errors}">
				<tr>
					<td>
						<div id="error" style="text-transform: none;">ERROR:<br>
							<c:forEach var="err" items="${errors}">
								- <c:out value="${err}" escapeXml="false" />
								<br />
							</c:forEach>
						</div>
					</td>
				</tr>
			</c:if>
	</table>
</form:form>
</body>

<c:if test="${not empty jenis}">
	<script type="text/javascript">
		var a = '${jenis}';
		document.getElementById('jenis').value = a;
	</script>
</c:if>

<c:if test="${not empty kurs}">
	<script type="text/javascript">
		var b = '${kurs}';
		document.getElementById('kurs').value = b;
	</script>
</c:if>

<%@ include file="/include/page/footer.jsp"%>