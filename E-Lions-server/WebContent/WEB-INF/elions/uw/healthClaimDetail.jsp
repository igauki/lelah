<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<script type="text/javascript" src="${path }/include/js/default.js"></script>
<script type="text/javascript">
	function hide(div,key){
		
		var state = document.getElementById(div).style.display;
		if (state == 'block') {
			document.getElementById(div).style.display = 'none';
		}else{
			document.getElementById(div).style.display = 'block';
		}
		
		var multi_array = parent.multiparam.split("%23");
		removeByElement(multi_array,key);
		parent.multiparam = multi_array.join("%23",multi_array);
	}
	
	//tes
	function removeByElement(arrayName,arrayElement)
	{
		for(var i=0; i<arrayName.length;i++ )
		{ 
			//if(arrayName[i]==arrayElement)
			if(i==arrayElement)
			arrayName.splice(i,1); 
		} 
	}
	//end

	function detailKlaim(c,d,e){ //Define arbitrary function to run desired DHTML Window widget codes
			parent.detailKlaim(c,d,e); //Run custom code when window is about to be closed
	}
</script>
<BODY style="height: 100%; text-align: center;" onload="loadImages()">
<div id="tes"></div>
<div id="hidepage" style="position: absolute; left:5px; top:5px; background-color: #FFFFCC; layer-background-color: #FFFFCC; height: 100%; width: 100%;"> 
	<table width=100%><tr><td>Page loading ... Please wait.</td></tr></table>
</div> 
	<fieldset style="text-align: center;">
		<legend>Riwayat Klaim Kesehatan</legend>
		<c:forEach items="${isinya}" var="isi" varStatus="s">
			<div id="${isi.polis}" style="display: block;">
				<br/>
				<div style="width: 100%; font-size:9pt; background-color:#df3200; color:white; font-weight: bolder; text-align: center; border-bottom: 1px solid black;border-top: 1px solid black;">
					NO POLIS : ${isi.polis}
					<div style="float:right;width: 100px;margin-top:-14px;z-index: 10px;"><input type="button" name="button" id="button" value="Close" onclick="hide('${isi.polis}','${isi.key}');"></div>
				</div>
			<c:choose>
			<c:when test="${isi.kode eq \"1\" }">
				<fieldset>
					<legend>DISTRIBUSI : INDIVIDU | STATUS CLAIM : ALL STATUS</legend>
					<c:choose>
						<c:when test="${empty isi.claim}">
								<b> tidak ada data</b>
						</c:when>
						<c:otherwise>
						
								<c:choose>
								<c:when test="${sessionScope.currentUser.jn_bank eq \"2\" or sessionScope.currentUser.jn_bank eq \"3\"}">
								</c:when>
								<c:when test="${sessionScope.currentUser.lde_id eq \"19\" or sessionScope.currentUser.lde_id eq \"20\"}">
									<table class="displaytag">
										<thead>
										<tr>
											<th>NAMA PEMEGANG</th>
											<th>TGL DIRAWAT</th>
											<th>JUMLAH  HARI RAWAT</th>
											<th></th>
										</tr>
										</thead>
										<tbody>
										<c:forEach items="${isi.claim}" var="claim" varStatus="s">
											<tr <c:if test="${s.count%2 eq 1}">class="odd"</c:if>>
												<td>${claim.MCL_FIRST}</td>
												<td><fmt:formatDate value="${claim.DATE_RI_1}" pattern="dd/MM/yyyy"/></td>
												<td>${claim.AMOUNT_RI}</td>
												<td><input type="button" name="tracking" value="Tracking Claim" onclick="popWin('${path}/uw/uw.htm?window=healthClaimTracking&regclaim=${claim.REGCLAIM}&regspajClaimDetail=${claim.REGSPAJ}', 250, 600); "/>
													<input type="button" value="Detail" onclick="detailKlaim('${claim.REGCLAIM}','5','${claim.EXPLANATION}');">
												</td>
											</tr>
										
										</c:forEach>
										</tbody>
									</table>
								</c:when>
								<c:otherwise>
									<table class="displaytag">
										<thead>
										<tr>
											<th>NAMA PEMEGANG</th>
											<th>NAMA TERTANGGUNG</th>
											<th>NO POLIS</th>
											<th>NAMA PRODUK</th>									
											<th>JENIS KLAIM</th>
											<th>TGL DIRAWAT</th>
											<th>JUMLAH  HARI RAWAT</th>
											<th>DIAGNOSA</th>
											<th>STATUS KLAIM</th>
											<th>KLAIM YANG DIAJUKAN</th>
											<th>JUMLAH KLAIM YANG DIBAYARKAN</th>
											<th></th>
										</tr>
										</thead>
										<tbody>
										<c:forEach items="${isi.claim}" var="claim" varStatus="s">
											<tr <c:if test="${s.count%2 eq 1}">class="odd"</c:if>>
												<td>${claim.MCL_FIRST}</td>
												<td>
													<c:choose>
														<c:when test="${claim.SEHAT_TTURUT gt 1}">${claim.NAMA}</c:when>
														<c:otherwise>${claim.C_NAMA } </c:otherwise>
													</c:choose>
													
												</td>
												<td><script type="text/javascript">document.write(formatPolis('${claim.MSPO_POLICY_NO }'));</script></td>
												<td>${claim.LSBS_NAME }</td>										
												<td>${claim.EXPLANATION}</td>
												<td><fmt:formatDate value="${claim.DATE_RI_1}" pattern="dd/MM/yyyy"/></td>
												<td>${claim.AMOUNT_RI}</td>
												<td>${claim.NM_DIAGNOS}</td>
												<td>${claim.ST_EXPL}</td>
												<td><fmt:formatNumber>${claim.AMT_CLAIM}</fmt:formatNumber></td>
												<td><fmt:formatNumber>${claim.PAY_CLAIM}</fmt:formatNumber></td>
												<td><input type="button" name="tracking" value="Tracking Claim" onclick="popWin('${path}/uw/uw.htm?window=healthClaimTracking&regclaim=${claim.REGCLAIM}&regspajClaimDetail=${claim.REGSPAJ}', 250, 600); "/>
													<input type="button" value="Detail" onclick="detailKlaim('${claim.REGCLAIM}','5','${claim.EXPLANATION}');">
												</td>
											</tr>
										
										</c:forEach>
										</tbody>
									</table>
								
								</c:otherwise>
								</c:choose>
							
							
						</c:otherwise>
					</c:choose>
				</fieldset>
				<%--<display:table name="isi.claim" id="isi.claim" class="displaytag">			
					<display:column property="MCL_FIRST" title="NAMA PEMEGANG"/>
					<display:column property="NAMA_TT" title="NAMA TERTANGGUNG"/>
					<display:column title="NO POLIS">
						<script type="text/javascript">document.write(formatPolis('${claim.MSPO_POLICY_NO }'));</script>
					</display:column>
					<display:column property="LSBS_NAME" title="NAMA PRODUK"/>
					<display:column property="DATE_RI_1" title="TGL DIRAWAT" format="{0, date, dd/MM/yyyy}" />
					<display:column property="NM_DIAGNOS" title="DIAGNOSA"/> 
					<display:column property="ST_EXPL" title="STATUS KLAIM"/>
					<display:column property="AMT_CLAIM" format="{0, number, #,##0.00;(#,##0.00)}" title="KLAIM YANG DIAJUKAN"/>
					<display:column property="PAY_CLAIM" format="{0, number, #,##0.00;(#,##0.00)}" title="JUMLAH KLAIM YANG DIBAYARKAN"/>
				</display:table>	--%>
			</c:when>
			<c:when test="${isi.kode eq \"2\" }">
				<fieldset>
					<legend>DISTRIBUSI : DMTM | STATUS CLAIM : ALL STATUS</legend>
					<c:choose>
						<c:when test="${empty isi.claim}">
								<b> tidak ada data</b>
						</c:when>
						<c:otherwise>
							<table class="displaytag">
								<thead>
								<tr>
									<th>NAMA PEMEGANG</th>
									<th>NAMA TERTANGGUNG</th>
									<th>NO POLIS</th>
									<th>NAMA PRODUK</th>
									<th>TGL DIRAWAT</th>
									<th>JUMLAH  HARI RAWAT</th>
									<th>DIAGNOSA</th>
									<th>STATUS KLAIM</th>
									<th>KLAIM YANG DIAJUKAN</th>
									<th>JUMLAH KLAIM YANG DIBAYARKAN</th>
									<th></th>
								</tr>
								</thead>
								<tbody>
								<c:forEach items="${isi.claim}" var="claim" varStatus="s">
									<tr <c:if test="${s.count%2 eq 1}">class="odd"</c:if>>
										<td>${claim.HOLDER_NAME}</td>
										<td>${claim.NAMA_PESERTA} </td>
										<td>${claim.NO_SERTIFIKAT }</td>
										<td>${claim.PRODUCT_NAME }</td>
										<td><fmt:formatDate value="${claim.MCM_TGL_1}" pattern="dd/MM/yyyy"/></td>
										<td>${claim.MCM_HARI}</td>
										<td>${claim.MCM_DIAGNOSA}</td>
										<td>${claim.STATUS_ACCEPT}</td>
										<td><fmt:formatNumber>${claim.MDCM_JLH_CLAIM}</fmt:formatNumber></td>
										<td><fmt:formatNumber>${claim.MDCM_BAYAR}</fmt:formatNumber></td>
										<td><input type="button" value="Detail" onclick="detailKlaim('${claim.MCE_NO_KLAIM}','6','');"></td>
									</tr>
								
								</c:forEach>
								</tbody>
							</table>
						</c:otherwise>
					</c:choose>
				</fieldset>
				<%--<display:table name="isi.claim" id="isi.claim" class="displaytag">			
					<display:column property="HOLDER_NAME" title="NAMA PEMEGANG"/>
					<display:column property="NAMA_PESERTA" title="NAMA TERTANGGUNG"/>
					<display:column property="NO_SERTIFIKAT" title="NO POLIS"/>
					<display:column property="PRODUCT_NAME" title="NAMA PRODUK"/>
					<display:column property="MCM_TGL_1" title="TGL DIRAWAT" format="{0, date, dd/MM/yyyy}" />
					<display:column property="MCM_DIAGNOSA" title="DIAGNOSA"/> 
					<display:column property="STATUS_ACCEPT" title="STATUS KLAIM"/>
					<display:column property="MDCM_JLH_CLAIM" format="{0, number, #,##0.00;(#,##0.00)}" title="KLAIM YANG DIAJUKAN"/>
					<display:column property="MDCM_BAYAR" format="{0, number, #,##0.00;(#,##0.00)}" title="JUMLAH KLAIM YANG DIBAYARKAN"/>
				</display:table> --%>
			</c:when>
			<c:when test="${isi.kode eq \"3\" }">		
				<c:if test="${not empty isi.claimAccept}">		
				<fieldset>
					<legend>DISTRIBUSI : EB | STATUS CLAIM : AKSEP</legend>
					<c:choose>
						<c:when test="${empty isi.claimAccept}">
								<b> tidak ada data</b>
						</c:when>
						<c:otherwise>
							<table class="displaytag">
								<thead>
								<tr>
									<th>JENIS KLAIM</th>
									<th>NAMA TERTANGGUNG</th>
									<th>TGL DIRAWAT</th>
									<th>JUMLAH  HARI RAWAT</th>
									<th>DIAGNOSA</th>
									<th>KLAIM YANG DIAJUKAN</th>
									<th>JUMLAH KLAIM YANG DIBAYARKAN</th>
									<th>DETAIL</th>
								</tr>
								</thead>
								<tbody>
								<c:forEach items="${isi.claimAccept}" var="claim" varStatus="s">
									<tr <c:if test="${s.count%2 eq 1}">class="odd"</c:if>>
										<td>${claim.LSDBS_NAME} </td>
										<td>${claim.NAMA} </td>
										<td><fmt:formatDate value="${claim.MCE_TGL_1}" pattern="dd/MM/yyyy"/></td>
										<td>${claim.MCE_HARI}</td>
										<td>${claim.LSD_DESC_ALL}</td>
										<td><fmt:formatNumber>${claim.MDC_JLH_CLAIM}</fmt:formatNumber></td>
										<td><fmt:formatNumber>${claim.MDC_BAYAR}</fmt:formatNumber></td>
										<td>
											<input type="button" value="Detail" onclick="detailKlaim('${claim.MCE_NO_KLAIM}','3','${claim.MSTE_INSURED_NO}');">
										</td>
									</tr>
								
								</c:forEach>
								</tbody>
							</table>
						</c:otherwise>
					</c:choose>
					
					<%-- <display:table name="isinya[1].claimAccept"   class="displaytag">
						<display:column property="KODE" title="NAMA TERTANGGUNG"/>
						<display:column property="MCE_TGL_1" title="TGL DIRAWAT" format="{0, date, dd/MM/yyyy}" />
						<display:column property="LSD_DESC_ALL" title="DIAGNOSA"/> 
						<display:column property="MDC_JLH_CLAIM" format="{0, number, #,##0.00;(#,##0.00)}" title="KLAIM YANG DIAJUKAN"/>
						<display:column property="MDC_BAYAR" format="{0, number, #,##0.00;(#,##0.00)}" title="JUMLAH KLAIM YANG DIBAYARKAN"/>
					</display:table>--%>
				</fieldset>
				</c:if>
				<c:if test="${not empty isi.claimTolak}">
				<fieldset>
				<legend>DISTRIBUSI : EB | STATUS CLAIM : TOLAK</legend>
					<c:choose>
						<c:when test="${empty isi.claimTolak}">
							<b> tidak ada data</b>
						</c:when>
						<c:otherwise>
							<table class="displaytag">
								<thead>
								<tr>
									<th>JENIS KLAIM</th>
									<th>NAMA TERTANGGUNG</th>
									<th>TGL DIRAWAT</th>
									<th>JUMLAH  HARI RAWAT</th>
									<th>DIAGNOSA</th>
									<th>KLAIM YANG DIAJUKAN</th>
									<th>JUMLAH KLAIM YANG DIBAYARKAN</th>
									<th>DETAIL</th>
								</tr>
								</thead>
								<tbody>
								<c:forEach items="${isi.claimTolak}" var="claim" varStatus="s">
									<tr <c:if test="${s.count%2 eq 1}">class="odd"</c:if>>
										<td>${claim.LSDBS_NAME} </td>
										<td>${claim.NAMA} </td>
										<td><fmt:formatDate value="${claim.MCE_TGL_1}" pattern="dd/MM/yyyy"/></td>
										<td>${claim.MCE_HARI}</td>
										<td>${claim.LSD_DESC_ALL}</td>
										<td><fmt:formatNumber>${claim.MDC_JLH_CLAIM}</fmt:formatNumber></td>
										<td><fmt:formatNumber>0</fmt:formatNumber></td>
										<td>
											<input type="button" value="Detail" onclick="detailKlaim('${claim.MCE_NO_KLAIM}','4','${claim.MSTE_INSURED_NO}');">
										</td>
									</tr>
								
								</c:forEach>
								</tbody>
							</table>
						</c:otherwise>
					</c:choose>
					<%--<display:table name="isi.CLAIMTOLAK" id="isi.CLAIMTOLAK" class="displaytag">
						<display:column property="NAMA" title="NAMA TERTANGGUNG"/>
						<display:column property="MCE_TGL_1" title="TGL DIRAWAT" format="{0, date, dd/MM/yyyy}" />
						<display:column property="LSD_DESC_ALL" title="DIAGNOSA"/> 
						<display:column property="MDC_JLH_CLAIM" format="{0, number, #,##0.00;(#,##0.00)}" title="KLAIM YANG DIAJUKAN"/>
						<display:column property="MDC_BAYAR" format="{0, number, #,##0.00;(#,##0.00)}" title="JUMLAH KLAIM YANG DIBAYARKAN"/>
					</display:table>--%>
				</fieldset>
				</c:if>
				<c:if test="${not empty isi.claimPreAccep}">
				<fieldset>
				<legend>DISTRIBUSI : EB | STATUS CLAIM : PRE AKSEP</legend>
					<c:choose>
						<c:when test="${empty isi.claimPreAccep}">
								<b> tidak ada data</b>
						</c:when>
						<c:otherwise>
							<table class="displaytag">
								<thead>
								<tr>
									<th>JENIS KLAIM</th>
									<th>NAMA TERTANGGUNG</th>
									<th>TGL DIRAWAT</th>
									<th>JUMLAH  HARI RAWAT</th>
									<th>DIAGNOSA</th>
									<th>KLAIM YANG DIAJUKAN</th>
									<th>JUMLAH KLAIM YANG DIBAYARKAN</th>
									<th>DETAIL</th>
								</tr>
								</thead>
								<tbody>
								<c:forEach items="${isi.claimPreAccep}" var="claim" varStatus="s">
									<tr <c:if test="${s.count%2 eq 1}">class="odd"</c:if>>
										<td>${claim.LSDBS_NAME} </td>
										<td>${claim.NAMA} </td>
										<td><fmt:formatDate value="${claim.MCE_TGL_1}" pattern="dd/MM/yyyy"/></td>
										<td>${claim.MCE_HARI}</td>
										<td>${claim.LSD_DESC_ALL}</td>
										<td><fmt:formatNumber>${claim.MDC_JLH_CLAIM}</fmt:formatNumber></td>
										<td><fmt:formatNumber>${claim.MDC_BAYAR}</fmt:formatNumber></td>
										<td>
											<input type="button" value="Detail" onclick="detailKlaim('${claim.MCE_NO_KLAIM}','1','${claim.MSTE_INSURED_NO}');">
										</td>
									</tr>
								
								</c:forEach>
								</tbody>
							</table>
						</c:otherwise>
					</c:choose>
					<%--<display:table name="isi.claimPreAccept" id="isi.claimPreAccept" class="displaytag">
						<display:column property="NAMA" title="NAMA TERTANGGUNG"/>
						<display:column property="MCE_TGL_1" title="TGL DIRAWAT" format="{0, date, dd/MM/yyyy}" />
						<display:column property="LSD_DESC_ALL" title="DIAGNOSA"/> 
						<display:column property="MDC_JLH_CLAIM" format="{0, number, #,##0.00;(#,##0.00)}" title="KLAIM YANG DIAJUKAN"/>
						<display:column property="MDC_BAYAR" format="{0, number, #,##0.00;(#,##0.00)}" title="JUMLAH KLAIM YANG DIBAYARKAN"/>
					</display:table>--%>
				</fieldset>
				</c:if>
				<c:if test="${not empty isi.claimPU}">
				<fieldset>
				<legend>DISTRIBUSI : EB | STATUS CLAIM : PU</legend>
					<c:choose>
						<c:when test="${empty isi.claimPU}">
								<b> tidak ada data</b>
						</c:when>
						<c:otherwise>
							<table class="displaytag">
								<thead>
								<tr>
									<th>JENIS KLAIM</th>
									<th>NAMA TERTANGGUNG</th>
									<th>TGL DIRAWAT</th>
									<th>JUMLAH  HARI RAWAT</th>
									<th>DIAGNOSA</th>
									<th>KLAIM YANG DIAJUKAN</th>
									<th>JUMLAH KLAIM YANG DIBAYARKAN</th>
									<th>DETAIL</th>
								</tr>
								</thead>
								<tbody>
								<c:forEach items="${isi.claimPU}" var="claim" varStatus="s">
									<tr <c:if test="${s.count%2 eq 1}">class="odd"</c:if>>
										<td>${claim.LSDBS_NAME} </td>
										<td>${claim.NAMA} </td>
										<td><fmt:formatDate value="${claim.MCE_TGL_1}" pattern="dd/MM/yyyy"/></td>
										<td>${claim.MCE_HARI}</td>
										<td>${claim.LSD_DESC_ALL}</td>
										<td><fmt:formatNumber>${claim.MDC_JLH_CLAIM}</fmt:formatNumber></td>
										<td><fmt:formatNumber>${claim.MDC_BAYAR}</fmt:formatNumber></td>
										<td>
											<input type="button" value="Detail" onclick="detailKlaim('${claim.MCE_NO_KLAIM}','2','${claim.MSTE_INSURED_NO}');">
										</td>
									</tr>
								
								</c:forEach>
								</tbody>
							</table>
						</c:otherwise>
					</c:choose>
					<%--<display:table name="isi.claimPreAccept" id="isi.claimPreAccept" class="displaytag">
						<display:column property="NAMA" title="NAMA TERTANGGUNG"/>
						<display:column property="MCE_TGL_1" title="TGL DIRAWAT" format="{0, date, dd/MM/yyyy}" />
						<display:column property="LSD_DESC_ALL" title="DIAGNOSA"/> 
						<display:column property="MDC_JLH_CLAIM" format="{0, number, #,##0.00;(#,##0.00)}" title="KLAIM YANG DIAJUKAN"/>
						<display:column property="MDC_BAYAR" format="{0, number, #,##0.00;(#,##0.00)}" title="JUMLAH KLAIM YANG DIBAYARKAN"/>
					</display:table>--%>
				</fieldset>
				</c:if>
				
			</c:when>
			<c:otherwise>
				Silahkan klik tombol "klaim" untuk melihat Riwayat klaim kesehatan
			</c:otherwise>
		</c:choose>
		</div>	
		</c:forEach>
		<%--
		<c:choose>
			<c:when test="${kode eq \"1\" }">
				<display:table name="claim" id="claim" class="displaytag">			
					<display:column property="MCL_FIRST" title="NAMA PEMEGANG"/>
					<display:column property="NAMA_TT" title="NAMA TERTANGGUNG"/>
					<display:column title="NO POLIS">
						<script type="text/javascript">document.write(formatPolis('${claim.MSPO_POLICY_NO }'));</script>
					</display:column>
					<display:column property="LSBS_NAME" title="NAMA PRODUK"/>
					<display:column property="DATE_RI_1" title="TGL DIRAWAT" format="{0, date, dd/MM/yyyy}" />
					<display:column property="NM_DIAGNOS" title="DIAGNOSA"/> 
					<display:column property="ST_EXPL" title="STATUS KLAIM"/>
					<display:column property="AMT_CLAIM" format="{0, number, #,##0.00;(#,##0.00)}" title="KLAIM YANG DIAJUKAN"/>
					<display:column property="PAY_CLAIM" format="{0, number, #,##0.00;(#,##0.00)}" title="JUMLAH KLAIM YANG DIBAYARKAN"/>
				</display:table>	
			</c:when>
			<c:when test="${kode eq \"2\" }">
				<display:table name="claim" id="claim" class="displaytag">			
					<display:column property="HOLDER_NAME" title="NAMA PEMEGANG"/>
					<display:column property="NAMA_PESERTA" title="NAMA TERTANGGUNG"/>
					<display:column property="NO_SERTIFIKAT" title="NO POLIS"/>
					<display:column property="PRODUCT_NAME" title="NAMA PRODUK"/>
					<display:column property="MCM_TGL_1" title="TGL DIRAWAT" format="{0, date, dd/MM/yyyy}" />
					<display:column property="MCM_DIAGNOSA" title="DIAGNOSA"/> 
					<display:column property="STATUS_ACCEPT" title="STATUS KLAIM"/>
					<display:column property="MDCM_JLH_CLAIM" format="{0, number, #,##0.00;(#,##0.00)}" title="KLAIM YANG DIAJUKAN"/>
					<display:column property="MDCM_BAYAR" format="{0, number, #,##0.00;(#,##0.00)}" title="JUMLAH KLAIM YANG DIBAYARKAN"/>
				</display:table>
			</c:when>
			<c:when test="${kode eq \"3\" }">
				<fieldset>
					<legend>TYPE CLAIM : AKSEP</legend>
					<display:table name="claimAccept" id="claimAccept" class="displaytag">
						<display:column property="NAMA" title="NAMA TERTANGGUNG"/>
						<display:column property="MCE_TGL_1" title="TGL DIRAWAT" format="{0, date, dd/MM/yyyy}" />
						<display:column property="LSD_DESC_ALL" title="DIAGNOSA"/> 
						<display:column property="MDC_JLH_CLAIM" format="{0, number, #,##0.00;(#,##0.00)}" title="KLAIM YANG DIAJUKAN"/>
						<display:column property="MDC_BAYAR" format="{0, number, #,##0.00;(#,##0.00)}" title="JUMLAH KLAIM YANG DIBAYARKAN"/>
					</display:table>
				</fieldset>
				<fieldset>
				<legend>TYPE CLAIM : TOLAK</legend>
					<display:table name="claimTolak" id="claimTolak" class="displaytag">
						<display:column property="NAMA" title="NAMA TERTANGGUNG"/>
						<display:column property="MCE_TGL_1" title="TGL DIRAWAT" format="{0, date, dd/MM/yyyy}" />
						<display:column property="LSD_DESC_ALL" title="DIAGNOSA"/> 
						<display:column property="MDC_JLH_CLAIM" format="{0, number, #,##0.00;(#,##0.00)}" title="KLAIM YANG DIAJUKAN"/>
						<display:column property="MDC_BAYAR" format="{0, number, #,##0.00;(#,##0.00)}" title="JUMLAH KLAIM YANG DIBAYARKAN"/>
					</display:table>
				</fieldset>
				<fieldset>
				<legend>TYPE CLAIM : PRE AKSEP</legend>
					<display:table name="claimPreAccept" id="claimPreAccept" class="displaytag">
						<display:column property="NAMA" title="NAMA TERTANGGUNG"/>
						<display:column property="MCE_TGL_1" title="TGL DIRAWAT" format="{0, date, dd/MM/yyyy}" />
						<display:column property="LSD_DESC_ALL" title="DIAGNOSA"/> 
						<display:column property="MDC_JLH_CLAIM" format="{0, number, #,##0.00;(#,##0.00)}" title="KLAIM YANG DIAJUKAN"/>
						<display:column property="MDC_BAYAR" format="{0, number, #,##0.00;(#,##0.00)}" title="JUMLAH KLAIM YANG DIBAYARKAN"/>
					</display:table>
				</fieldset>
			</c:when>
			<c:otherwise>
				Silahkan klik tombol "klaim" untuk melihat Riwayat klaim kesehatan
			</c:otherwise>
		</c:choose>--%>
		
	</fieldset>
</body>
<%@ include file="/include/page/footer.jsp"%>