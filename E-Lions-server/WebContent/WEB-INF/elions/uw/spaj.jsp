<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<META HTTP-EQUIV="X-UA-COMPATIBLE" CONTENT="IE=IE9">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<!-- DatePicker Script (jscalendar) -->
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	function cari(){
		if(trim(document.formpost.kata.value)=='') return false;
		else createLoadingMessage();
	}
	
	function backToParent1(spaj, polis, region)
	{
			ajaxManager.listcollect(spaj , 'region',
			{callback:function(map) {
		
				DWRUtil.useLoadingMessage();
				document.formpost.koderegion.value=map.LSRG_NAMA;
				document.formpost.kodebisnis.value = map.LSBS_ID;
				document.formpost.numberbisnis.value = map.LSDBS_NUMBER;
				document.formpost.jml_peserta.value = map.jml_peserta;
				backToParent(spaj, polis, map.LSRG_NAMA,map.LSBS_ID,map.LSDBS_NUMBER,map.jml_peserta);
			   },
			  timeout:15000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
	function backToParent(spaj, polis, region,kodebisnis,numberbisnis,jml_peserta){
		//var lockName = document.getElementById('lockName').value;
		var win = document.getElementById('win').value;
		//spaj = formatSpaj(spaj);
		//polis = formatPolis(polis);
		var retVal = spaj + " - " + polis;
		
		var dok;
		if(self.opener)
			dok = self.opener.document;
		else
			dok = parent.document;
		
		var forminput;
		if(dok.formpost) forminput = dok.formpost;
		else if(dok.frmParam) forminput = dok.frmParam;
		
		if('bac' == win){
			forminput.koderegion.value = region;
			forminput.kodebisnis.value = kodebisnis;
			forminput.numberbisnis.value = numberbisnis;
			forminput.jml_peserta.value = jml_peserta;
			addOptionToSelect(dok, forminput.spaj, retVal, spaj);
		}else if('komisi' == win){
			self.opener.ajaxPesan(spaj, 8);
		}else if('printpolis' == win){
			dok.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj;
		}else if('viewer' == win){
			if(self.opener)	{self.opener.ajaxPesan(spaj, -1);}
			else {parent.ajaxPesan(spaj, -1);}
		}else if('payment' == win){
			self.opener.ajaxPesan(spaj, 4);
		}else if('ttp' == win){
			//...
		}else if('uw' == win){
			forminput.koderegion.value = region;
			forminput.kodebisnis.value = kodebisnis;
			forminput.numberbisnis.value = numberbisnis;
			forminput.jml_peserta.value = jml_peserta;
			addOptionToSelect(dok, forminput.spaj, retVal, spaj);
		}
		if(forminput.spaj.type=='select-one'){
			addOptionToSelect(dok, forminput.spaj, retVal, spaj);
		}else
			dok.getElementById('spaj').value = spaj; 
		
		if(dok.getElementById('polis'))
			dok.getElementById('polis').value = polis;

		if('bac' == win){	
			dok.getElementById('infoFrame').src='${path}/bac/edit.htm?showSPAJ='+spaj; 
		}else if('viewer' == win){
			dok.getElementById('infoFrame').src='${path}/uw/view.htm?p=v&showSPAJ='+spaj+'&halForBlacklist=uw';
			//if(dok.getElementById('docFrame')) dok.getElementById('docFrame').src='${path}/common/util.htm?window=dokumen&spaj='+spaj;
			if(dok.getElementById('docFrame')) dok.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+spaj;
		}else if('uw' == win){
			if(dok.getElementById('docFrame')) dok.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+spaj;
			if(dok.getElementById('infoFrame')) dok.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj+'&halForBlacklist=uw';			
		}else if('printpolis' == win){
			if(dok.getElementById('docFrame')) dok.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+spaj;			
		}else if('payment' == win){
			if(dok.getElementById('infoFrame')) dok.getElementById('infoFrame').src='${path}/uw/inputpayment.htm?tahun_ke=1&premi_ke=1&spaj='+spaj;
			if(dok.getElementById('docFrame')) dok.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+spaj;
		}else if('printpolis' != win){
			if(dok.getElementById('infoFrame')) dok.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj; 
		}
		if(self.opener) window.close();
	}
	
	function referesh(){	
		if(trim(document.formpost.kata.value)=='') return false;
		else document.getElementById('refresh').click();
	}

</script>
</head>
<BODY onload="resizeCenter(800, 600); document.title='PopUp :: Cari SPAJ'; setupPanes('container1', 'tab1'); document.formpost.kata.select(); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Cari SPAJ</a>
				<a href="#" onClick="return showPane('pane2', this)" id="tab2">Cari PAS</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" action="${path }/uw/spaj.htm" style="text-align: center;">
					<input type="hidden" name="win" id="win" value="${param.win}">
					<input type="hidden" name="posisi" value="${param.posisi}">
					<input type="hidden" name="koderegion" >
					<input type="hidden" name="kodebisnis" >
					<input type="hidden" name="numberbisnis" >
					<input type="hidden" name="jml_peserta">
					<fieldset>
					<table class="entry2">
						<tr>
							<th rowspan="2">Cari:</th>
							<th class="left">
								<select name="tipe">
									<option value="0" <c:if test="${param.tipe eq \"0\" }">selected</c:if>>No. SPAJ</option>
									<option value="1" <c:if test="${param.tipe eq \"1\" }">selected</c:if>>No. Polis</option>
									<option value="2" <c:if test="${param.tipe eq \"2\" }">selected</c:if>>Pemegang</option>
									<option value="3" <c:if test="${param.tipe eq \"3\" }">selected</c:if>>Tertanggung</option>
									<option value="4" <c:if test="${param.tipe eq \"4\" }">selected</c:if>>Perusahaan</option>
									<option value="5" <c:if test="${param.tipe eq \"5\" }">selected</c:if>>No. Kartu</option>
									<option value="6" <c:if test="${param.tipe eq \"6\" }">selected</c:if>>No. Blanko</option>
									<option value="7" <c:if test="${param.tipe eq \"7\" }">selected</c:if>>No. Billing</option>
									<option value="8" <c:if test="${param.tipe eq \"8\" }">selected</c:if>>Kode FA</option>
									<option value="9" <c:if test="${param.tipe eq \"9\" }">selected</c:if>>No.SimasCard</option>
									<option value="10" <c:if test="${param.tipe eq \"10\" }">selected</c:if>>NIK</option>
									<option value="11" <c:if test="${param.tipe eq \"11\" }">selected</c:if>>No.VirtualACC</option>
									<option value="12" <c:if test="${param.tipe eq \"12\" }">selected</c:if>>No.Gadget</option>
									<option value="13" <c:if test="${param.tipe eq \"13\" }">selected</c:if>>No. Sertifikat (Bukopin)</option>
								</select>
								<select name="pilter">
									<option <c:if test="${\"LIKE%\" eq param.pilter}">selected</c:if> value="LIKE%">LIKE%</option>
									<option <c:if test="${\"EQ\" eq param.pilter}">selected</c:if> value="EQ">=</option>
								<!-- 
									<option <c:if test="${\"LIKE%\" eq param.pilter}">selected</c:if> value="LIKE%">LIKE%</option>
									<option <c:if test="${\"%LIKE\" eq param.pilter}">selected</c:if> value="%LIKE">%LIKE</option>
									<option <c:if test="${\"%LIKE%\" eq param.pilter}">selected</c:if> value="%LIKE%">%LIKE%</option>
									<option <c:if test="${\"LT\" eq param.pilter}">selected</c:if> value="LT"><</option>
									<option <c:if test="${\"LE\" eq param.pilter}">selected</c:if> value="LE"><=</option>
									<option <c:if test="${\"EQ\" eq param.pilter}">selected</c:if> value="EQ">=</option>
									<option <c:if test="${\"GE\" eq param.pilter}">selected</c:if> value="GE">>=</option>
									<option <c:if test="${\"GT\" eq param.pilter}">selected</c:if> value="GT">></option>
									
									 -->
								</select>					
								<input type="text" name="kata" size="35" value="${param.kata }" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
								<input type="submit" name="search" value="Search" onclick="return cari();"
									accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
							</th>
						</tr>
						<tr>
							<th class="left">
								<label for="lahir">
									<input class="noBorder" type="checkbox" name="centang" id="lahir" value="1">
									Tanggal Lahir (untuk pencarian berdasarkan Pemegang/Tertanggung) :
								</label>
								<script>inputDate('tgl_lahir', '', false);</script>
								<br>
								<div style="text-align: left;font-weight: bold;">
									* WARNA <font color="blue">BIRU</font> : ATTENTION LIST <br/>
									* WARNA <font color="RED">MERAH</font> : RED FLAG ( HIGH RISK PROFILE )									
								</div>
							</th>
						</tr>
					</table>
					</fieldset>		
					<table class="simple">
						<thead>
							<tr>
								<th style="text-align: center;">Distribusi</th>
								<th style="text-align: center;">No. SPAJ</th>
								<th style="text-align: center;">No. Polis</th>
								<th style="text-align: center;">Produk</th>
								<th style="text-align: center;">Pemegang Polis</th>
								<th style="text-align: center;">Tertanggung</th>
								<th style="text-align: center;">Posisi</th>
								<th style="text-align: center;">Status Polis</th>
								<c:if test="${param.tipe eq \"10\"}">
									<th style="text-align: center;">Nama Perusahaan</th>
									<th style="text-align: center;">NIK</th>
								</c:if>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="spaj" items="${cmd.listSpaj}" varStatus="stat">
								<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
									onclick="backToParent1('${spaj.reg_spaj }', '${spaj.cariSpaj.mspo_policy_no}' , '${spaj.cariSpaj.lsrg_nama}');">
									<c:choose>
										<c:when test="${spaj.mcl_blacklist eq 1}">
											<td><font color="blue">${spaj.cariSpaj.distribusi}</font></td>
											<td><font color="blue"><elions:spaj nomor="${spaj.reg_spaj }"/></font></td>
											<td><font color="blue"><elions:polis nomor="${spaj.cariSpaj.mspo_policy_no}"/></font></td>
											<td><font color="blue">${spaj.cariSpaj.lsdbs_name}</font></td>
											<td><font color="blue">${spaj.cariSpaj.pp}</font></td>
											<td><font color="blue">${spaj.cariSpaj.tt}</font></td>
											<td><font color="blue">${spaj.cariSpaj.lspd_position}</font></td>
											<td><font color="blue">${spaj.cariSpaj.lssp_status}</font></td>
											<c:if test="${param.tipe eq \"10\"}">
												<td><font color="blue">${spaj.cariSpaj.perusahaan}</font></td>
												<td><font color="blue">${spaj.cariSpaj.nik}</font></td>
											</c:if>
										</c:when>
										<c:when test="${spaj.mcl_blacklist eq 0}">
											<td>${spaj.cariSpaj.distribusi}</td>
											<td><elions:spaj nomor="${spaj.reg_spaj }"/></td>
											<td><elions:polis nomor="${spaj.cariSpaj.mspo_policy_no}"/></td>
											<td>${spaj.cariSpaj.lsdbs_name}</td>
											<td>${spaj.cariSpaj.pp}</td>
											<td>${spaj.cariSpaj.tt}</td>
											<td>${spaj.cariSpaj.lspd_position}</td>
											<td>${spaj.cariSpaj.lssp_status}</td>
											<c:if test="${param.tipe eq \"10\"}">
												<td>${spaj.cariSpaj.perusahaan}</td>
												<td>${spaj.cariSpaj.nik}</td>
											</c:if>
										</c:when>
										<c:otherwise>
											<td>${spaj.cariSpaj.distribusi}</td>
											<td><elions:spaj nomor="${spaj.reg_spaj }"/></td>
											<td><elions:polis nomor="${spaj.cariSpaj.mspo_policy_no}"/></td>
											<td>${spaj.cariSpaj.lsdbs_name}</td>
											<td>${spaj.cariSpaj.pp}</td>
											<td>${spaj.cariSpaj.tt}</td>
											<td>${spaj.cariSpaj.lspd_position}</td>
											<td>${spaj.cariSpaj.lssp_status}</td>
											<c:if test="${param.tipe eq \"10\"}">
												<td>${spaj.cariSpaj.perusahaan}</td>
												<td>${spaj.cariSpaj.nik}</td>
											</c:if>
										</c:otherwise>
									</c:choose>
								</tr>
								<c:set var="jml" value="${stat.count}"/>
								<c:if test="${stat.count eq 1}">
									<c:set var="v1" value="${spaj.reg_spaj}"/>
									<c:set var="v2" value="${spaj.cariSpaj.mspo_policy_no}"/>
									<c:set var="v3" value="${spaj.cariSpaj.lsrg_nama}"/>
								</c:if>
							</c:forEach>
						</tbody>
					</table>					
					<table  class="simple" align="left" cellpadding="2" bgcolor="#C2C2C2" >       				
			        <tr>
			            <td  width="800" class="">Page ${currPage} of ${lastPage} 
			            <input type="submit" name="refresh" value="refresh" id="refresh" style="visibility: hidden;" /> 
			            </td>			           
			            <td  width="270" align="right" class="">Go
			                <input name="cPage" type="text" style="width:45px" maxlength="4" >
			                <input type="button" value="Go" id="go" style="width: 40px;"  onclick="referesh()"/>
			               <label> &nbsp;&nbsp; &nbsp;</label>
			                &nbsp;			           
			                
			                <c:choose>
			        			<c:when test="${currPage eq '1'}">
			        			<font color="gray">First | Prev |</font> 
			        			</c:when>
			        			<c:otherwise>
			               			<a href="#" class="" onclick="document.forms[ 0 ].cPage.value='${firstPage}'; referesh()">First</a> | 
			                		<a href="#" class="" onclick="document.forms[ 0 ].cPage.value='${previousPage}'; referesh()">Prev</a> | 
			                	</c:otherwise>
			                </c:choose>
			                
			                <c:choose>
			        			<c:when test="${currPage eq lastPage}">
			        			<font color="gray">Next | Last </font>
			        			</c:when>
			        			<c:otherwise>
			               			<a href="#" class="" onclick="document.forms[ 0 ].cPage.value='${nextPage}'; referesh()">Next</a> | 
			                		 <a href="#" class="" onclick="document.forms[ 0 ].cPage.value='${lastPage}'; referesh()">Last</a> 
			                	</c:otherwise>
			                </c:choose>                
               			 </td>
      				  </tr>
    				</table>  
					<input type="hidden" name="flag" value="${cmd.flag }" >
					<input type="hidden" name="lssaId" value="${cmd.lssaId}" >
					<input type="hidden" name="lsspId" value="${cmd.lsspId}" >
				</form>
			</div>
			<div id="pane2" class="panes">
				<iframe src="${path}/uw/viewer/viewer_pas.htm" name="pasFrame" id="pasFrame" width="100%" height="450px"> Please Wait... </iframe>
			</div>
		</div>
	</div>

</form>
<c:if test="${jml eq 1}">
<script>backToParent1('${v1}', '${v2}' , '${v3}');</script>
</c:if>
</body>
<%@ include file="/include/page/footer.jsp"%>