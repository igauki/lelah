<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>
<script>
	function backToParent(nomor){
		self.opener.document.getElementById('nomor').value = nomor;
		self.opener.document.getElementById('infoFrame').src='${path }/uw/voidpayment.htm?nomor='+nomor;
		window.close();
	}
	

</script>

<BODY onload="setFocus('nomor'); document.title='PopUp :: Cari Void Payment';" style="height: 100%;">
<form method="post" name="formpost" >
	<fieldset>
		<legend>Cari Data Nasabah</legend>
		<table class="entry2">
			<tr>
				<th>Cari Nasabah</th>
				<td>
					<select name="tipe" >
						<option value="1" <c:if test="${tipe eq 1 }" >selected</c:if>>No Spaj</option>
						<option value="2" <c:if test="${tipe eq 2 }" >selected</c:if>>No Polis</option>
					</select>
					<input type="text" name="nomor" maxLength="20" size="20" value="${nomor}" />
					<input type="submit" name="btnCari" value="Cari">
					<input type="hidden" name="fusionNomor" id="fusionNomor" value="${fusion}">
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<table class="simple">
						<thead>
							<tr>
								<th style="text-align: center;">No Spaj</th>
								<th style="text-align: center;">No Polis</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="x" items="${lsCariPayment}" varStatus="xt">
								<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
									onclick="backToParent('${x.SPAJ_FORMATTED}~${x.POLICY_FORMATTED}');">
									<td align="center">${x.SPAJ_FORMATTED }</td>
									<td align="center">${x.POLICY_FORMATTED}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<th colspan="2"><input type="button" name="btnClose" value="Close" onClick="window.close()"></th>
				<input type="hidden" name="flag" value="1" >
			</tr>
		</table>
	</fieldset>
</form>
</body>

<c:if test="${fusion ne null}">
<script>
	var no = document.getElementById('fusionNomor').value;
	backToParent(no);
</script>
</c:if>

<%@ include file="/include/page/footer.jsp"%>