<%@ include file="/include/page/header_jquery.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>    
    <title>PT. Asuransi Jiwa Sinarmas MSIG</title>
    <link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">    
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<c:set var="pesan" value="${error}"/>
	<script type="text/javascript">

	$(document).ready(function() {
	   if($('#0').click(function(){
	     $('#en_penyakit').show();
	     $('#en_ssu').hide(); 
	     $('#en_ssu_pasal').hide();
	     $('#save_ssu').hide();  
	   }));
	   if($('#1').click(function(){
	      $('#en_ssu').show();
	      $('#en_ssu_pasal').show();
	      $('#save_ssu').show();  
	      $('#en_penyakit').hide();
	   }));
  	    $('#up').priceFormat({
	   	 prefix: '',
	     centsSeparator: '.',
    	 thousandsSeparator: '.',
    	 centsLimit: 0
		 });
	 });
	 

	</script>
	<style type="text/css">
	body
	{
		font-size: 1px;
		color: #996600;
		background-color: #F2F2F2;
	}
	</style>

  </head>
  
  <body bgcolor="ffffff" onLoad="Body_onload();">
	  <form name="formpost" method="post">
	    <table width="100%" cellspacing="2" cellpadding="2">
	    	<tr>
	    		<td colspan="4" align="center" bgcolor="#B40404" style="color: white;"><b>ENDORSEMENT POLIS</b></td>
	    	</tr>
	    	<tr>
	    		<td colspan="4">&nbsp;</td>
	    	</tr>
	    	<tr>
	    		<td style="width: 180px"><b>Nomor Registrasi (SPAJ)</b></td>
	    		<td style="width: 10px">:</td>
	    		<td style="width: 300px"><input type="text" name="spaj" value='${spaj}' style='background-color :#D4D4D4;' readOnly></td>
	    	</tr>
	    	<tr>
	    		<td style="width: 180px"><b>Surat Endorsement</b></td>
	    		<td style="width: 10px">:</td>
	    		<td style="width: 300px">
	    			<input type="radio" name="suratendorsment" id="0" value="0" checked="checked"/><b>Endorsement Penyakit</b>
	    			<input type="radio" name="suratendorsment" id="1" value="1"/><b>Endorsement SSU</b>
	    		</td> 		
	    	</tr>
	    	<tr style="display: visible;" id="en_penyakit">
	    		<td style="width: 180px"><b>Nama Penyakit</b></td>
	    		<td style="width: 10px">:</td>
	    		<td style="width: 300px"><input type="text" name="penyakit" id="penyakit" size="70"  <c:if test="${ not empty status.errorMessage}">
	    		style='background-color :#FFE1FD'
	    		</c:if>>
	    		</td>
	    		<td><input type="submit" value="Save" name="savePenyakit" id="savePenyakit"/> </td>
	    	</tr>
	    	<tr style="display: none;" id="en_ssu_pasal">
	    		<td style="width: 180px"><b>Pasal</b></td>
	    		<td style="width: 10px">:</td>
	    		<td style="width: 300px" colspan="2"><input type="text" name="pasal" id="pasal" size="10"></td>
	    	</tr>
	    	<tr style="display: none;" id="en_ssu">
	    		<td style="width: 180px"><b>Nominal Uang Pertanggungan</b></td>
	    		<td style="width: 10px">:</td>
	    		<td style="width: 300px" colspan="2"><input type="text" name="up" id="up" size="70"></td>
	    	</tr>
	    	<tr style="display: none;" id="save_ssu">
	    		<td colspan="4" align="center"><input type="submit" value="Save" name="saveUp" id="saveUp"/></td>
	    	</tr>
	    	<tr>
	    		<td colspan="4">&nbsp;</td>
	    	</tr> 
	    	<tr>
	    		<td colspan="4" bgcolor="#B40404">&nbsp;</td>
	    	</tr>
	    	<c:if test="${not empty error}">
				<tr>
					<td colspan="4" align="center">
						<div id="error">
							${error}
						</div>	
					</td>
				</tr>
			</c:if>
	    </table>
	  </form>
  </body>
</html>
<%@ include file="/include/page/footer.jsp"%>