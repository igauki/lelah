<%@ include file="/include/page/header.jsp"%>
<body onload="setupPanes('container1', 'tab1'); document.title='PopUp :: View Nilai Tunai';" style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Nilai Tunai</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">

				<c:choose>
					<c:when test="${empty daftar}">
						<div id="error">
							${pesanError}
						</div>
					</c:when>
					<c:otherwise>
						<div id="contents">
							<display:table style="width:750px;" id="baris" name="daftar" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" 
								requestURI="${requestScope.requestURL}" export="true">
								<display:column property="tahun" title="TAHUN"  sortable="true"/>                                                                    
								<display:column property="nilai_tunai" title="NILAI TUNAI"  sortable="true" format="{0, number, #,##0.00;(#,##0.00)}" />
								<display:column property="tahapan" title="TAHAPAN"  sortable="true" format="{0, number, #,##0.00;(#,##0.00)}" />
								<display:column property="bonus" title="BONUS"  sortable="true" format="{0, number, #,##0.00;(#,##0.00)}" />
								<display:column property="saving" title="SAVING"  sortable="true" format="{0, number, #,##0.00;(#,##0.00)}" />
								<display:column property="deviden" title="DEVIDEN"  sortable="true" format="{0, number, #,##0.00;(#,##0.00)}" />
								<display:column property="maturity" title="MATURITY"  sortable="true" format="{0, number, #,##0.00;(#,##0.00)}" />
							</display:table>
						</div>
					</c:otherwise>
				</c:choose>
		
			</div>
		</div>
	</div>
	
</body>
<%@ include file="/include/page/footer.jsp"%>