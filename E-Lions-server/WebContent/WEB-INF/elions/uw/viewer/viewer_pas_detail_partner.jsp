<%@ include file="/include/page/header.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<%User currentUser = (User) session.getAttribute("currentUser");%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>

<script type="text/javascript">
	hideLoadingMessage();
	
	function agen_cek()
	{
		if (document.frmParam.elements['msag_id'].value != "")
		{
			listdataagen2(document.frmParam.elements['msag_id'].value);
		}
	}
	
	function pin_cek()
	{
		if (document.frmParam.elements['pin'].value != "")
		{
			//alert(document.frmParam.elements['pin'].value);
			cekPin(document.frmParam.elements['pin'].value);
		}
	}
	
	function update(){
		if(trim(document.frmParam.kata.value)=='') return false;
		else createLoadingMessage();	
	}
	
	function cekElseEnvir(){
		if(document.getElementById('msp_fire_insured_addr_envir').value == '5'){
			document.getElementById('msp_fire_ins_addr_envir_else').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_ins_addr_envir_else').style.visibility = "hidden";
		}
	}
	
	function cekElseSourceFund(){
		if(document.getElementById('msp_fire_source_fund2').value == 'LAINNYA'){
			document.getElementById('msp_fire_source_fund').style.visibility = "visible";
			document.getElementById('msp_fire_source_fund').value='';
		}else{
			document.getElementById('msp_fire_source_fund').style.visibility = "hidden";
		}
	}
	
	function cekElseBusiness(){
		if(document.getElementById('msp_fire_type_business2').value == 'LAINNYA'){
			document.getElementById('msp_fire_type_business').style.visibility = "visible";
			document.getElementById('msp_fire_type_business').value='';
		}else{
			document.getElementById('msp_fire_type_business').style.visibility = "hidden";
		}
	}
	
	function cekElseOccupation(){
		if(document.getElementById('msp_fire_occupation2').value == 'LAIN-LAIN'){
			document.getElementById('msp_fire_occupation').style.visibility = "visible";
			document.getElementById('msp_fire_occupation').value='';
		}else{
			document.getElementById('msp_fire_occupation').style.visibility = "hidden";
		}
	}
	
	function cekElseTrio(){
		if(document.getElementById('msp_fire_source_fund2').value == 'LAINNYA'){
			document.getElementById('msp_fire_source_fund').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_source_fund').style.visibility = "hidden";
		}
		if(document.getElementById('msp_fire_type_business2').value == 'LAINNYA'){
			document.getElementById('msp_fire_type_business').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_type_business').style.visibility = "hidden";
		}
		if(document.getElementById('msp_fire_occupation2').value == 'LAIN-LAIN'){
			document.getElementById('msp_fire_occupation').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_occupation').style.visibility = "hidden";
		}
	}
	
	function cekElseOkupasi(){
		if(document.getElementById('msp_fire_okupasi').value == 'L'){
			document.getElementById('msp_fire_okupasi_else').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_okupasi_else').style.visibility = "hidden";
		}
	}
	
	function dofo(index, flag) {
		if(flag==1){
			var a="rekening["+(index+1)+"]";
			var b="rekening["+(index)+"]";		
			document.frmParam.elements[a].focus();
			
		}else{
			var a="rekening["+(index+1)+"]";		
			document.frmParam.elements[a].focus();
		}
	}
	
	function dofo_autodebet(index, flag) {
		if(flag==1){
			var a="rekening_autodebet["+(index+1)+"]";
			var b="rekening_autodebet["+(index)+"]";		
			document.frmParam.elements[a].focus();
			
		}else{
			var a="rekening_autodebet["+(index+1)+"]";		
			document.frmParam.elements[a].focus();
		}
	}
	
	function doRek() {
		document.getElementById('msp_no_rekening').value = '';
		for(i = 0 ; i < 21 ; i++){
			document.getElementById('msp_no_rekening').value = document.getElementById('msp_no_rekening').value + document.frmParam.elements["rekening["+i+"]"].value;
		}
	}
	
	function doRek_autodebet() {
		document.getElementById('msp_no_rekening_autodebet').value = '';
		for(i = 0 ; i < 21 ; i++){
			document.getElementById('msp_no_rekening_autodebet').value = document.getElementById('msp_no_rekening_autodebet').value + document.frmParam.elements["rekening_autodebet["+i+"]"].value;
		}
	}
	
	function doPp() {
		if(document.getElementById('msp_rek_nama').value == ''){
			document.getElementById('msp_rek_nama').value = document.getElementById('nama_pp').value;
		}
		document.getElementById('msp_pas_nama_pp').value = document.getElementById('nama_pp').value;
		
	}
	
	function inPp() {
		document.getElementById('nama_pp').value = document.getElementById('msp_pas_nama_pp').value;
	}
	
	function inRek() {
		var rek = document.getElementById('msp_no_rekening').value
		var size = rek.length;
		for(i = 0 ; i < size ; i++){
			document.frmParam.elements["rekening["+i+"]"].value = rek.charAt(i);
		}
	}
	
	function inRek_autodebet() {
		var rek = document.getElementById('msp_no_rekening_autodebet').value
		var size = rek.length;
		for(i = 0 ; i < size ; i++){
			document.frmParam.elements["rekening_autodebet["+i+"]"].value = rek.charAt(i);
		}
	}
	
	function doPremi(){
		var lsdbs_number = document.getElementById('produk').value;
		var lscb_id = document.getElementById('lscb_id').value;
		var prm = 0;
		
		if(lsdbs_number == '1' && lscb_id == 3){
				document.getElementById('premi').value = '150000';
			}else if(lsdbs_number == '1' && lscb_id == 6){
				document.getElementById('premi').value = '15000';
			}else if(lsdbs_number == '1' && lscb_id == 1){
				prm = 150000 * 0.27;
				document.getElementById('premi').value = prm;
			}else if(lsdbs_number == '1' && lscb_id == 2){
				prm = 150000 * 0.52;
				document.getElementById('premi').value = prm;
			}else if(lsdbs_number == '2' && lscb_id == 3){
				document.getElementById('premi').value = '300000';
			}else if(lsdbs_number == '2' && lscb_id == 6){
				document.getElementById('premi').value = '30000';
			}else if(lsdbs_number == '2' && lscb_id == 1){
				prm = 300000 * 0.27;
				document.getElementById('premi').value = prm;
			}else if(lsdbs_number == '2' && lscb_id == 2){
				prm = 300000 * 0.52;
				document.getElementById('premi').value = prm;
			}else if(lsdbs_number == '3' && lscb_id == 3){
				document.getElementById('premi').value = '500000';
			}else if(lsdbs_number == '3' && lscb_id == 6){
				document.getElementById('premi').value = '50000';
			}else if(lsdbs_number == '3' && lscb_id == 1){
				prm = 500000 * 0.27;
				document.getElementById('premi').value = prm;
			}else if(lsdbs_number == '3' && lscb_id == 2){
				prm = 500000 * 0.52;
				document.getElementById('premi').value = prm;
			}else if(lsdbs_number == '4' && lscb_id == 3){
				document.getElementById('premi').value = '900000';
			}else if(lsdbs_number == '4' && lscb_id == 6){
				document.getElementById('premi').value = '90000';
			}else if(lsdbs_number == '4' && lscb_id == 1){
				prm = 900000 * 0.27;
				document.getElementById('premi').value = prm;
			}else if(lsdbs_number == '4' && lscb_id == 2){
				prm = 900000 * 0.52;
				document.getElementById('premi').value = prm;
			}else{
				document.getElementById('premi').value = '0';
			}
	} 
	
	function bodyOnLoad(){
		//cekElseEnvir();
		//cekElseOkupasi();
		//cekElseTrio();
		inRek();
		inRek_autodebet()
		inPp();
		doPremi();
		agen_cek();
	}
	
</script>

</head>

<body onload="resizeCenter(750,600);bodyOnLoad();" style="height: 100%;">
<XML ID=xmlData></XML>
<div class="tab-container" id="container1">

		<div class="tab-panes">

			<div id="pane1" class="panes">
<form:form commandName="cmd" name="frmParam" id="frmParam" method="post">
						<fieldset>
					  <legend>PAS</legend>
					   <table class="entry2">
					   <tr>
							<th colspan="2">Data Agen</th>
						</tr>
						<tr>
							<th align="left">kode agen:</th>
							<td>
								<form:input path="msag_id_pp" size="8" cssErrorClass="inpError" disabled="disabled"/>
							<font class="error">*</font></td>
		</tr>
					   	<tr>
							<th align="left">Tanggal Aktivasi (efektif Polis):</th>
							<td><fmt:formatDate value="${cmd.msp_pas_beg_date}" pattern="dd/MM/yyyy" /></td>
						</tr>
						<tr>
							<th colspan="2">Data Pemegang Polis</th>
						</tr>
						<tr>
							<th align="left">Nama (sesuai KTP):</th>
							<td><input type="text" name="nama_pp" size="60" id="nama_pp" onblur="doPp();" disabled="disabled"/><font class="error">*</font>
							<span style="visibility: hidden"><form:input path="msp_pas_nama_pp" cssErrorClass="inpError"/></span></td>
						</tr>
						<tr>
							<th align="left">Tempat, Tgl. lahir:</th>
							<td><form:input path="msp_pas_tmp_lhr_pp" cssErrorClass="inpError" disabled="disabled"/>
								<fmt:formatDate value="${cmd.msp_pas_dob_pp}" pattern="dd/MM/yyyy" />
							</td>
						</tr>
						<tr>
							<th align="left">Jenis Kelamin:</th>
							<td>
							<spring:bind path="cmd.msp_sex_pp">
								<label for="cowok"> <input type="radio" class=noBorder disabled="disabled"
									name="${status.expression}" value="1"
									<c:if test="${cmd.msp_sex_pp eq 1 or cmd.msp_sex_pp eq null}"> 
												checked</c:if>
									id="cowok">Pria </label>
								<label for="cewek"> <input type="radio" class=noBorder disabled="disabled"
									name="${status.expression}" value="0"
									<c:if test="${cmd.msp_sex_pp eq 0}"> 
												checked</c:if>
									id="cewek">Wanita </label>
							</spring:bind>
							<font class="error">*</font>
							</td>
						</tr>
						<tr>
							<th align="left">No Identitas:</th>
							<td><form:input path="msp_identity_no" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th align="left">Alamat:</th>
							<td><form:input path="msp_address_1" size="80" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th align="left">Kota:</th>
							<td><form:input path="msp_city" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th align="left">Kode Pos:</th>
							<td><form:input path="msp_postal_code" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th align="left">Telp:</th>
							<td><form:input path="msp_pas_phone_number" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th align="left">No. HP:</th>
							<td><form:input path="msp_mobile" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th align="left">No. HP2:</th>
							<td><form:input path="msp_mobile2" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th align="left">Alamat Email:</th>
							<td><form:input path="msp_pas_email" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th align="left">Hubungan dengan Tertanggung:</th>
							<td>
								<select name="lsre_id" onchange="cekElseEnvir();" disabled="disabled">
									<c:forEach var="rp" items="${relasi_pas}"> 
										<option <c:if test="${cmd.lsre_id eq rp.ID}"> SELECTED </c:if> value="${rp.ID}">${rp.RELATION}
										</option>
									</c:forEach> 
								</select>
							<font class="error">*</font></td>
						</tr>
						<tr>
							<th colspan="2">Data Tertanggung</th>
						</tr>
						<tr>
							<th align="left">Nama Tertanggung:</th>
							<td><form:input path="msp_full_name" cssErrorClass="inpError" size="60" disabled="disabled"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th align="left">Tempat, Tgl. lahir:</th>
							<td><form:input path="msp_pas_tmp_lhr_tt" cssErrorClass="inpError" disabled="disabled"/>
								<fmt:formatDate value="${cmd.msp_date_of_birth}" pattern="dd/MM/yyyy" />
							</td>
						</tr>
						<tr>
							<td align="left">No Identitas:</td>
							<td><form:input path="msp_identity_no_tt" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th colspan="2">Data PAS</th>
						</tr>
						<tr>
							<th align="left">Paket:</th>
							<td>
								<select name="produk" id="produk" onchange="cekElseEnvir();" disabled="disabled">
									<c:forEach var="paket_p" items="${produk_pas}"> 
										<option <c:if test="${cmd.produk eq paket_p.ID}"> SELECTED </c:if> value="${paket_p.ID}">${paket_p.PRODUK}
										</option>
									</c:forEach> 
								</select>
							</td>
						</tr>
						<tr>
							<th align="left">jumlah premi:</th>
							<td>
								<input type="text" name="premi" id="premi" disabled="disabled"/>
							</td>
						</tr>
						<tr>
							<th align="left">cara pembayaran:</th>
							<td>
								<select name="msp_flag_cc" disabled="disabled">
									<c:forEach var="ap" items="${autodebet_pas}"> 
										<option <c:if test="${cmd.msp_flag_cc eq ap.ID}"> SELECTED </c:if> value="${ap.ID}">${ap.AUTODEBET}
										</option>
									</c:forEach> 
								</select>
							<font class="error">*</font></td>
						</tr>
						<tr>
							<th align="left">bentuk pembayaran:</th>
							<td>
								<select name="lscb_id" id="lscb_id" onchange="doPremi();" disabled="disabled">
									<c:forEach var="cp" items="${carabayar_pas}"> 
										<option <c:if test="${cmd.lscb_id eq cp.ID}"> SELECTED </c:if> value="${cp.ID}">${cp.PAYMODE}
										</option>
									</c:forEach> 
								</select>
							<font class="error">*</font></td>
						</tr>
						<tr>
							<th colspan="2">Data Agen</th>
						</tr>
						<tr>
							<th align="left">kode agen:</th>
							<td>
								<spring:bind path="cmd.msag_id" >
									<input type="text" name="${status.expression}" value="${status.value }" size="8" onblur="agen_cek();" disabled="disabled"/>
								</spring:bind>
								<input type="text" name="nama_agen" id="nama_agen" size="50" disabled="disabled"/>
							<font class="error">*</font></td>
						</tr>
						<tr>
							<th align="left">cabang:</th>
							<td>
								<input type="text" name="cabang_agen" id="cabang_agen" size="30" disabled="disabled"/>
							</td>
						</tr>
						<tr>
							<th colspan="2">Data Rekening</th>
						</tr>
						<tr>
							<th align="left">bank:</th>
							<td><div id="bank1"> 
								<select name="lsbp_id" disabled="disabled">
				                  <option value="${cmd.lsbp_id}">${cmd.lsbp_nama}</option>
				                </select>
				                </div>
							</td>
						</tr>
						<tr>
							<th align="left">no rekening:</th>
							<td>
							<input type="text" name="rekening[0]" size="1"  disabled="disabled"  maxlength="1" onkeyup="dofo(0, 1);doRek();" />
							<input type="text" name="rekening[1]" size="1"  disabled="disabled"  maxlength="1" onkeyup="dofo(1, 1);doRek();" />
							<input type="text" name="rekening[2]" size="1"  disabled="disabled"  maxlength="1" onkeyup="dofo(2, 1);doRek();" />
							<input type="text" name="rekening[3]" size="1"  disabled="disabled"  maxlength="1" onkeyup="dofo(3, 1);doRek();" />
							<input type="text" name="rekening[4]" size="1"  disabled="disabled"  maxlength="1" onkeyup="dofo(4, 1);doRek();" />
							<input type="text" name="rekening[5]" size="1"  disabled="disabled"  maxlength="1" onkeyup="dofo(5, 1);doRek();" />
							<input type="text" name="rekening[6]" size="1"  disabled="disabled"  maxlength="1" onkeyup="dofo(6, 1);doRek();" />
							<input type="text" name="rekening[7]" size="1"  disabled="disabled"  maxlength="1" onkeyup="dofo(7, 1);doRek();" />
							<input type="text" name="rekening[8]" size="1"  disabled="disabled"  maxlength="1" onkeyup="dofo(8, 1);doRek();" />
							<input type="text" name="rekening[9]" size="1"  disabled="disabled"  maxlength="1" onkeyup="dofo(9, 1);doRek();" />
							<input type="text" name="rekening[10]" size="1"  disabled="disabled"  maxlength="1" onkeyup="dofo(10, 1);doRek();" />
							<input type="text" name="rekening[11]" size="1"  disabled="disabled"  maxlength="1" onkeyup="dofo(11, 1);doRek();" />
							<input type="text" name="rekening[12]" size="1"  disabled="disabled"  maxlength="1" onkeyup="dofo(12, 1);doRek();" />
							<input type="text" name="rekening[13]" size="1"  disabled="disabled"  maxlength="1" onkeyup="dofo(13, 1);doRek();" />
							<input type="text" name="rekening[14]" size="1"  disabled="disabled"  maxlength="1" onkeyup="dofo(14, 1);doRek();" />
							<input type="text" name="rekening[15]" size="1"  disabled="disabled"  maxlength="1" onkeyup="dofo(15, 1);doRek();" />
							<input type="text" name="rekening[16]" size="1"  disabled="disabled"  maxlength="1" onkeyup="dofo(16, 1);doRek();" />
							<input type="text" name="rekening[17]" size="1"  disabled="disabled"  maxlength="1" onkeyup="dofo(17, 1);doRek();" />
							<input type="text" name="rekening[18]" size="1"  disabled="disabled"  maxlength="1" onkeyup="dofo(18, 1);doRek();" />
							<input type="text" name="rekening[19]" size="1"  disabled="disabled"  maxlength="1" onkeyup="dofo(19, 1);doRek();" />
							<input type="text" name="rekening[20]" size="1"  disabled="disabled"  maxlength="1" onkeyup="dofo(20, 1);doRek();" /><font class="error">*</font>
							<span style="visibility: hidden;"><form:input path="msp_no_rekening" cssErrorClass="inpError" size="1" disabled="disabled"/></span></td>
						</tr>
						<tr>
							<th align="left">cabang:</th>
							<td><form:input path="msp_rek_cabang" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th align="left">kota:</th>
							<td><form:input path="msp_rek_kota" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th align="left">atas nama:</th>
							<td><form:input path="msp_rek_nama" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th colspan="2">Data Rekening Pemegang Polis (AUTODEBET)</th>
						</tr>
						<tr>
							<th align="left">bank:</th>
							<td><div id="bank2"> 
								<select name="lsbp_id_autodebet"  disabled="disabled">
				                  <option value="${cmd.lsbp_id_autodebet}">${cmd.lsbp_nama_autodebet}</option>
				                </select>
				                </div>
							</td>
						</tr>
						<tr>
							<th align="left">no rekening:</th>
							<td>
							<input type="text" name="rekening_autodebet[0]" size="1"   disabled="disabled"  maxlength="1" onkeyup="dofo_autodebet(0, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[1]" size="1"   disabled="disabled"  maxlength="1" onkeyup="dofo_autodebet(1, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[2]" size="1"   disabled="disabled"  maxlength="1" onkeyup="dofo_autodebet(2, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[3]" size="1"   disabled="disabled"  maxlength="1" onkeyup="dofo_autodebet(3, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[4]" size="1"   disabled="disabled"  maxlength="1" onkeyup="dofo_autodebet(4, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[5]" size="1"   disabled="disabled"  maxlength="1" onkeyup="dofo_autodebet(5, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[6]" size="1"   disabled="disabled"  maxlength="1" onkeyup="dofo_autodebet(6, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[7]" size="1"   disabled="disabled"  maxlength="1" onkeyup="dofo_autodebet(7, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[8]" size="1"   disabled="disabled"  maxlength="1" onkeyup="dofo_autodebet(8, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[9]" size="1"   disabled="disabled"  maxlength="1" onkeyup="dofo_autodebet(9, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[10]" size="1"   disabled="disabled"  maxlength="1" onkeyup="dofo_autodebet(10, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[11]" size="1"   disabled="disabled"  maxlength="1" onkeyup="dofo_autodebet(11, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[12]" size="1"   disabled="disabled"  maxlength="1" onkeyup="dofo_autodebet(12, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[13]" size="1"   disabled="disabled"  maxlength="1" onkeyup="dofo_autodebet(13, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[14]" size="1"   disabled="disabled"  maxlength="1" onkeyup="dofo_autodebet(14, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[15]" size="1"   disabled="disabled"  maxlength="1" onkeyup="dofo_autodebet(15, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[16]" size="1"   disabled="disabled"  maxlength="1" onkeyup="dofo_autodebet(16, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[17]" size="1"   disabled="disabled"  maxlength="1" onkeyup="dofo_autodebet(17, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[18]" size="1"   disabled="disabled"  maxlength="1" onkeyup="dofo_autodebet(18, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[19]" size="1"   disabled="disabled"  maxlength="1" onkeyup="dofo_autodebet(19, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[20]" size="1"   disabled="disabled"  maxlength="1" onkeyup="dofo_autodebet(20, 1);doRek_autodebet();" /><font class="error">*</font>
							<span style="visibility: hidden;"><form:input path="msp_no_rekening_autodebet" cssErrorClass="inpError" size="1"  disabled="disabled"/></span></td>
						</tr>
						<tr>
							<th align="left">atas nama:</th>
							<td><form:input path="msp_rek_nama_autodebet" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th align="left">tanggal debet:</th>
							<td>
								<fmt:formatDate value="${cmd.msp_tgl_debet}" pattern="dd/MM/yyyy" />
							<font class="error">*</font></td>
						</tr>
						<tr>
							<th align="left">tanggal valid:</th>
							<td>
								<fmt:formatDate value="${cmd.msp_tgl_valid}" pattern="dd/MM/yyyy" />
							<font class="error">*</font></td>
						</tr>
						</table></fieldset>
						<fieldset>
					   <legend>-</legend>
					   <table class="entry2">
						<tr>
							<th colspan="2" align="left">
								<font class="error"></font>
							</th>
						</tr>
						<tr>
							<th colspan="2">
					<input type="button" name="close" value="Close" onclick="window.close();"
						accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();" />
							</th>
						</tr>
					</table>
					</fieldset>
					</div>
		</div>
	</div>	
</form:form>
</body>
</html>