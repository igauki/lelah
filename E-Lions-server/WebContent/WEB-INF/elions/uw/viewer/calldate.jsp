<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/header.jsp"%>
<script>
	function awal(){
		email='${email}';
		if(email=='0'){
			alert("email cabang tidak ada harap masukan email cabang tersebut!");
		}
	}
</script>
<body onLoad="awal();">
	<form method="post" name="formpost">	
	<table class="entry2" width="100%">
			<tr>
				<th>Call Date</th>
				<th style="text-align: left">
					<script>inputDate('tanggalTerima', '${tanggalTerima}', false, '', 9);</script>
				</th>
			</tr>
	   <tr>
		<th>Waktu</th>
		<th style="text-align: left">
			<select name="hh">
				<c:forEach items="${listHH}" var="h">
					<option value="${h}" <c:if test="${h eq hh}">selected</c:if>>${h}</option>
				</c:forEach>
			</select> : 
			<select name="mm">
				<c:forEach items="${listMM}" var="m">
					<option value="${m}" <c:if test="${m eq mm}">selected</c:if>>${m}</option>
				</c:forEach>
			</select>
		</th>
	</tr>
	<tr><td colspan="3">
		<c:if test="${success ne null }">
        	<div id="success">
	        	 ${success }
	    	</div>
		</c:if>
		<c:if test="${not empty lsError}">
			<div id="error">
				 Informasi:<br>
					<c:forEach var="error" items="${lsError}">
								 - <c:out value="${error}" escapeXml="false" />
						<br/>
					</c:forEach>
			</div>
		</c:if>			
	</td></tr>	
		<tr>
			<th colspan="3">
				<input type="submit" <c:if test="${success ne null }" >disabled</c:if> name="save" value="save">
				<input type="hidden" name="flag" value="1">
			</th>
		</tr>
	</table>	
	</form>		
</body>
<%@ include file="/include/page/footer.jsp"%>