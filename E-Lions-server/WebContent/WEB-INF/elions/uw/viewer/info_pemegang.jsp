<%@ include file="/include/page/taglibs.jsp"%>

<table class="entry2">
	<c:choose>
	<c:when test="${cmd.pemegang.mcl_jenis == 1}">
	<tr>
		<th nowrap="nowrap">
			Informasi Medis
		</th>
		<th nowrap="nowrap" class="left">
			<select disabled>
				<c:forEach var="medis" items="${select_medis}">
				<option
					<c:if test="${cmd.dataUsulan.mste_medical eq medis.ID}"> SELECTED </c:if>
					value="${medis.ID}">${medis.MEDIS}</option>
				</c:forEach>
			</select>
			Nomor Seri
			<input type="text" value="${cmd.pemegang.mspo_no_blanko}" size="20" readonly>
		</th>
		<th nowrap="nowrap" class="left">
			SUMBER BISNIS
			<input type="text" value="${cmd.pemegang.nama_sumber}" size="50" readonly>
		</th>
	</tr>
	<tr> <!-- Nana Add Vip Flag Start -->
		<th nowrap="nowrap">
			Vip Flag 
		</th>
		<th nowrap="nowrap" class="left">
			<select disabled>
				<c:forEach var="data" items="${select_medis_desc}"> 
				 	<option value="${data.FLAG_ID}" 
				 		<c:if test="${data.FLAG_ID eq cmd.dataUsulan.flag_vip}">selected</c:if> >
				 					${data.LABEL_DESCRIPTION}
				 	</option> 
				</c:forEach> 
			</select>
		</th>
		<th>
		</th>
	</tr> <!-- Nana Add Vip Flag End -->
	<tr>
		<th nowrap="nowrap">
			Jenis & Nama Badan Hukum / Usaha
		</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select name="pemegang.lti_id" disabled="disabled">
				<option value=""></option>
				<c:forEach var="l" items="${gelar}">
					<option
						<c:if test="${cmd.pemegang.lti_id eq l.key}"> SELECTED </c:if>
						value="${l.key}">${l.value}</option>
				</c:forEach>
			</select>				
			<input type="text" value="${cmd.pemegang.mcl_first}" size="35" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">
			Bukti Identitas NPWP
		</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<input type="text" value="${cmd.pemegang.mspe_no_identity}" size="30" readonly> /
			<input type="text" value="<fmt:formatDate value="${cmd.personal.tgl_npwp}" pattern="dd/MM/yyyy"/>" size="12" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">
			Bukti Identitas SIUP
		</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<input type="text" value="${cmd.personal.mpt_siup}" size="30" readonly> /
			<input type="text" value="<fmt:formatDate value="${cmd.personal.tgl_siup}" pattern="dd/MM/yyyy"/>" size="12" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">
			Tempat/Tanggal Berdiri
		</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<input type="text" value="${cmd.pemegang.mspe_place_birth}" size="25" readonly> / 
			<input type="text" value="<fmt:formatDate value="${cmd.pemegang.mspe_date_birth}" pattern="dd/MM/yyyy"/>" size="12" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Alamat</th>
		<th nowrap="nowrap" class="left">
			<textarea rows="2" cols="45" readonly>${cmd.pemegang.alamat_rumah}</textarea>
		</th>
		<th nowrap="nowrap" class="left">
			Kode Pos
			<input type="text" value="${cmd.pemegang.kd_pos_rumah}" size="9" readonly>
			Kota
			<input type="text" value="${cmd.pemegang.kota_rumah}" size="20" readonly>
			<br>
			No. Telepon
			<select style="width: 191px;" >
			   
				<OPTGROUP label="Telepon">
					<c:if test="${not empty cmd.pemegang.telpon_rumah}"><option>${cmd.pemegang.area_code_rumah} - ${cmd.pemegang.telpon_rumah}</option></c:if>
					<c:if test="${not empty cmd.pemegang.telpon_rumah2}"><option>${cmd.pemegang.area_code_rumah2} - ${cmd.pemegang.telpon_rumah2}</option></c:if>
				</OPTGROUP>
				 <OPTGROUP selected label="Handphone">
					<c:if  test="${not empty cmd.pemegang.no_hp}"><option selected >${cmd.pemegang.no_hp} </option></c:if>
					<c:if test="${not empty cmd.pemegang.no_hp2}"><option>${cmd.pemegang.no_hp2}</option></c:if>										
				</OPTGROUP>
				<OPTGROUP label="Fax">
					<c:if test="${not empty cmd.pemegang.no_fax}"><option>${cmd.pemegang.area_code_fax} - ${cmd.pemegang.no_fax}</option></c:if>
				</OPTGROUP>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Alamat Penagihan</th>
		<th nowrap="nowrap" class="left">
			<textarea rows="2" cols="45" readonly>${cmd.addressbilling.msap_address}</textarea>
		</th>
		<th nowrap="nowrap" class="left">
			Kode Pos
			<input type="text" value="${cmd.addressbilling.msap_zip_code}" size="9" readonly>
			Kota
			<input type="text" value="${cmd.addressbilling.kota_tgh}" size="20" readonly>
			Negara
			<select disabled>
				<c:forEach var="negara" items="${select_negara}">
					<option
						<c:if test="${cmd.addressbilling.lsne_id eq negara.ID}"> SELECTED </c:if>
						value="${negara.ID}">${negara.NEGARA}</option>
				</c:forEach>
			</select>
			<br>
			No. Telepon
			<select style="width: 191px;" >
				<OPTGROUP label="No. Telepon">
					<c:if test="${not empty cmd.addressbilling.msap_phone1}"><option>${cmd.addressbilling.msap_area_code1} - ${cmd.addressbilling.msap_phone1}</option></c:if>
					<c:if test="${not empty cmd.addressbilling.msap_phone2}"><option>${cmd.addressbilling.msap_area_code2} - ${cmd.addressbilling.msap_phone2}</option></c:if>
					<c:if test="${not empty cmd.addressbilling.msap_phone3}"><option>${cmd.addressbilling.msap_area_code3} - ${cmd.addressbilling.msap_phone3}</option></c:if>
				</OPTGROUP>
				<OPTGROUP label="Handphone">
					<c:if test="${not empty cmd.addressbilling.no_hp}"><option>${cmd.addressbilling.no_hp}</option></c:if>
					<c:if test="${not empty cmd.addressbilling.no_hp2}"><option>${cmd.addressbilling.no_hp2}</option></c:if>
				</OPTGROUP>
				<OPTGROUP label="Fax">
					<c:if test="${not empty cmd.addressbilling.msap_fax1}"><option>${cmd.addressbilling.msap_area_code_fax1} - ${cmd.addressbilling.msap_fax1}</option></c:if>
				</OPTGROUP>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Alamat E-mail</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<input type="text" value="${cmd.addressbilling.e_mail}" size="100" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Tujuan Membeli Asuransi</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select disabled style="width: 200px;">
				<c:forEach var="tujuan" items="${select_tujuan}">
					<option
						<c:if test="${cmd.pemegang.mkl_tujuan eq tujuan.ID}"> SELECTED </c:if>
						value="${tujuan.ID}">${tujuan.TUJUAN}</option>
				</c:forEach>
			</select>
			Lainnya
			<input style="width: 311px;" type="text" value="${cmd.pemegang.tujuana}" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Sumber Pendanaan<br>Pembelian Asuransi</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select disabled style="width: 200px;">
				<c:forEach var="dana" items="${select_dana_badan_usaha}">
					<option
						<c:if test="${cmd.pemegang.mkl_pendanaan eq dana.ID}"> SELECTED </c:if>
						value="${dana.ID}">${dana.DANA}</option>
				</c:forEach>
			</select>
			Lainnya
			<input style="width: 311px;" type="text" value="${cmd.pemegang.danaa}" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Aset</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select disabled style="width: 200px;">
				<c:forEach var="aset" items="${select_aset}">
					<option
						<c:if test="${cmd.personal.mkl_aset eq aset.ID}"> SELECTED </c:if>
						value="${aset.ID}">${aset.ASET}</option>
				</c:forEach>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Perkiraan Penghasilan<br>Kotor Per Tahun</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select disabled style="width: 200px;">
				<c:forEach var="penghasilan" items="${select_penghasilan}">
					<option
						<c:if test="${cmd.pemegang.mkl_penghasilan eq penghasilan.ID}"> SELECTED </c:if>
						value="${penghasilan.ID}">${penghasilan.PENGHASILAN}</option>
				</c:forEach>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Klasifikasi Bidang Industri</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select disabled style="width: 200px;">
				<c:forEach var="industri" items="${bidangUsaha}">
					<option
						<c:if test="${cmd.pemegang.mkl_industri eq industri.value}"> SELECTED </c:if>
						value="${industri.value}">${industri.value}</option>
				</c:forEach>
			</select>
			Lainnya
			<input style="width: 311px;" type="text" value="${cmd.pemegang.industria}" readonly>
		</th>
	</tr>
		
	<tr>
		<th nowrap="nowrap">
			Hubungan Calon Tertanggung dengan<br>Calon Pemegang Polis (Badan Usaha)
		</th>
		<th nowrap="nowrap" colspan="2" class="left">
			<select disabled>
				<c:forEach var="relasi" items="${select_relasi_badan_usaha}">
					<option
						<c:if test="${cmd.pemegang.lsre_id eq relasi.ID}"> SELECTED </c:if>
						value="${relasi.ID}">${relasi.RELATION}</option>
				</c:forEach>
			</select>
		</th>
	</tr>
	<tr>
		<th nowrap="nowrap">
			PIC
		</th>
		<th nowrap="nowrap" colspan="2" class="left">
			<c:if test="${cmd.contactPerson.pic_jenis eq 1}">Penanggung jawab sesuai yang tercantum di AD</c:if>
			<c:if test="${cmd.contactPerson.pic_jenis eq 2}">Yang diberi kuasa</c:if>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">
			Nama Lengkap 
		</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select name="pemegang.lti_id" disabled="disabled">
				<option value=""></option>
				<c:forEach var="l" items="${select_gelar}">
					<option
						<c:if test="${cmd.contactPerson.lti_id eq l.ID}"> SELECTED </c:if>
						value="${l.ID}">${l.GELAR}</option>
				</c:forEach>
			</select>				
			<input type="text" value="${cmd.contactPerson.nama_lengkap}" size="35" readonly>
			Gelar
			<input type="text" value="${cmd.contactPerson.mcl_gelar}" size="12" readonly>
			Ibu Kandung
			<input type="text"	value="${cmd.contactPerson.mspe_mother}" size="35" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">
			Bukti Identitas
		</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select disabled>
				<c:forEach var="identitas" items="${select_identitas}">
					<option
						<c:if test="${cmd.contactPerson.lside_id eq identitas.ID}"> SELECTED </c:if>
						value="${identitas.ID}">${identitas.TDPENGENAL}</option>
				</c:forEach>
			</select>
			<input type="text" value="${cmd.contactPerson.no_identity}" size="30" readonly>
			Warga Negara
			<select disabled>
				<c:forEach var="negara" items="${select_negara}">
					<option
						<c:if test="${cmd.contactPerson.lsne_id eq negara.ID}"> SELECTED </c:if>
						value="${negara.ID}">${negara.NEGARA}</option>
				</c:forEach>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">
			Tempat/Tanggal Lahir
		</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<input type="text" value="${cmd.contactPerson.place_birth}" size="25" readonly> / 
			<input type="text" value="<fmt:formatDate value="${cmd.contactPerson.date_birth}" pattern="dd/MM/yyyy"/>" size="12" readonly>
			Usia
			<input type="text" value="${cmd.contactPerson.mste_age}" size="3" readonly>
			Status
			<select disabled>
				<c:forEach var="marital" items="${select_marital}">
					<option
					<c:if test="${cmd.contactPerson.sts_mrt eq marital.ID}"> SELECTED </c:if>
					value="${marital.ID}">${marital.MARITAL}</option>
				</c:forEach>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Jenis Kelamin</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<input type="text" value="<c:choose><c:when test="${cmd.contactPerson.mste_sex eq 1 or cmd.contactPerson.mste_sex eq null}">Pria</c:when><c:otherwise>Wanita</c:otherwise></c:choose>">
			Agama
			<select disabled>
				<c:forEach var="agama" items="${select_agama}">
					<option
					<c:if test="${cmd.contactPerson.lsag_id eq agama.ID}"> SELECTED </c:if>
					value="${agama.ID}">${agama.AGAMA}</option>
				</c:forEach>
			</select>
			Pendidikan
			<select disabled>
				<c:forEach var="pendidikan" items="${select_pendidikan}">
				<option
					<c:if test="${cmd.contactPerson.lsed_id eq pendidikan.ID}"> SELECTED </c:if>
					value="${pendidikan.ID}">${pendidikan.PENDIDIKAN}</option>
				</c:forEach>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Alamat Rumah</th>
		<th nowrap="nowrap" class="left">
			<textarea rows="2" cols="45" readonly>${cmd.contactPerson.alamat_rumah}</textarea>
		</th>
		<th nowrap="nowrap" class="left">
			Kode Pos
			<input type="text" value="${cmd.contactPerson.kd_pos_rumah}" size="9" readonly>
			Kota
			<input type="text" value="${cmd.contactPerson.kota_rumah}" size="20" readonly>
			<br>
			No. Telepon
			<select style="width: 191px;" >
			   
				<OPTGROUP label="Telepon Rumah">
					<c:if test="${not empty cmd.contactPerson.telpon_rumah}"><option>${cmd.contactPerson.area_code_rumah} - ${cmd.contactPerson.telpon_rumah}</option></c:if>
					<c:if test="${not empty cmd.contactPerson.telpon_rumah2}"><option>${cmd.contactPerson.area_code_rumah2} - ${cmd.contactPerson.telpon_rumah2}</option></c:if>
				</OPTGROUP>
				 <OPTGROUP selected label="Handphone">
					<c:if  test="${not empty cmd.contactPerson.no_hp}"><option selected >${cmd.contactPerson.no_hp} </option></c:if>
					<c:if test="${not empty cmd.contactPerson.no_hp2}"><option>${cmd.contactPerson.no_hp2}</option></c:if>										
				</OPTGROUP>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Alamat Kantor</th>
		<th nowrap="nowrap" class="left">
			<textarea rows="2" cols="45" readonly>${cmd.contactPerson.alamat_kantor}</textarea>
		</th>
		<th nowrap="nowrap" class="left">
			Kode Pos
			<input type="text" value="${cmd.contactPerson.kd_pos_kantor}" size="9" readonly>
			Kota
			<input type="text" value="${cmd.contactPerson.kota_kantor}" size="20" readonly>
			<br>
			No. Telepon
			<select style="width: 191px;">
				<OPTGROUP label="Telepon Kantor">
					<c:if test="${not empty cmd.contactPerson.telpon_kantor}"><option>${cmd.contactPerson.area_code_kantor} - ${cmd.contactPerson.telpon_kantor}</option></c:if>
					<c:if test="${not empty cmd.contactPerson.telpon_kantor2}"><option>${cmd.contactPerson.area_code_kantor2} - ${cmd.contactPerson.telpon_kantor2}</option></c:if>
				</OPTGROUP>
				<OPTGROUP label="Fax">
					<c:if test="${not empty cmd.contactPerson.no_fax}"><option>${cmd.contactPerson.area_code_fax} - ${cmd.contactPerson.no_fax}</option></c:if>
				</OPTGROUP>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Alamat E-mail</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<input type="text" value="${cmd.contactPerson.email}" size="100" readonly>
		</th>
	</tr>
	
	</c:when>
	<c:otherwise>
	<tr>
		<th nowrap="nowrap">
			Informasi Medis
		</th>
		<th nowrap="nowrap" class="left">
			<select disabled>
				<c:forEach var="medis" items="${select_medis}">
				<option
					<c:if test="${cmd.dataUsulan.mste_medical eq medis.ID}"> SELECTED </c:if>
					value="${medis.ID}">${medis.MEDIS}</option>
				</c:forEach>
			</select>
			Nomor Seri
			<input type="text" value="${cmd.pemegang.mspo_no_blanko}" size="20" readonly>
		</th>
		<th nowrap="nowrap" class="left">
			SUMBER BISNIS
			<input type="text" value="${cmd.pemegang.nama_sumber}" size="50" readonly>
		</th>
	</tr>
	<tr> <!-- Nana Add Vip Flag Start -->
		<th nowrap="nowrap">
			Vip Flag 
		</th>
		<th nowrap="nowrap" class="left">
			<select disabled>
				<c:forEach var="data" items="${select_medis_desc}"> 
				 	<option value="${data.FLAG_ID}" 
				 		<c:if test="${data.FLAG_ID eq cmd.dataUsulan.flag_vip}">selected</c:if> >
				 					${data.LABEL_DESCRIPTION}
				 	</option> 
				</c:forEach> 
			</select>
		</th>
		<th nowrap="nowrap" class="left"> <!-- Nana Add Full Auto Sales Start -->
			Full Auto Sales 
			<select disabled>
				<c:forEach var="data" items="${select_full_autosales}"> 
				 	<option value="${data.FLAG_ID}" 
				 		<c:if test="${data.FLAG_ID eq cmd.dataUsulan.full_autosales}">selected</c:if> >
				 					${data.LABEL_DESCRIPTION}
				 	</option> 
				</c:forEach> 
			</select>  
		</th> <!-- Nana Add Full Auto Sales End -->
	</tr> <!-- Nana Add Vip Flag End -->
	<tr>
		<th nowrap="nowrap">Nomor Kartu</th>
		<th nowrap="nowrap" class="left">
			<input type="text" value="${cmd.pemegang.mspo_nasabah_acc}" size="35" readonly>
			No.Virtual Account		
			<input type="text" value="${cmd.tertanggung.mste_no_vacc}" size="25" readonly>
		</th>
		<th nowrap="nowrap" class="left">Jenis Form SPAJ	
			<c:choose>
				<c:when test="${jenisSpaj != null}">
					<select name="chooseJenisSpaj" disabled> 
						<option selected="selected">${jenisSpaj}</option>
					</select>	
				</c:when>
				<c:otherwise>
					<select name="pemegang.mspo_flag_spaj" disabled> 
						<option value="0" <c:if test="${cmd.pemegang.mspo_flag_spaj eq 0}">selected="selected"</c:if>>SPAJ LAMA</option>
						<option value="1" <c:if test="${cmd.pemegang.mspo_flag_spaj eq 1}">selected="selected"</c:if>>SPAJ BARU</option>
						<option value="2" <c:if test="${cmd.pemegang.mspo_flag_spaj eq 2}">selected="selected"</c:if>>SPAJ BARU</option>
						<option value="4" <c:if test="${cmd.pemegang.mspo_flag_spaj eq 4}">selected="selected"</c:if>>SPAJ SIO</option>
						<option value="5" <c:if test="${cmd.pemegang.mspo_flag_spaj eq 5}">selected="selected"</c:if>>SPAJ GIO</option>
						<option value="3" <c:if test="${cmd.pemegang.mspo_flag_spaj eq 3}">selected="selected"</c:if>>SPAJ FULL MARET 2015</option>
					</select>
				</c:otherwise>
			</c:choose>
		</th>				
	</tr>
	
	<tr>
		<th nowrap="nowrap">
			Nama Lengkap
		</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select name="pemegang.lti_id" disabled="disabled">
				<option value=""></option>
				<c:forEach var="l" items="${select_gelar}">
					<option
						<c:if test="${cmd.pemegang.lti_id eq l.ID}"> SELECTED </c:if>
						value="${l.ID}">${l.GELAR}</option>
				</c:forEach>
			</select>				
			<input type="text" value="${cmd.pemegang.mcl_first}" size="35" readonly>
			Gelar
			<input type="text" value="${cmd.pemegang.mcl_gelar}" size="12" readonly>
			Ibu Kandung
			<input type="text"	value="${cmd.pemegang.mspe_mother}" size="35" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">
			Bukti Identitas
		</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select disabled>
				<c:forEach var="identitas" items="${select_identitas}">
					<option
						<c:if test="${cmd.pemegang.lside_id eq identitas.ID}"> SELECTED </c:if>
						value="${identitas.ID}">${identitas.TDPENGENAL}</option>
				</c:forEach>
			</select>
			<input type="text" value="${cmd.pemegang.mspe_no_identity}" size="30" readonly>
			Warga Negara
			<select disabled>
				<c:forEach var="negara" items="${select_negara}">
					<option
						<c:if test="${cmd.pemegang.lsne_id eq negara.ID}"> SELECTED </c:if>
						value="${negara.ID}">${negara.NEGARA}</option>
				</c:forEach>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">
			Tempat/Tanggal Lahir
		</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<input type="text" value="${cmd.pemegang.mspe_place_birth}" size="25" readonly> / 
			<input type="text" value="<fmt:formatDate value="${cmd.pemegang.mspe_date_birth}" pattern="dd/MM/yyyy"/>" size="12" readonly>
			Usia
			<input type="text" value="${cmd.pemegang.mste_age}" size="3" readonly>
			Status
			<select disabled>
				<c:forEach var="marital" items="${select_marital}">
					<option
					<c:if test="${cmd.pemegang.mspe_sts_mrt eq marital.ID}"> SELECTED </c:if>
					value="${marital.ID}">${marital.MARITAL}</option>
				</c:forEach>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Jenis Kelamin</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<input type="text" value="<c:choose><c:when test="${cmd.pemegang.mspe_sex eq 1 or cmd.pemegang.mspe_sex eq null}">Pria</c:when><c:otherwise>Wanita</c:otherwise></c:choose>">
			Agama
			<select disabled>
				<c:forEach var="agama" items="${select_agama}">
					<option
					<c:if test="${cmd.pemegang.lsag_id eq agama.ID}"> SELECTED </c:if>
					value="${agama.ID}">${agama.AGAMA}</option>
				</c:forEach>
			</select>
			Pendidikan
			<select disabled>
				<c:forEach var="pendidikan" items="${select_pendidikan}">
				<option
					<c:if test="${cmd.pemegang.lsed_id eq pendidikan.ID}"> SELECTED </c:if>
					value="${pendidikan.ID}">${pendidikan.PENDIDIKAN}</option>
				</c:forEach>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Alamat Rumah</th>
		<th nowrap="nowrap" class="left">
			<textarea rows="2" cols="45" readonly>${cmd.pemegang.alamat_rumah}</textarea>
		</th>
		<th nowrap="nowrap" class="left">
			Kode Pos
			<input type="text" value="${cmd.pemegang.kd_pos_rumah}" size="9" readonly>
			Kota
			<input type="text" value="${cmd.pemegang.kota_rumah}" size="20" readonly>
			<br>
			No. Telepon
			<select style="width: 191px;" >
			   
				<OPTGROUP label="Telepon Rumah">
					<c:if test="${not empty cmd.pemegang.telpon_rumah}"><option>${cmd.pemegang.area_code_rumah} - ${cmd.pemegang.telpon_rumah}</option></c:if>
					<c:if test="${not empty cmd.pemegang.telpon_rumah2}"><option>${cmd.pemegang.area_code_rumah2} - ${cmd.pemegang.telpon_rumah2}</option></c:if>
				</OPTGROUP>
				 <OPTGROUP selected label="Handphone">
					<c:if  test="${not empty cmd.pemegang.no_hp}"><option selected >${cmd.pemegang.no_hp} </option></c:if>
					<c:if test="${not empty cmd.pemegang.no_hp2}"><option>${cmd.pemegang.no_hp2}</option></c:if>										
				</OPTGROUP>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Alamat Kantor</th>
		<th nowrap="nowrap" class="left">
			<textarea rows="2" cols="45" readonly>${cmd.pemegang.alamat_kantor}</textarea>
		</th>
		<th nowrap="nowrap" class="left">
			Kode Pos
			<input type="text" value="${cmd.pemegang.kd_pos_kantor}" size="9" readonly>
			Kota
			<input type="text" value="${cmd.pemegang.kota_kantor}" size="20" readonly>
			<br>
			No. Telepon
			<select style="width: 191px;">
				<OPTGROUP label="Telepon Kantor">
					<c:if test="${not empty cmd.pemegang.telpon_kantor}"><option>${cmd.pemegang.area_code_kantor} - ${cmd.pemegang.telpon_kantor}</option></c:if>
					<c:if test="${not empty cmd.pemegang.telpon_kantor2}"><option>${cmd.pemegang.area_code_kantor2} - ${cmd.pemegang.telpon_kantor2}</option></c:if>
				</OPTGROUP>
				<OPTGROUP label="Fax">
					<c:if test="${not empty cmd.pemegang.no_fax}"><option>${cmd.pemegang.area_code_fax} - ${cmd.pemegang.no_fax}</option></c:if>
				</OPTGROUP>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Alamat Penagihan</th>
		<th nowrap="nowrap" class="left">
			<textarea rows="2" cols="45" readonly>${cmd.addressbilling.msap_address}</textarea>
		</th>
		<th nowrap="nowrap" class="left">
			Kode Pos
			<input type="text" value="${cmd.addressbilling.msap_zip_code}" size="9" readonly>
			Kota
			<input type="text" value="${cmd.addressbilling.kota_tgh}" size="20" readonly>
			Negara
			<select disabled>
				<c:forEach var="negara" items="${select_negara}">
					<option
						<c:if test="${cmd.addressbilling.lsne_id eq negara.ID}"> SELECTED </c:if>
						value="${negara.ID}">${negara.NEGARA}</option>
				</c:forEach>
			</select>
			<br>
			No. Telepon
			<select style="width: 191px;" >
				<OPTGROUP label="No. Telepon">
					<c:if test="${not empty cmd.addressbilling.msap_phone1}"><option>${cmd.addressbilling.msap_area_code1} - ${cmd.addressbilling.msap_phone1}</option></c:if>
					<c:if test="${not empty cmd.addressbilling.msap_phone2}"><option>${cmd.addressbilling.msap_area_code2} - ${cmd.addressbilling.msap_phone2}</option></c:if>
					<c:if test="${not empty cmd.addressbilling.msap_phone3}"><option>${cmd.addressbilling.msap_area_code3} - ${cmd.addressbilling.msap_phone3}</option></c:if>
				</OPTGROUP>
				<OPTGROUP label="Handphone">
					<c:if test="${not empty cmd.addressbilling.no_hp}"><option>${cmd.addressbilling.no_hp}</option></c:if>
					<c:if test="${not empty cmd.addressbilling.no_hp2}"><option>${cmd.addressbilling.no_hp2}</option></c:if>
				</OPTGROUP>
				<OPTGROUP label="Fax">
					<c:if test="${not empty cmd.addressbilling.msap_fax1}"><option>${cmd.addressbilling.msap_area_code_fax1} - ${cmd.addressbilling.msap_fax1}</option></c:if>
				</OPTGROUP>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Alamat E-mail</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<input type="text" value="${cmd.addressbilling.e_mail}" size="100" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Tujuan Membeli Asuransi</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<%-- <select disabled style="width: 200px;">
				<c:forEach var="tujuan" items="${select_tujuan}">
					<option
						<c:if test="${cmd.pemegang.mkl_tujuan eq tujuan.ID}"> SELECTED </c:if>
						value="${tujuan.ID}">${tujuan.TUJUAN}</option>
				</c:forEach>
			</select>
			Lainnya --%>
			<input style="width: 311px;" type="text" value="${cmd.pemegang.tujuana}" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Perkiraan Penghasilan<br>Kotor Per Tahun</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select disabled style="width: 200px;">
				<c:forEach var="penghasilan" items="${select_penghasilan}">
					<option
						<c:if test="${cmd.pemegang.mkl_penghasilan eq penghasilan.ID}"> SELECTED </c:if>
						value="${penghasilan.ID}">${penghasilan.PENGHASILAN}</option>
				</c:forEach>
			</select>
			Jabatan
			<input style="width: 309px;" tipe="text" value="${cmd.pemegang.kerjab}" readonly>
		</th>
	</tr>
	<c:choose>
	<c:when test="${cmd.pemegang.mspo_flag_spaj < 2}">
	<tr>
		<th nowrap="nowrap">Sumber Pendanaan Pembelian Asuransi</th>
		<th nowrap="nowrap" class="left" colspan="2">
				<spring:bind path="cmd.pemegang.mkl_dana_from">
				<label for="sendiri2">
					<input type="radio" class=noBorder
						name="${status.expression}" value="0"
							<c:if test="${cmd.pemegang.mkl_dana_from eq 0 or cmd.pemegang.mkl_dana_from eq null}"> 
								  checked
							</c:if>
								id="sendiri2" disabled>Diri Sendiri
				</label>
				<label for="gaksendiri2">
					<input type="radio" class=noBorder
							 name="${status.expression}" value="1"
								<c:if test="${cmd.pemegang.mkl_dana_from eq 1}"> 
									checked
								</c:if>
								id="gaksendiri2" disabled>Bukan Diri Sendiri (Pembayar Premi bukan Pemegang Polis)
				</label>
				</spring:bind>
					<input type="hidden" name="tanda_pp" value='${cmd.pemegang.mkl_dana_from}'>
		</th>
	</tr>
	<tr>
		<th>&nbsp;</th>
		<th id ="mklSumberPremi" nowrap="nowrap" class="left">
			<font color="#CC3300">Pembayar Premi Adalah :</font>
				<spring:bind path="cmd.pemegang.mkl_sumber_premi">
					<label for="badan">
					<input type="radio" class="noBorder" name="${status.expression}" value="0"
						<c:if test="${cmd.pemegang.mkl_sumber_premi eq 0 or cmd.pemegang.mkl_sumber_premi eq null}"> 
							checked
						</c:if>
							id="badan" disabled>Badan Hukum/Usaha
					</label>
					<label for="orang">
					<input type="radio" class="noBorder" name="${status.expression}" value="1"
						<c:if test="${cmd.pemegang.mkl_sumber_premi eq 1}"> 
							checked
						</c:if>
							id="orang" disabled>Perorangan
					</label>
				</spring:bind>
					<input type="hidden" name="tanda_hub" value='${cmd.pemegang.mkl_sumber_premi}'>
		</th>
		<th colspan="2" id="lsreIdPremi" nowrap="nowrap" class="left">
			<select name="pemegang.lsre_id_premi" disabled style="width: 200px;">
				<c:forEach var="pre" items="${select_relasi_premi}">
					<option
						<c:if test="${cmd.pemegang.lsre_id_premi eq pre.ID}"> SELECTED </c:if>
						value="${pre.ID}">${pre.RELATION}
					</option>
				</c:forEach>
			</select>
		</th>
	</tr>
	<tr>
		<th nowrap="nowrap"></th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select name="pemegang.mkl_pendanaan" disabled style="width: 200px;">
<!-- 			x -->
				<c:forEach var="dana" items="${select_dana}">
					<option
						<c:if test="${cmd.pemegang.mkl_pendanaan eq dana.ID}"> SELECTED </c:if>
						value="${dana.ID}">${dana.DANA}
					</option>
				</c:forEach>
			</select>
				Lainnya
			<input style="width: 311px;" type="text" value="${cmd.pemegang.danaa}" readonly>
		</th>
	</tr>
	<c:forEach var="listkyc" items="${cmd.pemegang.daftarKyc}" varStatus="status">
	<tr>
		<th nowrap="nowrap">&nbsp;</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select name="listkyc.kyc_desc1${status.index +1}" disabled style="width: 200px;">
				<c:forEach var="sdana" items="${select_hasil}">
					<option 
						<c:if test="${listkyc.kyc_desc1 eq sdana.ID}"> SELECTED </c:if>
						value="${sdana.ID}">${sdana.DANA}
					</option>
				</c:forEach>
			</select>
			Lainnya
			<input type="text" name='listkyc.kyc_desc_x${status.index +1}' value ='${listkyc.kyc_desc_x}' size="42" maxlength="60">
		</th>
	</tr> 		
	</c:forEach>
	<tr>
		<th nowrap="nowrap">Sumber Penghasilan</th>
		<th nowrap="nowrap" class="left" colspan="2">
				<spring:bind path="cmd.pemegang.mkl_hasil_from">
				<label for="sendiri2">
					<input type="radio" class=noBorder
						name="${status.expression}" value="0"
							<c:if test="${cmd.pemegang.mkl_hasil_from eq 0 or cmd.pemegang.mkl_hasil_from eq null}"> 
								  checked
							</c:if>
								id="sendiri2" disabled>Diri Sendiri
				</label>
				<label for="gaksendiri2">
					<input type="radio" class=noBorder
							 name="${status.expression}" value="1"
								<c:if test="${cmd.pemegang.mkl_hasil_from eq 1}"> 
									checked
								</c:if>
								id="gaksendiri2" disabled>Bukan Diri Sendiri (Pembayar Premi bukan Pemegang Polis)
				</label>
				</spring:bind>
					<input type="hidden" name="tanda_pp" value='${cmd.pemegang.mkl_hasil_from}'>
		</th>
	</tr>
	<tr>
		<th nowrap="nowrap">&nbsp;</th>    
		<th nowrap="nowrap" class="left" colspan="2">
			<select name="pemegang.mkl_smbr_penghasilan" disabled style="width: 200px;">
				<c:forEach var="hasil" items="${select_hasil}">
					<option
						<c:if test="${cmd.pemegang.mkl_smbr_penghasilan eq hasil.ID}"> SELECTED </c:if>
						value="${hasil.ID}">${hasil.DANA}
					</option>
				</c:forEach>
			</select>
				Lainnya
			<input style="width: 311px;" type="text" value="${cmd.pemegang.shasil}" readonly>
		</th>
	</tr>
	<c:forEach var="listkyc2" items="${cmd.pemegang.daftarKyc2}" varStatus="status">
	<tr>
		<th nowrap="nowrap">&nbsp;</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select name="listkyc2.kyc_desc2${status.index +1}" disabled style="width: 200px;">
				<c:forEach var="sdana" items="${select_hasil}">
					<option 
						<c:if test="${listkyc2.kyc_desc2 eq sdana.ID}"> SELECTED </c:if>
							value="${sdana.ID}">${sdana.DANA}
					</option>
				</c:forEach>
			</select>
			Lainnya
			<input type="text" name='listkyc2.kyc_desc2_x${status.index +1}' value ='${listkyc2.kyc_desc2_x}' size="42" maxlength="60">
		</th>
	</tr> 		
	</c:forEach>
	<tr>
		<th nowrap="nowrap">Klasifikasi Pekerjaan</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<input type="text" style="width: 500px;"  value="${cmd.pemegang.mkl_kerja} " readonly="readonly">
			Lainnya
			<input style="width: 311px;" type="text" value="${cmd.pemegang.kerjaa}" readonly>
		</th>
	</tr>
<!-- 	jabatanxxx -->
	<tr>
		<th nowrap="nowrap">Jabatan</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<input style="width: 311px;" type="text" value="${cmd.pemegang.kerjab}" readonly>
		</th>
	</tr>
	<tr>
		<th nowrap="nowrap">Klasifikasi Bidang Industri</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select disabled style="width: 200px;">
				<c:forEach var="industri" items="${select_industri}">
					<option
						<c:if test="${cmd.pemegang.mkl_industri eq industri.ID}"> SELECTED </c:if>
						value="${industri.ID}">${industri.BIDANG}</option>
				</c:forEach>
			</select>
			Lainnya
			<input style="width: 311px;" type="text" value="${cmd.pemegang.industria}" readonly>
		</th>
	</tr>
	</c:when>
	<c:otherwise>
	<tr>
		<th nowrap="nowrap">Sumber Pendanaan</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<%-- <select disabled style="width: 200px;">
				<c:forEach var="tujuan" items="${select_tujuan}">
					<option
						<c:if test="${cmd.pemegang.mkl_tujuan eq tujuan.ID}"> SELECTED </c:if>
						value="${tujuan.ID}">${tujuan.TUJUAN}</option>
				</c:forEach>
			</select>
			Lainnya --%>
			<input style="width: 311px;" type="text" value="${cmd.pemegang.mkl_pendanaan}" readonly>
		</th>
	</tr>
	<%-- <tr>
		<th nowrap="nowrap">
			Klasifikasi Pekerjaan
		</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select disabled>
				<c:forEach var="mkl_kerja" items="${select_lst_pekerjaan}">
					<option
						<c:if test="${cmd.pemegang.mkl_kerja eq mkl_kerja.key}"> SELECTED </c:if>
						value="${mkl_kerja.key}">${mkl_kerja.value}</option>
				</c:forEach>
			</select>
		</th>
	</tr> --%>
		<tr>
		<th nowrap="nowrap">Klasifikasi Pekerjaan</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<input type="text" style="width: 500px;"  value="${cmd.pemegang.mkl_kerja} " readonly="readonly">
			Lainnya
			<input style="width: 311px;" type="text" value="${cmd.pemegang.kerjaa}" readonly>
		</th>
	</tr>
   </c:otherwise>
</c:choose>
	<tr>
		<th nowrap="nowrap">No.Resi Pengiriman</th>
		<th nowrap="nowrap" class="left" colspan="2">			
			<input  type="text" value="${cmd.pemegang.mspo_no_pengiriman}" size="50" readonly="readonly">
			Receiver
			<input  type="text" value="${cmd.receiver_polis}" size="20" readonly="readonly">
			Tgl.Terima
			<input  type="text" value="${cmd.tgl_receiver}" size="20" readonly="readonly">
		</th>
	</tr>
	<tr>
		<th nowrap="nowrap" colspan="3">
			Hubungan Calon Pemegang Polis dengan Calon Tertanggung 
			<select disabled>
				<c:forEach var="relasi" items="${select_relasi}">
					<option
						<c:if test="${cmd.pemegang.lsre_id eq relasi.ID}"> SELECTED </c:if>
						value="${relasi.ID}">${relasi.RELATION}</option>
				</c:forEach>
			</select>
		</th>
	</tr>
	</c:otherwise>
	</c:choose>
	
</table>
</body>