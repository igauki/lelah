<%@ include file="/include/page/header.jsp"%>
<body onload="document.title='PopUp :: Rekening Nasabah';setupPanes('container1','tab1');" style="height: 100%;">
	<c:if test="${not empty cmd.rekening.lsbp_id}">
		<script type="text/javascript">
			//ajaxPjngRek('${cmd.rekening.lsbp_id}', 1,'mrc_no_ac_split');
		</script>	
	</c:if>
	
	<script type="text/javascript">
		function dofo(index, flag) {
			
			if(flag==1){
				var a="mrc_no_ac_split["+(index+1)+"]";		
				document.frmParam.elements[a].focus();
			}else{
				var a="account_recur.mar_acc_no_split["+(index+1)+"]";		
				document.frmParam.elements[a].focus();
			}
				
		}
		
		function setRek(index, flag,element) {
			var jenisRecur=document.getElementById('mrc_jenis').value ;
			if(jenisRecur==1){
				ajaxPjngRek(index, flag,element);
			}else{
				for(var i=0;i<16;i++){	
					document.frmParam.elements[element+'['+i+']'].readOnly = false;
					document.frmParam.elements[element+'['+i+']'].style.backgroundColor ='#FFFFFF';
				}
			}
		}
	</script>
	
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Perubahan Rekening</a>
			</li>
			<li>
				<a href="#" onClick="return showPane('pane2', this)">History Perubahan</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="frmParam">
				    <table class="entry2">
				      <tr> 
				        <th nowrap="nowrap">Rekening Billing<br><span style="color: red;">(Billing Account)</span></th>
				        <td colspan="3">
							<input class="readOnly" type="text" readonly="readonly" value="${cmd.virtual_account}" style="width: 336px;">
				        </td>        
				      </tr>
				      <tr> 
				        <th nowrap="nowrap">Bank <span style="color: red;">*</span></th>
				        <td colspan="3">
				        	<input type="hidden" name="spaj" value="${param.spaj}">
							<select name="lsbp_id" style="width: 338px;" tabindex="1" onchange="setRek(this.value, 1,'mrc_no_ac_split')">
								<option value=""></option>
								<c:forEach var="s" items="${cmd.bang}">
									<option value="${s.LSBP_ID}"
										<c:if test="${s.LSBP_ID eq cmd.rekening.lsbp_id}">selected</c:if>>${s.LSBP_NAMA}</option>
								</c:forEach>
							</select>        
				        </td>        
				      </tr>
				      <tr>
				      	<th nowrap="nowrap">No. Rekening <span style="color: red;">*</span></th>
				      	<td colspan="3">
				      		<%--NO REKENING DI SPLIT --%>
				      		<input type="text" name="mrc_no_ac_split[0]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(0, 1);"  value="${cmd.rekening.mrc_no_ac_split[0]}" tabindex="2">
							<input type="text" name="mrc_no_ac_split[1]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(1, 1);"  value="${cmd.rekening.mrc_no_ac_split[1]}"  tabindex="3">
							<input type="text" name="mrc_no_ac_split[2]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(2, 1);"  value="${cmd.rekening.mrc_no_ac_split[2]}"  tabindex="4">
							<input type="text" name="mrc_no_ac_split[3]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(3, 1);"  value="${cmd.rekening.mrc_no_ac_split[3]}"  tabindex="5">
							<input type="text" name="mrc_no_ac_split[4]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(4, 1);"  value="${cmd.rekening.mrc_no_ac_split[4]}"  tabindex="6">
							<input type="text" name="mrc_no_ac_split[5]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(5, 1);"  value="${cmd.rekening.mrc_no_ac_split[5]}"  tabindex="7">
							<input type="text" name="mrc_no_ac_split[6]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(6, 1);"  value="${cmd.rekening.mrc_no_ac_split[6]}"  tabindex="8">
							<input type="text" name="mrc_no_ac_split[7]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(7, 1);"  value="${cmd.rekening.mrc_no_ac_split[7]}"  tabindex="9">
							<input type="text" name="mrc_no_ac_split[8]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(8, 1);"  value="${cmd.rekening.mrc_no_ac_split[8]}"  tabindex="10">
							<input type="text" name="mrc_no_ac_split[9]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(9, 1);"  value="${cmd.rekening.mrc_no_ac_split[9]}"  tabindex="11">
							<input type="text" name="mrc_no_ac_split[10]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(10, 1);"  value="${cmd.rekening.mrc_no_ac_split[10]}" tabindex="12">
							<input type="text" name="mrc_no_ac_split[11]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(11, 1);"  value="${cmd.rekening.mrc_no_ac_split[11]}"  tabindex="13">
							<input type="text" name="mrc_no_ac_split[12]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(12, 1);"  value="${cmd.rekening.mrc_no_ac_split[12]}"  tabindex="14">
							<input type="text" name="mrc_no_ac_split[13]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(13, 1);"  value="${cmd.rekening.mrc_no_ac_split[13]}" tabindex="15">
							<input type="text" name="mrc_no_ac_split[14]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(14, 1);"  value="${cmd.rekening.mrc_no_ac_split[14]}" tabindex="16">
							<input type="text" name="mrc_no_ac_split[15]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(15, 1);"  value="${cmd.rekening.mrc_no_ac_split[15]}" tabindex="17">
							<input type="text" name="mrc_no_ac_split[16]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(16, 1);"  value="${cmd.rekening.mrc_no_ac_split[16]}"  tabindex="18">
							<input type="text" name="mrc_no_ac_split[17]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(17, 1);"  value="${cmd.rekening.mrc_no_ac_split[17]}"  tabindex="19">
							<input type="text" name="mrc_no_ac_split[18]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(18, 1);"  value="${cmd.rekening.mrc_no_ac_split[18]}"  tabindex="20">
							<input type="text" name="mrc_no_ac_split[19]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(19, 1);" value="${cmd.rekening.mrc_no_ac_split[19]}"  tabindex="21">
							<input type="text" name="mrc_no_ac_split[20]" size="1"  maxlength="1" onfocus="select();"  value="${cmd.rekening.mrc_no_ac_split[20]}"  tabindex="21">
				      		
				      		<input type="hidden" name="mrc_no_ac" value="${cmd.rekening.mrc_no_ac}" style="width: 282px;">
				      		<input type="hidden" name="mrc_no_ac_lama" value="${cmd.rekening.mrc_no_ac_lama}">
							    <select name="mrc_kurs"  tabindex="22">
				                <c:forEach var="kurs" items="${cmd.select_kurs}"> <option 
											<c:if test="${cmd.rekening.mrc_kurs eq kurs.ID}"> SELECTED </c:if>
											value="${kurs.ID}">${kurs.SYMBOL}</option> 
				                </c:forEach> 
				               	</select>
				      	</td>
				      </tr>
				      <tr>
				      	<th nowrap="nowrap">Atas Nama <span style="color: red;">*</span></th>
				      	<td colspan="3"><input type="text" name="mrc_nama" value="${cmd.rekening.mrc_nama}"  tabindex="23" style="width: 336px;"></td>
				      </tr>
				      <tr>
				      	<th nowrap="nowrap">Cabang</th>
				      	<td><input type="text" name="mrc_cabang" value="${cmd.rekening.mrc_cabang}" size="20"  tabindex="24"></td>
				      	<th nowrap="nowrap">Kota</th>
				      	<td><input type="text" name="mrc_kota" value="${cmd.rekening.mrc_kota}" size="19"  tabindex="25"></td>
				      </tr>
				      <tr>
				      	<th nowrap="nowrap">Jenis Tabungan</th>
				      	<td>
							<select name="mrc_jenis"  style="width: 114px;"  tabindex="26">
				                <c:forEach var="tab" items="${cmd.select_jenis_tabungan}"> <option 
											<c:if test="${cmd.rekening.mrc_jenis eq tab.ID}"> SELECTED </c:if>
											value="${tab.ID}">${tab.AUTODEBET}</option> </c:forEach> 
				            </select>      	
				      	</td>
				      	<th nowrap="nowrap">Jenis Nasabah</th>
				      	<td>
							<select <c:if test="${empty enableJenisTabungan}">disabled="disabled"</c:if> name="mrc_jn_nasabah" tabindex="27" style="width: 108px;">
				                <c:forEach var="nsbh" items="${cmd.select_jenis_nasabah}"> <option 
									<c:if test="${cmd.rekening.mrc_jn_nasabah eq nsbh.ID}"> SELECTED </c:if>
									value="${nsbh.ID}">${nsbh.JENIS}</option> </c:forEach>
							</select>
				      	</td>
				      </tr>
				      <tr>
				      	<th nowrap="nowrap">Surat Kuasa <span style="color: red;">*</span></th>
				      	<td colspan="3">
				      		<input type="checkbox" name="mrc_kuasa" value="1" class="noBorder"  tabindex="28" onclick="enableDisableDate('tgl_surat');"
				      			<c:choose>
					      			<c:when test="${cmd.rekening.mrc_kuasa eq 1}">
					      				checked>
								      	<script>inputDate('tgl_surat', '<fmt:formatDate value="${cmd.sysDate}" pattern="dd/MM/yyyy"/>', false);</script>
					      			</c:when>
					      			<c:otherwise>
					      				>
								      	<script>inputDate('tgl_surat', '<fmt:formatDate value="${cmd.sysDate}" pattern="dd/MM/yyyy"/>', true);</script>
					      			</c:otherwise>
				      			</c:choose>
				      	</td>
				      </tr>
				      <tr>
				      	<th nowrap="nowrap">Keterangan <span style="color: red;">*</span></th>
				      	<td colspan="3">
							<textarea style="width: 336px;" cols="60" rows="2" name="keterangan" onkeyup="textCounter(this, 60); " onkeydown="textCounter(this, 60); "  tabindex="29">${cmd.rekening.keterangan}</textarea>
				      	</td>
				      </tr>
				      <tr>
				      	<th nowrap="nowrap">Nomor Rekening Yang Digunakan<span style="color: red;">*</span></th>
				      	<td colspan="3">
							<c:if test="${cmd.rekening.mrc_pp eq 0}"> BUKAN A/n Pemegang Polis</c:if> <c:if test="${cmd.rekening.mrc_pp eq 1}">A/n Pemegang Polis </c:if>
				      	</td>
				      </tr>
				      <tr>
				      	<th nowrap="nowrap"></th>
				      	<td colspan="3">
				      	
							<c:if test="${empty cmd.notUnderWriting}">
							    <input type="submit" name="save" value="Save" onclick="return confirm('Simpan perubahan?');"
									accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();"  tabindex="30">
							</c:if>								
								
						    <input type="button" name="close" value="Close" onclick="window.close();"
								accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();"  tabindex="31">
						    <c:if test="${not empty cmd.err }">
								<div id="error" style="width: 336px;">
								ERROR:<br>
								<c:forEach var="e" items="${cmd.err}">
									- ${e.defaultMessage}<br>
								</c:forEach> 
								</div>
							</c:if>
				      	</td>
				      </tr>
				    </table>
				
				</form>

			</div>

			<div id="pane2" class="panes">
				<display:table id="detail" name="cmd.historyRekening" class="displaytag">
					<display:column property="TGL" title="Tanggal Perubahan" format="{0, date, dd/MM/yyyy}" />
					<display:column property="LUS_LOGIN_NAME" title="User" style="text-align: left;" />
					<display:column property="NOTES" title="Keterangan" style="text-align: left;" />
					<display:column property="MRC_NO_AC_LAMA" title="From" style="text-align: left;" />
					<display:column property="MRC_NO_AC" title="To" style="text-align: left;" />
				</display:table>
			</div>

		</div>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>
