<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">

	function parseDate(str) {
	    var mdy = str.split('/');
	    return new Date(mdy[2], mdy[1], mdy[0]-1);
	}
	
	function daydiff(first, second) {
	    return Math.floor( (second-first)/(1000*60*60*24) );
	}
	
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}

		$( "#btnHitung" ).button({icons: {primary: "ui-icon-script"}});
		$( "#btnTutup" ).button({icons: {primary: "ui-icon-close"}});

		$("#btnHitung").click(function() {
			var jml_hr = daydiff(parseDate($("#bdate").val()), parseDate($("#tgl_byr").val()));
			var pinjaman = ${MSPIN_SISA};
			var bunga = ${MSPIN_BUNGA};
			var jml_byr = $("#jml_byr").val();
			
			var bunga_nominal = pinjaman * Math.pow(1+(bunga/100/365), jml_hr) - pinjaman;
			
			var sisa = pinjaman + bunga_nominal - jml_byr;
			
			$("#jml_hr").val(jml_hr);
			$("#bunga").val(bunga_nominal.toFixed(2));
			$("#sisa").val(sisa.toFixed(2));
			
		});

		$("#btnTutup").click(function() {
			window.close();
		});
		
	});
	
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 15em; }
	
	/* untuk align */
	.tengah { text-align: center; }
	.kanan { text-align: right; }

</style>

<body>

<form id="formPost" name="formPost" method="post" target="_blank">
<fieldset class="ui-widget ui-widget-content">
	<legend class="ui-widget-header ui-corner-all"><div>Hitung Bunga Pinjaman</div></legend>

	<table class="plain" width="100%">
		<tr>
			<th width="50%">No. Polis:</th>
			<td><input type="text" class="lebar ui-state-default tengah" readonly="readonly" value="${MSPO_POLICY_NO_FORMAT}"></td>
		</tr>
		<tr>
			<th>No. Pinjaman:</th>
			<td><input type="text" class="lebar ui-state-default tengah" readonly="readonly" value="${MSPIN_NO_PINJAMAN}"></td>
		</tr>
		<tr>
			<th>Periode:</th>
			<td>
				<input id="bdate" size="10" type="text" class="ui-state-default tengah" readonly="readonly" value="<fmt:formatDate value="${MSPIN_BEG_DATE}" pattern="dd/MM/yyyy"/>"> s/d 
				<input size="10" type="text" class="ui-state-default tengah" readonly="readonly" value="<fmt:formatDate value="${MSPIN_END_DATE}" pattern="dd/MM/yyyy"/>">
			</td>
		</tr>
		<tr>
			<th>Jumlah Pinjaman:</th>
			<td><input type="text" class="lebar ui-state-default kanan" readonly="readonly" value="<fmt:formatNumber value="${MSPIN_PINJAMAN}" type="currency" currencySymbol=""/>"></td>
		</tr>
		<tr>
			<th>Outstanding Pinjaman:</th>
			<td><input type="text" class="lebar ui-state-default kanan" readonly="readonly" value="<fmt:formatNumber value="${MSPIN_SISA}" type="currency" currencySymbol=""/>"></td>
		</tr>
		<tr>
			<th>Bunga (% p.a.):</th>
			<td><input type="text" class="lebar ui-state-default kanan" readonly="readonly" value="${MSPIN_BUNGA}"></td>
		</tr>
		<tr>
			<th>Cara Bayar:</th>
			<td><input type="text" class="lebar ui-state-default tengah" readonly="readonly" value="Setoran"></td>
		</tr>
		<tr>
			<th>Tgl Bayar:<em>*</em></th>
			<td><input id="tgl_byr" type="text" class="datepicker" value="<fmt:formatDate value="${today}" pattern="dd/MM/yyyy"/>"></td>
		</tr>
		<tr>
			<th>Jumlah Bayar:<em>*</em></th>
			<td><input id="jml_byr" type="text" class="lebar kanan" value="0"></td>
		</tr>
		<tr>
			<th>Jumlah Hari:</th>
			<td><input id="jml_hr" type="text" class="lebar ui-state-default kanan" readonly="readonly"></td>
		</tr>
		<tr>
			<th>Bunga:</th>
			<td><input id="bunga" type="text" class="lebar ui-state-default kanan" readonly="readonly"></td>
		</tr>
		<tr>
			<th>Sisa:</th>
			<td><input id="sisa" type="text" class="lebar ui-state-default kanan" readonly="readonly"></td>
		</tr>
		<tr>
			<th></th>
			<td>
				<button id="btnHitung" title="Hitung">Hitung</button>
				<button id="btnTutup" title="Tutup">Tutup</button>
			</td>
		</tr>
	</table>

</fieldset>
</form>

</body>
</html>