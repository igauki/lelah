<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header_jquery.jsp"%>

<body  style="height: 100%;" onload="setupPanes('container1', 'tab${pane}');">
<c:choose>
    <c:when test="${not empty notCS}">
        <div id="error">
            Maaf, Anda tidak memiliki akses untuk menu ini
        </div>
    </c:when>
    <c:otherwise>
        <div id="container1" class="tab-container">
            <ul class="tabs">
                <li>
                    <a href="#" onclick="return showPane('pane1', this)" id="tab1">History SMS</a>
                </li>
            </ul>
            
            <div class="tab-panes">
                <div id="pane1" class="panes">
                    <fieldset>
                        <legend>HISTORY SMS</legend>
                        <table class="simple">
                            <thead>
                                <tr>
                                    <th style="text-align: left">Tanggal</th>
                                    <th style="text-align: left">ID</th>
                                    <th style="text-align: left">Penerima</th>
                                    <th style="text-align: left">Operator</th>
                                    <th style="text-align: left">Batch ID</th>
                                    <th style="text-align: left">Tanggal Kirim Ke JATIS</th>
                                    <th style="text-align: left">Tanggal Request Delivery Report</th>
                                    <th style="text-align: left">Tanggal SMS Dikirim</th>
                                    <th style="text-align: left">Panjang Karakter</th>
                                    <th style="text-align: left">Total SMS</th>
                                    <th style="text-align: left">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${smsList}" var="sms" varStatus="s">
                                    <tr>
                                        <td><fmt:formatDate value="${sms.tgl_input}" pattern="dd/MM/yyyy hh:mm:ss" /></td>
                                        <td>${sms.id}</td>
                                        <td>${sms.recipient}</td>
                                        <td>${sms.operator}</td>
                                        <td>${sms.batch_id}</td>
                                        <td><fmt:formatDate value="${sms.sent_modem_date}" pattern="dd/MM/yyyy hh:mm:ss" /></td>
                                        <td><fmt:formatDate value="${sms.receive_modem_date}" pattern="dd/MM/yyyy hh:mm:ss" /></td>
                                        <td><fmt:formatDate value="${sms.sent_date}" pattern="dd/MM/yyyy hh:mm:ss" /></td>
                                        <td>${sms.length}</td>
                                        <td>${sms.total_sms}</td>
                                        <td>
                                            <c:choose>
                                                <c:when test="${sms.status eq 'S'}">
                                                    Sent
                                                </c:when>
                                                <c:when test="${sms.status eq 'D'}">
                                                    Delivered
                                                </c:when>
                                                <c:when test="${sms.status eq 'Q'}">
                                                    Queued
                                                </c:when>
                                                <c:when test="${sms.status eq 'A'}">
                                                    Aborted
                                                </c:when>
                                                <c:when test="${sms.status eq 'U'}">
                                                    Unsent
                                                </c:when>
                                                <c:when test="${sms.status eq 'F'}">
                                                    Failed
                                                </c:when>
                                                <c:when test="${sms.status eq 'P'}">
                                                    Pending
                                                </c:when>
                                                <c:otherwise>
                                                    
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </fieldset>
                </div>
            </div>
        </div>
    </c:otherwise>
</c:choose>
</body>