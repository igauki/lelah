<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/header.jsp"%>
<script>
function awal(){
		setFrameSize('infoFrame', 45);
		setFrameSize('docFrame', 45);
	}
$(document).ready(function() {
        <c:if test="${not empty pesanError}">
            alert('${pesanError}');
        </c:if>
    });
</script>
<body onLoad="awal();">
<c:choose>    
    <c:when test="${not empty main}">
        <div id="error">
            Maaf, Anda tidak memiliki akses untuk menu ini
        </div>
    </c:when>
   <c:otherwise>
   <form method="post" name="formpost">	
	<fieldset>
      <legend>Kirim Ulang Softcopy</legend>
		<table class="entry2" style="width: auto;">
		<tr>
			<th>Masukan E-mail <input type="text" size="30" name="email" value=${pmg.mspe_email} ></th>
		</tr>
		  <tr>
			 <th colspan="2">
				<input type="submit" name="send" value="send">
				<input type="hidden" name="flag" value="1">
				 	<c:if test="${not empty pesanError}"><br/>
                   	  <div id="error">Pesan : ${pesanError}</div>
                 	 </c:if>
			 </th>
		  </tr>
	  </table>
    </fieldset>	
  </form>		
 </c:otherwise>
</c:choose>
</body>
<%@ include file="/include/page/footer.jsp"%>