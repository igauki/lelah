<%@ include file="/include/page/header_mall.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<%User currentUser = (User) session.getAttribute("currentUser");%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>

 
<script type="text/javascript">
	hideLoadingMessage();
	
	function bodyEndLoad(){
		var successMessage = '${successMessage}';
		if(successMessage != ''){
		alert('${successMessage}');	
				//var flag_posisi = document.getElementById('flag_posisi').value;
				parentForm = self.opener.document.forms['formpost'];
				//if(${successMessage} == 'insert sukses'){
					parentForm.elements['tmms_id'].value='${tmms_id}';
					parentForm.elements['refreshButton'].click();
					window.close();
				//}
				}
	}
</script>

</head>

<body onload="resizeCenter(750,600);bodyOnLoad();" style="height: 100%;">
<XML ID=xmlData></XML>
<div class="tab-container" id="container1">

		<div class="tab-panes">

			<div id="pane1" class="panes">
<form:form commandName="cmd" name="frmParam" id="frmParam" method="post">
					<fieldset>
					<legend>PA</legend>
				    <table class="result_table2" cellspacing="3">
					    <tr>
						    <td colspan="2" style="border: 0px;">
							    <spring:bind path="cmd.*">
								    <c:if test="${not empty status.errorMessages}">
								        <div id="error">ERROR:<br>
										    <c:forEach var="error" items="${status.errorMessages}">
								                - <c:out value="${error}" escapeXml="false" />
											    <br />
										    </c:forEach>
									    </div>
								    </c:if>
				                </spring:bind>
			                </td>
		                </tr>
					    <%-- <c:if test="${currentUser.lca_id eq '58'}">
						    <tr>
						        <td>Appointment Id:</td>
						        <td><form:input path="tmms.mspo_plan_provider" size="50" cssErrorClass="inpError" readonly="readonly"/><font class="error">*</font></td>
						    </tr>
					    </c:if>
					    <tr>
					        <th colspan="2">Data Pemegang Polis</th>
					    </tr>
					    <tr>
					        <td align="left">Nama Pemegang:</td>
					        <td><form:input path="tmms.holder_name" size="80" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
					    </tr>
					    <tr>
						    <td align="left">Jenis Kelamin:</td>
							<td>
							<spring:bind path="cmd.tmms.sex">
								<label for="cowok"> <input type="radio" class=noBorder  disabled="disabled"
									name="${status.expression}" value="1"
									<c:if test="${cmd.tmms.sex eq 1 or cmd.tmms.sex eq null}"> 
												checked</c:if>
									id="cowok">Pria </label>
								<label for="cewek"> <input type="radio" class=noBorder  disabled="disabled"
									name="${status.expression}" value="0"
									<c:if test="${cmd.tmms.sex eq 0}"> 
												checked</c:if>
									id="cewek">Wanita </label>
							</spring:bind>
							<font class="error">*</font>
							</td>
					    </tr>
					    <tr>
							<td align="left">Tempat Lahir:</td>
							<td><form:input path="tmms.bod_tempat" cssErrorClass="inpError"  disabled="disabled"/><font class="error">*</font></td>
					    </tr>
					    <tr>
							<td align="left">Tanggal Lahir:</td>
							<td>
								<fmt:formatDate value="${cmd.tmms.bod_holder}" pattern="dd/MM/yyyy" />
							</td>
					    </tr>
					    <tr>
							<td align="left">No KTP:</td>
							<td><form:input path="tmms.no_identitas" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
					    </tr>
					    <tr>
							<td align="left">Status:</td>
							<td>
								<spring:bind path="cmd.tmms.status">
									<label for="menikah"> <input type="radio" class=noBorder  disabled="disabled"
										name="${status.expression}" value="1"
										<c:if test="${cmd.tmms.status eq 1 or cmd.tmms.sex eq null}"> 
													checked</c:if>
										id="menikah">Menikah </label>
									<label for="belum_menikah"> <input type="radio" class=noBorder  disabled="disabled"
										name="${status.expression}" value="0"
										<c:if test="${cmd.tmms.status eq 0}"> 
													checked</c:if>
										id="belum_menikah">Belum Menikah </label>
								</spring:bind>
								<font class="error">*</font>
							</td>
					    </tr>
					    <tr>
							<td align="left">Email:</td>
							<td><form:input path="tmms.email" size="60" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
					    </tr>
					    <tr>
							<td align="left">Alamat:</td>
							<td><form:input path="tmms.address1" size="80" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
					    </tr>
					    <tr>
							<td align="left">Kota:</td>
							<td><form:input path="tmms.city" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
					    </tr>
					    <tr>
							<td align="left">Kode Pos:</td>
							<td><form:input path="tmms.postal_code" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
					    </tr>
					    <tr>
							<td align="left">No Telepon:</td>
							<td><form:input path="tmms.home_phone" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
					    </tr>
					    <tr>
							<td align="left">No Hp:</td>
							<td><form:input path="tmms.mobile_no" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
					    </tr> --%>
					    <c:choose>
					        <c:when test="${paType eq 0}">
					            <c:if test="${currentUser.lca_id eq '58'}">
		                            <tr>
		                                <td>Appointment Id:</td>
		                                <td><form:input path="tmms.mspo_plan_provider" size="50" cssErrorClass="inpError" readonly="readonly"/><font class="error">*</font></td>
		                            </tr>
		                        </c:if>
		                        <tr>
		                            <th colspan="2">Data Pemegang Polis</th>
		                        </tr>
		                        <tr>
		                            <td align="left">Nama Pemegang:</td>
		                            <td><form:input path="tmms.holder_name" size="80" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
		                        </tr>
		                        <tr>
		                            <td align="left">Jenis Kelamin:</td>
		                            <td>
		                            <spring:bind path="cmd.tmms.sex">
		                                <label for="cowok"> <input type="radio" class=noBorder  disabled="disabled"
		                                    name="${status.expression}" value="1"
		                                    <c:if test="${cmd.tmms.sex eq 1 or cmd.tmms.sex eq null}"> 
		                                                checked</c:if>
		                                    id="cowok">Pria </label>
		                                <label for="cewek"> <input type="radio" class=noBorder  disabled="disabled"
		                                    name="${status.expression}" value="0"
		                                    <c:if test="${cmd.tmms.sex eq 0}"> 
		                                                checked</c:if>
		                                    id="cewek">Wanita </label>
		                            </spring:bind>
		                            <font class="error">*</font>
		                            </td>
		                        </tr>
		                        <tr>
		                            <td align="left">Tempat Lahir:</td>
		                            <td><form:input path="tmms.bod_tempat" cssErrorClass="inpError"  disabled="disabled"/><font class="error">*</font></td>
		                        </tr>
		                        <tr>
		                            <td align="left">Tanggal Lahir:</td>
		                            <td>
		                                <fmt:formatDate value="${cmd.tmms.bod_holder}" pattern="dd/MM/yyyy" />
		                            </td>
		                        </tr>
		                        <tr>
		                            <td align="left">No KTP:</td>
		                            <td><form:input path="tmms.no_identitas" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
		                        </tr>
		                        <tr>
		                            <td align="left">Status:</td>
		                            <td>
		                                <spring:bind path="cmd.tmms.status">
		                                    <label for="menikah"> <input type="radio" class=noBorder  disabled="disabled"
		                                        name="${status.expression}" value="1"
		                                        <c:if test="${cmd.tmms.status eq 1 or cmd.tmms.sex eq null}"> 
		                                                    checked</c:if>
		                                        id="menikah">Menikah </label>
		                                    <label for="belum_menikah"> <input type="radio" class=noBorder  disabled="disabled"
		                                        name="${status.expression}" value="0"
		                                        <c:if test="${cmd.tmms.status eq 0}"> 
		                                                    checked</c:if>
		                                        id="belum_menikah">Belum Menikah </label>
		                                </spring:bind>
		                                <font class="error">*</font>
		                            </td>
		                        </tr>
		                        <tr>
		                            <td align="left">Email:</td>
		                            <td><form:input path="tmms.email" size="60" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
		                        </tr>
		                        <tr>
		                            <td align="left">Alamat:</td>
		                            <td><form:input path="tmms.address1" size="80" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
		                        </tr>
		                        <tr>
		                            <td align="left">Kota:</td>
		                            <td><form:input path="tmms.city" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
		                        </tr>
		                        <tr>
		                            <td align="left">Kode Pos:</td>
		                            <td><form:input path="tmms.postal_code" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
		                        </tr>
		                        <tr>
		                            <td align="left">No Telepon:</td>
		                            <td><form:input path="tmms.home_phone" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
		                        </tr>
		                        <tr>
		                            <td align="left">No Hp:</td>
		                            <td><form:input path="tmms.mobile_no" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
		                        </tr>
					        </c:when>
					        <c:otherwise>
					            <%-- <c:if test="${currentUser.lca_id eq '58'}">
		                            <tr>
		                                <td>Appointment Id:</td>
		                                <td><form:input path="tmms.mspo_plan_provider" size="50" cssErrorClass="inpError" readonly="readonly"/><font class="error">*</font></td>
		                            </tr>
		                        </c:if> --%>
		                        <tr>
		                            <th colspan="2">Data Pemegang Polis</th>
		                        </tr>
		                        <tr>
		                            <td align="left">Nama Pemegang:</td>
		                            <td><form:input path="msp_pas_nama_pp" size="80" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
		                        </tr>
		                        <tr>
		                            <td align="left">Jenis Kelamin:</td>
		                            <td>
		                            <spring:bind path="cmd.msp_sex_pp">
		                                <label for="cowok"> <input type="radio" class=noBorder  disabled="disabled"
		                                    name="${status.expression}" value="1"
		                                    <c:if test="${cmd.msp_sex_pp eq 1 or cmd.msp_sex_pp eq null}"> 
		                                                checked</c:if>
		                                    id="cowok">Pria </label>
		                                <label for="cewek"> <input type="radio" class=noBorder  disabled="disabled"
		                                    name="${status.expression}" value="0"
		                                    <c:if test="${cmd.msp_sex_pp eq 0}"> 
		                                                checked</c:if>
		                                    id="cewek">Wanita </label>
		                            </spring:bind>
		                            <font class="error">*</font>
		                            </td>
		                        </tr>
		                        <tr>
		                            <td align="left">Tempat Lahir:</td>
		                            <td><form:input path="msp_pas_tmp_lhr_pp" cssErrorClass="inpError"  disabled="disabled"/><font class="error">*</font></td>
		                        </tr>
		                        <tr>
		                            <td align="left">Tanggal Lahir:</td>
		                            <td>
		                                <fmt:formatDate value="${cmd.msp_pas_dob_pp}" pattern="dd/MM/yyyy" />
		                            </td>
		                        </tr>
		                        <tr>
		                            <td align="left">No KTP:</td>
		                            <td><form:input path="msp_identity_no" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
		                        </tr>
		                        <tr>
		                            <td align="left">Email:</td>
		                            <td><form:input path="msp_pas_email" size="60" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
		                        </tr>
		                        <tr>
		                            <td align="left">Alamat:</td>
		                            <td><form:input path="msp_address_1" size="80" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
		                        </tr>
		                        <tr>
		                            <td align="left">Kota:</td>
		                            <td><form:input path="msp_city" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
		                        </tr>
		                        <tr>
		                            <td align="left">Kode Pos:</td>
		                            <td><form:input path="msp_postal_code" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
		                        </tr>
		                        <tr>
		                            <td align="left">No Telepon:</td>
		                            <td><form:input path="msp_pas_phone_number" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
		                        </tr>
		                        <tr>
		                            <td align="left">No Hp:</td>
		                            <td><form:input path="msp_mobile" cssErrorClass="inpError" disabled="disabled"/><font class="error">*</font></td>
		                        </tr>
					        </c:otherwise>
					    </c:choose>
					</table>
					</fieldset>
				    <fieldset>
				    <legend>-</legend>
				    <table class="result_table2">
					    <tr>
							<td colspan="2" style="text-align: center;">
							    <input type="hidden" name="flag_posisi" size="1" id="flag_posisi" value="${flag_posisi}"/>
							    <input type="hidden" name="kata" size="1" value="edit" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
								<input type="button" name="close" value="Close" onclick="window.close();" accesskey="C" />
						        <input type="hidden" name="result" size="1" value="${result}" />
							</td>
					    </tr>
				    </table>
				    </fieldset>
					</div>
		</div>
	</div>	
</form:form>

</body>
</html>