<%@ include file="/include/page/header.jsp"%>
<body onload="setupPanes('container1', 'tab1'); document.title='PopUp :: Endorsements'; setFrameSize('infoFrame', 170);"
    onresize="setFrameSize('infoFrame', 170);" style="height: 100%;">

    <div class="tab-container" id="container1">
        <ul class="tabs">
            <li>
                <a href="#" onClick="return showPane('pane1', this)" id="tab1">Print Surat Endors</a>
            </li>
        </ul>

        <div class="tab-panes">

            <div id="pane1" class="panes">

                <form name="formpost" method="post">
                <table width="100%">
                    <tr>
                        <td>
                            <table class="simple">
                                <thead>
                                    <tr align ="left">
                                        <th>No. Endorse</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="e" items="${cmd.endorseList}" varStatus="st">
                                        <c:choose>  
                                            <c:when test="${cmd.endorseno ne e.MSEN_ENDORS_NO}">
                                                <tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"   
                                                    onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
                                                   <%--  onClick="window.location='${path}/report/uw.htm?window=report_endors&no=${e.MSEN_ENDORS_NO}&spaj=${cmd.endorseInfo.REG_SPAJ }';"> --%>
                                                    onClick="window.location='${path }/report/uw.htm?window=report_endors&no=${e.MSEN_ENDORS_NO}&spaj=${param.spaj }';">
                                                    <td align="center">${e.MSEN_ENDORS_NO}</td>
                                                </tr>
                                            </c:when>
                                            <c:otherwise>
                                                <tr bgcolor="FFFFCC" onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;" 
                                                    onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
                                                     onClick="window.location='${path }/report/uw.htm?window=report_endors&no=${e.MSEN_ENDORS_NO}&spaj=${param.spaj }';">
                                                    <%--onClick="window.location='${path }/uw/viewer.htm?window=endorse&tipeendorse=${e.TIPE }&endorseno=${e.MSEN_ENDORS_NO}&spaj=${param.spaj }';">--%>
                                                    <td align="center">${e.MSEN_ENDORS_NO}</td>
                                                </tr>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </tbody>
                            </table>        
                            <div class="info">* Click for Detail</div>  
                        </td>
                        <td>
                            <table class="entry2">
                                <tr align ="left">
                                    <th>No. Polis : 
                                    <input type="text" style="background-color: '#FFFFCC';" value="<elions:polis nomor="${cmd.endorseInfo.MSPO_POLICY_NO }"/>">
                                    &nbsp;&nbsp;&nbsp;&nbsp;No. SPAJ :
                                    <input type="text" size="16" style="background-color: '#FFFFCC';" value="<elions:spaj nomor="${cmd.endorseInfo.REG_SPAJ }"/>">
                                    &nbsp;&nbsp;&nbsp;&nbsp;By :
                                    <input type="text" style="background-color: '#FFFFCC';" value="${cmd.endorseInfo.LUS_LOGIN_NAME}"></th>
                                </tr>
                            </table>
                            <table class="entry2">
                                <tr align="left">
                                    <th>Sumber :&nbsp;&nbsp;
                                    <input type="text" style="background-color: '#FFFFCC';" size="12" value="${cmd.endorseInfo.MSEN_INTERNAL}">
                                    </th>
                                    <th>Keterangan</th>
                                    <td><textarea rows="2" style="background-color: '#FFFFCC';" cols="72">${cmd.endorseInfo.MSEN_ALASAN}</textarea></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </form>
            </div>
        </div>
    </div>
</body>
<%@ include file="/include/page/footer.jsp"%>