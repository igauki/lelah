<%@ include file="/include/page/header.jsp"%>
<style type="text/css">
	html,body{
	     overflow: hidden;
	}
</style>
<script>
	function cekSpaj(){
		if(trim(document.formpost.spaj.value)=='') {
			alert('Harap cari nomor SPAJ terlebih dahulu');
			popWin('${path}/uw/spaj.htm?posisi=-1', 350, 450);
		}else return true;
		
		return false;
	}
	
	function awal(){
		if (document.formpost.spaj.value != "")
		{
			cariregion(document.formpost.spaj.value,'region');
		}
	}

	function monyong(y){
		if('cari'==y) {
			popWin('${path}/uw/spaj.htm?posisi=-1&win=viewer&kata='+document.formpost.spaj.value, 350, 450);
		}else if('spaj'==y) {
			if(cekSpaj()){
				document.getElementById('infoFrame').src='${path}/uw/view.htm?p=v&showSPAJ='+document.formpost.spaj.value+'&halForBlacklist=uw';
				document.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+document.formpost.spaj.value;
			}
		}else if('print'==y) {if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/uw/printpolis.htm?window=viewer&spaj='+document.formpost.spaj.value;
			//popWin('${path}/uw/printpolis.htm?window=viewer&spaj='+document.formpost.spaj.value, 500, 700, 'yes', 'yes'); 
		}else if('alamat'==y) {if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=addressbilling&spaj='+document.formpost.spaj.value;
		}else if('payment'==y) {
			if(cekSpaj())	document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=payment&spaj='+document.formpost.spaj.value;
		}else if('endorse'==y) {if(cekSpaj())
			/* popWin('${path}/uw/viewer.htm?window=endorse&spaj='+document.formpost.spaj.value, 500, 700);  */
			document.getElementById('infoFrame').src='${path}/uw/endorsenonmaterial.htm?window=editklarifikasi&spaj='+document.formpost.spaj.value;
		}else if('investasi'==y) {if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=investasi&spaj='+document.formpost.spaj.value, 500, 700; 
		}else if('uwinfo'==y) {if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=uwinfo&spaj='+document.formpost.spaj.value;
		}else if('nilaitunai'==y) {if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=nilaitunai&spaj='+document.formpost.spaj.value;
		}else if('view'==y) {if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=viewerkontrol&spaj='+document.formpost.spaj.value;
		}else if('viewendors'==y) {if(cekSpaj())
            document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=endorse&spaj='+document.formpost.spaj.value;
		}else if('sblink'==y) {if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/uw/inputsblink.htm?v=1&reg_spaj='+document.formpost.spaj.value;
		}else if('dokumen'==y) {if(cekSpaj()){
				if(document.getElementById('docFrame')) document.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+document.formpost.spaj.value;
				else document.getElementById('infoFrame').src='${path}/common/util.htm?window=doc_list&spaj='+document.formpost.spaj.value;
			}
		}else if('hcp'==y){
				if(cekSpaj()) popWin('${path}/bac/ttghcp.htm?sts=view&showSPAJ='+document.formpost.spaj.value, 500, 800); 
		}else if('akum'==y) {
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=akum';
		}else if('akum_new'==y) {
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=akum_new';
		}else if('statusss'==y) {
			document.getElementById('infoFrame').src='${path}/uw/status.htm?spaj='+document.formpost.spaj.value;
		}else if('call'==y) {
			if(cekSpaj())popWin('${path}/uw/viewer.htm?window=csfcall&spaj='+document.formpost.spaj.value, 480, 640); 
		}else if('checklist'==y){
			if(cekSpaj()) document.getElementById('infoFrame').src='${path}/checklist.htm?editable=0&reg_spaj='+document.formpost.spaj.value;
		}else if('summary'==y) {
			if(cekSpaj())popWin('${path}/report/bas.htm?window=csf_summary&spaj='+document.formpost.spaj.value, 700, 900);
		}else if('upload_nb'==y) {
			if(cekSpaj()) document.getElementById('infoFrame').src='${path}/uw/upload_nb.htm?reg_spaj='+document.formpost.spaj.value;
		}else if('summarylist'==y) {
			popWin('${path}/report/bas.htm?window=csf_summary_list&spaj='+document.formpost.spaj.value, 700, 900); 
		}else if('claim' == y){
			var spaj=document.formpost.spaj.value;
			popWin('${path}/uw/uw.htm?window=healthClaim&regspajClaimDetail='+spaj+'&spaj='+spaj, 400, 900,'yes','yes','yes');
		}else if('tunggakan'==y) {if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=hit_bunga_tunggakan&spaj='+document.formpost.spaj.value;
		}else if('auto'==y) {
			if(cekSpaj())document.getElementById('infoFrame').src='${path }/report/uw.htm?window=auto_debet&spaj='+document.formpost.spaj.value;
		}else if('nik'==y) {
			awal();
			if(cekSpaj())
			{
				alert('Hanya untuk produk Worksite');
				if  ((document.formpost.kodebisnis.value == 140)  || (document.formpost.kodebisnis.value == 141)  || (document.formpost.kodebisnis.value == 148)  || (document.formpost.kodebisnis.value == 149)  || (document.formpost.kodebisnis.value == 156) ||((document.formpost.kodebisnis.value == 143 && document.formpost.numberbisnis.value == 2) ) ||((document.formpost.kodebisnis.value == 158 && document.formpost.numberbisnis.value == 4) ))
				{
					popWin('${path}/bac/nik.htm?sts=insert&spaj='+document.formpost.spaj.value, 500, 800); 
				}else{
					alert('Nik tidak dapat diisi untuk polis ini');					
				}
			}
		}else if('faq'==y) {
			popWin('${path}/include/bas/faq_bas.jsp', 600, 800); 
		}else if('sms'==y) {
			createLoadingMessage();
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=cssms_in'; 
		}else if('worksheet'==y) {if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=worksheet&spaj='+document.formpost.spaj.value+'&copy_reg_spaj='+document.formpost.copy_reg_spaj.value;
		}else if('posisiberkas' == y) {
		    if(cekSpaj())
		    popWin('${path}/uw/viewer.htm?window=posisi_berkas&spaj='+document.formpost.spaj.value, 250, 450);
		}else if('nokartu' == y) {
		    if(cekSpaj())
		    popWin('${path}/uw/viewer.htm?window=no_kartu&spaj='+document.formpost.spaj.value, 250, 450);
		}else if('catatan_polis'==y) {
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=catatan_polis';
		}else if('exclude'==y) {
			if(cekSpaj())popWin('${path}/uw/exclude_admedika.htm?spaj='+document.formpost.spaj.value,300,800,'yes','no','no'); 
		}else if('retur'==y) {
			if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=polis_retur&spaj='+document.formpost.spaj.value;
		}else if('espajdmtm'==y) {
			if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/reports/espajdmtm2.pdf?spaj='+document.formpost.spaj.value;
		}else if('tglterimaAdmedika'==y) {
			if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=editTglTrmKrmSpaj&spaj='+document.formpost.spaj.value+'&show=3';
		}else if('cs' == y) {
		    document.getElementById('infoFrame').src = '${path}/uw/viewer.htm?window=view_report_cs';
		}else if('qc_ok_control' == y) {
			if(cekSpaj())
		    document.getElementById('infoFrame').src = '${path}/uw/printpolis.htm?window=qcControl&spaj='+document.formpost.spaj.value;
		}
		else if('medis' == y){
			if(cekSpaj())
			document.getElementById('infoFrame').src='${path }/uw/medical_new.htm?spaj='+document.formpost.spaj.value;	
		}
		else if('premi' == y){
			if(cekSpaj())
			document.getElementById('infoFrame').src='${path }/uw/premi.htm?spaj='+document.formpost.spaj.value;
		}
		else if('viewDMTM' == y){
			awal();
			if(cekSpaj()){
				alert('Hanya untuk produk DMTM');
				var lsbs = document.formpost.kodebisnis.value;
				var lsdbs = document.formpost.numberbisnis.value;
			    if ((lsbs == 183 && lsdbs > 30) ||( lsbs == 189 && lsdbs > 15 ) ||lsbs == 204 || lsbs==169 || lsbs==225 || (lsbs==197 && lsdbs==2) || 
					(lsbs == 163 & (lsdbs >= 21 && lsdbs <= 25)) || (lsbs == 173 & (lsdbs >= 7 && lsdbs <= 9))){ //helpdesk [148055] produk DMTM Dana Sejaterah 163 26-30 & Smile Sarjana 173 7-9 //helpdesk [150296] DMTM BSIM 163 21-25 tambah simple questionare SIO+
					 popWin('${path}/uw/uw.htm?window=view_kesehatanDMTM&json=0&lsbs='+lsbs+'&spaj='+document.formpost.spaj.value, 400, 900); 					
				}else if(lsbs == 221 & (lsdbs >= 1 && lsdbs <= 12)){
					popWin('${path}/uw/uw.htm?window=view_kesehatanDMTM&json=0&lsbs='+lsbs+'&spaj='+document.formpost.spaj.value, 600, 900);
				}else{
					alert('View Kesehatan / Questionare ini hanya untuk Produk DMTM(SMiLe Medical/Eka Waktu)');					
				}
			}
		}
		else if('profile_risk' == y){
			if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/bac/multi.htm?window=profile_risk&spaj=' + document.formpost.spaj.value + '&viewonly=1';
		}
		
	}

	<c:if test="${not empty cmd.showCsfReminder}">
		popWin('${path}/uw/viewer.htm?window=csfreminder', 320, 700);
	</c:if>
	
	
	function cariregion(spaj,nama)
	{
			ajaxManager.listcollect(spaj , nama,
			{callback:function(map) {
		
				DWRUtil.useLoadingMessage();
				document.formpost.koderegion.value=map.LSRG_NAMA;
				document.formpost.numberbisnis.value = map.LSDBS_NUMBER;
				document.formpost.kodebisnis.value = map.LSBS_ID;
			   },
			  timeout:15000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
		
</script>
<body 
	onload="setFrameSize('infoFrame', 90); setFrameSize('docFrame', 90);" 
	onresize="setFrameSize('infoFrame', 90); setFrameSize('docFrame', 90);" style="height: 100%;">
<form name="formpost" method="post">
<div class="tabcontent">
<table class="entry2" style="width: 98%;">
	<tr>
		<th style="width: 50px;">SPAJ</th>
		<th>
			<input type="hidden" name="koderegion" >
			<input type="hidden" name="kodebisnis" >
			<input type="hidden" name="numberbisnis" >
			<input type="hidden" name="copy_reg_spaj" />
			<input type="text" size="15" readonly="readonly" class="readOnly" id="spaj" name="spaj" ></th>
		<td>
			<input type="button" value="Info" name="tampilkan"
				onclick="monyong('spaj');" style="width: 35px;"
				accesskey="T" onmouseover="return overlib('Alt-T', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Cari" name="search"
				onclick="monyong('cari');" style="width: 35px;"
				accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Print Polis" name="print"
				onclick="monyong('print');" style="width: 75px;"
				accesskey="P" onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Alamat" name="alamat"
				onclick="monyong('alamat');" style="width: 55px;"
				accesskey="A" onmouseover="return overlib('Alt-A', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Payment" name="payment" ${cmd.paymentDisabled }
				onclick="monyong('payment');" style="width: 60px;"
				accesskey="Y" onmouseover="return overlib('Alt-Y', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Endorse" name="endorsement" 
				onclick="monyong('endorse');" style="width: 60px;"
				accesskey="E" onmouseover="return overlib('Alt-E', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Dokumen" name="dokumen" ${cmd.viewDokumenPolisBanc}
				onclick="monyong('dokumen');" style="width: 60px;"
				accesskey="D" onmouseover="return overlib('Alt-D', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Akum" name="akum" ${cmd.viewDokumenPolis}
				onclick="monyong('akum');" style="width: 60px;"
				accesskey="M" onmouseover="return overlib('Alt-M', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Akum New" name="akum" ${viewDokumenPolisNew}
				onclick="monyong('akum_new');" style="width: 80px;"
				accesskey="M" onmouseover="return overlib('Alt-M', AUTOSTATUS, WRAP);" onmouseout="nd();">				
			<input type="button" value="Status" name="status" ${cmd.viewDokumenPolisUW}
				onclick="monyong('statusss');" style="width: 60px;"
				accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="SB LINK" name="sblink" ${cmd.viewDokumenPolis}
				onclick="monyong('sblink');" style="width: 60px;">
					
			<input type="button" value="View Kesehatan" name="claim" 
				onclick="monyong('claim');" style="width: 120px;">
<!-- 			<input type="button" value="SM/Eka Sehat/HCP FAMILY" name="hcp"	 -->
<!-- 				onclick="monyong('hcp');"  -->
<!-- 				accesskey="H" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">	 -->
			<input type="button" value="Upload Scan" name="upload_nb"	
				onclick="monyong('upload_nb');">
			<input type="button" value="Catatan Polis Saya" name="catatan_polis"	
				onclick="monyong('catatan_polis');">
			 <input name="btn_espaj" type="button" value="E-SPAJ DMTM" 
			 	onClick="monyong('espajdmtm');">
			 <input name="tglTerima" type="button" value="Tgl Terima Admedika"
				onclick="monyong('tglterimaAdmedika');">
			<input name="viewdmtm" type="button" value="View Kesehatan DMTM"
				onclick="monyong('viewDMTM');">
			<input name="profile_risiko_nasabah" type="button" value="Profile Risiko Nasabah"   
				onclick="monyong('profile_risk');">   
		</td>
	</tr>
	<tr>
		<th>Polis</th>
		<th><input type="text" size="20" readonly="readonly" class="readOnly" id="polis" name="polis"></th>
		<td>
			<input type="button" value="Investasi" name="investasi" id="investasi"
				onclick="monyong('investasi');" style="width: 70px;" 
				accesskey="I" onmouseover="return overlib('Alt-I', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="U/W Info" name="uwinfo" 
				onclick="monyong('uwinfo');" style="width: 60px;"
				accesskey="U" onmouseover="return overlib('Alt-U', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="QC OK" name="qcok" 
				onclick="monyong('qc_ok_control');" style="width: 60px;"
				accesskey="W" onmouseover="return overlib('Alt-W', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Nilai Tunai" name="tabel"
				onclick="monyong('nilaitunai');" style="width: 80px;"
				accesskey="N" onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="View" name="view"
				onclick="monyong('view');" style="width: 38px;"
				accesskey="V" onmouseover="return overlib('Alt-V', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="View Endors" name="view"
                onclick="monyong('viewendors');" style="width: 100px;"
                accesskey="Z" onmouseover="return overlib('Alt-Z', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="NIK" name="nik"	onclick="monyong('nik');"
				accesskey="K" onmouseover="return overlib('Alt-K', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="FAQ-BAS" name="faq"	onclick="monyong('faq');"
				accesskey="Q" onmouseover="return overlib('Alt-Q', AUTOSTATUS, WRAP);" onmouseout="nd();">

			<input type="button" value="Call" name="call" 
				onclick="monyong('call');" style="width: 35px;"
				accesskey="L" onmouseover="return overlib('Alt-L', AUTOSTATUS, WRAP);" onmouseout="nd();">
			
			<input type="button" value="Summary" name="summary" 
				onclick="monyong('summary');" style="width: 65px;"
				accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">

			<input type="button" value="Summary List" name="summarylist" 
				onclick="monyong('summarylist');" style="width: 88px;"
				accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
			
			<input type="button" value="SMS" name="sms" 
				onclick="monyong('sms');" style="width: 35px;"
				accesskey="L" onmouseover="return overlib('Alt-L', AUTOSTATUS, WRAP);" onmouseout="nd();">
			
			<input type="button" value="Checklist" name="checklist" 
				onclick="monyong('checklist');" style="width: 88px;">
				
<!-- 		Deddy (2 Nov 2012) -ditutup dulu karena masih ada yg harus direvisi, selesai revisi, akan dibuka kembali	<input type="button" value="Bunga Tunggakan" name="tunggakan"  -->
<!-- 				onclick="monyong('tunggakan');" style="width: 120px;"> -->
			
			<input type="button" value="Worksheet" name="tunggakan" 
				onclick="monyong('worksheet');" style="width: 120px;">	
				
			<input type="button" value="Auto Debet" name="auto" 
				onclick="monyong('auto');" style="width: 120px;">
			
			<input type="button" value="Valid Simas Card" name="auto" 
				onclick="qc_ok_control('VALID PENGINPUTAN ULANG SIMAS CARD','Proses Valid Simas Card telah selesai. Simpan ?');" style="width: 120px;">

			<input type="button" value="List Cab BSIM" onclick="window.location='${path}/include/uw/LIST BANK SINARMAS.xls'">
			<input type="button" value="List Cab SMS" onclick="window.location='${path}/include/uw/LIST SEKURITAS SINARMAS.xls'">
			
			<input type="button" value="Posisi SK Autodebet" name="posisiberkas" onclick="monyong('posisiberkas')">
			
			<input type="button" value="No Kartu" name="nokartu" onclick="monyong('nokartu')">
			<c:if test="${sessionScope.currentUser.lde_id eq 11}">
				<input type="button" value="Exclude Admedika" name="exclude" onclick="monyong('exclude')">
			</c:if>	
			
			<input type="button" value="Polis Return" name="retur" 
				onclick="monyong('retur');" style="width: 120px;">	
			
			<c:if test="${sessionScope.currentUser.lde_id eq '01' or sessionScope.currentUser.lde_id eq '12' or sessionScope.currentUser.lde_id eq '32'}">
			    <input type="button" value="Customer Service" name="cs" onclick="monyong('cs');">
			</c:if>
			<c:if test="${sessionScope.currentUser.lus_id eq '1161' or sessionScope.currentUser.lus_id eq '1569' or sessionScope.currentUser.lus_id eq '687' or sessionScope.currentUser.lus_id eq '3813'}">
			    <input type="button" value="Medis" name="medis" onclick="monyong('medis');">
			</c:if>
			
			<c:if test="${sessionScope.currentUser.lde_id eq 11}">
			<input name="btn_premi"  type="button" value="Premium" onClick="monyong('premi');">
			</c:if>	
		</td>
	</tr>   
	
	<tr>
		<td colspan="3">
			<table style="margin: 0 0 0 0; padding: 0 0 0 0; width: 100%;">
				<tr>
					<c:choose>
						<c:when test="${not empty cmd.wideScreen}">
							<td style="width: 60%;">
								<spring:bind path="cmd.*">
								<iframe src="${path}/uw/spaj.htm?posisi=-1&win=viewer" name="infoFrame" id="infoFrame" width="100%"> Please Wait... </iframe>
								</spring:bind>
							</td>
							<td style="width: 40%;">
								<iframe src="" name="docFrame" id="docFrame" width="100%">E-Lions</iframe>
							</td>
						</c:when>
						<c:otherwise>
							<td>
								<spring:bind path="cmd.*">
								<iframe src="${path}/uw/spaj.htm?posisi=-1&win=viewer" name="infoFrame" id="infoFrame" width="100%"> Please Wait... </iframe>
								</spring:bind>
							</td>
						</c:otherwise>
					</c:choose>
				</tr>
			</table>
		</td>
	</tr>
</table>
</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>