<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script>
	function awal(){
		email='${email}';
		if(email=='0'){
			alert("email cabang tidak ada harap masukan email cabang tersebut!");
		}
	}
	function lock(value){
		var jne=value;
		if(jne == '01'){			
			document.getElementById('resi').value = "UPDATE RESI DI VIEWER"; 
		}else{
			document.getElementById('resi').value = ""; 
		}
		
	}
</script>
<body onLoad="awal();">
	<form method="post" name="formpost">	
	<table class="entry2" style="width: auto;">
	<c:choose>
		<c:when test="${show eq 0 }">	
			<tr>
				<th>Edit Tanggal Terima Spaj</th>
				<th style="text-align: left">
					<script>inputDate('tanggalTerima', '${tanggalTerima}', false, '', 9);</script>
				</th>
			</tr>
		</c:when>
		<c:when test="${show eq 1 }">	
			<tr>
				<th valign="top">Edit Tanggal Kirim Polis</th>
				<th style="text-align: left">
					<script>inputDate('tanggalKirim', '${tanggalKirim}', false, '', 9);</script>
				</th>
			</tr>
		</c:when>
		<c:when test="${show eq 2 }">	
			<tr>
				<th>Edit Tanggal Spaj Doc</th>
				<th>
					<script>inputDate('tglSpajDoc', '${tglSpajDoc}', false, '', 9);</script>
				</th>
			</tr>
		</c:when>
		<c:when test="${show eq 3 }">	
			<tr>
				<th>Edit Tanggal Terima Admedika</th>
				<th>
					<script>inputDate('tglTerimaAdmedika', '${tglTerimaAdmedika}', false, '', 9);</script>
				</th>
			</tr>
		</c:when>
		<c:when test="${show eq 4 }">	
			<tr>
				<th>Edit Tanggal Terima Admin</th>
				<th>
					<script>inputDate('tanggalTerimaAdmin', '${tanggalTerimaAdmin}', false, '', 9);</script>
				</th>
			</tr>
		</c:when>
	</c:choose>
	<tr>
		<th>Waktu</th>
		<th style="text-align: left">
			<select name="hh">
				<c:forEach items="${listHH}" var="h">
					<option value="${h}" <c:if test="${h eq hh}">selected</c:if>>${h}</option>
				</c:forEach>
			</select> : 
			<select name="mm">
				<c:forEach items="${listMM}" var="m">
					<option value="${m}" <c:if test="${m eq mm}">selected</c:if>>${m}</option>
				</c:forEach>
			</select>
		</th>
	</tr>
	<c:if test="${show eq 1 }">	
		<tr>
			<th>Kartu Admedika</th>
			<th>
				<input name="adm" id="adm" type="radio" class="noBorder" value="0">ADA
				<input name="adm" id="adm" type="radio" class="noBorder" value="1">TIDAK ADA
			</th>
		</tr>
		<tr>
			<th>SimasCard</th>
			<th>
				<input name="sms" id="sms" type="radio" class="noBorder" value="0">ADA
				<input name="sms" id="sms" type="radio" class="noBorder" value="1">TIDAK ADA
			</th>
		</tr>		
		<tr>
			<th>No.Resi</th>
			<th>
				<input name="resi" id="resi" type="text"  value="" size="50">			
			</th>
		</tr>
		<tr>
			<th>Keterangan</th>
			<th>
				 <select  name="proses" id="proses" title="Silahkan pilih" class="noBorder" style="width: 250px;"  onchange="lock(this.value)">												
					 	<c:forEach var="c" items="${listproses}" varStatus="s">
								<option value="${c.key}">${c.value}</option>
						</c:forEach> 
				</select>						
			</th>
		</tr>
	</c:if>
	<c:if test="${show ne 1 }">	
		<tr>
			<th colspan="2">Keterangan:
				<textarea name="keterangan" cols="50" rows="3" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); ">${keterangan }</textarea>
				<c:if test="${email eq 0 }">
					Masukan E-mail <input type="text" size="30" name="email" > 
					<input type="submit" value="send" name="btn_send" onClick="">
				</c:if>	
			</th>
		</tr>
	</c:if>
	<tr><td colspan="2">
		<c:if test="${success ne null }">
        	<div id="success">
	        	 ${success }
	    	</div>
		</c:if>
		<c:if test="${not empty lsError}">
			<div id="error">
				 Informasi:<br>
					<c:forEach var="error" items="${lsError}">
								 - <c:out value="${error}" escapeXml="false" />
						<br/>
					</c:forEach>
			</div>
		</c:if>			
	</td></tr>	
		<tr>
			<th colspan="2">
				<input type="submit" <c:if test="${success ne null }" >disabled</c:if> name="save" id="save" value="save">
				<input type="hidden" name="flag" value="1">
			</th>
		</tr>
	</table>	
	</form>		
</body>
<%@ include file="/include/page/footer.jsp"%>