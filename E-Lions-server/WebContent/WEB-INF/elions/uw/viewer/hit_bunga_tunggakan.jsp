<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<c:set var="serverName" value="${pageContext.request.serverName}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="PT. Asuransi Jiwa Sinarmas MSIG">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen"> <!-- CSS untuk DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script><!-- DatePicker Script (jscalendar) -->
	</head>
	<script>
		function tampil(){
			dd=formpost.dd.value;
			mm=formpost.mm.value;
			year=formpost.year.value;
			premike=formpost.premike.value;
			tahunke=formpost.tahunke.value;
			
<!--			window.location='${path}/viewer.htm?window=hit_bunga_tunggakan&tgl_bayar='+tgl_bayar+-->
<!--			'&premike=' + document.getElementById('premike').value + -->
<!--			'&tahunke=' + document.getElementById('tahunke').value + -->

				window.location='${path}/viewer.htm?window=hit_bunga_tunggakan&tgl_bayar='+tgl_bayar+
		}
	</script>
<form method="post" name="formpost" id="formside" action="">
	<table border="1" cellpadding="1" cellspacing="1" style="font-size: 8pt;">
		<tr></tr>
<!--		<tr>-->
<!--			<th >Premi Ke</th>-->
<!--			<th nowrap="nowrap" class="left" style="text-align: left;">-->
<!--				<td><input type="text" name="premike" id="premike"value="${premike}" size="5"></td>				-->
<!--			</th>-->
<!--		</tr>-->
<!--		<tr>-->
<!--			<th >Tahun Ke</th>-->
<!--			<th nowrap="nowrap" class="left" style="text-align: left;">-->
<!--				<td><input type="text" name="tahunke" id="tahunke"value="${tahunke}" size="5"></td>				-->
<!--			</th>-->
<!--		</tr>-->
		<tr>
			<th >Tanggal Bayar</th>	
			<th nowrap="nowrap" class="left" style="text-align: left;">
				<td><input type="text" name="tgl_bayar" id="tgl_bayar"value="<fmt:formatDate value="${dateBesok}" pattern="dd/MM/yyyy"/>" size="12"></td>				
			</th>
			
			<th><input type="submit" name="btnShow" id="btnShow" value="Apply" /></th>	
			<tr>
				<input type="hidden" id="SukuBunga" name="SukuBunga"/>
			</tr>
			<tr>
				<input type="hidden" id="bunga" name="bunga"/>
			</tr>			
			<tr>
				<input type="hidden" id="reg_spaj" name="reg_spaj"/>
			</tr>
			<tr>
				<input type="hidden" id="byrKe" name="byrKe"/>
			</tr>
		</tr>
	</table>
</form>	
<fieldset>
	<legend>Perhitungan Bunga Tunggakan</legend>
	<table border="1" cellpadding="1" cellspacing="1" style="font-size: 8pt;">	
		<tr >
<!--		<th>-->
<!--			Tahun-->
<!--		</th>-->
<!--		<th>-->
<!--			Premi Ke-->
<!--		</th>-->
		<th>
			Beg Aktif
		</th>
		<th>
			End Aktif
		</th>
		<th>
			Tanggal Bayar
		</th>
		<th>
			Jumlah Premi
		</th>				
		<th>
			Discount
		</th>
		<th>
			Tahapan
		</th>
		<th>
			Sub Total
		</th>
		<th>
			Jumlah Hari
		</th>
		<th>
			Bunga
		</th>
		<th>
			Tagihan Premi
		</th>
		<th>
			Materai
		</th>	
		<th>
			Total Tagih
		</th>				
		</tr>

		<tr>
<!--			<td>-->
<!--				${tahun_ke} -->
<!--			</td>-->
<!--			<td>-->
<!--				${premi_ke}-->
<!--			</td>-->
			<td>
				<fmt:formatDate value="${MSBI_BEG_DATE }" pattern="dd/MM/yyyy"/>
			</td>
			<td>
				<fmt:formatDate value="${MSBI_END_DATE}" pattern="dd/MM/yyyy"/>
			</td>
			<td>
				<fmt:formatDate value="${tglBayar}" pattern="dd/MM/yyyy"/>
			</td>
			<td>
				<fmt:formatNumber pattern="#,##0.00">${totPremi}</fmt:formatNumber>
			</td>
			<td>
				<fmt:formatNumber pattern="#,##0.00">${DISCOUNT}</fmt:formatNumber>
			</td>
			<td>
				<fmt:formatNumber pattern="#,##0.00">${tahapan}</fmt:formatNumber>
			</td>
			<td><fmt:formatNumber pattern="#,##0.00">${subTotal}</fmt:formatNumber></td>
			<td>
				${SELISIH}
			</td>
			<td>
				<fmt:formatNumber pattern="#,##0.00">${BUNGA}</fmt:formatNumber>
			</td>
			<td>
				<fmt:formatNumber pattern="#,##0.00">${TagihanPremi}</fmt:formatNumber>
			</td>
			<td>
				<fmt:formatNumber pattern="#,##0.00">${MATERAI}</fmt:formatNumber>
			</td>
			<td>
				<fmt:formatNumber pattern="#,##0.00">${TotTagih}</fmt:formatNumber>
			</td>
		</tr>
		<c:if test="${not empty error}">
					<tr>
						<td colspan="3">
							<div id="errorBox" class="error" style="width: 100%">
								Error Message:
								<c:forEach items="${error}" var="e" varStatus="s">
									<br>${e}
								 </c:forEach>
							</div>
						</td>
					</tr>
				</c:if>

	</table>
</fieldset>
</html>