<%@ include file="/include/page/taglibs.jsp"%>

<table class="entry2">
	<tr>
		<td>
			<input type="button" name="btnAccount" value="Rekening Pemegang Polis"
			onclick="popWin('${pageContext.request.contextPath}/uw/viewer.htm?window=rekening&spaj=${param.showSPAJ}', 400, 700);">
			<input type="button" name="btnAccount1" value="Rekening Auto Debet"
			onclick="popWin('${pageContext.request.contextPath}/uw/viewer.htm?window=autodebet&spaj=${param.showSPAJ}', 400, 700);">
		</td>
	</tr>
</table>

<c:choose>

	<c:when test="${not empty cmd.bukanUnitLink}">
		<div id="error">
			Produk ini bukan produk Unit-link maupun Powersave
		</div>
	</c:when>
	
	<c:otherwise>
		
		<c:choose>
			<c:when test="${not empty cmd.stableLink}">
				<table class="displaytag">
				<thead>
					<tr>
						<th>Manfaat Bulanan</th>
						<th>Transaksi</th>
						<th>Periode</th>
						<th>Posisi</th>
						<th>MTI</th>
						<th>Rate(%)</th>
						<th>Rollover</th>
						<th>Jml Hari</th>
						<th>Kurs</th>
						<th>Premi</th>
						<th>Bunga</th>
						<th>Jenis Bunga</th>
						<th>No Memo</th>
						<th>BP</th>
						<th>Tambahan Hari</th>
						<th>Rekening</th>
					</tr>
				</thead>
				<tbody>
					<c:set var="tmp" value="" />
					<c:forEach items="${cmd.stableLink}" var="s">
						<tr>
							<td class="center">
								<c:choose>
									<c:when test="${s.MANF_BUL eq 'Ya'}"><a target="_blank" href="#" onclick="popWin('${path}/uw/viewer.htm?window=slinkbayar&spaj=${s.REG_SPAJ}&msl_no=${s.MSL_NO}', 600, 1000); return false;">${s.MANF_BUL}</a></c:when>
									<c:otherwise>${s.MANF_BUL}</c:otherwise>
								</c:choose>
							</td>
							<td class="left">
							<c:choose>
								<c:when test="${tmp ne s.MSL_TU_KE}">
									<c:set var="tmp" value="${s.MSL_TU_KE}" />
									<span style="font-weight: bold; color: red;">${s.MSL_DESC}</span>
								</c:when>
								<c:otherwise>${s.MSL_DESC}</c:otherwise>
							</c:choose>
							</td>
							<td class="center"><fmt:formatDate value="${s.MSL_BDATE}" pattern="dd-MM-yyyy"/> s/d <fmt:formatDate value="${s.MSL_EDATE}" pattern="dd-MM-yyyy"/></td>
							<td class="left">${s.POSISI}</td>
							<td>${s.MSL_MGI} bln</td>
							<td><fmt:formatNumber value="${s.MSL_RATE}" maxFractionDigits="2" minFractionDigits="2" /></td>
							<td class="left">${s.RO}</td>
							<td>${s.MSL_HARI }</td>
							<td class="left">${s.KURS}</td>
							<td><fmt:formatNumber value="${s.MSL_PREMI}" maxFractionDigits="2" minFractionDigits="2" /></td>
							<td><fmt:formatNumber value="${s.MSL_BUNGA}" maxFractionDigits="2" minFractionDigits="2" /></td>
							<td>
							<select disabled>
								<c:forEach var="bg" items="${select_jenisbunga}">
									<option <c:if test="${s.FLAG_SPECIAL eq bg.ID}"> SELECTED </c:if> value="${bg.ID}">${bg.JENIS}</option>
								</c:forEach> 
							</select>
							</td>
							<td>${s.MSL_NOTE}</td>
							<td><fmt:formatNumber value="${s.MSL_BP}" maxFractionDigits="2" minFractionDigits="2" /></td>
							<td class="left">${s.TAMBAHAN}</td>
							<td>${s.REK}</td>
						</tr>
					</c:forEach>
				</tbody>
				</table>
				<br/><span class="info">(*) Khusus Transaksi Manfaat Bulanan, silahkan click pada kolom "Manfaat Bulanan" bersangkutan untuk melihat detail transaksi bulanan.</span>                                                                                                                       			
			</c:when>
			<c:when test="${not empty stableSave}">
				<display:table id="baris" name="stableSave" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="" export="true" pagesize="20">
					<display:column property="MSS_NO" title="No."  sortable="true"/>                                                                                           
					<display:column property="ROLLOVER" title="Rollover"  sortable="true"/>                                                                                       
					<display:column property="MSS_BDATE" title="BegDate" format="{0, date, dd/MM/yyyy}"  sortable="true"/>                                                      
					<display:column property="MSS_EDATE" title="EndDate" format="{0, date, dd/MM/yyyy}"  sortable="true"/>                                                      
					<display:column property="MSS_PREMI" title="Premi" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                           
					<display:column property="MSS_RATE" title="Rate" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>
					<display:column property="MSS_HARI" title="Hari" sortable="true"/>                                             
					<display:column property="MSS_BUNGA" title="Jumlah Bunga" format="{0, number, #,##0.00;(#,##0.00)}" />
					<display:column property="MSS_MGI" title="MTI"  sortable="true"/>                                                                                         
					<display:column property="MSS_INPUT_DATE" title="Tgl Input" format="{0, date, dd/MM/yyyy}"  sortable="true"/>                                            
					<display:column property="LUS_LOGIN_NAME" title="User"  sortable="true"/>                                                                           
					<display:column property="MSS_NOTE" title="Deskripsi"  sortable="true"/>  
					<display:column property="INFO_FLAG_BULANAN" title="MANFAAT BULANAN"  sortable="true"/>  
				</display:table>                                                                                                                                             			
			</c:when>
			<c:when test="${not empty cmd.unitLink}">
<%-- 
				<fieldset>
					<legend>Informasi Unit Link</legend>
					
					<table class="entry2">
					
						<tr>
							<th nowrap="nowrap">
								Jumlah Premi
							</th>
							<th nowrap="nowrap" class="left">
								<input type="text" style="text-align: right;" value="<fmt:formatNumber value="${cmd.unitLink[0].mu_jlh_premi}" type="currency" currencySymbol="" maxFractionDigits="2" minFractionDigits="0" />" readonly>
								<input type="text" value="${cmd.unitLink[0].mu_ke}" size="3" class="readOnly" readonly>
							</th>
							<th nowrap="nowrap">
								Tgl NAB
							</th>
							<th nowrap="nowrap" class="left">
								<input type="text" value="<fmt:formatDate value="${cmd.unitLink[0].mu_tgl_nab}" pattern="dd/MM/yyyy"/>" class="readOnly" readonly>
							</th>
						</tr>
		
						<tr>
							<th nowrap="nowrap">
								Top-up
							</th>
							<th nowrap="nowrap" class="left">
								<select disabled>
									<c:forEach var="jnstopup" items="${select_jns_top_up}">
									<option
										<c:if test="${cmd.unitLink[0]. mu_periodic_tu eq jnstopup.ID}"> SELECTED </c:if>
										value="${jnstopup.ID}">${jnstopup.JENIS}</option>
									</c:forEach>
								</select>
							</th>
							<th nowrap="nowrap">
								Tgl Surat
							</th>
							<th nowrap="nowrap" class="left">
								<input type="text" value="<fmt:formatDate value="${cmd.unitLink[0].mu_tgl_surat}" pattern="dd/MM/yyyy"/>" readonly>
							</th>
						</tr>
						
						<tr>
							<th nowrap="nowrap">
								Jumlah Top-up
							</th>
							<th nowrap="nowrap" class="left">
								<input type="text" style="text-align: right;" value="<fmt:formatNumber value="${cmd.unitLink[0].mu_jlh_tu}" type="currency" currencySymbol="" maxFractionDigits="2" minFractionDigits="0" />" readonly>
							</th>
							<th nowrap="nowrap">
								Frekuensi Surat (bulan)
							</th>
							<th nowrap="nowrap" class="left">
								<input type="text" value="${cmd.unitLink[0].mu_bulan_surat}" readonly>
							</th>
						</tr>
						
						<tr>
							<th nowrap="nowrap">
								Total Premi
							</th>
							<th nowrap="nowrap" class="left">
								<input type="text" style="text-align: right;" value="<fmt:formatNumber value="${cmd.unitLink[0].total}" type="currency" currencySymbol="" maxFractionDigits="2" minFractionDigits="0" />" class="readOnly" readonly>
							</th>
							<th nowrap="nowrap">
								Posisi 
							</th>
							<th nowrap="nowrap" class="left">
								<input type="text" value="${cmd.unitLink[0].lspd_position}" class="readOnly" readonly size="50" readonly>
							</th>
						</tr>
						
					</table>
				</fieldset>
--%>				
				<fieldset>
					<legend>Premi</legend>
					<display:table id="premi" name="cmd.daftarPremi" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator">
						<display:column property="MU_KE" title="Premi Ke" style="text-align: left;" />
						<display:column property="LT_TRANSKSI" title="Premi" style="text-align: left;" />
						<display:column property="MU_JLH_PREMI" title="Jumlah" format="{0, number, #,##0.00;(#,##0.00)}" total="true" />
						<display:column property="MU_TGL_NAB" title="Tanggal NAB" format="{0, date, dd/MM/yyyy}" style="text-align: center;" />
						<display:column property="MU_TGL_SURAT" title="Tanggal Surat" format="{0, date, dd/MM/yyyy}" style="text-align: center;" />
						<display:column property="MU_BULAN_SURAT" title="Frekuensi Surat (bulan)" style="text-align: center;" />
						<display:column property="LSPD_POSITION" title="Posisi" style="text-align: left;" />
					</display:table>
				</fieldset>
				
				<fieldset>
					<legend>Investasi</legend>
					<display:table id="detail" name="cmd.detailUnitLink" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator">
						<display:column property="lji_invest" title="Jenis Investasi" style="text-align: left;" />
						<display:column property="mdu_persen" title="Persen" format="{0, number, #,##0.00;(#,##0.00)}" total="true" />
						<display:column property="mdu_jumlah" title="Alokasi Investasi" format="{0, number, #,##0.00;(#,##0.00)}" total="true"/>
						<display:column property="mdu_saldo_unit" title="Saldo Unit" format="{0, number, #,##0.000;(#,##0.000)}" />
						<display:column property="mdu_last_trans" title="Tanggal Transaksi Terakhir" format="{0, date, dd/MM/yyyy}" />
					</display:table>
				</fieldset>
				
				<fieldset>
					<legend>Biaya</legend>
					<display:table id="biaya" name="cmd.biayaUnitLink" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator">
						<display:column property="ljb_biaya" title="Jenis Biaya" style="text-align: left;" />
						<display:column property="mbu_jumlah" title="Jumlah" format="{0, number, #,##0.00;(#,##0.00)}" total="true"/>
						<display:column property="mbu_persen" title="Persen" format="{0, number, #,##0.000;(#,##0.000)}" total="true"/>
					</display:table>
				</fieldset>	
									
				<c:if test="${not empty cmd.detailDTH}">
					<fieldset>
						<legend>Dana Talangan Haji</legend>
						<display:table id="dth" name="cmd.detailDTH" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator">
							<display:column property="rownum" title="No" style="text-align: center;"/>
							<display:column property="jumlah" title="Jumlah Penarikan" format="{0, number, #,##0.00;(#,##0.00)}"/>
							<display:column property="tgl_penarikan" title="Tanggal Penarikan" format="{0, date, dd/MM/yyyy}" style="text-align: center;"/>
							<display:column property="keterangan" title="Keterangan" style="text-align: left;"/>
						</display:table>      
					</fieldset>                                                                                                                                       			
				</c:if>
				
				<fieldset>
					<legend>Dana Investasi di Alokasikan</legend>							
						<c:if test="${cmd.dataUsulan.mste_flag_investasi eq 0}">
							<b>Langsung setelah dana diterima di rekening yang ditentukan oleh PT. Asuransi Jiwa Sinarmas MSIG dan SPAJ telah diterima di bagian Underwriting.</b>
						</c:if>		
						<c:if test="${cmd.dataUsulan.mste_flag_investasi eq 1}">
							<b>Setelah permintaan asuransi disetujui oleh Bagian Underwriting dan Calon Pemegang Polis telah setuju serta membayar ekstra premi (jika ada).</b>
						</c:if>		
				</fieldset>	
			</c:when>
			
			<c:when test="${not empty cmd.powerSave}">
				<fieldset>
					<legend>Informasi Produk PowerSave</legend>
					<display:table id="ps" name="cmd.powerSave" class="displaytag">
						<display:column title="Jangka Waktu" style="width: 100px; text-align: center;">
							<select disabled>
				                <c:forEach var="jk" items="${select_jangka_invest}">
				                	<option <c:if test="${ps.mps_jangka_inv eq jk.ID}"> SELECTED </c:if> value="${jk.ID}">${jk.JANGKAWAKTU}</option>
				                </c:forEach> 
							</select>
						</display:column>
						<display:column property="mps_rate" title="Bunga" format="{0, number, #,##0.000;(#,##0.000)}" />
						<display:column property="mps_deposit_date" title="Periode Awal" format="{0, date, dd/MM/yyyy}"/>
						<display:column property="mps_batas_date" title="Periode Akhir/ Jatuh Tempo" format="{0, date, dd/MM/yyyy}"/>
						<display:column property="mps_prm_deposit" title="Jumlah Investasi" format="{0, number, #,##0.00;(#,##0.00)}" />
						<display:column property="mpr_hari" title="Jumlah Hari"/>
						<display:column property="mps_prm_interest" title="Jumlah Bunga" format="{0, number, #,##0.00;(#,##0.00)}" />
						<display:column title="Jenis Roll-over" style="width: 200px; text-align: center;">
							<select disabled>
								<c:forEach var="roll" items="${select_rollover}">
									<option <c:if test="${ps.mps_roll_over eq roll.ID}"> SELECTED </c:if> value="${roll.ID}">${roll.ROLLOVER}</option>
								</c:forEach> 
							</select>
						</display:column>
					</display:table>
					<display:table id="ps2" name="cmd.powerSave" class="displaytag">
						<display:column title="Jenis Bunga" style="width: 200px; text-align: center;">
							<select disabled>
								<c:forEach var="bg" items="${select_jenisbunga}">
									<option <c:if test="${ps2.mps_jenis_plan eq bg.ID}"> SELECTED </c:if> value="${bg.ID}">${bg.JENIS}</option>
								</c:forEach> 
							</select>
						</display:column>
						<display:column property="mpr_note" title="Nomor Memo" />
						<display:column property="mpr_breakable" title="Breakable" />
						<display:column title="Status Karyawan" style="width: 200px; text-align: center;">
							<select disabled>
								<c:forEach var="kr" items="${select_karyawan}">
									<option <c:if test="${ps2.mps_employee eq kr.ID}"> SELECTED </c:if> value="${kr.ID}">${kr.KRY}</option>
								</c:forEach> 
							</select>
						</display:column>
					</display:table>
					<table class="entry2">
						<tr>
							<th nowrap="nowrap">
								<input type="checkbox" class="noBorder" <c:if test="${cmd.dataUsulan.mspo_flag_sph eq 1}">checked</c:if> disabled> Nasabah mau menandatangani SPH
							</th>
						</tr>
					</table>
				</fieldset>
				<fieldset>
					<legend>Dana Investasi di Alokasikan</legend>							
						<c:if test="${cmd.dataUsulan.mste_flag_investasi eq 0}">
							<b>Langsung setelah dana diterima di rekening yang ditentukan oleh PT. Asuransi Jiwa Sinarmas MSIG dan SPAJ telah diterima di bagian Underwriting.</b>
						</c:if>		
						<c:if test="${cmd.dataUsulan.mste_flag_investasi eq 1}">
							<b>Setelah permintaan asuransi disetujui oleh Bagian Underwriting dan Calon Pemegang Polis telah setuju serta membayar ekstra premi (jika ada).</b>
						</c:if>		
				</fieldset>	
			</c:when>
		</c:choose>
	</c:otherwise>
</c:choose>