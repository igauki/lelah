<%@ include file="/include/page/header.jsp"%>
<script>
<!-- 
	var totalTambah = ${cmd.totalTambah};
	var totalCall = ${cmd.totalTambah};

	var startTime = new Date(<fmt:formatDate value="${cmd.sysDate}" pattern="yyyy,MM,dd,hh,mm,ss"/>);

	setTimeout("updateTimer()", 1000);

	function updateTimer(){
		startTime.setTime(startTime.getTime()+1000);
		setTimeout("updateTimer()", 1000);
	}
		
	function insertPosisi(){
			var spaj = document.getElementById('spaj').value;
			var lde_id ='${sessionScope.currentUser.lde_id}';
			if (lde_id != 11){
				alert ("Hanya User U/W yang bisa melakukan proses ini!");
			}else{
			 ajaxManager.posisiSpt(spaj,'${sessionScope.currentUser.lus_id}','${sessionScope.currentUser.lde_id}');
			 alert ("Berhasil Disimpan di history U/W");
			}
	}

	function setNilaiTerakhir(angka){
		var a = document.getElementsByName('s_end');
		if(a.length>1) a[a.length-angka].value = formatDate(startTime,'hh:mm:ss');
	}

	function addRow(tableID){
		var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
		var panjang = oTable.rows.length;
		var oRow = oTable.insertRow(panjang);
		var oCells = oRow.cells;
		if(document.all){
			totalTambah++;
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td >"+totalTambah+".<input type=\"hidden\" name=\"s_dial\" value=\""+totalTambah+"\"><input type=\"hidden\" name=\"s_jenis\" value=\"baru\"></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td ><input type=\"text\" name=\"user\" style=\"background-color: #FFFFE0; \" value=\"${sessionScope.currentUser.name}\" size=\"15\"></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td ><textarea rows=\"4\" cols=\"50\" style=\"background-color: #FFFFE0; \" name=\"s_ket\"></textarea></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td ><input type=\"text\" readonly name=\"s_start\" size=\"10\" value=\""+formatDate(startTime,'hh:mm:ss')+"\"></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td ><input type=\"text\" readonly name=\"s_end\" size=\"10\"></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td ><input style=\"background-color: #FFFFE0; \" type=\"text\" readonly name=\"s_callback\" id=\"s_callback"+totalTambah+"\" size=\"12\"></td>";
			
		    Calendar.setup({
		        inputField     :    "s_callback"+totalTambah,
		        ifFormat       :    "%d/%m/%Y",
		        align          :    "Tl"
		    });

			setNilaiTerakhir(2);
		}
	}
	
	function deleteRow(tableID){
		var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
		var panjang = oTable.rows.length;
		while(totalTambah>totalCall){
			oTable.deleteRow();
			totalTambah--;
		}
		document.formpost.reset();
	}
	
	function followUp(flag){
		var spaj = document.getElementById('spaj').value;
		popWin('${path}/uw/viewer.htm?window=followup_csfcall&spaj='+spaj+'&flag='+flag, 480, 640);
	}
//-->
</script>
<body onload="document.title='PopUp :: CSF Call';" style="height: 100%;">
	<div id="contents">
	<fieldset>
	<form method="post" name="formpost">
		<input type="hidden" name="spaj" value="${cmd.spaj }">
		<legend>
			<select name="inout" onchange="document.formpost.submit();">
				<option value="I" >Incoming Call</option>
				<option value="O" <c:if test="${cmd.inout eq \"O\"}">selected</c:if>>Outgoing Call</option>
			</select>
			For <fmt:formatDate value="${cmd.sysDate}" pattern="dd/MM/yyyy"/>
		</legend>
		<br/><br/>
		<table class="simple" id="callList">
			<thead>
				<tr>
					<td>Call Ke.</td>
					<td>User</td>
					<td>Pesan</td>
					<td>Start</td>
					<td>End</td>
					<td>Tanggal Callback</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="call" items="${cmd.callList}" varStatus="st">
					<tr>
						<td >
							${call.mscsf_dial_ke}.
							<input type="hidden" name="s_dial" value="${call.mscsf_dial_ke}">
							<input type="hidden" name="s_jenis" value="lama">
						</td>
						<td >
							<input type="text" name="user" value="${sessionScope.currentUser.name}" readonly="readonly" size="15">
						</td>
						<td >
							<textarea rows="4" cols="50" name="s_ket">${call.mscsf_ket}</textarea>
						</td>
						<td >
							<input type="text" readonly name="s_start" size="10" value="<fmt:formatDate value="${call.mscsf_beg_tgl_dial}" pattern="hh:mm:ss"/>">
						</td>
						<td >
							<input type="text" readonly name="s_end" size="10" value="<fmt:formatDate value="${call.mscsf_end_tgl_dial}" pattern="hh:mm:ss"/>">
						</td>
						<td >
							<script>inputDate('s_callback', '<fmt:formatDate value="${call.mscsf_tgl_back}" pattern="dd/MM/yyyy"/>', false);</script>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<c:if test="${not empty errorMessage}">
			<div id="error">
				Tidak semua data berhasil disimpan:<br>
				<c:forEach var="error" items="${errorMessage}">
					- <c:out value="${error}" escapeXml="false" />
					<br />
				</c:forEach>
			</div>
		</c:if>
		<br/>
		<input type="button" name="add" value="Add" onclick="addRow('callList');">
		<input type="submit" name="save" value="Save Changes" onclick="setNilaiTerakhir(1);">
		<input type="button" name="spt" value="SPT OK" onclick="insertPosisi();">
		<input type="button" name="cancel" value="Cancel" onclick="deleteRow('callList');">
		<input type="button" name="close" value="Close" onclick="window.close();">
		<input type="button" name="followup" value="Follow Up" onclick="window.close();followUp(0);">
		<input type="button" name="followup" value="Berhasil" onclick="window.close();followUp(1);">
	</form>
	</fieldset>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>