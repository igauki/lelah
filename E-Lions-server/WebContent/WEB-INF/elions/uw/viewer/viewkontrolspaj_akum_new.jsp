<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript">
	function tampil(akum,window){
		//alert(document.getElementsByName("tglLahir").value);
		//alert(document.getElementById("tglSar").value);
		flag=0;
		if(document.formpost.flag.checked){
			flag=1;
			if(document.formpost.tglLahir.value == "00/00/0000"){
				alert("Harap mengisi tanggal lahir");
				return;
			}
		}	

		if(akum && document.formpost.pilter2.value!='') {
			var bln = document.getElementById('bulan').value;
			if(bln == '') bln = 0;
			var url = '${path}/uw/viewer.htm?show=true&window=displaytag&tipeView=19'
					 +'&spaj=${spaj}&pptt='+document.getElementById('pptt').value
					 +'&pilter='+escape(document.getElementById('pilter').value)+'&pilter2='+document.getElementById('pilter2').value
					 +'&flagTglLahir='+flag+'&tglLahir='+document.formpost.tglLahir.value
					 +'&bulanKe='+bln+'&tglSar='+document.getElementById("tglSar").value;
					 		
			if(window == "simultan") {
				url += '&flagLifeOnly=true';
				//alert(url);
				parent.popWin(url,300,900,'yes','no','yes');
			}
			else  {
				document.getElementById('infoFrame').src= url;			
			}
			return;		
		}
	}
	
	function numOnly(e,v,i){
		var unicode=e.charCode? e.charCode : e.keyCode;	
		//alert(unicode);
		if (unicode!=8){ 
			if (unicode<48||unicode>57) 
				return false; 
			//if(unicode == 47)
				//return false;
		}
		return true;						
	}
	
</script>
<style type="text/css">
	html,body{
	     overflow: hidden;
	}
</style>
<body onload="document.title='PopUp :: Akumulasi Polis'; setupPanes('container1', 'tab1'); setFrameSize('infoFrame', 105);" onresize="setFrameSize('infoFrame', 105);" style="height: 100%;">
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Akumulasi Polis</a>
			</li>
		</ul>
		<div class="tab-panes">
			<div id="pane1" class="panes">
				<c:choose>
					<c:when test="${not empty pesanError}">
						<div id="error">${pesanError}</div>
					</c:when>
					<c:otherwise>
						<form method="post" name="formpost">
							<table class="entry2" width="98%" border="0">		
								<tr>
									<td colspan="4">
										<div id="filter">
											<select name="pptt">
												<option value="6">Pemegang</option>
												<option value="7">Tertanggung</option>
											</select>
											<select name="pilter">
												<option <c:if test="${\"LIKE%\" eq param.pilter}">selected</c:if> value="LIKE&#37">LIKE%</option>
												<option <c:if test="${\"%LIKE\" eq param.pilter}">selected</c:if> value="&#37LIKE">%LIKE</option>
												<option <c:if test="${\"%LIKE%\" eq param.pilter}">selected</c:if> value="&#37LIKE&#37">%LIKE%</option>
												<option <c:if test="${\"EQ\" eq param.pilter}">selected</c:if> value="EQ">=</option>
											</select>
											<input type="text" name="pilter2" value="${param.pilter2 }">
										</div>
									</td>
									<td width="478" rowspan="3">
										<!-- Permintaan Ariani tidak dimunculkan untuk user admin-->
										<c:if test="${pusatOrCabang eq 'pusat'}">
										<table width="100%" cellspacing="3">
											<tr>
												<td style="border-right: 1px #e0e0e0 solid; border-bottom: 1px #e0e0e0 solid; vertical-align: middle;" width="15" align="center"><img src="${path}/include/image/black.jpg" width="10" height="10"></td>
												<td style="border-right: 1px #e0e0e0 solid; border-bottom: 1px #e0e0e0 solid; font-size: 9px;">POLIS INDIVIDU & DMTM</td>
												<td style="border-right: 1px #e0e0e0 solid; border-bottom: 1px #e0e0e0 solid;">1 $US : Rp 8,000</td>
											</tr>
											<tr>
												<td style="border-right: 1px #e0e0e0 solid; border-bottom: 1px #e0e0e0 solid; vertical-align: middle;" width="15" align="center"><img src="${path}/include/image/black.jpg" width="10" height="10"> </td>
												<td style="border-right: 1px #e0e0e0 solid; border-bottom: 1px #e0e0e0 solid; font-size: 9px;">TOTAL RETENSI POLIS INFORCE</td>
												<td style="border-right: 1px #e0e0e0 solid; border-bottom: 1px #e0e0e0 solid;">Retensi Rp : 750,000,000</td>
											</tr>											
											<tr>
												<td style="border-right: 1px #e0e0e0 solid; border-bottom: 1px #e0e0e0 solid; vertical-align: middle;" width="15" align="center"><img src="${path}/include/image/red.jpg" width="10" height="10"> </td>
												<td style="border-right: 1px #e0e0e0 solid; border-bottom: 1px #e0e0e0 solid; color: red; font-size: 9px;">POLIS MRI</td>
												<td style="border-right: 1px #e0e0e0 solid; border-bottom: 1px #e0e0e0 solid;">Retensi $US : 75,000</td>
											</tr>
											<tr>
												<td style="border-right: 1px #e0e0e0 solid; border-bottom: 1px #e0e0e0 solid; vertical-align: middle;" width="15" align="center"><img src="${path}/include/image/blue.jpg" width="10" height="10"> </td>
												<td style="border-right: 1px #e0e0e0 solid; border-bottom: 1px #e0e0e0 solid; color: blue; font-size: 9px;">TOTAL RETENSI POLIS TIDAK INFORCE</td>
												<td>&nbsp;</td>
											</tr>																						
										</table>
										</c:if>
									</td>									
								</tr>
								<tr>
									<td colspan="4">
										<div id="filter2">
											<input type="checkbox" class="noBorder" onClick="tekanTgl();" name="flag">AND TANGGAL LAHIR
											<script>inputDate('tglLahir', '00/00/0000', false, '', 9);</script>
										</div>
									</td>
								</tr>
								<tr>
									<th width="120">SAR MRI pertanggal</th>
									<td width="95"><script>inputDate('tglSar', '00/00/0000', false, '', 9);</script></td>								
									<th width="60">Bulan ke</th>									
									<td >
										<input type="text" name="bulan" value="${param.bulan}" size="5" onkeypress="return numOnly(event,this.value,this.id);"> 
										<input type="button" name="show2" value="Show" onclick="createLoadingMessage(); tampil('yes','');">&nbsp;&nbsp;
										<input type="button" name="show3" value="Simultan Medis" onclick="tampil('yes','simultan');">
									</td>
								</tr>
							</table>
							<iframe name="infoFrame" id="infoFrame" width="100%" frameborder="YES">-</iframe>
						</form>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>