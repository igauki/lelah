<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/disableKeys.js"></script>
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		
		<style type="text/css">
		
	    	#map {
	 			height: 50%;
			}
		
			html, body {
	  			height: 100%;
	  			margin: 0;
	  			padding: 0;
			}
			
    	</style>

		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCll9SXOxkyRt42oaQFRcoB-v7dpCS-bo0"></script>
		
	</head>
	<body style="height: 100%;text-align: center;" onload='setupPanes("container1", "tab1");'>

	<c:choose>
		<c:when test="${empty spaj }">
			<div id="success">Silahkan cari nomor SPAJ terlebih dahulu</div>
		</c:when>
		<c:otherwise>
			<fieldset>
				<table class="entry2">
					<tr>
						<th class="left">
							No. Polis
							<input readonly type=text size=20 value="<elions:polis nomor="${polis}"/>">
							No. SPAJ
							<input readonly type=text size=15 value="<elions:spaj nomor="${spaj}"/>">
							Status Polis
							<input readonly type=text size=30 value="<elions:spaj nomor="${info.LSSP_STATUS}"/>">
						</th>
					</tr>
					<tr>
						<th class="left">
							Posisi Dokumen
							<input readonly type=text size=60 value="<elions:spaj nomor="${info.LSPD_POSITION}"/>">
							Status Aksep
							<input readonly type=text size=30 value="<elions:polis nomor="${info.STATUS_ACCEPT}"/>">
						</th>
					</tr>
				</table>
			</fieldset>

			<div class="tab-container" id="container1">
				<ul class="tabs">
					<li>
						<a href="#" onClick="return showPane('pane1', this)"  
						onmouseover="return overlib('Alt-1', AUTOSTATUS, WRAP);" onmouseout="nd();"
						onfocus="return showPane('pane1', this)" accesskey="1" id="tab1">Posisi</a>
					</li>
					<li>
						<a href="#" onClick="return showPane('pane2', this)"  
						onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();"
						onfocus="return showPane('pane2', this)" accesskey="2">Ulangan</a>
					</li>
					<li>
						<a href="#" onClick="return showPane('pane3', this)"
						onmouseover="return overlib('Alt-3', AUTOSTATUS, WRAP);" onmouseout="nd();"
						onfocus="return showPane('pane3', this)" accesskey="3">Begin Date</a>
					</li>
					<li>
						<a href="#" onClick="return showPane('pane4', this)"
						onmouseover="return overlib('Alt-4', AUTOSTATUS, WRAP);" onmouseout="nd();"
						onfocus="return showPane('pane4', this)" accesskey="4">Status</a>
					</li>
					<li>
						<a href="#" onClick="return showPane('pane5', this)"
						onmouseover="return overlib('Alt-5', AUTOSTATUS, WRAP);" onmouseout="nd();"
						onfocus="return showPane('pane5', this)" accesskey="5">Batal</a>
					</li>
					<li>
						<a href="#" onClick="return showPane('pane6', this)"
						onmouseover="return overlib('Alt-6', AUTOSTATUS, WRAP);" onmouseout="nd();"
						onfocus="return showPane('pane6', this)" accesskey="6">Billing Change</a>
					</li>
					<li>
						<a href="#" onClick="return showPane('pane7', this)"
						onmouseover="return overlib('Alt-7', AUTOSTATUS, WRAP);" onmouseout="nd();"
						onfocus="return showPane('pane7', this)" accesskey="7">History Salah</a>
					</li>
					<li>
						<a href="#" onClick="return showPane('pane8', this)"
						onmouseover="return overlib('Alt-8', AUTOSTATUS, WRAP);" onmouseout="nd();"
						onfocus="return showPane('pane8', this)" accesskey="8">Print Billing</a>
					</li>
					<li>
						<a href="#" onClick="return showPane('pane10', this)"
						onmouseover="return overlib('Alt-10', AUTOSTATUS, WRAP);" onmouseout="nd();"
						onfocus="return showPane('pane10', this)" accesskey="10">Status Pengiriman</a>
					</li>
					<li>
						<a href="#" onClick="return showPane('pane9', this)"
						onmouseover="return overlib('Alt-9', AUTOSTATUS, WRAP);" onmouseout="nd();"
						onfocus="return showPane('pane9', this)" accesskey="8">Tanggal</a>
					</li>
				</ul>
	
				<div class="tab-panes">
	
					<div id="pane1" class="panes">
						<display:table id="baris" name="daftar0" class="displaytag" requestURI="${requestScope.requestURL}" export="true">
							<display:caption>Posisi SPAJ</display:caption>
								<display:column property="mspo_policy_no" title="Polis" />
								<display:column property="msps_date" title="Tanggal" format="{0, date, dd/MM/yyyy (HH:mm:ss)}"  />
								<display:column property="lspd_position" title="Posisi Dokumen"  />
								<display:column property="lssp_status" title="Status Polis"  />
								<display:column property="status_accept" title="Status Aksep"  />
								<display:column property="lus_login_name" title="User"  />
								<display:column property="msps_desc" title="Keterangan" />
						</display:table>
					</div>
	
					<div id="pane2" class="panes">
						<display:table id="baris" name="daftar1" class="displaytag" requestURI="${requestScope.requestURL}" export="true">
							<display:caption>List Ulangan</display:caption>
								<display:column property="TANGGAL" title="Tanggal" format="{0, date, dd/MM/yyyy}"/>
								<display:column property="JENIS" title="Posisi Dokumen"/>                                                                                            
								<display:column property="LSSP_STATUS" title="Status Polis"/>
								<display:column property="LUS_LOGIN_NAME" title="User"/>                                                                          
								<display:column property="KETERANGAN" title="Keterangan"/>                                                                                  
						</display:table>
					</div>
	
					<div id="pane3" class="panes">
						<display:table id="baris" name="daftar2" class="displaytag" requestURI="${requestScope.requestURL}" export="true">
							<display:caption>Begin Date</display:caption>
							<display:column property="TGL_UPDATE" title="Tgl Update" format="{0, date, dd/MM/yyyy}"/>
							<display:column property="BEG_DATE" title="Begin Date" format="{0, date, dd/MM/yyyy}"/>
							<display:column property="OSUSER" title="OS User"/>
							<display:column property="MACHINE" title="Machine"/>
							<display:column property="PROGRAM" title="Program"/>
						</display:table>
					</div>
	
					<div id="pane4" class="panes">
						<display:table id="baris" name="daftar3" class="displaytag" requestURI="${requestScope.requestURL}" export="true">
							<display:caption>Status Polis</display:caption>
							<display:column property="TGL_UPDATE" title="Tgl Update" format="{0, date, dd/MM/yyyy}"/>
							<display:column property="STATUS1" title="Status From"/>
							<display:column property="STATUS2" title="Status To"/>
							<display:column property="OSUSER" title="OS User"/>
							<display:column property="MACHINE" title="Machine"/>
							<display:column property="PROGRAM" title="Program"/>
						</display:table>
					</div>
	
					<div id="pane5" class="panes">
						<display:table id="baris" name="daftar4" class="displaytag" requestURI="${requestScope.requestURL}" export="true">
							<display:caption>Detail Batal</display:caption>
							<display:column property="MSB_NO_BATAL" title="No. Pembatalan"/>
							<display:column property="MSB_TGL_BATAL" title="Tanggal Batal" format="{0, date, dd/MM/yyyy}"  />
							<display:column property="MSB_ALASAN" title="Alasan"  />
							<display:column property="LUS_LOGIN_NAME" title="Oleh" />
						</display:table>
					</div>
	
					<div id="pane6" class="panes">
						<display:table id="baris" name="daftar5" class="displaytag" requestURI="${requestScope.requestURL}" export="true">
							<display:caption>Billing Change</display:caption>
							<display:column property="TGL_UPDATE" title="Tgl Update" format="{0, date, dd/MM/yyyy}" />                                                                
							<display:column property="TAHUN_KE" title="Tahun" />                                                                                                   
							<display:column property="PREMI_KE" title="Premi" />                                                                                                   
							<display:column property="BEG_AKTIF" title="Beg Aktif" format="{0, date, dd/MM/yyyy}" />                                                                  
							<display:column property="END_AKTIF" title="End Aktif" format="{0, date, dd/MM/yyyy}" />                                                                  
							<display:column property="NEXT_BILL" title="Next Bill" format="{0, date, dd/MM/yyyy}" />                                                                  
							<display:column property="NEW_TH_KE" title="Tahun" />                                                                                                 
							<display:column property="NEW_PRM_KE" title="Premi" />                                                                                               
							<display:column property="NEW_BAKTIF" title="Beg Aktif" format="{0, date, dd/MM/yyyy}" />                                                                
							<display:column property="NEW_EAKTIF" title="End Aktif" format="{0, date, dd/MM/yyyy}" />                                                                
							<display:column property="NEW_NEXT_BILL" title="Next Bill" format="{0, date, dd/MM/yyyy}" />                                                          
							<display:column property="LUS_LOGIN_NAME" title="User" />                                                                                       
						</display:table>
					</div>
	
					<div id="pane7" class="panes">
						<display:table id="baris" name="daftar6" class="displaytag" requestURI="${requestScope.requestURL}" export="true">
							<display:caption>History Salah</display:caption>
							<display:column property="MSL_TGL" title="Tanggal" format="{0, date, dd/MM/yyyy}" />                                                                      
							<display:column property="LUS_SALAH" title="User Salah" />                                                                                                 
							<display:column property="LUS_USER" title="Batal Oleh" />                                                                                                   
							<display:column property="MSL_TAHUN_KE" title="Tahun Ke" />                                                                                           
							<display:column property="MSL_PREMI_KE" title="Premi Ke" />                                                                                           
							<display:column property="MSL_NO_PRE" title="Nomor PRE" />                                                                                               
							<display:column property="LSL_KET" title="Jenis Salah" />                                                                                                     
							<display:column property="MSL_KET" title="Notes" />                                                                                                     
						</display:table>
					</div>
	
					<div id="pane8" class="panes">
						<display:table id="baris" name="daftar7" class="displaytag" requestURI="${requestScope.requestURL}" export="true">
							<display:caption>Print Billing</display:caption>
							<display:column property="TAHUN_KE" title="Tahun Ke" />
							<display:column property="PREMI_KE" title="Premi Ke" />
							<display:column property="TANGGAL" title="Tanggal" format="{0, date, dd/MM/yyyy}" />
							<display:column property="KETERANGAN" title="Keterangan" />
							<display:column property="LUS_LOGIN_NAME" title="User" />
						</display:table>
					</div>
					
					<div id="pane9" class="panes">
						<display:table id="baris" name="daftar9" class="displaytag" requestURI="${requestScope.requestURL}" export="true">
							<display:caption>Tanggal</display:caption>
							<display:column property="MSTE_BEG_DATE" title="Awal Periode Pertanggungan" format="{0, date, dd/MM/yyyy}" />                                                                
							<display:column property="MSTE_END_DATE" title="Akhir Periode Pertanggungan" format="{0, date, dd/MM/yyyy}" />  
							<display:column property="TGL_TERIMA_SPAJ" title="Tanggal Terima SPAJ" format="{0, date, dd/MM/yyyy}" />
							<display:column property="TGL_INPUT_SPAJ" title="Tanggal Input SPAJ" format="{0, date, dd/MM/yyyy}" />
							<display:column property="TGL_SPAJ" title="Tanggal SPAJ" format="{0, date, dd/MM/yyyy}" />
							<display:column property="KYC" title="Tanggal Verifikasi KYC" format="{0, date, dd/MM/yyyy}" />
							<display:column property="TGL_AKEP" title="Tanggal Akseptasi" format="{0, date, dd/MM/yyyy}" />
							<display:column property="TGL_VALID_PRINT" title="Tanggal Valid Print Cabang" format="{0, date, dd/MM/yyyy}" />
							<display:column property="TGL_PRINT" title="Tanggal Print Polis" format="{0, date, dd/MM/yyyy}" />
							<display:column property="KRM_POLIS" title="Tanggal Kirim Polis" format="{0, date, dd/MM/yyyy}" />
							<display:column property="TGL_TAGIHAN_BERIKUTNYA" title="Tanggal Pembayaran Berikutnya" format="{0, date, dd/MM/yyyy}" />
							<display:column property="TG_AKHR_BYR" title="Tanggal Akhir Bayar" format="{0, date, dd/MM/yyyy}" />    
							<display:column property="TGL_KRM_LB" title="Tanggal Kirim LB" format="{0, date, dd/MM/yyyy}" />
							<display:column property="TGL_TERIMA_LB" title="Tanggal Terima LB" format="{0, date, dd/MM/yyyy}" />
							<display:column property="TGL_KOMISI" title="Tanggal Komisi" format="{0, date, dd/MM/yyyy}" />
						</display:table>
					</div>
				
					<!-- Begin Nana -->
					<div id="pane10" class="panes">
						<c:set var="fl" value="${flag}" />
						<c:choose>
							<c:when test="${fl != null}">
								${statusServer} 
							</c:when>
							<c:otherwise>
								<!--<div id="pane10" class="panes"> -->
								<div align="left" style="margin-bottom: 5px;">
									<span style="font-weight: bold">Tracking Details</span>
								</div>
								<div align="left">
									<!-- <span style="font-weight: bold"> Shipping History </span> -->
									<table class="simple">
										<thead>
											<tr>
												<th style="text-align: left">Order Id</th>
												<th style="text-align: left">No AWB</th>
												<th style="text-align: left">Tgl Pengiriman diterima JNE</th>
												<th style="text-align: left">Nomer Konsumen</th>
												<th style="text-align: left">Status Pengiriman</th>
												<th style="text-align: left">Nama Penerima</th>
												<th style="text-align: left">Tanggal Sampai Barang</th>
												<th style="text-align: left">Signature</th>
												<th style="text-align: left">Photo</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="cnote" items="${cnoteList}">
												<td>${cnote.reference_number}</td>
												<td>${cnote.cnote_no}</td>
												<td>${cnote.cnote_date}</td>
												<td>${cnote.cnote_cust_no}</td>
												<td>${cnote.pod_status}</td>
												<td>${cnote.cnote_pod_receiver}</td>
												<td>${cnote.cnote_pod_date}</td>
												
												<td align="center">
													<c:choose>
														<c:when test="${cnote.signature != null}">
															<a href=# target=_self onClick="pop('${cnote.signature}')">
																<img src="${cnote.signature}" alt="img" style="width:30px;">
															</a>			
														</c:when>
														<c:otherwise>
															<a href=# target=_self onClick="pop('${cnote.signature}')">
																<span style="text-transform: lowercase;">unavailable</span>
															</a>
														</c:otherwise>
													</c:choose>
												</td>
												
												<td align="center">
													<c:choose>
														<c:when test="${cnote.photo != null}">
															<a href=# target=_self onClick="pop('${cnote.photo}')">
																<img src="${cnote.photo}" alt="img" style="width:30px;">
															</a>		
														</c:when>
														<c:otherwise>
															<a href=# target=_self onClick="pop('${cnote.photo}')">
																<span style="text-transform: lowercase;">unavailable</span>
															</a>
														</c:otherwise>
													</c:choose>
												</td>
												
											</c:forEach>
										</tbody>
									</table>
								</div>
					
								<br>
								<div align="left">
									<!--  <span style="font-weight: bold"> Details </span> -->
									<table class="simple">
										<thead>
											<tr>
												<th style="text-align: left">Nama Kota Asal Pengirim</th>
												<th style="text-align: left">Nama Pengirim</th>
												<th style="text-align: left">Alamat Pengirim 1</th>
												<th style="text-align: left">Alamat Pengirim 2</th>
												<th style="text-align: left">Alamat Pengirim 3</th>
												<th style="text-align: left">Nama Asal Kota Pengirim</th>
												<th style="text-align: left">Nama Penerima</th>
												<th style="text-align: left">Alamat Penerima 1</th>
												<th style="text-align: left">Alamat Penerima 2</th>
												<th style="text-align: left">Alamat Penerima 3</th>
												<th style="text-align: left">Nama Kota Penerima</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="detail" items="${detailList}">
												<td>${detail.cnote_origin}</td>
												<td>${detail.cnote_shipper_name}</td>
												<td>${detail.cnote_shipper_addr1}</td>
												<td>${detail.cnote_shipper_addr2}</td>
												<td>${detail.cnote_shipper_addr3}</td>
												<td>${detail.cnote_shipper_city}</td>
												<td>${detail.cnote_receiver_name}</td>
												<td>${detail.cnote_receiver_addr1}</td>
												<td>${detail.cnote_receiver_addr2}</td>
												<td>${detail.cnote_receiver_addr3}</td>
												<td>${detail.cnote_receiver_city}</td>
											</c:forEach>
										</tbody>
									</table>
								</div>
					
								<!-- <br> -->
								<div align="left">
									<!-- <span style="font-weight: bold"> History </span> -->
									<br>
									<table class="simple">
										<tbody>
											<c:forEach var="history" items="${historyList}">
												<tr>
													<td>${history.date}</td>
													<td>${history.desc}</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
					
								<br>
								<div align="left">
									<c:forEach var="cn" items="${cnoteList}">
										<input type="hidden" id="latlng" type="text"
											value="${cn.latitude}, ${cn.longitude}">
										<c:choose>
											<c:when test="${cn.latitude != null && cn.longitude != null}">
												Lihat detail lokasi
											<button id="submit" onclick="geocodeLatLng()">click!</button>
											</c:when>
											<c:otherwise>
												
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</div>
								<!--</div>-->
							</c:otherwise>
						</c:choose>
					</div> <!-- End Nana -->
					
				</div>
			</div>
		</c:otherwise>
	</c:choose>
	
	<script type="text/javascript">
	  function geocodeLatLng(){
		  var input = document.getElementById("latlng").value;
		  console.log("latitude and longitude " + input);
		  // var w = window.open('', '_blank'); //you must use predefined window name here for IE.
		  var w = window.open("", "MsgWindow", "width=700,height=500");
		  var head = w.document.getElementsByTagName('head')[0];
		  w.document.head.innerHTML = '<title>Simple Map</title></head>';
		  w.document.body.innerHTML = '<body><div id="map_canvas" style="display: block; width: 100%; height: 500px; margin: 0; padding: 0;"></div></body>';
		  var loadScript = w.document.createElement('script');
		  //Link to script that load google maps from hidden elements.
		  loadScript.type = "text/javascript";
		  loadScript.async = true;
		  // loadScript.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyCll9SXOxkyRt42oaQFRcoB-v7dpCS-bo0&amp;libraries=places";
		  loadScript.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyCll9SXOxkyRt42oaQFRcoB-v7dpCS-bo0&sensor=false&callback=initialize";
		  var googleMapScript = w.document.createElement('script');
		  //Link to google maps js, use callback=... URL parameter to setup the calling function after google maps load.
		  googleMapScript.type = "text/javascript";
		  googleMapScript.async = false;
		  googleMapScript.text = 'var map;';
		  googleMapScript.text += 'function initialize() {';
		  googleMapScript.text += '  var latlng = new google.maps.LatLng('+input+');';
		  googleMapScript.text += '  var mapOptions = {';
		  googleMapScript.text += '    center: latlng,';
		  googleMapScript.text += '    zoom: 11, ';
		  googleMapScript.text += '  };';
		  googleMapScript.text += '  map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);';
		  googleMapScript.text += '  var marker = new google.maps.Marker({';
		  googleMapScript.text += '    position: latlng,';
		  googleMapScript.text += '    map: map';
		  googleMapScript.text += ' });';
		  googleMapScript.text += 'var geocoder = new google.maps.Geocoder;';
		  googleMapScript.text += 'var infowindow = new google.maps.InfoWindow;';
		  googleMapScript.text += 'geocoder.geocode({"location": latlng}, function(results, status) {';
		  googleMapScript.text += ' infowindow.setContent(results[0].formatted_address);';
		  googleMapScript.text += ' infowindow.open(map, marker);';
		  googleMapScript.text += '});';
		  googleMapScript.text += '}';
		  head.appendChild(loadScript);
		  head.appendChild(googleMapScript);
		}
	</script>
	
	<script type="text/javascript">
		function pop(link) { 
			window.open(link,'pop',"height=500,width=700,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=auto,resizable=yes,copyhistory=no"); 
		}
	</script>
	
	</body>
	<%@ include file="/include/page/footer.jsp"%>