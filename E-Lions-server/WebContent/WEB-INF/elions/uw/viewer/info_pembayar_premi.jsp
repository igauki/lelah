<%@ include file="/include/page/taglibs.jsp"%>
<table class="entry2">
<c:choose>
	<c:when test="${empty cmd.ppremi}">
		<div id="error" align="center">
			Tidak Ada Data Calon Pembayar Premi
		</div>
	</c:when>
	<c:otherwise>
			<tr>
					<th nowrap="nowrap" colspan="5">
						Calon Pembayar Premi adalah
						<select disabled>
							<c:forEach var="d" items="${select_cpp}">
								<option
									<c:if test="${cmd.ppremi.lsre_id_premi eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
					</th>
				</tr>
				<tr>
					<th nowrap="nowrap">
						Nama Pembayar Premi
					</th>
					<th nowrap="nowrap" class="left" colspan="2">
						<select name="pemegang.lti_id" disabled="disabled">
							<option value=""></option>
							<c:forEach var="l" items="${select_gelar}">
								<option
									<c:if test="${cmd.pemegang.lti_id eq l.ID}"> SELECTED </c:if>
									value="${l.ID}">${l.GELAR}</option>
							</c:forEach>
						</select>				
						<input type="text" value="${cmd.ppremi.perusahaan}" size="60" readonly>
						<%-- Ibu Kandung
						<input type="text"	value="${cmd.pemegang.mspe_mother}" size="40" readonly> --%>
					</th>
			</tr>
			<tr>
					<th nowrap="nowrap">
						Bukti Identitas
					</th>
					<th nowrap="nowrap" class="left" colspan="2">
						<input type="text" value="${cmd.pemegang.mspe_no_identity}" size="30" readonly> /
					</th>
			</tr>
			<tr>
					<th nowrap="nowrap">
						Alamat Perusahaan
					</th>
					<th nowrap="nowrap" class="left">
					<textarea rows="2" cols="70" readonly>${cmd.ppremi.alamat_perusahaan}</textarea></th>
					<th align="left">
					Kode Pos<input type="text" value="${cmd.ppremi.kdpos_perusahaan}" size="9" readonly>
					Kota<input type="text" value="${cmd.ppremi.kota_perusahaan}" size="20" readonly><br>
					No. Telepon
					<select >
						<OPTGROUP label="Telepon">
							<c:if test="${not empty cmd.ppremi.telp_rumah}"><option>${cmd.ppremi.area_code_rumah} - ${cmd.ppremi.telp_rumah}</option></c:if>
						</OPTGROUP>
						 <OPTGROUP selected label="Handphone">
							<c:if  test="${not empty cmd.ppremi.no_hp}"><option selected >${cmd.ppremi.no_hp} </option></c:if>
						</OPTGROUP>
						<OPTGROUP label="Fax">
							<c:if test="${not empty cmd.ppremi.no_fax}"><option>${cmd.ppremi.area_code_fax} - ${cmd.ppremi.no_fax}</option></c:if>
						</OPTGROUP>
					</select>
					</th>
			</tr>
			
			<tr>
				<th>Bidang Usaha</th>
				<th align="left" colspan="4"><select disabled>
							<c:forEach var="relasi" items="${select_pekerjaan}">
								<option
									<c:if test="${cmd.ppremi.mkl_kerja eq relasi.key}"> SELECTED </c:if> 
									value="${relasi.key}">${relasi.value}</option>
							</c:forEach>
						</select>
				Tempat/Tanggal Berdiri
						<input type="text" value="${cmd.ppremi.tempat_lahir}" size="25" readonly> / 
						<input type="text" value="<fmt:formatDate value="${cmd.ppremi.mspe_date_birth_3rd_pendirian}" pattern="dd/MM/yyyy"/>" size="12" readonly>
					</th>
			</tr>
			<tr>
				<th nowrap="nowrap">Penghasilan Dan Sumber Penghasilan Calon Pembayar Premi</th>
				<th nowrap="nowrap" class="left" colspan="2">
					<select disabled style="width: 200px;">
						<c:forEach var="penghasilan" items="${select_penghasilan}">
							<option
								<c:if test="${cmd.ppremi.bulan_penghasilan eq penghasilan.ID}"> SELECTED </c:if>
								value="${penghasilan.ID}">${penghasilan.PENGHASILAN}</option>
						</c:forEach>
					</select>
				</th>
			</tr>
	
	<tr>
			<th nowrap="nowrap">Sumber Pendapatan Rutin</th>
			<th nowrap="nowrap" class="left" colspan="2">
				<select name="pemegang.mkl_pendanaan" disabled style="width: 200px;">
	<!-- 			x -->
					<c:forEach var="dana" items="${select_dana}">
						<option
							<c:if test="${cmd.ppremi.tujuan_dana eq dana.ID}"> SELECTED </c:if>
							value="${dana.ID}">${dana.DANA}
						</option>
					</c:forEach>
				</select>
					Lainnya
				<input style="width: 311px;" type="text" value="${cmd.ppremi.tujuan_dana}" readonly>
			</th>
	</tr>
	
	<c:forEach var="listkyc" items="${cmd.ppremi.daftarKyc}" varStatus="status">
	<tr>
		<c:if test="${listkyc.kyc_id eq 7}">
			<th nowrap="nowrap">Jumlah Total pendapatan rutin per bulan</th>
			<th nowrap="nowrap" class="left" colspan="2"><input type="text" value="${listkyc.kyc_desc}" size="60" readonly></th>
		</c:if>
		<c:if test="${listkyc.kyc_id eq 8}">
			<th nowrap="nowrap">Jumlah Total pendapatan non-rutin per tahun</th>
			<th nowrap="nowrap" class="left" colspan="2"><input type="text" value="${listkyc.kyc_desc}" size="60" readonly></th>
		</c:if>
		<c:if test="${listkyc.kyc_id eq 3}">
			<th nowrap="nowrap">Sumber pendapatan rutin per bulan</th>
			<th nowrap="nowrap" class="left" colspan="2"><input type="text" value="${listkyc.kyc_desc}" size="60" readonly></th>
		</c:if>
		<c:if test="${listkyc.kyc_id eq 4}">
			<th nowrap="nowrap">Sumber pendapatan non rutin per tahun</th>
			<th nowrap="nowrap" class="left" colspan="2"><input type="text" value="${listkyc.kyc_desc}" size="60" readonly></th>
		</c:if>
		<c:if test="${listkyc.kyc_id eq 5}">
			<th nowrap="nowrap">Tujuan Pengajuan Asuransi</th>
			<th nowrap="nowrap" class="left" colspan="2"><input type="text" value="${listkyc.kyc_desc}" size="100" readonly></th>
		</c:if>
	</tr> 		
	</c:forEach>
	</c:otherwise>
</c:choose>
</table>
	<%-- <fieldset>
					<legend>Data Pihak Ketiga </legend>
					<table class="entry2">
							<tr align="left">
							<th nowrap="nowrap" colspan="5">
								Nama Pihak Ketiga
							
							<input type="text" value="${cmd.ppremi.nama_pihak_ketiga}" size="60" readonly></th>
						</tr>
						
						<tr>
							<th align="left" colspan="5">
							Tempat/Tanggal Lahir
							<input type="text" value="${cmd.ppremi.tempat_lahir_3rd}" size="25" readonly> / 
							<input type="text" value="<fmt:formatDate value="${cmd.ppremi.mspe_date_birth_3rd}" pattern="dd/MM/yyyy"/>" size="12" readonly>
							NPWP <input type="text" value="${cmd.ppremi.no_npwp}" size="25" readonly>
						</th>
			</tr>
			<tr>
			<th align="left" colspan="5"> Pekerjaan
			<select disabled>
							<c:forEach var="relasi" items="${select_pekerjaan}">
								<option
									<c:if test="${cmd.ppremi.pekerjaan eq relasi.key}"> SELECTED </c:if> 
									value="${relasi.key}">${relasi.value}</option>
							</c:forEach>
						</select></th>
			</tr>
			<tr>
				<th align="left" colspan="5">Bidang Usaha
				<input type="text" value="${cmd.ppremi.usaha_berbadan_hukum}" size="60" readonly>
				</th>
			</tr>
			<tr>
				<th align="left" colspan="5">Sumber Pendanaan
				<input type="text" value="${cmd.ppremi.sumber_dana}" size="60" readonly>
				Tujuan Pendanaan<input type="text" value="${cmd.ppremi.tujuan_dana_3rd}" size="60" readonly>
				</th>
			</tr>
	</table>
	</fieldset> --%>