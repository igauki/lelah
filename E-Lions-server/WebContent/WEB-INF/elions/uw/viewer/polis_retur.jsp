<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	    $().ready(function() {
		$("#tabs").tabs();
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		$("#resi").hide();
		$("#noresi").hide();
		
		$("#proses").change(function() {
			var proses=$("#proses").val();
			if(proses=="02"){
				$("#resi").show();
				$("#noresi").show();
				$("#inputalasan").hide();
			}else{
				$("#resi").hide();
				$("#noresi").hide();
				$("#inputalasan").show();
			}
		});	
		$("#send").click(function() {
			
			var dist=$("#proses").val();
			
			var alasan=$("#alasan").val();
			var resi=$("#resi").val();
			
		   if(dist=="00"){
			      alert('Silahkan Pilih Distribusi terlebih dahulu');
				  return false;	
			}else if((alasan==null || alasan=="" ) && dist=="01" ){
				 alert('Silahkan Isi alasan');
				  return false;	
			}else if((resi==null || resi=="" ) && dist=="02" ){
				alert('Silahkan Input no resi');
				return false;	
			}else{
				 	$("#send").val("Loading....Please Wait...!");
				 	return true;
			}
		});	
		
		$("#showPDF").click(function() {
			
			var dist=$("#proses1").val();
		
			
			
		   if(dist=="00"){
			     alert('Silahkan Pilih Distribusi terlebih dahulu');
				  return false;	
			}
			});	
		
		$("#showXLS").click(function() {
			
			var dist=$("#proses1").val();
			
			
			
		   if(dist=="00"){
			     alert('Silahkan Pilih Distribusi terlebih dahulu');
				  return false;	
			}
		});	
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}	
		var pesan = '${pesan}';
		if(pesan!=null && pesan !=''){
			alert(pesan);
		}
	  
		var index = $('div#tabs li a').index($('a[href="#tab-${showTab}"]').get(0));
				$('div#tabs').tabs({selected:index});
	});
	
	
	
</script>


	<style type="text/css">
		/* font utama */
		body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }
	
		/* fieldset */
		fieldset { margin-bottom: 1em; padding: 0.5em; }
		fieldset legend { width: 99%; }
		fieldset legend div { margin: 0.3em 0.5em; }
		fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
		fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
		fieldset .rowElem .jtable { position: relative; left: 12.5em; }
	
		/* tanda bintang (*) untuk menandakan required field */
		em { color: red; font-weight: bold; }
	
		/* agar semua datepicker align center, dan ukurannya fix */
		.datepicker { text-align: center; width: 7em; }
	
		/* styling untuk client-side validation error message */
		#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }
	
		/* styling untuk label khusus checkbox dan radio didalam rowElement */
		fieldset .rowElem label.radioLabel { width: auto; }
		
		/* lebar untuk form elements */
		.lebar { width: 24em; }
		
		/* untuk align center */
		.tengah { text-align: center; }
	
	</style>

	<body>
	<label>No SPAJ:</label> 
	 <input name="spaj" id="spaj" type="text" title="" value="${spaj}" size="11" readonly="readonly"> 
		<div id="tabs">		
				<ul>
					<li><a href="#tab-1">Proses Polis Retur</a></li>
					<li><a href="#tab-2">Report </a></li>				
			    </ul>
				<div id="tab-1">				
					<form id="formPost" name="formPost" method="post">
							
									   <div class="rowElem" id="dropdown"> 
					 						<label>Jenis Proses :</label> 
					 							  <select  name="proses" id="proses" title="Silahkan pilih jenis proses">												
					 										<c:forEach var="c" items="${listproses}" varStatus="s">
																<option value="${c.key}">${c.value}</option>
															 </c:forEach> 
					 							  </select>															
										</div>
										<br>					
									<div class="rowElem">
										<label>Pilih Tanggal :</label>
										<input name="bdate" id="bdate" type="text" class="datepicker" title="Tanggal Terima/Tanggal Kirim" value="${bdate}">
									</div>
									<br>	 
									<div class="rowElem" id="inputalasan">
										<label>Input Alasan :</label>
										<textarea  maxlength="200" id="alasan" name="alasan" style=" width: 300px;"></textarea>										 	
									</div>
									<br>
									<div class="rowElem">
										<label id="noresi">No.Resi Baru :</label>
										<input name="resi" id="resi" type="text"  title="No.Resi">
										
									</div> 	 
									<br>
									<div class="rowElem"> 
										<input type="submit" name="send" id="send" value="Send">											
										<input type="hidden" name="sending" value="1"> 
									</div>	
									
									 												
										
														
					 </form>			
				</div>
				<div id="tab-2">
					<form id="formPost1" name="formPost1" method="post" target="_blank"  >									
							<fieldset class="ui-widget ui-widget-content">									
														
									<div class="rowElem">
										<label>Periode Tanggal Kirim/Retur:</label>
										<input name="bdate1" id="bdate1" type="text" class="datepicker" title="Tanggal Awal" value="${bdate}"> s/d 
										<input name="edate1" id="edate1" type="text" class="datepicker" title="Tanggal Akhir" value="${edate}">
									</div> 
									
						 								
								    <div class="rowElem"> 
										<input type="submit" name="showPDF" id="showPDF" value="Show (PDF)">
										<input type="submit" name="showXLS" id="showXLS" value="Show (Excel)">									
										<input type="hidden" name="showReport" value="1"> 
									</div>	 	
													
							</fieldset>						
				  </form>
			</div>				
		</div>	
	 </body>
</html>


