<%@ include file="/include/page/header.jsp"%>

<script type="text/javascript">
	hideLoadingMessage();
	

function Right(str, n){
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}


function DateAdd(objDate, intDays)
{
  var iSecond=1000;	 // Dates are represented in milliseconds
  var iMinute=60*iSecond;
  var iHour=60*iMinute;
  var iDay=24*iHour;
  var objReturnDate=new Date();
  objReturnDate.setTime(objDate.getTime()+(intDays*iDay));
  return objReturnDate;
}

function listtgl()
{
	var tanggal_beg = document.frmParam.tgl1.value;
	
	var arrMonthDays = new Array (	"31", "29", "31",
					"30", "31", "30",
					"31", "31", "30",
					"31","30","31"	  );

		var tahun_s = tanggal_beg.substring(6,10);
		var bulan_s = tanggal_beg.substring(3,5);
		var tanggal_s = tanggal_beg.substring(0,2);
		var tanggal  = tanggal_s+"/"+bulan_s+"/"+tahun_s;
		
	if ((bulan_s==2)&&  (tahun_s%4>0) ) {
	    tanggal_s=28;
	}else{
		tanggal_s=arrMonthDays[bulan_s-1];
	}	
	tanggal  = tanggal_s+"/"+bulan_s+"/"+tahun_s;

	document.frmParam._tgl2.value=tanggal;
	document.frmParam.tgl2.value=tanggal;
	document.frmParam.tgl3.value=tanggal;

}

function listtgl2()
{
	tanggal = document.frmParam._tgl2.value;
	document.frmParam._tgl2.value=tanggal;
	document.frmParam.tgl2.value=tanggal;
	document.frmParam.tgl3.value=tanggal;

}	
	
function cari(str){
	switch (str) {
	case "perkodeagen" : 
		document.getElementById('infoFrame').src='${path}/uw/viewer/komisi.htm?window=perkodeagen'; 
		break;
	case "pertglbayar" :
		document.getElementById('infoFrame').src='${path}/uw/viewer/komisi.htm?window=pertglbayar'; 
		break;
	case "pertglproduksi" :
		document.getElementById('infoFrame').src='${path}/uw/viewer/komisi.htm?window=pertglproduksi'; 
		break;
	case "percabang" :
		document.getElementById('infoFrame').src='${path}/uw/viewer/komisi.htm?window=percabang'; 
		break;
	case "perttp" :
		document.getElementById('infoFrame').src='${path}/uw/viewer/komisi.htm?window=perttp'; 
		break;	
	}
}

function caridaftar1()
{
	if ( document.frmParam.kode_agen.value=="" )
	{
		alert('Silahkan isi kode agen terlebih dahulu');
	}else{
		frmParam.submit();
	}
}

function caridaftar2()
{
 	if ( document.frmParam.kode_agen.value=="")
		{
			alert('Silahkan isi kode agen terlebih dahulu');
		}else{
			if ( document.frmParam.tgl1.value=="__/__/____" || document.frmParam.tgl1.value=="" || document.frmParam.tgl2.value=="__/__/____" || document.frmParam.tgl2.value=="")
			{
				alert('Silahkan isi tanggal terlebih dahulu');
			}else{
				var tanggal_beg = document.frmParam.tgl1.value;
				var tahun_s = tanggal_beg.substring(6,10);
				var bulan_s = tanggal_beg.substring(3,5);
				var tanggal_s = tanggal_beg.substring(0,2);
				var tanggal1  = tahun_s+bulan_s+tanggal_s;
				var tanggal_end = document.frmParam.tgl2.value;
				tahun_s = tanggal_end.substring(6,10);
				bulan_s = tanggal_end.substring(3,5);
				tanggal_s = tanggal_end.substring(0,2);
				var tanggal2  = tahun_s+bulan_s+tanggal_s;
				frmParam.submit();
			}
		}
	
}
	
function caridaftar4()
{
 	if ( document.frmParam.cabang.value=="")
		{
			alert('Silahkan pilih cabang terlebih dahulu');
		}else{
			if ( document.frmParam.tgl1.value=="__/__/____" || document.frmParam.tgl1.value=="" || document.frmParam.tgl2.value=="__/__/____" || document.frmParam.tgl2.value=="")
			{
				alert('Silahkan isi tanggal terlebih dahulu');
			}else{
				var tanggal_beg = document.frmParam.tgl1.value;
				var tahun_s = tanggal_beg.substring(6,10);
				var bulan_s = tanggal_beg.substring(3,5);
				var tanggal_s = tanggal_beg.substring(0,2);
				var tanggal1  = tahun_s+bulan_s+tanggal_s;
				var tanggal_end = document.frmParam.tgl2.value;
				tahun_s = tanggal_end.substring(6,10);
				bulan_s = tanggal_end.substring(3,5);
				tanggal_s = tanggal_end.substring(0,2);
				var tanggal2  = tahun_s+bulan_s+tanggal_s;
				frmParam.submit();
			}
		}
	
}	
</script>
<body style="height: 100%;"
	onload="setFrameSize('infoFrame', 68);"
	onresize="setFrameSize('infoFrame', 68);">
<form name="frmParam" method="post">

<div class="tabcontent">
<c:if test="${kunci eq null}"> 
<table class="entry" width="98%">
	<tr>
		<th> Window Komisi Agen </th>
		<td>
		<input type="button" value="KOMISI PER KODE AGEN" name="search" onClick="cari('perkodeagen');"> 
		 <input type="button" value="KOMISI PER TANGGAL BAYAR" name="search1" onClick="cari('pertglbayar');">
		<input type="button" value="KOMISI PER TANGGAL PRODUKSI" name="search3" onClick="cari('pertglproduksi');">
		<input type="button" value="KOMISI PER CABANG" name="search2" onClick="cari('percabang');">
		<input type="button" value="KOMISI PER TANGGAL TERIMA POLIS" name="search4" onClick="cari('perttp');">
		</td>
		<td><div id="prePostError" style="display:none;"></div></td>
	</tr>
	<tr>
		<td colspan="3">
			<iframe src="${path}/uw/viewer/komisi.htm?window=main" name="infoFrame" id="infoFrame"
				width="100%"> Please Wait... </iframe>
		</td>
	</tr>
</table>
</c:if>
<c:if test="${kunci eq \"perkode\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  > VIEW KOMISI PER KODE AGEN
	   </th>
      </tr>
      <tr> 
        <th width="13%" >Kode Agen</th>
        <th width="83%">
		<input type="text" name="kode_agen" value="${kode_agen}" >
		</th>
      </tr>	  
      <tr> 
        <th colspan="2" ><input type="button" value="Cari" name="search" onClick="caridaftar1();"> 
		</th>
      </tr>
      <tr> 
        <td> </td>
      </tr>

    </table>
</c:if>
<c:if test="${kunci eq \"pertglbayar\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  >VIEW KOMISI PER TANGGAL BAYAR
	   </th>
      </tr>
      <tr> 
        <th width="13%" >Kode Agen</th>
        <th width="83%">
		<input type="text" name="kode_agen" value="${kode_agen}" >
		</th>
      </tr>	 
  <tr> 
        <th >Tanggal Bayar</th>
        <th>
              <script>inputDate('tgl1', '${tgl1}', false,'listtgl();');</script>
			s/d
              <script>inputDate('tgl2', '${tgl2}', false,'listtgl2();');</script>
			<input type="hidden" name="tgl3">
		</th>
      </tr>       
      <tr> 
        <th colspan="2" ><input type="button" value="Cari" name="search" onClick="caridaftar2();"> 
		</th>
      </tr>
      <tr> 
        <td> </td>
      </tr>

    </table>
</c:if>
<c:if test="${kunci eq \"pertglproduksi\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  >VIEW KOMISI PER TANGGAL PRODUKSI
	   </th>
      </tr>
      <tr> 
        <th width="13%" >Kode Agen</th>
        <th width="83%">
		<input type="text" name="kode_agen" value="${kode_agen}" >
		</th>
      </tr>	 
  <tr> 
        <th >Tanggal Produksi</th>
        <th>
              <script>inputDate('tgl1', '${tgl1}', false,'listtgl();');</script>
			s/d
              <script>inputDate('tgl2', '${tgl2}', false,'listtgl2();');</script>
			<input type="hidden" name="tgl3">
		</th>
      </tr>       
      <tr> 
        <th colspan="2" ><input type="button" value="Cari" name="search" onClick="caridaftar2();"> 
		</th>
      </tr>
      <tr> 
        <td> </td>
      </tr>

    </table>
</c:if>
<c:if test="${kunci eq \"percabang\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  >VIEW KOMISI PER CABANG
	   </th>
      </tr>
      <tr> 
        <th width="13%" >Cabang</th>
        <th width="83%">
		<select name="cabang" tabindex="14">
					<c:forEach var="r" items="${listcabang}" varStatus="perush"> 
					<option value="${r.KEY}">${r.VALUE}</option> 
					</c:forEach> 
			</select>
		</th>
      </tr>	 
  <tr> 
        <th >Tanggal Bayar</th>
        <th>
              <script>inputDate('tgl1', '${tgl1}', false,'listtgl();');</script>
			s/d
              <script>inputDate('tgl2', '${tgl2}', false,'listtgl2();');</script>
			<input type="hidden" name="tgl3">
		</th>
      </tr>       
      <tr> 
        <th colspan="2" ><input type="button" value="Cari" name="search" onClick="caridaftar4();"> 
		</th>
      </tr>
      <tr> 
        <td> </td>
      </tr>

    </table>
</c:if>
<c:if test="${kunci eq \"perttp\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  >VIEW KOMISI PER TANGGAL TERIMA POLIS
	   </th>
      </tr>
      <tr> 
        <th width="13%" >Kode Agen</th>
        <th width="83%">
		<input type="text" name="kode_agen" value="${kode_agen}" >
		</th>
      </tr>	 
  <tr> 
        <th >Tanggal Terima Polis</th>
        <th>
              <script>inputDate('tgl1', '${tgl1}', false,'listtgl();');</script>
			s/d
              <script>inputDate('tgl2', '${tgl2}', false,'listtgl2();');</script>
			<input type="hidden" name="tgl3">
		</th>
      </tr>       
      <tr> 
        <th colspan="2" ><input type="button" value="Cari" name="search" onClick="caridaftar2();"> 
		</th>
      </tr>
      <tr> 
        <td> </td>
      </tr>

    </table>
</c:if>
</div>
</form>
</body>

<%@ include file="/include/page/footer.jsp"%>