<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/disableKeys.js"></script>
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
	</head>
	<body style="height: 100%;text-align: center;" onload='setupPanes("container1", "tab1");'>

	<c:choose>
		<c:when test="${empty spaj }">
			<div id="success">Silahkan cari nomor SPAJ terlebih dahulu</div>
		</c:when>
		<c:otherwise>
			<fieldset>
				<table class="entry2">
					<tr>
						<th class="left">
							No. Polis
							<input readonly type=text size=20 value="<elions:polis nomor="${polis}"/>">
							No. SPAJ
							<input readonly type=text size=15 value="<elions:spaj nomor="${spaj}"/>">
							No. Kartu
							<input readonly type=text size=25 value="<elions:polis nomor="${nokartu}"/>">
						</th>
					</tr>
					<tr>
						<th class="left">
							Posisi Dokumen
							<input readonly type=text size=60 value="<elions:spaj nomor="${info.LSPD_POSITION}"/>">
							Status Aksep
							<input readonly type=text size=30 value="<elions:polis nomor="${info.STATUS_ACCEPT}"/>">
						</th>
					</tr>
				</table>
			</fieldset>

			<div class="tab-container" id="container1">
				<ul class="tabs">
					<li>
						<a href="#" onClick="return showPane('pane1', this)"  
						onmouseover="return overlib('Alt-1', AUTOSTATUS, WRAP);" onmouseout="nd();"
						onfocus="return showPane('pane1', this)" accesskey="1" id="tab1">Posisi Pas</a>
					</li>
					<li>
						<a href="#" onClick="return showPane('pane2', this)"  
						onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();"
						onfocus="return showPane('pane2', this)" accesskey="2">Posisi Fire</a>
					</li>
				</ul>
	
				<div class="tab-panes">
	
					<div id="pane1" class="panes">
						<display:table id="baris" name="daftar0" class="displaytag" requestURI="${requestScope.requestURL}" export="true">
							<display:caption>Posisi PAS</display:caption>
								<display:column property="reg_id" title="Reg Id" />
								<display:column property="msps_date" title="Tanggal" format="{0, date, dd/MM/yyyy (HH:mm:ss)}"  />
								<display:column property="msps_desc" title="Posisi Dokumen"  />
								<display:column property="lus_login_name" title="User"  />
						</display:table>
					</div>
	
					<div id="pane2" class="panes">
						<display:table id="baris" name="daftar1" class="displaytag" requestURI="${requestScope.requestURL}" export="true">
							<display:caption>Posisi Fire</display:caption>
								<display:column property="reg_id" title="Reg Id" />
								<display:column property="msps_date" title="Tanggal" format="{0, date, dd/MM/yyyy (HH:mm:ss)}"  />
								<display:column property="msps_desc" title="Posisi Dokumen"  />
								<display:column property="lus_login_name" title="User"  />
						</display:table>
					</div>
	
				</div>
			</div>
		</c:otherwise>
	</c:choose>
	</body>
	<%@ include file="/include/page/footer.jsp"%>