<%@ include file="/include/page/taglibs.jsp"%>
<fieldset>
    <legend>Viewer Endorse</legend>
    <table class="entry2">
        <tr align ="left">
        <th>No. Polis :<input readonly type=text style="background-color: '#FFFFCC';" size=25 value="<elions:spaj nomor="${cmd.endorseInfo.MSPO_POLICY_NO }"/>"></th>                    
        <th>&nbsp;&nbsp;&nbsp;&nbsp;No. SPAJ :<input readonly type="text" size="16" style="background-color: '#FFFFCC';" value="<elions:spaj nomor="${cmd.endorseInfo.REG_SPAJ }"/>">
        &nbsp;&nbsp;&nbsp;&nbsp;By :<input readonly type="text" style="background-color: '#FFFFCC';" value="${cmd.endorseInfo.LUS_LOGIN_NAME}"></th>
        </tr>
    </table>
     <table class="entry2">
        <tr align ="left">
            <th>Sumber :&nbsp;&nbsp;<input readonly type="text" style="background-color: '#FFFFCC';" size="12" value="${cmd.endorseInfo.MSEN_INTERNAL}">
            &nbsp;&nbsp;Active / Transfer Date :&nbsp;&nbsp;
            <input readonly type="text" style="background-color: '#FFFFCC';" size="16" value="<fmt:formatDate value="${cmd.endorseInfo.MSEN_ACTIVE_DATE }" pattern="dd/MM/yyyy"/> ">
            &nbsp;&nbsp;<input readonly type="text" style="background-color: '#FFFFCC';" value="<fmt:formatDate value="${cmd.endorseInfo.MSEN_TGL_TRANS }" pattern="dd/MM/yyyy (hh:mm)"/>">
             </th>
        </tr>
    </table>
     <fieldset>
    <legend>Data Viewer Endors</legend>
    <c:if test="${not empty cmd.listendors}">
        <display:table id="endors" name="cmd.listendors" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator">
            <display:column property="ENDORSNO" title="No Endorse" style="background-color: '#FFFFCC';" />
            <display:column property="SPAJ" title="No Spaj" style="background-color: '#FFFFCC';" />
            <display:column property="INPUT" title="Input Date" format="{0, date, dd/MM/yyyy}" style="background-color: '#FFFFCC';" />
            <display:column property="ACTIVE" title="Active Date" format="{0, date, dd/MM/yyyy}" style="background-color: '#FFFFCC';"  />
            <display:column property="POSISI" title="Posisi" style="background-color: '#FFFFCC';" />
            <display:column property="PERUBAHAN" title="Jenis Perubahan"  style="background-color: '#FFFFCC';"/>
            <display:column property="USER_PROSES" title="User Yang Proses" style="background-color: '#FFFFCC';" />
            <display:column property="ALASAN" title="Alasan" style="background-color: '#FFFFCC';"/>
            </display:table>        
        </c:if>
        </fieldset>
 </fieldset>    