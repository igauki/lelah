<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		
		$("#userBas").hide();	
		
		// user memilih cabang, maka tampilkan wakil dan region nya
		$("#cabang").change(function() {
			$("#wakil").empty();
			var url = "${path}/report/bas.htm?window=reportsimascardcabang&json=1&lca=" +$("#cabang").val();
			$.getJSON(url, function(result) {
				$("<option/>").val("ALL").html("ALL").appendTo("#wakil");
				$.each(result, function() {
					$("<option/>").val(this.key).html(this.value).appendTo("#wakil");
				});
				$("#wakil option:first").attr('selected','selected');
			});
			
			$("#region").empty();
			var url2 = "${path}/report/bas.htm?window=reportsimascardcabang&json=2&lca=" + $("#cabang").val();
			$.getJSON(url2, function(result2) {
				$("<option/>").val("ALL").html("ALL").appendTo("#region");
				$.each(result2, function() {
					$("<option/>").val(this.key).html(this.value).appendTo("#region");
				});
				$("#region option:first").attr('selected','selected');
			});
			
			//kopi untuk disubmit (isinya, bukan valuenya)
			$("#cabang2").val($("#cabang option:selected").text());
			$("#wakil2").val("ALL");
			$("#region2").val("ALL");
	
		});

		// user memilih wakil, maka tampilkan region nya
		$("#wakil").change(function() {
			$("#region").empty();
			var url2 = "${path}/report/bas.htm?window=reportsimascardcabang&json=2&lca=" + $("#cabang").val() + "&lwk=" + $("#wakil").val();
			$.getJSON(url2, function(result2) {
				$("<option/>").val("ALL").html("ALL").appendTo("#region");
				$.each(result2, function() {
					$("<option/>").val(this.key).html(this.value).appendTo("#region");
				});
				$("#region option:first").attr('selected','selected');
			});
			
			//kopi untuk disubmit (isinya, bukan valuenya)
			$("#wakil2").val($("#wakil option:selected").text());
			$("#region2").val("ALL");
			
		});

		$("#region").click(function() {
			//kopi untuk disubmit (isinya, bukan valuenya)
			$("#region2").val($("#region option:selected").text());
		});	
		
		 $("#branch").click(function() {
// 	      	 $("#dropdown").attr("disabled", false);
	         $("#dropdown").show();   //To Show the dropdown
	         $("#userBas").hide();
         });
                    
         $("#user").change(function() {
	//       $("#dropdown").attr("disabled", true);
	         $("#dropdown").hide();//To hide the dropdown
	         var mode = '${mode}';
	         if(mode==1){
				 $("#userBas").show();
				  $(".chxbox3").attr("checked", false);
				 //document.getElementById('userBas').style.display = 'block';
			 }else{
				 $("#userBasOnly").show();
				 $(".chxbox2").attr("checked", false);
				 //document.getElementById('userBas').style.display = 'none';
			 }
         });	
                    
		 $("#jumlah").change(function() {
//       	 $("#dropdown").show();   //To Show the dropdown
	 		 $("#dropdown").hide();
	         $("#userBas").hide();
         });	
                    
		//autocomplete user
		$( "#seluserBas2" ).autocomplete({
			minLength: 3,
			source: "${path}/common/json.htm?window=getUser",
			focus: function( event, ui ) {
				$( "#seluserBas2" ).val( ui.item.value );
				return false;
			},
			select: function( event, ui ) {
				$( "#seluserBas2" ).val( ui.item.value);
				$( "#seluserBas" ).val( ui.item.key);
				return false;
			}
		})
		
		.data( "autocomplete" )._renderItem = function( ul, item ) {
			return $( "<li></li>" )
				.data( "item.autocomplete", item )
				.append( "<a>" + item.value + "</a>" )
				.appendTo( ul );
		};
		
		$("#seluserBas2").change(function() {
			if($("#seluserBas2").val().toUpperCase()=='ALL'){
				$("#seluserBas").val('');
			}else if($("#seluserBas2").val()==''){
				$("#seluserBas").val('');
			}
		});
	});
	
	function test1(){
		if ($(".chxbox1").is(':checked')){
			$(".byk1").hide();
			$('.cb').attr('checked', false); // Unchecks it
		}else{
			$(".byk1").show();
		}
	}
	
	function test2(){
		if ($(".chxbox2").is(':checked')){
			$(".byk2").hide();
		}else{
			$(".byk2").show();
		}
	}

</script>


<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }
	
	.dub {margin: 5px;}
	#users {width: auto;}
	.chxbox1 {padding : 10px;}
	.chxbox2 {padding : 10px;}

</style>

<body>

<form id="formPost" name="formPost" method="post" target="_blank">
<fieldset class="ui-widget ui-widget-content">
	<legend class="ui-widget-header ui-corner-all"><div>Summary Penginputan</div></legend>

	<div class="rowElem" id="dropdown">
		<label>Cabang/Wakil/Region:</label>
		<select size="10" name="cabang" id="cabang" title="Silahkan pilih Cabang">
			<option value="ALL" selected="selected">ALL</option>
			<c:forEach var="c" items="${listCabang}" varStatus="s">
				<option value="${c.key}">${c.value}</option>
			</c:forEach>
		</select>
		<select size="10" name="wakil" id="wakil" title="Silahkan pilih Kantor Perwakilan">
			<option value="ALL" selected="selected">ALL</option>
			<c:forEach var="c" items="${listWakil}" varStatus="s">
				<option value="${c.key}">${c.value}</option>
			</c:forEach>
		</select>
		<select size="10" name="region" id="region" title="Silahkan pilih Region">
			<option value="ALL" selected="selected">ALL</option>
			<c:forEach var="c" items="${listRegion}" varStatus="s">
				<option value="${c.key}">${c.value}</option>
			</c:forEach>
		</select>
	</div>
	
	<div class="rowElem" id="userBas" style="display: none;">
		<%--<label>User :</label>--%>
		<%-- <select name="seluserBas" id="seluserBas" title="Silahkan pilih User">
			<option value="ALL" selected="selected">ALL</option>
			<c:forEach var="u" items="${userBas}" varStatus="c">
				<option value="${u.key}">${u.value}</option>
			</c:forEach>
		</select> --%>
		<%--<input name="seluserBas2" id="seluserBas2" size="50" type="text" title="User" value="">
		<input name="seluserBas" id="seluserBas" size="6" type="hidden" title="User" value="">--%>
		
		<fieldset> 
			<legend id="users"><strong>Product:</strong></legend>
			<div><input class="chxbox1" name="prodbas" type="checkbox" value="ALL" onclick="test1()" checked/>ALL</div>
		 
			<div class="byk1" style="display: none;">
				<fieldset>
					<input class="cb" name="prodbas" type="checkbox" value="BANCASSURANCE"/>Bancassurance
					<input class="cb" name="prodbas" type="checkbox" value="AGENCY"/>Agency
				</fieldset>
			</div>
		</fieldset>
		
		<fieldset> 
			<div id="userBasAll">
			<legend id="users"><strong>User:</strong></legend>
				<div><input class="chxbox2" name="usbas" type="checkbox" value="ALL" onclick="test2()" checked/>ALL</div>
				
			<div class="byk2" style="display: none;">
			<fieldset>
				<table>
						<tr>
							<td>
								<c:forEach var="x" items="${datauser}" begin="0" end="39">
									<div><input name="usbas" type="checkbox" value="${x.LUS_ID}"/>[${x.LUS_ID}] ${x.LUS_FULL_NAME}</div>
								</c:forEach>
							</td>
							<td>
								<c:forEach var="x" items="${datauser}" begin="40" end="79">
									<div><input name="usbas" type="checkbox" value="${x.LUS_ID}"/>[${x.LUS_ID}] ${x.LUS_FULL_NAME}</div>
								</c:forEach>
							</td>
							<td>
								<c:forEach var="x" items="${datauser}" begin="80" end="119">
									<div><input name="usbas" type="checkbox" value="${x.LUS_ID}"/>[${x.LUS_ID}] ${x.LUS_FULL_NAME}</div>
								</c:forEach>
							</td>
							<td>
								<c:forEach var="x" items="${datauser}" begin="120">
									<div><input name="usbas" type="checkbox" value="${x.LUS_ID}"/>[${x.LUS_ID}] ${x.LUS_FULL_NAME}</div>
								</c:forEach>
							</td>
						</tr>	
				</table>	
			</fieldset>
			</div>
			</div>
		</fieldset>
	</div>
	
	<div id="userBasOnly" style="display: none;">
		<fieldset>
			<legend id="users"><strong>User:</strong></legend>
			<div><input class="chxbox3" name="usbas" type="checkbox" value="${user_lus_id}" checked/>${user_name}</div>
		</fieldset>
	</div>
	
	<div class="rowElem">
		<label>Periode Tanggal :</label>
		<input name="bdate" id="bdate" type="text" class="datepicker" title="Tanggal Awal" value="${bdate}"> s/d 
		<input name="edate" id="edate" type="text" class="datepicker" title="Tanggal Akhir" value="${edate}">
	</div>
	
	<div class="rowElem">
		<label>Jenis Summary Input :</label>
		 <input name="jn_report" id="user" type="radio" class="radio" value="0" >Summary Penginputan Berdasarkan User	   
	    <input name="jn_report" id="branch" type="radio" class="radio" value="1" checked="checked">Summary Penginputan Berdasarkan Cabang
	    <input name="jn_report" id="jumlah" type="radio" class="radio" value="2">Summary Penginputan Berdasarkan Jumlah Inputan
	
	</div>
	<div class="rowElem">
		<label></label>
		<input type="submit" name="showPDF" id="showPDF" value="Show (PDF)">
		<input type="submit" name="showXLS" id="showXLS" value="Show (Excel)">
		<input type="hidden" name="cabang2" id="cabang2" value="ALL">
		<input type="hidden" name="wakil2" id="wakil2" value="ALL">
		<input type="hidden" name="region2" id="region2" value="ALL">
		<input type="hidden" name="showReport" value="1">
	</div>
</fieldset>
</form>

</body>
</html>