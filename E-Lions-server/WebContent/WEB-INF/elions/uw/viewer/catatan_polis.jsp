<%@ include file="/include/page/header.jsp"%>

<body>
	<form method="post" name="formpost" id="formpost">
		<h3>Catatan Polis Saya :</h3>
		<b>Nama : </b><input type="text" name="pemegang" id="pemegang" value="${pemegang}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<b>Tanggal Lahir :</b>
				<script>inputDate('bdate', '${bdate}', false);</script>
		&nbsp;<input type="submit" nama="btnView" id="btnView" value="View">
		<input type="hidden" name="btnView" value="1">
		<br/>
		<br/>
		<c:if test="${not empty err}">
			<div class="error">
					${err}
			</div>
		</c:if>
	</form>
	<br>
	<c:if test="${not empty data }">
		<table class="entry2">
			<tr>
				<th>No</th>
				<th>Pemegang</th>
				<th>No Polis</th>
				<th>Billing Account</th>
				<th>Tanggal Jatuh Tempo</th>
				<th>Premi/Cara Bayar</th>
			</tr>
			<c:forEach items="${data }" var="d" varStatus="s">
			<tr>
				<td align="center">${s.index +1 }</td>
				<td align="center">${d.PEMEGANG }</td>
				<td align="center">${d.POLIS }</td>
				<td align="center">${d.ACCOUNT_BILLING }</td>
				<td align="center"><fmt:formatDate value="${d.JT_TEMPO }" pattern="dd/MM/yyyy"/></td>
				<td align="center">${d.KURS } <fmt:formatNumber>${d.PREMI }</fmt:formatNumber>/${d.CARA_BAYAR }</td>
			</tr>
			</c:forEach>
		</table>
	</c:if>
</body>
</html>