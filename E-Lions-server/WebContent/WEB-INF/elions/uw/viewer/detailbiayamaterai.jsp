<%@ include file="/include/page/header_jquery.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path}/include/js/default.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (jQueryUI Tabs) init tab2 Utama
		$("#tabs").tabs();

		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title], label[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
	});
</script>


<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }
	
	/* untuk warna kolom input */
	.warna { color: grey; }
	
	/* styling untuk server-side validation error */
	.errorField { border: 1px solid red; }
	.errorMessage { color: red; display: block;}
</style>

<body>
	<form id="formPost" name="formPost" method="post" target="">
		<fieldset class="ui-widget ui-widget-content" id="fieldset1">
			<legend class="ui-widget-header ui-corner-all">
				<div>List SPAJ Pengajuan Biaya Materai</div>
			</legend>
		<table class="displaytag" cellpadding='2' cellspacing='0' border="1"
			align="center" style="width: 100%;">
			<tr>
				<th rowspan="2" bgcolor="#EAEAEA">No Spaj</th>
				<th rowspan="2" bgcolor="#EAEAEA">No Polis</th>
				<th rowspan="2" bgcolor="#EAEAEA">Nama Pemegang</th>
				<th rowspan="2" bgcolor="#EAEAEA">Nama Tertanggung</th>

			</tr>
			<tr>
			</tr>
			<c:forEach items="${result}" var="s">
					<th>${s.NO_SPAJ }</th>
					<th>${s.NO_POLIS }</th>
					<th>${s.NAMA_PP }</th>
					<th>${s.NAMA_TT }</th>
				</tr>
			</c:forEach>
		</table>
		</fieldset>
	</form>
</body>

</html>
