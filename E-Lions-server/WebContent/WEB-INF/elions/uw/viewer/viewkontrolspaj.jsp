<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript">
	function tampil(tipe, akum){
		
		if(tipe=='')return;
		document.formpost.show.disabled=false;
		document.formpost.show2.disabled=true;
		document.getElementById('pol').style.display='block';
		document.getElementById('filter').style.display='none';
		document.formpost.btnBungaPinjaman.style.display='none';
		document.formpost.btnBungaSimpanan.style.display='none';
		document.formpost.btnBungaTahapan.style.display='none';
		document.formpost.btnIlustrasiDisc.style.display='none';
		
		if(tipe=='0'){ //Pinjaman
			document.formpost.btnBungaPinjaman.style.display='block'; //FIXME BELUM DIKERJAKAN
		}else if(tipe=='1'){ //Simpanan
			document.formpost.btnBungaSimpanan.style.display='block'; //FIXME BELUM DIKERJAKAN
		}else if(tipe=='2'){ //Billing
			document.formpost.btnIlustrasiDisc.style.display='block';
		}else if(tipe=='3'){ //Tahapan
			document.formpost.btnBungaTahapan.style.display='block';
		}else if(tipe=='4'){ //Claim Nilai Tunai
		}else if(tipe=='5'){ //Powersave
		}else if(tipe=='7'){ //Agent
		}else if(tipe=='8'){ //reas
		}else if(tipe=='9'){ //akumulasi polis
			document.getElementById('filter').style.display='block';
			document.getElementById('filter2').style.display='block';
			document.getElementById('pol').style.display='none';
			document.formpost.show.disabled=true;
			document.formpost.show2.disabled=false;
			flag=0;
			if(document.formpost.flag.checked){
				flag=1
			}	
				
			if(akum && document.formpost.pilter2.value!='')
				document.getElementById('infoFrame').src=
					'${path}/uw/viewer.htm?show=true&window=displaytag&tipeView='
					+tipe+'&spaj=${spaj}&pptt='+document.getElementById('pptt').value
					+'&pilter='+escape(document.getElementById('pilter').value)+'&pilter2='+document.getElementById('pilter2').value
					+'&flagTglLahir='+flag+'&tglLahir='+document.formpost.tglLahir.value;
			return;
		}else if(tipe=='10'){ //maturity
		}else if(tipe=='11'){ //privasi
		}else if(tipe=='12'){ //reinstate
		}else if(tipe=='13'){ //komisi
		}else if(tipe=='14'){ //rewards
		}else if(tipe=='15'){ //nav
		}else if(tipe=='16'){ //medis
			document.getElementById('infoFrame').src='${path}/uw/medical_new.htm?spaj=${spaj}&mode=viewer';
			return;
		}
		else if(tipe=='17'){ //bonus
		}else if(tipe=='18'){ //stable link
		}else if(tipe=='19'){ //Deduct
		}

		createLoadingMessage();
		document.getElementById('infoFrame').src='${path}/uw/viewer.htm?show=true&window=displaytag&tipeView='+tipe+'&spaj=${spaj}';
	}
	
	function tekanTgl(){
		if(document.formpost.flag.checked)
			alert("SIlahkan masukan Tanggal Lahir dengan Benar");
	}
	
</script>
<style type="text/css">
	html,body{
	     overflow: hidden;
	}
</style>
<body onload="document.title='PopUp :: Viewer Control SPAJ'; setupPanes('container1', 'tab1'); setFrameSize('infoFrame', 105);" onresize="setFrameSize('infoFrame', 105);" style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Viewer Control SPAJ</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				
				<c:choose>
					<c:when test="${not empty pesanError}">
						<div id="error">${pesanError}</div>
					</c:when>
					<c:otherwise>
						<form method="post" name="formpost">
							<table class="entry2" width="98%">		
								<tr>
									<th>Jenis View: </th>
									<td>
										<select name="tipeView" onchange="tampil(this.value);">
											<option value="-1"></option>
											<c:forEach var="a" items="${tipeViewList}" varStatus="st">
												<option value="${a.key}" 
												<c:if test="${a.key eq tipeView}">selected</c:if> 
												>${a.value}</option>
											</c:forEach>
										</select>
										<input type="button" name="show" value="Show" onclick="tampil(document.formpost.tipeView.value);">
										<input type="button" name="close" value="Close" onclick="window.close();">
									</td>
								</tr>
								<tr>
									<th>Info:</th>
									<td>
										<input type="button" name="btnBungaPinjaman" value="Bunga Pinjaman" style="display:none;" onclick="popWin('${path}/uw/viewer.htm?window=bunga_pinjaman&spaj=${spaj}', 400, 520);">
										<input type="button" name="btnBungaSimpanan" value="Bunga Simpanan" style="display:none;">
										<input type="button" name="btnBungaTahapan" value="Bunga Tahapan" style="display:none;">
										<input type="button" name="btnIlustrasiDisc" value="Ilustrasi Disc" style="display:none;" onclick="popWin('${path}/uw/viewer.htm?window=ilustrasi_disc&spaj=${spaj}', 200, 600);">
									</td>
								</tr>
								<tr>
									<th>
									</th>
									<td>
										<div id="pol">
											Polis: [<elions:polis nomor="${info.MSPO_POLICY_NO}"/>]
											<c:if test="${not empty otherSpaj }">
												SPAJ: 
												<c:forEach var="a" items="${otherSpaj }">
													[<elions:spaj nomor="${a}"/>]
												</c:forEach>
											</c:if>
											Pemegang: [${info.MCL_FIRST}]
										</div>
										<div id="filter" style="display:none;">
											<select name="pptt">
												<option value="2">Pemegang</option>
												<option value="3">Tertanggung</option>
											</select>
											<select name="pilter">
												<option <c:if test="${\"LIKE%\" eq param.pilter}">selected</c:if> value="LIKE&#37">LIKE%</option>
												<option <c:if test="${\"%LIKE\" eq param.pilter}">selected</c:if> value="&#37LIKE">%LIKE</option>
												<option <c:if test="${\"%LIKE%\" eq param.pilter}">selected</c:if> value="&#37LIKE&#37">%LIKE%</option>
												<option <c:if test="${\"LT\" eq param.pilter}">selected</c:if> value="LT"><</option>
												<option <c:if test="${\"LE\" eq param.pilter}">selected</c:if> value="LE"><=</option>
												<option <c:if test="${\"EQ\" eq param.pilter}">selected</c:if> value="EQ">=</option>
												<option <c:if test="${\"GE\" eq param.pilter}">selected</c:if> value="GE">>=</option>
												<option <c:if test="${\"GT\" eq param.pilter}">selected</c:if> value="GT">></option>
											</select>
											<input type="text" name="pilter2" value="${param.pilter2 }">
											<input type="button" name="show2" value="Show" onclick="createLoadingMessage(); tampil(document.formpost.tipeView.value, 'yes');">
										</div>
									</td>
								</tr>
								<tr>
									<th>
									</th>
									<td>
										<div id="filter2" style="display:none;">
											<input type="checkbox" class="noBorder" onClick="tekanTgl();" name="flag">AND
											<script>inputDate('tglLahir', '00/00/0000', false, '', 9);</script>
										</div>
									</td>
								</tr>
							</table>
							<iframe name="infoFrame" id="infoFrame"
								width="100%" frameborder="YES">-</iframe>
						</form>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>