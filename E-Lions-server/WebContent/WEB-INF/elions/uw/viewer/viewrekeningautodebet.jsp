<%@ include file="/include/page/header.jsp"%>
<body onload="document.title='PopUp :: Rekening Nasabah';setupPanes('container1','tab1');" style="height: 100%;">
	<c:if test="${not empty cmd.rekening.lsbp_id}">
		<script type="text/javascript">
			/// nanti di cek lg
			//ajaxPjngRek('${cmd.rekening.lsbp_id}', 1,'mar_acc_no');ajaxPjngRek(split(this.value), 1,'mrc_no_ac_split')"
		</script>	
	</c:if>
	
	<script type="text/javascript">
		function dofo(index, flag) {
			
			if(flag==1){
				var a="mrc_no_ac_split["+(index+1)+"]";		
				document.frmParam.elements[a].focus();
			}else{
				var a="account_recur.mar_acc_no_split["+(index+1)+"]";		
				document.frmParam.elements[a].focus();
			}
		}
		
		function split(value) {
			var x = new Array();
			x = value.split('-');
			document.getElementById('lbn_id').value = x[0]; 
			return x[1];
		}
		
		function setRek(kodebank,flag,element){		
			var jenisRecur=document.getElementById('mar_jenis').value ;
			
			if(jenisRecur!=1){		
				ajaxPjngRek(kodebank, flag,element);
			
			}else{
			    for(var i=0;i<16;i++){	
					document.frmParam.elements[element+'['+i+']'].readOnly = false;
					document.frmParam.elements[element+'['+i+']'].style.backgroundColor ='#FFFFFF';
				}
			
			}
			
		
			
		}
	</script>
	
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Perubahan Rekening</a>
			</li>
			<li>
				<a href="#" onClick="return showPane('pane2', this)">History Perubahan</a>
			</li>
		</ul>
		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form method="post" name="frmParam">
				    <table class="entry2">
				      <tr> 
				        <th nowrap="nowrap">Bank <span style="color: red;">*</span></th>
				        <td colspan="3">
				        	<input type="hidden" name="spaj" value="${param.spaj}">
				        	<input type="hidden" name="lbn_id">
							<select name="lsbp_id" style="width: 338px;" tabindex="1" onchange="setRek(split(this.value), 1,'mrc_no_ac_split')">
								<option value=""></option>
								<c:forEach var="s" items="${cmd.bang}">
									<option value="${s.LBN_ID}-${s.LSBP_ID}"
										<c:if test="${s.LBN_ID eq cmd.rekening.lbn_id}">selected</c:if>>${s.LSBP_NAMA} - ${s.LBN_NAMA}</option>
								</c:forEach>
							</select>				        	
				        	<!--<input type="text" name="mar_holder" value="${cmd.rekening.bank}"  tabindex="23" style="width: 336px;">-->
							<!--<option value=""></option>-->
							<!--<c:forEach var="s" items="${cmd.nm_bank}">
								<option value="${s.LSBP_ID}"
									<c:if test="${s.LSBP_ID eq cmd.rekening.lsbp_id}">selected</c:if>>${s.LSBP_NAMA}</option>
							</c:forEach>-->       
				        </td>        
				      </tr>
				      <tr>
				      	<th nowrap="nowrap">No. Rekening <span style="color: red;">*</span></th>
				      	<td colspan="3">
				      		<%--NO REKENING DI SPLIT --%>
				      		<input type="text" name="mrc_no_ac_split[0]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(0, 1);"  value="${cmd.rekening.mrc_no_ac_split[0]}" tabindex="2">
							<input type="text" name="mrc_no_ac_split[1]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(1, 1);"  value="${cmd.rekening.mrc_no_ac_split[1]}"  tabindex="3">
							<input type="text" name="mrc_no_ac_split[2]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(2, 1);"  value="${cmd.rekening.mrc_no_ac_split[2]}"  tabindex="4">
							<input type="text" name="mrc_no_ac_split[3]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(3, 1);"  value="${cmd.rekening.mrc_no_ac_split[3]}"  tabindex="5">
							<input type="text" name="mrc_no_ac_split[4]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(4, 1);"  value="${cmd.rekening.mrc_no_ac_split[4]}"  tabindex="6">
							<input type="text" name="mrc_no_ac_split[5]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(5, 1);"  value="${cmd.rekening.mrc_no_ac_split[5]}"  tabindex="7">
							<input type="text" name="mrc_no_ac_split[6]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(6, 1);"  value="${cmd.rekening.mrc_no_ac_split[6]}"  tabindex="8">
							<input type="text" name="mrc_no_ac_split[7]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(7, 1);"  value="${cmd.rekening.mrc_no_ac_split[7]}"  tabindex="9">
							<input type="text" name="mrc_no_ac_split[8]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(8, 1);"  value="${cmd.rekening.mrc_no_ac_split[8]}"  tabindex="10">
							<input type="text" name="mrc_no_ac_split[9]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(9, 1);"  value="${cmd.rekening.mrc_no_ac_split[9]}"  tabindex="11">
							<input type="text" name="mrc_no_ac_split[10]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(10, 1);"  value="${cmd.rekening.mrc_no_ac_split[10]}" tabindex="12">
							<input type="text" name="mrc_no_ac_split[11]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(11, 1);"  value="${cmd.rekening.mrc_no_ac_split[11]}"  tabindex="13">
							<input type="text" name="mrc_no_ac_split[12]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(12, 1);"  value="${cmd.rekening.mrc_no_ac_split[12]}"  tabindex="14">
							<input type="text" name="mrc_no_ac_split[13]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(13, 1);"  value="${cmd.rekening.mrc_no_ac_split[13]}" tabindex="15">
							<input type="text" name="mrc_no_ac_split[14]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(14, 1);"  value="${cmd.rekening.mrc_no_ac_split[14]}" tabindex="16">
							<input type="text" name="mrc_no_ac_split[15]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(15, 1);"  value="${cmd.rekening.mrc_no_ac_split[15]}" tabindex="17">
							<input type="text" name="mrc_no_ac_split[16]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(16, 1);"  value="${cmd.rekening.mrc_no_ac_split[16]}"  tabindex="18">
							<input type="text" name="mrc_no_ac_split[17]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(17, 1);"  value="${cmd.rekening.mrc_no_ac_split[17]}"  tabindex="19">
							<input type="text" name="mrc_no_ac_split[18]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(18, 1);"  value="${cmd.rekening.mrc_no_ac_split[18]}"  tabindex="20">
							<input type="text" name="mrc_no_ac_split[19]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(19, 1);" value="${cmd.rekening.mrc_no_ac_split[19]}"  tabindex="21">
							<input type="text" name="mrc_no_ac_split[20]" size="1"  maxlength="1" onfocus="select();"  value="${cmd.rekening.mrc_no_ac_split[20]}"  tabindex="21">
				      		
				      		<input type="hidden" name="mrc_no_ac" value="${cmd.rekening.mrc_no_ac}" style="width: 282px;">
				      		<input type="hidden" name="mrc_no_ac_lama" value="${cmd.rekening.mrc_no_ac_lama}">
				      	</td>
				      </tr>
		    		 <c:if test="${not empty cmd.rekening.mrc_kurs}">
					      <tr>
					      	<th nowrap="nowrap">Mata Uang  <span style="color: red;">*</span></th>
					      	<td colspan="3">
							    <select name="mrc_kurs"  tabindex="22" disabled="disabled">
				                <c:forEach var="kurs" items="${cmd.select_kurs}"> <option 
											<c:if test="${cmd.rekening.lku_id eq kurs.ID}"> SELECTED </c:if>
											value="${kurs.ID}">${kurs.SYMBOL}</option> 
				                </c:forEach> 
				               	</select>				      	
					      		<!--<input type="text" name="lku_id" value="${cmd.rekening.mrc_kurs}" style="width: 20px;">-->
					      	</td>
					      </tr>
				      </c:if>
				      <tr>
				      	<th nowrap="nowrap">Atas Nama <span style="color: red;">*</span></th>
				      	<td colspan="3"><input type="text" name="mar_holder" value="${cmd.rekening.mar_holder}"  tabindex="23" style="width: 336px;"></td>
				      </tr>
					<tr>
						<th nowrap="nowrap">Jenis Tabungan</th>
						<td>
							<select name="mar_jenis"  style="width: 114px;"  tabindex="26">
								<c:forEach var="tab" items="${cmd.select_jenis_tabungan}"> 
									<option <c:if test="${cmd.rekening.mar_jenis eq tab.key}"> SELECTED </c:if> 
									value="${tab.key}">${tab.value}</option> 
								</c:forEach> 
							</select>      	
						</td>				      
					</tr>
					<tr>
						<th nowrap="nowrap">Jenis Rekening</th>
						<td>
							<select name="flag_jn_tabungan">
	                   	 <option <c:if test="${cmd.rekening.flag_jn_tabungan eq '0'}">SELECTED</c:if> value="0">Regular</option> 
	           	       	 <option <c:if test="${cmd.rekening.flag_jn_tabungan eq '1'}">SELECTED</c:if> value="1">Tabunganku</option> 
						</select>      	
						</td>				      
					</tr>
				    <c:if test="${not empty cmd.rekening.mste_tgl_recur}">
						<tr> 
							<th nowrap="nowrap">Tanggal Debet</th>
							<td colspan="3">
								<script>inputDate('mste_tgl_recur', '${cmd.rekening.mste_tgl_recur}', true);</script>
							</td>
						</tr>
				      </c:if>	
				      	<tr>
				      	<th nowrap="nowrap">Tanggal Valid</th>
            			<td colspan="3">
				      		<script>inputDate('mar_expired', '${cmd.rekening.mar_expired}', false);</script>
				      	</td>
         			  </tr>
         			   <c:if test="${not empty cmd.rekening.mspo_nasabah_acc}">
					      <tr>
					      	<th nowrap="nowrap">Nomor Sinar Card (Khusus Produk BM)</th>
					      	<td colspan="3"><input type="text" name="mspo_nasabah_acc" value="${cmd.rekening.mspo_nasabah_acc}"  tabindex="23" style="width: 336px;" readonly="readonly"></td>
					      </tr>
				      </c:if>
				      <tr>
				      	<th nowrap="nowrap">Keterangan <span style="color: red;">*</span></th>
				      	<td colspan="3">
							<textarea style="width: 336px;" cols="60" rows="2" name="keterangan" onkeyup="textCounter(this, 60); " onkeydown="textCounter(this, 60); "  tabindex="29">${cmd.rekening.keterangan}</textarea>
				      	</td>
				      </tr>	
				      <tr>
				      	<th nowrap="nowrap">Autodebet Premi Pertama</th>
				      	<td colspan="3">
				      		<input type="checkbox" disabled="disabled" name="flag_autodebet_nb" <c:if test="${cmd.rekening.flag_autodebet_nb eq '1'}">checked="checked"</c:if> >
				      	</td>
				      </tr>				      
				      <tr>
				      	<th nowrap="nowrap"></th>
				      	<td colspan="3">
							<!--<c:if test="${empty cmd.notUnderWriting}">
							    <input type="submit" name="save" value="Save" onclick="return confirm('Simpan perubahan?');"
									accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();"  tabindex="30">
							</c:if>	-->							
						    <input type="button" name="close" value="Close" onclick="window.close();"
								accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();"  tabindex="31">
						    <c:if test="${not empty cmd.err }">
								<div id="error" style="width: 336px;">
								ERROR:<br>
								<c:forEach var="e" items="${cmd.err}">
									- ${e.defaultMessage}<br>
								</c:forEach> 
								</div>
							</c:if>
							<!--ANTASARI-->
							<c:if test="${not empty cmd.scc }">
								<div id="success" style="width: 336px;">
								DATA BERHASIL DISIMPAN<br>
								</div>
							</c:if>
				      	</td>
				      </tr>
				    </table>
				</form>
			</div>
			<div id="pane2" class="panes">
				<display:table id="detail" name="cmd.historyRekening" class="displaytag">
					<display:column property="TGL" title="Tanggal Perubahan" format="{0, date, dd/MM/yyyy}" />
					<display:column property="LUS_LOGIN_NAME" title="User" style="text-align: left;" />
					<display:column property="NOTES" title="Keterangan" style="text-align: left;" />
					<display:column property="MRC_NO_AC_LAMA" title="From" style="text-align: left;" />
					<display:column property="MRC_NO_AC" title="To" style="text-align: left;" />
				</display:table>
			</div>
		</div>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>
