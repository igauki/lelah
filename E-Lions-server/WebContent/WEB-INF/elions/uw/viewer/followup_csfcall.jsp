<%@ include file="/include/page/header.jsp"%>
<script>



</script>
<body onload="document.title='PopUp :: Follow Up CSF Call';" style="height: 100%;">
	<div id="contents">
	<fieldset>
	<form method="post" name="formpost">
		<input type="hidden" name="spaj" value="${spaj}">
		<input type="hidden" name="flag" value="${flag}">
		<legend>Follow Up</legend>
		
		<c:choose>
			<c:when test="${flag eq \"0\" }">
				<table id="tabel1">
			<tr>
				<td valign="top">Kategori</td>
				<td valign="top"> : </td>
				<td>
					<input type="checkbox" class="noborder" name="chbox1" id="chbox1" value="Surat Perintah Tranfer (SPT)"/>Surat Perintah Tranfer (SPT)
					<br/>
					<input type="checkbox" class="noborder" name="chbox2" id="chbox2" value="SP Beda Tanda Tangan Pemegang Polis"/>SP Beda Tanda Tangan Pemegang Polis
					<br/>
					<input type="checkbox" class="noborder" name="chbox3" id="chbox3" value="SP Beda Tanda Tangan Tertanggung"/>SP Beda Tanda Tangan Tertanggung
				</td>
			</tr>
			<tr>
				<td valign="top">Keterangan</td>
				<td valign="top"> : </td>
				<td>
					<textarea rows="4" cols="50" name="keterangan"></textarea>
				</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>
					<input type="checkbox" class="noborder" name="chboxAdmin" id="chboxAdmin"/>Kirim Pesan Ke Admin/MKT
				</td>
			</tr>
		</table>
		</c:when>
		<c:otherwise>
		<table id="tabel1">
			<tr>
				<td valign="top">Kategori</td>
				<td valign="top"> : </td>
				<td>
					<input type="checkbox" class="noborder" name="chbox1" id="chbox1" value="Berhasil Validasi Surat Perintah Transfer (SPT)"/>Berhasil Validasi Surat Perintah Transfer (SPT)
					<br/>
					<input type="checkbox" class="noborder" name="chbox2" id="chbox2" value="SP Beda Tanda Tangan Pemegang Polis"/>Berhasil Validasi SP Beda Tanda Tangan Pemegang Polis
					<br/>
					<input type="checkbox" class="noborder" name="chbox3" id="chbox3" value="SP Beda Tanda Tangan Tertanggung"/>Berhasil Validasi SP Beda Tanda Tangan Tertanggung
				</td>
			</tr>
			<tr>
				<td valign="top">Keterangan</td>
				<td valign="top"> : </td>
				<td>
					<textarea rows="4" cols="50" name="keterangan"></textarea>
				</td>
			</tr>
		</table>
			</c:otherwise>
		</c:choose>
		
		<br/>
		<c:if test="${not empty errorMessage}">
			<div id="error">
				Perhatian:<br/>
				<c:forEach var="error" items="${errorMessage}">
					- <c:out value="${error}" escapeXml="false"/>
					<br/>
				</c:forEach>
			</div>
		</c:if>
		<br/>
		<input type="submit" name="save" value="Save"/>
		<input type="button" name="close" value="Close" onclick="window.close();"/>
	</form>
	</fieldset>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>