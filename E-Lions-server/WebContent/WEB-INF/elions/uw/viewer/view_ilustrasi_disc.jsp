<%@ include file="/include/page/header.jsp"%>
<body onload="setupPanes('container1', 'tab1'); document.title='PopUp :: View Ilustrasi Disc';" style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Ilustrasi Disc</a>
			</li>
		</ul>

		<div class="tab-panes">
			<div id="pane1" class="panes">
				<c:choose>
					<c:when test="${not empty pesanError}">
						<div id="error">
							test pesan error
						</div>
					</c:when>
					<c:otherwise>
						<form method="post">
							<fieldset style="width: auto">
							<legend>Ilustrasi Perhitungan Discount (Multi Invest)</legend>
							<table class="entry2" style="width: auto">
								<tr>
									<th>Tgl Bayar</th>
									<td><script>inputDate('tgl_trans', '<fmt:formatDate value="${tgl_trans}" pattern="dd/MM/yyyy" />', false);</script></td>
									<th>Jatuh Tempo</th>
									<td><script>inputDate('jt_tempo', '<fmt:formatDate value="${jt_tempo}" pattern="dd/MM/yyyy" />', false);</script></td>
									<th>Selisih</th>
									<td><input size="4" style="text-align: center" type="text" name="bln_disc" value="${bln_disc}" class="readOnly" readonly="readonly"> Bulan</td>
								</tr>
								<tr>
									<th>Premi</th>
									<td colspan="3">
										<c:if test="${kurs eq \"01\" }"><input size="3" type="text" name="kurs" value="Rp." class="readOnly" readonly="readonly"></c:if>
										<c:if test="${kurs eq \"02\" }"><input size="3" type="text" name="kurs" value="US$" class="readOnly" readonly="readonly"></c:if>
										<input size="12" style="text-align: right" class="readOnly" readonly="readonly" type="text" name="premi" value="<fmt:formatNumber value="${premi}" maxFractionDigits="2" minFractionDigits="0" />"> x 
										<input size="3" style="text-align: center" type="text" name="kali" value="${kali}">
									</td>
									<th>Total Premi</th>
									<td><input size="15" style="text-align: right" class="readOnly" readonly="readonly" type="text" name="total" value="<fmt:formatNumber value="${total}" maxFractionDigits="2" minFractionDigits="0" />"></td>
								</tr>
								<tr>
									<th>Disc Rate</th>
									<td colspan="3"><input size="7" style="text-align: right" class="readOnly" readonly="readonly" type="text" name="pct_disc" value="<fmt:formatNumber value="${pct_disc}" maxFractionDigits="2" minFractionDigits="0" />"> %</td>
									<th>Discount</th>
									<td><input size="15" style="text-align: right" class="readOnly" readonly="readonly" type="text" name="disc" value="<fmt:formatNumber value="${disc}" maxFractionDigits="2" minFractionDigits="0" />"></td>
								</tr>
								<tr>
									<td colspan="4"></td>
									<td colspan="2"><hr style="color: black;"></td>
								</tr>
								<tr>
									<td colspan="4"></td>
									<th>Total Bayar</th>
									<td><input size="15" style="text-align: right" class="readOnly" readonly="readonly" type="text" name="bayar" value="<fmt:formatNumber value="${bayar}" maxFractionDigits="2" minFractionDigits="0" />"></td>
								</tr>
							</table>
							</fieldset>
							<input type="submit" name="hitung" value="Hitung">
							<input type="reset" name="reset" value="Reset">
							<input type="button" name="print" value="Print" onclick="window.print();">
						</form>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
	
</body>
<%@ include file="/include/page/footer.jsp"%>