<%@ include file="/include/page/header.jsp"%>
<body onload="document.title='PopUp :: CSF Reminder';" style="height: 100%;">
	<div id="contents">
		<span class="subtitle">
			Call Summary
		</span>
		
		No. Polis: <elions:polis nomor="${cmd.daftar[0].POLIS}"/><br>
		Nama Pemegang Polis: ${cmd.daftar[0].PP}
		
		<div id="contents">
			<display:table id="baris" style="width:90%; " name="cmd.daftar" class="displaytag" requestURI="${requestScope.requestURL}" export="true" pagesize="20">
				<display:column property="INOUT" title="Jenis Call"  sortable="true" group="1"/>
				<display:column property="USERNAME" title="User"  />
				<display:column property="AWAL" title="Tanggal Call"  />
				<display:column property="AKHIR" title="Akhir Call"  />
				<display:column property="CALLBACK" title="Call Back"  />
				<display:column property="KETERANGAN" title="Keterangan"  />
			</display:table>
			<input type="button" value="Close" onclick="window.close()">
		</div>		
		
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>