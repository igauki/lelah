<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<c:set var="currentPage" value="${pageContex.request.requestURI}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/mallinsurance1.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<!-- DatePicker Script (jscalendar) -->
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	function cariData(){
		//var pilter = document.getElementById('pilter').value;
		//var tipe = document.getElementById('tipe').value;
		//var kata = document.getElementById('kata').value;
		var pdfName = document.getElementById('pdfName').value;
		
		var url = 'viewer_pa_free.htm?tmms_id='+tmms_id;
		window.open(url, '_self');
		
		//document.getElementById('refreshPage').href = 'pa_free_detail.htm?tmms_id='+tmms_id;
		//document.getElementById('refreshPage').click();
	
	}
	
	function openPdf(){
		//var pdfName = document.getElementById('pdfName').value;
		
		//var link = 'pa_free_detail.htm?pdfName='+pdfName;
		//window.open( link, '_blank' );
		//document.getElementById('refreshPage').href = link;
		document.getElementById('search').click();
	
	}
	
	function bodyEndLoad(){
		if(document.getElementById('popUpInsert').value == 'open'){
			document.getElementById('inputFreePa').click();
		}
	}
	
</script>
<style>
    #pagination {
        margin: 10px 0;
        color: #CC0000;
    }
    
    #pagination a {
        color: inherit;
        text-decoration: none;
    }
    
    #pagination .currentPage {
        color: #000000;
    }
</style>
</head>
<BODY onload="bodyEndLoad();" style="height: 100%;">
	<div class="tab-container" id="container1">
		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form method="post" name="formpost" action="#" style="text-align: center;">
					<a href="pa_free_detail.htm" id="refreshPage" name="refreshPage"></a>
					<input type="button" name="refreshButton" onclick="document.getElementById('search').click();" style="visibility: hidden;"/>
					<input type="hidden" name="action" id="action" value="" />
					<input type="hidden" name="tmms_id" id="tmms_id" value="" />
					<input type="hidden" name="pdfName" id="pdfName"/>
					<input type="hidden" name="win" value="${param.win}">
					<input type="hidden" name="cari" value="${param.cari}">
					<input type="hidden" name="posisi" value="${param.posisi}">
					<fieldset>
		 					<legend>Pencarian</legend>
					<table class="result_table2">
						<tr>
							<th rowspan="1">Cari:</th>
							<td style="width: 75px;">
							    <input type="radio" name="paType" value="0"<c:if test="${paType eq 0}"> checked</c:if>> Free PA<br>
							    <input type="radio" name="paType" value="1"<c:if test="${paType eq 1}"> checked</c:if>> PA BSM
							</td>
							<td>
								<select name="tipe">
									<option value="1" <c:if test="${param.tipe eq \"1\" }">selected</c:if>>Nama</option>
									<option value="2" <c:if test="${param.tipe eq \"2\" }">selected</c:if>>No.Bukti Identitas</option>
								</select>
								<select name="pilter">
									<option <c:if test="${\"LIKE\" eq param.pilter}">selected</c:if> value="LIKE">LIKE</option>
									<option <c:if test="${\"LT\" eq param.pilter}">selected</c:if> value="LT"><</option>
									<option <c:if test="${\"LE\" eq param.pilter}">selected</c:if> value="LE"><=</option>
									<option <c:if test="${\"EQ\" eq param.pilter}">selected</c:if> value="EQ">=</option>
									<option <c:if test="${\"GE\" eq param.pilter}">selected</c:if> value="GE">>=</option>
									<option <c:if test="${\"GT\" eq param.pilter}">selected</c:if> value="GT">></option>
								</select>					
								<input type="text" name="kata" size="34" value="${param.kata }" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
								<input type="submit" name="search" id="search" value="Search" onclick="cariData();" class="button2"
								onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
								onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />
							</td>
						</tr>
					</table>
					</fieldset >
					<div style="visibility: hidden;">
					<input type="submit" name="search" id="search" value="Search" onclick="cariData();" class="button2"
								onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
								onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />
								</div>
					<table class="result_table">
						<thead>
							<tr>
								<th style="text-align: left">Nama</th>
								<th style="text-align: left">Tempat</th>
								<th style="text-align: left">Tgl.Lahir</th>
								<th style="text-align: left">No. Identitas</th>
								<th style="text-align: left">Alamat</th>
								<th style="text-align: left">Kota</th>
								<th style="text-align: left">Kode Pos</th>
								<th style="text-align: left">E-Mail</th>
								<th style="text-align: left">Action</th>
							</tr>
						</thead>
						<tbody>
							<c:set var="backGroundColor" value="#FED9D9" />

							<%-- <c:forEach var="pa" items="${tmmsList}" varStatus="stat">
											<c:choose>
												<c:when test='${"#FED9D9" == backGroundColor}'>
													<c:set var="backGroundColor" value="#FFF2F2" />
												</c:when>
												<c:otherwise>
													<c:set var="backGroundColor" value="#FED9D9" />
												</c:otherwise>
											</c:choose>
								<tr style="background-color: ${backGroundColor};">
										<td>${pa.holder_name}</td>
										<td>${pa.bod_tempat}</td>
										<td>
											<fmt:formatDate value="${pa.bod_holder}" pattern="dd/MM/yyyy" />
										</td>
										<td>${pa.no_identitas}</td>
										<td width="200px">${pa.address1}</td>
										<td>${pa.city}</td>
										<td>${pa.postal_code}</td>
										<td>${pa.email}</td>
										<td width="140px">
											<input type="button" value="DETAIL" name="paDetail"
											onclick="popWin('${path}/uw/viewer/viewer_pa_free_detail.htm?tmms_id='+${pa.id}, 350, 450); " class="button2" 
											onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
											onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />
											<!-- input type="button" value="VIEW PDF" name="printPdfPolis"
											onclick="document.getElementById('pdfName').value='${pa.no_sertifikat}';openPdf();"
											class="button2"
											onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
											onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" / -->
										</td>
								</tr>
							</c:forEach> --%>
							
							<c:choose>
							    <c:when test="${paType eq 0}">
							        <c:forEach var="pa" items="${list.pageList}" varStatus="stat">
                                        <c:choose>
                                            <c:when test='${"#FED9D9" == backGroundColor}'>
                                                <c:set var="backGroundColor" value="#FFF2F2" />
                                            </c:when>
                                            <c:otherwise>
                                                <c:set var="backGroundColor" value="#FED9D9" />
                                            </c:otherwise>
                                        </c:choose>
		                                <tr style="background-color: ${backGroundColor};">
		                                        <td>${pa.holder_name}</td>
		                                        <td>${pa.bod_tempat}</td>
		                                        <td>
		                                            <fmt:formatDate value="${pa.bod_holder}" pattern="dd/MM/yyyy" />
		                                        </td>
		                                        <td>${pa.no_identitas}</td>
		                                        <td width="200px">${pa.address1}</td>
		                                        <td>${pa.city}</td>
		                                        <td>${pa.postal_code}</td>
		                                        <td>${pa.email}</td>
		                                        <td width="140px">
		                                            <input type="button" value="DETAIL" name="paDetail"
		                                            onclick="popWin('${path}/uw/viewer/viewer_pa_free_detail.htm?tmms_id='+${pa.id}, 350, 450); " class="button2" 
		                                            onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
		                                            onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />
		                                            <!-- input type="button" value="VIEW PDF" name="printPdfPolis"
		                                            onclick="document.getElementById('pdfName').value='${pa.no_sertifikat}';openPdf();"
		                                            class="button2"
		                                            onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
		                                            onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" / -->
		                                        </td>
		                                </tr>
		                            </c:forEach>
							    </c:when>
							    <c:otherwise>
							        <c:forEach var="pas" items="${list.pageList}" varStatus="stat">
							            <c:choose>
                                            <c:when test='${"#FED9D9" == backGroundColor}'>
                                                <c:set var="backGroundColor" value="#FFF2F2" />
                                            </c:when>
                                            <c:otherwise>
                                                <c:set var="backGroundColor" value="#FED9D9" />
                                            </c:otherwise>
                                        </c:choose>
                                        <tr style="background-color: ${backGroundColor};">
                                            <td>${pas.msp_pas_nama_pp}</td>
                                            <td>${pas.msp_pas_tmp_lhr_pp}</td>
                                            <td>
		                                        <fmt:formatDate value="${pas.msp_pas_dob_pp}" pattern="dd/MM/yyyy" />
		                                    </td>
                                            <td>${pas.msp_identity_no}</td>
                                            <td width="200px">${pas.msp_address_1}</td>
                                            <td>${pas.msp_city}</td>
                                            <td>${pas.msp_postal_code}</td>
                                            <td>${pas.msp_pas_email}</td>
                                            <td width="140px">
                                                <input type="button" value="DETAIL" name="paDetail"
                                                    onclick="popWin('${path}/uw/viewer/viewer_pa_free_detail.htm?tmms_id='+${pas.msp_id}+'&paType=1', 350, 450); " class="button2" 
                                                    onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
                                                    onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />
                                            </td>
                                        </tr>
							        </c:forEach>
							    </c:otherwise>
							</c:choose>
						</tbody>
					</table>
					<!-- <br> -->
					<div id="pagination">
                        <c:if test="${list.page ne 0}"><span style="margin-right: 5px;"><a href="${currentPage}?page=1" title="First Page">&lt;&lt;</a></span></c:if>
                        <c:if test="${list.page ne 0}"><span style="margin-right: 5px;"><a href="${currentPage}?page=${list.page}" title="Previous Page">Prev</a></span></c:if>
                            <c:if test="${list.pageCount > 1}">
                                <c:set var="j" value="10"/>
                                <c:forEach var="i" begin="1" end="${list.pageCount}" step="1">
                                <c:choose>
                                    <c:when test="${i eq (list.page + 1)}">
                                        <span class="currentPage">${i}</span><c:if test="${i ne list.pageCount}">,</c:if>
                                        <c:if test="${i eq j}">
                                            <c:set var="j" value="${j + 10}"/>
                                        </c:if>
                                    </c:when>
                                    <c:when test="${((list.page + 4) <= i || (list.page - 2) >= i)}">
                                        <c:choose>
                                            <c:when test="${i < 10 && (list.page + 1 < 10)}">
                                                <a href="${currentPage}?page=<c:out value="${i}"/>" title="Page ${i}">${i}</a><c:if test="${i ne list.pageCount}">,</c:if>
                                            </c:when>
                                            <c:when test="${i eq 10 && (list.page + 1 < 10)}">
                                                 <a href="${currentPage}?page=<c:out value="${i}"/>" title="Page ${i}">${i}</a> ...
                                                <c:set var="j" value="${j + 10}"/>
                                            </c:when>
                                            <c:when test="${i eq j}">
                                                <c:choose>
	                                                <c:when test="${((j + 10) > (list.page -1) && j < (list.page + 1) && j ne (list.page - 2)) || (j + 10) eq (list.page - 2)}">
	                                                    <a href="${currentPage}?page=<c:out value="${j}"/>" title="Page ${j}">${j}</a>
	                                                </c:when>
	                                                <c:when test="${j eq (list.page - 2)}">
	                                                    ... <a href="${currentPage}?page=<c:out value="${i}"/>" title="Page ${i}">${i}</a>,
	                                                </c:when>
	                                                <c:otherwise>
	                                                    <a href="${currentPage}?page=<c:out value="${j}"/>" title="Page ${j}">${j}</a><c:if test="${(list.pageCount - j) >= 10}">,</c:if>
	                                                </c:otherwise>
	                                            </c:choose>
	                                            <c:set var="j" value="${j + 10}"/>
                                            </c:when>
                                            <c:when test="${(list.page +4) eq i}">
                                                <a href="${currentPage}?page=<c:out value="${i}"/>" title="Page ${i}">${i}</a> ...
                                            </c:when>
                                            <c:when test="${(list.page - 2) eq i}">
                                                ... <a href="${currentPage}?page=<c:out value="${i}"/>" title="Page ${i}">${i}</a>,
                                            </c:when>
                                        </c:choose>
                                    </c:when>
                                    <c:otherwise>
                                        <a href="${currentPage}?page=<c:out value="${i}"/>" title="Page ${i}">${i}</a><c:if test="${i ne list.pageCount}">,</c:if>
                                        <c:if test="${i eq j}">
                                            <c:set var="j" value="${j + 10}"/>
                                        </c:if>
                                    </c:otherwise>
                                </c:choose>
                                </c:forEach>
                            </c:if>
                        <c:if test="${(list.page + 1) ne list.pageCount}"><span style="margin-left: 5px;"><a href="${currentPage}?page=${list.page + 2}" title="Next Page">Next</a></span></c:if>
                        <c:if test="${(list.page + 1) ne list.pageCount}"><span style="margin-left: 5px;"><a href="${currentPage}?page=${list.pageCount}" title="Last Page">&gt;&gt;</a></span></c:if>
                    </div>
				</form>
			</div>
		</div>
	</div>

</body>
<%@ include file="/include/page/footer.jsp"%>