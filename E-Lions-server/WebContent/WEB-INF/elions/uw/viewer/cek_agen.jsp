<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/disableKeys.js"></script>
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
	</head>
	<BODY onload="resizeCenter(650,400); document.title='PopUp :: Cek Agen'; setupPanes('container1', 'tab1'); document.formpost.cari.select(); " style="height: 100%;">

	<form method="post" name="formpost">	
	<fieldset>
	<legend></legend>
		<table class="entry2">
			<tr>
				<th>Masukan Kode Agen : </th>
				<td>
					<input type="text" size="30" name="msag_id" >
					<input type="submit" value="Cari" name="btn_send" onClick="">
				</td>
			</tr>
		</table>
	</fieldset>
	
		

	<c:if test='${flag eq 1}'>
		<fieldset>
			<legend>Info Agen</legend>
			<div id="pane1" class="panes">
					<table class="entry2">
						<tr>
							<th nowrap="nowrap">
								Kode Agen : </th>
							<td><input class="readOnly" readonly type="text" name=Kode Agen size="20" value="${param.msag_id}" maxlength="6"></td>
							<th nowrap="nowrap">Region Agen : </th>
							<td><input class="readOnly" readonly type="text" name=Kode Agen size="20" value="${region}" maxlength="6"></td>
						</tr>
						</tr>
							<th nowrap="nowrap">Nama Agen : </th>
							<td><input class="readOnly" readonly type="text" name=Kode Agen size="50" value="${nama_agen}" maxlength="6"></td>							
						</tr>
						
						<tr>
							<th nowrap="nowrap">Email Agen : </th>
							<td><input class="readOnly" readonly type="text" name=Kode Agen size="60" value="${email_agen}" maxlength="6"></td>
						</tr>
						
						<tr>
							<th nowrap="nowrap">Nama Bank : </th>
							
							<td><input class="readOnly" readonly type="text" name=Kode Agen size="40" value="${bank}" maxlength="6"></td>
							<th nowrap="nowrap">Bank Cabang : </th>
							<td><input class="readOnly" readonly type="text" name=Kode Agen size="40" value="${bank_cabang}" maxlength="6"></td>
						</tr>
						
						<tr>
							<th nowrap="nowrap">No. Rekening : </th>
							<td><input class="readOnly" readonly type="text" name=Kode Agen size="40" value="${bank_rekening}" maxlength="6"></td>
							<th nowrap="nowrap">NPWP : </th>
							<td><input class="readOnly" readonly type="text" name=Kode Agen size="40" value="${npwp_agen}" maxlength="6"></td>
						</tr>
						
						<tr>
							<th nowrap="nowrap">Sertifikat : </th>
							<td><input class="readOnly" readonly type="text" name=Kode Agen size="40" value="${sertifikat}" maxlength="6"></td>
							<th nowrap="nowrap">NO.Sertifikat : </th>
							<td><input class="readOnly" readonly type="text" name=Kode Agen size="40" value="${sertifikat_no}" maxlength="6"></td>
						</tr>
						
						<tr>
							<th nowrap="nowrap">Tanggal Sertifikat</th>
							<td><input class="readOnly" readonly type="text" name=Kode Agen size="40" value="<fmt:formatDate value="${sertifikat_aktif}" pattern="dd/MM/yyyy"/>" maxlength="6"></td>
							<th nowrap="nowrap">Tanggal Aktif</th>
							<td><input class="readOnly" readonly type="text" name=Kode Agen size="40" value="<fmt:formatDate value="${tgl_aktif}" pattern="dd/MM/yyyy"/>" maxlength="6"></td>
						</tr>
						
						<tr>
							<th nowrap="nowrap">Tanggal Mulai Kontrak </th>
							<td><input class="readOnly" readonly type="text" name=Kode Agen size="40" value="<fmt:formatDate value="${beg_date_kontrak}" pattern="dd/MM/yyyy"/>"  maxlength="6"></td>
							<th>Tanggal Akhir Kontrak </th>
							<td><input class="readOnly" readonly type="text" name=Kode Agen size="40" value="<fmt:formatDate value="${end_date_kontrak}" pattern="dd/MM/yyyy"/>"  maxlength="6"></td>
						</tr>
						
					</table>
			</div>
		</fieldset>
	</c:if>
			

	</form>
	</body>