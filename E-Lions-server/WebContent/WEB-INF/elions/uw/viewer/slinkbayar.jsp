<%@ include file="/include/page/header.jsp"%>
<body onload="document.title='PopUp :: Jadwal Pembayaran Bunga Bulanan';setupPanes('container1','tab1');" style="height: 100%;">
	
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Jadwal Pembayaran Bunga Bulanan</a>
			</li>
		</ul>

		<div class="tab-panes">
			<div id="pane1" class="panes">
				<c:if test="${not empty slinkBayar}">
				<fieldset>
				<table class="entry2">
					<tr>
						<th style="width: 200px;">Polis:</th>
						<td>${slinkBayar[0].MSPO_POLICY_NO}</td>
					</tr>
					<tr>
						<th>Nama Pemegang Polis:</th>
						<td>${slinkBayar[0].PEMEGANG}</td>
					</tr>
					<tr>
						<th>Transaksi:</th>
						<td>
							<c:choose>
								<c:when test="${slinkBayar[0].MSL_TU_KE eq 0}">Premi Pokok</c:when>
								<c:otherwise>Premi Topup ke-${slinkBayar[0].MSL_TU_KE}</c:otherwise>
							</c:choose>
						</td>
					</tr>
					<tr>
						<th>MTI:</th>
						<td>${slinkBayar[0].MSL_MGI} Bln</td>
					</tr>
					<tr>
						<th>Premi Deposit:</th>
						<td><fmt:formatNumber value="${slinkBayar[0].MSLB_PREMI}"/></td>
					</tr>
					<tr>
						<th>Tgl Masuk:</th>
						<td><fmt:formatDate value="${slinkBayar[0].MSL_BDATE}" pattern="dd/MM/yyyy" /></td>
					</tr>
					<tr>
						<th>Tgl Akhir:</th>
						<td><fmt:formatDate value="${slinkBayar[0].MSL_EDATE}" pattern="dd/MM/yyyy" /></td>
					</tr>
				</table>
				<display:table id="detail" name="slinkBayar" class="displaytag">
					<display:column title="Tanggal Mulai" property="MSLB_BEG_PERIOD" format="{0, date, dd/MM/yyyy}" />
					<display:column title="Tanggal Jatuh Tempo Investasi" property="MSLB_END_PERIOD" format="{0, date, dd/MM/yyyy}" />
					<display:column title="Tanggal Pembayaran Bunga" property="MSLB_PAID_DATE" format="{0, date, dd/MM/yyyy}" />
					<display:column title="Jumlah Hari" property="MSLB_JML_HARI" />
					<display:column title="Bunga per Tahun" property="MSLB_RATE" />
					<display:column title="Bunga Bulanan" property="MSLB_BUNGA" format="{0, number, #,##0.00;(#,##0.00)}"/>
					<display:column title="No Register Pembayaran" property="MSLB_BAYAR_ID" />
					<display:column title="Tanggal Print" property="MSLB_PRINT_DATE" format="{0, date, dd/MM/yyyy}" />
					<display:column title="Hari Lebih" property="MSLB_HARI" />
					<display:column title="Manfaat Tambahan" property="MSLB_TAMBAH" format="{0, number, #,##0.00;(#,##0.00)}"/>
					<display:column title="Total Dibayar" property="MSLB_JUM_BAYAR" format="{0, number, #,##0.00;(#,##0.00)}"/>
					<display:column title="Rekening Nasabah" property="MSLB_REKENING" />
				</display:table>
				<br/><span class="info">(*)  Apabila tanggal pembayaran manfaat jatuh pada hari libur, maka manfaat dibayarkan pada hari kerja berikutnya, dan akan dibayarkan manfaat tambahan.</span>
				</fieldset>
				</c:if>
			</div>
		</div>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>
