<%@ include file="/include/page/header.jsp"%>
<script>
	function cari(){
		formpost.p.value="1";
		formpost.submit();
	}
</script>
<BODY onload="setFocus('kata'); document.title='PopUp :: Cari Simultan';" style="height: 100%;">
<form method="post" name="formpost" >

	<table align="center" width="100%" class="entry2">
		<tr>
			<th>Nama :</th>	
			<td><input type="text" size="50" name="nama" value="${nama}"></td>
		</tr>
		<tr>
			<th>Tanggal Lahir:</th>	
			<td>
				<script>inputDate('tglLahir', '${tglLahir}', false);</script>
			</td>
		</tr>
		<tr>
			<th></th>	
			<td>
				<input type="button" name="btnCari" value="Cari" onClick="cari();">
				<input type="hidden" name="p" value="0" >
			</td>
		</tr>
	</table>
	<fieldset>
		<legend>Detail Simultan Polis </legend>
		<display:table id="baris" name="lsSimultan" class="displaytag"   decorator="org.displaytag.decorator.TotalTableDecorator">   
			<display:column property="BRS" title="No."  sortable="true"/>                                                                                                
			<display:column property="MCL_ID" title="CLIENT ID"  sortable="true"/>                                                                                          
			<display:column property="MCL_GELAR" title="GELAR"  sortable="true"/>                                                                                    
			<display:column property="MCL_FIRST" title="NAMA"  sortable="true"/>                                                                                    
			<display:column property="MCL_BLACKLIST" title="ATTENTION LIST"  sortable="true"/>                                                                            
			<display:column property="MSPE_MOTHER" title="NAMA IBU KANDUNG"  sortable="true"/>                                                                                
			<display:column property="LSIDE_NAME" title="LSIDE_NAME"  sortable="true"/>                                                                                  
			<display:column property="MSPE_NO_IDENTITY" title="MSPE_NO_IDENTITY"  sortable="true"/>                                                                      
			<display:column property="MSPE_PLACE_BIRTH" title="MSPE_PLACE_BIRTH"  sortable="true"/>                                                                      
			<display:column property="MSPE_DATE_BIRTH" title="MSPE_DATE_BIRTH" format="{0, date, dd/MM/yyyy}"  sortable="true"/>                                         
			<display:column property="MSPE_SEX" title="MSPE_SEX"  sortable="true"/>                                                                                      
			<display:column property="MSPE_STS_MRT" title="MSPE_STS_MRT"  sortable="true"/>                                                                              
			<display:column property="MPN_JOB_DESC" title="MPN_JOB_DESC"  sortable="true"/>                                                                              
			<display:column property="ALAMAT_RUMAH" title="ALAMAT_RUMAH"  sortable="true"/>                                                                              
			<display:column property="KD_POS_RUMAH" title="KD_POS_RUMAH"  sortable="true"/>                                                                              
			<display:column property="AREA_CODE_RUMAH" title="AREA_CODE_RUMAH"  sortable="true"/>                                                                        
			<display:column property="TELPON_RUMAH" title="TELPON_RUMAH"  sortable="true"/>         
		</display:table>       
	</fieldset>
	    
	<table align="center" width="100%">
		<tr><td>
			<c:if test="${not empty lsError}">
				<div id="error">
					 Informasi:<br>
						<c:forEach var="error" items="${lsError}">
									 - <c:out value="${error}" escapeXml="false" />
							<br/>
						</c:forEach>
				</div>
			</c:if>			
		</td></tr>	

		<tr>
			<td align="center" ><input type="button" name="btnClose" value="Close" onClick="javascript:window.close()"></td>
		</tr>
	</table>
</form>
</BODY>
<%@ include file="/include/page/footer.jsp"%>