<%@ include file="/include/page/header.jsp"%>

<script type="text/javascript">
	hideLoadingMessage();
	

function Right(str, n){
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}


function DateAdd(objDate, intDays)
{
  var iSecond=1000;	 // Dates are represented in milliseconds
  var iMinute=60*iSecond;
  var iHour=60*iMinute;
  var iDay=24*iHour;
  var objReturnDate=new Date();
  objReturnDate.setTime(objDate.getTime()+(intDays*iDay));
  return objReturnDate;
}

function listtgl()
{
	var tanggal_beg = document.frmParam.tgl1.value;
	
	var arrMonthDays = new Array (	"31", "29", "31",
					"30", "31", "30",
					"31", "31", "30",
					"31","30","31"	  );

		var tahun_s = tanggal_beg.substring(6,10);
		var bulan_s = tanggal_beg.substring(3,5);
		var tanggal_s = tanggal_beg.substring(0,2);
		var tanggal  = tanggal_s+"/"+bulan_s+"/"+tahun_s;
		
	if ((bulan_s==2)&&  (tahun_s%4>0) ) {
	    tanggal_s=28;
	}else{
		tanggal_s=arrMonthDays[bulan_s-1];
	}	
	tanggal  = tanggal_s+"/"+bulan_s+"/"+tahun_s;

	document.frmParam._tgl2.value=tanggal;
	document.frmParam.tgl2.value=tanggal;
	document.frmParam.tgl3.value=tanggal;

}

function listtgl2()
{
	tanggal = document.frmParam._tgl2.value;
	document.frmParam._tgl2.value=tanggal;
	document.frmParam.tgl2.value=tanggal;
	document.frmParam.tgl3.value=tanggal;
}	
	

function caridaftar()
{
 	if ( document.frmParam.tgl1.value=="__/__/____" || document.frmParam.tgl1.value=="" || document.frmParam.tgl2.value=="__/__/____" || document.frmParam.tgl2.value=="")
	{
		alert('Silahkan isi tanggal terlebih dahulu');
	}else{
		var tanggal_beg = document.frmParam.tgl1.value;
		var tahun_s = tanggal_beg.substring(6,10);
		var bulan_s = tanggal_beg.substring(3,5);
		var tanggal_s = tanggal_beg.substring(0,2);
		var tanggal1  = tahun_s+bulan_s+tanggal_s;
		var tanggal_end = document.frmParam.tgl2.value;
		tahun_s = tanggal_end.substring(6,10);
		bulan_s = tanggal_end.substring(3,5);
		tanggal_s = tanggal_end.substring(0,2);
		var tanggal2  = tahun_s+bulan_s+tanggal_s;
		frmParam.submit();
	}
}
	
</script>
<body style="height: 100%;"
	onload="setFrameSize('infoFrame', 68);"
	onresize="setFrameSize('infoFrame', 68);">
<form name="frmParam" method="post">

 <table class="entry" width="98%">
      <tr> 
        <th colspan="2" ><center>SUMMARY PT. GUTHRIE  PECCONINA  INDONESIA</center>
	   </th>
      </tr> 
          </tr>  
            <th colspan="7" class="subtitle">&nbsp;</th>
          </tr>
  <tr> 
        <th >Periode Tanggal</th>
        <th>
              <script>inputDate('tgl1', '${tgl1}', false,'listtgl();');</script>
			s/d
              <script>inputDate('tgl2', '${tgl2}', false,'listtgl2();');</script>
			<input type="hidden" name="tgl3">
		</th>
      </tr>  
          </tr>  
            <th colspan="7" class="subtitle"><center><input type="button" value="Cari" name="search" onClick="caridaftar();"> </center></th>
          </tr>     
      <tr> 
        <td> </td>
      </tr>

    </table>
</form>
</body>

<%@ include file="/include/page/footer.jsp"%>