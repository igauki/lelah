<%@ include file="/include/page/header.jsp"%>
<body onload="document.title='PopUp :: CSF Reminder';" style="height: 100%;">
	<div id="contents">
		<fieldset>
		<legend>
			Daftar Customer yang harus di Follow-Up
		</legend>
		
		<div id="contents">
			<display:table id="baris" style="width:90%; " name="cmd.daftar" class="displaytag" requestURI="${requestScope.requestURL}" export="true" pagesize="20">
				<display:column property="NO_POLIS" title="No. Polis"  sortable="true"/>
				<display:column property="NAMA" title="Nama Pemegang Polis"  sortable="true"/>
				<display:column property="LUS_LOGIN_NAME" title="User"  sortable="true"/>
			</display:table>
			<input type="button" value="Close" onclick="window.close()">
		</div>		
		</fieldset>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>