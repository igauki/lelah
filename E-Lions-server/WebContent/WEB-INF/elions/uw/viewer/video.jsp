<%@ include file="/include/page/header_jquery.jsp"%>
<script type="text/javascript" src="${path }/include/js/default.js"></script>
<script type="text/javascript" src="${path}/include/js/flowplayer/flowplayer-3.2.13.min.js"></script>
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$(document).ready(function(){

	});
	
	function getVideo(dir, file){
		//window.open('${path}/uw/viewer.htm?window=video&video=1&dir='+dir+'&file='+file, 300, 425);
		window.location.href = "${path}/uw/viewer.htm?window=video&video=1&dir="+dir+"&file="+file;
	}
	hideLoadingMessage();
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 23em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 23.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 23em; }
	
	/* untuk align center */
	.tengah { text-align: center; }
	.kiri { text-align: left; }

	/* untuk tombol2 kecil di report */
	ul#icons {margin: 0; padding: 0;}
	ul#icons li {margin: 0 1px 0 0; position: relative; padding: 0px; cursor: pointer; float: left;  list-style: none;}
	ul#icons span.ui-icon {float: left; margin: 0px;}

	/* styling untuk server-side validation error */
	.errorField { border: 1px solid red; }
	.errorMessage { color: red; display: block;}
	.readonly { background-color: #CCC;}
	
	/* styling untuk table input */
	table.inputTable{
		width: 700px;
	}
	table.inputTable tr th{
		width: 150px;
	}
	
	/* styling untuk table info tanggal */
	div#infoTanggal{
		text-align: center;
		background-color: white;
		width: 345px;
		padding: 8px;
		border: 1px solid gray;
		position: fixed;
		left: 580px;
		top: 50px;
	}

</style>

<body>
	<br>
	<div style="width: 100%;top: 20px;">
	<div style="margin-left: 25%;">
		<c:choose>
			<c:when test="${empty vid }">
				<a href=""
				   style="display:block;width:635px;height:450px;"
				   id="player">
				</a>
			</c:when>
			<c:otherwise>
				<a href="http://intranet.sinarmasmsiglife.co.id/video/${dir }/${vid}"
				   style="display:block;width:635px;height:450px;"
				   id="player">
				</a>
			</c:otherwise>
		</c:choose>
	</div>
	</div>
	<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>DEEPER</div></legend>
		<ul>
			<li><a href="#" onclick="getVideo('deeper','Fund_Fact_Sheet.flv');">Fund Fact Sheet</a></li>
			<li><a href="#" onclick="getVideo('deeper','Handling_Objection.flv');">Handling Objection</a></li>
			<li><a href="#" onclick="getVideo('deeper','Income_Protection.flv');">Income Protection</a></li>
			<li><a href="#" onclick="getVideo('deeper','Simpol_Health.flv');">Simpol Health</a></li>
			<li><a href="#" onclick="getVideo('deeper','Simpol_Pension.flv');">Simpol Pension</a></li>
		</ul>
	</fieldset>
	
	<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>FASTER</div></legend>
		<ul>
			<li><a href="#" onclick="getVideo('faster','Approach_and_Hearing.flv');">Approach and Hearing</a></li>
			<li><a href="#" onclick="getVideo('faster','Changing_Mindset.flv');">Changing Mindset</a></li>
			<li><a href="#" onclick="getVideo('faster','Daily_Activity_Planning.flv');">Daily Activity Planning</a></li>
			<li><a href="#" onclick="getVideo('faster','Presentation_and_Closing.flv');">Presentation and Closing</a></li>
			<li><a href="#" onclick="getVideo('faster','Prospecting_and_Telephone_Appointment.flv');">Prospecting and Telephone Appointment</a></li>
			<li><a href="#" onclick="getVideo('faster','Simpol.flv');">Simpol</a></li>
			<li><a href="#" onclick="getVideo('faster','Simpol_Education_Plan.flv');">Simpol Education Plan</a></li>
		</ul>
	</fieldset>
	
	<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>HIGHER</div></legend>
		<ul>
			<li><a href="#" onclick="getVideo('higher','Cross_Selling_Up_Selling.flv');">Cross Selling Up Selling</a></li>
			<li><a href="#" onclick="getVideo('higher','Increasing_Case_Size.flv');">Increasing Case Size</a></li>
			<li><a href="#" onclick="getVideo('higher','Referral.flv');">Referral</a></li>
			<li><a href="#" onclick="getVideo('higher','Simpol_Benchmarking.flv');">Simpol Benchmarking</a></li>
		</ul>
	</fieldset>
	
	<script language="JavaScript">
	  flowplayer("player", "${path}/include/js/flowplayer/flowplayer-3.2.18.swf");
	</script>
</body>
</html>