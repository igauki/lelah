<%@ include file="/include/page/header.jsp"%>

<script type="text/javascript">
	hideLoadingMessage();
	

function Right(str, n){
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}


function DateAdd(objDate, intDays)
{
  var iSecond=1000;	 // Dates are represented in milliseconds
  var iMinute=60*iSecond;
  var iHour=60*iMinute;
  var iDay=24*iHour;
  var objReturnDate=new Date();
  objReturnDate.setTime(objDate.getTime()+(intDays*iDay));
  return objReturnDate;
}

function listtgl()
{
	var tanggal_beg = document.frmParam.tgl1.value;
	
	var arrMonthDays = new Array (	"31", "29", "31",
					"30", "31", "30",
					"31", "31", "30",
					"31","30","31"	  );

		var tahun_s = tanggal_beg.substring(6,10);
		var bulan_s = tanggal_beg.substring(3,5);
		var tanggal_s = tanggal_beg.substring(0,2);
		var tanggal  = tanggal_s+"/"+bulan_s+"/"+tahun_s;
		
	if ((bulan_s==2)&&  (tahun_s%4>0) ) {
	    tanggal_s=28;
	}else{
		tanggal_s=arrMonthDays[bulan_s-1];
	}	
	tanggal  = tanggal_s+"/"+bulan_s+"/"+tahun_s;

	document.frmParam._tgl2.value=tanggal;
	document.frmParam.tgl2.value=tanggal;
	document.frmParam.tgl3.value=tanggal;

}

function listtgl2()
{
	tanggal = document.frmParam._tgl2.value;
	document.frmParam._tgl2.value=tanggal;
	document.frmParam.tgl2.value=tanggal;
	document.frmParam.tgl3.value=tanggal;

}	

function cari(str){
	switch (str) {
	case "monitor" : 
		//document.getElementById('infoFrame').src='${path}/uw/viewer/summary_polis.htm?window=monitor'; 
		document.getElementById('infoFrame').src='${path}/report/uw.htm?window=monitorpolis'; 
		break;
	case "service" :
		//document.getElementById('infoFrame').src='${path}/uw/viewer/summary_polis.htm?window=service'; 
		document.getElementById('infoFrame').src='${path}/report/uw.htm?window=servicepolis'; 
		break;
	case "service_level" :
		//service_level yang baru
		document.getElementById('infoFrame').src='${path}/report/uw.htm?window=servicelevel'; 
		break;
	case "service_level_reinstate" :
		document.getElementById('infoFrame').src='${path}/report/uw.htm?window=servicelevel_reinstate'; 
		break;
	case "softcopy" :
		document.getElementById('infoFrame').src='${path}/uw/viewer/summary_polis.htm?window=softcopy'; 
		break;
	case "softcopy_perhari" :
		document.getElementById('infoFrame').src='${path}/uw/viewer/summary_polis.htm?window=softcopy_perhari'; 
		break;	
	case "simassehat" :
		document.getElementById('infoFrame').src='${path}/uw/viewer/summary_polis.htm?window=simassehat'; 
		break;		
	case "perbaikan" :
		document.getElementById('infoFrame').src='${path}/uw/viewer/summary_polis.htm?window=perbaikan'; 
		break;	
	case "service_level_ytd" :
		document.getElementById('infoFrame').src='${path}/report/uw.htm?window=servicelevelytd';
		break;	
	case "service_level_pasbp" :
		document.getElementById('infoFrame').src='${path}/report/uw.htm?window=service_level_pasbp';
		break;	
	case "report_sub_standard" :
		document.getElementById('infoFrame').src='${path}/report/uw.htm?window=report_sub_standard';
		break;	
    case "report_declined" :
        document.getElementById('infoFrame').src='${path}/report/uw.htm?window=report_spaj_declined';
        break;
    case "report_spaj_pending" :
        document.getElementById('infoFrame').src='${path}/report/uw.htm?window=report_spaj_pending'; 
	}
}


function caridaftar5()
{
 	if ( document.frmParam.nopolis.value=="" )
	{
		alert('Silahkan isi No Polis terlebih dahulu');
	}else{
		var formats = document.frmParam.formats;
		var format_ = 'pdf';
		for(i=0; i<formats.length; i++) if(formats[i].checked) format_ = formats[i].value;
		document.frmParam.bentuk.value=format_;
		frmParam.submit();
	}
} 

function caridaftar1()
{
 	if ( document.frmParam.tgl2.value=="__/__/____" || document.frmParam.tgl2.value=="" )
	{
		alert('Silahkan isi tanggal terlebih dahulu');
	}else{
		var tanggal_beg = document.frmParam.tgl2.value;
		var tahun_s = tanggal_beg.substring(6,10);
		var bulan_s = tanggal_beg.substring(3,5);
		var tanggal_s = tanggal_beg.substring(0,2);
		var tanggal2  = tahun_s+bulan_s+tanggal_s;
		var formats = document.frmParam.formats;
		var format_ = 'pdf';
		for(i=0; i<formats.length; i++) if(formats[i].checked) format_ = formats[i].value;
		document.frmParam.bentuk.value=format_;
		frmParam.submit();
	}
} 

function caridaftar()
{
 	if ( document.frmParam.tgl1.value=="__/__/____" || document.frmParam.tgl1.value=="" || document.frmParam.tgl2.value=="__/__/____" || document.frmParam.tgl2.value=="")
	{
		alert('Silahkan isi tanggal terlebih dahulu');
	}else{
		var tanggal_beg = document.frmParam.tgl1.value;
		var tahun_s = tanggal_beg.substring(6,10);
		var bulan_s = tanggal_beg.substring(3,5);
		var tanggal_s = tanggal_beg.substring(0,2);
		var tanggal1  = tahun_s+bulan_s+tanggal_s;
		var tanggal_end = document.frmParam.tgl2.value;
		tahun_s = tanggal_end.substring(6,10);
		bulan_s = tanggal_end.substring(3,5);
		tanggal_s = tanggal_end.substring(0,2);
		var tanggal2  = tahun_s+bulan_s+tanggal_s;
		var formats = document.frmParam.formats;
		var format_ = 'pdf';
		for(i=0; i<formats.length; i++) if(formats[i].checked) format_ = formats[i].value;
		document.frmParam.bentuk.value=format_;
		frmParam.submit();
	}
}	
</script>
<body style="height: 100%;"
	onload="setFrameSize('infoFrame', 68);"
	onresize="setFrameSize('infoFrame', 68);">
<form name="frmParam" method="post">

<div class="tabcontent">
<c:if test="${kunci eq null}"> 
<table class="entry" width="98%">
	<tr>
		<th>Window Policy Issue</th>
		<td>
		 <input type="button" value="MONITOR TERBIT POLIS" name="search" onClick="cari('monitor');"> 
		 <input type="button" value="SL POLICY ISSUE" name="search2" onClick="cari('service');">
		 <input type="button" value="SOFTCOPY" name="search2" onClick="cari('softcopy');"> 
		 <input type="button" value="LIST E-MAIL" name="search2" onClick="cari('softcopy_perhari');"> 
		 <input type="button" value="LIST SSE" name="search2" onClick="cari('simassehat');"> 
		 <input type="button" value="PERBAIKAN POLIS" name="search2" onClick="cari('perbaikan');"> 
		 <input type="button" value="SERVICE LEVEL YTD" name="search2" onClick="cari('service_level_ytd');">
		 <input type="button" value="SERVICE LEVEL" onClick="cari('service_level');" disabled>
		 <input type="button" value="SERVICE LEVEL REINSTATE" onClick="cari('service_level_reinstate');">
		 <input type="button" value="SERVICE LEVEL PAS-BP" onClick="cari('service_level_pasbp');">
		 <input type="button" value="REPORT SUB STANDARD" onClick="cari('report_sub_standard');">
		 <input type="button" value="REPORT DECLINED" onClick="cari('report_declined');">
		 <input type="button" value="REPORT SPAJ PENDING" onClick="cari('report_spaj_pending');">
		</td>
		<td><div id="prePostError" style="display:none;"></div></td>
	</tr>
	<tr>
		<td colspan="3">
			<iframe  name="infoFrame" id="infoFrame"
				width="100%"> Please Wait... </iframe>
		</td>
	</tr>
</table>
</c:if>
<c:if test="${kunci eq \"monitor\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  >MONITORING PENERBITAN POLIS ( DETAIL BY POLICY )
	   </th>
      </tr>
  <tr> 
        <th >Periode</th>
        <th>
              <script>inputDate('tgl1', '${tgl1}', false,'listtgl();');</script>
			s/d
              <script>inputDate('tgl2', '${tgl2}', false,'listtgl2();');</script>
			<input type="hidden" name="tgl3">
		</th>
      </tr>  
      <tr> 
        <th >Format</th>
        <th>
			<input type="radio" name="formats" value="pdf" class="noborder"> PDF
			<input type="radio" name="formats" value="xls" class="noborder"> Excel
			<input type="radio" name="formats" value="html" class="noborder"> HTML
			<input type ="hidden" name="bentuk" >
		</th>
      </tr>           
      <tr> 
        <th colspan="2" ><input type="button" value="Cari" name="search" onClick="caridaftar();"> 
		</th>
      </tr>
      <tr> 
        <td> </td>
      </tr>
    </table>
</c:if>
<c:if test="${kunci eq \"service\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  >SERVICE LEVEL (SL) POLICY ISSUE
	   </th>
      </tr>
  <tr> 
        <th >Periode</th>
        <th>
              <script>inputDate('tgl1', '${tgl1}', false,'listtgl();');</script>
			s/d
              <script>inputDate('tgl2', '${tgl2}', false,'listtgl2();');</script>
			<input type="hidden" name="tgl3">
		</th>
      </tr>  
      <tr> 
        <th >Format</th>
        <th>
			<input type="radio" name="formats" value="pdf" class="noborder"> PDF
			<input type="radio" name="formats" value="xls" class="noborder"> Excel
			<input type="radio" name="formats" value="html" class="noborder"> HTML
			<input type ="hidden" name="bentuk" >
		</th>
      </tr>           
      <tr> 
        <th colspan="2" ><input type="button" value="Cari" name="search" onClick="caridaftar();"> 
		</th>
      </tr>
      <tr> 
        <td> </td>
      </tr>
    </table>
</c:if>
<c:if test="${kunci eq \"softcopy\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  >SOFTCOPY POLIS
	   </th>
      </tr>
  <tr> 
        <th >Periode</th>
        <th>
              <script>inputDate('tgl1', '${tgl1}', false,'listtgl();');</script>
			s/d
              <script>inputDate('tgl2', '${tgl2}', false,'listtgl2();');</script>
			<input type="hidden" name="tgl3">
		</th>
      </tr>  
      <tr> 
        <th >Format</th>
        <th>
			<input type="radio" name="formats" value="pdf" class="noborder"> PDF
			<input type="radio" name="formats" value="xls" class="noborder"> Excel
			<input type="radio" name="formats" value="html" class="noborder"> HTML
			<input type ="hidden" name="bentuk" >
		</th>
      </tr>           
      <tr> 
        <th colspan="2" ><input type="button" value="Cari" name="search" onClick="caridaftar();"> 
		</th>
      </tr>
      <tr> 
        <td> </td>
      </tr>
    </table>
</c:if>
<c:if test="${kunci eq \"simassehat\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  >LIST SIMAS SEHAT EXECUTIVE
	   </th>
      </tr>
  <tr> 
        <th >Periode</th>
        <th>
              <script>inputDate('tgl1', '${tgl1}', false,'listtgl();');</script>
			s/d
              <script>inputDate('tgl2', '${tgl2}', false,'listtgl2();');</script>
			<input type="hidden" name="tgl3">
		</th>
      </tr>  
      <tr> 
        <th >Format</th>
        <th>
			<input type="radio" name="formats" value="pdf" class="noborder"> PDF
			<input type="radio" name="formats" value="xls" class="noborder"> Excel
			<input type="radio" name="formats" value="html" class="noborder"> HTML
			<input type ="hidden" name="bentuk" >
		</th>
      </tr>           
      <tr> 
        <th colspan="2" ><input type="button" value="Cari" name="search" onClick="caridaftar();"> 
		</th>
      </tr>
      <tr> 
        <td> </td>
      </tr>
    </table>
</c:if>
<c:if test="${kunci eq \"softcopy_perhari\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  >LIST E-MAIL ADDRESS
	   </th>
      </tr>
  <tr> 
        <th >Periode</th>
        <th>
              <script>inputDate('tgl2', '${tgl2}', false,'');</script>
		</th>
      </tr>  
      <tr> 
        <th >Format</th>
        <th>
			<input type="radio" name="formats" value="pdf" class="noborder"> PDF
			<input type="radio" name="formats" value="xls" class="noborder"> Excel
			<input type="radio" name="formats" value="html" class="noborder"> HTML
			<input type ="hidden" name="bentuk" >
		</th>
      </tr>           
      <tr> 
        <th colspan="2" ><input type="button" value="Cari" name="search" onClick="caridaftar1();"> 
		</th>
      </tr>
      <tr> 
        <td> </td>
      </tr>
    </table>
</c:if>

<c:if test="${kunci eq \"perbaikan\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  >PERBAIKAN POLIS
	   </th>
      </tr>
  <tr> 
        <th >No Polis</th>
        <th>
			<input type="text" name="nopolis">
		</th>
      </tr>  
      <tr> 
        <th >Format</th>
        <th>
			<input type="radio" name="formats" value="pdf" class="noborder"> PDF
			<input type="radio" name="formats" value="xls" class="noborder"> Excel
			<input type="radio" name="formats" value="html" class="noborder"> HTML
			<input type ="hidden" name="bentuk" >
		</th>
      </tr>           
      <tr> 
        <th colspan="2" ><input type="button" value="Cari" name="search" onClick="caridaftar5();"> 
		</th>
      </tr>
      <tr> 
        <td> </td>
      </tr>
    </table>
</c:if>
</div>
</form>
</body>

<%@ include file="/include/page/footer.jsp"%>