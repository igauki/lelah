<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path}/include/js/default.js"></script>
<script type="text/javascript">

	
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		
//  		$("#dropdown2").attr('disabled','disabled');
        $("#jn_report").change(function() {
			if($("#jn_report").val() == 1){
				$("#dropdown2").removeAttr('disabled','disabled');
			}else{
				$("#dropdown2").attr('disabled','disabled');
				
			}
        });
//         if('${jnreport == 1}'){
// 			$("#jn_report option:second").attr('selected','selected');
// 		}else{
// 			$("#jn_report option:first").attr('selected','selected');
// 		}
        

// 		$("#showCL").click(function() {
//			$("#fieldset").show();
// 		});

		if('${warn}'){
			<c:if test="${warn eq 'Polis Berhasil Diproses'}">	
				spaj = "${temp_spaj}";
				popWin('${path}/uw/uw.htm?window=reportCoverLetterJne&flag=clBsm&spaj='+spaj+'&dist=1', 300, 400);
		  	</c:if>
		  	<c:if test="${warn ne 'Polis Berhasil Diproses'}">		  	  	
		  	  	alert('${warn}');	  
		  	</c:if>
		}

	});
</script>


<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

</style>

<body>

<form id="formPost" name="formPost" method="post" target="">
	<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>Pengiriman Polis BSM Melalui Dept. General Affair (GA)</div></legend>
		
		<div class="rowElem" id="dropdown1">
			<label>Jenis Report :</label>
			<select size="1" name="jn_report" id="jn_report" title="Pilih Jenis Report">
				<option value="0" <c:if test="${jnreport ne 1}">selected="selected"</c:if>>COVERING LETTER</option>
				<option value="1" <c:if test="${jnreport eq 1}">selected="selected"</c:if>>COVERING LETTER PER CABANG</option>
			</select>
		</div>
		
		<div class="rowElem" id="dropdown2" <c:if test="${jnreport ne 1}">disabled="disabled"</c:if>>
			<label>Cabang BSM :</label>
			<select size="1" name="branch" id="branch" title="Pilih Cabang BSM">
				<option value="emp">---</option>
				<c:forEach var="c" items="${bradmin}" varStatus="s">
					<option value="${c.KEY}" <c:if test="${branch eq c.KEY}">selected="selected"</c:if>
					>${c.VALUE}</option>
				</c:forEach>
			</select>
		</div>
	
		<div class="rowElem">
			<label>Tgl Print Polis :</label>
			<input name="bdate" id="bdate" type="text" class="datepicker" title="Tanggal Awal" value="${bdate}"> s/d 
			<input name="edate" id="edate" type="text" class="datepicker" title="Tanggal Akhir" value="${edate}">
		</div>
		
		<div class="rowElem">
			<label>Status Polis :</label>
			<c:if test="${option eq \"uw\"}">
				<input name="st_polis" id="st_polis" type="radio" class="radio" value="0" <c:if test="${stpolis eq 0}">checked="checked"</c:if>>Polis Yang Belum Dikirim Ke GA
		    	<input name="st_polis" id="st_polis" type="radio" class="radio" value="1" <c:if test="${stpolis eq 1}">checked="checked"</c:if>>Polis Yang Sudah Dikirim Ke GA
		    </c:if>
		    <c:if test="${option eq \"ga\"}">
		    	<input name="st_polis" id="st_polis" type="radio" class="radio" value="1" <c:if test="${stpolis eq 1}">checked="checked"</c:if>>Polis Yang Sudah Dikirim Oleh UW
		    	<input name="st_polis" id="st_polis" type="radio" class="radio" value="2" <c:if test="${stpolis eq 2}">checked="checked"</c:if>>Polis Yang Sudah Diterima Oleh GA dan Belum Dikirim Ke ADMIN
		    	<input name="st_polis" id="st_polis" type="radio" class="radio" value="3" <c:if test="${stpolis eq 3}">checked="checked"</c:if>>Polis Yang Sudah Dikirim Ke ADMIN
		    </c:if>
		    <c:if test="${option eq \"adm\"}">
		    	<input name="st_polis" id="st_polis" type="radio" class="radio" value="3" <c:if test="${stpolis eq 3}">checked="checked"</c:if>>Polis Yang Sudah Dikirim Oleh GA
		    	<input name="st_polis" id="st_polis" type="radio" class="radio" value="4" <c:if test="${stpolis eq 4}">checked="checked"</c:if>>Polis Yang Sudah Diterima Oleh ADMIN dan Belum Dikirim ke AGENT
		    	<input name="st_polis" id="st_polis" type="radio" class="radio" value="5" <c:if test="${stpolis eq 5}">checked="checked"</c:if>>Polis Yang Sudah Dikirim Ke AGENT
		    </c:if>
		</div>

		<c:if test="${option eq \"all\"}">
			<div class="rowElem">
				<label>Status Polis (UW)  :</label>
				<input name="st_polis" id="st_polis" type="radio" class="radio" value="0">Belum Dikirim Ke GA	   
			    <input name="st_polis" id="st_polis" type="radio" class="radio" value="1">Sudah Dikirim Ke GA
			</div>
			<div class="rowElem">
				<label>Status Polis (GA)  :</label>
			    <input name="st_polis" id="st_polis" type="radio" class="radio" value="1">Sudah Dikirim Oleh UW   
			    <input name="st_polis" id="st_polis" type="radio" class="radio" value="2">Sudah Diterima Oleh GA dan Belum Dikirim Ke Cabang BSM
			    <input name="st_polis" id="st_polis" type="radio" class="radio" value="3">Sudah Dikirim Ke Cabang BSM
			</div>
			<div class="rowElem">
				<label>Status Polis (Admin)  :</label>
			    <input name="st_polis" id="st_polis" type="radio" class="radio" value="3">Sudah Dikirim Oleh GA
			    <input name="st_polis" id="st_polis" type="radio" class="radio" value="4">Sudah Diterima Oleh Cabang BSM dan Belum Dikirim ke Cabang BSM
			    <input name="st_polis" id="st_polis" type="radio" class="radio" value="5">Sudah Dikirim Ke Agent
			</div>
		</c:if>

		<div class="rowElem">
			<label></label>
			<input type="submit" name="showCL" id="showCL" value="Show Polis">
			<c:if test="${option eq \"all\" or option eq \"uw\"}">
				<label></label>
				<input type="submit" name="showReport" id="showReport" value="Show Report">
			</c:if>
		</div>
	</fieldset>

	<c:if test="${(option eq \"uw\" and stpolis eq \"1\") or (option eq \"ga\" and stpolis eq \"3\") or (option eq \"adm\" and stpolis eq \"5\")}">
		<fieldset id="fieldset">
			<table class="displaytag" cellpadding='0' cellspacing='0' border="1" align="center">
				<tr>
					<!-- <th>Check All<br/><input type="checkbox" name="allCheck" onClick="selectallMe()"></th> -->
					<th nowrap align="center" width="120" bgcolor="#b7b7b7">No. Polis</th>
					<th nowrap align="center" width="200" bgcolor="#b7b7b7">Pemegang Polis</th>
					<th nowrap align="center" width="200" bgcolor="#b7b7b7">Tertanggung</th>
					<th nowrap align="center" width="200" bgcolor="#b7b7b7">Nama Refferal</th>
					<th nowrap align="center" width="120" bgcolor="#b7b7b7">Produk</th>
					<th nowrap align="center" width="100" bgcolor="#b7b7b7">Tgl. Cetak Polis</th>
					<th nowrap align="center" width="50" bgcolor="#b7b7b7">SimasCard</th>
					<th nowrap align="center" width="50" bgcolor="#b7b7b7">Kartu Admedika</th>
					<c:if test="${option eq \"ga\"}">
						<th nowrap align="center" width="150" bgcolor="#b7b7b7">No. PO</th>
					</c:if>
				</tr>
				<c:forEach items="${dbpolis}" var="d">
				<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';return true;" onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;">
					<td nowrap>${d.NOPOL}</td>
					<td nowrap>${d.PP}</td>
					<td nowrap>${d.TT}</td>
					<td nowrap>${d.NAMAREFF}</td>
					<td nowrap>${d.PROD}</td>
					<td nowrap>${d.DATPRINT}</td>
					<td nowrap>${d.SIMCARD}</td>
					<td nowrap>${d.ADMED}</td>
					<c:if test="${option eq \"ga\"}">
						<td nowrap>${d.NOPO}</td>
					</c:if>
				</tr>
				</c:forEach>       
			</table>
		</fieldset>
	</c:if>
	
	<c:if test="${(option eq \"uw\" and stpolis eq \"0\") or (option eq \"ga\" and stpolis eq \"1\") or (option eq \"ga\" and stpolis eq \"2\") or (option eq \"adm\" and stpolis eq \"3\") or (option eq \"adm\" and stpolis eq \"4\") or (option eq \"all\")}">
		<fieldset id="fieldset">
			<table class="displaytag" cellpadding='0' cellspacing='0' border="1" align="center">
				<tr>
					<!-- <th>Check All<br/><input type="checkbox" name="allCheck" onClick="selectallMe()"></th> -->
					<th nowrap align="center" bgcolor="#b7b7b7">Check</th>
					<th nowrap align="center" width="120" bgcolor="#b7b7b7">No. Polis</th>
					<th nowrap align="center" width="200" bgcolor="#b7b7b7">Pemegang Polis</th>
					<th nowrap align="center" width="200" bgcolor="#b7b7b7">Tertanggung</th>
					<th nowrap align="center" width="200" bgcolor="#b7b7b7">Nama Refferal</th>
					<th nowrap align="center" width="120" bgcolor="#b7b7b7">Produk</th>
					<th nowrap align="center" width="100" bgcolor="#b7b7b7">Tgl. Cetak Polis</th>
					<th nowrap align="center" width="50" bgcolor="#b7b7b7">SimasCard</th>
					<th nowrap align="center" width="50" bgcolor="#b7b7b7">Kartu Admedika</th>
					<c:if test="${(option eq \"ga\" and stpolis eq \"2\")}">
						<th nowrap align="center" width="150" bgcolor="#b7b7b7">No. PO</th>
					</c:if>
				</tr>
				<c:forEach items="${dbpolis}" var="d">
				<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';return true;" onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;">
					<td align="center"><input type="checkbox" class="noBorder" name="chbox" value="${d.NOPOL}~${d.IDCAB}" id="chbox"></td>
					<td nowrap>${d.NOPOL}</td>
					<td nowrap>${d.PP}</td>
					<td nowrap>${d.TT}</td>
					<td nowrap>${d.NAMAREFF}</td>
					<td nowrap>${d.PROD}</td>
					<td nowrap>${d.DATPRINT}</td>
					<c:if test="${(option eq \"uw\" and stpolis eq \"0\")}">
						<td align="center"><input type="checkbox" class="noBorder" name="chsim" value="${d.NOPOL}" id="chsim" <c:if test="${(d.SIMCARD) eq \"ADA\"}">checked="checked"</c:if>></td>
						<td align="center"><input type="checkbox" class="noBorder" name="chadme" value="${d.NOPOL}" id="chadme" <c:if test="${(d.ADMED) eq \"ADA\"}">checked="checked"</c:if></td>
					</c:if>
					<c:if test="${(option ne \"uw\" and stpolis ne \"0\")}">
						<td nowrap>${d.SIMCARD}</td>
						<td nowrap>${d.ADMED}</td>
					</c:if>
					<c:if test="${(option eq \"ga\" and stpolis eq \"2\")}">
						<td align="center"><input type="text" id="po" name="po" value=""/></td>
					</c:if>
				</tr>
				</c:forEach>       
			</table>
			<table cellpadding='0' cellspacing='0' align="center">
				<tr>
					<th></th><td><br/></td>
				</tr>
				<tr>
					<div class="rowElem">
						<label></label>
						<input type="submit" name="sendCL" id="sendCL" value="Send/Save<c:if test="${qa ne null}">${qa}</c:if>">
					</div>
				</tr>
			</table>
		</fieldset>
	</c:if>
	
</form>

</body>
</html>