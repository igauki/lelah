<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<style>
    #email input,
    #email textarea {
        text-transform: none;
    }
</style>
<script type="text/javascript">
	function ganti(flag){	
		if(flag==2){
			document.getElementById('sms').style.display = "block";
			document.getElementById('save').value="Send SMS";
		} else if(flag == 5) {
		    var from = '${cmd.sms_in.originator}';//document.formpost.dari.value;
		    var sent = document.formpost.tgl.value;
		    var pesan = document.formpost.pesan.value;
		    var result = '${cmd.result}';
		    if(result != 'Update Follow Up SMS Berhasil') {
		        document.formpost.email_msg.value = "From: " + from + "\n" +
                "Sent: " + sent + "\n\n" + pesan;
		    }
		    document.getElementById('sms').style.display = "none";
		    document.getElementById('email').style.display = "block";
		    document.getElementById('save').value = "Send Email";
		}else{
			document.getElementById('sms').style.display = "none";
			document.getElementById('email').style.display = "none";
			document.getElementById('save').value="Save Changes";
		}
	}	
	
</script>


<body  style="height: 100%;" onload="setupPanes('container1', 'tab${cmd.pane}');">
<c:choose>
	<c:when test="${not empty cmd.notCS}">
		<div id="error">
			Maaf, Anda tidak memiliki akses untuk menu ini
		</div>
	</c:when>
	<c:otherwise>
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)"id="tab1">VIEW SMS</a>
					<a href="#" onClick="return showPane('pane2', this)" id="tab2">HISTORY SMS IN</a>					
					<a href="#" onClick="return showPane('pane3', this)" id="tab3">HISTORY SMS Out</a>	
				</li>
			</ul>
	
			<div class="tab-panes">
	
				<div id="pane1" class="panes">
					<div id="contents">
					<fieldset>
					<form method="post" name="formpost">
						<input type="hidden" name="id" value="${cmd.id }">
						<legend>
							VIEW SMS
						</legend>
						<br/><br/>
						<table class="simple" id="callList" style="vertical-align: top">
							<tbody>
								<tr>
									<th style="vertical-align: top">
										Dari 
									</th>
									<th style="vertical-align: top"> :</th>
									<td>
										<input type="text" size="30" readonly="readonly" name="dari" value="${cmd.sms_in.originator}">
									</td>
								</tr>
								<tr>
									<th style="vertical-align: top">
										Tanggal Diterima 
									</th>
									<th style="vertical-align: top"> :</th>
									<td>
										<input type="text" size="30" readonly="readonly" name="tgl" value="<fmt:formatDate value="${cmd.sms_in.receive_date}" pattern="dd/MM/yyyy HH:mm:ss"/>">
										
									</td>
								</tr>
								<tr>
									<th style="vertical-align: top">
										Pesan 
									</th>
									<th style="vertical-align: top"> :</th>
									<td>
										<textarea rows="5" cols="100" name="pesan" readonly="readonly"> ${cmd.sms_in.text}</textarea>
									</td>
								</tr>
								<tr>
									<th style="vertical-align: top">
										USER FOLLOW UP NOW
									</th>
									<th style="vertical-align: top"> :</th>
									<td>
										<input type="text" size="30" readonly="readonly" name="dari" value="${sessionScope.currentUser.name}">
									</td>
								</tr>
								<tr>
									<th style="vertical-align: top">
										Status Follow Up
									</th>
									<th style="vertical-align: top"> :</th>
									<td>
										<select name="process" onchange="ganti(this.value);"  <c:if test="${cmd.sms_in.process ne \"0\" and cmd.sms_in.process ne \"4\" }">disabled="disabled"</c:if>>
											<option value="0" <c:if test="${cmd.process eq \"0\"}">selected="selected"</c:if>>belum di follow up</option>
											<option value="4" <c:if test="${cmd.process eq \"4\"}">selected="selected"</c:if>>perlu follow up lanjutan</option>
											<option value="1" <c:if test="${cmd.process eq \"1\"}">selected="selected"</c:if>>follow up by phone</option>
											<option value="2" <c:if test="${cmd.process eq \"2\"}">selected="selected"</c:if>>follow up by sms</option>
											<option value="3" <c:if test="${cmd.process eq \"3\"}">selected="selected"</c:if>>tidak perlu di follow up</option>
											<option value="5" <c:if test="${cmd.process eq \"5\"}">selected="selected"</c:if>>follow up by email</option>
										</select>
									</td>
								</tr>
								<tr>
									<th style="vertical-align: top">
										Keterangan Follow Up
									</th>
									<th style="vertical-align: top"> :</th>
									<td>
										<textarea rows="5" cols="70" name="keterangan">${cmd.keterangan }</textarea>
									</td>
								</tr>
							
							</tbody>
						</table>
						<table id="sms" style="display: none;">
							<tr>
								<th style="vertical-align: top">
									Isi SMS Balasan
								</th>
								<th style="vertical-align: top"> :</th>
								<td>
									<textarea rows="5" cols="70" name="msg_reply">${cmd.msg_reply}</textarea>
								</td>
							</tr>
						</table>
						<table id="email" class="simple" style="display: none; margin-top: 10px;">
						    <tr>
						        <th colspan="3" style="text-align: center; font-size: 16px;">Email Follow Up</th>
						    </tr>
						    <tr>
						        <th style="vertical-align: top">TO</th>
						        <th style="vertical-align: top">:</th>
						        <td>
						            <input type="text" name="email_to" size="100" value="${cmd.email_to}" title="Pisahkan email tujuan dengan titik koma (;)">
						        </td>
						    </tr>
						    <tr>
						        <th style="vertical-align: top">CC</th>
                                <th style="vertical-align: top">:</th>
						        <td>
						            <input type="text" name="email_cc" size="100" value="${cmd.email_cc}" title="Pisahkan email tujuan dengan titik koma (;)">
						        </td>
						    </tr>
						    <tr>
						        <th style="vertical-align: top">BCC</th>
                                <th style="vertical-align: top">:</th>
						        <td>
						            <input type="text" name="email_bcc" size="100" value="${cmd.email_bcc}" title="Pisahkan email tujuan dengan titik koma (;)">
						        </td>
						    </tr>
						    <tr>
						        <th style="vertical-align: top">SUBJECT</th>
                                <th style="vertical-align: top">:</th>
						        <td>
						            <input type="text" name="email_subject" size="100" value="${cmd.email_subject}">
						        </td>
						    </tr>
						    <tr>
						        <th style="vertical-align: top">MESSAGE</th>
                                <th style="vertical-align: top">:</th>
						        <td>
						            <textarea rows="15" style="width: 100%" name="email_msg">${cmd.email_msg}</textarea>
						        </td>
						    </tr>
						</table>
						<c:if test="${not empty cmd.result}">
							<div id="success">
								${cmd.result}
							</div>
						</c:if>
						<c:if test="${not empty cmd.errorMessage}">
							<div id="error">
								Data tidak berhasil disimpan:<br>
								<c:forEach var="error" items="${cmd.errorMessage}">
									- <c:out value="${error}" escapeXml="false" />
									<br />
								</c:forEach>
							</div>
						</c:if>
						<br/>
						<c:if test="${cmd.sms_in.process eq \"0\" or cmd.sms_in.process eq \"4\"}">
							<input type="submit" id="save" name="save" value="Save Changes" >
							<input type="button" name="close" value="Close" onclick="window.close();">
						</c:if>
					</form>
					</fieldset>
					</div>
				</div>
				<div id="pane2" class="panes">
				
						<fieldset>
						<legend>
							History SMS masuk dari ${cmd.sms_in.originator}
						</legend>
						
						
							<display:table id="baris" uid="barisan" style="width:90%; " name="cmd.sms_in_hist" class="displaytag" requestURI="${path}/uw/viewer.htm?window=cssms_view&pane=2"  export="true" pagesize="20">
								<display:column property="id" title="ID"  style="width:50px; vertical-align: top; text-align: center;"   sortable="true" />
								<display:column property="originator" format="''{0}"  title="Pengirim" style="width:90px; vertical-align: top; text-align: center;"  sortable="true" />
								<display:column property="receive_date" title="Tanggal Terima" style="width:90px; vertical-align: top; text-align: center;" format="{0, date, dd/MM/yyyy HH:mm:ss}" sortable="true"/>
								<display:column property="text" title="PESAN" style="vertical-align: top; text-align: left;"  sortable="true"/>
								<display:column style="width:90px; vertical-align: top; text-align: center;"  title="Follow Up" href="${path}/uw/viewer.htm?window=cssms_view" paramId="id" paramProperty="id" sortable="true">
									<c:choose>
										<c:when test="${barisan.process eq \"1\" }">
											Follow Up  by Phone
										</c:when>
										<c:when test="${barisan.process eq \"2\" }">
											Follow Up by SMS
										</c:when>
										<c:when test="${barisan.process eq \"3\" }">
											Tidak Perlu DI Follow Up
										</c:when>
										<c:when test="${barisan.process eq \"4\" }">
											Perlu Follow Up Lanjutan
										</c:when>
										<c:otherwise>
											Belum di follow up
										</c:otherwise>
									</c:choose>
									
								</display:column>
								<display:column property="username"  style="width:90px; vertical-align: top; text-align: center;" title="User Follow Up" sortable="true" />
								<display:column property="process_date"  style="width:90px; vertical-align: top; text-align: center;" title="Tgl Follow Up" sortable="true" format="{0, date, dd/MM/yyyy HH:mm:ss}"/>
								<display:column property="proses_ket"  style="width:90px; vertical-align: top; text-align: center;" title="Ket Follow Up" sortable="true" />
								<display:column property="msg_reply" title="SMS Balasan" style="width:150px;vertical-align: top; text-align: left;"  sortable="true"/>
								<display:column style="vertical-align: center; text-align: center;" title="STATUS SMS" sortable="true">
									<c:choose>
										<c:when test="${barisan.status_delivery eq \"S\"}">
											Sent
										</c:when>
										<c:when test="${barisan.status_delivery eq \"D\"}">
											Delivered
										</c:when>
										<c:when test="${barisan.status_delivery eq \"Q\"}">
											Queued
										</c:when>
										<c:when test="${barisan.status_delivery eq \"A\"}">
											Aborted
										</c:when>
										<c:when test="${barisan.status_delivery eq \"U\"}">
											Unsent
										</c:when>
										<c:when test="${barisan.status_delivery eq \"F\"}">
											Failed
										</c:when>
										<c:when test="${barisan.status_delivery eq \"P\"}">
											Pending
										</c:when>
										<c:otherwise>
											
										</c:otherwise>
									</c:choose>										
								</display:column>
							</display:table>
						
						</fieldset>
				
				</div>
				<div id="pane3" class="panes">
					
						<fieldset>
						<legend>
							History SMS keluar ke ${cmd.sms_in.originator}
						</legend>
					
							<display:table id="baris2" uid="barisan2" style="width:90%; " name="cmd.sms_out_hist" class="displaytag" requestURI="${path}/uw/viewer.htm?window=cssms_view&pane=3"  export="true" pagesize="2">
								<display:column property="id" title="ID"  sortable="true" />
								<display:column property="recipient" format="''{0}" title="Penerima" style="width:90px; vertical-align: top; text-align: center;"  sortable="true" />								
								<display:column style="vertical-align: center; text-align: center;" title="Tanggal Kirim" sortable="true">
									<fmt:formatDate value="${barisan2.sent_modem_date }" pattern="dd/MM/yyyy hh:mm:ss" /> 
									<br> --- 
									<fmt:formatDate value="${barisan2.sent_date}" pattern="dd/MM/yyyy hh:mm:ss" />
								</display:column>
								<display:column property="text" title="PESAN" style="vertical-align: top; text-align: left;"  sortable="true"/>
								<display:column property="username" title="User Pengirim" style="width:90px; vertical-align: top; text-align: center;"  sortable="true" />
								<display:column style="vertical-align: center; text-align: center;" title="STATUS SMS" sortable="true">
									<c:choose>
										<c:when test="${barisan2.status eq \"S\"}">
											Sent
										</c:when>
										<c:when test="${barisan2.status eq \"D\"}">
											Delivered
										</c:when>
										<c:when test="${barisan2.status eq \"Q\"}">
											Queued
										</c:when>
										<c:when test="${barisan2.status eq \"A\"}">
											Aborted
										</c:when>
										<c:when test="${barisan2.status eq \"U\"}">
											Unsent
										</c:when>
										<c:when test="${barisan2.status eq \"F\"}">
											Failed
										</c:when>
										<c:when test="${barisan2.status eq \"P\"}">
											Pending
										</c:when>
										<c:otherwise>
											
										</c:otherwise>
									</c:choose>										
								</display:column>
								<display:column property="id_refrence" style="vertical-align: center; text-align: center;" title="ID SMS MASUK" href="${path}/uw/viewer.htm?window=cssms_view" paramId="id"/>
							</display:table>
						
						</fieldset>
					
				</div>
			</div>
		</div>
		<script type="text/javascript">		
				ganti("${cmd.process}");
			
		</script>
	</c:otherwise>
</c:choose>
</body>
<%@ include file="/include/page/footer.jsp"%>