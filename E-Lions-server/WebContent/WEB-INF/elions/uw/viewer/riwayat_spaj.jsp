<br><%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<script type="text/javascript">
</script>
<script type="text/javascript" src="${path }/include/js/default.js"></script>
<BODY>
            <fieldset>
                <legend>List Produk Kesehatan</legend>
                <display:table name="healthproduct" id="H" class="displaytag" export="false">          
                    <display:column property="NAMA_PP" title="NAMA PP" />
                    <display:column property="NAMA_TT" title="NAMA TTG"/>
                    <%--<display:column property="BOD_PP" title="TGL LAHIR PMG" format="{0, date, dd/MM/yyyy}" />--%>
                    <display:column property="BOD_TT" title="TGL LAHIR TTG" format="{0, date, dd/MM/yyyy}" />
                    <display:column title="NO POLIS">
                        <script type="text/javascript">document.write(formatPolis('${H.POLIS }'));</script>
                    </display:column>
                    <display:column property="MSPO_BEG_DATE" title="MULAI PERTANGGUNGAN" format="{0, date, dd/MM/yyyy}" />
                    <display:column property="MSPO_END_DATE" title="SELESAI PERTANGGUNGAN" format="{0, date, dd/MM/yyyy}" />
                    <display:column property="LSBS_NAME" title="NAMA PRODUK"/>
                    <display:column property="KODE_DESC" title="DISTRIBUSI"/> 
                </display:table>
            </fieldset>
             <fieldset>
                <legend>History Pengajuan SPAJ/POLIS a/n ${cmd.pemegang.mcl_first}</legend>
                <display:table name="history_pengajuan" id="H" class="displaytag" export="false">          
                    <display:column title="NO SPAJ">
                        <script type="text/javascript">document.write(formatPolis('${H.REG_SPAJ }'));</script>
                    </display:column>
                    <display:column title="NO POLIS">
                        <script type="text/javascript">document.write(formatPolis('${H.MSPO_POLICY_NO }'));</script>
                    </display:column>
                    <display:column property="PEMEGANG" title="PEMEGANG POLIS" />
                     <display:column property="TERTANGGUNG" title="TERTANGGUNG" />
                     <display:column property="LSDBS_NAME" title="PRODUK" />
                    <display:column property="MSPR_BEG_DATE" title="MULAI PERTANGGUNGAN" format="{0, date, dd/MM/yyyy}" />
                    <display:column property="MSPR_END_DATE" title="SELESAI PERTANGGUNGAN" format="{0, date, dd/MM/yyyy}" />
                    <display:column property="MSPR_TSI" title="UANG PERTANGGUNGAN"/>
                    <display:column property="MSPR_PREMIUM" title="PREMI"/> 
                </display:table>
            </fieldset>
            <fieldset>
                <legend>Attention List</legend>
                <display:table name="blacklist" id="H" class="displaytag" export="false">          
                    <display:column property="LBL_NAMA" title="NAMA" />
                    <display:column property="LBL_TGL_LAHIR" title="TANGGAL LAHIR" format="{0, date, dd/MM/yyyy}" />
                    <display:column property="LBL_SUMBER_INFORMASI" title="SUMBER INFORMASI" />
                    <display:column property="LBL_ASURANSI" title="PRODUK ASURANSI" />
                    <display:column property="LBL_ALAMAT" title="ALAMAT" />
                    <display:column property="LBL_ALASAN" title="ALASAN" />
                    <display:column property="LUS_FULL_NAME" title="USER PROSES" />
                </display:table>
            </fieldset>
</body>
<%@ include file="/include/page/footer.jsp"%>