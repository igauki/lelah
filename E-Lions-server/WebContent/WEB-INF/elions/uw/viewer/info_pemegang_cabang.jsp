<%@ include file="/include/page/taglibs.jsp"%>

<table class="entry2">

	<tr>
		<th nowrap="nowrap">
			Nama Lengkap
		</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select name="pemegang.lti_id" disabled="disabled">
				<option value=""></option>
				<c:forEach var="l" items="${select_gelar}">
					<option
						<c:if test="${cmd.pemegang.lti_id eq l.ID}"> SELECTED </c:if>
						value="${l.ID}">${l.GELAR}</option>
				</c:forEach>
			</select>				
			<input type="text" value="${cmd.pemegang.mcl_first}" size="35" readonly>
			Gelar
			<input type="text" value="${cmd.pemegang.mcl_gelar}" size="12" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">No. Telepon Rumah</th>
		<th nowrap="nowrap" class="left">
			<select style="width: 191px;">
				<OPTGROUP label="Telepon Rumah">
					<c:if test="${not empty cmd.pemegang.telpon_rumah}"><option>${cmd.pemegang.area_code_rumah} - ${cmd.pemegang.telpon_rumah}</option></c:if>
					<c:if test="${not empty cmd.pemegang.telpon_rumah2}"><option>${cmd.pemegang.area_code_rumah2} - ${cmd.pemegang.telpon_rumah2}</option></c:if>
				</OPTGROUP>
				<OPTGROUP label="Handphone">
					<c:if test="${not empty cmd.pemegang.no_hp}"><option>${cmd.pemegang.no_hp}</option></c:if>
					<c:if test="${not empty cmd.pemegang.no_hp2}"><option>${cmd.pemegang.no_hp2}</option></c:if>										
				</OPTGROUP>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">No. Telepon Kantor</th>
		<th nowrap="nowrap" class="left">
			<select style="width: 191px;">
				<OPTGROUP label="Telepon Kantor">
					<c:if test="${not empty cmd.pemegang.telpon_kantor}"><option>${cmd.pemegang.area_code_kantor} - ${cmd.pemegang.telpon_kantor}</option></c:if>
					<c:if test="${not empty cmd.pemegang.telpon_kantor2}"><option>${cmd.pemegang.area_code_kantor2} - ${cmd.pemegang.telpon_kantor2}</option></c:if>
				</OPTGROUP>
				<OPTGROUP label="Fax">
					<c:if test="${not empty cmd.pemegang.no_fax}"><option>${cmd.pemegang.area_code_fax} - ${cmd.pemegang.no_fax}</option></c:if>
				</OPTGROUP>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">No. Telepon Lainnya</th>
		<th nowrap="nowrap" class="left">
			<select style="width: 191px;">
				<OPTGROUP label="No. Telepon">
					<c:if test="${not empty cmd.addressbilling.msap_phone1}"><option>${cmd.addressbilling.msap_area_code1} - ${cmd.addressbilling.msap_phone1}</option></c:if>
					<c:if test="${not empty cmd.addressbilling.msap_phone2}"><option>${cmd.addressbilling.msap_area_code2} - ${cmd.addressbilling.msap_phone2}</option></c:if>
					<c:if test="${not empty cmd.addressbilling.msap_phone3}"><option>${cmd.addressbilling.msap_area_code3} - ${cmd.addressbilling.msap_phone3}</option></c:if>
				</OPTGROUP>
				<OPTGROUP label="Handphone">
					<c:if test="${not empty cmd.addressbilling.no_hp}"><option>${cmd.addressbilling.no_hp}</option></c:if>
					<c:if test="${not empty cmd.addressbilling.no_hp2}"><option>${cmd.addressbilling.no_hp2}</option></c:if>
				</OPTGROUP>
				<OPTGROUP label="Fax">
					<c:if test="${not empty cmd.addressbilling.msap_fax1}"><option>${cmd.addressbilling.msap_area_code_fax1} - ${cmd.addressbilling.msap_fax1}</option></c:if>
				</OPTGROUP>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap" colspan="3">
			Hubungan Calon Pemegang Polis dengan Calon Tertanggung
			<select disabled>
				<c:forEach var="relasi" items="${select_relasi}">
					<option
						<c:if test="${cmd.pemegang.lsre_id eq relasi.ID}"> SELECTED </c:if>
						value="${relasi.ID}">${relasi.RELATION}</option>
				</c:forEach>
			</select>
		</th>
	</tr>
	<tr>
		<th nowrap="nowrap" colspan="3">
			Jenis Form SPAJ
			<select name="pemegang.mspo_flag_spaj" disabled> 
						<option value="0" <c:if test="${cmd.pemegang.mspo_flag_spaj eq 0}">selected="selected"</c:if>>SPAJ LAMA</option>
						<option value="1" <c:if test="${cmd.pemegang.mspo_flag_spaj eq 1}">selected="selected"</c:if>>SPAJ BARU</option>
			</select>
			No.Virtual Account
			<input type="text" value="${cmd.tertanggung.mste_no_vacc}" size="25" readonly>
			
		</th>
	</tr>
	
</table>