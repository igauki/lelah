<%@ include file="/include/page/header.jsp"%><c:set var="path" value="${pageContext.request.contextPath}" />
<style type="text/css">
	html,body{
	     overflow: hidden;
	}
</style>
<script>
	function cekSpaj(){
		if(trim(document.formpost.spaj.value)=='') {
			alert('Harap cari nomor SPAJ terlebih dahulu');
			popWin('${path}/uw/spaj.htm?posisi=-1', 350, 450);
		}else return true;
		
		return false;
	}
	
	function awal(){
		if (document.formpost.spaj.value != "")
		{
			cariregion(document.formpost.spaj.value,'region');
		}
	}

	function monyong(y){
		if('cari'==y) {
			popWin('${path}/uw/spaj.htm?posisi=-1&win=viewer&kata='+document.formpost.spaj.value, 350, 450);
		}else if('spaj'==y) {
			if(cekSpaj()){
				document.getElementById('infoFrame').src='${path}/uw/view.htm?p=v&showSPAJ='+document.formpost.spaj.value;
			}
		}else if('alamat'==y) {if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=addressbilling&spaj='+document.formpost.spaj.value;
		}else if('upload_nb'==y) {
			if(cekSpaj()) document.getElementById('infoFrame').src='${path}/uw/upload_nb.htm?reg_spaj='+document.formpost.spaj.value;
		}else if('uwinfo'==y) {
			if(cekSpaj()) document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=uwinfo&spaj='+document.formpost.spaj.value;
		}else if('view'==y) {if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=viewerkontrol&spaj='+document.formpost.spaj.value;
		}else if('investasi'==y) {if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=investasi&spaj='+document.formpost.spaj.value, 500, 700; 
		}
	}

	function cariregion(spaj,nama)
	{
			ajaxManager.listcollect(spaj , nama,
			{callback:function(map) {
		
				DWRUtil.useLoadingMessage();
				document.formpost.koderegion.value=map.LSRG_NAMA;
				document.formpost.numberbisnis.value = map.LSDBS_NUMBER;
				document.formpost.kodebisnis.value = map.LSBS_ID;
			   },
			  timeout:15000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
	
</script>
<body 
	onload="setFrameSize('infoFrame', 90); setFrameSize('docFrame', 90);" onresize="setFrameSize('infoFrame', 90); setFrameSize('docFrame', 90);" style="height: 100%;">
<form name="formpost" method="post">
<div class="tabcontent">
<table class="entry2" style="width: 98%;">
	<tr>
		<th style="width: 50px;">SPAJ</th>
		<th>
			<input type="hidden" name="koderegion" >
			<input type="hidden" name="kodebisnis" >
			<input type="hidden" name="numberbisnis" >
			<input type="text" size="15" readonly="readonly" class="readOnly" id="spaj" name="spaj" ></th>
		<td>
			<input type="button" value="Info" 	name="tampilkan" 	onclick="monyong('spaj');">
			<input type="button" value="Cari" 	name="search" 		onclick="monyong('cari');">
			<input type="button" value="Alamat" name="alamat" 		onclick="monyong('alamat');">
			<input type="button" value="Upload Scan" name="upload_nb" 
			<c:if test="${currentUser.lus_id eq 3951}">
				disabled
			</c:if>
			onclick="monyong('upload_nb');">	
		</td>
	</tr>
	<tr>
		<th>Polis</th>
		<th><input type="text" size="20" readonly="readonly" class="readOnly" id="polis" name="polis"></th>
		<td>
			<input type="button" value="Histori Dokumen" name="uwinfo" onclick="monyong('uwinfo');">
			<input type="button" value="Investasi" name="investasi" id="investasi" ${cmd.dsDisabled } onclick="monyong('investasi');" 
					style="width: 70px;" accesskey="I" onmouseover="return overlib('Alt-I', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="View" name="view" onclick="monyong('view');" ${cmd.dsDisabled }
					style="width: 38px;" accesskey="V" onmouseover="return overlib('Alt-V', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Auto Debet" name="auto" onclick="monyong('auto');" ${cmd.dsDisabled }
					style="width: 120px;">							
		</td>
	</tr>
	
	<tr>
		<td colspan="3">
			<table style="margin: 0 0 0 0; padding: 0 0 0 0; width: 100%;">
				<tr>
					<td>
						<spring:bind path="cmd.*">
							<iframe src="${path}/uw/spaj.htm?posisi=-1&win=viewer" name="infoFrame" id="infoFrame" width="100%"> Please Wait... </iframe>
						</spring:bind>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>