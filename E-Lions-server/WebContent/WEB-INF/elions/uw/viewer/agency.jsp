<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	function cari(){
		if(trim(document.formpost.kata.value)=='') return false;
		else createLoadingMessage();	
	}
	
	function backToParent1(msag_id){
		
		msag_id = "000000"+msag_id;
		msag_id = msag_id.substr(msag_id.length-6,msag_id.length);
		
		popWin('${path}/uw/viewer/agen.htm?window=agen&msag_id='+msag_id, 500, 700); 
	}


</script>
</head>
<BODY onload="resizeCenter(550,400); document.title='PopUp :: Cari SPAJ'; setupPanes('container1', 'tab1'); document.formpost.kata.select(); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Cari SPAJ</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" action="${path }/uw/viewer/agency.htm" style="text-align: center;">
					<input type="hidden" name="win" value="${param.win}">
					<input type="hidden" name="posisi" value="${param.posisi}">
					<table class="entry2">
						<tr>
							<th>Cari:</th>
							<th class="left">
								<select name="tipe">
									<option value="0" <c:if test="${param.tipe eq \"0\" }">selected</c:if>>Kode Agen</option>
									<option value="1" <c:if test="${param.tipe eq \"1\" }">selected</c:if>>Nama Agen </option>
								</select>
								<select name="pilter">
									<option <c:if test="${\"LIKE%\" eq param.pilter}">selected</c:if> value="LIKE%">LIKE%</option>
									<option <c:if test="${\"%LIKE\" eq param.pilter}">selected</c:if> value="%LIKE">%LIKE</option>
									<option <c:if test="${\"%LIKE%\" eq param.pilter}">selected</c:if> value="%LIKE%">%LIKE%</option>
									<option <c:if test="${\"EQ\" eq param.pilter}">selected</c:if> value="EQ">=</option>
								</select>					
								<input type="text" name="kata" size="35" value="${param.kata }" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
								<input type="submit" name="search" value="Search" onclick="return cari();"
									accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
							</th>
						</tr>
					</table>
					<table class="simple">
						<thead>
							<tr>
								<th>Kode Agen</th>
								<th>Nama Agen</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="agen" items="${cmd.listagen}" varStatus="stat">
								<tr  onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
									onclick="backToParent1('${agen.MSAG_ID}');">
									<td >${agen.MSAG_ID}</td>
									<td>${agen.MCL_FIRST}</td>
								</tr>
								<c:set var="jml" value="${stat.count}"/>
								<c:if test="${stat.count eq 1}">
									<c:set var="v1" value="${agen.MSAG_ID}"/>
								</c:if>
							</c:forEach>
						</tbody>
					</table>
					<br>
					<input type="hidden" name="flag" value="${cmd.flag }" >
					<input type="button" name="close" value="Close" onclick="window.close();"
						accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">

				</form>
			</div>
		</div>
	</div>

</form>
<c:if test="${jml eq 1}">
<script>backToParent1('${v1}');</script>
</c:if>
</body>
<%@ include file="/include/page/footer.jsp"%>