<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>
<body onload="setupPanes('container1', 'tab1'); document.title='PopUp :: Alamat Penagihan';" style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Alamat Penagihan</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">

				<form method="post" name="formpost">
				    <table class="entry2">
				      <tr> 
				        <th nowrap="nowrap">Nomor SPAJ:</th>
				        <td nowrap="nowrap" colspan="4">
				        	<input type="text" size="16" class="readOnly" readonly="readonly" name="reg_spaj" value="<elions:spaj nomor="${cmd.address.reg_spaj}"/>">
				        </td>        
				      </tr>
				      <tr> 
				        <th nowrap="nowrap">Region:</th>
				        <td nowrap="nowrap" colspan="4">
							<input type="text" readonly="readonly" class="readOnly" name="region" id="region" value="${cmd.address.region}" size="8">
							<input type="text" size="78" onfocus="this.select();" name="region_name" id="region_name" value="${cmd.address.region_name}"
								onmouseover="return overlib('Masukkan min 5 huruf. Contoh: SEMAR -> Semarang', AUTOSTATUS, WRAP);" onmouseout="nd();">
							<span id="indicator" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
				        </td>        
				      </tr>
				      <tr> 
				        <th nowrap="nowrap">Contact Person :</th>
				        <td nowrap="nowrap" colspan="4">
							<select name="lti_id">
								<c:forEach var="s" items="${cmd.gelar}">
									<option value="${s.key}"
										<c:if test="${s.key eq cmd.address.lti_id}">selected</c:if>>${s.value}</option>
								</c:forEach>
							</select>        
				          <input name="msap_contact" type="text" size="67" value="${cmd.address.msap_contact}"></td>
				      </tr>
				      <tr> 
				        <th nowrap="nowrap">Alamat :</th>
				        <td nowrap="nowrap" colspan="4"><textarea name="msap_address" cols="76" rows="3">${cmd.address.msap_address}</textarea></td>
				      </tr>
				      <tr> 
				        <th nowrap="nowrap">Kota :</th>
				        <td nowrap="nowrap" >
							<input type="text" size="27" onfocus="this.select();" name="kota_tgh" id="kota_tgh" value="${cmd.address.kota_tgh}"
								onmouseover="return overlib('Masukkan min 5 huruf. Contoh: SEMAR -> Semarang', AUTOSTATUS, WRAP);" onmouseout="nd();">
							<span id="indicator_kota" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
				        </td>
				        <th nowrap="nowrap">Kode Pos:</th>
				        <td nowrap="nowrap" colspan="2"><input name="msap_zip_code" type="text" size="27" VALUE="${cmd.address.msap_zip_code}"></td>
				      </tr>
				      <tr> 
				        <th nowrap="nowrap" rowspan="3">Telepon :</th>
				        <td nowrap="nowrap">
				        	<input name="msap_area_code1" type="text" size="4" maxlength="4" value="${cmd.address.msap_area_code1}">
				        	<input type="text" name="msap_phone1" value="${cmd.address.msap_phone1}">
				        </td>
				        <th nowrap="nowrap" rowspan="2">No. Fax : </th>
				        <td nowrap="nowrap">
				        	<input name="msap_area_code_fax1" type="text" size="4" maxlength="4" value="${cmd.address.msap_area_code_fax1}">
				        	<input type="text" name="msap_fax1" value="${cmd.address.msap_fax1}">
				        </td>
				      </tr>
				      <tr> 
				        <td nowrap="nowrap">
				        	<input name="msap_area_code2" type="text" size="4" maxlength="4" value="${cmd.address.msap_area_code2}">
				        	<input type="text" name="msap_phone2" value="${cmd.address.msap_phone2}">
				        </td>
				        <td nowrap="nowrap">
				        	<input name="msap_area_code_fax2" type="text" size="4" maxlength="4" value="${cmd.address.msap_area_code_fax2}">
				        	<input type="text" name="msap_fax2" value="${cmd.address.msap_fax2}">
				        </td>
				      </tr>
				      <tr> 
				        <td nowrap="nowrap">
				        	<input name="msap_area_code3" type="text" size="4" maxlength="4" value="${cmd.address.msap_area_code3}">
				        	<input type="text" name="msap_phone3" value="${cmd.address.msap_phone3}">
				        </td>
				        <th >Autodebet Credit Card :</th>
				        <td nowrap="nowrap"><select name="flag_cc">
				        	<option value="0" <c:if test="${0 eq cmd.address.flag_cc}">selected</c:if>>No</option>
				        	<option value="1" <c:if test="${1 eq cmd.address.flag_cc}">selected</c:if>>Yes</option></select>
				        </td>
				      </tr>
				      <tr> 
				        <th nowrap="nowrap">E-mail :</th>
				        <td nowrap="nowrap"><input type="text" name="e_mail" value="${cmd.address.e_mail}"></td>
				        <th nowrap="nowrap">No. HP 1 :</th>
				        <td nowrap="nowrap"><input type="text" name="no_hp" value="${cmd.address.no_hp}"></td>
				      </tr>
				      <tr> 
				      	<th nowrap="nowrap"></th>
				      	<th nowrap="nowrap"></th>
				        <th nowrap="nowrap">No. HP 2 :</th>
				        <td nowrap="nowrap" colspan="2">
				        	<input type="text" name="no_hp2" value="${cmd.address.no_hp2}">
				        </td>
				      </tr>
				      <tr> 
				        <th nowrap="nowrap">Alasan :</th>
				        <td nowrap="nowrap" colspan="4"><textarea name="msap_address" cols="76" rows="3">${cmd.address.msap_address}</textarea></td>
				      </tr>
				      <c:if test="${not empty cmd.editAlamat}">
					      <tr> 
					      	<th nowrap="nowrap"></th>
					        <td nowrap="nowrap" colspan="3">
							    <input type="submit" name="save" value="Save" onclick="return confirm('Simpan perubahan?');">
							</td>
					      </tr>
				      </c:if>
				    </table>
				</form>
			</div>
		</div>
	</div>

<ajax:autocomplete
	  source="region_name"
	  target="region"
	  baseUrl="${path}/servlet/autocomplete?s=region_name&q=region"
	  className="autocomplete"
	  indicator="indicator"
	  minimumCharacters="5"
	  parser="new ResponseXmlToHtmlListParser()" />

<ajax:autocomplete
	  source="kota_tgh"
	  target="kota_tgh"
	  baseUrl="${path}/servlet/autocomplete?s=kota_tgh&q=wilayah"
	  className="autocomplete"
	  indicator="indicator_kota"
	  minimumCharacters="5"
	  parser="new ResponseXmlToHtmlListParser()" />			  
</body>
<%@ include file="/include/page/footer.jsp"%>