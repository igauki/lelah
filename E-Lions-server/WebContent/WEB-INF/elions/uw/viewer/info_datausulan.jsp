<%@ include file="/include/page/taglibs.jsp"%>

<fieldset>
	<legend>Produk Utama</legend>
	<table class="entry2">
	
		<tr>
			<th nowrap="nowrap">
				Produk
			</th>
			<th nowrap="nowrap" class="left">
				<select disabled>
					<c:forEach var="lsdbs" items="${listlsdbs}">
						<option 
						<c:if test="${cmd.dataUsulan.lsdbs_number eq lsdbs.lsdbs_number}">selected</c:if> >[${lsdbs.lsbs_id}] ${lsdbs.lsdbs_name}</option> 
					</c:forEach> 
				</select>
				Jenis Produk : 
				<select disabled>
					<c:forEach var="tipe" items="${listtipeproduk}"> <option 
					<c:if test="${cmd.dataUsulan.tipeproduk eq tipe.lstp_id}">selected</c:if> >${tipe.lstp_produk}</option> 
					</c:forEach> 
				</select>
				Class
				<input type="text" value="${cmd.dataUsulan.mspr_class}" size="3" readonly>
			</th>
		</tr>
		<tr>
			<th nowrap="nowrap">
				Paket
			</th>
			<th nowrap="nowrap" class="left">
				<input type="text" value="${cmd.dataUsulan.nama_paket}" size="15" readonly>
				Campaign
				<input type="text" value="<c:if test="${not empty cmd.dataUsulan.campaign_id}">${cmd.dataUsulan.campaign_name}</c:if><c:if test="${empty cmd.dataUsulan.campaign_id}">None</c:if>" readonly>
			<fmt:parseNumber var="cf" value="${cmd.dataUsulan.covid_flag}" integerOnly="true"/>
		
				SPAJ Non Face to Face
				<input type="text" value="<c:if test="${cf eq 0 }">DEFAULT</c:if><c:if test="${cf eq 1 }">${cmd.dataUsulan.covid_name}</c:if>" readonly>
			
			</th>
		</tr>
		<c:if test="${cmd.dataUsulan.flag_bulanan ne null}">
			<tr>
				<th nowrap="nowrap">
					Manfaat
				</th>
				<th nowrap="nowrap" class="left">
					<c:if test="${ cmd.dataUsulan.flag_bulanan eq 0}"> Bukan Manfaat bulanan </c:if>
					<c:if test="${ cmd.dataUsulan.flag_bulanan eq 1}">Manfaat bulanan </c:if>
					<span class="info">* Khusus Stable Link/Stable Save</span>
				</th>
			</tr>
		</c:if>
		<tr>
			<th nowrap="nowrap">
				Masa Berlaku Polis
			</th>
			<th nowrap="nowrap" class="left">
				<input type="text" value="<fmt:formatDate value="${cmd.dataUsulan.mste_beg_date}" pattern="dd/MM/yyyy"/>" size="12" readonly>
				s/d			
				<input type="text" value="<fmt:formatDate value="${cmd.dataUsulan.mste_end_date}" pattern="dd/MM/yyyy"/>" size="12" readonly>
				Lama Tanggung
				<input type="text" value="${cmd.dataUsulan.mspr_ins_period}" size="3" readonly>
				Cuti Premi Setelah Tahun ke
				<input type="text" value="${cmd.dataUsulan.mspo_installment}" size="3" readonly>
			</th>
		</tr>
		
		<tr>
			<th nowrap="nowrap">
				Uang Pertanggungan
			</th>
			<th nowrap="nowrap" class="left">
				<input style="text-align: right;" type="text" value="<fmt:formatNumber value="${cmd.dataUsulan.mspr_tsi}" type="currency" currencySymbol="${cmd.dataUsulan.lku_symbol} " maxFractionDigits="2" minFractionDigits="0" />" size="30" readonly>
				Premi
				<input style="text-align: right;" type="text" value="<fmt:formatNumber value="${cmd.dataUsulan.mspr_premium}" type="currency" currencySymbol="${cmd.dataUsulan.lku_symbol} " maxFractionDigits="2" minFractionDigits="0" />" size="30" readonly>
			</th>
		</tr>
		
		<tr>
			<th nowrap="nowrap">
				Lama Bayar
			</th>
			<th nowrap="nowrap" class="left">
				<input type="text" value="${cmd.dataUsulan.mspo_pay_period}" size="3" readonly>
				Cara Bayar
				<input type="text" value="${cmd.dataUsulan.lscb_pay_mode}" size="15" readonly>
				Tanggal Debet
				<input type="text" value="<fmt:formatDate value="${cmd.pemegang.mste_tgl_recur}" pattern="dd/MM/yyyy"/>" size="12" readonly>
			</th>
		</tr>
		<tr>
			<th nowrap="nowrap">
				Autodebet
			</th>
			<th nowrap="nowrap" class="left">
				<c:choose>
					<c:when test="${cmd.dataUsulan.mste_flag_cc eq 0}">None</c:when>
					<c:when test="${cmd.dataUsulan.mste_flag_cc eq 1}">Credit Card</c:when>
					<c:when test="${cmd.dataUsulan.mste_flag_cc eq 2}">Tabungan</c:when>
					<c:when test="${cmd.dataUsulan.mste_flag_cc eq 3}">Payroll</c:when>
					<c:when test="${cmd.dataUsulan.mste_flag_cc eq 8}">Penagihan Ke Sekuritas</c:when>
				</c:choose>
			</th>
		</tr>
		<tr>
			<th nowrap="nowrap">Flag Produk Karyawan</th>
			<th nowrap="nowrap" align="left"> 
				<c:choose>
					<c:when test="${cmd.dataUsulan.mste_flag_el eq 0}">NON KARYAWAN</c:when>
					<c:when test="${cmd.dataUsulan.mste_flag_el eq 1}">KARYAWAN</c:when>
					<c:when test="${cmd.dataUsulan.mste_flag_el eq 2}">KELUARGA KARYAWAN</c:when>
					<c:otherwise>KERABAT KARYAWAN</c:otherwise>
				</c:choose>
			</th>
		</tr>
		<tr>
			<th nowrap="nowrap">Provider</th>
			<th nowrap="nowrap" align="left"><input class="noBorder" type="checkbox" disabled <c:if test="${cmd.dataUsulan.mspo_provider eq 2}" >checked</c:if>></th>
		</tr>
	</table>
</fieldset>

<c:if test="${not empty cmd.dataUsulan.daftaRider}">
	<fieldset>
		<legend>Rider</legend>
		<display:table id="produk" name="cmd.dataUsulan.daftaRider" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator">
			<display:column property="lsdbs_name" title="Produk" style="text-align: left;" />
			<display:column property="mspr_unit" title="Unit" />
			<display:column property="mspr_class" title="Class" />
			<display:column property="mspr_tsi" title="Uang Pertanggungan" format="{0, number, #,##0.00;(#,##0.00)}" />
			<display:column property="mspr_premium" title="Premi" format="{0, number, #,##0.00;(#,##0.00)}" total="true" />
			<display:column property="mspr_ins_period" title="Lama Tanggung" />
			<display:column property="mspr_beg_date" title="Awal Pertanggungan" format="{0, date, dd/MM/yyyy}" />
			<display:column property="mspr_end_date" title="Akhir Pertanggungan" format="{0, date, dd/MM/yyyy}" />
			<display:column property="mspr_end_pay" title="Akhir Bayar" format="{0, date, dd/MM/yyyy}" />
			<display:column property="mspr_extra" title="EM" format="{0, number, #,##0.00;(#,##0.00)}" />
			<display:column property="mspr_rate" title="Rate" format="{0, number, #,##0.00;(#,##0.00)}" />
		</display:table>
	</fieldset>
</c:if>

<c:if test="${not empty cmd.dataUsulan.daftaExtra}">
	<fieldset>
		<legend>Extra Premi</legend>
		<table class="entry2">
			<tr align="left">
				<th nowrap="nowrap">Jenis Extra</th>
				<th nowrap="nowrap">Up</th>
				<th nowrap="nowrap">Keterangan</th>
			</tr>
			<c:forEach var="extra" items="${cmd.dataUsulan.daftaExtra}" varStatus="exta">
			<tr align="left">
			<th><input type="text" value="${extra.LSDBS_NAME}"  size="70" readonly></th>
			<th><input type="text" value="${extra.UP}"  readonly></th>
			<th><input type="text" value="${extra.KETERANGAN}" size="120"  readonly></th>
			</tr>
			</c:forEach>
		</table>
	</fieldset>
</c:if>

<c:if test="${not empty cmd.dataUsulan.daftarplus}">
	<fieldset>
		<legend>Peserta Tambahan</legend>
		<display:table id="plus" name="cmd.dataUsulan.daftarplus" class="displaytag">
		<display:column property="no_urut" title="Nomor" />
		<display:column property="nama" title="Nama" />
		<display:column property="tgl_lahir" title="Tanggal Lahir" format="{0, date, dd/MM/yyyy}"/>
		<display:column property="umur" title="Usia" />
		<display:column property="sex" title="Jenis Kelamin" />
		<display:column property="lsre_relation" title="Hubungan dengan Pemegang Polis" />
		<display:column property="jenis_peserta" title="Jenis Rider" />
		</display:table>
	</fieldset>
</c:if>

<c:if test="${not empty cmd.dataUsulan.daftabenef}">
	<fieldset>
		<legend>Penerima Manfaat</legend>
		<display:table id="benef" name="cmd.dataUsulan.daftabenef" class="displaytag">
			<display:column property="msaw_number" title="Nomor" />
			<display:column property="msaw_first" title="Nama" style="text-align: left;" />
			<display:column property="msaw_birth" title="Tanggal Lahir" format="{0, date, dd/MM/yyyy}" />
			<display:column property="lsre_relation" title="Hubungan dengan Calon Tertanggung" />
			<display:column property="msaw_persen" title="%" />
			<display:column property="msaw_sex_detail" title="Jenis Kelamin" />
			<display:column property="msaw_ket" title="Instruksi Khusus" />
		</display:table>
	</fieldset>
</c:if>
<c:if test="${cmd.dataUsulan.worksite ne null}">
	<fieldset>
		<legend>Informasi Perusahaan Worksite</legend>
			<table class="entry2">
				<tr>
					<th>Nama Perusahaan :${cmd.dataUsulan.worksite.MCL_GELAR} ${cmd.dataUsulan.worksite.MCL_FIRST }</th>
				</tr>
				<tr>
					<th>NIK : ${cmd.dataUsulan.worksite.NIK }</th>
				</tr>
			</table>
	</fieldset>
</c:if>

<c:if test="${cmd.dataUsulan.psn ne null}">
	<fieldset>
		<legend>Informasi Program Sosial Nasabah</legend>
		<table class="displaytag" style="width: 30%; float:left;">
			<tr style="background-color: rgb(220, 220, 220)">
				<th>Jenis PSN</th>
				<th>Besaran</th>
			</tr>
			<tr class="odd">
				<td style="text-align: right;">${cmd.dataUsulan.psn.JENIS_PSN }</td>
				<td style="text-align: right;"><fmt:formatNumber value="${cmd.dataUsulan.psn.BESARAN}" type="currency" currencySymbol="${cmd.dataUsulan.lku_symbol} " maxFractionDigits="2" minFractionDigits="0" /></td>
			</tr>
		</table> 
	</fieldset>
</c:if>