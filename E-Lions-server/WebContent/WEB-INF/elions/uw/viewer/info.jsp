<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<script>

			var today = new Date();

			function info_awal(){
				var info="${cmd.info}";
				if(info=='1')
					setupPanes("container1", "tab8");
				else
					setupPanes("container1", "tab1");
				var suc="${cmd.suc}";
				
				if(suc=='1'){
					alert("Berhasil di simpan ${param.showSPAJ}");
					window.location='${path }/uw/view.htm?showSPAJ=${param.showSPAJ}';
				}
				
				document.body.style.visibility='visible';

				<c:if test="${not empty daftarPesan}">
					<c:forEach items="${daftarPesan}" var="d">
						alert('${d}');
					</c:forEach>
				</c:if>
				
				<c:if test="${not empty transferInbox}">
					<c:forEach items="${transferInbox}" var="e">
						if(confirm('${e}')){
							//window.location='${path }/uw/view.htm?showSPAJ=${param.showSPAJ}';
							transferInbox('${param.showSPAJ}','${sessionScope.currentUser.lus_id}');
						}else{
						
						}
					</c:forEach>
				</c:if>
			}
			
		</script>
	</head>
	<body style="height: 100%;text-align: center; visibility: hidden; ">
	<c:choose>
		<c:when test="${empty param.showSPAJ }">
			<div id="success">Silahkan gunakan menu diatas untuk memilih detail SPAJ</div>
		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when test="${cmd.proses eq 'mri'}">
					<div id="success">Polis MRI. Tidak dapat melihat data</div>
				</c:when>
				<c:otherwise>
					<c:if test="${cmd.docPos.LSPD_ID eq 2 }">
						<div id="success">
							Proses Selanjutnya adalah ${cmd.proses}
						</div>
					</c:if>
			
					<fieldset>
						<table class="entry2">
							<tr>
								<th>No. Polis</th>
								<th><input readonly type=text size=20 value="<elions:polis nomor="${cmd.policyNo}"/>"></th>
								<th>No. SPAJ</th>
								<th><input readonly type=text size=15 value="<elions:spaj nomor="${param.showSPAJ}"/>"></th>
								<c:if test="${reins ne null }">
									<th>No. Reinstate</th>
									<th>
										<input type="text" value=" <elions:spaj nomor="${reins}"/>" size="15" readonly>
									</th>
								</c:if>
								<th>Pemegang</th>
								<th><input type="text" value="${cmd.pemegang.mcl_first}" size="35" readonly>
								<img style="border:none;" alt="Refresh" src="${path}/include/image/action_refresh.gif" onclick="history.go(); return false;">
								</th>
								<c:if test="${infoNasabah ne null }">
									<th>Jenis Nasabah</th>
									<th>
										<input type="text" value="${infoNasabah}" size="15" readonly>
									</th>
								</c:if>
							</tr>
						</table>
						<table class="entry2">
							<tr>
								<th>No. Gadget/E-SPAJ</th>
								<th><input readonly type=text size=20 value="${gadget.NO_TEMP}"></th>
							
								<th>Polis Lama (SURRENDER ENDORSEMENT)</th>
								<th><input readonly type=text size=20 value="${polisLama.MSPO_POLICY_NO_FORMAT}"></th>
								
								<th>Pega Case ID</th>
								<th><input readonly type=text size=20 value="${cmd.pemegang.pega_case_id}"></th>
							</tr>
						</table>
					</fieldset>
					<div style="text-align: left;font-weight: bold;">
					* WARNA <font color="blue">BIRU</font> : ATTENTION LIST <br/>
					* WARNA <font color="RED">MERAH</font> : RED FLAG ( HIGH RISK PROFILE )
					</div>
					<%-- <div id="success">Processing Time : ${selisih}</div> --%>
			
				<div class="tab-container" id="container1" style="width: 115%">
						<ul class="tabs" style="width: 100%">
							<c:forEach items="${lsPanes}" var="x" varStatus="xt">
								<li>
									<c:choose>
										<c:when test="${(blacklistFont eq 'blue' && ppf eq x.NAMES ) || (blacklistFont eq 'orange' && terf eq x.NAMES )}">
											<a href="#" onClick="return showPane('pane${xt.count}', this)" 
											onmouseover="return overlib('Alt-${xt.count}', AUTOSTATUS, WRAP);" onmouseout="nd();"
											onfocus="return showPane('pane${xt.count}', this)" accesskey="${xt.count}" id="tab${xt.count }"><font color="${blacklistFont}">${x.NAMES}</font></a>
										</c:when>
										<c:when test="${(blacklistFont eq 'red' && ppf eq x.NAMES)|| (blacklistFont eq 'red' && terf eq x.NAMES)}">
											<a href="#" onClick="return showPane('pane${xt.count}', this)" 
											onmouseover="return overlib('Alt-${xt.count}', AUTOSTATUS, WRAP);" onmouseout="nd();"
											onfocus="return showPane('pane${xt.count}', this)" accesskey="${xt.count}" id="tab${xt.count }"><font color="${blacklistFont}">${x.NAMES}</font></a>
										</c:when>
										<c:otherwise>
											<a href="#" onClick="return showPane('pane${xt.count}', this)" 
											onmouseover="return overlib('Alt-${xt.count}', AUTOSTATUS, WRAP);" onmouseout="nd();"
											onfocus="return showPane('pane${xt.count}', this)" accesskey="${xt.count}" id="tab${xt.count }">${x.NAMES}</a>
										</c:otherwise>
									</c:choose>
								</li>
							</c:forEach>	
						</ul>
						<div class="tab-panes">
							<c:forEach items="${lsPanes}" var="x" varStatus="xt" >
								<div id="pane${xt.count}" class="panes">
									<c:choose>
										<c:when test="${xt.count==9}">
										<iframe src="${path}/uw/viewer/ulangan.htm?spaj=${param.showSPAJ }" name="ulanganFrame" id="ulanganFrame"
												width="100%"  height="100%"> Please Wait... </iframe>
										</c:when>
										<c:otherwise>
											<jsp:include page="${x.PAGES}" flush="true" />
										</c:otherwise>
									</c:choose>
								</div>
							</c:forEach>	
						</div>
					</div>				
				</c:otherwise>
			</c:choose>
		</c:otherwise>
	</c:choose>
		<div>
			<script>
				window.onload = function() { 
					setTimeout("info_awal()", 1);	
				};
	
				var today2 = new Date();
				window.status = ((today2.valueOf() - today.valueOf()) / 1000);
				
			</script>
		</div>
	</body>
	<%@ include file="/include/page/footer.jsp"%>