<%@ include file="/include/page/taglibs.jsp"%>
<table class="entry2">

	<tr>
		<th nowrap="nowrap">
			Nama Lengkap
		</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select name="tertanggung.lti_id" disabled="disabled">
				<option value=""></option>
				<c:forEach var="l" items="${select_gelar}">
					<option
						<c:if test="${cmd.tertanggung.lti_id eq l.ID}"> SELECTED </c:if>
						value="${l.ID}">${l.GELAR}</option>
				</c:forEach>
			</select>				
			<input type="text" value="${cmd.tertanggung.mcl_first}" size="35" readonly>
			Gelar
			<input type="text" value="${cmd.tertanggung.mcl_gelar}" size="12" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">No. Telepon Rumah</th>
		<th nowrap="nowrap" class="left">
			<select style="width: 191px;">
				<OPTGROUP label="Telepon Rumah">
					<c:if test="${not empty cmd.tertanggung.telpon_rumah}"><option>${cmd.tertanggung.area_code_rumah} - ${cmd.tertanggung.telpon_rumah}</option></c:if>
					<c:if test="${not empty cmd.tertanggung.telpon_rumah2}"><option>${cmd.tertanggung.area_code_rumah2} - ${cmd.tertanggung.telpon_rumah2}</option></c:if>
				</OPTGROUP>
				<OPTGROUP label="Handphone">
					<c:if test="${not empty cmd.tertanggung.no_hp}"><option>${cmd.tertanggung.no_hp}</option></c:if>
					<c:if test="${not empty cmd.tertanggung.no_hp2}"><option>${cmd.tertanggung.no_hp2}</option></c:if>										
				</OPTGROUP>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">No. Telepon Kantor</th>
		<th nowrap="nowrap" class="left">
			<select style="width: 191px;">
				<OPTGROUP label="Telepon Kantor">
					<c:if test="${not empty cmd.tertanggung.telpon_kantor}"><option>${cmd.tertanggung.area_code_kantor} - ${cmd.tertanggung.telpon_kantor}</option></c:if>
					<c:if test="${not empty cmd.tertanggung.telpon_kantor2}"><option>${cmd.tertanggung.area_code_kantor2} - ${cmd.tertanggung.telpon_kantor2}</option></c:if>
				</OPTGROUP>
				<OPTGROUP label="Fax">
					<c:if test="${not empty cmd.tertanggung.no_fax}"><option>${cmd.tertanggung.area_code_fax} - ${cmd.tertanggung.no_fax}</option></c:if>
				</OPTGROUP>
			</select>
		</th>
	</tr>
	

</table>