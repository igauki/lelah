<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<META HTTP-EQUIV="X-UA-COMPATIBLE" CONTENT="IE=IE9">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">

<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/include/js/csspopup.js"></script><!-- CSS POP UP -->
<script type="text/javascript">
    var contextPath = "${path}";
    hideLoadingMessage();
    
    function formPosisiSubmit() {
        var checked = false,
            select = document.formPosisi.posisi;
        for(var i = 0; i < select.length;i++) {
            if(select[i].checked) {
                checked = true;
                break;
            }
        }
        
        if(!checked) {
            alert('Posisi belum dipilih!');
            return false;
        }
    }
</script>
</head>

<body onload="resizeCenter(450, 250); document.title='SK Debet/Kredit'; setupPanes('container1', 'tab1')">
    <c:if test="${not empty msg}">
        <script>
            alert('${msg}');
            window.close();
        </script>
    </c:if>
    <div class="tab-container" id="container1">
        <ul class="tabs">
            <li>
                <a href="#" onClick="return showPane('pane1', this)" id="tab1">SK Debet/Kredit</a>
            </li>
        </ul>
        
        <div class="tab-panes">
            <div id="pane1" class="panes">
                <form name="formPosisi" method="post" action="${path}/uw/viewer.htm?window=posisi_berkas&spaj=${spaj}" onsubmit="return formPosisiSubmit();">
                    <table class="entry" style="width: 100%;">
                        <tr>
                            <td rowspan="2">
                                SK Debet/Kredit sudah diterima di bagian ?
                            </td>
                            <td>
                                <input type="radio" name="posisi" value="0"> Underwriting
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="radio" name="posisi" value="1"> Finance
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                Keterangan :
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input style="width: 100%; height: 25px;" type="text" name="keterangan" value="">
                            </td>
                        </tr>
                        <c:if test="${not empty errMsg}">
                            <tr>
                               <td colspan="2">
                                   <div id="error">
                                       Error:<br>
                                       ${errMsg}
                                   </div>
                               </td>
                            </tr>
                        </c:if>
                        <tr>
                            <td colspan="2" style="text-align: center;">
                                <input type="submit" name="save" value="Save"> 
                                <input type="button" value="Cancel" onClick="window.close();">
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</body>
<%@ include file="/include/page/footer.jsp"%>