<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">

    $().ready(function() {
		
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});
		
		// Search sms keluar
		$('#search5').change(function() {
		    // Disable & hide semua search field terlebih dahulu
	        $('#tgl_kirim').hide();
            $('#tgl_kirim input[type=text]').attr('disabled', true);
            $('#spaj5').hide();
            $('#spaj5 input[name=reg_spaj5]').attr('disabled', true);
            $('#nopol5').hide();
            $('#nopol5 input[name=no_polis5]').attr('disabled', true);
            $('#nohp5').hide();
            $('#nohp5 input[name=no_hp5]').attr('disabled', true);
            $('#status5').hide();
            $('#status5 select[name=status5]').attr('disabled', true);
            $('#status5 input[type=text]').attr('disabled', true);
            $('#tgl_create').hide();
            $('#tgl_create input[type=text]').attr('disabled', true);
            
            // Baru enable & show berdasarkan tipe search yg dipilih
		    var search = $(this).val();
		    if (search == 1) {
		        $('#tgl_kirim').show();
		        $('#tgl_kirim input[type=text]').attr('disabled', false);
		    } else if (search == 2) {
		        $('#spaj5').show();
		        $('#spaj5 input[name=reg_spaj5]').attr('disabled', false);
		    } else if (search == 3) {
	            $('#nopol5').show();
	            $('#nopol5 input[name=no_polis5]').attr('disabled', false);
		    } else if (search == 4) {
	            $('#nohp5').show();
	            $('#nohp5 input[name=no_hp5]').attr('disabled', false);
		    } else if (search == 5) {
	            $('#status5').show();
	            $('#status5 select[name=status5]').attr('disabled', false);
                $('#status5 input[type=text]').attr('disabled', false);
		    } else if (search == 6) {
                $('#tgl_create').show();
                $('#tgl_create input[type=text]').attr('disabled', false);
            }
		});
		
		$('.viewReportBtn').click(function() {
		    var month = $('#month6').val();
		    var year = $('#year6').val();
		    var param = 'showPDF';
		    var type = 'uw.pdf';
		    
		    if(this.id == 'showTotalSMSOutXLS') {
		        param = 'showXLS';
		        type = 'uw.xls';
		    }
		    
		    $('#loading_bar6').css('visibility', 'visible');
		    $('#iframeTotalSMSOut').attr('src', '${path}/report/' + type + '?window=report_total_sms_out_bulanan&month=' + month + '&year=' + year + '&' + param + '=true');
		    
		    if(param === 'showXLS') setTimeout(hideLoadingBar6, 3000);
		});
		
		$('#iframeTotalSMSOut').load(function() {
		    $('#loading_bar6').css('visibility', 'hidden');
		});
	});
	
	function hideLoadingBar6() {
	    $('#loading_bar6').css('visibility', 'hidden');
	}
</script>

<body  style="height: 100%;" onload="setupPanes('container1', 'tab${cmd.pane}');">
<script type="text/javascript">
   

	function reFresh() {
	  location.reload(true)
	}
	/* Set the number below to the amount of delay, in milliseconds,
	you want between page reloads: 1 minute = 60000 milliseconds. */
	window.setInterval("reFresh()",60000*5);
	
	function referesh(){	
		document.getElementById('refresh').click();
		document.getElementById('loading_bar').style.visibility = "visible";	
	}
	function referesh2(){	
		document.getElementById('refresh2').click();
		document.getElementById('loading_bar2').style.visibility = "visible";		
	}
	function referesh3(){	
		document.getElementById('refresh3').click();
		document.getElementById('loading_bar3').style.visibility = "visible";
	}
    function referesh5(){   
        document.getElementById('refresh5').click();
        document.getElementById('loading_bar5').style.visibility = "visible";
    }
</script>



<c:choose>
	<c:when test="${not empty cmd.notCS}">
		<div id="error">
			Maaf, Anda tidak memiliki akses untuk menu ini
		</div>
	</c:when>
	<c:otherwise>	
		<div class="tab-container" id="container1">
		
				<ul class="tabs">
					<li>
						<a href="#" onClick="return showPane('pane1', this)"id="tab1">Daftar SMS Masuk</a>
						<a href="#" onClick="return showPane('pane2', this)" id="tab2">Follow Up By User</a>					
						<c:if test="${sessionScope.currentUser.lus_id eq \"558\"or sessionScope.currentUser.lus_id eq \"1651\"or sessionScope.currentUser.lus_id eq \"1653\"or sessionScope.currentUser.lus_id eq \"1493\" or sessionScope.currentUser.lus_id eq \"1436\" or sessionScope.currentUser.lus_id eq \"91\"}">
							<a href="#" onClick="return showPane('pane3', this)" id="tab3">Follow Up ALL</a>		
						</c:if>
						<a href="#" onClick="return showPane('pane4', this)" id="tab4">Report</a>
						<a href="#" onClick="return showPane('pane5', this)" id="tab5">Daftar SMS Keluar</a>
                        <a href="#" onClick="return showPane('pane6', this)" id="tab5">Report Total SMS Keluar</a>
					</li>
				</ul>
		
				<div class="tab-panes">
					
					<div id="pane1" class="panes">
						<form method="post" name="formpost" action="#" style="text-align: center;">	
							<fieldset>
							<legend>
								Daftar SMS Masuk
							</legend>							
							<div id="loading_bar" style="position:absolute; right:40; width:135;visibility:visible; background-color: #F7F7F7;">
			          		<span style="float:right; vertical-align: top; background-color:gray; color:white; font-weight:bold; font-size:12; text-align:center; cursor:pointer" onclick="Loading()">&nbsp;&nbsp;&nbsp;&nbsp;Loading ........!!&nbsp;&nbsp;</span>
			        		</div>			        		
							 <table  class="entry" style="width:100%" align="center" cellpadding="2"  >       				
			       			 <tr>
			           		 <td style="width:65%" class="">Page ${currPage} of ${lastPage} </td>
			           		 <td   align="right" class="">Go
			                <input name="cPage" id="cPage" type="text" style="width:45px" maxlength="4" title="Input Nomor Halaman yg akan dikunjungi" />
			                <input type="button" value="Go" id="go" style="width: 40px;"  onclick="referesh()"/>
			                <input type="submit" name="refresh" value="refresh" id="refresh" style="visibility: hidden;" /> 
			              	 <label></label>
			                &nbsp;
			                <c:choose>
			        			<c:when test="${currPage eq '1'}">
			        			<font color="gray">First | Prev |</font> 
			        			</c:when>
			        			<c:otherwise>
			               			<a href="#" class="" onclick="document.getElementById('cPage').value='${firstPage}'; referesh()">First</a> | 
			                		<a href="#" class="" onclick="document.getElementById('cPage').value='${previousPage}'; referesh()">Prev</a> | 
			                	</c:otherwise>
			                </c:choose>			                
			                <c:choose>
			        			<c:when test="${currPage eq lastPage}">
			        			<font color="gray">Next | Last </font>
			        			</c:when>
			        			<c:otherwise>
			               			<a href="#" class="" onclick="document.getElementById('cPage').value='${nextPage}'; referesh()">Next</a> | 
			                		 <a href="#" class="" onclick="document.getElementById('cPage').value='${lastPage}'; referesh()">Last</a> 
			                	</c:otherwise>
			                </c:choose>                
               				 </td>
      				 		 </tr>
    						</table>
							 <table  class="displaytag" style="width:100%" align="center" cellpadding="2"  > 
								<thead>
									<tr>
									Daftar SMS Masuk yang harus di Follow-Up
									</tr>
									<tr>
									<th style="text-align: left" style="width:20px">ID</th>
									<th style="text-align: left" style="width:80px">Pengirim</th>
									<th style="text-align: left" style="width:100px">Tanggal Terima</th>
									<th style="text-align: left" style="width:150px">PESAN<!--  Tertanggung --></th>
									<th style="text-align: left" style="width:90px">--</th>									
								</tr>
								</thead>
							<tbody>
							<c:forEach var="pas" items="${daftar}" varStatus="stat">
								<tr>
										<td class="left">${pas.id}</td>
										<td class="left">${pas.originator}</td>								
										<td class="left"><fmt:formatDate value="${pas.receive_date}" pattern="dd/MM/yyyy hh:mm:ss" /></td>										
										<td class="left">${pas.text}</td>
										<td class="left">
											<input type="button" value="follow up" name="followup" onclick="popWin('${path}/uw/viewer.htm?window=cssms_view&id=${pas.id}', 500, 1078);"/>										
										</td>
								</tr>
							</c:forEach>
						
							</tbody>
							</table>							
					
								<input type="button" value="Refresh" onclick="reFresh();">
									
							</fieldset>
					</form>	
					</div>
				
					<div id="pane2" class="panes">
						<form method="post" name="formpost" action="#" style="text-align: center;">	
							<fieldset>
							<legend>
								SMS FOLLOW UP BY ${sessionScope.currentUser.name}
							</legend>
							<div id="loading_bar2" style="position:absolute; right:40; width:135;visibility:visible; background-color: #F7F7F7;">
			          			<span style="float:right; vertical-align: top; background-color:gray; color:white; font-weight:bold; font-size:12; text-align:center; cursor:pointer" onclick="Loading()">&nbsp;&nbsp;&nbsp;&nbsp;Loading ........!!&nbsp;&nbsp;</span>
			        		</div>
							 <table  class="entry" style="width:100%" align="center" cellpadding="2"  >       				
			       			 <tr>
			           		 <td style="width:65%" class="">Page ${currPage2} of ${lastPage2} </td>
			           		 <td   align="right" class="">Go
			                <input name="cPage2" id="cPage2" type="text" style="width:45px" maxlength="4" title="Input Nomor Halaman yg akan dikunjungi"/>
			                <input type="button" value="Go" id="go" style="width: 40px;"  onclick="referesh2()"/>
			                <input type="submit" name="refresh2" value="refresh" id="refresh2" style="visibility: hidden;" /> 
			              	 <label></label>
			                &nbsp;
			                <c:choose>
			        			<c:when test="${currPage2 eq '1'}">
			        			<font color="gray">First | Prev |</font> 
			        			</c:when>
			        			<c:otherwise>
			               			<a href="#" class="" onclick="document.getElementById('cPage2').value='${firstPage2}'; referesh2()">First</a> | 
			                		<a href="#" class="" onclick="document.getElementById('cPage2').value='${previousPage2}'; referesh2()">Prev</a> | 
			                	</c:otherwise>
			                </c:choose>			                
			                <c:choose>
			        			<c:when test="${currPage2 eq lastPage2}">
			        			<font color="gray">Next | Last </font>
			        			</c:when>
			        			<c:otherwise>
			               			<a href="#" class="" onclick="document.getElementById('cPage2').value='${nextPage2}'; referesh2()">Next</a> | 
			                		 <a href="#" class="" onclick="document.getElementById('cPage2').value='${lastPage2}'; referesh2()">Last</a> 
			                	</c:otherwise>
			                </c:choose>                
               				 </td>
      				 		 </tr>
    						</table>
							<table  class="displaytag" style="width:100%" align="center" cellpadding="2"  > 
								<thead>
									<tr>
									SMS FOLLOW UP BY ${sessionScope.currentUser.name}
									</tr>
									<tr>
									<th style="text-align: left" style="width:20px">ID</th>
									<th style="text-align: left" style="width:80px">Pengirim</th>
									<th style="text-align: left" style="width:100px">Tanggal Terima</th>
									<th style="text-align: left" style="width:150px">PESAN<!--  Tertanggung --></th>
									<th style="text-align: left" style="width:90px">Follow Up</th>	
									<th style="text-align: left" style="width:90px">User Follow Up</th>	
									<th style="text-align: left" style="width:90px">Tgl Follow Up</th>	
									<th style="text-align: left" style="width:90px">Ket Follow Up</th>		
									<th style="text-align: left" style="width:90px">SMS Balasan</th>	
									<th style="text-align: left" style="width:90px">STATUS SMS</th>	
									<th style="text-align: left" style="width:90px">--</th>						
								</tr>
								</thead>
							<tbody>
							<c:forEach var="pas2" items="${daftarFollowup}" varStatus="stat">
								<tr>
										<td class="left">${pas2.id}</td>
										<td class="left">${pas2.originator}</td>								
										<td class="left"><fmt:formatDate value="${pas2.receive_date}" pattern="dd/MM/yyyy hh:mm:ss" /></td>										
										<td class="left">${pas2.text}</td>
										<td class="left">
											<c:choose>
												<c:when test="${pas2.process eq \"1\" }">
													Follow Up  by Phone
												</c:when>
												<c:when test="${pas2.process eq \"2\" }">
													Follow Up by SMS
												</c:when>
												<c:when test="${pas2.process eq \"3\" }">
													Tidak Perlu DI Follow Up
												</c:when>
												<c:when test="${pas2.process eq \"4\" }">
													Perlu Follow Up Lanjutan
												</c:when>
												<c:otherwise>
													Belum di follow up
												</c:otherwise>
											</c:choose>
										</td>
									<td class="left">${pas2.username}</td>
									<td class="left"><fmt:formatDate value="${pas2.process_date}" pattern="dd/MM/yyyy hh:mm:ss" /></td>
									<td class="left">${pas2.proses_ket}</td>
									<td class="left">${pas2.msg_reply}</td>
									<td class="left">
										<c:choose>
											<c:when test="${pas2.status_delivery eq \"S\"}">
												Sent
											</c:when>
											<c:when test="${pas2.status_delivery eq \"D\"}">
												Delivered
											</c:when>
											<c:when test="${pas2.status_delivery eq \"Q\"}">
												Queued
											</c:when>
											<c:when test="${pas2.status_delivery eq \"A\"}">
												Aborted
											</c:when>
											<c:when test="${pas2.status_delivery eq \"U\"}">
												Unsent
											</c:when>
											<c:when test="${pas2.status_delivery eq \"F\"}">
												Failed
											</c:when>
											<c:when test="${pas2.status_delivery eq \"P\"}">
												Pending
											</c:when>
											<c:otherwise>
												
											</c:otherwise>
										</c:choose>		
									</td>
									<td> <input type="button" value="VIEW" name="followup" onclick="popWin('${path}/uw/viewer.htm?window=cssms_view&id=${pas2.id}', 500, 1078);"/> </td>									
								</tr>
							</c:forEach>
						
							</tbody>
							</table>		
							
							</fieldset>
						</form>					
					</div>
					<div id="pane4" class="panes">
					<fieldset>
							<legend>
								Report SMS
							</legend>
							<form id="formpost" method="post">
							<table class="entry2">
							<tr>
								<th width="10%">Tanggal :</th>
								<td width="50%">
										<input name="bdate" id="bdate" type="text" class="datepicker" title="Tanggal Awal" value="${cmd.bdate}"> s/d 
									    <input name="edate" id="edate" type="text" class="datepicker" title="Tanggal Akhir" value="${cmd.edate}">
								</td>
									<td align="left" rowSpan="3"><input type="submit" name="showPDF" value="Show PDF" >
																<input type="submit" name="showXLS" value="Show Excel">
																<input type="hidden" name="showReport" value="1">
								  </td>
							</tr>
							<tr>
								<th>Jenis Report</th>
								<td>
								 		<input name="jn_report" id="harian" type="radio" class="radio" value="0">Harian   
	   									 <input name="jn_report" id="bulanan" type="radio" class="radio" value="1" checked="checked">Bulanan
	   
								</td>
							
							</tr>
							<tr>
								<th>User Follow Up </th>
								<td>
									<select name="seluserCs" id="seluserCs" title="Silahkan pilih User">
										<option value="ALL" selected="selected">ALL</option>
										<c:forEach  items="${cmd.userCS}"  var="u" varStatus="c">
											<option value="${u.KEY}">${u.VALUE}</option>
										</c:forEach>
								</select>
								</td>
							</tr>							
							
						</table>
					</form>
							
					</fieldset>		
					</div>
					<c:if test="${sessionScope.currentUser.lus_id eq \"558\" or sessionScope.currentUser.lus_id eq \"1436\" or sessionScope.currentUser.lus_id eq \"1651\" or sessionScope.currentUser.lus_id eq \"1653\" or sessionScope.currentUser.lus_id eq \"1493\" or sessionScope.currentUser.lus_id eq \"91\"}">
					<div id="pane3" class="panes">
						<form method="post" name="formpost" action="#" style="text-align: center;">	
							<fieldset>
							<legend>
								SMS FOLLOW UP ALL
							</legend>
							<div id="loading_bar3" style="position:absolute; right:40; width:135;visibility:visible; background-color: #F7F7F7;">
			          			<span style="float:right; vertical-align: top; background-color:gray; color:white; font-weight:bold; font-size:12; text-align:center; cursor:pointer" onclick="Loading()">&nbsp;&nbsp;&nbsp;Loading ........!!&nbsp;&nbsp;</span>
			        		</div>
							<table  class="entry" style="width:100%" align="center" cellpadding="2"  >       				
			       			 <tr>
			           		 <td style="width:65%" class="">Page ${currPage3} of ${lastPage3} </td>
			           		 <td   align="right" class="">Go
			                <input name="cPage3" id="cPage3" type="text" style="width:45px" maxlength="4" title="Input Nomor Halaman yg akan dikunjungi" />
			                <input type="button" value="Go" id="go" style="width: 40px;"  onclick="referesh3()"/>
			                <input type="submit" name="refresh3" value="refresh" id="refresh3" style="visibility: hidden;" /> 
			              	 <label></label>
			                &nbsp;
			                <c:choose>
			        			<c:when test="${currPage3 eq '1'}">
			        			<font color="gray">First | Prev |</font> 
			        			</c:when>
			        			<c:otherwise>
			               			<a href="#" class="" onclick="document.getElementById('cPage3').value='${firstPage3}'; referesh3()">First</a> | 
			                		<a href="#" class="" onclick="document.getElementById('cPage3').value='${previousPage3}'; referesh3()">Prev</a> | 
			                	</c:otherwise>
			                </c:choose>			                
			                <c:choose>
			        			<c:when test="${currPage3 eq lastPage3}">
			        			<font color="gray">Next | Last </font>
			        			</c:when>
			        			<c:otherwise>
			               			<a href="#" class="" onclick="document.getElementById('cPage3').value='${nextPage3}'; referesh3()">Next</a> | 
			                		 <a href="#" class="" onclick="document.getElementById('cPage3').value='${lastPage3}'; referesh3()">Last</a> 
			                	</c:otherwise>
			                </c:choose>                
               				 </td>
      				 		 </tr>
    						</table>
							
							<table  class="displaytag" style="width:100%" align="center" cellpadding="2"  > 
								<thead>
									<tr>
									SMS FOLLOW UP ALL
									</tr>
									<tr>
									<th style="text-align: left" style="width:20px">ID</th>
									<th style="text-align: left" style="width:80px">Pengirim</th>
									<th style="text-align: left" style="width:100px">Tanggal Terima</th>
									<th style="text-align: left" style="width:150px">PESAN<!--  Tertanggung --></th>
									<th style="text-align: left" style="width:90px">Follow Up</th>	
									<th style="text-align: left" style="width:90px">User Follow Up</th>	
									<th style="text-align: left" style="width:90px">Tgl Follow Up</th>	
									<th style="text-align: left" style="width:90px">Ket Follow Up</th>		
									<th style="text-align: left" style="width:90px">SMS Balasan</th>	
									<th style="text-align: left" style="width:90px">STATUS SMS</th>	
									<th style="text-align: left" style="width:90px">--</th>						
								</tr>
								</thead>
							<tbody>
							<c:forEach var="pas3" items="${daftarFollowupSPV}" varStatus="stat">
								<tr>
										<td class="left">${pas3.id}</td>
										<td class="left">${pas3.originator}</td>								
										<td class="left"><fmt:formatDate value="${pas3.receive_date}" pattern="dd/MM/yyyy hh:mm:ss" /></td>										
										<td class="left">${pas3.text}</td>
										<td class="left">
											<c:choose>
												<c:when test="${pas3.process eq \"1\" }">
													Follow Up  by Phone
												</c:when>
												<c:when test="${pas3.process eq \"2\" }">
													Follow Up by SMS
												</c:when>
												<c:when test="${pas3.process eq \"3\" }">
													Tidak Perlu DI Follow Up
												</c:when>
												<c:when test="${pas3.process eq \"4\" }">
													Perlu Follow Up Lanjutan
												</c:when>
												<c:otherwise>
													Belum di follow up
												</c:otherwise>
											</c:choose>
										</td>
									<td class="left">${pas3.username}</td>
									<td class="left"><fmt:formatDate value="${pas3.process_date}" pattern="dd/MM/yyyy hh:mm:ss" /></td>
									<td class="left">${pas3.proses_ket}</td>
									<td class="left">${pas3.msg_reply}</td>
									<td class="left">
										<c:choose>
											<c:when test="${pas3.status_delivery eq \"S\"}">
												Sent
											</c:when>
											<c:when test="${pas3.status_delivery eq \"D\"}">
												Delivered
											</c:when>
											<c:when test="${pas3.status_delivery eq \"Q\"}">
												Queued
											</c:when>
											<c:when test="${pas3.status_delivery eq \"A\"}">
												Aborted
											</c:when>
											<c:when test="${pas3.status_delivery eq \"U\"}">
												Unsent
											</c:when>
											<c:when test="${pas3.status_delivery eq \"F\"}">
												Failed
											</c:when>
											<c:when test="${pas3.status_delivery eq \"P\"}">
												Pending
											</c:when>
											<c:otherwise>
												
											</c:otherwise>
										</c:choose>		
									</td>
									<td> <input type="button" value="VIEW" name="followup" onclick="popWin('${path}/uw/viewer.htm?window=cssms_view&id=${pas3.id}', 500, 1078);"/></td>									
								</tr>
							</c:forEach>						
							</tbody>
							</table>					
							</fieldset>
					  </form>
					</div>
					</c:if>				
					<div id="pane5" class="panes">
					    <form method="post" name="formpost" action="#" style="text-align: center;">
						    <fieldset>
						        <legend>Daftar SMS Keluar</legend>
						        <div id="loading_bar5" style="position:absolute; right:40; width:135;visibility:visible; background-color: #F7F7F7;">
	                                <span style="float:right; vertical-align: top; background-color:gray; color:white; font-weight:bold; font-size:12; text-align:center; cursor:pointer" onclick="Loading()">&nbsp;&nbsp;&nbsp;Loading ........!!&nbsp;&nbsp;</span>
	                            </div>
	                            <table  class="entry" style="width:100%" align="center" cellpadding="2">
	                                <tr>
	                                   <td colspan="3">
	                                       <div class="info">
	                                           Keterangan Status :<br/>
	                                           - Unsent : SMS belum dikirim ke JATIS<br/>
	                                           - Queued : SMS sudah dikirim ke JATIS dan masih menunggu request delivery status dari JATIS<br/>
	                                           - Sent : SMS sudah dikirim oleh JATIS ke penerima<br/>
	                                           - Delivered : SMS sudah diterima oleh penerima<br/>
	                                           - Aborted : SMS dibatalkan<br/>
	                                           - Failed : SMS gagal dikirim oleh JATIS<br/>
	                                           - Pending : SMS dalam status pending
	                                       </div>
	                                   </td>
	                                </tr>
	                                <tr>
	                                    <td style="width:25%" class="">Page ${currPage5} of ${lastPage5}</td>
	                                    <td style="width:48%">
	                                        Search :<br>
	                                        <select id="search5" name="search5">
	                                           <option value="1"<c:if test="${search5 eq 1}"> selected</c:if>>Tgl Kirim</option>
                                               <option value="6"<c:if test="${search5 eq 6}"> selected</c:if>>Tgl Create</option>
                                               <option value="2"<c:if test="${search5 eq 2}"> selected</c:if>>SPAJ</option>
                                               <option value="3"<c:if test="${search5 eq 3}"> selected</c:if>>NO. POLIS</option>
                                               <option value="4"<c:if test="${search5 eq 4}"> selected</c:if>>NO. HP</option>
                                               <option value="5"<c:if test="${search5 eq 5}"> selected</c:if>>Status</option>
	                                        </select>
	                                        <span id="tgl_kirim"<c:if test="${search5 ne 1}"> style="display: none"</c:if>>
	                                            <input type="text" class="datepicker" name="tgl_kirim1" size="10" value="${tgl_kirim1}"<c:if test="${search5 ne 1}"> disabled</c:if>/> s/d
	                                            <input type="text" class="datepicker" name="tgl_kirim2" size="10" value="${tgl_kirim2}"<c:if test="${search5 ne 1}"> disabled</c:if>/>
	                                        </span>
                                            <span id="tgl_create"<c:if test="${search5 ne 6}"> style="display: none"</c:if>>
                                                <input type="text" class="datepicker" name="tgl_create1" size="10" value="${tgl_create1}"<c:if test="${search5 ne 6}"> disabled</c:if>/> s/d
                                                <input type="text" class="datepicker" name="tgl_create2" size="10" value="${tgl_create2}"<c:if test="${search5 ne 6}"> disabled</c:if>/>
                                            </span>
	                                        <span id="spaj5"<c:if test="${search5 ne 2}"> style="display: none"</c:if>>
	                                            <input type="text" name="reg_spaj5" value="${reg_spaj5}"<c:if test="${search5 ne 2}"> disabled</c:if>/>
	                                        </span>
                                            <span id="nopol5"<c:if test="${search5 ne 3}"> style="display: none"</c:if>>
                                                <input type="text" name="no_polis5" value="${no_polis5}"<c:if test="${search5 ne 3}"> disabled</c:if>/>
                                            </span>
                                            <span id="nohp5"<c:if test="${search5 ne 4}"> style="display: none"</c:if>>
                                                <input type="text" name="no_hp5" value="${no_hp5}"<c:if test="${search5 ne 4}"> disabled</c:if>/>
                                            </span>
                                            <span id="status5"<c:if test="${search5 ne 5}"> style="display: none"</c:if>>
                                                <select name="status5" style="margin-bottom: 2px"<c:if test="${search5 ne 5}"> disabled</c:if>>
                                                    <option value="U"<c:if test="${status5 eq 'U'}"> selected</c:if>>Unsent</option>
                                                    <option value="Q"<c:if test="${status5 eq 'Q'}"> selected</c:if>>Queued</option>
                                                    <option value="S"<c:if test="${status5 eq 'S'}"> selected</c:if>>Sent</option>
                                                    <option value="D"<c:if test="${status5 eq 'D'}"> selected</c:if>>Delivered</option>
                                                    <option value="A"<c:if test="${status5 eq 'A'}"> selected</c:if>>Aborted</option>
                                                    <option value="F"<c:if test="${status5 eq 'F'}"> selected</c:if>>Failed</option>
                                                    <option value="P"<c:if test="${status5 eq 'P'}"> selected</c:if>>Pending</option>
                                                </select><br/>
                                                <input type="text" class="datepicker" name="tgl_status1" size="10" value="${tgl_status1}"<c:if test="${search5 ne 5}"> disabled</c:if>/> s/d
                                                <input type="text" class="datepicker" name="tgl_status2" size="10" value="${tgl_status2}"<c:if test="${search5 ne 5}"> disabled</c:if>/>
                                            </span>
                                            <input type="button" value="Search" onclick="referesh5()"/>
	                                    </td>
	                                    <td align="right" class="">Go
				                            <input name="cPage5" id="cPage5" type="text" style="width:45px" maxlength="4" title="Input Nomor Halaman yg akan dikunjungi" />
				                            <input type="button" value="Go" id="go" style="width: 40px;"  onclick="referesh5()"/>
				                            <input type="submit" name="refresh5" value="refresh" id="refresh5" style="visibility: hidden;"/> 
				                            <label>&nbsp;</label>
				                            <c:choose>
				                                <c:when test="${currPage5 eq '1'}">
				                                    <font color="gray">First | Prev |</font> 
				                                </c:when>
				                                <c:otherwise>
				                                    <a href="#" class="" onclick="document.getElementById('cPage5').value='${firstPage5}'; referesh5()">First</a> | 
				                                    <a href="#" class="" onclick="document.getElementById('cPage5').value='${previousPage5}'; referesh5()">Prev</a> | 
				                                </c:otherwise>
				                            </c:choose>                         
				                            <c:choose>
				                                <c:when test="${currPage5 eq lastPage5}">
				                                    <font color="gray">Next | Last </font>
				                                </c:when>
				                                <c:otherwise>
				                                    <a href="#" class="" onclick="document.getElementById('cPage5').value='${nextPage5}'; referesh5()">Next</a> | 
				                                     <a href="#" class="" onclick="document.getElementById('cPage5').value='${lastPage5}'; referesh5()">Last</a> 
				                                </c:otherwise>
				                            </c:choose>                
			                            </td>
	                                </tr>
	                                <tr>
	                                   <td colspan="3">
	                                       Total : ${totalResult5}
	                                   </td>
	                                </tr>
	                            </table>
	                            <table class="displaytag" style="width:100%" align="center" cellpadding="2">
	                                <thead>
	                                    <tr>DAFTAR SMS KELUAR</tr>
	                                    <tr>
	                                        <th style="text-align: left" style="width:20px">ID</th>
		                                    <th style="text-align: left" style="width:80px">Penerima</th>
                                            <th style="text-align: left" style="width:100px">Tanggal Create</th>
		                                    <th style="text-align: left" style="width:100px">Tanggal Kirim</th>
                                            <th style="text-align: left" style="width:100px">No SPAJ</th>
                                            <th style="text-align: left" style="width:100px">No Polis</th>
		                                    <th style="text-align: left" style="width:150px">PESAN</th>
		                                    <th style="text-align: left" style="width:90px">Status</th>
                                            <th style="text-align: left" style="width:90px">--</th>
	                                    </tr>
	                                </thead>
	                                <tbody>
	                                    <c:forEach items="${daftarSmsOut}" var="pas5" varStatus="stat">
	                                        <tr>
	                                            <td class="left">${pas5.id}</td>
	                                            <td class="left">${pas5.recipient}</td>
	                                            <td class="left">
	                                                <fmt:formatDate value="${pas5.create_date}" pattern="dd/MM/yyyy hh:mm:ss" />
	                                            </td>
	                                            <td class="left">
	                                                <fmt:formatDate value="${pas5.sent_modem_date}" pattern="dd/MM/yyyy hh:mm:ss" /> - 
	                                                <fmt:formatDate value="${pas5.sent_date}" pattern="dd/MM/yyyy hh:mm:ss" />
	                                            </td>
                                                <td class="left">${pas5.reg_spaj}</td>
                                                <td class="left">${pas5.mspo_policy_no}</td>
                                                <td class="left">${pas5.text}</td>
			                                    <td class="left">
			                                        <c:choose>
			                                            <c:when test="${pas5.status eq \"S\"}">
			                                                Sent
			                                            </c:when>
			                                            <c:when test="${pas5.status eq \"D\"}">
			                                                Delivered
			                                            </c:when>
			                                            <c:when test="${pas5.status eq \"Q\"}">
			                                                Queued
			                                            </c:when>
			                                            <c:when test="${pas5.status eq \"A\"}">
			                                                Aborted
			                                            </c:when>
			                                            <c:when test="${pas5.status eq \"U\"}">
			                                                Unsent
			                                            </c:when>
			                                            <c:when test="${pas5.status eq \"F\"}">
			                                                Failed
			                                            </c:when>
			                                            <c:when test="${pas5.status eq \"P\"}">
			                                                Pending
			                                            </c:when>
			                                            <c:otherwise>
			                                                
			                                            </c:otherwise>
			                                        </c:choose>     
			                                    </td>
			                                    <td class="left">
			                                        <input type="button" value="HISTORY" name="viewhistory" onclick="popWin('${path}/uw/viewer.htm?window=cssms_out_hist&id=${pas5.id}', 500, 1078);"/>
			                                    </td>
	                                        </tr>
	                                    </c:forEach>
	                                </tbody>
	                            </table>
						    </fieldset>
					    </form>
					</div>
					<div id="pane6" class="panes">
                        <fieldset>
                            <legend>Report Total SMS Keluar</legend>
                            <div id="loading_bar6" style="position:absolute; right:40; width:135;visibility:visible; background-color: #F7F7F7;">
                                <span style="float:right; vertical-align: top; background-color:gray; color:white; font-weight:bold; font-size:12; text-align:center; cursor:pointer" onclick="Loading()">&nbsp;&nbsp;&nbsp;Loading ........!!&nbsp;&nbsp;</span>
                            </div>
                            <table class="entry2">
                                <tr>
                                    <th>Periode</th>
                                    <td>
                                        <select id="month6" name="month6">
                                            <c:forEach items="${cmd.monthList}" var="month">
                                                <option value="${month.value}"<c:if test="${cmd.month6 eq month.value}"> selected</c:if>>${month.label}</option>
                                            </c:forEach>
                                        </select>
                                        <select id="year6" name="year6">
                                            <c:forEach items="${cmd.yearList}" var="year">
                                                <option value="${year}"<c:if test="${cmd.year6 eq year}"> selected</c:if>>${year}</option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td>
                                        <input type="button" id="showTotalSMSOutPDF" class="viewReportBtn" value="Show PDF">
                                        <input type="button" id="showTotalSMSOutXLS" class="viewReportBtn" value="Show Excel">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <iframe id="iframeTotalSMSOut" src="" style="width: 100%; height: 600px; border: 0;"></iframe>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
				</div>
				
			</div>
		
	</c:otherwise>
</c:choose>
	<script type="text/javascript">	
		document.getElementById('loading_bar').style.visibility = "hidden";	
		document.getElementById('loading_bar2').style.visibility = "hidden";
		if(document.getElementById('loading_bar3'))
		    document.getElementById('loading_bar3').style.visibility = "hidden";
        document.getElementById('loading_bar5').style.visibility = "hidden";
        document.getElementById('loading_bar6').style.visibility = "hidden";
	</script>	
</body>


<%@ include file="/include/page/footer.jsp"%>