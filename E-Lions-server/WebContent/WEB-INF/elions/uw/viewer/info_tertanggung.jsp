<%@ include file="/include/page/taglibs.jsp"%>
<table class="entry2">

	<tr>
		<th nowrap="nowrap">
			Nama Lengkap
		</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select name="tertanggung.lti_id" disabled="disabled">
				<option value=""></option>
				<c:forEach var="l" items="${select_gelar}">
					<option
						<c:if test="${cmd.tertanggung.lti_id eq l.ID}"> SELECTED </c:if>
						value="${l.ID}">${l.GELAR}</option>
				</c:forEach>
			</select>				
			<input type="text" value="${cmd.tertanggung.mcl_first}" size="35" readonly>
			Gelar
			<input type="text" value="${cmd.tertanggung.mcl_gelar}" size="12" readonly>
			Ibu Kandung
			<input type="text"	value="${cmd.tertanggung.mspe_mother}" size="35" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">
			Bukti Identitas
		</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select disabled>
				<c:forEach var="identitas" items="${select_identitas}">
					<option
						<c:if test="${cmd.tertanggung.lside_id eq identitas.ID}"> SELECTED </c:if>
						value="${identitas.ID}">${identitas.TDPENGENAL}</option>
				</c:forEach>
			</select>
			<input type="text" value="${cmd.tertanggung.mspe_no_identity}" size="30" readonly>
			Warga Negara
			<select disabled>
				<c:forEach var="negara" items="${select_negara}">
					<option
						<c:if test="${cmd.tertanggung.lsne_id eq negara.ID}"> SELECTED </c:if>
						value="${negara.ID}">${negara.NEGARA}</option>
				</c:forEach>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">
			Tempat/Tanggal Lahir
		</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<input type="text" value="${cmd.tertanggung.mspe_place_birth}" size="25" readonly> / 
			<input type="text" value="<fmt:formatDate value="${cmd.tertanggung.mspe_date_birth}" pattern="dd/MM/yyyy"/>" size="12" readonly>
			Usia
			<input type="text" value="${cmd.tertanggung.mste_age}" size="3" readonly>
			Status
			<select disabled>
				<c:forEach var="marital" items="${select_marital}">
					<option
					<c:if test="${cmd.tertanggung.mspe_sts_mrt eq marital.ID}"> SELECTED </c:if>
					value="${marital.ID}">${marital.MARITAL}</option>
				</c:forEach>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Jenis Kelamin</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<input type="text" value="<c:choose><c:when test="${cmd.tertanggung.mspe_sex eq 1 or cmd.tertanggung.mspe_sex eq null}">Pria</c:when><c:otherwise>Wanita</c:otherwise></c:choose>">
			Agama
			<select disabled>
				<c:forEach var="agama" items="${select_agama}">
					<option
					<c:if test="${cmd.tertanggung.lsag_id eq agama.ID}"> SELECTED </c:if>
					value="${agama.ID}">${agama.AGAMA}</option>
				</c:forEach>
			</select>
			Pendidikan
			<select disabled>
				<c:forEach var="pendidikan" items="${select_pendidikan}">
				<option
					<c:if test="${cmd.tertanggung.lsed_id eq pendidikan.ID}"> SELECTED </c:if>
					value="${pendidikan.ID}">${pendidikan.PENDIDIKAN}</option>
				</c:forEach>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Alamat Rumah</th>
		<th nowrap="nowrap" class="left">
			<textarea rows="2" cols="45" readonly>${cmd.tertanggung.alamat_rumah}</textarea>
		</th>
		<th nowrap="nowrap" class="left">
			Kode Pos
			<input type="text" value="${cmd.tertanggung.kd_pos_rumah}" size="9" readonly>
			Kota
			<input type="text" value="${cmd.tertanggung.kota_rumah}" size="20" readonly>
			<br>
			No. Telepon
			<select style="width: 191px;">
				<OPTGROUP label="Telepon Rumah">
					<c:if test="${not empty cmd.tertanggung.telpon_rumah}"><option>${cmd.tertanggung.area_code_rumah} - ${cmd.tertanggung.telpon_rumah}</option></c:if>
					<c:if test="${not empty cmd.tertanggung.telpon_rumah2}"><option>${cmd.tertanggung.area_code_rumah2} - ${cmd.tertanggung.telpon_rumah2}</option></c:if>
				</OPTGROUP>
				<OPTGROUP label="Handphone">
					<c:if test="${not empty cmd.tertanggung.no_hp}"><option selected>${cmd.tertanggung.no_hp}</option></c:if>
					<c:if test="${not empty cmd.tertanggung.no_hp2}"><option>${cmd.tertanggung.no_hp2}</option></c:if>										
				</OPTGROUP>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Alamat Kantor</th>
		<th nowrap="nowrap" class="left">
			<textarea rows="2" cols="45" readonly>${cmd.tertanggung.alamat_kantor}</textarea>
		</th>
		<th nowrap="nowrap" class="left">
			Kode Pos
			<input type="text" value="${cmd.tertanggung.kd_pos_kantor}" size="9" readonly>
			Kota
			<input type="text" value="${cmd.tertanggung.kota_kantor}" size="20" readonly>
			<br>
			No. Telepon
			<select style="width: 191px;">
				<OPTGROUP label="Telepon Kantor">
					<c:if test="${not empty cmd.tertanggung.telpon_kantor}"><option>${cmd.tertanggung.area_code_kantor} - ${cmd.tertanggung.telpon_kantor}</option></c:if>
					<c:if test="${not empty cmd.tertanggung.telpon_kantor2}"><option>${cmd.tertanggung.area_code_kantor2} - ${cmd.tertanggung.telpon_kantor2}</option></c:if>
				</OPTGROUP>
				<OPTGROUP label="Fax">
					<c:if test="${not empty cmd.tertanggung.no_fax}"><option>${cmd.tertanggung.area_code_fax} - ${cmd.tertanggung.no_fax}</option></c:if>
				</OPTGROUP>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Alamat E-mail</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<input type="text" value="${cmd.tertanggung.email}" size="100" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Tujuan Membeli Asuransi</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<%-- <select disabled style="width: 200px;">
				<c:forEach var="tujuan" items="${select_tujuan}">
					<option
						<c:if test="${cmd.tertanggung.mkl_tujuan eq tujuan.ID}"> SELECTED </c:if>
						value="${tujuan.ID}">${tujuan.TUJUAN}</option>
				</c:forEach>
			</select>
			Lainnya --%>
			<input style="width: 311px;" type="text" value="${cmd.tertanggung.tujuana}" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Perkiraan Penghasilan<br>Kotor Per Tahun</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select disabled style="width: 200px;">
				<c:forEach var="penghasilan" items="${select_penghasilan}">
					<option
						<c:if test="${cmd.tertanggung.mkl_penghasilan eq penghasilan.ID}"> SELECTED </c:if>
						value="${penghasilan.ID}">${penghasilan.PENGHASILAN}</option>
				</c:forEach>
			</select>
			Jabatan
			<input style="width: 309px;" tipe="text" value="${cmd.tertanggung.kerjab}" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Sumber Pendanaan<br>Pembelian Asuransi</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<%-- <select name="tertanggung.mkl_pendanaan" disabled style="width: 200px;">
				<c:forEach var="dana" items="${select_dana}">
					<option
						<c:if test="${cmd.tertanggung.mkl_pendanaan eq dana.ID}"> SELECTED </c:if>
						value="${dana.ID}">${dana.DANA}
					</option>
				</c:forEach>
			</select>
				Lainnya --%>
			<spring:bind path="cmd.tertanggung.danaa">
				<input type="text" name="${status.expression}"
					value="${status.value }" size="42" maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
			</spring:bind>
		</th>
	</tr>
	<c:forEach var="listkyc" items="${cmd.pemegang.daftarKyc}" varStatus="status">
		<c:if test="${cmd.pemegang.lsre_id eq 1}">
	<tr>
		<th nowrap="nowrap">&nbsp;</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select name="listkyc.kyc_desc1${status.index +1}" disabled style="width: 200px;">
				<c:forEach var="sdana" items="${select_hasil}">
					<option 
						<c:if test="${listkyc.kyc_desc1 eq sdana.ID}"> SELECTED </c:if>
						value="${sdana.ID}">${sdana.DANA}
					</option>
				</c:forEach>
			</select>
			Lainnya
			<input type="text" name='listkyc.kyc_desc_x${status.index +1}' value ='${listkyc.kyc_desc_x}' size="42" maxlength="60">
		</th>
	</tr>
		</c:if> 		
	</c:forEach>
	<%-- <tr>
		<th nowrap="nowrap">Sumber Penghasilan</th>    
		<th nowrap="nowrap" class="left" colspan="2">
			<select name="tertanggung.mkl_smbr_penghasilan" disabled style="width: 200px;">
				<c:forEach var="hasil" items="${select_hasil}">
					<option
						<c:if test="${cmd.tertanggung.mkl_smbr_penghasilan eq hasil.ID}"> SELECTED </c:if>
						value="${hasil.ID}">${hasil.DANA}
					</option>
				</c:forEach>
			</select>
				Lainnya
			<spring:bind path="cmd.tertanggung.shasil">
				<input type="text" name="${status.expression}"
					value="${status.value }" size="42" maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
			</spring:bind>
		</th>
	</tr> --%>
	<c:forEach var="listkyc2" items="${cmd.pemegang.daftarKyc2}" varStatus="status">
	<c:if test="${cmd.pemegang.lsre_id eq 1}">
	<tr>
		<th nowrap="nowrap">&nbsp;</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select name="listkyc2.kyc_desc2${status.index +1}" disabled style="width: 200px;">
				<c:forEach var="sdana" items="${select_hasil}">
					<option 
						<c:if test="${listkyc2.kyc_desc2 eq sdana.ID}"> SELECTED </c:if>
							value="${sdana.ID}">${sdana.DANA}
					</option>
				</c:forEach>
			</select>
			Lainnya
			<input type="text" name='listkyc2.kyc_desc2_x${status.index +1}' value ='${listkyc2.kyc_desc2_x}' size="42" maxlength="60">
		</th>
	</tr>
	</c:if> 		
	</c:forEach>
				
	<tr>
		<th nowrap="nowrap">Klasifikasi Pekerjaan</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<input type="text" style="width: 500px;"  value="${cmd.tertanggung.mkl_kerja} " readonly="readonly">
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Klasifikasi Bidang Industri</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select disabled style="width: 200px;">
				<c:forEach var="industri" items="${select_industri}">
					<option
						<c:if test="${cmd.tertanggung.mkl_industri eq industri.ID}"> SELECTED </c:if>
						value="${industri.ID}">${industri.BIDANG}</option>
				</c:forEach>
			</select>
			Lainnya
			<input style="width: 311px;" type="text" value="${cmd.tertanggung.industria}" readonly>
		</th>
	</tr>

</table>