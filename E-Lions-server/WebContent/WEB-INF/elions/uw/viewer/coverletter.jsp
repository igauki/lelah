<%@ include file="/include/page/header_jquery.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path}/include/js/default.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (jQueryUI Tabs) init tab2 Utama
		$("#tabs").tabs();

		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title], label[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if($("#jenis_report").val() == 0){
			$("#branch").attr('disabled','disabled');
		}
		
		$("#jenis_report").change(function() {
			if($("#jenis_report").val() == 1){
				$("#branch").removeAttr('disabled','disabled');
			}else{
				$("#branch").attr('disabled','disabled');
			}
        });
        
        if($("#pesan").val() == 'Proses transfer polis pada sistem Cover Letter berhasil dilakukan!'){
        	var spaj = $("#spajproses").val();
			var dist = document.getElementById("dist").value;
			popWin('${path}/uw/uw.htm?window=reportCoverLetterJne&flag=cl&spaj='+spaj+'&dist='+dist, 300, 400);
        
        }else if($("#pesan").val() == 'Cetak CoverLetter Dalam Excel!'){
			var spaj = $("#spajproses").val();
			var dist = document.getElementById("dist").value;
			popWin('${path}/uw/uw.htm?window=reportCoverLetterJne&flag=cl&spaj='+spaj+'&dist='+dist, 300, 400);
		}
	});
</script>


<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }
	
	/* untuk warna kolom input */
	.warna { color: grey; }
	
	/* styling untuk server-side validation error */
	.errorField { border: 1px solid red; }
	.errorMessage { color: red; display: block;}
</style>

	<body>
		<form:form method="post" name="formpost" id="formpost" commandName="cmd">
			<fieldset class="ui-widget ui-widget-content" id="fieldset1">
				<legend class="ui-widget-header ui-corner-all"><div>Pengiriman Polis Melalui General Affair (GA)</div></legend>
				
<!-- 				<form:hidden path="datapolis"/> -->
				
				<table class="displaytag" id="tab1">
					<tr></tr>
					<tr></tr>
					<tr></tr>
					<tr>
						<th style="width: 200px;" align="left">Jenis report</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:select path="jenis_report">
								<form:option value="0" label="COVERING LETTER"/>
								<form:option value="1" label="COVERING LETTER PER ADMIN"/>
							</form:select>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">Branch Admin</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:select path="branch">
								<form:option value="" label="-NONE-"/>
								<form:options items="${listbranch}" itemValue="key" itemLabel="value"/>
							</form:select>
						</td>
						<td>
							<form:errors path="branch" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">Tgl Cetak Polis</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input path="tglcetak_awal" cssClass="datepicker"/>
							s/d
							<form:input path="tglcetak_akhir" cssClass="datepicker"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">Status Polis</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<c:if test="${cmd.jenis_user eq \"uw\"}">
								<form:radiobutton path="status_polis" value="0"/>Polis Yang Belum Dikirim Ke GA
						    	<form:radiobutton path="status_polis" value="1"/>Polis Yang Sudah Dikirim Ke GA
						    </c:if>
						    <c:if test="${cmd.jenis_user eq \"ga\"}">
						    	<form:radiobutton path="status_polis" value="1"/>Polis Yang Sudah Dikirim Oleh UW
						    	<form:radiobutton path="status_polis" value="2"/>Polis Yang Sudah Diterima Oleh GA dan Belum Dikirim Ke ADMIN
						    	<form:radiobutton path="status_polis" value="3"/>Polis Yang Sudah Dikirim Ke ADMIN
						    </c:if>
						    <c:if test="${cmd.jenis_user eq \"adm\"}">
						    	<form:radiobutton path="status_polis" value="3"/>Polis Yang Sudah Dikirim Oleh GA
						    	<form:radiobutton path="status_polis" value="4"/>Polis Yang Sudah Diterima Oleh ADMIN dan Belum Dikirim ke AGENT
						    	<form:radiobutton path="status_polis" value="5"/>Polis Yang Sudah Dikirim Ke AGENT
						    </c:if>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">Jalur Distribusi</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:radiobutton path="jalur_dist" value="0"/>Non DMTM
						    <form:radiobutton path="jalur_dist" value="1"/>DMTM
						</td>
					</tr>
					<tr>
						<th></th>
						<th></th>
						<td>
							<input type="submit" name="btnShow" id="btnShow" title="Tampilkan Polis" value="Tampilkan Polis">
						</td>
					</tr>
					<tr>
						<th></th>
						<th></th>
						<td>
							<form:hidden path="pesan"/>
							<form:hidden path="spajproses"/>
							<input type="hidden" id="dist" value="${dist}"> 
							<form:errors path="pesan" cssClass="errorMessage"/>
						</td>
					</tr>
				</table>
			</fieldset>
			
			
			<c:if test="${cmd.jenis_user eq \"uw\"}">
			<fieldset class="ui-widget ui-widget-content" id="fieldset2" >
				<table class="displaytag" id="tab2">
					<tr>
						<th style="width: 200px;" align="left">Tgl Proses UW</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input path="tglproses_awal" cssClass="datepicker"/>
							s/d
							<form:input path="tglproses_akhir" cssClass="datepicker"/>
						</td>
					</tr>
					<tr>
						<th></th>
						<th></th>
						<td>
							<input type="submit" name="btnPrint" id="btnPrint" title="Cetak Report" value="Cetak Report">
						</td>
					</tr>
				</table>
			</fieldset>
			</c:if>
			

			<c:if test="${(cmd.jenis_user eq \"uw\" and cmd.status_polis eq \"1\") or (cmd.jenis_user eq \"ga\" and cmd.status_polis eq \"3\") or
						  (cmd.jenis_user eq \"adm\" and cmd.status_polis eq \"5\")}">
			<fieldset id="fieldset3">
				<table class="displaytag" cellpadding='5' cellspacing='0' border="1" align="center">
					<tr>
						<!-- <th>Check All<br/><input type="checkbox" name="allCheck" onClick="selectallMe()"></th> -->
						<c:if test="${cmd.jenis_user eq \"uw\"}">
							<th nowrap align="center" bgcolor="#b7b7b7">Check</th>
						</c:if>
						<th nowrap align="center" width="120" bgcolor="#b7b7b7">No. Polis</th>
						<th nowrap align="center" width="200" bgcolor="#b7b7b7">Pemegang Polis</th>
						<th nowrap align="center" width="200" bgcolor="#b7b7b7">Tertanggung</th>
						<th nowrap align="center" width="200" bgcolor="#b7b7b7">Admin Input</th>
						<th nowrap align="center" width="120" bgcolor="#b7b7b7">Produk</th>
						<th nowrap align="center" width="100" bgcolor="#b7b7b7">Tgl. Cetak Polis</th>
						<th nowrap align="center" width="50" bgcolor="#b7b7b7">SimasCard</th>
						<th nowrap align="center" width="50" bgcolor="#b7b7b7">Kartu Admedika</th>
						<c:if test="${cmd.jenis_user eq \"ga\"}">
							<th nowrap align="center" width="150" bgcolor="#b7b7b7">No. PO</th>
						</c:if>
					</tr>
					<c:forEach items="${cmd.datapolis}" var="d" varStatus="st">
					<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';return true;" onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;">
						<c:if test="${cmd.jenis_user eq \"uw\"}">
							<td align="center"><form:checkbox cssClass="noBorder" path="cek_polis" value="${st.index}"/></td>
						</c:if>
						<td nowrap>${d.no_polis}</td>
						<td nowrap>${d.pemegangpolis}</td>
						<td nowrap>${d.tertanggung}</td>
						<td nowrap>${d.admininput}</td>
						<td nowrap>${d.produk}</td>
						<td nowrap align="center">${d.tgl_printpolis}</td>
						<td nowrap>${d.simascard}</td>
						<td nowrap>${d.admedika}</td>
						<c:if test="${cmd.jenis_user eq \"ga\"}">
							<td nowrap>${d.no_resi}</td>
						</c:if>
					</tr>
					</c:forEach>       
				</table>
				<c:if test="${cmd.jenis_user eq \"uw\"}">
					<table cellpadding='0' cellspacing='0' align="center">
						<tr>
							<th></th><td><br/></td>
						</tr>
						<tr>
							<div class="rowElem">
								<label></label>
								<input type="submit" name="btnXls" id="btnXls" title="Cetak XLS" value="Cetak XLS">
							</div>
						</tr>
					</table>
				</c:if>
			</fieldset>
			</c:if>
			
			<c:if test="${(cmd.jenis_user eq \"uw\" and cmd.status_polis eq \"0\") or (cmd.jenis_user eq \"ga\" and cmd.status_polis eq \"1\") or
						  (cmd.jenis_user eq \"ga\" and cmd.status_polis eq \"2\") or (cmd.jenis_user eq \"adm\" and cmd.status_polis eq \"3\") or
						  (cmd.jenis_user eq \"adm\" and cmd.status_polis eq \"4\") or (cmd.jenis_user eq \"all\")}">
			<fieldset id="fieldset4">
				<table class="displaytag" cellpadding='2' cellspacing='0' border="1" align="center">
					<tr>
						<!-- <th>Check All<br/><input type="checkbox" name="allCheck" onClick="selectallMe()"></th> -->
						<th nowrap align="center" bgcolor="#b7b7b7">Check</th>
						<th nowrap align="center" width="120" bgcolor="#b7b7b7">No. Polis</th>
						<th nowrap align="center" width="200" bgcolor="#b7b7b7">Pemegang Polis</th>
						<th nowrap align="center" width="200" bgcolor="#b7b7b7">Tertanggung</th>
						<th nowrap align="center" width="200" bgcolor="#b7b7b7">Admin Input</th>
						<th nowrap align="center" width="120" bgcolor="#b7b7b7">Produk</th>
						<th nowrap align="center" width="100" bgcolor="#b7b7b7">Tgl. Cetak Polis</th>
						<th nowrap align="center" width="50" bgcolor="#b7b7b7">SimasCard</th>
						<th nowrap align="center" width="50" bgcolor="#b7b7b7">Kartu Admedika</th>
						<c:if test="${(cmd.jenis_user eq \"ga\" and cmd.status_polis eq \"2\")}">
							<th nowrap align="center" width="150" bgcolor="#b7b7b7">No. PO</th>
						</c:if>
					</tr>
					<c:forEach items="${cmd.datapolis}" var="d" varStatus="st">
					<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';return true;" onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;">
						<td align="center"><form:checkbox cssClass="noBorder" path="cek_polis" value="${st.index}"/></td>
						<td nowrap>${d.no_polis}</td>
						<td nowrap>${d.pemegangpolis}</td>
						<td nowrap>${d.tertanggung}</td>
						<td nowrap>${d.admininput}</td>
						<td nowrap>${d.produk}</td>
						<td nowrap align="center">${d.tgl_printpolis}</td>
						<c:if test="${(cmd.jenis_user eq \"uw\" and cmd.status_polis eq \"0\")}">
							<td align="center"><form:checkbox cssClass="noBorder" path="cek_simascard" value="${st.index}"/></td>
							<td align="center"><form:checkbox cssClass="noBorder" path="cek_admedika" value="${st.index}"/></td>
						</c:if>
						<c:if test="${(cmd.jenis_user ne \"uw\" and cmd.status_polis ne \"0\")}">
							<td nowrap>${d.simascard}</td>
							<td nowrap>${d.admedika}</td>
						</c:if>
						<c:if test="${(cmd.jenis_user eq \"ga\" and cmd.status_polis eq \"2\")}">
							<td align="center"><form:input path="input_resi"/></td>
<!-- 							<td align="center"><input type="text" id="po" name="po" value=""/></td> -->
						</c:if>
					</tr>
					</c:forEach>       
				</table>
				<table cellpadding='0' cellspacing='0' align="center">
					<tr>
						<th></th><td><br/></td>
					</tr>
					<tr>
						<div class="rowElem">
							<label></label>
							<input type="submit" name="btnSave" id="btnSave" title="Simpan" value="Simpan">
						</div>
					</tr>
				</table>
			</fieldset>
			</c:if>
			
		</form:form>
	</body>
	
</html>
