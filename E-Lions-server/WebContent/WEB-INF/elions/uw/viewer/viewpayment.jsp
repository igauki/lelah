<%@ include file="/include/page/header.jsp"%>
<script>
			function show(tipe){
				var lnk;
				/*
				if(tipe=='jasper'){
					document.getElementById('infoFrame').src='${path}/include/page/appletviewer.jsp?tipe='+tipe
					+'&show=true&startDate='+document.formpost.startDate.value
					+'&endDate='+document.formpost.endDate.value
					+'&ket='+document.formpost.ket.value.substring(0,4)
					+'&report=viewpayment';
				}else */
				if(tipe=='pdfprint'){
					lnk='${path}/report/uw.pdf?window=viewpayment&print=true'
					+'&show=true&startDate='+document.formpost.startDate.value
					+'&endDate='+document.formpost.endDate.value
					+'&ket='+document.formpost.ket.value.substring(0,4);
					document.getElementById('infoFrame').width=5;
					document.getElementById('infoFrame').height=5;
				}else{
					lnk='${path}/report/uw.html?window=viewpayment'
					+'&show=true&attached=1&startDate='+document.formpost.startDate.value
					+'&endDate='+document.formpost.endDate.value
					+'&ket='+document.formpost.ket.value.substring(0,4);
				}
				document.getElementById('infoFrame').src = lnk;
			}
</script>
<body style="height: 100%;" 
	onload="setupPanes('container1', 'tab1'); document.title='PopUp :: Pembayaran Selisih'; setFrameSize('infoFrame', 62); <c:if test="${not empty param.printPDF}">document.getElementById('infoFrame').height=5;</c:if>"
	onresize="setFrameSize('infoFrame', 62);">
	
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">View Pembayaran</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form name="formpost" method="post">
				<table class="entry2" width="100%">
					<tr>
						<th>Tanggal:</th>
						<th>
							<script>inputDate('startDate', '<fmt:formatDate value="${cmd.sysdate}" pattern="dd/MM/yyyy"/>', false);</script>
							s/d 
							<script>inputDate('endDate', '<fmt:formatDate value="${cmd.sysdate}" pattern="dd/MM/yyyy"/>', false);</script>
						</th>
						<th>Jenis:</th>
						<th>
							<select name="ket">
							<c:forEach var="s" items="${cmd.ktg}">
								<option value="${s}">${s}</option>
							</c:forEach>
							</select>
						</th>
						<th>
							<input type="button" value="PRINT " name="showPDFPRINT"
								onclick="show('pdfprint');document.getElementById('infoFrame').height=5;document.getElementById('infoFrame').width=5;"> 
							<input style="display:none;" type="button" value="SHOW " name="showPDF"
								onclick="show('pdf');">
							<input type="button" value="Show" name="showHTML" onclick="show('html');">
							<!-- <input type="button"
							value="SHOW " name="showJASPER" class="applet"
							onclick="show('jasper');"> -->
						</th>
					</tr>
					<tr>
						<td colspan=5>
							<iframe name="infoFrame" id="infoFrame" width="100%" frameborder="YES" />		
						</td>
					</tr>
				</table>
				</form>
			</div>
		</div>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>