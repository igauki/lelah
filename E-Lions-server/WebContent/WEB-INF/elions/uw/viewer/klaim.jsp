<%@ include file="/include/page/header.jsp"%>

<script type="text/javascript">
	hideLoadingMessage();
	

function Right(str, n){
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}


function DateAdd(objDate, intDays)
{
  var iSecond=1000;	 // Dates are represented in milliseconds
  var iMinute=60*iSecond;
  var iHour=60*iMinute;
  var iDay=24*iHour;
  var objReturnDate=new Date();
  objReturnDate.setTime(objDate.getTime()+(intDays*iDay));
  return objReturnDate;
}

function listtgl()
{
	var tanggal_beg = document.frmParam.tgl1.value;
	
	var arrMonthDays = new Array (	"31", "29", "31",
					"30", "31", "30",
					"31", "31", "30",
					"31","30","31"	  );

		var tahun_s = tanggal_beg.substring(6,10);
		var bulan_s = tanggal_beg.substring(3,5);
		var tanggal_s = tanggal_beg.substring(0,2);
		var tanggal  = tanggal_s+"/"+bulan_s+"/"+tahun_s;
		
	if ((bulan_s==2)&&  (tahun_s%4>0) ) {
	    tanggal_s=28;
	}else{
		tanggal_s=arrMonthDays[bulan_s-1];
	}	
	tanggal  = tanggal_s+"/"+bulan_s+"/"+tahun_s;

	document.frmParam._tgl2.value=tanggal;
	document.frmParam.tgl2.value=tanggal;
	document.frmParam.tgl3.value=tanggal;
}

function listtgl2()
{
	tanggal = document.frmParam._tgl2.value;
	document.frmParam._tgl2.value=tanggal;
	document.frmParam.tgl2.value=tanggal;
	document.frmParam.tgl3.value=tanggal;
}	
	
function cari(str){
	switch (str) {
	case "matibayarindividu" : 
		document.getElementById('infoFrame').src='${path}/uw/viewer/klaim.htm?window=matibayar&id=1'; 
		break;
	case "matipendingindividu" :
		document.getElementById('infoFrame').src='${path}/uw/viewer/klaim.htm?window=matipending&id=1'; 
		break;
	case "sehatindividu" :
		document.getElementById('infoFrame').src='${path}/uw/viewer/klaim.htm?window=sehat&id=1'; 
		break;
	case "matibayarbac" : 
		document.getElementById('infoFrame').src='${path}/uw/viewer/klaim.htm?window=matibayar&id=2'; 
		break;
	case "matipendingbac" :
		document.getElementById('infoFrame').src='${path}/uw/viewer/klaim.htm?window=matipending&id=2'; 
		break;
	case "sehatbac" :
		document.getElementById('infoFrame').src='${path}/uw/viewer/klaim.htm?window=sehat&id=2'; 
		break;
	}
}

function caridaftar()
{
	if ( document.frmParam.tgl1.value=="__/__/____" || document.frmParam.tgl1.value=="" || document.frmParam.tgl2.value=="__/__/____" || document.frmParam.tgl2.value=="")
	{
		alert('Silahkan isi tanggal terlebih dahulu');
	}else{
		var tanggal_beg = document.frmParam.tgl1.value;
		var tahun_s = tanggal_beg.substring(6,10);
		var bulan_s = tanggal_beg.substring(3,5);
		var tanggal_s = tanggal_beg.substring(0,2);
		var tanggal1  = tahun_s+bulan_s+tanggal_s;
		var tanggal_end = document.frmParam.tgl2.value;
		tahun_s = tanggal_end.substring(6,10);
		bulan_s = tanggal_end.substring(3,5);
		tanggal_s = tanggal_end.substring(0,2);
		var tanggal2  = tahun_s+bulan_s+tanggal_s;
		frmParam.submit();
	}
}
</script>
<body style="height: 100%;"
	onload="setFrameSize('infoFrame', 68);"
	onresize="setFrameSize('infoFrame', 68);">
<form name="frmParam" method="post">

<div class="tabcontent">
<c:if test="${kunci eq null}"> 
<table class="entry" width="98%">
	<tr>
		<th colspan="3"> Window View Klaim</th>
	</tr>
	<tr>
		<th> INDIVIDU </th>
		<td>
		<input type="button" value="LAPORAN KLAIM KEMATIAN DIBAYAR" name="search" onClick="cari('matibayarindividu');"> 
		 <input type="button" value="LAPORAN KLAIM KEMATIAN PENDING" name="search1" onClick="cari('matipendingindividu');">
		<input type="button" value="LAPORAN KLAIM KESEHATAN" name="search2" onClick="cari('sehatindividu');">
		</td>
		<td><div id="prePostError" style="display:none;"></div></td>
	</tr>
	<tr>
		<th> BANCASSURANCE </th>
		<td>
		<input type="button" value="LAPORAN KLAIM KEMATIAN DIBAYAR" name="search3" onClick="cari('matibayarbac');"> 
		 <input type="button" value="LAPORAN KLAIM KEMATIAN PENDING" name="search4" onClick="cari('matipendingbac');">
		<input type="button" value="LAPORAN KLAIM KESEHATAN" name="search5" onClick="cari('sehatbac');">
		</td>
		<td><div id="prePostError" style="display:none;"></div></td>
	</tr>
	<tr>
		<td colspan="3">
			<iframe src="${path}/uw/viewer/klaim.htm?window=main" name="infoFrame" id="infoFrame"
				width="100%"> Please Wait... </iframe>
		</td>
	</tr>
</table>
</c:if>
<c:if test="${kunci eq \"matibayarindividu\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th colspan="2"> LAPORAN KLAIM KEMATIAN DIBAYAR (INDIVIDU)
	   </th>
      </tr>
  <tr> 
        <th >Tanggal Klaim</th>
        <th><input type="hidden" name="id" value="1">
              <script>inputDate('tgl1', '${tgl1}', false,'listtgl();');</script>
			s/d
              <script>inputDate('tgl2', '${tgl2}', false,'listtgl2();');</script>
			<input type="hidden" name="tgl3">
		</th>
      </tr>       
      <tr> 
        <th colspan="2" ><input type="button" value="Cari" name="search" onClick="caridaftar();"> 
		</th>
      </tr>
      <tr> 
        <td> </td>
      </tr>
    </table>
</c:if>
<c:if test="${kunci eq \"matipendingindividu\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  >LAPORAN KLAIM KEMATIAN PENDING (INDIVIDU)
	   </th>
      </tr>	 
  <tr> 
        <th >Tanggal Klaim</th>
        <th><input type="hidden" name="id" value="1">
              <script>inputDate('tgl1', '${tgl1}', false,'listtgl();');</script>
			s/d
              <script>inputDate('tgl2', '${tgl2}', false,'listtgl2();');</script>
			<input type="hidden" name="tgl3">
		</th>
      </tr>       
      <tr> 
        <th colspan="2" ><input type="button" value="Cari" name="search" onClick="caridaftar();"> 
		</th>
      </tr>
      <tr> 
        <td> </td>
      </tr>
    </table>
</c:if>
<c:if test="${kunci eq \"sehatindividu\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  >LAPORAN KLAIM KESEHATAN (INDIVIDU)
	   </th>
      </tr>
  <tr> 
        <th >Tanggal Klaim</th>
        <th><input type="hidden" name="id" value="1">
              <script>inputDate('tgl1', '${tgl1}', false,'listtgl();');</script>
			s/d
              <script>inputDate('tgl2', '${tgl2}', false,'listtgl2();');</script>
			<input type="hidden" name="tgl3">
		</th>
      </tr>      
      <tr> 
        <th colspan="2" ><input type="button" value="Cari" name="search" onClick="caridaftar();"> 
		</th>
      </tr>
      <tr> 
        <td> </td>
      </tr>
    </table>
</c:if>
<c:if test="${kunci eq \"matibayarbac\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  > LAPORAN KLAIM KEMATIAN DIBAYAR (BANCASSURANCE)
	   </th>
      </tr>
  <tr> 
        <th >Tanggal Klaim</th>
        <th><input type="hidden" name="id" value="2">
              <script>inputDate('tgl1', '${tgl1}', false,'listtgl();');</script>
			s/d
              <script>inputDate('tgl2', '${tgl2}', false,'listtgl2();');</script>
			<input type="hidden" name="tgl3">
		</th>
      </tr>       
      <tr> 
        <th colspan="2" ><input type="button" value="Cari" name="search" onClick="caridaftar();"> 
		</th>
      </tr>
      <tr> 
        <td> </td>
      </tr>
    </table>
</c:if>
<c:if test="${kunci eq \"matipendingbac\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  >LAPORAN KLAIM KEMATIAN PENDING (BANCASSURANCE)
	   </th>
      </tr>	 
  <tr> 
        <th >Tanggal Klaim</th>
        <th><input type="hidden" name="id" value="2">
              <script>inputDate('tgl1', '${tgl1}', false,'listtgl();');</script>
			s/d
              <script>inputDate('tgl2', '${tgl2}', false,'listtgl2();');</script>
			<input type="hidden" name="tgl3">
		</th>
      </tr>       
      <tr> 
        <th colspan="2" ><input type="button" value="Cari" name="search" onClick="caridaftar();"> 
		</th>
      </tr>
      <tr> 
        <td> </td>
      </tr>
    </table>
</c:if>
<c:if test="${kunci eq \"sehatbac\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  >LAPORAN KLAIM KESEHATAN (BANCASSURANCE)
	   </th>
      </tr>
  <tr> 
        <th >Tanggal Klaim</th>
        <th><input type="hidden" name="id" value="2">
              <script>inputDate('tgl1', '${tgl1}', false,'listtgl();');</script>
			s/d
              <script>inputDate('tgl2', '${tgl2}', false,'listtgl2();');</script>
			<input type="hidden" name="tgl3">
		</th>
      </tr>      
      <tr> 
        <th colspan="2" ><input type="button" value="Cari" name="search" onClick="caridaftar();"> 
		</th>
      </tr>
      <tr> 
        <td> </td>
      </tr>
    </table>
</c:if>
</div>
</form>
</body>

<%@ include file="/include/page/footer.jsp"%>