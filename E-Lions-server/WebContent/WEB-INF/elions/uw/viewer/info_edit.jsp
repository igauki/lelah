<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>
<script type="text/javascript" src="${path }/include/js/bac/pemegang.js"></script>
<script>
	function info_add(){
		formpost.flag.value="1";
		formpost.submit();
	}
	
	function info_delete(){
		formpost.flag.value="2";
		formpost.submit();
	}
	
	function info_simpan(){
	
		var edt='${cmd.flagEditAll}';
		var pil1,pil2,pil3,pil4,pil6,pil9,pil11,pil12,pil13,pil14;	
		
		var cuti = document.getElementById('cuti').value;		
		if (cuti =="" || cuti==null){document.getElementById('cuti').value='0'};
			
		if(edt==1){
	
			
			pil1=formpost.pil1.checked;
			pil2=formpost.pil2.checked;
			pil4=formpost.pil4.checked;
			pil6=formpost.pil6.checked;
			pil9=formpost.pil9.checked;
			pil11=formpost.pil11.checked;
			pil12=formpost.pil12.checked;
			pil13=formpost.pil13.checked;			
			
			
			
		}
				
		pil3=formpost.pil3.checked;			
		if(formpost.pil5==null || formpost.pil7==null || formpost.pil8==null || formpost.pil10==null){
	
			if( pil1 || pil2 || pil3 || pil4 || pil6 || pil9 ||pil11 ||pil12 ||pil13 ||pil14){
			    
				formpost.flag.value="3";
				formpost.submit();
			}else
				alert("Silahkan Pilih Data yang ada mao simpan");
				 document.getElementById('cuti').value="";
		}else{
		
			pil5=formpost.pil5.checked;
			pil7=formpost.pil7.checked;
			pil8=formpost.pil8.checked;
			pil10=formpost.pil10.checked;
			pil14=formpost.pil14.checked;
			if( pil1 || pil2 || pil3 || pil4 || pil5 || pil6 || pil7 || pil8 || pil9 ||pil10 ||pil11||pil12||pil13||pil14){
					
				formpost.flag.value="3";
				formpost.submit();
			}else
				alert("Silahkan Pilih Data yang ada mao simpan");
				document.getElementById('cuti').value="";
		}
	}
	
	function pilih_all(){
		if(formpost.pilAll.value=='1'){
			c=true;
			formpost.pilAll.value='0';
		}else {
			formpost.pilAll.value='1';
			c=false;
		}
		
		formpost.pil1.checked=c;
		formpost.pil2.checked=c;
		formpost.pil3.checked=c;
		formpost.pil4.checked=c;
		formpost.pil6.checked=c;
		formpost.pil9.checked=c;
		formpost.pil11.checked=c;
		formpost.pil12.checked=c;
		formpost.pil13.checked=c;
		
		if(formpost.pil5!=null ||formpost.pil7!=null ||formpost.pil8!=null ||formpost.pil10!=null ){
			formpost.pil5.checked=c;
			formpost.pil7.checked=c;
			formpost.pil8.checked=c;
			formpost.pil10.checked=c;
			formpost.pil14.checked=c;
		}
	}

	
</script>
<form name="formpost" method="post">
<c:if test="${not empty boleh}">
<c:if test="${cmd.flagEditAll eq 1}">
<fieldset>
	<legend>Data Spaj</legend>
	<table class="entry2">
		<tr>
			<th width="15%">
				Nomor Blanko
			</th>
			<th class="left">
				<spring:bind path="cmd.blangko">
					<input type="text" name="${status.expression}"  value="${status.value}" size="10" readonly="readonly">
				</spring:bind>
			</th>
			<th>
				<div align="right"><input <c:if test="${FLAG eq  1}">disabled</c:if> type="checkbox" name="pil1" onmouseover="return overlib('Silahkan di pilih untuk melakukan perubahan', AUTOSTATUS, WRAP);" onmouseout="nd();" class="noBorder"></div>
			</th>
		</tr>
		<tr>
            <th width="15%">
                No Identitas Pemegang
            </th>
            <th class="left">
                    <select name="identitas_pp" >
                        <c:forEach var="x" items="${select_identitas}">
                            <option value="${x.ID}"
                                <c:if test="${cmd.pemegang.lside_id eq x.ID }">Selected</c:if>>
                                     ${x.TDPENGENAL}
                            </option>
                        </c:forEach>
                    </select>
                <spring:bind path="cmd.pemegang.mspe_no_identity">
                    <input type="text" name="${status.expression}"  value="${status.value}" size="30">
                </spring:bind>
            </th>
            <th>
                <div align="right"><input <c:if test="${FLAG eq  1}">disabled</c:if> type="checkbox" name="pil6" onmouseover="return overlib('Silahkan di pilih untuk melakukan perubahan', AUTOSTATUS, WRAP);" onmouseout="nd();" class="noBorder"></div>
            </th>
        </tr>
        	<tr>
            <th width="15%">
               Jenis Kelamin Pemegang
            </th>
            <th class="left">
                   <select name="sex_pp">
	                   	 <option <c:if test="${cmd.pemegang.mspe_sex eq '0'}">SELECTED</c:if> value="0">Wanita</option> 
	           	       	 <option <c:if test="${cmd.pemegang.mspe_sex eq '1'}">SELECTED</c:if> value="1">Laki-Laki</option> 
						</select> 
            </th>
            <th>
                <div align="right"><input <c:if test="${FLAG eq  1}">disabled</c:if> type="checkbox" name="pil9" onmouseover="return overlib('Silahkan di pilih untuk melakukan perubahan', AUTOSTATUS, WRAP);" onmouseout="nd();" class="noBorder"></div>
            </th>
        </tr>
         <c:if test="${cmd.pemegang.lsre_id ne 1}" >  
        <tr>
            <th width="15%">
                No Identitas Tertanggung
            </th>
            <th class="left">
                    <select name="identitas_tt" >
                        <c:forEach var="x" items="${select_identitas}">
                            <option value="${x.ID}"
                                <c:if test="${cmd.tertanggung.lside_id eq x.ID }">Selected</c:if>>
                                     ${x.TDPENGENAL}
                            </option>
                        </c:forEach>
                    </select>
                <spring:bind path="cmd.tertanggung.mspe_no_identity">
                    <input type="text" name="${status.expression}"  value="${status.value}" size="30">
                </spring:bind>
            </th>
            <th>
                <div align="right"><input <c:if test="${FLAG eq  1}">disabled</c:if> type="checkbox" name="pil7" onmouseover="return overlib('Silahkan di pilih untuk melakukan perubahan', AUTOSTATUS, WRAP);" onmouseout="nd();" class="noBorder"></div>
            </th>
        </tr>
        <tr>
            <th width="15%">
               Jenis Kelamin tertanggung
            </th>
            <th class="left">
                   <select name="sex_tt">
	                   	 <option <c:if test="${cmd.tertanggung.mspe_sex eq '0'}">SELECTED</c:if> value="0">Wanita</option> 
	           	       	 <option <c:if test="${cmd.tertanggung.mspe_sex eq '1'}">SELECTED</c:if> value="1">Laki-Laki</option> 
						</select> 
            </th>
            <th>
                <div align="right"><input <c:if test="${FLAG eq  1}">disabled</c:if> type="checkbox" name="pil10" onmouseover="return overlib('Silahkan di pilih untuk melakukan perubahan', AUTOSTATUS, WRAP);" onmouseout="nd();" class="noBorder"></div>
            </th>
        </tr>
        </c:if>
		<tr>
			<th nowrap="nowrap">
				Tanggal SPAJ 
			</th>
			<th class="left">
				<spring:bind path="cmd.tglSpaj">
					<script>
						inputDate('${status.expression}', 
						'${status.value}'
						, false, '', 9);
					</script>
				</spring:bind>
			</th>
			<th>
				<div align="right"><input <c:if test="${FLAG eq  1}">disabled</c:if> type="checkbox" name="pil2" onmouseover="return overlib('Silahkan di pilih untuk melakukan perubahan', AUTOSTATUS, WRAP);" onmouseout="nd();" class="noBorder"></div>
			</th>
		</tr>
		<tr>
			<th nowrap="nowrap">
				Cuti Premi 
			</th>
			<th class="left">
				<spring:bind path="cmd.dataUsulan.mspo_installment">
					 <input type="text" name="${status.expression}"  value="${status.value}" size="5" id="cuti" >
				</spring:bind>
			</th>
			<th>
				<div align="right"><input <c:if test="${FLAG eq  1}">disabled</c:if> type="checkbox" name="pil12" onmouseover="return overlib('Silahkan di pilih untuk melakukan perubahan', AUTOSTATUS, WRAP);" onmouseout="nd();" class="noBorder"></div>
			</th>
		</tr>
		<tr>
			<th width="15%">
               Status Pemegang
            </th>
             <th class="left">
                    <select name="status_mrt_pemegang" >
                        <c:forEach var="marital" items="${select_marital}">
                            <option value="${marital.ID}"
                                <c:if test="${cmd.pemegang.mspe_sts_mrt eq marital.ID }">Selected</c:if>
                                 value="${marital.ID}">${marital.MARITAL}</option>
                            </option>
                        </c:forEach>
                    </select>               
            </th>
            <th>
				<div align="right"><input <c:if test="${FLAG eq  1}">disabled</c:if> type="checkbox" name="pil13" onmouseover="return overlib('Silahkan di pilih untuk melakukan perubahan', AUTOSTATUS, WRAP);" onmouseout="nd();" class="noBorder"></div>
			</th>		
		</tr>
		<c:if test="${cmd.pemegang.lsre_id ne 1}" >  
		<tr>
			<th width="15%">
               Status Tertanggung
            </th>
             <th class="left">
                    <select name="status_mrt_ttg" >
                        <c:forEach var="marital" items="${select_marital}">
                            <option value="${marital.ID}"
                                <c:if test="${cmd.tertanggung.mspe_sts_mrt eq marital.ID }">Selected</c:if>
                                 value="${marital.ID}">${marital.MARITAL}</option>
                            </option>
                        </c:forEach>
                    </select>               
            </th>
            <th>
				<div align="right"><input <c:if test="${FLAG eq  1}">disabled</c:if> type="checkbox" name="pil14" onmouseover="return overlib('Silahkan di pilih untuk melakukan perubahan', AUTOSTATUS, WRAP);" onmouseout="nd();" class="noBorder"></div>
			</th>		
		</tr>
		</c:if>
		<c:if test="${cmd.pemegang.lsre_id ne 1}" >  
		<tr>
            <th width="15%">
               Hubungan Pemegang dan Tertanggung
            </th>
            <th class="left">
            
            <c:choose>
				<c:when test="${cmd.pemegang.lsre_id eq 1}">
            
                    <select name="relation" >
                        <c:forEach var="x" items="${select_relasi}">
                            <option value="${x.ID}"
                                <c:if test="${cmd.pemegang.lsre_id eq x.ID }">Selected</c:if>>
                                     ${x.RELATION}
                            </option>
                        </c:forEach>
                    </select>
                </c:when>
                <c:otherwise>
					<select name="relation" >
                        <c:forEach var="x" items="${select_relasi_non_sendiri}">
                            <option value="${x.ID}"
                                <c:if test="${cmd.pemegang.lsre_id eq x.ID }">Selected</c:if>>
                                     ${x.RELATION}
                            </option>
                        </c:forEach>
                    </select>
				</c:otherwise>
                </c:choose>
            </th>
            <th>
                <div align="right"><input <c:if test="${FLAG eq  1}">disabled</c:if> type="checkbox" name="pil8" onmouseover="return overlib('Silahkan di pilih untuk melakukan perubahan', AUTOSTATUS, WRAP);" onmouseout="nd();" class="noBorder"></div>
            </th>
      </tr>
      </c:if>
		<tr align="left" >
			<th colspan="2">
				<font color="#0033FF"> <strong><u>Hubungan Pemegang dan Tertanggung =${cmd.pemegang.lsre_relation}</u></strong></font>
			</th>
			<th></th>
		</tr>
	</table>
</fieldset>
</c:if>
<fieldset>
	<legend>Data Penerima manfaat</legend>
	<table align="left" class="entry2">
		<tr>
			<th width="5%">
				No
			</th>
			<th width="30%">
				Nama
			</th>
			<th width="10%">
				Tanggal Lahir
			</th>
			<th width="20%">
				Hubungan Dengan Calon Tertanggung
			</th>
			<th width="5%">
				%
			</th>
			<th width="20%">
				Jenis Kelamin
			</th>
			<th width="30%">
				<div align="right"><input type="checkbox" name="pil3" onmouseover="return overlib('Silahkan di pilih untuk melakukan perubahan', AUTOSTATUS, WRAP);" onmouseout="nd();" class="noBorder"></div>
			</th>
		</tr>
		<c:forEach var="s" items="${cmd.dataUsulan.daftabenef}" varStatus="xt">
			<tr>
				<th>
					<spring:bind path="cmd.dataUsulan.daftabenef[${xt.index}].msaw_number">
						${status.value}
					</spring:bind>
				</th>
				<th class="left" style="white-space: nowrap;">
					<spring:bind path="cmd.dataUsulan.daftabenef[${xt.index}].msaw_first">
						<input type="text" size="50" name="${status.expression}" value="${status.value}">
					</spring:bind>
				</th>
				<th class="left" style="white-space: nowrap;">
					<spring:bind path="cmd.dataUsulan.daftabenef[${xt.index}].smsaw_birth">
					<script>
						inputDate('${status.expression}','${status.value}', false, '', 9);
					</script>
					</spring:bind>
				</th>
				<th class="left" style="white-space: nowrap;">
					<select name="relasi" >
						<c:forEach var="x" items="${select_relasi}">
							<option value="${x.ID}"
								<c:if test="${s.lsre_id eq  x.ID }">Selected</c:if>>
									 ${x.RELATION}
							</option>
						</c:forEach>
					</select>
				</th>
				<th class="left" style="white-space: nowrap;">
					<spring:bind path="cmd.dataUsulan.daftabenef[${xt.index}].msaw_persen">
						<input type="text" size="3" maxLength="3" name="${status.expression}" value="${status.value}">
					</spring:bind>
				</th>
				<th>
				
						<select name="sex">
	                   	 <option <c:if test="${s.msaw_sex eq '0'}">SELECTED</c:if> value="0">Wanita</option> 
	           	       	 <option <c:if test="${s.msaw_sex eq '1'}">SELECTED</c:if> value="1">Laki-Laki</option> 
						</select> 
				</th>
				<th></th>
			</tr>
		</c:forEach>	
		<tr>
			<th  colspan="5">
				<div align="center">
					<input type="button" value="Add" name="Add" onClick="info_add()">
					<input type="button" value="Delete" name="Delete" onClick="info_delete()">
				</div>	
			</th>
			<th></th>
		</tr>
	</table>
</fieldset>
<c:if test="${cmd.flagEditAll eq 1}">
<fieldset>
	<legend>
		<c:if test="${cmd.pemegang.lsre_id ne 1}" >	
			Data Alamat Pemegang
		</c:if>	
		<c:if test="${cmd.pemegang.lsre_id eq 1}" >	
			Data Alamat Pemegang dan Tertanggung
		</c:if>	
	</legend>
	<table class ="entry2">
		<tr>
			<td>
				<table class="entry2">
					<tr>
						<th width="10%">Alamat Rumah</th>
						<th width="25%" class="left">
							<spring:bind path="cmd.pemegang.alamat_rumah">
								<textarea name="${status.expression}" rows="2" cols="45" >${status.value}</textarea>
							</spring:bind>	
						</th>
						<th  width="10%">Kode Pos
						</th>
						<th width="15%" class="left">	
							<spring:bind path="cmd.pemegang.kd_pos_rumah">
								<input type="text" name="${status.expression}" value="${status.value}" size="9" >
							</spring:bind>	
						</th>
						<th width="10%">Kota</th>
						<th width="15%" class="left">	
							<spring:bind path="cmd.pemegang.kota_rumah">
								<input type="text" name="${status.expression}" value="${status.value}" size="20" >
							</spring:bind>	
						</th>	
						<th width="15%">
							<div align="right"><input <c:if test="${FLAG eq  1}">disabled</c:if> type="checkbox" name="pil4" onmouseover="return overlib('Silahkan di pilih untuk melakukan perubahan', AUTOSTATUS, WRAP);" onmouseout="nd();" class="noBorder"></div>
						</th>
					</tr>
					<tr>
						<th>No. Telepon 1</th>
						<th class="left" style="white-space: nowrap;">	
							<spring:bind path="cmd.pemegang.area_code_rumah">
								<input type="text" size="5" maxLength="5" name="${status.expression}" value="${status.value }">
							</spring:bind>	
							<spring:bind path="cmd.pemegang.telpon_rumah">
								<input type="text" name="${status.expression}" value="${status.value }">
							</spring:bind>	
						</th>
						<th>No. Telepon 2</th>
						<th class="left" style="white-space: nowrap;">	
							<spring:bind path="cmd.pemegang.area_code_rumah2">
								<input type="text" size="5" maxLength="5" name="${status.expression}" value="${status.value }">
							</spring:bind>	
							<spring:bind path="cmd.pemegang.telpon_rumah2">
								<input type="text" name="${status.expression}" value="${status.value }">
							</spring:bind>	
						</th>
						<th>E-mail</th>
						<th colspan="2" class="left">	
							<spring:bind path="cmd.pemegang.email">
								<input type="text" name="${status.expression}" value="${status.value }">
							</spring:bind>	
						</th>
					</tr>
					<tr>
						<th>No. HandPhone 1</th>
						<th class="left" style="white-space: nowrap;">
							<spring:bind path="cmd.pemegang.no_hp">
								<input type="text" name="${status.expression}" value="${status.value }">
							</spring:bind>	
						</th>	
						<th>No. HandPhone 2</th>
						<th colspan="4" class="left" style="white-space: nowrap;">
							<spring:bind path="cmd.pemegang.no_hp2">
								<input type="text" name="${status.expression}" value="${status.value }">
							</spring:bind>	
						
						</th>	
					</tr>
					<tr>
						<th>Alamat Kantor</th>
						<th class="left" style="white-space: nowrap;">
							<spring:bind path="cmd.pemegang.alamat_kantor">
								<textarea name="${status.expression }" rows="2" cols="45" >${status.value}</textarea>
							</spring:bind>	
						</th>
						<th>Kode Pos
						</th>
						<th class="left" style="white-space: nowrap;">	
							<spring:bind path="cmd.pemegang.kd_pos_kantor">
								<input type="text" name="${status.expression}" value="${status.value}" size="9" >
							</spring:bind>	
						</th>
						<th>Kota</th>
						<th colspan="2" class="left" style="white-space: nowrap;">	
							<spring:bind path="cmd.pemegang.kota_kantor">
								<input type="text" name="${status.expression }" value="${status.value}" size="20" >
							</spring:bind>	
						</th>	
					</tr>
					<tr>
						<th>No. Telepon 1</th>
						<th class="left" style="white-space: nowrap;">	
							<spring:bind path="cmd.pemegang.area_code_kantor">
								<input type="text" size="5" maxLength="5" name="${status.expression}" value="${status.value }">
							</spring:bind>	
							<spring:bind path="cmd.pemegang.telpon_kantor">
								<input type="text" name="${status.expression}" value="${status.value }">
							</spring:bind>	
						</th>
						<th>No. Telepon 2</th>
						<th class="left" style="white-space: nowrap;">	
							<spring:bind path="cmd.pemegang.area_code_kantor2">
								<input type="text" size="5" maxLength="5" name="${status.expression}" value="${status.value }">
							</spring:bind>	
							<spring:bind path="cmd.pemegang.telpon_kantor2">
								<input type="text" name="${status.expression}" value="${status.value }">
							</spring:bind>	
						</th>
						<th>No. Fax</th>
						<th colspan="2" class="left" style="white-space: nowrap;">	
							<spring:bind path="cmd.pemegang.area_code_fax">
								<input type="text" size="5" maxLength="5" name="${status.expression}" value="${status.value }">
							</spring:bind>	
							<spring:bind path="cmd.pemegang.no_fax">
								<input type="text" name="${status.expression}" value="${status.value }">
							</spring:bind>	
						</th>
					</tr>
					<tr>
						<th width="10%">Alamat Penagihan</th>
						<th width="25%" class="left">
							<spring:bind path="cmd.addressbilling.msap_address">
								<textarea name="${status.expression}" rows="2" cols="45" >${status.value}</textarea>
							</spring:bind>	
						</th>
						<th  width="10%">Kode Pos
						</th>
						<th width="15%" class="left">	
							<spring:bind path="cmd.addressbilling.msap_zip_code">
								<input type="text" name="${status.expression}" value="${status.value}" size="9" >
							</spring:bind>	
						</th>
						<th width="10%">Kota</th>
						<th width="15%" class="left">	
							<spring:bind path="cmd.addressbilling.kota_tgh"><br>
								<input type="text" name="${status.expression }" value="${status.value}"  onfocus="this.select();"
								<c:if test="${ not empty status.errorMessage}">
										style='background-color :#FFE1FD'
								</c:if>>						
		    	 			<span id="indicator_tagih" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
							</spring:bind>
						</th>	
						<th width="15%">
							<div align="right"><input <c:if test="${FLAG eq  1}">disabled</c:if> type="checkbox" name="pil11" onmouseover="return overlib('Silahkan di pilih untuk melakukan perubahan', AUTOSTATUS, WRAP);" onmouseout="nd();" class="noBorder"></div>
						</th>
					</tr>
					<tr>
						<th>No. Telepon Penagihan 1</th>
						<th class="left" style="white-space: nowrap;">	
							<spring:bind path="cmd.addressbilling.msap_area_code1">
								<input type="text" size="5" maxLength="5" name="${status.expression}" value="${status.value }">
							</spring:bind>	
							<spring:bind path="cmd.addressbilling.msap_phone1">
								<input type="text" name="${status.expression}" value="${status.value }">
							</spring:bind>	
						</th>
						<th>No. Telepon Penagihan 2</th>
						<th class="left" style="white-space: nowrap;">	
						
							<spring:bind path="cmd.addressbilling.msap_area_code2">
								<input type="text" size="5" maxLength="5" name="${status.expression}" value="${status.value }">
							</spring:bind>	
							<spring:bind path="cmd.addressbilling.msap_phone2">
								<input type="text" name="${status.expression}" value="${status.value }">
							</spring:bind>	
						</th>
						<th>No. Telepon Penagihan 3</th>
						<th colspan="2" class="left" style="white-space: nowrap;">	
							<spring:bind path="cmd.addressbilling.msap_area_code3">
								<input type="text" size="5" maxLength="5" name="${status.expression}" value="${status.value }">
							</spring:bind>	
							<spring:bind path="cmd.addressbilling.msap_phone3">
								<input type="text" name="${status.expression}" value="${status.value }">
							</spring:bind>	
						</th>						
					</tr>
					<tr>
						<th>E-mail Penagihan</th>
						<th class="left">	
							<spring:bind path="cmd.addressbilling.e_mail">
								<input type="text" name="${status.expression}" value="${status.value }">
							</spring:bind>	
						</th>
						<th>No. Handphone Penagihan 1</th>
						<th class="left" style="white-space: nowrap;">								
							<spring:bind path="cmd.addressbilling.no_hp">
								<input type="text" name="${status.expression}" value="${status.value }">
							</spring:bind>	
						</th>	
						<th>No. Handphone Penagihan 2</th>
						<th colspan="2" class="left" style="white-space: nowrap;">	
							<spring:bind path="cmd.addressbilling.no_hp2">
								<input type="text"  name="${status.expression}" value="${status.value }">
							</spring:bind>								
						</th>									
					</tr>
					<tr>
						<th>Negara</th>
						<th class="left" colspan="4">
							<spring:bind path="cmd.addressbilling.lsne_id">
								<select name="${status.expression}">
									<c:forEach var="negara" items="${select_negara}">
									<option
										<c:if test="${cmd.addressbilling.lsne_id eq negara.ID}"> SELECTED </c:if>
											value="${negara.ID}">${negara.NEGARA}</option>
									</c:forEach>
									</select>								
							</spring:bind>
						</th>
					</tr>	
				</table>
			</td>
		</tr>		
	</table>	
</fieldset>
<c:if test="${cmd.pemegang.lsre_id ne 1}" >	
	<fieldset>
	<legend>Data Alamat Tertanggung</legend>
	<table class ="entry2">
		<tr>
			<td>
				<table class="entry2">
					<tr>
						<th width="10%">Alamat Rumah</th>
						<th width="25%" class="left" style="white-space: nowrap;">
							<spring:bind path="cmd.tertanggung.alamat_rumah">
								<textarea name="${status.expression}" rows="2" cols="45" >${status.value}</textarea>
							</spring:bind>	
						</th>
						<th  width="10%">Kode Pos
						</th>
						<th width="15%" class="left" style="white-space: nowrap;">	
							<spring:bind path="cmd.tertanggung.kd_pos_rumah">
								<input type="text" name="${status.expression}" value="${status.value}" size="9" >
							</spring:bind>	
						</th>
						<th width="10%">Kota</th>
						<th width="15%" class="left" style="white-space: nowrap;">	
							<spring:bind path="cmd.tertanggung.kota_rumah">
								<input type="text" name="${status.expression}" value="${status.value}" size="20" >
							</spring:bind>	
						</th>	
						<th width="15%">
							<div align="right"><input <c:if test="${FLAG eq  1}">disabled</c:if> type="checkbox" name="pil5" onmouseover="return overlib('Silahkan di pilih untuk melakukan perubahan', AUTOSTATUS, WRAP);" onmouseout="nd();" class="noBorder"></div>
						</th>
					</tr>
					<tr>
						<th>No. Telepon 1</th>
						<th class="left" style="white-space: nowrap;">	
							<spring:bind path="cmd.tertanggung.area_code_rumah">
								<input type="text" size="5" maxLength="5" name="${status.expression}" value="${status.value }">
							</spring:bind>	
							<spring:bind path="cmd.tertanggung.telpon_rumah">
								<input type="text" name="${status.expression}" value="${status.value }">
							</spring:bind>	
						</th>
						<th>No. Telepon 2</th>
						<th class="left" style="white-space: nowrap;">	
							<spring:bind path="cmd.tertanggung.area_code_rumah2">
								<input type="text" size="5" maxLength="5" name="${status.expression}" value="${status.value }">
							</spring:bind>	
							<spring:bind path="cmd.tertanggung.telpon_rumah2">
								<input type="text" name="${status.expression}" value="${status.value }">
							</spring:bind>	
						</th>
						<th>E-mail</th>
						<th colspan="2" class="left" style="white-space: nowrap;">	
							<spring:bind path="cmd.tertanggung.email">
								<input type="text" name="${status.expression}" value="${status.value }">
							</spring:bind>	
						</th>
					</tr>
					<tr>
						<th>No. HandPhone 1</th>
						<th class="left" style="white-space: nowrap;">
							<spring:bind path="cmd.tertanggung.no_hp">
								<input type="text" name="${status.expression}" value="${status.value }">
							</spring:bind>	
						</th>	
						<th>No. HandPhone 2</th>
						<th colspan="4" class="left" style="white-space: nowrap;">
							<spring:bind path="cmd.tertanggung.no_hp2">
								<input type="text" name="${status.expression}" value="${status.value }">
							</spring:bind>	
						
						</th>	
					</tr>
					<tr>
						<th>Alamat Kantor</th>
						<th class="left" style="white-space: nowrap;">
							<spring:bind path="cmd.tertanggung.alamat_kantor">
								<textarea name="${status.expression }" rows="2" cols="45" >${status.value}</textarea>
							</spring:bind>	
						</th>
						<th>Kode Pos
						</th>
						<th class="left" style="white-space: nowrap;">	
							<spring:bind path="cmd.tertanggung.kd_pos_kantor">
								<input type="text" name="${status.expression}" value="${status.value}" size="9" >
							</spring:bind>	
						</th>
						<th>Kota</th>
						<th colspan="2" class="left" style="white-space: nowrap;">	
							<spring:bind path="cmd.tertanggung.kota_kantor">
								<input type="text" name="${status.expression }" value="${status.value}" size="20" >
							</spring:bind>	
						</th>	
					</tr>
					<tr>
						<th>No. Telepon 1</th>
						<th class="left" style="white-space: nowrap;">	
							<spring:bind path="cmd.tertanggung.area_code_kantor">
								<input type="text" size="5" maxLength="5" name="${status.expression}" value="${status.value }">
							</spring:bind>	
							<spring:bind path="cmd.tertanggung.telpon_kantor">
								<input type="text" name="${status.expression}" value="${status.value }">
							</spring:bind>	
						</th>
						<th>No. Telepon 2</th>
						<th class="left" style="white-space: nowrap;">	
							<spring:bind path="cmd.tertanggung.area_code_kantor2">
								<input type="text" size="5" maxLength="5" name="${status.expression}" value="${status.value }">
							</spring:bind>	
							<spring:bind path="cmd.tertanggung.telpon_kantor2">
								<input type="text" name="${status.expression}" value="${status.value }">
							</spring:bind>	
						</th>
						<th>No. Fax</th>
						<th colspan="2" class="left" style="white-space: nowrap;">	
							<spring:bind path="cmd.tertanggung.area_code_fax">
								<input type="text" size="5" maxLength="5" name="${status.expression}" value="${status.value }">
							</spring:bind>	
							<spring:bind path="cmd.tertanggung.no_fax">
								<input type="text" name="${status.expression}" value="${status.value }">
							</spring:bind>	
						</th>
					</tr>
				</table>
			</td>
		</tr>	
	</table>	
</fieldset>
</c:if>	
</c:if>
<table align="left" class ="entry2">
	<tr align="center">
		<td>
			<input type="hidden" value="0" name="flag">
			<input type="button" name="save" value="Save" onClick="info_simpan();">
			<c:if test="${FLAG ne  1}">
				<div align="right"><Strong><i><a href="#" onClick="pilih_all();" onmouseover="return overlib('Check/Uncheck All', AUTOSTATUS, WRAP);" onmouseout="nd();">Check/Uncheck All </a></i>
				</Strong><input type="hidden" value="1" name="pilAll" ></div>
			</c:if>
		</td>
	</tr>
	<tr>
		<td>
			<spring:bind path="cmd.*">
				<c:if test="${not empty status.errorMessages}">
					<div id="error">
						Informasi:<br>
							<c:forEach var="error" items="${status.errorMessages}">
										- <c:out value="${error}" escapeXml="false" />
								<br/>
							</c:forEach>
					</div>
				</c:if>									
			</spring:bind>
		
		</td>
	</tr>	
</table>
</c:if>
</form>
<ajax:autocomplete
				  source="addressbilling.kota_tgh"
				  target="addressbilling.kota_tgh"
				  baseUrl="${path}/servlet/autocomplete?s=addressbilling.kota_tgh&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_tagih"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />	
