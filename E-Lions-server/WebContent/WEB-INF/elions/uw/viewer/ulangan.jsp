<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path}/include/js/disableKeys.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script>
	function tekan(i){
	//1==add ; 2==save
		if(i==3)
			window.location='${path}/uw/viewer/ulangan.htm?spaj=${cmd.spaj}'
		else{
			formpost.flag_ut.value=i;
			formpost.submit();
		}	
	}
</script>
<form:form id="formpost" name="formpost" commandName="cmd">
	<fieldset>
		<legend>Ulangan</legend>
		<table class="entry2">
			<tr>
				<th>Tanggal</th>
				<th>Jenis</th>
				<th>Status Polis</th>
				<th>User</th>
				<th>Keterangan</th>
			</tr>
			<c:forEach items="${cmd.lsUlangan}" var="x" varStatus="xt">
				<tr>	
					<td valign="top">
						<fmt:formatDate value="${x.tanggal}" pattern="dd/MM/yyyy"/>				
					</td>
					<td>
						<c:choose>
							<c:when test="${x.flagAdd eq null }">
								${x.jenis}
							</c:when>
							<c:otherwise>
								<form:textarea cols="35"  rows="5" path="lsUlangan[${xt.index}].jenis"/>
							</c:otherwise>	
						</c:choose>
					</td>
					<td valign="top">
						${x.lssp_status}
					</td>
					<td valign="top">${x.lus_full_name}</td>
					<td>
						<c:choose>
							<c:when test="${x.flagAdd eq null }">
								${x.keterangan}
							</c:when>
							<c:otherwise>
								<form:textarea cols="35"  rows="5" path="lsUlangan[${xt.index}].keterangan"/>
							</c:otherwise>	
						</c:choose>
					</td>
				</tr>
			</c:forEach>
			<tr>
				<th colspan="5">
					<c:if test="${bolehEdit eq true}">
						<input type="button" 
							<c:if test="${cmd.flagAdd eq 1 }">disabled</c:if>
							value="Add" name="btnAdd" onclick="tekan(1);" 
							accesskey='D' onmouseover="return overlib('Alt-D', AUTOSTATUS, WRAP);" onmouseout="nd();"
						>
						<input type="button" 
							<c:if test="${cmd.flagAdd ne 1 }">disabled</c:if>
							value="Save" name="btnSave" onclick="tekan(2);" 
							accesskey='V' onmouseover="return overlib('Alt-V', AUTOSTATUS, WRAP);" onmouseout="nd();"	
						>
						<input type="button" 
							value="Cancel" name="btnCancel" onclick="tekan(3);" 
							accesskey='C' onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();"	
						>
					</c:if>
					<form:hidden path="flag_ut"/>
				</th>
			</tr>
			
			<tr>
				<td colspan="5">
					<c:if test="${not empty submitSuccess }">
						<script>
							alert("Berhasil Simpan");
							window.location='${path}/uw/viewer/ulangan.htm?spaj=${cmd.spaj}'
						</script>
			        </c:if>	
		  			<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
								Informasi:<br>
									<c:forEach var="error" items="${status.errorMessages}">
												- <c:out value="${error}" escapeXml="false" />
										<br/>
									</c:forEach>
							</div>
						</c:if>									
					</spring:bind>
		        </td>
			</tr>
		</table>
	</fieldset>	
</form:form>