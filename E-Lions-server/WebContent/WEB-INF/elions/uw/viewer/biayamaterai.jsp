<%@ include file="/include/page/header_jquery.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path}/include/js/default.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (jQueryUI Tabs) init tab2 Utama
		$("#tabs").tabs();

		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title], label[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
	});
</script>


<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }
	
	/* untuk warna kolom input */
	.warna { color: grey; }
	
	/* styling untuk server-side validation error */
	.errorField { border: 1px solid red; }
	.errorMessage { color: red; display: block;}
</style>

<body>
	<form id="formPost" name="formPost" method="post" target="">
		<fieldset class="ui-widget ui-widget-content" id="fieldset1">
			<legend class="ui-widget-header ui-corner-all">
				<div>Proses Penggantian Biaya Materai</div>
			</legend>
			<table class="displaytag" id="tab1">
				<tr></tr>
				<tr></tr>
				<tr></tr>
				<tr>
					<th style="width: 200px;" align="left">Status Pengajuan</th>
					<th>:&nbsp;&nbsp;&nbsp;</th>
					<td><select name="jenis_report">
							<option value="5" label="All Status" />
							<option value="0" label="Belom Proses" />
							<option value="1" label="Proses Finance" />
							<option value="2" label="Sukses Transfer" />
							<option value="3" label="Gagal Transfer" />
							<select>
					</td>
				</tr>
				<tr>
					<th style="width: 200px;" align="left">Periode Pengajuan</th>
					<th>:&nbsp;&nbsp;&nbsp;</th>
					<td><input name="bdate" id="bdate" type="text"
						class="datepicker" title="Tanggal Awal" value="${bdate}"> s / d <input name="edate" id="edate" type="text"
						class="datepicker" title="Tanggal Akhir" value="${edate}">
					</td>
				</tr>
				<!-- <tr>
					<th style="width: 200px;" align="left">Nama Cabang</th>
					<th>:&nbsp;&nbsp;&nbsp;</th>
					<td><input name="filter" id="filter" type="text" title="filter">
					</td>
				</tr> -->
				<tr>
					<th></th>
					<th></th>
					<td><input type="submit" name="btnShow" id="btnShow"
						title="Tampilkan Polis" value="Tampilkan Data">
					</td>
				</tr>
				<table class="displaytag" cellpadding='2' cellspacing='0' border="1" align="center" style="width: 100%;">
			<tr>
				<th rowspan="2" bgcolor="#EAEAEA">Detail</th>
				<th rowspan="2" bgcolor="#EAEAEA">Cabang BSM/SMS</th>
				<th rowspan="2" bgcolor="#EAEAEA">Pemilik Rekening</th>
				<th rowspan="2" bgcolor="#EAEAEA">No Rekening</th>
				<th rowspan="2" bgcolor="#EAEAEA">Nominal Materai</th>
				<th rowspan="2" bgcolor="#EAEAEA">Verifikasi Cabang</th>
				<th colspan="2" bgcolor="#EAEAEA">Status</th>
				<th colspan="3" bgcolor="#EAEAEA">Tanggal Pengajuan</th>
				<th colspan="2" bgcolor="#EAEAEA">Tanggal Info</th>
				<th rowspan="2" bgcolor="#EAEAEA">Kirim</th>
				<th rowspan="2" bgcolor="#EAEAEA">Kirim Susulan</th>
				<th rowspan="2" bgcolor="#EAEAEA">Update </th>
			</tr>
			<tr>
				<th bgcolor="#FAFAFA">Sukses</th>
				<th bgcolor="#FAFAFA">Tolakan</th>
				<th bgcolor="#FAFAFA">BSM</th>
				<th bgcolor="#FAFAFA">Ke Finance</th>
				<th bgcolor="#FAFAFA">Ke Finance(Susulan)</th>
				<th bgcolor="#FAFAFA">Sukses</th>
				<th bgcolor="#FAFAFA">Tolakan</th>
			</tr>
			<c:forEach items="${result}" var="s">
			<%-- <input type="hidden" id="jenis_${s.JENIS }" name="lsjs_${s.JENIS }" value="${s.JENIS}">
			<input type="hidden" id="posisi_${s.MSF_POSISI }" name="posisi_${s.MSF_POSISI }" value="${s.MSF_POSISI}">
			<input type="hidden" id="email_${s.MSF_POSISI }" name="email_${s.MSF_POSISI }" value="${s.MSF_POSISI}"> --%>
				<c:choose>
					<c:when test="${s.MSF_POSISI eq 0}">
						<tr bgcolor="#FAFAFA" onMouseOver="Javascript:this.bgColor='#808080';return true;" onMouseOut="Javascript:this.bgColor='#FAFAFA';return true;">
					</c:when>
					<c:when test="${s.MSF_POSISI eq 1}">
						<tr bgcolor="#FFFF91" onMouseOver="Javascript:this.bgColor='#808080';return true;" onMouseOut="Javascript:this.bgColor='#FFFF91';return true;">
					</c:when>
					<c:when test="${s.MSF_POSISI eq 2}">
						<tr bgcolor="#B2E0FF" onMouseOver="Javascript:this.bgColor='#808080';return true;" onMouseOut="Javascript:this.bgColor='#B2E0FF';return true;">
					</c:when>
					<c:when test="${s.MSF_POSISI eq 3}">
						<tr bgcolor="#FF6C47" onMouseOver="Javascript:this.bgColor='#808080';return true;" onMouseOut="Javascript:this.bgColor='#FF6C47';return true;">
					</c:when>
				</c:choose>
				<td><img src="${path}/include/image/edit.gif" width="18"
					height="18" style="cursor: pointer;" border="0" title="Detail"
					onclick="popWin('${path}/uw/uw.htm?window=viewDetailbiayamaterai&no=${s.MSF_NO}', 500, 500);" />
				</td>
				<th><%-- (${s.MSF_CABANG }&nbsp;${s.JENIS}) --%> ${s.JENIS_BANK}</th>
				<th>${s.MSF_PIC }</th>
				<th>${s.MSF_NOREK }</th>
				<th>${s.MSF_NOMINAL }</th>
				<th>${s.MSF_CABANG }</th>
				<th><label for="r_flag"><input type="radio"
						class=noBorder name="s_${s.MSF_NO}" value="2" id="sukses_${s.MSF_NO}"
						<c:if test="${s.MSF_POSISI eq 2}">checked</c:if>>
				</label>
				</th>
				<th><label for="r_flag"><input type="radio"
						class=noBorder name="s_${s.MSF_NO}" value="3" id="tolak_${s.MSF_NO}" <c:if test="${s.MSF_POSISI eq 3}">checked</c:if>>
				</label>
				</th>
				<th>${s.MSF_DATE_VALIDASI}
				</th>
				<th>${s.MSF_DATE_SEND}
				</th>
				<th>${s.MSF_DATE_SEND2}
				</th>
				<th>${s.MSF_DATE_ACC}
				</th>
				<th>${s.MSF_DATE_DEC}
				</th>
				<th><input name="chbox" type="checkbox" id="chbox"
					value="${s.MSF_NO}" style="border: none;"  <c:if test="${s.MSF_POSISI ne 0}">disabled</c:if>>
				</th>
				<th><input name="chbox2" type="checkbox" id="chbox2"
					value="${s.MSF_NO}" style="border: none;" <c:if test="${s.MSF_POSISI ne 3}">disabled</c:if>>
				</th>
				<th><input name="chbox3" type="checkbox" id="chbox3"
					value="${s.MSF_NO}" style="border: none;" <c:if test="${s.MSF_POSISI eq 0 or s.MSF_POSISI eq 3 or s.MSF_POSISI eq 2}">disabled</c:if> <c:if test="${s.MSF_POSISI eq 3 or s.MSF_POSISI eq 2 }">checked</c:if>>
				</th>
				</tr>
			</c:forEach>
		</table>
			</table>
		</fieldset>
		<table cellpadding='0' cellspacing='0' align="left">
			<tr>
				<th></th>
				<td><br /></td>
			</tr>
			<tr>
				<th><input type="submit" name="kirim" id="kirim" title="Kirim Ke Finance" value="Kirim Ke Finance" <c:if test="${status eq 1 or status eq 2 or status eq 3}">disabled</c:if>>
				<th><input type="submit" name="kirim2" id="kirim2"title="Kirim Susulan Ke Finance" value="Kirim Susulan Ke Finance" <c:if test="${status eq 1 or status eq 2}">disabled</c:if>></th>
				<th><input type="submit" name="update" id="update"title="Update" value="Update" <c:if test="${status eq 0 or status eq 2 or status eq 3 or status eq 5}">disabled</c:if>></th>
			</tr>
		</table>
		<br><br><br>
			<table class="displaytag" cellpadding='4' cellspacing='0' border="1"
			align="right" style="width: 50%;">
			<th bgcolor="#FAFAFA">Belum Diproses</th>
			<th bgcolor="#FFFF91">Proses Finance</th>
			<th bgcolor="#B2E0FF">Sukses Transfer</th>
			<th bgcolor="#FF6C47">Gagal Transfer</th>
			</table>
	</form>
</body>
<c:if test="${not empty pesan}">
	<script>alert('${pesan}');</script>
</c:if>
</html>
