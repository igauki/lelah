<%@ include file="/include/page/taglibs.jsp"%>

<fieldset>
	<legend>Billing</legend>

	<table class="entry2">
		<tr>					
			<th nowrap="nowrap">Posisi Dokumen</th>
			<th><input readonly type="text" size="40" value="${cmd.docPos.LSPD_POSITION}"></th>
			<th nowrap="nowrap">Tgl Next Bill</th>
			<th nowrap="nowrap">
				<input readonly type="text" size="14" value="<fmt:formatDate value="${cmd.docPos.MSPO_NEXT_BILL}" pattern="dd-MM-yyyy"/>">
			</th>
		</tr>
		<tr>
			<th nowrap="nowrap">Status Polis</th>
			<th><input readonly type="text" size="40" value="${cmd.docPos.LSSP_STATUS}"></th>
			<th nowrap="nowrap">Flag Proses Bill</th>
			<th nowrap="nowrap"><input class="noBorder" type="checkbox" disabled <c:if test="${cmd.docPos.MSPO_PROSES_BILL eq 1}" >checked</c:if>></th>
		</tr>
	</table>

	<c:if test="${not empty cmd.billing}">
		<display:table id="bill" name="cmd.billing" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator">
			<display:column property="TIPE" title="Jenis" />
			<display:column property="TAHUNKE" title="Thn Ke" />
			<display:column property="PREMIKE" title="Premi" />
			<display:column property="BEGDATE" title="Beg Date" format="{0, date, dd/MM/yyyy}" />
			<display:column property="ENDDATE" title="End Date" format="{0, date, dd/MM/yyyy}" />
			<display:column property="DUEDATE" title="Due Date" format="{0, date, dd/MM/yyyy}" />
			<display:column property="KURS" title="Kurs" />
			<display:column property="TOTAL8" title="Sisa Tagihan" format="{0, number, #,##0.00;(#,##0.00)}" total="true" />
			<display:column title="Paid">
				<input class="noBorder" type="checkbox" disabled <c:if test="${bill.PAID eq 1}" >checked</c:if>>
			</display:column>
			<display:column title="Print">
				<input class="noBorder" type="checkbox" disabled <c:if test="${bill.PRINT eq 1}" >checked</c:if>>
			</display:column>
			<display:column property="KPL" title="No. KPL" />
			<display:column property="LSPD_ID" title="Pos" />
			<display:column property="PERSEN" title="%" />
			<display:column property="MSPA_DATE_BOOK" title="Tgl Rk" format="{0, date, dd/MM/yyyy}" />
			<display:column property="MSPA_INPUT_DATE" title="Tgl Input" format="{0, date, dd/MM/yyyy}" />
			<display:column property="MSPA_PAYMENT" title="Jumlah Bayar"/>
		</display:table>		
	</c:if>

</fieldset>