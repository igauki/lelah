<%@ include file="/include/page/taglibs.jsp"%>

<table class="entry2">
	<tr>
		<td>
			<%--<input type="button" name="btnAccount" value="Rekening Pemegang Polis"
			onclick="popWin('${pageContext.request.contextPath}/uw/viewer.htm?window=rekening&spaj=${param.showSPAJ}', 400, 700);">--%>
			<b>Rekening Billing &nbsp;&nbsp;:&nbsp;&nbsp;<input type="text" readonly="readonly" size="25" value=" ${cmd.tertanggung.mste_no_vacc}"><br/>
			<font style="color: red;">(Billing Account)</font></b>
		</td>
	</tr>
</table>

<c:choose>

	<c:when test="${not empty cmd.bukanUnitLink}">
		<div id="error">
			Produk ini bukan produk Unit-link maupun Powersave
		</div>
	</c:when>
	
	<c:otherwise>
		
		<c:choose>
			<c:when test="${not empty cmd.stableLink}">
				<table class="displaytag">
				<thead>
					<tr>
						<th>Manfaat Bulanan</th>
						<th>Transaksi</th>
						<th>Periode</th>
						<th>Posisi</th>
						<th>MTI</th>
						<th>Rate(%)</th>
						<th>Rollover</th>
						<th>Jml Hari</th>
						<th>Kurs</th>
						<th>Premi</th>
						<th>Bunga</th>
						<th>BP</th>
						<th>Tambahan Hari</th>
						<th>Rekening</th>
					</tr>
				</thead>
				<tbody>
					<c:set var="tmp" value="" />
					<c:forEach items="${cmd.stableLink}" var="s">
						<tr>
							<td class="center">
								<c:choose>
									<c:when test="${s.MANF_BUL eq 'Ya'}"><a target="_blank" href="#" onclick="popWin('${path}/uw/viewer.htm?window=slinkbayar&spaj=${s.REG_SPAJ}&msl_no=${s.MSL_NO}', 600, 1000); return false;">${s.MANF_BUL}</a></c:when>
									<c:otherwise>${s.MANF_BUL}</c:otherwise>
								</c:choose>
							</td>
							<td class="left">
							<c:choose>
								<c:when test="${tmp ne s.MSL_TU_KE}">
									<c:set var="tmp" value="${s.MSL_TU_KE}" />
									<span style="font-weight: bold; color: red;">${s.MSL_DESC}</span>
								</c:when>
								<c:otherwise>${s.MSL_DESC}</c:otherwise>
							</c:choose>
							</td>
							<td class="center"><fmt:formatDate value="${s.MSL_BDATE}" pattern="dd-MM-yyyy"/> s/d <fmt:formatDate value="${s.MSL_EDATE}" pattern="dd-MM-yyyy"/></td>
							<td class="left">${s.POSISI}</td>
							<td>${s.MSL_MGI} bln</td>
							<td><fmt:formatNumber value="${s.MSL_RATE}" maxFractionDigits="2" minFractionDigits="2" /></td>
							<td class="left">${s.RO}</td>
							<td>${s.MSL_HARI }</td>
							<td class="left">${s.KURS}</td>
							<td><fmt:formatNumber value="${s.MSL_PREMI}" maxFractionDigits="2" minFractionDigits="2" /></td>
							<td><fmt:formatNumber value="${s.MSL_BUNGA}" maxFractionDigits="2" minFractionDigits="2" /></td>
							<td><fmt:formatNumber value="${s.MSL_BP}" maxFractionDigits="2" minFractionDigits="2" /></td>
							<td class="left">${s.TAMBAHAN}</td>
							<td>${s.REK}</td>
						</tr>
					</c:forEach>
				</tbody>
				</table>                                                                                                                                          			
				<br/><span class="info">(*) Khusus Transaksi Manfaat Bulanan, silahkan click pada kolom "Manfaat Bulanan" bersangkutan untuk melihat detail transaksi bulanan.</span>                                                                                                                       			
			</c:when>
			
			<c:when test="${not empty cmd.unitLink}">

				<fieldset>
					<legend>Premi</legend>
					<display:table id="premi" name="cmd.daftarPremi" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator">
						<display:column property="MU_KE" title="Premi Ke" style="text-align: left;" />
						<display:column property="LT_TRANSKSI" title="Premi" style="text-align: left;" />
						<display:column property="MU_JLH_PREMI" title="Jumlah" format="{0, number, #,##0.00;(#,##0.00)}" total="true" />
						<display:column property="MU_TGL_NAB" title="Tanggal NAB" format="{0, date, dd/MM/yyyy}" style="text-align: center;" />
						<display:column property="MU_TGL_SURAT" title="Tanggal Surat" format="{0, date, dd/MM/yyyy}" style="text-align: center;" />
						<display:column property="MU_BULAN_SURAT" title="Frekuensi Surat (bulan)" style="text-align: center;" />
						<display:column property="LSPD_POSITION" title="Posisi" style="text-align: left;" />
					</display:table>
				</fieldset>
				
			</c:when>
		
			<c:when test="${not empty cmd.powerSave}">
				<fieldset>
					<legend>Informasi Produk PowerSave</legend>
					<display:table id="ps" name="cmd.powerSave" class="displaytag">
						<display:column title="Jangka Waktu" style="width: 100px; text-align: center;">
							<select disabled>
				                <c:forEach var="jk" items="${select_jangka_invest}">
				                	<option <c:if test="${ps.mps_jangka_inv eq jk.ID}"> SELECTED </c:if> value="${jk.ID}">${jk.JANGKAWAKTU}</option>
				                </c:forEach> 
							</select>
						</display:column>
						<display:column property="mps_batas_date" title="Tanggal Jatuh Tempo" format="{0, date, dd/MM/yyyy}"/>
						<display:column property="mps_prm_deposit" title="Jumlah Investasi" format="{0, number, #,##0.00;(#,##0.00)}" />
						<display:column title="Jenis Roll-over" style="width: 200px; text-align: center;">
							<select disabled>
								<c:forEach var="roll" items="${select_rollover}">
									<option <c:if test="${ps.mps_roll_over eq roll.ID}"> SELECTED </c:if> value="${roll.ID}">${roll.ROLLOVER}</option>
								</c:forEach> 
							</select>
						</display:column>
					</display:table>
				</fieldset>	
			</c:when>
		</c:choose>
	</c:otherwise>
</c:choose>