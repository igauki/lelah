<%@ include file="/include/page/header.jsp"%><c:set var="path" value="${pageContext.request.contextPath}" />
<body style="height: 100%;">
	<div id="contents">
		<c:choose>
			<c:when test="${param.tipeView eq 0}">
					  <c:set var="widthTable" value=""/>
                    <c:choose>
                        <c:when test="${pusatOrCabang eq 'pusat'}">
                            <c:set var="widthTable" value="1700px"/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="widthTable" value="1200px"/>
                        </c:otherwise>
                    </c:choose>
                    
				<display:table style="width:${widthTable};" id="baris" name="daftar" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}" export="true" pagesize="20">                                                         
					<display:caption>Pinjaman</display:caption>
					<display:column property="MSPIN_PINJAMAN_KE" title="KE"  sortable="true"/>                                                                    
					<display:column property="NO_PINJAMAN" title="NO PINJAMAN"  sortable="true"/>                                                                                
					<display:column property="TGL_INPUT" title="TGL INPUT"  sortable="true"/>                                                                                    
					<display:column property="BEG_DATE" title="BEG DATE"  sortable="true"/>                                                                                      
					<display:column property="END_DATE" title="END DATE"  sortable="true"/>                                                                                      
					<display:column property="MSPIN_PINJAMAN" title="JUMLAH" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                
					<display:column property="MSPIN_AKTIF" title="AKTIF"  sortable="true"/>                                                                                
					<c:if test="${pusatOrCabang eq 'pusat'}"> 
					<display:column property="RATE" title="RATE" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                                    
					<display:column property="BUNGA" title="BUNGA"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                                    
					</c:if>
					<display:column property="ROLLOVER_KE" title="ROLLOVER KE"  sortable="true"/>   
					<c:if test="${pusatOrCabang eq 'pusat'}">                                                                              
					<display:column property="MSPIN_PAID" title="PAID"  sortable="true"/>                                                                                  
					<display:column property="MSPIN_TGL_TRANS" title="TGL TRANSFER"  sortable="true"/>  
					<display:column property="PINJAMAN_AKHIR" title="PINJAMAN AKHIR"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>     
					<display:column property="MSBAP_BUNGA" title="BUNGA BAYAR"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                                                                                                        
					<display:column property="MSBAP_JUMLAH" title="BAYAR"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/> 
    				<display:column property="MSBAP_SISA" title="SISA BAYAR"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/> 
					<display:column property="MSPIN_TGL_BAYAR" title="TGL BAYAR"  sortable="true"/>    
					<display:column property="CARA" title="BAYAR DENGAN"  sortable="true"/>  
					<display:column property="MSBAP_TGL_INPUT" title="TGL INPUT"  sortable="true"/>  
					<display:column property="MSPIN_JLH_NT" title="NILAI TUNAI"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                                                                                                        
					<display:column property="BENEFIT_DAY" title="MANF.TAMBAHAN(HR)"  sortable="true"/>  
					<display:column property="MSPIN_JLH_BUNGA" title="NILAI MANF.TAMBAHAN"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                                                                                                        
                    <display:column property="KETERANGAN" title="KETERANGAN"  sortable="true"/>  
                    </c:if>
				</display:table>
			</c:when>
			<c:when test="${param.tipeView eq 1}">
		<c:set var="widthTable" value=""/>
                    <c:choose>
                        <c:when test="${pusatOrCabang eq 'pusat'}">
                            <c:set var="widthTable" value="1800px"/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="widthTable" value="1200px"/>
                        </c:otherwise>
                    </c:choose>
				<display:table style="width:${widthTable};" id="baris" name="daftar" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}" export="true" pagesize="20">                                                         
					<display:caption>Simpanan</display:caption>
					<display:column property="MSSIM_NO_DEPOSITO" title="NO DEPOSITO"  sortable="true"/>                                                                    
					<display:column property="MSSIM_NO_REG" title="NO REGISTER"  sortable="true"/>      
					<c:if test="${pusatOrCabang eq 'pusat'}">                                                                        
					<display:column property="MSSIM_JUMLAH" title="JUMLAH" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                    
					<display:column property="MSSIM_PERSEN" title="PERSEN"  sortable="true"/>      
					</c:if>                                                                        
					<display:column property="BEG_DATE" title="BEG DATE"  sortable="true"/>                                                                                      
					<display:column property="END_DATE" title="END DATE"  sortable="true"/>                                                                                      
					<display:column property="TGL_INPUT" title="INPUT"  sortable="true"/>                                                                                    
					<display:column property="TGL_KLAIM" title="TGL KLAIM"  sortable="true"/>                                                                                    
					<display:column property="TGL_TRANS" title="TGL TRANSFER"  sortable="true"/>                                                                                    
					<display:column property="LSCSI_CLAIM" title="JENIS KLAIM"  sortable="true"/>      
					<c:if test="${pusatOrCabang eq 'pusat'}">                                                                          
					<display:column property="MBS_JUMLAH" title="JML BAYAR" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                        
					<display:column property="MBS_DESC" title="KETERANGAN"  sortable="true"/>                                                                                      
					<display:column property="NO_TAHAPAN" title="NO TAHAPAN"  sortable="true"/>                                                                                  
					<display:column property="MSSIM_OLD_DEPOSITO" title="OLD DEPOSITO"  sortable="true"/>                                                                  
					</c:if>
				</display:table>
			</c:when>
			<c:when test="${param.tipeView eq 2}">
				<display:table style="width:1600px;" id="baris" name="daftar" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}" export="true" pagesize="20">                                                         
					<display:caption>Billing</display:caption>
					<display:column title="SHOW"  sortable="true">
						<input type="button" value="Pay" onclick="alert('Maaf tetapi menu ini masih dalam tahap pengembangan');">
						<input type="button" value="HCR" onclick="alert('Maaf tetapi menu ini masih dalam tahap pengembangan');">
					</display:column>
					<display:column property="PAID" title=" "  sortable="true"/>                                                                                              
					<display:column property="MSBI_TAHUN_KE" title="THN"  sortable="true"/>                                                                            
					<display:column property="MSBI_PREMI_KE" title="PRM KE"  sortable="true"/>                                                                            
					<display:column property="BEG_DATE" title="BEG DATE"  sortable="true"/>                                                                                      
					<display:column property="END_DATE" title="END DATE"  sortable="true"/>                                                                                      
					<display:column property="MSBI_BILL_NO" title="NO KPL"  sortable="true"/>                                                                              
					<display:column property="LKU_SYMBOL" title="CURR"  sortable="true"/>                                                                                  
					<display:column property="PREMI_STD" title="PRM STD"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                        
					<display:column property="PREMI_RIDER" title="PRM RIDER"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                        
					<display:column property="PREMI_EXTRA" title="PRM EXTRA"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                        
					<display:column property="PREMI_HCR" title="PRM HCR"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                        
					<display:column property="DISCOUNT" title="DISCOUNT"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                        
					<display:column property="MSBI_STAMP" title="STAMP" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                        
					<display:column property="TOTAL_TAGIH" title="TOTAL TAGIH"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                        
					<display:column property="MSPA_DATE_BOOK" title="TGL RK"  sortable="true"/>                                                                          
					<display:column property="MSPA_INPUT_DATE" title="TGL INPUT"  sortable="true"/>                                                                        
					<display:column property="MSPA_NO_VOUCHER" title="VOUCHER"  sortable="true"/>                                                                        
					<display:column property="MSPA_PAYMENT" title="JML BAYAR" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                    
					<display:column property="PROD_DATE" title="TGL PROD"  sortable="true"/>                                                                                    
					<display:column property="MSPRO_NILAI_KURS" title="NILAI KURS"  sortable="true"/>                                                                      
					<display:column property="PRINT" title="PRINT"  sortable="true"/>                                                                                            
					<display:column property="LUS_LOGIN_NAME" title="USER INPUT"  sortable="true"/>                                                                          
					<display:column property="USER_BILLING" title="USER BILLING"  sortable="true"/>           
					<display:column property="LSPD_POSITION" title="POSISI"  sortable="true"/>                                                                            
					<display:column property="MSBI_INPUT_DATE" title="TGL PROSES"  sortable="true"/>                                                                        
					<display:column property="MSBI_ADD_BILL" title="ADD"  sortable="true"/>                                                                            
				</display:table>
			</c:when>
			<c:when test="${param.tipeView eq 3}">
		<c:set var="widthTable" value=""/>
                    <c:choose>
                        <c:when test="${pusatOrCabang eq 'pusat'}">
                            <c:set var="widthTable" value="1500px"/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="widthTable" value="1200px"/>
                        </c:otherwise>
                    </c:choose>
				<display:table style="width:${widthTable};" id="baris" name="daftar" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}" export="true" pagesize="20">                                                         
					<display:caption>Tahapan</display:caption>
					<display:column property="MSTAH_TAHAPAN_KE" title="KE"  sortable="true"/>                                                                      
					<display:column property="NO_TAHAPAN" title="NO TAHAPAN"  sortable="true"/>   
					<c:if test="${pusatOrCabang eq 'pusat'}">                                                                                 
					<display:column property="BEG_AKTIF" title="BEG AKTIF"  sortable="true"/>                                                                                    
					</c:if>
					<display:column property="MSTAH_JUMLAH" title="JUMLAH" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                    
					<c:if test="${pusatOrCabang eq 'pusat'}">  
					<display:column property="MSTAH_PERSEN" title="PERSEN"  sortable="true"/>                                                                              
					</c:if>
					<display:column property="JATUH_TEMPO" title="JATUH TEMPO"  sortable="true"/>     
					<c:if test="${pusatOrCabang eq 'pusat'}">                                                                             
					<display:column property="TGL_PROSES" title="TGL PROSES"  sortable="true"/>                                                                                  
					<display:column property="TGL_PRINT" title="TGL PRINT"  sortable="true"/>                                                                                    
					</c:if>
					<display:column property="TGL_KONF" title="TGL KLAIM"  sortable="true"/>                                                                                      
					<display:column property="TGL_TRANS" title="TGL TRANSFER"  sortable="true"/>                                                                                    
					<display:column property="LSJTA_TYPE" title="JENIS KLAIM"  sortable="true"/>                                                                                  
					<display:column property="MSBAT_JUMLAH" title="JUMLAH BAYAR" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                    
					<c:if test="${pusatOrCabang eq 'pusat'}">
					<display:column property="BEG_DATE_BILL" title="BEG BILL"  sortable="true"/>     
					<display:column property="MSBAT_DESC" title="KETERANGAN"  sortable="true"/>                                                                                  
					</c:if>
				</display:table>
			</c:when>
			<c:when test="${param.tipeView eq 4}">
			<c:set var="widthTable" value=""/>
                    <c:choose>
                        <c:when test="${pusatOrCabang eq 'pusat'}">
                            <c:set var="widthTable" value="1200px"/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="widthTable" value="1000px"/>
                        </c:otherwise>
                    </c:choose>
				<display:table style="width:${widthTable};" id="baris" name="daftar" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}" export="true" pagesize="20">                                                         
					<display:caption>Claim Nilai Tunai</display:caption>
					<display:column property="MSSUR_TAHUN_KE" title="THN KE"  sortable="true"/>                                                                          
					<display:column property="MSSUR_NO_SURR" title="NO CLAIM"  sortable="true"/>                                                                            
					<display:column property="TGL_KLAIM" title="TGL KLAIM"  sortable="true"/>   
					<c:if test="${pusatOrCabang eq 'pusat'}">                                                                                  
					<display:column property="TGL_AKTIF" title="TGL AKTIF"  sortable="true"/>                                                                                    
					</c:if>
					<display:column property="TGL_INPUT" title="TGL INPUT"  sortable="true"/>    
					<c:if test="${pusatOrCabang eq 'pusat'}">                                                                                  
					<display:column property="MSSUR_JML_NT" title="JUMLAH" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                    
					<display:column property="MSSUR_JML_BONUS" title="BONUS" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                              
					<display:column property="MSSUR_ALASAN" title="ALASAN"  sortable="true"/>                                                                              
					<display:column property="TGL_PROD" title="TGL PROD"  sortable="true"/>         
					</c:if>                                                                             
					<display:column property="LSCCN_CARA" title="CARA BAYAR"  sortable="true"/>                                                                                  
					<c:if test="${pusatOrCabang eq 'pusat'}">  
					<display:column property="MSCLN_JUMLAH" title="KLAIM BAYAR" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                    
					<display:column property="MSCLN_DESC" title="KETERANGAN"  sortable="true"/>                                                                                  
					</c:if>
				</display:table>
			</c:when>
			<c:when test="${param.tipeView eq 5}">
						<c:set var="widthTable" value=""/>
                    <c:choose>
                        <c:when test="${pusatOrCabang eq 'pusat'}">
                            <c:set var="widthTable" value="1800px"/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="widthTable" value="1200px"/>
                        </c:otherwise>
                    </c:choose>
				<display:table style="width:${widthTable};" id="baris" name="daftar" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}" export="true" pagesize="20">                                                         
					<display:caption>Powersave</display:caption>
					<display:column property="NOMOR" title="NO"  sortable="true"/>                                                                                            
					<display:column property="PERIODE" title="PERIODE"  sortable="true"/>                                                                                        
					<display:column property="MATURITY" title="MATURITY DATE"  sortable="true"/>                                                                                      
					<display:column property="JANGKA_INVEST" title="JANGKA INVEST"  sortable="true"/>  
					<c:if test="${pusatOrCabang eq 'pusat'}">
					<display:column property="MPR_NETT_TAX" title="JNS RATE"  sortable="true"/>					   
					<display:column property="MPR_RATE" title="RATE %" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                            
					<display:column property="RATE_DATE" title="RATE DATE"  sortable="true"/>  
					<display:column property="MPR_TGL_CONFIRM" title="CONFIRM DATE"  sortable="true"/>                                                                                    
					</c:if>
					<display:column property="LKU_SYMBOL" title="KURS"  sortable="true"/>                                                                                  
					<c:if test="${pusatOrCabang eq 'pusat'}">       
					<display:column property="MPR_DEPOSIT" title="PRM DEPOSIT" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                               
					<display:column property="MPR_INTEREST" title="PRM INTEREST" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                    
					<display:column property="MPR_DEBIT" title="PRM DEBET"  sortable="true"/>                                                                                    
					<display:column property="MPR_INSURANCE" title="PRM INSURANCE" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                  
					<display:column property="MPR_TAX" title="TAX" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                              
					<display:column property="MPR_DEBET" title="PRM DIBAYAR" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>   
					</c:if>                                       
					<display:column property="MPR_JNS_RO" title="ROLL OVER"  sortable="true"/>  
					<display:column property="PRINT_DATE" title="RO PRINT DATE"  sortable="true"/>  
					<display:column property="TRANSAKSI" title="TRANSAKSI"  sortable="true"/>  
					<c:if test="${pusatOrCabang eq 'pusat'}">
					<display:column property="MPR_FLAG_COMM" title="KOMISI LANJUTAN"  sortable="true"/>  
					<display:column property="JENIS_TAX" title="JENIS TAX"  sortable="true"/>    
					<display:column property="PRM_INTEREST" title="BAYAR PRM INTEREST"  sortable="true"/>  
					<display:column property="MPR_HARI" title="JUM HARI"  sortable="true"/>  
					<display:column property="MPR_JNS_RUMUS" title="JNS RUMUS"  sortable="true"/>  
					<display:column property="NOTE" title="NOTE"  sortable="true"/> 
					<display:column property="NO_REKENING" title="NO REKENING" sortable="true"/> 
					<display:column property="USER_NAME" title="USER" sortable="true"/> 
					</c:if>
					<display:column property="PROSES_DATE" title="PROSES DATE" sortable="true"/> 
				</display:table>
			</c:when>
			<c:when test="${param.tipeView eq 7}">
				<display:table style="width:700px;" id="baris" name="daftar" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}" export="true" pagesize="20">                                                         
					<display:caption>Agent</display:caption>
					<display:column property="MSAG_ID" title="NO. AGENT"  sortable="true"/>                                                                                        
					<display:column property="NAMA" title="NAMA AGENT"  sortable="true"/>                                                                                              
					<display:column property="LEPEL" title="LEVEL"  sortable="true"/>                                                                                            
					<display:column property="STATUS" title="AKTIF"  sortable="true"/>                                                                                          
					<display:column property="KOMISI" title="KOMISI"  sortable="true"/>                                                                                          
				</display:table>

			</c:when>
			<c:when test="${param.tipeView eq 8}">
				<display:table style="width:1500px;" id="baris" name="daftar" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}" export="true" pagesize="20">                                                         
					<display:caption>Reas</display:caption>
					<display:column property="POLIS" title="POLICY NO"  sortable="true"/>                                                                                            
					<display:column property="SERTIFIKAT" title="CERTIFICATE NO"  sortable="true"/>                                                                                  
					<display:column property="PP" title="POLICY HOLDER"  sortable="true"/>                                                                                                  
					<display:column property="TT" title="INSURED NAME"  sortable="true"/>                                                                                                  
					<display:column property="REINSURER" title="REINSURER"  sortable="true"/>                                                                                    
					<display:column property="MEDIS" title="MEDIC"  sortable="true"/>                                                                                            
					<display:column property="BEG_DATE" title="BEGIN DATE"  sortable="true"/>                                                                                      
					<display:column property="END_DATE" title="END DATE"  sortable="true"/>                                                                                      
					<display:column property="MSRE_BUNGA_KPR" title="KPR"  sortable="true"/>                                                                          
					<display:column property="MSRPR_PA_CLASS" title="CLASS"  sortable="true"/>                                                                          
					<display:column property="MSRPR_PA_RISK" title="RISK"  sortable="true"/>                                                                            
					<display:column property="LSPD_POSITION" title="DOCUMENT"  sortable="true"/>                                                                            
					<display:column property="LKU_SYMBOL" title="CURR"  sortable="true"/>                                                                                  
					<display:column property="LSBS_ID" title="BUSINESSID"  sortable="true"/>                                                                                        
					<display:column property="MSRPR_SIMULTAN" title="SIMULTAN" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                
					<display:column property="MSRPR_TSI" title="TSI" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                          
					<display:column property="MSRPR_RESIKO_AWAL" title="RESIKO AWAL" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                          
					<display:column property="MSRPR_RETENSI" title="RETENSI" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                  
					<display:column property="MSRPR_TSI_REAS" title="TSI REAS" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                
					<display:column property="TGL_RECAP" title="TGL RECAP" sortable="true"/>                                 
				</display:table>
			</c:when>
			<c:when test="${param.tipeView eq 9}">
				<display:table style="width:1500px;" id="baris" name="daftar" class="displaytag" defaultsort="4" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}" export="true" pagesize="20">                                                         
					<display:caption>Akumulasi Polis</display:caption>
					<display:column property="A" title="POLICY HOLDER"  sortable="true"/>                                                                                                    
					<display:column property="B" title="INSURED NAME"  sortable="true"/>                                                                                                    
					
					<display:column title="No Polis" sortable="true">
						<a href="javascript:popWin('${path}/uw/detailsimultan_new.htm?spajAwal=${baris.M}&flag_tt=0', 400, 800,1,0) ;">${baris.C}</a> 
					</display:column>                                                                                      
					                                                                                                    
					<display:column title="PRODUK" style="text-align: left;">
						[${baris.O}-${baris.U}] ${baris.V}
					</display:column>
					<display:column property="D" title="KURS"  sortable="true" group="1" />
					<display:column property="E" title="TSI" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true" total="true"/>
					<display:column property="F" title="SAR"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true" total="true"/>
					<display:column property="W" title="RETENSI"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true" total="true"/>
					<display:column property="G" title="PREMI STD" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                                          
					<display:column property="H" title="RIDER"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                                          
					<display:column property="I" title="EXTRA"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                                          
					<display:column property="J" title="BEG DATE"  sortable="true"/>                                                                                                    
					<display:column property="K" title="END DATE"  sortable="true"/>                                                                                                    
					<display:column property="L" title="SP"  sortable="true"/>                                                                                                    
					<display:column property="N" title="TAHUN KE"  sortable="true"/>                                                                                                    
				</display:table>
			</c:when>
			<c:when test="${param.tipeView eq 10}">
			<c:set var="widthTable" value=""/>
                    <c:choose>
                        <c:when test="${pusatOrCabang eq 'pusat'}">
                            <c:set var="widthTable" value="1500px"/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="widthTable" value="1000px"/>
                        </c:otherwise>
                    </c:choose>
				<display:table style="width:${widthTable};" id="baris" name="daftar" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}" export="true" pagesize="20">                                                         
					<display:caption>Maturity</display:caption>
					<c:if test="${pusatOrCabang eq 'pusat'}">
					<display:column property="NO_KLAIM" title="NO KLAIM"  sortable="true"/>                                                                                      
					</c:if>
					<display:column property="NO_POLIS" title="NO POLIS"  sortable="true"/>                                                                                      
					<display:column property="TAHUN_KE" title="THN KE"  sortable="true"/>                                                                                      
					<display:column property="TGL_TEMPO" title="JATUH TEMPO"  sortable="true"/>                                                                                    
					<display:column property="TGL_INPUT" title="TGL INPUT"  sortable="true"/>                                                                                    
					<display:column property="TGL_KLAIM" title="TGL KLAIM"  sortable="true"/>       
					<c:if test="${pusatOrCabang eq 'pusat'}">                                                                               
					<display:column property="TGL_PROSES" title="TGL PROSES"  sortable="true"/>       
					<display:column property="KURS" title="KURS"  sortable="true"/>                                                                                              
					<display:column property="NILAI_MATURITY" title="NILAI MATURITY" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                
					<display:column property="NILAI_KLAIM" title="NILAI KLAIM" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                      
					<display:column property="MM_NOTE" title="KETERANGAN"  sortable="true"/>    
					</c:if>                                                                                    
					<display:column property="CARA" title="MANFAAT"  sortable="true"/>                                                                                              
					<c:if test="${pusatOrCabang eq 'pusat'}">
					<display:column property="REG_SPAJ_BAYAR" title="KURS"  sortable="true"/>                                                                          
					<display:column property="MBM_JUMLAH" title="NILAI MANFAAT" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                        
					<display:column property="MBM_DESC" title="KETERANGAN MANFAAT"  sortable="true"/>                                                                                      
					</c:if>
				</display:table>
			</c:when>
			<c:when test="${param.tipeView eq 11}">
				<display:table style="width:1500px;" id="baris" name="daftar" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}" export="true" pagesize="20">                                                         
					<display:caption>Privasi</display:caption>
					<display:column property="TGL_INTEREST" title="INTEREST"  sortable="true"/>                                                                              
					<display:column property="JENIS_PROSES" title="JENIS PROSES"  sortable="true"/>                                                                              
					<display:column property="RATE_DATE" title="RATE DATE"  sortable="true"/>                                                                                    
					<display:column property="RATE" title="RATE%" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                                    
					<display:column property="KURS" title="KURS"  sortable="true"/>                                                                                              
					<display:column property="PREMI" title="PREMI"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>
					<display:column property="DEPOSIT" title="TOT DEPOSIT"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>
					<display:column property="INTEREST" title="INTEREST + TOT DEPOSIT"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>
					<display:column property="BAYAR_PREMI" title="BAYAR PREMI"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>
					<display:column property="PAID_DATE" title="PAID DATE"  sortable="true"/>                                                                                    
					<display:column property="WITHDRAW_DATE" title="WITHDRAW DATE"  sortable="true"/>                                                                            
					<display:column property="TOT_WITHDRAW" title="TOT WITHDRAW"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>
					<display:column property="NOTE" title="NOTE"  sortable="true"/>                                                                                              
				</display:table>
			</c:when>
			<c:when test="${param.tipeView eq 12}">
				<display:table style="width:1000px;" id="baris" name="daftar" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}" export="true" pagesize="20">                                                         
					<display:caption>Reinstate</display:caption>
					<display:column property="MSBI_TAHUN_KE" title="THN KE"  sortable="true"/>                                                                            
					<display:column property="MSBI_PREMI_KE" title="PREMI KE"  sortable="true"/>                                                                            
					<display:column property="MSBI_BEG_DATE" title="BEG KE" format="{0, date, dd/MM/yyyy}"  sortable="true"/>                                             
					<display:column property="MSBI_END_DATE" title="END DATE" format="{0, date, dd/MM/yyyy}"  sortable="true"/>                                             
					<display:column property="MSBI_DUE_DATE" title="DUE DATE" format="{0, date, dd/MM/yyyy}"  sortable="true"/>                                             
					<display:column property="LKU_SYMBOL" title="KURS"  sortable="true"/>                                                                                  
					<display:column property="MSUR_TOTAL_UNBAYAR" title="PREMI" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                        
					<display:column property="MSUR_TOTAL_BUNGA_UNBAYAR" title="BUNGA" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>            
					<display:column property="SISA_TAGIHAN" title="SISA TAGIHAN"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>            
					<display:column property="MSBI_PAID" title="PAID"  sortable="true"/>                                                                                    
					<display:column property="MSBI_PRINT" title="PRINT"  sortable="true"/>                                                                                  
					<display:column property="MSBI_BILL_NO" title="NO. KPL"  sortable="true"/>                                                                              
					<display:column property="LSPD_ID" title="POS"  sortable="true"/>                                                                                        
				</display:table>
			</c:when>
			<c:when test="${param.tipeView eq 13}">
				<display:table style="width:1000px;" id="baris" name="daftar" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}" export="true" pagesize="20">                                                         
					<display:caption>Komisi</display:caption>
					<display:column title="SHOW"  sortable="true">
						<input type="button" value="Deduct" onclick="alert('Maaf tetapi menu ini masih dalam tahap pengembangan');">
					</display:column>
					<display:column property="MSBI_TAHUN_KE" title="THN KE"  sortable="true"/>                                                                            
					<display:column property="MSBI_PREMI_KE" title="PREMI KE"  sortable="true"/>                                                                            
					<display:column property="MSAG_ID" title="KODE AGENT"  sortable="true"/>                                                                                        
					<display:column property="MCL_FIRST" title="NAMA AGENT"  sortable="true"/>                                                                                    
					<display:column property="KOMISI" title="KOMISI"  sortable="true"/>                                                                                          
					<display:column property="MSCO_COMM" title="JML KOMISI" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                          
					<display:column property="MSCO_TAX" title="TAX" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                            
					<display:column property="TOTAL_KOMISI" title="TOTAL KOMISI"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                          
					<display:column property="MSCO_DATE" title="TGL U/W"  sortable="true"/>                                                                                    
					<display:column property="MSCO_PAY_DATE" title="TGL TRANSFER"  sortable="true"/>                                                                            
					<display:column property="PAID" title="BAYAR"  sortable="true"/>                                                                                              
					<display:column property="LUS_LOGIN_NAME" title="USER"  sortable="true"/>                                                                          
					<display:column property="MSCO_NILAI_KURS" title="KURS"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                          
				</display:table>
			</c:when>
			<c:when test="${param.tipeView eq 14}">
				<display:table style="width:1200px;" id="baris" name="daftar" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}" export="true" pagesize="20">                                                         
					<display:caption>Rewards</display:caption>
					<display:column property="MSBI_TAHUN_KE" title="THN KE"  sortable="true"/>
					<display:column property="MSBI_PREMI_KE" title="PREMI KE"  sortable="true"/>                                                                            
					<display:column property="JENIS" title="JENIS"  sortable="true"/>                                                                                            
					<display:column property="MSRK_ID" title="KODE AGENT"  sortable="true"/>                                                                                        
					<display:column property="NAMA" title="NAMA AGENT"  sortable="true"/>                                                                                              
					<display:column property="MSCO_COMM" title="REWARD" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                          
					<display:column property="MSCO_TAX" title="TAX" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                            
					<display:column property="NETT" title="NETT"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>
					<display:column property="MSCO_DATE" title="TGL REWARD"  sortable="true"/>                                                                                    
					<display:column property="MSCO_PAY_DATE" title="TGL BAYAR"  sortable="true"/>                                                                            
					<display:column property="LUS_LOGIN_NAME" title="USER"  sortable="true"/>                                                                          
					<display:column property="MSCO_NILAI_KURS" title="KURS"  sortable="true"/>                                                                        
				</display:table>
			</c:when>
			<c:when test="${param.tipeView eq 15}">
				<display:table style="width:1000px;" id="baris" name="daftar" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}" export="true" pagesize="20">                                                         
					<display:caption>NAV</display:caption>
					<display:column property="TH_KE" title="THN" sortable="true" />       
					<display:column property="MNS_KE" title="KE" sortable="true" />                                                                                          
					<display:column property="BDATE" title="TGL" sortable="true" />                                                                                            
					<display:column property="MNS_HARGA" title="HARGA" format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" />                                          
					<display:column property="MNS_UNIT" title="UNIT" format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" />                                            
					<display:column property="LDEC_RATE" title="NILAI TUNAI YG DIJAMIN" format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" />  
					<display:column property="MNS_SALDO" title="SALDO" format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" />                                          
					<display:column property="MANF_NON_KEMATIAN" title="MANF KEMATIAN BKN KRN KECELAKAAN" format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" />                                          
					<display:column property="MANF_KEMATIAN" title="MANF KEMATIAN KRN KECELAKAAN" format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" />                                          
					<display:column property="MNS_TGL_INPUT" title="TGL PROSES" format="{0, date, dd/MM/yyyy}" sortable="true" />                                             
					<display:column property="MNS_TGL_PRINT" title="TGL PRINT" format="{0, date, dd/MM/yyyy}" sortable="true" />                                             
					<display:column property="PRINT" title="PRINT" sortable="true" />                                                                                            
					<display:column property="LUS_LOGIN_NAME" title="USER" sortable="true" />                                                                          
				</display:table>

			</c:when>
			<c:when test="${param.tipeView eq 16}">
				<!--<display:table style="width:534px;" id="baris" name="daftar" class="displaytag" requestURI="${requestScope.requestURL}" export="true" pagesize="20">
					<display:caption>Medis</display:caption>
						<display:column title="Tanggal"  sortable="true" style="21%">
							<p align="center"><fmt:formatDate value="${baris.MSDM_INPUT_DATE}" pattern="dd/MM/yyyy"/></p>
						</display:column>
						<display:column title="User Input"  sortable="true" style="20%">
							<p align="center">${baris.LUS_FULL_NAME}</p>
						</display:column>
						<display:column title="Keterangan">
							<textarea name="descMedis" readonly cols="80" rows="4" style="overflow: hidden;text-transform: none">${baris.MSDM_DESC}</textarea>
						</display:column>
				</display:table>-->
				<!--<iframe id="viewer" width="100%" height="100%" src="${path}/uw/medical_new.htm?spaj=${spaj}"></iframe>-->
			</c:when>
			<c:when test="${param.tipeView eq 17}">
				<display:table style="width:1500px;" id="baris" name="daftar" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}" export="true" pagesize="20">                                                         
					<display:caption>Bonus</display:caption>
					<display:column property="MSBI_TAHUN_KE" title="THN KE" sortable="true" />       
					<display:column property="MSBI_PREMI_KE" title="PREMI KE" sortable="true" /> 
					<display:column property="JENIS" title="TIPE"  sortable="true"/>                                                                                          
					<display:column property="NAMA" title="NAMA" sortable="true" />                                                                                           
					<display:column property="NO_ACCOUNT" title="NO ACCOUNT" sortable="true" />   
					<display:column property="NAMA_BANK" title="NAMA BANK" sortable="true" />                                             
					<display:column property="MSCO_COMM" title="KOMISI" format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" />                                            
					<display:column property="MSCO_TAX" title="PAJAK" format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" />                                          
					<display:column property="MSCO_NILAI_KURS" title="NILAI KURS" format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" />                                          
					<display:column property="MSCO_DATE" title="TGL KOMISI" format="{0, date, dd/MM/yyyy}" sortable="true" />                                             
					<display:column property="MSCO_PAY_DATE" title="TGL BAYAR" format="{0, date, dd/MM/yyyy}" sortable="true" />                                             
					<display:column property="MSCO_PAID" title="SUDAH DIBAYAR" sortable="true" /> 
					<display:column property="MSAG_ID" title="KODE AGEN" sortable="true" />   
					<display:column property="MCL_FIRST" title="NAMA AGENT"  sortable="true"/>                                                                                            
					<display:column property="LUS_LOGIN_NAME" title="USER" sortable="true" />                                                                          
				</display:table>
			</c:when>
			<c:when test="${param.tipeView eq 18}">
			  <c:set var="widthTable" value=""/>
                    <c:choose>
                        <c:when test="${pusatOrCabang eq 'pusat'}">
                            <c:set var="widthTable" value="2800px"/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="widthTable" value="1200px"/>
                        </c:otherwise>
                    </c:choose>
				<display:table style="width:${widthTable};" id="baris" name="daftar" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}" export="true" pagesize="20">                                                         
					<display:caption>Stable Link</display:caption>
						<display:column property="ROW_NUMBER" title="NO" sortable="true" /> 
						<display:column property="PERIODE_DATE" title="PERIODE" sortable="true" /> 
						<display:column property="MSL_BDATE" title="BEG DATE" sortable="true" />
						<c:if test="${pusatOrCabang eq 'pusat'}">
						<display:column property="NO_REG" title="NO REGISTER" sortable="true" />
						</c:if>
						<display:column property="MSL_DESC" title="DESCRIPTION" sortable="true" />
						<display:column property="MSL_MGI" title="MTI" sortable="true" />
						<c:if test="${pusatOrCabang eq 'pusat'}">
						<display:column property="FLAG_RATE" title="JNS RATE" sortable="true" />
						<display:column property="MSL_RATE" title="RATE %" format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" />                                            						
						<display:column property="MSL_CONFIRM_DATE" title="CONFIRM DATE" sortable="true" />
						</c:if>
						<display:column property="MSL_RO" title="ROLL OVER" sortable="true" />
						<c:if test="${pusatOrCabang eq 'pusat'}">
						<display:column property="MSL_HARI" title="JUM HARI" sortable="true" />
						<display:column property="MSL_JN_RUMUS" title="JNS RUMUS" sortable="true" />
						<display:column property="MSL_PRINT_DATE" title="RO PRINT DATE" sortable="true" />
						</c:if>
						<display:column property="LKU_ID" title="KURS" sortable="true" />
						<c:if test="${pusatOrCabang eq 'pusat'}">
						<display:column property="MSL_PREMI" title="PREMI" format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" />                                            						
						<display:column property="MSL_BUNGA" title="INTEREST" format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" /> 
						<display:column property="MSL_TGL_NAB" title="NAV DATE" sortable="true" />
						<display:column property="MSL_NAB" title="NAV" format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" />                                            						
						<display:column property="MSL_UNIT" title="UNIT" format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" />                                            						
						<display:column property="MSL_BP_RATE" title="BP RATE%" format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" />  
						<display:column property="MSL_TGL_NAB_BP" title="NAV DATE(BP)"  sortable="true"/>     
						<display:column property="MSL_NAB_BP" title="NAV(BP)" format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" />  
						<display:column property="MSL_NILAI_POLIS" title="UNIT(BP)" format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" />  						
						<display:column property="MSL_BP" title="NILAI BP" format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" />                                            						
						<display:column property="MSL_BP_PT" title="NILAI BP PT" format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" />                                            						
						<display:column property="NOTE" title="NOTE" sortable="true" />
						<display:column property="PRM_INTEREST" title="BAYAR PRM INTEREST" sortable="true" />
						<display:column property="NO_REKENING" title="NO_REKENING" sortable="true" />
						<display:column property="USER_NAME" title="USER" sortable="true" />
						</c:if>
				</display:table>
			</c:when>
			<c:when test="${param.tipeView eq 19}">
			 <c:choose>
                        <c:when test="${pusatOrCabang eq 'pusat'}">
                            <c:set var="widthTable" value="1800px"/>
	                            <display:table style="width:1800px;" id="baris" name="daftar" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}" export="true" pagesize="40">                                                         
								<display:caption>Akumulasi Polis</display:caption>
								<display:column title="POLICY HOLDER" style="text-align: left;" media="excel" property="A" />                                                                                          
								<display:column title="INSURED NAME" style="text-align: left;" media="excel" property="B" /> 
								<display:column title="No Polis" style="text-align: center;" media="excel" property="C" />
								<display:column title="PRODUK" style="text-align: left;" media="excel">
									<c:if test="${not empty baris.O}">[${baris.O}-${baris.U}] ${baris.V}</c:if>
									<c:if test="${not empty baris.E1}">${baris.E1}</c:if>					
								</display:column>
								<display:column title="KURS" style="text-align: center;" media="excel" property="D" />
								<display:column title="TSI" media="excel">
				                    <c:choose>
				                        <c:when test="${not empty baris.E}">
				                            <fmt:formatNumber pattern="#,##0.00">${baris.E}</fmt:formatNumber>
				                        </c:when>
				                        <c:otherwise>
				                            ${baris.E}
				                        </c:otherwise>
				                    </c:choose>					
								</display:column>
								<display:column title="SAR" media="excel">
				                    <c:choose>
				                        <c:when test="${not empty baris.F}">
				                            <fmt:formatNumber pattern="#,##0.00">${baris.F}</fmt:formatNumber>
				                        </c:when>
				                        <c:otherwise>
				                            ${baris.F}
				                        </c:otherwise>
				                    </c:choose>                    						
								</display:column>
								<display:column title="RETENSI" media="excel">
				                    <c:choose>
				                        <c:when test="${not empty baris.E}">
				                            <fmt:formatNumber pattern="#,##0.00">${baris.W}</fmt:formatNumber>
				                        </c:when>
				                        <c:otherwise>
						                    <c:choose>
						                        <c:when test="${not empty baris.flag}">
						                            <fmt:formatNumber pattern="#,##0.00">${baris.W}</fmt:formatNumber>
						                        </c:when>
						                        <c:otherwise>
								                    <c:choose>
								                        <c:when test="${not empty baris.C1}">
								                        	<fmt:formatNumber pattern="#,##0.00">${baris.W}</fmt:formatNumber>
								                        </c:when>
								                        <c:when test="${empty baris.C1 and not empty baris.W}">
								                        	<fmt:formatNumber pattern="#,##0.00">${baris.W}</fmt:formatNumber>
								                        </c:when>					                        
								                        <c:otherwise>
								                        	${baris.W}
								                        </c:otherwise>
								                    </c:choose>					                        
						                        </c:otherwise>
						                    </c:choose>				                           
				                        </c:otherwise>
				                    </c:choose>						
								</display:column>
								<display:column title="TSI REAS" media="excel"> 
				                    <c:choose>
				                        <c:when test="${not empty baris.A1}">
				                            <fmt:formatNumber pattern="#,##0.00">${baris.A1}</fmt:formatNumber>
				                        </c:when>
				                        <c:otherwise>
				                            ${baris.A1}
				                        </c:otherwise>
				                    </c:choose>                    						
								</display:column>														                  
								<display:column title="TYPE REAS" style="text-align: center;" media="excel" property="C1" />
								<display:column title="PREMI STD" sortable="true" media="excel"> 
				                    <c:choose>
				                        <c:when test="${not empty baris.G}">
				                            <fmt:formatNumber pattern="#,##0.00">${baris.G}</fmt:formatNumber>
				                        </c:when>
				                        <c:otherwise>
				                            ${baris.G}
				                        </c:otherwise>
				                    </c:choose>	                    						
								</display:column>	
								<display:column title="EXTRA %" media="excel" property="B1" />
								<display:column title="EXTRA" media="excel"> 
				                    <c:choose>
				                        <c:when test="${not empty baris.I}">
				                            <fmt:formatNumber pattern="#,##0.00">${baris.I}</fmt:formatNumber>
				                        </c:when>
				                        <c:otherwise>
				                            ${baris.I}
				                        </c:otherwise>
				                    </c:choose>                    						
								</display:column>					                                                          
								<display:column title="BEG DATE" style="text-align: center;" media="excel" property="J" />
								<display:column title="END DATE" style="text-align: center;" media="excel" property="K" />
								<display:column title="KETERANGAN" style="text-align: center;" media="excel" property="D1" />
								<display:column title="SP" style="text-align: center;" media="excel" property="L" />
								<display:column title="TAHUN KE" style="text-align: center;" media="excel" property="N" />
								
								<display:column title="POLICY HOLDER" style="text-align: left;" media="html">
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11 or baris.lstb_id eq 1 or baris.lstb_id eq 111}">
				                            ${baris.A}
				                        </c:when>
				                        <c:otherwise>
				                            <font color="#FF0000">${baris.A}</font>
				                        </c:otherwise>
				                    </c:choose>
								</display:column>
								<display:column title="INSURED NAME" style="text-align: left;" media="html">
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
				                            ${baris.B}
				                        </c:when>
				                        <c:otherwise>
				                            <font color="#FF0000">${baris.B}</font>
				                        </c:otherwise>
				                    </c:choose>
								</display:column> 					                                                                                                    
								<display:column title="No Polis" sortable="true" group="1" style="text-align: center;" media="html">
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1}">
				                            <a href="javascript:popWin('${path}/uw/detailsimultan_new.htm?spajAwal=${baris.M}&flag_tt=0', 400, 800,1,0) ;" style="color: #000">${baris.C}</a> 
				                        </c:when>
				                        <c:when test="${baris.lstb_id eq 11}">
				                            <a style="color: #000">${baris.C}</a> 
				                        </c:when>
				                        <c:otherwise>
				                            <a href="javascript:popWin('${path}/uw/detailsimultan_new.htm?spajAwal=${baris.M}&flag_tt=0', 400, 800,1,0) ;" style="color: #FF0000">${baris.C}</a> 
				                        </c:otherwise>
				                    </c:choose>					
									
								</display:column>                                                                                      
								<display:column title="PRODUK" style="text-align: left;" media="html">
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
											<c:if test="${not empty baris.O}">[${baris.O}-${baris.U}] ${baris.V}</c:if>
											<c:if test="${not empty baris.E1}"><b>${baris.E1}</b></c:if>
				                        </c:when>
				                        <c:otherwise>
											<c:if test="${not empty baris.O}"><font color="#FF0000">[${baris.O}-${baris.U}] ${baris.V}</font></c:if>
											<c:if test="${not empty baris.E1}"><b>${baris.E1}</b></c:if>	                            
				                        </c:otherwise>
				                    </c:choose>					
								</display:column>
								<display:column title="KURS" style="text-align: center;" media="html">
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
				                            ${baris.D}
				                        </c:when>
				                        <c:otherwise>
						                    <c:choose>
						                        <c:when test="${not empty baris.E1}">
						                            <b>${baris.D}</b>
						                        </c:when>
						                        <c:otherwise>
						                            <font color="#FF0000">${baris.D}</font>
						                        </c:otherwise>
						                    </c:choose>		                            
				                        </c:otherwise>
				                    </c:choose>
								</display:column>					
								<display:column title="TSI" media="html"> 
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
						                    <c:choose>
						                        <c:when test="${not empty baris.E}">
						                            <fmt:formatNumber pattern="#,##0.00">${baris.E}</fmt:formatNumber>
						                        </c:when>
						                        <c:otherwise>
						                            ${baris.E}
						                        </c:otherwise>
						                    </c:choose>
				                        </c:when>
				                        <c:otherwise>
						                    <c:choose>
						                        <c:when test="${not empty baris.E}">
						                            <font color="#FF0000"><fmt:formatNumber pattern="#,##0.00">${baris.E}</fmt:formatNumber></font>
						                        </c:when>
						                        <c:otherwise>
						                            ${baris.E}
						                        </c:otherwise>
						                    </c:choose>
				                        </c:otherwise>
				                    </c:choose>	                    						
								</display:column>  
								<display:column title="SAR" media="html">
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
						                    <c:choose>
						                        <c:when test="${not empty baris.F}">
						                            <fmt:formatNumber pattern="#,##0.00">${baris.F}</fmt:formatNumber>
						                        </c:when>
						                        <c:otherwise>
						                            ${baris.F}
						                        </c:otherwise>
						                    </c:choose>
				                        </c:when>
				                        <c:otherwise>
						                    <c:choose>
						                        <c:when test="${not empty baris.F}">
								                    <c:choose>
								                        <c:when test="${not empty baris.E}">
								                            <font color="#FF0000"><fmt:formatNumber pattern="#,##0.00">${baris.F}</fmt:formatNumber></font>
								                        </c:when>
								                        <c:otherwise>
								                            <b><fmt:formatNumber pattern="#,##0.00">${baris.F}</fmt:formatNumber></b>
								                        </c:otherwise>
								                    </c:choose>			                        
						                        </c:when>
						                        <c:otherwise>
						                            ${baris.F}
						                        </c:otherwise>
						                    </c:choose>
				                        </c:otherwise>
				                    </c:choose>	                    						
								</display:column> 
								<display:column title="RETENSI" media="html"> 
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
						                    <c:choose>
						                        <c:when test="${not empty baris.E}">
						                            <fmt:formatNumber pattern="#,##0.00">${baris.W}</fmt:formatNumber>
						                        </c:when>
						                        <c:otherwise>
								                    <c:choose>
								                        <c:when test="${not empty baris.flag}">
								                            <b><fmt:formatNumber pattern="#,##0.00">${baris.W}</fmt:formatNumber></b>
								                        </c:when>
								                        <c:otherwise>
										                    <c:choose>
										                        <c:when test="${not empty baris.C1}">
										                        	<font color="#0000FF"><fmt:formatNumber pattern="#,##0.00">${baris.W}</fmt:formatNumber><font color="#FF0000">
										                        </c:when>
										                        <c:otherwise>
										                        	<b>${baris.W}</b>
										                        </c:otherwise>
										                    </c:choose>					                        
								                        </c:otherwise>
								                    </c:choose>			                           
						                        </c:otherwise>
						                    </c:choose>			                    
				                        </c:when>
				                        <c:otherwise>
						                    <c:choose>
						                        <c:when test="${not empty baris.E}">
						                            <font color="#FF0000"><fmt:formatNumber pattern="#,##0.00">${baris.W}</fmt:formatNumber></font>
						                        </c:when>
						                        <c:otherwise>
								                    <c:choose>
								                        <c:when test="${not empty baris.flag}">
								                            <b><fmt:formatNumber pattern="#,##0.00">${baris.W}</fmt:formatNumber></b>
								                        </c:when>
								                        <c:otherwise>
										                    <c:choose>
										                        <c:when test="${not empty baris.C1}">
										                        	<font color="#0000FF"><fmt:formatNumber pattern="#,##0.00">${baris.W}</fmt:formatNumber><font color="#FF0000">
										                        </c:when>
										                        <c:otherwise>
										                        	<b><fmt:formatNumber pattern="#,##0.00">${baris.W}</fmt:formatNumber></b>
										                        </c:otherwise>
										                    </c:choose>					                        
								                        </c:otherwise>
								                    </c:choose>			                           
						                        </c:otherwise>
						                    </c:choose>
				                        </c:otherwise>
				                    </c:choose>	                    					
								</display:column> 	
								<display:column title="TSI REAS" media="html"> 
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
						                    <c:choose>
						                        <c:when test="${not empty baris.A1}">
						                            <fmt:formatNumber pattern="#,##0.00">${baris.A1}</fmt:formatNumber>
						                        </c:when>
						                        <c:otherwise>
						                            ${baris.A1}
						                        </c:otherwise>
						                    </c:choose>
				                        </c:when>
				                        <c:otherwise>
						                    <c:choose>
						                        <c:when test="${not empty baris.A1}">
						                            <font color="#FF0000"><fmt:formatNumber pattern="#,##0.00">${baris.A1}</fmt:formatNumber></font>
						                        </c:when>
						                        <c:otherwise>
						                            ${baris.A1}
						                        </c:otherwise>
						                    </c:choose>
				                        </c:otherwise>
				                    </c:choose>	                    						
								</display:column>														                  
								<display:column title="TYPE REAS" style="text-align: center;" media="html">
				                    <c:choose>
				                        <c:when test="${not empty baris.flag}">
				                            ${baris.C1}
				                        </c:when>
				                        <c:otherwise>
				                           <font color="#0000FF">${baris.C1}</font>
				                        </c:otherwise>
				                    </c:choose>	
								</display:column>					
								<display:column title="PREMI STD" sortable="true" media="html"> 
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
						                    <c:choose>
						                        <c:when test="${not empty baris.G}">
						                            <fmt:formatNumber pattern="#,##0.00">${baris.G}</fmt:formatNumber>
						                        </c:when>
						                        <c:otherwise>
						                            ${baris.G}
						                        </c:otherwise>
						                    </c:choose>
				                        </c:when>
				                        <c:otherwise>
						                    <c:choose>
						                        <c:when test="${not empty baris.G}">
						                            <font color="#FF0000"><fmt:formatNumber pattern="#,##0.00">${baris.G}</fmt:formatNumber></font>
						                        </c:when>
						                        <c:otherwise>
						                            ${baris.G}
						                        </c:otherwise>
						                    </c:choose>
				                        </c:otherwise>
				                    </c:choose>	                    						
								</display:column>	
								<display:column title="EXTRA %" media="html">
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
				                            ${baris.B1}
				                        </c:when>
				                        <c:otherwise>
				                            <font color="#FF0000">${baris.B1}</font>
				                        </c:otherwise>
				                    </c:choose>
								</display:column>					
								<display:column title="EXTRA" media="html"> 
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
						                    <c:choose>
						                        <c:when test="${not empty baris.I}">
						                            <fmt:formatNumber pattern="#,##0.00">${baris.I}</fmt:formatNumber>
						                        </c:when>
						                        <c:otherwise>
						                            ${baris.I}
						                        </c:otherwise>
						                    </c:choose>
				                        </c:when>
				                        <c:otherwise>
						                    <c:choose>
						                        <c:when test="${not empty baris.I}">
						                            <font color="#FF0000"><fmt:formatNumber pattern="#,##0.00">${baris.I}</fmt:formatNumber></font>
						                        </c:when>
						                        <c:otherwise>
						                            ${baris.I}
						                        </c:otherwise>
						                    </c:choose>
				                        </c:otherwise>
				                    </c:choose>	                    						
								</display:column>					                                                          
								<display:column title="BEG DATE" style="text-align: center;" media="html">
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
				                            ${baris.J}
				                        </c:when>
				                        <c:otherwise>
				                            <font color="#FF0000">${baris.J}</font>
				                        </c:otherwise>
				                    </c:choose>
								</display:column>					                                                                                                    
								<display:column title="END DATE" style="text-align: center;" media="html">
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
				                            ${baris.K}
				                        </c:when>
				                        <c:otherwise>
				                            <font color="#FF0000">${baris.K}</font>
				                        </c:otherwise>
				                    </c:choose>
								</display:column>					
								<display:column title="KETERANGAN" style="text-align: center;" media="html">
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
				                            ${baris.D1}
				                        </c:when>
				                        <c:otherwise>
				                            <font color="#FF0000">${baris.D1}</font>
				                        </c:otherwise>
				                    </c:choose>
								</display:column>					                                                                                                    
								<display:column title="SP" style="text-align: center;" media="html">
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
				                            ${baris.L}
				                        </c:when>
				                        <c:otherwise>
				                            <font color="#FF0000">${baris.L}</font>
				                        </c:otherwise>
				                    </c:choose>
								</display:column>					                                                                                                    
								<display:column title="TAHUN KE" style="text-align: center;" media="html">
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
				                            ${baris.N}
				                        </c:when>
				                        <c:otherwise>
				                            <font color="#FF0000">${baris.N}</font>
				                        </c:otherwise>
				                    </c:choose>
								</display:column> 					                                                                                                    
							</display:table>
                        </c:when>
                        <c:otherwise>
                            <c:set var="widthTable" value="1200px"/>
                            <display:table style="width:1200px;" id="baris" name="daftar" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}" export="true" pagesize="40">                                                         
								<display:caption>Akumulasi Polis</display:caption>
								<display:column title="POLICY HOLDER" style="text-align: left;" media="excel" property="A" />                                                                                          
								<display:column title="INSURED NAME" style="text-align: left;" media="excel" />
								<display:column title="KURS" style="text-align: center;" media="excel" property="D" />
								<display:column title="TSI" media="excel">
				                    <c:choose>
				                        <c:when test="${not empty baris.E}">
				                            <fmt:formatNumber pattern="#,##0.00">${baris.E}</fmt:formatNumber>
				                        </c:when>
				                        <c:otherwise>
				                            ${baris.E}
				                        </c:otherwise>
				                    </c:choose>					
								</display:column>
								<display:column title="SAR" media="excel">
				                    <c:choose>
				                        <c:when test="${not empty baris.F}">
				                            <fmt:formatNumber pattern="#,##0.00">${baris.F}</fmt:formatNumber>
				                        </c:when>
				                        <c:otherwise>
				                            ${baris.F}
				                        </c:otherwise>
				                    </c:choose>                    						
								</display:column>
								<display:column title="EXTRA" media="excel"> 
				                    <c:choose>
				                        <c:when test="${not empty baris.I}">
				                            <fmt:formatNumber pattern="#,##0.00">${baris.I}</fmt:formatNumber>
				                        </c:when>
				                        <c:otherwise>
				                            ${baris.I}
				                        </c:otherwise>
				                    </c:choose>                    						
								</display:column>					                                                          
								<display:column title="BEG DATE" style="text-align: center;" media="excel" property="J" />
								<display:column title="KETERANGAN" style="text-align: center;" media="excel" property="D1" />
								<display:column title="SP" style="text-align: center;" media="excel" property="L" />
								<display:column title="TAHUN KE" style="text-align: center;" media="excel" property="N" />
								
								<display:column title="POLICY HOLDER" style="text-align: left;" media="html">
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11 or baris.lstb_id eq 1 or baris.lstb_id eq 111}">
				                            ${baris.A}
				                        </c:when>
				                        <c:otherwise>
				                            <font color="#FF0000">${baris.A}</font>
				                        </c:otherwise>
				                    </c:choose>
								</display:column>
								<display:column title="INSURED NAME" style="text-align: left;" media="html">
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
				                            ${baris.B}
				                        </c:when>
				                        <c:otherwise>
				                            <font color="#FF0000">${baris.B}</font>
				                        </c:otherwise>
				                    </c:choose>
								</display:column> 					                                                                                                    
								<display:column title="KURS" style="text-align: center;" media="html">
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
				                            ${baris.D}
				                        </c:when>
				                        <c:when test="${empty baris.F}">
				                            <!-- blank -->
				                        </c:when>
				                        <c:otherwise>
						                    <c:choose>
						                        <c:when test="${not empty baris.E1}">
						                            <b>${baris.D}</b>
						                        </c:when>
						                        <c:otherwise>
						                            <font color="#FF0000">${baris.D}</font>
						                        </c:otherwise>
						                    </c:choose>		                            
				                        </c:otherwise>
				                    </c:choose>
								</display:column>					
								<display:column title="TSI" media="html"> 
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
						                    <c:choose>
						                        <c:when test="${not empty baris.E}">
						                            <fmt:formatNumber pattern="#,##0.00">${baris.E}</fmt:formatNumber>
						                        </c:when>
						                        <c:otherwise>
						                            ${baris.E}
						                        </c:otherwise>
						                    </c:choose>
				                        </c:when>
				                        <c:otherwise>
						                    <c:choose>
						                        <c:when test="${not empty baris.E}">
						                            <font color="#FF0000"><fmt:formatNumber pattern="#,##0.00">${baris.E}</fmt:formatNumber></font>
						                        </c:when>
						                        <c:otherwise>
						                            ${baris.E}
						                        </c:otherwise>
						                    </c:choose>
				                        </c:otherwise>
				                    </c:choose>	                    						
								</display:column>  
								<display:column title="SAR" media="html">
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
						                    <c:choose>
						                        <c:when test="${not empty baris.F}">
						                            <fmt:formatNumber pattern="#,##0.00">${baris.F}</fmt:formatNumber>
						                        </c:when>
						                        <c:otherwise>
						                            ${baris.F}
						                        </c:otherwise>
						                    </c:choose>
				                        </c:when>
				                        <c:otherwise>
						                    <c:choose>
						                        <c:when test="${not empty baris.F}">
								                    <c:choose>
								                        <c:when test="${not empty baris.E}">
								                            <font color="#FF0000"><fmt:formatNumber pattern="#,##0.00">${baris.F}</fmt:formatNumber></font>
								                        </c:when>
								                        <c:otherwise>
								                            <b><fmt:formatNumber pattern="#,##0.00">${baris.F}</fmt:formatNumber></b>
								                        </c:otherwise>
								                    </c:choose>			                        
						                        </c:when>
						                        <c:otherwise>
						                            ${baris.F}
						                        </c:otherwise>
						                    </c:choose>
				                        </c:otherwise>
				                    </c:choose>	                    						
								</display:column> 
								<display:column title="EXTRA" media="html"> 
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
						                    <c:choose>
						                        <c:when test="${not empty baris.I}">
						                            <fmt:formatNumber pattern="#,##0.00">${baris.I}</fmt:formatNumber>
						                        </c:when>
						                        <c:otherwise>
						                            ${baris.I}
						                        </c:otherwise>
						                    </c:choose>
				                        </c:when>
				                        <c:otherwise>
						                    <c:choose>
						                        <c:when test="${not empty baris.I}">
						                            <font color="#FF0000"><fmt:formatNumber pattern="#,##0.00">${baris.I}</fmt:formatNumber></font>
						                        </c:when>
						                        <c:otherwise>
						                            ${baris.I}
						                        </c:otherwise>
						                    </c:choose>
				                        </c:otherwise>
				                    </c:choose>	                    						
								</display:column>					                                                          
								<display:column title="BEG DATE" style="text-align: center;" media="html">
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
				                            ${baris.J}
				                        </c:when>
				                        <c:otherwise>
				                            <font color="#FF0000">${baris.J}</font>
				                        </c:otherwise>
				                    </c:choose>
								</display:column>					                                                                                                    
								<display:column title="KETERANGAN" style="text-align: center;" media="html">
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
				                            ${baris.D1}
				                        </c:when>
				                        <c:otherwise>
				                            <font color="#FF0000">${baris.D1}</font>
				                        </c:otherwise>
				                    </c:choose>
								</display:column>					                                                                                                    
								<display:column title="SP" style="text-align: center;" media="html">
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
				                            ${baris.L}
				                        </c:when>
				                        <c:otherwise>
				                            <font color="#FF0000">${baris.L}</font>
				                        </c:otherwise>
				                    </c:choose>
								</display:column>					                                                                                                    
								<display:column title="TAHUN KE" style="text-align: center;" media="html">
				                    <c:choose>
				                        <c:when test="${baris.lstb_id eq 1 or baris.lstb_id eq 11}">
				                            ${baris.N}
				                        </c:when>
				                        <c:otherwise>
				                            <font color="#FF0000">${baris.N}</font>
				                        </c:otherwise>
				                    </c:choose>
								</display:column> 					                                                                                                    
							</display:table>
							<c:if test="${not empty klaim }">
								<fieldset>
								<legend>RIWAYAT KLAIM KESEHATAN</legend>
								<c:forEach items="${klaim }" var="k" varStatus="s">
									<div style="background-color: #df3200;color: #FFFFFF;text-align: center; font-weight: bold;border-bottom: 2px solid #000;">NO POLIS : ${k.POLIS }</div>
									<div style="background-color: #df3200;color: #FFFFFF;text-align: center; font-weight: bold;">HARAP REFER KEBAGIAN UNDERWRITING</div><br>
								</c:forEach>
								</fieldset>
							</c:if>
							<c:if test="${not empty attention }">
								<fieldset>
								<legend>ATTENTION LIST</legend>
								<c:forEach items="${attention }" var="a" varStatus="s">
									<div style="background-color: #df3200;color: #FFFFFF;text-align: center; font-weight: bold;border-bottom: 2px solid #000;">NO POLIS : ${a.POLIS }</div>
									<div style="background-color: #df3200;color: #FFFFFF;text-align: center; font-weight: bold;">HARAP REFER KEBAGIAN UNDERWRITING</div><br>
								</c:forEach>
								</fieldset>
							</c:if>
                        </c:otherwise>
             </c:choose>
				
			</c:when>		
			
			<c:when test="${param.tipeView eq 20}">
				<display:table style="width:700px;" id="baris" name="daftar" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}" export="true" pagesize="20">                                                         
					<display:caption>REFERRER</display:caption>  
					<display:column property="CABANG_PROD" title="CABANG PRODUKSI"  sortable="true"/>                                                                                              
					<display:column property="PENUTUP" title="NAMA PENUTUP"  sortable="true"/>                                                                                            
					<display:column property="NM_CABANG_PENUTUP" title="CABANG PENUTUP"  sortable="true"/>                                                                                          
					<display:column property="NM_REFERRER" title="NAMA REFERRER"  sortable="true"/>
					<display:column property="NM_CABANG_REFERRER" title="CABANG REFERRER"  sortable="true"/>                                                                                           
				</display:table>

			</c:when>	
			<c:otherwise>
				<script>alert('Maaf, tetapi modul ini masih dalam tahap pengembangan');</script>
			</c:otherwise>
		</c:choose>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>