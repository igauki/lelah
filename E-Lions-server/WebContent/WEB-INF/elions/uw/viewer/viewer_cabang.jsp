<%@ include file="/include/page/header.jsp"%><c:set var="path" value="${pageContext.request.contextPath}" />
<style type="text/css">
	html,body{
	     overflow: hidden;
	}
</style>
<script>
	function cekSpaj(){
		if(trim(document.formpost.spaj.value)=='') {
			alert('Harap cari nomor SPAJ terlebih dahulu');
			popWin('${path}/uw/spaj.htm?posisi=-1', 350, 450);
		}else return true;
		
		return false;
	}
	
	function awal(){
		if (document.formpost.spaj.value != "")
		{
			cariregion(document.formpost.spaj.value,'region');
		}
	}

	function monyong(y){
		if('cari'==y) {
			popWin('${path}/uw/spaj.htm?posisi=-1&win=viewer&kata='+document.formpost.spaj.value, 350, 450);
		}else if('spaj'==y) {if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/uw/view.htm?p=v&showSPAJ='+document.formpost.spaj.value;
		}else if('print'==y) {if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/uw/printpolis.htm?window=viewer&spaj='+document.formpost.spaj.value;
			//popWin('${path}/uw/printpolis.htm?window=viewer&spaj='+document.formpost.spaj.value, 500, 700, 'yes', 'yes'); 
		}else if('alamat'==y) {if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=addressbilling&spaj='+document.formpost.spaj.value;
		}else if('payment'==y) {
			if(cekSpaj())	document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=payment&spaj='+document.formpost.spaj.value;
		}else if('endorse'==y) {if(cekSpaj())
			popWin('${path}/uw/viewer.htm?window=endorse&spaj='+document.formpost.spaj.value, 500, 700); 
		}else if('investasi'==y) {if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=investasi&spaj='+document.formpost.spaj.value, 500, 700; 
		}else if('uwinfo'==y) {if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=uwinfo&spaj='+document.formpost.spaj.value;
		}else if('nilaitunai'==y) {if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=nilaitunai&spaj='+document.formpost.spaj.value;
		}else if('view'==y) {if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=viewerkontrol&spaj='+document.formpost.spaj.value;
		}else if('dokumen'==y) {if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/common/util.htm?window=doc_list&spaj='+document.formpost.spaj.value;
		}else if('call'==y) {
			if(cekSpaj())popWin('${path}/uw/viewer.htm?window=csfcall&spaj='+document.formpost.spaj.value, 480, 640); 
		}else if('summary'==y) {
			if(cekSpaj())popWin('${path}/report/bas.htm?window=csf_summary&spaj='+document.formpost.spaj.value, 500, 700); 
		}else if('upload_nb'==y) {
			if(cekSpaj()) document.getElementById('infoFrame').src='${path}/uw/upload_nb.htm?reg_spaj='+document.formpost.spaj.value;
		}else if('summarylist'==y) {
			if(cekSpaj())popWin('${path}/report/bas.htm?window=csf_summary_list&spaj='+document.formpost.spaj.value, 500, 700); 
		}else if('tunggakan'==y) {if(cekSpaj())
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=hit_bunga_tunggakan&spaj='+document.formpost.spaj.value;
		}else if('auto'==y) {if(cekSpaj())
			var rspaj=document.formpost.spaj.value;
			document.getElementById('infoFrame').src='${path }/report/uw.htm?window=auto_debet&spaj='+document.formpost.spaj.value;
		}else if('akum_new'==y) {
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=akum_new';
		}else if('nik'==y) {
			awal();
			if(cekSpaj())
			{
				alert('Hanya untuk produk Worksite');
				if  ((document.formpost.kodebisnis.value == 140)  || (document.formpost.kodebisnis.value == 141)  || (document.formpost.kodebisnis.value == 148)  || (document.formpost.kodebisnis.value == 149)  || (document.formpost.kodebisnis.value == 156) ||((document.formpost.kodebisnis.value == 143 && document.formpost.numberbisnis.value == 2) ) ||((document.formpost.kodebisnis.value == 158 && document.formpost.numberbisnis.value == 4) ))
				{
					popWin('${path}/bac/nik.htm?sts=insert&spaj='+document.formpost.spaj.value, 500, 800); 
				}else{
					alert('Nik tidak dapat diisi untuk polis ini');					
				}
			}
		}else if('faq'==y) {
			popWin('${path}/include/bas/faq_bas.jsp', 600, 800); 
		}else if('claim' == y){
			var spaj=document.formpost.spaj.value;
			popWin('${path}/uw/uw.htm?window=healthClaim&cab=true&regspajClaimDetail='+spaj+'&spaj='+spaj, 400, 900,'yes','yes','yes');
			
		}else if('catatan_polis'==y) {
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=catatan_polis';
		}
	}

	<c:if test="${not empty cmd.showCsfReminder}">
		popWin('${path}/uw/viewer.htm?window=csfreminder', 320, 700);
	</c:if>
	
	
	function cariregion(spaj,nama)
	{
			ajaxManager.listcollect(spaj , nama,
			{callback:function(map) {
		
				DWRUtil.useLoadingMessage();
				document.formpost.koderegion.value=map.LSRG_NAMA;
				document.formpost.numberbisnis.value = map.LSDBS_NUMBER;
				document.formpost.kodebisnis.value = map.LSBS_ID;
			   },
			  timeout:15000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
		
</script>
<body 
	onload=" setFrameSize('infoFrame', 90);"
	onresize="setFrameSize('infoFrame', 90);" style="height: 100%;">
<form name="formpost" method="post">
<div class="tabcontent">
<table class="entry2" style="width: 98%;">
	<tr>
		<th style="width: 50px;">SPAJ</th>
		<th>
			<input type="hidden" name="koderegion" >
			<input type="hidden" name="kodebisnis" >
			<input type="hidden" name="numberbisnis" >
			<input type="text" size="15" readonly="readonly" class="readOnly" id="spaj" name="spaj" ></th>
		<td>
			<input type="button" value="Info" name="tampilkan"
				onclick="monyong('spaj');" style="width: 35px;"
				accesskey="T" onmouseover="return overlib('Alt-T', AUTOSTATUS, WRAP);" onmouseout="nd();">
		
			<input type="button" value="Cari" name="search"
				onclick="monyong('cari');" style="width: 35px;"
				accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">

			<input type="button" value="Call" name="call" 
				onclick="monyong('call');" style="width: 35px;"
				accesskey="L" onmouseover="return overlib('Alt-L', AUTOSTATUS, WRAP);" onmouseout="nd();">

			<input type="button" value="Call Summary" name="summary" 
				onclick="monyong('summary');" style="width: 90px;"
				accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
				
			<input type="button" value="View" name="view"
				onclick="monyong('view');" style="width: 35px;"
				accesskey="V" onmouseover="return overlib('Alt-V', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="View Kesehatan" name="claim" 
				onclick="monyong('claim');" style="width: 120px;">
			<input type="button" value="Upload Scan" name="upload_nb"	
				onclick="monyong('upload_nb');">
				
			<input type="button" value="Dokumen" name="dokumen" ${cmd.viewDokumenPolis}
				onclick="monyong('dokumen');" style="width: 60px;"
				accesskey="D" onmouseover="return overlib('Alt-D', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Akum New" name="akum" ${viewDokumenPolisNew}
				onclick="monyong('akum_new');" style="width: 80px;"
				accesskey="M" onmouseover="return overlib('Alt-M', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Catatan Polis Saya" name="catatan_polis"	
				onclick="monyong('catatan_polis');">					
		</td>
	</tr>
	<tr>
		<th>Polis</th>
		<th><input type="text" size="20" readonly="readonly" class="readOnly" id="polis" name="polis"></th>
		<td>
			<input type="button" value="U/W Info" name="uwinfo" 
				onclick="monyong('uwinfo');" style="width: 60px;"
				accesskey="U" onmouseover="return overlib('Alt-U', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="FAQ-BAS" name="faq"	onclick="monyong('faq');"
				accesskey="Q" onmouseover="return overlib('Alt-Q', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Bunga Tunggakan" name="tunggakan" 
				onclick="monyong('tunggakan');" style="width: 120px;">
			<input type="button" value="Auto Debet" name="auto" 
				onclick="monyong('auto');" style="width: 120px;">
		</td>
	</tr>
	
	<tr>
		<td colspan="3"><spring:bind path="cmd.*">
			<iframe src="${path}/uw/spaj.htm?posisi=-1&win=viewer" name="infoFrame" id="infoFrame" 
				width="100%"> Please Wait... </iframe>
		</spring:bind></td>
	</tr>
</table>
</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>