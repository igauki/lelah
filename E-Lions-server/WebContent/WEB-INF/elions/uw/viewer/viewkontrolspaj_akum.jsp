<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript">
	function tampil(akum){
		flag=0;
		if(document.formpost.flag.checked){
			flag=1
		}	
	
		if(akum && document.formpost.pilter2.value!='')
			document.getElementById('infoFrame').src=
				'${path}/uw/viewer.htm?show=true&window=displaytag&tipeView=9'
				+'&spaj=${spaj}&pptt='+document.getElementById('pptt').value
				+'&pilter='+escape(document.getElementById('pilter').value)+'&pilter2='+document.getElementById('pilter2').value
				+'&flagTglLahir='+flag+'&tglLahir='+document.formpost.tglLahir.value;
		return;
	}
	
	function tekanTgl(){
	}
	
</script>
<style type="text/css">
	html,body{
	     overflow: hidden;
	}
</style>
<body onload="document.title='PopUp :: Akumulasi Polis'; setupPanes('container1', 'tab1'); setFrameSize('infoFrame', 105);" onresize="setFrameSize('infoFrame', 105);" style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Akumulasi Polis</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				
				<c:choose>
					<c:when test="${not empty pesanError}">
						<div id="error">${pesanError}</div>
					</c:when>
					<c:otherwise>
						<form method="post" name="formpost">
							<table class="entry2" width="98%">		
								<tr>
									<th>
									</th>
									<td>
										<div id="filter">
											<select name="pptt">
												<option value="2">Pemegang</option>
												<option value="3">Tertanggung</option>
											</select>
											<select name="pilter">
												<option <c:if test="${\"LIKE%\" eq param.pilter}">selected</c:if> value="LIKE&#37">LIKE%</option>
												<option <c:if test="${\"%LIKE\" eq param.pilter}">selected</c:if> value="&#37LIKE">%LIKE</option>
												<option <c:if test="${\"%LIKE%\" eq param.pilter}">selected</c:if> value="&#37LIKE&#37">%LIKE%</option>
												<option <c:if test="${\"LT\" eq param.pilter}">selected</c:if> value="LT"><</option>
												<option <c:if test="${\"LE\" eq param.pilter}">selected</c:if> value="LE"><=</option>
												<option <c:if test="${\"EQ\" eq param.pilter}">selected</c:if> value="EQ">=</option>
												<option <c:if test="${\"GE\" eq param.pilter}">selected</c:if> value="GE">>=</option>
												<option <c:if test="${\"GT\" eq param.pilter}">selected</c:if> value="GT">></option>
											</select>
											<input type="text" name="pilter2" value="${param.pilter2 }">
											<input type="button" name="show2" value="Show" onclick="createLoadingMessage(); tampil('yes');">
										</div>
									</td>
								</tr>
								<tr>
									<th>
									</th>
									<td>
										<div id="filter2">
											<input type="checkbox" class="noBorder" onClick="tekanTgl();" name="flag">AND
											<script>inputDate('tglLahir', '00/00/0000', false, '', 9);</script>
										</div>
									</td>
								</tr>
								<tr>
									<th></th>									
									<td></td>
								</tr>
							</table>
							<iframe name="infoFrame" id="infoFrame"
								width="100%" frameborder="YES">-</iframe>
		
						</form>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>