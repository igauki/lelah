<%@ include file="/include/page/header.jsp"%>
<script>

	function awal(){
		setFrameSize('infoFrame', 45);
	}
	

</script>
<body  onload="awal();" onresize="setFrameSize('infoFrame', 145);" style="height: 100%;">
<form name="formpost" method="post">
<div class="tabcontent">
<table class="entry2" style="width: 100%;">
	<tr>
		<th colspan="2">View Level Agen</th>
	</tr>
	<c:forEach var="agen" items="${cmd.ketagen}" varStatus="stat">
		<tr>
			<td>Kode Agen  : </td>
			<td>${agen.msag_id}</td>
		</tr>
		<tr>
			<td>Nama Agen  : </td>
			<td>${agen.mcl_first}</td>
		</tr>
		<tr>
			<td>Level Agen : </td>
			<td>${agen.lsle_name}</td>			
		</tr>
		<tr>
			<td>Aktif :  </td>
			<td>
				 <c:if test="${agen.msag_active eq 1}"> AKTIF </c:if>
				 <c:if test="${agen.msag_active eq 0}"> TIDAK AKTIF </c:if>
				 
			</td>			
		</tr>		
		<tr>
			<td>&nbsp; </td>
			<td>&nbsp;</td>			
		</tr>		
	</c:forEach>	
</table>
</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>