<%@ include file="/include/page/header.jsp"%>
<script>
var flag = false;
function tambah(tombol){
	var oTable = document.all ? document.all['rincian3'] : document.getElementById('rincian3');
	var row = oTable.rows.length;

	if(flag){
		tombol.value='Ilustrasi';
		if(row>0)	oTable.deleteRow(row-1);
		flag=false;
		return;
	}
	
	tombol.value='Reset';
	
	var selek = '<select name="jenisIlustrasi">';
	<c:forEach var="s" items="${jenisTrans}">
	selek += '<option value="${s.LT_ID}">${s.LT_TRANSKSI}</option>';	
	</c:forEach>
	selek += '</select>';
	
	var oRow = oTable.insertRow(row);
	var oCells = oRow.cells;
	if (document.all) {
		var cell = oRow.insertCell(oCells.length);
		cell.innerHTML = "<td><input type=\"radio\" name=\"centang\" class=\"noBorder\" checked/></td>";
		var cell = oRow.insertCell(oCells.length);
		cell.innerHTML = "<td>ILUSTRASI</td>";
		var cell = oRow.insertCell(oCells.length);
		cell.innerHTML = "<td>"+selek+"</td>";
		var cell = oRow.insertCell(oCells.length);
		cell.innerHTML = "<td>"+TextDateInput('tanggalIlustrasi', false, 'YYYYMMDD')+"</td>";
		var cell = oRow.insertCell(oCells.length);
		cell.innerHTML = "<td><input type='text' name='premiIlustrasi' value='0' size='10' align='right' /></td>";
		var cell = oRow.insertCell(oCells.length);
		cell.innerHTML = "<td><input type='text' name='unitIlustrasi' value='0' size='10' align='right' /></td>";
		var cell = oRow.insertCell(oCells.length);
		cell.innerHTML = "<td><fmt:formatDate value="${sysDate}" pattern="yyyyMMdd"/></td>";
		var cell = oRow.insertCell(oCells.length);
		cell.innerHTML = "<td>ILUSTRASI</td>";
		var cell = oRow.insertCell(oCells.length);
		cell.innerHTML = "<td>ILUSTRASI</td>";
		
		flag = true;
	}	
}
</script>
<body onload="setupPanes('container1', 'tab1'); document.title='PopUp :: Investasi';" style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Window Investasi</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">

				<c:choose>
					<c:when test="${not empty pesanError}">
						<div id="error">${pesanError}</div>
					</c:when>
					<c:otherwise>
						<form name="formpost" method="post">
								
							<c:set var="uri" value="${pageContext.request.requestURL}" />
				
							<table>
								<tr>
									<td colspan="2">
										<display:table style="width:760px;" id="rincian1" name="rincian" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${uri}">
											<display:caption>Rincian Investasi</display:caption>
											<display:column property="INVES" title="INVESTASI"  sortable="true" group="1"/>                                                                                            
											<display:column property="TANGGAL" title="TANGGAL" format="{0, date, dd/MM/yyyy}"  sortable="true"/>                                                         
											<display:column property="TRANSAKSI" title="TRANSAKSI"  sortable="true"/>                                                                                    
											<display:column property="JUMLAH" title="JUMLAH" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                                
											<display:column property="FLAG_BIAYA" title="FLAG_BIAYA"  sortable="true"/>                                                                                  
											<display:column property="HARGA_UNIT" title="HARGA_UNIT" format="{0, number, #,##0.0000;(#,##0.0000)}"  sortable="true"/>                                        
											<display:column property="JUMLAH_UNIT" title="JUMLAH_UNIT" format="{0, number, #,##0.0000;(#,##0.0000)}"  sortable="true" total="true"/>
										</display:table>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<display:table style="width:760px;" id="rincian2" name="rincian2" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${uri}">
											<display:caption>Total Nilai Polis dengan NAB Terakhir</display:caption>
											<display:column property="JENIS" title="JENIS INVESTASI"  sortable="true"/>                                                                                            
											<display:column property="SALDO" title="SALDO UNIT TERAKHIR" format="{0, number, #,##0.00000;(#,##0.00000)}"  sortable="true"/>                                                  
											<display:column property="HARGA" title="NAB"  format="{0, number, #,##0.000;(#,##0.000)}"  sortable="true"/>                                                  
											<display:column property="NILAI_POLIS" title="NILAI POLIS"  format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true" total="true"/>                                                  
											<display:column property="INVEST" title="% INVEST" sortable="true"/>                                                
										</display:table>
									</td>
								</tr>
								<tr>
									<td colspan="2"> 
										<display:table style="width:760px;" id="rincian3" name="rincian3" class="displaytag" requestURI="${uri}">
											<display:caption>Rincian Transaksi</display:caption>
											<display:column title="SHOW">
												<input type="radio" name="centang" class="noBorder"/>
											</display:column>
											<display:column property="NO_REGISTER" title="NO REGISTER"  sortable="true"/>                                                                                
											<display:column property="LT_TRANSKSI" title="JENIS TRANSAKSI"  sortable="true"/>                                                                                
											<display:column property="MU_TGL_TRANS" title="TGL TRANS" format="{0, date, dd/MM/yyyy}"  sortable="true"/>                                               
											<display:column property="MU_JLH_PREMI" title="JUMLAH PREMI" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                    
											<display:column property="MU_JLH_UNIT" title="UNIT TRANSAKSI" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                      
											<display:column property="MU_TGL_INPUT" title="TGL INPUT" format="{0, date, dd/MM/yyyy hh:mm:ss}"  sortable="true"/>                                               
											<display:column property="MU_TAHUN_KE" title="THN"  sortable="true"/>                                                                                
											<display:column property="MU_LSPD_ID" title="POSISI"  sortable="true">
												${rincian3.first_name}
											</display:column>
										</display:table>
										<br>
										<input type="button" name="btnIlustrasi" value="Ilustrasi" onclick="tambah(this);" disabled>
										<input type="button" name="btnView" value="View" disabled>						
									</td>
								</tr>
								<tr>
									<td>
										<div style="width:320; height:400; overflow:auto;">
											<display:table style="width:300px;" id="baris" name="daftar" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}">
												<display:caption>NAV</display:caption>
													<display:column property="LJI_INVEST" title="Jenis Investasi"  sortable="true"/>                                                                                  
													<display:column property="TGL" title="Tanggal"  sortable="true"/>                                                                                        
													<display:column property="LNU_NILAI" title="Nilai" format="{0, number, #,##0.00000;(#,##0.00000)}"  sortable="true"/>                                          
											</display:table>
										</div>
									</td>
									<td>
										<cewolf:chart id="rup" title="NAV Excellink (Rupiah)" type="line" xaxislabel="Tanggal" yaxislabel="NAV">
										    <cewolf:colorpaint color="#EEEEFF"/>
										    <cewolf:data>
										        <cewolf:producer id="navRupiah" />
										    </cewolf:data>
										</cewolf:chart>
										<cewolf:img chartid="rup" renderer="/cewolf" width="440" height="200" />
				
										<cewolf:chart id="doll" title="NAV Excellink (Dollar)" type="line" xaxislabel="Tanggal" yaxislabel="NAV">
										    <cewolf:colorpaint color="#EEEEFF"/>
										    <cewolf:data>
										        <cewolf:producer id="navDollar" />
										    </cewolf:data>
										</cewolf:chart>
										<cewolf:img chartid="doll" renderer="/cewolf" width="440" height="200" />
									</td>
								</tr>
							</table>
						</form>
					</c:otherwise>
				</c:choose>

			</div>
		</div>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>