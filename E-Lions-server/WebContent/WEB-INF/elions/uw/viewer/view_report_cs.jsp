<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header_jquery.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />

<body style="height: 100%;" onload="setupPanes('container1', 'tab1'); setFrameSize('reportFrame', 65);" onresize="setFrameSize('reportFrame', 65);">
<c:choose>
    <c:when test="${empty access}">
        <div id="error">
            Maaf, Anda tidak memiliki akses untuk menu ini
        </div>
    </c:when>
    <c:otherwise>
        <div class="tab-container" id="container1">
            <ul class="tabs">
                <li>
                    <a href="#" onclick="return showPane('pane1', this)" id="tab1">Report Customer Service</a>
                </li>
                <li>
                    <a href="#" onclick="return showPane('pane2', this)" id="tab2">Result</a>
                </li>
            </ul>
            <form id="formpost" method="post" target="reportFrame" action="${path}/uw/viewer.htm?window=view_report_cs">
                <div class="tab-panes">
	                <div class="panes" id="pane1">
	                    <table class="entry2" style="width: 100%">
	                        <tr>
	                            <th>Jenis Report</th>
	                            <td>
	                                <select name="jenisReport">
	                                    <c:forEach var="report" items="${listReport}">
	                                        <option value="${report.key}">${report.value}</option>
	                                    </c:forEach>
	                                </select>
	                            </td>
	                        </tr>
	                        <tr>
	                            <th>Periode</th>
	                            <td>
	                                <!-- <input type="text" class="datepicker" name="begDate" size="11"> s/d 
	                                <input type="text" class="datepicker" name="endDate" size="11"> -->
	                                <script>
	                                    inputDate(
	                                        'begDate',
	                                        '${begDate}'
	                                    );
	                                </script>
	                                <script>
	                                    inputDate(
	                                        'endDate',
	                                        '${endDate}'
	                                    );
	                                </script>
	                            </td>
	                        </tr>
	                        <tr>
	                            <th></th>
	                            <td>
	                                <input type="submit" name="showPDF" id="showPDF" value="Show (PDF)" onclick="return tampilkan('pdf');">
	                                <input type="submit" name="showXLS" id="showXLS" value="Show (Excel)" onclick="return tampilkan('xls');">
	                                <input type="hidden" name="showReport">
	                                <input type="hidden" name="tipe">
	                            </td>
	                        </tr>
	                    </table>
	                </div>
	                <div class="panes" id="pane2">
	                    <table class="entry2">
	                        <tr>
	                            <th>
	                                <a href="#" onclick="return tampilkan('back');"
	                                    onmouseover="return overlib('Alt-B > Back', AUTOSTATUS, WRAP);" onmouseout="nd();" accesskey="B">
	                                    <img style="border:none;" alt="Back" src="${path}/include/image/action_back.gif"/></a>
	                            </th>
	                            <th>Format : </th>
	                            <th>
	                                <a href="#" onclick="return tampilkan('pdf');"
	                                    onmouseover="return overlib('Alt-P > View (PDF)', AUTOSTATUS, WRAP);" onmouseout="nd();" accesskey="P">
	                                    <img style="border:none;" alt="PDF" src="${path}/include/image/pdf.gif"/></a>
	                                &nbsp;
	                                <a href="#" onclick="return tampilkan('xls');"
	                                    onmouseover="return overlib('Alt-X > View (EXCEL)', AUTOSTATUS, WRAP);" onmouseout="nd();" accesskey="X">
	                                    <img style="border:none;" alt="EXCEL" src="${path}/include/image/excel.gif"/></a>
	                                &nbsp;
	                            </th>
	                        </tr>
	                        <tr>
	                            <th colspan="3">
	                                <iframe name="reportFrame" id="reportFrame" width="100%" src="${path}/report/uw.htm?window=loading_screen">
	                                    Please Wait...
	                                </iframe>
	                            </th>
	                        </tr>
	                    </table>
	                </div>
	            </div>
            </form>
        </div>
        <script>
            $(function() {
                $('.datepicker').datepicker({
                    showOn : 'both',
                    buttonImage : '${path}/include/image/calendar.jpg',
                    buttonImageOnly : true,
                    dateFormat : 'dd/mm/yy'
                });
            });
            
            function tampilkan(jenis) {
                showPane('pane2',   document.getElementById('tab2'));
                if(jenis == 'back') {
                    showPane('pane1', document.getElementById('tab1'));
                } else if(jenis == 'pdf') {
                    $('#formpost input[name=showForm]').val(1);
                    $('#formpost input[name=tipe]').val(jenis);
                    $('#formpost').submit();
                } else if(jenis == 'xls') {
                    $('#formpost input[name=showForm]').val(1);
                    $('#formpost input[name=tipe]').val(jenis);
                    $('#formpost').submit();
                }
                
                return false;
            }
        </script>
    </c:otherwise>
</c:choose>
</body>

<%@ include file="/include/page/footer.jsp"%>