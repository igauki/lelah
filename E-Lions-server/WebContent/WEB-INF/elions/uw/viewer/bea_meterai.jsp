<%@ include file="/include/page/header.jsp"%>

<script type="text/javascript">
	hideLoadingMessage();
	
//  Returns true if strNum  is only number

function isNumber( strNum ) {
	var intCounter=0 , intLoop;
	strNum = Trim(strNum);

	for (intLoop = 0; intLoop < strNum.length; intLoop++) { // 0-9 only
		if (((strNum.charAt(intLoop) < "0") || (strNum.charAt(intLoop) > "9")
				))
			return false;
	}

	return true; // value is 0
}

function Right(str, n){
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}


function DateAdd(objDate, intDays)
{
  var iSecond=1000;	 // Dates are represented in milliseconds
  var iMinute=60*iSecond;
  var iHour=60*iMinute;
  var iDay=24*iHour;
  var objReturnDate=new Date();
  objReturnDate.setTime(objDate.getTime()+(intDays*iDay));
  return objReturnDate;
}

function listtgl()
{
	var tanggal_beg = document.frmParam.tgl1.value;
	
	var arrMonthDays = new Array (	"31", "29", "31",
					"30", "31", "30",
					"31", "31", "30",
					"31","30","31"	  );

		var tahun_s = tanggal_beg.substring(6,10);
		var bulan_s = tanggal_beg.substring(3,5);

	var tanggal_awal = "01/"+bulan_s+"/"+tahun_s;
	document.frmParam._tgl1.value = tanggal_awal;
	document.frmParam.tgl1.value = tanggal_awal;
}

function cari(str){

	switch (str) {
	case "permohonan" : 
		document.getElementById('infoFrame').src='${path}/uw/viewer/bea_meterai.htm?window=permohonan'; 
		break;
	case "estimasi" :
		document.getElementById('infoFrame').src='${path}/uw/viewer/bea_meterai.htm?window=estimasi'; 
		break;
	case "bubuh" :
		document.getElementById('infoFrame').src='${path}/uw/viewer/bea_meterai.htm?window=bubuh'; 
		break;
	case "saldo" :
		document.getElementById('infoFrame').src='${path}/uw/viewer/bea_meterai.htm?window=saldo'; 
		break;
	case "produk" :
		document.getElementById('infoFrame').src='${path}/uw/viewer/bea_meterai.htm?window=produk'; 
		break;		
	case "penggunaan" :
		document.getElementById('infoFrame').src='${path}/uw/viewer/bea_meterai.htm?window=penggunaan'; 
		break;
	case "uploadbeajs" :
		document.getElementById('infoFrame').src='${path}/uw/viewer/bea_meterai.htm?window=uploadbeajs'; 
		break;	
	}
}

function jns(hasil)
{
  if (hasil == "lama")
  {
  		document.frmParam.kode.readOnly = false;
  		document.frmParam.kode.disabled = false;
		document.frmParam.kode.style.backgroundColor ='#FFFFFF';		
		
		enableDisableDate('tgl1');
  		document.frmParam.tgl1.style.backgroundColor ='#D4D4D4';
		document.frmParam.tgl1.value="__/__/____";
		
		document.frmParam._tgl1.style.backgroundColor ='#D4D4D4';
		document.frmParam._tgl1.value="__/__/____";

		enableDisableDate('tgl4');
		document.frmParam.tgl4.style.backgroundColor ='#D4D4D4';
		document.frmParam.tgl4.value="__/__/____";
		
		document.frmParam._tgl4.readOnly = true;
		document.frmParam._tgl4.disabled = true;
		document.frmParam._tgl4.style.backgroundColor ='#D4D4D4';
		document.frmParam._tgl4.value="__/__/____";
		
  		document.frmParam.saldo.readOnly = true;
		document.frmParam.saldo.style.backgroundColor ='#D4D4D4';
		document.frmParam.saldo.value="";

  }else{
    	document.frmParam.kode.readOnly = true;
    	document.frmParam.kode.disabled = true;
		document.frmParam.kode.style.backgroundColor ='#D4D4D4';
		document.frmParam.kode.value="0";					
  	
  		document.frmParam._tgl1.readOnly = false;
  		document.frmParam._tgl1.disabled = false;
		document.frmParam._tgl1.style.backgroundColor ='#FFFFFF';
		
  		enableDisableDate('tgl1');
		document.frmParam.tgl1.style.backgroundColor ='#FFFFFF';
		
  		document.frmParam._tgl4.readOnly = false;
  		document.frmParam._tgl4.disabled = false;
		document.frmParam._tgl4.style.backgroundColor ='#FFFFFF';
		
  		enableDisableDate('tgl4');
		document.frmParam.tgl4.style.backgroundColor ='#FFFFFF';		
		
  		document.frmParam.saldo.readOnly = false;
		document.frmParam.saldo.style.backgroundColor ='#FFFFFF';
 }
}

function caridaftar()
{
	alert("t");
	var jenis = document.frmParam.jenis;
	var jenis_ = 'baru';
	for(i=0; i<jenis.length; i++) if(jenis[i].checked) jenis_ = jenis[i].value;
	document.frmParam.jenis_bea.value=jenis_;

	if (document.frmParam.jenis_bea.value == "lama")
	{
		if (document.frmParam.kode.value == "0")
		{
			alert('Silahkan pilih kode yang tersedia terlebih dahulu.');
		}else{
			var formats = document.frmParam.formats;
			var format_ = 'pdf';
			for(i=0; i<formats.length; i++) if(formats[i].checked) format_ = formats[i].value;
			document.frmParam.bentuk.value=format_;

			frmParam.submit();
		}
	}else{
	
	 	if ( document.frmParam.tgl1.value=="__/__/____" || document.frmParam.tgl1.value=="" )
		{
			alert('Silahkan isi tanggal periode terlebih dahulu');
		}else if ( document.frmParam.tgl4.value=="__/__/____" || document.frmParam.tgl4.value=="" )
			{
				alert('Silahkan isi tanggal pembayaran terlebih dahulu');
			}else if (document.frmParam.saldo.value=="")
				{
					alert('Silahkan isi bea meterai terlebih dahulu'); 
				}else{
					if (! isNumber( document.frmParam.saldo.value ) )
					{
						alert('Silahkan isi bea meterai  dalam bentuk numerik saja');
					}else{
						var formats = document.frmParam.formats;
						var format_ = 'pdf';
						for(i=0; i<formats.length; i++) if(formats[i].checked) format_ = formats[i].value;
						document.frmParam.bentuk.value=format_;
			
						frmParam.submit();
					}
				}
	}
}	

function caridaftar3()
{
	if (document.frmParam.kode.value == "0")
	{
		alert('Silahkan pilih kode yang tersedia terlebih dahulu.');
	}else if ( document.frmParam.tgl4.value=="__/__/____" || document.frmParam.tgl4.value=="" )
		{
			alert('Silahkan isi tanggal pembayaran terlebih dahulu');
		}else if (document.frmParam.saldo.value=="")
			{
				alert('Silahkan isi bea meterai terlebih dahulu'); 
			}else{
				if (! isNumber( document.frmParam.saldo.value ) )
				{
					alert('Silahkan isi bea meterai  dalam bentuk numerik saja');
				}else{
					var formats = document.frmParam.formats;
					var format_ = 'pdf';
					for(i=0; i<formats.length; i++) if(formats[i].checked) format_ = formats[i].value;
					document.frmParam.bentuk.value=format_;
			
					frmParam.submit();
			}
	}
}	

function caridaftar1()
{
	if (document.frmParam.kode.value == "0")
	{
		alert('Silahkan pilih kode yang tersedia terlebih dahulu.');
	}else{
		var formats = document.frmParam.formats;
		var format_ = 'pdf';
		for(i=0; i<formats.length; i++) if(formats[i].checked) format_ = formats[i].value;
		document.frmParam.bentuk.value=format_;
		frmParam.submit();
	}
}	

function caridaftar2()
{

	if (document.frmParam.kode.value == "0")
	{
		alert('Silahkan pilih kode yang tersedia terlebih dahulu.');
	}else{
		popWin('${path}/uw/viewer/editkodedirjen.htm?kode='+document.frmParam.kode.value, 500, 800);
	}
}

function caridaftar4()
{
 	if ( document.frmParam.tgl1.value=="__/__/____" || document.frmParam.tgl1.value=="" ||  document.frmParam.tgl2.value=="__/__/____" || document.frmParam.tgl2.value=="")
		{
			alert('Silahkan isi tanggal terlebih dahulu');
		}else{
			var tanggal_beg = document.frmParam.tgl1.value;
			var tahun_s = tanggal_beg.substring(6,10);
			var bulan_s = tanggal_beg.substring(3,5);
			var tanggal_s = tanggal_beg.substring(0,2);
			var tanggal1  = tahun_s+bulan_s+tanggal_s;
			var tanggal_end = document.frmParam.tgl2.value;
			tahun_s = tanggal_end.substring(6,10);
			bulan_s = tanggal_end.substring(3,5);
			tanggal_s = tanggal_end.substring(0,2);
			var tanggal2  = tahun_s+bulan_s+tanggal_s;
			
			var formats = document.frmParam.formats;
			var format_ = 'pdf';
			for(i=0; i<formats.length; i++) if(formats[i].checked) format_ = formats[i].value;
			document.frmParam.bentuk.value=format_;
	
			frmParam.submit();
		}
}


function DateAdd(objDate, intDays)
{
  var iSecond=1000;	 // Dates are represented in milliseconds
  var iMinute=60*iSecond;
  var iHour=60*iMinute;
  var iDay=24*iHour;
  var objReturnDate=new Date();
  objReturnDate.setTime(objDate.getTime()+(intDays*iDay));
  return objReturnDate;
}

function listtgl()
{
	var tanggal_beg = document.frmParam.tgl1.value;
	
	var arrMonthDays = new Array (	"31", "29", "31",
					"30", "31", "30",
					"31", "31", "30",
					"31","30","31"	  );

		var tahun_s = tanggal_beg.substring(6,10);
		var bulan_s = tanggal_beg.substring(3,5);
		var tanggal_s = tanggal_beg.substring(0,2);
		var tanggal  = tanggal_s+"/"+bulan_s+"/"+tahun_s;
		
	if ((bulan_s==2)&&  (tahun_s%4>0) ) {
	    tanggal_s=28;
	}else{
		tanggal_s=arrMonthDays[bulan_s-1];
	}	
	tanggal  = tanggal_s+"/"+bulan_s+"/"+tahun_s;

	//document.frmParam._tgl2.value=tanggal;
	document.frmParam.tgl2.value=tanggal;
	document.frmParam.tgl3.value=tanggal;

}

function listtgl2()
{
	tanggal = document.frmParam._tgl2.value;
	document.frmParam._tgl2.value=tanggal;
	document.frmParam.tgl2.value=tanggal;
	document.frmParam.tgl3.value=tanggal;
}	
	
</script>
<body style="height: 100%;"
	onload="setFrameSize('infoFrame', 68); "
	onresize="setFrameSize('infoFrame', 68);">
<form name="frmParam" method="post">

<div class="tabcontent">
<c:if test="${kunci eq null}"> 
<table class="entry" width="98%">
	<tr>
		<th> Window Bea Meterai</th>
		<td>
		 <input type="button" value="SURAT PERMOHONAN BEA METERAIx" name="search" onClick="cari('permohonan');"> 
		 <input type="button" value="ESTIMASI DAN REALISASI BEA METERAI" name="search2" onClick="cari('estimasi');">
		 <input type="button" value="SURAT PERMOHONAN TAMBAHAN" name="search3" onClick="cari('bubuh');">
		 <input type="button" value="SALDO TERKINI" name="search4" onClick="cari('saldo');">
<!--		 <input type="button" value="PEMAKAIAN BEA METERAI PER PRODUK" name="search5" onClick="cari('produk');">-->
		  <input type="button" value="PEMAKAIAN BEA METERAI PER PRODUK" name="search6" onClick="cari('penggunaan');">
		  <input type="button" value="UPLOAD BEA MATERAI" name="uploadbea" onClick="cari('uploadbeajs');">
		</td>
		<td><div id="prePostError" style="display:none;"></div></td>
	</tr>
	<tr>
		<td colspan="3">
			<iframe src="${path}/uw/viewer/bea_meterai.htm?window=main" name="infoFrame" id="infoFrame"
				width="100%"> Please Wait... </iframe>
		</td>
	</tr>
</table>
</c:if>
<c:if test="${kunci eq \"permohonan\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  >SURAT PERMOHONAN BEA METERAIx
	   </th>
      </tr>
      <tr>
      	<th>Pilihan : </th>
      	<th><input type="radio" name="jenis" value="lama" class="noborder" onClick="jns('lama');"  <c:if test="${jenis eq \"lama\"}"> checked </c:if>> Data Lama
			<input type="radio" name="jenis" value="baru" class="noborder" onClick="jns('baru');" <c:if test="${jenis eq null or jenis eq \"baru\"}"> checked </c:if>> Data Baru
			<input type="hidden" name="jenis_bea" value="${jenis_bea}"> 
	</th>
      </tr>
  <tr> 
        <th >Kode</th>
        <th>
              <select name="kode" tabindex="14"  <c:if test="${jenis eq null or jenis eq \"baru\"}"> readOnly  disabled style='background-color :#D4D4D4' </c:if> >
              		<option value="0">NONE</option> 
					<c:forEach var="r" items="${listkode}" varStatus="perush"> 
					<option value="${r.MSTM_KODE}">[${r.MSTM_BULAN}] ${r.MSTM_KODE}</option> 
					</c:forEach> 
			</select>
		</th>
      </tr>  
  <tr> 
        <th >Periode Permohonan</th>
        <th>
              <script>inputDate('tgl1', '${tgl1}', false,'');</script>
		</th>
      </tr> 
  <tr> 
        <th >Tanggal Pembayaran</th>
        <th>
              <script>inputDate('tgl4', '${tgl4}', false,'');</script>
		</th>
      </tr>       
  <tr> 
        <th >Saldo yang Dibayarkan</th>
        <th>
             <input type="text" name="saldo" value="${saldo}" >
             <input type="hidden" name="status" value="${status}">
		</th>
      </tr>       
      <tr> 
        <th >Format</th>
        <th>
			<input type="radio" name="formats" value="pdf" class="noborder" checked="checked"> PDF
			<input type="radio" name="formats" value="xls" class="noborder"> Excel
			<input type="radio" name="formats" value="html" class="noborder"> HTML
			<input type="hidden" name="bentuk" >
		</th>
      </tr>           
      <tr> 
        <th colspan="2" >
        	<input type="button" value="Submitx" name="search" onClick="caridaftar();"> 
			<input type="button" value="Uploadx" name="upload" onClick="uploadmaterai();"> 
		</th>
      </tr>
      <tr> 
        <td colspan="2"><c:if test="${status eq \"batal\"}">
				Bea meterai untuk periode tersebut sudah ada, silahkan memilih jenis data lama dan kode bea meterai yang sudah ada.			
			</c:if>
			<c:if test="${status eq \"gagal\"}">
				Pembuatan "Surat Permohonan Pembubuhan Tanda Bea Meterai Lunas" gagal.			
			</c:if>
			 </td>
      </tr>
    </table>
</c:if>
<c:if test="${kunci eq \"uploadbeajs\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  >UPLOAD BEA METERAI
	   </th>
      </tr>
	  <tr>
			<th>Attach New File:</th>
			<td>
				<input type="file" name="file1" size="50" />
			</td>
	  </tr>
      <tr> 
        <th >Format</th>
        <th>
			<input type="radio" name="formats" value="pdf" class="noborder" checked="checked"> PDF
			<input type="radio" name="formats" value="xls" class="noborder"> Excel
			<input type="radio" name="formats" value="html" class="noborder"> HTML
			<input type="hidden" name="bentuk" >
		</th>
      </tr>           
      <tr> 
        <th colspan="2" ><input type="button" value="Submit" name="search" onClick="caridaftar();"> 
        &nbsp;&nbsp;&nbsp;<input type="button" value="Edit Kode Dirjen" name="search1" onClick="caridaftar2();"> 
        
		</th>
      </tr>
    </table>
</c:if>
<c:if test="${kunci eq \"estimasi\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  >ESTIMASI DAN REALISASI PEMAKAIAN BEA METERAI
	   </th>
      </tr>
  <tr> 
        <th >Kode</th>
        <th>
              <select name="kode" tabindex="14" >
              		<option value="0">NONE</option> 
					<c:forEach var="r" items="${listkode}" varStatus="perush"> 
					<option value="${r.MSTM_KODE}">[${r.MSTM_BULAN}] ${r.MSTM_KODE}</option> 
					</c:forEach> 
			</select>
		</th>
      </tr>     
      <tr> 
        <th >Format</th>
        <th>
			<input type="radio" name="formats" value="pdf" class="noborder" checked="checked"> PDF
			<input type="radio" name="formats" value="xls" class="noborder"> Excel
			<input type="radio" name="formats" value="html" class="noborder"> HTML
			<input type="hidden" name="bentuk" >
		</th>
      </tr>           
      <tr> 
        <th colspan="2" ><input type="button" value="Submit" name="search" onClick="caridaftar1();"> 
        &nbsp;&nbsp;&nbsp;<input type="button" value="Edit Kode Dirjen" name="search1" onClick="caridaftar2();"> 
        
		</th>
      </tr>
    </table>
</c:if>
<c:if test="${kunci eq \"bubuh\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  >SETORAN PEMBUBUHAN TANDA BEA METERAI LUNAS
	   </th>
      </tr>
  <tr> 
        <th >Kode</th>
        <th>
              <select name="kode" tabindex="14" >
              		<option value="0">NONE</option> 
					<c:forEach var="r" items="${listkode}" varStatus="perush"> 
					<option value="${r.MSTM_KODE}">[${r.MSTM_BULAN}] ${r.MSTM_KODE}</option> 
					</c:forEach> 
			</select>
		</th>
      </tr> 
  <tr> 
        <th >Tanggal Setoran</th>
        <th>
              <script>inputDate('tgl4', '${tgl4}', false,'');</script>
		</th>
      </tr> 
  <tr> 
        <th >Pembubuhan Bea Meterai</th>
        <th>
             <input type="text" name="saldo" value="${saldo}" >
             <input type="hidden" name="status" value="${status}">
		</th>
      </tr>         
      <tr> 
        <th >Format</th>
        <th>
			<input type="radio" name="formats" value="pdf" class="noborder" checked="checked"> PDF
			<input type="radio" name="formats" value="xls" class="noborder"> Excel
			<input type="radio" name="formats" value="html" class="noborder"> HTML
			<input type="hidden" name="bentuk" >
		</th>
      </tr>           
      <tr> 
        <th colspan="2" ><input type="button" value="Submit" name="search" onClick="caridaftar3();"> 
        </th>
      </tr>
     <tr> 
        <td colspan="2"><c:if test="${status eq \"batal\"}">
				Proses simpan data pembubuhan tanda bea meterai batal karena kode dirjen belum ada.			
			</c:if>
			<c:if test="${status eq \"gagal\"}">
				Pembuatan "Surat Setoran Pembubuhan Tanda Bea Meterai Lunas" gagal.			
			</c:if>
			 </td>
      </tr>
    </table>
</c:if>

<c:if test="${kunci eq \"saldo\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th>SALDO TERKINI
	   </th>
      </tr>
  	<tr> 
        <th>PERIODE     :  ${mstm_bulan}</th>
      </tr> 
 	 <tr> 
        <th >SALDO AWAL  : Rp.  <fmt:formatNumber type="number" value="${saldo_awal}"/></th>
      </tr>     
 	 <tr> 
        <th >SALDO AKHIR : Rp.  <fmt:formatNumber type="number" value="${saldo_akhir}"/></th>
      </tr>     
    </table>
</c:if>
<c:if test="${kunci eq \"produk\"}"> 
   <table class="entry" width="98%">
      <tr> 
        <th colspan="2"  >PEMAKAIAN BEA METERAI PER PRODUK
	   </th>
      </tr>
 	 <tr> 
        <th >Periode</th>
        <th>
              <script>inputDate('tgl1', '', false,'listtgl();');</script>
		
			s/d
              <script>inputDate('tgl2', '', false,'listtgl2();');</script>
			<input type="hidden" name="tgl3">
		</th>
      </tr>       
       <tr> 
        <th >Format</th>
        <th>
			<input type="radio" name="formats" value="pdf" class="noborder" checked="checked"> PDF
			<input type="radio" name="formats" value="xls" class="noborder"> Excel
			<input type="radio" name="formats" value="html" class="noborder"> HTML
			<input type="hidden" name="bentuk" >
		</th>
      </tr>           
      <tr> 
        <th colspan="2" ><input type="button" value="Submit" name="search" onClick="caridaftar4();"> 
		</th>
      </tr>
    </table>
</c:if>
</div>
</form>
</body>

<%@ include file="/include/page/footer.jsp"%>