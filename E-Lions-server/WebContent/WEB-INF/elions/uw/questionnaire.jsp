<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	function awal(){
		var pesan = '${cmd.pesan}';
		if(pesan != ''){alert(pesan);}
	}
</script>
</head>
<BODY onload="setupPanes('container1', 'tab1'); awal();" style="height: 100%;">
 
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Validasi Random Sampling Kuesioner BII</a>
			</li>
		</ul>

		<div class="tab-panes">
		
			<div id="pane1" class="panes">
				<form:form name="formpost" id="formpost" commandName="cmd">
					<form:hidden path="reg_spaj"/>
					<spring:hasBindErrors name="cmd">
						<div id="error">
							<strong>Mohon lengkapi data-data berikut:</strong>
							<br />
							<form:errors path="*" delimiter="<br>" />
						</div>
					</spring:hasBindErrors>

					<fieldset>
						<legend>Daftar Pertanyaan untuk SPAJ ${cmd.daftarQuestionnaire[0].reg_spaj}</legend>
						<table class="entry2">
							<tr>
								<th rowspan="2">No</th>
								<th rowspan="2">Pertanyaan</th>
								<th colspan="2">Sesuai dengan Isian SPAJ</th>
								<th rowspan="2">Keterangan<br>(diisi, jika jawaban "tidak sesuai" dengan SPAJ)</th>
							</tr>
							<tr>
								<th>Ya</th>
								<th>Tidak</th>
							</tr>
							<c:forEach items="${cmd.daftarQuestionnaire}" var="q" varStatus="s">
								<tr>
									<td>${s.count}<form:hidden path="daftarQuestionnaire[${s.index}].lsqu_id"/></td>
									<td><span style="text-transform: none;">${q.lsqu_desc}</span></td>
									<td><form:radiobutton id="jawaban1_${s.index}" path="daftarQuestionnaire[${s.index}].msqu_jawab" cssClass="noBorder" value="1"/></td>
									<td><form:radiobutton id="jawaban0_${s.index}" path="daftarQuestionnaire[${s.index}].msqu_jawab" cssClass="noBorder" value="0"/></td>
									<td><form:textarea path="daftarQuestionnaire[${s.index}].msqu_desc" cols="40" rows="3" onkeyup="textCounter(this, 200);" onkeydown="textCounter(this, 200);" cssErrorClass="inpError"/></td>
								</tr>
							</c:forEach>
						</table>
					</fieldset>
					<input type="submit" name="save" value="Simpan" onclick="return confirm('Simpan?');">
				</form:form>
			</div>
			
		</div>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>