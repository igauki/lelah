<%@ include file="/include/page/header.jsp"%>
<script>
	function awal(){
		var info=framepost.info.value;
		//if(info!=0)
		//	window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
		
		if(info==1){
			framepost.btnOk.disabled=true;
			alert("Posisi SPaj ada di "+framepost.posisi.value);
		}else if(info==2){
			framepost.btnOk.disabled=true;
			alert("Polis ASM");
			window.location='${path }/uw/viewer.htm?window=akseptasi_asm&spaj=${cmd.spaj}';	
		}else if(info==3){
			alert(framepost.pesan.value);
			document.getElementById('statAksep').options[0].selected;
			//framepost.statAksep.selectedIndex=1;
		}else if(info==4){
//			alert("WAJIB melampirkan general checkup terakhir");
		}
		//jika proses submit berhasil dan 
		if(framepost.hasil.value==0 && framepost.nopolis.value!=''){
			framepost.btnOk.disabled=true;
			alert("No Polis "+framepost.nopolis.value);
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
		}
	}


	function aksep(){
		framepost.proses.value="1";
		framepost.submit();
	}
	
	function cekStatus(flag){	
		lssa_id = flag;	
		bancass1="${team }";
		if(lssa_id=="8"){
			if("1"=="${cmd.flag_investasi}"){
				alert("Mohon dilakukan underwrite manual karena dana investasi dialokasikan setelah akseptasi");
			}
		}
		if(lssa_id=="10" && bancass1=="1"){
			
				alert("Pastikan sudah ada memo approval  GM Bancass 1");
			
		}
		
		ajaxSelectWithParam(lssa_id,'selectLstStatusAcceptSub','statusacceptsub','substatus','','SUB_ID','SUB_DESC','');
		//window.location= '${path}/uw/status.htm?lssa_id='+statusid;
		document.getElementById('substatus').style.visibility = "visible";
		
		
	}
	
	
</script>

<c:choose>
<c:when test="${not empty cmd.block}" >
	<div id="error" style="text-align:center; text-transform:uppercase;">${cmd.block }</div>
</c:when>
<c:otherwise>
	
<form name="framepost"  method="post">
<body onload="awal();">
	<table class="entry" width="60%">
	 	<span class="subtitle">Status</span> 
			<tr>
				<th>Tanggal	</th>		
				<th>Akseptor	</th>		
				<th>Status	</th>		
				<th>Keterangan	</th>		
			</tr>	   
			<c:forEach var="s" items="${cmd.lsStatusAksep}" varStatus="xt">
				<tr>
					<spring:bind path="cmd.lsStatusAksep[${xt.index }].msps_date">
						<td>${status.value }</td>
					</spring:bind>	
					<td>${s.lus_login_name}</td>
					<td>
						<c:choose>
	       					<c:when test="${xt.index < cmd.size-1}" > 
								<select name="statAksep"  id="statAksep" 
								 <c:if test="${ xt.index ne (cmd.listSize-1)}">disabled</c:if> 
								>
									<option />
									<c:forEach var="x" items="${lsStatus}">
										<option value="${x.LSSA_ID}"
											<c:if test="${s.lssa_id eq  x.LSSA_ID }">Selected</c:if>>
												 ${x.STATUS_ACCEPT}
										</option>
									</c:forEach>
								</select>
								
								<c:if test="${s.lssa_id eq 5}">
								<br/>
								<select name="substatus"  
								 <c:if test="${ xt.index ne (cmd.listSize-1)}">disabled</c:if> 
								 onchange="cekStatus(this.value);">
									<c:forEach var="x" items="${lsStatusAksepSub}">
										<option value="${x.SUB_ID}"
											<c:if test="${s.sub_id eq  x.SUB_ID }">Selected</c:if>>
											${x.SUB_DESC}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</option>
									</c:forEach>
								</select>
								</c:if>			
						
								
							</c:when>
							<c:otherwise>
								<select name="statAksep"  
								 <c:if test="${ xt.index ne (cmd.listSize-1)}">disabled</c:if> 
								 onchange="cekStatus(this.value);">
									<c:forEach var="x" items="${lsStatusAksep}">
										<option value="${x.LSSA_ID}"
											<c:if test="${s.lssa_id eq  x.LSSA_ID }">Selected</c:if>>
											${x.STATUS_ACCEPT}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</option>
									</c:forEach>
								</select>
								
								<c:if test="${s.lssa_id eq 3 or s.lssa_id eq 5}">	
									<br>
									<div id="statusacceptsub">
										<select name="substatus" id="substatus" >
						                  <option value="${substatus.SUB_ID}">${substatus.sub_desc}</option>
						                </select>
					                </div>
								</c:if>
								<script type="text/javascript">
									cekStatus('${s.lssa_id}');
								</script>
								
							</c:otherwise>
						</c:choose>
							
					</td>
					<td><textarea 
					 <c:if test="${ xt.index ne (cmd.listSize-1) }">disabled</c:if> 
					 name=txtmsps_desc cols=60  rows="5"><c:if test="${ s.lssa_id eq 5 }">${cmd.substandart }</c:if> ${s.msps_desc }</textarea></td>
					
				</tr>
			</c:forEach>
			<tr>
		      <td colspan="4"><div align="center">
	    			<input type="button" name="btnOk" value="Ok" onclick="aksep()">
					<input type="hidden" name="info" value="${cmd.info1}">
					<input type="hidden" name="pesan" value="${cmd.pesan}">
					<input type="hidden" name="info2" value="${cmd.info2}">
					<input type="hidden" name="posisi" value="${cmd.lsposDoc}">
					<input type="hidden" name="proses" value="0">
					<input type="hidden" name="hasil" value="${hasil}">
					<input type="hidden" name="nopolis" value="${nopolis}">					
			  </div></td>
			</tr>
			<tr>
				 <td colspan="4">
					<c:if test="${submitSuccess eq true}">
			        	<div id="success">
				        	Berhasil
			        	</div>
			        </c:if>	
		  			<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
								Informasi:<br>
									<c:forEach var="error" items="${status.errorMessages}">
												- <c:out value="${error}" escapeXml="false" />
										<br/>
									</c:forEach>
							</div>
						</c:if>									
					</spring:bind>
				</td>
			</tr>
		</table>
</body>                                                                                   
</form>  
</c:otherwise>
</c:choose>
<%@ include file="/include/page/footer.jsp"%>