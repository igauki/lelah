<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		$("#tabs").tabs();
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		$("#company").hide();
		$("#company1").hide();
		$("#company2").hide();
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}	
		// user
		$("#dist").change(function() {
			var s=$("#dist").val();
			
			
			if(s ==00 || s==05 || s==04){
			 	$("#company").hide();
				$("#company").empty();
				
			}else{
				 $("#company").show();
				 $("#company").empty();
				 var url = "${path}/uw/upload_excel.htm?window=handleRequest&json=1&lca=" +$("#dist").val();
				 alert(url);			
				 $.getJSON(url, function(result) {
					$("<option/>").val("ALL").html("ALL").appendTo("#company");
					$.each(result, function() {
						$("<option/>").val(this.key).html(this.value).appendTo("#company");
					});
					$("#company option:first").attr('selected','selected');
				});	
			
			//kopi untuk disubmit (isinya, bukan valuenya)
			$("#distribusi").val($("#dist option:selected").text());			
			
 			
			    
			}
			
			
	
		});
		
		$("#dist1").change(function() {
			var z=$("#dist1").val();
			
			
			if(z ==00 || z==05 || z==04){
			 	$("#company1").hide();
				$("#company1").empty();
				
			}else{
				 $("#company1").show();
				 $("#company1").empty();
				 var url = "${path}/uw/upload_excel.htm?window=handleRequest&json1=1&lca1=" +$("#dist1").val();
							
				 $.getJSON(url, function(result) {
					$("<option/>").val("ALL").html("ALL").appendTo("#company1");
					$.each(result, function() {
						$("<option/>").val(this.key).html(this.value).appendTo("#company1");
					});
					$("#company1 option:first").attr('selected','selected');
				});	
			
			//kopi untuk disubmit (isinya, bukan valuenya)
 			$("#distribusi1").val($("#dist1 option:selected").text());		
 	         
 	        }		
	
		});
		
		$("#dist2").change(function() {
			var x=$("#dist2").val();
			
			if(x ==00 || x==05 || x==04){
			 	$("#company2").hide();
				$("#company2").empty();
				
			}else{
				 $("#company2").show();
				 $("#company2").empty();
				 var url = "${path}/uw/upload_excel.htm?window=handleRequest&json1=1&lca1=" +$("#dist2").val();
							
				 $.getJSON(url, function(result) {
					$("<option/>").val("ALL").html("ALL").appendTo("#company2");
					$.each(result, function() {
						$("<option/>").val(this.key).html(this.value).appendTo("#company2");
					});
					$("#company2 option:first").attr('selected','selected');
				});	
			
			//kopi untuk disubmit (isinya, bukan valuenya)
 			$("#distribusi2").val($("#dist2 option:selected").text());		
 	         
 	        }		
	
		});

		$("#upload").click(function() {
			var file=$("#file1").val();
			var dist=$("#dist").val();
			var cmpny=$("#company").val();
			
		    if(file=="" || file==null){
					 alert('Silahkan pilih file terlebih dahulu');
					 return false;
			}else{
				 	$("#upload").val("Sedang Diproses, Harap Menunggu. . . . . !");
				 	return true;
				}
			
			
	
		});
		
		$("#showPolis").click(function() {
			//var file=$("#file1").val();
			var dist=$("#dist2").val();
			var cmpny=$("#company2").val();
			var sts=$("#st_polis").val();
		});
		

		$("#company").click(function() {
			//kopi untuk disubmit (isinya, bukan valuenya)
			$("#company2").val($("#company option:selected").text());
		});	
		$("#company1").click(function() {
			//kopi untuk disubmit (isinya, bukan valuenya)
			$("#company3").val($("#company1 option:selected").text());
		});	
		$("#company2").click(function() {
			//kopi untuk disubmit (isinya, bukan valuenya)
			$("#company3").val($("#company2 option:selected").text());
		});	
			var index = $('div#tabs li a').index($('a[href="#tab-${showTab}"]').get(0));
				$('div#tabs').tabs({selected:index});
				
	  
		
	});
	
	
	
</script>


<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

</style>

<body>
		<div id="tabs">		
				<ul>
					<li><a href="#tab-1">Upload Data </a></li>
					<li><a href="#tab-3">Hasil Upload</a></li>
					
					
				</ul>
				<div id="tab-1">
				
									<form id="formPost" name="formPost" method="post"   enctype="multipart/form-data">
													<fieldset class="ui-widget ui-widget-content"> 
													<legend class="ui-widget-header ui-corner-all"><div>Info Dalam Tata Cara UPLOAD</div></legend> 
					 									<div class="rowElem">						
					 											<ul> 
					 												<li>Format Tanggal Harus dd/mm/yyy , contoh : 01/01/2001</li> 
					 												<li>Jangan sampai ada kolom/baris yang kosong</li> 
																
					 											</ul>		
					 									   </div> 
													   </fieldset>
					 								<div class="rowElem"> 
															<label>File Excel(*.xls) :</label> 
					 										<input type="file" name="file1" id="file1" size="70" />
															<input type="hidden" name="file_fp" id="file_fp"/>	
					 										<input type="submit" name="upload" id="upload" value="Upload">
					 										
															<input type="hidden" name="distribusi" id="distribusi" value="ALL">	 
															<input type="hidden" name="company" id="company2 value="ALL">	 
													</div>	 
										<div class="rowElem"> 
											
												<c:forEach items="${pesanList}" var="d" varStatus="st">
												<c:choose>
												<c:when test="${d.sts eq 'ERROR'}">
												<font color="red">${st.index+1}. ${d.msg}</font>
												</c:when>
												<c:otherwise>
												<font color="blue">${st.index+1}. ${d.msg}</font>
												</c:otherwise>
												</c:choose>
					
												<br/>
												</c:forEach>
										</div>	 	
								</form>		       
				
				</div>					
				<div id="tab-3">
						<form id="formPost2" name="formPost2" method="post" target=""  >									
												<fieldset class="ui-widget ui-widget-content">	
												    <div class="rowElem"> 
															<input type="submit" name="showPolis" id="showPolis" value="Show Polis">
																			 										
															<input type="hidden" name="distribusi2" id="distribusi2" value="ALL">	 
															<input type="hidden" name="company2" id="company3" value="ALL">	
															<input type="hidden" name="showPolicy" value="1"> 
													</div> 	
													
											</fieldset>
											
										    <fieldset class="ui-widget ui-widget-content">

											        <table class="displaytag" cellpadding='0' cellspacing='0' border="1" align="center">
														<tr>															
															<th nowrap align="center" bgcolor="#b7b7b7">Check</th>
															<th nowrap align="center" width="100" bgcolor="#b7b7b7">Tanggal Proses</th>
															<th nowrap align="center" width="140" bgcolor="#b7b7b7">No.Polis</th>																													
															<th nowrap align="center" width="200" bgcolor="#b7b7b7">Produk</th>
															<th nowrap align="center" width="70" bgcolor="#b7b7b7">Jumlah</th>
															<th nowrap align="center" width="70" bgcolor="#b7b7b7">Tax</th>
															<th nowrap align="center" width="110" bgcolor="#b7b7b7">Jenis</th>
															<th nowrap align="center" width="160" bgcolor="#b7b7b7">Nomor Upload</th>
															 <c:if test="${stpolis eq '1'}">
															 	<th  width="100" bgcolor="#b7b7b7">Action</th>
															 </c:if>
														</tr>
														<c:forEach items="${dbpolis}" var="d">													
														<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';return true;" onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;">															
															<td align="center"><input type="checkbox" class="noBorder" name="chbox" value="${d.MUA_POLICY_NO}" id="chbox"></td>
															<td nowrap>${d.MUA_TGL_PROSES}</td>
															<td nowrap>${d.MUA_POLICY_NO}</td>
															<td nowrap>${d.MUA_PRODUCT_NAME}</td>
															<td nowrap>${d.MUA_JUMLAH}</td>		
															<td nowrap>${d.MUA_TAX}</td>														
															<td nowrap>${d.MUA_STATUS}</td>	
															<td nowrap>${d.MUA_KLOTER}</td>														
															<c:if test="${stpolis eq '1'}">
															   <td>
																 	<input type="submit" name="Transfer" id="Transfer" value="Transfer" onclick="showSPAJ('${d.MUA_POLICY_NO}');">
																 	<input type="hidden" name="polisnya" id="polisnya" value="${d.MUA_POLICY_NO}">
																 	<input type="hidden" name="kloternya" id="kloternya" value="${d.MUA_KLOTER}">
															 	</td>
															 </c:if>										
														</tr>
													</c:forEach>	
											</table>
												<td>	
												<input type="submit" name="Transfer" id="Transfer" value="Transfer ALL(Yang Di Check)" onclick="showSPAJ('${d.MUA_POLICY_NO}');">
												</td>	
											<div class="rowElem"> 
																		 										
															<input type="hidden" name="distribusi2" id="distribusi2" value="ALL">	 
															<input type="hidden" name="company2" id="company3" value="ALL">	
															<input type="hidden" name="InputDel" value="1"> 
													</div> 	
											</fieldset>
											
												<div class="rowElem"> 
											
												<c:forEach items="${pesanList}" var="d" varStatus="st">
												<c:choose>
												<c:when test="${d.sts eq 'ERROR'}">
												<font color="red">${st.index+1}. ${d.msg}</font>
												</c:when>
												<c:otherwise>
												<font color="blue">${st.index+1}. ${d.msg}</font>
												</c:otherwise>
												</c:choose>
					
												<br/>
												</c:forEach>
										</div>	 			
								
								</form>						              
				
				</div>
				
				
		</div>		

			
	 </body>
</html>


