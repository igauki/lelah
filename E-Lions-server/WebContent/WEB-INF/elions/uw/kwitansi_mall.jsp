<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>	
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-blue.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<script type="text/javascript">
			hideLoadingMessage();
			
			function tampilkan(btn) {
				document.getElementById('submitMode').value = btn.name;
				document.getElementById('formpost').submit();	
			}
		</script>	
	</head>
	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">
		<div class="tab-container" id="container1"><div align="left"> 
			<ul class="tabs"> 
				<li> 
					<a href="#" onclick="return showPane('pane1', this)" id="tab1">Kwitansi MallAssurance</a> 
				</li> 
			</ul> 
			</div><div class="tab-panes"><div align="left"> 
				</div><div id="pane1" class="panes"><div align="left"> 
					</div><form id="formpost" method="post"><div align="left"> 
						</div><table class="entry2" width="100%" border="0">
							<tr align="left">
								<th width="10%">No Spaj :</th>
								<td width="50%">
									<input type="text" name="spaj" id="spaj"> <input type="submit" name="tampil" value="Tampilkan">
								</td>
							</tr>
							 
								<tr> 
									<td colspan="2"> 
										<iframe id="frame" width="100%" height="800">..Loading..</iframe> 
									</td> 
								</tr>								 
							
						</table>
					</form>
				</div>
			</div>
		</div>
  </body>
</html>
