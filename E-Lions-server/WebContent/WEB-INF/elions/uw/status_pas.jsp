<%@ include file="/include/page/header.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<%User currentUser = (User) session.getAttribute("currentUser");%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>

<script type="text/javascript">
	hideLoadingMessage();
	
	function proses(actionButton){
		if(actionButton == 'save'){
			if(window.confirm('Do you want to save?')){
				document.getElementById('action').value = 'save';
				document.getElementById('frmParam').submit();
			}
		}else if(actionButton == 'emailsave'){
			if(window.confirm('Do you want to save & email?')){
				if(document.getElementById('emailAgen').value == '' || document.getElementById('emailAdmin').value == ''){
					alert('Email Admin / Email Agen tidak boleh kosong');
				}else{
					document.getElementById('action').value = 'emailsave';
					document.getElementById('frmParam').submit();
				}
			}
		}
	}
	
</script>

</head>

<body onload="resizeCenter(800,250);bodyOnLoad();" style="height: 100%;">
	<div id="contents">
		<form:form commandName="cmd" name="frmParam" id="frmParam" method="post">
		<table class="entry2">
			<tr>
				<th>Tanggal</th>
				<th>Akseptor</th>
				<th>Status</th>
				<th>Keterangan</th>
				<th>Solved</th>
			</tr>
			<c:forEach var="sts" items="${statusList}">
				<tr>
					<td>
						${sts.MSPS_DATE}
					</td>
					<td>
						${sts.AKSEPTOR}
					</td>
					<td>
						FURTHER REQUIREMENT
					</td>
					<td>
						${sts.MSPS_DESC}
					</td>
					<td>
						.
					</td>
				</tr>
			</c:forEach>	 
				<tr>
					<td>
						<input type="text" size="22" readOnly name="tanggal" value="${cmd.msp_tgl_status2}" />
					</td>
					<td>
						<input type="text" readOnly name="lus id"
						<c:forEach var="user" items="${lsUser}">
							<c:if test="${ (LUS_ID eq user.LUS_ID) }">
							 value="${user.LUS_LOGIN_NAME}"
							 </c:if>
						</c:forEach>	 
						/>
					</td>
					<td>
						FURTHER REQUIREMENT
						<br>
					</td>
					<td>
						<spring:bind path="cmd.msp_ket_status" >
        		        	<textarea cols="50" rows="7" name="${status.expression }" >${cmd.msp_ket_status}</textarea>
                        </spring:bind>
					</td>
					<td>
						<spring:bind path="cmd.msp_status" >
							<select name="${status.expression }" onchange="cekElseEnvir();">
								<option <c:if test="${cmd.msp_status eq '1'}"> SELECTED </c:if> value="1">NO
								</option>
								<option <c:if test="${cmd.msp_status eq '0'}"> SELECTED </c:if> value="0">YES
								</option>
							</select>
						</spring:bind>
					</td>
				</tr>
	</table>
	<table width="60%" class="entry2">

			<tr>
				<th colspan="5" align="left">
				Email Admin : <input type="text" name="emailAdmin" id="emailAdmin" size="80" value="${cmd.emailAdmin}" readonly/>
				</th>
   		  </tr>
   		  <tr>
				<th colspan="5" align="left">
				Email Agen &nbsp;&nbsp;: <input type="text" name="emailAgen" id="emailAgen" size="80" value="${cmd.emailAgen}" readonly/>
				</th>
   		  </tr>
	</table>
	<table width="60%" class="entry2">

			<tr>
				<th colspan="5">
				<input type="hidden" name="action" id="action" size="1" value="" />
				<input type="button" name="save" id="save" value="Save" onclick="proses('save');"/>
				<input type="button" name="emailsave" id="emailsave" value="Save & Email" onclick="proses('emailsave');"/>
				<input type="button" name="close" value="Close" onclick="window.close();" />
				</th>
   		  </tr>
	</table>
	</form:form>
	<c:if  test="${not empty successMessage}">	
			<script type="text/javascript">													
				alert('${successMessage}');	
				parentForm = self.opener.document.forms['formpost'];
				parentForm.elements['search'].click();
				window.close();
			</script>
	</c:if>
	</div>
</body>
</html>