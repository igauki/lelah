<%@ include file="/include/page/header.jsp"%>
<fieldset>
	<legend>Detail Simultan Polis </legend>
	<display:table id="baris" name="lsSimultan" class="displaytag" >   
		<display:column href="detailsimultan.htm?spajAwal=${spajAwal}&flags=${flag_tt}&brs=${baris.BRS }&info=${info}&mclId=${mclId}" title="No Spaj"   >
		<a href="detailsimultan.htm?spajAwal=${spajAwal}&flags=${flag_tt}&brs=${baris.BRS }&info=${info}&mclId=${mclId}">${baris.REG_SPAJ }</a> 
		</display:column>                                                                                      
		<display:column property="MSPO_POLICY_NO" title="MSPO_POLICY_NO"  />                                                                          
		<display:column property="MEDIS" title="MEDIS"  />                                                                                            
		<display:column property="STANDARD" title="STANDARD"  />                                                                                      
		<display:column property="STATUS_ACCEPT" title="STATUS_ACCEPT"  />                                                                            
		<display:column property="STATUS_POLIS" title="STATUS_POLIS"  />                                                                              
		<display:column  title="KURS" >
		<c:choose>
			<c:when test="${baris.LKU_ID eq \"01\" }">
			Rp.
			</c:when>
			<c:when test="${baris.LKU_ID eq \"02\" }">
			US $
			</c:when>
		</c:choose>
		</display:column>                                                                                              
		<display:column property="MSPR_TSI" title="MSPR_TSI" format="{0, number, #,##0.00;(#,##0.00)}"  />                                            
		<display:column property="SAR_POLIS" title="SAR_POLIS" format="{0, number, #,##0.00;(#,##0.00)}" />                                                                                    
		<display:column property="MSPR_PREMIUM" title="PREMIUM" format="{0, number, #,##0.00;(#,##0.00)}"  />                                    
		<display:column property="LSDBS_NAME" title="Produk"  />                                                                                  
		<display:column property="MSPO_INS_PERIOD" title="LT"  />                                                                        
		<display:column property="MSPO_PAY_PERIOD" title="LB"  />                                                                        
		<display:column property="LSCB_ID" title="CB"  />                                                                                        
		<display:column property="MSTE_BEG_DATE" title="BEG_DATE" format="{0, date, dd/MM/yyyy}"  />                                             
		<display:column property="PEMEGANG" title="PEMEGANG"  />                                                                                      
		<display:column property="TERTANGGUNG" title="TERTANGGUNG"  />                                                                                
	
		<display:column property="MSTE_AGE" title="MSTE_AGE"  />                                                                                     
	</display:table>       
</fieldset>
<p>
</p>
<fieldset>
	<legend>Status Accept Spaj Number : ${spajTemp }</legend>
	<display:table id="baris" name="lsStatus" class="displaytag" >   
		<display:column property="MSPS_DATE" title="Tanggal"  />                                                                                    
		<display:column property="LSPD_POSITION" title="Posisi"  />                                                                                        
		<display:column property="STATUS_ACCEPT" title="Status"  />                                                                                        
		<display:column property="LUS_LOGIN_NAME" title="Akseptor"  />                                                                                          
		<display:column property="MSPS_DESC" title="Keterangan"  />                                                                                    
	</display:table>   
</fieldset>	
<p>
</p>
<fieldset>
	<legend>Premium Spaj Number : ${spajTemp }</legend>
	<display:table id="baris" name="lsPremi" class="displaytag"  decorator="org.displaytag.decorator.TotalTableDecorator" >   
		<display:column property="LSBS_NAME" title="Produk"  />                                                                                    
		<display:column property="LKU_ID" title="Kurs"  >
		<c:choose>
			<c:when test="${baris.LKU_ID eq \"01\" }">
			Rp.
			</c:when>
			<c:when test="${baris.LKU_ID eq \"02\" }">
			US $
			</c:when>
		</c:choose>
		
		</display:column>                                                                                          
		<display:column property="MSPR_TSI" title="Sum Insured" format="{0, number, #,##0.00;(#,##0.00)}"  />                                            
		<display:column property="MSPR_EXTRA" title="EM" format="{0, number, #,##0.00;(#,##0.00)}"  />                                        
		<display:column property="MSPR_RATE" title="RATE" format="{0, number, #,##0.00;(#,##0.00)}"  />                                          
		<display:column property="MSPR_PREMIUM" total="true" title="PREMIUM" format="{0, number, #,##0.00;(#,##0.00)}"  />                                    
		<display:column property="DISCOUNT" title=""  />                                                                                      
		<display:column property="MSPR_DISCOUNT" title="DISCOUNT" format="{0, number, #,##0.00;(#,##0.00)}"  />                                  
		<display:column property="MSPR_PERSEN" title="%"  />                                                                                
		<display:column property="MSPR_INS_PERIOD" title="INS. PERIOD"  />                                                                        
		<display:column property="MSPR_END_DATE" title="END DATE" format="{0, date, dd/MM/yyyy}"  />                                             
	</display:table>        
</fieldset>                 
<table align="center" width="100%">
	<tr>
		<td align="center" ><input type="button" name="btnClose" value="Close" onClick="javascript:window.close()"></td>
	</tr>
</table>
<%@ include file="/include/page/footer.jsp"%>