<%@ include file="/include/page/header.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<%User currentUser = (User) session.getAttribute("currentUser");%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>

<script type="text/javascript">
	hideLoadingMessage();
	
	function update(){
		if(trim(document.frmParam.kata.value)=='') return false;
		else createLoadingMessage();	
	}
	
	function cekElseEnvir(){
		if(document.getElementById('msp_fire_insured_addr_envir').value == '5'){
			document.getElementById('msp_fire_ins_addr_envir_else').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_ins_addr_envir_else').style.visibility = "hidden";
		}
	}
	
	function cekElseOkupasi(){
		if(document.getElementById('msp_fire_okupasi').value == 'L'){
			document.getElementById('msp_fire_okupasi_else').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_okupasi_else').style.visibility = "hidden";
		}
	}
	
	function bodyOnLoad(){
		cekElseEnvir();
		cekElseOkupasi();
	}
	
</script>
</head>

<body onload="resizeCenter(750,600);bodyOnLoad();" style="height: 100%;">
<XML ID=xmlData></XML>
<div class="tab-container" id="container1">

		<div class="tab-panes">

			<div id="pane1" class="panes">
<form:form commandName="cmd" name="frmParam" id="frmParam" method="post">
						<fieldset>
					   <legend>PAS</legend>
					   <table class="entry2">
						<tr>
							<th>pembayaran:</th>
							<td>
								<select name="lscb_id">
									<c:forEach var="cp" items="${carabayar_pas}"> 
										<option <c:if test="${cmd.lscb_id eq cp.ID}"> SELECTED </c:if> value="${cp.ID}">${cp.PAYMODE}
										</option>
									</c:forEach> 
								</select>
							<font class="error">*</font></td>
						</tr>
						<tr>
							<th>Telp:</th>
							<td><form:input path="msp_pas_phone_number" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						</table></fieldset>
						<fieldset>
					   <legend>AGEN</legend>
					   <table class="entry2">
						<tr>
							<th>kode agen:</th>
							<td>
								<form:input path="msag_id" size="8" cssErrorClass="errField" cssStyle="text-align: center;"/>
								<input type="button" class="button" value="Cari" name="btnCariAgent"
									onclick="var msag_id=document.frmParam.elements['msag_id'].value;popWin('${path}/bac/multi.htm?window=cek_struktur&msag_id='+msag_id, 350, 450);">
							<font class="error">*</font></td>
						</tr>
						</table>
						</fieldset>
						<fieldset>
						   <legend>REKENING</legend>
						   <table class="entry2">
						<tr>
							<th>cari bank:</th>
							<td><input type="text" name="caribank1" onkeypress="if(event.keyCode==13){ document.frmParam.btncari1.click(); return false;}"> 
	              				<input type="button" name="btncari1" value="Cari" onclick="ajaxSelectWithParam1a(document.frmParam.caribank1.value,'select_bank1','bank1','lsbp_id','', 'BANK_ID', 'BANK_NAMA', '','Silahkan pilih BANK','3');"></td>
						</tr>
						<tr>
							<th>bank:</th>
							<td><div id="bank1"> 
								<select name="lsbp_id" >
				                  <option value="${cmd.lsbp_id}">${cmd.lsbp_nama}</option>
				                </select>
				                </div>
							</td>
						</tr>
						<tr>
							<th>no rekening:</th>
							<td><form:input path="msp_no_rekening" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th>cabang:</th>
							<td><form:input path="msp_rek_cabang" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th>kota:</th>
							<td><form:input path="msp_rek_kota" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th>atas nama:</th>
							<td><form:input path="msp_rek_nama" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						</table>
						</fieldset>
						<fieldset>
	   <legend>ASURANSI KEBAKARAN</legend>
	   <table class="entry2">
	   					<tr>
							<th colspan="4">INFORMASI UTAMA</th>
						</tr>
						<tr>
							<th>Nama Tertanggung:</th>
							<td colspan="3">
								<select name="msp_fire_code_name">
									<c:forEach var="kn" items="${kode_nama_list}"> 
										<option <c:if test="${cmd.msp_fire_code_name eq kn.ID}"> SELECTED </c:if> value="${kn.ID}">${kn.NAMA}
										</option>
									</c:forEach> 
								</select>
								<form:input path="msp_fire_name" size="50" cssErrorClass="inpError"/><font class="error">*</font>
							</td>
						</tr>
						<tr>
							<th>Nomor KTP:</th>
							<td colspan="3"><form:input path="msp_fire_identity" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th>Tanggal Lahir:</th>
							<td colspan="3">
								<spring:bind path="cmd.msp_fire_date_of_birth2" >
        		                    <script>
                                        var isDisabled;
                                        var status = '${statusForm}';
                                        if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
                                <font color="#CC3300">*</font></spring:bind>
							</td>
						</tr>
						<tr>
							<th>Jenis Pekerjaan:</th>
							<td><form:input path="msp_fire_occupation" cssErrorClass="inpError"/><font class="error">*</font></td>
							<th>Bidang Usaha:</th>
							<td><form:input path="msp_fire_type_business" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th>Sumber Dana:</th>
							<td colspan="3"><form:input path="msp_fire_source_fund" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th>Alamat Tertanggung:</th>
							<td colspan="3">
							<select name="msp_fire_addr_code">
									<c:forEach var="ka" items="${kode_alamat_list}"> 
										<option <c:if test="${cmd.msp_fire_addr_code eq ka.ID}"> SELECTED </c:if> value="${ka.ID}">${ka.ALAMAT}
										</option>
									</c:forEach> 
							</select>
							<form:input path="msp_fire_address_1" size="70" cssErrorClass="inpError"/><font class="error">*</font>
							</td>
						</tr>
						<tr>
							<th>kode pos:</th>
							<td colspan="3"><form:input path="msp_fire_postal_code" maxlength="5" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th>Telp:</th>
							<td><form:input path="msp_fire_phone_number" cssErrorClass="inpError"/><font class="error">*</font></td>
							<th>Hp:</th>
							<td><form:input path="msp_fire_mobile" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th>Alamat Email:</th>
							<td colspan="3"><form:input path="msp_fire_email" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th colspan="4">INFORMASI OBYEK PERTANGGUNGAN</th>
						</tr>
						<tr>
							<th>Alamat Pertanggungan:</th>
							<td colspan="3">
							<select name="msp_fire_insured_addr_code">
									<c:forEach var="ka" items="${kode_alamat_list}"> 
										<option <c:if test="${cmd.msp_fire_insured_addr_code eq ka.ID}"> SELECTED </c:if> value="${ka.ID}">${ka.ALAMAT}
										</option>
									</c:forEach> 
							</select>
							<form:input path="msp_fire_insured_addr" size="60" cssErrorClass="inpError"/>
							No.<form:input path="msp_fire_insured_addr_no" maxlength="5" size="5" cssErrorClass="inpError"/>
							</td>
						</tr>
						<tr>
							<th>Kode Pos:</th>
							<td><form:input path="msp_fire_insured_postal_code" maxlength="5" cssErrorClass="inpError"/><font class="error">*</font></td>
							<th>Kota:</th>
							<td><form:input path="msp_fire_insured_city" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th>Telp:</th>
							<td colspan="3"><form:input path="msp_fire_insured_phone_number" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<th>Jenis Bangunan Disekeliling Pertanggungan:</th>
							<td colspan="3">
								<select name="msp_fire_insured_addr_envir" onchange="cekElseEnvir();">
									<c:forEach var="kos" items="${kode_obyek_sekitar_list}"> 
										<option <c:if test="${cmd.msp_fire_insured_addr_envir eq kos.ID}"> SELECTED </c:if> value="${kos.ID}">${kos.OBYEK}
										</option>
									</c:forEach> 
								</select><br/>
								<form:input path="msp_fire_ins_addr_envir_else" size="80" cssErrorClass="inpError"/>
							</td>
						</tr>
						<tr>
							<th>Penggunaan Bangunan:</th>
							<td colspan="3">
								<select name="msp_fire_okupasi" onchange="cekElseOkupasi();">
									<c:forEach var="ko" items="${kode_okupasi_list}"> 
										<option <c:if test="${cmd.msp_fire_okupasi eq ko.ID}"> SELECTED </c:if> value="${ko.ID}">${ko.OKUPASI}
										</option>
									</c:forEach> 
								</select><br/>
								<form:input path="msp_fire_okupasi_else" size="80" cssErrorClass="inpError"/>
							</td>
						</tr>
						</table>
						</fieldset>
						<fieldset>
					   <legend>-</legend>
					   <table class="entry2">
						<tr>
							<th colspan="2" align="left">
								<font class="error">* HARUS DIISI</font>
							</th>
						</tr>
						<tr>
							<th colspan="2">
							<input type="hidden" name="kata" size="1" value="edit" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
								<input type="submit" name="save" value="Save"  onclick="return update();"
									accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();"/>
					<input type="button" name="close" value="Close" onclick="window.close();"
						accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">
							</th>
						</tr>
					</table>
					</fieldset>
					</div>
		</div>
	</div>	
</form:form>
</body>
</html>