<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path}/include/js/disableKeys.js"></script>
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script>
	hideLoadingMessage();
	
	function cari(){
		if(trim(document.formpost.kata.value)=='') return false;
		else createLoadingMessage();	
	}
	
	var fieldName='chbox';
	
			function selectall(){
			  var i=document.formpost.elements.length;
			  var e=document.formpost.elements;
			  var name=new Array();
			  var value=new Array();
			  var j=0;
			  for(var k=0;k<i;k++)
			  {
			    if(document.formpost.elements[k].name==fieldName)
			    {
			      if(document.formpost.elements[k].checked==true){
			        value[j]=document.formpost.elements[k].value;
			        j++;
			      }
			    }
			  }
			  checkSelect();
			}
				
			function selectCheck(obj)
			{
			 var i=document.formpost.elements.length;
			  for(var k=0;k<i;k++)
			  {
			    if(document.formpost.elements[k].name==fieldName)
			    {
			      document.formpost.elements[k].checked=obj;
			    }
			  }
			  selectall();
			}
				
			function selectallMe()
			{
			  if(document.formpost.allCheck.checked==true)
			  {
			   selectCheck(true);
			  }
			  else
			  {
			    selectCheck(false);
			  }
			}
				
			function checkSelect()
			{
			 var i=document.formpost.elements.length;
			 var berror=true;
			  for(var k=0;k<i;k++)
			  {
			    if(document.formpost.elements[k].name==fieldName)
			    {
			      if(document.formpost.elements[k].checked==false)
			      {
			        berror=false;
			        break;
			      }
			    }
			  }
			  if(berror==false)
			  {
			    document.formpost.allCheck.checked=false;
			  }else
			  {
			    document.formpost.allCheck.checked=true;
			  }
			}
</script>
</head>
<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">List Spaj / Polis Yang Sudah Dicetak</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form id="formpost" name="formpost" action="${path}/uw/uw.htm?window=validasimaterai" method="post">
					<table class="entry2" style="width: auto;">
						<tr>
							<th>Nama Cabang</th>
							<th>Nama PIC</th>
							<th>No Rekening</th>
							<th>Email PIC</th>
						</tr>
						<c:forEach items="${result}" var="a" varStatus="st">
							<tr>
								<td>${a.NAMA_CABANG}</td>
								<td>${a.NAMA_REK_PIC}</td>
								<td>${a.NO_REK_PIC}</td>
								<td>${a.EMAIL_PIC}</td>
							</tr>
						</c:forEach>
					</table>
					<table>
					<tr>
					<td align="left">
					<a href="${path}/uw/uw.htm?window=validasimaterai&action=prev&dari=${dari}&sampai=${sampai}"> << Previous</a> |
					<a href="${path}/uw/uw.htm?window=validasimaterai&action=next&dari=${dari}&sampai=${sampai}">Next >> </a>
					<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					<td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					<td align="right"></td>
					<td>Page ${myself} of ${page}</td>
					</tr>
					<tr></tr><br>
				    </table>
					
					
					<table class="entry2" style="width: auto;">
						<tr>
							<th>Daftar Polis</th>
							<td>
								<table class="displaytag" style="width: auto;">
									<tr>
										<th>Nomor Register</th>
										<th>Nomor Polis</th>
										<th>Tanggal Cetak Terakhir</th>
										<th>Pilih</th>
										<th>Pilih Semua <input type="checkbox" name="allCheck" onclick="selectallMe()" style="border: none;"></th>
									</tr>
									<c:forEach items="${daftarPolis}" var="a" varStatus="st">
										<tr>
											<td>
												${a.reg_spaj}
											</td>
											<td>
												${a.mspo_policy_no_format}
											</td>
											<td>
												<fmt:formatDate value="${a.mspo_date_print}" pattern="dd/MM/yyyy (hh:mm)"/>
											</td>
											<td>
												<input name="chbox" type="checkbox" id="chbox" value="${a.reg_spaj}" style="border: none;">
											</td>
										</tr>
									</c:forEach>
								</table>
							</td>
						</tr>
					</table>
					<table class="entry2" style="width: auto;">
						<tr>
							<td>
								<br/>
								<input type="submit" name="proses" value="proses" onclick="return confirm('Proses No SPAJ/ POLIS ini ?');">
								<c:if test="${not empty pesanError}">
									<br/>
									<div id="error">${pesanError}</div>
								</c:if>
							</td>
						</tr>
					</table>
				</form>
			</div>
			
		</div>
	</div>

</body>
</html>
<c:if test="${not empty pesanError}">
<script>alert('${pesanError}');</script>
</c:if>