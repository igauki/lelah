<%@ include file="/include/page/header.jsp"%>
<script>
	function tambah(){
		formpost.flagAdd.value="1";
		formpost.submit();
	}

	function simpan(){
		formpost.flagAdd.value="2";
		formpost.btn_save. disabled = "disabled";
		formpost.submit();
			
	}

	/*function hapus(){
		formpost.flagAdd.value="3";
		formpost.submit();		
	}*/
	
	function edit() {
		//alert(document.getElementById('cekCount').value); 
		for(a=0;a<=document.getElementById('cekCount').value;a++) {
			var cek = document.getElementById('lsMedical'+a+'.cek');
			//if(document.getElementById('lsMedical'+a+'.cek').checked)
			alert(cek);
		}
		//formpost.flagAdd.value="4";
		//formpost.submit();	
	}
	
	function doEmailMedis(){
		formpost.submit();
        window.location='${path}/uw/medical_email.htm?spaj=${cmd.spaj}';
    }

    function maxChar(e,string){
		var count = string.length + 1;
		document.getElementById('charCount').value = count; 
		if(count > 650) {
			alert('input tidak bisa lebih dari 650 karakter');
			return false;
		}
		return true;
	}
</script>

<body onresize="setFrameSize('infoFrame', 45);" style="height: 100%;">
<form:form id="formpost" name="formpost" commandName="cmd">
	<div class="tabcontent">
	<fieldset>
	<legend>Medis</legend>
	<table class="entry2" width="60%">
		<tr>
			<!--<th width="9%">Hapus</th>-->
			<c:choose>
				<c:when test="${cmd.version eq 0}">
					<th>Jenis</th>
					<th>Hasil</th>
				</c:when>
				<c:otherwise>
					<th width="21%">Tanggal</th>
					<th width="20%">User Input</th>
				</c:otherwise>
			</c:choose>
			<th>Keterangan</th>
		</tr>
		<c:forEach var="s" items="${cmd.lsMedical}" varStatus="xt">
			<tr>
				<!--<td style="text-align: center;">
					<c:if test="${sessionScope.currentUser.lus_id eq s.msdm_lus_id}">
						<form:checkbox cssClass="noBorder" path="lsMedical[${xt.index}].cek" id="lsMedical[${xt.index}].cek" value="1"/>
					</c:if>
					<c:if test="${xt.last}">
						<input type="hidden" id="cekCount" name="cekCount" value="${xt.index}">
					</c:if>	
				</td>-->
				<c:choose>
					<c:when test="${cmd.version eq 0}">				
						<td style="text-align: center;">
							<form:select path="lsMedical[${xt.index}].lsmc_id" >
								<form:option label="" value=""/>
								<form:options items="${lsJenis}" itemLabel="value" itemValue="key" />
							</form:select> 
						</td>	
						<td style="text-align: center;">
							<label for="msdm_status1${xt.index}"><form:radiobutton cssClass="noBorder" path="lsMedical[${xt.index}].msdm_status" id="msdm_status1${xt.index}" value="1"/> Normal</label>
							<label for="msdm_status0${xt.index}"><form:radiobutton cssClass="noBorder" path="lsMedical[${xt.index}].msdm_status" id="msdm_status0${xt.index}" value="0"/> Tidak Normal</label>
						</td>
					</c:when>
					<c:otherwise>	
						<td style="text-align: center;"><fmt:formatDate value="${s.msdm_input_date}" pattern="dd/MM/yyyy [hh:mm:ss a]"/></td>
						<td style="text-align: center;">${s.lus_full_name}</td>	
					</c:otherwise>
				</c:choose>								
				<td style="text-align: center;">
					<c:choose>
						<c:when test="${empty s.msdm_desc or sessionScope.currentUser.lus_id eq s.msdm_lus_id}">
							<form:textarea rows="9" cols="60" id="d${xt.index}" path="lsMedical[${xt.index}].msdm_desc" cssStyle="width: 100%;text-transform: none;" onkeypress="return maxChar(event,this.value)"/>
						</c:when>
						<c:otherwise>
							<textarea rows="9" cols="60" style="width: 100%;text-transform: none;" readonly="readonly"  class="readOnly"/>${s.msdm_desc}</textarea>
						</c:otherwise>
					</c:choose>
					<!--<input type="text" id="count" value="${xt.index}">--> 					
				</td>
			</tr>
		</c:forEach>
		<tr>
			<td colspan="4" style="text-align: center;">
				<form:hidden path="flagAdd" />
				<input type="button" name="btn_save" value="Save" onclick="simpan();" <c:if test="${cmd.mode eq \'viewer\'}">disabled="disabled"</c:if>>
				<input type="button" name="btn_add" value="Add" onclick="tambah();" <c:if test="${cmd.mode eq \'viewer\'}">disabled="disabled"</c:if>>
				<!--<input type="button" name="btn_delete" value="Delete" onclick="hapus();">-->
				<!--<input type="button" name="btn_email" value="Email Medis &raquo;" onclick="doEmailMedis();">-->
				<!--<input type="button" name="btn_edit" value="Edit" onclick="edit();">-->
				<input type="button" name="btn_print" value="Print" onclick="popWin('${path}/report/uw.pdf?window=report_print_medis&nospaj=${cmd.spaj}',600,800);">
				<input type="button" name="btn_icd" value="ICD Code" onclick="popWin('${path}/uw/icd_code.htm?spaj=${cmd.spaj}&mode=${cmd.mode}',300,800,'yes','no','no');">
				<input type="button" name="btn_reas" value="Hasil Reas" onclick="popWin('${path}/uw/hasil_reas.htm?spaj=${cmd.spaj}&mode=${cmd.mode}',300,920,'yes','no','no');">
				<input type="button" name="exclude" value="Exclude Admedika" onclick="popWin('${path}/uw/exclude_admedika.htm?spaj=${cmd.spaj}',400,920,'yes','no','no');">
				<input type="text" id="charCount" readonly="readonly" size="4" alt="char count" class="readOnly">
				
			</td>
		</tr>
	</table>
	</fieldset>
	<!-- 
	<fieldset>
	<legend>Email Cabang</legend>
	<table class="entry2" >
		<tr>
			<th>
				Keterangan Email 
			</th>
			<th>
				<textarea rows="5" cols="50" name="desc" ></textarea>
			</th>
			<th>
				<input type="button" name="sendEmail" value="Send Email" >
			</th>
		</tr>
	</table>
	</fieldset> -->
<table width="50%">
	<tr>
		<td colspan="2">
			<c:if test="${not empty submitSuccess}">
	        	<div id="success">
		        	Berhasil
	        	</div>
	        </c:if>	
  			<spring:bind path="cmd.*">
				<c:if test="${not empty status.errorMessages}">
					<div id="error">
						ERROR:<br>
							<c:forEach var="error" items="${status.errorMessages}">
										- <c:out value="${error}" escapeXml="false" />
								<br/>
							</c:forEach>
					</div>
				</c:if>									
			</spring:bind>
		</td>
	</tr>
</table>
	</div>
</form:form>
</body>
<%@ include file="/include/page/footer.jsp"%>