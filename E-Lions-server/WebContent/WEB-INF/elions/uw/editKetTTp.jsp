<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/header.jsp"%>
<script>
	function awal(){
		email='${email}';
		if(email=='0'){
			alert("email cabang tidak ada harap masukan email cabang tersebut!");
		}
	}
</script>
<body onLoad="awal();">
	<form method="post" name="formpost">	
	<table class="entry2" style="width: auto;">
		
	<tr>
		<th colspan="2">Keterangan:
			<textarea name="keterangan" cols="50" rows="3" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); ">${keterangan }</textarea>
			<c:if test="${email eq 0 }">
				Masukan E-mail <input type="text" size="30" name="email" > 
				<input type="submit" value="send" name="btn_send" onClick="">
			</c:if>	
		</th>
	</tr>
	<tr><td colspan="2">
		<c:if test="${success ne null }">
        	<div id="success">
	        	 ${success }
	    	</div>
		</c:if>
		<c:if test="${not empty lsError}">
			<div id="error">
				 Informasi:<br>
					<c:forEach var="error" items="${lsError}">
								 - <c:out value="${error}" escapeXml="false" />
						<br/>
					</c:forEach>
			</div>
		</c:if>			
	</td></tr>	
		<tr>
			<th colspan="2">
				<input type="submit" <c:if test="${success ne null }" >disabled</c:if> name="save" value="save">
				<input type="hidden" name="flag" value="1">
			</th>
		</tr>
	</table>	
	</form>		
</body>
<%@ include file="/include/page/footer.jsp"%>