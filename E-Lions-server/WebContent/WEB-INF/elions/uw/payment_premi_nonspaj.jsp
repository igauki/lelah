<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
	<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta NAME="Description" CONTENT="EkaLife">
	<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<meta HTTP-EQUIV="Expires" CONTENT="-1">
	<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
	<link href="${path}/include/image/eas.ico" rel="shortcut icon">
	<!--  -->
	<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
	<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
	<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
	<!--  -->
	<script type="text/javascript" src="${path }/include/js/default.js"></script>
	<!-- Ajax Related -->
	<script type="text/javascript" src="${path}/include/js/ajax.js"></script>
	<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script>
	<script type="text/javascript" src="${path }/dwr/engine.js"></script>
	<script type="text/javascript" src="${path }/dwr/util.js"></script>
	<script type="text/javascript" src="${path }/include/js/refund/refund_common.js"></script>
	<script type="text/javascript">
		function pindahHal(jumlahReal, simbol)
		{
			var jumlahInput = document.getElementById('nominalPayment').value;
			jumlahInput = jumlahInput.replace(/,/g,"");
			if( jumlahInput != '' &&  jumlahInput != null && jumlahInput !='0.00')
			{
				if( parseFloat(jumlahInput) > parseFloat(jumlahReal) )
				{
					alert('Premi yang diinput harus lebih kecil dari jumlah!');
				}
				else
				{
					window.returnValue = document.getElementById('nominalPayment').value;	
					window.close();
				}
			}
			else
			{
				alert('Premi harus diisi terlebih dahulu!');
			}
        }
                
        function onEnter()
		{
			if(event.keyCode == 13) 
			{
				document.getElementById('btnEnter').click();
			}
		}
	</script>
</head>
<body style="height: 100%;">
	<div class="tab-container" id="container1">
		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form method="post" name="formpost" action="${path}/uw/uw.htm?window=drekNonSpaj">
					<fieldset>
						<legend>Payment</legend>
						<table class="entry2" style="width: auto;">
					
							<tr style="display: ${detailDisplay}">
								<th nowrap="nowrap"><font color="BLACK">Detail</font></th>
								<td></td>
							</tr>
						
							<c:forEach items="${mstDrekDetBasedNoTrx}" var="listDetail">
								<tr>
									<th nowrap="nowrap">${listDetail.descr}</th>
									<td>
										${simbol}<fmt:formatNumber value="${listDetail.jumlahForDisplay}"/>
									</td>
								</tr>
							</c:forEach>
						
							<tr style="display: ${detailDisplay}">
								<td><hr></td>
								<td><hr></td>
							</tr>
						
							<tr style="display: ${detailDisplay}">
								<th nowrap="nowrap">Total premi yg sudah terpakai</th>
								<td>
									${simbol}<fmt:formatNumber value="${total}"/>
								</td>
							</tr>
						
							<tr style="display: ${detailDisplay}">
								<td><br></td>
								<td><br></td>
							</tr>
						
							<c:choose>
								<c:when test="${jumlah eq '.00'}">
								</c:when>
								<c:otherwise>
									<tr>
										<th nowrap="nowrap">Premi blm terpakai</th>
										<td>
											${simbol}<fmt:formatNumber value="${jumlah}"/>
										</td>
									</tr>
									<tr>
										<th nowrap="nowrap">Jumlah Premi yang ingin diinput</th>
										<td>
											<input type="text" value="${jumlah}" id="nominalPayment" onkeypress="onEnter();"
											onfocus=" showForm( 'countHelp', 'true' );"
															onblur="showForm( 'countHelp', 'false' );"
															onkeyup="showFormatCurrency('countHelp', this.value);"/>
										</td>
										<td>
											<input type="button" id="btnEnter" name="btnEnter" value="submit" onclick="pindahHal('${jumlah}','${simbol}');"/>
										</td>
									</tr>
									<tr>
										<th nowrap="nowrap"></th>
										<td>
											<input type="text" id="countHelp" disabled="disabled" style="display: none;" tabindex="-1"  />
											<input type="hidden" id="helpIndex" name="helpIndex" />
										</td>
									</tr>
								</c:otherwise>
							</c:choose>
						</table>
					</fieldset>					
				</form>
			</div>
		</div>
	</div>
</body>
</html>