<%@ include file="/include/page/header_jquery.jsp"%>
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path}/include/js/default.js"></script>

<script type="text/javascript">
	hideLoadingMessage();
	
	//Perintah untuk menjalankan jQuery setelah seluruh dokumen selesai di load.
	$().ready(function(){
	
		// (jQueryUI Tabs) init tab2 Utama
		$("#tabs").tabs();

		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title], label[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
//  		$(".opt1, .opt2").hide();
		
		$("#checkAll").change(function(){
	        if(this.checked == true){
	             $(".chbox").attr("checked", true);
	             $(".datarow").css("background-color", "yellow");
// 	             $(".opt1, .opt2").show();
	        }else{
	             $(".chbox").attr("checked", false);
	             $(".datarow").css("background-color", "white");
// 	             $(".opt1, .opt2").hide();
// 	             $(".opt1").val("1");
// 	             $(".opt2").val("");
	        }
    	});
    	
    	$("#btnSubmit").click(function(){
    		var n = $(".chbox:checked").length;
    		if(n==0){
    			alert("Harap pilih data yang akan diproses!");
    			return false;
    		}else{
    			return true;
    		}
    	});
    	
	});
	
	function klikcheckbox(index){
		if(document.getElementById("check[" + index + "]").checked == true){
			document.getElementById("row[" + index + "]").style.backgroundColor = "yellow";
// 			document.getElementById("lsjb_id[" + index + "]").style.display = '';
// 			document.getElementById("flag_merchant[" + index + "]").style.display = '';
		}else{
			document.getElementById("row[" + index + "]").style.backgroundColor = "white";
// 			document.getElementById("lsjb_id[" + index + "]").style.display = 'none';
// 			document.getElementById("flag_merchant[" + index + "]").style.display = 'none';
// 			document.getElementById("lsjb_id[" + index + "]").value = "1";
// 			document.getElementById("flag_merchant[" + index + "]").value = "";
		}
	}
	
</script>

	<style type="text/css">
		/* font utama */
		body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }
	
		/* fieldset */
		fieldset { margin-bottom: 1em; padding: 0.5em; }
		fieldset legend { width: 99%; }
		fieldset legend div { margin: 0.3em 0.5em; }
		fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
		fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
		fieldset .rowElem .jtable { position: relative; left: 12.5em; }
	
		/* tanda bintang (*) untuk menandakan required field */
		em { color: red; font-weight: bold; }
	
		/* agar semua datepicker align center, dan ukurannya fix */
		.datepicker { text-align: center; width: 7em; }
	
		/* styling untuk client-side validation error message */
		#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }
	
		/* styling untuk label khusus checkbox dan radio didalam rowElement */
		fieldset .rowElem label.radioLabel { width: auto; }
		
		/* lebar untuk form elements */
		.lebar { width: 7em; }
		
		/* untuk align center */
		.tengah { text-align: center; }
		
		/* untuk warna kolom input */
		.warna { color: grey; }
			
		/* styling untuk server-side validation error */
		.errorField { border: 1px solid red; }
		.errorMessage { color: red; display: block;}
		
		#tab1 td, #tab1 th { padding: 2px; }
		
		#tab1 th { background-color: #eee; }
		
	</style>
	
	<body>
		<form:form method="post" name="formpost" id="formpost" commandName="cmd">
			<fieldset class="ui-widget ui-widget-content">
				<legend class="ui-widget-header ui-corner-all"><div>Proses AutoPayment BSM</div></legend>
				<br><br>
				<table id="tab0">
					<tr>
						<td>
							<form:errors path="pesan" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<td><br><br></td>
					</tr>
				</table>
				<table class="displaytag" id="tab1">
					<tr>
						<th>
							Check All<br>
							<input type="checkbox" id="checkAll" style="border: none">
						</th>
						<th nowrap>No. SPAJ</th>
						<th nowrap>No. Kontrak / CIF</th>
						<th nowrap>Nama Produk</th>
						<th nowrap>Nama Pemegang Rek</th>
						<th nowrap>No. Rekening</th>
						<th>Kurs</th>
						<th width="75px">Premi</th>
					</tr>
					<c:forEach items="${cmd.listSpaj}" var="d" varStatus="st">
						<tr class="datarow" id="row[${st.index}]">
							<td class="center">
								<form:checkbox path="listSpaj[${st.index}].check" value="1" cssClass="chbox noBorder" id="check[${st.index}]"
									onclick="klikcheckbox('${st.index}')"/>
							</td>
							<td class="left" nowrap>${d.reg_spaj}</td>
							<td class="left" nowrap>${d.cif}</td>
							<td class="left" nowrap>${d.nama_produk}</td>
							<td class="left">${d.nama_rek}</td>
							<td class="left">${d.no_rek}</td>
							<td class="left">
								<c:choose>
									<c:when test="${d.lku_id eq '01'}">
										Rp
									</c:when>
									<c:otherwise>
										USD
									</c:otherwise>
								</c:choose>
							</td>
							<td>${d.premi_pokok}</td>
						</tr>
					</c:forEach>
				</table>
				<br>
				<table id="tab2">
					<tr>
						<td>
							<input type="submit" name="btnSubmit" id="btnSubmit" title="Submit" value="Submit">
						</td>
					</tr>
				</table>
			</fieldset>
		</form:form>
	</body>
	
</html>