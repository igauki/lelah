<%@ include file="/include/page/header.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<%User currentUser = (User) session.getAttribute("currentUser");%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>

<script type="text/javascript">
	hideLoadingMessage();
	function trfToAksep(){
	        var jenis_upload_produk = document.getElementById('jenis_upload_produk').value;
			var upload_produk = "";
			if(jenis_upload_produk == 1)upload_produk = "PAS BP";
			else if(jenis_upload_produk == 2)upload_produk = "PAS BP SMS";
			else if(jenis_upload_produk == 3)upload_produk = "PAS BP CARD";			
			else if(jenis_upload_produk == 4)upload_produk = "PAS SYARIAH";
			else if(jenis_upload_produk == 6)upload_produk = "SMART ACCIDENT CARE";
			else if(jenis_upload_produk == 10)upload_produk = "SMART SAVE & SMiLe MEDICAL(+)";
			if(jenis_upload_produk == 10){
				if(confirm("Apakah anda yakin untuk TRANSFER "+ upload_produk +" berikut ke Posisi PAYMENT?")){				
					document.getElementById('transfer3').click();
					document.getElementById('list_prestasi_agen').style.visibility = "visible";	
				}
			}else{
				if(confirm("Apakah anda yakin untuk TRANSFER "+ upload_produk +" berikut ke Posisi PAYMENT?")){
						document.getElementById('transfer').click();
						document.getElementById('list_prestasi_agen').style.visibility = "visible";	
					}
			}		
	}	
	
	function trfToAksep2(){	      						
		var jenisPas = "${jenis_pas}";
		if(confirm("Apakah anda yakin untuk simpan & blast email Polis produk " + jenisPas + " berikut?")){
					document.getElementById('transfer2').click();
					document.getElementById('list_prestasi_agen').style.visibility = "visible";	
				}
	}	
	
	function uploadPasBp(){
		var fileUpload	= document.getElementById('file1').value;
		var tglAwal 	= document.getElementById('tanggalAwal').value;
		var jenis_upload_produk = document.getElementById('jenis_upload_produk').value;
		
		if(jenis_upload_produk == 0){
			alert("Jenis Upload Produk harus dipilih!");
		}else{
			var upload_produk = "";
			if(jenis_upload_produk == 1)upload_produk = "PAS BP";
			else if(jenis_upload_produk == 2)upload_produk = "PAS BP SMS";
			else if(jenis_upload_produk == 3)upload_produk = "PAS BP CARD";
			else if(jenis_upload_produk == 4)upload_produk = "PAS SYARIAH";
			else if(jenis_upload_produk == 5)upload_produk = "PAS FREE";
			else if(jenis_upload_produk == 6)upload_produk = "SMART ACCIDENT CARE";
            else if(jenis_upload_produk == 7)upload_produk = "FREE PA BANK DKI";
            else if(jenis_upload_produk == 8)upload_produk = "FREE DBD BANK DKI";
            else if(jenis_upload_produk == 9)upload_produk = "FREE PA & DBD BANK DKI";
            else if(jenis_upload_produk == 10)upload_produk = "SMART SAVE & SMiLe MEDICAL(+)";
			if(confirm("Jenis Upload Produk yang dipilih : "+upload_produk+", Lanjutkan?")){
				if((fileUpload=='' || fileUpload==null) && (tglAwal=='' || tglAwal==null)){
					alert("File Excel(.xls) dan Tanggal BegDate belum diisi!");
				}else if((fileUpload=='' || fileUpload==null) && (!tglAwal=='' || !tglAwal==null)){
					alert("File Excel(.xls) belum diupload!");
				}else if((fileUpload!='' || fileUpload!=null) && (tglAwal=='' || tglAwal==null)){
					alert("Tanggal BegDate harus diisi!");
				}else{
				     if(upload_produk == "PAS FREE" || upload_produk == "FREE PA BANK DKI" || upload_produk == "FREE DBD BANK DKI"
				        || upload_produk == "FREE PA & DBD BANK DKI" ){				     
				   		  document.getElementById('upload_pasfree').click();				   		 
				     }else  if( upload_produk == "SMART SAVE & SMiLe MEDICAL(+)") {
				     	  document.getElementById('upload_smartsave').click();
				     }
				     else{
				    	 document.getElementById('upload').click();
					 }
				}
			}
		}	
	}
	
	function buttonLinks(str){
			popWin('${path}/uw/upload_nb.htm?reg_spaj='+str, 550, 550);
		}
		
	function cariData(){
		var cari_upload_produk = document.getElementById('cari_upload_produk').value;
		var tgl_upload 	= document.getElementById('tgl_upload').value;
		
		if(cari_upload_produk == 0){
			alert("Produk harus dipilih!");
			return false;
		}else if (tgl_upload=='' || tgl_upload==null){
			alert("Tanggal Upload harus diisi!");
			return false;		
		}else{			
			 createLoadingMessage();
		}
	}
	
	function referesh(){	
		document.getElementById('refresh').click();
	}
</script>

</head>

<body onload="" style="height: 100%;">
<div id="contents">
<div class="tab-container" id="container1">

		<div class="tab-panes">

			<div id="pane1" class="panes">
<form method="post" name="formpost" style="text-align: center;" enctype="multipart/form-data">
<fieldset>
<legend>Upload Data : (PAS) Personal Accident SinarmasLife </legend>
<br>
									<table class="entry2">									
										<tr>
										<th align="right">Download Template</th>
										<td>
											<c:choose>
												<c:when test="${currentUser.lde_id eq '11' or currentUser.lde_id eq '01' or currentUser.lde_id eq '29' or currentUser.lca_id eq '40'}">														
		        		                   			<input type="submit" name="dl_pasbp" id="dl_pasbp" value="PAS BP" title="contoh template excel untuk uploadan PAS BP standart. Upload dengan format kolom Excel Standard PAS-BP digunakan untuk Insert Data "/>
		        		                   			<input type="submit" name="dl_pasbpsms" id="dl_pasbpsms" value="PAS BP SMS" title="contoh template excel untuk uploadan PAS BP SMS. Upload dengan format kolom Excel PAS-BP-SMS **dgn kolom No.Kartu,Fire.Id** (Download:: dari MenuPAS:'Input PAS BP SMS': [REPORT UW]) digunakan untuk Update Data Sebelum Transfer/UW "/>   
		        		                   			<input type="submit" name="dl_pasbpcard" id="dl_pasbpcard" value="PAS BP CARD" title="contoh template excel untuk uploadan PAS BP dengan kartu. Upload dengan format kolom Excel PAS-BP **dgn kolom No.Kartu** " />                           	
													<input type="submit" name="dl_passyariah" id="dl_pasdmtm" value="PAS SYARIAH" title="contoh template excel untuk uploadan PAS SYARIAH** "/> 
													<input type="submit" name="dl_pasfree" id="dl_pasfree" value="PAS FREE" title="contoh template excel untuk uploadan FREE PA** " /> 
													<input type="submit" name="dl_sac" id="dl_pasdmtm" value="SMART ACCIDENT CARE" title="contoh template excel untuk upload data SMART ACCIDENT CARE** "/> 
                                                    <%-- <input type="submit" name="dl_pasfree_bankdki" id="dl_pasfree_bankdki" value="FREE PA BANK DKI" title="contoh template excel untuk uploadan FREE PA BANK DKI** " /> 
                                                    <input type="submit" name="dl_dbdfree_bankdki" id="dl_dbdfree_bankdki" value="FREE DBD BANKDKI" title="contoh template excel untuk uploadan FREE DBD BANK DKI** " />  --%>
                                                    <input type="submit" name="dl_pasdbdfree_bankdki" id="dl_pasdbdfree_bankdki" value="FREE PA &amp; DBD BANKDKI" title="contoh template excel untuk uploadan FREE PA &amp; DBD BANK DKI**" /> 
												    <input type="submit" name="dl_smartsave_medicalplus" id="dl_smartsave_medicalplus" value="SMART SAVE &amp; SMiLe MEDICAL(+)" title="contoh template excel untuk uploadan SMART SAVE &amp; SMiLe Medical(+)**" /> 
												
												</c:when>
		        		                   		<c:otherwise>
													<input type="submit" name="dl_passyariah" id="dl_pasdmtm" value="PAS SYARIAH" title="contoh template excel untuk uploadan PAS SYARIAH** " /> 
												</c:otherwise>
											</c:choose>   
										</td>
										<td>
										<th align="center">PERHATIKAN: </th><td>
										<font class="error">[1.] Gunakan Format  "DD/MM/YYYY"  untuk data Tanggal dalam file xls yg diupload!</font>
										</td>
										</td>										
										</tr>
										<tr>
										<th align="right">Pilih Upload Produk</th>
										<td>
											<c:choose>
												<c:when test="${currentUser.lde_id eq '11' or currentUser.lde_id eq '01' or currentUser.lde_id eq '29' or currentUser.lca_id eq '40'}">
												
		        		                   			<select name="jenis_upload_produk" id="jenis_upload_produk">       
		        		                   				<option value="0">-</option>
		        		                   				<option value="1" <c:if test="${jenis_upload_produk eq 1}">selected</c:if>>PAS BP</option>
		        		                   				<option value="2" <c:if test="${jenis_upload_produk eq 2}">selected</c:if>>PAS BP SMS</option>
		        		                   				<option value="3" <c:if test="${jenis_upload_produk eq 3}">selected</c:if>>PAS BP CARD</option>
		        		                   				<option value="4" <c:if test="${jenis_upload_produk eq 4}">selected</c:if>>PAS SYARIAH</option>
		        		                   			    <option value="5" <c:if test="${jenis_upload_produk eq 5}">selected</c:if>>PAS FREE</option>      
		        		                   			    <option value="6" <c:if test="${jenis_upload_produk eq 6}">selected</c:if>>SMART ACCIDENT CARE</option>             			
                                                        <%-- <option value="7" <c:if test="${jenis_upload_produk eq 7}">selected</c:if>>FREE PA BANK DKI</option>
                                                        <option value="8" <c:if test="${jenis_upload_produk eq 8}">selected</c:if>>FREE DBD BANK DKI</option> --%>
                                                        <option value="9" <c:if test="${jenis_upload_produk eq 9}">selected</c:if>>FREE PA &amp; DBD BANK DKI</option>
                                                       <option value="10" <c:if test="${jenis_upload_produk eq 10}">selected</c:if>>SMART SAVE &amp; SMiLe MEDICAL(+)</option>
		        		                   			</select> 
		        		                   		</c:when>
		        		                   		<c:otherwise>
		        		                   			<select name="jenis_upload_produk" id="jenis_upload_produk">       
		        		                   				<option value="0">-</option>		        		                   				
		        		                   				<option value="4" <c:if test="${jenis_upload_produk eq 4}">selected</c:if>>PAS SYARIAH</option>
		        		                   			</select> 
		        		                   		</c:otherwise>
											</c:choose>        		                   		                 	
										</td>	
										<td>
										<td></td><td>
										<font class="error">[2.] Verifikasi ulang setelah Proses!</font>
										</td>
										</td>									
										</tr>
										<tr>
										<th align="right">Tanggal BegDate</th>
										<td>		
        		                   		 <script>                                                                              
                                      		 inputDate('tanggalAwal', '${tglAwal}', false);
                                  		  </script>                                   	
										</td>
										<td>
										<td></td><td>
										<font class="error">[3.] Jumlah data upload Maksimal 50 data </font>
										</td>
										</td>											
										</tr>
										<tr>
											<th align="right">File Excel (.XLS)</th>
										<td>
												<input type="file" name="file1" id="file1" size="70" />
												<input type="hidden" name="file_fp" id="file_fp"/>	
												<input type="text" name="kata" id="kata" size="34" value="${param.kata }" style="visibility: hidden;"/>											
											</td>
										
										</tr>																																					
									</table>
						</fieldset>
						<fieldset>
					   <legend>-</legend>
					   <table class="entry2">
						<tr>
							<th  align="center">
							<c:if test="${not empty success}">							
							   <input type="button" name="down" value="        UPLOAD UNTUK CEK VALIDITAS DATA        " onclick="uploadPasBp();"/>   
							   <input type="button" name="up" value="PROSES AKSEPTASI DAN TRANSFER KE PAYMENT" onclick="trfToAksep();" />
							</c:if>						
							<c:if test="${empty success and empty success2}">
							    <input type="button" name="down" value="        UPLOAD UNTUK CEK VALIDITAS DATA        " onclick="uploadPasBp();"/>
							</c:if>								
							<c:if test="${not empty success2 }">							
							   <input type="button" name="down" value="        UPLOAD UNTUK CEK VALIDITAS DATA        " onclick="uploadPasBp();"/>   
							   <input type="button" name="up" value="     PROSES SAVE DAN BLAST EMAIL  " onclick="trfToAksep2();" />
							</c:if>					
							</th>
						</tr>
					</table>
					</fieldset>
					
				<div id="list_prestasi_agen" style="position:absolute; right:0; width:830;visibility:visible; background-color: #F7F7F7;">
          		<span style="float:right; vertical-align: top; background-color:gray; color:white; font-weight:bold; font-size:14; text-align:center; cursor:pointer" onclick="Loading()">&nbsp;&nbsp;&nbsp;&nbsp;Processing  .........(Please Wait.!!)&nbsp;&nbsp;</span>
        		</div>
					
<fieldset style="text-align: left;">
<legend>[Status Verifikasi Data Upload]         ***      [Status Akseptasi Dan Transfer Ke Payment]  :</legend>
<table style="width: 100%;" border="1" bordercolor="white">
<c:forEach items="${successMessage}" var="d" varStatus="st">
<tr>
<c:choose>
<c:when test="${d.sts eq 'FAILED'}">
<td style="width: 90%;">
<font color="red">${st.index+1}. ${d.msg}</font>
</td>
<td></td> 
</c:when>
<c:otherwise>
<td style="width: 90%;">
<font color="blue">${st.index+1}. ${d.msg}</font>
</td>
<td align="center">
  <c:choose>
	<c:when test="${not empty d.noSpaj}">
	<input type="button" value="Upload Scan Dok" name="upload_nb" onclick="buttonLinks(${d.noSpaj});" accesskey="C" onmouseover="return overlib('Upload BSB', AUTOSTATUS, WRAP);" onmouseout="nd();">
	</c:when>
	</c:choose>
</td>
</c:otherwise>

</c:choose>

</tr>
</c:forEach>
</table>
</fieldset>
			</div>
		</div>
	</div>	
	
<c:if test="${empty success and empty success2}">
<table class="entry2" border=1 style="width: 98%;">
						<tr>
							<th width="135"  rowspan="2" style>Viewer Data Upload:</th>							
							<td class="left">
								Pilih Produk:
								<select name="cari_upload_produk" id="cari_upload_produk">       
		        		       	<option value="0">-</option>		        		                   				
		        		        <option value="4" <c:if test="${cari_upload_produk eq 4}">selected</c:if>>PAS SYARIAH</option>
		        		        <option value="5" <c:if test="${cari_upload_produk eq 5}">selected</c:if>>PAS FREE</option>       
		        		        <option value="6" <c:if test="${cari_upload_produk eq 6}">selected</c:if>>SMART ACCIDENT CARE</option>
		        		        </select> 
								<br>															
								TANGGAL  Upload    :								
								<script>inputDate('tgl_upload', "${param.tgl_upload}", false);</script>									
								<!--<input type="button" name="search" id="search" value="Search" onclick="cariData();" class="button2" 
								onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
								onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />-->
															
								 &nbsp; <input type="submit" name="search" value="  Search  " id="search" onclick="return cariData();"  
									accesskey="S" class="button2" 
								onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
								onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />										
								
							</td>							
						</tr>
</table>
<c:if test="${not empty uploadList or not empty uploadList2}">
<table  class="entry2" align="center" cellpadding="2" style="width: 98%;" >       				
			        <tr>
			            <td width="573" class="">Page ${currPage} of ${lastPage} </td>
			            <td width="493" align="right" class="">Go
			                <input name="cPage" type="text" style="width:45px" maxlength="4" >
			                <input type="button" value="Go" id="go" style="width: 40px;"  onclick="referesh()"/>
			                <input type="submit" name="refresh" value="refresh" id="refresh" style="visibility: hidden;" /> 
			               <label></label>
			                &nbsp;			           
			                
			                <c:choose>
			        			<c:when test="${currPage eq '1'}">
			        			<font color="gray">First | Prev |</font> 
			        			</c:when>
			        			<c:otherwise>
			               			<a href="#" class="" onclick="document.forms[ 0 ].cPage.value='${firstPage}'; referesh()">First</a> | 
			                		<a href="#" class="" onclick="document.forms[ 0 ].cPage.value='${previousPage}'; referesh()">Prev</a> | 
			                	</c:otherwise>
			                </c:choose>
			                
			                <c:choose>
			        			<c:when test="${currPage eq lastPage}">
			        			<font color="gray">Next | Last </font>
			        			</c:when>
			        			<c:otherwise>
			               			<a href="#" class="" onclick="document.forms[ 0 ].cPage.value='${nextPage}'; referesh()">Next</a> | 
			                		 <a href="#" class="" onclick="document.forms[ 0 ].cPage.value='${lastPage}'; referesh()">Last</a> 
			                	</c:otherwise>
			                </c:choose>                
               			 </td>
      				  </tr>
</table>
</c:if>									
 <table class="entry2" style="width: 98%;" border=1>
	<thead>
		<tr>		
			<th style="text-align: left">Jenis Upload</th>
			<c:choose>
				<c:when test="${not empty uploadList}">
					<th style="text-align: left">No.SPAJ</th>
				</c:when>
				<c:otherwise>
					<th style="text-align: left">No.Sertifikat</th>
				</c:otherwise>
			</c:choose>
			<th style="text-align: left">Nama Tertanggung (TTG)</th>
			<c:choose>
				<c:when test="${not empty uploadList}">
					<th style="text-align: left">No.POLIS</th>
				</c:when>
				<c:otherwise>					
				</c:otherwise>
			</c:choose>			
			<th style="text-align: left">Tgl.Mulai</th>
			<th style="text-align: left">Tgl.Berakhir</th>
			<th style="text-align: left">No.KTP</th>	
			<th style="text-align: left">Premi</th>
			<th style="text-align: left">UP</th>
			<th style="text-align: left">No.Handphone</th>
			<th style="text-align: left">Tgl.Lahir</th>											
		</tr>
	</thead>
	<tbody>
	   <c:forEach var="upload" items="${uploadList}" varStatus="stat">
		<tr>
			<td>${upload.jenis_pas}</td>
			<td>${upload.reg_spaj}</td>
			<td width="180px">${upload.msp_full_name}</td>
			<td>${upload.mspo_policy_no}</td>
			<td>${upload.msp_pas_beg_date2}</td>
			<td>${upload.msp_pas_end_date2}</td>
			<td>${upload.msp_identity_no}</td>
			<td>${upload.msp_premi}</td>
			<td>${upload.msp_up}</td>
			<td>${upload.msp_mobile}</td>
			<td>${upload.msp_date_of_birth2}</td>
		</tr>
		</c:forEach>		
		<c:forEach var="upload" items="${uploadList2}" varStatus="stat">
		<tr>
			<td>${upload.application_id}</td>
			<td>${upload.no_sertifikat}</td>
			<td width="180px">${upload.holder_name}</td>			
			<td>${upload.beg_date2}</td>
			<td>${upload.end_date2}</td>
			<td>${upload.cust_no}</td>upload_pasfree
			<td>${upload.premium}</td>
			<td>${upload.sum_insured}</td>
			<td>${upload.mobile_no}</td>
			<td>${upload.bod_holder2}</td>
		</tr>
		</c:forEach>
	</tbody>
 </table>
</c:if>
					
    <input type="submit" name="transfer" id="transfer" value="Aksep And Transfer To Payment" style="visibility: hidden;" onmouseout="nd();"/>
 	<input type="submit" name="upload" id="upload" value="Upload" style="visibility: hidden;" onclick="document.getElementById('file_fp').value=document.getElementById('file1').value"/>
    <input type="submit" name="upload_pasfree" id="upload_pasfree" value="upload_pasfree" style="visibility: hidden;" onclick="document.getElementById('file_fp').value=document.getElementById('file1').value"/>
	<input type="submit" name="transfer2" id="transfer2" value="Cetak Polis" style="visibility: hidden;" onmouseout="nd();"/>
	<input type="submit" name="transfer3" id="transfer3" value="Cetak Polis" style="visibility: hidden;" onmouseout="nd();"/>
    <input type="submit" name="upload_smartsave" id="upload_smartsave" value="upload_smartsave" style="visibility: hidden;" onclick="document.getElementById('file_fp').value=document.getElementById('file1').value"/>

</form>
	</div>
<script type="text/javascript">	
		document.getElementById('list_prestasi_agen').style.visibility = "hidden";	
	</script>		
	
</body>
</html>
