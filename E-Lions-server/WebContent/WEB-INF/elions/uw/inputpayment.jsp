<%@ include file="/include/page/header.jsp"%>
<script>
	var errorMessages='';
	
	function bank(elm, nama, _nama, nilai){
		if(elm.value.length==5){
			ajaxSelectWithParam(elm.value, 'selectBankEkaLife2', _nama, nama, nilai, 'LSREK_ID', 'NAMA', '');
			var monyong='1';
			if(elm.value=='00000' || elm.value == '0') monyong='0';
			if(document.formpost.lsjb_id.value != 3) ajaxSelectWithParam(monyong, 'listCaraBayar', '_lsjb_id', 'lsjb_id', '', 'LSJB_ID', 'LSJB_TYPE', 'merchantDisplay(this.value)');
		}
	}
	
	function tampil(tipe, payment_id){
		if(tipe=='edit'){
			window.location='${path}/uw/inputpayment.htm?tahun_ke=${cmd.tahun_ke}&premi_ke=${cmd.premi_ke}&spaj=${cmd.reg_spaj}&payment_id='+payment_id+'&actionType='+tipe;
		}else if(tipe=='refresh'){
			window.location='${path}/uw/inputpayment.htm?tahun_ke=${cmd.tahun_ke}&premi_ke=${cmd.premi_ke}&spaj=${cmd.reg_spaj}';
		}else if(tipe=='add'){
			window.location='${path}/uw/inputpayment.htm?tahun_ke=${cmd.tahun_ke}&premi_ke=${cmd.premi_ke}&spaj=${cmd.reg_spaj}&add=true&actionType='+tipe;
		}else if(tipe=='save'){
			return confirm('Simpan Perubahan?');
		}
	}
	
	function pp(nopol,np){
	    var nopol=nopol.value;		
		ajaxManager.listnamapp(nopol,
			{callback:function(m) {
				DWRUtil.useLoadingMessage();		
				np.value= m.nama_pemegang;	
			},
		   	timeout:180000,
	   		errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
	
	function getdataRkCc(noSpaj){
		var spaj = noSpaj;
		ajaxManager.listdataRkCc(spaj,
			{callback:function(data){
				DWRUtil.useLoadingMessage();
				
				var lima = data.norek_ajs.substr(data.norek_ajs.length-5);
				document.formpost.ucup.value = lima;
				document.formpost.ucup.onkeyup();
				var flag = "";
				if( data.flag_tg == 0 )
				{
				flag = "Tunggal";
				}
				else if( data.flag_tg == 1 )
				{
					flag = "Gabungan";
				}
				else
				{
					flag = "";
				}
				document.formpost.tipe.value = flag;
				document.formpost.mspa_date_book.value = data.tgl_rk;
				document.formpost.mspa_desc.value = 'CEK IBANK (No Transaksi ' + data.no_trx + ')';	
				document.formpost.no_trx.value = data.no_trx;
				document.formpost.lku_id.value = data.kurs;
				document.formpost.mspa_payment.value = data.jml_bayar;
				document.formpost.nilai_kurs.value = data.nilai_kurs;
			},
			timeout:180000,
			errorHandler:function(message){
				if(message == "null"){
					alert("Data Rekening Kartu Kredit Tidak Ada");
				}else{
					alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah.");
				}
			}
		});
		ajaxSelectWithParam(3, 'listCaraBayar', '_lsjb_id', 'lsjb_id', '', 'LSJB_ID', 'LSJB_TYPE', 'merchantDisplay(this.value)');
	}
	
	function getdataRkVa(noSpaj){
		var spaj = noSpaj;		
		ajaxManager.listdataVirtualAccount(spaj,
			{callback:function(data){
				DWRUtil.useLoadingMessage();
				var premi_sisa= data.premi_sisa;
				if(data.jml_bayar<= premi_sisa){				
					var lima = data.norek_ajs.substr(data.norek_ajs.length-5);				
					document.formpost.ucup.value = lima;
					document.formpost.ucup.onkeyup();
					var flag = "";					
					if( data.flag_tg == 0 )
					{
						flag = "Tunggal";
					}
					else if( data.flag_tg == 1 )
					{
						flag = "Gabungan";
					}
					else
					{
						flag = "";
					}					
					document.formpost.tipe.value = flag;
					document.formpost.mspa_date_book.value = data.tgl_rk;
					document.formpost.mspa_desc.value = 'CEK IBANK (No Transaksi ' + data.no_trx + ')';	
					document.formpost.no_trx.value = data.no_trx;
					document.formpost.lku_id.value = data.kurs;
					document.formpost.mspa_payment.value = data.jml_bayar;
					document.formpost.nilai_kurs.value = data.nilai_kurs;
					
				}else{
					alert ('Saldo ibank ini tidak cukup');
					return;
				}
			},
			
			timeout:180000,
			errorHandler:function(message){
				if(message == "null"){
					alert("Data Virtual Account Untuk SPAJ Ini Tidak Ada");
				}else{
					alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah.");
				}
			}
		});
	}
	
	function merchantDisplay(param){
		if(param == 3){
			document.getElementById("list_merchant").style.display = "";
		}else{
			document.getElementById("list_merchant").style.display = "none";
			document.formpost.mspa_flag_merchant.value = "";
		}
	}
</script>

<body style="height: 100%;" onload="document.formpost.lsrek_id.focus();">
	<div id="contents">
		<c:choose>
			<c:when test="${not empty cmd.initialErrors}">
				<div id="error">
					ERROR:<br>
					<c:forEach var="error" items="${cmd.initialErrors}">
						- <c:out value="${error}" escapeXml="false" />
						<br />
					</c:forEach>
				</div>
			</c:when>
			<c:otherwise>
				<form method="post" name="formpost" action="${path}/uw/inputpayment.htm">
				
					<input type="hidden" name="lca_polis" value="${lca_polis}">
					<spring:bind path="cmd.mspa_payment_id">
						<input type="hidden" name="${status.expression}" value="${status.value}">
					</spring:bind>
					<c:if test="${not empty billInfo}"><input type="hidden" name="bill_lku_id" value="${billInfo[0].LKU_ID}"></c:if>
					
					<fieldset>
						<legend>Billing</legend>
		
						<display:table id="billing" name="billInfo" class="displaytag" >
							<display:column property="MSBI_BEG_DATE" title="Begin Date"/>
							<display:column property="MSPO_POLICY_NO" title="No. Polis"/>
							<display:column property="MSBI_STAMP" title="Materai" format="{0, number, #,##0.00;(#,##0.00)}"/>
							<display:column property="LKU_SYMBOL" title="Mata Uang"/>
							<display:column property="BIAYA" title="Tagihan" format="{0, number, #,##0.00;(#,##0.00)}"/>
							<display:column property="BAYAR" title="Total Bayar" format="{0, number, #,##0.00;(#,##0.00)}"/>
							<display:column property="SISA" title="Sisa Bayar" format="{0, number, #,##0.00;(#,##0.00)}"/>
							<display:column property="KETERANGAN" title="Status"/>
						</display:table>
			
					</fieldset>
					
					<fieldset>
						<legend><div id="status">Payment (No Blanko : ${blankon})</div></legend>
						<table>
							<tr>
								<td style="vertical-align: top;">
									<br>
									<table class="simple" style="width: 120px;">
										<thead>
											<tr>
												<th colspan="3">Payment History</th>
											</tr>
											<tr>
												<th>Ke</th>
												<th>Jenis</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="pay" items="${paymentInfo }" varStatus="st">
												<tr onMouseOver="Javascript:this.style.cursor='hand';return true;"	
													onMouseOut="Javascript:return true;"
													onClick="tampil('edit', '${pay.mspa_payment_id}');"
													<c:if test="${pay.mspa_payment_id eq cmd.mspa_payment_id}"> style="background-color: #FFF2CA;" </c:if>
													>
													<td>
														${st.count} 
													</td>
													<td>
														${pay.tipe}
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
									<span class="info">* Click untuk detail</span>
								</td>
								<td>
									<fieldset style="width: 570px; ">
										<legend>Bank AJ Sinarmas</legend>
										<table class="entry" style="width: 100%;">
											<tr>
												<th>
													Masukkan Kode Bank
													<br><span class="info">(5 digit terakhir)</span>
												</th>
												<th colspan="5">
													<spring:bind path="cmd.lsrek_id">
														<input id="ucup" type="text" value="${cmd.kode}" maxlength="5" size="5" onkeyup="bank(this, '${status.expression}', '_${status.expression}', '${status.value}');">
														<div id="_${status.expression}">
															<select name="${status.expression}"></select>
														</div>
														<c:if test="${not empty cmd.kode}">
															<script>
																bank(document.formpost.ucup, '${status.expression}', '_${status.expression}', '${cmd.kode}');
															</script>
														</c:if>
													</spring:bind>
												</th>
											</tr>
											<tr>
												<th>Tanggal RK</th>
												<th>
													<spring:bind path="cmd.mspa_date_book">
														<script>inputDate('${status.expression}', '${status.value}', false);</script>
													</spring:bind>
												</th>
												<th>No. Pre</th>
												<th><input type="text" disabled="disabled" class="disabled" value="${cmd.mspa_no_pre }" size="12"/></th>
												<th>No. Voucher</th>
												<th><input type="text" disabled="disabled" class="disabled" value="${cmd.mspa_no_voucher}" size="12"/></th>
											</tr>
											<tr>
												<th colspan="6">
													<input type="button" value="Tarik Data Berdasarkan Tanggal RK" onclick="popWin('${path}/uw/uw.htm?window=drek&startDate='+document.formpost.mspa_date_book.value+'&endDate='+document.formpost.mspa_date_book.value+'&noSpaj=${cmd.reg_spaj}'+'&flagHalaman= 1'+'&startNominal=${billInfo[0].BIAYA}' + '&endNominal=${billInfo[0].BIAYA}', 600, 999, 'yes', 'yes');"
														accesskey="T" onmouseover="return overlib('Alt-T', AUTOSTATUS, WRAP);" onmouseout="nd();" <c:if test="${cmd.tanggalRkDisabled eq 1}"> disabled="disabled" </c:if> >
												</th>
											</tr>
											<tr>
								            	<th colspan="3">
								            		<input type="button" name="rkcc" value="Tarik Data Berdasarkan CC" onClick="getdataRkCc('${cmd.reg_spaj}');" <c:if test="${cmd.tanggalRkDisabled eq 1}"> disabled="disabled" </c:if>>
								            	</th>
								            	<th colspan="3">
								            		<input type="button" name="rkva" value="Tarik Data Berdasarkan VA" onClick="getdataRkVa('${cmd.reg_spaj}');" <c:if test="${cmd.tanggalRkDisabled eq 1}"> disabled="disabled" </c:if>>
								            	</th>
								            </tr>
										</table>
									</fieldset><br/>
									<fieldset style="width: 570px; ">
										<legend>Pembayaran</legend>
										<table class="entry" style="width: 100%;">
											<tr>
												<th>Tanggal Bayar</th>
												<th colspan="5">
													<spring:bind path="cmd.pay_date">
														<script>inputDate('${status.expression}', '${status.value}', false);</script>
													</spring:bind>								
													No. KTPP
													<spring:bind path="cmd.no_kttp">	
														<input type="text" name="${status.expression }" value="${status.value }">
													</spring:bind>
													User 
													<input type="text" name="lus_login_name" value="${cmd.lus_login_name }" disabled="disabled" class="disabled">
												</th>
											</tr>
											<tr>
												<th>Cara Bayar</th>
												<th colspan="2">
													<spring:bind path="cmd.lsjb_id">
														<div id="_${status.expression}"></div>
														<script>ajaxSelectWithParam('<c:choose><c:when test="${cmd.lsrek_id eq 0}">0</c:when><c:otherwise>1</c:otherwise></c:choose>', 'listCaraBayar', '_${status.expression }', '${status.expression }', '${status.value}', 'LSJB_ID', 'LSJB_TYPE', 'merchantDisplay(this.value)')</script>
													</spring:bind><br>
		
													Ref Polis No
													<spring:bind path="cmd.mspa_old_policy">	
														<input id="nopol"type="text" maxlength="14" size="18" name="${status.expression}" value="${cmd.mspa_old_policy}" onchange="pp(nopol,np)"/>
													</spring:bind>
												</th>
												<th width="150">
													Tipe<br><br>
												    Nama Pemegang Polis
												                			
												</th>
             									<th width="250">
             							 			<spring:bind path="cmd.tipe"> 
		                								<input type="text" name="${status.expression }" value="${status.value }"  style='background-color :#D4D4D4' readOnly>
		            									<c:if test="${ not empty status.errorMessage}"><a href="#"><img src="${path}/include/image/false.gif" width="11" height="11"  border="0" onMouseOver="showtip(this,event,'${status.errorMessage }')" onMouseOut="hidetip()" onClick="showtip(this,event,'${status.errorMessage }')"></a> 
		              									</c:if>						
		              							  	</spring:bind><br><br>
              							  			<spring:bind path="cmd.mcl_first"> 
                										<input id="np" type="text" name="${status.expression}"	value="${cmd.mcl_first}"    style='background-color :#D4D4D4' readOnly>
                								   		<c:if test="${not empty cmd.mspa_old_policy}">
															<script>
																pp(nopol,pp);
															</script>
														</c:if>             								
													</spring:bind>
												</th>
											</tr>
											<tr id="list_merchant" <c:if test="${cmd.lsjb_id ne 3}">style="display: none"</c:if>> 
												<th>Merchant</th>
								              	<th colspan="5">
									              	<spring:bind path="cmd.mspa_flag_merchant">
									                	<select name="${status.expression}">
									                  		<option value="" />
										                  	<c:forEach var="fee" items="${listMerchant}">
										                  		<option value="${fee.ID_MERCHANT}"
																	<c:if test="${fee.ID_MERCHANT eq status.value}">selected</c:if>>${fee.NAMA}
											                  	</option>
										                  	</c:forEach>
									                	</select>
									             		<c:if test="${ not empty status.errorMessage}"><a href="#"><img src="${path}/include/image/false.gif" width="11" height="11"  border="0" onMouseOver="showtip(this,event,'${status.errorMessage }')" onMouseOut="hidetip()" onClick="showtip(this,event,'${status.errorMessage }')"></a> 
									              		</c:if>
									              	</spring:bind>
								              	</th>
								            </tr>
											<tr style="display: ${jenis}"> 
												<th width="90">Jenis Transaksi</th>
												<th colspan="4">
													<spring:bind path="cmd.jenis"> 
									                	<select name="${status.expression }">
															<option value="" />
															<c:forEach var="jenis" items="${jenisTransaksiList}">
																<option value="${jenis.tahunKeAndPremiKe}" <c:if test="${jenis.tahunKeAndPremiKe eq status.value}">selected</c:if>>${jenis.descr}</option>
															</c:forEach>
									                	</select>
									             		<c:if test="${ not empty status.errorMessage}"><a href="#"><img src="${path}/include/image/false.gif" width="11" height="11"  border="0" onMouseOver="showtip(this,event,'${status.errorMessage }')" onMouseOut="hidetip()" onClick="showtip(this,event,'${status.errorMessage }')"></a> 
									              		</c:if>
								                	</spring:bind>
								                </th>
            								</tr>
											<tr>
												<th>Jumlah Bayar</th>
												<th colspan="5">
													<spring:bind path="cmd.lku_id">
														<select name="${status.expression }"
															onChange="return getkurs(this.value, document.formpost.mspa_date_book.value,'${sysDate}', this, document.formpost.nilai_kurs)">
															<option value="" />
															<c:forEach var="pay" items="${listKurs}">
																<option value="${pay.LKU_ID }"
																	<c:if test="${pay.LKU_ID eq status.value}">selected</c:if>
																>${pay.LKU_SYMBOL }</option>
															</c:forEach>
														</select>
													</spring:bind>
													<spring:bind path="cmd.mspa_payment">	
														<input type="text" name="${status.expression }" value="${status.value }">
													</spring:bind>
													Kurs $: 
													<c:choose>
														<c:when test="${not empty cmd.mspa_nilai_kurs_dollar}">
															<input type="text" name="nilai_kurs" value="${cmd.mspa_nilai_kurs_dollar}" readonly="readonly">
														</c:when>
														<c:otherwise>
															<input type="text" name="nilai_kurs" value="${cmd.mspa_nilai_kurs}" readonly="readonly">
														</c:otherwise>
													</c:choose>
												</th>
											</tr>
											<tr>
												<th>Untuk Pembayaran</th>
												<th colspan="5">
													<spring:bind path="cmd.premi_ke">
														<select name="${status.expression }">
															<c:forEach var="b" items="${billInfo}">
																<option value="${b.MSBI_PREMI_KE}"
																	<c:if test="${b.MSBI_PREMI_KE eq status.value}">selected</c:if>
																>Billing ${b.BIAYA }</option>
															</c:forEach>
														</select>
													</spring:bind>													
												</th>
											</tr>
											<tr>
												<th>Keterangan</th>
												<th colspan="5">
													<spring:bind path="cmd.mspa_desc">
														<textarea cols="64" rows="5" name="${status.expression }">${status.value }</textarea>
													</spring:bind>
													<spring:bind path="cmd.no_trx">
														<br/><input type="text" name="${status.expression}" value="${status.value}" readonly="readonly" class="readOnly" width="10">
													</spring:bind>
												</th>
											</tr>
										</table>
									</fieldset><br/>
									<fieldset style="width: 570px; ">
										<legend>Detail Pembayaran</legend>
										<table class="entry">
											<tr>
												<th>Client Bank</th>
												<th>
													<spring:bind path="cmd.client_bank">
														<select name="${status.expression }">
															<option value="" />
															<c:forEach var="bank" items="${listBankPusat}">
																<option value="${bank.LSBP_ID}"
																	<c:if test="${bank.LSBP_ID eq status.value}">selected</c:if>
																>${bank.LSBP_NAMA }</option>
															</c:forEach>
														</select>
													</spring:bind>
												</th>
											</tr>
											<tr>
												<th>No Giro / Cek / Credit Card</th>
												<th>
													<spring:bind path="cmd.mspa_no_rek">	
														<input type="text" name="${status.expression }" value="${status.value }">
													</spring:bind>
												</th>
											</tr>
											<tr>
												<th>Tanggal Jatuh Tempo</th>
												<th>
													<spring:bind path="cmd.mspa_due_date">	
														<script>inputDate('${status.expression}', '${status.value}', false);</script>
													</spring:bind>
												</th>
											</tr>
										</table>
									</fieldset>
		
									<div id="buttons">
										<c:choose>
											<c:when test="${not empty lunas}">
												<script>document.getElementById('status').innerHTML='PAYMENT [LUNAS]';</script>
											</c:when>
											<c:otherwise>
												<input type="button" name="add" value="Tambah Payment" onclick="tampil('add');"
													accesskey="A" onmouseover="return overlib('Alt-A', AUTOSTATUS, WRAP);" onmouseout="nd();">
											</c:otherwise>
										</c:choose>
										<input type="submit" name="save" value="Simpan Perubahan" onclick="return tampil('save');"
											accesskey="V" onmouseover="return overlib('Alt-V', AUTOSTATUS, WRAP);" onmouseout="nd();">
										<input type="button" name="cancel" value="Batalkan" onclick="tampil('refresh');"
											accesskey="L" onmouseover="return overlib('Alt-L', AUTOSTATUS, WRAP);" onmouseout="nd();">
									</div>							
									
									<spring:bind path="cmd.*">
										<c:if test="${not empty status.errorMessages}">
											<div id="error">
											ERROR:<br>
											<c:forEach var="error" items="${status.errorMessages}">
														- <c:out value="${error}" escapeXml="false" />
												<br />
												<script>errorMessages += '- ${error}\n';</script>
											</c:forEach></div>
										</c:if>									
									</spring:bind>
								</td>
							</tr>
						</table>
					</fieldset>
				</form>
			</c:otherwise>
		</c:choose>
	</div>
</body>
<script>
	if(errorMessages!=''){
		alert("ERROR: \n" + errorMessages);
	}
</script>
<%@ include file="/include/page/footer.jsp"%>