<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<script>

	function showPage(hal){
		//document.getElementById("speedy").disabled = false; 
		var spaj = document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value;
		var lsbs = document.formpost.kodebisnis.value;
		var lsdbs = document.formpost.numberbisnis.value;
		var copy_reg_spaj = document.formpost.copy_reg_spaj.value;
		if(spaj == '' &&( hal!='akum_new' && hal!='blacklist')){
			alert('Harap pilih SPAJ terlebih dahulu!');
		}else{
			createLoadingMessage();
			if('tampil' == hal){
				document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj+'&halForBlacklist=uw';
			}else if('endors' == hal){
				document.getElementById('infoFrame').src='${path}/bac/endors.htm?window=main&spaj='+spaj;
			}else if('edit' == hal){
				document.getElementById('infoFrame').src='${path}/bac/multi.htm?window=testRedirect&spaj='+spaj;
			}else if('editagen' == hal){
				document.getElementById('infoFrame').src='${path}/bac/editagenpenutup.htm?spaj='+spaj;			
			}else if('uwinfo' == hal){
				document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=uwinfo&spaj='+spaj;			
			}else if('checklist' == hal){
				document.getElementById('infoFrame').src='${path}/checklist.htm?lspd_id=2&reg_spaj='+spaj;
			}else if('sblink' == hal){
				popWin('${path}/uw/inputsblink.htm?reg_spaj='+spaj, 550, 875); 
			}else if('questionnaire' == hal){
				document.getElementById('infoFrame').src='${path}/uw/questionnaire.htm?reg_spaj='+spaj; 
			}else if('aksepManual' == hal){
				popWin('${path}/bac/pending.htm?spaj='+spaj, 250, 375);
			}else if('batal' == hal){
				document.getElementById('infoFrame').src='${path }/uw/cancelUw.htm?spaj='+spaj;
			}else if('status' == hal){
				document.getElementById('infoFrame').src='${path }/uw/status.htm?spaj='+spaj;
			}else if('premi' == hal){
				document.getElementById('infoFrame').src='${path }/uw/premi.htm?spaj='+spaj;
			}else if('medis' == hal){
				document.getElementById('infoFrame').src='${path }/uw/medical_new.htm?spaj='+spaj;			
			}else if('kyc' == hal){
				var result = confirm("Apakah termasuk Transaksi Keuangan Mencurigakan? Tekan YES untuk YA, dan CANCEL untuk TIDAK.", "U/W");
				proses = 0;
				if(result == true) proses=1;
				document.getElementById('infoFrame').src='${path }/uw/uw.htm?window=kyc_proses&spaj='+spaj+'&proses='+proses;
			}else if('simultan' == hal){
				document.getElementById('infoFrame').src='${path }/uw/simultan_new.htm?spaj='+spaj;
			}else if('reas' == hal){
				var result=confirm("Apakah Data Anda Sudah Benar ?\nCek Extra Premi, Medis dll..!", "U/W");
				if(result==true) document.getElementById('infoFrame').src='${path }/uw/reas_new.htm?spaj='+spaj;
			}else if('emailexpired' == hal){
				var result=confirm("Apakah Anda ingin mengirim email pemberitahuan pengembalian premi ?", "U/W");
				if(result==true) document.getElementById('infoFrame').src='${path }/uw/viewer.htm?window=emailexpired&spaj='+spaj;
			}else if('worksheet' == hal){
				document.getElementById('infoFrame').src='${path }/uw/worksheet.htm?spaj='+spaj+'&copy_reg_spaj='+copy_reg_spaj;
			}else if('reasguthrie' == hal){
				var result=confirm("SPAJ GUTHRIE! Lanjutkan Proses REAS?", "U/W");
				if(result==true) document.getElementById('infoFrame').src='${path }/uw/viewer.htm?window=reas_guthrie&spaj='+spaj;
			}else if('aksep' == hal){
				if(confirm("Apakah Anda Ingin Aksep/Fund Polis Ini?")){
					document.getElementById('infoFrame').src='${path }/uw/akseptasi.htm?spaj='+spaj;
				}
			}else if('transfer' == hal){
				if(confirm("Apakah Anda Ingin Transfer Polis Ini?")){
					document.getElementById('infoFrame').src='${path }/uw/transfer.htm?spaj='+spaj+'&flagNew='+1;
				}	
			}else if('approve' == hal){
				if(confirm("Apakah Semua Proses / Data sudah Di check ?")){
					document.getElementById('infoFrame').src='${path}/uw/uw.htm?window=prosesSpeedyUnclean&flag=1&spaj='+spaj;
				}	
			}else if('baby' == hal){
				document.getElementById('infoFrame').src='${path}/uw/uw.htm?window=prosesSmileBaby&spaj='+spaj;
			}else if('ubahreas' == hal){
				document.getElementById('infoFrame').src='${path }/uw/ubah_reas.htm?spaj='+spaj;
			}else if('nik' == hal){
				if  (lsbs == 138 || lsbs == 140 || lsbs == 141 || lsbs == 148 || lsbs == 149 || lsbs == 156 || lsbs == 139 || 
						(lsbs == 142 && lsdbs == 4) || (lsbs == 143 && lsdbs == 2) || (lsbs == 158 && lsdbs == 4) || (lsbs == 158 && lsdbs == 7)){
					popWin('${path}/bac/nik.htm?sts=insert&spaj='+spaj, 500, 800);
				}else{
					alert('Nik tidak dapat diisi untuk polis ini');					
				}
			}else if('tglspaj' == hal){
				document.getElementById('infoFrame').src='${path }/uw/viewer.htm?window=editTglTrmKrmSpaj&spaj='+spaj+'&show=2';
			}else if('viewlimit' == hal){
				popWin('${path}/common/menu.htm?frame=view_limit_uw', 600, 800);
			}else if('viewsimultan' == hal){
				popWin('${path }/uw/viewer.htm?window=view_simultan', 600, 800);
			}else if('viewkyc' == hal){
				popWin('${path }/uw.htm?window=kyc_proses', 600, 800);
			}else if('hcp' == hal){
				if (lsbs != 161){
					if(lsbs==183 || lsbs==189 || lsbs==193){
							popWin('${path}/bac/ttghcp.htm?sts=insert&showSPAJ='+spaj, 500, 800);
					}else {
						if (eval(document.formpost.jml_peserta.value) > 0){
							popWin('${path}/bac/ttghcp.htm?sts=insert&showSPAJ='+spaj, 500, 800);
						}else{
							alert("Tidak bisa mengisi data SM/Eka Sehat/HCP Family karena belum mengambil plan BASIC, Silahkan melakukan penginputan plan BASIC pada halaman penginputan spaj utama terlebih dahulu.");
						}
					}
				}else{
						alert("Produk ini tidak bisa mengisi data SM/Eka Sehat/HCP Family");
				}
			}else if('reffbii' == hal){
				popWin('${path}/bac/reff_bank.htm?window=main&spaj='+spaj, 400, 700);
			}else if('claim' == hal){
				popWin('${path}/uw/uw.htm?window=healthClaim&spaj='+spaj, 400, 900);
			}else if('tglTerimaSpaj' == hal){
				popWin('${path}/uw/viewer.htm?window=editTglTrmKrmSpaj&spaj='+spaj+'&show=0', 150, 350);
 			}else if('titipanpremi' == hal){
 				popWin('${path}/ttppremi/titipan_premi.htm?sts=insert&editSPAJ='+spaj, 500, 800);
 			}else if('upload_nb' == hal){
 				document.getElementById('infoFrame').src='${path}/uw/upload_nb.htm?reg_spaj='+document.formpost.spaj.value;
 			}else if('spesial_hadiah' == hal){
 				//alert('${path}/uw/uw.htm?window=spesial_hadiah&reg_spaj='+spaj);
 				//popWin('${path}/uw/uw.htm?window=spesial_hadiah&reg_spaj='+spaj, 400, 900);
 				document.getElementById('infoFrame').src='${path}/uw/uw.htm?window=spesial_hadiah&reg_spaj='+document.formpost.spaj.value;
 			}else if('viewDMTM' == hal){
 			
 			    if (lsbs == 183 && lsdbs > 30 || lsbs == 189 && lsdbs > 15  ||lsbs == 204 || lsbs==169 || (lsbs==197 && lsdbs==2) || (lsbs == 212 && lsdbs == 8) || (lsbs == 73 && lsdbs == 15) || (lsbs == 203 && lsdbs == 4) || 
					(lsbs == 163 & (lsdbs >= 21 && lsdbs <= 25)) || (lsbs == 173 & (lsdbs >= 7 && lsdbs <= 9))){ //helpdesk [148055] produk DMTM Dana Sejaterah 163 26-30 & Smile Sarjana 173 7-9 //helpdesk [150296] DMTM BSIM 163 21-25 tambah simple questionare SIO+
					popWin('${path}/uw/uw.htm?window=view_kesehatanDMTM&json=0&lsbs='+lsbs+'&spaj='+document.formpost.spaj.value, 400, 900); 					
				}else if((lsbs == 221 & (lsdbs >= 1 && lsdbs <= 12)) || (lsbs == 195 & (lsdbs >= 61 && lsdbs <= 72))){
					popWin('${path}/uw/uw.htm?window=view_kesehatanDMTM&json=0&lsbs='+lsbs+'&spaj='+document.formpost.spaj.value, 600, 900);
				}else{
					alert('View Kesehatan / Questionare ini hanya untuk Produk DMTM(SMiLe Medical/Eka Waktu)');					
				}
				
/*			}else if((lsbs==163 && (lsdbs>=6 && lsdbs<=10)) ){
 					  popWin('${path}/uw/uw.htm?window=view_kesehatanDMTM&json=0&lsbs='+lsbs+'&spaj='+document.formpost.spaj.value, 400, 900); */
 			}else if('questionare_dmtm' == hal){
 				popWin('${path}/bac/questionareSimple.htm?spaj='+spaj+'&show=4', 400, 700);
 			}else if('call' == hal) {
				popWin('${path}/uw/viewer.htm?window=csfcall&spaj='+spaj, 480, 640); 
			}else if('speedy' == hal){
				var owner = check('owner_sign');
				var owner2 = check('owner2_sign');
				var parent = check('parent_sign');
				var agent = check('agent_sign');
				var reviewed = check('reviewed');
				var health_quest = check('health_quest');
				
				ajaxManager.listcollect(document.formpost.spaj.value , 'region',
					{callback:function(map) {
						DWRUtil.useLoadingMessage();
						document.formpost.flag_spaj.value = map.flag_spaj;
						document.formpost.flag_produk.value = map.flag_produk;
					   },
					  timeout:15000,
					  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
					});
				
				var result = confirm("Apakah SPAJ "+spaj+" akan di proses speedy?\nHarap dicek Nama Pemegang, Nama Tertanggung, Tgl Lahir Pemegang dan Tertanggung\nKarena akan dilakukan simultan di proses speedy sehingga tidak bisa dilakukan edit data-data tertentu contohnya tgl lahir, dll.", "SPEEDY");
				
				var fs = document.formpost.flag_spaj.value;
				var fp = document.formpost.flag_produk.value;
				
				if(fs==2){
					if(fp>0){
						if(result==true){
							if(owner==null){
								alert("Harap isi Owner's & Insured's Signature");
							}else if(owner2==null){
								alert("Harap isi Owner's & Insured's Signature Same With Application");
							}else if(agent==null){
								alert("Harap isi Agent's Signature");
							}else if(reviewed==null){
								alert("Harap isi Reviewed by User");
							}else if(health_quest==null){
								alert("produk ini harap isi Health Quest");
							}else{
								document.getElementById('infoFrame').src='${path}/uw/uw.htm?window=prosesSpeedy&flag=1&spaj='+spaj+'&owner='+owner+'&owner2='+owner2+'&parent='+parent+'&agent='+agent+'&reviewed='+reviewed+'&health_quest='+health_quest;
								//document.getElementById('infoFrame').src='${path}/uw/transferparalel.htm?flag=1&spaj='+spaj+'&owner='+owner+'&owner2='+owner2+'&parent='+parent+'&agent='+agent+'&reviewed='+reviewed+'&health_quest='+health_quest;
								document.getElementById("speedy").disabled = true; 
							}
						}else{
							hideLoadingMessage();
						}
					}else{
						if(result==true){
							if(owner==null){
								alert("Harap isi Owner's & Insured's Signature");
							}else if(owner2==null){
								alert("Harap isi Owner's & Insured's Signature Same With Application");
							}else if(agent==null){
								alert("Harap isi Agent's Signature");
							}else if(reviewed==null){
								alert("Harap isi Reviewed by User");
							}else{
								//document.getElementById('infoFrame').src='${path}/uw/transferparalel.htm?flag=1&spaj='+spaj+'&owner='+owner+'&owner2='+owner2+'&parent='+parent+'&agent='+agent+'&reviewed='+reviewed+'&health_quest='+health_quest;
								document.getElementById('infoFrame').src='${path}/uw/uw.htm?window=prosesSpeedy&flag=1&spaj='+spaj+'&owner='+owner+'&owner2='+owner2+'&parent='+parent+'&agent='+agent+'&reviewed='+reviewed+'&health_quest='+health_quest;
								document.getElementById("speedy").disabled = true; 
							}
						}else{
							hideLoadingMessage();
						}
					}
				}else{
					if(result==true){
						if(owner==null){
							alert("Harap isi Owner's & Insured's Signature");
						}else if(owner2==null){
							alert("Harap isi Owner's & Insured's Signature Same With Application");
						}else if(agent==null){
							alert("Harap isi Agent's Signature");
						}else if(reviewed==null){
							alert("Harap isi Reviewed by User");
						}else if(health_quest==null){
							alert("Harap isi Health Quest");
						}else{
							//document.getElementById('infoFrame').src='${path}/uw/transferparalel.htm?flag=1&spaj='+spaj+'&owner='+owner+'&owner2='+owner2+'&parent='+parent+'&agent='+agent+'&reviewed='+reviewed+'&health_quest='+health_quest;
							document.getElementById('infoFrame').src='${path}/uw/uw.htm?window=prosesSpeedy&flag=1&spaj='+spaj+'&owner='+owner+'&owner2='+owner2+'&parent='+parent+'&agent='+agent+'&reviewed='+reviewed+'&health_quest='+health_quest;
							document.getElementById("speedy").disabled = true; 
						}
					}else{
						hideLoadingMessage();
					}
				}
				
			}else if('transferToUw' == hal){
				if(confirm("Apakah Anda Ingin Transfer ke UW Proses Polis Ini?")){
					document.getElementById('infoFrame').src='${path }/uw/uw.htm?window=transferToUw&spaj='+spaj;
				}else{
					hideLoadingMessage();
				}	
			}else if('transferToUwHelpdesk' == hal){
 				if(confirm("Apakah Anda Ingin Transfer ke UW Helpdesk Polis Ini?")){
					document.getElementById('infoFrame').src='${path }/uw/uw.htm?window=transferToUwHelpdesk&spaj='+spaj;
 				}else{
 					hideLoadingMessage();
 				}	
			}else if('email_further' == hal){
				document.getElementById('infoFrame').src='${path}/uw/uw.htm?window=email_further&spaj='+spaj;
			}else if('questionare' == hal){
				// document.getElementById('infoFrame').src='${path}/bac/questionarenew.htm?spaj='+spaj+'&mode=1';
				document.getElementById('infoFrame').src='${path}/bac/questionarenew.htm?flag=1&mode=1&spaj='+spaj;
			}else if('backtobas' == hal){
				document.getElementById('infoFrame').src='${path}/uw/uw.htm?window=backtobas&spaj='+spaj;
			}else if('akum_new' == hal){				
				document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=akum_new';
			}else if('blacklist' == hal){				
				popWin('${path}/uw/blacklist.htm', 400, 900);
			}else if('worksheet' == hal){
				document.getElementById('infoFrame').src='${path }/uw/worksheet.htm?spaj='+spaj+'&copy_reg_spaj='+copy_reg_spaj;
			}
		}
	}
	
	function jalurkhusus(hal){
	 if('autoproses' == hal){
					document.getElementById('infoFrame').src='${path}/uw/uw.htm?window=autoprosesUwSimas';
			}
	}
	/*
	function simultan(){
		createLoadingMessage();
		var spaj=document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value
		document.getElementById('infoFrame').src='${path }/uw/simultan.htm?spaj='+spaj;
	}
	function reas(){
		createLoadingMessage();
		var spaj=document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value
		var result=confirm("Apakah Data Anda Sudah Benar ?\nCek Extra Premi, Medis dll..!", "U/W");
		if(result==true)
			document.getElementById('infoFrame').src='${path }/uw/reas.htm?spaj='+spaj;
	}
	*/
	
	function cariregion(spaj,nama){
		if(spaj != ''){
			if(document.getElementById('docFrame')) document.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+spaj;
			if(document.getElementById('infoFrame')) document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj+'&halForBlacklist=uw';
			ajaxManager.listcollect(spaj , nama,
			{callback:function(map) {
				DWRUtil.useLoadingMessage();
				document.formpost.koderegion.value=map.LSRG_NAMA;
				document.formpost.kodebisnis.value = map.LSBS_ID;
				document.formpost.numberbisnis.value = map.LSDBS_NUMBER;
				document.formpost.jml_peserta.value = map.jml_peserta;
				document.formpost.flag_spaj.value = map.flag_spaj;
				document.formpost.flag_produk.value = map.flag_produk;
				
				if(document.formpost.flag_spaj.value==2 || document.formpost.flag_spaj.value==3 || document.formpost.flag_spaj.value==4){
					document.getElementById("questionare").disabled = false; 
				}else{
					document.getElementById("questionare").disabled = true; 
				}
				document.getElementById("speedy").disabled = false; 
			   },
			  timeout:15000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
			});
			
		}	
	}
	
	function awal(){
		if('${snow_spaj}'!=''){ 
			document.formpost.spaj.value='${snow_spaj}';
			document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value='${snow_spaj}';
			cariregion('${snow_spaj}','region');
		}else{
			cariregion(document.formpost.spaj.value,'region');
		}
		setFrameSize('infoFrame', 90);
		setFrameSize('docFrame', 90);
	}
	
	function copyToClipboard(txt){
	
		if( window.clipboardData && clipboardData.setData ){
		
			clipboardData.setData("Text", txt);
		}
		else{
		 alert("Internet Explorer required"); 
		}
	}
	
	function run(){
		var shell = new ActiveXObject("WScript.Shell"); 
		var commandtoRun = "\\\\aplikasi\\lions\\secan4400f.exe";
	   
		if (shell){		
		 shell.run(commandtoRun); 
		} 
		else{ 
			alert("program or file doesn't exist on your system."); 
		}
	}
	
	function copyAndRun(){ 
	 	txt = document.getElementById("spaj").value;
	 	copyToClipboard(txt);
	 	run();		
	}
	
	function check(name){
	    var res = null;
	    var currObj = document.formpost[name];
	    
	    for (var i=0;i<currObj.length;i++) {
			if (currObj[i].checked) {
				res = currObj[i].value;
			}
		}
	    
	    return res;
	}

</script>
<body  onload="awal();" onresize="setFrameSize('infoFrame', 90); setFrameSize('docFrame', 90);" style="height: 100%;">
<form name="formpost" method="post">
<div class="tabcontent">
<table class="entry2" style="width: 98%;">
	<tr>
		<th>Cari SPAJ</th>
		<td> <input type="hidden" name="halForBlacklist" value="uw"> 
		  <input type="hidden" name="koderegion" > 
          <input type="hidden" name="kodebisnis">
          <input type="hidden" name="numberbisnis">
          <input type="hidden" name="jml_peserta">
          <input type="hidden" name="flag_spaj">
          <input type="hidden" name="flag_produk">
          <input type="hidden" name="copy_reg_spaj" value="" />
          <input type="hidden" name="spajrecur" id="spajrecur" value="${spajrecur}" />
			<select name="spaj" id="spaj" onChange="cariregion(this.options[this.selectedIndex].value,'region');">
				<option value="">[--- Silahkan Pilih/Cari SPAJ ---]</option>
				<c:forEach var="s" items="${daftarSPAJ}">
					<option value="${s.REG_SPAJ }" style="background-color: ${s.BG};" 
						<c:if test="${s.REG_SPAJ eq param.spaj}">selected<c:set var="ada" value="ada" /></c:if>>${s.SPAJ_FORMATTED}
					- ${s.POLICY_FORMATTED }</option>
				</c:forEach>
			</select>
			
			<input type="button" value="Info" name="info" 				onclick="showPage('tampil');" accesskey="I" onmouseover="return overlib('Alt-I', AUTOSTATUS, WRAP);" onmouseout="nd();">
			
			<c:choose>
				<c:when test="${posisi_uw  eq 27 }">
					<input type="button" value="Cari" name="search" 			onclick="popWin('${path}/uw/spaj.htm?posisi=27&win=uw', 600, 800); " accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">
				</c:when>
				<c:when test="${posisi_uw  eq 209 }">
					<input type="button" value="Cari" name="search" 			onclick="popWin('${path}/uw/spaj.htm?posisi=209&win=uw', 600, 800); " accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">
				</c:when>
				<c:otherwise>
					<input type="button" value="Cari" name="search" 			onclick="popWin('${path}/uw/spaj.htm?posisi=2&win=uw', 600, 800); " accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">
				</c:otherwise>
			</c:choose>
			
			<input type="button" value="Edit" name="info" 				onclick="showPage('edit');" accesskey="J" onmouseover="return overlib('Alt-J', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Edit Agen" name="agen" 			onclick="showPage('editagen');" accesskey="F">
			<input type="button" value="U/W Info" name="uwinfo" 		onclick="showPage('uwinfo');" accesskey="W" onmouseover="return overlib('Alt-W', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Reff Bank" name="reffbiiiiiii"	onclick="showPage('reffbii');">
			<input type="button" value="Scan" name="copySpaj"			onclick="copyAndRun();">
			<input type="button" value="List Powersave By Name" 		onclick="document.getElementById('infoFrame').src='${path}/report/uw.htm?window=list_polis_powersave';" name="list_powersave">
			<input type="button" value="Upload Scan" name="upload_nb"	onclick="showPage('upload_nb');" accesskey="O" onmouseover="return overlib('Alt-O', AUTOSTATUS, WRAP);" onmouseout="nd();">
		</td>
	</tr>
	
	<c:choose>
		<c:when test="${posisi_uw  eq 27 }">
		<!-- Speedy -->
		<tr>
			<th>Speedy</th>
			<td>
				<table class="entry2" style="width: 700px;">
					<tr>
						<th>Owner's & Insured's Signature</th>
						<td>
							<input name="owner_sign" id="owner_sign_yes" type="radio" class="radio" value="1" style="border: none;" >Ada &nbsp;	   
				  				<input name="owner_sign" id="owner_sign_no" type="radio" class="radio" value="0" style="border: none;" >Tidak
						</td>
						<th>Parent's Signature if Insured age < 17 yo</th>
						<td>
							<input name="parent_sign" id="parent_sign_yes" type="radio" class="radio" value="1" style="border: none;" >Ada &nbsp;	   
				  				<input name="parent_sign" id="parent_sign_no" type="radio" class="radio" value="0" style="border: none;" >Tidak
						</td>
					</tr>
					<tr>
						<th>Agent's Signature</th>
						<td>
							<input name="agent_sign" id="agent_sign_yes" type="radio" class="radio" value="1" style="border: none;" >Ada &nbsp;	   
				  				<input name="agent_sign" id="agent_sign_no" type="radio" class="radio" value="0" style="border: none;" >Tidak
						</td>
						<th>Reviewed by User</th>
						<td>
							<input name="reviewed" id="reviewed_yes" type="radio" class="radio" value="1" style="border: none;" >Ya &nbsp;	   
				  				<input name="reviewed" id="reviewed_no" type="radio" class="radio" value="0" style="border: none;" >Tidak
						</td>
					</tr>
					<tr>
						<th>Health Quest</th>
						<td>
							<input name="health_quest" id="health_quest_yes" type="radio" class="radio" value="1" style="border: none;" >Clean &nbsp;	   
				  				<input name="health_quest" id="health_quest_no" type="radio" class="radio" value="0" style="border: none;" >Unclean
						</td>
						<th>Owner's & Insured's Signature Same With Application</th>
						<td>
							<input name="owner2_sign" id="owner2_sign_yes" type="radio" class="radio" value="1" style="border: none;" >Ya &nbsp;	   
				  				<input name="owner2_sign" id="owner2_sign_no" type="radio" class="radio" value="0" style="border: none;" >Tidak
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<!-- <tr>
						<th>Berkas Tidak Lengkap</th>
						<td colspan="3">
							<input type="checkbox" name="chk_bsb" id="chk_bsb" style="border: none;" />BSB
							<input type="checkbox" name="chk_kuasa_db" id="chk_kuasa_db" style="border: none;" />Surat Kuasa Debet
							<input type="checkbox" name="chk_pernyataan" id="chk_pernyataan" style="border: none;" />Surat Pernyataan
							<input type="checkbox" name="chk_proposal" id="chk_proposal" style="border: none;" />Proposal
							<input type="checkbox" name="chk_spaj" id="chk_spaj" style="border: none;" />SPAJ
							<input type="checkbox" name="chk_pap" id="chk_pap" style="border: none;" />PAP
						</td>
					</tr> -->
					<tr>
						<td colspan="7">
							<input type="button" value="Speedy" name="speedy" onclick="showPage('speedy');">
							<!-- <input type="button" value="E-Mail Further & Transfer" name="email_further" onclick="showPage('email_further');"> -->
							<input type="button" value="Questionare" id="questionare" name="questionare" onclick="showPage('questionare');">
							<input type="button" value="Back To BAS" name="backtobas" onclick="showPage('backtobas');">
							<input name="ttp" type="button" value="Titipan Premi"  onclick="showPage('titipanpremi')" accesskey="Z" onmouseover="return overlib('Titipan Premi', AUTOSTATUS, WRAP);" onmouseout="nd();">
							<input type="button" value="View Kesehatan DMTM" name="dmtm" onclick="showPage('viewDMTM');" onmouseover="return overlib('VIEW QUESTIONARE DMTM', AUTOSTATUS, WRAP);" onmouseout="nd();">
							<input type="button" value="Pengisian Kuisioner" name="btn_questionare" onClick="showPage('questionare_dmtm');" onmouseover="return overlib('Pengisian Questionare DMTM', AUTOSTATUS, WRAP);" onmouseout="nd();">
							<input name="btn_akumnew" type="button" value="Akum New" onClick="showPage('akum_new');" onmouseover="return overlib('Akum New', AUTOSTATUS, WRAP);" onmouseout="nd();">
							<input type="button" value="View Kesehatan" name="claim" onclick="showPage('claim');" onmouseover="return overlib('VIEW POLIS KESEHATAN', AUTOSTATUS, WRAP);" onmouseout="nd();">
							<input type="button" value="Attention List" name="blacklist" onclick="showPage('blacklist');" onmouseover="return overlib('VIEW ATTENTION LIST', AUTOSTATUS, WRAP);" onmouseout="nd();">
							<input type="button" value="Worksheet" name="btn_uw_worksheet" onClick="showPage('worksheet');" onmouseover="return overlib('UW WORKSHEET', AUTOSTATUS, WRAP);" onmouseout="nd();">
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<!-- End Speedy -->
		</c:when>
		<c:when test="${posisi_uw  eq 209 }">
		<!-- UW Helpdesk -->
		<tr>
			<th>Proses Utama</th>
			<td>
				<!--
				<input name="btn_simultan" type="button" value="Simultan" onClick="simultan()" accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input disabled name="btn_reas"  type="button" value="Reas" onClick="reas()" accesskey="R" onmouseover="return overlib('Alt-R', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input disabled name="btn_transfer" type="button" value="Transfer" onClick="transfer('0')" accesskey="T" onmouseover="return overlib('Alt-T', AUTOSTATUS, WRAP);" onmouseout="nd();">
				-->
				<input name="btn_TglTerimaSpaj" type="button" value="Tgl Spaj Doc"	 	onClick="showPage('tglspaj');" accesskey="G" onmouseover="return overlib('Alt-G', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_endors" type="button" value="Endors"		 			onClick="showPage('endors');">
				<input name="ttp" type="button" value="Titipan Premi"  onclick="showPage('titipanpremi')" accesskey="Z" onmouseover="return overlib('Titipan Premi', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_simultan_new" type="button" value="Simultan" 			onClick="showPage('simultan');" accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_kyc" type="button" value="KYC" 						onClick="showPage('kyc');" accesskey="K" onmouseover="return overlib('Alt-K', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_reas_new"  type="button" value="Reas" 					onClick="showPage('reas');" accesskey="R" onmouseover="return overlib('Alt-R', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_email_expired"  type="button" value="Email Expired" 	onClick="showPage('emailexpired');" onmouseover="return overlib('EMAIL EXPIRED SPAJ', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_uw_worksheet"  type="button" value="Worksheet" 		onClick="showPage('worksheet');" onmouseover="return overlib('UW WORKSHEET', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_aksep" type="button" value="Akseptasi" 				onClick="showPage('aksep');" accesskey="A" onmouseover="return overlib('Alt-A', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<!-- <input name="btn_transfer_new" type="button" value="Transfer" 			onClick="showPage('transfer')" accesskey="T" onmouseover="return overlib('Alt-T', AUTOSTATUS, WRAP);" onmouseout="nd();"> -->
				<input name="btn_transfer_new" type="button" value="Transfer to UW Proses" onClick="showPage('transferToUw')" accesskey="T" onmouseover="return overlib('Alt-T', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="Questionare" id="questionare" name="questionare" onclick="showPage('questionare');" accesskey="Q" onmouseover="return overlib('Alt-Q', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_reas_guthrie" type="button" value="Reas Guthrie" 		onClick="showPage('reasguthrie');" onmouseover="return overlib('REAS KHUSUS PT GUTHRIE', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_checklist" type="button" value="Checklist" 			onClick="showPage('checklist');" accesskey="N" onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_sblink" type="button" value="SB Link" 					onClick="showPage('sblink');" onmouseover="return overlib('Input Save Bayar Link', AUTOSTATUS, WRAP);" onmouseout="nd();">			
				<input name="btn_questionnaire" type="button" value="Random Sample BII"	onClick="showPage('questionnaire');" onmouseover="return overlib('Input Random Sampling Kuesioner BII', AUTOSTATUS, WRAP);" onmouseout="nd();">			
			    <input name="btn_aksepManual" type="button" value="Aksep Manual" 		onClick="showPage('aksepManual');" onmouseover="return overlib('Aksep Manual (Worksite)', AUTOSTATUS, WRAP);" onmouseout="nd();">
			  
			</td>
		</tr>
		<tr>
			<th>Proses Tambahan</th>
			<td><%--<input name="btn_batal" type="button" value="Batalkan" 			onClick="showPage('batal');" accesskey="B" onmouseover="return overlib('Alt-B	', AUTOSTATUS, WRAP);" onmouseout="nd();">--%>
				
				<input name="btn_status" type="button" value="Status" 			onClick="showPage('status');" accesskey="U" onmouseover="return overlib('Alt-U', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_medis" type="button" value="Medis" 			onClick="showPage('medis');" accesskey="M" onmouseover="return overlib('Alt-M', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_change" type="button" value="Ubah Reas" 		onClick="showPage('ubahreas');" accesskey="H" onmouseover="return overlib('Alt-H', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_premi"  type="button" value="Premium" 			onClick="showPage('premi');" accesskey="E" onmouseover="return overlib('Alt-E', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_nik"  type="button" value="NIK" 				onClick="showPage('nik');" accesskey="Y" onmouseover="return overlib('Alt-Y', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="View Tabel U/W" name="limit"		onclick="showPage('viewlimit');" accesskey="L" onmouseover="return overlib('Alt-L > View Limit U/W Individu', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="View Simultan" name="view simultan"	onclick="showPage('viewsimultan');" onmouseover="return overlib('View Simultan Polis', AUTOSTATUS, WRAP);" onmouseout="nd();">
	<!-- 		<input type="button" value="SM/Eka Sehat/HCP FAMILY" name="hcp"				onclick="showPage('hcp');" onmouseover="return overlib('HCP FAMILY', AUTOSTATUS, WRAP);" onmouseout="nd();"> -->
				<input type="button" value="View Kesehatan" name="claim" onclick="showPage('claim');" onmouseover="return overlib('VIEW POLIS KESEHATAN', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_TglTerimaSpajMall" type="button" value="Tgl Terima SPAJ"	 	onClick="showPage('tglTerimaSpaj');" accesskey="Z" onmouseover="overlib('Alt-Z', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_SpesialHadiah" type="button" value="Spesial Hadiah" onClick="showPage('spesial_hadiah');" accesskey="P" onmouseover="overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="Call" name="call" onclick="showPage('call');" style="width: 35px;" onmouseover="return overlib('Alt-L', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="Summary" name="summary" onclick="showPage('summary');" style="width: 65px;"	 onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="Auto Proses H+1" name="autoproses" onclick="jalurkhusus('autoproses');" style="width: 120px;"	 onmouseover="return overlib('Auto Proses Untuk H+1', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="View Kesehatan DMTM" name="dmtm" onclick="showPage('viewDMTM');" onmouseover="return overlib('VIEW QUESTIONARE DMTM', AUTOSTATUS, WRAP);" onmouseout="nd();">
				
			</td>
		</tr>
		<!-- End UW Helpdesk -->
		</c:when>
		<c:otherwise>
		<tr>
			<th>Proses Utama</th>
			<td>
				<!--
				<input name="btn_simultan" type="button" value="Simultan" onClick="simultan()" accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input disabled name="btn_reas"  type="button" value="Reas" onClick="reas()" accesskey="R" onmouseover="return overlib('Alt-R', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input disabled name="btn_transfer" type="button" value="Transfer" onClick="transfer('0')" accesskey="T" onmouseover="return overlib('Alt-T', AUTOSTATUS, WRAP);" onmouseout="nd();">
				-->
				<input name="btn_TglTerimaSpaj" type="button" value="Tgl Spaj Doc"	 	onClick="showPage('tglspaj');" accesskey="G" onmouseover="return overlib('Alt-G', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_endors" type="button" value="Endors"		 			onClick="showPage('endors');">
				<input name="ttp" type="button" value="Titipan Premi"  onclick="showPage('titipanpremi')" accesskey="Z" onmouseover="return overlib('Titipan Premi', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_simultan_new" type="button" value="Simultan" 			onClick="showPage('simultan');" accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_kyc" type="button" value="KYC" 						onClick="showPage('kyc');" accesskey="K" onmouseover="return overlib('Alt-K', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_reas_new"  type="button" value="Reas" 					onClick="showPage('reas');" accesskey="R" onmouseover="return overlib('Alt-R', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_email_expired"  type="button" value="Email Expired" 	onClick="showPage('emailexpired');" onmouseover="return overlib('EMAIL EXPIRED SPAJ', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_uw_worksheet"  type="button" value="Worksheet" 		onClick="showPage('worksheet');" onmouseover="return overlib('UW WORKSHEET', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_aksep" type="button" value="Akseptasi" 				onClick="showPage('aksep');" accesskey="A" onmouseover="return overlib('Alt-A', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_transfer_new" type="button" value="Transfer" 			onClick="showPage('transfer')" accesskey="T" onmouseover="return overlib('Alt-T', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<!-- <input name="btn_transfer_to_helpdesk" type="button" value="Transfer to UW Helpdesk" onClick="showPage('transferToUwHelpdesk')" accesskey="H" onmouseover="return overlib('Alt-H', AUTOSTATUS, WRAP);" onmouseout="nd();"> -->
				<input name="btn_reas_guthrie" type="button" value="Reas Guthrie" 		onClick="showPage('reasguthrie');" onmouseover="return overlib('REAS KHUSUS PT GUTHRIE', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_checklist" type="button" value="Checklist" 			onClick="showPage('checklist');" accesskey="N" onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_sblink" type="button" value="SB Link" 					onClick="showPage('sblink');" onmouseover="return overlib('Input Save Bayar Link', AUTOSTATUS, WRAP);" onmouseout="nd();">			
				<input name="btn_questionnaire" type="button" value="Random Sample BII"	onClick="showPage('questionnaire');" onmouseover="return overlib('Input Random Sampling Kuesioner BII', AUTOSTATUS, WRAP);" onmouseout="nd();">			
			    <input name="btn_aksepManual" type="button" value="Aksep Manual" 		onClick="showPage('aksepManual');" onmouseover="return overlib('Aksep Manual (Worksite)', AUTOSTATUS, WRAP);" onmouseout="nd();">
			    <input name="btn_akumnew" type="button" value="Akum New" 				onClick="showPage('akum_new');" onmouseover="return overlib('Akum New', AUTOSTATUS, WRAP);" onmouseout="nd();">
			</td>
		</tr>
		<tr>
			<th>Proses Tambahan</th>
			<td><%--<input name="btn_batal" type="button" value="Batalkan" 			onClick="showPage('batal');" accesskey="B" onmouseover="return overlib('Alt-B	', AUTOSTATUS, WRAP);" onmouseout="nd();">--%>
				
				<input name="btn_status" type="button" value="Status" 			onClick="showPage('status');" accesskey="U" onmouseover="return overlib('Alt-U', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_medis" type="button" value="Medis" 			onClick="showPage('medis');" accesskey="M" onmouseover="return overlib('Alt-M', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_change" type="button" value="Ubah Reas" 		onClick="showPage('ubahreas');" accesskey="H" onmouseover="return overlib('Alt-H', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_premi"  type="button" value="Premium" 			onClick="showPage('premi');" accesskey="E" onmouseover="return overlib('Alt-E', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_nik"  type="button" value="NIK" 				onClick="showPage('nik');" accesskey="Y" onmouseover="return overlib('Alt-Y', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="View Tabel U/W" name="limit"		onclick="showPage('viewlimit');" accesskey="L" onmouseover="return overlib('Alt-L > View Limit U/W Individu', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="View Simultan" name="view simultan"	onclick="showPage('viewsimultan');" onmouseover="return overlib('View Simultan Polis', AUTOSTATUS, WRAP);" onmouseout="nd();">
	<!-- 		<input type="button" value="SM/Eka Sehat/HCP FAMILY" name="hcp"				onclick="showPage('hcp');" onmouseover="return overlib('HCP FAMILY', AUTOSTATUS, WRAP);" onmouseout="nd();"> -->
				<input type="button" value="View Kesehatan" name="claim" onclick="showPage('claim');" onmouseover="return overlib('VIEW POLIS KESEHATAN', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_TglTerimaSpajMall" type="button" value="Tgl Terima SPAJ"	 	onClick="showPage('tglTerimaSpaj');" accesskey="Z" onmouseover="overlib('Alt-Z', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input name="btn_SpesialHadiah" type="button" value="Spesial Hadiah" onClick="showPage('spesial_hadiah');" accesskey="P" onmouseover="overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="Call" name="call" onclick="showPage('call');" style="width: 35px;" onmouseover="return overlib('Alt-L', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="Summary" name="summary" onclick="showPage('summary');" style="width: 65px;"	 onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="Auto Proses H+1" name="autoproses" onclick="jalurkhusus('autoproses');" style="width: 120px;"	 onmouseover="return overlib('Auto Proses Untuk H+1', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="View Kesehatan DMTM" name="dmtm" onclick="showPage('viewDMTM');" onmouseover="return overlib('VIEW QUESTIONARE DMTM', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="Questionare" id="questionare" name="questionare" onclick="showPage('questionare');">
				<input type="button" value="Attention List" name="blacklist" onclick="showPage('blacklist');" onmouseover="return overlib('VIEW ATTENTION LIST', AUTOSTATUS, WRAP);" onmouseout="nd();">
			     <input type="button" value="" name="speedy">
			     <input name="btn_approve" type="button" value="Approve(Proses NB)" onClick="showPage('approve')">
				 <input name="baby" type="button" value="Smile Baby" onClick="showPage('baby')">
			</td>
		</tr>
		</c:otherwise>
	</c:choose>
	
	<tr>
		<td colspan="2">
			<table style="margin: 0 0 0 0; padding: 0 0 0 0; width: 100%;">
				<tr>
					<c:choose>
						<c:when test="${sessionScope.currentUser.wideScreen}">
							<td style="width: 60%;">
								<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
									width="100%"  height="100%"> Please Wait... </iframe>
							</td>
							<td style="width: 40%;">
								<iframe src="" name="docFrame" id="docFrame" width="100%">E-Lions</iframe>
							</td>
						</c:when>
						<c:otherwise>
							<td>
								<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
									width="100%"  height="100%"> Please Wait... </iframe>
							</td>
						</c:otherwise>
					</c:choose>
				</tr>
			</table>		
		</td>	
	</tr>
</table>
</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>