<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<script type="text/javascript" src="${path }/include/js/default.js"></script>
<script type="text/javascript">
	function hide(div){
		var state = document.getElementById(div).style.display;
		if (state == 'block') {
			document.getElementById(div).style.display = 'none';
		}else{
			document.getElementById(div).style.display = 'block';
		}
	}
</script>
<BODY style="height: 100%; text-align: center;" onload="loadImages()">

	<fieldset style="text-align: center;">
		<legend>Detail Riwayat Klaim Kesehatan</legend>
				<br/>
				<div style="width: 100%; font-size:9pt; background-color:#df3200; color:white; font-weight: bolder; text-align: center; border-bottom: 1px solid black;border-top: 1px solid black;">NO KLAIM : ${no_klaim}</div>

			<c:choose>
			<c:when test="${kode eq \"1\" }">				
				<c:choose>
					<c:when test="${empty claimPreAccep}">
			
					</c:when>
					<c:otherwise>
						<fieldset>
						<legend>STATUS KLAIM : DALAM PROSES</legend>
				
						<table class="displaytag">
							<thead>
							<tr>
								<th>JENIS KLAIM</th>
								<th>TGL DIRAWAT</th>
								<th>JUMLAH HARI RAWAT INAP</th>
								<th>DIAGNOSA</th>
								<th>DETAIL</th>
								<th>KLAIM YANG DIAJUKAN</th>
								<th>KLAIM YANG DIBAYARKAN</th>
								<th>KETERANGAN</th>
							</tr>
							</thead>
							<tbody>
							<c:forEach items="${claimPreAccep}" var="claim" varStatus="s">
								<tr <c:if test="${s.count%2 eq 1}">class="odd"</c:if>>
										<td>${claim.LSDBS_NAME} </td>
									<td><fmt:formatDate value="${claim.MCE_TGL_1}" pattern="dd/MM/yyyy"/></td>
									<td>${claim.MCE_HARI}</td>
									<td>${claim.LSD_DESC_ALL}</td>
									<td>${claim.LJJ_JENIS_JAMINAN}</td>
									<td><fmt:formatNumber>${claim.MDC_JLH_CLAIM}</fmt:formatNumber></td>
									<td><fmt:formatNumber>${claim.MDC_BAYAR}</fmt:formatNumber></td>
									<td>${claim.LA_DESC}</td>
								</tr>
							
							</c:forEach>
							</tbody>
						</table>
						</fieldset>
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:when test="${kode eq \"2\" }">
				<c:choose>
					<c:when test="${empty claimPU}">
			
					</c:when>
					<c:otherwise>
						<fieldset>
						<legend>STATUS KLAIM : PROSES ULANG</legend>
				
						<table class="displaytag">
							<thead>
							<tr>
								<th>JENIS KLAIM</th>
								<th>TGL DIRAWAT</th>
								<th>JUMLAH HARI RAWAT INAP</th>
								<th>DIAGNOSA</th>
								<th>DETAIL</th>
								<th>KLAIM YANG DIAJUKAN</th>
								<th>KLAIM YANG DIBAYARKAN</th>
								<th>KETERANGAN</th>
							</tr>
							</thead>
							<tbody>
							<c:forEach items="${claimPU}" var="claim" varStatus="s">
								<tr <c:if test="${s.count%2 eq 1}">class="odd"</c:if>>
									<td>${claim.LSDBS_NAME} </td>
									<td><fmt:formatDate value="${claim.MCE_TGL_1}" pattern="dd/MM/yyyy"/></td>
									<td>${claim.MCE_HARI}</td>
									<td>${claim.LSD_DESC_ALL}</td>
									<td>${claim.LJJ_JENIS_JAMINAN}</td>
									<td><fmt:formatNumber>${claim.MDC_JLH_CLAIM}</fmt:formatNumber></td>
									<td><fmt:formatNumber>${claim.MDC_BAYAR}</fmt:formatNumber></td>
									<td>${claim.LA_DESC }</td>
								</tr>
							
							</c:forEach>
							</tbody>
						</table>
						</fieldset>
					</c:otherwise>
				</c:choose>
				</c:when>
				<c:when test="${kode eq \"3\" }">
				<c:choose>
						<c:when test="${empty claimAccept}">
								
						</c:when>
						<c:otherwise>
							<fieldset>
							<legend>STATUS KLAIM : DITERIMA</legend>
				
							<table class="displaytag">
								<thead>
								<tr>
									<th>JENIS KLAIM</th>
									<th>TGL DIRAWAT</th>
									<th>JUMLAH HARI RAWAT INAP</th>
									<th>DIAGNOSA</th>
									<th>DETAIL</th>
									<th>KLAIM YANG DIAJUKAN</th>
									<th>KLAIM YANG DIBAYARKAN</th>
									<th>TGL BAYAR</th>
									<th>KETERANGAN</th>
								</tr>
								</thead>
								<tbody>
								<c:forEach items="${claimAccept}" var="claim" varStatus="s">
									<tr <c:if test="${s.count%2 eq 1}">class="odd"</c:if>>
										<td>${claim.LSDBS_NAME} </td>
										<td><fmt:formatDate value="${claim.MCE_TGL_1}" pattern="dd/MM/yyyy"/></td>
										<td>${claim.MCE_HARI}</td>
										<td>${claim.LSD_DESC_ALL}</td>
										<td>${claim.LJJ_JENIS_JAMINAN}</td>
										<td><fmt:formatNumber>${claim.MDC_JLH_CLAIM}</fmt:formatNumber></td>
										<td><fmt:formatNumber>${claim.MDC_BAYAR}</fmt:formatNumber></td>
										<td><fmt:formatDate value="${claim.MBC_TGL_BACK}" pattern="dd/MM/yyyy"/></td>
										<td>${claim.LA_DESC}</td>
									</tr>
								
								</c:forEach>
								</tbody>
							</table>
							</fieldset>
						</c:otherwise>
					</c:choose>
					</c:when>
					<c:when test="${kode eq \"4\" }">
					
				
					<c:choose>
						<c:when test="${empty claimTolak}">
							
						</c:when>
						<c:otherwise>
							<fieldset>
							<legend>STATUS KLAIM : DITOLAK</legend>
				
							<table class="displaytag">
								<thead>
								<tr>
									<th>JENIS KLAIM</th>
									<th>TGL DIRAWAT</th>
									<th>JUMLAH HARI RAWAT INAP</th>
									<th>DIAGNOSA</th>
									<th>DETAIL</th>
									<th>KLAIM YANG DIAJUKAN</th>
									<th>KLAIM YANG DIBAYARKAN</th>
									<th>KETERANGAN</th>
								</tr>
								</thead>
								<tbody>
								<c:forEach items="${claimTolak}" var="claim" varStatus="s">
									<tr <c:if test="${s.count%2 eq 1}">class="odd"</c:if>>
										<td>${claim.LSDBS_NAME} </td>
										<td><fmt:formatDate value="${claim.MCE_TGL_1}" pattern="dd/MM/yyyy"/></td>
										<td>${claim.MCE_HARI}</td>
										<td>${claim.LSD_DESC_ALL}</td>
										<td>${claim.LJJ_JENIS_JAMINAN}</td>
										<td><fmt:formatNumber>${claim.MDC_JLH_CLAIM}</fmt:formatNumber></td>
										<td><fmt:formatNumber>0</fmt:formatNumber></td>
										<td>${claim.LA_DESC }</td>
									</tr>
								
								</c:forEach>
								</tbody>
							</table>
							</fieldset>
						</c:otherwise>
					</c:choose>
				
			</c:when>
			<c:when test="${kode eq \"5\" }">
					
				
					<c:choose>
						<c:when test="${empty claimIndividu}">
							
						</c:when>
						<c:otherwise>
							<fieldset>
							<legend>DISTRIBUSI : INDIVIDU | STATUS CLAIM : ALL STATUS</legend>
				
							<table class="displaytag">
								<thead>
								<tr>
									<th>NAMA PEMEGANG</th>
									<th>NAMA TERTANGGUNG</th>
									<th>NO POLIS</th>
									<th>NAMA PRODUK</th>									
									<th>JENIS KLAIM</th>
									<th>TGL DIRAWAT</th>
									<th>JUMLAH  HARI RAWAT</th>
									<th>DIAGNOSA</th>
									<th>STATUS KLAIM</th>
									<th>KLAIM YANG DIAJUKAN</th>
									<th>JUMLAH KLAIM YANG DIBAYARKAN</th>
								</tr>
								</thead>
								<tbody>
								<c:forEach items="${claimIndividu}" var="claim" varStatus="s">
									<tr <c:if test="${s.count%2 eq 1}">class="odd"</c:if>>
										<td>${claim.MCL_FIRST}</td>
										<td>${claim.NAMA_TT} </td>
										<td><script type="text/javascript">document.write(formatPolis('${claim.MSPO_POLICY_NO }'));</script></td>
										<td>${claim.LSBS_NAME }</td>										
										<td>${claim.EXPLANATION}</td>
										<td><fmt:formatDate value="${claim.DATE_RI_1}" pattern="dd/MM/yyyy"/></td>
										<td>${claim.AMOUNT_RI}</td>
										<td>${claim.NM_DIAGNOS}</td>
										<td>${claim.ST_EXPL}</td>
										<td><fmt:formatNumber>${claim.AMT_CLAIM}</fmt:formatNumber></td>
										<td><fmt:formatNumber>${claim.PAY_CLAIM}</fmt:formatNumber></td>
									</tr>
								
								</c:forEach>
								</tbody>
							</table>
							</fieldset>
						</c:otherwise>
					</c:choose>
				
			</c:when>
			<c:when test="${kode eq \"6\" }">
					
				
					<c:choose>
						<c:when test="${empty claimDMTM}">
							
						</c:when>
						<c:otherwise>
							<fieldset>
							<legend>DISTRIBUSI : DMTM | STATUS CLAIM : ALL STATUS</legend>
				
							<table class="displaytag">
								<thead>
								<tr>
									<th>NAMA PEMEGANG</th>
									<th>NAMA TERTANGGUNG</th>
									<th>NO POLIS</th>
									<th>NAMA PRODUK</th>
									<th>TGL DIRAWAT</th>
									<th>JUMLAH  HARI RAWAT</th>
									<th>DIAGNOSA</th>
									<th>STATUS KLAIM</th>
									<th>KLAIM YANG DIAJUKAN</th>
									<th>JUMLAH KLAIM YANG DIBAYARKAN</th>
								</tr>
								</thead>
								<tbody>
								<c:forEach items="${claimDMTM}" var="claim" varStatus="s">
									<tr <c:if test="${s.count%2 eq 1}">class="odd"</c:if>>
										<td>${claim.HOLDER_NAME}</td>
										<td>${claim.NAMA_PESERTA} </td>
										<td>${claim.NO_SERTIFIKAT }</td>
										<td>${claim.PRODUCT_NAME }</td>
										<td><fmt:formatDate value="${claim.MCM_TGL_1}" pattern="dd/MM/yyyy"/></td>
										<td>${claim.MCM_HARI}</td>
										<td>${claim.MCM_DIAGNOSA}</td>
										<td>${claim.STATUS_ACCEPT}</td>
										<td><fmt:formatNumber>${claim.MDCM_JLH_CLAIM}</fmt:formatNumber></td>
										<td><fmt:formatNumber>${claim.MDCM_BAYAR}</fmt:formatNumber></td>
									</tr>
								
								</c:forEach>
								</tbody>
							</table>
							</fieldset>
						</c:otherwise>
					</c:choose>
				
			</c:when>
			<c:otherwise>
				Tidak Ada Riwayat klaim kesehatan
			</c:otherwise>
		</c:choose>
			
		
		
	</fieldset>
</body>
<%@ include file="/include/page/footer.jsp"%>