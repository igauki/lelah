<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript">

    if( '' != '${generalMsg}' )
    {
        alert( '${generalMsg}' );
    }

    function popWin(href, height, width,scrollbar,stat)
    {
        var vWin;
        if(scrollbar!='no')scrollbar='yes';
        if(stat!='yes')stat='no';

        vWin = window.open(href,'','height='+height+',width='+width+
                                   ',toolbar=no,directories=no,status=no,menubar=no,scrollbars='+scrollbar+',resizable=yes,modal=yes,status='+stat+
                                   ',left='+((screen.availWidth-width)/2)+
                                   ',top='+((screen.availHeight-height)/2));
        vWin.opener = self;
    }

    function checkForm( id, isChecked )
    {
        document.getElementById( id ).checked = isChecked;
    }

    function checkMedicalCheckupAccordingToJenisMedis( jnsMedisCd, isChecked )
    {
        ${contentScript}
    }

    function buttonLinks( str )
    {
        var spaj = document.forms[ 0 ].spaj.value;
        document.formpost.theEvent.value=str;
        document.getElementById('formpost').submit();

        if( str=='onPressButtonPreviewMedicalPdf' )
        {
			popWin('${path}/common/std_download.htm?theEvent=' + str + 'spaj=${cmd.spaj}', 600, 800);
		}
        else if( str=='onPressButtonSendMedicalEmail' )
        {
        }
    }

</script>
</head>
<BODY onload="resizeCenter(550,400); document.title='PopUp :: Email Medical'; setupPanes('container1', 'tab1');"
      style="height: 100%;">

<div class="tab-container" id="container1">
    <ul class="tabs">
        <li>
            <a href="#" onClick="return showPane('pane1', this)" id="tab1">EMAIL MEDIS</a>
        </li>
    </ul>

    <div class="tab-panes">
        <div id="pane1" class="panes">
            <form:form id="formpost" name="formpost" commandName="cmd" method="post">

                    <table style="width: 100%;">

                        <tr style="height: 10px;">
                            <td colspan="6" style="text-align: left;">
                                <table style="margin-top: 10px;">
                                    <tr>
                                        <td style="width: 140px;">Nomor Spaj</td>
                                        <td style="text-transform: none;">: ${cmd.beautifiedSpaj}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 140px;">Nama Pemegang Polis</td>
                                        <td style="text-transform: none;">: ${cmd.policyHolderName}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 140px;">Nama Tertanggung</td>
                                        <td style="text-transform: none;">: ${cmd.insuredName}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 140px;">Nomor Surat Medis</td>
                                        <td>: <form:input path="letterNo" cssStyle="width: 150px; text-transform: none;" title="001/MED/UND/III/08"/></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 140px;">Awalan Nama PP</td>
                                        <td>: <form:input path="policyHolderPrefix" cssStyle="width: 150px; text-transform: none;" title="( mis: Ibu )"/></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 140px;">Akhiran Nama PP</td>
                                        <td>: <form:input path="policyHolderSuffix" cssStyle="width: 150px; text-transform: none;" title="( mis: MM )"/></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 140px;">Awalan Nama TT</td>
                                        <td>: <form:input path="insuredPrefix" cssStyle="width: 150px; text-transform: none;" title="( mis: Ibu )"/></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 140px;">Akhiran Nama TT</td>
                                        <td>: <form:input path="insuredSuffix" cssStyle="width: 150px; text-transform: none;" title="( mis: MM )"/></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr style="vertical-align: top;">
                            <td style="width: 110px;">
                                <fieldset>
                                    <legend>Jenis Medis</legend>
                                    <table class="entry2">
                                        <c:forEach var="jenisMedicalVO" items="${cmd.jenisMedicalVOList}" varStatus="xt">
                                            <tr>
                                                <td style="text-align: left;">
                                                    <form:radiobutton path="jenisMedisCd" value="${jenisMedicalVO.cd}" id="jenisMedicalVOList[${xt.index}].cd" onclick="checkMedicalCheckupAccordingToJenisMedis( ${jenisMedicalVO.cd}, this.checked );" />
                                                    <label for="jenisMedicalVOList[${xt.index}].cd">
                                                            ${jenisMedicalVO.descr}
                                                    </label>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </table>
                                </fieldset>
                            </td>
                            <td style="width: 10px;">
                                &nbsp;
                            </td>
                            <td style="width: 280px;">
                                <fieldset>
                                    <legend>Jenis Pemeriksaan</legend>
                                    <table class="entry2">
                                        <c:forEach var="medicalCheckupVO" items="${cmd.medicalCheckupVOList}" varStatus="xt">
                                            <tr>
                                                <td style="text-align: left;">
                                                    <form:checkbox path="medicalCheckupVOList[${xt.index}].flag" id="medicalCheckupVOList[${xt.index}].flag" value="true" />
                                                    <label for="medicalCheckupVOList[${xt.index}].flag">
                                                            ${medicalCheckupVO.descr}
                                                    </label>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </table>
                                </fieldset>
                            </td>
                            <td style="width: 10px;">
                                &nbsp;
                            </td>
                            <td style="width: 250px;">
                                <fieldset>
                                    <legend>Atas Pertimbangan</legend>
                                    <table class="entry2">
                                        <c:forEach var="pertimbanganMedicalVO" items="${cmd.pertimbanganMedicalVOList}" varStatus="xt">
                                            <tr>
                                                <td style="text-align: left;">
                                                    <form:checkbox path="pertimbanganMedicalVOList[${xt.index}].flag" id="pertimbanganMedicalVOList[${xt.index}].flag" value="true" />
                                                    <label for="pertimbanganMedicalVOList[${xt.index}].flag">
                                                            ${pertimbanganMedicalVO.descr}
                                                    </label>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </table>
                                </fieldset>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>

                        <tr style="height: 10px;">
                            <td colspan="5" style="text-align: center;">
                                <br/>
                                <input type="button" name="limit" value="Preview Attachment"
                                       onclick="return buttonLinks('onPressButtonPreviewMedicalPdf');"
                                       accesskey="P" onmouseover="return overlib('Alt-P > Preview Attachment', AUTOSTATUS, WRAP);" onmouseout="nd();">
                                <input type="button" name="limit" value="Send Medical Email"
                                       onclick="return buttonLinks('onPressButtonSendMedicalEmail');"
                                       accesskey="S" onmouseover="return overlib('Alt-S > Send Medical Email', AUTOSTATUS, WRAP);" onmouseout="nd();">

                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>

                    <input type="hidden" name="theEvent" id="theEvent"/>
                    <input type="hidden" name="spaj" value="${cmd.spaj}">
                </form:form>
			</div>
		</div>
	</div>



</body>
</html>