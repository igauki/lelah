<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />

<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->

<script type="text/javascript">
$().ready(function() {
	$("#tabs").tabs();
	
	$('#f_list').hide();
		
	$('#btnNew').click(function(){
		window.location.href = '${path}/uw/uw.htm?window=generateNoKartuPAS';
	});
	
	var m_id = '${tgl_input}';
	if(m_id!=null && m_id!=''){
		$('#tgl_input').val(m_id);
		$('#f_list').show();
		$('#f_permintaan').hide();
	}else{
		$('#f_list').hide();
		$('#f_permintaan').show();
	}
	
	$('#btnShow').click(function(){
		if($('#msv_id').val()==null || $('#msv_id').val()==''){
			alert("Harap pilih id permintaan!");
		}else{
			window.location.href = '${path}/uw/uw.htm?window=generateNoKartuPAS&btnShow=1&tgl_input='+$('#tgl_input').val();			
		}
	});
	
	$('#btnExcel').click(function(){
		if($('#tgl_input').val()==null || $('#tgl_input').val()==''){
			alert("Harap pilih id permintaan!");
		}else{
			window.location.href = '${path}/uw/uw.htm?window=generateNoKartuPAS&btnShow=1&btnExcel=1&tgl_input='+$('#tgl_input').val();			
		}
	});
	
	$('#tgl_input').click(function(){
		window.location.href = '${path}/uw/uw.htm?window=generateNoKartuPAS&btnShow=1&tgl_input='+$('#tgl_input').val();			
	});
});
hideLoadingMessage();
</script>  
<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

</style>
<body>
	<div id="tabs">
		<ul>
			<li><a href="#tab-1">Generate Kartu PAS</a></li>					
		</ul>
		<div id="tab-1">
			<form name="formpost" method="post">
				<table class="entry2">
					<tr>
						<th style="width: 120px;" valign="top">
							<input type="button" id="btnShow" name="btnShow" value="Show" />
							<input type="button" id="btnNew" name="btnNew" value="New" /><br><br>
							<select id="tgl_input" name="tgl_input" style="width: 110px;" size="${sessionScope.currentUser.comboBoxSize}">
								<c:forEach items="${id }" var="m" varStatus="s">
									<option value="${m.key }"> ${m.value }</option>
								</c:forEach>
							</select>
						</th>
						<td valign="top">
							<fieldset>
							<c:if test="${not empty pesan }">
							<div id="success">
								${pesan }
							</div>
							</c:if>
							<!-- untuk form permintaan -->
							<div id="f_permintaan">
								<legend>Generate Kartu PAS</legend>
								<table class="entry2">
									<tr>
										<th>Product</th>
										<td>:</td>
										<td>
											<select name="sub_code" id="sub_code">
												<c:forEach items="${prod}" var="p">
													<option value="${p.LSBS_ID}-${p.LSDBS_NUMBER}">${p.LSDBS_NAME }
												</c:forEach>
											</select>
										</td>
									</tr>
									<tr>
										<th>Total Generate</th>
										<td>:</td>
										<td><input type="text" name="totalGenerate" id="totalGenerate"></td>
									</tr>
									<tr>
										<th>Premi</th>
										<td>:</td>
										<td><input type="text" name="premi" id="premi"></td>
									</tr>
									<tr>
										<th>UP</th>
										<td>:</td>
										<td><input type="text" name="up" id="up"></td>
									</tr>
									<tr>
										<th>Generate ID Number</th>
										<td>:</td>
										<td><input type="text" name="generateIdNumber" id="generateIdNumber" value="0544"></td>
									</tr>
									<tr>
										<td rowspan="3">
											<input type="submit" name="submit" id="submit" value="Simpan">
										</td>
									</tr>
								</table>
							</div>
							<!-- untuk view list kartu PAS -->
							<div id="f_list" style="width:100%">
								<legend>List Kartu PAS</legend>
								<input type="button" id="btnExcel" name="btnExcel" value="Export to Excel" />
								<table class="entry2" width="100%">
									<tr>
										<th>No</th>
										<th>No Kartu</th>
										<th>Flag</th>
										<th>Tanggal Input</th>
										<th>Premi</th>
										<th>UP</th>
										<th>Product</th>
										<th>No VA</th>
										<th>User Input</th>
									</tr>
									<c:if test="${not empty kartu }">
									<pg:paging url="${path}/uw/uw.htm?window=generateNoKartuPAS&btnShow=1&tgl_input=${tgl_input }" pageSize="500">
									<c:forEach items="${kartu }" var="l" varStatus="s">
										<pg:item>
										<tr>
											<td>${s.index +1 }</td>
											<td>${l.NO_KARTU }</td>
											<td>${l.FLAG }</td>
											<td>${l.TGL_INPUT }</td>
											<td>${l.PREMI }</td>
											<td>${l.UP }</td>
											<td>${l.PRODUCT }</td>
											<td>${l.NO_VA }</td>
											<td>${l.NAMA }</td>
										</tr>
										</pg:item>
									</c:forEach>
										<pg:index>
											  	<pg:page><%=thisPage%></pg:page> 
										</pg:index>
									</pg:paging>
									</c:if>
								</table>
							</div>
							</fieldset>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>

<div class="tabcontent">

	
</table>
</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>