<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		$("#tabs").tabs();
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		$("#dp").change(function() {
			var z=$("#dp").val();
			var x=$("#spaj").val();		
					
			if(z!=10){	
					
				var url = "${path}/uw/uw.htm?window=view_kesehatan&json=1&spaj="+x+"&fjp="+z;
				 			
				 $.getJSON(url, function(result) {
				  $.each(result, function() {
						 $("#bb").val(this.BB);
						 $("#tb").val(this.TB);
						 $("#sehat").val(this.SEHAT);
						 $("#sehat1").val(this.SAKIT);
						 $("#medis").val(this.MEDIS);
						 $("#medis1").val(this.MEDIS1);
						 $("#hamil").val(this.HAMIL);
						 $("#hamil1").val(this.HAMIL1);
						 $("#clear").val(this.CLEAR);
						  $("#medis_alt").val(this.MEDIS_ALT);
						 $("#em").val(this.EM);
						 $("#bb_berubah").val(this.BB_BERUBAH);
						 $("#penyakit").val(this.PENYAKIT);
						 $("#bb_berubah_desc").val(this.BB_BERUBAH_DESC);
						  $("#penyakit_desc").val(this.PENYAKIT_DESC);
						  $("#medis_alt_desc").val(this.MEDIS_ALT_DESC);
					 
				  });	
				});	
				
			}			
	
		});
		
		
	});
	
	
	
</script>


<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

</style>

<body>
		<div id="tabs">		
				<ul>
					<li><a href="#tab-1">View Kesehatan</a></li>					
					
				</ul>
		<div id="tab-1">			
		  <form id="formPost" name="formPost" method="post"   target="">
		      <fieldset class="ui-widget ui-widget-content">
			  <legend class="ui-widget-header ui-corner-all"><div>Questionare Kesehatan</div></legend>
			  <div class="rowElem" id="dropdown2"> 
					<label>Daftar Peserta:</label> 
					<select  name="dp" id="dp" title="Silahkan pilih jenis peserta">												
						<c:forEach var="c" items="${daftarPeserta}" varStatus="s">
							<option value="${c.key}">${c.value}</option>
				        </c:forEach> 
				    </select> 									
			</div>	
			<div class="rowElem">
		             <label>No. SPAJ</label>
     				<input type="text" size="12" value="${spaj}" id="spaj" name="spaj" style="background: #D4D4D4;" readonly="readonly">
			</div>																											
	        <div class="rowElem">		
				    <table  border="1px" >
					      	 <tr align="left"> 
					      	 	<th rowspan="2">1.</th>
					      	 	<td>
					      	 		a.Berat Badan/Kg
					      	 	</td>
					      	 	<td>
					      	 		<input type="text" size="3" id="bb" name="bb">/kg
					      	 	</td>
					      	 </tr>
					      	 <tr align="left"> 				      	 	
					      	 	<td>
					      	 		b.Tinggi Badan/Cm
					      	 	</td>
					      	 	<td>
					      	 		<input type="text" size="3" id="tb" name="tb">/cm
					      	 	</td>
					      	 </tr>
					      	 
					      	  <tr align="left"> 
					      	 	<th rowspan="2">2.</th>
					      	 	<td>
					      	 		Apakah berat badan Anda berubah dalam 12 bulan terakhir? jika "YA", jelaskan berapa kg penurunan/kenaikannya dan jelaskan penyebabnya !
					      	 	</td>
					      	 	<td>
					      	 		<input type="text" size="7" id="bb_berubah" name="bb_berubah" >
					      	 	</td>
					      	 </tr>
					      	  <tr align="left"> 				      	 	
					      	 	<td>Penjelasan jika "YA"</td>
					      	 	<td>
					      	 		<textarea rows="3" cols="50" id="bb_berubah_desc" name="bb_berubah_desc"></textarea>
					      	 	</td>
					      	 </tr>
					      	 
					      	 <tr align="left"> 
					      	 	<th rowspan="2">3.</th>
					      	 	<td>
					      	 		Apakah Anda sedang atau pernah menderita, atau pernah diberitahu atau dalam konsultasi/perawatan/pengobatan/pengawasan medis sehubungan dengan salah satu atau beberapa penyakit/gangguan/gejala lainnya? jika "YA", jelaskan : nama penyakit, kapan, obat yang diberikan, fotokopi hasil pemeriksaan laboratorium, nama dan alamat dokter yang merawat!
					      	 	</td>
					      	 	<td>
					      	 		<input type="text" size="7" id="penyakit" name="penyakit" >
					      	 	</td>
					      	 </tr>
					      	  <tr align="left"> 				      	 	
					      	 	<td>Penjelasan jika "YA"</td>
					      	 	<td>
					      	 		<textarea rows="3" cols="50" id="penyakit_desc" name="penyakit_desc"></textarea>
					      	 	</td>
					      	 </tr>
					      	 
					      	 
					      	 <tr align="left"> 
					      	 	<th rowspan="2">4</th>
					      	 	<td>
					      	 		a. Apakah Anda sedang atau pernah menjalani konsultasi / rawat inap / operasi / biopsi / pemeriksaan laboratorium / rontgen / EKG / Treadmill / Echocardiography / USG / CT Scan / MRI atau pemeriksaan lainnya? Jika "YA", jelaskan : pemeriksaan apa, kapan dilakukan, alasan dilakukan pemeriksaan tersebut, haslnya(lampirkan fotokopi hasil pemeriksaan)!
					      	 	</td>
					      	 	<td>
					      	 		<input type="text" size="7" id="sehat" name="sehat" >
					      	 	</td>
					      	 </tr>
					      	  <tr align="left"> 				      	 	
					      	 	<td>Penjelasan jika "YA"</td>
					      	 	<td>
					      	 		<textarea rows="3" cols="50" id="sehat1" name="sehat1"></textarea>
					      	 	</td>
					      	 </tr>
					      	 
					      	  	 <tr align="left"> 
					      	 	<th rowspan="2">4</th>
					      	 	<td>
					      	 		b. Apakah Anda sedang atau pernah menjalani pengobotan ahli jiwa / radiasi / kemoterapi / pengobatan tradisional / pengobatan alternatif, menerima transfusi darah atau ditolak untuk menjadi donor darah? Jika "YA", jelaskan : kapan, berapa lama, dimana, pengobatan yang diberikan, alasan diberikan pengobatan!
					      	 	</td>
					      	 	<td>
					      	 		<input type="text" size="7" id="medis_alt" name="medis_alt" >
					      	 	</td>
					      	 </tr>
					      	  <tr align="left"> 				      	 	
					      	 	<td>Penjelasan jika "YA"</td>
					      	 	<td>
					      	 		<textarea rows="3" cols="50" id="medis_alt_desc" name="medis_alt_desc"></textarea>
					      	 	</td>
					      	 </tr>
					      	 
					      	 <tr align="left"> 
					      	 	<th rowspan="2">5.</th>
					      	 	<td>
					      	 		Sudah Pernah Medical Check up?Jika "YA" mohon dijelaskan
					      	 	</td>
					      	 	<td>
					      	 		<input type="text" size="7" id="medis" name="medis">
					      	 	</td>
					      	 </tr>
					      	  <tr align="left"> 				      	 	
					      	 	<td>Penjelasan jika "YA"</td>
					      	 	<td>
					      	 		<textarea rows="3" cols="50" id="medis1" name="medis1"></textarea>
					      	 	</td>
					      	 </tr>
					      	 
					      	  <tr align="left"> 
					      	 	<th rowspan="2">6.</th>
					      	 	<td>
					      	 		Sudah Pernah Medical Check up?Jika "YA" mohon dijelaskan
					      	 	</td>
					      	 	<td>
					      	 		<input type="text" size="7" id="medis" name="medis">
					      	 	</td>
					      	 </tr>
					      	  <tr align="left"> 				      	 	
					      	 	<td>Penjelasan jika "YA"</td>
					      	 	<td>
					      	 		<textarea rows="3" cols="50" id="medis1" name="medis1"></textarea>
					      	 	</td>
					      	 </tr>
					      	 
					      	 <tr align="left"> 
					      	 	<th rowspan="2">4.</th>
					      	 	<td>
					      	 		Khusus Untuk Wanita "Apakah Anda sedang Hamil ? Jika "YA", berapa minggu usia kandungan Anda?"
					      	 	</td>
					      	 	<td>
					      	 		<input type="text" size="7" id="hamil" name="hamil" >
					      	 	</td>
					      	 </tr>
					      	  <tr align="left"> 				      	 	
					      	 	<td>Penjelasan jika "YA"</td>
					      	 	<td>
					      	 		<textarea rows="3" cols="50" id="hamil1" name="hamil1"></textarea>
					      	 	</td>
					      	 </tr>	
					      	  <tr align="left"> 
					      	 	<th >5.</th>
					      	 	<td>
					      	 		Clear Case
					      	 	</td>
					      	 	<td>
					      	 		<input type="text" size="7" id="clear" name="clear" >
					      	 	</td>
					      	 </tr>	
					      	 
					      	 <tr align="left"> 
					      	 	<th rowspan="2">6.</th>
					      	 	<td>
					      	 		EM
					      	 	</td>
					      	 	<td>
					      	 		<input type="text" size="7" id="em" name="em" >
					      	 	</td>
					      	 </tr>			      		      
				      </table>
				   </div>   
			  </fieldset>					
		   </form>	
						
	     </div>	
	</div>
			
			

			
</body>
</html>


