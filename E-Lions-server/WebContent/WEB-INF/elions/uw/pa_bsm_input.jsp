<%@ include file="/include/page/header_mall.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<%User currentUser = (User) session.getAttribute("currentUser");%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>
<script type="text/javascript" src="${path }/include/js/ajax.js"></script>

 
<script type="text/javascript">
	hideLoadingMessage();
	
	function agen_cek()
	{
		if (document.getElementById('msag_id') != null && document.getElementById('msag_id').value != "")
		{
			ajaxManager.listagenpas(document.getElementById('msag_id').value,
			{callback:function(map) {
		
				DWRUtil.useLoadingMessage();
				document.getElementById('nama_agen').value = map.nama_penutup;
				document.getElementById('cabang_agen').value = map.nama_regional;
				
			   },
			  timeout:180000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
			});
		}
	}
	
	function insert(){
		document.getElementById('kata').value = 'insert';
		if(trim(document.getElementById('kata').value)=='') return false;
		else {
	        // Copy reff_id jika reff_id nya kosong
	        var form = document.getElementById('frmParam');
	        var reff_id = form.reff_id.value;
	        if(reff_id == null || reff_id == '') {
	            var lrb_id = form.lrb_id.value;
	            var nama_agent = form.nama_agent.value;
	            copy_reff(nama_agent, lrb_id);
	            form.reff_id.value = lrb_id;
	        }
		    createLoadingMessage();	
	    }
	}
	
	function cekElseEnvir(){
		if(document.getElementById('msp_fire_insured_addr_envir').value == '5'){
			document.getElementById('msp_fire_ins_addr_envir_else').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_ins_addr_envir_else').style.visibility = "hidden";
		}
	}
	
	function cekElseSourceFund(){
		if(document.getElementById('msp_fire_source_fund2').value == 'LAINNYA'){
			document.getElementById('msp_fire_source_fund').style.visibility = "visible";
			document.getElementById('msp_fire_source_fund').value='';
		}else{
			document.getElementById('msp_fire_source_fund').style.visibility = "hidden";
		}
	}
	
	function cekElseBusiness(){
		if(document.getElementById('msp_fire_type_business2').value == 'LAINNYA'){
			document.getElementById('msp_fire_type_business').style.visibility = "visible";
			document.getElementById('msp_fire_type_business').value='';
		}else{
			document.getElementById('msp_fire_type_business').style.visibility = "hidden";
		}
	}
	
	function cekElseOccupation(){
		if(document.getElementById('msp_fire_occupation2').value == 'LAIN-LAIN'){
			document.getElementById('msp_fire_occupation').style.visibility = "visible";
			document.getElementById('msp_fire_occupation').value='';
		}else{
			document.getElementById('msp_fire_occupation').style.visibility = "hidden";
		}
	}
	
	function cekElseTrio(){
		if(document.getElementById('msp_fire_source_fund2').value == 'LAINNYA'){
			document.getElementById('msp_fire_source_fund').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_source_fund').style.visibility = "hidden";
		}
		if(document.getElementById('msp_fire_type_business2').value == 'LAINNYA'){
			document.getElementById('msp_fire_type_business').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_type_business').style.visibility = "hidden";
		}
		if(document.getElementById('msp_fire_occupation2').value == 'LAIN-LAIN'){
			document.getElementById('msp_fire_occupation').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_occupation').style.visibility = "hidden";
		}
	}
	
	function cekElseOkupasi(){
		if(document.getElementById('msp_fire_okupasi').value == 'L'){
			document.getElementById('msp_fire_okupasi_else').style.visibility = "visible";
		}else{
			document.getElementById('msp_fire_okupasi_else').style.visibility = "hidden";
		}
	}
	
	function dofo(index, flag) {
        var panjang_no_rek = '${cmd.panjang_no_rek}';
		if(flag==1){
			var a="rekening["+(index+1)+"]";
			var b="rekening["+(index)+"]";	
			if(index + 1 < panjang_no_rek) {
			    document.getElementById(a).focus();
		    }
		}else{	
			var a="rekening["+(index+1)+"]";
			if(index + 1 < panjang_no_rek) {
			    document.getElementById(a).focus();
		    }
		}
	}
	
	function dofo_autodebet(index, flag) {
        var panjang_no_rek = '${cmd.panjang_no_rek}';
		if(flag==1){
			var a="rekening_autodebet["+(index+1)+"]";
			var b="rekening_autodebet["+(index)+"]";
			if(index + 1 < panjang_no_rek) {
			    document.getElementById(a).focus();
			}
		}else{
			var a="rekening_autodebet["+(index+1)+"]";		
			if(index + 1 < panjang_no_rek) {
			    document.getElementById(a).focus();
			}
		}
	}
	
	function doRek() {
		document.getElementById('msp_no_rekening').value = '';
		for(i = 0 ; i < 21 ; i++){
			document.getElementById('msp_no_rekening').value = document.getElementById('msp_no_rekening').value + document.getElementById("rekening["+i+"]").value;
		}
	}
	
	function doRek_autodebet() {
		document.getElementById('msp_no_rekening_autodebet').value = '';
		for(i = 0 ; i < 21 ; i++){
			document.getElementById('msp_no_rekening_autodebet').value = document.getElementById('msp_no_rekening_autodebet').value + document.getElementById("rekening_autodebet["+i+"]").value;
		}
	}
	
	function doPp() {
		if(document.getElementById('input_type').value != 'typeafree'){
			if(document.getElementById('msp_rek_nama') && document.getElementById('msp_rek_nama').value == ''){
				document.getElementById('msp_rek_nama').value = document.getElementById('nama_pp').value;
			}
			if(document.getElementById('msp_rek_nama_autodebet')) {
			    if(document.getElementById('msp_rek_nama_autodebet').value == '')
			        document.getElementById('msp_rek_nama_autodebet').value = document.getElementById('nama_pp').value;
			}
		}
		document.getElementById('msp_pas_nama_pp').value = document.getElementById('nama_pp').value;
		
	}
	
	function inPp() {
		document.getElementById('nama_pp').value = document.getElementById('msp_pas_nama_pp').value;
	}
	
	function inRek() {
		var rek = document.getElementById('msp_no_rekening').value
		var size = rek.length;
		for(i = 0 ; i < size ; i++){
			document.getElementById('rekening['+i+']').value = rek.charAt(i);
		}
	}
	
	function inRek_autodebet() {
		var rek = document.getElementById('msp_no_rekening_autodebet').value
		var size = rek.length;
		for(i = 0 ; i < size ; i++){
			document.getElementById('rekening_autodebet['+i+']').value = rek.charAt(i);
		}
	}
	
	function doEnableDisableRek() {
		if(document.getElementById('msp_flag_cc').value == '2' || document.getElementById('msp_flag_cc').value == '1'){//tabungan/kartu kredit
			//autodebet
			/*
			document.getElementById('caribank2').value = '';document.getElementById('caribank2').disabled = '';
			document.getElementById('btncari2').disabled = '';
			document.getElementById('lsbp_id_autodebet').value = '';document.getElementById('lsbp_id_autodebet').disabled = '';
			document.getElementById('msp_rek_nama_autodebet').value = '';document.getElementById('msp_rek_nama_autodebet').disabled = '';
			document.getElementById('msp_tgl_debet_div').disabled = '';
			document.getElementById('msp_tgl_debet_div').style.visibility = 'visible';
			if(document.getElementById('msp_flag_cc').value == '1'){
				document.getElementById('msp_tgl_valid_div').disabled = '';
				document.getElementById('msp_tgl_valid_div').style.visibility = 'visible';
			}else{
				document.getElementById('msp_tgl_valid_div').disabled = 'disabled';
				document.getElementById('msp_tgl_valid_div').style.visibility = 'hidden';
			}
			for(i = 0 ; i < 21 ; i++){
				document.getElementById('rekening_autodebet['+i+']').value = '';
				document.getElementById('rekening_autodebet['+i+']').disabled = '';
			}
			document.getElementById('msp_no_rekening_autodebet').value = '';
			*/
		}else{
			//rekening
			document.getElementById('caribank2').value = '';document.getElementById('caribank2').disabled = 'disabled';
			document.getElementById('btncari2').disabled = 'disabled';
			document.getElementById('lsbp_id_autodebet').value = '';document.getElementById('lsbp_id_autodebet').disabled = 'disabled';
			document.getElementById('msp_rek_nama_autodebet').value = '';document.getElementById('msp_rek_nama_autodebet').disabled = 'disabled';
			document.getElementById('msp_tgl_debet_div').disabled = 'disabled';
			document.getElementById('msp_tgl_debet_div').style.visibility = 'hidden';
			document.getElementById('msp_tgl_valid_div').disabled = 'disabled';
			document.getElementById('msp_tgl_valid_div').style.visibility = 'hidden';
			for(i = 0 ; i < 21 ; i++){
				document.getElementById('rekening_autodebet['+i+']').value = '';
				document.getElementById('rekening_autodebet['+i+']').disabled = 'disabled';
			}
			document.getElementById('msp_no_rekening_autodebet').value = '';
		}
	}
	
	function changeInputAsuransi(){
		var flag = document.getElementById('tambah_input_asuransi').value;
		// kalau pilih tidak, semua inputan fire dikosongin dan didisable otomatis
		if(flag == '0'){
			document.getElementById('msp_fire_code_name').disabled = 'disabled';document.getElementById('msp_fire_phone_number').disabled = 'disabled';
			document.getElementById('msp_fire_name').disabled = 'disabled';document.getElementById('msp_fire_mobile').disabled = 'disabled';
			document.getElementById('msp_fire_identity').disabled = 'disabled';document.getElementById('msp_fire_email').disabled = 'disabled';
			document.getElementById('msp_fire_insured_addr_code').disabled = 'disabled';
			document.getElementById('msp_fire_occupation2').disabled = 'disabled';document.getElementById('msp_fire_insured_addr').disabled = 'disabled';
			document.getElementById('msp_fire_occupation').disabled = 'disabled';document.getElementById('msp_fire_insured_addr_no').disabled = 'disabled';
			document.getElementById('msp_fire_type_business2').disabled = 'disabled';document.getElementById('msp_fire_insured_postal_code').disabled = 'disabled';
			document.getElementById('msp_fire_type_business').disabled = 'disabled';document.getElementById('msp_fire_insured_city').disabled = 'disabled';
			document.getElementById('msp_fire_source_fund2').disabled = 'disabled';document.getElementById('msp_fire_insured_phone_number').disabled = 'disabled';
			document.getElementById('msp_fire_source_fund').disabled = 'disabled';document.getElementById('msp_fire_insured_addr_envir').disabled = 'disabled';
			document.getElementById('msp_fire_addr_code').disabled = 'disabled';document.getElementById('msp_fire_ins_addr_envir_else').disabled = 'disabled';
			document.getElementById('msp_fire_address_1').disabled = 'disabled';document.getElementById('msp_fire_okupasi').disabled = 'disabled';
			document.getElementById('msp_fire_postal_code').disabled = 'disabled';document.getElementById('msp_fire_okupasi_else').disabled = 'disabled';
		}else{// kalau pilih ya, semua inputan fire dibuka otomatis
			document.getElementById('msp_fire_code_name').disabled = '';document.getElementById('msp_fire_phone_number').disabled = '';
			document.getElementById('msp_fire_name').disabled = '';document.getElementById('msp_fire_mobile').disabled = '';
			document.getElementById('msp_fire_identity').disabled = '';document.getElementById('msp_fire_email').disabled = '';
			document.getElementById('msp_fire_insured_addr_code').disabled = '';
			document.getElementById('msp_fire_occupation2').disabled = '';document.getElementById('msp_fire_insured_addr').disabled = '';
			document.getElementById('msp_fire_occupation').disabled = '';document.getElementById('msp_fire_insured_addr_no').disabled = '';
			document.getElementById('msp_fire_type_business2').disabled = '';document.getElementById('msp_fire_insured_postal_code').disabled = '';
			document.getElementById('msp_fire_type_business').disabled = '';document.getElementById('msp_fire_insured_city').disabled = '';
			document.getElementById('msp_fire_source_fund2').disabled = '';document.getElementById('msp_fire_insured_phone_number').disabled = '';
			document.getElementById('msp_fire_source_fund').disabled = '';document.getElementById('msp_fire_insured_addr_envir').disabled = '';
			document.getElementById('msp_fire_addr_code').disabled = '';document.getElementById('msp_fire_ins_addr_envir_else').disabled = '';
			document.getElementById('msp_fire_address_1').disabled = '';document.getElementById('msp_fire_okupasi').disabled = '';
			document.getElementById('msp_fire_postal_code').disabled = '';document.getElementById('msp_fire_okupasi_else').disabled = '';
		}
	}
	
	function getPremi(){
		var input_type = document.getElementById("input_type").value;
		var premi = 0;
		
		if(input_type == 'typeafree'){
			//free
			var premi = 0;
		} else {
			var rate = 0.00;
			var mill = 1000.00;
			var produk = document.getElementById("produk").value;
			var up = document.getElementById("msp_up").value;
			if(produk == 1 || produk == 12){
				rate = 0.65;
			}else if(produk == 2 || produk == 13){
				rate = 1.45;
			}else if(produk == 3 || produk == 14){
				//rate = 3.05;
				rate = 3.00;
			}
			var premi = (up * rate) / mill;
		}
		
		document.getElementById("premi").value = premi;
	}
	
	function bodyOnLoad(){
		if(document.getElementById('input_type').value != 'typeafree'){
			//inRek();
			inRek_autodebet();
			//doEnableDisableRek();
			agen_cek();
		}
		inPp();
		getPremi();
		document.getElementById('caribank2').value='BANK SINARMAS';
		if(document.getElementById('lsbp_id_autodebet').value == ''){
			document.getElementById('btncari2').click();
		}
		var jn_bank = "${sessionScope.currentUser.jn_bank}";
		if(jn_bank == 2){
			document.getElementById('caribank2').disabled='disabled';
		}
		//document.getElementById('btncari2').disabled='disabled';
		//document.getElementById('btncari1').disabled='disabled';
		
		var panjang_no_rek = '${cmd.panjang_no_rek}';
		for(var i = 0 ; i < 21 ; i++){
			if(i > panjang_no_rek-1){
				//document.getElementById("rekening["+i+"]").readOnly=true;
				document.getElementById("rekening_autodebet["+i+"]").readOnly=true;
				//document.getElementById("rekening["+i+"]").style.visibility="hidden";
				document.getElementById("rekening_autodebet["+i+"]").style.visibility="hidden";
			}
		}
	}
	
	function cekKartu() {
       var form = document.getElementById('frmParam');
	   var no_kartu = document.getElementById('no_kartu').value;
	   if(no_kartu.length > 15) {
           createLoadingMessage();
	       form.action = '${path}/uw/pa_bsm_input.htm?input_type=typeabd';
	       document.getElementById('cek_kartu').value = true;
	       form.submit();
	   } else {
	       form.action = '${path}/uw/pa_bsm_input.htm?input_type=typeafree';
	   }
	}
	
	function cekKartuOnBlur() {
	   var form = document.getElementById('frmParam');
	   var no_kartu = document.getElementById('no_kartu').value;
	   if(no_kartu.length == 1 && no_kartu == 0) {
	       createLoadingMessage();
	       form.action = '${path}/uw/pa_bsm_input.htm?input_type=typeafree';
           document.getElementById('cek_kartu').value = true;
           form.submit();	       
	   } else if(no_kartu.length < 16) {
	       alert('Masukkan no kartu dengan lengkap');
	   }
	}
	
	function cari_reff(nilai) {
	   if(nilai==1) { //cari agen penutup
	       ajaxSelectWithParamReffBiiShinta(document.forms['frmParam'].carinama.value,2,false,'select_reff_shinta','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'copy_reff(this.options[this.selectedIndex].text, this.options[this.selectedIndex].value);');
	   } else if(nilai==2) { //cari referral
	       ajaxSelectWithParamReffBiiShinta(document.forms['frmParam'].carinama2.value,2,false,'select_reff_shinta2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'set_nama_reff(this.options[this.selectedIndex].text);');
	   } else if(nilai==3){ //cari agen penutup
	       ajaxSelectWithParamReffBiiShinta(document.forms['frmParam'].carikode.value,2,false,'select_reff_shintabyagentcode','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'copy_reff(this.options[this.selectedIndex].text, this.options[this.selectedIndex].value);');
	   } else if(nilai==4){ //cari referral
	       ajaxSelectWithParamReffBiiShinta(document.forms['frmParam'].carikode2.value,2,false,'select_reff_shintabyagentcode2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'set_nama_reff(this.options[this.selectedIndex].text);');
	   }
	}
	
	function copy_reff(label, value) {
	   document.forms['frmParam'].nama_agent.value = label;
	   var text = '<select name="reff_id">' +
	                   '<option value="' + value + '">' + label + '</option>' +
	               '</select>';
	   var reff_id = document.forms['frmParam'].reff_id.options;
       if(document.getElementById('nama2') && reff_id.length == 1 && reff_id[0].value == '') {
           document.getElementById('nama2').innerHTML = text;
           document.forms['frmParam'].nama_reff.value = label;
       }
	}
	
	function set_nama_reff(nama) {
	   document.forms['frmParam'].nama_reff.value = nama;
	}
	
</script>

</head>

<body onload="resizeCenter(750,600);bodyOnLoad();" style="height: 100%;">
<XML ID=xmlData></XML>
<div class="tab-container" id="container1">

		<div class="tab-panes">

			<div id="pane1" class="panes">
<form:form commandName="cmd" name="frmParam" id="frmParam" method="post">
<fieldset>
					   <legend>ACTION</legend>
					   <table class="result_table2">
						<tr>
							<td colspan="2">
								<input type="hidden" name="msp_id" id="msp_id" value="${msp_id}" />
								<input type="hidden" name="input_type" id="input_type" value="${cmd.input_type}" />
								<input type="button" name="edit" id="edit" value="Edit" style="visibility: hidden;width: 1px" onclick="return update();"/>
								<input type="submit" name="save" id="save" value="Save"  onclick="return insert();"/>
								<input type="button" name="close" id="close" value="Close" onclick="window.close();" />
								<a href="input_fire.htm" id="refreshPage"></a>
							</td>
						</tr>
					</table>
					</fieldset>
						<fieldset>
					  <legend>Produk</legend>
					   <table class="result_table2" >
					   <tr><td colspan="2" style="border: 0px;">
					   <spring:bind path="cmd.*">
			<c:if test="${not empty status.errorMessages}">
				<script>alert('Ada kesalahan pengisian !');</script>
				<div id="error">ERROR:<br>
				<c:forEach var="error" items="${status.errorMessages}">
	- <c:out value="${error}" escapeXml="false" />
					<br />
				</c:forEach></div>
			</c:if>
		</spring:bind>	</td></tr>
						<tr>
							<th colspan="2">Data Pemegang Polis</th>
						</tr>
						<c:if test="${sessionScope.currentUser.jn_bank eq '2' or sessionScope.currentUser.lde_id eq '01'}" >
						<tr>
						  <td align="left">No. Kartu:</td>
						  <td>
						      <form:input path="no_kartu" cssErrorClass="inpError" onkeyup="cekKartu();" onblur="cekKartuOnBlur();"/><span class="error">*</span> tanpa titik (.)
						      <input type="hidden" id="cek_kartu" name="cek_kartu"/>
						  </td>
						</tr>
						</c:if>
						<tr>
							<td align="left">Nama (sesuai KTP):</td>
							<td><input type="text" id="nama_pp" name="nama_pp" id="nama_pp" onblur="doPp();" /><font class="error">*</font>
							<span style="visibility: hidden"><form:input path="msp_pas_nama_pp" cssErrorClass="inpError"/></span></td>
						</tr>
						<tr>
							<td align="left">Tempat, Tgl. lahir:</td>
							<td><form:input path="msp_pas_tmp_lhr_pp" cssErrorClass="inpError"/>
								<spring:bind path="cmd.msp_pas_dob_pp" >
        		                    <script>
                                        var isDisabled;
                                        var status = '${statusForm}';
                                        if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
                                <font color="#CC3300">*</font>
                                </spring:bind>
							</td>
						</tr>
						<tr>
							<td align="left">Jenis Kelamin:</td>
							<td>
							<spring:bind path="cmd.msp_sex_pp">
								<label for="cowok"> <input type="radio" class=noBorder
									name="${status.expression}" value="1"
									<c:if test="${cmd.msp_sex_pp eq 1 or cmd.msp_sex_pp eq null}"> 
												checked</c:if>
									id="cowok">Pria </label>
								<label for="cewek"> <input type="radio" class=noBorder
									name="${status.expression}" value="0"
									<c:if test="${cmd.msp_sex_pp eq 0}"> 
												checked</c:if>
									id="cewek">Wanita </label>
							</spring:bind>
							<font class="error">*</font>
							</td>
						</tr>
						<tr>
							<td align="left">No Identitas:</td>
							<td><form:input path="msp_identity_no" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">Alamat:</td>
							<td><form:input path="msp_address_1" size="80" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">Kota:</td>
							<td><form:input path="msp_city" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">Kode Pos:</td>
							<td><form:input path="msp_postal_code" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">Telp:</td>
							<td><form:input path="msp_pas_phone_number" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">No. HP:</td>
							<td><form:input path="msp_mobile" cssErrorClass="inpError" /><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">No. HP2:</td>
							<td><form:input path="msp_mobile2" cssErrorClass="inpError" /><font class="error"></font></td>
						</tr>
						<tr>
							<td align="left">Alamat Email:</td>
							<td><form:input path="msp_pas_email" cssErrorClass="inpError"/><font class="error"></font></td>
						</tr>
						<tr>
							<td align="left">Hubungan dengan Tertanggung:</td>
							<td>
								<select name="lsre_id" id="lsre_id" onchange="cekElseEnvir();">
									<c:forEach var="rp" items="${relasi_pas}"> 
										<option <c:if test="${cmd.lsre_id eq rp.ID}"> SELECTED </c:if> value="${rp.ID}">${rp.RELATION}
										</option>
									</c:forEach> 
								</select>
							<font class="error">*</font></td>
						</tr>
						<c:if test="${cmd.input_type ne 'typeafree' && cmd.product_code ne '73' && cmd.input_type ne 'default'}">
						<tr>
							<th colspan="2">Data Tertanggung</th>
						</tr>
						<tr>
							<td align="left">Nama Tertanggung:</td>
							<td><form:input path="msp_full_name" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">Tempat, Tgl. lahir:</td>
							<td><form:input path="msp_pas_tmp_lhr_tt" cssErrorClass="inpError"/>
								<spring:bind path="cmd.msp_date_of_birth" >
        		                    <script>
                                        var isDisabled;
                                        var status = '${statusForm}';
                                        if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
                                <font color="#CC3300">*</font>
                                </spring:bind>
							</td>
						</tr>
						<tr>
							<td align="left">No Identitas:</td>
							<td><form:input path="msp_identity_no_tt" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						</c:if>
						<tr>
							<th colspan="2">Data Produk</th>
						</tr>
						<c:if test="${cmd.product_code eq '73'}">
						<tr>
						    <td align="left">No. Virtual Account</td>
						    <td>${no_va}</td>
						</tr>
						</c:if>
						<tr>
							<td align="left">Paket:</td>
							<td>
								<select name="produk" id="produk" onchange="getPremi();">
									<c:forEach var="paket_p" items="${produk_pa_bsm}"> 
										<option <c:if test="${cmd.produk eq paket_p.ID}"> SELECTED </c:if> value="${paket_p.ID}">${paket_p.PRODUK}
										</option>
									</c:forEach> 
								</select>
							</td>
						</tr>
						<tr>
							<td align="left">UP:</td>
							<td>
								<select name="msp_up" id="msp_up" onchange="getPremi();">
									<c:forEach var="pub" items="${produk_pa_up_bsm}"> 
										<option <c:if test="${cmd.msp_up eq pub.VALUE}"> SELECTED </c:if> value="${pub.VALUE}">${pub.LABEL}
										</option>
									</c:forEach> 
								</select>
							<font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">jumlah premi:</td>
							<td>
								<input type="text" name="premi" id="premi" disabled="disabled"/>
							</td>
						</tr>
						<tr>
							<td align="left">cara pembayaran:</td>
							<td>
								<c:if test="${cmd.input_type ne 'typeafree'}">
									<select name="msp_flag_cc" id="msp_flag_cc" onchange="doEnableDisableRek();">
										<c:forEach var="ap" items="${autodebet_pas}"> 
											<option <c:if test="${cmd.msp_flag_cc eq ap.ID}"> SELECTED </c:if> value="${ap.ID}">${ap.AUTODEBET}
											</option>
										</c:forEach> 
									</select>
								</c:if>
								<c:if test="${cmd.input_type eq 'typeafree'}">
									FREE
								</c:if>
							<font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">bentuk pembayaran:</td>
							<td>
								TAHUNAN
							<font class="error">*</font></td>
						</tr>
						<c:if test="${cmd.input_type ne 'typeafree' && cmd.product_code ne '73' && cmd.input_type ne 'default'}">
						<tr>
							<th colspan="2">Data Agen</th>
						</tr>
						<tr>
							<td align="left">kode agen:</td>
							<td>
								<spring:bind path="cmd.msag_id" >
									<input type="text" id="msag_id" name="${status.expression}" value="${status.value }" size="8" onblur="agen_cek();"/>
								</spring:bind>
								<input type="text" name="nama_agen" id="nama_agen" size="50" disabled="disabled"/>
							<font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">kode_ao:</td>
							<td>
								<form:input path="kode_ao" cssErrorClass="inpError"/>
							</td>
						</tr>
						<tr>
							<td align="left">pribadi:</td>
							<td>
								<form:checkbox id="pribadi" cssClass="noBorder" path="pribadi" value="1"/>
							</td>
						</tr>
							<td align="left">cabang:</td>
							<td>
								<input type="text" name="cabang_agen" id="cabang_agen" size="30" disabled="disabled"/>
							</td>
						</tr>
						</c:if>
						<c:if test="${cmd.product_code eq '73'}">
						<tr>
						    <th colspan="2">Data Referral</th>
						</tr>
						<tr>
						    <td colspan="2">
						        <table class="result_table2">
						            <tr>
			                            <td>
			                                <table class="result_table2">
			                                    <tr>
			                                        <th colspan="2">Informasi Agen Penutup</th>
			                                    </tr>
			                                    <tr>
			                                        <td>Cari Agen Penutup(Nama)</td>
			                                        <td>
			                                            <input type="text" value="" size="27" name="carinama" onfocus="this.select();" onkeypress="if(event.keyCode==13){ document.forms['frmParam'].btncarireff1.click(); return false;}"/>
			                                            <input type="button" name="btncarireff1" value="Cari" onclick="cari_reff(1);"/>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td>Cari Agen Penutup(Kode)</td>
			                                        <td>
			                                            <input type="text" value="" size="27" name="carikode" onfocus="this.select();" onkeypress="if(event.keyCode==13){ document.forms['frmParam'].btncarireff3.click(); return false;}"/>
			                                            <input type="button" name="btncarireff3" value="Cari" onclick="cari_reff(3);"/>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td>Agen Penutup</td>
			                                        <td>
			                                            <div id="nama">
			                                                <select name="lrb_id">
			                                                    <option value="${cmd.lrb_id}">${cmd.nama_agent}</option>
			                                                </select>
			                                            </div>
                                                        <input type="hidden" name="nama_agent" value="${cmd.nama_agent}"/>
			                                        </td>
			                                    <tr>
			                                </table>
			                            </td>
			                            <td>
			                                <table class="result_table2">
			                                    <tr>
			                                        <th colspan="2">INFORMASI REFERRAL (Khusus BSM/SMS)</th>
			                                    </tr>
			                                    <tr>
			                                        <td>Cari Referral(Nama)</td>
			                                        <td>
			                                            <input type="text" size="27" value="" name="carinama2" onfocus="this.select();" onkeypress="if(event.keyCode==13){ document.forms['frmParam'].btncarireff2.click(); return false;}"/>
			                                            <input type="button" name="btncarireff2" value="Cari" onclick="cari_reff(2);"/>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td>Cari Referal(Kode)</td>
			                                        <td>
			                                            <input type="text" value="" size="27" name="carikode2" onfocus="this.select();" onkeypress="if(event.keyCode==13){ document.forms['frmParam'].btncarireff4.click(); return false;}"/>
			                                            <input type="button" name="btncarireff4" value="Cari" onclick="cari_reff(4);"/>
			                                        </td>
			                                    </tr>
			                                    <tr>
			                                        <td>Referral</td>
			                                        <td>
			                                            <div id="nama2">
			                                                <select name="reff_id">
			                                                    <option value="${cmd.reff_id}">${cmd.nama_reff}</option>
			                                                </select>
			                                            </div>
                                                        <input type="hidden" name="nama_reff" value="${cmd.nama_reff}"/>
			                                        </td>
			                                    <tr>
			                                </table>
			                            </td>
			                        </tr>
						        </table>
						    </td>
						</tr>
						</c:if>
						<c:if test="${sessionScope.currentUser.jn_bank ne '2' && cmd.product_code ne '73'}" >
						<tr>
							<th colspan="2">Data Rekening Pemegang Polis</th>
						</tr>
						<tr>
							<td align="left">cari bank:</td>
							<td><input type="text" name="caribank1" id="caribank1" value="" onkeypress="if(event.keyCode==13){ document.forms['frmParam'].btncari1.click(); return false;}"> 
	              				<input type="button" name="btncari1" id="btncari1" value="Cari" onclick="ajaxSelectWithParam1a(document.getElementById('caribank1').value,'select_bank1','bank1','lsbp_id','', 'BANK_ID', 'BANK_NAMA', '','Silahkan pilih BANK','3');"></td>
						</tr>
						<tr>
							<td align="left">bank:</td>
							<td><div id="bank1"> 
								<select name="lsbp_id" id="lsbp_id" >
				                  <option value="${cmd.lsbp_id}">${cmd.lsbp_nama}</option>
				                </select>
				                </div>
							</td>
						</tr>
						<tr>
							<td align="left">no rekening:</td>
							<td>
							<input type="text" name="rekening[0]" id="rekening[0]" size="1"    maxlength="1" onkeyup="dofo(0, 1);doRek();" />
							<input type="text" name="rekening[1]" id="rekening[1]" size="1"    maxlength="1" onkeyup="dofo(1, 1);doRek();" />
							<input type="text" name="rekening[2]" id="rekening[2]" size="1"    maxlength="1" onkeyup="dofo(2, 1);doRek();" />
							<input type="text" name="rekening[3]" id="rekening[3]" size="1"    maxlength="1" onkeyup="dofo(3, 1);doRek();" />
							<input type="text" name="rekening[4]" id="rekening[4]" size="1"    maxlength="1" onkeyup="dofo(4, 1);doRek();" />
							<input type="text" name="rekening[5]" id="rekening[5]" size="1"    maxlength="1" onkeyup="dofo(5, 1);doRek();" />
							<input type="text" name="rekening[6]" id="rekening[6]" size="1"    maxlength="1" onkeyup="dofo(6, 1);doRek();" />
							<input type="text" name="rekening[7]" id="rekening[7]" size="1"    maxlength="1" onkeyup="dofo(7, 1);doRek();" />
							<input type="text" name="rekening[8]" id="rekening[8]" size="1"    maxlength="1" onkeyup="dofo(8, 1);doRek();" />
							<input type="text" name="rekening[9]" id="rekening[9]" size="1"    maxlength="1" onkeyup="dofo(9, 1);doRek();" />
							<input type="text" name="rekening[10]" id="rekening[10]" size="1"    maxlength="1" onkeyup="dofo(10, 1);doRek();" />
							<input type="text" name="rekening[11]" id="rekening[11]" size="1"    maxlength="1" onkeyup="dofo(11, 1);doRek();" />
							<input type="text" name="rekening[12]" id="rekening[12]" size="1"    maxlength="1" onkeyup="dofo(12, 1);doRek();" />
							<input type="text" name="rekening[13]" id="rekening[13]" size="1"    maxlength="1" onkeyup="dofo(13, 1);doRek();" />
							<input type="text" name="rekening[14]" id="rekening[14]" size="1"    maxlength="1" onkeyup="dofo(14, 1);doRek();" />
							<input type="text" name="rekening[15]" id="rekening[15]" size="1"    maxlength="1" onkeyup="dofo(15, 1);doRek();" />
							<input type="text" name="rekening[16]" id="rekening[16]" size="1"    maxlength="1" onkeyup="dofo(16, 1);doRek();" />
							<input type="text" name="rekening[17]" id="rekening[17]" size="1"    maxlength="1" onkeyup="dofo(17, 1);doRek();" />
							<input type="text" name="rekening[18]" id="rekening[18]" size="1"    maxlength="1" onkeyup="dofo(18, 1);doRek();" />
							<input type="text" name="rekening[19]" id="rekening[19]" size="1"    maxlength="1" onkeyup="dofo(19, 1);doRek();" />
							<input type="text" name="rekening[20]" id="rekening[20]" size="1"    maxlength="1" onkeyup="dofo(20, 1);doRek();" /><font class="error"></font>
							<span style="visibility: hidden;"><form:input path="msp_no_rekening" id="msp_no_rekening" cssErrorClass="inpError" size="1"/></span></td>
						</tr>
						<tr>
							<td align="left">cabang:</td>
							<td><form:input path="msp_rek_cabang" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">kota:</td>
							<td><form:input path="msp_rek_kota" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">atas nama:</td>
							<td><form:input path="msp_rek_nama" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						</c:if>
						<c:if test="${cmd.input_type ne 'typeafree' && cmd.input_type ne 'default'}">
						<tr>
							<th colspan="2">Data Rekening Pemegang Polis (AUTODEBET)</th>
						</tr>
						<tr>
							<td align="left">cari bank:</td>
							<td><input type="text" name="caribank2" id="caribank2" onkeypress="if(event.keyCode==13){ document.forms['frmParam'].btncari2.click(); return false;}"> 
	              				<input type="button" name="btncari2" id="btncari2" value="Cari" onclick="ajaxSelectWithParam1a(document.getElementById('caribank2').value,'select_bank2','bank2','lsbp_id_autodebet','', 'BANK_ID', 'BANK_NAMA', '','Silahkan pilih BANK','3');"></td>
						</tr>
						<tr>
							<td align="left">bank:</td>
							<td><div id="bank2"> 
								<select name="lsbp_id_autodebet" id="lsbp_id_autodebet">
				                  <option value="${cmd.lsbp_id_autodebet}">${cmd.lsbp_nama_autodebet}</option>
				                </select>
				                </div>
							</td>
						</tr>
						<tr>
							<td align="left">no rekening:</td>
							<td>
							<input type="text" name="rekening_autodebet[0]" id="rekening_autodebet[0]" size="1"    maxlength="1" onkeyup="dofo_autodebet(0, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[1]" id="rekening_autodebet[1]" size="1"    maxlength="1" onkeyup="dofo_autodebet(1, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[2]" id="rekening_autodebet[2]" size="1"    maxlength="1" onkeyup="dofo_autodebet(2, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[3]" id="rekening_autodebet[3]" size="1"    maxlength="1" onkeyup="dofo_autodebet(3, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[4]" id="rekening_autodebet[4]" size="1"    maxlength="1" onkeyup="dofo_autodebet(4, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[5]" id="rekening_autodebet[5]" size="1"    maxlength="1" onkeyup="dofo_autodebet(5, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[6]" id="rekening_autodebet[6]" size="1"    maxlength="1" onkeyup="dofo_autodebet(6, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[7]" id="rekening_autodebet[7]" size="1"    maxlength="1" onkeyup="dofo_autodebet(7, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[8]" id="rekening_autodebet[8]" size="1"    maxlength="1" onkeyup="dofo_autodebet(8, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[9]" id="rekening_autodebet[9]" size="1"    maxlength="1" onkeyup="dofo_autodebet(9, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[10]" id="rekening_autodebet[10]" size="1"    maxlength="1" onkeyup="dofo_autodebet(10, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[11]" id="rekening_autodebet[11]" size="1"    maxlength="1" onkeyup="dofo_autodebet(11, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[12]" id="rekening_autodebet[12]" size="1"    maxlength="1" onkeyup="dofo_autodebet(12, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[13]" id="rekening_autodebet[13]" size="1"    maxlength="1" onkeyup="dofo_autodebet(13, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[14]" id="rekening_autodebet[14]" size="1"    maxlength="1" onkeyup="dofo_autodebet(14, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[15]" id="rekening_autodebet[15]" size="1"    maxlength="1" onkeyup="dofo_autodebet(15, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[16]" id="rekening_autodebet[16]" size="1"    maxlength="1" onkeyup="dofo_autodebet(16, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[17]" id="rekening_autodebet[17]" size="1"    maxlength="1" onkeyup="dofo_autodebet(17, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[18]" id="rekening_autodebet[18]" size="1"    maxlength="1" onkeyup="dofo_autodebet(18, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[19]" id="rekening_autodebet[19]" size="1"    maxlength="1" onkeyup="dofo_autodebet(19, 1);doRek_autodebet();" />
							<input type="text" name="rekening_autodebet[20]" id="rekening_autodebet[20]" size="1"    maxlength="1" onkeyup="dofo_autodebet(20, 1);doRek_autodebet();" /><font class="error"></font>
							<span style="visibility: hidden;"><form:input path="msp_no_rekening_autodebet" id="msp_no_rekening_autodebet" cssErrorClass="inpError" size="1"/></span></td>
						</tr>
						<tr>
							<td align="left">atas nama:</td>
							<td><form:input path="msp_rek_nama_autodebet" cssErrorClass="inpError"/><font class="error">*</font></td>
						</tr>
						<tr>
							<td align="left">tanggal debet:</td>
							<td><div id="msp_tgl_debet_div" >
								<spring:bind path="cmd.msp_tgl_debet" >
        		                    <script>
                                        var isDisabled;
                                        var status = '${statusForm}';
                                        if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
                                </spring:bind>
							<font class="error">*</font></div></td>
						</tr>
						<tr>
							<td align="left">tanggal valid:</td>
							<td><div id="msp_tgl_valid_div" >
								<spring:bind path="cmd.msp_tgl_valid" >
        		                    <script>
                                        var isDisabled;
                                        var status = '${statusForm}';
                                        if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
                                </spring:bind>
							<font class="error">*</font></div></td>
						</tr>
                        <c:if test="${cmd.product_code eq '73'}">
                        <tr>
                            <th colspan="2">Validasi</th>
                        </tr>
                        <tr>
                            <td align="left">
                                Apakah Surat Kuasa Debet sudah diisi dan ditandatangani nasabah?
                            </td>
                            <td>
                                <input type="radio" name="validasiDebet" value="1"> Ya
                                <input type="radio" name="validasiDebet" value="0"> Tidak
                            </td>
                        </tr>
                        </c:if>
						</c:if>
						</table></fieldset>
						<fieldset>
					   <legend>ACTION</legend>
					   <table class="result_table2">
						<tr>
							<td colspan="2">
								<input type="hidden" name="msp_id" id="msp_id" value="${msp_id}" />
								<input type="hidden" name="kata" id="kata" size="1" value="insert" />
								<input type="button" name="edit" id="edit" value="Edit" style="visibility: hidden;width: 1px" onclick="return update();"/>
								<input type="submit" name="save" id="save" value="Save"  onclick="return insert();" />
								<input type="button" name="close" id="save" value="Close" onclick="window.close();" />
								<a href="input_fire.htm" id="refreshPage"></a>
							</td>
						</tr>
					</table>
					</fieldset>
					</div>
		</div>
	</div>	
</form:form>
	<c:if  test="${not empty successMessage}">	
			<script type="text/javascript">													
				alert('${successMessage}');	
				parentForm = self.opener.document.forms['formpost'];
				window.close();
			</script>
	</c:if>
    <c:if test="${not empty msgKartu}">
        <script>
            alert('${msgKartu}');
        </script>
    </c:if>
</body>
</html>