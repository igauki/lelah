<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	function chg(asdf){
			document.formpost.cabangBII.disabled = true;
			document.formpost.cabangBankSinarmas.disabled = true;
		if(asdf=='bii'){
			document.formpost.cabangBII.disabled = false;
		}else if(asdf=='bank_sinarmas'){
			document.formpost.cabangBankSinarmas.disabled = false;
		}
	}
	
	function rubah(lca){
		if(lca == '09') {
			document.formpost.cabangBII.disabled = false;
			//document.formpost.cabangBankSinarmas.disabled = false;
			document.getElementById('bii').disabled = false;
			document.getElementById('bank_sinarmas').disabled = false;
		}else{
			document.formpost.cabangBII.disabled = true;
			document.formpost.cabangBankSinarmas.disabled = true;
			document.getElementById('bii').disabled = true;
			document.getElementById('bank_sinarmas').disabled = true;
		}
	}
</script>
</head>
<BODY onload="setupPanes('container1', 'tab1'); setFrameSize('infoFrame', 170);" style="height: 100%;" onresize="setFrameSize('infoFrame', 170);">
	<form method="get" name="formpost" target="infoFrame" action="${path}/uw/simas/print.htm">
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Print Simas Card</a>
			</li>
		</ul>
		<div class="tab-panes">
			<div id="pane1" class="panes">
				<table class="entry2">
					<tr>
						<th style="width: 100px;">Cetak</th>
						<td style="width: 200px;">
							<c:forEach items="${daftarCetak}" var="d">
								<label for="${d.value}"><input type="radio" id="${d.value}" class="noBorder" name="cetak" value="${d.key}" checked="checked">${d.value}</label>
							</c:forEach>
						</td>
					</tr>
					<tr>
						<th>Jenis</th>
						<td>
							<select name="jenis" style="width: 180px;">
								<c:forEach items="${daftarJenis}" var="j">
									<option value="${j.key}">${j.value}</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<th>Cabang</th>
						<td>
							<select name="cabang" onchange="rubah(this.value);" style="width: 180px;">
								<c:forEach items="${daftarCabang}" var="c">
									<option value="${c.key}">${c.value}</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<th>Bank</th>
						<td>
							<label for="bii"><input type="radio" onclick="chg('bii');" id="bii" name="bank" class="noBorder" disabled="disabled" value="0" checked="checked">BII</label>
							<select name="cabangBII" disabled="disabled">
								<c:forEach items="${daftarCabangBII}" var="c">
									<option value="${c.key}">${c.value}</option>
								</c:forEach>
							</select>
						</td>
						<td>
							<label for="bank_sinarmas"><input type="radio" onclick="chg('bank_sinarmas');" id="bank_sinarmas" name="bank" class="noBorder" disabled="disabled" value="1">Bank Sinarmas</label>
							<select name="cabangBankSinarmas" disabled="disabled">
								<c:forEach items="${daftarCabangBankSinarmas}" var="c">
									<option value="${c.key}">${c.value}</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<th>Jumlah Print</th>
						<td colspan="2">
							<input type="text" name="jumlah_print" value="50" size="3">
							<input type="submit" value="Retrieve" name="retrieve">
							<input type="submit" value="Print" name="print">
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<iframe name="infoFrame" id="infoFrame" width="99%"  height="100%"> Please Wait... </iframe>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>