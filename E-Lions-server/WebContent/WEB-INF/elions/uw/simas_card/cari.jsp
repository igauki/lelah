<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script>
<script type="text/javascript" src="${path }/dwr/util.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	function cari(){
		if(trim(document.formpost.kata.value)=='') return false;
		else createLoadingMessage();	
	}

	function backToParent(msc_jenis, no_kartu){
		var dok;
		if(self.opener)
			dok = self.opener.document;
		else
			dok = parent.document;
		
		var forminput;
		if(dok.formpost) forminput = dok.formpost;
		else if(dok.frmParam) forminput = dok.frmParam;
		
		forminput.msc_jenis.value = msc_jenis;
		forminput.no_kartu.value = no_kartu;
		forminput.tampil.click();
			
		if(self.opener) window.close();
	}

</script>
</head>
<BODY onload="resizeCenter(800, 400); document.title='PopUp :: Cari Simas Card'; setupPanes('container1', 'tab1'); document.formpost.kata.select(); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Cari Simas Card</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" action="${path }/uw/simas/cari.htm" style="text-align: center;">
					<fieldset>
					<table class="entry2">
						<tr>
							<th rowspan="2">Cari:</th>
							<th class="left">
								<select name="jenis">
									<c:forEach items="${daftarJenis}" var="j">
										<option value="${j.key}" <c:if test="${jenis eq j.key}">selected</c:if>>${j.value}</option>
									</c:forEach>
								</select>
								<input type="text" name="kata" size="35" value="${kata}" 
									onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
								<input type="submit" name="search" value="Search" onclick="return cari();">
								<input type="button" name="close2" value="Close" onclick="window.close();">
							</th>
						</tr>
					</table>
					</fieldset>
					<table class="simple">
						<thead>
							<tr>
								<th style="text-align: center;">Jenis</th>
								<th style="text-align: center;">No. Kartu</th>
								<th style="text-align: center;">Nama</th>
								<th style="text-align: center;">No. SPAJ</th>
								<th style="text-align: center;">No. Kartu</th>
								<th style="text-align: center;">Tgl Lahir</th>
								<th style="text-align: center;">Cabang</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="s" items="${daftarSimasCard}" varStatus="st">
								<tr 	onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"
										onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
										onclick="backToParent('${s.msc_jenis}', '${s.no_kartu}');">
									<td>
										<c:forEach items="${daftar}" var="d">
											<c:if test="${s.msc_jenis eq d.key}">${d.value}</c:if>
										</c:forEach>
									</td>
									<td>${s.no_kartu}</td>
									<td>${s.nama}</td>
									<td><elions:spaj nomor="${s.reg_spaj}"/></td>
									<td><elions:polis nomor="${s.no_kartu}"/></td>
									<td><fmt:formatDate value="${s.tgl_lahir}" pattern="dd/MM/yyyy"/></td>
									<td>${s.lca_cabang}</td>
									<c:if test="${st.count eq 1}">
										<c:set var="v1" value="${s.msc_jenis}"/>
										<c:set var="v2" value="${s.no_kartu}"/>
									</c:if>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<br>
					<input type="button" name="close" value="Close" onclick="window.close();">

				</form>
			</div>
		</div>
	</div>

</form>
<c:if test="${jml eq 1}">
<script>backToParent1('${v1}', '${v2}');</script>
</c:if>
</body>
<%@ include file="/include/page/footer.jsp"%>