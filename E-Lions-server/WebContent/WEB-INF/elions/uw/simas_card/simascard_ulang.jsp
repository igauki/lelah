<%@ include file="/include/page/header.jsp"%>

<body style="height: 100%;">
	<form commandName="cmd" method="post" name="frmParam" id="frmParam">
		<table class="entry2">
			<tr>
				<th style="width: 150px;">Tanggal</th>
				<td>
					
					<input size="20" style="text-align: center;" class="readOnly" name="tgl" type="text" value="<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy hh:mm"/>" readonly="readonly">
					<input size="30" class="readOnly" name="status" type="text" value="${sessionScope.currentUser.name}" readonly="readonly">
				</td>
			</tr>
			<tr>
				<th style="width: 150px;">No Polis/SPAJ</th>
				<td>
					<input type="text" name="reg_spaj" type="text" id="reg_spaj" value="${reg_spaj}">
				</td>
			</tr>
			<tr>          
			    <th width="15%"  height="20">Cari 
				    Nomor Kartu&nbsp;
				</th>
				<td colspan="3"> 
					<input type="text" name="carinomor" maxlength="6" onkeypress="if(event.keyCode==13){ document.frmParam.btncari1.click(); return false;}"> 
					<font color="#CC3300" style="text-transform: none;">* Masukkan 6 digit terakhir no Simas Card untuk memudahkan pencarian</font> 
				</td>
			</tr>
			<tr>      
				<th width="15%"  height="20">Nomor Kartu&nbsp; </th>
				<td colspan="3"> 
					<div id="kartu"
						<c:if test="${ not empty status.errorMessage}">
								style='background-color :#FFE1FD'
						</c:if>> 
				         <select name="mrc_no_kartu" id="mrc_no_kartu">
				                <option value="${mrc_no_kartu}" >${mrc_no_kartu}</option>
				         </select>
				         <input type="button" name="btncari1" value="Cari" onclick="ajaxSelectWithParam1a(document.frmParam.carinomor.value,'select_simas_card','kartu','mrc_no_kartu','', 'KARTU_ID', 'NO_KARTU', '','Silahkan pilih Nomor Kartu','1');">
				    </div>
				    <input type="hidden" name="no_kartu" id="no_kartu" value="${no_kartu}" >
				</td>            
         	</tr>			
			<tr>
				<th>Keterangan</th>
				<td>
					<textarea rows="4" cols="51" name="ket">${ket }</textarea>
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<input type="submit" name="save" value="Save Simas Card">

					<c:choose>
						<c:when test="${not empty success}">
							<div id="success">
						        	 ${success }
						    	</div>
						</c:when>
						<c:when test="${not empty err}">
							<div id="error">${err}</div>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>					
				</td>
			</tr>
		</table>
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>