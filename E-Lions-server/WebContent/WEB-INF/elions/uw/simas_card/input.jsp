<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<!-- DatePicker Script (jscalendar) -->
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	function mulai(){
		<c:if test="${param.a eq 1}">
			alert('Data berhasil disimpan');
		</c:if>
	}
</script>
</head>
<BODY onload="setupPanes('container1', 'tab1'); mulai();" style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Input Simas Card</a>
			</li>
		</ul>

		<div class="tab-panes">
		
			<div id="pane1" class="panes">
				<form:form name="formpost" id="formpost" commandName="cmd">
					<fieldset style="text-align: left;">
						<table class="entry2" style="width: auto;">
							<spring:hasBindErrors name="cmd">
								<tr>
									<td colspan="4">
										<div id="error">
											<strong>Data yang dimasukkan tidak lengkap. Mohon lengkapi data-data berikut:</strong>
											<br />
											<form:errors path="*" delimiter="<br>" />
										</div>
									</td>
								</tr>
							</spring:hasBindErrors>
							<tr>
								<th style="width: 120px;">Jenis</th>
								<td style="width: 250px;">
									<c:choose>
										<c:when test="${not empty cmd.lus_id}">
											<form:select path="msc_jenis" items="${daftarJenis}" itemValue="key" itemLabel="value" disabled="true" cssClass="readOnly"/>
										</c:when>
										<c:otherwise>
											<form:select path="msc_jenis" items="${daftarJenis}" itemValue="key" itemLabel="value" />
										</c:otherwise>
									</c:choose>
								</td>
								<th style="width: 120px;">Cabang</th>
								<td>
									<form:select path="lca_id" items="${daftarCabang}" itemValue="key" itemLabel="value" />
								</td>
							</tr>
							<tr>
								<th>No. Kartu</th>
								<td>
									<c:choose>
										<c:when test="${not empty cmd.lus_id}">
											<form:input path="no_kartu" size="25" maxlength="20" readonly="true" cssClass="readOnly"/>
										</c:when>
										<c:otherwise>
											<form:input path="no_kartu" size="25" maxlength="20"/>
										</c:otherwise>
									</c:choose>

									<input name="tampil" type="button" onclick="window.location='${path}/uw/simas/input.htm?mj=' + document.formpost.msc_jenis.value + '&nk=' + document.formpost.no_kartu.value;" style="visibility: hidden;">
								</td>
								<th>Nama</th>
								<td>
									<form:input path="nama" size="60" maxlength="100"/>
								</td>
							</tr>
							<tr>
								<th>Valid Thru</th>
								<td>
									<spring:bind path="cmd.tgl_akhir">
										<script>inputDate('${status.expression}', '${status.value}', false);</script>
									</spring:bind>
								</td>
								<th>Tgl Lahir</th>
								<td>
									<spring:bind path="cmd.tgl_lahir">
										<script>inputDate('${status.expression}', '${status.value}', false);</script>
									</spring:bind>
								</td>
							</tr>
						</table>
					</fieldset>
					<fieldset style="text-align: left;">
						<table class="entry2" style="width: auto;">
							<tr>
								<th style="width: 120px;">Alamat</th>
								<td colspan="3">
									<form:textarea path="alamat" rows="3" cols="60" onkeyup="textCounter(this, 200);" onkeydown="textCounter(this, 200);"/>
								</td>
							</tr>
							<tr>
								<th>Kota</th>
								<td style="width: 250px;">
									<form:input path="kota" size="30" maxlength="30"/>
								</td>
								<th style="width: 120px;">Kode Pos</th>
								<td>
									<form:input path="kode_pos" size="15" maxlength="10"/>
								</td>
							</tr>
						</table>
					</fieldset>
					<fieldset style="text-align: left;">
						<table class="entry2" style="width: auto;">
							<tr>
								<th style="width: 120px;">Flag Print</th>
								<td style="width: 250px;">
									<form:select path="flag_print" items="${daftarFlagPrint}" itemValue="key" itemLabel="value" />
								</td>
								<th style="width: 120px;">Tgl Print</th>
								<td>
									<spring:bind path="cmd.tgl_print">
										<script>inputDate('${status.expression}', '${status.value}', false);</script>
									</spring:bind>
								</td>
							</tr>
							<tr>
								<th>Flag Aktif</th>
								<td>
									<form:select path="flag_aktif" items="${daftarYesNo}" itemValue="key" itemLabel="value" />
								</td>
								<th>Flag Roll</th>
								<td>
									<form:select path="flag_roll" items="${daftarYesNo}" itemValue="key" itemLabel="value" />
								</td>
								<th style="width: 120px;">Kartu Ke</th>
								<td>
									<form:input path="kartu_ke" size="3" maxlength="2"/>
								</td>
							</tr>
							<tr>
								<th>Notes</th>
								<td colspan="3">
									<form:textarea path="notes" rows="3" cols="60" onkeyup="textCounter(this, 50);" onkeydown="textCounter(this, 50);"/>
								</td>
							</tr>
							<tr>
								<th>User</th>
								<td>
									<input type="text" size="30" readonly="readonly" class="readOnly" value="${cmd.lus_login_name}">
								</td>
								<th>Tgl Input</th>
								<td>
									<input type="text" readonly="readonly" class="readOnly" value="<fmt:formatDate value="${cmd.tgl_input}" pattern="dd/MM/yyyy (HH:mm)"/>">
								</td>
							</tr>
							<tr>
								<th></th>
								<td>
									<input type="button" value="New" onclick="window.location='${path}/uw/simas/input.htm';">
									<input type="button" value="Edit" onclick="popWin('${path}/uw/simas/cari.htm', 400, 800);">
									<input type="submit" value="Save" name="save" onclick="return confirm('Simpan Data?');">
									<input type="button" value="Print &raquo;" onclick="window.location='${path}/uw/simas/print.htm';">
								</td>
							</tr>
						</table>
					</fieldset>
				</form:form>
			</div>
			
		</div>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>