<%@ include file="/include/page/header.jsp"%>
	<script>
		function backToParent(nama){
			var parentForm = self.opener.document.forms['formpost'];
			var indx = document.getElementById("indx").value;
			var sub = document.getElementById("sub").value;
			
			if(parentForm) {
				parentForm.elements[sub+'_tmpMcu'+indx].value = nama;
				parentForm.elements['submitMode'].value = "medicalType";
				parentForm.elements['medicalType'].value = sub+"_"+indx;
				parentForm.submit();
			}	
			
			window.close();	
		}
	</script>	
	<body onload="document.title='PopUp :: Cari Provider'; document.formpost.data.focus(); ">
		<form method="post" name="formpost" onsubmit="createLoadingMessage();">
			<fieldset>
				<legend>Search Key</legend>
				<table class="entry2" border="0">
					<tr>
						<th width="60%">Nama</th>
						<th width="40%">Alamat</th>
						<th rowspan="2"><input type="submit" name="btnCari" value="Search"></th>
					</tr>
					<tr align="left">
						<th width="60%"><input type="text" id="name" name="name" style="width: 100%"></th>
						<th width="40%"><input type="text" id="addr" name="addr" style="width: 100%"></th>
					</tr>
				</table>
			</fieldset>
			<fieldset>
				<legend>Search Result</legend>	
				<table class="entry2">
					<thead>
						<tr>
							<th width="60%">Nama</th>
							<th>Alamat</th>
						</tr>
					</thead>
					<tbody>
					<c:forEach var="x" items="${daftar}" varStatus="xt">
						<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;" onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
						onclick="backToParent('${x.RSNAMA}');">
							<td>${x.RSNAMA}</td>
							<td>${x.RSALAMAT}</td>
						</tr>
					</c:forEach>
					</tbody>	
					<tr>
						<td colspan="3">
							<div align="center">
								<input type="button" name="close" value="Close" onclick="window.close();">
							</div>
						</td>
					</tr>				
				</table>
			</fieldset>
			<input type="hidden" id="indx" name="indx" value="${indx}">	
			<input type="hidden" id="sub" name="sub" value="${sub}">
		</form>
	</body>
<%@ include file="/include/page/footer.jsp"%>
