<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<!-- DatePicker Script (jscalendar) -->
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	function onLoad(){
		if(trim(document.formpost.result.value)!=''){
			alert(document.formpost.result.value);
		}
	}
	
	function cariData(){
		var pilter = document.getElementById('pilter').value;
		var tipe = document.getElementById('tipe').value;
		var kata = document.getElementById('kata').value;
		var jenisDist = '';
		if(document.getElementById('jenisDist_idv').checked){
			jenisDist = document.getElementById('jenisDist_idv').value;
		}else if(document.getElementById('jenisDist_dmtm').checked){
			jenisDist = document.getElementById('jenisDist_dmtm').value;
		}
		document.getElementById('refreshPage').href = 'akseptasi_fire.htm?tipe='+tipe+'&kata='+kata+'&pilter='+pilter+'&jenisDist='+jenisDist;
		document.getElementById('refreshPage').click();
	
	}
	
	function update(msp_id, action){
		document.getElementById('msp_id').value = msp_id;
		document.getElementById('action').value = action;
		if(trim(document.getElementById('action').value)=='') return false;
		else createLoadingMessage();
	}
	
	function update_all(action){
		document.getElementById('action').value = action;
		if(trim(document.getElementById('action').value)=='') return false;
		else createLoadingMessage();
	}
	
	function alasan(isi_alasan, msp_id){
				do{
					isi_alasan = prompt("HARAP MASUKKAN ALASANNYA :", isi_alasan);
					if(isi_alasan != null){
						isi_alasan = trim(isi_alasan);
					}
				}while(isi_alasan == '');
				// tombol cancel -> null
				if(isi_alasan == null){
					document.getElementById('isi_alasan').value = '';
					//return false;
				}else{
					document.getElementById('isi_alasan').value = isi_alasan;
					update(msp_id, 'reject');
					document.getElementById('reject').click();
					//return true;
				}
	}
	
	function alasan_all(){
				do{
					isi_alasan = prompt("HARAP MASUKKAN ALASANNYA :", '');
					if(isi_alasan != null){
						isi_alasan = trim(isi_alasan);
					}
				}while(isi_alasan == '');
				// tombol cancel -> null
				if(isi_alasan == null){
					document.getElementById('isi_alasan').value = '';
					//return false;
				}else{
					document.getElementById('isi_alasan').value = isi_alasan;
					update_all('reject all');
					document.getElementById('reject').click();
					//return true;
				}
	}
	
	function aksep(nama, msp_id){
				if(confirm('Apakah anda yakin untuk AKSEP fire atas fire id = '+nama+'?')){
					update(msp_id, 'aksep');
					document.getElementById('save').click();
					//return true;
				}
	}
	
	function resend_process(nama, msp_id){
				if(confirm('Apakah anda yakin untuk RESEND fire atas fire id '+nama+'?')){
					update(msp_id, 'resend');
					document.getElementById('resend').click();
					//return true;
				}
	}
	
	function jenis_dist(){
		var jenisDist = '${jenisDist}';
		if(jenisDist == 'individu'){
			document.getElementById('jenisDist_idv').checked = 'checked';
		}else if(jenisDist == 'dmtm'){
			document.getElementById('jenisDist_dmtm').checked = 'checked';
		}else{
			document.getElementById('jenisDist_all').checked = 'checked';
		}
	}
	
	function reject_process(nama, isi_alasan, msp_id){
		if(isi_alasan == ''){
			if(confirm('Apakah anda yakin untuk REJECT fire atas fire id '+nama+'?')){
					alasan(isi_alasan, msp_id);
				}
		}else{
			if(confirm('Apakah anda yakin untuk REJECT fire atas fire id '+nama+' karena '+isi_alasan+'?')){
					alasan(isi_alasan, msp_id);
				}
		}
	}
	
	function reject_all_process(){
		if(confirm('Apakah anda yakin untuk REJECT fire id free simas rumah?')){
				alasan_all();
			}
	}
	
</script>
</head> 
<BODY onload="jenis_dist();resizeCenter(650,400); setupPanes('container1', 'tab1'); document.formpost.kata.select(); onLoad();" style="height: 100%;">
	<div class="tab-container" id="container1" style="background-color: pink;">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1"> 
				FIRE 
				</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes"  style="background-color: pink;">
				<form method="post" name="formpost" action="#" style="text-align: center;">
					<table class="simple" style="border-color: black;">
						<thead >
							<tr>
								<th style="text-align: left;border-color: black;">Dist.</th>
								<th style="text-align: left;border-color: black;">PAS</th>
								<th style="text-align: left;border-color: black;">Nama</th>
								<th style="text-align: left;border-color: black;">Tgl.Lahir</th>
								<th style="text-align: left;border-color: black;">No. Identitas</th>
								<th style="text-align: left;border-color: black;">No. Kartu</th>
								<th style="text-align: left;border-color: black;">PIN</th>
								<th style="text-align: left;border-color: black;">No. HP</th>
								<th style="text-align: left;border-color: black;" width="150px">Alamat</th>
								<th style="text-align: left;border-color: black;">Kota</th>
								<th style="text-align: left;border-color: black;">Kode Pos</th>
								<th style="text-align: left;border-color: black;">E-Mail</th>
								<th style="text-align: left;border-color: black;">Fire Id</th>
								<th style="text-align: left;border-color: black;" width="100px">Alasan Ditolak</th>
								<th style="text-align: left;border-color: black;" width="200px">Action</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="pas" items="${cmd}" varStatus="stat">
								<tr>
										<td style="border-color: black;">
											<c:choose>
												<c:when test="${pas.dist eq '05'}">
													individu
												</c:when>
												<c:when test="${pas.dist eq '01'}">
													dmtm
												</c:when>
												<c:otherwise>
													dmtm
												</c:otherwise>
											</c:choose>
										</td>
										<td style="border-color: black;">
											<c:choose>
												<c:when test="${pas.msp_flag_pas eq \"0\"}">
													NO
												</c:when>
												<c:when test="${pas.msp_flag_pas eq \"1\"}">
													YES
												</c:when>
												<c:otherwise>
													N/A
												</c:otherwise>
											</c:choose>
										
										</td>
										<td style="border-color: black;">${pas.msp_fire_name}</td>
										<td style="border-color: black;">${pas.msp_fire_date_of_birth2}</td>
										<td style="border-color: black;">${pas.msp_fire_identity}</td>
										<td style="border-color: black;">${pas.no_kartu}</td>
										<td style="border-color: black;">${pas.pin}</td>
										<td style="border-color: black;">${pas.msp_fire_mobile}</td>
										<td style="border-color: black;">${pas.msp_fire_address_1}</td>
										<td style="border-color: black;">${pas.msp_fire_insured_city}</td>
										<td style="border-color: black;">${pas.msp_fire_postal_code}</td>
										<td style="border-color: black;">${pas.msp_fire_email}</td>
										<td style="border-color: black;">${pas.msp_fire_id}</td>
										<td style="border-color: black;">${pas.msp_fire_fail_desc}</td>
										<td style="border-color: black;">
											<c:choose>
												<c:when test="${pas.msp_fire_export_flag == 4}">
													<input type="button" value="VIEW" name="inputDetail"
																onclick="popWin('${path}/uw/input_fire_detail.htm?posisi=1&win=edit&msp_id='+${pas.msp_id}, 350, 450); "
																onmouseover="return overlib('Input PAS Detail', AUTOSTATUS, WRAP);" onmouseout="nd();" />
													<input type="button" name="resend_btn" value="resend_btn" onclick="resend_process('${pas.msp_fire_id}','${pas.msp_id}');"
														onmouseout="nd();"/>
													<input type="button" name="reject_btn" value="reject" onclick="reject_process('${pas.msp_fire_id}', '${pas.msp_fire_fail_desc}', '${pas.msp_id}');"/>
													<input type="checkbox" name="ckra" value="${pas.msp_id}" />
												</c:when>
												<c:otherwise>
													<c:choose>
														<c:when test="${pas.msp_fire_export_flag == null}">
															<input type="button" value="VIEW" name="inputDetail"
																onclick="popWin('${path}/uw/input_fire_detail.htm?posisi=1&win=edit&msp_id='+${pas.msp_id}, 350, 450); "
																onmouseover="return overlib('Input PAS Detail', AUTOSTATUS, WRAP);" onmouseout="nd();" />
															<input type="button" name="save_btn" value="Aksep"  onclick="aksep('${pas.msp_fire_id}','${pas.msp_id}');"
																onmouseout="nd();"/>
															<input type="button" name="reject_btn" value="reject" onclick="reject_process('${pas.msp_fire_id}','${pas.msp_fire_fail_desc}','${pas.msp_id}');"/>
															<input type="checkbox" name="ckra" value="${pas.msp_id}" />
														</c:when>
														<c:otherwise>
															<input type="button" value="VIEW" name="inputDetail" disabled="disabled" />
															<input type="button" name="save_btn" value="Aksep" disabled="disabled"/>
														</c:otherwise>
													</c:choose>
												</c:otherwise>
											</c:choose>
										</td>
								</tr>
							</c:forEach>
						</tbody>
						<tr>
							<td colspan="15" style="text-align: right; padding-top: 10px;">
								<input type="button" name="reject_all_btn" value="reject all" onclick="reject_all_process();"/>
							</td>
						</tr>
					</table>
					<br>
					<input type="hidden" name="win" value="${param.win}">
					<input type="hidden" name="cari" value="${param.cari}">
					<input type="hidden" name="posisi" value="${param.posisi}">
					<input type="hidden" name="result" id="result" value="${result}" />
					<a href="akseptasi_fire.htm" id="refreshPage"></a>
					<input type="submit" name="reject" id="reject" value="reject" style="visibility: hidden;" onmouseout="nd();"/>
					<input type="submit" name="save" id="save" value="Aksep" style="visibility: hidden;" onmouseout="nd();"/>
					<input type="submit" name="resend" id="resend" value="resend" style="visibility: hidden;" onmouseout="nd();"/>
					<input type="hidden" name="action" value="" />
					<input type="hidden" name="isi_alasan" value="" />
					<input type="hidden" name="msp_id" id="msp_id" value="" />
					<div style="visibility: hidden;">
					<table class="entry2" style="background-color: pink;">
						<tr >
							<th rowspan="1">Cari:</th>
							<th class="left">
								<select name="tipe" id="tipe">
									<option value="5" <c:if test="${param.tipe eq \"5\" }">selected</c:if>>Nama</option>
									<option value="6" <c:if test="${param.tipe eq \"6\" }">selected</c:if>>No.Bukti Identitas</option>
									<option value="8" <c:if test="${param.tipe eq \"8\" }">selected</c:if>>No.HP</option>
									<option value="3" <c:if test="${param.tipe eq \"3\" }">selected</c:if>>Fire Id</option>
									<option value="4" <c:if test="${param.tipe eq \"4\" }">selected</c:if>>No.Kartu</option>
								</select>
								<select name="pilter" id="pilter">
									<option <c:if test="${\"LIKE\" eq param.pilter}">selected</c:if> value="LIKE">LIKE</option>
									<option <c:if test="${\"LT\" eq param.pilter}">selected</c:if> value="LT"><</option>
									<option <c:if test="${\"LE\" eq param.pilter}">selected</c:if> value="LE"><=</option>
									<option <c:if test="${\"EQ\" eq param.pilter}">selected</c:if> value="EQ">=</option>
									<option <c:if test="${\"GE\" eq param.pilter}">selected</c:if> value="GE">>=</option>
									<option <c:if test="${\"GT\" eq param.pilter}">selected</c:if> value="GT">></option>
								</select>		
								<input type="text" name="kata" id="kata" size="34" value="${param.kata }" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}" />
								<input type="button" name="search" id="search" value="Search" onclick="cariData();" onmouseout="nd();" />
								<input type="radio" name="jenisDist" id="jenisDist_all" value="fire" onclick="cariData();"/>			
								ALL
								<input type="radio" name="jenisDist" id="jenisDist_idv" value="fire_individu" onclick="cariData();"/>			
								INDIVIDU
								<input type="radio" name="jenisDist" id="jenisDist_dmtm" value="fire_dmtm" onclick="cariData();"/>
								DMTM
								<input type="button" name="refreshButton" onclick="document.getElementById('search').click();" style="visibility: hidden;"/>
							</th>
						</tr>
					</table>
					</div>
				</form>
			</div>
		</div>
	</div>

<c:if test="${jml eq 1}">
<script>backToParent1('${v1}', '${v2}');</script>
</c:if>
<c:if  test="${not empty successMessage}">	
			<script type="text/javascript">													
				alert('${successMessage}');	
				document.getElementById('search').click();
			</script>
	</c:if>
</body>
<%@ include file="/include/page/footer.jsp"%>