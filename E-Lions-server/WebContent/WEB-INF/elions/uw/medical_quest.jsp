<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-blue.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path}/include/js/default.js"></script> 
		<script type="text/javascript" src="${path }/include/js/jquery-1.3.2.min.js"></script>
		<script type="text/javascript"><!--
			$(document).ready(function() {
				setupPanes('container1', 'tab${cmd.showTab}');
				<c:if test="${not empty param.pesan}">
					alert('${param.pesan}');
				</c:if>
				
				
				BodyOnload();
				//$('.button').click(function(e) {
				//	if(this.id == "simpanTtg") {
				//		if(confirm("Simpan pernyataan kesehatan untuk TTG ?")) {
				//			$('#'+this.id).attr('disabled','true');
				//			$('#submitMode').val(this.id);
				//			//createLoadingMessage();
				//			$('#formpost').submit();
				//		}
				//	}
				//	else if(this.id == "simpanPp") {
				//		if(confirm("Simpan pernyataan kesehatan untuk PP ?")) {
				//			$('#'+this.id).attr('disabled','true');
				//			$('#submitMode').val(this.id);
				//			//createLoadingMessage();
				//			$('#formpost').submit();
				//		}
				//	}
				//});
				
			});
			
			function onClickPP(){
				if(confirm("Simpan pernyataan kesehatan untuk PP ?")) {
						$('#'+this.id).attr('disabled','true');
						$('#submitMode').val("simpanPp");
						//createLoadingMessage();
						$('#formpost').submit();
				}
			}
			
			function onClickTT(){
				if(confirm("Simpan pernyataan kesehatan untuk TTG ?")) {
						$('#'+this.id).attr('disabled','true');
						$('#submitMode').val("simpanTtg");
						//createLoadingMessage();
						$('#formpost').submit();
				}
			}
			
			
			function cek_kesehatan(value){
				if(value==0){
					popWin ('${path}/docs/uw/uw_sehat_guide.html',300,400);
					<c:forEach items="${cmd.lsMedQuests}" var="x" varStatus="s">
							//document.getElementById('msadm_sehat_std').disabled = false;
							//document.formpost.elements['msadm_sehat_std_ya${s.index}'].disabled = false;
							//document.formpost.elements['msadm_sehat_std_tdk${s.index}'].disabled = false;
							document.getElementById('msadm_sehat_std_ya${s.index}').disabled = false;
							document.getElementById('msadm_sehat_std_tdk${s.index}').disabled = false;
							
					</c:forEach>
				}else{
					<c:forEach items="${cmd.lsMedQuests}" var="x" varStatus="s">
							document.getElementById('msadm_sehat_std_ya${s.index}').disabled = true;
							document.getElementById('msadm_sehat_std_tdk${s.index}').disabled = true;
					</c:forEach>
				}
			}
			
			function cek_sakit(value){
				if(value==1){
					popWin ('${path}/docs/uw/uw_sakit_guide.html',400,600);
					<c:forEach items="${cmd.lsMedQuests}" var="x" varStatus="s">
							//document.formpost.elements['lsMedQuests[${s.index}].msadm_penyakit_std'].disabled = false;
							document.getElementById('msadm_penyakit_std_ya${s.index}').disabled = false;
							document.getElementById('msadm_penyakit_std_tdk${s.index}').disabled = false;
					</c:forEach>
				}else{
					<c:forEach items="${cmd.lsMedQuests}" var="x" varStatus="s">
						//document.formpost.elements['lsMedQuests[${s.index}].msadm_penyakit_std'].disabled = false;
						document.getElementById('msadm_penyakit_std_ya${s.index}').disabled = true;
						document.getElementById('msadm_penyakit_std_tdk${s.index}').disabled = true;
					</c:forEach>
				}
			}
			
			function cek_pregnant(value){
			alert(value);
				<c:forEach items="${cmd.lsMedQuests}" var="x" varStatus="s">
						if(value ==1){
							document.getElementById('msadm_pregnant_time${s.index}').disabled = false;
							document.getElementById('msadm_pregnant_time${s.index}').style.backgroundColor ='#FFFFFF';
						}else{
							document.getElementById('msadm_pregnant_time${s.index}').value="";
							document.getElementById('msadm_pregnant_time${s.index}').disabled = true;
							document.getElementById('msadm_pregnant_time${s.index}').style.backgroundColor ='#ece9d8';
						}
				</c:forEach>
				
			}
			
			function cek_usg(value){
				<c:forEach items="${cmd.lsMedQuests}" var="x" varStatus="s">
					if(value ==1){
						document.getElementById('msadm_usg_mcu_ya${s.index}').disabled = false;
						document.getElementById('msadm_usg_mcu_tdk${s.index}').disabled = false;
					}else{
						document.getElementById('msadm_usg_mcu_ya${s.index}').value = null;
						document.getElementById('msadm_usg_mcu_tdk${s.index}').value= null;
						document.getElementById('msadm_usg_mcu_ya${s.index}').disabled = true;
						document.getElementById('msadm_usg_mcu_tdk${s.index}').disabled = true;	
						//document.formpost.elements['lsMedQuests[${s.index}].msadm_usg_mcu'].disabled = true;
					}
				</c:forEach>
			}
			
			function cek_usg_std(value){
				<c:forEach items="${cmd.lsMedQuests}" var="x" varStatus="s">
					if(value ==1){
						document.getElementById('usg_mcu${s.index}').style.visibility="visible";
					}else{
						document.getElementById('usg_mcu${s.index}').style.visibility="hidden";
					}
				</c:forEach>
			}
			
			function BodyOnload(){
			<c:forEach items="${cmd.lsMedQuests}" var="x" varStatus="s">
				var msadm_sehat = document.getElementById('msadm_sehat${s.index}').value;
				if(msadm_sehat ==0){
					//document.getElementById('msadm_sehat_std').disabled = false;
					document.getElementById('msadm_sehat_std_ya${s.index}').disabled = false;
					document.getElementById('msadm_sehat_std_tdk${s.index}').disabled = false;
				}
				var msadm_penyakit = document.getElementById('msadm_penyakit${s.index}').value;
				if(msadm_penyakit ==1){
					document.getElementById('msadm_penyakit_std_ya${s.index}').disabled = false;
					document.getElementById('msadm_penyakit_std_tdk${s.index}').disabled = false;
				}
				var msadm_pregnant = document.getElementById('msadm_pregnant${s.index}').value;
				if(msadm_pregnant ==1){
					document.getElementById('msadm_pregnant_time${s.index}').disabled = false;
					document.getElementById('msadm_pregnant_time${s.index}').style.backgroundColor ='#FFFFFF';
				}
				var msadm_usg = document.getElementById('msadm_usg${s.index}').value;
				if(msadm_usg ==1){
					document.getElementById('msadm_usg_mcu_ya${s.index}').disabled = false;
					document.getElementById('msadm_usg_mcu_tdk${s.index}').disabled = false;
				}
			</c:forEach>
			}
			
		--></script>
	</head>
	<body style="height: 100%;" onload="setupPanes('container1', 'tab${cmd.showTab}');">
		<form:form id="formpost" name="formpost" commandName="cmd" >
			<div class="tab-container" id="container1">
				<ul class="tabs">
					<li>
						<a href="#" onClick="return showPane('pane1', this)" id="tab1">Calon PP</a>
						<a href="#" onClick="return showPane('pane2', this)" id="tab2">Calon TTG</a>
					</li>
				</ul>
				<div class="tab-panes">
					<c:forEach items="${cmd.lsMedQuests}" var="x" varStatus="s">
						<div id="pane${s.index+1}" class="panes">
							<fieldset>
								
								<legend>Pernyataan Kesehatan</legend>
								<table  width="100%" align="left" >
									<spring:bind path="cmd.*"> 
										<c:if test="${not empty status.errorMessages}"> 
											<div id="error" style="background-color: #B2D6AA;color: #618325;padding: 5px;"> WARNING:<br>
									            <c:forEach var="error" items="${status.errorMessages}"> - <c:out value="${error}" escapeXml="false" /> 
									            <br />
									            </c:forEach>
									        </div>
								        </c:if> 
							        </spring:bind>
									<tr style="background-color: #FFF2F2;">
										<td style="width: 300px;color: #CC0000;">
											<table width="100%" border="0">
												<tr>
													<td width="18%" style="text-align: center;color: #CC0000;">Berat Badan</td>
									    			<td width="20%" style="color: #CC0000;"><form:input id="msadm_berat" size="4" maxlength="3" path="lsMedQuests[${s.index}].msadm_berat" cssErrorClass="errField" /> kg</td>
									    			<td width="15%" style="text-align: center;color: #CC0000;">Tinggi</td>
									    			<td width="20%" style="color: #CC0000;"><form:input id="msadm_tinggi" size="4" maxlength="3" path="lsMedQuests[${s.index}].msadm_tinggi" cssErrorClass="errField" /> cm</td>
									    			<td width="10%" style="text-align: center;color: #CC0000;">EM</td>
									    			<td width="17%" style="color: #CC0000;"><form:input id="msadm_em_life" cssStyle="background-color:#ece9d8;" readonly="readonly" size="4" maxlength="3" path="lsMedQuests[${s.index}].msadm_em_life" cssErrorClass="errField" /> %</td>
												</tr>
											</table>
										</td>
										<td  colspan="4">&nbsp;</td>
									</tr>
<!--							    	<tr style="background-color: #FED9D9;">-->
<!--							    		<td style="color: #CC0000;" width="300">-->
<!--							    			Apakah berat badan anda berubah dalam 12 bulan terakhir ini ? Jika "YA", Jelaskan !-->
<!--							    		</td><br/>-->
<!--							    		<td style="text-align: left;color: #CC0000;">-->
<!--							    			<label for="msadm_berat_berubah_ya${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadm_berat_berubah" value="1" id="msadm_berat_berubah_ya${s.index}" cssClass="noBorder" />Ya</label><br/>-->
<!--							    			<label for="msadm_berat_berubah_tdk${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadm_berat_berubah" value="0" id="msadm_berat_berubah_tdk${s.index}" cssClass="noBorder" />Tidak</label>-->
<!--							    		</td>-->
<!--							    		<td width="300px" style="text-align: left;color: #CC0000;" colspan="1" >-->
<!--							    			<form:textarea  cols="60" rows="5" id="msadm_berubah_desc" path="lsMedQuests[${s.index}].msadm_berubah_desc" cssErrorClass="errField" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200);"/>-->
<!--							    		</td>-->
<!--							    		<td nowrap="nowrap" style="text-align: left;color: #CC0000;" colspan="2" >-->
<!--							    		</td>-->
<!--							    	</tr>									-->
									<tr style="background-color: #FFF2F2;">
										<td style="color: #CC0000;" width="300">
											Apakah anda sekarang dalam keadaan sehat ? Jika "TIDAK", Jelaskan !
										</td>
<!--										<th width="300px"><textarea cols="60" rows="5" readonly="readonly" class="readOnly" style="" >Apakah anda sekarang dalam keadaan sehat ? Jika "TIDAK" jelaskan !</textarea></th>-->
							    			<td width="70px" style="text-align: left;color: #CC0000;">
							    			<label for="msadm_sehat_ya${s.index}"><form:radiobutton id="msadm_sehat_ya${s.index}" path="lsMedQuests[${s.index}].msadm_sehat" value="1"  cssClass="noBorder" onclick="cek_kesehatan(1);"/>Ya</label><br/>
<!--							    			/>Ya</label><br/>-->
										    <label for="msadm_sehat_tdk${s.index}"><form:radiobutton id="msadm_sehat_tdk${s.index}" path="lsMedQuests[${s.index}].msadm_sehat" value="0"  cssClass="noBorder" onclick="cek_kesehatan(0);" />Tidak</label>
											<input type="hidden" name="msadm_sehat${s.index}" id="msadm_sehat${s.index}" value="${x.msadm_sehat}">
										</td>
										<td colspan="1">
											<form:textarea cols="60" rows="5" id="msadm_sehat_desc" path="lsMedQuests[${s.index}].msadm_sehat_desc" cssErrorClass="errField" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200);" />
										</td>
										<td nowrap="nowrap" style="text-align: left;color: #CC0000;" colspan="2" >
<!--							    			<form:checkbox disabled="disabled;" value="0" cssClass="noBorder" id="msadm_sehat_std" path="lsMedQuests[${s.index}].msadm_sehat_std" cssErrorClass="errField"  />Standar-->
							    			<label for="msadm_sehat_std_ya${s.index}"><form:radiobutton disabled="disabled;" path="lsMedQuests[${s.index}].msadm_sehat_std" value="1" id="msadm_sehat_std_ya${s.index}" cssClass="noBorder" />Standar</label><br/>
<!--							    			/>Ya</label><br/>-->
										    <label for="msadm_sehat_std_tdk${s.index}"><form:radiobutton disabled="disabled;" path="lsMedQuests[${s.index}].msadm_sehat_std" value="0" id="msadm_sehat_std_tdk${s.index}" cssClass="noBorder" />Non Standar</label>
							    		</td>
							    	</tr>
							    	<tr style="background-color: #FED9D9;">
							    		<td style="color: #CC0000;" width="300">
							    			Apakah Anda pernah atau sedang menderita penyakit atau gangguan : jantung, tekanan darah tinggi, stroke, tumor / benjolan / kanker, TBC, asma, sakit kuning / hepatitis, kencing manis, penyakit ginjal, gangguan jiwa, cacat, kelainan bawaan atau penyakit / gangguan lainnya ? Jika "YA", Jelaskan !
							    		</td>
<!--							    		<th><textarea cols="60" rows="7" readonly="readonly" class="readOnly">Apakah Anda pernah atau sedang menderita penyakit atau gangguan : jantung, tekanan darah tinggi, stroke, tumor / benjolan / kanker, TBC, asma, sakit kuning / hepatitis, kencing manis, penyakit ginjal, gangguan jiwa, cacat, kelainan bawaan atau penyakit / gangguan lainnya ? Jika YA, jelaskan !</textarea></th>-->
							    		<td style="text-align: left;color: #CC0000;">
							    			<label for="msadm_penyakit_ya${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadm_penyakit" value="1" id="msadm_penyakit_ya${s.index}" cssClass="noBorder" onclick="cek_sakit(1);" />Ya</label><br/>
							    			<label for="msadm_penyakit_tdk${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadm_penyakit" value="0" id="msadm_penyakit_tdk${s.index}" cssClass="noBorder" onclick="cek_sakit(0);"  />Tidak</label>
							    			<input type="hidden" name="msadm_penyakit${s.index}" id="msadm_penyakit${s.index}" value="${x.msadm_penyakit}">
							    		</td>
							    		<td colspan="1">
							    			<form:textarea cols="60" rows="7" id="msadm_penyakit_desc" path="lsMedQuests[${s.index}].msadm_penyakit_desc" cssErrorClass="errField" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200);"/>
							    		</td>
							    		<td nowrap="nowrap" style="text-align: left;color: #CC0000;" colspan="2" >
<!--							    			<form:checkbox disabled="disabled;" value="0" cssClass="noBorder" id="msadm_sehat_std" path="lsMedQuests[${s.index}].msadm_sehat_std" cssErrorClass="errField"  />Standar-->
							    			<label for="msadm_penyakit_std_ya${s.index}"><form:radiobutton disabled="disabled;" path="lsMedQuests[${s.index}].msadm_penyakit_std" value="1" id="msadm_penyakit_std_ya${s.index}" cssClass="noBorder" />Standar</label><br/>
<!--							    			/>Ya</label><br/>-->
										    <label for="msadm_penyakit_std_tdk${s.index}"><form:radiobutton disabled="disabled;" path="lsMedQuests[${s.index}].msadm_penyakit_std" value="0" id="msadm_penyakit_std_tdk${s.index}" cssClass="noBorder" />Non Standar</label>
							    		</td>
							    	</tr>
							    	<tr style="background-color: #FFF2F2;">
							    		<td style="color: #CC0000;" width="300">
							    			Apakah dalam 5 tahun terakhir Anda pernah dioperasi / dirawat di Rumah Sakit atau dalam masa perawatan / pengobatan yang membutuhkan obat-obatan dalam masa yang lama ? Jika "YA", Jelaskan !
							    		</td>
<!--							    		<th><textarea cols="60" rows="5" readonly="readonly" class="readOnly" >Apakah dalam 5 tahun terakhir Anda pernah dioperasi / dirawat di Rumah Sakit atau dalam masa perawatan / pengobatan yang membutuhkan obat-obatan dalam masa yang lama ? Jika YA, jelaskan !</textarea></th>-->
							    		<td style="text-align: left;color: #CC0000;">
							    			<label for="msadm_operasi_ya${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadm_operasi" value="1" id="msadm_operasi_ya${s.index}" cssClass="noBorder" />Ya</label><br/>
							    			<label for="msadm_operasi_tdk${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadm_operasi" value="0" id="msadm_operasi_tdk${s.index}" cssClass="noBorder" />Tidak</label>
							    		</td>
							    		<td colspan="3">
							    			<form:textarea cols="60" rows="5" id="msadm_operasi_desc" path="lsMedQuests[${s.index}].msadm_operasi_desc" cssErrorClass="errField" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200);"/>
							    		</td>
							    	</tr>
							    	<tr style="background-color: #FED9D9;">
							    		<td style="color: #CC0000;" width="300">
							    			Apakah dalam 5 tahun terakhir Anda pernah atau dianjurkan :<br/>
							    			A. Menjalani konsultasi / rawat inap / rawat jalan / pembedahan / biopsi / pemeriksaan laboratorium / pemeriksaan penunjang lainnya ( EKG, Treadmill, Echocardiography, USG, CT scan, MRI dsb)  atau mengkonsumsi obat-obat ? Jika "YA",  Jelaskan !<br/><br/>
							    			B. Menjalani Pengobatan Ahli Jiwa, radiasi, kemoterapi, pengobatan tradisional, pengobatan alternatif, mengalami keracunan atau kecelakaan menerima transfusi darah atau ditolan untuk donor darah? Jika "YA", Jelaskan !<br/><br/>
							    		</td>
<!--							    		<th><textarea cols="60" rows="12" readonly="readonly" class="readOnly"">Apakah dalam 5 tahun terakhir Anda pernah atau dianjurkan : -->
<!--A. Menjalani konsultasi / rawat inap / rawat jalan / pembedahan / biopsi / pemeriksaan laboratorium / pemeriksaan penunjang lainnya ( EKG, Treadmill, Echocardiography, USG, CT scan, MRI dsb)  atau mengkonsumsi obat-obat ? Jika YA,  jelaskan !-->
<!--B. Menjalani Pengobatan Ahli Jiwa, radiasi, kemoterapi, pengobatan tradisional, pengobatan alternatif, mengalami keracunan atau kecelakaan menerima transfusi darah atau ditolan untuk donor darah? Jika YA, jelaskan !</textarea>-->
<!--										</th>-->
							    		<td style="text-align: left;color: #CC0000;">
							    			<label for="msadm_medis_ya${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadm_medis" value="1" id="msadm_medis_ya${s.index}" cssClass="noBorder" />Ya</label><br/>
							    			   <label for="msadm_medis_tdk${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadm_medis" value="0" id="msadm_medis_tdk${s.index}" cssClass="noBorder" />Tidak</label><br/><br/><br/><br/><br/><br/>
							    			<label for="msadm_medis_alt_ya${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadm_medis_alt" value="1" id="msadm_medis_alt_ya${s.index}" cssClass="noBorder" />Ya</label><br/>
							    			   <label for="msadm_medis_alt_tdk${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadm_medis_alt" value="0" id="msadm_medis_alt_tdk${s.index}" cssClass="noBorder" />Tidak</label><br/>
							    		</td>
							    		<td colspan="3">
							    			<form:textarea cols="60" rows="3" id="msadm_medis_desc" path="lsMedQuests[${s.index}].msadm_medis_desc" cssErrorClass="errField" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200);"/><br/><br/><br/><br/><br/><br/>
							    			<form:textarea cols="60" rows="3" id="msadm_medis_alt_desc" path="lsMedQuests[${s.index}].msadm_medis_alt_desc" cssErrorClass="errField" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200);"/>
							    		</td>
							    	</tr>
							    	<tr style="background-color: #FFF2F2;">
							    		<td style="color: #CC0000;" width="300">
							    			<u> Keterangan Keluarga</u><br/>
							    			Apakah Ada anggota Keluarga atau Sanak Saudara yang menderita/ sedang menderita/ meninggal dunia karena penyakit? Jika "YA", jelaskan hubungan keluarga, usia, nama penyakit yang diderita.
							    		</td>
							    		<td style="text-align: left;color: #CC0000;">
							    				<label for="msadm_family_sick_ya${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadm_family_sick" value="1" id="msadm_family_sick_ya${s.index}" cssClass="noBorder" />Ya</label><br/>
							    			   	<label for="msadm_family_sick_tdk${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadm_family_sick" value="0" id="msadm_family_sick_tdk${s.index}" cssClass="noBorder" />Tidak</label><br/>
							    		</td>
							    		<td style="color: #CC0000;" colspan="3">
							    			<form:textarea cols="60" rows="3" id="msadm_family_sick_desc" path="lsMedQuests[${s.index}].msadm_family_sick_desc" cssErrorClass="errField" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200);"/><br/>
							    		</td>
							    	</tr>
							    	<tr style="background-color: #FED9D9;">
							    		<td style="color: #CC0000;" width="300">
							    			<u> KHUSUS UNTUK WANITA</u><br/>
							    			A. Apakah saat ini Anda sedang Hamil? Jika "YA", Jelaskan !<br/><br/>
							    			B. Apakah Anda pernah mengalami keguguran, aborsi atau kehamilan di luar kandungan, pernah menderita kelainan payudara, gangguan menstruasi, endometriosis, gangguan/penyakit saat melahirkan maupun kelainan alat reproduksi lainnya? Jika "YA", Jelaskan !<br/><br/>
							    			C. Apakah Anda pernah melakukan pemeriksaan USG kandungan, pap smear, mamografi? Jika "YA", Jelaskan !<br/>
							    		</td>
							    		<td style="text-align: left;color: #CC0000;">
							    			<label for="msadm_pregnant_ya${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadm_pregnant" value="1" id="msadm_pregnant_ya${s.index}" cssClass="noBorder" onclick="cek_pregnant(1);" />Ya</label><br/>
							    			<label for="msadm_pregnant_tdk${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadm_pregnant" value="0" id="msadm_pregnant_tdk${s.index}" cssClass="noBorder" onclick="cek_pregnant(0);" />Tidak</label><br/><br/>
							    			<label for="msadm_abortion_ya${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadm_abortion" value="1" id="msadm_abortion_ya${s.index}" cssClass="noBorder" />Ya</label><br/>
							    			<label for="msadm_abortion_tdk${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadm_abortion" value="0" id="msadm_abortion_tdk${s.index}" cssClass="noBorder" />Tidak</label><br/><br/><br/><br/>
							    			<label for="msadm_usg_ya${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadm_usg" value="1" id="msadm_usg_ya${s.index}" cssClass="noBorder" onclick="cek_usg(1);" />Ya</label><br/>
							    			<label for="msadm_usg_tdk${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadm_usg" value="0" id="msadm_usg_tdk${s.index}" cssClass="noBorder" onclick="cek_usg(0);" />Tidak</label><br/>
							    			<input type="hidden" name="msadm_pregnant${s.index}" id="msadm_pregnant${s.index}" value="${x.msadm_pregnant}">
							    			<input type="hidden" name="msadm_usg${s.index}" id="msadm_usg${s.index}" value="${x.msadm_usg}">
							    		</td>
							    		<td style="color: #CC0000;" colspan="1">
							    			<form:textarea cols="60" rows="3" id="msadm_pregnant_desc" path="lsMedQuests[${s.index}].msadm_pregnant_desc" cssErrorClass="errField" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200);"/><br/><br/>
							    			<form:textarea cols="60" rows="3" id="msadm_abortion_desc" path="lsMedQuests[${s.index}].msadm_abortion_desc" cssErrorClass="errField" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200);"/><br/><br/><br/><br/>
							    			<form:textarea cols="60" rows="3" id="msadm_usg_desc" path="lsMedQuests[${s.index}].msadm_usg_desc" cssErrorClass="errField" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200);"/>
							    		</td>
							    		<td valign="top" width="180px;" style="color: #CC0000;" colspan="1">
							    			<br/>Lama Kehamilan <form:input disabled="disabled;"  cssStyle="background-color:#ece9d8;" id="msadm_pregnant_time${s.index}" size="4" maxlength="3" path="lsMedQuests[${s.index}].msadm_pregnant_time" cssErrorClass="errField" /> Minggu<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
							    			Pemeriksaan dilakukan Untuk :<br/>
							    			<label for="msadm_usg_mcu_ya${s.index}"><form:radiobutton disabled="disabled" path="lsMedQuests[${s.index}].msadm_usg_mcu" value="1" id="msadm_usg_mcu_ya${s.index}" cssClass="noBorder" onclick="cek_usg_std(1)" />MCU(MEDICAL CHECK UP)</label><br/>
							    			<label for="msadm_usg_mcu_tdk${s.index}"><form:radiobutton disabled="disabled" path="lsMedQuests[${s.index}].msadm_usg_mcu" value="0" id="msadm_usg_mcu_tdk${s.index}" cssClass="noBorder" onclick="cek_usg_std(0)"  />Karena Hal Lain</label><br/><br/>
							    			<input type="hidden" name="msadm_usg_mcu${s.index}" id="msadm_usg_mcu${s.index}" value="${x.msadm_usg_mcu}">
							    		</td>
								    		<td valign="bottom" style="color: #CC0000;" colspan="1">
								    		<div id="usg_mcu${s.index}" style="visibility: hidden;">
								    			Hasil MCU?<br/>
								    			<label for="msadm_usg_mcu_std_ya${s.index}"><form:radiobutton  path="lsMedQuests[${s.index}].msadm_usg_mcu_std" value="1" id="msadm_usg_mcu_std_ya${s.index}" cssClass="noBorder"  />Baik</label><br/>
								    			<label for="msadm_usg_mcu_std_tdk${s.index}"><form:radiobutton  path="lsMedQuests[${s.index}].msadm_usg_mcu_std" value="0" id="msadm_usg_mcu_std_tdk${s.index}" cssClass="noBorder"  />Ada Kelainan</label><br/><br/>
								    		</div>
								    		</td>
							    		
							    	</tr>
<!--							    	<tr>-->
<!--							    		<th>-->
<!--							    		<textarea cols="60" rows="8" readonly="readonly" class="readOnly">KHUSUS UNTUK WANITA-->
<!--Apakah dalam 5 tahun terakhir Anda pernah atau dianjurkan : -->
<!--Menjalani konsultasi / rawat inap / rawat jalan / pembedahan / biopsi / pemeriksaan laboratorium / pemeriksaan penunjang lainnya ( EKG, Treadmill, Echocardiography, USG, CT scan, MRI dsb)  atau mengkonsumsi obat-obat ? Jika YA,  jelaskan !</textarea>-->
<!--										</th>-->
<!--										<th style="text-align: left;">-->
<!--							    			<label for="msadm_pregnant_ya${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadm_pregnant" value="1" id="msadm_pregnant_ya${s.index}" cssClass="noBorder" />Ya</label><br/>-->
<!--							    			<label for="msadm_pregnant_tdk${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadm_pregnant" value="0" id="msadm_pregnant_tdk${s.index}" cssClass="noBorder" />Tidak</label>-->
<!--							    		</th>-->
<!--							    		<th>-->
<!--							    			<form:textarea cols="60" rows="8" id="msadm_pregnant_desc" path="lsMedQuests[${s.index}].msadm_pregnant_desc" cssErrorClass="errField" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200);"/>-->
<!--							    		</th>-->
<!--							    	</tr>-->
								</table>
							</fieldset>
							<fieldset>
								<legend>Kegiatan & Kebiasaan Pribadi</legend>
								<table width="100%">
									<tr style="background-color: #FFF2F2;">
										<td style="color: #CC0000;" width="300px">
							    			Apakah Anda melakukan atau mempunyai kegemaran yang berisiko tinggi ? ( seperti balap mobil, scuba diving/menyelam, panjat tebing, terbang layang, berkuda, ski  es, mendaki gunung, bungee jumping dsb.?<br/><br/>
							    		</td>
<!--										<th width="300px"><textarea cols="60" rows="5" readonly="readonly" class="readOnly" style="" >Apakah Anda melakukan atau mempunyai kegemaran yang berisiko tinggi ? ( seperti balap mobil, scuba diving/menyelam, panjat tebing, terbang layang, berkuda, ski  es, mendaki gunung, bungee jumping dsb.?</textarea></th>-->
							    		<td style="color: #CC0000;" width="70px" style="text-align: left;">
							    			<label for="msadd_hobby_ya${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadd_hobby" value="1" id="msadd_hobby_ya${s.index}" cssClass="noBorder" />Ya</label><br/>
										    <label for="msadd_hobby_tdk${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadd_hobby" value="0" id="msadd_hobby_tdk${s.index}" cssClass="noBorder" />Tidak</label>
										</td>
										<td colspan="3">&nbsp;</td>
							    	</tr>
							    	<tr style="background-color: #FED9D9;">
							    		<td style="color: #CC0000;"  width="300px">
							    			Apakah Anda biasa melakuakn perjalanan / sebagai penumpang pada penerbangan yang tidak terjadwal tetap ( unregular schedule airline / charter flight ) ?<br/><br/>
							    		</td>
<!--							    		<th><textarea cols="60" rows="5" readonly="readonly" class="readOnly" >Apakah Anda biasa melakuakn perjalanan / sebagai penumpang pada penerbangan yang tidak terjadwal tetap ( unregular schedule airline / charter flight ) ?</textarea></th>-->
							    		<td style="text-align: left;color: #CC0000;">
							    			<label for="msadd_flight_ya${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadd_flight" value="1" id="msadd_flight_ya${s.index}" cssClass="noBorder" />Ya</label><br/>
							    			<label for="msadd_flight_tdk${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadd_flight" value="0" id="msadd_flight_tdk${s.index}" cssClass="noBorder" />Tidak</label>
							    		</td>
							    		<td colspan="3">&nbsp;</td>
							    	</tr>
							    	<tr style="background-color: #FFF2F2;">
							    		<td style="color: #CC0000;" width="300px">
							    			Apakah Anda mempunyai kebiasaan-kebiasaaan ini ?<br/>
							    			A. Merokok. Jika YA, berapa batang / hari ?<br/><br/>
							    			B. Minum minuman beralkohol. Jika YA, sebutkan jenisnya, jumlah konsumsi per hari, berapa lama sudah mengkonsumsi.<br/><br/>
							    			C. Menggunakan narkotika/ obat terlarang/ obat penenang (sebutkan nama obat, jumlah konsumsi, dan berapa lama konsumsinya)<br/><br/>
							    		</td>
<!--							    		<th rowspan="2"><textarea cols="60" rows="7" readonly="readonly" class="readOnly" >Apakah Anda mempunyai kebiasaan-kebiasaaan ini ? -->
<!--A. Merokok. Jika YA, berapa batang / hari ?-->
<!--B. Minum minuman beralkohol. Jika YA, sebutkan jenisnya, jumlah konsumsi per hari, berapa lama sudah mengkonsumsi.</textarea>-->
<!--										</th>-->
							    		<td style="text-align: left;color: #CC0000;">
							    			<label for="msadd_smoke_ya${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadd_smoke" value="1" id="msadd_smoke_ya${s.index}" cssClass="noBorder" />Ya</label><br/>
							    			<label for="msadd_smoke_tdk${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadd_smoke" value="0" id="msadd_smoke_tdk${s.index}" cssClass="noBorder" />Tidak</label><br/>
							    			<label for="msadd_drink_beer_ya${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadd_drink_beer" value="1" id="msadd_drink_beer_ya${s.index}" cssClass="noBorder" />Ya</label><br/>
							    			<label for="msadd_drink_beer_tdk${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadd_drink_beer" value="0" id="msadd_drink_beer_tdk${s.index}" cssClass="noBorder" />Tidak</label><br/><br/>
							    			<label for="msadd_drugs_ya${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadd_drugs" value="1" id="msadd_drugs_ya${s.index}" cssClass="noBorder" />Ya</label><br/>
							    			<label for="msadd_drugs_tdk${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadd_drugs" value="0" id="msadd_drugs_tdk${s.index}" cssClass="noBorder" />Tidak</label>
							    			
							    		</td>
							    		<td style="color: #CC0000;" colspan="3">
							    			<form:input id="nsadd_many_cig" size="4" maxlength="3" path="lsMedQuests[${s.index}].nsadd_many_cig" cssErrorClass="errField" /> batang/hari<br/><br/><br/>
							    			<form:textarea cols="60" rows="3" id="msadd_drink_beer_desc" path="lsMedQuests[${s.index}].msadd_drink_beer_desc" cssErrorClass="errField" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200);"/><br/><br/>
							    			<form:textarea cols="60" rows="3" id="msadd_reason_drugs" path="lsMedQuests[${s.index}].msadd_reason_drugs" cssErrorClass="errField" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200);"/>
							    		</td>
							    	</tr>
							    	<tr style="background-color: #FED9D9;">
							    		<td style="color: #CC0000;" width="300px">
							    			Apakah Anda sudah mempunyai atau sedang mengajukan Polis Asuransi Jiwa / Kecelakaan / Kesehatan baik di PT. Asuransi Jiwa Sinarmas MSIG maupun perusahaan asuransi lainnya ? Jika YA, sebutkan Perusahaan Asuransi mana, jenis asuransi dan tarif premi yang dikenakan (standar/ekstra premi/ditolak/ditunda) dan alasannya.<br/>
							    		</td>
<!--							    		<th><textarea cols="60" rows="8" readonly="readonly" class="readOnly">Apakah Anda sudah mempunyai atau sedang mengajukan Polis Asuransi Jiwa / Kecelakaan / Kesehatan baik di PT. Asuransi Jiwa Sinarmas MSIG maupun perusahaan asuransi lainnya ? Jika YA, sebutkan Perusahaan Asuransi mana, jenis asuransi dan tarif premi yang dikenakan (standar/ekstra premi/ditolak/ditunda) dan alasannya.</textarea></th>-->
							    		<td style="text-align: left;color: #CC0000;">
							    			<label for="msadd_life_ins_ya${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadd_life_ins" value="1" id="msadd_life_ins_ya${s.index}" cssClass="noBorder" />Ya</label><br/>
							    			<label for="msadd_life_ins_tdk${s.index}"><form:radiobutton path="lsMedQuests[${s.index}].msadd_life_ins" value="0" id="msadd_life_ins_tdk${s.index}" cssClass="noBorder" />Tidak</label>
							    		</td>
							    		<td><br><br>
							    		<br></td>
							    	</tr>
								</table>								
							</fieldset>		
							<br/>
							<c:choose>
								<c:when test="${s.index eq 0}"><input type="button" class="button" id="simpanPp" value="Simpan PP" onclick="onClickPP();" style="width:100px;"></c:when>
								<c:otherwise><input type="submit" class="button" id="simpanTtg" value="Simpan TTG" onclick="onClickTT();" style="width:100px;"></c:otherwise>
							</c:choose>					
						</div>
					</c:forEach>
				</div>
			</div>
			<input type="hidden" name="submitMode" id="submitMode">	
			<form:hidden path="reg_spaj"/>			
		</form:form>
	</body>
</html>
