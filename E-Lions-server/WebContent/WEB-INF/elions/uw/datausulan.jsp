<%@ include file="/include/page/header.jsp"%>
<body>

 <fieldset>
	   <legend>Data Usulan Detail</legend>
		   <table class="entry">
		   <tr>
		   		<th>Produk</th>
		   		<td>:</td>
		   		<td width=400>${detail.LSDBS_NAME }</td>
		   </tr>

		   <tr>
		   		<th>Sum Insured</th>
		   		<td>:</td>
		   		<td width=250>
			   		<c:forEach var="s" items="${lsKurs}" varStatus="xt">
							<c:if test="${detail.LKU_ID eq s.LKU_ID }">${s.LKU_SYMBOL}</c:if>
			   		</c:forEach>
			   		<fmt:formatNumber value="${detail.MSPR_TSI}" type="currency" currencySymbol=""/>	
			   	</td>
		   		<th width="160">Premium </th>
		   		<td>:</td>
		   		<td width=250>
					<c:forEach var="s" items="${lsKurs}" varStatus="xt">
							<c:if test="${detail.LKU_ID eq s.LKU_ID }">${s.LKU_SYMBOL}</c:if>
			   		</c:forEach>
			   		<fmt:formatNumber value="${detail.MSPR_PREMIUM}" type="currency" currencySymbol=""/>	
		   </tr>
				   <tr>
		   		<th>Lama Pembayaran</th>
				<td>:</td>
				<td>${detail.MSPO_PAY_PERIOD} Tahun</td>
				<th>Cara Bayar</th>
				<td>:</td>
				<td>
				<c:forEach var="s" items="${lsCaraBayar}" varStatus="xt">
						<c:if test="${detail.LSCB_ID eq s.LSCB_ID }">${s.LSCB_PAY_MODE}</c:if>
		   		</c:forEach>
		   		</td>
		   </tr>
		
		   <tr>
		   		<th>Medis</th>
		   		<td>:</td>
		   		<td width=250>
		   			${detail.MSTE_MEDICAL}
		   			<c:choose>
						<c:when test="${detail.MSTE_MEDICAL eq 0 }">
						Tidak
						</c:when>
						<c:when test="${detail.MSTE_MEDICAL eq 1 }">
						Ya
						</c:when>
					</c:choose>
		   		</td>
		   		<th>Begin Date (dd-mm-yyyy)</th>
		   		<td>:</td>
		   		<td width=250>
		   			<fmt:formatDate value="${detail.MSPR_BEG_DATE }" pattern="dd-MM-yyyy"/>
		   		</td>
				<th>End Date (dd-mm-yyyy)</th>
		   		<td>:</td>
		   		<td width=250>
		   				<fmt:formatDate value="${detail.MSPR_END_DATE }" pattern="dd-MM-yyyy"/>
		   		</td>
		   </tr>
		   <tr>
		   		<th>Masa Pertanggungan</th>
				<td>:</td>
				<td>${detail.MSPO_PAY_PERIOD } Tahun</td>
				<th>Auto Debet</th>
				<td>:</td>
				<td>
				<c:forEach var="s" items="${lsAutoDebet}" varStatus="xt">
						<c:if test="${detail.MSTE_FLAG_CC eq s.ID }">${s.VALUE}</c:if>
		   		</c:forEach>
		   		</td>
				<th width="180">Tanggal Debet (dd-mm-yyyy)</th>
				<td>:</td>
				<td>
	   				<fmt:formatDate value="${detail.MSTE_TGL_RECUR }" pattern="dd-MM-yyyy"/>
				</td>
		   </tr>
		   <tr>
		   		<th>Alamat Penagihan</th>
				<td>:</td>
				<td>${detail.MSAP_ADDRESS }</td>
				<th>Kota</th>
				<td>:</td>
				<td>${detail.KOTA }</td>
				<th>Kode Pos</th>
				<td>:</td>
				<td>${detail.MSAP_ZIP_CODE }</td>
		   </tr>
		   <tr>
		   		<th>No Telepon</th>
		   		<td>:</td>
		   		<td>(${detail.MSAP_AREA_CODE1 }) ${detail.MSAP_PHONE1 }</td>
		   		<th>No Fax</th>
		   		<td>:</td>
		   		<td>(${detail.MSAP_AREA_CODE_FAX1 }) ${detail.MSAP_FAX1 }</td>
		   		<th>No HandPhone</th>
		   		<td>:</td>
		   		<td>${detail.NO_HP }</td>
		   </tr>
		   <tr>
		   		<td></td>
		   		<td></td>
		   		<td>(${detail.MSAP_AREA_CODE2}) ${detail.MSAP_PHONE2 }</td>
		   		<td></td>
		   		<td></td>
		   		<td>(${detail.MSAP_AREA_CODE_FAX2 }) ${detail.MSAP_FAX2 }</td>
		   		<td></td>
		   		<td></td>
		   		<td></td>
		   </tr>
		   <tr>
		   		<td></td>
		   		<td></td>
		   		<td>(${detail.MSAP_AREA_CODE3}) ${detail.MSAP_PHONE3 }</td>
		   		<td></td>
		   		<td></td>
		   		<td></td>
		   		<td></td>
		   		<td></td>
		   		<td></td>
		   </tr>
		   </table>
	   </fieldset>

<c:if test="${not empty productInsured}">
	<fieldset>
	<legend>Premi Asuransi</legend>
	<display:table id="produk" style="width:850px; " name="productInsured" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator">
		<display:column property="LSDBS_NAME" title="Produk" />
		<display:column property="MSPR_TSI" title="Uang Pertanggungan" format="{0, number, #,##0.00;(#,##0.00)}" />
		<display:column property="MSPR_EXTRA" title="EM" format="{0, number, #,##0.00;(#,##0.00)}" />
		<display:column property="MSPR_RATE" title="Rate" format="{0, number, #,##0.00;(#,##0.00)}" />
		<display:column property="TOTAL5" title="Premium" format="{0, number, #,##0.00;(#,##0.00)}" total="true" />
	    <display:column title="Discount" >
	    	<elions:currency jumlah="${produk.DISC}" /><br/>
	    	<elions:currency jumlah="${produk.MSPR_DISCOUNT}" />
	    </display:column>
		<display:column property="TOTAL7" title="Total" format="{0, number, #,##0.00;(#,##0.00)}" total="true"/>
		<display:column title="%">
			<input class="noBorder" type="checkbox" disabled <c:if test="${produk.MSPR_PERSEN eq 1}" >checked</c:if>>
		</display:column>
		<display:column title="Ins Period">
			${produk.MSPR_INS_PERIOD} Y
			</display:column>
			<display:column property="MSPR_END_DATE" title="End Date" format="{0, date, dd/MM/yyyy}" />
		</display:table>
		<br/>
		
	</fieldset>
</c:if>
<c:if test="${not empty daftar}">
	<fieldset>
		<legend>Penerima Manfaat</legend>
			<display:table id="baris" name="daftar" class="simple" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="" export="true" pagesize="20">   
			<display:column property="REG_SPAJ" title="REG_SPAJ"  sortable="true"/>                                                                                      
			<display:column property="MSTE_INSURED_NO" title="MSTE_INSURED_NO"  sortable="true"/>                                                                        
			<display:column property="MSAW_NUMBER" title="MSAW_NUMBER"  sortable="true"/>                                                                                
			<display:column property="MSAW_FIRST" title="MSAW_FIRST"  sortable="true"/>                                                                                  
			<display:column property="MSAW_MIDDLE" title="MSAW_MIDDLE"  sortable="true"/>                                                                                
			<display:column property="MSAW_LAST" title="MSAW_LAST"  sortable="true"/>                                                                                    
			<display:column property="MSAW_BIRTH" title="MSAW_BIRTH" format="{0, date, dd/MM/yyyy}"  sortable="true"/>                                                   
			<display:column property="LSRE_ID" title="LSRE_ID"  sortable="true"/>                                                                                        
			<display:column property="MSAW_PERSEN" title="MSAW_PERSEN" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                      
			<display:column property="LSRE_RELATION" title="LSRE_RELATION"  sortable="true"/>                                                                            
			</display:table>                                                                                                                                             
	</fieldset>
</c:if>
<table align="center">
	<tr>
		<td>
			<input type="Button" name="btnClose" value="Close" title="Close" onClick="javascript:window.close();">
		</td>	
	</tr>
</table>	
</body>

<%@ include file="/include/page/footer.jsp"%>