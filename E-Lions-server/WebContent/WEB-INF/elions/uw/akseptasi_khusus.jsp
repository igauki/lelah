<%@ include file="/include/page/header.jsp"%>
<script>
	
	function cek(spaj){
		if(spaj == ''){
			alert('Harap pilih SPAJ terlebih dahulu!');
			return false;
		}else{
			return true;
		}
	}
	
	function buttonLink(b,spaj){
		if(b=='u'){
			if(cek(spaj))
			if(confirm("Anda Yakin ingin Update Status Polis ini ("+spaj+") Menjadi ACCEPTED?")){
				desc=prompt("Masukan Keterangan Untuk Akseptasi Khusus..","");
				if(desc == null){
				alert("Update Status dibatalkan");
				}else if(desc != ''){
					document.getElementById('infoFrame').src='${path }/uw/uw.htm?window=update_akseptasi&spaj='+spaj+'&desc='+desc;
				}else{
					alert("Update Status dibatalkan");
				}
			}
			
		}else if(b=='r'){
			document.getElementById('infoFrame').src='${path }/uw/uw.htm?window=report_akseptasi_khusus';
		}else if(b=='k'){
			document.getElementById('infoFrame').src='${path }/report/uw.htm?window=akseptasiPendingKomisi';
		}else if(b=='f'){
			document.getElementById('infoFrame').src='${path }/report/uw.htm?window=reportFurtherRequirement';
		}else if(b=='p'){
			document.getElementById('infoFrame').src='${path }/report/uw.htm?window=postponed';
		}else if(b=='a'){
			document.getElementById('infoFrame').src='${path }/report/uw.htm?window=reportFurtherRBanca';
		}else if(b=='e'){
		    if (cek(spaj))
		   // document.getElementById('infoFrame').src='${path}/uw/uw.htm?window=editblanko&spaj='+spaj;
		    //document.getElementById('infoFrame').src='${path}/uw/endorseNonMaterial.htm?spaj='+spaj;
		    document.getElementById('infoFrame').src='${path}/uw/endorsenonmaterial.htm?window=endorseNonMaterial&spaj='+spaj;
		}else if(b=='d'){
			document.getElementById('infoFrame').src='${path }/report/uw.htm?window=reportDecline';
		}else if(b=='c'){
			if(cek(spaj))
			document.getElementById('infoFrame').src='${path}/checklist.htm?reg_spaj='+spaj;
		}else if(b=='s') {
			if(cek(spaj))
			document.getElementById('infoFrame').src='${path}/uw/status.htm?spaj='+spaj;
		}else if(b=='call') {
			if(cek(spaj))popWin('${path}/uw/viewer.htm?window=csfcall&spaj='+spaj, 480, 640); 
		}else if(b=='summary') {
			if(cek(spaj))popWin('${path}/report/bas.htm?window=csf_summary&spaj='+spaj, 700, 900); 
		}else if(b=='agen'){
		    if(cek(spaj))popWin('${path}/bac/editagenpenutup.htm?spaj='+spaj, 360, 480);
		}else if(b=='i'){
			if(cek(spaj))
			if(document.getElementById('infoFrame')){
				document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj;
				document.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+spaj;
			}
		}else if(b=='y') {
			if(cek(spaj))
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=uwinfo&spaj='+spaj;
		}else if(b=='en') {
			if(cek(spaj))
			document.getElementById('infoFrame').src='${path}/uw/endorsenonmaterial.htm?window=editklarifikasi&spaj='+spaj;
		}else if(b=='upload') {
			if(cek(spaj))
			document.getElementById('infoFrame').src='${path}/uw/upload_nb.htm?reg_spaj='+spaj;
		}else if(b=='list_rk'){
			document.getElementById('infoFrame').src= '${path}/uw/uw.htm?window=drek&piyu=1&startDate=${startDate}&endDate=${endDate}';
		}
	}
	
	function awal(){
		setFrameSize('infoFrame', 45);
		setFrameSize('docFrame', 45);
	}
	 //onload="awal();"
	
</script>
<body onload="setFrameSize('infoFrame', 90); setFrameSize('docFrame', 90);" 
	onresize="setFrameSize('infoFrame', 90); setFrameSize('docFrame', 90);" style="height: 100%;">
	
	<form name="formpost" method="post">
		<div class="tabcontent">
			<table class="entry2" style="width: 98%;">
				<tr>
					<th>SPAJ</th>
					<td>
						<input type="hidden" name="koderegion">
						<input type="hidden" name="kodebisnis">
						<input type="hidden" name="numberbisnis">
						<input type="hidden" name="jml_peserta">
						<!--
						<select name="spaj" onchange="ajaxPesan(this.value, 6); tampilkan('info', this.value);">
						-->
						<select name="spaj">
							<option value="">[--- Silahkan Pilih/Cari SPAJ ---]</option>
							<c:forEach var="s" items="${daftarSPAJ}">
								<option value="${s.REG_SPAJ }"
									style="background-color: ${s.BG}"
									<c:if test="${s.REG_SPAJ eq param.spaj }">selected<c:set var="ada" value="ada" /></c:if>>
									${s.SPAJ_FORMATTED} - ${s.POLICY_FORMATTED }
								</option>
							</c:forEach>
						</select>

						<input type="button" value="Info" name="info"
							accesskey="I"
							onmouseover="return overlib('Alt-I', AUTOSTATUS, WRAP);"
							onmouseout="nd();"
							onclick="buttonLink('i',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
						<input type="button" value="Cari" name="search"
							onclick="popWin('${path}/uw/spaj.htm?posisi=2,6&win=uw&lssaId=10', 350, 450); "
							accesskey="C"
							onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);"
							onmouseout="nd();">
						<input type="button" value="Update Status" name="update"
							onclick="buttonLink('u',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);"
							accesskey=U
							"onmouseover="return overlib('Alt-U', AUTOSTATUS, WRAP);"
							onmouseout="nd();">
						<input type="button" value="Checklist" name="checklist"
							onclick="buttonLink('c',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
						
						<input type="button" value="U/W Info" name="uwinfo" 
							onclick="buttonLink('y',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">

						<input type="button" value="Status" name="statusss" 
							onclick="buttonLink('s',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">

                         <input type="button" value="Call" name="call"
                            onclick="buttonLink('call',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
                
                        <input type="button" value="Summary" name="summary"
                            onclick="buttonLink('summary',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
                        
                        <input type="button" value="Edit Agen" name="agen"
                            onclick="buttonLink('agen',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
                        
                       <input type="button" value="Endorse Non Material" name="endorse"
                            accesskey="E"
                            onmouseover="return overlib('Alt-E', AUTOSTATUS, WRAP);"
                            onmouseout="nd();"
                           onclick="buttonLink('e',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
                           
					</td>
				</tr>
				<tr>
					<th>Summary</th>
					<td>
						<input type="hidden" name="koderegion">
						<input type="hidden" name="koderegion">
						<input type="hidden" name="koderegion">
						<input type="hidden" name="koderegion">
						<input type="button" value="Akseptasi Khusus" name="report"
							onclick="buttonLink('r',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);"
							accesskey="R"
							onmouseover="return overlib('Alt-R', AUTOSTATUS, WRAP);"
							onmouseout="nd();">
						<input type="button" value="Pending Komisi"
							name="report"
							onclick="buttonLink('k',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);"
							accesskey="K"
							onmouseover="return overlib('Alt-K', AUTOSTATUS, WRAP);"
							onmouseout="nd();">
						<input type="button" value="Further Req" name="report"
							onclick="buttonLink('f',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);"
							accesskey="F"
							onmouseover="return overlib('Alt-F', AUTOSTATUS, WRAP);"
							onmouseout="nd();">
						<input type="button" value="Further Req (Bancass)" name="report"
							onclick="buttonLink('a',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);"
							accesskey="R"
							onmouseover="return overlib('Alt-A', AUTOSTATUS, WRAP);"
							onmouseout="nd();">
						<input type="button" value="Postponed" name="report"
							onclick="buttonLink('p',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);"
							accesskey="B"
							onmouseover="return overlib('Alt-B', AUTOSTATUS, WRAP);"
							onmouseout="nd();">
						<input type="button" value="Decline" name="report"
							onclick="buttonLink('d',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);"
							accesskey="D"
							onmouseover="return overlib('Alt-D', AUTOSTATUS, WRAP);"
							onmouseout="nd();">
						<input type="button" value="Endorse" name="endorsement"
						onclick="buttonLink('en',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">								
						<input type="button"  value="Upload Scan" name="upload_nb"
						onclick="buttonLink('upload',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);">
						<input type="button" value="List RK" name="list_rk"
							onclick="buttonLink('list_rk',document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value);" 
							accesskey="R"
							onmouseover="return overlib('Alt-R', AUTOSTATUS, WRAP);"
							onmouseout="nd();">
					</td>
				</tr>
				<tr>
					<td colspan="2">
					<table style="margin: 0 0 0 0; padding: 0 0 0 0; width: 100%;">
						<tr>
					
					<c:choose>
						<c:when test="${not empty wideScreen}">
							<td style="width: 60%;">
								<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
									width="100%" height="100%">
									Please Wait...
								</iframe>
							</td>
							<td style="width: 40%;">
								<iframe src="" name="docFrame" id="docFrame" width="100%">E-Lions</iframe>
							</td>
						</c:when>
						<c:otherwise>
							<td>
								<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
									width="100%" height="100%">
									Please Wait...
								</iframe>
							</td>
						</c:otherwise>
					</c:choose>
						</tr>
					</table>
					</td>
				</tr>
			</table>
		</div>
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>