<%@ include file="/include/page/header.jsp"%>
	<script>
		function backToParent(id, desc){
			var parentForm = self.opener.document.forms['formpost'];
			var no = document.getElementById("no").value;
			var indx = document.getElementById("indx").value;
			var sub = document.getElementById("sub").value;
			//alert(sub);
			if(parentForm) {
				/*if(indx == "" && no == -1) {
					parentForm.elements['nonMedisIcdId'].value = id;
					parentForm.elements['nonMedisIcdDesc'].value = desc;
				}
				else*/ if(indx != "" && no == -1) {
					parentForm.elements[sub+'_lic_id'+indx].value = id;
					parentForm.elements[sub+'_lic_desc'+indx].value = desc;
				}
				else if(indx != "" && no > -1) {
					parentForm.elements[sub+indx+'_lic_id'+no].value = id;
					parentForm.elements[sub+indx+'_lic_desc'+no].value = desc;	
				}
			}
			window.close();	
		}
		
		function klasifikasi(selection) {
			document.getElementById('klasifikasi').value = selection;
			document.getElementById('formpost').submit();
		}
	</script>	
	<body onload="document.title='PopUp :: Cari ICD'; document.formpost.data.focus(); ">
		<form method="post" id="formpost" name="formpost" onsubmit="createLoadingMessage();">
			<fieldset>
				<legend>Search Key</legend>
				<table class="entry2" border="0">
					<tr align="left">
						<th width="20"><input type="text" id="icdId" name="icdId" size="8"></th>
						<th width="360"><input type="text" id="icdDesc" name="icdDesc" style="width: 100%"></th>
						<th width="115">
							<!--<label for="type9"><input type="radio" id="type9" value="9" name="icdType" title="ICD 9">ICD 9</label>-->
							<label for="type10"><input type="radio" id="type10" value="10" name="icdType" title="ICD 10" checked="checked">ICD 10</label>
						</th>
						<th><input type="submit" name="btnCari" value="Search"></th>
					</tr>
				</table>
			</fieldset>
			<fieldset>
				<legend>Klasifikasi Penyakit</legend>
				<table class="entry2">
					<tr>
						<th width="90"><a href="javascript:klasifikasi('A-B99')">A00-B99</a></th>
						<td>Certain infectious and parasitic diseases</td>
					</tr>
					<tr>
						<th><a href="javascript:klasifikasi('C-D48')">C00-D48</a></th>
						<td>Neoplasms</td>
					</tr>
					<tr>
						<th><a href="javascript:klasifikasi('D50-D89')">D50-D89</a></th>
						<td>Diseases of the blood and blood-forming organs and certain disorders involving the immune mechanism</td>
					</tr>
					<tr>
						<th><a href="javascript:klasifikasi('E-E90')">E00-E90</a></th>
						<td>Endocrine, nutritional and metabolic diseases</td>
					</tr>
					<tr>
						<th><a href="javascript:klasifikasi('F-F99')">F00-F99</a></th>
						<td>Mental and behavioural disorders</td>
					</tr>
					<tr>
						<th><a href="javascript:klasifikasi('G-G99')">G00-G99</a></th>
						<td>Diseases of the nervous system</td>
					</tr>
					<tr>
						<th><a href="javascript:klasifikasi('H-H59')">H00-H59</a></th>
						<td>Diseases of the eye and adnexa</td>
					</tr>
					<tr>
						<th><a href="javascript:klasifikasi('H60-H95')">H60-H95</a></th>
						<td>Diseases of the ear and mastoid process</td>
					</tr>
					<tr>
						<th><a href="javascript:klasifikasi('I-I99')">I00-I99</a></th>
						<td>Diseases of the circulatory system</td>
					</tr>
					<tr>
						<th><a href="javascript:klasifikasi('J-J99')">J00-J99</a></th>
						<td>Diseases of the respiratory system</td>
					</tr>
					<tr>
						<th><a href="javascript:klasifikasi('K-K93')">K00-K93</a></th>
						<td>Diseases of the digestive system;</td>
					</tr>
					<tr>
						<th><a href="javascript:klasifikasi('L-L99')">L00-L99</a></th>
						<td>Diseases of the skin and subcutaneous tissue</td>
					</tr>
					<tr>
						<th><a href="javascript:klasifikasi('M-M99')">M00-M99</a></th>
						<td>Diseases of the musculoskeletal system and connective tissue</td>
					</tr>
					<tr>
						<th><a href="javascript:klasifikasi('N-N99')">N00-N99</a></th>
						<td>Diseases of the genitourinary system</td>
					</tr>
					<tr>
						<th><a href="javascript:klasifikasi('O-O99')">O00-O99</a></th>
						<td>Pregnancy, childbirth and the puerperium</td>
					</tr>
					<tr>
						<th><a href="javascript:klasifikasi('P-P96')">P00-P96</a></th>
						<td>Certain conditions originating in the perinatal period</td>
					</tr>
					<tr>
						<th><a href="javascript:klasifikasi('Q-Q99')">Q00-Q99</a></th>
						<td>Congenital malformations, deformations and chromosomal abnormalities</td>
					</tr>
					<tr>
						<th><a href="javascript:klasifikasi('R-R99')">R00-R99</a></th>
						<td>Symptoms, signs and abnormal clinical and laboratory findings, not elsewhere classified</td>
					</tr>
					<tr>
						<th><a href="javascript:klasifikasi('S-T98')">S00-T98</a></th>
						<td>Injury, poisoning and certain other consequences of external causes</td>
					</tr><tr>
						<th><a href="javascript:klasifikasi('V-Y98')">V01-Y98</a></th>
						<td>External causes of morbidity and mortality</td>
					</tr>
					<tr>
						<th><a href="javascript:klasifikasi('Z-Z99')">Z00-Z99</a></th>
						<td>Factors influencing health status and contact with health services</td>
					</tr>
					<tr>
						<th><a href="javascript:klasifikasi('U-U99')">U00-U99</a></th>
						<td>Codes for special purposes</td>
					</tr>
				</table>
			</fieldset>
			<fieldset>
				<legend>Search Result</legend>	
				<table class="entry2">
					<thead>
						<tr>
							<th width="55">ID</th>
							<th>Description</th>
						</tr>
					</thead>
					<tbody>
					<c:forEach var="x" items="${daftar}" varStatus="xt">
						<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;" onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
						onclick="backToParent('${x.LIC_ID}','${x.LIC_DESC}');">
							<td style="${x.color}">${x.LIC_ID}.</td>
							<td style="${x.color}">${x.LIC_DESC}</td>
						</tr>
					</c:forEach>
					</tbody>	
					<tr>
						<td colspan="3">
							<div align="center">
								<input type="button" name="close" value="Close" onclick="window.close();">
							</div>
						</td>
					</tr>				
				</table>
			</fieldset>
			<input type="hidden" id="klasifikasi" name="klasifikasi">
			<input type="hidden" id="no" name="no" value="${no}">
			<input type="hidden" id="indx" name="indx" value="${indx}">		
			<input type="hidden" id="sub" name="sub" value="${sub}">
		</form>
	</body>
<%@ include file="/include/page/footer.jsp"%>
