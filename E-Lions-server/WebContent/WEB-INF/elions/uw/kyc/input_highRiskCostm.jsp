<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript">
	hideLoadingMessage();
</script>
</head>
<BODY onload="document.title='PopUp :: Input High Risk Costumer';formpost.Desk.focus()" style="height: 100%;">

<form method="post" name="formpost" action="${path }/uw/viewer.htm?window=input_highRiskCostm" style="text-align: center;">
	<div id="contents">
	<fieldset>
		<legend>Input High Risk Costumer</legend>
		<table class="entry2">
			<tr>
				<th>Silahkan masukan High Risk Costumer terbaru</th>
			</tr>
			<tr>
				<td align="center">										
					<input type="text" name="Desc" value="${Desc}" size="30">
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div align="center">
					<input type="hidden" name="simpan" value="true">
					<input type="submit" value="Save" 
						accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
					</div>
				</td>
			</tr>
			<tr>
				<th>
					Daftar ini akan menyaring Nasabah yang tergolong sebagai High Risk Costumer. 
				</th>
			</tr>
		</table>		
	</fieldset>
	<fieldset style="width: 30%">
		<legend></legend>

			<display:table style="font-size: 12px; text-align: center; width: 100%;"  name="HighRiskCostm" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}" export="false" >                                                         
					 	<display:column property="lshc_desc"  title="Daftar High Risk Costumer" style="text-align: center; font-size: 12px; font-weight: bolder;" sortable="false"/>
			</display:table>
			
	</fieldset>
	</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>