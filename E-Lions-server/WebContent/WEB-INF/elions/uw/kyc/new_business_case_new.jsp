<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<script type="text/javascript">
	hideLoadingMessage();
	function simpan(){
		formpost.proses.value=1;
		formpost.submit();
	}
	
	function cari(){
		indicator.style.display='';
		formpost.proses.value=0;
		formpost.submit();
	}
	function laporan(){
		var tgl1=formpost.tglAwal.value;
		var tgl2=formpost.tglAkhir.value;
		window.location="${path}/report/uw.htm?window=report_new_business_case&tanggalAwal="+tgl1+"&tanggalAkhir="+tgl2;
	}
	function awal(){
		document.title='PopUp :: Daily Monitoring  KYC - New Business Case';
		if('${cmd.sub}'==1){
			cari();
		}	
		
	}
</script>
</head>

<BODY onload="awal();" style="height: 100%;">

<form:form id="formpost" name="formpost" commandName="cmd">
	<fieldset>
		<legend>Daily Monitoring  KYC - New Business Case</legend>
		<table class="entry2">
			<tr>
				<td>
					Tanggal Terima SPAJ : dari <script>inputDate('tglAwal', '${cmd.tglAwal}', false, '', 9);</script> s/d <script>inputDate('tglAkhir', '${cmd.tglAkhir}', false, '', 9);</script>
						<span id="indicator" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
						<input type="button" value="Retrieve" onClick="cari();"
							accesskey="R" onmouseover="return overlib('Alt-R', AUTOSTATUS, WRAP);" onmouseout="nd();">
						<input type="button" value="Report" onClick="laporan();"
							accesskey="L" onmouseover="return overlib('Alt-L', AUTOSTATUS, WRAP);" onmouseout="nd();">
			
				</td>
			</tr>
		</table>	
	</fieldset>
		<display:table id="baris" name="${cmd.lsKycNewBus}" class="simple" decorator="org.displaytag.decorator.TotalTableDecorator" >
				<display:column property="reg_spaj" title="REG_SPAJ" sortable="true" />
				<display:column style="width: 200px" property="pemegang"
					title="Nama Pemegang Polis" sortable="true" />
				<display:column style="width: 200px" property="tertanggung"
					title="Nama Tertanggung" sortable="true" />
				<display:column property="mste_beg_date" title="Beg Date"
					format="{0, date, dd/MM/yyyy}" sortable="true" />
				<display:column property="lsdbs_name" title="Nama Produk"
					sortable="true" />
				<display:column property="lku_symbol" title="" sortable="true" />
				<display:column property="premi_pokok" title="PREMI_POKOK"
					format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" />
				<display:column property="total_tu" title="Jumlah Top-Up"
					format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" />
				<display:column property="nama_topup" title="Jenis TopUp"
					sortable="true" />
				<display:column property="frek" title="FREK" sortable="true" />
				<display:column title="Deskripsi Pekerjaan">
					<form:textarea path="lsKycNewBus[${baris.row}].mpn_job_desc" rows="3" cols="15"/>
				</display:column>
				<display:column title="Pekerjaan">
					<form:textarea path="lsKycNewBus[${baris.row}].mkl_kerja" rows="3" cols="15"/>
				</display:column>
				<display:column title="Bidang Usaha">
					<form:textarea path="lsKycNewBus[${baris.row}].mkl_industri" rows="3" cols="15"/>
				</display:column>
				<display:column property="mkl_penghasilan" title="Sumber Penghasilan"
					sortable="true" />
				<display:column property="mkl_pendanaan" title="Sumber Dana"
					sortable="true" />
				<display:column property="mkl_tujuan" title="Tujuan Beli Asuransi"
					sortable="true" />
				<display:column title="KYC Result">
					<form:textarea path="lsKycNewBus[${baris.row}].kycResult" rows="3" cols="15"/>
				</display:column>
				<display:column title="Update">
					<form:checkbox cssClass="noBorder" path="lsKycNewBus[${baris.row}].flagKyc" value="1"/>
				</display:column>
				<display:column title="Update Y UW">
					<form:checkbox cssClass="noBorder" path="lsKycNewBus[${baris.row}].flagYUw" value="1"/>
				</display:column>
				<display:column title="Update T UW">
					<form:checkbox cssClass="noBorder" path="lsKycNewBus[${baris.row}].flagTUw" value="1"/>
				</display:column>
				<display:column title="Update Y UKM">
					<form:checkbox cssClass="noBorder" path="lsKycNewBus[${baris.row}].flagYUkm" value="1"/>
				</display:column>
				<display:column title="Update T UKM">
					<form:checkbox cssClass="noBorder" path="lsKycNewBus[${baris.row}].flagTUkm" value="1"/>
				</display:column>
				<display:column title="Update Y DIREKSI">
					<form:checkbox cssClass="noBorder" path="lsKycNewBus[${baris.row}].flagYDirec" value="1"/>
				</display:column>
				<display:column title="Update T DIREKSI">
					<form:checkbox cssClass="noBorder" path="lsKycNewBus[${baris.row}].flagTDirec" value="1"/>
				</display:column>
				
			</display:table>                                                     		
			<table class="entry2">
			<tr>
			 <td>
		  			<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
								Info:<br>
									<c:forEach var="error" items="${status.errorMessages}">
												- <c:out value="${error}" escapeXml="false" />
										<br/>
									</c:forEach>
							</div>
						</c:if>									
					</spring:bind>
				</td>
			</tr>
			<c:if test="${suc eq 1}">
			<script type="text/javascript">
				alert("Berhasil Update KYC Result (Total Yang di Update =${tot})");
				window.location='${path}/uw/new_business_case.htm?tglAwal=${tglAwal}&tglAkhir=${tglAkhir}&sub=1';
			</script>
				<tr>
					<td>
						<div id="success">
							Berhasil Update KYC Result (Total Yang di Update =${tot})
						</div>
					</td>
				</tr>
			</c:if>	
				<tr>
					<th>
						<form:hidden path="proses"  />
						<input type="button" value="save" onClick="simpan();">
					</th>
				</tr>
			</table>
</form:form>
</body>
<%@ include file="/include/page/footer.jsp"%>
