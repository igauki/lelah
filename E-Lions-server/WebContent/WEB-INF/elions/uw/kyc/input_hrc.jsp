<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<html>
<script type="text/javascript">
	hideLoadingMessage();
	
	function buttonLink(btn){
		if(btn=='a'){//add
			formpost.proses.value="1";
		}else if(btn=='s'){//save
			formpost.proses.value="2";
		}else if(btn=='d'){//delete
			formpost.proses.value="3";
		}
		formpost.submit();
	}
	
	function pilAll(){
		flag=formpost.cekAll.checked;
		jml='${cmd.size}';		
		//jika cuma satu data
		if(jml==1){
			if(flag)
				formpost.elements['lsHrc[0].cek'].checked=true;			
			else
				formpost.elements['lsHrc[0].cek'].checked=false;						
		}else{
			if(flag){
				
				for(i=0;i<jml;i++){
					formpost.elements['lsHrc['+i+'].cek'].checked=true;			
				}
			}else{
				for(i=0;i<jml;i++){
					formpost.elements['lsHrc['+i+'].cek'].checked=false;			
				}
			}
		}	
	}
</script>
<BODY style="height: 100%;">
<form:form id="formpost" name="formpost" commandName="cmd">
	<div id="contents">
	<fieldset style="width: 40%">
		<legend>Input High Risk Costumer</legend>
		<table class="entry2">

			<tr>
				<th align="left">
					Daftar ini akan menyaring Nasabah yang tergolong sebagai High Risk Costumer. 
				</th>
			</tr>
		</table>		
		<display:table style="font-size: 12px; text-align: center; width: 100%;" id="baris" name="${cmd.lsHrc}" class="displaytag" decorator="org.displaytag.decorator.TotalTableDecorator" requestURI="${requestScope.requestURL}" export="false" >                                                         
		 	<display:column  property="brs"  title="No." />
			<display:column title="List High Risk Costumer" style="text-align: left;">
				<form:textarea path="lsHrc[${baris.brs-1}].lshc_desc" rows="3" cols="50"/>
			</display:column>
			<display:column title="Choose For Delete">
				<form:checkbox cssClass="noBorder" path="lsHrc[${baris.brs-1}].cek" value="1"/>
			</display:column>
		</display:table>
		<table class="entry2">
			<tr>
				<td width="80%">
					<div align="center">
					<input type="button" value="Add" onClick="buttonLink('a');"
						accesskey="A" onmouseover="return overlib('Alt-A', AUTOSTATUS, WRAP);" onmouseout="nd();">
					<input type="hidden" name="simpan" value="true">
					<input type="button" value="Save" onClick="buttonLink('s');" 
						accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
					<input type="button" value="Delete" onClick="buttonLink('d');"
						accesskey="D" onmouseover="return overlib('Alt-D', AUTOSTATUS, WRAP);" onmouseout="nd();">
					</div>
					<form:hidden path="proses" />
					<input type="hidden" name="pil_all" value="0">
				</td>
				<td>
					Check/Uncheck All
					<input type="checkbox" class="noBorder" name="cekAll" onclick="pilAll();" >
				</td>
			</tr>		
			<tr>
				<td>
		  			<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
								Info:<br>
									<c:forEach var="error" items="${status.errorMessages}">
												- <c:out value="${error}" escapeXml="false" />
										<br/>
									</c:forEach>
							</div>
						</c:if>									
					</spring:bind>
				</td>
			</tr>
			<c:if test="${submitSuccess eq true}">
			<script type="text/javascript">
				alert("Berhasil Update Input High Risk Customer");
				window.location='${path}/uw/kyc/input_hrc.htm';
			</script>
				<tr>
					<td>
						<div id="success">
							Berhasil Update Input High Risk Customer
						</div>
					</td>
				</tr>
			</c:if>				
		</table>			
	</fieldset>
	</div>

</form:form>
</body>
<%@ include file="/include/page/footer.jsp"%>