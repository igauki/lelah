<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<script type="text/javascript">
	hideLoadingMessage();
// 	function simpan(){
// 		formpost.proses.value=1;
// 		formpost.submit();
// 	}
	
	function cari(){
		indicator.style.display='';
// 		formpost.proses.value=0;
		formpost.submit();
	}
	function laporan(){
		var tgl1=formpost.tglAwal.value;
		var tgl2=formpost.tglAkhir.value;
		window.location="${path}/report/uw.htm?window=report_pencairan_case&tanggalAwal="+tgl1+"&tanggalAkhir="+tgl2;
	}
	function awal(){
		document.title='PopUp :: Daily Monitoring  KYC - Top Up Case';
		if('${cmd.sub}'==1){
			cari();
		}	
		
	}
</script>
</head>

<BODY onload="awal();" style="height: 100%;">

<form:form id="formpost" name="formpost" commandName="cmd">
	<fieldset>
		<legend>Daily Monitoring  KYC - Pencairan Sebagian / Penarikan</legend>
		<table class="entry2">
			<tr>
				<td>
					Tanggal Terima SPAJ : dari <script>inputDate('tglAwal', '${cmd.tglAwal}', false, '', 9);</script> s/d <script>inputDate('tglAkhir', '${cmd.tglAkhir}', false, '', 9);</script>
						<span id="indicator" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
						<input type="button" value="Retrieve" onClick="cari();"
							accesskey="R" onmouseover="return overlib('Alt-R', AUTOSTATUS, WRAP);" onmouseout="nd();">
						<input type="button" value="Report" onClick="laporan();"
							accesskey="L" onmouseover="return overlib('Alt-L', AUTOSTATUS, WRAP);" onmouseout="nd();">
			
				</td>
			</tr>
		</table>	
	</fieldset>
		<display:table id="baris" name="${cmd.lsKycPencairan}" class="simple" decorator="org.displaytag.decorator.TotalTableDecorator" >
				<display:column property="reg_spaj" title="REG_SPAJ" sortable="true" />
				<display:column property="pemegang" title="NAMA PEMEGANG POLIS" sortable="true" />
				<display:column property="tertanggung" title="NAMA TERTANGGUNG" sortable="true" />
				<display:column property="lsdbs_name" title="NAMA PRODUK" sortable="true" />
				<display:column property="tgl_cair" title="TANGGAL PENCAIRAN"
					format="{0, date, dd/MM/yyyy}" sortable="true" />
				<display:column property="nominal_cair" title="TOTAL NILAI PENCAIRAN"
					format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" />
				<display:column property="mste_beg_date" title="TANGGAL EFEKTIF POLIS"
					format="{0, date, dd/MM/yyyy}" sortable="true" />
				<display:column property="lku_symbol" title="" sortable="true" />
				<display:column property="premi_pokok" title="PREMI POKOK"
					format="{0, number, #,##0.00;(#,##0.00)}" sortable="true" />
				<display:column property="frek" title="FREK" sortable="true" />
				<display:column title="Deskripsi Pekerjaan">
					<form:textarea path="lsKycPencairan[${baris.row}].mpn_job_desc" rows="3" cols="15"/>
				</display:column>
				<display:column title="PEKERJAAN">
					<form:textarea path="lsKycPencairan[${baris.row}].mkl_kerja" rows="3" cols="15"/>
				</display:column>
				<display:column title="BIDANG USAHA">
					<form:textarea path="lsKycPencairan[${baris.row}].mkl_industri" rows="3" cols="15"/>
				</display:column>
				<display:column property="mkl_penghasilan" title="SUMBER PENGHASILAN" sortable="true" />
				<display:column property="mkl_pendanaan" title="SUMBER PENDANAAN" sortable="true" />
				<display:column property="mkl_tujuan" title="TUJUNA MEMBELI ASURANSI" sortable="true" />
				<display:column title="User yang mengakseptasi (User Finance)">
					<form:textarea path="lsKycPencairan[${baris.row}].kycResult" rows="3" cols="15"/>
				</display:column>
			</display:table>                                                     		
			<table class="entry2">
			<tr>
			 <td>
		  			<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
								Info:<br>
									<c:forEach var="error" items="${status.errorMessages}">
												- <c:out value="${error}" escapeXml="false" />
										<br/>
									</c:forEach>
							</div>
						</c:if>									
					</spring:bind>
				</td>
			</tr>
			<c:if test="${suc eq 1}">
			<script type="text/javascript">
				alert("Berhasil Update KYC Result (Total Yang di Update =${tot})");
				window.location='${path}/uw/pencairan_case.htm?tglAwal=${tglAwal}&tglAkhir=${tglAkhir}&sub=1';
			</script>
				<tr>
					<td>
						<div id="success">
							Berhasil Update KYC Result (Total Yang di Update =${tot})
						</div>
					</td>
				</tr>
			</c:if>	
				<tr>
					<th>
						<form:hidden path="proses"  />
<!-- 						<input type="button" value="save" onClick="simpan();"> -->
					</th>
				</tr>
			</table>
</form:form>
</body>
<%@ include file="/include/page/footer.jsp"%>
