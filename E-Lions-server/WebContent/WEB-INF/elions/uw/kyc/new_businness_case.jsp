<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript">
	hideLoadingMessage();
</script>
<style>
<!--table
	{mso-displayed-decimal-separator:"\,";
	mso-displayed-thousand-separator:"\.";}
@page
	{margin:1.0in .75in 1.0in .75in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;}
tr
	{mso-height-source:auto;}
col
	{mso-width-source:auto;}
br
	{mso-data-placement:same-cell;}
.style0
	{mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	white-space:nowrap;
	mso-rotate:0;
	mso-background-source:auto;
	mso-pattern:auto;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:1;
	border:none;
	mso-protection:locked visible;
	mso-style-name:Normal;
	mso-style-id:0;}
td
	{mso-style-parent:style0;
	padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:1;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border:none;
	mso-background-source:auto;
	mso-pattern:auto;
	mso-protection:locked visible;
	white-space:nowrap;
	mso-rotate:0;}
.xl24
	{mso-style-parent:style0;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	white-space:normal;}
.xl25
	{mso-style-parent:style0;
	vertical-align:top;
	border-top:none;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	white-space:normal;}
.xl26
	{mso-style-parent:style0;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	white-space:normal;}
.xl27
	{mso-style-parent:style0;
	vertical-align:top;
	border-top:none;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	white-space:normal;}
.xl28
	{mso-style-parent:style0;
	vertical-align:top;
	border-top:none;
	border-right:.5pt solid windowtext;
	border-bottom:none;
	border-left:.5pt solid windowtext;
	white-space:normal;}
.xl29
	{mso-style-parent:style0;
	vertical-align:top;
	white-space:normal;}
.xl30
	{mso-style-parent:style0;
	vertical-align:top;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:.5pt solid windowtext;
	white-space:normal;}
.xl31
	{mso-style-parent:style0;
	vertical-align:top;
	border-top:none;
	border-right:.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	white-space:normal;}
.xl32
	{mso-style-parent:style0;
	font-weight:700;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	text-align:center;
	vertical-align:middle;
	border-top:.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:none;
	border-left:.5pt solid windowtext;
	white-space:normal;}
.xl33
	{mso-style-parent:style0;
	font-weight:700;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	text-align:center;
	vertical-align:middle;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	white-space:normal;}
.xl34
	{mso-style-parent:style0;
	font-weight:700;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	text-align:center;
	vertical-align:middle;
	border-top:.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	white-space:normal;}
.xl35
	{mso-style-parent:style0;
	font-weight:700;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	text-align:center;
	vertical-align:middle;
	border-top:none;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	white-space:normal;}
.xl36
	{mso-style-parent:style0;
	font-weight:700;
	font-family:Arial, sans-serif;
	mso-font-charset:0;
	text-align:center;
	vertical-align:middle;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	white-space:normal;}
-->
</style>
</head>

<BODY onload="document.title='PopUp :: Daily Monitoring  KYC - New Business Case';" style="height: 100%;">

<form method="post" name="formpost" action="${path }/uw/viewer.htm?window=new_businness_case" style="text-align: center;">
	<div id="contents">
	<fieldset>
		<legend>Daily Monitoring  KYC - New Business Case</legend>
		<table class="entry2">
			<tr>
				<th>
					Tanggal Terima SPAJ : dari <script>inputDate('dariTanggal', '${cmd.s_tgl_rk}', false, '', 9);</script> s/d <script>inputDate('sampaiTanggal', '${cmd.s_tgl_rk}', false, '', 9);</script>
						<input type="hidden" name="simpan" value="true">
						<input type="submit" value="Retrieve" 
							accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
			
				</th>
			</tr>
			<tr>
			</tr>
			<tr>
				<td>
					
				</td>
			</tr>			
		</table>
		<c:choose>
			<c:when test="${not empty KYCnewBis}">
				<table x:str border=0 cellpadding=0 cellspacing=0 width=1778 style='border-collapse:
			 collapse;table-layout:fixed;width:1336pt'>
			 <col width=34 style='mso-width-source:userset;mso-width-alt:1243;width:26pt'>
			 <col width=104 style='mso-width-source:userset;mso-width-alt:3803;width:78pt'>
			 <col width=121 style='mso-width-source:userset;mso-width-alt:4425;width:91pt'>
			 <col width=151 style='mso-width-source:userset;mso-width-alt:5522;width:113pt'>
			 <col width=100 style='mso-width-source:userset;mso-width-alt:3657;width:75pt'>
			 <col width=109 style='mso-width-source:userset;mso-width-alt:3986;width:82pt'>
			 <col width=114 style='mso-width-source:userset;mso-width-alt:4169;width:86pt'>
			 <col width=131 style='mso-width-source:userset;mso-width-alt:4790;width:98pt'>
			 <col width=106 span=2 style='mso-width-source:userset;mso-width-alt:3876;
			 width:80pt'>
			 <col width=143 style='mso-width-source:userset;mso-width-alt:5229;width:107pt'>
			 <col width=120 style='mso-width-source:userset;mso-width-alt:4388;width:90pt'>
			 <col width=143 style='mso-width-source:userset;mso-width-alt:5229;width:107pt'>
			 <col width=138 style='mso-width-source:userset;mso-width-alt:5046;width:104pt'>
			 <col width=158 style='mso-width-source:userset;mso-width-alt:5778;width:119pt'>
			 <tr height=17 style='height:12.75pt'>
			  <td rowspan=2 height=51 class=xl32 width=34 style='border-bottom:.5pt solid black;
			  height:38.25pt;width:26pt'>No</td>
			  <td rowspan=2 class=xl32 width=104 style='border-bottom:.5pt solid black;
			  width:78pt'>No.Register</td>
			  <td rowspan=2 class=xl32 width=121 style='border-bottom:.5pt solid black;
			  width:91pt'>Pemegang Polis</td>
			  <td rowspan=2 class=xl32 width=151 style='border-bottom:.5pt solid black;
			  width:113pt'>Tertanggung</td>
			  <td rowspan=2 class=xl32 width=100 style='border-bottom:.5pt solid black;
			  width:75pt'>Efektif Polis</td>
			  <td rowspan=2 class=xl32 width=109 style='border-bottom:.5pt solid black;
			  width:82pt'>Produk</td>
			  <td rowspan=2 class=xl32 width=114 style='border-bottom:.5pt solid black;
			  width:86pt'>Premi Pokok (Rp/US$)</td>
			  <td colspan=2 class=xl33 width=237 style='border-right:.5pt solid black;
			  width:178pt'>Top Up</td>
			  <td rowspan=2 class=xl32 width=106 style='border-bottom:.5pt solid black;
			  width:80pt'>Frek.bayar (M/Q/SA/A)</td>
			  <td rowspan=2 class=xl32 width=143 style='border-bottom:.5pt solid black;
			  width:107pt'>Pekerjaan</td>
			  <td rowspan=2 class=xl32 width=120 style='border-bottom:.5pt solid black;
			  width:90pt'>Bidang usaha</td>
			  <td rowspan=2 class=xl32 width=143 style='border-bottom:.5pt solid black;
			  width:107pt'>Penghasilan/thn</td>
			  <td rowspan=2 class=xl32 width=138 style='border-bottom:.5pt solid black;
			  width:104pt'>Sumber dana</td>
			  <td rowspan=2 class=xl32 width=158 style='border-bottom:.5pt solid black;
			  width:119pt'>Tujuan Beli Asuransi</td>
			   <td rowspan=2 class=xl32 width=158 style='border-bottom:.5pt solid black;
			  width:119pt'>KYC Result</td>
			   <td rowspan=2 width=158 style='width:119pt'></td>
			 </tr>
			 <tr height=34 style='height:25.5pt'>
			  <td height=34 class=xl36 width=131 style='height:25.5pt;width:98pt'>Jumlah
			  Top Up(Rp/US$)</td>
			  <td class=xl35 width=106 style='width:80pt'>Jenis Top Up</td>
			 </tr>

			 <c:forEach items="${KYCnewBis}" var="S" varStatus="s">
				 <tr height=17 style='height:12.75pt' align="center">
				  <td height=17 class=xl25 width=34 style='height:17pt;width:26pt'> <input type="text" name="" size="4" style="border-color: white;"></td>
				  <td class=xl25 width=104 style='border-left:none;width:78pt'> <input type="text" name=""  value="${S.REG_SPAJ}"size="17" style="border-color: white;"></td>
				  <td class=xl25 width=121 style='border-left:none;width:91pt'> <input type="text" name=""  value="${S.PEMEGANG}" size="21" style="border-color: white;"></td>
				  <td class=xl25 width=151 style='border-left:none;width:113pt'> <input type="text" name="" value="${S.TERTANGGUNG}" size="27" style="border-color: white;"></td>
				  <td class=xl25 width=100 style='border-left:none;width:75pt'> <input type="text" name="" value="${S.MSTE_BEG_DATE}" size="17" style="border-color: white;"></td>
				  <td class=xl26 width=109 style='width:82pt'> <input type="text" name="" value="${S.LSDBS_NAME}" size="19" style="border-color: white;"></td>
				  <td class=xl24 width=114 style='width:86pt'> <input type="text" name="" value="${S.LKU_SYMBOL}" size="20" style="border-color: white;"></td>
				  <td class=xl25 width=131 style='width:98pt'> <input type="text" name="" value="${S.TERTANGGUNG}" size="23" style="border-color: white;"></td>
				  <td class=xl25 width=106 style='border-left:none;width:80pt'> <input type="text" name="" value="${S.TERTANGGUNG}" value="${S.TERTANGGUNG}" size="18" style="border-color: white;"></td>
				  <td class=xl26 width=106 style='width:80pt'> <input type="text" name="" value="${S.FREK}" size="18" style="border-color: white;"></td>
				  <td class=xl25 width=143 style='width:107pt'> <input type="text" name="" value="${S.TERTANGGUNG}" size="25" style="border-color: white;"></td>
				  <td class=xl25 width=120 style='border-left:none;width:90pt'> <input type="text" name="" value="${S.TERTANGGUNG}" size="21" style="border-color: white;"></td>
				  <td class=xl25 width=143 style='border-left:none;width:107pt'> <input type="text" name="" value="${S.TERTANGGUNG}" size="25" style="border-color: white;"></td>
				  <td class=xl27 width=138 style='width:104pt'> <input type="text" name="" value="${S.TERTANGGUNG}" size="25" style="border-color: white;"></td>
				  <td class=xl25 width=158 style='border-left:none;width:119pt'> <input type="text" name="" value="${S.TERTANGGUNG}" size="28" style="border-color: white;"></td>
				  <td class=xl25 width=158 style='border-left:none;width:119pt'> <input type="text" name="" value="${S.TERTANGGUNG}" size="28" style="border-color: white;"></td>
				  <td  width=158 style='width:119pt' align="left"> <input type="button" value="simpan"></td>
				 </tr>
			 </c:forEach>
			</table>
			</c:when>
			<c:otherwise>
				Silahkan masukan Tanggal Terima SPAJ yang di inginkan.
			</c:otherwise>
		</c:choose>
		
		
	</fieldset>
	
	</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>