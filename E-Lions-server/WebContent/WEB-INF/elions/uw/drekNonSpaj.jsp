<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />

<head>
	<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta NAME="Description" CONTENT="EkaLife">
	<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<meta HTTP-EQUIV="Expires" CONTENT="-1">
	<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
	<link href="${path}/include/image/eas.ico" rel="shortcut icon">
	<!--  -->
	<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
	<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
	<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
	<!--  -->
	<script type="text/javascript" src="${path }/include/js/default.js"></script>
	<!--  -->
	<script type="text/javascript" src="${path}/include/jquery/jquery-1.5.1.min.js"></script>
	<script type="text/javascript" src="${path}/include/jquery/jquery-ui-1.8.14.custom.min.js"></script>
	<!-- Ajax Related -->
	<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
	<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
	<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
	<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
	<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
	<script type="text/javascript">
		hideLoadingMessage();

		function centang(elemen){
			document.getElementById(elemen).checked = true;
		}

		function cek(name){
  			xx = document.getElementsByName(name);
  			if(xx[0].checked == true && xx[1].checked == false) {
  				 xx[1].disabled = true;
  			}
  			else if(xx[0].checked == false && xx[1].checked == false){
				xx[0].disabled = false;	
				xx[1].disabled = false;
  			}
  			else if(xx[1].checked == true && xx[0].checked == false) {
  				xx[0].disabled = true;
 			}	
		}
		
		function setEndNominal(name){
 			document.getElementById('endNominal').value = name;
		}
		
		function setEndDate(){
			document.getElementById('_endDate').value = document.getElementById('startDate').value;
			document.getElementById('endDate').value = document.getElementById('startDate').value;
		}
		
		function kembali(index, temp_premi, tipe, no_spaj, tgl_trx, lku_id, nilai_trx, norek_ajs, ket, jumlah){
			var no_trx = document.getElementById('noTrx'+index).value;
			var premi = window.showModalDialog('${path}/uw/uw.htm?window=paymentNonSpaj&jumlah='+document.getElementById('nilaiTrx'+index).value+'&simbol='+document.getElementById('lkuSymbol'+index).value+'&premiTerpakai='+jumlah+'&no_trx='+no_trx+'&norek_ajs='+norek_ajs, 'CONTENT', 'dialogWidth:400px; dialogHeight:400px; resizable:yes;');
			var tipe = '';
			
			ajaxManager.ibankTunggalOrGabungan(no_trx, no_spaj,
				{callback:function(output) {
					DWRUtil.useLoadingMessage();
					tipe = output;
			
					if((no_spaj != '' && tipe == '0') || (no_spaj != '' &&  tipe == '')){
						alert('RK ini sudah digunakan pada nomor SPAJ ' + no_spaj);
					}else{
						if(premi != 'undefined'){
							//Tanggal RK
							self.opener.document.getElementById('tgl_rk').value = tgl_trx;
							//Kurs
							self.opener.document.getElementById('lkuId').value = lku_id;
							//No Transaksi
							self.opener.document.getElementById('no_trx').value = no_trx;
							//Premi
							self.opener.document.getElementById('premitmp').value = premi;
							window.close();
						}else{
							window.close();
						}
					}
					
				 },
			  	timeout:180000,
			  	errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
			});
		}
		
		//Validate Number
		function CalcKeyCode(aChar){
			var character = aChar.substring(0,1);
		  	var code = aChar.charCodeAt(0);
		  	return code;
		}
		
		function checkNumber(val){
		  	var strPass = val.value;
		  	var strLength = strPass.length;
		  	var lchar = val.value.charAt((strLength) - 1);
		  	var cCode = CalcKeyCode(lchar);
		
		  	/* 
		  		Check if the keyed in character is a number
		       	do you want alphabetic UPPERCASE only ?
		       	or lower case only just check their respective
		       	codes and replace the 48 and 57
		    */
			if(cCode < 48 || cCode > 57 ){
		    	var myNumber = val.value.substring(0, (strLength) - 1);
		    	val.value = myNumber;
		  	}
		  	return false;
		}
		
		// Jalankan semua script jquery, setelah seluruh document selesai loading
		$().ready(function(){
			// user memilih wakil, maka tampilkan region nya
			$("#no_rek").change(function() {
				if($("#no_rek").val()==null || $("#no_rek").val() == ''){
					window.location.reload(true); 
				}else{ 
					$("#lsrek_id").empty();
					var url2 = "${path}/uw/uw.htm?window=drek&piyu=1&json=1&no_rek=" + $("#no_rek").val();
					$.getJSON(url2, function(result2) {
						$.each(result2, function() {
							$("<option/>").val(this.key).html(this.value).appendTo("#lsrek_id");
						});
						$("#lsrek_id option:first").attr('selected','selected');
					});	
				};
			});
		});
	</script>
</head>

<body onload="setupPanes('container1','tab1');" style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="showPane('pane1', this)" id="tab1">Daftar Transfer Bank</a>
			</li>
		</ul>
		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form method="post" name="formpost" action="${path}/uw/uw.htm?window=drekNonSpaj">
					<fieldset>
						<legend>Parameter</legend>
						<input type="hidden" id="tempPremi" />
						<input type="hidden" id="tempJumlah" />
						<input type="hidden" id="tunggalOrGabungan"/>
						<input type="hidden" id="noSpaj" name="noSpaj" value="${regSpaj}"/>
						
						<table class="entry2" style="width: auto;">
							<!-- Tambahan -->
							<tr>
								<th nowrap="nowrap">No. Rekening (5 digit terakhir)</th>
								<td><input type="text" name="no_rek" id="no_rek" size="5" maxlength="5" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)"></td>
							</tr>
							<!-- End Tambahan -->
							<tr>
								<th nowrap="nowrap">Rekening AJS</th>
								<td>
									<select name="lsrek_id" id="lsrek_id">
										<c:set var="grup" value=""/>
										<c:forEach items="${daftarRekAjs}" var="b">
											<c:choose>
												<c:when test="${b.desc ne grup}">
													<c:set var="grup" value="${b.desc}"/>				 
													<OPTGROUP label="${b.desc}">
													<option value="${b.key}" <c:if test="${b.key eq lsrek_id}"> selected </c:if> >${b.value}</option>
												</c:when>
												<c:otherwise>
													<option value="${b.key}" <c:if test="${b.key eq lsrek_id}"> selected </c:if> >${b.value}</option>
												</c:otherwise>
											</c:choose>
											<c:if test="${b.desc ne grup}">
												<c:set var="grup" value="${b.desc}"/>				
												</OPTGROUP>
											</c:if>
										</c:forEach>
									</select>
								</td>
							</tr>
							<tr>
								<th nowrap="nowrap">Cabang Bank Nasabah (Khusus Bank Sinarmas)</th>
								<td>
									<select name="kode">
										<c:forEach items="${daftarCab}" var="b">
											<option value="${b.key}" <c:if test="${b.key eq kode}"> selected </c:if> >${b.value}</option>
										</c:forEach>
									</select>
								</td>
							</tr>
							<tr>
								<th nowrap="nowrap">Tgl RK</th>
								<td>
									<script>inputDate('startDate', '${startDate}', false, 'setEndDate()');</script> s/d 
									<script>inputDate('endDate', '${endDate}', false);</script>									
								</td>
							</tr>
							<tr>
								<th nowrap="nowrap">Nominal</th>
								<td>
									<input type="text" name="startNominal" id="startNominal" value="${startNominal}" onkeyup="setEndNominal(this.value)"> s/d 
									<input type="text" name="endNominal" id="endNominal" value="${endNominal}">
								</td>
							</tr>
							<tr>
								<th nowrap="nowrap"></th>
								<td>
									<input type="submit" name="btnCari" value="Cari">
									<input type="button" name="btnTutup" value="Tutup" onclick="window.close();">
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<c:set var="ajs" value="" />
									<table class="displaytag">
										<thead>
											<tr>
												<th style="width:80px;">Tanggal TRX</th>
												<th style="width:150px;">Cabang</th>
												<th style="width:100px;">No. SPAJ</th>
												<th style="width:80px;">Kode TRX</th>
												<th style="width:40px;">Jenis</th>
												<th style="width:60px;">Kurs TRX</th>
												<th style="width:90px;">Nilai TRX</th>
												<th style="width:150px;">Ket</th>
												<th style="width:100px;">Premi blm terpakai</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${daftarDrek}" var="d" varStatus="st">
												<c:choose>
													<c:when test="${ajs ne d.norek_ajs}">
														<tr>
															<td class="left" colspan="9" style="font-size: 8pt; font-weight: bold; color: #FF0000"><br/>[${d.norek_ajs}] ${d.bank_pusat_ajs} ${d.bank_ajs}</td>
														</tr>
														<c:set var="ajs" value="${d.norek_ajs}" />
													</c:when>
													<c:otherwise>
													</c:otherwise>
												</c:choose>
												<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
														onclick="kembali(${st.index},document.getElementById('tempPremi').value,document.getElementById('tunggalOrGabungan').value,'${d.no_spaj}',
														'<fmt:formatDate pattern="dd/MM/yyyy" value="${d.tgl_trx}" />', '${d.lku_id}', '<fmt:formatNumber value="${d.nilai_trx}"/>', '${d.norek_ajs}','textarea', '${d.jumlah}');"
														<c:if test="${not empty d.no_spaj}"> style="background-color: #C0C0C0" </c:if>
												>
													<td class="center"><fmt:formatDate pattern="dd/MM/yyyy" value="${d.tgl_trx}" /></td>
													<td class="left">${d.nama_cab}</td>
													<td class="left"><input size="15" type="text" id="reg_spaj${st.index}" name="reg_spaj${st.index}" value="${d.no_spaj}" onfocus="centang('rubah${st.index}');"></td>
													<td class="left">${d.no_trx}</td> 
													<td class="center">${d.jenis}</td>
													<td class="center">${d.lku_symbol}</td>
													<td><fmt:formatNumber value="${d.nilai_trx}"/></td>
													<td class="left"><textarea readonly="readonly" name="textarea1" id="textarea" cols="35" rows="5">${d.ket}</textarea></td>
													<td class="left">
														<input type="text" <c:if test="${d.flag_gabungan eq 'checked'}"> value="<fmt:parseNumber value="${d.jumlah}"/>" </c:if>readonly="readonly"/>
														<input type="hidden" id="lkuSymbol${st.index}" value="${d.lku_symbol}" />
														<input type="hidden" id="nilaiTrx${st.index}" value="<fmt:parseNumber value="${d.nilai_trx}"/>" />
														<input type="hidden" id="noTrx${st.index}" value="${d.no_trx}" />
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</td>
							</tr>
						</table>
					</fieldset>					
				</form>
			</div>
		</div>
	</div>
</body>
</html>