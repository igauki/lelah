<%@ include file="/include/page/header.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<script type="text/javascript" src="${path}/include/js/jquery-1.3.2.min.js"></script>
<script>
$(document).ready(function() {
        <c:if test="${not empty param.pesan}">
            alert('${param.pesan}');
        </c:if>
    });
</script>
<body onload="document.title='PopUp :: Alamat Penagihan';setupPanes('container1','tab1');" style="height: 100%;">
	<div class="tab-container" id="container1">
		<ul class="tabs"><li><a href="#" onClick="return showPane('pane1', this)" id="tab1">Alamat Penagihan</a></li></ul>
		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form:form id="formpost" name="formpost" commandName="cmd">
				<fieldset>
                      <legend>Info User</legend>
                         <table class="entry2">
                   <tr>
                       <th nowrap="nowrap">User</th><th align ="left">&nbsp;[${infoDetailUser.LUS_ID}]&nbsp;&nbsp;${infoDetailUser.LUS_LOGIN_NAME}</th>
                  </tr>
                  <tr>
                       <th nowrap="nowrap">Nama<br></th><th align ="left">&nbsp;${infoDetailUser.LUS_FULL_NAME}&nbsp;[${infoDetailUser.LDE_DEPT}]</th>
                 </tr>
               </table>
            </fieldset>
            <fieldset>
                      <legend>Alamat Penagihan</legend>
				    <table class="entry2">
				     <tr> 
                        <th nowrap="nowrap">Spaj</th>
                        <td><form:input path="reg_spaj" readonly="readonly" maxlength="10"/></td>
                      </tr>
                      <tr> 
                        <th nowrap="nowrap">Region</th>
                        <td><form:input path="region" readonly="readonly" maxlength="6"/>&nbsp;<form:input path="region_name" readonly="readonly" size="80" maxlength="100"/></td>
                      </tr>
                      <tr> 
                        <th nowrap="nowrap">Contact Person</th>
                         <c:if test ="${not empty gelar}">
                                 <td>
                                    <div id="gelar">
                                  <select name="gelar">
                                   <c:forEach items="${gelar}" var="c">
                                     <option 
                                     <c:if test="${cmd.lti_id eq c.LTI_ID}"> SELECTED </c:if>
                                     value="${c.LTI_ID}">${c.LTI_NOTE}</option>
                                </c:forEach>
                            </select>
                  </c:if>
                      <form:input path="msap_contact" size="30" maxlength="30"/></td>
                      </tr>
				     <tr> 
				        <th nowrap="nowrap">Alamat Penagihan</th>
				        <td><form:textarea path="msap_address" cols="100" rows="5" onkeyup="textCounter(this, 200);" onkeydown="textCounter(this, 200);"/></td>
				      </tr>
				      <tr> 
				        <th nowrap="nowrap">Kode Pos</th>
				        <td><form:input path="msap_zip_code" maxlength="10"/></td>
				      </tr>
				      <tr> 
				        <th nowrap="nowrap">Kota</th>
				        <td><form:input path="kota" maxlength="30"/></td>
				      </tr>
				      <tr> 
				        <th nowrap="nowrap">No Telp 1</th>
				        <td><form:input path="msap_area_code1" maxlength="4" size="4"/>&nbsp;<form:input path="msap_phone1" maxlength="20" size="12"/></td>      
				      </tr>
				      <tr> 
				        <th nowrap="nowrap">No Telp 2</th>
				        <td><form:input path="msap_area_code2" maxlength="4" size="4"/>&nbsp;<form:input path="msap_phone2" maxlength="20" size="12"/></td>        
				      </tr>
				      <tr> 
				        <th nowrap="nowrap">No Fax</th>
				        <td><form:input path="msap_area_code_fax1" maxlength="4" size="4"/>&nbsp;<form:input path="msap_fax1" maxlength="20" size="12"/></td>        
				      </tr>
				      <tr> 
				        <th nowrap="nowrap">No HP</th>
				        <td><form:input path="no_hp" maxlength="20"/></td>        
				      </tr>
				      <tr> 
				        <th nowrap="nowrap">No HP2</th>
				        <td><form:input path="no_hp2" maxlength="20"/></td>        
				      </tr>
				      <tr> 
				        <th nowrap="nowrap">Email</th>
				        <td><form:input path="e_mail" maxlength="50" size="60"/></td>        
				      </tr>
				      <tr>
				      	<th nowrap="nowrap"></th>
				      	<td colspan="3">				      	
						    <input type="submit" name="save" value="Save">&nbsp;
						    <c:if test="${not empty pesan}"><br/>
                                  <div id="error">${pesan}</div>
                              </c:if>
				      	</td>
				      </tr>
				    </table>
				    </fieldset>
				</form:form>
			</div>
		</div>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>