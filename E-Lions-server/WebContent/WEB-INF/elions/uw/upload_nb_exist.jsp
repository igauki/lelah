<%@ include file="/include/page/header_jquery.jsp"%>
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (jQueryUI Tabs) init tab2 Utama
		$("#tabs").tabs();

		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title], label[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		
		// button icons
		//$( "#btnShow" ).button({icons: {primary: "ui-icon-search"}});
		$( "#btnSimpan" ).button({icons: {primary: "ui-icon-note"}});
		//$( "#btnHistory" ).button({icons: {primary: "ui-icon-script"}});		
		
		
		var pesan = '${pesan}';
		if(pesan!='' && pesan!=null){
			alert(pesan);
		}
		
		$("#flag_standard").change(function() {
			if(this.value==1){
				$("#flag_active").val(1);
				alert("Hadiah standart selalu aktif");
			}
		});
		
		$("#flag_active").change(function() {
			if($("#flag_standard").val()==1){
				$("#flag_active").val(1);
			}
		});
		
		$("#lh_harga").keypress(function (e){
		  var charCode = (e.which) ? e.which : e.keyCode;
		  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		    return false;
		  }
		});
		
	});
	
	var fieldName='copy';

	function selectall(){
	  var i=document.formpost.elements.length;
	  var e=document.formpost.elements;
	  var name=new Array();
	  var value=new Array();
	  var j=0;
	  for(var k=0;k<i;k++)
	  {
	    if(document.formpost.elements[k].name==fieldName)
	    {
	      if(document.formpost.elements[k].checked==true){
	        value[j]=document.formpost.elements[k].value;
	        j++;
	      }
	    }
	  }
	  checkSelect();
	}
		
	function selectCheck(obj)
	{
	 var i=document.formpost.elements.length;
	  for(var k=0;k<i;k++)
	  {
	    if(document.formpost.elements[k].name==fieldName)
	    {
	      document.formpost.elements[k].checked=obj;
	    }
	  }
	  selectall();
	}
		
	function selectallMe()
	{
	  if(document.formpost.allCheck.checked==true)
	  {
	   selectCheck(true);
	  }
	  else
	  {
	    selectCheck(false);
	  }
	}
		
	function checkSelect()
	{
	 var i=document.formpost.elements.length;
	 var berror=true;
	  for(var k=0;k<i;k++)
	  {
	    if(document.formpost.elements[k].name==fieldName)
	    {
	      if(document.formpost.elements[k].checked==false)
	      {
	        berror=false;
	        break;
	      }
	    }
	  }
	  if(berror==false)
	  {
	    document.formpost.allCheck.checked=false;
	  }
	  else
	  {
	    document.formpost.allCheck.checked=true;
	  }
	}
	
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 23em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 23.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: left; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 36em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

	/* untuk tombol2 kecil di report */
	ul#icons {margin: 0; padding: 0;}
	ul#icons li {margin: 0 1px 0 0; position: relative; padding: 0px; cursor: pointer; float: left;  list-style: none;}
	ul#icons span.ui-icon {float: left; margin: 0px;}
	
	.styleTable { border-collapse: separate; }
	.styleTable TD { font-weight: normal !important; padding: .4em; }
	.styleTable TH { text-align: left; padding: .8em .4em; }

</style>
<BODY onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Copy Hasil Scan Dari SPAJ/NO POLIS lama ke SPAJ/NO POLIS baru </a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" style="text-align: left;">
			
					<table class="displaytag">
					<tr>
						<div class="rowElem">
							<label >Copy Dari SPAJ : </label>
								<input name="spaj" id=""spaj"" type="text" title="Masukan SPAJ yang ingin di copy" value="${spaj}"> <input type="submit" name=view value="view">
						</div>
					</tr>
						<tr>
							<th style="vertical-align: top;">
								
								<fieldset>
									<legend class="ui-widget-header ui-corner-all" style="height: 15px;padding:2px;">Daftar File di SPAJ ${spaj}  : </legend>
									<table class="displaytag">
										<thead>
											<tr>
												<th>File</th>
												<th>Check File</th>
												<th><input type="checkbox" name="allCheck" onclick="selectallMe()"></th>
											</tr>
										</thead>
										<tbody class="ui-widget-content">
											<c:forEach var="s" items="${daftarAdaLama}">
												<tr>
													<td style="background-color: white; text-align: left;">${s.key}</td>
													<td><label><input type="checkbox" name="copy" id="copy" value="${s.key}"></td></label>
													<%-- <td style="background-color: white; text-align: center;">${s.value}</td> --%>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</fieldset>
								
								<!-- <fieldset>
									<legend class="ui-widget-header ui-corner-all">Action</legend> -->
									<input type="submit" name=action value="Copy" onclick="return confirm('Sudah cek kebenaran datanya?');">
								<!-- </fieldset> -->
							</th>
							<th>&nbsp;&nbsp;&nbsp;&nbsp;</th>
							<th valign="top">
									<fieldset class="ui-widget ui-widget-content">
									<legend class="ui-widget-header ui-corner-all" style="height: 15px;padding:2px;">Hasil Copy dari SPAJ [ ${spaj} ]  ke SPAJ ${reg_spaj}</legend>
																		
									<table class="entry2">
										<c:forEach var="s" items="${daftarAda}">
												<tr>
													<td style="background-color: white; text-align: left;">${s.key}</td>
												</tr>
											</c:forEach>
									</table>
								</fieldset>
							</th>
						</tr>
					</table>			

				</form>
			</div>
		</div>
	</div>

</form>
</body>
<%@ include file="/include/page/footer.jsp"%>