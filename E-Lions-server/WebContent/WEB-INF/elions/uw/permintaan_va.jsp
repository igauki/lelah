<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />

<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->

<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		$("#tabs").tabs();
		
		if('${showTab}' !== '') {
		    $('a[href=#${showTab}]').click();
		}
		
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy",
			showOn: 'both',
			buttonImage: '${path}/include/image/calendar.jpg',
			buttonImageOnly: true,
			buttonText: 'Select Date'
		});

		//function untuk halaman ini
		$('.numeric').bind('keypress', function (e) {
		    return !(e.which != 8 && e.which != 0 &&
		            (e.which < 48 || e.which > 57) && e.which != 46);
	    });
		    
		$('#f_list').hide();
		$("#cetak").hide();
		
		//
		$("#aa").html("8006");
		$("#zz").html("0");
		$("#xx").html("0");
		$("#yy").html(($("#tgl").val()).substr($("#tgl").val().length - 2));
		$("#pp").html("01");
		
		var m_id = '${msv_id}';
		if(m_id!=null && m_id!=''){
			$("#msv_id").val(m_id);
			$('#f_list').show();
			$('#f_permintaan').hide();
		}else{
			$('#f_list').hide();
			$('#f_permintaan').show();
		}
		
		$('#btnNew').click(function(){
			window.location.href = '${path}/uw/uw.htm?window=permintaan_va';
		});
		
		
		//jika pilih tanggal
		$('#tgl').change(function(){
			$("#yy").html(($("#tgl").val()).substr($("#tgl").val().length - 2));
		});

		//jika pilih jenis va
		$('input:radio[name="jn_va"]').change(function(){
			if($('input[name=jn_va]:checked', '#formPost').val()==0){
				if($('input[name=jn_produk]:checked', '#formPost').val()==0){
					if($('input[name=jn_spaj]:checked', '#formPost').val()==20){
						$("#aa").html("7720");
						$("#xx").html("0");
					}else{
						$("#xx").html("0");
						$("#aa").html("8006");
					}
				}else if($('input[name=jn_produk]:checked', '#formPost').val()==1){
					if($('input[name=jn_spaj]:checked', '#formPost').val()==20){
						$("#aa").html("7721");
						$("#xx").html("1");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()==22){
						$("#aa").html("71404");
						$("#zz").html("2");
						$("#xx").html("2");
						$("#yy").html("3");
						$("#pp").html(($("#tgl").val()).substr($("#tgl").val().length - 4));
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()==23){
						$("#aa").html("71404");
						$("#zz").html("2");
						$("#xx").html("2");
						$("#yy").html("4");
						$("#pp").html(($("#tgl").val()).substr($("#tgl").val().length - 4));
					}else{
						$("#xx").html("1");
						$("#aa").html("8076");
					}
				}
			}
			else if($('input[name=jn_va]:checked', '#formPost').val()==1){
				if($('input[name=jn_produk]:checked', '#formPost').val()==0){
					if($('input[name=jn_spaj]:checked', '#formPost').val()==17){
						$("#xx").html("2");
						$("#aa").html("8387");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()==20){
						$("#xx").html("2");
						$("#aa").html("7720");
					}else{
						$("#xx").html("2");
						$("#aa").html("8006");
					}
				}else if($('input[name=jn_produk]:checked', '#formPost').val()==1){
					if($('input[name=jn_spaj]:checked', '#formPost').val()==20){
						$("#xx").html("3");
						$("#aa").html("7721");
					}else{
						$("#xx").html("3");
						$("#aa").html("8076");
					}
				}
				
			}
			else if($('input[name=jn_va]:checked', '#formPost').val()==2){
				$("#xx").html("4");
				$("#aa").html("8093");
			}
			else if($('input[name=jn_va]:checked', '#formPost').val()==3){
				$("#xx").html("5");
				$("#aa").html("8093");
			}
	      	
	      	//generate format baru untuk nomor virtual account
	      	VAFormatBaru();
		});
		
		//jika pilih jenis produk
		$('input:radio[name="jn_produk"]').change(function(){
			if($('input[name=jn_produk]:checked', '#formPost').val()==0){
				if($('input[name=jn_va]:checked', '#formPost').val()==0){
					$("#xx").html("0");
					$("#aa").html("8006");
				}
				else if($('input[name=jn_va]:checked', '#formPost').val()==1){
					$("#xx").html("2");
					$("#aa").html("8006");
				}
				
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='01')$("#pp").html("01");
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='02')$("#pp").html("02");
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='03')$("#pp").html("03");
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='04')$("#pp").html("04");
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='05')$("#pp").html("05");
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='06')$("#pp").html("06");
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='07')$("#pp").html("07");
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='08')$("#pp").html("08");
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='12')$("#pp").html("12");
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='15')$("#pp").html("15");
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='17'){
				$("#pp").html("17");
				$("#aa").html("8387");
				}
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='20'){
				$("#pp").html("14");
				$("#aa").html("7720");
				}
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='21')$("#pp").html("16");
			}
			else if($('input[name=jn_produk]:checked', '#formPost').val()==1){
				if($('input[name=jn_va]:checked', '#formPost').val()==0){
					$("#xx").html("1");
					$("#aa").html("8076");
				}
				else if($('input[name=jn_va]:checked', '#formPost').val()==1){
					$("#xx").html("3");
					$("#aa").html("8076");
				}
				
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='01')$("#pp").html("09");
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='02')$("#pp").html("10");
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='03')$("#pp").html("11");
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='04')$("#pp").html("04");
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='05')$("#pp").html("13");
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='06')$("#pp").html("06");
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='07')$("#pp").html("14");
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='08')$("#pp").html("08");
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='12')$("#pp").html("12");
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='15')$("#pp").html("15");
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='17')$("#pp").html("17");
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='20'){
				$("#pp").html("14");
				$("#aa").html("7721");
				}
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='21')$("#pp").html("16");
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='22'){
					$("#pp").html(($("#tgl").val()).substr($("#tgl").val().length - 4));
					$("#aa").html("71404");
					$("#zz").html("2");
					$("#xx").html("2");
					$("#yy").html("3");
				}
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='23'){
					$("#pp").html(($("#tgl").val()).substr($("#tgl").val().length - 4));
					$("#aa").html("71404");
					$("#zz").html("2");
					$("#xx").html("2");
					$("#yy").html("4");
				}
			}
			
			//generate format baru untuk nomor virtual account
	      	VAFormatBaru();
		});
		
		//jika pilih jenis link
		$('input:radio[name="jn_link"]').change(function(){
			if($('input[name=jn_spaj]:checked', '#formPost').val()!='22' && $('input[name=jn_spaj]:checked', '#formPost').val()!='23'){
				if($('input[name=jn_link]:checked', '#formPost').val()==0)$("#zz").html("0");
				else if($('input[name=jn_link]:checked', '#formPost').val()==1)$("#zz").html("1");
			}
		});
		
		//jika merubah jml permintaan
		$('#amount_req').change(function(){
			if($('input[name=jn_va]:checked', '#formPost').val()==0){
				var ctr = 0;
				
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='01'){
					if($('input[name=jn_produk]:checked', '#formPost').val()==1)ctr = parseInt('${paper_ul_syariah }');
					else ctr = parseInt('${paper_ul }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='02'){
					if($('input[name=jn_produk]:checked', '#formPost').val()==1)ctr = parseInt('${paper_nul_syariah }');
					else ctr = parseInt('${paper_nul }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='03'){
					if($('input[name=jn_produk]:checked', '#formPost').val()==1)ctr = parseInt('${paper_simpol_syariah }');
					else ctr = parseInt('${paper_simpol }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='04'){
					ctr = parseInt('${paper_sprima }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='05'){
					if($('input[name=jn_produk]:checked', '#formPost').val()==1)ctr = parseInt('${paper_psave_syariah }');
					else ctr = parseInt('${paper_psave }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='06'){
					ctr = parseInt('${paper_slinksatu }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='07'){
					if($('input[name=jn_produk]:checked', '#formPost').val()==1)ctr = parseInt('${paper_kcl_syariah }');
					else ctr = parseInt('${paper_kcl }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='08'){
					ctr = parseInt('${paper_familyplan }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='12'){
					ctr = parseInt('${paper_powersave_syariah }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='15'){
					ctr = parseInt('${paper_danamas_prima }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='17'){
					ctr = parseInt('${paper_smultimate }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='20'){
					ctr = parseInt('${paper_harda }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='21'){
					ctr = parseInt('${paper_primemagna }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='22'){
					ctr = parseInt('${paper_btn_syariah_life }');
				}
				
				var jml_awal = ctr + 1;
				var jml_akhir = ctr + parseInt($('#amount_req').val());
				
				$('#start_no_va_req').val(jml_awal);
				$('#end_no_va_req').val(jml_akhir);
				
				$('#start_no_va_cetak').val(jml_awal);
				$('#end_no_va_cetak').val(jml_akhir);
			}else if($('input[name=jn_va]:checked', '#formPost').val()==1){
				var ctr = 0;
				
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='01'){
					if($('input[name=jn_produk]:checked', '#formPost').val()==1)ctr = parseInt('${gadget_ul_syariah }');
					else ctr = parseInt('${gadget_ul }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='02'){
					if($('input[name=jn_produk]:checked', '#formPost').val()==1)ctr = parseInt('${gadget_nul_syariah }');
					else ctr = parseInt('${gadget_nul }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='03'){
					if($('input[name=jn_produk]:checked', '#formPost').val()==1)ctr = parseInt('${gadget_simpol_syariah }');
					else ctr = parseInt('${gadget_simpol }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='04'){
					ctr = parseInt('${gadget_sprima }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='05'){
					if($('input[name=jn_produk]:checked', '#formPost').val()==1)ctr = parseInt('${gadget_psave_syariah }');
					else ctr = parseInt('${gadget_psave }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='06'){
					ctr = parseInt('${gadget_slinksatu }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='07'){
					if($('input[name=jn_produk]:checked', '#formPost').val()==1)ctr = parseInt('${gadget_kcl_syariah }');
					else ctr = parseInt('${gadget_kcl }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='08'){
					ctr = parseInt('${gadget_familyplan }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='12'){
					ctr = parseInt('${gadget_powersave_syariah }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='15'){
					ctr = parseInt('${gadget_danamas_prima }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='17'){
					ctr = parseInt('${gadget_smultimate }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='20'){
					ctr = parseInt('${paper_harda }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='21'){
					ctr = parseInt('${paper_primemagna }');
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='22'){
					ctr = parseInt('${paper_btn_syariah_life }');
				}
				
				var jml_awal = ctr + 1;
				var jml_akhir = ctr + parseInt($('#amount_req').val());
				
				$('#start_no_va_req').val(jml_awal);
				$('#end_no_va_req').val(jml_akhir);
				
				$('#start_no_va_cetak').val(jml_awal);
				$('#end_no_va_cetak').val(jml_akhir);
			}else if($('input[name=jn_va]:checked', '#formPost').val()==2){
				var ctr = 0;
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='07'){
					ctr = parseInt('${l_konven }');
					
					var jml_awal = ctr + 1;
					var jml_akhir = ctr + parseInt($('#amount_req').val());
					
					$('#start_no_va_req').val(jml_awal);
					$('#end_no_va_req').val(jml_akhir);
					
					$('#start_no_va_cetak').val(jml_awal);
					$('#end_no_va_cetak').val(jml_akhir);
				}else{
					alert('Produk hanya bisa kecelakaan');
					$('#amount_req').val(null);
				};
			}else if($('input[name=jn_va]:checked', '#formPost').val()==3){
				var ctr = 0;
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='07'){
					ctr = parseInt('${lainnya }');
					
					var jml_awal = ctr + 1;
					var jml_akhir = ctr + parseInt($('#amount_req').val());
					
					$('#start_no_va_req').val(jml_awal);
					$('#end_no_va_req').val(jml_akhir);
					
					$('#start_no_va_cetak').val(jml_awal);
					$('#end_no_va_cetak').val(jml_akhir);
				}else{
					alert('Produk hanya bisa kecelakaan');
					$('#amount_req').val(null);
				};
			};
		});
		
		$('#btnShow').click(function(){
			if($('#msv_id').val()==null || $('#msv_id').val()==''){
				alert("Harap pilih id permintaan!");
			}else{
				window.location.href = '${path}/uw/uw.htm?window=permintaan_va&btnShow=1&msv_id='+$('#msv_id').val();			
			}
		});
		
		$('#btnExcel').click(function(){
			if($('#msv_id').val()==null || $('#msv_id').val()==''){
				alert("Harap pilih id permintaan!");
			}else{
				window.location.href = '${path}/uw/uw.htm?window=permintaan_va&btnShow=1&btnExcel=1&msv_id='+$('#msv_id').val();			
			}
		});
		
		$('#msv_id').click(function(){
			window.location.href = '${path}/uw/uw.htm?window=permintaan_va&btnShow=1&msv_id='+$('#msv_id').val();			
		});
		
		//jika memilih jenis va
		$('input:radio[name="jn_va"]').change(function(){
			if($('input[name=jn_va]:checked', '#formPost').val()==1){
				//$("#start_no_va_cetak").attr("readonly","readonly");
				//$("#end_no_va_cetak").attr("readonly","readonly");
				$("#cetak").hide();
				
				if($('#amount_req').val()!='' && $('#amount_req').val()!=null){
					var ctr = 0;
				
					if($('input[name=jn_spaj]:checked', '#formPost').val()=='01'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1)ctr = parseInt('${gadget_ul_syariah }');
						else ctr = parseInt('${gadget_ul }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='02'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1)ctr = parseInt('${gadget_nul_syariah }');
						else ctr = parseInt('${gadget_nul }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='03'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1)ctr = parseInt('${gadget_simpol_syariah }');
						else ctr = parseInt('${gadget_simpol }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='04'){
						ctr = parseInt('${gadget_sprima }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='05'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1)ctr = parseInt('${gadget_psave_syariah }');
						else ctr = parseInt('${gadget_psave }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='06'){
						ctr = parseInt('${gadget_slinksatu }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='07'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1)ctr = parseInt('${gadget_kcl_syariah }');
						else ctr = parseInt('${gadget_kcl }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='08'){
						ctr = parseInt('${gadget_familyplan }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='12'){
						ctr = parseInt('${gadget_powersave_syariah }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='15'){
						ctr = parseInt('${gadget_danamas_prima }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='17'){
						ctr = parseInt('${gadget_smultimate }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='20'){
						ctr = parseInt('${paper_harda }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='21'){
						ctr = parseInt('${paper_primemagna }');
					}
					
					
					var jml_awal = ctr + 1;
					var jml_akhir = ctr + parseInt($('#amount_req').val());
					
					$('#start_no_va_req').val(jml_awal);
					$('#end_no_va_req').val(jml_akhir);
					
					$('#start_no_va_cetak').val(jml_awal);
					$('#end_no_va_cetak').val(jml_akhir);
				}
			}else{
				//$("#start_no_va_cetak").removeAttr("readonly","readonly");
				//$("#end_no_va_cetak").removeAttr("readonly","readonly");
				$("#cetak").hide();
				
				if($('#amount_req').val()!='' && $('#amount_req').val()!=null){
					var ctr = 0;
				
					if($('input[name=jn_spaj]:checked', '#formPost').val()=='01'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1)ctr = parseInt('${paper_ul_syariah }');
						else ctr = parseInt('${paper_ul }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='02'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1)ctr = parseInt('${paper_nul_syariah }');
						else ctr = parseInt('${paper_nul }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='03'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1)ctr = parseInt('${paper_simpol_syariah }');
						else ctr = parseInt('${paper_simpol }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='04'){
						ctr = parseInt('${paper_sprima }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='05'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1)ctr = parseInt('${paper_psave_syariah }');
						else ctr = parseInt('${paper_psave }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='06'){
						ctr = parseInt('${paper_slinksatu }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='07'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1)ctr = parseInt('${paper_kcl_syariah }');
						else ctr = parseInt('${paper_kcl }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='08'){
						ctr = parseInt('${paper_familyplan }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='12'){
						ctr = parseInt('${paper_powersave_syariah }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='15'){
						ctr = parseInt('${paper_danamas_prima }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='17'){
						ctr = parseInt('${paper_smultimate }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='20'){
						ctr = parseInt('${paper_harda }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='21'){
						ctr = parseInt('${paper_primemagna }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='22'){
						ctr = parseInt('${paper_btn_syariah_life }');
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='23'){
						ctr = parseInt('${paper_btn_syariah_link }');
					}
					
					var jml_awal = ctr + 1;
					var jml_akhir = ctr + parseInt($('#amount_req').val());
					
					$('#start_no_va_req').val(jml_awal);
					$('#end_no_va_req').val(jml_akhir);
					
					$('#start_no_va_cetak').val(jml_awal);
					$('#end_no_va_cetak').val(jml_akhir);
				};
			};
		});
		
		//jika memilih jenis spaj
		$('input:radio[name="jn_spaj"]').change(function(){
			$("#cetak").hide();
			if($('#amount_req').val()!='' && $('#amount_req').val()!=null){
				if($('input[name=jn_va]:checked', '#formPost').val()==0){
					var ctr = 0;
					
					if($('input[name=jn_spaj]:checked', '#formPost').val()=='01'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1){
							ctr = parseInt('${paper_ul_syariah }');
							$("#pp").html("09");
						}else{
							ctr = parseInt('${paper_ul }');
							$("#pp").html("01");
						}
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='02'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1){
							ctr = parseInt('${paper_nul_syariah }');
							$("#pp").html("10");
						}else{
							ctr = parseInt('${paper_nul }');
							$("#pp").html("02");
						}
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='03'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1){
							ctr = parseInt('${paper_simpol_syariah }');
							$("#pp").html("11");
						}else{
							ctr = parseInt('${paper_simpol }');
							$("#pp").html("03");
						}
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='04'){
						ctr = parseInt('${paper_sprima }');
						$("#pp").html("04");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='05'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1){
							ctr = parseInt('${paper_psave_syariah }');
							$("#pp").html("13");
						}else{
							ctr = parseInt('${paper_psave }');
							$("#pp").html("05");
						}
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='06'){
						ctr = parseInt('${paper_slinksatu }');
						$("#pp").html("06");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='07'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1){
							ctr = parseInt('${paper_kcl_syariah }');
							$("#pp").html("14");
						}else{
							ctr = parseInt('${paper_kcl }');
							$("#pp").html("07");						
						}
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='08'){
						ctr = parseInt('${paper_familyplan }');
						$("#pp").html("08");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='12'){
						ctr = parseInt('${paper_powersave_syariah }');
						$("#pp").html("12");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='15'){
						ctr = parseInt('${paper_danamas_prima }');
						$("#pp").html("15");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='17'){
						ctr = parseInt('${paper_smultimate }');
						$("#pp").html("17");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='20'){
						ctr = parseInt('${paper_harda }');
						$("#pp").html("14");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='21'){
						ctr = parseInt('${paper_primemagna }');
						$("#pp").html("16");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='22'){
						ctr = parseInt('${paper_btn_syariah_life }');
						$("#pp").html(($("#tgl").val()).substr($("#tgl").val().length - 4));
						$("#zz").html("2");
						$("#xx").html("2");
						$("#yy").html("3");
						$("#aa").html("71404");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='23'){
						ctr = parseInt('${paper_btn_syariah_link }');
						$("#pp").html(($("#tgl").val()).substr($("#tgl").val().length - 4));
						$("#zz").html("2");
						$("#xx").html("2");
						$("#yy").html("4");
						$("#aa").html("71404");
					}
					
					var jml_awal = ctr + 1;
					var jml_akhir = ctr + parseInt($('#amount_req').val());
					
					$('#start_no_va_req').val(jml_awal);
					$('#end_no_va_req').val(jml_akhir);
					
					$('#start_no_va_cetak').val(jml_awal);
					$('#end_no_va_cetak').val(jml_akhir);
				}else if($('input[name=jn_va]:checked', '#formPost').val()==1){
					var ctr = 0;
					
					if($('input[name=jn_spaj]:checked', '#formPost').val()=='01'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1){
							ctr = parseInt('${gadget_ul_syariah }');
							$("#pp").html("09");
						}else{
							ctr = parseInt('${gadget_ul }');
							$("#pp").html("01");						
						}
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='02'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1){
							ctr = parseInt('${gadget_nul_syariah }');
							$("#pp").html("10");
						}else{
							ctr = parseInt('${gadget_nul }');
							$("#pp").html("02");						
						}
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='03'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1){
							ctr = parseInt('${gadget_simpol_syariah }');
							$("#pp").html("11");
						}else{
							ctr = parseInt('${gadget_simpol }');
							$("#pp").html("03");
						}
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='04'){
						ctr = parseInt('${gadget_sprima }');
						$("#pp").html("04");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='05'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1){
							ctr = parseInt('${gadget_psave_syariah }');
							$("#pp").html("13");
						}else{
							ctr = parseInt('${gadget_psave }');
							$("#pp").html("05");
						}
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='06'){
						ctr = parseInt('${gadget_slinksatu }');
						$("#pp").html("06");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='07'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1){
							ctr = parseInt('${gadget_kcl_syariah }');
							$("#pp").html("14");
						}else{
							ctr = parseInt('${gadget_kcl }');
							$("#pp").html("07");
						}
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='08'){
						ctr = parseInt('${gadget_familyplan }');
						$("#pp").html("08");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='12'){
						ctr = parseInt('${gadget_powersave_syariah }');
						$("#pp").html("12");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='15'){
						ctr = parseInt('${gadget_danamas_prima }');
						$("#pp").html("15");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='17'){
						ctr = parseInt('${gadget_smultimate }');
						$("#pp").html("17");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='20'){
						ctr = parseInt('${paper_harda }');
						$("#pp").html("14");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='21'){
						ctr = parseInt('${paper_primemagna }');
						$("#pp").html("16");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='22'){
						ctr = parseInt('${paper_btn_syariah_life }');
						$("#pp").html(($("#tgl").val()).substr($("#tgl").val().length - 4));
						$("#zz").html("2");
						$("#xx").html("2");
						$("#yy").html("3");
						$("#aa").html("71404");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='23'){
						ctr = parseInt('${paper_btn_syariah_link }');
						$("#pp").html(($("#tgl").val()).substr($("#tgl").val().length - 4));
						$("#zz").html("2");
						$("#xx").html("2");
						$("#yy").html("4");
						$("#aa").html("71404");
					}
					
					var jml_awal = ctr + 1;
					var jml_akhir = ctr + parseInt($('#amount_req').val());
					
					$('#start_no_va_req').val(jml_awal);
					$('#end_no_va_req').val(jml_akhir);
					
					$('#start_no_va_cetak').val(jml_awal);
					$('#end_no_va_cetak').val(jml_akhir);
				};
			}else{
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='01'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1)$("#pp").html("09");
						else $("#pp").html("01");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='02'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1)$("#pp").html("10");
						else $("#pp").html("02");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='03'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1)$("#pp").html("11");
						else $("#pp").html("03");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='04'){
						$("#pp").html("04");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='05'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1)$("#pp").html("13");
						else $("#pp").html("05");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='06'){
						$("#pp").html("06");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='07'){
						if($('input[name=jn_produk]:checked', '#formPost').val()==1)$("#pp").html("14");
						else $("#pp").html("07");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='08'){
						$("#pp").html("08");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='12'){
						$("#pp").html("12");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='15'){
						$("#pp").html("15");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='17'){
						$("#pp").html("17");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='20'){
						$("#pp").html("14");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='21'){
						$("#pp").html("16");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='22'){
						ctr = parseInt('${paper_btn_syariah_life }');
						$("#pp").html(($("#tgl").val()).substr($("#tgl").val().length - 4));
						$("#zz").html("2");
						$("#xx").html("2");
						$("#yy").html("3");
						$("#aa").html("71404");
					}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='23'){
						ctr = parseInt('${paper_btn_syariah_link }');
						$("#pp").html(($("#tgl").val()).substr($("#tgl").val().length - 4));
						$("#zz").html("2");
						$("#xx").html("2");
						$("#yy").html("4");
						$("#aa").html("71404");
					};
			};	
		
			//generate format baru untuk nomor virtual account
	      	VAFormatBaru();
		});
	});
	
	function VAFormatBaru(){ //generate format baru untuk nomor virtual account
		if($('input[name=jn_va]:checked', '#formPost').val()==0){ //paper
			if($('input[name=jn_produk]:checked', '#formPost').val()==0){ //konven
			}else if($('input[name=jn_produk]:checked', '#formPost').val()==1){ //syariah
				$("#aa").html("87111");
				
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='02'){
					$("#pp").html("10");
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='21'){
					$("#pp").html("16");
				}
			}
		}else if($('input[name=jn_va]:checked', '#formPost').val()==1){ //online
			if($('input[name=jn_produk]:checked', '#formPost').val()==0){ //konven
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='17'){
					$("#aa").html("87222");
					$("#pp").html("17");
				}
			}else if($('input[name=jn_produk]:checked', '#formPost').val()==1){ //syariah
				$("#aa").html("87111");
				
				if($('input[name=jn_spaj]:checked', '#formPost').val()=='02'){
					$("#xx").html("3");
					$("#pp").html("10");
				}else if($('input[name=jn_spaj]:checked', '#formPost').val()=='21'){
					$("#xx").html("3");
					$("#pp").html("16");
				}
			}
		}
	}
	
	function ValidateForm(){
		
        if ($("#amount_req").val() == null || $("#amount_req").val() == "") {
            $("#amount_req").focus();
            alert("Harap isi jumlah request!");
            return false;
        }
         if ($("#jbank").val() == null || $("#jbank").val() == "") {
            $("#jbank").focus();
            alert("Harap isi Jenis Bank!");
            return false;
        }
        
        if ($("#start_no_va_req").val() == null || $("#start_no_va_req").val() == "") {
            $("#start_no_va_req").focus();
            alert("Harap isi nomor awal request!");
            return false;
        }
        if ($("#end_no_va_req").val() == null || $("#end_no_va_req").val() == "") {
            $("#end_no_va_req").focus();
            alert("Harap isi nomor akhir request!");
            return false;
        }
        
        if($('input[name=jn_va]:checked', '#formPost').val()==0){
        	if ($("#start_no_va_cetak").val() == null || $("#start_no_va_cetak").val() == "") {
	            $("#start_no_va_cetak").focus();
	            alert("Harap isi nomor awal cetak!");
	            return false;
	        }
	        if ($("#end_no_va_cetak").val() == null || $("#end_no_va_cetak").val() == "") {
	            $("#end_no_va_cetak").focus();
	            alert("Harap isi nomor akhir cetak!");
	            return false;
	        }
        }
        
        return true;
    }
	
	hideLoadingMessage();
</script>


<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

</style>

<body>
	<div id="tabs">		
		<ul>
			<li><a href="#tab-1">Permintaan Virtual Account</a></li>	
			<!-- <li><a href="#tab-2">Reset Counter</a></li>	 -->			
			<li><a href="#tab-3">Detail Virtual Account</a></li>
		</ul>
		
		<div id="tab-1">			
			<form id="formPost" name="formPost" method="post" onSubmit="return ValidateForm()">			 
				<table class="entry2">
					<tr>
						<th style="width: 120px;" valign="top">
							<input type="button" id="btnShow" name="btnShow" value="Show" />
							<input type="button" id="btnNew" name="btnNew" value="New" /><br><br>
							<select id="msv_id" name="msv_id" style="width: 110px;" size="${sessionScope.currentUser.comboBoxSize}">
								<c:forEach items="${id }" var="m" varStatus="s">
									<option value="${m.key }">${m.value }</option>
								</c:forEach>
							</select>
						</th>
						<td valign="top">
							<fieldset>
							<!-- untuk for permintaan -->
							<div id="f_permintaan">
								<legend>Permintaan Virtual Account</legend>
									<table class="entry2">
										<tr>
											<th style="width: 180px;text-align: left;">Pemohon</th>
											<td><input type="text" id="pemohon" name="pemohon" value="${sessionScope.currentUser.lus_full_name}" class="readOnly" readonly="readonly"/></td>
										</tr>
										<tr>
											<th style="width: 180px;text-align: left;">Jenis Bank</th>
											<td>
											<select id="jbank" name="jbank">
												<option value="" selected="selected"></option>
												<c:forEach items="${jbank}" var="j">J_BANK
												<option value="${j.J_BANK}">${j.LSBP_NAMA}</option>
												</c:forEach>
											</select>
											</td>
										</tr>
										<tr>
											<th style="width: 180px;text-align: left;">Tanggal</th>
											<td><input type="text" id="tgl" name="tgl" value="${tgl}" class="datepicker"/></td>
										</tr>
										<tr>
											<th style="width: 180px;text-align: left;">Counter</th>
											<td>
												<table class="entry2">
													<tr>
														<th colspan="2">PAPER</th>
														<th colspan="2">GADGET</th>
													</tr>
													<tr>
														<th style="width: 30%;text-align: left;">Paper Unit Link</th><td><strong style="color: #FF0000;">${paper_ul }</strong></td>
														<th style="width: 30%;text-align: left;">Gadget Unit Link</th><td><strong style="color: #FF0000;">${gadget_ul }</strong></td>
													</tr>
													<!-- syariah -->
													<tr>
														<th style="width: 30%;text-align: left;">Paper Unit Link Syariah</th><td><strong style="color: #FF0000;">${paper_ul_syariah }</strong></td>
														<th style="width: 30%;text-align: left;">Gadget Unit Link Syariah</th><td><strong style="color: #FF0000;">${gadget_ul_syariah }</strong></td>
													</tr>
													<!-- end syariah -->
													<tr>
														<td>&nbsp;</td><td>&nbsp;</td>
														<td>&nbsp;</td><td>&nbsp;</td>
													</tr>
													<tr>
														<th style="width: 30%;text-align: left;">Paper Non Unit Link</th><td><strong style="color: #FF0000;">${paper_nul }</strong></td>
														<th style="width: 30%;text-align: left;">Gadget Non Unit Link</th><td><strong style="color: #FF0000;">${gadget_nul }</strong></td>
													</tr>
													<!-- syariah -->
													<tr>
														<th style="width: 30%;text-align: left;">Paper Non Unit Link Syariah</th><td><strong style="color: #FF0000;">${paper_nul_syariah }</strong></td>
														<th style="width: 30%;text-align: left;">Gadget Non Unit Link Syariah</th><td><strong style="color: #FF0000;">${gadget_nul_syariah }</strong></td>
													</tr>
													<!-- end syariah -->
													<tr>
														<td>&nbsp;</td><td>&nbsp;</td>
														<td>&nbsp;</td><td>&nbsp;</td>
													</tr>
													<tr>
														<th style="width: 30%;text-align: left;">Paper Simpol</th><td><strong style="color: #FF0000;">${paper_simpol }</strong></td>
														<th style="width: 30%;text-align: left;">Gadget Simpol</th><td><strong style="color: #FF0000;">${gadget_simpol }</strong></td>
													</tr>
													<!-- syariah -->
													<tr>
														<th style="width: 30%;text-align: left;">Paper Simpol Syariah</th><td><strong style="color: #FF0000;">${paper_simpol_syariah }</strong></td>
														<th style="width: 30%;text-align: left;">Gadget Simpol Syariah</th><td><strong style="color: #FF0000;">${gadget_simpol_syariah }</strong></td>
													</tr>
													<!-- end syariah -->
													<tr>
														<td>&nbsp;</td><td>&nbsp;</td>
														<td>&nbsp;</td><td>&nbsp;</td>
													</tr>
													<tr>
														<th style="width: 30%;text-align: left;">Paper Simas Prima</th><td><strong style="color: #FF0000;">${paper_sprima }</strong></td>
														<th style="width: 30%;text-align: left;">Gadget Simas Prima</th><td><strong style="color: #FF0000;">${gadget_sprima }</strong></td>
													</tr>
													<!-- syariah -->
													<tr>
														<th style="width: 30%;text-align: left;">Paper Power Save Syariah</th><td><strong style="color: #FF0000;">${paper_powersave_syariah }</strong></td>
														<th style="width: 30%;text-align: left;">Gadget Power Save Syariah</th><td><strong style="color: #FF0000;">${gadget_powersave_syariah }</strong></td>
													</tr>
													<!-- end syariah -->
													<tr>
														<td>&nbsp;</td><td>&nbsp;</td>
														<td>&nbsp;</td><td>&nbsp;</td>
													</tr>
													<tr>
														<th style="width: 30%;text-align: left;">Paper Produk Save</th><td><strong style="color: #FF0000;">${paper_psave }</strong></td>
														<th style="width: 30%;text-align: left;">Gadget Produk Save</th><td><strong style="color: #FF0000;">${gadget_psave }</strong></td>
													</tr>
													<!-- syariah -->
													<tr>
														<th style="width: 30%;text-align: left;">Paper Produk Save Syariah</th><td><strong style="color: #FF0000;">${paper_psave_syariah }</strong></td>
														<th style="width: 30%;text-align: left;">Gadget Produk Save Syariah</th><td><strong style="color: #FF0000;">${gadget_psave_syariah }</strong></td>
													</tr>
													<!-- end syariah -->
													<tr>
														<td>&nbsp;</td><td>&nbsp;</td>
														<td>&nbsp;</td><td>&nbsp;</td>
													</tr>
													<tr>
														<th style="width: 30%;text-align: left;">Paper SMiLe Link Satu</th><td><strong style="color: #FF0000;">${paper_slinksatu }</strong></td>
														<th style="width: 30%;text-align: left;">Gadget SMiLe Link Satu</th><td><strong style="color: #FF0000;">${gadget_slinksatu }</strong></td>
													</tr>
													<tr>
														<th style="width: 30%;text-align: left;">Paper Danamas Prima</th><td><strong style="color: #FF0000;">${paper_danamas_prima }</strong></td>
														<th style="width: 30%;text-align: left;">Gadget Danamas Prima</th><td><strong style="color: #FF0000;">${gadget_danamas_prima }</strong></td>
													</tr>
													<tr>
														<td>&nbsp;</td><td>&nbsp;</td>
														<td>&nbsp;</td><td>&nbsp;</td>
													</tr>
													<tr>
														<th style="width: 30%;text-align: left;">Paper Kecelakaan</th><td><strong style="color: #FF0000;">${paper_kcl }</strong></td>
														<th style="width: 30%;text-align: left;">Gadget Kecelakaan</th><td><strong style="color: #FF0000;">${gadget_kcl }</strong></td>
													</tr>
													<!-- syariah -->
													<tr>
														<th style="width: 30%;text-align: left;">Paper Kecelakaan Syariah</th><td><strong style="color: #FF0000;">${paper_kcl_syariah }</strong></td>
														<th style="width: 30%;text-align: left;">Gadget Kecelakaan Syariah</th><td><strong style="color: #FF0000;">${gadget_kcl_syariah }</strong></td>
													</tr>
													<!-- end syariah -->
													<tr>
														<td>&nbsp;</td><td>&nbsp;</td>
														<td>&nbsp;</td><td>&nbsp;</td>
													</tr>
													<tr>
														<th style="width: 30%;text-align: left;">Paper VIP Family Plan</th><td><strong style="color: #FF0000;">${paper_familyplan }</strong></td>
														<th style="width: 30%;text-align: left;">Gadget VIP Family Plan</th><td><strong style="color: #FF0000;">${gadget_familyplan }</strong></td>
													</tr>
													<tr>
														<th style="width: 30%;text-align: left;">Paper Smile Link Ultimate</th><td><strong style="color: #FF0000;">${paper_smultimate }</strong></td>
														<th style="width: 30%;text-align: left;">Gadget Smile Link Ultimate</th><td><strong style="color: #FF0000;">${gadget_smultimate }</strong></td>
													</tr>
													<tr>
														<td>&nbsp;</td><td>&nbsp;</td>
														<td>&nbsp;</td><td>&nbsp;</td>
													</tr>
													<tr>
														<th style="width: 30%;text-align: left;">Paper Bank Harda</th><td><strong style="color: #FF0000;">${paper_harda }</strong></td>
														<th style="width: 30%;text-align: left;">Paper Smile Life Syariah</th><td><strong style="color: #FF0000;">${paper_btn_syariah_life }</strong></td>
													</tr>
													<tr>
														<th style="width: 30%;text-align: left;">Paper Prime Magna Link</th><td><strong style="color: #FF0000;">${paper_primemagna }</strong></td>
														<th style="width: 30%;text-align: left;">Paper Smile LInk Pro Syariah</th><td><strong style="color: #FF0000;">${paper_btn_syariah_link }</strong></td>
													</tr>
												</table>
											</td>
										</tr>
										<%-- <tr>
											<th style="width: 180px;text-align: left;">Counter Lainnya</th>
											<td>
												<table class="entry2">
													<tr>
														<th style="width: 180px;text-align: left;">Lain-lain Konven</th><td><strong style="color: #FF0000;">${l_konven }</strong></td>
													</tr>
													<tr>
														<th style="width: 180px;text-align: left;">Lain-lain</th><td><strong style="color: #FF0000;">${lainnya }</strong></td>
													</tr>
												</table>
											</td>
										</tr> --%>
										<tr>
											<th style="width: 180px;text-align: left;">Jenis SPAJ</th>
											<td>
												<table class="entry2" style="width: 600px;">
													<tr>
														<td><input type="radio" name="jn_spaj" id="jn_spaj1" value="01" checked="checked" style="border: none;">Unit Link&nbsp;</td>
														<td><input type="radio" name="jn_spaj" id="jn_spaj2" value="02" style="border: none;">Non Unit Link&nbsp;</td>
														<td><input type="radio" name="jn_spaj" id="jn_spaj3" value="03" style="border: none;">Simpol&nbsp;</td>
														<td><input type="radio" name="jn_spaj" id="jn_spaj4" value="04" style="border: none;">Simas Prima&nbsp;</td>
													</tr>
													<tr>
														<td><input type="radio" name="jn_spaj" id="jn_spaj5" value="05" style="border: none;">Produk Save&nbsp;</td>
														<td><input type="radio" name="jn_spaj" id="jn_spaj6" value="06" style="border: none;">SMiLe Link Satu&nbsp;</td>
														<td><input type="radio" name="jn_spaj" id="jn_spaj7" value="07" style="border: none;">Kecelakaan&nbsp;</td>
														<td><input type="radio" name="jn_spaj" id="jn_spaj8" value="08" style="border: none;">VIP Family Plan&nbsp;</td>
													</tr>
													<tr>
														<td><input type="radio" name="jn_spaj" id="jn_spaj5" value="12" style="border: none;">Power Save Syariah&nbsp;</td>
														<td><input type="radio" name="jn_spaj" id="jn_spaj6" value="15" style="border: none;">Danamas Prima&nbsp;</td>
														<td><input type="radio" name="jn_spaj" id="jn_spaj17" value="17" style="border: none;">Smile Link Ultimate&nbsp;</td>
														<td><input type="radio" name="jn_spaj" id="jn_spaj20" value="20" style="border: none;">Superlink&nbsp;</td>
														
													</tr>
													<tr>
														<td><input type="radio" name="jn_spaj" id="jn_spaj21" value="21" style="border: none;">Prime Magna Link&nbsp;</td>
														<td><input type="radio" name="jn_spaj" id="jn_spaj22" value="22" style="border: none;">SMiLe Life Syariah&nbsp;</td>
														<td><input type="radio" name="jn_spaj" id="jn_spaj23" value="23" style="border: none;">SMiLe Link Pro Syariah&nbsp;</td>
														<td>&nbsp;</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<th style="width: 180px;text-align: left;">Jenis Virtual Account</th>
											<td>
												<input type="radio" name="jn_va" id="jn_va1" value="0" checked="checked" style="border: none;">Paper&nbsp;
												<input type="radio" name="jn_va" id="jn_va2" value="1" style="border: none;">Online
												<!-- <input type="radio" name="jn_va" id="jn_va3" value="2" style="border: none;">Lain-lain Konven
												<input type="radio" name="jn_va" id="jn_va4" value="3" style="border: none;">Lainnya -->
											</td>
										</tr>
										<tr>
											<th style="width: 180px;text-align: left;">Jenis Produk</th>
											<td>
												<input type="radio" name="jn_produk" id="jn_produk1" value="0" checked="checked" style="border: none;">Konvensional&nbsp;
												<input type="radio" name="jn_produk" id="jn_produk2" value="1" style="border: none;">Syariah
												<!-- <input type="radio" name="jn_produk" id="jn_produk3" value="2" style="border: none;">Harda (Khusus Bank Harda) -->
											</td>
										</tr>
										<tr>
											<th style="width: 180px;text-align: left;">Link/NonLink</th>
											<td>
												<input type="radio" name="jn_link" id="jn_link1" value="0" checked="checked" style="border: none;">Link&nbsp;
												<input type="radio" name="jn_link" id="jn_link2" value="1" style="border: none;">Non Link
											</td>
										</tr>
										<tr>
											<th style="width: 180px;text-align: left;">Amount Request</th>
											<td><input type="text" name="amount_req" id="amount_req" class="numeric" maxlength="6" size="6"/></td>
										</tr>
										<tr>
											<th style="width: 180px;text-align: left;">No Request</th>
											<td>
												<span id="aa"></span>-<span id="zz"></span>-<span id="xx"></span>-<span id="yy"></span>-<span id="pp"></span>-
												<input type="text" name="start_no_va_req" id="start_no_va_req" class="numeric readOnly" maxlength="6" size="6" readonly="readonly"/> - <input type="text" name="end_no_va_req" id="end_no_va_req" class="numeric readOnly" maxlength="6" size="6" readonly="readonly"/>
											</td>
										</tr>
										<tr id="cetak">
											<th style="width: 180px;text-align: left;">No Cetak</th>
											<td><input type="text" name="start_no_va_cetak" id="start_no_va_cetak" class="numeric readOnly" maxlength="6" size="6" readonly="readonly"/> - <input type="text" name="end_no_va_cetak" id="end_no_va_cetak" class="numeric readOnly" maxlength="6" size="6" readonly="readonly"/></td>
										</tr>
										<tr>
											<td colspan="2"></td>
										</tr>
										<tr>
											<td colspan="2"><input type="submit" name="btnSimpan" id="btnSimpan" value="Simpan"/></td>
										</tr>
									</table>
							</div>
							
							<!-- untuk view list virtual account -->
							<div id="f_list">
								<legend>List Virtual Account</legend>
								<input type="button" id="btnExcel" name="btnExcel" value="Export to Excel" />
								<table class="entry2">
									<tr>
										<th>No</th>
										<th>Virtual Account</th>
										<th>Kertas/Online</th>
										<th>Jenis Produk</th>
										<th>Link/Non Link</th>
										<th>User</th>
										<th>Spaj Temp</th>
										<th>Spaj</th>
									</tr>
									<c:if test="${not empty l_msv }">
									<pg:paging url="${path}/uw/uw.htm?window=permintaan_va&btnShow=1&msv_id=${msv_id }" pageSize="100">
									<c:forEach items="${l_msv }" var="l" varStatus="s">
										<pg:item>
										<tr>
											<td>${s.index +1 }</td>
											<td>${l.VIRTUAL_ACC }</td>
											<td>${l.JENIS_VA }</td>
											<td>${l.JENIS_SYARIAH }</td>
											<td>${l.JENIS_LINK }</td>
											<td>${l.USER_INPUT }</td>
											<td>${l.SPAJ_TEMP }</td>
											<td>${l.SPAJ }</td>
										</tr>
										</pg:item>
									</c:forEach>
										<pg:index>
											  	<pg:page><%=thisPage%></pg:page> 
										</pg:index>
									</pg:paging>
									</c:if>
								</table>
							</div>
							
							<c:if test="${not empty pesan }">
							<div id="success">
								${pesan }
							</div>
							</c:if>
							</fieldset>
						</td>
					</tr>
				</table>
			</form>			 
     	</div>	
     	<%-- <div id="tab-2">
     		<form id="formPost2" name="formPost2" method="post" >			 
				<table class="entry2">
					<tr>
						<th style="width: 180px;text-align: left;">Paper Unit Link</th><td><strong style="color: #FF0000;">${paper_ul }</strong>&nbsp;&nbsp;<input type="button" value="Reset" onclick="resetCounter('171')"/></td>
						<th style="width: 180px;text-align: left;">Gadget Unit Link</th><td><strong style="color: #FF0000;">${gadget_ul }</strong>&nbsp;&nbsp;<input type="button" value="Reset" onclick="resetCounter('172')"/></td>
					</tr>
					<tr>
						<th style="width: 180px;text-align: left;">Paper Non Unit Link</th><td><strong style="color: #FF0000;">${paper_nul }</strong>&nbsp;&nbsp;<input type="button" value="Reset" onclick="resetCounter('177')"/></td>
						<th style="width: 180px;text-align: left;">Gadget Non Unit Link</th><td><strong style="color: #FF0000;">${gadget_nul }</strong>&nbsp;&nbsp;<input type="button" value="Reset" onclick="resetCounter('178')"/></td>
					</tr>
					<tr>
						<th style="width: 180px;text-align: left;">Paper Simpol</th><td><strong style="color: #FF0000;">${paper_simpol }</strong>&nbsp;&nbsp;<input type="button" value="Reset" onclick="resetCounter('179')"/></td>
						<th style="width: 180px;text-align: left;">Gadget Simpol</th><td><strong style="color: #FF0000;">${gadget_simpol }</strong>&nbsp;&nbsp;<input type="button" value="Reset" onclick="resetCounter('180')"/></td>
					</tr>
					<tr>
						<th style="width: 180px;text-align: left;">Paper Simas Prima</th><td><strong style="color: #FF0000;">${paper_sprima }</strong>&nbsp;&nbsp;<input type="button" value="Reset" onclick="resetCounter('181')"/></td>
						<th style="width: 180px;text-align: left;">Gadget Simas Prima</th><td><strong style="color: #FF0000;">${gadget_sprima }</strong>&nbsp;&nbsp;<input type="button" value="Reset" onclick="resetCounter('182')"/></td>
					</tr>
					<tr>
						<th style="width: 180px;text-align: left;">Paper Produk Save</th><td><strong style="color: #FF0000;">${paper_psave }</strong>&nbsp;&nbsp;<input type="button" value="Reset" onclick="resetCounter('183')"/></td>
						<th style="width: 180px;text-align: left;">Gadget Produk Save</th><td><strong style="color: #FF0000;">${gadget_psave }</strong>&nbsp;&nbsp;<input type="button" value="Reset" onclick="resetCounter('184')"/></td>
					</tr>
					<tr>
						<th style="width: 180px;text-align: left;">Paper SMiLe Link Satu</th><td><strong style="color: #FF0000;">${paper_slinksatu }</strong>&nbsp;&nbsp;<input type="button" value="Reset" onclick="resetCounter('185')"/></td>
						<th style="width: 180px;text-align: left;">Gadget SMiLe Link Satu</th><td><strong style="color: #FF0000;">${gadget_slinksatu }</strong>&nbsp;&nbsp;<input type="button" value="Reset" onclick="resetCounter('186')"/></td>
					</tr>
					<tr>
						<th style="width: 180px;text-align: left;">Paper Kecelakaan</th><td><strong style="color: #FF0000;">${paper_kcl }</strong>&nbsp;&nbsp;<input type="button" value="Reset" onclick="resetCounter('187')"/></td>
						<th style="width: 180px;text-align: left;">Gadget Kecelakaan</th><td><strong style="color: #FF0000;">${gadget_kcl }</strong>&nbsp;&nbsp;<input type="button" value="Reset" onclick="resetCounter('188')"/></td>
					</tr>
					<tr>
						<th style="width: 180px;text-align: left;">Paper VIP Family Plan</th><td><strong style="color: #FF0000;">${paper_familyplan }</strong>&nbsp;&nbsp;<input type="button" value="Reset" onclick="resetCounter('189')"/></td>
						<th style="width: 180px;text-align: left;">Gadget VIP Family Plan</th><td><strong style="color: #FF0000;">${gadget_familyplan }</strong>&nbsp;&nbsp;<input type="button" value="Reset" onclick="resetCounter('190')"/></td>
					</tr>
					<tr>
						<td><input type="submit" name="resetAll" id="resetAll" value="Reset All" ></td>
						<td></td>
					</tr>
				</table>
			</form>
     	</div> --%>
     	<div id="tab-3">
     	    <form id="formPost3" name="formPost3" method="post">
     	        <fieldset>
     	            <legend style="width: 240px">Detail Permintaan Virtual Account</legend>
     	            <table class="entry2">
	                    <tr>
	                        <th style="width: 180px;">Nomor VA</th>
	                        <td><input type="text" name="detva_no" value="${detva_no}"></td>
	                    </tr>
	                    <tr>
	                        <th>Tgl Pengajuan</th>
	                        <td><input type="text" class="datepicker" name="detva_tgl1" value="${detva_tgl1}"> s/d <input type="text" class="datepicker" name="detva_tgl2" value="${detva_tgl2}"></td>
	                    </tr>
	                    <tr>
	                        <th></th>
	                        <td><input type="submit" name="detva_search" value="Search"></td>
	                    </tr>
	                    <tr>
	                        <td colspan="2">
	                            <table class="entry2">
	                                <tr>
	                                    <th style="text-align: left;">No. Pengajuan</th>
	                                    <th style="text-align: left;">No. Virtual Account</th>
	                                    <th style="text-align: left;">Jumlah</th>
	                                    <th style="text-align: left;">Kertas / Online</th>
	                                    <th style="text-align: left;">Jenis Produk</th>
	                                    <th style="text-align: left;">Link / Non Link</th>
	                                    <th style="text-align: left;">User</th>
	                                    <th style="text-align: left;">Tanggal Pengajuan</th>
	                                </tr>
	                                <c:forEach items="${listPermintaanVa}" var="detVa">
	                                    <tr>
	                                        <td>${detVa.MSV_ID}</td>
	                                        <td>${detVa.NO_VA}</td>
	                                        <td>${detVa.MSV_AMOUNT_REQ}</td>
	                                        <td>${detVa.JENIS_VA}</td>
	                                        <td>${detVa.JENIS_SYARIAH}</td>
	                                        <td>${detVa.JENIS_LINK}</td>
	                                        <td>${detVa.USER_CREATE}</td>
	                                        <td>${detVa.CREATE_DATE}</td>
	                                    </tr>
	                                </c:forEach>
	                            </table>
	                        </td>
	                    </tr>
	                </table>
     	        </fieldset>
     	    </form>
     	</div>
	</div>
</body>
</html>


