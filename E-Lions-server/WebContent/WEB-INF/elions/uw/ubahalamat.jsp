<%@ include file="/include/page/header.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<script type="text/javascript" src="${path}/include/js/jquery-1.3.2.min.js"></script>
<script>
	$(document).ready(function() {
		<c:if test="${not empty param.pesan}">
			alert('${param.pesan}');
		</c:if>
	});
</script>
<body style="height: 100%;">
	<div class="tab-container" id="container1">
        <div class="tab-panes">
            <div id="pane1" class="panes">
                <form name="form1" method="post" action="http://elions.sinarmasmsiglife.co.id/uw/uw.htm?window=aksepEmailBlastNew&id1=${id1}&id2=${id2}">
					<fieldset>
						<legend>FORM PERUBAHAAN </legend>
	                    	<table class="entry2">
	                  		<tr style="background-color: #F5F5F5;" align="center">
								<th><ul>KONFIRMASI PERUBAHAAN</ul></th>
	                      	</tr>
	                        <tr style="background-color: #F5F5F5;" align="left">
								<th>Terhitung Sejak Tanggal  : <script >inputDate('tanggal','<fmt:formatDate value="${dt_now}" pattern="dd/MM/yyyy"/>',true);</script>&nbsp;</th>
	                      	</tr>
	                        <tr style="background-color: #F5F5F5;" align="left">
								<th>Nama Pemegang Polis		 : <input type="text" value="${pemegang.mcl_first}" size="50" readonly></th>
	                      	</tr>
	                        <tr style="background-color: #F5F5F5;" align="left">
								<th>Nomor Polis              : <input type="text" value="${pemegang.mspo_policy_no}" size="35" readonly></th>
	                      	</tr>
	                      	<tr style="background-color: #F5F5F5;" align="left">
								<th>Alamat Lama              : <font><textarea rows="3" cols="20" style="width: 40%; text-transform: uppercase;" name=oldAddress readonly="readonly">${pemegang.alamat_rumah}</textarea></font></th>
	                      	</tr>
	                      	<tr style="background-color: #F5F5F5;" align="left">
								<th>Kode Pos                 : <input type="text" value="${pemegang.kd_pos_rumah}" size="35" readonly></th>
	                      	</tr>
	                      	<tr style="background-color: #F5F5F5;" align="left">
								<th>&nbsp;</th>
	                      	</tr>
	                        <tr style="background-color: #F5F5F5;" align="left">
								<th>Alamat Baru              : <font><textarea rows="3" cols="20" style="width: 40%; text-transform: uppercase;" name=newAddress></textarea></font></th>
	                      	</tr>
	                      	<tr style="background-color: #F5F5F5;" align="left">
								<th>Kode Pos                 : <input type="text" size="35" name=newKdPos></th>
	                      	</tr>
	                        <tr style="background-color: #F5F5F5;" align="left">
								<th>Telepon Rumah Baru       : <input type="text" size="35" name=newTelp></th>
	                      	</tr>
	                        <tr style="background-color: #F5F5F5;" align="left">
								<th>Telepon Kantor Baru      : <input type="text" size="35" name=newWorkTelp></th>
	                      	</tr>
	                        <tr style="background-color: #F5F5F5;" align="left">
								<th>Handphone Baru 1         : <input type="text" size="35" name=hpNew></th>
	                      	</tr>
	                      	<tr style="background-color: #F5F5F5;" align="left">
								<th>Handphone Baru 2         : <input type="text" size="35" name=hpNew2></th>
	                      	</tr>
						</table>
					</fieldset>
		            <table class="entry2">
						<tr align = "left">
							<th colspan="1">                        
				            	<input type="hidden" name="submit" id="submit">  
				            	<input type="submit" name="submit" value="Save">
				        	</th>
			        	</tr>
					</table>
                </form>
            </div>
        </div>
    </div>
</body>
<%@ include file="/include/page/footer.jsp" %>