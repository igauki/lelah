<%@ include file="/include/page/header_jquery.jsp"%>
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<script type="text/javascript" src="${path}/include/js/default.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	function isNumberKey(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode;
		if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
		return true;
	}
	
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (jQueryUI Tabs) init tab2 Utama
		$("#tabs").tabs();

		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title], label[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		//Jika klik tombol simpan
		$("#btnSave").click(function(){
			var sv = confirm("Apakah anda sudah yakin untuk menyimpan setoran premi dengan data-data tersebut?");
			if(sv){
				return true;
			}else{
				return false;
			}
		});
		
		$("#btnReset").click(function(){
			var sv = confirm("Apakah anda sudah yakin untuk mereset data-data tersebut?");
			if(sv){
				return true;
			}else{
				return false;
			}
		});
		
		$("#potongan").change(function(){
			if(this.value==1){//BNI
				$("#premirefundtmp").val(($("#premitmp").val() / 0.982).toFixed(3));
				
			}else if(this.value==2){//BCA
				$("#premirefundtmp").val(($("#premitmp").val() / 0.991).toFixed(3));
				
			}else if(this.value==3){//VisaMaster
				$("#premirefundtmp").val(($("#premitmp").val() / 0.975).toFixed(3));
				
			}else{
				$("#premirefundtmp").val($("#premitmp").val());
			}
		});
		
		$("#premitmp").keyup(function(){
			$("#potongan").change();
		});
		
		document.getElementById('jenisProses').value = "0";
		
		//Bila ada pesan
		var pesan = document.getElementById('pesan').value;
		if(pesan != '') alert(pesan);
	});
	
	function setData(id, pp, ccnama, ccno, premi, tglrk){
		document.getElementById("idCentrix").value = id;
		document.getElementById("namaPp").value = pp;
		document.getElementById("kliAtasNama").value = ccnama;
		document.getElementById("kliNoRek").value = ccno;
		document.getElementById("kliNamaBank").value = "";
		document.getElementById("kliCabangBank").value = "-";
		document.getElementById("kliKotaBank").value = "-";
		document.getElementById("premitmp").value = premi;
		document.getElementById("premirefundtmp").value = premi;
		document.getElementById("tgl_rk").value = tglrk;
		document.getElementById("potongan").value = "0";
		document.getElementById("lkuId").value = "";
		document.getElementById("no_trx").value = "";
	}
 	
</script>
	
<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }
	input, select, textarea { text-transform: uppercase; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 7em; }
	
	/* untuk align center */
	.tengah { text-align: center; }
	
	/* untuk warna kolom input */
	.warna { color: grey; }
		
	/* styling untuk table input */
	table.inputTable{
		width: 700px;
	}
	table.inputTable tr th{
		width: 200px;
	}
		
	#tab1 td, #tab1 th { padding: 2px; }
		
	#tab1 th { background-color: #eee; }	
	
	/* styling untuk server-side validation error */
	.errorField { border: 1px solid red; }
	.errorMessage { color: red; display: block;}
	
</style>

	<body>
		<form:form method="post" name="formpost" id="formpost" commandName="cmd">
			<table>
				<tr>
					<td>
						<input type="button" name="btnData" id="btnData" title="List SPAJ" value="List SPAJ">
						<input type="button" name="btnInput" id="btnInput" title="Input Data" value="Input Data">
					</td>
				</tr>
			</table>
			<fieldset>
				<table class="displaytag" id="tab1">
					<tr>
						<th>ID</th>
						<th>Pemegang Polis</th>
						<th>Atas Nama CC</th>
						<th>No. CC</th>
						<th>Nama Bank</th>
						<th>Jumlah Premi</th>
						<th>Tgl. RK</th>
						<th></th>
					</tr>
					<c:forEach items="${cmd.listCentrix}" var="d" varStatus="st">
						<tr class="datarow" id="row[${st.index}]">
							<td class="left">${d.applicationID}</td>
							<td class="left">${d.policyHolder}</td>
							<td class="left">${d.ccHolderName}</td>
							<td class="left">${d.ccNo}</td>
							<td class="left">${d.ccBankName}</td>
							<td>${d.initialPremium}</td>
							<td class="center">${d.trxDate}</td>
							<td class="center">
								<input type="button" name="btnCopy[${st.index}]" id="btnCopy[${st.index}]" title="Copy Data" value="Copy Data"
									onclick="setData('${d.applicationID}', '${d.policyHolder}', '${d.ccHolderName}', '${d.ccNo}', '${d.initialPremium}', '${d.trxDate}')">
							</td>
						</tr>
					</c:forEach>
				</table>
			</fieldset>
			
			<fieldset>
				<form:hidden id="idCentrix" path="idCentrix"/>
				<form:hidden id="pesan" path="pesan"/>
				<form:hidden id="lsbsId" path="lsbsId"/>
				<form:hidden id="policyNo" path="policyNo"/>
				<form:hidden id="namaProduk" path="namaProduk"/>
				<form:hidden id="jenisProses" path="jenisProses"/>
			
				<table id="tab2">
					<tr>
						<th style="width: 200px;" align="left">Pemegang Polis</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="Pemegang Polis" path="namaPp" size="55"/>
						</td>
						<td>
							<form:errors path="namaPp" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">Atas Nama Rekening</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="Atas Nama Rekening" path="kliAtasNama" size="55"/>
						</td>
						<td>
							<form:errors path="kliAtasNama" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">Nomor Rekening</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="Nomor Rekening" path="kliNoRek" size="55"/>
						</td>
						<td>
							<form:errors path="kliNoRek" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">Nama Bank</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:select path="kliNamaBank">
								<form:option value="" label="-- Pilih Bank --"/>
								<form:options items="${listBankPusat}" itemValue="key" itemLabel="value"/>
							</form:select>
						</td>
						<td>
							<form:errors path="kliNamaBank" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">Cabang Bank</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="Cabang Bank" path="kliCabangBank" size="55"/>
						</td>
						<td>
							<form:errors path="kliCabangBank" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">Kota Bank</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="Kota Bank" path="kliKotaBank" size="55"/>
						</td>
						<td>
							<form:errors path="kliKotaBank" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">Kurs</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:select path="lkuId">
								<form:option value="" label="-- Pilih Kurs --"/>
								<form:options items="${listKurs}" itemValue="key" itemLabel="value"/>
							</form:select>
						</td>
						<td>
							<form:errors path="lkuId" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">Jumlah Premi</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="Jumlah Premi" path="premitmp" maxlength="15" cssErrorClass="errorField" onkeypress="return isNumberKey(event)"/>
							<form:errors path="premitmp" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">Jumlah Premi Refund</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="Jumlah Premi Refund" path="premirefundtmp" maxlength="20" readonly="true" />
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">Debet CC</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:select path="potongan">
								<form:option value="0" label="-- Tidak Ada --"/>
								<form:option value="1" label="CC BNI/SINARMAS (Potongan 1,8 %)"/>
								<form:option value="2" label="CC BCA (Potongan 0,9 %)"/>
								<form:option value="3" label="CC Visa Master (Potongan 2,5 %)"/>
							</form:select>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">Tanggal RK</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="Tanggal RK" path="tgl_rk" cssClass="datepicker" cssErrorClass="datepicker errorField"/>
							<form:errors path="tgl_rk" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">Nomor TRX</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="Nomor TRX" path="no_trx" maxlength="15" cssErrorClass="errorField"  readonly="true"/>
							<form:errors path="no_trx" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left"></th>
						<th>&nbsp;&nbsp;&nbsp;</th>
						<td>
							<input type="button" name="btnIbank" id="btnIbank" title="Pilih Ibank" value="Pilih Ibank"  onclick="popWin('${path}/uw/uw.htm?window=drekNonSpaj&startDate='+document.getElementById('tgl_rk').value+'&endDate='+document.getElementById('tgl_rk').value+'&startNominal='+document.getElementById('premitmp').value+'&endNominal='+document.getElementById('premitmp').value, 600, 999);">
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left"></th>
						<th>&nbsp;&nbsp;&nbsp;</th>
						<td>
							<input type="submit" name="btnSave" id="btnSave" title="Simpan" value="Simpan">
							<input type="reset" name="btnReset" id="btnReset" title="Reset" value=" Reset ">
						</td>
					</tr>
				</table>
			</fieldset>
		</form:form>
	</body>
</html>
