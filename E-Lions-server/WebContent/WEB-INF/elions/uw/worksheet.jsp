<%@ include file="/include/page/header.jsp"%>
	<script>
	
	parent.document.formpost.copy_reg_spaj.value = '';
	parent.document.formpost.copy_peserta.value = '';
	
		function tampilkan(btn,indx) {
			var success = true;
			if(btn.name == "hitBMI") {
				if(indx == null) {
					if(document.getElementById('nonMedisTb').value == "" || document.getElementById('nonMedisBb').value == "") {
						success = false;
						alert("silahkan isi tinggi & berat badan");
					}
				}
				else {
					if(document.getElementById('lpkTb'+indx).value == "" || document.getElementById('lpkBb'+indx).value == "") {
						success = false;
						alert("silahkan isi tinggi & berat badan");
					}
				}
			}
			else if(btn.name == "hitBloodPres") {
				if(document.getElementById('lpkSystolic'+indx).value == "" || document.getElementById('lpkDiastolic'+indx).value == "") {
					alert("silahkan isi systolic & diastolic");
					success = false;
				}	
			}
			else if(btn.name == "addPenyDahLpk" || btn.name == "addPenySekLpk" || btn.name == "addPenyKelLpk" || btn.name == "addKelLpk") {
				document.getElementById('index').value = indx;
			}
			else if(btn.name == "copyWorksheet") {
				var copy_spaj = document.getElementById('copy_reg_spaj').value;
				var copy_peserta = document.getElementById('copy_peserta').value;
				if(copy_spaj == '' || copy_peserta == ''){
					alert("Masukkan no SPAJ & no peserta referensi");
					success = false;
				}
			}
			if(success) {
				document.getElementById('submitMode').value = btn.name;
				if(btn.name != "printWorksheet")  btn.disabled=true;
				document.getElementById('formpost').submit();			
			}
			return false;
		}
		
		function plhPeserta(btn,nama,no,umur,birth,mclId) {
			//alert(nama + " " + no + " " + umur + " " + birth);
			document.getElementById('namaPeserta').value = nama;
			document.getElementById('insured_no').value = no;
			document.getElementById('umur').value = umur;
			document.getElementById('birthDate').value = birth;
			document.getElementById('mcl_id').value = mclId;
		
			document.getElementById('submitMode').value = btn.name;
			btn.disabled=true;
			document.getElementById('formpost').submit();
		}
		
		function copyToDesc(mode,index,sub) {
			document.getElementById('mode').value = mode;
			document.getElementById('index').value = index;
			document.getElementById('sub').value = sub;
			
			document.getElementById('submitMode').value = "addToUwDesc";
			document.getElementById('formpost').submit();
		}
		
		function setDefault(btn,index) {
			document.getElementById('index').value = index;
			
			document.getElementById('submitMode').value = btn.name;
			btn.disabled=true;
			document.getElementById('formpost').submit();
		}
		
		function show_hide(id,value) {
			//alert(id + " " + value);
			if(value == 0 || value == 2) {
				document.getElementById(id).style.height= "0";
				document.getElementById(id).style.display= "none";
			}
			else if(value == 1) {
				document.getElementById(id).style.display= "block";
			}
		}
		
		function maxChar(e,string,max){
			var count = string.length + 1;
			if(count > max) {
				alert('input tidak bisa lebih dari '+max+' karakter');
				return false;
			}
			return true;
		}
		
	</script>
	<body>
		<form:form commandName="cmd" id="formpost" method="post">
			<div id="error" style="width: 15%;margin-left:3px;border: 1px solid #006FB5;background-color: #D7EBFF;color: #000000;">
				<c:choose>
					<c:when test="${cmd.position eq 1}">
						Posisi di UW-1
					</c:when>
					<c:when test="${cmd.position eq 2}">
						Posisi di UW-2
					</c:when>
					<c:otherwise>
						Posisi di Staff
					</c:otherwise>	
				</c:choose>  
			</div>
			<c:if test="${not empty cmd.listWarning}">
				<div id="error" style="margin: 5px 5px 5px 3px;border: 1px solid #DC6F18;background-color: #FFB58A;color: #000000;"">
					DATA KOSONG YANG PERLU DIPERHATIKAN:			
					<br/>
					<c:forEach items="${cmd.listWarning}" var="x" varStatus="y">
						- ${x}<br/>
					</c:forEach>
				</div>
			</c:if>	
			<spring:hasBindErrors name="cmd">
				<div id="error" style="margin: 5px 5px 5px 3px;">
					HARAP LENGKAPI DATA DIBAWAH:			
					<br>- <form:errors path="*" delimiter="<br>- "/>
				</div>
			</spring:hasBindErrors>			
			<table width="100%" class="entry2" border="0">
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td>
									<c:forEach items="${lsPeserta}" var="x" varStatus="y">
										<input type="button" id="peserta" name="peserta" value="${x.PERSON} [${x.MCL_FIRST} - ${x.UMUR}th]" onclick="return plhPeserta(this,'${x.MCL_FIRST}',${x.INSURED_NO},${x.UMUR},'${x.MSPE_DATE_BIRTH}','${x.MCL_ID}');" <c:if test="${x.INSURED_NO eq cmd.insured_no}">style="background-color:#B3F34A"</c:if>>&nbsp;
									</c:forEach>
									<input type="hidden" name="namaPeserta" id="namaPeserta" value="${cmd.namaPeserta}">
									<input type="hidden" name="insured_no" id="insured_no" value="${cmd.insured_no}">
									<input type="hidden" name="umur" id="umur" value="${cmd.umur}">
									<input type="hidden" name="birthDate" id="birthDate" value="${cmd.birthDate}">
									<input type="hidden" name="sex" id="sex" value="${cmd.sex}">
									<input type="hidden" name="mcl_id" id="mcl_id" value="${cmd.mcl_id}">
									<input type="hidden" name="position" id="position" value="${cmd.position}">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><fieldset><legend>copy worksheet</legend>
						no spaj <input type="text" name="copy_reg_spaj" id="copy_reg_spaj" value="${ref_reg_spaj}" />
						no peserta <input type="text" name="copy_peserta" id="copy_peserta" size="3" value="${ref_peserta}" />
						<input type="button" class="button" name="copyWorksheet" value="Search Worksheet" onclick="return tampilkan(this);" />
						<br/>(ref : ${ref_reg_spaj} - ${ref_peserta}) <font color="${copyMessageColor}" style="font-weight: bolder;">${copyMessage}</font>
						<c:forEach items="${copyDataPeserta}" var="cx" varStatus="cy">
								<br/>No: ${cx.INSURED_NO} - Peserta: ${cx.PERSON} [${cx.MCL_FIRST} - ${cx.UMUR}th]
						</c:forEach>
						</fieldset>
					<td>
				</tr>
				<tr>
					<td>
						<label for="nm"><form:radiobutton path="msw_medis" value="0" id="nm" cssStyle="margin-right: 3px" />Non Medis</label>
						<label for="m"><form:radiobutton path="msw_medis" value="1" id="m" cssStyle="margin-right: 3px" />Medis</label>
						<input type="button" class="button" name="changeForm" value="Change Form" onclick="return tampilkan(this);">
					<td>
				</tr>
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
							<tr>
								<th width="110" style="color: blue;">Data Simultan</th>
								<!--<td style="padding-left: 3px" width="110">
									<form:select id="pptt" path="pptt" title="pptt">
										<form:options items="${lsPptt}" itemLabel="key" itemValue="value"/>  
									</form:select>							
								</td>-->
								<td style="padding-left: 3px" width="210">
									<label for="all"><form:radiobutton path="modeSim" value="0" id="all" cssStyle="margin-right: 3px" />All Inforce</label>
									<label for="l2y"><form:radiobutton path="modeSim" value="1" id="l2y" cssStyle="margin-right: 3px" />2th terakhir</label>
								</td>
								<td><input type="button" name="viewSimultan" value="View Simultan" onclick="return tampilkan(this);"></td>								
							</tr>
						</table>
					</td>						
				</tr>
				<c:if test="${not empty cmd.listSimultan or not empty cmd.listSimultanProc}">
					<tr>
						<td>
							<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
								<tr>
									<th width="110" style="color: blue;">Simultan Polis</th>
									<td style="padding-left: 3px">
										<table>
											<tr>
												<td><b>Simultan Inforce</b></td>
											</tr>
											<tr>
												<td>
													<table width="100%" class="displaytag">
														<thead>
															<tr>
																<!--<th width="25">No.</th>-->
																<th>Pemegang</th>
																<th>Tertanggung</th>
																<th>No. Polis</th>
																<th>Masa Asuransi</th>
																<th>Plan</th>
																<th>SAR</th>
																<th>Rating & Alasan</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${cmd.listSimultan}" var="x"  varStatus="y">
																<tr>
																	<!--<td>${y.index +1}.</td>-->
																	<td class="left">${x.A}</td>
																	<td class="left">${x.B}</td>
																	<td class="left">${x.C}</td>
																	<td class="left">${x.pol_year} th [${x.J} s/d ${x.K}]</td>
																	<td class="left">[${x.O}-${x.U}] ${x.V}</td>
																	<td><fmt:formatNumber currencyCode="#.###">${x.E}</fmt:formatNumber></td>
																	<td class="left">${x.D1}</td>
																</tr>
															</c:forEach>
														</tbody>
													</table>												
												</td>
											</tr>
										</table>
										<table>
											<tr>
												<td><b>Polis Baru</b></td>
											</tr>
											<tr>
												<td>
													<table width="100%" class="displaytag">
														<thead>
															<tr>
																<!--<th width="25">No.</th>-->
																<th>Pemegang</th>
																<th>Tertanggung</th>
																<th>No. Polis</th>
																<th>Masa Asuransi</th>
																<th>Plan</th>
																<th>SAR</th>
																<th>Rating & Alasan</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${cmd.listSimultanProc}" var="x"  varStatus="y">
																<tr>
																	<!--<td>${y.index +1}.</td>-->
																	<td class="left">${x.A}</td>
																	<td class="left">${x.B}</td>
																	<td class="left">${x.C}</td>
																	<td class="left">${x.pol_year} th [${x.J} s/d ${x.K}]</td>
																	<td class="left">[${x.O}-${x.U}] ${x.V}</td>
																	<td><fmt:formatNumber currencyCode="#.###">${x.E}</fmt:formatNumber></td>
																	<td class="left">${x.D1}</td>
																</tr>
															</c:forEach>
														</tbody>
													</table>												
												</td>
											</tr>
										</table>										
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</c:if>
				<!--<tr>
					<td>
						<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
							<tr>
								<th width="110" style="color: blue;">Kategori</th>
								<td style="padding-left: 3px">
									<form:select id="msw_sex" path="msw_sex" title="kategori medis" cssStyle="width: 373">
										<form:options items="${lsSex}" itemLabel="key" itemValue="value"/>  
									</form:select>
								</td>
							</tr>	
						</table>		
					</td>
				</tr>-->
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
							<tr>
								<th width="110" style="color: blue;">BMI<br/>[BB (kg) / TB (m<sup>2</sup>)]</th>
								<td style="padding-left: 3px">
									<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
										<tr>
											<th width="102">Tinggi Badan</th>
											<td width="60" style="padding-left: 3px"><form:input id="nonMedisTb" size="4" maxlength="4" path="nonMedisTb" /> cm</td>
											<th width="102">Kelainan</th>
											<td style="padding-left: 3px">
												<%--<form:input id="nonMedisIcdId" size="8" path="nonMedisIcdId" />
												<form:input id="nonMedisIcdDesc" path="nonMedisIcdDesc" cssStyle="width: 91%"/>--%>
												<form:input id="nonMedisBmiKelainan" path="nonMedisBmiKelainan" cssStyle="width: 190" onkeypress="return maxChar(event,this.value,30)" />
												<img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('nonMedBmi')">
											</td>
											
										</tr>
										<tr>
											<th width="102">Berat Badan</th>
											<td width="60" style="padding-left: 3px"><form:input id="nonMedisBb" size="4" maxlength="4" path="nonMedisBb" /> kg</td>
											<c:choose>
						                        <c:when test="${not empty cmd.emBmi}">
							                        <th rowspan="2" width="102">EM</th>
													<td rowspan="2" style="padding-left: 3px">
							                            <table cellpadding="0" cellspacing="0" class="displaytag" style="width: 30%">
							                            	<tr>
							                            		<th>Life</th>
							                            		<th>ADB</th>
							                            		<th>TPD</th>
							                            		<th>WOP</th>
							                            	</tr>
							                            	<tr>
							                            		<td style="text-align: center;">${cmd.emBmi.LIFE}</td>
							                            		<td style="text-align: center;">${cmd.emBmi.ADB}</td>
							                            		<td style="text-align: center;">${cmd.emBmi.TPD}</td>
							                            		<td style="text-align: center;">${cmd.emBmi.WOP}</td>
							                            	</tr>
							                            </table>
							                           </td> 
						                        </c:when>
						                        <c:otherwise>
						                            <td colspan="2">&nbsp;</td>
						                        </c:otherwise>	
						                	</c:choose>        						
										</tr>
										<tr>
											<td width="102" style="padding-top: 3px"><input type="button" name="hitBMI" value="BMI" onclick="return tampilkan(this);"></td>
											<td width="60" style="padding-left: 3px">
												<c:choose>
													<c:when test="${not empty cmd.emBmi}">
														<form:input id="nonMedisBmi" size="4" maxlength="4" path="nonMedisBmi" readonly="readonly" cssClass="readOnly" />
													</c:when>
													<c:otherwise>
														&nbsp;
													</c:otherwise>	
												</c:choose>
											</td>
										</tr>
										<!--<tr>
											<td colspan="4" style="padding-top: 3px">
												<input type="button" name="hitBMI" value="BMI" onclick="return tampilkan(this);">
												<input type="button" name="listIcd" value="List ICD" onclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=nonMed',550,700)">
											</td>
										</tr>-->
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
							<tr>
								<th width="110" style="color: blue;">Pekerjaan</th>
								<td style="padding-left: 3px">
									<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
										<tr>
											<td width="10" style="padding-right: 3px">
												<form:select id="nonMedisOccupation" path="nonMedisOccupation" title="occupation" cssStyle="width: 373">
													<form:option label="" value=""/>
													<form:options items="${lsOccupation}" itemLabel="key" itemValue="value"/>  
												</form:select>	
											</td>
											<c:choose>
						                        <c:when test="${not empty cmd.emOcc}">
							                        <th rowspan="2" width="60">EM</th>
													<td rowspan="2" style="padding-left: 3px">
							                            <table cellpadding="0" cellspacing="0" class="displaytag" style="width: 50%">
							                            	<tr>
							                            		<th>Life</th>
							                            		<th>ADB</th>
							                            		<th>TPD</th>
							                            		<th>CI</th>
							                            		<th>WOP</th>
							                            		<th>DI</th>
							                            	</tr>
							                            	<tr>
							                            		<td style="text-align: center;">${cmd.emOcc.LIFE}</td>
							                            		<td style="text-align: center;">${cmd.emOcc.ADB}</td>
							                            		<td style="text-align: center;">${cmd.emOcc.TPD}</td>
							                            		<td style="text-align: center;">${cmd.emOcc.CI}</td>
							                            		<td style="text-align: center;">${cmd.emOcc.WOP}</td>
							                            		<td style="text-align: center;">${cmd.emOcc.DI}</td>
							                            	</tr>
							                            </table>
							                    	</td>        
						                        </c:when>
						                        <c:otherwise>
						                            &nbsp;
						                        </c:otherwise>
						                    </c:choose>
										</tr>
										<tr>
											<td>
												<form:input id="nonMedisOccDesc" path="nonMedisOccDesc" cssStyle="width: 94%" onkeypress="return maxChar(event,this.value,100)"/>
												<img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('nonMedOcc')">												
											</td>
										</tr>
										<tr>
											<td style="padding-top: 3px" colspan="3">
												<input type="button" name="occ" value="View EM" onclick="return tampilkan(this);">
											</td>
										</tr>
									</table>	
								</td>
							</tr>
						</table>
					</td>		
				</tr>
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
							<tr>
								<th width="110" style="color: blue;">Habits / Kebiasaan</th>
								<td style="padding: 0px 5px 0px 3px" width="300"><form:textarea id="habits" path="nonMedisHabits" tabindex="6" cols="58" rows="4" cssStyle="width: 368" onkeypress="return maxChar(event,this.value,100)" /></td>
								<th width="102">Kelainan</th>
								<td style="padding-left: 3px">
									<form:input id="nonMedisHabitsKelainan" path="nonMedisHabitsKelainan" cssStyle="width: 190" onkeypress="return maxChar(event,this.value,100)"/>
									<img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('nonMedHabits')">
								</td>
							</tr>
						</table>		
					</td>
				</tr>								
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
							<tr>
								<th width="110" style="color: blue;">High Risk Sport / Hobby</th>
								<td style="padding-left: 3px">
									<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
										<tr>
											<td width="10" style="padding-right: 3px">
												<form:select id="nonMedisHobby" path="nonMedisHobby" title="hobby" cssStyle="width: 373">
													<form:option label="" value=""/>
													<form:options items="${lsHobby}" itemLabel="key" itemValue="value"/>  
												</form:select>	
											</td>
											<c:choose>
						                        <c:when test="${not empty cmd.emHobby}">
							                        <th rowspan="2" width="60">EM</th>
													<td rowspan="2" style="padding-left: 3px">
							                            <table cellpadding="0" cellspacing="0" class="displaytag" style="width: 50%">
							                            	<tr>
							                            		<th>Life</th>
							                            		<th>ADB</th>
							                            		<th>TPD</th>
							                            		<th>CI</th>
							                            		<th>WOP</th>
							                            		<th>DI</th>
							                            	</tr>
							                            	<tr>
							                            		<td style="text-align: center;">${cmd.emHobby.LIFE}</td>
							                            		<td style="text-align: center;">${cmd.emHobby.ADB}</td>
							                            		<td style="text-align: center;">${cmd.emHobby.TPD}</td>
							                            		<td style="text-align: center;">${cmd.emHobby.CI}</td>
							                            		<td style="text-align: center;">${cmd.emHobby.WOP}</td>
							                            		<td style="text-align: center;">${cmd.emHobby.DI}</td>
							                            	</tr>
							                            </table>
							                    	</td>        
						                        </c:when>
						                        <c:otherwise>
						                            &nbsp;
						                        </c:otherwise>
						                    </c:choose>
										</tr>
										<tr>
											<td>
												<form:input id="nonMedisHobbyDesc" path="nonMedisHobbyDesc" cssStyle="width: 94%" onkeypress="return maxChar(event,this.value,100)"/>
												<img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('nonMedHob')">												
											</td>
										</tr>
										<tr>
											<td style="padding-top: 3px" colspan="3">
												<input type="button" name="hobby" value="View EM" onclick="return tampilkan(this);">
											</td>
										</tr>
									</table>	
								</td>
							</tr>
						</table>
						<input type="hidden" id="mode" name="mode">
						<input type="hidden" id="index" name="index">
						<input type="hidden" id="sub" name="sub">
					</td>		
				</tr>
				<c:if test="${cmd.msw_medis eq 0}">
					<tr>
						<td>
							<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
								<tr>
									<th width="110" style="color: blue;">Kelainan / Info Medis</th>
									<td style="padding: 0px 5px 0px 3px" width="300"><form:textarea id="kelainan" path="nonMedisKelainan" tabindex="6" cols="58" rows="4" cssStyle="width: 368"/></td>
									<th width="102">Kelainan</th>
									<td style="padding-left: 3px">
										<form:input id="nonMedisKelainanKelainan" path="nonMedisKelainanKelainan" cssStyle="width: 190"/>
										<img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('nonMedKelainan')">
									</td>
								</tr>
							</table>		
						</td>
					</tr>
					<tr>
						<td>
							<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
								<tr>
									<th width="110" style="color: blue;">Jenis Kuesioner (Quest)</th>
									<td style="padding-left: 3px">
										<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
											<c:forEach items="${cmd.listQuest}" var="b" varStatus="s">
												<tr>
													<td width="10">
														<form:select id="lsqu_jenis" path="listQuest[${s.index}].lw_id" title="questioner" cssStyle="width: 370">
															<form:option label="" value=""/>
															<form:options items="${lsQuest}" itemLabel="key" itemValue="value"/>  
														</form:select>												
													</td>
													<td style="padding-left: 3px">
														<form:input id="lsqu_desc" path="listQuest[${s.index}].kesimpulan" cssStyle="width: 370" />
														<img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('nonMedQuest',${s.index})">												
													</td>
												</tr>
											</c:forEach>
											<tr>
												<td colspan="2" style="padding-top: 3px"><input type="button" class="button" name="addQuest" value="Add Quest" onclick="return tampilkan(this);"></td>
											</tr>										
										</table>
									</td>
								</tr>
							</table>		
						</td>
					</tr>
				</c:if>
				<c:if test="${cmd.msw_medis eq 1}">
					<tr>
						<td>
							<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
								<tr>
									<th width="110" style="color: blue;">Sumber MCU</th>
									<td style="padding-left: 3px">
										<label for="pribadi"><form:radiobutton path="sumMCU" value="0" id="pribadi" cssStyle="margin-right: 3px" />MCU Pribadi</label>
										<label for="reqByAjs"><form:radiobutton path="sumMCU" value="1" id="reqByAjs" cssStyle="margin-right: 3px" />MCU requested by AJS</label>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
								<tr>
									<th width="110" style="color: blue;">Jenis Medis</th>
									<td style="padding-left: 3px">
										<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
											<tr>
												<td>
													<form:select id="jenis_medis" path="jenisMedis" title="Jenis Medis" cssStyle="width: 100%">
														<form:option label="ALL" value="ALL"/>
														<form:options items="${lsJnsMds}" itemLabel="key" itemValue="value"/>  
													</form:select>
												</td>
											</tr>
											<tr>
												<td style="padding-top: 3px"><input type="button" class="button" name="changeMedis" value="Change Medis" onclick="return tampilkan(this);">										</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<c:if test="${fn:length(cmd.listLpk) ne 0 }">
						<tr>
							<td>
								<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
									<tr>
										<th width="110" style="color: blue;">LPK</th>
										<td style="padding-left: 3px">
											<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
												<tr>
													<td>
														<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
															<c:forEach items="${cmd.listLpk}" var="b" varStatus="s">
																<tr>
																	<td>
																		<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																			<tr>
																				<th width="102">Tanggal MCU</th>
																				<td style="padding-left: 3px" width="104">
																					<spring:bind  path="cmd.listLpk[${s.index}].tglMcuLpk">
																						<script>inputDate('${status.expression}', '${status.value}', false);</script> <span style="color: #FF0000">*</span>
																					</spring:bind>									
																				</td>
																				<th width="102">Tempat MCU</th>
																				<td style="padding-left: 3px">									
																					<form:input id="lpk_tmpMcu${s.index}" path="listLpk[${s.index}].tmpMcuLpk" cssStyle="width: 97%" /> <span style="color: #FF0000">*</span>
																				</td>
																				<c:choose>
																					<c:when test="${s.index eq fn:length(cmd.listLpk)-1}">
																						<td style="padding-left: 3px" width="240">
																							<input type="button" name="lsProv" value="List Provider" onclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_provider&sub=lpk&indx=${s.index}',480,640)">
																							<input type="button" name="setDef" value="Set Default" onclick="setDefault(this,${s.index})">
																						</td>
																						</c:when>
																					<c:otherwise>
																						<td style="padding-left: 3px" width="110">
																							<input type="button" name="lsProv" value="List Provider" onclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_provider&sub=lpk&indx=${s.index}',480,640)">
																						</td>
																					</c:otherwise>	
																				</c:choose>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td>
																		<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																			<tr>
																				<th width="102">Tinggi Badan</th>
																				<td width="60" style="padding-left: 3px"><form:input id="lpkTb${s.index}" size="4" maxlength="4" path="listLpk[${s.index}].lpkTb" /> cm</td>
																				<th width="102">Kelainan</th>
																				<td style="padding-left: 3px">
																					<form:input id="lpkBmiKelainan${s.index}" path="listLpk[${s.index}].lpkBmiKelainan" cssStyle="width: 188" onkeypress="return maxChar(event,this.value,30)" />
																					<img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('lpkBmi',${s.index})">
																				</td>	
																			</tr>
																			<tr>
																				<th width="102">Berat Badan</th>
																				<td width="60" style="padding-left: 3px"><form:input id="lpkBb${s.index}" size="4" maxlength="4" path="listLpk[${s.index}].lpkBb" /> kg</td>
																				<c:choose>
																					<c:when test="${not empty cmd.listLpk[s.index].emBmiLpk}">
																						<th rowspan="2">EM</th>
																						<td rowspan="2" style="padding-left: 3px">
																							<table cellpadding="0" cellspacing="0" class="displaytag" style="width: 30%">
																								<tr>
																									<th>Life</th>
																									<th>ADB</th>
																									<th>TPD</th>
																									<th>WOP</th>
																								</tr>
																								<tr>
																									<td style="text-align: center;">${cmd.listLpk[s.index].emBmiLpk.LIFE}</td>
																									<td style="text-align: center;">${cmd.listLpk[s.index].emBmiLpk.ADB}</td>
																									<td style="text-align: center;">${cmd.listLpk[s.index].emBmiLpk.TPD}</td>
																									<td style="text-align: center;">${cmd.listLpk[s.index].emBmiLpk.WOP}</td>
																								</tr>
																							</table>
																						</td> 
																					</c:when>
																					<c:otherwise>
																						<td colspan="2" rowspan="2">&nbsp;</td>
																					</c:otherwise>	
																				</c:choose>
																			</tr>
																			<tr>
																				<td style="padding-top: 3px">
																					<input type="button" name="hitBMI" value="BMI" onclick="return tampilkan(this,${s.index});">
																				</td>
																				<td style="padding-left: 3px">
																					<c:choose>
																						<c:when test="${not empty cmd.listLpk[s.index].lpkBmi}">
																							<form:input id="lpkBmi" size="4" maxlength="4" path="listLpk[${s.index}].lpkBmi" readonly="readonly" cssClass="readOnly" />
																						</c:when>
																						<c:otherwise>
																							&nbsp;
																						</c:otherwise>	
																					</c:choose>
																				</td>
																			</tr>		
																		</table>																
																	</td>
																</tr>
																<tr>
																	<td>
																		<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																			<tr>
																				<th width="102">Systolic</th>
																				<td width="35" style="padding-left: 3px"><form:input id="lpkSystolic${s.index}" size="4" maxlength="4" path="listLpk[${s.index}].lpkSystolic" /></td>
																				<c:choose>
																					<c:when test="${not empty cmd.listLpk[s.index].emBloodPresure}">
																						<th width="102">EM</th>
																						<td style="padding-left: 3px" ><form:input id="emBloodPresure" size="5" maxlength="5" path="listLpk[${s.index}].emBloodPresure" readonly="readonly" cssClass="readOnly" /></td>
																					</c:when>
																					<c:otherwise>
																						<td colspan="2">&nbsp;</td>
																					</c:otherwise>	
																				</c:choose>
																			</tr>
																			<tr>
																				<th width="102">Diastolic</th>
																				<td width="35" style="padding-left: 3px"><form:input id="lpkDiastolic${s.index}" size="4" maxlength="4" path="listLpk[${s.index}].lpkDiastolic" /></td>
																				<td style="padding-top: 3px" colspan="2"><input type="button" name="hitBloodPres" value="Blood Pres" onclick="return tampilkan(this,${s.index});"></td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td>
																		<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																			<tr>
																				<th width="102">Deyut Nadi</th>
																				<td width="80" style="padding-left: 3px;text-transform: none;"><form:input id="lpkDeyutNadi" size="4" maxlength="4" path="listLpk[${s.index}].lpkDeyutNadi" /> x/mnt</td>
																				<th width="102">Normal</th>
																				<td style="padding-left: 3px;text-transform: none;"><form:input id="nv.lpk_deyut_nadi" size="8" path="listLpk[${s.index}].nv_lpk_deyut_nadi" cssStyle="background-color: #6FC1F3"/> x/mnt</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td>
																		<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																			<tr>
																				<th width="102" style="vertical-align:top;padding-top: 4px;">Riwayat Penyakit Dahulu</th>
																				<td>
																					<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
																						<tr>
																							<td colspan="3" style="padding: 0px 0px 3px 3px" width="210">
																								<label for="lpkRPDNo"><form:radiobutton path="listLpk[${s.index}].lpkRPD" value="0" id="lpkRPDNo" cssStyle="margin-right: 3px" />Tidak Ada</label>
																								<label for="lpkRPDYes"><form:radiobutton path="listLpk[${s.index}].lpkRPD" value="1" id="lpkRPDYes" cssStyle="margin-right: 3px" />Ada</label>
																							</td>
																						</tr>
																						<c:forEach items="${cmd.listLpk[s.index].listRpd}" var="b" varStatus="x">
																							<tr>
																								<td style="padding: 0px 3px 0px 3px" width="275">
																									<form:input id="listRpd_rp_desc${x.index}" path="listLpk[${s.index}].listRpd[${x.index}].rp_desc" cssStyle="width: 250px" onkeypress="return maxChar(event,this.value,100)" />
																									<img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('rpd',${s.index},${x.index})">												
																								</td>
																								<%--<td width="50"><form:input id="lpkRpd${s.index}_lic_id${x.index}" size="8" path="listLpk[${s.index}].listRpd[${x.index}].lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=lpkRpd&indx=${s.index}&no=${x.index}',550,700)" /></td>
																								<td style="padding-left: 3px"><form:input id="lpkRpd${s.index}_lic_desc${x.index}" size="8" path="listLpk[${s.index}].listRpd[${x.index}].lic_desc" cssStyle="width: 100%" /></td>--%>
																							</tr>
																						</c:forEach>
																						<tr>
																							<td style="padding: 3px 0px 3px 3px"><input type="button" name="addPenyDahLpk" value="Add" onclick="return tampilkan(this,${s.index});"></td>
																						</tr>		
																					</table>	
																				</td>
																			</tr>
																			<tr>
																				<th width="102" style="vertical-align:top;padding-top: 4px;">Riwayat Penyakit Sekarang</th>
																				<td>
																					<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
																						<tr>
																							<td colspan="3" style="padding: 0px 0px 3px 3px" width="210">
																								<label for="lpkRPSNo"><form:radiobutton path="listLpk[${s.index}].lpkRPS" value="0" id="lpkRPSNo" cssStyle="margin-right: 3px" />Tidak Ada</label>
																								<label for="lpkRPSYes"><form:radiobutton path="listLpk[${s.index}].lpkRPS" value="1" id="lpkRPSYes" cssStyle="margin-right: 3px" />Ada</label>
																							</td>
																						</tr>
																						<c:forEach items="${cmd.listLpk[s.index].listRps}" var="b" varStatus="x">
																							<tr>
																								<td style="padding: 0px 3px 0px 3px" width="275">
																									<form:input id="listRps_rp_desc${x.index}" path="listLpk[${s.index}].listRps[${x.index}].rp_desc" cssStyle="width: 250px" onkeypress="return maxChar(event,this.value,100)" />
																									<img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('rps',${s.index},${x.index})">												
																								</td>
																								<%--<td width="50"><form:input id="lpkRps${s.index}_lic_id${x.index}" size="8" path="listLpk[${s.index}].listRps[${x.index}].lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=lpkRps&indx=${s.index}&no=${x.index}',550,700)" /></td>
																								<td style="padding-left: 3px"><form:input id="lpkRps${s.index}_lic_desc${x.index}" size="8" path="listLpk[${s.index}].listRps[${x.index}].lic_desc" cssStyle="width: 100%" /></td>--%>
																							</tr>
																						</c:forEach>
																						<tr>
																							<td style="padding: 3px 0px 3px 3px"><input type="button" name="addPenySekLpk" value="Add" onclick="return tampilkan(this,${s.index});"></td>
																						</tr>																					
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102" style="vertical-align:top;padding-top: 4px;">Riwayat Penyakit Keluarga</th>
																				<td>
																					<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
																						<tr>
																							<td colspan="3" style="padding: 0px 0px 3px 3px" width="210">
																								<label for="lpkRPKNo"><form:radiobutton path="listLpk[${s.index}].lpkRPK" value="0" id="lpkRPKNo" cssStyle="margin-right: 3px" />Tidak Ada</label>
																								<label for="lpkRPKYes"><form:radiobutton path="listLpk[${s.index}].lpkRPK" value="1" id="lpkRPKYes" cssStyle="margin-right: 3px" />Ada</label>
																							</td>
																						</tr>
																						<c:forEach items="${cmd.listLpk[s.index].listRpk}" var="b" varStatus="x">
																							<tr>
																								<td style="padding: 0px 3px 0px 3px" width="275">
																									<form:input id="listRpk_rp_desc${x.index}" path="listLpk[${s.index}].listRpk[${x.index}].rp_desc" cssStyle="width: 250px" onkeypress="return maxChar(event,this.value,100)" />
																									<img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('rpk',${s.index},${x.index})">												
																								</td>
																								<%--<td width="50"><form:input id="lpkRpk${s.index}_lic_id${x.index}" size="8" path="listLpk[${s.index}].listRpk[${x.index}].lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=lpkRpk&indx=${s.index}&no=${x.index}',550,700)" /></td>
																								<td style="padding-left: 3px"><form:input id="lpkRpk${s.index}_lic_desc${x.index}" size="8" path="listLpk[${s.index}].listRpk[${x.index}].lic_desc" cssStyle="width: 100%" /></td>--%>
																							</tr>
																						</c:forEach>
																						<tr>
																							<td style="padding: 3px 0px 3px 3px"><input type="button" name="addPenyKelLpk" value="Add" onclick="return tampilkan(this,${s.index});"></td>
																						</tr>																					
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102" style="vertical-align:top;padding-top: 4px;">Kelainan / Info Medis</th>
																				<td>
																					<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
																						<tr>
																							<td colspan="3" style="padding: 0px 0px 3px 3px" width="210">
																								<label for="lpkKelainanNo"><form:radiobutton path="listLpk[${s.index}].lpkKelainan" value="0" id="lpkKelainanNo" cssStyle="margin-right: 3px" />Tidak Ada</label>
																								<label for="lpkKelainanYes"><form:radiobutton path="listLpk[${s.index}].lpkKelainan" value="1" id="lpkKelainanYes" cssStyle="margin-right: 3px" />Ada</label>
																							</td>
																						</tr>
																						<c:forEach items="${cmd.listLpk[s.index].listKelainan}" var="b" varStatus="x">
																							<tr>
																								<td style="padding: 0px 3px 0px 3px" width="275">
																									<form:input id="listKelainan_rp_desc${x.index}" path="listLpk[${s.index}].listKelainan[${x.index}].rp_desc" cssStyle="width: 250px" onkeypress="return maxChar(event,this.value,100)" />
																									<img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('keln',${s.index},${x.index})">												
																								</td>
																								<%--<td width="50"><form:input id="lpkKel${s.index}_lic_id${x.index}" size="8" path="listLpk[${s.index}].listKelainan[${x.index}].lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=lpkKel&indx=${s.index}&no=${x.index}',550,700)" /></td>
																								<td style="padding-left: 3px"><form:input id="lpkKel${s.index}_lic_desc${x.index}" size="8" path="listLpk[${s.index}].listKelainan[${x.index}].lic_desc" cssStyle="width: 100%" /></td>--%>
																							</tr>
																						</c:forEach>
																						<tr>
																							<td style="padding: 3px 0px 3px 3px"><input type="button" name="addKelLpk" value="Add" onclick="return tampilkan(this,${s.index});"></td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td style="padding-top: 3px">
																		<c:choose>
																			<c:when test="${s.index eq fn:length(cmd.listLpk)-1}">
																				<input type="button" name="addLpk" value="Add LPK" onclick="return tampilkan(this);">
																			</c:when>
																			<c:otherwise>
																				&nbsp;
																			</c:otherwise>	
																		</c:choose>
																	</td>
																</tr>																
															</c:forEach>												
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>				
						</tr>
					</c:if>
					<c:if test="${fn:length(cmd.listUrin) ne 0 }">
						<tr>
							<td>
								<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
									<tr>
										<th width="110" style="color: blue;">Urin</th>
										<td style="padding-left: 3px">
											<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
												<tr>
													<td>
														<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
															<c:forEach items="${cmd.listUrin}" var="b" varStatus="s">
																<tr>
																	<td>
																		<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																			<tr>
																				<th width="102">Tanggal MCU</th>
																				<td style="padding-left: 3px" width="104">
																					<spring:bind  path="cmd.listUrin[${s.index}].tglMcuUrin">
																						<script>inputDate('${status.expression}', '${status.value}', false);</script> <span style="color: #FF0000">*</span>
																					</spring:bind>									
																				</td>
																				<th width="102">Tempat MCU</th>
																				<td style="padding-left: 3px">									
																					<form:input id="urin_tmpMcu${s.index}" path="listUrin[${s.index}].tmpMcuUrin" cssStyle="width: 97%"/> <span style="color: #FF0000">*</span>
																				</td>
																				<td style="padding-left: 3px" width="110">
																					<input type="button" name="lsProv" value="List Provider" onclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_provider&sub=urin&indx=${s.index}',480,640)">
																				</td>																	
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td>
																		<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																			<tr>
																				<th width="102">Warna</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagWarnaNorm"><form:radiobutton path="listUrin[${s.index}].flagWarna" value="0" id="flagWarnaNorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_warna${s.index}',0)" />Normal</label>
																								<label for="flagWarnaAbnorm"><form:radiobutton path="listUrin[${s.index}].flagWarna" value="1" id="flagWarnaAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_warna${s.index}',1)" />Abnormal</label>
																								<label for="flagWarnaNot"><form:radiobutton path="listUrin[${s.index}].flagWarna" value="2" id="flagWarnaNot" cssStyle="margin-right: 3px" onclick="show_hide('urin_warna${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="urin_warna${s.index}" style="<c:if test="${cmd.listUrin[s.index].flagWarna eq 0 or cmd.listUrin[s.index].flagWarna eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('warna',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center"><form:input id="urin_warna${s.index}" path="listUrin[${s.index}].warna" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_nv_urin_warna${s.index}" size="8" path="listUrin[${s.index}].nv_urin_warna" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_warna_kelainan${s.index}" path="listUrin[${s.index}].warna_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_warna_lic_id${s.index}" size="8" path="listUrin[${s.index}].warna_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=urin_warna&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="urin_warna_lic_desc${s.index}" path="listUrin[${s.index}].warna_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Kejernihan</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagkejernihanNorm"><form:radiobutton path="listUrin[${s.index}].flagkejernihan" value="0" id="flagkejernihanNorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_kejernihan${s.index}',0)" />Normal</label>
																								<label for="flagkejernihanAbnorm"><form:radiobutton path="listUrin[${s.index}].flagkejernihan" value="1" id="flagkejernihanAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_kejernihan${s.index}',1)" />Abnormal</label>
																								<label for="flagkejernihanNot"><form:radiobutton path="listUrin[${s.index}].flagkejernihan" value="2" id="flagkejernihanNot" cssStyle="margin-right: 3px" onclick="show_hide('urin_kejernihan${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="urin_kejernihan${s.index}" style="<c:if test="${cmd.listUrin[s.index].flagkejernihan eq 0 or cmd.listUrin[s.index].flagkejernihan eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('kerjenihan',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center"><form:input id="urin_kejernihan${s.index}" path="listUrin[${s.index}].kejernihan" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_nv_urin_kejernihan${s.index}" size="7" path="listUrin[${s.index}].nv_urin_kejernihan" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_kejernihan_kelainan${s.index}" path="listUrin[${s.index}].kejernihan_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_kejernihan_lic_id${s.index}" size="8" path="listUrin[${s.index}].kejernihan_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=urin_kejernihan&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="urin_kejernihan_lic_desc${s.index}" path="listUrin[${s.index}].kejernihan_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Berat Jenis</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagBjNorm"><form:radiobutton path="listUrin[${s.index}].flagBj" value="0" id="flagBjNorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_bj${s.index}',0)" />Normal</label>
																								<label for="flagBjAbnorm"><form:radiobutton path="listUrin[${s.index}].flagBj" value="1" id="flagBjAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_bj${s.index}',1)" />Abnormal</label>
																								<label for="flagBjNot"><form:radiobutton path="listUrin[${s.index}].flagBj" value="2" id="flagBjNot" cssStyle="margin-right: 3px" onclick="show_hide('urin_bj${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="urin_bj${s.index}" style="<c:if test="${cmd.listUrin[s.index].flagBj eq 0 or cmd.listUrin[s.index].flagBj eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('bj',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center"><form:input id="urin_bj${s.index}" path="listUrin[${s.index}].bj" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_nv_urin_bj${s.index}" size="13" path="listUrin[${s.index}].nv_urin_berat_jenis" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_bj_kelainan${s.index}" path="listUrin[${s.index}].bj_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_bj_lic_id${s.index}" size="8" path="listUrin[${s.index}].bj_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=urin_bj&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="urin_bj_lic_desc${s.index}" path="listUrin[${s.index}].bj_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">pH</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagPhNorm"><form:radiobutton path="listUrin[${s.index}].flagPh" value="0" id="flagPhNorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_ph${s.index}',0)" />Normal</label>
																								<label for="flagPhAbnorm"><form:radiobutton path="listUrin[${s.index}].flagPh" value="1" id="flagPhAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_ph${s.index}',1)" />Abnormal</label>
																								<label for="flagPhNot"><form:radiobutton path="listUrin[${s.index}].flagPh" value="2" id="flagPhNot" cssStyle="margin-right: 3px" onclick="show_hide('urin_ph${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="urin_ph${s.index}" style="<c:if test="${cmd.listUrin[s.index].flagPh eq 0 or cmd.listUrin[s.index].flagPh eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('ph',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center"><form:input id="urin_ph${s.index}" path="listUrin[${s.index}].ph" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_nv_urin_ph${s.index}" size="8" path="listUrin[${s.index}].nv_urin_ph" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_ph_kelainan${s.index}" path="listUrin[${s.index}].ph_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_ph_lic_id${s.index}" size="8" path="listUrin[${s.index}].ph_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=urin_ph&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="urin_ph_lic_desc${s.index}" path="listUrin[${s.index}].ph_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Protein</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagProteinNorm"><form:radiobutton path="listUrin[${s.index}].flagProtein" value="0" id="flagProteinNorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_protein${s.index}',0)" />Normal</label>
																								<label for="flagProteinAbnorm"><form:radiobutton path="listUrin[${s.index}].flagProtein" value="1" id="flagProteinAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_protein${s.index}',1)" />Abnormal</label>
																								<label for="flagProteinNot"><form:radiobutton path="listUrin[${s.index}].flagProtein" value="2" id="flagProteinNot" cssStyle="margin-right: 3px" onclick="show_hide('urin_protein${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="urin_protein${s.index}" style="<c:if test="${cmd.listUrin[s.index].flagProtein eq 0 or cmd.listUrin[s.index].flagProtein eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('protein',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center"><form:input id="urin_protein${s.index}" path="listUrin[${s.index}].protein" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_nv_urin_protein${s.index}" size="24" path="listUrin[${s.index}].nv_urin_protein" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_protein_kelainan${s.index}" path="listUrin[${s.index}].protein_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_protein_lic_id${s.index}" size="8" path="listUrin[${s.index}].protein_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=urin_protein&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="urin_protein_lic_desc${s.index}" path="listUrin[${s.index}].protein_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Glukosa</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagGlukosaNorm"><form:radiobutton path="listUrin[${s.index}].flagGlukosa" value="0" id="flagGlukosaNorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_glukosa${s.index}',0)" />Normal</label>
																								<label for="flagGlukosaAbnorm"><form:radiobutton path="listUrin[${s.index}].flagGlukosa" value="1" id="flagGlukosaAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_glukosa${s.index}',1)" />Abnormal</label>
																								<label for="flagGlukosaNot"><form:radiobutton path="listUrin[${s.index}].flagGlukosa" value="2" id="flagGlukosaNot" cssStyle="margin-right: 3px" onclick="show_hide('urin_glukosa${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="urin_glukosa${s.index}" style="<c:if test="${cmd.listUrin[s.index].flagGlukosa eq 0 or cmd.listUrin[s.index].flagGlukosa eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('glukosa',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center"><form:input id="urin_glukosa${s.index}" path="listUrin[${s.index}].glukosa" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_nv_urin_glukosa${s.index}" size="23" path="listUrin[${s.index}].nv_urin_glukosa" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_glukosa_kelainan${s.index}" path="listUrin[${s.index}].glukosa_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_glukosa_lic_id${s.index}" size="8" path="listUrin[${s.index}].glukosa_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=urin_glukosa&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="urin_glukosa_lic_desc${s.index}" path="listUrin[${s.index}].glukosa_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Keton</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagKetonNorm"><form:radiobutton path="listUrin[${s.index}].flagKeton" value="0" id="flagKetonNorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_keton${s.index}',0)" />Normal</label>
																								<label for="flagKetonAbnorm"><form:radiobutton path="listUrin[${s.index}].flagKeton" value="1" id="flagKetonAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_keton${s.index}',1)" />Abnormal</label>
																								<label for="flagKetonNot"><form:radiobutton path="listUrin[${s.index}].flagKeton" value="2" id="flagKetonNot" cssStyle="margin-right: 3px" onclick="show_hide('urin_keton${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="urin_keton${s.index}" style="<c:if test="${cmd.listUrin[s.index].flagKeton eq 0 or cmd.listUrin[s.index].flagKeton eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('keton',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center"><form:input id="urin_keton${s.index}" path="listUrin[${s.index}].keton" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_nv_urin_keton${s.index}" size="25" path="listUrin[${s.index}].nv_urin_keton" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_keton_kelainan${s.index}" path="listUrin[${s.index}].keton_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_keton_lic_id${s.index}" size="8" path="listUrin[${s.index}].keton_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=urin_keton&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="urin_keton_lic_desc${s.index}" path="listUrin[${s.index}].keton_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Bilirubin</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagBilirubinNorm"><form:radiobutton path="listUrin[${s.index}].flagBilirubin" value="0" id="flagBilirubinNorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_bilirubin${s.index}',0)" />Normal</label>
																								<label for="flagBilirubinAbnorm"><form:radiobutton path="listUrin[${s.index}].flagBilirubin" value="1" id="flagBilirubinAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_bilirubin${s.index}',1)" />Abnormal</label>
																								<label for="flagBilirubinNot"><form:radiobutton path="listUrin[${s.index}].flagBilirubin" value="2" id="flagBilirubinNot" cssStyle="margin-right: 3px" onclick="show_hide('urin_bilirubin${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="urin_bilirubin${s.index}" style="<c:if test="${cmd.listUrin[s.index].flagBilirubin eq 0 or cmd.listUrin[s.index].flagBilirubin eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('bilirubin',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center"><form:input id="urin_bilirubin${s.index}" path="listUrin[${s.index}].bilirubin" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_nv_urin_bilirubin${s.index}" size="24" path="listUrin[${s.index}].nv_urin_bilirubin" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_bilirubin_kelainan${s.index}" path="listUrin[${s.index}].bilirubin_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_bilirubin_lic_id${s.index}" size="8" path="listUrin[${s.index}].bilirubin_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=urin_bilirubin&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="urin_bilirubin_lic_desc${s.index}" path="listUrin[${s.index}].bilirubin_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Urobilinogen</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagUrobilinogenNorm"><form:radiobutton path="listUrin[${s.index}].flagUrobilinogen" value="0" id="flagUrobilinogenNorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_urobilinogen${s.index}',0)" />Normal</label>
																								<label for="flagUrobilinogenAbnorm"><form:radiobutton path="listUrin[${s.index}].flagUrobilinogen" value="1" id="flagUrobilinogenAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_urobilinogen${s.index}',1)" />Abnormal</label>
																								<label for="flagUrobilinogenNot"><form:radiobutton path="listUrin[${s.index}].flagUrobilinogen" value="2" id="flagUrobilinogenNot" cssStyle="margin-right: 3px" onclick="show_hide('urin_urobilinogen${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="urin_urobilinogen${s.index}" style="<c:if test="${cmd.listUrin[s.index].flagUrobilinogen eq 0 or cmd.listUrin[s.index].flagUrobilinogen eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('urobilinogen',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center"><form:input id="urin_urobilinogen${s.index}" path="listUrin[${s.index}].urobilinogen" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_nv_urin_urobilinogen${s.index}" size="8" path="listUrin[${s.index}].nv_urin_urobilinogen" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_urobilinogen_kelainan${s.index}" path="listUrin[${s.index}].urobilinogen_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_urobilinogen_lic_id${s.index}" size="8" path="listUrin[${s.index}].urobilinogen_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=urin_urobilinogen&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="urin_urobilinogen_lic_desc${s.index}" path="listUrin[${s.index}].urobilinogen_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Nitrit</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagNitritNorm"><form:radiobutton path="listUrin[${s.index}].flagNitrit" value="0" id="flagNitritNorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_nitrit${s.index}',0)" />Normal</label>
																								<label for="flagNitritAbnorm"><form:radiobutton path="listUrin[${s.index}].flagNitrit" value="1" id="flagNitritAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_nitrit${s.index}',1)" />Abnormal</label>
																								<label for="flagNitritNot"><form:radiobutton path="listUrin[${s.index}].flagNitrit" value="2" id="flagNitritNot" cssStyle="margin-right: 3px" onclick="show_hide('urin_nitrit${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="urin_nitrit${s.index}" style="<c:if test="${cmd.listUrin[s.index].flagNitrit eq 0 or cmd.listUrin[s.index].flagNitrit eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('nitrit',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center"><form:input id="urin_nitrit${s.index}" path="listUrin[${s.index}].nitrit" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_nv_urin_nitrit${s.index}" size="9" path="listUrin[${s.index}].nv_urin_nitrit" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_nitrit_kelainan${s.index}" path="listUrin[${s.index}].nitrit_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_nitrit_lic_id${s.index}" size="8" path="listUrin[${s.index}].nitrit_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=urin_nitrit&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="urin_nitrit_lic_desc${s.index}" path="listUrin[${s.index}].nitrit_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Darah Samar</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagDarah_samarNorm"><form:radiobutton path="listUrin[${s.index}].flagDarah_samar" value="0" id="flagDarah_samarNorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_darah_samar${s.index}',0)" />Normal</label>
																								<label for="flagDarah_samarAbnorm"><form:radiobutton path="listUrin[${s.index}].flagDarah_samar" value="1" id="flagDarah_samarAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_darah_samar${s.index}',1)" />Abnormal</label>
																								<label for="flagDarah_samarNot"><form:radiobutton path="listUrin[${s.index}].flagDarah_samar" value="2" id="flagDarah_samarNot" cssStyle="margin-right: 3px" onclick="show_hide('urin_darah_samar${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="urin_darah_samar${s.index}" style="<c:if test="${cmd.listUrin[s.index].flagDarah_samar eq 0 or cmd.listUrin[s.index].flagDarah_samar eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('dar_sam',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center"><form:input id="urin_darah_samar${s.index}" path="listUrin[${s.index}].darah_samar" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_nv_urin_darah_samar${s.index}" size="9" path="listUrin[${s.index}].nv_urin_darah_samar" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_darah_samar_kelainan${s.index}" path="listUrin[${s.index}].darah_samar_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_darah_samar_lic_id${s.index}" size="8" path="listUrin[${s.index}].darah_samar_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=urin_darah_samar&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="urin_darah_samar_lic_desc${s.index}" path="listUrin[${s.index}].darah_samar_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Lekosit Esterase</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagLeukosit_esteraseNorm"><form:radiobutton path="listUrin[${s.index}].flagLeukosit_esterase" value="0" id="flagLeukosit_esteraseNorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_leukosit_esterase${s.index}',0)" />Normal</label>
																								<label for="flagLeukosit_esteraseAbnorm"><form:radiobutton path="listUrin[${s.index}].flagLeukosit_esterase" value="1" id="flagLeukosit_esteraseAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_leukosit_esterase${s.index}',1)" />Abnormal</label>
																								<label for="flagLeukosit_esteraseNot"><form:radiobutton path="listUrin[${s.index}].flagLeukosit_esterase" value="2" id="flagLeukosit_esteraseNot" cssStyle="margin-right: 3px" onclick="show_hide('urin_leukosit_esterase${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="urin_leukosit_esterase${s.index}" style="<c:if test="${cmd.listUrin[s.index].flagLeukosit_esterase eq 0 or cmd.listUrin[s.index].flagLeukosit_esterase eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('lek_est',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center"><form:input id="urin_lekosit_esterase${s.index}" path="listUrin[${s.index}].lekosit_esterase" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_nv_urin_lekosit_esterase${s.index}" size="9" path="listUrin[${s.index}].nv_urin_lekosit_esterase" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_lekosit_esterase_kelainan${s.index}" path="listUrin[${s.index}].lekosit_esterase_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_lekosit_esterase_lic_id${s.index}" size="8" path="listUrin[${s.index}].lekosit_esterase_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=urin_lekosit_esterase&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="urin_lekosit_esterase_lic_desc${s.index}" path="listUrin[${s.index}].lekosit_esterase_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Sedimen Eritrosit</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagSedimen_eritrositNorm"><form:radiobutton path="listUrin[${s.index}].flagSedimen_eritrosit" value="0" id="flagSedimen_eritrositNorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_sed_eritrosit${s.index}',0)" />Normal</label>
																								<label for="flagSedimen_eritrositAbnorm"><form:radiobutton path="listUrin[${s.index}].flagSedimen_eritrosit" value="1" id="flagSedimen_eritrositAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_sed_eritrosit${s.index}',1)" />Abnormal</label>
																								<label for="flagSedimen_eritrositeNot"><form:radiobutton path="listUrin[${s.index}].flagSedimen_eritrosit" value="2" id="flagSedimen_eritrositeNot" cssStyle="margin-right: 3px" onclick="show_hide('urin_sed_eritrosit${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="urin_sed_eritrosit${s.index}" style="<c:if test="${cmd.listUrin[s.index].flagSedimen_eritrosit eq 0 or cmd.listUrin[s.index].flagSedimen_eritrosit eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('sed_eri',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="urin_sedimen_eritrosit${s.index}" path="listUrin[${s.index}].sedimen_eritrosit" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="urin_nv_urin_sedimen_eritrosit${s.index}" size="9" path="listUrin[${s.index}].nv_urin_eritrosit" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_sedimen_eritrosit_kelainan${s.index}" path="listUrin[${s.index}].sedimen_eritrosit_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_sedimen_eritrosit_lic_id${s.index}" size="8" path="listUrin[${s.index}].sedimen_eritrosit_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=urin_sedimen_eritrosit&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="urin_sedimen_eritrosit_lic_desc${s.index}" path="listUrin[${s.index}].sedimen_eritrosit_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Sedimen Leukosit</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagSedimen_leukositNorm"><form:radiobutton path="listUrin[${s.index}].flagSedimen_leukosit" value="0" id="flagSedimen_leukositNorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_sed_leukosit${s.index}',0)" />Normal</label>
																								<label for="flagSedimen_leukositAbnorm"><form:radiobutton path="listUrin[${s.index}].flagSedimen_leukosit" value="1" id="flagSedimen_leukositAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_sed_leukosit${s.index}',1)" />Abnormal</label>
																								<label for="flagSedimen_leukositNot"><form:radiobutton path="listUrin[${s.index}].flagSedimen_leukosit" value="2" id="flagSedimen_leukositNot" cssStyle="margin-right: 3px" onclick="show_hide('urin_sed_leukosit${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="urin_sed_leukosit${s.index}" style="<c:if test="${cmd.listUrin[s.index].flagSedimen_leukosit eq 0 or cmd.listUrin[s.index].flagSedimen_leukosit eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('sed_leu',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center"style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="urin_sedimen_leukosit${s.index}" path="listUrin[${s.index}].sedimen_leukosit" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="urin_nv_urin_sedimen_leukosit${s.index}" size="9" path="listUrin[${s.index}].nv_urin_lekosit" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_sedimen_leukosit_kelainan${s.index}" path="listUrin[${s.index}].sedimen_leukosit_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_sedimen_leukosit_lic_id${s.index}" size="8" path="listUrin[${s.index}].sedimen_leukosit_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=urin_sedimen_leukosit&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="urin_sedimen_leukosit_lic_desc${s.index}" path="listUrin[${s.index}].sedimen_leukosit_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Sedimen Epitel</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagSedimen_epitelNorm"><form:radiobutton path="listUrin[${s.index}].flagSedimen_epitel" value="0" id="flagSedimen_epitelNorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_sed_epitel${s.index}',0)" />Normal</label>
																								<label for="flagSedimen_epitelAbnorm"><form:radiobutton path="listUrin[${s.index}].flagSedimen_epitel" value="1" id="flagSedimen_epitelAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_sed_epitel${s.index}',1)" />Abnormal</label>
																								<label for="flagSedimen_epitelNot"><form:radiobutton path="listUrin[${s.index}].flagSedimen_epitel" value="2" id="flagSedimen_epitelNot" cssStyle="margin-right: 3px" onclick="show_hide('urin_sed_epitel${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="urin_sed_epitel${s.index}" style="<c:if test="${cmd.listUrin[s.index].flagSedimen_epitel eq 0 or cmd.listUrin[s.index].flagSedimen_epitel eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('sed_epi',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center"style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="urin_sedimen_epitel${s.index}" path="listUrin[${s.index}].sedimen_epitel" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="urin_nv_sedimen_epitel_epitel${s.index}" size="9" path="listUrin[${s.index}].nv_urin_epitel" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_sedimen_epitel_kelainan${s.index}" path="listUrin[${s.index}].sedimen_epitel_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_sedimen_epitel_lic_id${s.index}" size="8" path="listUrin[${s.index}].sedimen_epitel_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=urin_sedimen_epitel&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="urin_sedimen_epitel_lic_desc${s.index}" path="listUrin[${s.index}].sedimen_epitel_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Sedimen Silinder</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagSedimen_silinderNorm"><form:radiobutton path="listUrin[${s.index}].flagSedimen_silinder" value="0" id="flagSedimen_silinderNorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_sed_silinder${s.index}',0)" />Normal</label>
																								<label for="flagSedimen_silinderAbnorm"><form:radiobutton path="listUrin[${s.index}].flagSedimen_silinder" value="1" id="flagSedimen_silinderAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_sed_silinder${s.index}',1)" />Abnormal</label>
																								<label for="flagSedimen_silinderNot"><form:radiobutton path="listUrin[${s.index}].flagSedimen_silinder" value="2" id="flagSedimen_silinderNot" cssStyle="margin-right: 3px" onclick="show_hide('urin_sed_silinder${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="urin_sed_silinder${s.index}" style="<c:if test="${cmd.listUrin[s.index].flagSedimen_silinder eq 0 or cmd.listUrin[s.index].flagSedimen_silinder eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('sed_sil',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center"><form:input id="urin_sedimen_silinder${s.index}" path="listUrin[${s.index}].sedimen_silinder" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_nv_urin_sedimen_silinder${s.index}" size="9" path="listUrin[${s.index}].nv_urin_silinder" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_sedimen_silinder_kelainan${s.index}" path="listUrin[${s.index}].sedimen_silinder_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_sedimen_silinder_lic_id${s.index}" size="8" path="listUrin[${s.index}].sedimen_silinder_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=urin_sedimen_silinder&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="urin_sedimen_silinder_lic_desc${s.index}" path="listUrin[${s.index}].sedimen_silinder_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Sedimen Kristal</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagSedimen_kristalNorm"><form:radiobutton path="listUrin[${s.index}].flagSedimen_kristal" value="0" id="flagSedimen_kristalNorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_sed_kristal${s.index}',0)" />Normal</label>
																								<label for="flagSedimen_kristalAbnorm"><form:radiobutton path="listUrin[${s.index}].flagSedimen_kristal" value="1" id="flagSedimen_kristalAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_sed_kristal${s.index}',1)" />Abnormal</label>
																								<label for="flagSedimen_kristalNot"><form:radiobutton path="listUrin[${s.index}].flagSedimen_kristal" value="2" id="flagSedimen_kristalNot" cssStyle="margin-right: 3px" onclick="show_hide('urin_sed_kristal${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="urin_sed_kristal${s.index}" style="<c:if test="${cmd.listUrin[s.index].flagSedimen_kristal eq 0 or cmd.listUrin[s.index].flagSedimen_kristal eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('sed_kri',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center"><form:input id="urin_sedimen_kristal${s.index}" path="listUrin[${s.index}].sedimen_kristal" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_nv_urin_sedimen_kristal${s.index}" size="9" path="listUrin[${s.index}].nv_urin_kristal" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_sedimen_kristal_kelainan${s.index}" path="listUrin[${s.index}].sedimen_kristal_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_sedimen_kristal_lic_id${s.index}" size="8" path="listUrin[${s.index}].sedimen_kristal_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=urin_sedimen_kristal&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="urin_sedimen_kristal_lic_desc${s.index}" path="listUrin[${s.index}].sedimen_kristal_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Bakteri</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagBakteriNorm"><form:radiobutton path="listUrin[${s.index}].flagBakteri" value="0" id="flagBakteriNorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_bakteri${s.index}',0)" />Normal</label>
																								<label for="flagBakteriAbnorm"><form:radiobutton path="listUrin[${s.index}].flagBakteri" value="1" id="flagBakteriAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('urin_bakteri${s.index}',1)" />Abnormal</label>
																								<label for="flagBakteriNot"><form:radiobutton path="listUrin[${s.index}].flagBakteri" value="2" id="flagBakteriNot" cssStyle="margin-right: 3px" onclick="show_hide('urin_bakteri${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="urin_bakteri${s.index}" style="<c:if test="${cmd.listUrin[s.index].flagSedimen_kristal eq 0 or cmd.listUrin[s.index].flagSedimen_kristal eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('sed_kri',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center"><form:input id="urin_bakteri${s.index}" path="listUrin[${s.index}].bakteri" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_nv_urin_bakteri${s.index}" size="9" path="listUrin[${s.index}].nv_urin_bakteri" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_bakteri_kelainan${s.index}" path="listUrin[${s.index}].bakteri_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="urin_bakteri_lic_id${s.index}" size="8" path="listUrin[${s.index}].bakteri_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=urin_bakteri&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="urin_bakteri_lic_desc${s.index}" path="listUrin[${s.index}].bakteri_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td style="padding-top: 3px">
																		<c:choose>
																			<c:when test="${s.index eq fn:length(cmd.listUrin)-1}">
																				<input type="button" name="addUrin" value="Add Urin" onclick="return tampilkan(this);">
																			</c:when>
																			<c:otherwise>
																				&nbsp;
																			</c:otherwise>	
																		</c:choose>
																	</td>
																</tr>
															</c:forEach>
														</table>													
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</c:if>
					<c:if test="${fn:length(cmd.listAda) ne 0 }">	
						<tr>
							<td>
								<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
									<tr>
										<th width="110" style="color: blue;">ADA - ADAL</th>
										<td style="padding-left: 3px">
											<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
												<tr>
													<td>
														<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
															<c:forEach items="${cmd.listAda}" var="b" varStatus="s">
																<tr>
																	<td>
																		<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																			<tr>
																				<td style="padding: 0px 0px 3px 2px" colspan="5">
																					<label for="flagAdaAda"><form:radiobutton path="listAda[${s.index}].flagAda" value="0" id="flagAdaAda" cssStyle="margin-right: 3px" />ADA</label>
																					<label for="flagAdaAdal"><form:radiobutton path="listAda[${s.index}].flagAda" value="1" id="flagAdaAdal" cssStyle="margin-right: 3px" />ADAL</label>
																					<input type="button" class="button" name="changeFormADA/L" value="Change Form" onclick="return tampilkan(this,${s.index});">
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Tanggal MCU</th>
																				<td style="padding-left: 3px" width="104">
																					<spring:bind  path="cmd.listAda[${s.index}].tglMcuAda">
																						<script>inputDate('${status.expression}', '${status.value}', false);</script> <span style="color: #FF0000">*</span>
																					</spring:bind>									
																				</td>
																				<th width="102">Tempat MCU</th>
																				<td style="padding-left: 3px">									
																					<form:input id="ada_tmpMcu${s.index}" path="listAda[${s.index}].tmpMcuAda" cssStyle="width: 97%"/> <span style="color: #FF0000">*</span>
																				</td>
																				<td style="padding-left: 3px" width="110">
																					<input type="button" name="lsProv" value="List Provider" onclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_provider&sub=ada&indx=${s.index}',480,640)">
																				</td>																	
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td>
																		<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																			<tr>
																				<th width="102">Hemoglobin</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagHemoglobinNorm"><form:radiobutton path="listAda[${s.index}].flagHemoglobin" value="0" id="flagHemoglobinNorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_hemoglobin${s.index}',0)" />Normal</label>
																								<label for="flagHemoglobinAbnorm"><form:radiobutton path="listAda[${s.index}].flagHemoglobin" value="1" id="flagHemoglobinAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_hemoglobin${s.index}',1)" />Abnormal</label>
																								<label for="flagHemoglobinNot"><form:radiobutton path="listAda[${s.index}].flagHemoglobin" value="2" id="flagHemoglobinNot" cssStyle="margin-right: 3px" onclick="show_hide('ada_hemoglobin${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="ada_hemoglobin${s.index}" style="<c:if test="${cmd.listAda[s.index].flagHemoglobin eq 0 or cmd.listAda[s.index].flagHemoglobin eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('hemoglobin',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_hemoglobin${s.index}" path="listAda[${s.index}].hemoglobin" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_nv_darah_rutin_hemoglobin${s.index}" size="16" path="listAda[${s.index}].nv_darah_rutin_hemoglobin" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_hemoglobin_kelainan${s.index}" path="listAda[${s.index}].hemoglobin_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_hemoglobin_lic_id${s.index}" size="8" path="listAda[${s.index}].hemoglobin_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=ada_hemoglobin&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="ada_hemoglobin_lic_desc${s.index}" path="listAda[${s.index}].hemoglobin_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Leukosit</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagLeukositNorm"><form:radiobutton path="listAda[${s.index}].flagLeukosit" value="0" id="flagLeukositNorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_leukosit${s.index}',0)" />Normal</label>
																								<label for="flagLeukositAbnorm"><form:radiobutton path="listAda[${s.index}].flagLeukosit" value="1" id="flagLeukositAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_leukosit${s.index}',1)" />Abnormal</label>
																								<label for="flagLeukositNot"><form:radiobutton path="listAda[${s.index}].flagLeukosit" value="2" id="flagLeukositNot" cssStyle="margin-right: 3px" onclick="show_hide('ada_leukosit${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="ada_leukosit${s.index}" style="<c:if test="${cmd.listAda[s.index].flagLeukosit eq 0 or cmd.listAda[s.index].flagLeukosit eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('leukosit',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_leukosit${s.index}" path="listAda[${s.index}].leukosit" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_nv_darah_rutin_leukosit${s.index}" size="19" path="listAda[${s.index}].nv_darah_rutin_leukosit" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_leukosit_kelainan${s.index}" path="listAda[${s.index}].leukosit_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_leukosit_lic_id${s.index}" size="8" path="listAda[${s.index}].leukosit_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=ada_leukosit&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="ada_leukosit_lic_desc${s.index}" path="listAda[${s.index}].leukosit_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Eosinofil</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagEosinofilNorm"><form:radiobutton path="listAda[${s.index}].flagEosinofil" value="0" id="flagEosinofilNorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_eosinofil${s.index}',0)" />Normal</label>
																								<label for="flagEosinofilAbnorm"><form:radiobutton path="listAda[${s.index}].flagEosinofil" value="1" id="flagEosinofilAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_eosinofil${s.index}',1)" />Abnormal</label>
																								<label for="flagEosinofilNot"><form:radiobutton path="listAda[${s.index}].flagEosinofil" value="2" id="flagEosinofilNot" cssStyle="margin-right: 3px" onclick="show_hide('ada_eosinofil${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="ada_eosinofil${s.index}" style="<c:if test="${cmd.listAda[s.index].flagEosinofil eq 0 or cmd.listAda[s.index].flagEosinofil eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('eosinofil',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_eosinofil${s.index}" path="listAda[${s.index}].eosinofil" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_nv_darah_rutin_eosinofil${s.index}" size="6" path="listAda[${s.index}].nv_darah_rutin_eosinofil" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_eosinofil_kelainan${s.index}" path="listAda[${s.index}].eosinofil_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_eosinofil_lic_id${s.index}" size="8" path="listAda[${s.index}].eosinofil_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=ada_eosinofil&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="ada_eosinofil_lic_desc${s.index}" path="listAda[${s.index}].eosinofil_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Basofil</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagBasofilNorm"><form:radiobutton path="listAda[${s.index}].flagBasofil" value="0" id="flagBasofilNorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_basofil${s.index}',0)" />Normal</label>
																								<label for="flagBasofilAbnorm"><form:radiobutton path="listAda[${s.index}].flagBasofil" value="1" id="flagBasofilAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_basofil${s.index}',1)" />Abnormal</label>
																								<label for="flagBasofilNot"><form:radiobutton path="listAda[${s.index}].flagBasofil" value="2" id="flagBasofilNot" cssStyle="margin-right: 3px" onclick="show_hide('ada_basofil${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="ada_basofil${s.index}" style="<c:if test="${cmd.listAda[s.index].flagBasofil eq 0 or cmd.listAda[s.index].flagBasofil eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('basofil',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_basofil${s.index}" path="listAda[${s.index}].basofil" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_nv_darah_rutin_basofil${s.index}" size="6" path="listAda[${s.index}].nv_darah_rutin_basofil" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_basofil_kelainan${s.index}" path="listAda[${s.index}].basofil_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_basofil_lic_id${s.index}" size="8" path="listAda[${s.index}].basofil_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=ada_basofil&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="ada_basofil_lic_desc${s.index}" path="listAda[${s.index}].basofil_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Neutrofil Batang</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagNeutrofil_batangNorm"><form:radiobutton path="listAda[${s.index}].flagNeutrofil_batang" value="0" id="flagNeutrofil_batangNorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_neutrofil_batang${s.index}',0)" />Normal</label>
																								<label for="flagNeutrofil_batangAbnorm"><form:radiobutton path="listAda[${s.index}].flagNeutrofil_batang" value="1" id="flagNeutrofil_batangAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_neutrofil_batang${s.index}',1)" />Abnormal</label>
																								<label for="flagNeutrofil_batangNot"><form:radiobutton path="listAda[${s.index}].flagNeutrofil_batang" value="2" id="flagNeutrofil_batangNot" cssStyle="margin-right: 3px" onclick="show_hide('ada_neutrofil_batang${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="ada_neutrofil_batang${s.index}" style="<c:if test="${cmd.listAda[s.index].flagNeutrofil_batang eq 0 or cmd.listAda[s.index].flagNeutrofil_batang eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('neu_bat',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_neutrofil_batang${s.index}" path="listAda[${s.index}].neutrofil_batang" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_nv_darah_rutin_neutrofil_batang${s.index}" size="6" path="listAda[${s.index}].nv_darah_rutin_neutrofil_batang" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_neutrofil_batang_kelainan${s.index}" path="listAda[${s.index}].neutrofil_batang_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_neutrofil_batang_lic_id${s.index}" size="8" path="listAda[${s.index}].neutrofil_batang_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=ada_neutrofil_batang&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="ada_neutrofil_batang_lic_desc${s.index}" path="listAda[${s.index}].neutrofil_batang_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Neutrofil Segmen</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagNeutrofil_segmenNorm"><form:radiobutton path="listAda[${s.index}].flagNeutrofil_segmen" value="0" id="flagNeutrofil_segmenNorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_neutrofil_segmen${s.index}',0)" />Normal</label>
																								<label for="flagNeutrofil_segmenAbnorm"><form:radiobutton path="listAda[${s.index}].flagNeutrofil_segmen" value="1" id="flagNeutrofil_segmenAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_neutrofil_segmen${s.index}',1)" />Abnormal</label>
																								<label for="flagNeutrofil_segmenNot"><form:radiobutton path="listAda[${s.index}].flagNeutrofil_segmen" value="2" id="flagNeutrofil_segmenNot" cssStyle="margin-right: 3px" onclick="show_hide('ada_neutrofil_segmen${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="ada_neutrofil_segmen${s.index}" style="<c:if test="${cmd.listAda[s.index].flagNeutrofil_segmen eq 0 or cmd.listAda[s.index].flagNeutrofil_segmen eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('neu_seg',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_neutrofil_segmen${s.index}" path="listAda[${s.index}].neutrofil_segmen" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_nv_darah_rutin_neutrofil_segmen${s.index}" size="9" path="listAda[${s.index}].nv_darah_rutin_neutrofil_segmen" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_neutrofil_segmen_kelainan${s.index}" path="listAda[${s.index}].neutrofil_segmen_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_neutrofil_segmen_lic_id${s.index}" size="8" path="listAda[${s.index}].neutrofil_segmen_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=ada_neutrofil_segmen&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="ada_neutrofil_segmen_lic_desc${s.index}" path="listAda[${s.index}].neutrofil_segmen_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Limfosit</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagLimfositNorm"><form:radiobutton path="listAda[${s.index}].flagLimfosit" value="0" id="flagLimfositNorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_limfosit${s.index}',0)" />Normal</label>
																								<label for="flagLimfositAbnorm"><form:radiobutton path="listAda[${s.index}].flagLimfosit" value="1" id="flagLimfositAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_limfosit${s.index}',1)" />Abnormal</label>
																								<label for="flagLimfositNot"><form:radiobutton path="listAda[${s.index}].flagLimfosit" value="2" id="flagLimfositNot" cssStyle="margin-right: 3px" onclick="show_hide('ada_limfosit${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="ada_limfosit${s.index}" style="<c:if test="${cmd.listAda[s.index].flagLimfosit eq 0 or cmd.listAda[s.index].flagLimfosit eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('limfosit',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_limfosit${s.index}" path="listAda[${s.index}].limfosit" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_nv_darah_rutin_limfosit${s.index}" size="9" path="listAda[${s.index}].nv_darah_rutin_limfosit" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_limfosit_kelainan${s.index}" path="listAda[${s.index}].limfosit_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_limfosit_lic_id${s.index}" size="8" path="listAda[${s.index}].limfosit_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=ada_limfosit&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="ada_limfosit_lic_desc${s.index}" path="listAda[${s.index}].limfosit_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Monosit</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagMonositNorm"><form:radiobutton path="listAda[${s.index}].flagMonosit" value="0" id="flagMonositNorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_monosit${s.index}',0)" />Normal</label>
																								<label for="flagMonositAbnorm"><form:radiobutton path="listAda[${s.index}].flagMonosit" value="1" id="flagMonositAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_monosit${s.index}',1)" />Abnormal</label>
																								<label for="flagMonositNot"><form:radiobutton path="listAda[${s.index}].flagMonosit" value="2" id="flagMonositNot" cssStyle="margin-right: 3px" onclick="show_hide('ada_monosit${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="ada_monosit${s.index}" style="<c:if test="${cmd.listAda[s.index].flagMonosit eq 0 or cmd.listAda[s.index].flagMonosit eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('monosit',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_monosit${s.index}" path="listAda[${s.index}].monosit" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_nv_darah_rutin_monosit${s.index}" size="6" path="listAda[${s.index}].nv_darah_rutin_monosit" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_monosit_kelainan${s.index}" path="listAda[${s.index}].monosit_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_monosit_lic_id${s.index}" size="8" path="listAda[${s.index}].monosit_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=ada_monosit&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="ada_monosit_lic_desc${s.index}" path="listAda[${s.index}].monosit_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Trombosit</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagTrombositNorm"><form:radiobutton path="listAda[${s.index}].flagTrombosit" value="0" id="flagTrombositNorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_trombosit${s.index}',0)" />Normal</label>
																								<label for="flagTrombositAbnorm"><form:radiobutton path="listAda[${s.index}].flagTrombosit" value="1" id="flagTrombositAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_trombosit${s.index}',1)" />Abnormal</label>
																								<label for="flagTrombositNot"><form:radiobutton path="listAda[${s.index}].flagTrombosit" value="2" id="flagTrombositNot" cssStyle="margin-right: 3px" onclick="show_hide('ada_trombosit${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="ada_trombosit${s.index}" style="<c:if test="${cmd.listAda[s.index].flagTrombosit eq 0 or cmd.listAda[s.index].flagTrombosit eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('trombosit',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_trombosit${s.index}" path="listAda[${s.index}].trombosit" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_nv_darah_rutin_trombosit${s.index}" size="23" path="listAda[${s.index}].nv_darah_rutin_trombosit" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_trombosit_kelainan${s.index}" path="listAda[${s.index}].trombosit_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_trombosit_lic_id${s.index}" size="8" path="listAda[${s.index}].trombosit_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=ada_trombosit&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="ada_trombosit_lic_desc${s.index}" path="listAda[${s.index}].trombosit_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Eritrosit</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagEritrositNorm"><form:radiobutton path="listAda[${s.index}].flagEritrosit" value="0" id="flagEritrositNorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_eritrosit${s.index}',0)" />Normal</label>
																								<label for="flagEritrositAbnorm"><form:radiobutton path="listAda[${s.index}].flagEritrosit" value="1" id="flagEritrositAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_eritrosit${s.index}',1)" />Abnormal</label>
																								<label for="flagEritrositNot"><form:radiobutton path="listAda[${s.index}].flagEritrosit" value="2" id="flagEritrositNot" cssStyle="margin-right: 3px" onclick="show_hide('ada_eritrosit${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="ada_eritrosit${s.index}" style="<c:if test="${cmd.listAda[s.index].flagTrombosit eq 0 or cmd.listAda[s.index].flagTrombosit eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('eritrosit',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_eritrosit${s.index}" path="listAda[${s.index}].eritrosit" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_nv_darah_rutin_eritrosit${s.index}" size="16" path="listAda[${s.index}].nv_darah_rutin_eritrosit" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_eritrosit_kelainan${s.index}" path="listAda[${s.index}].eritrosit_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_eritrosit_lic_id${s.index}" size="8" path="listAda[${s.index}].eritrosit_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=ada_eritrosit&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="ada_eritrosit_lic_desc${s.index}" path="listAda[${s.index}].eritrosit_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">Hematokrit</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagHematokritNorm"><form:radiobutton path="listAda[${s.index}].flagHematokrit" value="0" id="flagHematokritNorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_hematokrit${s.index}',0)" />Normal</label>
																								<label for="flagHematokritAbnorm"><form:radiobutton path="listAda[${s.index}].flagHematokrit" value="1" id="flagHematokritAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_hematokrit${s.index}',1)" />Abnormal</label>
																								<label for="flagHematokritNot"><form:radiobutton path="listAda[${s.index}].flagHematokrit" value="2" id="flagHematokritNot" cssStyle="margin-right: 3px" onclick="show_hide('ada_hematokrit${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="ada_hematokrit${s.index}" style="<c:if test="${cmd.listAda[s.index].flagHematokrit eq 0 or cmd.listAda[s.index].flagHematokrit eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('hematokrit',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_hematokrit${s.index}" path="listAda[${s.index}].hematokrit" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_nv_darah_rutin_hematokrit${s.index}" size="9" path="listAda[${s.index}].nv_darah_rutin_hematokrit" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_hematokrit_kelainan${s.index}" path="listAda[${s.index}].hematokrit_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_hematokrit_lic_id${s.index}" size="8" path="listAda[${s.index}].hematokrit_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=ada_hematokrit&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="ada_hematokrit_lic_desc${s.index}" path="listAda[${s.index}].hematokrit_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">LED</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagLedNorm"><form:radiobutton path="listAda[${s.index}].flagLed" value="0" id="flagLedNorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_led${s.index}',0)" />Normal</label>
																								<label for="flagLedAbnorm"><form:radiobutton path="listAda[${s.index}].flagLed" value="1" id="flagLedAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_led${s.index}',1)" />Abnormal</label>
																								<label for="flagLedNot"><form:radiobutton path="listAda[${s.index}].flagLed" value="2" id="flagLedNot" cssStyle="margin-right: 3px" onclick="show_hide('ada_led${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="ada_led${s.index}" style="<c:if test="${cmd.listAda[s.index].flagLed eq 0 or cmd.listAda[s.index].flagLed eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('led',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_led${s.index}" path="listAda[${s.index}].led" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_nv_darah_rutin_led${s.index}" size="14" path="listAda[${s.index}].nv_darah_rutin_led" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_led_kelainan${s.index}" path="listAda[${s.index}].led_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_led_lic_id${s.index}" size="8" path="listAda[${s.index}].led_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=ada_led&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="ada_led_lic_desc${s.index}" path="listAda[${s.index}].led_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">MCV</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagMcvNorm"><form:radiobutton path="listAda[${s.index}].flagMcv" value="0" id="flagMcvNorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_mcv${s.index}',0)" />Normal</label>
																								<label for="flagMcvAbnorm"><form:radiobutton path="listAda[${s.index}].flagMcv" value="1" id="flagMcvAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_mcv${s.index}',1)" />Abnormal</label>
																								<label for="flagMcvNot"><form:radiobutton path="listAda[${s.index}].flagMcv" value="2" id="flagMcvNot" cssStyle="margin-right: 3px" onclick="show_hide('ada_mcv${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="ada_mcv${s.index}" style="<c:if test="${cmd.listAda[s.index].flagMcv eq 0 or cmd.listAda[s.index].flagMcv eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('mcv',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_mcv${s.index}" path="listAda[${s.index}].mcv" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_nv_darah_rutin_mcv${s.index}" size="10" path="listAda[${s.index}].nv_darah_rutin_mcv" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_mcv_kelainan${s.index}" path="listAda[${s.index}].mcv_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_mcv_lic_id${s.index}" size="8" path="listAda[${s.index}].mcv_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=ada_mcv&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="ada_mcv_lic_desc${s.index}" path="listAda[${s.index}].mcv_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">MCH</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagMchNorm"><form:radiobutton path="listAda[${s.index}].flagMch" value="0" id="flagMchNorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_mch${s.index}',0)" />Normal</label>
																								<label for="flagMchAbnorm"><form:radiobutton path="listAda[${s.index}].flagMch" value="1" id="flagMchAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_mch${s.index}',1)" />Abnormal</label>
																								<label for="flagMchNot"><form:radiobutton path="listAda[${s.index}].flagMch" value="2" id="flagMchNot" cssStyle="margin-right: 3px" onclick="show_hide('ada_mch${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="ada_mch${s.index}" style="<c:if test="${cmd.listAda[s.index].flagMch eq 0 or cmd.listAda[s.index].flagMch eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('mch',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_mch${s.index}" path="listAda[${s.index}].mch" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_nv_darah_rutin_mch${s.index}" size="14" path="listAda[${s.index}].nv_darah_rutin_mch" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_mch_kelainan${s.index}" path="listAda[${s.index}].mch_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_mch_lic_id${s.index}" size="8" path="listAda[${s.index}].mch_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=ada_mch&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="ada_mch_lic_desc${s.index}" path="listAda[${s.index}].mch_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">MCHC</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagMchcNorm"><form:radiobutton path="listAda[${s.index}].flagMchc" value="0" id="flagMchcNorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_mchc${s.index}',0)" />Normal</label>
																								<label for="flagMchcAbnorm"><form:radiobutton path="listAda[${s.index}].flagMchc" value="1" id="flagMchcAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_mchc${s.index}',1)" />Abnormal</label>
																								<label for="flagMchcNot"><form:radiobutton path="listAda[${s.index}].flagMchc" value="2" id="flagMchcNot" cssStyle="margin-right: 3px" onclick="show_hide('ada_mchc${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="ada_mchc${s.index}" style="<c:if test="${cmd.listAda[s.index].flagMchc eq 0 or cmd.listAda[s.index].flagMchc eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('mchc',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_mchc${s.index}" path="listAda[${s.index}].mchc" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_nv_darah_rutin_mchc${s.index}" size="12" path="listAda[${s.index}].nv_darah_rutin_mchc" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_mchc_kelainan${s.index}" path="listAda[${s.index}].mchc_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_mchc_lic_id${s.index}" size="8" path="listAda[${s.index}].mchc_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=ada_mchc&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="ada_mchc_lic_desc${s.index}" path="listAda[${s.index}].mchc_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<th width="102">RDW</th>
																				<td>
																					<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																						<tr>
																							<td style="padding: 0px 0px 3px 2px">
																								<label for="flagRdwNorm"><form:radiobutton path="listAda[${s.index}].flagRdw" value="0" id="flagRdwNorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_rdw${s.index}',0)" />Normal</label>
																								<label for="flagRdwAbnorm"><form:radiobutton path="listAda[${s.index}].flagRdw" value="1" id="flagRdwAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('ada_rdw${s.index}',1)" />Abnormal</label>
																								<label for="flagRdwNot"><form:radiobutton path="listAda[${s.index}].flagRdw" value="2" id="flagRdwNot" cssStyle="margin-right: 3px" onclick="show_hide('ada_rdw${s.index}',2)" />Tidak Ada</label>
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="ada_rdw${s.index}" style="<c:if test="${cmd.listAda[s.index].flagRdw eq 0 or cmd.listAda[s.index].flagRdw eq 2}">height:0;display:none;</c:if>">
																									<tr>
																										<th>Hasil</th>
																										<th>Normal</th>
																										<th>Kelainan</th>
																										<th>ICD</th>
																										<th style="width: 100%">ICD Desc</th>
																										<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('rdw',${s.index})"></td>
																									</tr>
																									<tr>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_rdw${s.index}" path="listAda[${s.index}].rdw" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ada_nv_darah_rutin_rdw${s.index}" size="14" path="listAda[${s.index}].nv_darah_rutin_rdw" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_rdw_kelainan${s.index}" path="listAda[${s.index}].rdw_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																										<td align="center"><form:input id="ada_rdw_lic_id${s.index}" size="8" path="listAda[${s.index}].rdw_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=ada_rdw&indx=${s.index}',550,700)" /></td>
																										<td align="center"><form:input id="ada_rdw_lic_desc${s.index}" path="listAda[${s.index}].rdw_lic_desc" cssStyle="width:100%" /></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<c:if test="${cmd.listAda[s.index].flagAda eq 1}">
																				<tr>
																					<th width="102">Gula Darah</th>
																					<td>
																						<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																							<tr>
																								<th width="102">Gula Darah Puasa</th>
																								<td>
																									<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																										<tr>
																											<td style="padding: 0px 0px 3px 2px">
																												<label for="flagGulaDarahPuasaNorm"><form:radiobutton path="listAda[${s.index}].flagGulaDarahPuasa" value="0" id="flagGulaDarahPuasaNorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_gula_darah_puasa${s.index}',0)" />Normal</label>
																												<label for="flagGulaDarahPuasaAbnorm"><form:radiobutton path="listAda[${s.index}].flagGulaDarahPuasa" value="1" id="flagGulaDarahPuasaAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_gula_darah_puasa${s.index}',1)" />Abnormal</label>
																												<label for="flagGulaDarahPuasaNot"><form:radiobutton path="listAda[${s.index}].flagGulaDarahPuasa" value="2" id="flagGulaDarahPuasaNot" cssStyle="margin-right: 3px" onclick="show_hide('adal_gula_darah_puasa${s.index}',2)" />Tidak Ada</label>
																											</td>
																										</tr>
																										<tr>
																											<td>
																												<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="adal_gula_darah_puasa${s.index}" style="<c:if test="${cmd.listAda[s.index].flagGulaDarahPuasa eq 0 or cmd.listAda[s.index].flagGulaDarahPuasa eq 2}">height:0;display:none;</c:if>">
																													<tr>
																														<th>Hasil</th>
																														<th>Normal</th>
																														<th>Kelainan</th>
																														<th>ICD</th>
																														<th style="width: 100%">ICD Desc</th>
																														<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('glu_dar',${s.index})"></td>
																													</tr>
																													<tr>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="gula_darah_puasa${s.index}" path="listAda[${s.index}].gula_darah_puasa" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="nv_gula_darah_puasa${s.index}" size="13" path="listAda[${s.index}].nv_gula_darah_glukosa_darah_puasa" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="gula_darah_puasa_kelainan${s.index}" path="listAda[${s.index}].gula_darah_puasa_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="gula_darah_puasa_lic_id${s.index}" size="8" path="listAda[${s.index}].gula_darah_puasa_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=gula_darah_puasa&indx=${s.index}',550,700)" /></td>
																														<td align="center"><form:input id="gula_darah_puasa_lic_desc${s.index}" path="listAda[${s.index}].gula_darah_puasa_lic_desc" cssStyle="width:100%" /></td>
																													</tr>
																												</table>
																											</td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																							<tr>
																								<th width="102">Glukosa 2 jam pp</th>
																								<td>
																									<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																										<tr>
																											<td style="padding: 0px 0px 3px 2px">
																												<label for="flagGulaDarahPpNorm"><form:radiobutton path="listAda[${s.index}].flagGulaDarahPp" value="0" id="flagGulaDarahPpNorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_gula_darah_pp${s.index}',0)" />Normal</label>
																												<label for="flagGulaDarahPpAbnorm"><form:radiobutton path="listAda[${s.index}].flagGulaDarahPp" value="1" id="flagGulaDarahPpAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_gula_darah_pp${s.index}',1)" />Abnormal</label>
																												<label for="flagGulaDarahPpNot"><form:radiobutton path="listAda[${s.index}].flagGulaDarahPp" value="2" id="flagGulaDarahPpNot" cssStyle="margin-right: 3px" onclick="show_hide('adal_gula_darah_pp${s.index}',2)" />Tidak Ada</label>
																											</td>
																										</tr>
																										<tr>
																											<td>
																												<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="adal_gula_darah_pp${s.index}" style="<c:if test="${cmd.listAda[s.index].flagGulaDarahPp eq 0 or cmd.listAda[s.index].flagGulaDarahPp eq 2}">height:0;display:none;</c:if>">
																													<tr>
																														<th>Hasil</th>
																														<th>Normal</th>
																														<th>Kelainan</th>
																														<th>ICD</th>
																														<th style="width: 100%">ICD Desc</th>
																														<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('glu_2',${s.index})"></td>
																													</tr>
																													<tr>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="gula_darah_pp${s.index}" path="listAda[${s.index}].gula_darah_pp" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="nv_gula_darah_glukosa_pp${s.index}" size="13" path="listAda[${s.index}].nv_gula_darah_glukosa_pp" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="gula_darah_pp_kelainan${s.index}" path="listAda[${s.index}].gula_darah_pp_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="gula_darah_pp_lic_id${s.index}" size="8" path="listAda[${s.index}].gula_darah_pp_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=gula_darah_puasa&indx=${s.index}',550,700)" /></td>
																														<td align="center"><form:input id="gula_darah_pp_lic_desc${s.index}" path="listAda[${s.index}].gula_darah_pp_lic_desc" cssStyle="width:100%" /></td>
																													</tr>
																												</table>
																											</td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																							<tr>
																								<th width="102">HbA1c</th>
																								<td>
																									<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																										<tr>
																											<td style="padding: 0px 0px 3px 2px">
																												<label for="flagHb1cNorm"><form:radiobutton path="listAda[${s.index}].flagHb1c" value="0" id="flagHb1cNorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_hba1c${s.index}',0)" />Normal</label>
																												<label for="flagHb1cAbnorm"><form:radiobutton path="listAda[${s.index}].flagHb1c" value="1" id="flagHb1cAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_hba1c${s.index}',1)" />Abnormal</label>
																												<label for="flagHb1cNot"><form:radiobutton path="listAda[${s.index}].flagHb1c" value="2" id="flagHb1cNot" cssStyle="margin-right: 3px" onclick="show_hide('adal_hba1c${s.index}',2)" />Tidak Ada</label>
																											</td>
																										</tr>
																										<tr>
																											<td>
																												<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="adal_hba1c${s.index}" style="<c:if test="${cmd.listAda[s.index].flagHb1c eq 0 or cmd.listAda[s.index].flagHb1c eq 2}">height:0;display:none;</c:if>">
																													<tr>
																														<th>Hasil</th>
																														<th>Normal</th>
																														<th>Kelainan</th>
																														<th>ICD</th>
																														<th style="width: 100%">ICD Desc</th>
																														<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('hba1c',${s.index})"></td>
																													</tr>
																													<tr>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="hba1c${s.index}" path="listAda[${s.index}].hba1c" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="nv_gula_darah_glukosa_hba1c${s.index}" size="8" path="listAda[${s.index}].nv_gula_darah_glukosa_hba1c" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="hba1c_kelainan${s.index}" path="listAda[${s.index}].hba1c_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="hba1c_lic_id${s.index}" size="8" path="listAda[${s.index}].hba1c_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=hba1c&indx=${s.index}',550,700)" /></td>
																														<td align="center"><form:input id="hba1c_lic_desc${s.index}" path="listAda[${s.index}].hba1c_lic_desc" cssStyle="width:100%" /></td>
																													</tr>
																												</table>
																											</td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																				<tr>
																					<th width="102">Lemak Darah</th>
																					<td>
																						<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																							<tr>
																								<th width="102">Satuan</th>
																								<td style="padding-left: 3px;" colspan="3">
																									<form:select id="satuanChol" path="listAda[${s.index}].satuanChol" title="satuan cholesterol">
																										<form:options items="${lsSatChol}" itemLabel="key" itemValue="value"/>  
																									</form:select>
																								</td>
																							</tr>
																							<tr>
																								<th width="102">Total Cholesterol</th>
																								<td style="padding-left: 3px;"><form:input id="total_cholesterol${s.index}" path="listAda[${s.index}].total_cholesterol" size="6" onkeypress="return maxChar(event,this.value,3)" /></td>
																								<th width="102">Normal</th>
																								<td style="text-transform: none;padding-left: 3px" nowrap="nowrap" width="45"><form:input id="nv_lemak_darah_total_cholesterol${s.index}" size="13" path="listAda[${s.index}].nv_lemak_darah_total_cholesterol" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																								<c:choose> 
																									<c:when test="${not empty cmd.listAda[s.index].chol_hdl}">
																										<th width="102">Chol / HDL</th>
																										<td style="padding-left: 3px;" width="45"><form:input id="chol_hdl${s.index}" path="listAda[${s.index}].chol_hdl" size="6" /></td>
																										<th width="102">Normal</th>
																										<td style="text-transform: none;padding-left: 3px" nowrap="nowrap"><form:input id="nv_lemak_darah_ratio_total_chol_hdl${s.index}" size="5" path="listAda[${s.index}].nv_lemak_darah_ratio_total_chol_hdl" cssStyle="background-color: #6FC1F3;text-transform: none;" /></td>
																									</c:when>
																									<c:otherwise>
																										<td colspan="4">&nbsp;</td>
																									</c:otherwise>	
																								</c:choose>	
																							</tr>
																							<tr>
																								<th width="102">HDL Cholesterol</th>
																								<td style="padding-left: 3px;" width="45"><form:input id="hdl_cholesterol${s.index}" path="listAda[${s.index}].hdl_cholesterol" size="6" onkeypress="return maxChar(event,this.value,3)" /></td>
																								<th width="102">Normal</th>
																								<td style="text-transform: none;padding-left: 3px" nowrap="nowrap"><form:input id="nv_lemak_darah_hdl_cholesterol${s.index}" size="13" path="listAda[${s.index}].nv_lemak_darah_hdl_cholesterol" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																								<c:choose>
																									<c:when test="${not empty cmd.listAda[s.index].ratio_cholesterol}">
																										<th width="102">Rating</th>
																										<td style="padding-left: 3px;" width="45"><form:input id="ratio_cholesterol${s.index}" path="listAda[${s.index}].ratio_cholesterol" size="6" /></td>
																										<!--<th width="102">Normal</th>
																										<td style="text-transform: none;padding-left: 3px" nowrap="nowrap"><form:input id="nv_lemak_darah_ratio_total_chol_hdl${s.index}" size="5" path="listAda[${s.index}].nv_lemak_darah_ratio_total_chol_hdl" cssStyle="background-color: #6FC1F3;text-transform: none;" /></td>-->
																									</c:when>
																									<c:otherwise>
																										<td colspan="2">&nbsp;</td>
																									</c:otherwise>	
																								</c:choose>	
																							</tr>
																							<tr>
																								<th width="102">LDL Cholesterol</th>
																								<td style="padding-left: 3px;" width="45"><form:input id="ldl_cholesterol${s.index}" path="listAda[${s.index}].ldl_cholesterol" size="6" onkeypress="return maxChar(event,this.value,3)" /></td>
																								<th width="102">Normal</th>
																								<td style="text-transform: none;padding-left: 3px" nowrap="nowrap" width="80"><form:input id="nv_lemak_darah_ldl_cholesterol${s.index}" size="13" path="listAda[${s.index}].nv_lemak_darah_ldl_cholesterol" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																								<c:choose>
																									<c:when test="${not empty cmd.listAda[s.index].ldl_hdl}">
																										<th width="102">LDL / HDL</th>
																										<td style="padding-left: 3px;"><form:input id="ldl_hdl${s.index}" path="listAda[${s.index}].ldl_hdl" size="6" /></td>
																										<th width="102">Normal</th>
																										<td style="text-transform: none;padding-left: 3px" nowrap="nowrap"><form:input id="nv_lemak_darah_ratio_ldl_hdl${s.index}" size="5" path="listAda[${s.index}].nv_lemak_darah_ratio_ldl_hdl" cssStyle="background-color: #6FC1F3;text-transform: none;" /></td>
																									</c:when>
																									<c:otherwise>
																										<td colspan="4">&nbsp;</td>
																									</c:otherwise>	
																								</c:choose>
																							</tr>
																							<tr>
																								<th width="102">Trigiliserida</th>
																								<td style="padding-left: 3px;"><form:input id="trigiliserida${s.index}" path="listAda[${s.index}].trigiliserida" size="6" onkeypress="return maxChar(event,this.value,10)" /></td>
																								<th width="102">Normal</th>
																								<td style="text-transform: none;padding-left: 3px" nowrap="nowrap"><form:input id="nv_lemak_darah_trigliserida${s.index}" size="13" path="listAda[${s.index}].nv_lemak_darah_trigliserida" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																								<td colspan="4" style="padding-top: 3px;"></td>
																							</tr>
																						</table>		
																					</td>
																				</tr>
																				<tr>
																					<th width="102">Fungsi Hati</th>
																					<td>
																						<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																							<tr>
																								<td colspan="2" style="color:red;">
																									<input type="button" name="countChol" value="Count" style="color:red;" onclick="return tampilkan(this);"> ==> COUNT LIFE RATING
																								</td>
																							</tr>
																							<c:if test="${ not empty rating}">
																							<tr>
																								<td colspan="2">
																									<fieldset>
																									<legend>LIFE RATING</legend>
																									<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																										<tr>
																											<td>
																												<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="adal_sgot${s.index}" style="<c:if test="${cmd.listAda[s.index].flagSgot eq 0 or cmd.listAda[s.index].flagSgot eq 2}">height:0;display:none;</c:if>">
																													<tr>
																														<th>SGOT</th><th style="background-color: yellow;">${sgot_rate}</th>
																													</tr>
																													<tr>
																														<th>SGPT</th><th style="background-color: yellow;">${sgpt_rate}</th>
																													</tr>
																													<tr>
																														<th>GGT</th><th style="background-color: yellow;">${ggt_rate}</th>
																													</tr>
																												</table>
																											</td>
																										</tr>
																										<tr>
																											<td>
																												<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="adal_sgot${s.index}" style="<c:if test="${cmd.listAda[s.index].flagSgot eq 0 or cmd.listAda[s.index].flagSgot eq 2}">height:0;display:none;</c:if>">
																													<tr">
																														<th style="background-color: yellow;">Life</th>
																														<th style="background-color: yellow;">TPD</th>
																														<th style="background-color: yellow;">ADB</th>
																														<th style="background-color: yellow;">WOP</th>
																													</tr>
																													<c:forEach items="${rating}" var="rating" >
																														<tr>
																															<th style="background-color: yellow;">${rating.T_VALUE}</th>
																															<th style="background-color: yellow;">${rating.TPD}</th>
																															<th style="background-color: yellow;">${rating.ADB}</th>
																															<th style="background-color: yellow;">${rating.WOP}</th>
																														</tr>
																													</c:forEach>
																												</table>
																											</td>
																										</tr>
																									</table>
																									</fieldset>
																								</td>
																							</tr>
																							</c:if>
																							<tr>
																								<th width="102">SGOT</th>
																								<td>
																									<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																										<tr>
																											<td style="padding: 0px 0px 3px 2px">
																												<label for="flagSgotNorm"><form:radiobutton path="listAda[${s.index}].flagSgot" value="0" id="flagSgotNorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_sgot${s.index}',0)" />Normal</label>
																												<label for="flagSgotAbnorm"><form:radiobutton path="listAda[${s.index}].flagSgot" value="1" id="flagSgotAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_sgot${s.index}',1)" />Abnormal</label>
																												<label for="flagSgotNot"><form:radiobutton path="listAda[${s.index}].flagSgot" value="2" id="flagSgotNot" cssStyle="margin-right: 3px" onclick="show_hide('adal_sgot${s.index}',2)" />Tidak Ada</label>
																											</td>
																										</tr>
																										<tr>
																											<td>
																												<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="adal_sgot${s.index}" style="<c:if test="${cmd.listAda[s.index].flagSgot eq 0 or cmd.listAda[s.index].flagSgot eq 2}">height:0;display:none;</c:if>">
																													<tr>
																														<th>Hasil</th>
																														<th>Normal</th>
																														<th>Kelainan</th>
																														<th>ICD</th>
																														<th style="width: 100%">ICD Desc</th>
																														<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('sgot',${s.index})"></td>
																													</tr>
																													<tr>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="sgot${s.index}" path="listAda[${s.index}].sgot" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="nv_fungsi_hati_sgot_ast${s.index}" size="13" path="listAda[${s.index}].nv_fungsi_hati_sgot_ast" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="sgot${s.index}" path="listAda[${s.index}].sgot_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="sgot_lic_id${s.index}" size="8" path="listAda[${s.index}].sgot_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=sgot&indx=${s.index}',550,700)" /></td>
																														<td align="center"><form:input id="sgot_lic_desc${s.index}" path="listAda[${s.index}].sgot_lic_desc" cssStyle="width:100%" /></td>
																													</tr>
																												</table>
																											</td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																							<tr>
																								<th width="102">SGPT</th>
																								<td>
																									<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																										<tr>
																											<td style="padding: 0px 0px 3px 2px">
																												<label for="flagSgptNorm"><form:radiobutton path="listAda[${s.index}].flagSgpt" value="0" id="flagSgptNorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_sgptt${s.index}',0)" />Normal</label>
																												<label for="flagSgptAbnorm"><form:radiobutton path="listAda[${s.index}].flagSgpt" value="1" id="flagSgptAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_sgptt${s.index}',1)" />Abnormal</label>
																												<label for="flagSgptNot"><form:radiobutton path="listAda[${s.index}].flagSgpt" value="2" id="flagSgptNot" cssStyle="margin-right: 3px" onclick="show_hide('adal_sgptt${s.index}',2)" />Tidak Ada</label>
																											</td>
																										</tr>
																										<tr>
																											<td>
																												<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="adal_sgptt${s.index}" style="<c:if test="${cmd.listAda[s.index].flagSgpt eq 0 or cmd.listAda[s.index].flagSgpt eq 2}">height:0;display:none;</c:if>">
																													<tr>
																														<th>Hasil</th>
																														<th>Normal</th>
																														<th>Kelainan</th>
																														<th>ICD</th>
																														<th style="width: 100%">ICD Desc</th>
																														<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('sgpt',${s.index})"></td>
																													</tr>
																													<tr>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="sgpt${s.index}" path="listAda[${s.index}].sgpt" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="nv_fungsi_hati_sgpt_alt${s.index}" size="11" path="listAda[${s.index}].nv_fungsi_hati_sgpt_alt" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="sgpt_kelainan${s.index}" path="listAda[${s.index}].sgpt_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="sgpt_lic_id${s.index}" size="8" path="listAda[${s.index}].sgpt_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=sgpt&indx=${s.index}',550,700)" /></td>
																														<td align="center"><form:input id="sgpt_lic_desc${s.index}" path="listAda[${s.index}].sgpt_lic_desc" cssStyle="width:100%" /></td>
																													</tr>
																												</table>
																											</td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																							<tr>
																								<th width="102">GGT</th>
																								<td>
																									<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																										<tr>
																											<td style="padding: 0px 0px 3px 2px">
																												<label for="flagGgtNorm"><form:radiobutton path="listAda[${s.index}].flagGgt" value="0" id="flagGgtNorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_ggt${s.index}',0)" />Normal</label>
																												<label for="flagGgtAbnorm"><form:radiobutton path="listAda[${s.index}].flagGgt" value="1" id="flagGgtAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_ggt${s.index}',1)" />Abnormal</label>
																												<label for="flagGgtNot"><form:radiobutton path="listAda[${s.index}].flagGgt" value="2" id="flagGgtNot" cssStyle="margin-right: 3px" onclick="show_hide('adal_ggt${s.index}',2)" />Tidak Ada</label>
																											</td>
																										</tr>
																										<tr>
																											<td>
																												<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="adal_ggt${s.index}" style="<c:if test="${cmd.listAda[s.index].flagGgt eq 0 or cmd.listAda[s.index].flagGgt eq 2}">height:0;display:none;</c:if>">
																													<tr>
																														<th>Hasil</th>
																														<th>Normal</th>
																														<th>Kelainan</th>
																														<th>ICD</th>
																														<th style="width: 100%">ICD Desc</th>
																														<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('ggt',${s.index})"></td>
																													</tr>
																													<tr>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ggt${s.index}" path="listAda[${s.index}].ggt" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="nv_fungsi_hati_gamma_gt_ggtp${s.index}" size="12" path="listAda[${s.index}].nv_fungsi_hati_gamma_gt_ggtp" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="ggt_kelainan${s.index}" path="listAda[${s.index}].ggt_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="ggt_lic_id${s.index}" size="8" path="listAda[${s.index}].ggt_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=ggt&indx=${s.index}',550,700)" /></td>
																														<td align="center"><form:input id="ggt_lic_desc${s.index}" path="listAda[${s.index}].ggt_lic_desc" cssStyle="width:100%" /></td>
																													</tr>
																												</table>
																											</td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																							<tr>
																								<th width="102">Fosfatase Alkali</th>
																								<td>
																									<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																										<tr>
																											<td style="padding: 0px 0px 3px 2px">
																												<label for="flagFosfatase_alkaliNorm"><form:radiobutton path="listAda[${s.index}].flagFosfatase_alkali" value="0" id="flagFosfatase_alkaliNorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_fosfatase_alkali${s.index}',0)" />Normal</label>
																												<label for="flagFosfatase_alkaliAbnorm"><form:radiobutton path="listAda[${s.index}].flagFosfatase_alkali" value="1" id="flagFosfatase_alkaliAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_fosfatase_alkali${s.index}',1)" />Abnormal</label>
																												<label for="flagFosfatase_alkaliNot"><form:radiobutton path="listAda[${s.index}].flagFosfatase_alkali" value="2" id="flagFosfatase_alkaliNot" cssStyle="margin-right: 3px" onclick="show_hide('adal_fosfatase_alkali${s.index}',2)" />Tidak Ada</label>
																											</td>
																										</tr>
																										<tr>
																											<td>
																												<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="adal_fosfatase_alkali${s.index}" style="<c:if test="${cmd.listAda[s.index].flagFosfatase_alkali eq 0 or cmd.listAda[s.index].flagFosfatase_alkali eq 2}">height:0;display:none;</c:if>">
																													<tr>
																														<th>Hasil</th>
																														<th>Normal</th>
																														<th>Kelainan</th>
																														<th>ICD</th>
																														<th style="width: 100%">ICD Desc</th>
																														<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('fos_alk',${s.index})"></td>
																													</tr>
																													<tr>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="fosfatase_alkali${s.index}" path="listAda[${s.index}].fosfatase_alkali" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="nv_fungsi_hati_alkali_fosfatase${s.index}" size="13" path="listAda[${s.index}].nv_fungsi_hati_alkali_fosfatase" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="fosfatase_alkali_kelainan${s.index}" path="listAda[${s.index}].fosfatase_alkali_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="fosfatase_alkali_lic_id${s.index}" size="8" path="listAda[${s.index}].fosfatase_alkali_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=fosfatase_alkali&indx=${s.index}',550,700)" /></td>
																														<td align="center"><form:input id="fosfatase_alkali_lic_desc${s.index}" path="listAda[${s.index}].fosfatase_alkali_lic_desc" cssStyle="width:100%" /></td>
																													</tr>
																												</table>
																											</td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																							<tr>
																								<th width="102">Bilirubin Direk</th>
																								<td>
																									<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																										<tr>
																											<td style="padding: 0px 0px 3px 2px">
																												<label for="flagBilirubin_direkNorm"><form:radiobutton path="listAda[${s.index}].flagBilirubin_direk" value="0" id="flagBilirubin_direkNorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_bilirubin_direk${s.index}',0)" />Normal</label>
																												<label for="flagBilirubin_direkAbnorm"><form:radiobutton path="listAda[${s.index}].flagBilirubin_direk" value="1" id="flagBilirubin_direkAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_bilirubin_direk${s.index}',1)" />Abnormal</label>
																												<label for="flagBilirubin_direkNot"><form:radiobutton path="listAda[${s.index}].flagBilirubin_direk" value="2" id="flagBilirubin_direkNot" cssStyle="margin-right: 3px" onclick="show_hide('adal_bilirubin_direk${s.index}',2)" />Tidak Ada</label>
																											</td>
																										</tr>
																										<tr>
																											<td>
																												<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="adal_bilirubin_direk${s.index}" style="<c:if test="${cmd.listAda[s.index].flagBilirubin_direk eq 0 or cmd.listAda[s.index].flagBilirubin_direk eq 2}">height:0;display:none;</c:if>">
																													<tr>
																														<th>Hasil</th>
																														<th>Normal</th>
																														<th>Kelainan</th>
																														<th>ICD</th>
																														<th style="width: 100%">ICD Desc</th>
																														<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('bil_dir',${s.index})"></td>
																													</tr>
																													<tr>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="bilirubin_direk${s.index}" path="listAda[${s.index}].bilirubin_direk" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="nv_fungsi_hati_bilirubin_direk${s.index}" size="12" path="listAda[${s.index}].nv_fungsi_hati_bilirubin_direk" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="bilirubin_direk_kelainan${s.index}" path="listAda[${s.index}].bilirubin_direk_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="bilirubin_direk_lic_id${s.index}" size="8" path="listAda[${s.index}].bilirubin_direk_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=bilirubin_direk&indx=${s.index}',550,700)" /></td>
																														<td align="center"><form:input id="bilirubin_direk_lic_desc${s.index}" path="listAda[${s.index}].bilirubin_direk_lic_desc" cssStyle="width:100%" /></td>
																													</tr>
																												</table>
																											</td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																							<tr>
																								<th width="102">Bilirubin Indirek</th>
																								<td>
																									<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																										<tr>
																											<td style="padding: 0px 0px 3px 2px">
																												<label for="flagBilirubin_indirekNorm"><form:radiobutton path="listAda[${s.index}].flagBilirubin_indirek" value="0" id="flagBilirubin_indirekNorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_bilirubin_indirek${s.index}',0)" />Normal</label>
																												<label for="flagBilirubin_indirekAbnorm"><form:radiobutton path="listAda[${s.index}].flagBilirubin_indirek" value="1" id="flagBilirubin_indirekAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_bilirubin_indirek${s.index}',1)" />Abnormal</label>
																												<label for="flagBilirubin_indirekNot"><form:radiobutton path="listAda[${s.index}].flagBilirubin_indirek" value="2" id="flagBilirubin_indirekNot" cssStyle="margin-right: 3px" onclick="show_hide('adal_bilirubin_indirek${s.index}',2)" />Tidak Ada</label>
																											</td>
																										</tr>
																										<tr>
																											<td>
																												<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="adal_bilirubin_indirek${s.index}" style="<c:if test="${cmd.listAda[s.index].flagBilirubin_indirek eq 0 or cmd.listAda[s.index].flagBilirubin_indirek eq 2}">height:0;display:none;</c:if>">
																													<tr>
																														<th>Hasil</th>
																														<th>Normal</th>
																														<th>Kelainan</th>
																														<th>ICD</th>
																														<th style="width: 100%">ICD Desc</th>
																														<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('bil_ind',${s.index})"></td>
																													</tr>
																													<tr>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="bilirubin_indirek${s.index}" path="listAda[${s.index}].bilirubin_indirek" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="nv_fungsi_hati_bilirubin_indirek${s.index}" size="12" path="listAda[${s.index}].nv_fungsi_hati_bilirubin_indirek" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="bilirubin_indirek_kelainan${s.index}" path="listAda[${s.index}].bilirubin_indirek_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="bilirubin_indirek_lic_id${s.index}" size="8" path="listAda[${s.index}].bilirubin_indirek_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=bilirubin_indirek&indx=${s.index}',550,700)" /></td>
																														<td align="center"><form:input id="bilirubin_indirek_lic_desc${s.index}" path="listAda[${s.index}].bilirubin_indirek_lic_desc" cssStyle="width:100%" /></td>
																													</tr>
																												</table>
																											</td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																							<tr>
																								<th width="102">Bilirubin Total</th>
																								<td>
																									<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																										<tr>
																											<td style="padding: 0px 0px 3px 2px">
																												<label for="flagBilirubin_totalNorm"><form:radiobutton path="listAda[${s.index}].flagBilirubin_total" value="0" id="flagBilirubin_totalNorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_bilirubin_total${s.index}',0)" />Normal</label>
																												<label for="flagBilirubin_totalAbnorm"><form:radiobutton path="listAda[${s.index}].flagBilirubin_total" value="1" id="flagBilirubin_totalAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_bilirubin_total${s.index}',1)" />Abnormal</label>
																												<label for="flagBilirubin_totalNot"><form:radiobutton path="listAda[${s.index}].flagBilirubin_total" value="2" id="flagBilirubin_totalNot" cssStyle="margin-right: 3px" onclick="show_hide('adal_bilirubin_total${s.index}',2)" />Tidak Ada</label>
																											</td>
																										</tr>
																										<tr>
																											<td>
																												<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="adal_bilirubin_total${s.index}" style="<c:if test="${cmd.listAda[s.index].flagBilirubin_total eq 0 or cmd.listAda[s.index].flagBilirubin_total eq 2}">height:0;display:none;</c:if>">
																													<tr>
																														<th>Hasil</th>
																														<th>Normal</th>
																														<th>Kelainan</th>
																														<th>ICD</th>
																														<th style="width: 100%">ICD Desc</th>
																														<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('bil_tot',${s.index})"></td>
																													</tr>
																													<tr>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="bilirubin_total${s.index}" path="listAda[${s.index}].bilirubin_total" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="nv_fungsi_hati_bilirubin_total${s.index}" size="12" path="listAda[${s.index}].nv_fungsi_hati_bilirubin_total" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="bilirubin_total_kelainan${s.index}" path="listAda[${s.index}].bilirubin_total_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="bilirubin_total_lic_id${s.index}" size="8" path="listAda[${s.index}].bilirubin_total_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=bilirubin_total&indx=${s.index}',550,700)" /></td>
																														<td align="center"><form:input id="bilirubin_total_lic_desc${s.index}" path="listAda[${s.index}].bilirubin_total_lic_desc" cssStyle="width:100%" /></td>
																													</tr>
																												</table>
																											</td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																							<tr>
																								<th width="102">Albumin</th>
																								<td>
																									<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																										<tr>
																											<td style="padding: 0px 0px 3px 2px">
																												<label for="flagAlbuminNorm"><form:radiobutton path="listAda[${s.index}].flagAlbumin" value="0" id="flagAlbuminNorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_albumin${s.index}',0)" />Normal</label>
																												<label for="flagAlbuminAbnorm"><form:radiobutton path="listAda[${s.index}].flagAlbumin" value="1" id="flagAlbuminAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_albumin${s.index}',1)" />Abnormal</label>
																												<label for="flagAlbuminNot"><form:radiobutton path="listAda[${s.index}].flagAlbumin" value="2" id="flagAlbuminNot" cssStyle="margin-right: 3px" onclick="show_hide('adal_albumin${s.index}',2)" />Tidak Ada</label>
																											</td>
																										</tr>
																										<tr>
																											<td>
																												<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="adal_albumin${s.index}" style="<c:if test="${cmd.listAda[s.index].flagAlbumin eq 0 or cmd.listAda[s.index].flagAlbumin eq 2}">height:0;display:none;</c:if>">
																													<tr>
																														<th>Hasil</th>
																														<th>Normal</th>
																														<th>Kelainan</th>
																														<th>ICD</th>
																														<th style="width: 100%">ICD Desc</th>
																														<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('albumin',${s.index})"></td>
																													</tr>
																													<tr>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="albumin${s.index}" path="listAda[${s.index}].albumin" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="nv_fungsi_hati_albumin${s.index}" size="13" path="listAda[${s.index}].nv_fungsi_hati_albumin" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="albumin_kelainan${s.index}" path="listAda[${s.index}].albumin_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="albumin_lic_id${s.index}" size="8" path="listAda[${s.index}].albumin_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=albumin&indx=${s.index}',550,700)" /></td>
																														<td align="center"><form:input id="albumin_lic_desc${s.index}" path="listAda[${s.index}].albumin_lic_desc" cssStyle="width:100%" /></td>
																													</tr>
																												</table>
																											</td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																							<tr>
																								<th width="102">Globulin</th>
																								<td>
																									<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																										<tr>
																											<td style="padding: 0px 0px 3px 2px">
																												<label for="flagGlobulinNorm"><form:radiobutton path="listAda[${s.index}].flagGlobulin" value="0" id="flagGlobulinNorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_globulin${s.index}',0)" />Normal</label>
																												<label for="flagGlobulinAbnorm"><form:radiobutton path="listAda[${s.index}].flagGlobulin" value="1" id="flagGlobulinAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_globulin${s.index}',1)" />Abnormal</label>
																												<label for="flagGlobulinNot"><form:radiobutton path="listAda[${s.index}].flagGlobulin" value="2" id="flagGlobulinNot" cssStyle="margin-right: 3px" onclick="show_hide('adal_globulin${s.index}',2)" />Tidak Ada</label>
																											</td>
																										</tr>
																										<tr>
																											<td>
																												<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="adal_globulin${s.index}" style="<c:if test="${cmd.listAda[s.index].flagGlobulin eq 0 or cmd.listAda[s.index].flagGlobulin eq 2}">height:0;display:none;</c:if>">
																													<tr>
																														<th>Hasil</th>
																														<th>Normal</th>
																														<th>Kelainan</th>
																														<th>ICD</th>
																														<th style="width: 100%">ICD Desc</th>
																														<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('globulin',${s.index})"></td>
																													</tr>
																													<tr>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="globulin${s.index}" path="listAda[${s.index}].globulin" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="nv_fungsi_hati_globulin${s.index}" size="13" path="listAda[${s.index}].nv_fungsi_hati_globulin" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="globulin_kelainan${s.index}" path="listAda[${s.index}].globulin_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="globulin_lic_id${s.index}" size="8" path="listAda[${s.index}].globulin_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=globulin&indx=${s.index}',550,700)" /></td>
																														<td align="center"><form:input id="globulin_lic_desc${s.index}" path="listAda[${s.index}].globulin_lic_desc" cssStyle="width:100%" /></td>
																													</tr>
																												</table>
																											</td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																							<tr>
																								<th width="102">Total Protein</th>
																								<td>
																									<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																										<tr>
																											<td style="padding: 0px 0px 3px 2px">
																												<label for="flagTotal_proteinNorm"><form:radiobutton path="listAda[${s.index}].flagTotal_protein" value="0" id="flagTotal_proteinNorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_total_protein${s.index}',0)" />Normal</label>
																												<label for="flagTotal_proteinAbnorm"><form:radiobutton path="listAda[${s.index}].flagTotal_protein" value="1" id="flagTotal_proteinAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_total_protein${s.index}',1)" />Abnormal</label>
																												<label for="flagTotal_proteinNot"><form:radiobutton path="listAda[${s.index}].flagTotal_protein" value="2" id="flagTotal_proteinNot" cssStyle="margin-right: 3px" onclick="show_hide('adal_total_protein${s.index}',2)" />Tidak Ada</label>
																											</td>
																										</tr>
																										<tr>
																											<td>
																												<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="adal_total_protein${s.index}" style="<c:if test="${cmd.listAda[s.index].flagTotal_protein eq 0 or cmd.listAda[s.index].flagTotal_protein eq 2}">height:0;display:none;</c:if>">
																													<tr>
																														<th>Hasil</th>
																														<th>Normal</th>
																														<th>Kelainan</th>
																														<th>ICD</th>
																														<th style="width: 100%">ICD Desc</th>
																														<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('tot_pro',${s.index})"></td>
																													</tr>
																													<tr>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="total_protein${s.index}" path="listAda[${s.index}].total_protein" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="nv_fungsi_hati_total_protein${s.index}" size="13" path="listAda[${s.index}].nv_fungsi_hati_total_protein" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="total_protein_kelainan${s.index}" path="listAda[${s.index}].total_protein_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="total_protein_lic_id${s.index}" size="8" path="listAda[${s.index}].total_protein_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=total_protein&indx=${s.index}',550,700)" /></td>
																														<td align="center"><form:input id="total_protein_lic_desc${s.index}" path="listAda[${s.index}].total_protein_lic_desc" cssStyle="width:100%" /></td>
																													</tr>
																												</table>
																											</td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																				<tr>	
																					<th width="102">Imunulogi - Hepatitis</th>
																					<td>
																						<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">																				
																							<tr>
																								<th width="102">HBs Ag</th>
																								<td>
																									<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																										<tr>
																											<td style="padding: 0px 0px 3px 2px">
																												<label for="flagHbs_agNorm"><form:radiobutton path="listAda[${s.index}].flagHbs_ag" value="0" id="flagHbs_agNorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_hbs_ag${s.index}',0)" />Normal</label>
																												<label for="flagHbs_agAbnorm"><form:radiobutton path="listAda[${s.index}].flagHbs_ag" value="1" id="flagHbs_agAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_hbs_ag${s.index}',1)" />Abnormal</label>
																												<label for="flagHbs_agNot"><form:radiobutton path="listAda[${s.index}].flagHbs_ag" value="2" id="flagHbs_agNot" cssStyle="margin-right: 3px" onclick="show_hide('adal_hbs_ag${s.index}',2)" />Tidak Ada</label>
																											</td>
																										</tr>
																										<tr>
																											<td>
																												<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="adal_hbs_ag${s.index}" style="<c:if test="${cmd.listAda[s.index].flagHbs_ag eq 0 or cmd.listAda[s.index].flagHbs_ag eq 2}">height:0;display:none;</c:if>">
																													<tr>
																														<th>Hasil</th>
																														<th>Normal</th>
																														<th>Kelainan</th>
																														<th>ICD</th>
																														<th style="width: 100%">ICD Desc</th>
																														<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('hbs_ag',${s.index})"></td>
																													</tr>
																													<tr>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="hbs_ag${s.index}" path="listAda[${s.index}].hbs_ag" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="nv_hepatitis_hbsag${s.index}" size="20" path="listAda[${s.index}].nv_hepatitis_hbsag" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="hbs_ag_kelainan${s.index}" path="listAda[${s.index}].hbs_ag_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="hbs_ag_lic_id${s.index}" size="8" path="listAda[${s.index}].hbs_ag_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=hbs_ag&indx=${s.index}',550,700)" /></td>
																														<td align="center"><form:input id="hbs_ag_lic_desc${s.index}" path="listAda[${s.index}].hbs_ag_lic_desc" cssStyle="width:100%" /></td>
																													</tr>
																												</table>
																											</td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																							<tr>
																								<th width="102">HBe Ag</th>
																								<td>
																									<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																										<tr>
																											<td style="padding: 0px 0px 3px 2px">
																												<label for="flagHbe_agNorm"><form:radiobutton path="listAda[${s.index}].flagHbe_ag" value="0" id="flagHbe_agNorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_hbe_ag${s.index}',0)" />Normal</label>
																												<label for="flagHbe_agAbnorm"><form:radiobutton path="listAda[${s.index}].flagHbe_ag" value="1" id="flagHbe_agAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_hbe_ag${s.index}',1)" />Abnormal</label>
																												<label for="flagHbe_agNot"><form:radiobutton path="listAda[${s.index}].flagHbe_ag" value="2" id="flagHbe_agNot" cssStyle="margin-right: 3px" onclick="show_hide('adal_hbe_ag${s.index}',2)" />Tidak Ada</label>
																											</td>
																										</tr>
																										<tr>
																											<td>
																												<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="adal_hbe_ag${s.index}" style="<c:if test="${cmd.listAda[s.index].flagHbe_ag eq 0 or cmd.listAda[s.index].flagHbe_ag eq 2}">height:0;display:none;</c:if>">
																													<tr>
																														<th>Hasil</th>
																														<th>Normal</th>
																														<th>Kelainan</th>
																														<th>ICD</th>
																														<th style="width: 100%">ICD Desc</th>
																														<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('hbe_ag',${s.index})"></td>
																													</tr>
																													<tr>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="hbe_ag${s.index}" path="listAda[${s.index}].hbe_ag" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="nv_hepatitis_hbeag${s.index}" size="20" path="listAda[${s.index}].nv_hepatitis_hbeag" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="hbe_ag_kelainan${s.index}" path="listAda[${s.index}].hbe_ag_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="hbe_ag_lic_id${s.index}" size="8" path="listAda[${s.index}].hbe_ag_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=hbe_ag&indx=${s.index}',550,700)" /></td>
																														<td align="center"><form:input id="hbe_ag_lic_desc${s.index}" path="listAda[${s.index}].hbe_ag_lic_desc" cssStyle="width:100%" /></td>
																													</tr>
																												</table>
																											</td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																				<tr>	
																					<th width="102">Fungsi Ginjal</th>
																					<td>
																						<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">																				
																							<tr>
																								<th width="102">Creatinin</th>
																								<td>
																									<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																										<tr>
																											<td style="padding: 0px 0px 3px 2px">
																												<label for="flagCreatininNorm"><form:radiobutton path="listAda[${s.index}].flagCreatinin" value="0" id="flagCreatininNorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_creatinin${s.index}',0)" />Normal</label>
																												<label for="flagCreatininAbnorm"><form:radiobutton path="listAda[${s.index}].flagCreatinin" value="1" id="flagCreatininAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_creatinin${s.index}',1)" />Abnormal</label>
																												<label for="flagCreatininNot"><form:radiobutton path="listAda[${s.index}].flagCreatinin" value="2" id="flagCreatininNot" cssStyle="margin-right: 3px" onclick="show_hide('adal_creatinin${s.index}',2)" />Tidak Ada</label>
																											</td>
																										</tr>
																										<tr>
																											<td>
																												<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="adal_creatinin${s.index}" style="<c:if test="${cmd.listAda[s.index].flagCreatinin eq 0 or cmd.listAda[s.index].flagCreatinin eq 2}">height:0;display:none;</c:if>">
																													<tr>
																														<th>Hasil</th>
																														<th>Normal</th>
																														<th>Kelainan</th>
																														<th>ICD</th>
																														<th style="width: 100%">ICD Desc</th>
																														<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('creatinin',${s.index})"></td>
																													</tr>
																													<tr>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="creatinin${s.index}" path="listAda[${s.index}].creatinin" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="nv_fungsi_ginjal_creatinin${s.index}" size="14" path="listAda[${s.index}].nv_fungsi_ginjal_creatinin" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="creatinin_kelainan${s.index}" path="listAda[${s.index}].creatinin_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="creatinin_lic_id${s.index}" size="8" path="listAda[${s.index}].creatinin_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=creatinin&indx=${s.index}',550,700)" /></td>
																														<td align="center"><form:input id="creatinin_lic_desc${s.index}" path="listAda[${s.index}].creatinin_lic_desc" cssStyle="width:100%" /></td>
																													</tr>
																												</table>
																											</td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																							<tr>
																								<th width="102">Ureum</th>
																								<td>
																									<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																										<tr>
																											<td style="padding: 0px 0px 3px 2px">
																												<label for="flagUreumNorm"><form:radiobutton path="listAda[${s.index}].flagUreum" value="0" id="flagUreumNorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_ureum${s.index}',0)" />Normal</label>
																												<label for="flagUreumAbnorm"><form:radiobutton path="listAda[${s.index}].flagUreum" value="1" id="flagUreumAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_ureum${s.index}',1)" />Abnormal</label>
																												<label for="flagUreumNot"><form:radiobutton path="listAda[${s.index}].flagUreum" value="2" id="flagUreumNot" cssStyle="margin-right: 3px" onclick="show_hide('adal_ureum${s.index}',2)" />Tidak Ada</label>
																											</td>
																										</tr>
																										<tr>
																											<td>
																												<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="adal_ureum${s.index}" style="<c:if test="${cmd.listAda[s.index].flagUreum eq 0 or cmd.listAda[s.index].flagUreum eq 2}">height:0;display:none;</c:if>">
																													<tr>
																														<th>Hasil</th>
																														<th>Normal</th>
																														<th>Kelainan</th>
																														<th>ICD</th>
																														<th style="width: 100%">ICD Desc</th>
																														<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('ureum',${s.index})"></td>
																													</tr>
																													<tr>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="ureum${s.index}" path="listAda[${s.index}].ureum" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="nv_fungsi_ginjal_ureum${s.index}" size="17" path="listAda[${s.index}].nv_fungsi_ginjal_ureum" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="ureum_kelainan${s.index}" path="listAda[${s.index}].ureum_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="ureum_lic_id${s.index}" size="8" path="listAda[${s.index}].ureum_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=ureum&indx=${s.index}',550,700)" /></td>
																														<td align="center"><form:input id="ureum_lic_desc${s.index}" path="listAda[${s.index}].ureum_lic_desc" cssStyle="width:100%" /></td>
																													</tr>
																												</table>
																											</td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																							<tr>
																								<th width="102">Asam Urat</th>
																								<td>
																									<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																										<tr>
																											<td style="padding: 0px 0px 3px 2px">
																												<label for="flagAsam_uratNorm"><form:radiobutton path="listAda[${s.index}].flagAsam_urat" value="0" id="flagAsam_uratNorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_asam_urat${s.index}',0)" />Normal</label>
																												<label for="flagAsam_uratAbnorm"><form:radiobutton path="listAda[${s.index}].flagAsam_urat" value="1" id="flagAsam_uratAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('adal_asam_urat${s.index}',1)" />Abnormal</label>
																												<label for="flagAsam_uratNot"><form:radiobutton path="listAda[${s.index}].flagAsam_urat" value="2" id="flagAsam_uratNot" cssStyle="margin-right: 3px" onclick="show_hide('adal_asam_urat${s.index}',2)" />Tidak Ada</label>
																											</td>
																										</tr>
																										<tr>
																											<td>
																												<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="adal_asam_urat${s.index}" style="<c:if test="${cmd.listAda[s.index].flagAsam_urat eq 0 or cmd.listAda[s.index].flagAsam_urat eq 2}">height:0;display:none;</c:if>">
																													<tr>
																														<th>Hasil</th>
																														<th>Normal</th>
																														<th>Kelainan</th>
																														<th>ICD</th>
																														<th style="width: 100%">ICD Desc</th>
																														<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('asm_ura',${s.index})"></td>
																													</tr>
																													<tr>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="asam_urat${s.index}" path="listAda[${s.index}].asam_urat" size="7" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center" style="text-transform: none;padding-right: 2px" nowrap="nowrap"><form:input id="nv_fungsi_ginjal_asam_urat${s.index}" size="14" path="listAda[${s.index}].nv_fungsi_ginjal_asam_urat" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="asam_urat_kelainan${s.index}" path="listAda[${s.index}].asam_urat_kelainan" size="30" onkeypress="return maxChar(event,this.value,30)" /></td>
																														<td align="center"><form:input id="asam_urat_lic_id${s.index}" size="8" path="listAda[${s.index}].asam_urat_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=asam_urat&indx=${s.index}',550,700)" /></td>
																														<td align="center"><form:input id="asam_urat_lic_desc${s.index}" path="listAda[${s.index}].asam_urat_lic_desc" cssStyle="width:100%" /></td>
																													</tr>
																												</table>
																											</td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr>																				
																			</c:if>	
																		</table>
																	</td>
																</tr>			
																<tr>
																	<td style="padding-top: 3px">
																		<c:choose>
																			<c:when test="${s.index eq fn:length(cmd.listAda)-1}">
																				<input type="button" name="addAda" value="Add ADA<c:if test="${cmd.listAda[s.index].flagAda eq 1}">L</c:if>" onclick="return tampilkan(this);">
																			</c:when>
																			<c:otherwise>
																				&nbsp;
																			</c:otherwise>	
																		</c:choose>
																	</td>
																</tr>
															</c:forEach>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</c:if>
					<c:if test="${fn:length(cmd.listHiv) ne 0 }">	
						<tr>
							<td>
								<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
									<tr>
										<th width="110" style="color: blue;">Anti HIV</th>
										<td style="padding-left: 3px">
											<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">		
												<c:forEach items="${cmd.listHiv}" var="b" varStatus="s">
													<tr>
														<td>
															<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																<tr>
																	<th width="102">Tanggal MCU</th>
																	<td style="padding-left: 3px" width="104">
																		<spring:bind  path="cmd.listHiv[${s.index}].tglMcuHiv">
																			<script>inputDate('${status.expression}', '${status.value}', false);</script> <span style="color: #FF0000">*</span>
																		</spring:bind>									
																	</td>
																	<th width="102">Tempat MCU</th>
																	<td style="padding-left: 3px">									
																		<form:input id="hiv_tmpMcu${s.index}" path="listHiv[${s.index}].tmpMcuHiv" cssStyle="width: 97%"/> <span style="color: #FF0000">*</span>
																	</td>
																	<td style="padding-left: 3px" width="110">
																		<input type="button" name="lsProv" value="List Provider" onclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_provider&sub=hiv&indx=${s.index}',480,640)">
																	</td>																	
																</tr>
															</table>												
														</td>
													</tr>
													<tr>
														<td>
															<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																<tr>
																	<th width="102">Anti HIV</th>
																	<td style="padding: 0px 3px 0px 3px" width="50"><form:input id="hiv_antiHiv${s.index}" path="listHiv[${s.index}].antiHiv" size="15" onkeypress="return maxChar(event,this.value,30)" /></td>
																	<th width="102">Normal</th>
																	<td style="text-transform: none;padding-left: 3px" nowrap="nowrap"><form:input id="nv_anti_hiv_anti_hiv${s.index}" size="14" path="listHiv[${s.index}].nv_anti_hiv_anti_hiv" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,30)" /></td>
																</tr>
															</table>							
														</td>
													</tr>
													<tr>
														<td style="padding-top: 3px">
															<c:choose>
																<c:when test="${s.index eq fn:length(cmd.listHiv)-1}">
																	<input type="button" name="addHiv" value="Add HIV" onclick="return tampilkan(this);">
																</c:when>
																<c:otherwise>
																	&nbsp;
																</c:otherwise>	
															</c:choose>
														</td>
													</tr>
												</c:forEach>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</c:if>
					<c:if test="${fn:length(cmd.listTumor) ne 0 }">	
						<tr>
							<td>
								<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
									<tr>
										<th width="110" style="color: blue;">Tumor Maker</th>
										<td style="padding-left: 3px">			
											<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">		
												<c:forEach items="${cmd.listTumor}" var="b" varStatus="s">
													<tr>
														<td>
															<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																<tr>
																	<th width="102">Tanggal MCU</th>
																	<td style="padding-left: 3px" width="104">
																		<spring:bind  path="cmd.listTumor[${s.index}].tglMcuTumor">
																			<script>inputDate('${status.expression}', '${status.value}', false);</script> <span style="color: #FF0000">*</span>
																		</spring:bind>									
																	</td>
																	<th width="102">Tempat MCU</th>
																	<td style="padding-left: 3px">									
																		<form:input id="tumor_tmpMcu${s.index}" path="listTumor[${s.index}].tmpMcuTumor" cssStyle="width: 97%"/> <span style="color: #FF0000">*</span>
																	</td>
																	<td style="padding-left: 3px" width="110">
																		<input type="button" name="lsProv" value="List Provider" onclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_provider&sub=tumor&indx=${s.index}',480,640)">
																	</td>																	
																</tr>
															</table>													
														</td>
													</tr>
													<tr>
														<td>
															<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																<tr>
																	<th width="102">CEA</th>
																	<td style="padding: 0px 3px 0px 3px;text-transform: none;" width="120" nowrap="nowrap"><form:input id="cea${s.index}" path="listTumor[${s.index}].cea" size="15" onkeypress="return maxChar(event,this.value,10)" /></td>
																	<th width="102">Normal</th>
																	<td style="text-transform: none;padding-left: 3px" nowrap="nowrap"><form:input id="nv_tumor_marker_cea${s.index}" size="13" path="listTumor[${s.index}].nv_tumor_marker_cea" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,20)" /></td>
																</tr>
															</table>							
														</td>
													</tr>
													<tr>
														<td>
															<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																<tr>
																	<th width="102">AFP</th>
																	<td style="padding: 0px 3px 0px 3px;text-transform: none;" width="120" nowrap="nowrap"><form:input id="afp${s.index}" path="listTumor[${s.index}].afp" size="15" onkeypress="return maxChar(event,this.value,10)" /></td>
																	<th width="102">Normal</th>
																	<td style="text-transform: none;padding-left: 3px" nowrap="nowrap"><form:input id="nv_tumor_marker_afp${s.index}" size="13" path="listTumor[${s.index}].nv_tumor_marker_afp" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,20)" /></td>
																</tr>
															</table>							
														</td>
													</tr>
													<tr>
														<td>
															<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																<tr>
																	<th width="102">PSA (pria)</th>
																	<td style="padding: 0px 3px 0px 3px;text-transform: none;" width="120" nowrap="nowrap"><form:input id="psa${s.index}" path="listTumor[${s.index}].psa" size="15" onkeypress="return maxChar(event,this.value,10)" /></td>
																	<th width="102">Normal</th>
																	<td style="text-transform: none;padding-left: 3px" nowrap="nowrap"><form:input id="nv_tumor_marker_psa${s.index}" size="13" path="listTumor[${s.index}].nv_tumor_marker_psa" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,20)" /></td>
																</tr>
															</table>							
														</td>
													</tr>
													<tr>
														<td>
															<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																<tr>
																	<th width="102">CA 15-3 (wanita)</th>
																	<td style="padding: 0px 3px 0px 3px;text-transform: none;" width="120" nowrap="nowrap"><form:input id="ca_15_3${s.index}" path="listTumor[${s.index}].ca_15_3" size="15" onkeypress="return maxChar(event,this.value,10)" /></td>
																	<th width="102">Normal</th>
																	<td style="text-transform: none;padding-left: 3px" nowrap="nowrap"><form:input id="nv_tumor_marker_ca_15_3${s.index}" size="13" path="listTumor[${s.index}].nv_tumor_marker_ca_15_3" cssStyle="background-color: #6FC1F3;text-transform: none;" onkeypress="return maxChar(event,this.value,20)" /></td>
																</tr>
															</table>							
														</td>
													</tr>
													<tr>
														<td style="padding-top: 3px">
															<c:choose>
																<c:when test="${s.index eq fn:length(cmd.listTumor)-1}">
																	<input type="button" name="addTumor" value="Add Tumor" onclick="return tampilkan(this);">
																</c:when>
																<c:otherwise>
																	&nbsp;
																</c:otherwise>	
															</c:choose>
														</td>
													</tr>
												</c:forEach>
											</table>				
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</c:if>
					<c:if test="${fn:length(cmd.listAbdomen) ne 0 }">	
						<tr>
							<td>
								<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
									<tr>
										<th width="110" style="color: blue;">USG Abdomen</th>
										<td style="padding-left: 3px">			
											<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">		
												<c:forEach items="${cmd.listAbdomen}" var="b" varStatus="s">
													<tr>
														<td>
															<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																<tr>
																	<th width="102">Tanggal MCU</th>
																	<td style="padding-left: 3px" width="104">
																		<spring:bind  path="cmd.listAbdomen[${s.index}].tglMcuAbdomen">
																			<script>inputDate('${status.expression}', '${status.value}', false);</script> <span style="color: #FF0000">*</span>
																		</spring:bind>									
																	</td>
																	<th width="102">Tempat MCU</th>
																	<td style="padding-left: 3px">									
																		<form:input id="abdomen_tmpMcu${s.index}" path="listAbdomen[${s.index}].tmpMcuAbdomen" cssStyle="width: 97%"/> <span style="color: #FF0000">*</span>
																	</td>
																	<td style="padding-left: 3px" width="110">
																		<input type="button" name="lsProv" value="List Provider" onclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_provider&sub=abdomen&indx=${s.index}',480,640)">
																	</td>																	
																</tr>
															</table>													
														</td>
													</tr>
													<tr>
														<td>
															<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																<tr>
																	<td style="padding: 0px 0px 3px 2px">
																		<label for="flagAbdomenNorm"><form:radiobutton path="listAbdomen[${s.index}].flagAbdomen" value="0" id="flagAbdomenNorm" cssStyle="margin-right: 3px" onclick="show_hide('abdomen${s.index}',0)" />Normal</label>
																		<label for="flagAbdomenAbnorm"><form:radiobutton path="listAbdomen[${s.index}].flagAbdomen" value="1" id="flagAbdomenAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('abdomen${s.index}',1)" />Abnormal</label>
																		<label for="flagAbdomenNot"><form:radiobutton path="listAbdomen[${s.index}].flagAbdomen" value="2" id="flagAbdomenNot" cssStyle="margin-right: 3px" onclick="show_hide('abdomen${s.index}',2)" />Tidak Ada</label>
																	</td>
																</tr>
																<tr>
																	<td>
																		<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="abdomen${s.index}" style="<c:if test="${cmd.listAbdomen[s.index].flagAbdomen eq 0 or cmd.listAbdomen[s.index].flagAbdomen eq 2}">height:0;display:none;</c:if>">
																			<tr>
																				<th>Kelainan</th>
																				<th>ICD</th>
																				<th style="width: 100%">ICD Desc</th>
																				<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('abdomen',${s.index})"></td>
																			</tr>
																			<tr>
																				<td align="center"><form:input id="abdomen_kelainan${s.index}" path="listAbdomen[${s.index}].abdomen_kelainan" size="100" onkeypress="return maxChar(event,this.value,30)" /></td>
																				<td align="center"><form:input id="abdomen_lic_id${s.index}" size="8" path="listAbdomen[${s.index}].abdomen_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=abdomen&indx=${s.index}',550,700)" /></td>
																				<td align="center"><form:input id="abdomen_lic_desc${s.index}" path="listAbdomen[${s.index}].abdomen_lic_desc" cssStyle="width:100%" /></td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td style="padding-top: 3px">
															<c:choose>
																<c:when test="${s.index eq fn:length(cmd.listAbdomen)-1}">
																	<input type="button" name="addAbdomen" value="Add Abdomen" onclick="return tampilkan(this);">
																</c:when>
																<c:otherwise>
																	&nbsp;
																</c:otherwise>	
															</c:choose>
														</td>
													</tr>
												</c:forEach>
											</table>	
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</c:if>
					<c:if test="${fn:length(cmd.listDadaPa) ne 0 }">	
						<tr>
							<td>
								<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
									<tr>
										<th width="110" style="color: blue;">Rotgen dada PA</th>
										<td style="padding-left: 3px">			
											<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">		
												<c:forEach items="${cmd.listDadaPa}" var="b" varStatus="s">
													<tr>
														<td>
															<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																<tr>
																	<th width="102">Tanggal MCU</th>
																	<td style="padding-left: 3px" width="104">
																		<spring:bind  path="cmd.listDadaPa[${s.index}].tglMcuDadaPA">
																			<script>inputDate('${status.expression}', '${status.value}', false);</script> <span style="color: #FF0000">*</span>
																		</spring:bind>									
																	</td>
																	<th width="102">Tempat MCU</th>
																	<td style="padding-left: 3px">									
																		<form:input id="dada_pa_tmpMcu${s.index}" path="listDadaPa[${s.index}].tmpMcuDadaPA" cssStyle="width: 97%"/> <span style="color: #FF0000">*</span>
																	</td>
																	<td style="padding-left: 3px" width="110">
																		<input type="button" name="lsProv" value="List Provider" onclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_provider&sub=dadaPa&indx=${s.index}',480,640)">
																	</td>																	
																</tr>
															</table>													
														</td>
													</tr>
													<tr>
														<td>
															<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																<tr>
																	<td style="padding: 0px 0px 3px 2px">
																		<label for="flagDadaPaNorm"><form:radiobutton path="listDadaPa[${s.index}].flagDadaPa" value="0" id="flagDadaPaNorm" cssStyle="margin-right: 3px" onclick="show_hide('dada_pa${s.index}',0)" />Normal</label>
																		<label for="flagDadaPaAbnorm"><form:radiobutton path="listDadaPa[${s.index}].flagDadaPa" value="1" id="flagDadaPaAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('dada_pa${s.index}',1)" />Abnormal</label>
																		<label for="flagDadaPaNot"><form:radiobutton path="listDadaPa[${s.index}].flagDadaPa" value="2" id="flagDadaPaNot" cssStyle="margin-right: 3px" onclick="show_hide('dada_pa${s.index}',2)" />Tidak Ada</label>
																	</td>
																</tr>
																<tr>
																	<td>
																		<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="dada_pa${s.index}" style="<c:if test="${cmd.listDadaPa[s.index].flagDadaPa eq 0 or cmd.listDadaPa[s.index].flagDadaPa eq 2}">height:0;display:none;</c:if>">
																			<tr>
																				<th>Kelainan</th>
																				<th>ICD</th>
																				<th style="width: 100%">ICD Desc</th>
																				<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('dada_pa',${s.index})"></td>
																			</tr>
																			<tr>
																				<td align="center"><form:input id="dada_pa_kelainan${s.index}" path="listDadaPa[${s.index}].dada_pa_kelainan" size="100" onkeypress="return maxChar(event,this.value,50)" /></td>
																				<td align="center"><form:input id="dada_pa_lic_id${s.index}" size="8" path="listDadaPa[${s.index}].dada_pa_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=dada_pa&indx=${s.index}',550,700)" /></td>
																				<td align="center"><form:input id="dada_pa_lic_desc${s.index}" path="listDadaPa[${s.index}].dada_pa_lic_desc" cssStyle="width:100%" /></td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td style="padding-top: 3px">
															<c:choose>
																<c:when test="${s.index eq fn:length(cmd.listDadaPa)-1}">
																	<input type="button" name="addDadaPa" value="Add PA" onclick="return tampilkan(this);">
																</c:when>
																<c:otherwise>
																	&nbsp;
																</c:otherwise>	
															</c:choose>
														</td>
													</tr>
												</c:forEach>
											</table>	
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</c:if>
					<c:if test="${fn:length(cmd.listEkg) ne 0 }">	
						<tr>
							<td>
								<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
									<tr>
										<th width="110" style="color: blue;">EKG</th>
										<td style="padding-left: 3px">			
											<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">		
												<c:forEach items="${cmd.listEkg}" var="b" varStatus="s">
													<tr>
														<td>
															<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																<tr>
																	<th width="102">Tanggal MCU</th>
																	<td style="padding-left: 3px" width="104">
																		<spring:bind  path="cmd.listEkg[${s.index}].tglMcuEkg">
																			<script>inputDate('${status.expression}', '${status.value}', false);</script> <span style="color: #FF0000">*</span>
																		</spring:bind>									
																	</td>
																	<th width="102">Tempat MCU</th>
																	<td style="padding-left: 3px">									
																		<form:input id="ekg_tmpMcu${s.index}" path="listEkg[${s.index}].tmpMcuEkg" cssStyle="width: 97%"/> <span style="color: #FF0000">*</span>
																	</td>
																	<td style="padding-left: 3px" width="110">
																		<input type="button" name="lsProv" value="List Provider" onclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_provider&sub=ekg&indx=${s.index}',480,640)">
																	</td>																	
																</tr>
															</table>													
														</td>
													</tr>
													<tr>
														<td>
															<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																<tr>
																	<td style="padding: 0px 0px 3px 2px">
																		<label for="flagEkgNorm"><form:radiobutton path="listEkg[${s.index}].flagEkg" value="0" id="flagEkgNorm" cssStyle="margin-right: 3px" onclick="show_hide('ekg${s.index}',0)" />Normal</label>
																		<label for="flagEkgAbnorm"><form:radiobutton path="listEkg[${s.index}].flagEkg" value="1" id="flagEkgAbnorm" cssStyle="margin-right: 3px" onclick="show_hide('ekg${s.index}',1)" />Abnormal</label>
																		<label for="flagEkgNot"><form:radiobutton path="listEkg[${s.index}].flagEkg" value="2" id="flagEkgNot" cssStyle="margin-right: 3px" onclick="show_hide('ekg${s.index}',2)" />Tidak Ada</label>
																	</td>
																</tr>
																<tr>
																	<td>
																		<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0" id="ekg${s.index}" style="<c:if test="${cmd.listEkg[s.index].flagEkg eq 0 or cmd.listEkg[s.index].flagEkg eq 2}">height:0;display:none;</c:if>">
																			<tr>
																				<th>Kelainan</th>
																				<th>ICD</th>
																				<th style="width: 100%">ICD Desc</th>
																				<td rowspan="2"><img src="${path }/include/image/dl.jpg" align="absmiddle" id="addToUwDesc" name="addToUwDesc" border="0" title="tambah ke uw decision" onclick="return copyToDesc('ekg',${s.index})"></td>
																			</tr>
																			<tr>
																				<td align="center"><form:input id="ekg_kelainan${s.index}" path="listEkg[${s.index}].ekg_kelainan" size="100" onkeypress="return maxChar(event,this.value,50)" /></td>
																				<td align="center"><form:input id="ekg_lic_id${s.index}" size="8" path="listEkg[${s.index}].ekg_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=ekg&indx=${s.index}',550,700)" /></td>
																				<td align="center"><form:input id="ekg_lic_desc${s.index}" path="listEkg[${s.index}].ekg_lic_desc" cssStyle="width:100%" /></td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td style="padding-top: 3px">
															<c:choose>
																<c:when test="${s.index eq fn:length(cmd.listEkg)-1}">
																	<input type="button" name="addEkg" value="Add EKG" onclick="return tampilkan(this);">
																</c:when>
																<c:otherwise>
																	&nbsp;
																</c:otherwise>	
															</c:choose>
														</td>
													</tr>
												</c:forEach>
											</table>	
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</c:if>
					<c:if test="${fn:length(cmd.listTreadmill) ne 0 }">		
						<tr>
							<td>
								<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
									<tr>
										<th width="110" style="color: blue;">Treadmill Test</th>
										<td style="padding-left: 3px">			
											<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">		
												<c:forEach items="${cmd.listTreadmill}" var="b" varStatus="s">
													<tr>
														<td>
															<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																<tr>
																	<th width="102">Tanggal MCU</th>
																	<td style="padding-left: 3px" width="104">
																		<spring:bind  path="cmd.listTreadmill[${s.index}].tglMcuTreadmill">
																			<script>inputDate('${status.expression}', '${status.value}', false);</script> <span style="color: #FF0000">*</span>
																		</spring:bind>									
																	</td>
																	<th width="102">Tempat MCU</th>
																	<td style="padding-left: 3px">									
																		<form:input id="treadmill_tmpMcu${s.index}" path="listTreadmill[${s.index}].tmpMcuTreadmill" cssStyle="width: 97%"/> <span style="color: #FF0000">*</span>
																	</td>
																	<td style="padding-left: 3px" width="110">
																		<input type="button" name="lsProv" value="List Provider" onclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_provider&sub=treadmill&indx=${s.index}',480,640)">
																	</td>																	
																</tr>
															</table>													
														</td>
													</tr>
													<tr>
														<td>
															<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																<tr>
																	<th width="102">Duration</th>
																	<td style="padding: 0px 0px 3px 2px">
																		<form:input id="treadmill_dur_men${s.index}" size="4" path="listTreadmill[${s.index}].duration_men" onkeypress="return maxChar(event,this.value,2)" /> mnt
																		<form:input id="treadmill_dur_det${s.index}" size="4" path="listTreadmill[${s.index}].duration_det" onkeypress="return maxChar(event,this.value,2)" /> dtk
																	</td>
																</tr>
																<tr>
																	<th width="102">METs</th>
																	<td style="padding-left:3px">
																		<form:input id="treadmill_mets${s.index}" path="listTreadmill[${s.index}].mets" onkeypress="return maxChar(event,this.value,10)" />
																	</td>
																</tr>
																<tr>
																	<th width="102">Reason of termination</th>
																	<td style="padding-left:3px">
																		<form:input id="treadmill_reason_of_termination${s.index}" path="listTreadmill[${s.index}].reason_of_termination" cssStyle="width: 97%" onkeypress="return maxChar(event,this.value,100)" />
																	</td>
																</tr>
																<tr>
																	<th width="102">Max. ST depresi</th>
																	<td style="padding-left:3px">
																		<form:input id="treadmill_max_st_depresi${s.index}" path="listTreadmill[${s.index}].max_st_depresi" onkeypress="return maxChar(event,this.value,15)" />
																	</td>
																</tr>
																<tr>
																	<th width="102">Resting BP</th>
																	<td style="padding-left:3px">
																		<form:input id="treadmill_resting_bp${s.index}" path="listTreadmill[${s.index}].resting_bp" onkeypress="return maxChar(event,this.value,10)" />
																	</td>
																</tr>
																<tr>
																	<th width="102">Max. BP</th>
																	<td style="padding-left:3px">
																		<form:input id="treadmill_max_bp${s.index}" path="listTreadmill[${s.index}].max_bp" onkeypress="return maxChar(event,this.value,10)" />
																	</td>
																</tr>
																<tr>
																	<th width="102">Max. Heart rate</th>
																	<td style="padding-left:3px">
																		<form:input id="treadmill_max_heart_rate${s.index}" size="4" path="listTreadmill[${s.index}].max_heart_rate" onkeypress="return maxChar(event,this.value,3)" />
																	</td>
																</tr>
																<tr>
																	<th width="102">Interpretasi</th>
																	<td style="padding-left:3px">
																		<form:input id="treadmill_interpretasi${s.index}" path="listTreadmill[${s.index}].interpretasi" cssStyle="width: 97%" onkeypress="return maxChar(event,this.value,100)" />
																	</td>
																</tr>
																<tr>
																	<th width="102">ICD</th>
																	<td style="padding-left:3px">
																		<form:input id="treadmill_lic_id${s.index}" size="8" path="listTreadmill[${s.index}].treadmill_lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=treadmill&indx=${s.index}',550,700)" />
																		<form:input id="treadmill_lic_desc${s.index}" path="listTreadmill[${s.index}].treadmill_lic_desc"  cssStyle="width: 91%" />
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td style="padding-top: 3px">
															<c:choose>
																<c:when test="${s.index eq fn:length(cmd.listTreadmill)-1}">
																	<input type="button" name="addTreadmill" value="Add Treadmill Test" onclick="return tampilkan(this);">
																</c:when>
																<c:otherwise>
																	&nbsp;
																</c:otherwise>	
															</c:choose>
														</td>
													</tr>
												</c:forEach>
											</table>	
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</c:if>
					<c:if test="${fn:length(cmd.listMedLain) ne 0 }">		
						<tr>
							<td>
								<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
									<tr>
										<th width="110" style="color: blue;">Medis Lain</th>
										<td style="padding-left: 3px">			
											<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">		
												<c:forEach items="${cmd.listMedLain}" var="b" varStatus="s">
													<tr>
														<td>
															<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
																<tr>
																	<th width="102">Tanggal MCU</th>
																	<td style="padding-left: 3px" width="104">
																		<spring:bind  path="cmd.listMedLain[${s.index}].tglMcuMedisLain">
																			<script>inputDate('${status.expression}', '${status.value}', false);</script> <span style="color: #FF0000">*</span>
																		</spring:bind>									
																	</td>
																	<th width="102">Tempat MCU</th>
																	<td style="padding-left: 3px">									
																		<form:input id="medis_lain_tmpMcu${s.index}" path="listMedLain[${s.index}].tmpMcuMedisLain" cssStyle="width: 97%"/> <span style="color: #FF0000">*</span>
																	</td>
																	<td style="padding-left: 3px" width="110">
																		<input type="button" name="lsProv" value="List Provider" onclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_provider&sub=medis_lain&indx=${s.index}',480,640)">
																	</td>																	
																</tr>
																<tr>
																	<th width="102">Keterangan</th>
																	<td style="padding-left: 3px" colspan="3"><form:textarea id="medis_lain_keterangan${s.index}" path="listMedLain[${s.index}].ketMedisLain" cols="60" rows="9" onkeypress="return maxChar(event,this.value,600)" cssStyle="width: 100%;text-transform: none;" /></td>	
																</tr>
															</table>													
														</td>
													</tr>
													<tr>
														<td style="padding-top: 3px">
															<c:choose>
																<c:when test="${s.index eq fn:length(cmd.listMedLain)-1}">
																	<input type="button" name="addMedisLain" value="Add Medis Test" onclick="return tampilkan(this);">
																</c:when>
																<c:otherwise>
																	&nbsp;
																</c:otherwise>	
															</c:choose>
														</td>
													</tr>
												</c:forEach>
											</table>	
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</c:if>		
				</c:if>	
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
							<tr>
								<th width="110" style="color: blue;">Input UW Decision</th>
								<td style="padding-left: 3px">
									<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
										<tr>
											<td>
												<table cellpadding="0" cellspacing="0" width="100%" class="displaytag" border="0">
													<tr>
														<th rowspan="3" width="30">#</th>
														<th rowspan="3" width="15%">Kelainan / Penyakit</th>
														<th colspan="2">${lsPlan[0].V}</th>
														<c:if test="${fn:length(lsPlan) gt 1}">
															<th colspan="${(fn:length(lsPlan)-1)*2}">Rider</th>	
														</c:if>	
														<th rowspan="3">ICD Code</th>
														<th rowspan="3" width="50%">Keterangan ICD Code</th>
													</tr>
													<tr>
														<th rowspan="2">%<br/>(persen)</th>
														<th rowspan="2">&permil;<br/>(permil)</th>
														<c:if test="${fn:length(lsPlan) gt 1 }">
															<c:forEach items="${lsPlan}" var="b" varStatus="s">
																<c:if test="${s.index gt 0}">
																	<th colspan="2">${b.V}</th>
																</c:if>
															</c:forEach>
														</c:if>	
													</tr>
													<tr>
														<c:if test="${fn:length(lsPlan) gt 1 }">
															<c:forEach items="${lsPlan}" var="b" varStatus="s">
																	<c:if test="${s.index gt 0}">
																		<th>%</th>
																		<th>&permil;</th>
																	</c:if>
																</c:forEach>
														</c:if>	
													</tr>
													<c:forEach items="${cmd.listUwDec}" var="b" varStatus="s">
														<c:if test="${s.index lt cmd.totalPenyakitUwDesc}">
															<tr>
																<td style="text-align: center;"><input type="checkbox" name="delPenyakit${s.index}" value="del"></td>
																<td style="text-align: center;"><form:input id="penyakit${s.index}" path="listUwDec[${s.index}].penyakit" cssStyle="width: 98%" onkeypress="return maxChar(event,this.value,60)" /></td>
																<td style="text-align: center;"><form:input id="prod_utama_persen${s.index}" path="listUwDec[${s.index}].prod_utama_persen" size="4" /></td>
																<td style="text-align: center;"><form:input id="prod_utama_permil${s.index}" path="listUwDec[${s.index}].prod_utama_permil" size="4" /></td>
																<c:if test="${fn:length(lsPlan) gt 1 }">
																	<c:forEach items="${lsPlan}" var="c" varStatus="x">
																		<c:if test="${x.index gt 0}">
																			<td style="text-align: center;"><form:input id="rider_prod_persen${s.index+1}" path="listUwDec[${s.index}].rider[${x.index-1}].rate_persen" size="4" /></td>
																			<td style="text-align: center;"><form:input id="rider_prod_permil${s.index+1}" path="listUwDec[${s.index}].rider[${x.index-1}].rate_permil" size="4" /></td>
																		</c:if>
																	</c:forEach>
																</c:if>
																<td style="text-align: center;"><form:input id="nonMedDec_lic_id${s.index}" size="8" path="listUwDec[${s.index}].lic_id" ondblclick="popWin('${path}/uw/uw.htm?window=windowWorksheet&mode=cari_icd&sub=nonMedDec&indx=${s.index}',550,700)" /></td>
																<td><form:input id="nonMedDec_lic_desc${s.index}" size="8" path="listUwDec[${s.index}].lic_desc" cssStyle="width: 99%" /></td>
															</tr>
														</c:if>	
													</c:forEach>
													<tr>
														<th style="text-align: center;" colspan="2">Total Rating</th>
														<th style="text-align: center;"><form:input id="prod_utama_persen${cmd.totalPenyakitUwDesc}" path="listUwDec[${cmd.totalPenyakitUwDesc}].prod_utama_persen" size="4" /></th>
														<th style="text-align: center;"><form:input id="prod_utama_permil${cmd.totalPenyakitUwDesc}" path="listUwDec[${cmd.totalPenyakitUwDesc}].prod_utama_permil" size="4" /></th>
														<c:if test="${fn:length(lsPlan) gt 1 }">
															<c:forEach items="${lsPlan}" var="c" varStatus="x">
																<c:if test="${x.index gt 0}">
																	<th style="text-align: center;"><form:input id="rider_prod_persen${cmd.totalPenyakitUwDesc+1}" path="listUwDec[${cmd.totalPenyakitUwDesc}].rider[${x.index-1}].rate_persen" size="4" /></th>
																	<th style="text-align: center;"><form:input id="rider_prod_permil${cmd.totalPenyakitUwDesc+1}" path="listUwDec[${cmd.totalPenyakitUwDesc}].rider[${x.index-1}].rate_permil" size="4" /></th>
																</c:if>
															</c:forEach>
														</c:if>
														<th style="text-align: center;">&nbsp;</th>
														<th>&nbsp;</th>
													</tr>
													<tr>
														<th style="text-align: center;" colspan="2">UW Decision<br/>[${cmd.listUwDec[cmd.totalPenyakitUwDesc+1].input_date}]</th>
														<th style="text-align: center;"><form:input id="prod_utama_persen${cmd.totalPenyakitUwDesc+1}" path="listUwDec[${cmd.totalPenyakitUwDesc+1}].prod_utama_persen" size="4" /></th>
														<th style="text-align: center;"><form:input id="prod_utama_permil${cmd.totalPenyakitUwDesc+1}" path="listUwDec[${cmd.totalPenyakitUwDesc+1}].prod_utama_permil" size="4" /></th>
														<c:if test="${fn:length(lsPlan) gt 1 }">
															<c:forEach items="${lsPlan}" var="c" varStatus="x">
																<c:if test="${x.index gt 0}">
																	<th style="text-align: center;"><form:input id="rider_prod_persen${cmd.totalPenyakitUwDesc+2}" path="listUwDec[${cmd.totalPenyakitUwDesc+1}].rider[${x.index-1}].rate_persen" size="4" /></th>
																	<th style="text-align: center;"><form:input id="rider_prod_permil${cmd.totalPenyakitUwDesc+2}" path="listUwDec[${cmd.totalPenyakitUwDesc+1}].rider[${x.index-1}].rate_permil" size="4" /></th>
																</c:if>
															</c:forEach>
														</c:if>
														<th style="text-align: center;">&nbsp;</th>
														<th>&nbsp;</th>
													</tr>
													<tr>
														<td style="text-align: center;" colspan="2">Catatan</td>
														<c:choose>
															<c:when test="${not empty cmd.emBmi}">
																<td style="text-align: center;" colspan="${fn:length(lsPlan)+4}"><form:textarea id="catatan" path="listUwDec[${cmd.totalPenyakitUwDesc+1}].catatan" rows="4" cssStyle="width: 98%" onkeypress="return maxChar(event,this.value,300)" /></td> 
															</c:when>
															<c:otherwise>
																<td style="text-align: center;" colspan="3"><form:textarea id="catatan" path="listUwDec[${cmd.totalPenyakitUwDesc+1}].catatan" rows="4" cssStyle="width: 98%" /></td> 
															</c:otherwise>	
														</c:choose>
														<td>&nbsp;</td>
													</tr>
													<c:forEach items="${cmd.listUwDec}" var="b" varStatus="s">
														<c:if test="${s.index gt cmd.totalPenyakitUwDesc+1}">
															<tr>
																<th style="text-align: center;" colspan="2">Final Rating<br/>[${cmd.listUwDec[s.index].input_date}]</th>
																<th style="text-align: center;"><form:input id="prod_utama_persen${s.index+1}" path="listUwDec[${s.index}].prod_utama_persen" size="4" /></th>
																<th style="text-align: center;"><form:input id="prod_utama_permil${s.index+1}" path="listUwDec[${s.index}].prod_utama_permil" size="4" /></th>
																<c:if test="${fn:length(lsPlan) gt 1 }">
																	<c:forEach items="${lsPlan}" var="c" varStatus="x">
																		<c:if test="${x.index gt 0}">
																			<th style="text-align: center;"><form:input id="rider_prod_persen${x.index}" path="listUwDec[${s.index}].rider[${x.index-1}].rate_persen" size="4" /></th>
																			<th style="text-align: center;"><form:input id="rider_prod_permil${x.index}" path="listUwDec[${s.index}].rider[${x.index-1}].rate_permil" size="4" /></th>
																		</c:if>
																	</c:forEach>
																</c:if>
																<th style="text-align: center;">&nbsp;</th>
																<th>&nbsp;</th>
															</tr>
															<tr>
																<td style="text-align: center;" colspan="2">Catatan</td>
																<c:choose>
																	<c:when test="${not empty cmd.emBmi}">
																		<td style="text-align: center;" colspan="${fn:length(lsPlan)+4}"><form:textarea id="catatan" path="listUwDec[${s.index}].catatan" rows="4" cssStyle="width: 98%" onkeypress="return maxChar(event,this.value,300)" /></td>
																	</c:when>
																	<c:otherwise>
																		<td style="text-align: center;" colspan="3"><form:textarea id="catatan" path="listUwDec[${s.index}].catatan" rows="4" cssStyle="width: 98%" /></td>
																	</c:otherwise>	
																</c:choose>
																<td>&nbsp;</td>
															</tr>														
														</c:if>
													</c:forEach>
												</table>											
											</td>
										</tr>
										<tr>
											<td style="padding-top: 3px">
												<input type="button" class="button" name="addKelainan" value="Add Kelainan" onclick="return tampilkan(this);">
												<input type="button" class="button" name="sumRating" value="Sum Rating" onclick="return tampilkan(this);">
												<input type="button" class="button" name="addFinRat" value="Add Final Rating" onclick="return tampilkan(this);">
												<input type="button" class="button" name="delKelainan" value="Delete Kelainan" onclick="return tampilkan(this);">
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
							<tr>
								<th width="110" style="color: blue;">Financial Statement</th>
								<td style="padding-left: 3px">
									<table cellpadding="0" cellspacing="1" width="100%" class="entry2" border="0">
										<tr>
											<th width="102">Financial Statement</th>
											<td style="padding: 0px 0px 3px 2px" width="90">
												<label for="flagFsYes"><form:radiobutton path="flag_fs" value="1" id="flagFsYes" cssStyle="margin-right: 3px" />Yes</label>
												<label for="flagFsNo"><form:radiobutton path="flag_fs" value="0" id="flagFsNo" cssStyle="margin-right: 3px" />No</label>
											</td>
											<th width="102">Tanggal FS</th>
											<td style="padding-left: 3px">
												<spring:bind  path="cmd.fs_date">
													<script>inputDate('${status.expression}', '${status.value}', false);</script>
												</spring:bind>									
											</td>
										</tr>
										<tr>
											<th width="102">Dokumen lain</th>	
											<td colspan="3">
												<table cellpadding="0" cellspacing="0" width="100%" class="entry2" border="0">
													<tr>
														<td style="padding-left: 3px;"><label for="fs_copy_rek_bank"><form:checkbox path="fs_copy_rek_bank" id="fs_copy_rek_bank" value="1" cssStyle="margin-right: 3px" />Copy Rekening Bank 3 bln terakhir</label></td>
													</tr>
													<tr>
														<td style="padding-left: 3px;"><label for="fs_copy_npwp"><form:checkbox path="fs_copy_npwp" id="fs_copy_npwp" value="1" cssStyle="margin-right: 3px" />Copy NPWP</label></td>
													</tr>
													<tr>
														<td style="padding-left: 3px;"><label for="fs_spt_pribadi"><form:checkbox path="fs_spt_pribadi" id="fs_spt_pribadi" value="1" cssStyle="margin-right: 3px" />SPT Pribadi</label></td>
													</tr>
													<tr>
														<td style="padding-left: 3px;"><label for="fs_copy_neraca_persh"><form:checkbox path="fs_copy_neraca_persh" id="fs_copy_neraca_persh" value="1" cssStyle="margin-right: 3px" />Copy Neraca Perusahaan</label></td>
													</tr>
													<tr>
														<td style="padding-left: 3px;">
															<label for="fs_lain"><form:checkbox path="fs_lain" id="fs_lain" value="1" cssStyle="margin-right: 3px" />lain-lain</label>
															<form:input id="fs_lain_desc" path="fs_lain_desc" cssStyle="width: 30%" onkeypress="return maxChar(event,this.value,30)" />
														</td>
													</tr>
												</table>	
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><span style="color: #FF0000">* Wajib diisi jika menginput data medis</span></td>
				</tr>
				<tr>
					<td align="center" style="padding-top: 5px;">
					<c:choose>
						<c:when test="${sessionScope.currentUser.lus_id eq 1592 || sessionScope.currentUser.lus_id eq 533 || sessionScope.currentUser.lus_id eq 144 || sessionScope.currentUser.lus_id eq 449 || sessionScope.currentUser.lus_id eq 3458 || sessionScope.currentUser.lus_id eq 3672 || sessionScope.currentUser.lus_id eq 3813}">
							<!--permintaan dari dr.rachel (buka akses sementara & jgn lupa bagian otorisasi): Rachel
						AdeW
						Layla
						Ingrid 
						Grace Beatrix 
						RUDI_HERMANSYAH -->	
							<input type="button" class="button" name="saveWorksheet" value="Save Worksheet" onclick="return tampilkan(this);" >
						</c:when>
						<c:otherwise>
							<input type="button" class="button" name="saveWorksheet" value="Save Worksheet" onclick="return tampilkan(this);" <c:if test="${cmd.okSave eq 0}">disabled="disabled"</c:if>>
						</c:otherwise>
					</c:choose>
						<input type="button" class="button" name="reasWorksheet" value="Reas Worksheet" onclick="popWin('${path}/uw/hasil_reas.htm?spaj=${cmd.reg_spaj}&mode=&no=${cmd.insured_no}',300,1000);">
						<input type="button" class="button" name="printWorksheet" value="Print Worksheet" onclick="return tampilkan(this);" <c:if test="${cmd.okPrint eq 0}">disabled="disabled"</c:if>>
						<c:if test="${cmd.position eq 0}">
							<input type="button" class="button" name="transfer_uw1" value="Transfer MED-UW1" onclick="return tampilkan(this);" <c:if test="${cmd.okTransfer1 eq 0}">disabled="disabled"</c:if>>
							<input type="button" class="button" name="transfer_uw2" value="Transfer MED-UW2" onclick="return tampilkan(this);" <c:if test="${cmd.okTransfer2 eq 0}">disabled="disabled"</c:if>>
						</c:if>
						<c:if test="${cmd.position eq 1}">
							<input type="button" class="button" name="reverse_staff" value="Reverse Staff" onclick="return tampilkan(this);" <c:if test="${cmd.okReverse1 eq 0}">disabled="disabled"</c:if>>
							<input type="button" class="button" name="transfer_uw2" value="Transfer MED-UW2" onclick="return tampilkan(this);" <c:if test="${cmd.okTransfer2 eq 0}">disabled="disabled"</c:if>>
						</c:if>
						<c:if test="${cmd.position eq 2}">
							<input type="button" class="button" name="reverse_staff" value="Reverse Staff" onclick="return tampilkan(this);" <c:if test="${cmd.okReverse1 eq 0}">disabled="disabled"</c:if>>
							<input type="button" class="button" name="reverse_uw1" value="Reverse MED-UW1" onclick="return tampilkan(this);" <c:if test="${cmd.okReverse2 eq 0}">disabled="disabled"</c:if>>
						</c:if>
					</td>
				</tr>	
			</table>
			<input type="hidden" name="submitMode" id="submitMode">
			<input type="hidden" name="spaj" value="${cmd.reg_spaj}">
			<input type="hidden" name="medicalType" id="medicalType">
			<script>
				<c:if test="${not empty cmd.successMsg}">
					alert('${cmd.successMsg}');
				</c:if>
			</script>
		</form:form>
	</body>
<%@ include file="/include/page/footer.jsp"%>