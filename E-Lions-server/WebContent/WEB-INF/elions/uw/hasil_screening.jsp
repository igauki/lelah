<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>
		<script>

			var today = new Date();
			
			window.onload = function() { 
					setTimeout("info_awal()", 1);	
			};
	
				var today2 = new Date();
				window.status = ((today2.valueOf() - today.valueOf()) / 1000);
				
			function info_awal(){
				setupPanes("container1", "tab1");
				document.body.style.visibility='visible';
			}
			
		</script>
</head>
<body style="background-color:#eaeded;">		
	<c:if test="${pilihan eq '1'}"> 
	<div class="tab-container" id="container1" style="width: 115%">
							<ul class="tabs" style="width: 100%">
								
									<li>
										
											
											
											
												<a href="#" onClick="return showPane('pane1', this)" 
												onmouseover="return overlib('Alt-1', AUTOSTATUS, WRAP);" onmouseout="nd();"
												onfocus="return showPane('pane1', this)" accesskey="1" id="tab1">Pemegang Polis</a>
											
										
									</li>
								
									<li>
										
											
											
											
												<a href="#" onClick="return showPane('pane2', this)" 
												onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();"
												onfocus="return showPane('pane2', this)" accesskey="2" id="tab2">Tertanggung</a>
											
										
									</li>
									
									<li>
										
											
											
											
												<a href="#" onClick="return showPane('pane3', this)" 
												onmouseover="return overlib('Alt-3', AUTOSTATUS, WRAP);" onmouseout="nd();"
												onfocus="return showPane('pane3', this)" accesskey="3" id="tab3">Penerima Manfaat</a>
											
										
									</li>
									
									<li>
										
											
											
											
												<a href="#" onClick="return showPane('pane4', this)" 
												onmouseover="return overlib('Alt-4', AUTOSTATUS, WRAP);" onmouseout="nd();"
												onfocus="return showPane('pane4', this)" accesskey="4" id="tab4">Tertanggung Tambahan</a>
											
										
									</li>
								
									
									
							</ul>
<div class="tab-panes">
	<div id="pane1" class="panes">
		<c:if test="${not empty pemegangDetail}">	
			<table id ="header_holder" class="entry2" style="width: 80%">
				<c:if test="${pemegangDetail.type_data eq 1}">
				    <tr>
				    			<th align = "left" nowrap="nowrap">Nama Pemegang Polis</th>
				    			<th align = "left" nowrap="nowrap"><input name="nama_pemegang" type="text" value="${pemegangDetail.mcl_first}">	
				    					<input type="button" value="View Detail Product" onClick="popWin('${path}/uw/view.htm?p=v&showSPAJ=${pemegangDetail.reg_spaj}&halForBlacklist=uw', 120, 500);" /></td> 					    			
				    			</th>
				    </tr>
				    
				    <tr>
				    			<th align = "left" nowrap="nowrap">Alamat Pemegang Polis</th>
				    			<th align = "left" nowrap="nowrap"><textarea id="alamat_pemegang" rows="4" cols="50">${pemegangDetail.alamat_rumah}</textarea>&nbsp;&nbsp;				    				
				    					<img style="border: none;" onClick="popWin('${path}/common/util.htm?window=multi_doc_list&spaj=${pemegangDetail.reg_spaj}', 500, 800);"  src="${path}/include/icons/pdf_icon.png">			    				
				    			</th>
				    </tr>
			    </c:if>
			    <c:if test="${pemegangDetail.type_data eq 2}">
				    <tr>	
				    	<th style="width:20%;vertical-align: top;height:50%;border: 20px solid #eaeded;" align = "left" nowrap="nowrap">Nama Pemegang Polis</th>
						<th style="width:20%;vertical-align: top;height:50%;border: 20px solid #eaeded;" align = "left" nowrap="nowrap"><input name="nama_pemegang" size="30" type="text" value="${pemegangDetail.mcl_first}"></th>
						<th style="width:20%;vertical-align: top;height:50%;border: 20px solid #eaeded;" align = "left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
						<th style="width:20%;vertical-align: top;height:50%;border: 20px solid #eaeded;" align = "left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>

				    </tr>
			    </c:if>
			</table>
					<fieldset>
				<legend>Ofac Result</legend>
					       <table class="displaytag" id = "result_holder" style="width: 80%">
					       		<thead>
						    		<tr>
						    			<th></th>
						    			<th>Nama SDN</th>
						    			<th>Alamat SDN</th>
						    			<th>Warga Negara</th>
						    			<th>Sdn Type</th>						    			
						    			<th>Status</th>
						    			<th>Skor</th>
						    			<!-- <th>Comment</th> -->
						    		</tr>
						    	</thead>
						    	<tbody>
							    	<c:if test="${not empty pemegangOfacResults}">
							    		<c:forEach var="pemegang_ofac_res" items="${pemegangOfacResults}" varStatus="stat">
								    		<tr class="odd">
								    			<td><input type="checkbox" id="v1" name="v1" value="Bike"></td>
								    			<td>${pemegang_ofac_res.sdn_name}</td>
								    			<td>${pemegang_ofac_res.address}</td>
								    			<td>${pemegang_ofac_res.country}</td>
								    			<td>${pemegang_ofac_res.sdn_type}</td>								    	
								    			<td>${pemegang_ofac_res.mofs_status_message}</td>
								    			<td>${pemegang_ofac_res.score}</td>
								    			<%-- <td><!-- <input type="button" name="refresh" value="Comment"/> 
								    				<input type="button" value="Comment" onClick="popWin('${path}/uw/input_comment.htm?window=tampil&mofs_id=${pemegang_res.mofs_id}&spaj=${pemegang_res.reg_spaj}&type=${pemegang_res.mofs_status_message}', 500, 500);" />-->
								    				<input type="button" value="Comment" onClick="popWin('${path}/uw/input_comment.htm?window=tampil&add_num=${pemegang_ofac_res.add_num}&mofs_id=${pemegang_ofac_res.mofs_id}&spaj=${pemegang_res.reg_spaj}&type=${pemegang_ofac_res.mofs_type}&nama=${pemegangDetail.mcl_first}', 120, 500);" /></td>
 --%>								    		</tr>	
								    	</c:forEach>				    	
							 		</c:if>   
									<c:if test="${empty pemegangOfacResults}">			    	
							    		<tr>
							    			<td colspan = "7" align="center"><font color = "red">Tidak ada hasil OFAC Screening pada pemegang polis Sertifikat/Polis ini</font></td>
							    		</tr>
						    		</c:if>
						    	</tbody>  		
					    	</table>
				</fieldset>
				</c:if>
				<c:if test="${empty pemegangDetail}">			    	
							    		<tr>
							    			<td colspan = "7" align="center"><font color = "red">Tidak ada hasil OFAC Screening pada pemegang polis Sertifikat/Polis ini</font></td>
							    		</tr>
				</c:if>
	</div>
									
	<div id="pane2" class="panes">
		<c:if test="${not empty tertanggungDetail}">	
			     	<table id = "header_tt" class="entry2" style="width: 80%">
				     	<c:if test="${tertanggungDetail.type_data eq 1}">
				    		<tr>
				    			<th align = "left" nowrap="nowrap">Nama Tertanggung</th>
				    			<th align = "left" nowrap="nowrap"><input name="nama_tertanggung" size="30" type="text" value="${tertanggungDetail.mcl_first}"></th>
				    		</tr>
				    		<tr>
				    			<th align = "left" nowrap="nowrap">Alamat Tertanggung</th>
				    			<th align = "left" nowrap="nowrap"><textarea id="alamat_tertanggung" rows="4" cols="50">${tertanggungDetail.alamat_rumah}</textarea>&nbsp;&nbsp;
					    				<img style="border: none;" onClick="popWin('${path}/common/util.htm?window=multi_doc_list&spaj=${tertanggungDetail.reg_spaj}', 500, 800);"  src="${path}/include/icons/pdf_icon.png">
					    				<%-- <a href='${path}/uw/input_comment.htm?window=tampil&spaj=09200077009&nama=IMAN'> simpan </a> --%>

				    			</th>
				    		</tr>
				    	</c:if>
				    	<c:if test="${tertanggungDetail.type_data eq 2}">
						    <tr>	
						    	<th style="width:20%;vertical-align: top;height:50%;border: 20px solid #eaeded;" align = "left" nowrap="nowrap">Nama Tertanggung</th>
								<th style="width:20%;vertical-align: top;height:50%;border: 20px solid #eaeded;" align = "left" nowrap="nowrap"><input name="nama_tertanggung" size="30" type="text" value="${tertanggungDetail.mcl_first}"></th>
								<th style="width:20%;vertical-align: top;height:50%;border: 20px solid #eaeded;" align = "left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th style="width:20%;vertical-align: top;height:50%;border: 20px solid #eaeded;" align = "left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
		
						    </tr>
			    		</c:if>
			    	</table>
			
					<fieldset> 
						<legend>Ofac Result</legend>
							<table class="displaytag" id = "result_holder" style="width: 80%">
					       		<thead>
						    		<tr>
						    			<th></th>
						    			<th>Nama SDN</th>
						    			<th>Alamat SDN</th>
						    			<th>Warga Negara</th>
						    			<th>Sdn Type</th>						    			
						    			<th>Status</th>
						    			<th>Skor</th>
						    			<!-- <th>Comment</th> -->
						    		</tr>
						    	</thead>
						    	<tbody>
							    	<c:if test="${not empty tertanggungOfacResults}">
							    		<c:forEach var="tertanggung_ofac_res" items="${tertanggungOfacResults}" varStatus="stat">
								    		<tr class="odd">
								    			<td><input type="checkbox" id="v1" name="v1" value="Bike"></td>
								    			<td>${tertanggung_ofac_res.sdn_name}</td>
								    			<td>${tertanggung_ofac_res.address}</td>
								    			<td>${tertanggung_ofac_res.country}</td>
								    			<td>${tertanggung_ofac_res.sdn_type}</td>								    	
								    			<td>${tertanggung_ofac_res.mofs_status_message}</td>
								    			<td>${tertanggung_ofac_res.score}</td>
								    			<%-- <td><!-- <input type="button" name="refresh" value="Comment"/> 
								    				<input type="button" value="Comment" onClick="popWin('${path}/uw/input_comment.htm?window=tampil&mofs_id=${pemegang_res.mofs_id}&spaj=${pemegang_res.reg_spaj}&type=${pemegang_res.mofs_status_message}', 500, 500);" />-->
								    				<input type="button" value="Comment" onClick="popWin('${path}/uw/input_comment.htm?window=tampil&add_num=${tertanggung_ofac_res.add_num}&mofs_id=${tertanggung_ofac_res.mofs_id}&spaj=${view_spaj}&type=${tertanggung_ofac_res.mofs_type}&nama=${tertanggungDetail.mcl_first}', 120, 500);" /></td> --%>
								    		</tr>	
								    	</c:forEach>				    	
							 		</c:if>   
									<c:if test="${empty tertanggungOfacResults}">			    	
							    		<tr>
							    			<td colspan = "7" align="center"><font color = "red">Tidak ada hasil OFAC Screening pada Terganggung Sertifikat/Polis ini</font></td>
							    		</tr>
						    		</c:if>
						    	</tbody>  		
					    	</table>
			
					  </fieldset>	   
			
				  </c:if>
				  <c:if test="${empty tertanggungDetail}">			    	
							    		<tr>
							    			<td colspan = "7" align="center"><font color = "red">Tidak ada hasil OFAC Screening pada Terganggung Sertifikat/Polis ini</font></td>
							    		</tr>
				</c:if>
	</div>
									
	<div id="pane3" class="panes">
		
		
		
		
				    
				    
				    
				    
				    
				    <c:if test="${not empty benefResults}">
				    		    <table id = "header_beneficiary" class="entry2" style="width: 80%">
				    		    		<c:forEach var="beneficiary_det" items="${benefResults}">
								    		   <tr>
								    			<th style="width:20%;vertical-align: top;height:50%;border: 20px solid #eaeded;" align = "left" nowrap="nowrap">Nama Penerima Manfaat</th>
								    			<th style="width:20%;vertical-align: top;height:50%;border: 20px solid #eaeded;" align = "left" nowrap="nowrap"><input name="nama_penmanfaat" size="30" type="text" value="${beneficiary_det.key}"></th>
								    			<th style="width:20%;vertical-align: top;height:50%;border: 20px solid #eaeded;" align = "left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
								    			<th style="width:20%;vertical-align: top;height:50%;border: 20px solid #eaeded;" align = "left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
								    			</tr>
				    		    		
				    		    		
				    		    			<tr>				    		
					    		<td colspan ="4">
							    	<fieldset>
										<legend>Ofac Result</legend>
									       <table  class="displaytag" id = "result_beneficiary">
									       		<thead>
										    		<tr>
										    			<th></th>
										    			<th>Nama SDN</th>
										    			<th>Alamat SDN</th>
										    			<th>Warga Negara</th>
										    			<th>Sdn Type</th>
										    			<th>Status</th>
										    			<th>Skor</th>
										    			<!-- <th>Comment</th> -->
										    		</tr>
										    	</thead>
										    	<tbody>
											    	<c:if test="${not empty beneficiary_det.value}">
												    	<c:forEach var="beneficiary_res" items="${beneficiary_det.value}" varStatus="stat">
													    		<tr class="odd">
													    			<td><input type="checkbox" id="v1" name="v1" value="Bike"></td>
													    			<td>${beneficiary_res.sdn_name}</td>
													    			<td>${beneficiary_res.address}</td>
													    			<td>${beneficiary_res.country}</td>
													    			<td>${beneficiary_res.sdn_type}</td>
													    			<td>${beneficiary_res.mofs_status_message}</td> 
													    		    <td>${beneficiary_res.score}</td> 
													    			<%-- <td><input type="button" value="Comment" onClick="popWin('${path}/uw/input_comment.htm?window=tampil&add_num=${beneficiary_res.add_num}&mofs_id=${beneficiary_res.mofs_id}&spaj=${view_spaj}&type=${beneficiary_res.mofs_type}&nama=${beneficiary_det.key}', 120, 500);" /></td> --%>
													    		</tr>
													    	
											    		</c:forEach>
											    	</c:if>
											    
													<c:if test="${empty beneficiary_det.value}">
												    		<tr>
												    			<td colspan = "7"><font color = "red">Tidak ada hasil OFAC Screening pada Penerima Manfaat Sertifikat/Polis ini</font></td>	
												    		</tr>
											    	</c:if>
										    </tbody> 
								    	</table>
								    </fieldset>
								</td>
							</tr>
				    	</c:forEach>
				     </table>
				    </c:if>
				    <c:if test="${empty benefResults}">
				    	<table align ="center" id = "empty_pp" align = "center">
				    		<tr>
				    		<td colspan = "7"><font color = "red">Sertifikat/Polis ini tidak mempunyai Penerima Manfaat</font></td>
							</tr>
				    	</table>
				  	</c:if>
				    
				    
				    
				    
				    
				    
				    
				    
				    
				    
				    
				    
				    
				    
				    
				    
				    
	</div>
									
	<div id="pane4" class="panes">
	
	
	
	
	
				    
				    
				    
				    <c:if test="${not empty pesertaTambahanResults}">
				    		    <table id = "header_beneficiary" class="entry2" style="width: 80%">
				    		    		<c:forEach var="beneficiary_det" items="${pesertaTambahanResults}">
								    		   <tr>
								    			<th style="width:20%;vertical-align: top;height:50%;border: 20px solid #eaeded;" align = "left" nowrap="nowrap">Nama Tertanggung Tambahan</th>
								    			<th style="width:20%;vertical-align: top;height:50%;border: 20px solid #eaeded;" align = "left" nowrap="nowrap"><input name="nama_penmanfaat" size="30" type="text" value="${beneficiary_det.key}"></th>
								    			<th style="width:20%;vertical-align: top;height:50%;border: 20px solid #eaeded;" align = "left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
								    			<th style="width:20%;vertical-align: top;height:50%;border: 20px solid #eaeded;" align = "left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
								    			</tr>
				    		    		
				    		    		
				    		    			<tr>				    		
									    		<td colspan ="4">
											    	<fieldset>
														<legend>Ofac Result</legend>
													       <table  class="displaytag" id = "result_beneficiary">
													       		<thead>
														    		<tr>
														    			<th></th>
														    			<th>Nama SDN</th>
														    			<th>Alamat SDN</th>
														    			<th>Warga Negara</th>
														    			<th>Sdn Type</th>
														    			<th>Status</th>
														    			<th>Skor</th>
														    			<!-- <th>Comment</th> -->
														    		</tr>
														    	</thead>
														    	<tbody>
															    	<c:if test="${not empty beneficiary_det.value}">
																    	<c:forEach var="beneficiary_res" items="${beneficiary_det.value}" varStatus="stat">
																    			<tr class="odd">
																	    			<td><input type="checkbox" id="v1" name="v1" value="Bike"></td>
																	    			<td>${beneficiary_res.sdn_name}</td>
																	    			<td>${beneficiary_res.address}</td>
																	    			<td>${beneficiary_res.country}</td>
																	    			<td>${beneficiary_res.sdn_type}</td>
																	    			<td>${beneficiary_res.mofs_status_message}</td> 
																	    		    <td>${beneficiary_res.score}</td> 
																	    			<%-- <td><input type="button" value="Comment" onClick="popWin('${path}/uw/input_comment.htm?window=tampil&add_num=${beneficiary_res.add_num}&mofs_id=${beneficiary_res.mofs_id}&spaj=${view_spaj}&type=${beneficiary_res.mofs_type}&nama=${beneficiary_det.key}', 120, 500);" /></td> --%>
																	    		</tr>
																	    	
															    		</c:forEach>
															    	</c:if>
															    
																	<c:if test="${empty beneficiary_det.value}">
																    	<tr>
																    		<td colspan = "7" style="align:center"><font color = "red">Tidak ada hasil OFAC Screening pada Tertanggung Tambahan Sertifikat/Polis ini</font></td>	
																    	</tr>
															    	</c:if>
														    </tbody> 
												    	</table>
												    </fieldset>
												</td>
											</tr>
				    		    		</c:forEach>
				    		    	</table>
				    </c:if>
				    <c:if test="${empty pesertaTambahanResults}">
				    	<table align ="center" id = "empty_pp" align = "center">
				    		<tr>
				    		<td colspan = "7"><font color = "red">Sertifikat/Polis ini tidak mempunyai Tertanggung Tambahan</font></td>
							</tr>
				    	</table>
				  	</c:if>
</div>

</div>
	</div>
	</c:if>
</body>