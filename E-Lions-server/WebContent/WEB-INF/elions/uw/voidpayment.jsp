<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>


<body style="height: 100%;">
	<form:form id="formpost" name="formpost" commandName="cmd">
		<table class="entry2">
			<input type="hidden" value="${cmd.nomor}" name="nomor" id="nomor">
			<thead>
				<tr>
					<th>NO</th>
					<th>NO ACCOUNT</th>
					<th>NAMA BANK</th>
					<th width="5%">MATA UANG</th>
					<th>JUMLAH PREMI</th>
					<th>TANGGAL BAYAR</th>
					<th>TANGGAL INPUT</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${cmd.payment.lsPayment}" var="s" varStatus="st">
					<tr onMouseOver="Javascript:this.bgColor='#FF7E7E';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#F7F7F7';return true;"
								onclick="window.parent.tes('${s.mspa_payment_id}-${cmd.nomor}');">
						<td style="text-align: center;">${st.count}</td>
						<td style="text-align: center;">${s.lre_acc_no}</td>
						<td style="text-align: center;">${s.lbn_nama} </td>
						<td style="text-align: center;">${s.lku_symbol}</td>
						<td style="text-align: right;"><fmt:formatNumber value="${s.mspa_payment}" type="number"></fmt:formatNumber></td>
						<td style="text-align: center;"><fmt:formatDate value="${s.mspa_pay_date}" pattern="dd/MM/yyyy"/></td>
						<td style="text-align: center;"><fmt:formatDate value="${s.mspa_input_date}" pattern="dd/MM/yyyy"/></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</form:form>
</body>



<%@ include file="/include/page/footer.jsp"%>