<%@ include file="/include/page/header.jsp"%>
<script>
	
	function cek_rb_tt(i){
	//alert(i);
		document.formpost.chooseTt.value=i;
	}
	
	function cek_rb_pp(i){
	//alert(i);
		document.formpost.choosePp.value=i;
	}
	function proses(){
		document.formpost.btnProses.disabled=true;
		formpost.submit();
	}
	function awal(){
		//untuk pilihan select
		p=formpost.choosePp.value;
		t=formpost.chooseTt.value;
		//
		val=formpost.flag.value;///1=tertanggung else pemengang dan tertanggung
		var mcl = document.getElementById('mcl').value;
		if(val=='1'){
			tLength=formpost.rb_tt.length;
			if(tLength!=null){
				for(i=0;i<tLength;i++){
					if(document.formpost.mclID[i].value==mcl){
						formpost.rb_tt[i].checked=true;	
						document.formpost.chooseTt.value=i;
					}
				}
				//if(t!=''){
				//	formpost.rb_tt[t].checked=true;		
				//}
			}	
			
		}else if(val=='0'){
			tLength=formpost.rb_tt.length;
			pLength=formpost.rb_pp.length;
			if(tLength!=null){
				if(t!=''){
					formpost.rb_tt[t].checked=true;		
					document.formpost.chooseTt.value=t;
				}
			}
			//
			if(pLength!=null){
				if(p!=''){
					formpost.rb_pp[p].checked=true;	
					document.formpost.choosePp.value=p;	
				}
			}
			
		}
	
		//
		info=${cmd.error};
		//alert(info);
		if(info==100){
			document.formpost.btnProses.disabled=true;
			//window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
			alert("Proses Simultan Berhasil");
		}else if(info==1){
			pos='${cmd.flagId}';
			document.formpost.btnProses.disabled=true;
			//window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
			alert("Posisi Polis ini Ada di"+pos);
		}else if(info==2){
			document.formpost.btnProses.disabled=true;
			//window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
			alert("Proses Simultan Sudah pernah dilakukan untuk Pemegang & Tertanggung Polis");
		}else if(info==3){
			document.formpost.btnProses.disabled=true;
			//window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
			alert("Tidak Bisa proses Simultan dan Reas \npolis ASM");
		}else if(info==103){
			document.formpost.btnProses.disabled=true;
			//window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
			alert("Error Pada saat get counter Silahkan hubungi EDP");
		}else if(info==102){
			document.formpost.btnProses.disabled=true;
			//window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
			alert("Tertanggung ini Memiliki Premi >= Rp 500jt ");
		}else if(info==104){
			document.formpost.btnProses.disabled=true;
			//window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
			alert("Pemegang ini Memiliki Premi >= Rp 500jt ");
		}
	}

	function updateSim(sts){
		
		if(sts==1){//pemegang
			indicatorPp.style.display='';
			pjg=document.formpost.cek_idPp.length;
			id_simPp='';
			cekTblPp='';
			j=0;
			corect=0;
			for(i=0;i<pjg;i++){
				if(document.formpost.cek_idPp[i].checked){
					corect++;
					id_simPp+=document.formpost.idSimPp[i].value+";";
					cekTblPp+=i+";";
				}
			}
			if(corect>1){
				id_simPp=id_simPp.substring(0,id_simPp.length-1);
				cekTblPp=cekTblPp.substring(0,cekTblPp.length-1);
				updateSimultan(id_simPp,cekTblPp,sts,'${cmd.currentUser.lus_full_name}');
			}else{
				indicatorPp.style.display='none';
				alert("Simultan ID Pemegang tidak BIsa Di Update.\nJumlah Min.. 2");
			}	
		}else if(sts==2){//tertanggung
			indicatorTt.style.display='';
			pjg=document.formpost.cek_idTt.length;
			id_simTt='';
			cekTblTt='';
			j=0;
			corect=0;
			for(i=0;i<pjg;i++){
				if(document.formpost.cek_idTt[i].checked){
					corect++;
					id_simTt+=document.formpost.idSimTt[i].value+";";
					cekTblTt+=i+";";
				}
			}
			if(corect>1){
				id_simTt=id_simTt.substring(0,id_simTt.length-1);
				cekTblTt=cekTblTt.substring(0,cekTblTt.length-1);
				updateSimultan(id_simTt,cekTblTt,sts,'${cmd.currentUser.lus_full_name}');
			}else{
				indicatorTt.style.display='none';
				alert("Simultan ID Tertanggung tidak BIsa Di Update.\nJumlah Min.. 2");
			}	
			
		}else if(sts==3){//pemegang dan tertanggung
			indicator.style.display='';
			pjg=document.formpost.cek_id.length;
			id_sim='';
			cekTbl='';
			j=0;
			corect=0;
			for(i=0;i<pjg;i++){
				if(document.formpost.cek_id[i].checked){
					corect++;
					id_sim+=document.formpost.idSim[i].value+";";
					cekTbl+=i+";";
				}
				//document.getElementById("idSim"+(i+1)).style.color = "red";
			}
			if(corect>1){
				id_sim=id_sim.substring(0,id_sim.length-1);
				cekTbl=cekTbl.substring(0,cekTbl.length-1);
				updateSimultan(id_sim,cekTbl,sts,'${cmd.currentUser.lus_full_name}');
			}else{
				indicator.style.display='none';
				alert("Simultan ID tidak BIsa Di Update.\nJumlah Min.. 2");
			}	
			
		}
	
	}
	
</script>

<form name="formpost" method="post">
	<body onload="awal()";>
		<c:choose>
			<c:when test="${cmd.flagAdd eq 1 }">
				<fieldset>
					<legend>
						Simultan Tertanggung (Hubungan Diri Sendiri)
					</legend>
					<display:table id="baris" name="cmd.lsSimultanTt"
						class="displaytag">
						<display:column title="Keterangan">
							<c:choose>
								<c:when
									test="${baris.SYMBOL eq \"XX\" || baris.SYMBOL eq \"WW\"  }">
									<a
										href="javascript:popWin('${path}/uw/detailsimultan_simple_bac.htm?msp_id=${cmd.spaj }&flag_tt=${baris.BRS }', 400, 800,1,0) ;">New
										Client</a>
								</c:when>
								<c:otherwise>
									<a
										href="javascript:popWin('${path}/uw/detailsimultan_simple_bac.htm?msp_id=${cmd.spaj }&flag_tt=${baris.BRS }', 400, 800,1,0) ;">Detail
										Simultan</a>
								</c:otherwise>
							</c:choose>
						</display:column>
						<display:column property="MCL_FIRST" title="Nama" />
						<display:column property="MSPE_PLACE_BIRTH" title="Tempat Lahir" />
						<display:column property="MSPE_DATE_BIRTH" title="Tanggal"
							format="{0, date, dd/MM/yyyy}" />
						<display:column title="Jenis Identitas">
							<c:forEach var="s" items="${lsIdentity}">
								<c:if test="${baris.LSIDE_ID eq s.LSIDE_ID}">${s.LSIDE_NAME }
						</c:if>
							</c:forEach>
						</display:column>
						<display:column property="MSPE_NO_IDENTITY" title="No Identitas" />
						<display:column title="Jns Kelamin">
							<c:choose>
								<c:when test="${baris.MSPE_SEX eq 1 }">
					Pria
					</c:when>
								<c:when test="${baris.MSPE_SEX eq 0 }">
					Wanita
					</c:when>
							</c:choose>
						</display:column>
						<display:column property="MSPE_STS_MRT" title="Status" />
						<display:column property="MSPE_MOTHER" title="Nama Ibu Kandung" />
						<display:column title="MCL_ID">
							<input type="text" readonly id="mclID${baris.BRS+1}" name="mclID"
								class="noBorder" value="${baris.MCL_ID}">
						</display:column>
						<display:column title="ID_SIMULTAN">
							<input type="text" readonly id="idSim${baris.BRS+1}" name="idSim"
								class="noBorder" value="${baris.ID_SIMULTAN}">
						</display:column>
						<display:column title="">
							<c:choose>
								<c:when test="${baris.BRS eq 0 }">
									<input type="radio" name="rb_tt" class="noBorder"
										onClick="cek_rb_tt(${baris.BRS })" checked>
								</c:when>
								<c:when test="${baris.BRS gt 0}">
									<input type="radio" name="rb_tt" class="noBorder"
										onClick="cek_rb_tt(${baris.BRS })">
								</c:when>
							</c:choose>
						</display:column>
						<display:column title="SIMULTAN">
							<c:if test="${cmd.rowPp-baris.BRS>=2}">
								<input type="checkbox" class="noBorder" name="cek_id">
							</c:if>
						</display:column>
					</display:table>
					<table class="entry2">
						<tr>
							<td align="right">
								<input type="button"
									<c:if test="${cmd.rowPp <= 2}">disabled</c:if>
									value="Simultan ID" name="btnsim" onClick="updateSim(3)";>
								<span id="indicator" style="display:none;"><img
										src="${path}/include/image/indicator.gif" />
								</span>

							</td>
						</tr>
					</table>
				</fieldset>
			</c:when>
			<c:when test="${cmd.flagAdd ne 1 }">
				<fieldset>
					<legend>
						Simultan Tertanggung
					</legend>
					<display:table id="baris" name="cmd.lsSimultanTt"
						class="displaytag">
						<display:column title="Keterangan">
							<c:choose>
								<c:when
									test="${baris.SYMBOL eq \"XX\" || baris.SYMBOL eq \"WW\"  }">
									<a
										href="javascript:popWin('${path}/uw/detailsimultan_new.htm?spajAwal=${cmd.spaj }&flag_tt=${baris.BRS }', 400, 800,1,0) ;">New
										Client</a>
								</c:when>
								<c:otherwise>
									<a
										href="javascript:popWin('${path}/uw/detailsimultan_new.htm?spajAwal=${cmd.spaj }&flag_tt=${baris.BRS }', 400, 800,1,0) ;">Detail
										Simultan</a>
								</c:otherwise>
							</c:choose>
						</display:column>
						<display:column property="MCL_FIRST" title="Nama" />
						<display:column property="MSPE_PLACE_BIRTH" title="Tempat Lahir" />
						<display:column property="MSPE_DATE_BIRTH" title="Tanggal"
							format="{0, date, dd/MM/yyyy}" />
						<display:column title="Jenis Identitas">
							<c:forEach var="s" items="${lsIdentity}">
								<c:if test="${baris.LSIDE_ID eq s.LSIDE_ID}">${s.LSIDE_NAME }
						</c:if>
							</c:forEach>
						</display:column>
						<display:column property="MSPE_NO_IDENTITY" title="No Identitas" />
						<display:column title="Jns Kelamin">
							<c:choose>
								<c:when test="${baris.MSPE_SEX eq 1 }">
					Pria
					</c:when>
								<c:when test="${baris.MSPE_SEX eq 0 }">
					Wanita
					</c:when>
							</c:choose>
						</display:column>
						<display:column property="MSPE_STS_MRT" title="Status" />
						<display:column property="MSPE_MOTHER" title="Nama Ibu Kandung" />
						<display:column property="MCL_ID" title="MCL_ID" />
						<display:column title="ID_SIMULTAN">
							<input type="text" readonly id="idSimTt${baris.BRS+1}" name="idSimTt" class="noBorder"
								value="${baris.ID_SIMULTAN}">
						</display:column>

						<display:column title="">
							<c:choose>
								<c:when test="${baris.BRS eq 0 }">
									<input type="radio" name="rb_tt" class="noBorder"
										onClick="cek_rb_tt(${baris.BRS })" checked>
								</c:when>
								<c:when test="${baris.BRS gt 0}">
									<input type="radio" name="rb_tt" class="noBorder"
										onClick="cek_rb_tt(${baris.BRS })">
								</c:when>
							</c:choose>
						</display:column>
						<display:column title="SIMULTAN">
							<c:if test="${cmd.rowTt-baris.BRS>=2}">
								<input type="checkbox"class="noBorder"  name="cek_idTt">
							</c:if>
						</display:column>
					</display:table>
					<table class="entry2">
						<tr>
							<td align="right" >
								<input type="button" value="Simultan ID TT"
									<c:if test="${cmd.rowTt <= 2}">disabled</c:if> name="btnsim2"
									onClick="updateSim(2)";>
								<span id="indicatorTt" style="display:none;"><img
										src="${path}/include/image/indicator.gif" />
								</span>

							</td>
						</tr>
					</table>
				</fieldset>
				<fieldset>
					<legend>
						Simultan Pemegang
					</legend>
					<display:table id="baris" name="cmd.lsSimultanPp"
						class="displaytag">
						<display:column title="Keterangan">
							<c:choose>
								<c:when
									test="${baris.SYMBOL eq \"XX\" || baris.SYMBOL eq \"WW\"  }">
									<a
										href="javascript:popWin('${path}/uw/detailsimultan_new.htm?spajAwal=${cmd.spaj }&flag_pp=${baris.BRS }', 400, 800,1,0) ;">New
										Client</a>
								</c:when>
								<c:otherwise>
									<a
										href="javascript:popWin('${path}/uw/detailsimultan_new.htm?spajAwal=${cmd.spaj }&flag_pp=${baris.BRS }', 400, 800,1,0) ;">Detail
										Simultan</a>
								</c:otherwise>
							</c:choose>
						</display:column>
						<display:column property="MCL_FIRST" title="Nama" />
						<display:column property="MSPE_PLACE_BIRTH" title="Tempat Lahir" />
						<display:column property="MSPE_DATE_BIRTH" title="Tanggal"
							format="{0, date, dd/MM/yyyy}" />
						<display:column title="Jenis Identitas">
							<c:forEach var="s" items="${lsIdentity}">
								<c:if test="${baris.LSIDE_ID eq s.LSIDE_ID}">${s.LSIDE_NAME }
						</c:if>
							</c:forEach>
						</display:column>
						<display:column property="MSPE_NO_IDENTITY" title="No Identitas" />
						<display:column title="Jns Kelamin">
							<c:choose>
								<c:when test="${baris.MSPE_SEX eq 1 }">
					Pria
					</c:when>
								<c:when test="${baris.MSPE_SEX eq 0 }">
					Wanita
					</c:when>
							</c:choose>
						</display:column>
						<display:column property="MSPE_STS_MRT" title="Status" />
						<display:column property="MSPE_MOTHER" title="Nama Ibu Kandung" />
						<display:column property="MCL_ID" title="MCL_ID" />
						<display:column title="ID_SIMULTAN">
							<input type="text" readonly id="idSimPp${baris.BRS+1}" name="idSimPp" class="noBorder"
								value="${baris.ID_SIMULTAN}">
						</display:column>
						<display:column title="">
							<c:choose>
								<c:when test="${baris.BRS eq 0 }">
									<input type="radio" name="rb_pp" class="noBorder"
										onClick="cek_rb_pp(${baris.BRS })" checked>
								</c:when>
								<c:when test="${baris.BRS gt 0}">
									<input type="radio" name="rb_pp" class="noBorder"
										onClick="cek_rb_pp(${baris.BRS })">
								</c:when>
							</c:choose>
						</display:column>
						<display:column title="SIMULTAN">
							<c:if test="${cmd.rowPp-baris.BRS>=2}">
								<input type="checkbox" class="noBorder" name="cek_idPp">
							</c:if>
						</display:column>
					</display:table>
					<table class="entry2">
						<tr>
							<td align="right" >
								<input type="button" value="Simultan ID PP"
									<c:if test="${cmd.rowPp <= 2}">disabled</c:if> name="btnsim1"
									onClick="updateSim(1)";>
								<span id="indicatorPp" style="display:none;"><img
										src="${path}/include/image/indicator.gif" />
								</span>
							</td>
						</tr>
					</table>

				</fieldset>
			</c:when>
		</c:choose>
		<table class="entry2">
			<tr>
				<td align="center">
					<input type="button" id "btnProses" value="Proses" onClick="proses()" accesskey="P"
						name="btnProses">
					<input type="hidden" value="${cmd.error }" name="error">
					<input type="hidden" value="${cmd.flagAdd}" name="flag">
					<input type="hidden" value="${t}" name="chooseTt">
					<input type="hidden" value="${p }" name="choosePp">
					<input type="hidden" value="${cmd.mclId}" id="mcl" name="mcl">
				</td>
			</tr>
			<tr>
				<td>
					<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
								ERROR:
								<br>
								<c:forEach var="error" items="${status.errorMessages}">
									- <c:out value="${error}" escapeXml="false" />
									<br />
								</c:forEach>
							</div>
						</c:if>
					</spring:bind>
				</td>
			</tr>

		</table>
	</body>
</form>
<%@ include file="/include/page/footer.jsp"%>
