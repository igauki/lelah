<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript" src="${path}/include/js/jquery-1.3.2.min.js"></script>
<script>
$(document).ready(function() {
        <c:if test="${not empty pesanError}">
            alert('${pesanError}');
        </c:if>
    });
</script>
   <body onload="document.title='PopUp :: Edit Nama Pemegang';setupPanes('container1','tab1');" style="height: 100%;">
     <div class="tab-container" id="container1">
              <ul class="tabs">
                <li>
                    <a href="#" onClick="return showPane('pane1', this)" id="tab1">Edit Nama Pemegang</a>
                </li>
            </ul>
            <div class="tab-panes">
                <div id="pane1" class="panes">
                     <form id="formpost" name="formpost" method="post">
                    <fieldset>
                      <legend>Info User</legend>
                         <table class="entry2">
                   <tr>
                       <th nowrap="nowrap">User</th><th align ="left">&nbsp;[${infoDetailUser.LUS_ID}]&nbsp;&nbsp;${infoDetailUser.LUS_LOGIN_NAME}</th>
                  </tr>
                  <tr>
                       <th nowrap="nowrap">Nama<br></th><th align ="left">&nbsp;${infoDetailUser.LUS_FULL_NAME}&nbsp;[${infoDetailUser.LDE_DEPT}]</th>
                 </tr>
                    </table>
                         </fieldset>
         <fieldset>
             <legend>Edit Nama Pemegang&nbsp;</legend>
                <table class="entry2">
                <tr>
                    <th colspan = "2" nowrap = "nowrap" align ="left"><input disabled="disabled" type=text size=40 value="${pemegang.mcl_first}"/></th>
                </tr>
                <tr>
                     <th colspan = "2" nowrap = "nowrap" align ="left">DIUBAH MENJADI</th>
                 </tr>
                 <tr>
                     <th colspan = "2" nowrap = "nowrap" align ="left"><input type ="text" size = 40 name="mcl_first"/><font class="error">*</font></th>
                 </tr>
                 <tr>
                 <th colspan = "2">&nbsp;</th>
                 </tr>
             <th colspan = "2" nowrap = "nowrap" align ="left"> Keterangan(Alasan Melakukan Edit):</font></th> </tr>
                 <tr>
                 <th colspan = "2" nowrap = "nowrap" align ="left"><textarea rows="4" cols="20" style="width: 70%; text-transform: uppercase;" name="alasan"></textarea></font></th>
             </tr>
              <c:if test="${not empty editPemegang}">
                 <tr align = "left">
                        <th colspan="2"> 
                            <input type="hidden" name="save" id="save">  
                            <input type="submit" name="save" value="Save" onclick="return confirm('Apakah Anda Yakin Untuk Melanjutkan Proses ?');"> 
                            <input type="button" name="close" value="Close" onclick="window.close();"></font>
                            <!--<c:if test="${not empty pesanError}"><br/>
                                  <div id="error">${pesanError}</div>
                              </c:if>
                             --></th>
                         </tr>
                         <tr>
                            <td><font color="#ff0000">* Proses ini akan masuk ke ulangan(History)</font></td>
                        </tr>
                        </c:if>
                     </table>
               </fieldset>
            </form>
        </div>
      </div>
   </div>
</body>
<%@ include file="/include/page/footer.jsp"%>