<%@ include file="/include/page/header.jsp"%>
<style type="text/css">
html,body {
	overflow: hidden;
}
</style>
<script> 
	function buttonLinks(str){
		var spaj = document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value; 
		
		if(str=='cari'){
			popWin('${path}/uw/spaj.htm?posisi=4&win=payment', 350, 450);  //&search=yes
		}else if(str=='list_rk'){
			document.getElementById('infoFrame').src= '${path}/uw/uw.htm?window=drek&piyu=1&flagHalaman=2&startDate=${startDate}&endDate=${endDate}';
			document.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+spaj;
		}else if('auto' == str){
			document.getElementById('infoFrame').src='${path}/uw/uw.htm?window=autopayment';
	    }else if('auto_pa' == str){
			document.getElementById('infoFrame').src='${path}/uw/uw.htm?window=autopayment_pa';
		}else if('auto_bsm' == str){
			document.getElementById('infoFrame').src='${path}/uw/autopayment_bsm.htm';
		}else{
			if(spaj == ''){
				alert('Harap cari SPAJ terlebih dahulu!');
				return false;
			}else{
				if('show' == str){
					document.getElementById('infoFrame').src='${path}/uw/view.htm?showSPAJ='+spaj;
				}else if('uwinfo' == str) {
					document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=uwinfo&spaj='+spaj;
				}else if('paymenttopup' == str){
					if(document.getElementById('infoFrame')){
							document.getElementById('infoFrame').style.display='block';
							document.getElementById('infoFrame').src='${path}/uw/inputpayment.htm?tahun_ke=1&premi_ke=2&spaj='+spaj;
					}
				}else if('paymentnewbusiness' == str){
					if(document.getElementById('infoFrame')){
							document.getElementById('infoFrame').style.display='block';
							document.getElementById('infoFrame').src='${path}/uw/inputpayment.htm?tahun_ke=1&premi_ke=1&spaj='+spaj;
					} 
				}else if('cancel' == str){
					if(confirm('Anda yakin untuk membatalkan?')){
						if(document.getElementById('error')) document.getElementById('error').style.display='none';
						if(document.getElementById('success')) document.getElementById('success').style.display='none';
						if(document.getElementById('infoFrame')){
							document.getElementById('infoFrame').style.display='block';
							document.getElementById('infoFrame').src='${path }/uw/cancel.htm?spaj='+spaj;
						} 
					}
				}else if('transfer' == str){ 
					document.getElementById('infoFrame').src='${path }/uw/transfer_to_print.htm?from=payment&spaj='+spaj;
				}else if('permintaanBSB' == str){
					if(confirm('menu ini hanya untuk H+1, Anda yakin untuk memproses?')){
						document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=permintaanBSB&spaj='+spaj;
					}
				}
			}
		}
		
		return true;
	}
	
	function tampilkan(asdf){
		if(document.getElementById('error')) document.getElementById('error').style.display='none';
		if(document.getElementById('success')) document.getElementById('success').style.display='none';
		if(document.getElementById('docFrame')) document.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+asdf;
		if(document.getElementById('infoFrame')){
			document.getElementById('infoFrame').style.display='block';
			if(asdf != ''){
				document.getElementById('infoFrame').src='${path}/uw/inputpayment.htm?tahun_ke=1&premi_ke=1&spaj='+asdf;
				ajaxPesan(asdf, 4);
			}
		}
	}
	
	function awal(){
		if('${snow_spaj}'!=''){
			document.formpost.spaj.value='${snow_spaj}';
			document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value='${snow_spaj}';
			tampilkan('${snow_spaj}');
		}
	}
</script>
<body onload="awal(); setFrameSize('infoFrame', 65); setFocus('spaj');"
	onresize="setFrameSize('infoFrame', 65);" style="height: 100%;">

	<form name="formpost" method="post" action="${path }/uw/payment.htm">
		<div class="tabcontent">
			<table class="entry2" style="width: 98%;">
				<tr>
					<th>
						Cari SPAJ
					</th>
					<td>
						<select name="spaj" onchange="tampilkan(this.options[this.selectedIndex].value);">
							<option value="">[--- Silahkan Pilih/Cari SPAJ ---]</option>
							<c:forEach var="s" items="${daftarSPAJ}">
								<option value="${s.REG_SPAJ }"
									style="background-color: ${s.BG}"
									>
									${s.SPAJ_FORMATTED} - ${s.POLICY_FORMATTED }
								</option>
							</c:forEach>
						</select>
						<input type="hidden" name="_spaj" value="${param.spaj }">
						
						<input type="button" value="Info" name="info" accesskey="I"
							onmouseover="return overlib('Alt-I', AUTOSTATUS, WRAP);"
							onmouseout="nd();"
							onclick="return buttonLinks('show');">
						<input type="button" value="Cari" name="search"
							onclick="return buttonLinks('cari');" accesskey="C"
							onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);"
							onmouseout="nd();">
						<input type="button" value="List RK" name="list_rk"
							onclick="return buttonLinks('list_rk');" accesskey="R"
							onmouseover="return overlib('Alt-R', AUTOSTATUS, WRAP);"
							onmouseout="nd();">
						<input type="button" value="U/W Info" name="uwinfo" accesskey="U"
							onmouseover="return overlib('Alt-U', AUTOSTATUS, WRAP);"
							onmouseout="nd();"
							onclick="return buttonLinks('uwinfo');">
					
							
					</td>
					<td rowspan=2>
						<div id="prePostError" style="display: none;"></div>
						</div>
					</td>
				</tr>
				<tr>
					<th>
						Proses
					</th>
					<td>
						<input type="button" value="Batalkan" name="cancel" id="cancel"
							onclick="return buttonLinks('cancel');" accesskey="B"
							onmouseover="return overlib('Alt-B', AUTOSTATUS, WRAP);"
							onmouseout="nd();">
						<input type="button" value="Payment" name="payment"
							onclick="return buttonLinks('paymentnewbusiness');" accesskey="P"
							onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);"
							onmouseout="nd();">
						<input type="button" value="Payment Top-up" name="topup"
							id="btnPaymentTopup"
							onclick="return buttonLinks('paymenttopup');" accesskey="U"
							onmouseover="return overlib('Alt-U', AUTOSTATUS, WRAP);"
							onmouseout="nd();">
						<input type="button" value="Transfer" name="transfer"
							onclick="return buttonLinks('transfer');" accesskey="F"
							onmouseover="return overlib('Alt-F', AUTOSTATUS, WRAP);"
							onmouseout="nd();">
						<input type="button" value="permintaan BSB" name="permintaanBSB" 
							onclick="return buttonLinks('permintaanBSB');">
						<input type="hidden" name="verifyPassword">
						<c:if test="${sessionScope.currentUser.lus_id eq 495 or sessionScope.currentUser.lus_id eq 4644 or sessionScope.currentUser.lus_id eq 4852 or sessionScope.currentUser.lus_id eq 4990 or sessionScope.currentUser.lus_id eq 6243}">
						<input type="button" value="Auto Payment" name="autopayment" accesskey="A"
							onmouseover="return overlib('Alt-A', AUTOSTATUS, WRAP);"
							onmouseout="nd();"
							onclick="return buttonLinks('auto');">
						<input type="button" value="Auto Payment PA" name="autopayment_pa" accesskey="O"
							onmouseover="return overlib('Alt-O', AUTOSTATUS, WRAP);"
							onmouseout="nd();"
							onclick="return buttonLinks('auto_pa');">
						<input type="button" value="Auto Payment BSM" name="autopayment_bsm" accesskey="M"
							onmouseover="return overlib('Alt-M', AUTOSTATUS, WRAP);"
							onmouseout="nd();"
							onclick="return buttonLinks('auto_bsm');">
						</c:if>	
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<table style="margin: 0 0 0 0; padding: 0 0 0 0; width: 100%;">
							<tr>
								<th width="60%">
								<spring:bind path="cmd.*">
								<c:choose>
								<c:when test="${not empty status.errorMessages}">
									<div id="error">
										ERROR:
										<br>
										<c:forEach var="error" items="${status.errorMessages}">
								- <c:out value="${error}" escapeXml="false" />
											<br />
										</c:forEach>
										<c:forEach var="error" items="${status.errorCodes}">
											<c:choose>
												<c:when
													test="${error eq \"payment.notPaid\" or error eq \"payment.notAuthorized\"}">
						- Harap Masukkan Password :
						<input type="password" id="inpPass" name="password"
														value="">
													<br />- <input type="submit" name="selisihkurs"
														value="Selisih Kurs">
													<input type="submit" name="potongkomisi"
														value="Potong Komisi">
													<input type="button" name="cancel2" value="Cancel"
														onclick="window.location='${path}/uw/payment.htm';">
													<script>
							document.formpost.spaj.disabled='true';
							document.formpost.info.disabled='true';
							document.formpost.search.disabled='true';							
							document.formpost.cancel.disabled='true';							
							document.formpost.payment.disabled='true';							
							document.formpost.topup.disabled='true';							
							document.formpost.transfer.disabled='true';			
							setFocus('inpPass');				
						</script>
												</c:when>
												<c:when test="${error eq \"payment.confirmRK\"}">
													<br>
						Silahkan Pilih Bulan Produksi:
						<input type="submit" name="bulanPrev" value="Bulan Sebelumnya">
													<input type="submit" name="bulanNow" value="Bulan Sekarang">
												</c:when>
											</c:choose>
										</c:forEach>
									</div>
									<iframe name="infoFrame" id="infoFrame" width="100%"
										frameborder="YES" style="display: none;" />
										<br>
								</c:when>
								<c:when test="${not empty submitSuccess}">
									<div id="success">
										<spring:message code="payment.transferSuccess" />
									</div>
									<iframe name="infoFrame" id="infoFrame" width="100%"
										frameborder="YES" style="display: none;" />
								</c:when>
								<c:otherwise>
									<iframe name="infoFrame" id="infoFrame" width="100%"
										frameborder="YES">
										-
									</iframe>
								</c:otherwise>
							</c:choose>
						</spring:bind>
								</th>
								<th width="40%">
									<iframe src="" name="docFrame" id="docFrame" width="100%" height="100%">E-Lions</iframe>
								</th>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>