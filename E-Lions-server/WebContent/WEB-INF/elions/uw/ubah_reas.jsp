<%@ include file="/include/page/header.jsp"%>
<script>
	function awal(){
		formpost.inpass.focus();
		
		var info='${cmd.info}';
		if(info=='1'){
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
			alert("Anda Tidak Mempunyai Akses.");
		}
		
		var succ='${submitSuccess}';
		if(succ=='true'){
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
			alert("Type Reas Telah Di Ubah Menjadi TREATY");
			window.location='${path}/uw/simultanpolis.htm?spaj=${cmd.spaj}';			
		}
			
		
	}
	function proses(){
		if(confirm("Apakah Anda Ingin Mengubah Status Polis dengan SPaj ${cmd.spaj} \n "+
					"FACULTATIVE Menjadi TREATY ?")){
			formpost.submit();		
		}
	}
	
</script>
<body onload="awal();">
<form name="formpost" method="post">
	<table class="entry">
		<tr>
			<th>Silahkan Masukan Password:</th>
			<td><input type="password" name="inpass" value="" ></td>
		</tr>
		<tr>
			<td><input type="button" name="btn_ok"  value="Ok" onclick="proses();" ></td>
		</tr>
		<tr>
			 <td>
				<c:if test="${submitSuccess eq true}">
		        	<div id="success">
			        	Berhasil
		        	</div>
		        </c:if>	
	  			<spring:bind path="cmd.*">
					<c:if test="${not empty status.errorMessages}">
						<div id="error">
							ERROR:<br>
								<c:forEach var="error" items="${status.errorMessages}">
											- <c:out value="${error}" escapeXml="false" />
									<br/>
								</c:forEach>
						</div>
					</c:if>									
				</spring:bind>
			</td>
		</tr>
	</table>
</form>	
</body>
<%@ include file="/include/page/footer.jsp"%>