<%@ include file="/include/page/header.jsp"%>
<script>
	function pencet(){
		popWin('/ReportServer/?new=true&rs=UW%5CAgenTakBerpolis&1kode=003702&1userName=${sessionScope.currentUser.name}', 600, 800); 	
	}
</script>
<body style="height: 100%;"> 
	<c:choose>
		<c:when test="${not empty errorPosisi}">
			<div id="contents">
				<div id="error">Harap Cek Posisi Dokumen untuk SPAJ ${param.spaj}</div>
			</div>
		</c:when>
		<c:when test="${not empty sukses}">
			<div id="contents">
				<div id="success">Polis berhasil di transfer ke Komisi</div>
			</div>
		</c:when>
		<c:otherwise>
			<form name="formpost" method="formpost" style="text-align: center;">
				<fieldset style="text-align: left; height: 100%;">
					<legend>Transfer ke Komisi</legend>
					<input type="hidden" name="window" value="transfer">
					<input type="hidden" name="spaj" value="${param.spaj}">
					<table class="entry">
						<tr>
							<th>Tanggal Kembali: </th>
							<td>
								<script>inputDate('tanggal', '<fmt:formatDate value="${sysdate}" pattern="dd/MM/yyyy"/>', false);</script>
							</td>
						</tr>
						<tr>
							<th></th>
							<td>
								<c:if test="${not empty pepesan}">
									<div id="success">${pepesan}</div>
								</c:if>
								<input type="submit" name="transfer" value="Transfer" onclick="return confirm('Anda yakin untuk transfer ke ${keterangan}');">
								<input type="button" name="info" value="Info Agen tak Berpolis" onclick="pencet();">
							</td>
						</tr>
					</table>
				</fieldset>
			</form>
		</c:otherwise>
	</c:choose>
</body>
<c:if test="${empty errorPosisi and empty sukses}">
	<script>pencet();</script>
</c:if>
<%@ include file="/include/page/footer.jsp"%>