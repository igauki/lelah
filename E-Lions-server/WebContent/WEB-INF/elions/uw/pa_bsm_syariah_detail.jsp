<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html> 
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/mallinsurance1.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<style>
    .button2:hover {
        float: none;
        display: inline;
        padding-right: 6px;
        font-size: 14px;
    }
</style>
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<!-- DatePicker Script (jscalendar) -->
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	/*
	function cariData(){
		var pilter = document.getElementById('pilter').value;
		var tipe = document.getElementById('tipe').value;
		var kata = document.getElementById('kata').value;
		var pdfName = document.getElementById('pdfName').value;
		var url = 'pa_bsm_detail.htm?pilter='+pilter+'&tipe='+tipe+'&kata='+kata+'&pdfName='+pdfName;
		document.getElementById('pdfName').value = '';
		window.open(url, '_self');
		
		//document.getElementById('refreshPage').href = 'pas_detail.htm?pilter='+pilter+'&tipe='+tipe+'&kata='+kata;
		//document.getElementById('refreshPage').click();
	
	}*/
	
	function cariData(){
		var val = document.formpost.kata.value.length;
		
		if(document.getElementById('lahir').checked==1)
		{
			if(trim(document.formpost.tgl_lahir.value)==''){
			 	alert('tanggal Pencarian Harus Diisi!');
				return false;
			}			
		}	
		/* else if(val<4){
				 alert('Kriteria Pencarian Harus Diisi Minimal 4 Huruf!');
				 return false;	 
		} */else{			
			 createLoadingMessage();
		}
	}
	

	function update(msp_id, action){
		document.getElementById('msp_id').value = msp_id;
		document.getElementById('action').value = action;
		if(trim(document.getElementById('action').value)=='') return false;
		else createLoadingMessage();	
	}
	
	function transfer_process(no_reg, msp_id){
		if(confirm('Apakah anda yakin untuk TRANSFER PA ('+no_reg+')')){
			update(msp_id, 'transfer');
			document.getElementById('transfer').click();
		}else{
			document.getElementById('search').click();
		}
	}
	
	function backToParent1(mcl_id, reg_spaj)
	{
			parentForm = self.opener.document.forms['frmParam'];
			if(parentForm) {
				if(parentForm.elements['kopiMclId']){
					parentForm.elements['kopiMclId'].value = mcl_id; 
					parentForm.elements['regSpaj'].value = reg_spaj; 
					parentForm.elements['submitKopiMclId'].click();
					window.close();
				}
			}
			backToParent(mcl_id, reg_spaj);
	}
	
	function openPdf(){
		document.getElementById('search').click();
	
	}
	
	function referesh(){	
		document.getElementById('refresh').click();
	}
	
	
	function viewPdfBsm(no_kartu, msp_id, product_code, product_sub_code) {
	   document.getElementById('pdfName').value = no_kartu;
	   document.getElementById('msp_id').value = msp_id;
	   document.getElementById('product_code').value = product_code;
	   document.getElementById('product_sub_code').value = product_sub_code;
	}
	
	function submitPaBsm(msp_id, no_kartu) {
	   //if(confirm('Submit PA ' + no_kartu + ' ?')) {
	   if(confirm('Data akan diproses ke tahap selanjutnya dan update tidak dapat dilakukan kembali.\nSilahkan cek kembali data anda dan klik tombol yes untuk melanjutkan')) {
	       update(msp_id, 'transfer');
	       document.getElementById('transfer').click();
	   } else {
	       document.getElementById('search').click();
	   }
	}

</script>
</head>
<BODY onload="document.formpost.kata.select(); " style="height: 100%;">
	<div class="tab-container" id="container1">

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" action="#" style="text-align: center;">
						<!-- <input type="button" value="INPUT PA BSM" name="inputPa" id="inputPa" class="button2"
						onclick="popWin('${path}/uw/pa_bsm_input.htm', 350, 450); "
						onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
						onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />		 -->
						<%-- <input type="button" value="INPUT FREE PA BSM" name="inputPa" id="inputPa" class="button2"
						onclick="popWin('${path}/uw/pa_bsm_input.htm?input_type=typeafree', 350, 450); "
						onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
						onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />	 --%>
						<input type="button" value="INPUT PRODUK BSM SYARIAH" name="inputPa" id="inputPa" class="button2"
                        onclick="popWin('${path}/uw/pa_bsm_syariah_input.htm?input_type=default', 350, 450); "
                        onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
                        onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" /> 		
					<a href="pa_bsm_syariah_detail.htm" id="refreshPage" name="refreshPage"></a>
					<input type="button" name="refreshButton" onclick="document.getElementById('search').click();" style="visibility: hidden;"/>
					<input type="submit" name="transfer" id="transfer" value="Transfer" style="visibility: hidden;" onmouseout="nd();"/>
					<input type="hidden" name="action" id="action" value="" />
					<input type="hidden" name="pdfName" id="pdfName"/>
					<input type="hidden" name="popUpInsert" id="popUpInsert" value="${popUpInsert}" />
					<input type="hidden" name="msp_id" id="msp_id" value="" />
					<input type="hidden" name="win" value="${param.win}">
					<input type="hidden" name="cari" value="${param.cari}">
					<input type="hidden" name="posisi" value="${param.posisi}">
					<input type="hidden" name="product_code" id="product_code" value="">
					<input type="hidden" name="product_sub_code" id="product_sub_code" value="">
					<table class="result_table2">
						<tr>
							<th rowspan="2">Cari:</th>
							<td class="left">
								<select name="tipe" id="tipe">
									<option value="1" <c:if test="${param.tipe eq \"1\" }">selected</c:if>>Nama</option>
									<option value="2" <c:if test="${param.tipe eq \"2\" }">selected</c:if>>No.Bukti Identitas</option>
								</select>
								<select name="pilter" id="pilter">
									<option <c:if test="${\"LIKE\" eq param.pilter}">selected</c:if> value="LIKE">LIKE</option>
									<option <c:if test="${\"LT\" eq param.pilter}">selected</c:if> value="LT"><</option>
									<option <c:if test="${\"LE\" eq param.pilter}">selected</c:if> value="LE"><=</option>
									<option <c:if test="${\"EQ\" eq param.pilter}">selected</c:if> value="EQ">=</option>
									<option <c:if test="${\"GE\" eq param.pilter}">selected</c:if> value="GE">>=</option>
									<option <c:if test="${\"GT\" eq param.pilter}">selected</c:if> value="GT">></option>
								</select>					
								<input type="text" name="kata" id="kata" size="34" value="${param.kata }" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
								<br>
								<label for="lahir">
									<input class="noBorder" type="checkbox" name="centang" id="lahir" value="1" <c:if test="${param.centang eq 1}">checked</c:if>>
									Tanggal Lahir<!--  (untuk pencarian berdasarkan Tertanggung) : -->
								</label>
								<script>inputDate('tgl_lahir', "${param.tgl_lahir}", false);</script>									
								<!--<input type="button" name="search" id="search" value="Search" onclick="cariData();" class="button2" 
								onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
								onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />-->
															
								<input type="submit" name="search" value="Search" id="search" onclick="return cariData();"  
									accesskey="S" class="button2" 
								onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
								onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />	
								<input type="button" name="btn_refresh" value="Refresh" id="btn_refresh" onclick="window.open('pa_bsm_syariah_detail.htm', '_self');"  
									accesskey="R" class="button2" 
								onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
								onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />										
								
							</td>
						</tr>
					</table>					
					<table  class="" width="1080" align="center" cellpadding="2" >       				
			        <tr>
			            <td width="573" class="">Page ${currPage} of ${lastPage} </td>
			            <td width="493" align="right" class="">Go
			                <input name="cPage" type="text" style="width:45px" maxlength="4" >
			                <input type="button" value="Go" id="go" style="width: 40px;"  onclick="referesh()"/>
			                <input type="submit" name="refresh" value="refresh" id="refresh" style="visibility: hidden;" /> 
			               <label></label>
			                &nbsp;			           
			                
			                <c:choose>
			        			<c:when test="${currPage eq '1'}">
			        			<font color="gray">First | Prev |</font> 
			        			</c:when>
			        			<c:otherwise>
			               			<a href="#" class="" onclick="document.forms[ 0 ].cPage.value='${firstPage}'; referesh()">First</a> | 
			                		<a href="#" class="" onclick="document.forms[ 0 ].cPage.value='${previousPage}'; referesh()">Prev</a> | 
			                	</c:otherwise>
			                </c:choose>
			                
			                <c:choose>
			        			<c:when test="${currPage eq lastPage}">
			        			<font color="gray">Next | Last </font>
			        			</c:when>
			        			<c:otherwise>
			               			<a href="#" class="" onclick="document.forms[ 0 ].cPage.value='${nextPage}'; referesh()">Next</a> | 
			                		 <a href="#" class="" onclick="document.forms[ 0 ].cPage.value='${lastPage}'; referesh()">Last</a> 
			                	</c:otherwise>
			                </c:choose>                
               			 </td>
      				  </tr>
    				</table>					
					<table class="result_table">
						<thead>
							<tr>
								<th style="text-align: left">Tipe</th>
								<th style="text-align: left">No. Virtual Account</th>
								<th style="text-align: left">Nama<!--  Tertanggung --></th>
								<th style="text-align: left">Tempat</th>
								<th style="text-align: left">Tgl.Lahir</th>
								<th style="text-align: left">No. Identitas</th>
								<th style="text-align: left">Alamat</th>
								<th style="text-align: left">Kota</th>
								<th style="text-align: left">Kode Pos</th>
								<th style="text-align: left">E-Mail</th>
								<th style="text-align: left">No. Reg</th>
								<th style="text-align: left">Detail</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="pas" items="${pasList}" varStatus="stat">
								<tr>
										<td>${pas.input_type}</td>
									
										<td>${pas.no_va}</td>
										<td>${pas.msp_full_name}</td>
										<td>${pas.msp_pas_tmp_lhr_tt}</td>
										<td>${pas.msp_date_of_birth2}</td>
										<td>${pas.msp_identity_no_tt}</td>
										<td width="200px">${pas.msp_address_1}</td>
										<td>${pas.msp_city}</td>
										<td>${pas.msp_postal_code}</td>
										<td>${pas.msp_pas_email}</td>
										<td>${pas.reg_spaj}</td>
										<td width="140px">
										<c:choose>
											<c:when test="${empty pas.no_sertifikat && pas.product_code ne '205'}">
												<%-- <input type="button" value="EDIT" name="inputDetail"
												   onclick="popWin('${path}/uw/pa_bsm_update.htm?posisi=1&win=edit&msp_id='+${pas.msp_id}, 350, 450); "
												   class="button2" 
												   onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
												   onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />											
													<input type="button" value="REFERRAL" name="inputReferral"
												   onclick="popWin('${path}/bac/reff_bank.htm?window=main&reffothers=n&pabsm=y&produk='+${pas.produk}+'&spaj='+${pas.reg_spaj}, 400, 700); "
												   class="button2" 
												   onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
												   onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />											
												<c:if test="${pas.reff_bii_flag != 0}">
													<input type="button" name="transfer_btn" value="TRANSFER" onclick="transfer_process('${pas.reg_spaj}','${pas.msp_id}');"
													class="button2" 
								onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
					onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />
												</c:if> --%>
											</c:when>
											<c:when test="${pas.product_code eq '205'}">
                                                <input type="submit" value="VIEW PDF" name="viewpdf"
                                                 onclick="viewPdfBsm('${pas.reg_spaj}', '${pas.msp_id}', '${pas.product_code}', '${pas.produk}');"
                                                 class="button2"
                                                 onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
                                                 onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />
                                                <c:if test="${empty pas.mspo_policy_no}">
	                                                <input type="button" value="EDIT" name="inputDetail"
	                                                   onclick="popWin('${path}/uw/pa_bsm_syariah_update.htm?posisi=1&win=edit&msp_id='+${pas.msp_id}, 350, 450); "
	                                                   class="button2" 
	                                                   onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
	                                                   onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />
	                                                <input type="button" name="transfer_btn" value="SUBMIT" onclick="submitPaBsm('${pas.msp_id}', '${pas.no_kartu}');this.disabled = true;"
	                                                    class="button2" 
						                                onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
						                                onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />
                                                </c:if>
											</c:when>
											<c:otherwise>
												<input type="submit" value="VIEW PDF" name="viewpdf"
											onclick="document.getElementById('pdfName').value='${pas.no_sertifikat}';"
											class="button2"
											onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
											onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />
											</c:otherwise>
										</c:choose>
											<c:if test="${lde_id eq \'11\'}">
												<input type="button" value="EDIT" name="inputDetail"
												   onclick="popWin('${path}/uw/pa_bsm_syariah_update.htm?posisi=1&win=edit&msp_id='+${pas.msp_id}, 350, 450); "
												   class="button2" 
												   onmouseover="this.style.background= 'url(${path }/include/mallinsurance_images/o_0_0.png) no-repeat scroll top right';" 
												   onmouseout="this.style.background= 'url(${path }/include/mallinsurance_images/r_0_0.png) no-repeat scroll top';" />	
											</c:if>
										</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<br>
				</form>
			</div>
		</div>
	</div>

<c:if  test="${not empty successMessage}">	
			<script type="text/javascript">													
				alert('${successMessage}');	
				document.getElementById('search').click();
			</script>
	</c:if>	
</body>
<%@ include file="/include/page/footer.jsp"%>