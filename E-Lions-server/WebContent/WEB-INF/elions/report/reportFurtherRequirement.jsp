<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen"> <!-- CSS untuk DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script><!-- DatePicker Script (jscalendar) -->
	</head>
	<script>
		function tampil(){
			lca_id=formpost.lca_id.value;
			tgl1=formpost.tanggalAwal.value;
			tgl2=formpost.tanggalAkhir.value;
			window.location='${path }/report/uw.htm?window=list_further_requirement&tanggalAwal='+tgl1+'&tanggalAkhir='+tgl2+'&lca_id='+lca_id;
		}
	</script>
	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Report Futher Requierement</a>
				</li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form id="formpost" method="post">
						<table class="entry2">
							<tr>
								<th width="10%">Tanggal :</th>
								<td width="20%">
									<script>inputDate('tanggalAwal', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>
									 s/d 
									 <script>inputDate('tanggalAkhir', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>
								</td>
								<td align="left" rowSpan="3"><input type="button" name="btnShow" value="SHOW" onClick="tampil();"></th>
							</tr>
							<tr>
								<th>Nama Cabang</th>
								<td>
									<select name="lca_id" >
										<option value="0">---Pilih---</option>
										<c:forEach items="${lsCabang}" var="x" varStatus="xt">
											<option value="${x.key}" 
													<c:if test="${x.key eq lca_id}">selected</c:if> >
													${x.value}
											</option>
										</c:forEach>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<div id="error">
						        		Daftar Cabang yang Memiliki Futher Requirement :<br>
						        			<c:if test="${flagCabang eq 1}">Tidak Ada</c:if>
						        			<c:forEach var="error" items="${lsCabangToday}" >
												- <c:out value="${error.value}" escapeXml="false" />
												<br/>
											</c:forEach>
									</div>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>