<%@ include file="/include/page/header_jquery.jsp"%>
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$(document).ready(function() {
		
		// (jQueryUI Tabs) init tab2 Utama (Pemegang, Tertanggung, dll)
		$("#tabs").tabs();

		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title], label[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			//$(this).removeClass();
			$(this).addClass("ui-widget-header");
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		
		// user memilih cabang, maka tampilkan wakil dan region nya
		$("#cabang").change(function() {
			$("#wakil").empty();
			var url = "${path}/report/bas.htm?window=report_lisensi_agen&json=1&lca=" +$("#cabang").val();
			$.getJSON(url, function(result) {
				$("<option/>").val("ALL").html("ALL").appendTo("#wakil");
				$.each(result, function() {
					$("<option/>").val(this.key).html(this.value).appendTo("#wakil");
				});
				$("#wakil option:first").attr('selected','selected');
			});
			
			$("#region").empty();
			var url2 = "${path}/report/bas.htm?window=report_lisensi_agen&json=2&lca=" + $("#cabang").val();
			$.getJSON(url2, function(result2) {
				$("<option/>").val("ALL").html("ALL").appendTo("#region");
				$.each(result2, function() {
					$("<option/>").val(this.key).html(this.value).appendTo("#region");
				});
				$("#region option:first").attr('selected','selected');
			});
			
			//kopi untuk disubmit (isinya, bukan valuenya)
			$("#cabang2").val($("#cabang option:selected").text());
			$("#wakil2").val("ALL");
			$("#region2").val("ALL");
	
		});

		// user memilih wakil, maka tampilkan region nya
		$("#wakil").change(function() {
			$("#region").empty();
			var url2 = "${path}/report/bas.htm?window=report_lisensi_agen&json=2&lca=" + $("#cabang").val() + "&lwk=" + $("#wakil").val();
			$.getJSON(url2, function(result2) {
				$("<option/>").val("ALL").html("ALL").appendTo("#region");
				$.each(result2, function() {
					$("<option/>").val(this.key).html(this.value).appendTo("#region");
				});
				$("#region option:first").attr('selected','selected');
			});
			
			//kopi untuk disubmit (isinya, bukan valuenya)
			$("#wakil2").val($("#wakil option:selected").text());
			$("#region2").val("ALL");
			
		});
		
		$("#region").click(function() {
			//kopi untuk disubmit (isinya, bukan valuenya)
			$("#region2").val($("#region option:selected").text());
		});	
	});
	
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 15em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 23.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 36em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

	/* untuk tombol2 kecil di report */
	ul#icons {margin: 0; padding: 0;}
	ul#icons li {margin: 0 1px 0 0; position: relative; padding: 0px; cursor: pointer; float: left;  list-style: none;}
	ul#icons span.ui-icon {float: left; margin: 0px;}

</style>

<body>
<form id="formPost" name="formPost" method="post" target="_blank" onSubmit="return formValidation();">
	<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>List Lisensi Agent</div></legend>		
		
		<div class="rowElem" id="dropdown">
			<label>Cabang/Wakil/Region:</label>
			<select size="10" name="cabang" id="cabang" title="Silahkan pilih Cabang">
				<option value="ALL" selected="selected">ALL</option>
				<c:forEach var="c" items="${listCabang}" varStatus="s">
					<option value="${c.key}">${c.value}</option>
				</c:forEach>
			</select>
			<select size="10" name="wakil" id="wakil" title="Silahkan pilih Kantor Perwakilan">
				<option value="ALL" selected="selected">ALL</option>
				<c:forEach var="c" items="${listWakil}" varStatus="s">
					<option value="${c.key}">${c.value}</option>
				</c:forEach>
			</select>
			<select size="10" name="region" id="region" title="Silahkan pilih Region">
				<option value="ALL" selected="selected">ALL</option>
				<c:forEach var="c" items="${listRegion}" varStatus="s">
					<option value="${c.key}">${c.value}</option>
				</c:forEach>
			</select>
		</div>
		
		<div class="rowElem">
			<label>Periode Lisensi :</label>
			<input name="bdate" id="bdate" type="text" class="datepicker" title="Tanggal Awal" value="${bdate}"> s/d 
			<input name="edate" id="edate" type="text" class="datepicker" title="Tanggal Akhir" value="${edate}">
		</div>
		
		<div class="rowElem">
			<label></label>
			<input type="submit" name="showPDF" id="showPDF" value="Show (PDF)">
			<input type="submit" name="showXLS" id="showXLS" value="Show (Excel)">
			<input type="hidden" name="cabang2" id="cabang2" value="ALL">
			<input type="hidden" name="wakil2" id="wakil2" value="ALL">
			<input type="hidden" name="region2" id="region2" value="ALL">
			<input type="hidden" name="showReport" value="1">
		</div>
	</fieldset>
</form>
</body>
</html>