<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type"
			content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<meta HTTP-EQUIV="Expires" CONTENT="-1">
		<link REL="Stylesheet" TYPE="text/css"
			HREF="${path }/include/css/default.css" media="screen">
		<link href="${path}/include/image/eas.ico" rel="shortcut icon">
		<link REL="Stylesheet" TYPE="text/css"
			HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css"
			media="screen">
		<!-- CSS untuk DatePicker Script (jscalendar) -->
		<script type="text/javascript"
			src="${path }/include/js/jscalendar/calendar.js"></script>
		<!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript"
			src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript"
			src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/default.js"></script>
		<!-- Common Javascripts -->
		<script type="text/javascript" src="${path }/dwr/util.js"></script>
		<!-- DWR (Ajax) Utils -->
		<script type="text/javascript"
			src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript">
		
	hideLoadingMessage();	
	function pesan(pepesan){
		if(pepesan != ''){
			alert(pepesan);
		}
	}
	function centangUcup(cekbok){
		var ukuran = ${cmd.ukuran};
		for(i=0; i<ukuran; i++){
			document.getElementById('ucup'+i).checked = cekbok.checked;
		}
	}
	
		function centangAllSpt(cekbok){
		var ukuran = ${cmd.ukuran};
		for(i=0; i<ukuran; i++){
			document.getElementById('checkBoxSpt'+i).checked = cekbok.checked;
		}
	}
	
	function info(value) {
		//alert(value);
		//alert(value.substring(0,1));
		//alert(value.substring(1,3));
		var x = value.substring(0,1);
		var y = value.substring(1,5);
		if(x == '1') {
			document.getElementById('keterangan'.concat(y)).style.visibility = "hidden";
			document.getElementById('keterangantambah'.concat(y)).style.visibility = "visible";
		}
		else {
			document.getElementById('keterangan'.concat(y)).style.visibility = "visible";
			document.getElementById('keterangantambah'.concat(y)).style.visibility = "hidden";
		}	
	}
	
	function fungsi(nilai){
	
	var x = document.getElementById('keterangan'.concat(nilai)).value;
		//if(x ==9 || x==13 || x ==21){
		if(x == 'Lain-lain'){
			document.getElementById('keterangantambah'.concat(nilai)).style.visibility = "visible";
		}else {
			document.getElementById('keterangantambah'.concat(nilai)).style.visibility = "hidden";
		}
	
	}
	
</script>
<script type="text/javascript">
	function submitForm(filename){
		var tampung="${cmd.tampung}";
		popWin('${path}/report/nb.htm?window=list_polis_main&download=1&filename='+filename+'&tampung='+tampung, 600, 800);
	}
</script>
	</head>
	<body style="height: 100%;"
		onload="setupPanes('container1', 'tab1'); pesan('${cmd.pesan}'); setFrameSize('reportFrame', 78); setFrameSize('docFrame', 78);"
		onresize="setFrameSize('reportFrame', 78); setFrameSize('docFrame', 78);">
		<div id="contents">
			<form method="post" name="formpost">
			<input type="hidden" name ="tampung" value="${cmd.tampung}">
				<div class="tab-container" id="container1">
					<ul class="tabs">
						<li>
							<a href="#" onClick="return showPane('pane1', this)" id="tab1">List
								Pengiriman Filing SPH / SPT</a>
						</li>
						<li>
							<a href="#" onClick="return showPane('pane2', this)">Report</a>
						</li>
						<li>
							<a href="#" onClick="return showPane('pane3', this)">History</a>
						</li>
					</ul>
					<div class="tab-panes">
						<div id="pane1" class="panes">
							<table class="entry2">
								<tr>
									<th>
										User
									</th>
									<td>
										<input type="text" value="${sessionScope.currentUser.name}"
											class="readOnly" readonly="readonly">
									</td>
								</tr>
								<tr>
									<th>
										Tanggal Akseptasi
									</th>
									<td>
										<script>inputDate('startDate', '${cmd.startDate}', false);</script>
										&nbsp;s/d&nbsp;
										<script>inputDate('endDate', '${cmd.endDate}', false);</script>
									<span class="info">* Format Tanggal (DD/MM/YYYY)</span>
									</td>
									
								</tr>
								<tr>
									<th>
										Kondisi
									</th>
									<td>
										<label for="kondisi0">
											<input type="radio" name="kondisi" value="0" id="kondisi0"
												class="noBorder"
												<c:if test="${cmd.kondisi eq 0}">checked</c:if>>
											Pending Kirim
										</label>
										<br />
										<label for="kondisi1">
											<input type="radio" name="kondisi" value="1" id="kondisi1"
												class="noBorder"
												<c:if test="${cmd.kondisi eq 1}">checked</c:if>>
											SPH/SPT Siap Kirim ke LB
										</label>
										<br />
										<label for="kondisi2">
											<input type="radio" name="kondisi" value="2" id="kondisi2"
												class="noBorder"
												<c:if test="${cmd.kondisi eq 2}">checked</c:if>>
											Pending Aksep LB
										</label>
										<br />
										<label for="kondisi3">
											<input type="radio" name="kondisi" value="3" id="kondisi3"
												class="noBorder"
												<c:if test="${cmd.kondisi eq 3}">checked</c:if>>
											Sudah Diaksep LB
										</label>
										<br />
										<label for="kondisi4">
											<input type="radio" name="kondisi" value="4" id="kondisi4"
												class="noBorder"
												<c:if test="${cmd.kondisi eq 4}">checked</c:if>>
											ALL
										</label>
									</td>
								</tr>
								<tr>
									<th>
										SPAJ Tambahan
									</th>
									<td>
										<textarea rows="3" cols="50" name="tambahan">${cmd.tambahan}</textarea>
										<br />
										<span class="info">* masukkan nomor spaj atau nomor
											polis, dipisahkan dengan tanda koma (,)</span>
									</td>
								</tr>
								<tr>
									<th></th>
									<td>
										<input type="submit" name="show" value="Show">
									</td>
								</tr>
								<c:choose>
									<c:when test="${sessionScope.currentUser.lde_id eq 27}">
										<c:if test="${not empty cmd.daftar}">
											<tr>
												<td colspan="2">
													<c:set var="prod" value="" />
													<table class="displaytag">
														<thead>
															<tr>
																<th>
																	
																</th>
																<th>
																	
																</th>
																<th>
																	No. SPAJ
																</th>
																<th>
																	No. Polis
																</th>
																<th>
																	Pemegang Polis
																</th>
																<th>
																	Efektif Polis
																</th>
																<th>
																	Tgl Kirim
																</th>
																<th>
																	Tgl Transfer TTP
																</th>
																<th>
																	Tgl Proses Komisi
																</th>
																<th>
																	S/H
																</th>
																<th>
																	Kirim LB
																</th>
																<th>
																	Terima LB
																</th>
																<th>
																	Keterangan
																</th>
															</tr>
														</thead>
														<tbody >
															<c:forEach items="${cmd.daftar}" var="d" varStatus="st">
																<c:if test="${d.lsdbs_name ne prod}">
																	<c:set var="prod" value="${d.lsdbs_name}" />
																	<tr>
																		<td style="text-align: left; font-weight: bold;"
																			colspan="11">
																			${prod}
																		</td>
																	</tr>
																</c:if>
																<tr>
																	<td>
																		<label for="valid${st.index}">
																			<input type="radio" name="valid${st.index}" value="1" id="valid${st.index}"
																				class="noBorder" onclick="info('1'.concat(${st.index}))"
																			<c:if test="${cmd.valid eq 1}">checked</c:if>>
																			Terima
																		</label>
																	</td>
																	<td>
																		<label for="valid${st.index}">
																			<input type="radio" name="valid${st.index}" value="2" id="valid${st.index}"
																				class="noBorder" onclick="info('2'.concat(${st.index}))"
																			<c:if test="${cmd.valid eq 2}">checked</c:if>>
																			Tolak
																		</label>
																	</td>
																	<td>
																		<input type="hidden" name="lsdbs_name" value="${d.lsdbs_name}">
																		<input type="hidden" name ="mste_tgl_aksep" value="<fmt:formatDate value="${d.mste_tgl_aksep}" pattern="dd/MM/yyyy"/>">
																		<input type="text" class="readOnly" readonly="readonly"
																			name="reg_spaj" value="${d.reg_spaj}">
																	</td>
																	<td style="text-align: center;">
																	<input type="hidden" name="mspo_policy_no_format" value="${d.mspo_policy_no_format}">
																		${d.mspo_policy_no_format}
																	</td>
																	<td style="text-align: left;">
																		${d.mcl_first}
																	</td>
																	<td>
																		<fmt:formatDate value="${d.mste_beg_date}"
																			pattern="dd/MM/yyyy" />
																	</td>
																	<td>
																		<fmt:formatDate value="${d.mste_tgl_kirim_polis}"
																			pattern="dd/MM/yyyy (HH:mm)" />
																	</td>
																	<td>
																		<fmt:formatDate value="${d.msps_date}"
																			pattern="dd/MM/yyyy" />
																	</td>
																	<td>
																		<fmt:formatDate value="${d.mste_tgl_komisi}"
																			pattern="dd/MM/yyyy (HH:mm)" />
																	</td>
																	<td>
																		${d.softcopy}
																	</td>
																	<td>
																		<input type="hidden" name="mste_tgl_kirim_lb" value="<fmt:formatDate value="${d.mste_tgl_kirim_lb}" pattern="dd/MM/yyyy"/>">
																		<fmt:formatDate value="${d.mste_tgl_kirim_lb}"
																			pattern="dd/MM/yyyy (HH:mm)" />
																	</td>
																	<td>
																		<fmt:formatDate value="${d.mste_tgl_terima_lb}"
																			pattern="dd/MM/yyyy (HH:mm)" />
																	</td>
																	<td>
																	
																	<select id="keterangan${st.index}" name="keterangan${st.index}" onchange ="fungsi(${st.index});" style="visibility: hidden;">
																		<option value=""></option>
																		<c:forEach items="${lsTolak}" var="x" varStatus="xt">
																			<option value="${x.KETERANGAN}">${x.KETERANGAN}</option>
																		</c:forEach>
																	</select>
																	<div align="left"><input size="100" type="text" name="keterangantambah${st.index}" id="keterangantambah${st.index}" style="visibility: hidden;">
																	</div>
																	
																		<!-- <option value="0">---Pilih---</option>
																			<c:forEach items="${lsTolak}" var="x" varStatus="xt">
																				<option value="${x.KEY}">
																						${x.VALUE}
																				</option>
																			</c:forEach>
																		 -->	
																	</td>
																</tr>
															</c:forEach>	
														</tbody>
													</table>
													<span class="info" >* Keterangan HARUS DIISI(TIDAK BOLEH KOSONG!) bila kondisi Tolak yang dipilih </span>
													<br />
													<input type="submit" name="save" value="Save"
													onclick="return confirm('Simpan Perubahan dan Tampilkan Report?');">
													<input type="submit" name="show_report" value="Show Report"
													onclick="return confirm('Tampilkan Report?');">
												</td>
											</tr>
										</c:if>
									</c:when>
									<c:otherwise>
										<c:if test="${not empty cmd.daftar}">
											<tr>
												<td colspan="2">
													<c:set var="prod" value="" />
													<table class="displaytag">
														<thead>
															<tr>
																<th> SPH
																	<input class="noBorder" type="checkbox"
																	onclick="centangUcup(this)"> 
																</th>
																<th> SPT
																	<input class="noBorder" type="checkbox"
																	onclick="centangAllSpt(this)">
																</th>
																<th>
																	No. SPAJ
																</th>
																<th>
																	No. Polis
																</th>
																<th>
																	Pemegang Polis
																</th>
																<th>
																	Efektif Polis
																</th>
																<th>
																	Tgl Kirim
																</th>
																<th>
																	Tgl Transfer TTP
																</th>
																<th>
																	Tgl Proses Komisi
																</th>
																<th>
																	S/H
																</th>
																<th>
																	Kirim LB
																</th>
																<th>
																	Terima LB
																</th>
																<th>
																	Status
																</th>
															</tr>
														</thead>
														<tbody >
															<c:forEach items="${cmd.daftar}" var="d" varStatus="st">
																<c:if test="${d.lsdbs_name ne prod}">
																	<c:set var="prod" value="${d.lsdbs_name}" />
																	<tr>
																		<td style="text-align: left; font-weight: bold;"
																			colspan="11">
																			${prod}
																		</td>
																	</tr>
																</c:if>
																<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;">
																	<c:if test="${d.lssa_id eq 5}">
																		<td>
																			<input class="noBorder" type="checkbox" 
																				name="ucup${st.index}" value="1" id="ucup${st.index}">
																		</td>
																		<td>
																		<input class="noBorder" type="checkbox" 
																				name="checkBoxSpt${st.index}" value="1" id="checkBoxSpt${st.index}">
																		</td>
																	</c:if>
																	<c:if test="${d.lssa_id ne 5}">
																		<td>
																			<input class="noBorder" disabled="disabled" type="checkbox" readonly="readonly"
																				name="ucup${st.index}" value="1" id="ucup${st.index}">
																		</td>
																		<td>
																		<input class="noBorder" type="checkbox" 
																				name="checkBoxSpt${st.index}" value="1" id="checkBoxSpt${st.index}">
																		</td>
																	</c:if>
																	<td>
																		<input type="text" class="readOnly" readonly="readonly"
																			name="reg_spaj" value="${d.reg_spaj}">
																	</td>
																	<td style="text-align: center;">
																		${d.mspo_policy_no_format}
																	</td>
																	<td style="text-align: left;">
																		${d.mcl_first}
																	</td>
																	<td>
																		<fmt:formatDate value="${d.mste_beg_date}"
																			pattern="dd/MM/yyyy" />
																	</td>
																	<td>
																		<fmt:formatDate value="${d.mste_tgl_kirim_polis}"
																			pattern="dd/MM/yyyy (HH:mm)" />
																	</td>
																	<td>
																		<fmt:formatDate value="${d.msps_date}"
																			pattern="dd/MM/yyyy" />
																	</td>
																	<td>
																		<fmt:formatDate value="${d.mste_tgl_komisi}"
																			pattern="dd/MM/yyyy (HH:mm)" />
																	</td>
																	<td>
																		${d.softcopy}
																	</td>
																	<td>
																		<fmt:formatDate value="${d.mste_tgl_kirim_lb}"
																			pattern="dd/MM/yyyy (HH:mm)" />
																	</td>
																	<td>
																		<fmt:formatDate value="${d.mste_tgl_terima_lb}"
																			pattern="dd/MM/yyyy (HH:mm)" />
																	</td>
																	<td>
																		${d.status_accept}
																	</td>
																</tr>
															</c:forEach>
														</tbody>
													</table>
												<input type="submit" name="save" value="Save"
													onclick="return confirm('Simpan Perubahan dan Tampilkan Report?');">
												<input type="submit" name="show_report" value="Show Report"
													onclick="return confirm('Tampilkan Report?');">
												</td>
											</tr>
										</c:if>
									</c:otherwise>
								</c:choose>
							</table>
						</div>
						<div id="pane2" class="panes">
							<iframe name="reportFrame" id="reportFrame" width="100%"
								src="${path}/${cmd.reportPath}">
								Please Wait...
							</iframe>
						</div>
						<div id="pane3" class="panes">
							<table class="entry2">
								<tr>
									<th>
										Tanggal Kirim
									</th>
									<td>
										<script>inputDate('hsStartDate', '${cmd.hsStartDate}', false);</script>
										&nbsp;s/d&nbsp;
										<script>inputDate('hsEndDate', '${cmd.hsEndDate}', false);</script>
										<span class="info">* Format Tanggal (DD/MM/YYYY)</span>
									</td>
								</tr>
								<tr>
									<th>
										<input type="submit" name="save_report_history" value="Save Report History">
										<input type="submit" name="viewer_report_history" value="Viewer Report History">
									</th>
								</tr>
								<tr>
									<td>
										<c:if test="${not empty cmd.daftarFile}">
											<table class="displaytag" style="width: auto;">
												<tr>
													<th>
														Dokumen
													</th>
													<th>
														Tanggal
													</th>
													<th></th>
												</tr>
													<c:forEach var="dok" items="${cmd.daftarFile}">
														<tr>
															<td style="text-align: left;">
																${dok.key}
															</td>
															<td style="text-align: center;">
																${dok.value}
															</td>
															
															
															<td>
																<input type="button" name="download" value="Show Dokumen" onclick="submitForm('${dok.key}')"> 
		
		
															</td>
														</tr>
													</c:forEach>
											</table>
										</c:if>
									</td>
								</tr>
							</table>
   							<!-- <iframe style="width: 950px; height: 375px;" src=""  id="frame" >
			 				</iframe> -->
						</div>
					</div>
				</div>
			</form>
		</div>
	</body>
	<%@ include file="/include/page/footer.jsp"%>
