<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title], a[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		
		$(".checkall").click(function () {
			tot = $(this).parent().find(":hidden");
			if(tot.val() == "true") tot.val(""); else tot.val("true");
			$(this).parent().parent().find(":checkbox").attr("checked", tot.val());
			return false;
		});
			
	});
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	table tr td label { display: inline-block; width: 30em;}
	
	/* styling untuk table th */
	table tr th { padding: 1em;}
	
</style>

<body>

<form id="formPost" name="formPost" method="post" target="_blank">
<fieldset class="ui-widget ui-widget-content">
	<legend class="ui-widget-header ui-corner-all"><div>${judul}</div></legend>

	<table>
		<tr style="background-color: #F5F5F5;">
			<th colspan="2"><a href="#" class="checkall" title="Check All">Usia:</a><input type="hidden" value="true"></th>
			<td colspan="2">
				<c:forEach items="${listUmur}" var="l" varStatus="s">
					<label for="umur${s.index}"><input type="checkbox" id="umur${s.index}" name="umur${s.index}" value="${l.key}" checked="true">${l.value}</label>
				</c:forEach>
			</td>
		</tr>
		<tr>
			<th colspan="2"><a href="#" class="checkall" title="Check All">Jenis Kelamin:</a><input type="hidden" value="true"></th>
			<td colspan="2">
				<c:forEach items="${listSex}" var="l" varStatus="s">
					<label for="sex${s.index}"><input type="checkbox" id="sex${s.index}" name="sex${s.index}" value="${l.key}" checked="true">${l.value}</label>
				</c:forEach>
			</td>
		</tr>
		<tr style="background-color: #F5F5F5;">
			<th colspan="2"><a href="#" class="checkall" title="Check All">Status Perkawinan:</a><input type="hidden" value="true"></th>
			<td colspan="2">
				<c:forEach items="${listMarital}" var="l" varStatus="s">
					<label for="marital${s.index}"><input type="checkbox" id="marital${s.index}" name="marital${s.index}" value="${l.key}" checked="true">${l.value}</label>
				</c:forEach>			
			</td>
		</tr>
		<tr>
			<th colspan="2"><a href="#" class="checkall" title="Check All">Agama:</a><input type="hidden" value="true"></th>
			<td colspan="2">
				<c:forEach items="${listAgama}" var="l" varStatus="s">
					<label for="agama${s.index}"><input type="checkbox" id="agama${s.index}" name="agama${s.index}" value="${l.key}" checked="true">${l.value}</label>
				</c:forEach>			
			</td>
		</tr>
		<%--
		<tr>
			<th colspan="2"><a href="#" class="checkall" title="Check All">Pekerjaan:</a><input type="hidden" value="true"></th>
			<td colspan="2">
			</td>
		</tr>
		--%>
		<tr style="background-color: #F5F5F5;">
			<th colspan="2"><a href="#" class="checkall" title="Check All">Penghasilan per Tahun:</a><input type="hidden" value="true"></th>
			<td colspan="2">
				<c:forEach items="${listPenghasilan}" var="l" varStatus="s">
					<label for="penghasilan${s.index}"><input type="checkbox" id="penghasilan${s.index}" name="penghasilan${s.index}" value="${l.key}" checked="true">${l.value}</label>
				</c:forEach>			
			</td>
		</tr>
		<c:if test="${listWilayah ne null }">
		<tr>
			<th colspan="2"><a href="#" class="checkall" title="Check All">Wilayah:</a><input type="hidden" value="true"></th>
			<td colspan="2">
				<c:forEach items="${listWilayah}" var="l" varStatus="s">
					<label for="wilayah${s.index}"><input type="checkbox" id="wilayah${s.index}" name="wilayah${s.index}" value="${l.key}" checked="true">${l.value}</label>
				</c:forEach>			
			</td>
		</tr>
		</c:if>
		<tr style="background-color: #F5F5F5;">
			<th rowspan="4">Produk:</th>
			<th><a href="#" class="checkall" title="Check All">Outstanding / Nilai Polis:</a><input type="hidden" value="true"></th>
			<td>
				<c:forEach items="${listOutstanding}" var="l" varStatus="s">
					<label for="outstanding${s.index}"><input type="checkbox" id="outstanding${s.index}" name="outstanding${s.index}" value="${l.key}" checked="true">${l.value}</label>
				</c:forEach>			
			</td>
		</tr>
		<tr>
			<th><a href="#" class="checkall" title="Check All">Tenor / Cara Bayar:</a><input type="hidden" value="true"></th>
			<td>
				<c:forEach items="${listMGI}" var="l" varStatus="s">
					<label for="mgi${s.index}"><input type="checkbox" id="mgi${s.index}" name="mgi${s.index}" value="${l.key}" checked="true">${l.value}</label>
				</c:forEach>			
			</td>
		</tr>
		<tr style="background-color: #F5F5F5;">
			<th><a href="#" class="checkall" title="Check All">Nama Produk:</a><input type="hidden" value="true"></th>
			<td>
				<c:forEach items="${listProduct}" var="l" varStatus="s">
					<label for="product${s.index}"><input type="checkbox" id="product${s.index}" name="product${s.index}" value="${l.key}" checked="true">${l.value}</label>
				</c:forEach>			
			</td>
		</tr>
		<tr>
			<th><a href="#" class="checkall" title="Check All">Mata Uang:</a><input type="hidden" value="true"></th>
			<td>
				<c:forEach items="${listKurs}" var="l" varStatus="s">
					<label for="kurs${s.index}"><input type="checkbox" id="kurs${s.index}" name="kurs${s.index}" value="${l.key}" checked="true">${l.value}</label>
				</c:forEach>			
			</td>
		</tr>
		<%--
		<tr style="background-color: #F5F5F5;">
			<th>Periode:</th>
			<td colspan="2">
				<input name="bdate" id="bdate" type="text" class="datepicker" title="Tanggal Awal" value="${today}"> s/d 
				<input name="edate" id="edate" type="text" class="datepicker" title="Tanggal Akhir" value="${today}">
			</td>
		</tr>		
		--%>
	</table>

	<div class="rowElem">
		<input type="submit" name="showPDF" id="showPDF" value="Show (PDF)">
		<input type="submit" name="showXLS" id="showXLS" value="Show (Excel)">
		<input type="hidden" name="showReport" value="1">
	</div>
</fieldset>
</form>

</body>
</html>