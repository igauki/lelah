<%@ include file="/include/page/header.jsp"%>
<script>
function tampil(){
	setFrameSize('infoFrame', 45);
	var awal = document.formpost.tglAwal.value;
	var akhir = document.formpost.tglAkhir.value;
	var cabang=document.formpost.cabang.value;
	
	if(cabang=='00'){
		document.getElementById('infoFrame').src='/ReportServer/?new=true&rs=UW/SummaryPrintPolis&4awal='+awal+'&4akhir='+akhir;
	}else{
		document.getElementById('infoFrame').src='/ReportServer/?new=true&rs=UW/SummaryPrintPolisCb&4awal='+awal+'&4akhir='+akhir+'&3cabang='+cabang;
	}
}

</script>
<body onload="document.title='PopUp :: Alamat Penagihan';" style="height: 100%;">
<form method="post" name="formpost">
  <div id="contents"> 
  <fieldset>
  <legend>Report Summary Print Polis</legend> 
    <table class="entry">
      <tr> 
        <th>Periode</th>
        <td>
			<script>inputDate('tglAwal', '${sysdate}', false);</script>
			s/d
			<script>inputDate('tglAkhir', '${sysdate}', false);</script>

		    <input type="button" name="show" value="Show" onclick="tampil();">

        </td>        
      </tr>
      <tr>
      	<th>Cabang</th>
      	<td>	
      		<select name="cabang">
      			<option value="00" >All</option>
				<c:forEach var="s" items="${cabang}">
					<option value="${s.LCA_ID }" >
					${s.LCA_NAMA }
					</option>
				</c:forEach>
			</select>
      	</td>
      </tr>
    </table>
    	
    <table class="entry" width="98%">
	    <tr>
	    	<td>
		    <iframe name="infoFrame" id="infoFrame"
						width="100%"  > Please Wait... </iframe>
		    </td>
	    </tr>
    </table>
  </div>  
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>
