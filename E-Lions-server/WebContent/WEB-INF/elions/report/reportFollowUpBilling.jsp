<%@ include file="/include/page/header_jquery.jsp"%>

<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}    
        
	});
	
	function load(){
		document.getElementById('kirim').disabled = true;
	}
	
	function kirimkan(){
		var to = document.getElementById('tujuan').value;
		var cc = document.getElementById('tujuan_cc').value;
	
		if(confirm("Apakah anda yakin akan mengirim e-mail Report Follow Up Billing ke Channel?"+"\nTO: "+to+"\nCC: "+cc)){
			return true;
		}else{
			return false;
		}
// 			window.location='${path}/report/bas.htm?window=report_fu_bas&tgl1='+tgl1+'&tgl2='+tgl2+'&prod='+prod+'&tipe='+tipe+'&status='+status;
	}
	
	function summ(){
		var status = document.getElementById('status').value;
		if (status != 1){
			alert("Status polis harus 'ALL'");
			return false;
		}
		
		if(!document.getElementById('tipe3').checked){
			alert("Harus memilih pilihan 'Hasil Follow Up BAS'");
			return false;
		}else{
			return true;
		}
	}
		
	function cekdl(val){
		if(val==3){
			document.getElementById('kirim').disabled = false;
		}else{
			document.getElementById('kirim').disabled = true;
		}
	}
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

</style>

<body onload="load()">
	<form id="formPost" name="formPost" method="post" target="_blank" >
	<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all" style="padding:5px;">Report Follow Up Billing</legend>
		
		<div class="rowElem">
			<label>Periode :</label>
			<input name="bdate" id="bdate" type="text" class="datepicker" title="Tanggal Awal" value="${bdate}"> s/d 
			<input name="edate" id="edate" type="text" class="datepicker" title="Tanggal Akhir" value="${edate}">
		</div>
		
		<div class="rowElem">
			<label>Produk :</label>
			<select name="prod" id="prod">
				<option value="3">All</option>
				<option value="1">Smile Prioritas</option>
				<option value="2">Smile Non Prioritas</option>
			</select>
		</div>
		
		<div class="rowElem">
			<label></label>
			<input type="radio" name="tipe" id="tipe1" value="1" onclick="cekdl(1);" checked >Follow Up
			<input type="radio" name="tipe" id="tipe2" value="2" onclick="cekdl(2);">Outstanding
			<input type="radio" name="tipe" id="tipe3" value="3" onclick="cekdl(3);">Hasil Follow Up BAS
		</div>
		
		<div class="rowElem">
			<label>Status Polis :</label>
			<select name="status" id="status">
				<option value="1">All</option>
				<option value="2">Inforce</option>
				<option value="3">Lapse</option>
			</select>
		</div>
		
		<div class="rowElem">
			<label></label>
			<input type="submit" name="showPDF" id="showPDF" value="Retrieve" >
			<input type="submit" name="showXLS" id="showXLS" value="Show (Excel)" >
			<input type="submit" name="kirim" id="kirim" value="Kirim Email ke Channel" onclick="return kirimkan();" >
			<input type="submit" name="summary" id="summary" value="Summary"  onclick="return summ();">
			<input type="hidden" name="showReport" value="1">
			<input type="hidden" name="prod_text" id="prod_text" >
			<input type="hidden" id="hsl" value="${err}">
			<input type="hidden" id="tujuan" value="${tujuan}">
			<input type="hidden" id="tujuan_cc" value="${tujuan_cc}">
		</div>
	</fieldset>

	</form>

</body>
</html>