<%@ include file="/include/page/header.jsp"%>
<style type="text/css">
	html,body{
	     overflow: hidden;
	}
</style>
<script>
	function tampil(){
		var cab_bank 		= document.formpost.cab_bank.value;
		var jenis_report 	= document.formpost.jenis_report.value;
		var startDate 		= document.formpost.startDate.value;
		var endDate 		= document.formpost.endDate.value;
		popWin('${path}/report/bancass.pdf?window=kanwil_bsm_report' +
			'&cab_bank=' 		+ cab_bank + 
			'&path=' 			+ jenis_report + 
			'&tanggalAwal=' 	+ startDate + 
			'&tanggalAkhir=' 	+ endDate
		, 550, 875);
	}
</script>
<body style="height: 100%;" onload="setupPanes('container1', 'tab1'); ">

	<div class="tab-container" id="container1">
		
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Report Kanwil BSM</a>
			</li>
		</ul>

		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form method="post" name="formpost">
					<table class="entry2">
						<tr>
							<th style="width: 150px;">Cabang BSM</th>
							<td>
								<select name="cab_bank">
									<c:set var="tmp" value="" />
									<c:forEach items="${cmd.daftarCabang}" var="c">
										<c:if test="${tmp ne c.NAMA_HEAD}">
											<c:set var="tmp" value="${c.NAMA_HEAD}" />
											<optgroup label="${tmp}">
										</c:if>
										<option value="${c.LCB_NO}">${c.NAMA_CABANG} [${c.KODE_CAB}]</option>
									</c:forEach>
								</select>
							</td>
						</tr>
						<tr>
							<th style="width: 150px;">Jenis Report</th>
							<td>
								<select name="jenis_report">
									<c:set var="tmp" value="" />
									<c:forEach items="${cmd.daftarReport}" var="c">
										<c:if test="${tmp ne c.key}">
											<c:set var="tmp" value="${c.key}" />
											<optgroup label="${tmp}">
										</c:if>
										<option value="${c.value}">${c.desc}</option>
									</c:forEach>
								</select>
							</td>
						</tr>
						<tr>
							<th>Periode</th>
							<td>
								<script>inputDate('startDate', '${cmd.sysDate}', false);</script> s/d 
								<script>inputDate('endDate', '${cmd.sysDate}', false);</script>
							</td>
						</tr>
						<tr>
							<th></th>
							<td>
								<input type="button" name="show" value="Show" onclick="tampil();">
							</td>
						</tr>
					</table>
				</form>			
			</div>
		</div>
		
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>