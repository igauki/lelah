<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		
		$("#msag_id").change(function() {
			$("#nm_agent").empty();
			var url2 = "${path}/report/bas.htm?window=komisi_agen_pending&json=1&msag_id=" + $("#msag_id").val();
			$.getJSON(url2, function(result2) {
				$("#nm_agent").val(result2.mcl_first);
			});
			
		});
		
		var pesan = '${pesan}';
		if(pesan!=null && pesan!=''){
			alert(pesan);
		}
        
	});
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

</style>

<body>

<form id="formPost" name="formPost" method="post" enctype="multipart/form-data">
<fieldset class="ui-widget ui-widget-content">
	<legend class="ui-widget-header ui-corner-all"><div>Pendingan Komisi Agent</div></legend>

	<div class="rowElem">
		<label>Kode Agent :</label>
		<input name="msag_id" id="msag_id" type="text" title="Kode Agent" value="${msag_id }" />
		Nama Agent :
		<input name="nm_agent" id="nm_agent" size="50" class="readOnly" readonly="readonly" type="text" title="Nama Agent" value="${nm_agent }">
	</div>
	
	<div class="rowElem">
		<label>No Polis :</label>
		<input name="polis" id="polis" type="text" title="Kode Agent" value="${polis }" />
	</div>
	
	<div class="rowElem">
		<label></label>
		<input type="submit" name="btnView" id="btnView" value="View"/>
	</div>
	
	<c:if test="${not empty ls_komisi }">
	<div class="rowElem">
		<table class="entry">
			<tr>
				<th>No</th>
				<th>No Polis</th>
				<th>Nama Pemegang</th>
				<th>Jumlah Komisi</th>
				<th>Tahun Ke</th>
				<th>Premi Ke</th>
				<th>Kode Agent</th>
				<th>Nama Agent</th>
				<th>Status Aktif</th>
				<th>Status Sertifikat</th>
				<th>Tabungan</th>
				<th>Status Bill</th>
				<th>Akseptasi Khusus</th>
			</tr>
			<c:forEach items="${ls_komisi }" var="k" varStatus="s">
			<tr>
				<td>${s.index +1 }.</td>
				<td>${k.MSPO_POLICY_NO }</td>
				<td>${k.PEMEGANG }</td>
				<td>${k.JUMLAH_KOMISI }</td>
				<td>${k.MSBI_TAHUN_KE }</td>
				<td>${k.MSBI_PREMI_KE }</td>
				<td>${k.AGENT_PENUTUP }</td>
				<td>${k.NAMA_AGENT }</td>
				<td>${k.STS_AKTIF }</td>
				<td>${k.STS_SERTIFIKASI }</td>
				<td>${k.BANK }</td>
				<td>${k.STAT_BILL }</td>
				<td>${k.AKSEPTASI_KHUSUS }</td>
			</tr>
			</c:forEach>
		</table>
	</div>
	
	<br>
	<div class="rowElem">
		<label>Alasan Pending :</label>
		<select id="selPending" name="selPending">
			<option value="1">Agent Expired / Belum Lisensi</option>
			<option value="2">Belum ada tabungan Sinarmas / Belum punya tutupan polis</option>
			<option value="3">TTP belum diterima</option>
		</select>
	</div>
	
	<div class="rowElem">
		<label>Keterangan :</label>
		<textarea id="ket" name="ket"></textarea>
	</div>
	
	<div class="rowElem">
		<label>Upload Dokumen :</label>
		<input type="file" name="file1" id="file1"/>
	</div>
	
	<div class="rowElem">
		<label></label>
		<input type="submit" name="btnKirim" id="btnKirim" value="Kirim"/>
	</div>
	</c:if>
</fieldset>
</form>

</body>
</html>