<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<html>
<head>
	<jsp:include page="/include/page/header_jquery.jsp">
		<jsp:param value="${path}" name="path"/>
	</jsp:include>
</head>
  
<body>
<form method="post" name="formpost">
<fieldset>
	<legend>REPORT CEK KANDIDAT KARYAWAN BARU</legend>
	<table class="entry2">
		<tr>
			<th>Cek Kandidat</th>
			<td>
				<input type="date" id="tglA" name="tglA" class="tgl"> &nbsp;s/d&nbsp;
				<input type="date" id="tglB" name="tglB" class="tgl">
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><input type="submit" name="print" id="print" value="Print"></td>
		</tr>
	</table>
</fieldset>
</form>
<script type="text/javascript">
$(document).ready(function(){
	$(".tgl").datepicker({
		dateFormat: 'dd-mm-yy',
        yearRange: '1900:c+1' ,
		changeMonth : true,
		changeYear : true
	});
	
	var pesan = '${pesan}';
	if(pesan!=null && pesan!=''){
		alert(pesan);
	}
});
</script>
</body>
</html>
