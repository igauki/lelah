<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title], a[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		
		$(".checkall").click(function () {
			tot = $(this).parent().find(":hidden");
			if(tot.val() == "true") tot.val(""); else tot.val("true");
			$(this).parent().parent().find(":checkbox").attr("checked", tot.val());
			return false;
		});	
		
		$("#wil_no").change(function() {
			$("#lcb_no").empty();
			var url = "${path}/report/bsm.htm?window=monitoring_bsm&json=1&wil_no=" +$("#wil_no").val();
			$.getJSON(url, function(result) {
				//$("<option/>").val("PILIH WILAYAH").html("").appendTo("#lcb_no");
				$.each(result, function() {
					$("<option/>").val(this.key).html(this.value).appendTo("#lcb_no");
				});
				$("#lcb_no option:first").attr('selected','selected');
			});
					
/* 			//kopi untuk disubmit (isinya, bukan valuenya)
			$("#cabang2").val($("#cabang option:selected").text());
			$("#wakil2").val("ALL");
			$("#region2").val("ALL"); */
	
		});
		
			
	});
	
	function ubahKategori(val){
	$("#jenis_report").empty();
			 if ('164-11'== val){
			 	$("#jenis_report").empty();
			 	$("#jenis_report").html("<label for='kategori0'><input type='checkbox' id='kategori0' name='kategori0' value='1' checked='true'>New Business</label> <label for='kategori1'><input type='checkbox' id='kategori1' name='kategori1' value='2' checked='true'>Top-Up</label> <label for='kategori2'><input type='checkbox' id='kategori2' name='kategori2' value='3' checked='true'>Outstanding</label> <label for='kategori3'><input type='checkbox' id='kategori3' name='kategori3' value='4' checked='true'>Demografi</label>");
			 }
			 if ('164-2'== val){
			 	$("#jenis_report").empty();
			 	$("#jenis_report").html("<label for='kategori4'><input type='checkbox' id='kategori4' name='kategori4' value='5' checked='true'>Roll over</label> <label for='kategori2'><input type='checkbox' id='kategori2' name='kategori2' value='3' checked='true'>Outstanding</label>");
			 }
			 if ('142-2'== val){
			 	$("#jenis_report").empty();
			 	$("#jenis_report").html("<label for='kategori0'><input type='checkbox' id='kategori0' name='kategori0' value='1' checked='true'>New Business</label> <label for='kategori4'><input type='checkbox' id='kategori4' name='kategori4' value='5' checked='true'>Roll over</label> <label for='kategori3'><input type='checkbox' id='kategori3' name='kategori3' value='4' checked='true'>Demografi</label> <label for='kategori2'><input type='checkbox' id='kategori2' name='kategori2' value='3' checked='true'>Outstanding</label>");
			 }
			 if ('120-10'== val ||'120-11'== val||'120-12'== val){
			 	$("#jenis_report").empty();
			 	$("#jenis_report").html("<label for='kategori0'><input type='checkbox' id='kategori0' name='kategori0' value='1' checked='true'>New Business</label> <label for='kategori1'><input type='checkbox' id='kategori1' name='kategori1' value='2' checked='true'>Renewal & Top-Up</label> <label for='kategori2'><input type='checkbox' id='kategori2' name='kategori2' value='3' checked='true'>Outstanding</label>");
			 }
	
		}
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	table tr td label { display: inline-block; width: 30em;}
	
	/* styling untuk table th */
	table tr th { padding: 1em;}
	
</style>

<body>

<form id="formPost" name="formPost" method="post" target="_blank">
<fieldset class="ui-widget ui-widget-content">
	<legend class="ui-widget-header ui-corner-all"><div>${judul}</div></legend>

	<table>
		<tr style="background-color: #F5F5F5;">
			<th colspan="2">Produk:<td>
				<select name="lsbs_id" onchange="ubahKategori(this.value);">
				<option value ="ALL">[PILIH PRODUK]</option>
			 	<c:forEach items="${listProduct}" var="x" varStatus="xt">
					<option value="${x.key}">${x.desc}</option>
			</c:forEach><!--  <option value ="142-2">SIMAS PRIMA</option> -->
			<option value ="164-2">[164-2]SIMAS STABIL LINK</option></select>
			</th>
		</tr>
		<tr style="background-color: #F5F5F5;">
				<th colspan="2">Nama Wilayah</th>
				<td>
					<select name="wil_no" id="wil_no">
					<option value ="ALL">[PILIH WILAYAH]</option>
				<c:forEach items="${listWilayah}" var="x" varStatus="xt">
					<option value="${x.key}" 
					<%-- <c:if test="${x.key eq lca_id}">selected</c:if> --%> >${x.value}</option>
			</c:forEach></select>
		</td>
	</tr>
		<tr style="background-color: #F5F5F5;">
		<th colspan="2">Nama Cabang</th>
			<td colspan="2">
			<input type="hidden" name="pil">
				<select name="lcb_no" id="lcb_no">
			<c:forEach items="${listcabang}" var="x">
				<option value="${x.key}">${x.value}</option></c:forEach></select>	
		</td>
	</tr>	
	<tr style="background-color: #F5F5F5;">
			<th colspan="2"><a href="#" class="checkall" title="Check All">Jenis Report</a><input type="hidden" value="true"></th>
			<th colspan="2"><div id ="jenis_report">
				<c:forEach items="${listkategori}" var="l" varStatus="s">
					<label for="kategori${s.index}"><input type="checkbox" id="kategori${s.index}" name="kategori${s.index}" value="${l.key}" checked="true">${l.value}</label>
				</c:forEach></div>		
			</th>
		</tr>
		<tr style="background-color: #F5F5F5;">
			<th colspan="2">Periode:</th>
			<td colspan="2">
				<input name="bdate" id="bdate" type="text" class="datepicker" title="Tanggal Awal" value="${today}"> s/d 
				<input name="edate" id="edate" type="text" class="datepicker" title="Tanggal Akhir" value="${today}">
			</td>
		</tr>	
	</table>

	<div class="rowElem">
		<input type="submit" name="showPDF" id="showPDF" value="Show (PDF)">
		<input type="submit" name="showXLS" id="showXLS" value="Show (Excel)">
		<input type="hidden" name="showReport" value="1">
	</div>
</fieldset>
</form>

</body>
</html>