<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen"> <!-- CSS untuk DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script><!-- DatePicker Script (jscalendar) -->
	</head>
	<script>
		function tampil(){
			tgl1=formpost.tanggalAwal.value;
			tgl2=formpost.tanggalAkhir.value;
			
			var cek = formpost.email.checked ;
			var to = formpost.to.value;
			var pdf = formpost.pdfAtt.checked ;
			var xls = formpost.excelAtt.checked ;
			if(tgl1 != "" && tgl2 != "") {
				window.location='${path }/report/uw.htm?window=report_decline&tanggalAwal='+tgl1+'&tanggalAkhir='+tgl2+'&cekMail='+cek+'&to='+to+'&isPDF='+pdf+'&isXls='+xls;
			}
			else {
				alert("tanggal tidak boleh kosong !!! ");
			}
		}

		function show() {
			var cek = formpost.email.checked ;

			if(cek == true) {
				document.getElementById("showMail").style.visibility = "visible";
				document.getElementById("btnShow").value = "SEND";
			}
			else {
				document.getElementById("showMail").style.visibility = "hidden";
				document.getElementById("btnShow").value = "SHOW"
			}
		}
	</script>
	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Report Decline</a>
				</li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form id="formpost" method="post">
						<table class="entry2">
							<tr>
								<th width="10%">Tanggal :</th>
								<td width="40%">
									<script>inputDate('tanggalAwal', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>
									 s/d 
									 <script>inputDate('tanggalAkhir', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>
								</td>
							</tr>
							<tr>
								<th></th>
								<td><input type="checkbox" id="email" onclick="show()">Email Report</td>
							</tr>
							<tr>
								<th></th>
								<td>
									<div id="pane3" class="panes">
										<table class="entry2">
											<tr>
												<td>
													<div id="showMail" style="visibility: hidden">
														To: <input type="text" value="Sugeng@ekalife2000.com;" id="to" size="50">
														<div><input type="checkbox" id="excelAtt" >Email Report dalam bentuk Excel</div>
														<div><input type="checkbox" id="pdfAtt" >Email Report dalam bentuk PDF</div>
													</div>
												</td>
											</tr>	
										</table>
									</div>
								</td>
							</tr>
							<tr>
								<th></th>
								<td colspan="3"><input type="button" name="btnShow" id="btnShow" value="SHOW" onClick="tampil();"></td>
								<td></td>
							</tr>
							<tr>
								<td colspan="2">
									<div id="error">
						        		Daftar Decline hari ini :<br>
						        			<c:if test="${flagCabang eq 1}">Tidak Ada</c:if>
						        			<c:forEach var="error" items="${lsCabangToday}" >
												- <c:out value="${error.value}" escapeXml="false" />
												<br/>
											</c:forEach>
									</div>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>