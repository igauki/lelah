<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>  
		<script type="text/javascript">
			function tampil() {
				var th = document.getElementById('tahun').value;
				var bl = document.getElementById('bulan').value;
				
				if(th != '' && bl != '') {
					document.getElementById('infoFrame').src = '${path}/report/uw.pdf?window=report_filing&tahun='+th+'&bulan='+bl; 
				}				
			}
		</script>
	</head>
	<body>
		<form id="formpost" method="post">
			<table class="entry2">
				<tr>
					<th colspan="2">REPORT FILING</th>
				</tr>
				<tr>
					<th>Tahun</th>
					<td>
						<select name="tahun" id="tahun" onChange="javascript:formpost.submit();" style="width: 97px;">
							<option value=""></option>
							<c:forEach items="${daftarTahun}" var="x" varStatus="xt">
								<option value="${x.TAHUN}"
									<c:if test="${x.TAHUN eq getTahun}">selected</c:if>>
									${x.TAHUN}
								</option>
							</c:forEach>
						</select>						
					</td>
				</tr>
				<tr>
					<th>Bulan</th>
					<td>
						<select name="bulan" id="bulan" style="width: 97px;">
							<option value=""></option>
							<c:forEach items="${daftarBulan}" var="x" varStatus="xt">
								<option value="${x.VALUE}">${x.LABEL}</option>
							</c:forEach>
						</select>					
					</td>
				</tr>
				<tr>
					<th>&nbsp;</th>
					<td><input type="button" value="Tampilkan" onclick="tampil()"></td>
				</tr>				
			</table>
			<c:if test="${not empty tampil}">
				<iframe name="infoFrame" id="infoFrame" style="width: 100%; height: 90%"> Please Wait... </iframe>			
			</c:if>	
		</form>
	</body>
</html>
