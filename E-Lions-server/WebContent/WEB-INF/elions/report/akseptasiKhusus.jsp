<%@ include file="/include/page/header.jsp"%>
<script>
function tampil(){
	setFrameSize('infoFrame', 45);
	var tgl1 = document.formpost.tglAwal.value;
	var tgl2 = document.formpost.tglAkhir.value;
	document.getElementById('infoFrame').src='${path }/report/uw.htm?window=reportAkseptasiKhusus&tanggalAwal='+tgl1+'&tanggalAkhir='+tgl2+ '&show=true';
}

</script>
<body onload="document.title='PopUp :: Alamat Penagihan';" style="height: 100%;">
<form method="post" name="formpost">
  <div id="contents"> 
  <fieldset>
  <legend>Report Akseptasi Khusus</legend> 
    <table class="entry">
      <tr> 
        <th>Periode</th>
        <td>
			<script>inputDate('tglAwal', '${sysdate}', false);</script>
			s/d
			<script>inputDate('tglAkhir', '${sysdate}', false);</script>

		    <input type="button" name="show" value="Show" onclick="tampil();">

        </td>        
      </tr>
    </table>
    	
    <table class="entry" width="98%">
	    <tr>
	    	<td>
		    <iframe name="infoFrame" id="infoFrame"
						width="100%"  > Please Wait... </iframe>
		    </td>
	    </tr>
    </table>
  </div>  
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>
