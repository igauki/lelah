<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen"> <!-- CSS untuk DatePicker Script (jscalendar) -->
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script><!-- DatePicker Script (jscalendar) -->
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script><!-- DatePicker Script (jscalendar) -->
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script><!-- DatePicker Script (jscalendar) -->
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	function tampilkan(jenis){
		document.formpost.action='${report.requestUri}'+'.'+jenis;
		//document.getElementById('show').value = jenis;
		showPane('pane2',	document.getElementById('tab2'));
		if('back' == jenis) {
			showPane('pane1',	document.getElementById('tab1'));
		} else if('html' == jenis) {
			document.formpost.submit();
		//} else if('jasper' == jenis) {
		//	document.formpost.submit();
		} else if('pdf' == jenis) {
			document.formpost.submit();
		} else if('xls' == jenis) {
			document.formpost.submit();
		} else if('email' == jenis) {
			document.formpost.email_ucup.value = 'email';
			document.formpost.submit();
			document.formpost.email_ucup.value = '';
		}
		return false;
	}
	function kirimEmail(){
		var s=document.formpost.to.value;
		popWin('${path}/common/menu.htm?frame=sendEmailReport&title=${report.title}&to='+s, 300, 400);
	}

    // added by Samuel
    function refreshPage()
    {
        document.formpost.submit();
    }
</script>
</head>
<BODY onload="setupPanes('container1', 'tab1'); setFrameSize('reportFrame', 78);" onresize="setFrameSize('reportFrame', 78);" style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">${report.title}</a>
			</li>
			<li>
				<a href="#" onClick="return showPane('pane2', this)" id="tab2">Result</a>
			</li>
			<c:if test="${report.flagEmail eq true}">
				<li>
					<a href="#" onClick="return showPane('pane3', this)" id="tab2">Email</a>
				</li>
			</c:if>
			<c:if test="${report.flagEmailCab eq true}">
				<li>
					<a href="#" onClick="return showPane('pane3', this)" id="tab2">Email</a>
				</li>
			</c:if>
		</ul>

		<form method="post" name="formpost" target="reportFrame" action="${report.requestUri}.${report.defaultView}">
			<div class="tab-panes">
	
				<div id="pane1" class="panes">
					<input type="hidden" name="window" value="${param.window}">
<!--					<input type="hidden" name="show" value="html" id="show">-->
					<table class="entry2" style="border-collapse:collapse;border: 0; padding: 5px;">
						<c:if test="${not empty report.reportPathList}">
							<tr>
								<th nowrap="nowrap">Jenis Report :</th>
								<td>
									<select name="reportPath">
										<c:forEach var="rep" items="${report.reportPathList}">
											<option value="${rep.key}">${rep.value}</option>
										</c:forEach>
									</select>
								</td>
							</tr>
						</c:if>
						<c:forEach items="${report.parameterList}" var="parameter">
							<tr>
								<c:choose>
									<c:when test="${parameter.type eq  \"depault\"}">
										<c:choose>
											<c:when test="${parameter.visible eq true}">
												<th nowrap="nowrap">${parameter.name}</th>
												<td>
													<input type="text" class="readOnly" readonly="readonly" 
														name="${parameter.id}" value="${parameter.value}"
														size="${parameter.max + 4}" maxlength="${parameter.max}">
												</td>
											</c:when>
											<c:otherwise>
												<th></th>
												<td>
													<input type="hidden" name="${parameter.id}" value="${parameter.value}"
														size="${parameter.max + 4}" maxlength="${parameter.max}">
												</td>
											</c:otherwise>
										</c:choose>
									</c:when>
									<c:when test="${parameter.type eq  \"text\"}">
										<th nowrap="nowrap">${parameter.name}</th>
										<td><input type="text" name="${parameter.id}" size="${parameter.max + 4}" maxlength="${parameter.max}" value="${parameter.depault}"></td>
									</c:when>
									<c:when test="${parameter.type eq  \"select\"}">
										<th nowrap="nowrap">${parameter.name}</th>
										<td>
											<select name="${parameter.id}" > 
												<option value="All">All</option>
												<c:forEach var="list" items="${parameter.list}">
													<option value="${list.KEY}">${list.VALUE}</option>
												</c:forEach>
											</select>
										</td>
									</c:when>
									<c:when test="${parameter.type eq  \"selectWithoutAll\"}">
										<th nowrap="nowrap">${parameter.name}</th>
										<td>
											<select name="${parameter.id}" >
                                                <c:forEach var="list" items="${parameter.list}">
													<option value="${list.KEY}">${list.VALUE}</option>
												</c:forEach>
											</select>
										</td>
									</c:when>
                                    <c:when test="${parameter.type eq  \"selectWithRefresh\"}">
										<th nowrap="nowrap">${parameter.name}</th>
										<td>
											<select name="${parameter.id}" onchange="refreshPage();">
												<c:forEach var="list" items="${parameter.list}">
													<option value="${list.KEY}">${list.VALUE}</option>
												</c:forEach>
											</select>
										</td>
									</c:when>
                                    <c:when test="${parameter.type eq  \"date\"}">
										<th nowrap="nowrap">${parameter.name}</th>
										<td><script>inputDate('${parameter.id}', '<fmt:formatDate value="${parameter.depault}" pattern="dd/MM/yyyy"/>', false);</script></td>
									</c:when>
									<c:when test="${parameter.type eq  \"dateRange\"}">
										<th nowrap="nowrap">${parameter.name}</th>
										<td>
											<script>inputDate('${parameter.id}Awal', '<fmt:formatDate value="${parameter.defaultAwal}" pattern="dd/MM/yyyy"/>', false);</script>
											&nbsp;s/d&nbsp;
											<script>inputDate('${parameter.id}Akhir', '<fmt:formatDate value="${parameter.defaultAkhir}" pattern="dd/MM/yyyy"/>', false);</script>
										</td>
									</c:when>
									<c:when test="${parameter.type eq  \"month\"}">
										<th nowrap="nowrap">${parameter.name}</th>
										<td>
											<select name="${parameter.id}_mm">
												<c:forEach var="bulan" items="${parameter.bulan}">
													<option value="${bulan.key}">${bulan.value}</option>
												</c:forEach>
											</select>
											<select name="${parameter.id}_yyyy">
												<c:forEach var="tahun" items="${parameter.tahun}" varStatus="st">
													<option value="${tahun }" selected="selected">${tahun}</option>
												</c:forEach>
											</select>
										</td>
									</c:when>
									<c:when test="${parameter.type eq  \"monthRange\"}">
										<th nowrap="nowrap">${parameter.name}</th>
										<td>
											<select name="${parameter.id}Awal_mm">
												<c:forEach var="bulan" items="${parameter.bulan}">
													<option value="${bulan.key}">${bulan.value}</option>
												</c:forEach>
											</select>
											<select name="${parameter.id}Awal_yyyy">
												<c:forEach var="tahun" items="${parameter.tahun}" varStatus="st">
													<option value="${tahun }" selected="selected">${tahun}</option>
												</c:forEach>
											</select>
											s/d
											<select name="${parameter.id}Akhir_mm">
												<c:forEach var="bulan" items="${parameter.bulan}" varStatus="st">
													<option value="${bulan.key}">${bulan.value}</option>
												</c:forEach>
											</select>
											<select name="${parameter.id}Akhir_yyyy">
												<c:forEach var="tahun" items="${parameter.tahun}" varStatus="st">
													<option value="${tahun }" selected="selected">${tahun}</option>
												</c:forEach>
											</select>												
										</td>
									</c:when>
									<c:otherwise></c:otherwise>
								</c:choose>
							</tr>
						</c:forEach>
                        <tr>
                            <th></th><td><br/></td>
                        </tr>
                        <tr>
							<th></th>
							<td>
								<input type="button" value="Show" name="show" onclick="tampilkan('${report.defaultView}');">
							</td>
						</tr>
						<c:if test="${report.reportCanBeEmailed eq true}">
							<tr>
								<th colspan="2">
									<fieldset>
										<legend>E-mail</legend>
										<table class="displaytag">
											<tr>
												<th>TO:</th>
												<td style="text-align: left;"><input size="110" readonly="true" value="<c:forEach items="${report.reportEmailTo}" var="e">${e};</c:forEach>"></td>
											</tr>
											<tr>
												<th>SUBJECT:</th>
												<td style="text-align: left;"><input size="110" type="text" readonly="true" value="${report.reportSubject}" name="reportSubject"></td>
											</tr>
											<tr>
												<th>MESSAGE:</th>
												<td style="text-align: left;">
													<textarea rows="5" cols="150" name="reportMessage">${report.reportMessage}</textarea>
												</td>
											</tr>
											<tr>
												<th></th>
												<td style="text-align: left;">
													<input type="hidden" id="email_ucup" name="email_ucup" value="">
													<input type="button" value="Email" name="btn_email" onclick="tampilkan('email');">
												</td>
											</tr>
										</table>
									</fieldset>
								</th>
							</tr>
							
						</c:if>
					</table>
				</div>
				<div id="pane2" class="panes">
					<table class="entry2">
						<tr>
							<th>
								<a href="#" onclick="return tampilkan('back');"
									onmouseover="return overlib('Alt-B > Back', AUTOSTATUS, WRAP);" onmouseout="nd();" accesskey="B">
									<img style="border:none;" alt="Back" src="${path}/include/image/action_back.gif"/></a>
							</th>
							<th>Format : </th>
							<th>
								<a href="#" onclick="return tampilkan('html');"
									onmouseover="return overlib('Alt-H > View (HTML)', AUTOSTATUS, WRAP);" onmouseout="nd();" accesskey="H">
									<img style="border:none;" alt="HTML" src="${path}/include/image/page_html.gif"/></a>
								&nbsp;
	<!--							<a href="#" onclick="return tampilkan('jasper');"-->
	<!--								onmouseover="return overlib('Alt-J > View (JAVA)', AUTOSTATUS, WRAP);" onmouseout="nd();" accesskey="J">-->
	<!--								<img style="border:none;" alt="JAVA" src="${path}/include/image/page_java.gif"/></a>-->
	<!--							&nbsp;-->
								<a href="#" onclick="return tampilkan('pdf');"
									onmouseover="return overlib('Alt-P > View (PDF)', AUTOSTATUS, WRAP);" onmouseout="nd();" accesskey="P">
									<img style="border:none;" alt="PDF" src="${path}/include/image/pdf.gif"/></a>
								&nbsp;
								<c:if test="${report.flagExcel eq false}">
									<a href="#" onclick="return tampilkan('xls');"
										onmouseover="return overlib('Alt-X > View (EXCEL)', AUTOSTATUS, WRAP);" onmouseout="nd();" accesskey="X">
										<img style="border:none;" alt="EXCEL" src="${path}/include/image/excel.gif"/></a>
									&nbsp;
								</c:if>
							</th>
							<th>
								<label for="att1">
									<input type="radio" class="noBorder" name="attachment" value="1" id="att1" checked="checked"> Tampilkan ke Layar
								</label>
								<label for="att2">
									<input type="radio" class="noBorder" name="attachment" value="0" id="att2"> Simpan ke File
								</label>
							</th>
						</tr>
						<tr>
							<th colspan="4">
								<iframe name="reportFrame" id="reportFrame" width="100%" src="${path}/report/uw.htm?window=loading_screen">
									Please Wait...
								</iframe>
							</th>
						</tr>
					</table>
				</div>
				<c:if test="${report.flagEmail eq true}">
					<div id="pane3" class="panes">
						<table class="entry2">
							<tr>
								<td>To:
									<input type="text" value="${report.to}" name="to" size="50">
								</td>
							</tr>	
							<tr>
								<td>
									<input type="button" value="Send" name="email" onClick="kirimEmail()">
								</td>	
						</table>
					</div>
				</c:if>	
				<c:if test="${report.flagEmailCab eq true}">
					<div id="pane3" class="panes">
						<table class="entry2">
							<tr>
								<td>To:
									<input type="text" value="${report.to}" name="to" size="50">
									<select onchange="document.formpost.to.value = this.value;">
										<c:forEach items="${daftarEmailCabang}" var="d">
											<option value="${d.LAR_EMAIL}">${d.LCA_NAMA} (${d.LAR_EMAIL})</option>
										</c:forEach>
									</select>
								</td>
							</tr>	
							<tr>
								<td>
									<input type="button" value="Send" name="email" onClick="kirimEmail()">
								</td>	
						</table>
					</div>
				</c:if>
			</div>
		</form>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>