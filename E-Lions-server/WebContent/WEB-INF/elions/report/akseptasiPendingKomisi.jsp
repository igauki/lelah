<%@ include file="/include/page/header.jsp"%>
<script>
function tampil(){
	setFrameSize('infoFrame', 45);
	var tgl1 = document.formpost.tglAwal.value;
	var tgl2 = document.formpost.tglAkhir.value;
			
			var cek = formpost.email.checked ;
			var to = formpost.to.value;
			var pdf = formpost.pdfAtt.checked ;
			var xls = formpost.excelAtt.checked ;
			if(tgl1 != "" && tgl2 != "") {	
	document.getElementById('infoFrame').src='${path }/report/uw.htm?window=reportAkseptasiKhusus&tanggalAwal='+tgl1+'&tanggalAkhir='+tgl2+ '&show=true'+'&cekMail='+cek+'&to='+to+'&isPDF='+pdf+'&isXls='+xls;
			}
			else {
				alert("tanggal tidak boleh kosong !!! ");
			}
		}
		
		function showEmail() {
			var cek = formpost.email.checked ;

			if(cek == true) {
				document.getElementById("showMail").style.visibility = "visible";
				document.getElementById("btnShow").value = "SEND";
			}
			else {
				document.getElementById("showMail").style.visibility = "hidden";
				document.getElementById("btnShow").value = "SHOW"
			}
		}

</script>
<body onload="document.title='PopUp :: Alamat Penagihan';" style="height: 100%;">
<form method="post" name="formpost">
  <div id="contents"> 
  <fieldset>
  <legend>Report Akseptasi Khusus</legend> 
    <table class="entry">
	    <tr> 
	        <th>Periode</th>
	        <td>
				<script>inputDate('tglAwal', '${sysdate}', false);</script>
				s/d
				<script>inputDate('tglAkhir', '${sysdate}', false);</script>
	
			    <input type="button" name="show" id="btnShow" value="Show" onclick="tampil();">
			</td>
		</tr>
		<tr>
			<th></th>
				<td><input type="checkbox" id="email" onclick="showEmail()">Email Report</td>
		</tr>
		<tr>
			<th></th>
			<td>
				<div id="pane3" class="panes">
					<table class="entry2">
						<tr>
							<td>
								<div id="showMail" style="visibility: hidden">
									To: <input type="text" value="Sugeng@ekalife2000.com;" id="to" size="50">
									<div><input type="checkbox" id="excelAtt" >Email Report dalam bentuk Excel</div>
									<div><input type="checkbox" id="pdfAtt" >Email Report dalam bentuk PDF</div>
								</div>
							</td>
						</tr>	
					</table>
				</div>
			</td>
		</tr>
    </table>
    	
    <table class="entry" width="98%">
	    <tr>
	    	<td>
		    <iframe name="infoFrame" id="infoFrame"
						width="100%"  > Please Wait... </iframe>
		    </td>
	    </tr>
    </table>
  </div>  
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>
