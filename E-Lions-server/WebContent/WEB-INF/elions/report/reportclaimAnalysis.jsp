<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		
		// user memilih grup bank, maka tampilkan nama bank nya
		$("#grup_bank").change(function() {
			$("#nama_bank").empty();
			var url = "${path}/report/uw.htm?window=reportclaimAnalysis&json=1&lcg=" +$("#grup_bank").val();
			$.getJSON(url, function(result) {
				$("<option/>").val("ALL").html("ALL").appendTo("#nama_bank");
				$.each(result, function() {
					$("<option/>").val(this.key).html(this.value).appendTo("#nama_bank");
				});
				$("#nama_bank option:first").attr('selected','selected');
			});
			
			//kopi untuk disubmit (isinya, bukan valuenya)
			$("#grup_bank2").val($("#grup_bank option:selected").text());
			$("#nama_bank2").val("ALL");
	
		});
		
		// user memilih nama bank
		$("#nama_bank").change(function() {			
			//kopi untuk disubmit (isinya, bukan valuenya)
			$("#nama_bank2").val($("#nama_bank option:selected").text());
	
		});
		
		//hide
		$("#hide").hide();
		$("#jenis_polis,#jenis_report").change(function() { 
			var a = $("#jenis_polis option:selected").val();
			//var b = $("#jenis_report option:selected").val();
			if(a==3){
				$("#hide").show();
			}else{
				$("#hide").hide();
			}
		}); 
		
	});
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

</style>

<body>

<form id="formPost" name="formPost" method="post" target="_blank">
<fieldset class="ui-widget ui-widget-content">
	<legend class="ui-widget-header ui-corner-all"><div>Report Claim Analysis</div></legend>
	
	<div class="rowElem">
		<label>Report :</label>
		<select name="jenis_report" id="jenis_report">
			<option value="3">Claim Detail</option>
			<option value="5">Claim By COD</option>
		    <option value="1">COD Based On Age at Death</option>
		    <option value="7">Entry  Age  &  Duration Of Policy</option>
		    <option value="2">COD Based On Duration of Policy</option>
		    <option value="6">COD Based on Medical/NM Case</option>
		    <option value="4">COD By Branch</option>
		    <option value="8">Ex-Gratia Claim</option>
		    <option value="9">Claim By Product</option>
		    <option value="10">Claim By Sum Assured</option>
		    <option value="11">Claim By Amount Claim Paid</option>
		  </select>
	</div>
	
	<div class="rowElem">
		<label>Polis :</label>
		<select name="jenis_polis" id="jenis_polis">
		    <option value="1">All</option>
		    <option value="2">Individu</option>
		    <option value="3">MRI</option>
		  </select>
	</div>
	
	<div class="rowElem" id="hide">
		<label style="vertical-align: top;">Grup Bank/Nama Bank:</label>
		<select size="10" name="grup_bank" id="grup_bank" title="Silahkan pilih Grup Bank">
			<option value="ALL" selected="selected">ALL</option>
			<c:forEach var="c" items="${listGrupBank}" varStatus="s">
				<option value="${c.key}">${c.value}</option>
			</c:forEach>
		</select>
		<select size="10" name="nama_bank" id="nama_bank" title="Silahkan pilih Nama Bank">
			<option value="ALL" selected="selected">ALL</option>
			<c:forEach var="c" items="${listNamaBank}" varStatus="s">
				<option value="${c.key}">${c.value}</option>
			</c:forEach>
		</select>
	</div>
	
	<div class="rowElem">
		<label>Periode Berdasarkan :</label>
		<select name="jenis_periode" id="jenis_periode">
		    <option value="1">Date Submitted</option>
		    <option value="2">Date Event</option>
		    <option value="3">Date Paid</option>
		  </select>
	</div>
	
	<div class="rowElem">
		<label>Tanggal :</label>
		<input name="bdate" id="bdate" type="text" class="datepicker" title="Tanggal Awal" value="${bdate}"> s/d 
		<input name="edate" id="edate" type="text" class="datepicker" title="Tanggal Akhir" value="${edate}">
	</div>
	
	<div class="rowElem">
		<label></label>
		<input type="submit" name="showPDF" id="showPDF" value="Show (PDF)">
		<input type="submit" name="showXLS" id="showXLS" value="Show (Excel)">
		<input type="hidden" name="grup_bank2" id="grup_bank2" value="ALL">
		<input type="hidden" name="nama_bank2" id="nama_bank2" value="ALL">
		<input type="hidden" name="showReport" value="1">
	</div>
</fieldset>
</form>

</body>
</html>