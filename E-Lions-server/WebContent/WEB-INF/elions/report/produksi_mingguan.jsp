<%@ include file="/include/page/header_jquery.jsp"%>
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$(document).ready(function() {
		
		// (jQueryUI Tabs) init tab2 Utama (Pemegang, Tertanggung, dll)
		$("#tabs").tabs();

		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title], label[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			//$(this).removeClass();
			$(this).addClass("ui-widget-header");
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		
		//autocomplete user
		$( "#user" ).autocomplete({
			minLength: 1,
			source: "${path}/report/bas.htm?window=report_produksi_mingguan&json=1",
			focus: function( event, ui ) {
				$( "#user" ).val( ui.item.LUS_LOGIN_NAME );
				$( "#lus_id" ).val( ui.item.LUS_ID);
				return false;
			},
			select: function( event, ui ) {
				$( "#user" ).val( ui.item.LUS_LOGIN_NAME);
				$( "#lus_id" ).val( ui.item.LUS_ID);
				return false;
			}
		})
		.data( "autocomplete" )._renderItem = function( ul, item ) {
			return $( "<li></li>" )
				.data( "item.autocomplete", item )
				.append( "<a>" + item.LUS_LOGIN_NAME+ "</a>" )
				.appendTo( ul );
		};	
		
	});
	
	function formValidation(){
		if(($("#kntr_pemasaran").val()=="") || ($("#kntr_pemasaran").val()==null)){
   			alert( "Harap isi alamat kantor pemasaran!" );
   			$("#kntr_pemasaran").focus();
   			return false;
   		}
   		
		if(($("#user").val()=="") || ($("#user").val()==null)){
   			alert( "Harap isi nama user!" );
   			$("#user").focus();
   			return false;
   		}
   		
   		return true;
	}
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 15em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 23.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 36em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

	/* untuk tombol2 kecil di report */
	ul#icons {margin: 0; padding: 0;}
	ul#icons li {margin: 0 1px 0 0; position: relative; padding: 0px; cursor: pointer; float: left;  list-style: none;}
	ul#icons span.ui-icon {float: left; margin: 0px;}

</style>

<body>
<form id="formPost" name="formPost" method="post" target="_blank" onSubmit="return formValidation();">
	<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>Report Produksi Mingguan</div></legend>		
		<div class="rowElem">
			<label>Tanggal :</label>
			<input name="bdate" id="bdate" type="text" class="datepicker" title="Tanggal Awal" value="${bdate}"> s/d 
			<input name="edate" id="edate" type="text" class="datepicker" title="Tanggal Akhir" value="${edate}">
		</div>
		
		<div class="rowElem">
			<label>Kantor Pemasaran :</label>
			<input name="kntr_pemasaran" id="kntr_pemasaran" type="text" class="lebar" title="Kantor Pemasaran" value="">
		</div>
		
		<div class="rowElem">
			<label>User :</label>
			<input name="user" id="user" type="text" title="User" value="">
		</div>
		
		<div class="rowElem">
			<label>&nbsp;</label>
			<input name="lus_id" id="lus_id" type="hidden" value="">
		</div>
		
		<div class="rowElem">
			<label></label>
			<input type="submit" name="showPDF" id="showPDF" value="Show (PDF)">
			<input type="submit" name="showXLS" id="showXLS" value="Show (Excel)">
			<input type="hidden" name="showReport" value="1">
		</div>
	</fieldset>
</form>
</body>
</html>