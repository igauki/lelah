<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
        
        //autocomplete user
		$( "#seluserBas2" ).autocomplete({
			minLength: 3,
			source: "${path}/common/json.htm?window=getUser",
			focus: function( event, ui ) {
				$( "#seluserBas2" ).val( ui.item.value );
				return false;
			},
			select: function( event, ui ) {
				$( "#seluserBas2" ).val( ui.item.value);
				$( "#seluserBas" ).val( ui.item.key);
				return false;
			}
		})
		.data( "autocomplete" )._renderItem = function( ul, item ) {
			return $( "<li></li>" )
				.data( "item.autocomplete", item )
				.append( "<a>" + item.value + "</a>" )
				.appendTo( ul );
		};
	});
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

</style>

<body>

<form id="formPost" name="formPost" method="post">
<fieldset class="ui-widget ui-widget-content">
	<legend class="ui-widget-header ui-corner-all"><div>${judul}</div></legend>

	<div class="rowElem">
		<label>Tanggal Permintaan :</label>
		<input name="bdate" id="bdate" type="text" class="datepicker" title="Tanggal Awal" value="${bdate}"> s/d 
		<input name="edate" id="edate" type="text" class="datepicker" title="Tanggal Akhir" value="${edate}">
	</div>
	<c:if test="${judul eq 'Report Stock SPAJ'}">
	<div class="rowElem" id="userBas">
		<label>User :</label>
		<input name="seluserBas2" id="seluserBas2" size="50" type="text" title="User" value="">
		<input name="seluserBas" id="seluserBas" size="6" type="hidden" title="User" value="">
	</div>
	
	<div class="rowElem">
		<label>Jenis Report :</label>
		<select name="jenis" id="jenis">
			<option value="Cabang">Cabang</option>
			<option value="Agent">Agent</option>
		</select>
	</div>
	</c:if>
	
	
	<c:if test="${judul eq 'Report SLA'}">
	<div class="rowElem">
		<label>Jenis Report :</label>
		<select name="jenis" id="jenis">
			<option value="Great">Team The Great</option>
			<option value="Agency">ALL Agency</option>
			<option value="Bancas">ALL Bancassurance (Simpol)</option>
		</select>
	</div>
	</c:if>
	
	<div class="rowElem">
		<label></label>
		<input type="submit" name="btnCari" value="View"/>
		<input type="hidden" name="mode" id="mode" value="report"/>
	</div>
</fieldset>
</form>

</body>
</html>