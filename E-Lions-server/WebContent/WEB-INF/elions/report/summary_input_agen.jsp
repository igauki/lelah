<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		
		// user memilih cabang, maka tampilkan wakil dan region nya
		$("#cabang").change(function() {
			$("#userinput").empty();
			var url = "${path}/report/bas.htm?window=summary_input_agen&json=1&cabang=" +$("#cabang").val();
			$.getJSON(url, function(result) {
				$("<option/>").val("ALL").html("ALL").appendTo("#userinput");
				$.each(result, function() {
					$("<option/>").val(this.key).html(this.value).appendTo("#userinput");
				});
				$("#userinput option:first").attr('selected','selected');
			});
			//kopi untuk disubmit (isinya, bukan valuenya)
			$("#cabang2").val($("#cabang option:selected").text());
			$("#userinput2").val("ALL");
	
		});
		
		$("#userinput").change(function() {
			//kopi untuk disubmit (isinya, bukan valuenya)
			$("#userinput2").val($("#userinput option:selected").text());
		});		
		
		$("#jn_report1").click(function() {
			$("#seluserBas").val("ALL");
            $("#userBas").hide();
        });
		
		$("#jn_report2").click(function() {
			//if($("#bas").val()==1)$("#userBas").show();
        });
		
	});
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

</style>

<body>

<form id="formPost" name="formPost" method="post" target="_blank">
<fieldset class="ui-widget ui-widget-content">
	<legend class="ui-widget-header ui-corner-all"><div>Report Summary Input Register Agen</div></legend>

	<div class="rowElem">
		<label>Cabang/User:</label>
		<select size="10" name="cabang" id="cabang" title="Silahkan pilih Cabang">
			<option value="ALL" selected="selected">ALL</option>
			<c:forEach var="c" items="${listCabang}" varStatus="s">
				<option value="${c.key}">${c.value}</option>
			</c:forEach>
		</select>
		<select size="10" name="userinput" id="userinput" title="Silahkan pilih User Input">
			<option value="ALL" selected="selected">ALL</option>
			<c:forEach var="c" items="${listUser}" varStatus="s">
				<option value="${c.key}">${c.value}</option>
			</c:forEach>
		</select>
	</div>
	
	<div class="rowElem">
		<label>Periode Input:</label>
		<input name="bdate" id="bdate" type="text" class="datepicker" title="Tanggal Awal" value="${sekarang}"> s/d 
		<input name="edate" id="edate" type="text" class="datepicker" title="Tanggal Akhir" value="${besok}">
	</div>
	
	<div class="rowElem" id="userBas" style="display: none;">
		<label>User :</label>
		<select name="seluserBas" id="seluserBas" title="Silahkan pilih User">
			<option value="ALL" selected="selected">ALL</option>
			<c:forEach var="u" items="${userBas}" varStatus="c">
				<option value="${u.key}">${u.value}</option>
			</c:forEach>
		</select>
	</div>
	
	<div class="rowElem">
		<label>Jenis Report :</label>
		 <input name="jn_report" id="jn_report1" type="radio" class="radio" value="0" checked="checked" >All	   
	    <input name="jn_report" id="jn_report2" type="radio" class="radio" value="1">Peruser
	</div>
	
	<div class="rowElem">
		<label></label>
		<input type="submit" name="showPDF" id="showPDF" value="Show (PDF)">
		<input type="submit" name="showXLS" id="showXLS" value="Show (Excel)">
		<input type="hidden" name="cabang2" id="cabang2" value="ALL">
		<input type="hidden" name="userinput2" id="userinput2" value="ALL">
		<input type="hidden" name="showReport" value="1">
		<input type="hidden" name="bas" id="bas" value="${bas }">
	</div>
</fieldset>
</form>

</body>
</html>