<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/header_jquery.jsp"%>
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script>
			$().ready(function() {
				$(".datepicker").datepicker({
					changeMonth: true,
					changeYear: true,
					dateFormat: "dd/mm/yy" 
				});
			 });	 
			 
			 hideLoadingMessage();
			 
		</script>
		
	</head>
	
	<body>
		<c:if test="${sessionScope.currentUser.lde_id eq 29}">	
		<form name="formpost" id="formpost" method="post" enctype="multipart/form-data" style='background-color :#FFE1FD'>
			<table class="entry2" width="100%" cellspacing="2" cellpadding="2">
				<tr>
					<td bgcolor="#B40404" style="color: white;" colspan="2"><b>Upload Outstanding</b></td>
				</tr>
				<tr>
					<td>Upload</td>
					<td>
						<input type="file" name="file1" id="file1" size="70" /> .xlsx, .xls
					</td>
				</tr>
				<tr>
					<td>Tanggal aging</td>
					<td>
						<input type="text" id="beg_date" name="beg_date" class="datepicker"> s/d 
						<input type="text" id="end_date" name="end_date" class="datepicker">
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="submit" name="upload" value="Upload" />
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<c:if test="${not empty sts }">
							<font color="blue">${sts}</font>
						</c:if>
						<c:if test="${not empty err }">
							<font color="red">${err}</font>
						</c:if>
					</td>
				</tr>
			</table>
		</form>
		<br>
		</c:if>		
		<form name="formpost2" id="formpost2" method="post" target="_blank">
			<table class="entry2" width="100%" cellspacing="2" cellpadding="2">
				<tr>
					<td bgcolor="#B40404" style="color: white;" colspan="2"><b>Report Aging Followup</b></td>
				</tr>
				<tr>
					<td>Tanggal aging</td>
					<td>
						<input type="text" id="a_beg_date" name="a_beg_date" class="datepicker"> s/d 
						<input type="text" id="a_end_date" name="a_end_date" class="datepicker">
					</td>
				</tr>
				<!-- <tr>
					<td>Tanggal followup</td>
					<td>
						<input type="text" id="f_beg_date" name="f_beg_date" class="datepicker"> s/d 
						<input type="text" id="f_end_date" name="f_end_date" class="datepicker">
					</td>
				</tr> -->
				<tr>
					<td>Jenis</td>
					<td>
						<input type="radio" id="type1" name="type" value="0" checked="checked"><label for="type1">Perkategori</label>
						<input type="radio" id="type2" name="type" value="1"><label for="type2">Peruser</label>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="submit" name="showPDF" id="showPDF" value="Show (PDF)">
						<input type="submit" name="showXLS" id="showXLS" value="Show (Excel)">
						<input type="hidden" name="showReport" value="2">
					</td>
				</tr>
			</table>
				<tr><td>&nbsp;</td></tr>
				<tr><td>&nbsp;</td></tr>
				<table class="entry2" width="100%" cellspacing="2" cellpadding="2">
				<tr><td>Billing :</td></tr>
				<tr><td>- Juni 2016  </td></tr>
				<tr><td>- Mei 2016  </td></tr>
				<tr><td>- April 2016  </td></tr>
				<tr><td>- Maret 2016  </td></tr>
				<tr><td>- Februari 2016  </td></tr>
				<tr><td>- Januari 2016  </td></tr>
				<tr><td>- Desember 2015  </td></tr>
				<tr><td>- November 2015  </td></tr>	
				<tr><td>- Oktober 2015  </td></tr>
				<tr><td>- September 2015  </td></tr>
				<tr><td>- Agustus 2015  </td></tr>
				<tr><td>- Juli 2015  </td></tr>
				<tr><td>- Juni 2015  </td></tr>
				<tr><td>- April s/d Mei 2015  </td></tr>	
				<tr><td>- Februari s/d Maret 2015  </td></tr>	
				<tr><td>- Desember 2014 s/d Januari 2015  </td></tr>
				<tr><td>- Oktober s/d November 2014 </td></tr>
				<tr><td>- Agustus s/d September 2014 </td></tr>
				<tr><td>- Juni s/d July 2014 </td></tr>
				<tr><td>- April s/d May 2014 </td></tr>
				<tr><td>- Oktober 2013 s/d Maret 2014</td></tr>
				</table>
		</form><br>
		<!-- <form name="formpost3" id="formpost3" method="post" target="_blank">
				<table class="entry2" width="100%" cellspacing="2" cellpadding="2">
				<tr>
					<td bgcolor="#B40404" style="color: white;" colspan="2"><b>Report Aging Belum Followup</b></td>
				</tr>
				<tr>
					<td>Tanggal aging</td>
					<td>
						<input type="text" id="a_beg_date_uf" name="a_beg_date_uf" class="datepicker"> s/d 
						<input type="text" id="a_end_date_uf" name="a_end_date_uf" class="datepicker">
					</td>
				</tr>
				<tr>
					<td>Jenis</td>
					<td>
						<input type="radio" id="type3" name="type2" value="0" checked="checked"><label for="type1">Perkategori</label>
						<input type="radio" id="type4" name="type2" value="1"><label for="type2">Peruser</label>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="submit" name="showPDF_uf" id="showPDF_uf" value="Show (PDF)">
						<input type="submit" name="showXLS_uf" id="showXLS_uf" value="Show (Excel)">
						<input type="hidden" name="showReport_uf" value="1">
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr><td>&nbsp;</td></tr>
				<table class="entry2" width="100%" cellspacing="2" cellpadding="2">
				<tr><td>Billing :</td></tr>
				<tr><td>1. Oktober 2013 s/d Maret 2014</td></tr>
				<tr><td>2. April s/d May 2014 </td></tr>
				<tr><td>3. Juni s/d July 2014 </td></tr>
				<tr><td>4. Agustus s/d September 2014 </td></tr>
				<tr><td>5. Oktober s/d November 2014 </td></tr>	
				<tr><td>6. Desember 2014 s/d Januari 2015  </td></tr>	
				<tr><td>7. Februari s/d Maret 2015  </td></tr>	
				<tr><td>8. April s/d Mei 2015  </td></tr>	
				<tr><td>9. Juni 2015  </td></tr>
				<tr><td>10. Juli 2015  </td></tr>
				<tr><td>11. Agustus 2015  </td></tr>
				<tr><td>12. September 2015  </td></tr>
				<tr><td>13. Oktober 2015  </td></tr>
				<tr><td>14. November 2015  </td></tr>	
				</table>			
			</table>
		</form> -->
			
	</body>
</html>
