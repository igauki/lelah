<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type"
			content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<meta HTTP-EQUIV="Expires" CONTENT="-1">
		<link REL="Stylesheet" TYPE="text/css"
			HREF="${path }/include/css/default.css" media="screen">
		<link href="${path}/include/image/eas.ico" rel="shortcut icon">
		<link REL="Stylesheet" TYPE="text/css"
			HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css"
			media="screen">
		<!-- CSS untuk DatePicker Script (jscalendar) -->
		<script type="text/javascript"
			src="${path }/include/js/jscalendar/calendar.js"></script>
		<!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript"
			src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript"
			src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/default.js"></script>
		<!-- Common Javascripts -->
		<script type="text/javascript" src="${path }/dwr/util.js"></script>
		<!-- DWR (Ajax) Utils -->
		<script type="text/javascript"
			src="${path }/include/js/ajaxtags/overlibmws.js"></script>

	</head>
	

	
	<body style="height: 100%;"
		onload="setupPanes('container1', 'tab1'); setFrameSize('reportFrame', 78); setFrameSize('docFrame', 78);"
		onresize="setFrameSize('reportFrame', 78); setFrameSize('docFrame', 78);">
		<div id="contents">
			<form method="post" name="formpost">
			<input type="hidden" name ="tampung" value="${cmd.tampung}">
				<div class="tab-container" id="container1">
					<ul class="tabs" id="tabs" >
						<li>
							<a href="#" onClick="return showPane('pane1', this)" id="tab1">AJS-BANK MUAMALAT</a>
						</li>
						<li>
							<a href="#" onClick="return showPane('pane2', this)" id="tab2">Report</a>
						</li>
						
					</ul>
					<div class="tab-panes">
						<div id="pane1" class="panes">
							<table class="entry2">
								<tr>
									<th nowrap="nowrap">Status</th>
									<td>
										<select name="status" > 
											<option value="AKSEP">AKSEP</option>
											<option value="TOLAK">TOLAK</option>
										</select>
									</td>
								</tr>
								<tr>
									<th>
										Tanggal Efektif Polis
									</th>
									<td>
										<script>inputDate('tglAwal', '${cmd.tglAwal}', false);</script>
										&nbsp;s/d&nbsp;
										<script>inputDate('tglAkhir', '${cmd.tglAkhir}', false);</script>
									<span class="info">* Format Tanggal (DD/MM/YYYY)</span>
									</td>
								</tr>
								<tr>
									<th></th>
									<td>
										<input type="submit" name="show" value="ShowReport">
									</td>
								</tr>
							</table>
						</div>
						<div id="pane2" class="panes">
							<iframe name="reportFrame" id="reportFrame" width="100%"
								src="${path}/${cmd.reportPath}">
								Please Wait...
							</iframe>
						</div>
					</div>
				</div>
			</form>
		</div>
	</body>	
<%@ include file="/include/page/footer.jsp"%>
