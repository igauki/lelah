<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<style>
#loading {
	color: blue;
	background: #ffc;
	border: 1px solid #4682b4;
	font-family: verdana, geneva, arial, helvetica, sans-serif;
	font-size: 7pt;
 	width: 200px;
 	height: 100px;
 	position: absolute;
 	left: 50%;
 	top: 50%;
 	margin-top: -50px;
 	margin-left: -100px;
 	text-align: center;
 	vertical-align: middle;
}
</style>
</head>
<BODY>
	<div id="loading">Please Wait...</div>
</body>
<%@ include file="/include/page/footer.jsp"%>