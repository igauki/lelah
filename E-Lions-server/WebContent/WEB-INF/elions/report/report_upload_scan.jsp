<%@ include file="/include/page/header_jquery.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />

<style type="text/css">
    /* font utama */
    body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

    /* fieldset */
    fieldset { margin-bottom: 1em; padding: 0.5em; }
    fieldset legend { width: 99%; }
    fieldset legend div { margin: 0.3em 0.5em; }
    fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
    fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
    fieldset .rowElem .jtable { position: relative; left: 12.5em; }

    /* tanda bintang (*) untuk menandakan required field */
    em { color: red; font-weight: bold; }

    /* agar semua datepicker align center, dan ukurannya fix */
    .datepicker { text-align: center; width: 7em; }

    /* styling untuk client-side validation error message */
    #formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

    /* styling untuk label khusus checkbox dan radio didalam rowElement */
    fieldset .rowElem label.radioLabel { width: auto; }
    
    /* lebar untuk form elements */
    .lebar { width: 24em; }
    
    /* untuk align center */
    .tengah { text-align: center; }

</style>

<body>
    <form id="formpost" method="POST" target="_blank">
        <fieldset class="ui-widget ui-widget-content">
            <legend class="ui-widget-header ui-corner-all"><div>Report Upload Scan</div></legend>
            
            <div class="rowElem">
                <label for="status">Status :</label>
                <select name="status" id="status">
                    <option value="0">All</option>
                    <option value="1">Further Requirement</option>
                    <option value="2">Akseptasi Khusus</option>
                </select>
            </div>
            
            <div class="rowElem">
                <label for="product">Produk :</label>
                <select name="product" id="product">
                    <option value="0">All</option>
                    <option value="1">Link</option>
                    <option value="2">Non Link</option>
                </select>
            </div>
            
            <div class="rowElem">
                <label for="tglAwal">Tanggal :</label>
                <input type="text" name="tglAwal" id="tglAwal" class="datepicker" 
                    title="Tanggal Awal" value="${tglAwal}"> 
                s/d 
                <input type="text" name="tglAkhir" id="tglAkhir" class="datepicker" 
                    title="Tanggal Akhir" value="${tglAkhir}">
            </div>
            
            <div class="rowElem">
                <label></label>
                <input type="submit" name="showPDF" id="showPDF" value="Show (PDF)">
                <input type="submit" name="showXLS" id="showXLS" value="Show (Excel)">
                <input type="hidden" name="showReport" value="1">
            </div>
        </fieldset>
    </form>
    
    <script>
        $(function() {
            // (Qtip2) init tooltip untuk semua element menggunakan title nya
	        $("input[title], select[title], textarea[title], button[title]").qtip();
	        
	        // (jQueryUI datepicker) init semua field datepicker
	        $(".datepicker").datepicker({
	            changeMonth: true,
	            changeYear: true,
	            dateFormat: "dd/mm/yy" 
	        });
	
	        // (jQueryUI themes) Styling untuk table
	        $(".jtable th").each(function() {
	            $(this).addClass("ui-widget-header"); //bisa juga ui-state-default
	        });
	        $(".jtable td").each(function() {
	            $(this).addClass("ui-widget-content");
	        });
	
	        // (jQueryUI themes) Remove Styling untuk table selain jtable
	        $(".plain th").each(function() {
	            $(this).removeClass();
	        });
	        $(".plain td").each(function() {
	            $(this).removeClass();
	        });
	
	        // (jQueryUI themes) Styling untuk form elements
	        $("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
	        //$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
	        
	        $("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
	        
	        if(parent){
	            if(parent.document){
	                var dZone = parent.document.getElementById('dZone');
	                if(dZone) dZone.style.visibility = 'hidden';
	                window.scrollbars = false;
	            }
	        }
        });
    </script>
</body>
</html>