<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<meta HTTP-EQUIV="Expires" CONTENT="-1">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link href="${path}/include/image/eas.ico" rel="shortcut icon">
		<!--  -->
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<!--  -->
		<script type="text/javascript" src="${path }/include/js/default.js"></script>
		<!-- Ajax Related -->
		<script type="text/javascript" src="${path}/include/js/ajax.js"></script>
		<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script>
		<script type="text/javascript" src="${path }/dwr/engine.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>
		<script type="text/javascript">
			hideLoadingMessage();
		</script>
	</head>
	<body onload="setupPanes('container1','tab1');" style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Bancassurance Support Trainee (BST)  / Bancass Coordinator (BAC)</a>
				</li>
			</ul>

			<div class="tab-panes">

				<div id="pane1" class="panes">

					<c:if test="${not empty command.pesan}">
						<fieldset style="text-align: left;">
							<legend>Pesan Sukses</legend>
							<div id="success" style="text-transform: none;">${command.pesan}</div>
						</fieldset>
					</c:if>

					<form:form method="post" name="formpost" id="formpost" commandName="command" action="${path}/cross_selling/fat.htm?window=main">

					<spring:hasBindErrors name="command">
						<fieldset style="text-align: left;">
							<legend>Pesan Error</legend>
							<div id="error">
								<form:errors path="*" delimiter="<br>" />
							</div>
						</fieldset>
					</spring:hasBindErrors>						
				
					<fieldset style="text-align: left;">
						<legend>Data Pengajuan BST/BAC</legend>

						<table class="entry2" style="width: auto;">
							<tr>
								<th style="width: 100px;">Periode Input/Update:</th>
								<td>
									<script>inputDate('begdate', '${command.begdate}', false);</script>
									 s/d 
									<script>inputDate('enddate', '${command.enddate}', false);</script>
								</td>
								<th style="width: 100px;">Nama:</th>
								<td>
									<form:input path="nama"/>
								</td>
								<th style="width: 100px;">Status:</th>
								<td>
									<spring:bind path="command.posisi">
										<select id="${status.expression}" name="${status.expression}">
											<c:forEach items="${command.listPosisi}" var="p">
												<option style="background-color: ${p.desc}" value="${p.key}" <c:if test="${p.key eq status.value}">selected="selected"</c:if>>${p.value}</option>
											</c:forEach>
										</select>
									</spring:bind>
								</td>
								<th></th>
								<td><input type="submit" name="cari" value="Cari"></td>
							</tr>
						</table>

						<table class="displaytag">
							<thead>
								<tr>
									<th title="Silahkan pilih salah satu untuk melanjutkan proses OTORISASI/PENCETAKAN">Pilih</th>
									<th title="Silahkan pilih salah satu tindakan">Tindakan</th>
									<th title="Status Pengajuan">Status</th>
									<%-- <th title="Cabang Bank">Cab</th> --%>
									<th title="ID">ID</th>
									<th title="Kode Agen">Kd Agen</th>
									<th title="Nama FAT/BAC">Nama</th>
									<th title="Posisi (FAT atau BAC)">Posisi</th>
									<th title="Tanggal Efektif Bekerja">Tgl Aktif</th>
									<%-- <th title="Status Aktif">Aktif</th> --%>
									<th title="Tanggal Data Diupdate Terakhir">Tgl Update</th>
									<th title="Keterangan Terminasi">Keterangan Terminasi</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${command.listFat}" var="f" varStatus="s">
									<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';return true;" onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;">
									
										<td class="center"><form:checkbox cssClass="noBorder" path="listFat[${s.index}].checked"/></td>
										<td class="center">
											<a href="#" onclick="popWin('${path}/cross_selling/fat.htm?window=input&fatid=${f.fatid}', 620, 900); return false;"><img style="border: 0" src="${path}/include/image/view_tts.gif" alt="Lihat / Edit Detil"/></a>
											&nbsp;
											<c:if test="${f.posisi eq 4}">
												<a href="#" onclick="if(confirm('Cetak Surat?'))popWin('${path}/report/bac.pdf?window=surat_fat&fatid=${f.fatid}', 700, 900); return false;"><img style="border: 0" src="${path}/include/image/print.gif" alt="Cetak Surat"/></a>
												&nbsp;
												<a href="#" onclick="if(confirm('Cetak Name Tag?'))popWin('${path}/report/bac.pdf?window=nametag_fat&fatid=${f.fatid}', 700, 900); return false;"><img style="border: 0" src="${path}/include/image/18_tablecell.gif" alt="Cetak Name Tag"/></a>
											</c:if>										
										</td>
										<td class="center" style="white-space: nowrap;">
											<c:forEach var="i" begin="1" end="7" step="1" varStatus="s">
												<c:set var="backgroundColor" value="white" />
												<c:if test="${f.posisi eq i}">
													<c:set var="backgroundColor" value="yellow" />
												</c:if>
												<input type="button" value="${i}" style="background-color: ${backgroundColor}; ">
												<c:if test="${i lt \"7\"}"><img src="${path}/include/image/arrow.gif"></c:if>
											</c:forEach>
										</td>
										<%-- <td class="left">${f.lcb_no}</td> --%>
										<td class="center">${f.fatid}</td>
										<td class="center">${f.kd_agent}</td>
										<td class="left">${f.nama}</td>
										<td class="center">${f.ket_jbt}</td>
										<td class="center"><fmt:formatDate value="${f.act_date}" pattern="dd/MM/yyyy"/></td>
										<%-- <td class="center">${f.is_active}</td> --%>
										<td class="center"><fmt:formatDate value="${f.tgl_update}" pattern="dd/MM/yyyy"/></td>
										<c:if test="${f.posisi ne \"5\"}">
											<td class="left">${f.ket_term}
											<%-- menyesuaikan jumlah list --%>
											<input type="hidden" id="ket_term" name="ket_term" value="" style="width: 1px;"/>
											</td>
										</c:if>
										<c:if test="${f.posisi eq 5}">
											<td class="center"><input type="text" id="ket_term" name="ket_term" value="${ket_term}"/></td>
										</c:if>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						
						<br/>
						<c:choose>
							<c:when test="${jenis eq 0}">
								<input type="button" name="new" value="Tambah Baru" onclick="popWin('${path}/cross_selling/fat.htm?window=input', 620, 900);">
								<input type="submit" name="otorisasi" value="Otorisasi" onclick="return confirm('Anda akan melakukan proses OTORISASI terhadap data yang dipilih.\nData yang sudah di OTORISASI sudah tidak dapat diedit kembali. Lanjutkan?')">
								<input type="submit" name="konfirmasi" value="Konfirmasi" onclick="return confirm('Anda akan melakukan proses KONFIRMASI terhadap data yang dipilih.\nProses KONFIRMASI akan menginstruksikan via email kepada pihak BSM untuk mencetak surat. Lanjutkan?')">
								<input type="submit" name="filling" value="Filling" onclick="return confirm('Anda akan melakukan proses FILLING terhadap data yang dipilih.\nProses FILLING akan menandakan bahwa proses pengajuan sudah selesai dan surat/nametag sudah tidak dapat dicetak kembali. Lanjutkan?')">
								<input type="submit" name="terminate" value="Terminasi" onclick="return confirm('Anda akan melakukan proses TERMINASI terhadap data yang dipilih.\nProses TERMINASI bersifat permanen. Lanjutkan?')">
								<input type="submit" name="konf_term" value="Konfirmasi Terminasi" onclick="return confirm('Anda akan melakukan proses KONFIRMASI TERMINASI terhadap data yang dipilih.\nProses KONFIRMASI TERMINASI bersifat permanen. Lanjutkan?')">
							</c:when>
							<c:when test="${jenis eq 1}">
								<input type="button" name="new" value="Tambah Baru" onclick="popWin('${path}/cross_selling/fat.htm?window=input', 620, 900);">
								<input type="submit" name="otorisasi" value="Otorisasi" onclick="return confirm('Anda akan melakukan proses OTORISASI terhadap data yang dipilih.\nData yang sudah di OTORISASI sudah tidak dapat diedit kembali. Lanjutkan?')">
								<input type="submit" name="filling" value="Filling" onclick="return confirm('Anda akan melakukan proses FILLING terhadap data yang dipilih.\nProses FILLING akan menandakan bahwa proses pengajuan sudah selesai dan surat/nametag sudah tidak dapat dicetak kembali. Lanjutkan?')">
								<input type="submit" name="terminate" value="Terminasi" onclick="return confirm('Anda akan melakukan proses TERMINASI terhadap data yang dipilih.\nProses TERMINASI bersifat permanen. Lanjutkan?')">
							</c:when>
							<c:when test="${jenis eq 2}">
								<input type="button" name="new" value="Tambah Baru" onclick="popWin('${path}/cross_selling/fat.htm?window=input', 620, 900);">
								<input type="submit" name="filling" value="Filling" onclick="return confirm('Anda akan melakukan proses FILLING terhadap data yang dipilih.\nProses FILLING akan menandakan bahwa proses pengajuan sudah selesai dan surat/nametag sudah tidak dapat dicetak kembali. Lanjutkan?')">
								<input type="submit" name="terminate" value="Terminasi" onclick="return confirm('Anda akan melakukan proses TERMINASI terhadap data yang dipilih.\nProses TERMINASI bersifat permanen. Lanjutkan?')">
							</c:when>
						</c:choose>
					</fieldset>
					</form:form>
					
					<fieldset style="text-align: left;">
						<legend>Informasi Flow Status Pengajuan</legend>
						<c:forEach items="${command.listPosisi}" var="p">
							<c:if test="${p.key ne \"0\"}">
								<input type="button" value="${p.value}" style="background-color: ${p.desc}; ">
								<c:if test="${p.key lt \"7\"}">
									&nbsp;<img src="${path}/include/image/arrow.gif">&nbsp;
								</c:if>
							</c:if>
						</c:forEach>
					</fieldset>

				</div>
			</div>
		</div>
	</body>
</html>