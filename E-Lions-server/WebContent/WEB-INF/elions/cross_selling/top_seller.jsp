<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
</script>
</head>
<BODY onload="resizeCenter(800,600); document.title='PopUp :: Top Seller BSIM'; setupPanes('container1', 'tab1'); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Top Seller BSIM</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<table class="simple">
					<caption>IDR</caption>
					<thead>
						<tr>
							<th style="text-align: center">Nama Reff</th>
							<th style="text-align: center">Kurs</th>
							<th style="text-align: center">Premi SP</th>
							<th style="text-align: center">Premi SSL</th>
							<th style="text-align: center">Total Premi</th>
							<th style="text-align: center">Jumlah Polis</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="l" items="${sessionScope.listTopSellerIDR}">
							<tr>
								<td>${l.NAMA_REFF}</td>
								<td>${l.KURS}</td>
								<td style="text-align: right"><fmt:formatNumber value="${l.PREMI_SP}" maxFractionDigits="2" minFractionDigits="0" /></td>
								<td style="text-align: right"><fmt:formatNumber value="${l.PREMI_SS}" maxFractionDigits="2" minFractionDigits="0" /></td>
								<td style="text-align: right"><fmt:formatNumber value="${l.TOT_PREMI}" maxFractionDigits="2" minFractionDigits="0" /></td>
								<td style="text-align: right"><fmt:formatNumber value="${l.JML_POLIS}" maxFractionDigits="0" minFractionDigits="0" /></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<table class="simple">
					<caption>USD</caption>
					<thead>
						<tr>
							<th class="center">Nama Reff</th>
							<th class="center">Kurs</th>
							<th class="center">Premi SP</th>
							<th class="center">Premi SSL</th>
							<th class="center">Total Premi</th>
							<th class="center">Jumlah Polis</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="l" items="${sessionScope.listTopSellerUSD}">
							<tr>
								<td>${l.NAMA_REFF}</td>
								<td>${l.KURS}</td>
								<td style="text-align: right"><fmt:formatNumber value="${l.PREMI_SP}" maxFractionDigits="2" minFractionDigits="0" /></td>
								<td style="text-align: right"><fmt:formatNumber value="${l.PREMI_SS}" maxFractionDigits="2" minFractionDigits="0" /></td>
								<td style="text-align: right"><fmt:formatNumber value="${l.TOT_PREMI}" maxFractionDigits="2" minFractionDigits="0" /></td>
								<td style="text-align: right"><fmt:formatNumber value="${l.JML_POLIS}" maxFractionDigits="0" minFractionDigits="0" /></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<br>
				<input type="button" name="close" value="Close" onclick="window.close();">
			</div>
		</div>
	</div>

</form>
<c:if test="${jml eq 1}">
<script>backToParent('${v1}');</script>
</c:if>
</body>
<%@ include file="/include/page/footer.jsp"%>