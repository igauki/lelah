<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
</script>
</head>
<BODY>

	<fieldset>
		<legend>Komisi</legend>
		<form name="formpost" id="formpost" method="post">
		<table class="entry2" style="width: auto;">
			<tr>
				<th>Pemegang/No Polis</th>
				<td>
					<input type="text" id="cari_komisi" name="cari_komisi" />
					<input type="submit" id="btnCari" name="btnCari" value="Cari"/>
				</td>
			</tr>
		</table>
		</form>
		
		<c:if test="${not empty komisi }">
		<br>
		<table class="entry2" style="text-align: center;">
			<tr>
				<th>SPAJ</th>
				<th>No Polis</th>
				<th>Pemegang</th>
				<th>Komisi</th>
				<th>Tax</th>
				<th>Status</th>
			</tr>
			<c:forEach items="${komisi }" var="k" varStatus="s">
			<tr>
				<td>${k.REG_SPAJ }</td>
				<td>${k.NO_POLIS }</td>
				<td>${k.PEMEGANG }</td>
				<td><fmt:formatNumber value="${k.KOMISI }" /></td>
				<td><fmt:formatNumber value="${k.TAX }"/></td>
				<td>${k.STATUS }</td>
			</tr>
			</c:forEach>
		</table>
		</c:if>
	</fieldset>
</body>
<%@ include file="/include/page/footer.jsp"%>