<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	function cari(){
		if(trim(document.formpost.kata.value)=='') return false;
		else createLoadingMessage();	
	}

	function backToParent(spaj){
		var dok;
		if(self.opener)
			dok = self.opener.document;
		else
			dok = parent.document;
		
		self.opener.window.location = '${path}/cross_selling/input.htm?reg_spaj='+spaj;
		if(self.opener) window.close();
	}

</script>
</head>
<BODY onload="resizeCenter(700,400); document.title='PopUp :: Cari SPAJ'; setupPanes('container1', 'tab1'); document.formpost.kata.select(); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Cari SPAJ</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" action="${path}/cross_selling/main.htm" style="text-align: center;">
					<input type="hidden" name="window" value="cari">
					<table class="entry2">
						<tr>
							<th>Cari:</th>
							<th class="left">
								<select name="tipe">
									<option value="0" <c:if test="${tipe eq \"0\" }">selected</c:if>>No. SPAJ</option>
									<option value="1" <c:if test="${tipe eq \"1\" }">selected</c:if>>No. Polis</option>
									<option value="2" <c:if test="${tipe eq \"2\" }">selected</c:if>>Pemegang</option>
								</select>
								<input type="text" name="kata" size="35" value="${kata}" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
								<input type="submit" name="search" value="Search" onclick="return cari();">
							</th>
						</tr>
					</table>
					<table class="simple">
						<thead>
							<tr>
								<th class="center">No. SPAJ</th>
								<th class="center">No. Polis</th>
								<th class="center">Pemegang Polis</th>
								<th class="center">Posisi</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="spaj" items="${listSpaj}" varStatus="stat">
								<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
									onclick="backToParent('${spaj.REG_SPAJ}');">
									<td><elions:spaj nomor="${spaj.REG_SPAJ}"/></td>
									<td>${spaj.MSCS_POLICY_NO}</td>
									<td>${spaj.MSCS_HOLDER}</td>
									<td>${spaj.LSPD_POSITION}</td>
								</tr>
								<c:set var="jml" value="${stat.count}"/>
								<c:if test="${stat.count eq 1}">
									<c:set var="v1" value="${spaj.REG_SPAJ}"/>
								</c:if>
							</c:forEach>
						</tbody>
					</table>
					<br>
					<input type="button" name="close" value="Close" onclick="tutup();">

				</form>
			</div>
		</div>
	</div>

</form>
<c:if test="${jml eq 1}">
<script>backToParent('${v1}');</script>
</c:if>
</body>
<%@ include file="/include/page/footer.jsp"%>