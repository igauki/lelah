<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<meta HTTP-EQUIV="Expires" CONTENT="-1">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link href="${path}/include/image/eas.ico" rel="shortcut icon">
		<!--  -->
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<!--  -->
		<script type="text/javascript" src="${path }/include/js/default.js"></script>
		<!-- Ajax Related -->
		<script type="text/javascript" src="${path}/include/js/ajax.js"></script>
		<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script>
		<script type="text/javascript" src="${path }/dwr/engine.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>
		<script type="text/javascript">
			hideLoadingMessage();
			
			function onload(){
				<spring:hasBindErrors name="cmd">
					alert('Data yang dimasukkan tidak lengkap. Silahkan lihat bagian paling bawah halaman untuk melengkapi data yang kurang. Terima kasih.');
					if(document.formpost.elements['policyCs.lsbs_lsdbs'].value != '') document.getElementById('hideAble').style.display = 'block';			
				</spring:hasBindErrors>
				
				<c:if test="${not empty param.pesan}">
					alert('${param.pesan}');
					if(document.formpost.elements['policyCs.lsbs_lsdbs'].value != '') document.getElementById('hideAble').style.display = 'block';			
				</c:if>
				
				<c:if test="${not empty param.pesanPolisLanjutanAtoBukan}">
					document.getElementById('temp_lanjutan').click();
				</c:if>
					
			}
			
			//tes
			function kodeagenbyholder(mscs_holder){
				ajaxManager.selectKodeAgenByHolder(mscs_holder, {
				  callback:function(map) {
					DWRUtil.useLoadingMessage();
					if(map.errMessage) {
						alert(map.errMessage);
					}else {
						document.formpost.elements['policyCs.daftarAgent[0].msag_id'].value = map.ID;
						document.getElementById("keterangan").innerHTML = "SPAJ : "+map.REG_SPAJ+" | Produk : "+map.PRODUK;
						populateAgentCrossSelling(map.ID);
					}
				   },
				  timeout:180000,
				  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
				});
			}
			
			function kodeagenbyspaj(reg_spaj){
				ajaxManager.selectKodeAgenBySpaj(reg_spaj, {
				  callback:function(map) {
					DWRUtil.useLoadingMessage();
					if(map.errMessage) {
						alert(map.errMessage);
					}else {
						document.formpost.elements['policyCs.daftarAgent[0].msag_id'].value = map.ID;
						document.getElementById("keterangan").innerHTML = "SPAJ : "+reg_spaj+" | Pemegang : "+map.PEMEGANG;
						populateAgentCrossSelling(map.ID);
					}
				   },
				  timeout:180000,
				  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
				});
			}
			//end
			
			function populateUmur(tgl_lahir){
				ajaxManager.hitung_umur(tgl_lahir, {
				  callback:function(hasil) {
					DWRUtil.useLoadingMessage();
					if(hasil) {
						document.formpost.elements['policyCs.mscs_age'].value = hasil.umur;
					}
				   },
				  timeout:180000,
				  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
				});
			}
			
			function populateEndDateCrossSelling(beg_date, ins_period){
				ajaxManager.populateEndDateCrossSelling(beg_date, ins_period, {
				  callback:function(hasil) {
					DWRUtil.useLoadingMessage();
					if(hasil) document.formpost.elements['policyCs.mscs_end_date'].value = hasil;
				   },
				  timeout:180000,
				  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
				});
			}
			
			function populateAgentCrossSelling(msag_id){
				if(msag_id){
					if(msag_id.length == 6){
						ajaxManager.populateAgentCrossSelling(msag_id, {
						  callback:function(map) {
							DWRUtil.useLoadingMessage();
							if(map.errMessage) {
								alert(map.errMessage);
								document.formpost.elements['policyCs.daftarAgent[0].msag_id'].value = '';
								document.formpost.elements['policyCs.daftarAgent[0].mcl_first'].value = '';
								document.formpost.elements['policyCs.daftarAgent[0].lca_nama'].value = '';
							}else {
								document.formpost.elements['policyCs.daftarAgent[0].msag_id'].value = map.ID;
								document.formpost.elements['policyCs.daftarAgent[0].mcl_first'].value = map.NAMA;
								document.formpost.elements['policyCs.daftarAgent[0].lsrg_nama'].value = map.REGION;
							}
						   },
						  timeout:180000,
						  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
						});
					}
				}
			}
			
			function populateFields(lsbs_lsdbs){
				if(lsbs_lsdbs != ''){
					ajaxManager.populateFieldsCrossSelling(lsbs_lsdbs, {
					  callback:function(map) {
						DWRUtil.useLoadingMessage();
						
						//ins period, pay period, flag_uppremi
						document.formpost.elements['policyCs.mscs_ins_period'].value = map.mspo_ins_period;
						document.formpost.elements['policyCs.mscs_pay_period'].value = map.mspo_pay_period;
	
						var beg_date = document.formpost.elements['policyCs.mscs_beg_date'].value;
						var ins_period = document.formpost.elements['policyCs.mscs_ins_period'].value;
						if(beg_date != '' && ins_period != ''){
							populateEndDateCrossSelling(beg_date, ins_period);
						}
	
						//dibuka semua
						/*
						if(map.flag_uppremi == 0){ //isian up
							document.formpost.elements['policyCs.mscs_tsi'].readOnly = false;
							document.formpost.elements['policyCs.mscs_tsi'].className = '';
							document.formpost.elements['policyCs.mscs_premium'].readOnly = true;
							document.formpost.elements['policyCs.mscs_premium'].className = 'readOnly';
						}else if(map.flag_uppremi == 1){ //isian premi
							document.formpost.elements['policyCs.mscs_tsi'].readOnly = true;
							document.formpost.elements['policyCs.mscs_tsi'].className = 'readOnly';
							document.formpost.elements['policyCs.mscs_premium'].readOnly = false;
							document.formpost.elements['policyCs.mscs_premium'].className = '';
						}else if(map.flag_uppremi == 2){ //isian up & premi
							document.formpost.elements['policyCs.mscs_tsi'].readOnly = false;
							document.formpost.elements['policyCs.mscs_tsi'].className = '';
							document.formpost.elements['policyCs.mscs_premium'].readOnly = false;
							document.formpost.elements['policyCs.mscs_premium'].className = '';
						}
						*/
						
						//lscb_id
						var text = '<select name="policyCs.lscb_id">';
						for(i=0; i<map.lst_pay_mode.length; i++){
							text += '<option value="'+map.lst_pay_mode[i].LSCB_ID+'">'+map.lst_pay_mode[i].LSCB_PAY_MODE+'</option>';
						}
						text += '</select>';
						if(document.getElementById('lscb_id')){
							document.getElementById('lscb_id').innerHTML=text;
						}
						
						//lku_id
						var text = '<select name="policyCs.lku_id">';
						for(i=0; i<map.lst_kurs.length; i++){
							text += '<option value="'+map.lst_kurs[i].LKU_ID+'">'+map.lst_kurs[i].LKU_SYMBOL+'</option>';
						}
						text += '</select>';
						
						if(document.getElementById('lku_id')){
							document.getElementById('lku_id').innerHTML=text;
						}
						
						//terakhir, baru unhide semuanya
						document.getElementById('hideAble').style.display = 'block';
					   },
					  timeout:180000,
					  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
					});
				}else{
					//hide semuanya bila produk tidak dipilih
					document.getElementById('hideAble').style.display = 'none';				
				}	
			}
			
			function lanjutan_ato_bukan(){
				doIt=confirm('Simpan SPAJ ini?');
				if(doIt){
						var birth_date = document.formpost.elements['policyCs.mscs_birth_date'].value;
						var nama_pp = document.formpost.elements['policyCs.mscs_holder'].value;
						ajaxManager.selectMstPolicyCs(nama_pp, birth_date, {
				  			callback:function(hasil) {
							DWRUtil.useLoadingMessage();
						if(hasil) {
							lanjutanOrNot=confirm("Apakah polis ini merupakan polis lanjutan dari polis dgn no " + hasil.MSCS_POLICY_NO + " dgn info sbb : \n" + hasil.MSCS_HOLDER + "\n" + hasil.MSCS_BIRTH_DATE + "\n" + hasil.LSDBS_NAME + "\n" + hasil.MSCS_PREMIUM + "\n" + hasil.MSCS_TSI + "\n" + hasil.MSCS_BEG_DATE + " " + hasil.MSCS_END_DATE);
							if(lanjutanOrNot){
								document.getElementById('tamp_lanjutan_or_not').value = 'yes';
								document.getElementById('save_spaj').click();
							}else{
								document.getElementById('tamp_lanjutan_or_not').value = 'no';
								document.getElementById('save_spaj').click();
							}
						}else{
							document.getElementById('save_spaj').click();
						}
				   	},
				  timeout:180000,
				  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
				});
  				}
			}
		</script>
	</head>
	<body onload="setupPanes('container1','tab1'); onload();" style="height: 100%;">
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">SPAJ</a>
				</li>
				<li>
					<a href="#" onClick="return showPane('pane2', this)">Histori Dokumen</a>
				</li>
				<c:if test="${sessionScope.currentUser.lus_id eq cmd.policyCs.lus_id}">
					<li>
						<a href="#" onClick="return showPane('pane3', this)">Informasi Lain</a>
					</li>
				</c:if>
				<li>
					<a href="#" onClick="return showPane('pane4', this)" id="tab4">Komisi</a>
				</li>
			</ul>

			<div class="tab-panes">

				<div id="pane1" class="panes">
					<form:form commandName="cmd" name="formpost" id="formpost">
						<fieldset>
							<legend>Polis</legend>
							<table class="entry2">
								<tr>
									<th nowrap="nowrap">
										Nomor Register SPAJ
									</th>
									<td>
										<form:input path="policyCs.reg_spaj" size="18" readonly="true" cssClass="readOnly" cssErrorClass="inpError"/>
									</td>
									<th nowrap="nowrap">
										Nomor Polis ASM
									</th>
									<td>
										<form:input path="policyCs.mscs_policy_no" size="25" cssErrorClass="inpError"/>
									</td>
									<th nowrap="nowrap">
										User Input
									</th>
									<td>
										<form:input path="policyCs.lus_login_name" size="50" readonly="true" cssClass="readOnly" cssErrorClass="inpError"/>
									</td>
								</tr>
								<tr>
									<td colspan="3">
										<div id="success">Posisi Dokumen : ${cmd.policyCs.lspd_position}</div>
									</td>
									<td colspan="3">
										<div id="success">Posisi Berikutnya : ${nextPosition}</div>
									</td>
								</tr>
							</table>
						</fieldset>
						<fieldset>
							<legend>Produk</legend>
							<table class="entry2">
								<tr>
									<th nowrap="nowrap" style="width: 180px;">
										Produk<span style="color: red;"> *</span>
									</th>
									<td>
										<form:select path="policyCs.lsbs_lsdbs" multiple="true" size="8" onchange="populateFields(this.value);" cssErrorClass="inpError">
											<form:option value="" label=""/>
											<form:options items="${listBisnis}" itemValue="key" itemLabel="value"/>
										</form:select>
									</td>
								</tr>
							</table>
						</fieldset>
						<div id="hideAble" style="display: none;">
							<fieldset>
								<legend>Detail Produk</legend>
								<table class="entry2">						
									<tr>
										<th nowrap="nowrap" style="width: 180px;">
											Lama Pertanggungan
										</th>
										<td style="width: 300px;">
											<form:input path="policyCs.mscs_ins_period" size="5" readonly="true" cssClass="readOnly"  cssErrorClass="inpError"/> Thn
										</td>
										<th nowrap="nowrap">
											Lama Pembayaran
										</th>
										<td>
											<form:input path="policyCs.mscs_pay_period" size="5" readonly="true" cssClass="readOnly"  cssErrorClass="inpError"/> Thn	
										</td>
									</tr>
									<tr>
										<th nowrap="nowrap">
											Tanggal Mulai Pertanggungan<span style="color: red;"> *</span>
										</th>
										<td>
											<spring:bind path="cmd.policyCs.mscs_beg_date">
												<script>inputDate('${status.expression}', '${status.value}', false, 'populateEndDateCrossSelling(document.formpost.elements[\'policyCs.mscs_beg_date\'].value, document.formpost.elements[\'policyCs.mscs_ins_period\'].value)');</script>
											</spring:bind> 
										</td>
										<th nowrap="nowrap">
											Tanggal Akhir Pertanggungan
										</th>
										<td>
											<form:input path="policyCs.mscs_end_date" size="15" readonly="true" cssClass="readOnly" cssErrorClass="inpError"/>
										</td>
									</tr>
									<tr>
										<th nowrap="nowrap">
											Cara Bayar<span style="color: red;"> *</span>
										</th>
										<td>
											<div id="lscb_id">
												<form:select path="policyCs.lscb_id" items="${listPayMode}" itemValue="key" itemLabel="value" cssErrorClass="inpError" />
											</div>
										</td>
										<th nowrap="nowrap">
											Kurs<span style="color: red;"> *</span>
										</th>
										<td>
											<div id="lku_id">
												<form:select path="policyCs.lku_id" items="${listKurs}" itemValue="key" itemLabel="value" cssErrorClass="inpError" />
											</div>
										</td>
									</tr>
									<tr>
										<th nowrap="nowrap">
											UP<span style="color: red;"> *</span>
										</th>
										<td>
											<form:input path="policyCs.mscs_tsi" cssErrorClass="inpError" size="25"/>
										</td>
										<th nowrap="nowrap">
											Premi<span style="color: red;"> *</span>
										</th>
										<td>
											<form:input path="policyCs.mscs_premium" cssErrorClass="inpError" size="25"/>
											<label for="atuo"><form:checkbox id="atuo" path="policyCs.auto" value="1" cssClass="noBorder"/>Hitung Otomatis</label>
										</td>
									</tr>
								</table>
							</fieldset>
							<fieldset>
								<legend>Pemegang</legend>
								<table class="entry2">
									<tr>
										<th nowrap="nowrap" style="width: 180px;">
											Nama Pemegang Polis<span style="color: red;"> *</span>
										</th>
										<td>
											<form:input path="policyCs.mscs_holder" size="50" maxlength="100" cssErrorClass="inpError" onblur="kodeagenbyholder(this.value);"/>
										</td>
										<th align="left" colspan="2"></th>
									</tr>
									<tr>
										<th nowrap="nowrap">
											Tanggal Lahir<span style="color: red;"> *</span>
										</th>
										<td style="width: 300px;">
											<spring:bind path="cmd.policyCs.mscs_birth_date">
												<script>inputDate('${status.expression}', '${status.value}', false, 'populateUmur(document.formpost.elements[\'policyCs.mscs_birth_date\'].value)');</script>
											</spring:bind>
										</td>
										<th nowrap="nowrap" style="width: 180px;">
											Umur Pemegang Polis
										</th>
										<td>
											<form:input path="policyCs.mscs_age" size="5" readonly="true" cssClass="readOnly" cssErrorClass="inpError" /> Thn									
										</td>
									</tr>
								</table>
							</fieldset>
							<fieldset>
								<legend>Informasi Kode Agen</legend>
								<table class="entry2">
									<tr>
										<th nowrap="nowrap" style="width: 180px;">
											Cari Kode Agen
										</th>
										<td>
											<input type="text" name="reg_spaj_lama" id="reg_spaj_lama" size="50" maxlength="100" cssErrorClass="inpError" onblur="kodeagenbyspaj(this.value);">
											<span style="color: red;"><em> *masukkan nomor spaj sebelumnya</em></span>
										</td>
										<th align="left" colspan="2"></th>
									</tr>
									<tr>
										<th nowrap="nowrap" style="width: 180px;">
											Info
										</th>
										<td>
											<div id="keterangan"></div>
										</td>
										<th align="left" colspan="2"></th>
									</tr>
								</table>
							</fieldset>
							<fieldset>
								<legend>Informasi Lain</legend>
								<table class="entry2">
									<tr>
										<th nowrap="nowrap" style="width: 180px;">
											Agen Penutup<span style="color: red;"> *</span>
										</th>
										<td colspan="3">
											<table class="displaytag" style="width: auto;">
												<thead>
													<tr>
														<th>Kode Agen</th>
														<th>Nama Agen</th>
														<th>Region</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${cmd.policyCs.daftarAgent}" var="a" varStatus="st">
														<tr>
															<td>
																<c:choose>
																	<c:when test="${st.index eq 0}">
																		<form:input path="policyCs.daftarAgent[${st.index}].msag_id" size="10" maxlength="6" cssErrorClass="inpError" onblur="populateAgentCrossSelling(this.value);"/>
																	</c:when>
																	<c:otherwise>
																		<form:input path="policyCs.daftarAgent[${st.index}].msag_id" size="10" maxlength="6" readonly="true" cssClass="readOnly" cssErrorClass="inpError"/>
																	</c:otherwise>
																</c:choose>
															</td>
															<td>
																<form:input path="policyCs.daftarAgent[${st.index}].mcl_first" size="75" readonly="true" cssClass="readOnly" cssErrorClass="inpError"/>
															</td>
															<td>
																<form:input path="policyCs.daftarAgent[${st.index}].lsrg_nama" size="40" readonly="true" cssClass="readOnly" cssErrorClass="inpError"/>
															</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</td>
									</tr>
									<tr>
										<th nowrap="nowrap" style="width: 180px;">
											Tanggal SPAJ<span style="color: red;"> *</span>
										</th>
										<td style="width: 300px;">
											<spring:bind path="cmd.policyCs.mscs_spaj_date">
												<script>inputDate('${status.expression}', '${status.value}', false);</script>
											</spring:bind> 
										</td>
										<th nowrap="nowrap">
											Tanggal Input
										</th>
										<td>
											<fmt:formatDate value="${cmd.policyCs.mscs_input_date}" pattern="dd/MM/yyyy (HH:mm)"/>
										</td>
									</tr>
									<tr>
										<th nowrap="nowrap">
											Tanggal TTP
										</th>
										<td>
											<spring:bind path="cmd.policyCs.mscs_ttp_date">
												<script>inputDate('${status.expression}', '${status.value}', false);</script>
											</spring:bind> 
										</td>
										<th nowrap="nowrap">
											Tanggal Bayar
										</th>
										<td>
											<spring:bind path="cmd.policyCs.mscs_pay_date">
												<script>inputDate('${status.expression}', '${status.value}', false);</script>
											</spring:bind> 
										</td>
									</tr>
									<tr>
										<th nowrap="nowrap">
											Status
										</th>
										<td>
											<spring:bind path="cmd.policyCs.mscs_policy_type">
								        		<select name="${status.expression}">
								        			<option value="0" <c:if test="${status.value eq 0}"> selected="selected"</c:if>>Baru</option>
								        			<option value="1" <c:if test="${status.value eq 1}"> selected="selected"</c:if>>Perpanjangan</option>
								        		</select>
								        	</spring:bind>
										</td>
									</tr>
								</table>
							</fieldset>
						</div>
						<table class="entry2" style="width: 100%;">
							<tr>
								<td style="text-align: left;">
									<input type="button" name="new"		value="Baru" onclick="window.location='${path}/cross_selling/input.htm';">
									<input type="button" name="search"	value="Cari" onclick="popWin('${path}/cross_selling/main.htm?window=cari', 400, 700);">
									<c:choose>
										<c:when test="${empty cmd.policyCs.lspd_id}"> <!-- Baru -->
											<input type="button" name="save_spaj_temp"	value="Simpan" onclick="lanjutan_ato_bukan();">
											<input type="hidden" name="tamp_lanjutan_or_not" id="tamp_lanjutan_or_not"/>
											<input type="submit" name="save_spaj" id="save_spaj" style="visibility: hidden;"/>
										</c:when>
										<c:when test="${cmd.policyCs.lspd_id eq 79}"> <!-- INPUT SPAJ (CROSS-SELLING) -->
											<input type="submit" name="save_spaj"	value="Simpan" onclick="return confirm('Simpan SPAJ ini?');">
											<input type="submit" name="transfer" 	value="Transfer" onclick="return confirm('Anda yakin transfer ke ${nextPosition}?');">
										</c:when>
										<c:when test="${cmd.policyCs.lspd_id eq 80}"> <!-- INPUT NOPOL ASM & TERIMA TTP (CROSS-SELLING) -->
											<input type="submit" name="save_nopol"	value="Simpan" onclick="return confirm('Simpan Nomor Polis dan Tanggal TTP?');">
											<input type="submit" name="transfer" 	value="Transfer" onclick="return confirm('Anda yakin transfer ke ${nextPosition}?');">
										</c:when>
										<c:when test="${cmd.policyCs.lspd_id eq 81}"> <!-- INPUT TGL PAYMENT/BSB (CROSS-SELLING) -->
											<input type="submit" name="save_ttp"	value="Simpan" onclick="return confirm('Simpan Tanggal Payment/BSB ?');">
											<input type="submit" name="transfer" 	value="Transfer" onclick="return confirm('Anda yakin transfer ke ${nextPosition}?');">
										</c:when>
										<c:when test="${cmd.policyCs.lspd_id eq 82}"> <!-- KONFIRMASI ASM (CROSS-SELLING) -->
											<div id="success">Polis berada di posisi ${cmd.policyCs.lspd_position}. Silahkan masuk ke menu KONFIRMASI ASM.</div>
										</c:when>
										<c:when test="${cmd.policyCs.lspd_id eq 8}"> <!-- AGENT COMMISSION -->
											<div id="success">Polis berada di posisi ${cmd.policyCs.lspd_position}. Silahkan masuk ke menu PEMBAYARAN KOMISI (FINANCE).</div>
										</c:when>
										<c:when test="${cmd.policyCs.lspd_id eq 99}"> <!-- FILLING -->
											<div id="success">Polis berada di posisi ${cmd.policyCs.lspd_position}.</div>
										</c:when>
										<c:otherwise></c:otherwise>
									</c:choose>
								</td>
								<td style="text-align: right;">
									<input type="button" name="payment" value="Konfirmasi ASM &raquo;" onclick="window.location='${path}/cross_selling/main.htm?window=konfirmasi';">
									<input type="button" name="report" value="Summary Input &raquo;" onclick="window.location='${path}/report/bas.htm?window=cross_selling_input';">
									<input type="button" name="report" value="Summary Komisi &raquo;" onclick="window.location='${path}/report/bas.htm?window=cross_selling_komisi';">
								</td>
							</tr>
							<spring:hasBindErrors name="cmd">
								<tr>
									<td colspan="2">
										<div id="error">
											<strong>Data yang dimasukkan tidak lengkap. Mohon lengkapi data-data berikut:</strong>
											<br />
											<form:errors path="*" delimiter="<br>" />
										</div>
									</td>
								</tr>
							</spring:hasBindErrors>
						</table>
					</form:form>
				</div>
				
				<div id="pane2" class="panes">
					<fieldset>
						<legend>Histori Dokumen</legend>
						<table class="entry2" style="width: auto;">
							<tr>
								<th>Flow</th>
								<th>History</th>
							</tr>
							<tr>
								<th>
									<c:forEach items="${listPosisi}" var="p" varStatus="st">
										<c:if test="${st.index ne 0}"><img src="${path}/include/image/arrow_down.png"/><br/></c:if>
										<input type="button" value="${p.value}" style="width: 275px; background-color: white; margin: 5px 5px 5px 5px;"><br/>
									</c:forEach>
								</th>
								<td style="vertical-align: top">
									<display:table id="detail" name="cmd.policyCs.historiPosisi"
										class="displaytag">
										<display:column property="mspc_date" title="Tanggal" format="{0, date, dd/MM/yyyy (HH:mm)}" />
										<display:column property="lus_login_name" title="User" style="text-align: left;" />
										<display:column property="lspd_position" title="Posisi" style="text-align: left;" />
										<display:column property="mspc_desc" title="Keterangan" style="text-align: left;" />
									</display:table>
								</td>
							</tr>
						</table>
					</fieldset>
				</div>

				<c:if test="${sessionScope.currentUser.lus_id eq cmd.policyCs.lus_id}">
					<div id="pane3" class="panes">
						<fieldset>
							<legend>Informasi Produksi</legend>
							<table class="displaytag" style="width: 60%;">
								<thead>
								<tr>
									<th>Tanggal Produksi</th>
									<th>Kurs</th>
									<th>UPP Evaluasi</th>
									<th>UPP Kontes</th>
									<th>UPP Lain</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td><fmt:formatDate value="${cmd.policyCs.mscs_prod_date}" pattern="dd/MM/yyyy"/></td>
									<td><fmt:formatNumber value="${cmd.policyCs.mscs_prod_kurs}"/></td>
									<td><fmt:formatNumber value="${cmd.policyCs.mscs_upp_eva}"/></td>
									<td><fmt:formatNumber value="${cmd.policyCs.mscs_upp_kontes}"/></td>
									<td><fmt:formatNumber value="${cmd.policyCs.mscs_upp_lain}"/></td>
								</tr>
								</tbody>
							</table>
						</fieldset>
						<fieldset>
							<legend>Informasi Komisi</legend>
							<table class="displaytag" style="width: 60%;">
								<thead>
								<tr>
									<th>Tanggal Bayar</th>
									<th>Komisi</th>
									<th>Pajak</th>
									<th>Kurs</th>
									<th>Status Bayar</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td><fmt:formatDate value="${cmd.policyCs.mscs_comm_pay_date}" pattern="dd/MM/yyyy"/></td>
									<td><fmt:formatNumber value="${cmd.policyCs.mscs_comm}"/></td>
									<td><fmt:formatNumber value="${cmd.policyCs.mscs_comm_tax}"/></td>
									<td><fmt:formatNumber value="${cmd.policyCs.mscs_comm_kurs}"/></td>
									<td>${cmd.policyCs.mscs_comm_paid}</td>
								</tr>
								</tbody>
							</table>
						</fieldset>
					</div>
				</c:if>
				
				<div id="pane4" class="panes" style="text-align: left;">
					<iframe src="${path }/cross_selling/main.htm?window=cari_komisi" width="100%" height="100%"></iframe>
				</div>
				
			</div>
		</div>

	</body>
</html>