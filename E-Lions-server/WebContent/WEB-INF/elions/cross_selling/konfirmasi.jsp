<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
<script type="text/javascript" src="${path }/include/js/default.js"></script>
<script type="text/javascript" src="${path }/dwr/util.js"></script>
<script type="text/javascript">
	hideLoadingMessage();

	function cari(){
		if(trim(document.formpost.kata.value)=='') return false;
		else createLoadingMessage();	
	}
</script>
</head>
<BODY onload="setupPanes('container1', 'tab1');" style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Konfirmasi ASM (Input Tgl Terima ASM)</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form:form commandName="cmd" action="${path}/cross_selling/main.htm" cssStyle="text-align: center;" name="formpost">
					<input type="hidden" name="window" value="konfirmasi">
					<table class="entry2">
						<tr>
							<th>Cari Berdasarkan Nomor SPAJ / Polis:</th>
							<td class="left">
								<form:input path="reg_spaj" size="35" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}"/>
							</td>
						</tr>
						<tr>
							<th>Cari Berdasarkan Nama Pemegang Polis:</th>
							<td class="left">
								<form:input path="mscs_holder" size="35" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}"/>
							</td>
						</tr>
						<tr>
							<th>Cari Berdasarkan Tanggal Input:</th>
							<td class="left">
								<spring:bind path="cmd.startDate">
									<script>inputDate('${status.expression}', '${status.value}', false);</script>
								</spring:bind>
								 s/d 
								<spring:bind path="cmd.endDate">
									<script>inputDate('${status.expression}', '${status.value}', false);</script>
								</spring:bind>
							</td>
						</tr>
						<tr>
							<th></th>
							<td class="left">
								<input type="submit" name="search" value="Search" onclick="return cari();">
							</td>
						</tr>
						<c:if test="${not empty cmd.daftarSpaj}">
							<tr>
								<td colspan="2">
									<table class="simple">
										<thead>
											<tr>
												<th style="text-align: center;">X</th>
												<th style="text-align: center;">Tgl Terima ASM</th>
												<th style="text-align: center;">No. SPAJ</th>
												<th style="text-align: center;">No. Polis</th>
												<th style="text-align: center;">Produk</th>
												<th style="text-align: center;">Pemegang Polis</th>
												<th style="text-align: center;">UP</th>
												<th style="text-align: center;">Premi</th>
												<%--
												<th style="text-align: center;">Masa Pertanggungan</th>
												<th style="text-align: center;">Tgl Input</th>
												<th style="text-align: center;">User Input</th>
												--%>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="spaj" items="${cmd.daftarSpaj}" varStatus="stat">
												<tr>
													<td class="nowrap" style="text-align: center;">
														<form:checkbox path="daftarSpaj[${stat.index}].selected" value="1" cssClass="noBorder"/>
													</td>
													<td class="nowrap" style="text-align: center;">
														<spring:bind path="cmd.daftarSpaj[${stat.index}].mscs_tgl_terima_asm">
															<script>inputDate('${status.expression}', '${status.value}', false);</script>
														</spring:bind>
													</td>
													<td class="nowrap" style="text-align: center;">
														<form:input path="daftarSpaj[${stat.index}].reg_spaj" size="15" readonly="true"/>
													</td>
													<td class="nowrap" style="text-align: center;">
														<form:input path="daftarSpaj[${stat.index}].mscs_policy_no" size="20" readonly="true"/>
													</td>
													<td class="nowrap">
														<form:input path="daftarSpaj[${stat.index}].lsbs_name" size="25" readonly="true"/>
														<form:input path="daftarSpaj[${stat.index}].lsdbs_name" size="35" readonly="true"/>
													</td>
													<td class="nowrap">
														<form:input path="daftarSpaj[${stat.index}].mscs_holder" size="35" readonly="true"/>
													</td>
													<td class="nowrap" style="text-align: right;"><fmt:formatNumber value="${spaj.mscs_tsi}"/></td>
													<td class="nowrap" style="text-align: right;"><fmt:formatNumber value="${spaj.mscs_premium}"/></td>
													<%--
													<td><fmt:formatDate value="${spaj.mscs_beg_date}" pattern="dd/MM/yyyy"/> s/d <fmt:formatDate value="${spaj.mscs_end_date}" pattern="dd/MM/yyyy"/></td>
													<td style="text-align: center;"><fmt:formatDate value="${spaj.mscs_input_date}" pattern="dd/MM/yyyy (HH:mm)"/></td>
													<td>${spaj.lus_login_name}</td>
													--%>
												</tr>
											</c:forEach>
										</tbody>
									</table>
									<br/>
									<input type="submit" name="save" value="Simpan dan Transfer ke PROSES KOMISI FINANCE" onclick="return confirm('Simpan perubahan?\nData yang dipilih (di-CEK) akan ditransfer ke PROSES KOMISI FINANCE. Harap pastikan TANGGAL TERIMA ASM yang anda masukkan benar.');">					
								</td>
							</tr>
						</c:if>
					</table>
					<input type="hidden" name="ukuran" value="${ukuran}">
				</form:form>
				<br/>
				<input type="button" name="payment" value="&laquo; Kembali ke Input SPAJ" onclick="window.location='${path}/cross_selling/input.htm';">
			</div>
		</div>
	</div>
</form>
<c:if test="${not empty pesan}">
	<script>alert('${pesan}');</script>
</c:if>
</body>
<%@ include file="/include/page/footer.jsp"%>