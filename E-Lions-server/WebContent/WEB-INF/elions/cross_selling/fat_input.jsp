<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<meta HTTP-EQUIV="Expires" CONTENT="-1">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link href="${path}/include/image/eas.ico" rel="shortcut icon">
		<!--  -->
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<!--  -->
		<script type="text/javascript" src="${path }/include/js/default.js"></script>
		<!-- Ajax Related -->
		<script type="text/javascript" src="${path}/include/js/ajax.js"></script>
		<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script>
		<script type="text/javascript" src="${path }/dwr/engine.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>
		<script type="text/javascript">
			hideLoadingMessage();
			
			function awal(){
				<c:if test="${not empty command.pesan}">
					window.alert('${command.pesan}');
				</c:if>
			}
			
		</script>
	</head>
	<body onload="setupPanes('container1','tab1'); awal();" style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Input Pengajuan Bancassurance Support Trainee (BST) / Bancass Coordinator (BAC)</a>
				</li>
			</ul>

			<div class="tab-panes">

				<div id="pane1" class="panes">

					<c:if test="${not empty command.pesan}">
						<fieldset style="text-align: left;">
							<legend>Pesan Sukses</legend>
							<div id="success" style="text-transform: none;">${command.pesan}</div>
						</fieldset>
					</c:if>

					<form:form method="post" name="formpost" id="formpost" commandName="command" action="${path}/cross_selling/fat.htm?window=input">

					<spring:hasBindErrors name="command">
						<fieldset style="text-align: left;">
							<legend>Pesan Error</legend>
							<div id="error">
								<form:errors path="*" delimiter="<br>" />
							</div>
						</fieldset>
					</spring:hasBindErrors>						
				
					<table>
						<tr>
							<td style="vertical-align: top;">
								<fieldset>
									<legend>Input Pengajuan BST/BAC</legend>
			
									<table class="entry2" style="width: auto;">
										<tr>
											<th style="width: 225px">FAT ID:</th>
											<td><form:input cssStyle="text-align: center" maxlength="12" size="18" readonly="readOnly" cssClass="readOnly" path="fat.fatid" cssErrorClass="inpError"/></td>
										</tr>
										<tr><td colspan="2" style="background-color: black"></td></tr>
										<tr>
											<th>Jabatan:<span class="error">*</span></th>
											<td>
												<c:choose>
													<c:when test="${not empty command.fat.fatid}">
														<form:select path="fat.ket_jbt" items="${listJabatan}" itemValue="key" itemLabel="value" cssErrorClass="inpError" disabled="true"/>
													</c:when>
													<c:otherwise>
														<form:select path="fat.ket_jbt" items="${listJabatan}" itemValue="key" itemLabel="value" cssErrorClass="inpError"/>
													</c:otherwise>
												</c:choose>
											</td>
										</tr>
										<tr>
											<th>Nama:<span class="error">*</span></th>
											<td><form:input maxlength="100" size="40" path="fat.nama" cssErrorClass="inpError"/></td>
										</tr>
										<tr>
											<th>Kode Agen:</th>
											<td><form:input maxlength="5" size="8" path="fat.kd_agent" cssErrorClass="inpError"/></td>
										</tr>
										<tr><td colspan="2" style="background-color: black"></td></tr>
										<tr>
											<th>No Identitas (KTP/NPWP/Lainnya):</th>
											<td><form:input maxlength="30" size="40" path="fat.no_identitas" cssErrorClass="inpError"/></td>
										</tr>
										<tr>
											<th>Tempat / Tanggal Lahir:</th>
											<td>
												<form:input maxlength="100" size="20" path="fat.tpl"/>
												<spring:bind path="command.fat.tgl_lahir"><script>inputDate('${status.expression}', '${status.value}', false);</script></spring:bind> 
											</td>
										</tr>
										<tr>
											<th>Jenis Kelamin:<span class="error">*</span></th>
											<td>
												<form:radiobutton cssClass="noBorder" path="fat.gender" value="1" id="pria"/><label for="pria">Pria</label>
												<form:radiobutton cssClass="noBorder" path="fat.gender" value="2" id="wanita"/><label for="wanita">Wanita</label>
											</td>
										</tr>
										<tr>
											<th>Alamat:</th>
											<td>
												<form:textarea cols="40" rows="5" path="fat.alamat" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); " cssErrorClass="inpError"/>
											</td>
										</tr>
										<tr>
											<th>No Telp / No HP:</th>
											<td>
												<form:input maxlength="25" size="19" path="fat.notelp" cssErrorClass="inpError"/>
												<form:input maxlength="25" size="19" path="fat.nohp" cssErrorClass="inpError"/>
											</td>
										</tr>
										<tr>
											<th>Cabang Bank Sinarmas (KC/KCP):<span class="error">*</span></th>
											<td>
												<c:choose>
													<c:when test="${not empty sessionScope.currentUser.cab_bank and sessionScope.currentUser.cab_bank ne \"SSS\"}">
														<form:hidden path="fat.lcb_no"/>
														<c:forEach items="${listBSM}" var="b">
															<c:if test="${b.key eq command.fat.lcb_no}"><input type="text" value="${b.value}" readonly="readonly" class="readOnly"></c:if>
														</c:forEach>
													</c:when>
													<c:otherwise>
														<form:select path="fat.lcb_no" cssErrorClass="inpError">
															<form:option value="" label="-- SILAHKAN PILIH --"/>
															<form:options items="${listBSM}" itemValue="key" itemLabel="value"/>
														</form:select>
													</c:otherwise>
												</c:choose>
											</td>
										</tr>
										<tr>
											<th>Masa Kerja:<span class="error">*</span></th>
											<td>
												<spring:bind path="command.fat.act_date"><script>inputDate('${status.expression}', '${status.value}', false);</script></spring:bind> s/d 
												<spring:bind path="command.fat.end_date"><script>inputDate('${status.expression}', '${status.value}', false);</script></spring:bind> 
											</td>
										</tr>
										<tr><td colspan="2" style="background-color: black"></td></tr>
										<tr>
											<th>No Rekening Bank Sinarmas:<span class="error">*</span></th>
											<td><form:input size="40" maxlength="30" path="fat.norek" cssErrorClass="inpError"/></td>
										</tr>
										<tr>
											<th>Honor per Hari:<span class="error">*</span></th>
											<td><form:input cssStyle="text-align: right" maxlength="10" size="18" path="fat.honor" cssErrorClass="inpError"/></td>
										</tr>
										<tr>
											<th>Target:<span class="error">*</span></th>
											<td><form:input cssStyle="text-align: right" maxlength="15" size="18" path="fat.target" cssErrorClass="inpError"/></td>
										</tr>
										<tr>
											<th>Target NOA:<span class="error">*</span></th>
											<td><form:input cssStyle="text-align: right" maxlength="6" size="18" path="fat.targetnoa" cssErrorClass="inpError"/></td>
										</tr>
										<tr><td colspan="2" style="background-color: black"></td></tr>
										<tr>
											<th>Aktivasi/Terminasi:</th>
											<td>
												<c:choose>
													<c:when test="${not empty command.fat.fatid}">
														<form:radiobutton cssClass="noBorder" path="fat.is_active" value="1" id="aktif"/><label for="aktif">Aktif</label>
														<form:radiobutton cssClass="noBorder" path="fat.is_active" value="0" id="nonaktif"/><label for="nonaktif">Tidak Aktif</label>
													</c:when>
													<c:otherwise>
														<form:radiobutton cssClass="noBorder" path="fat.is_active" value="1" id="aktif"/><label for="aktif">Aktif</label>
													</c:otherwise>
												</c:choose>
											</td>
										</tr>
										<tr>
											<th>Alasan Resign/Terminasi:</th>
											<td>
												<c:choose>
													<c:when test="${not empty command.fat.fatid}">
														<form:input maxlength="100" size="40" path="fat.resign_why" cssErrorClass="inpError"/>
													</c:when>
													<c:otherwise>
														<form:input maxlength="100" size="40" readonly="readOnly" cssClass="readOnly" path="fat.resign_why" cssErrorClass="inpError"/>
													</c:otherwise>
												</c:choose>
											</td>
										</tr>							
									</table>
								</fieldset>
							</td>
							<td style="vertical-align: top;">
								<fieldset>
									<legend>Informasi Lainnya</legend>
									
									<table class="entry2" style="width: auto;">
										<tr>
											<th style="width: 225px">Posisi:</th>
											<td>
												<c:forEach items="${listPosisi}" var="p">
													<c:if test="${p.key eq command.fat.posisi}">
														<input type="button" value="${p.value}" style="background-color: ${p.desc}; ">
													</c:if>
												</c:forEach>
												<form:hidden path="fat.posisi"/>
											</td>
										</tr>
										<tr>
											<th style="width: 225px">Tgl Input / Tgl Update Terakhir:</th>
											<td><form:input cssStyle="text-align: center" readonly="readOnly" cssClass="readOnly" path="fat.create_date"/><form:input cssStyle="text-align: center" readonly="readOnly" cssClass="readOnly" path="fat.tgl_update"/></td>
										</tr>
										<tr>
											<th style="width: 225px">User Input / NIK Input:</th>
											<td><form:input cssStyle="text-align: center" readonly="readOnly" cssClass="readOnly" path="fat.lus_id"/><form:input cssStyle="text-align: center" readonly="readOnly" cssClass="readOnly" path="fat.nik_input"/></td>
										</tr>
									</table>
								</fieldset>
								<fieldset>
									<legend>Histori Perubahan</legend>
									<display:table id="baris" name="command.listFatHistory" class="displaytag">
										<display:column property="posisi" title="Posisi"/>                                                                                           
										<display:column property="lus_id" title="User"/>                                                                                           
										<display:column property="nik_input" title="NIK"/>                                                                                           
										<display:column property="tanggal" title="Tanggal" format="{0, date, dd/MM/yyyy HH:mm}"/>                                                      
										<display:column property="keterangan" title="Keterangan"/>                                                                                           
									</display:table>                                                                                                                                              			
								</fieldset>
							</td>
						</tr>
					</table>
				
					<br/>
					<input type="button" name="new" value="Tambah Baru" onclick="window.location = '${path}/cross_selling/fat.htm?window=input';">
					<input type="submit" name="simpan" value="Simpan" onclick="return confirm('Simpan data?')">
					<input type="reset" name="reset">
					<input type="button" name="tutup" value="Tutup" onclick="window.close()">

					</form:form>

				</div>
			</div>
		</div>
	</body>
</html>