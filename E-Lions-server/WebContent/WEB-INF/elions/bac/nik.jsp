<%@ include file="/include/page/header.jsp"%>
<script language="JavaScript">
<!--
	function Body_onload() {
	
		if (document.frmParam.elements['pemegang.mspo_pribadi'].value == '1')
		{
			document.frmParam.mspo_pribadi1.checked = true;
		}else{
			document.frmParam.mspo_pribadi1.checked = false;
		}
		
		if (document.frmParam.elements['datausulan.indeks_validasi'].value == '1')
		{
			alert('Data berhasil disimpan');
		}else if (document.frmParam.elements['datausulan.indeks_validasi'].value == '0')
			{
				alert('Data tidak berhasil disimpan');
			}
		if (document.frmParam.elements['datausulan.mspr_discount'].value == null)
		{
			document.frmParam.elements['datausulan.mspr_discount'].value = 0;
		}
		
		if(document.frmParam.elements['datausulan.mste_flag_el'].value == '1'){
			document.frmParam.mste_flag_el1.checked = true;
			document.frmParam.cariperusahaan1.disabled = true;
			document.frmParam.cariperusahaan1.style.backgroundColor ='#D4D4D4';
			document.frmParam.btnperusahaan1.disabled =true;		
			document.frmParam.cariperusahaan1.value='';
		}else{
			document.frmParam.mste_flag_el1.checked = false;
			document.frmParam.cariperusahaan1.disabled = false;
			document.frmParam.cariperusahaan1.style.backgroundColor ='#FFFFFF';
			document.frmParam.btnperusahaan1.disabled = false;		
		}
	
		if (document.frmParam.elements['datausulan.flag_as'].value=='2')
		{
			document.frmParam.elements['employee.nik'].readOnly = false;
			document.frmParam.elements['employee.nik'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['employee.no_urut'].value = '1';
			
			document.frmParam.mste_flag_el1.checked = false;
			document.frmParam.mste_flag_el1.disabled = true;
			document.frmParam.cariperusahaan1.disabled = true;
			document.frmParam.cariperusahaan1.style.backgroundColor ='#D4D4D4';
			document.frmParam.btnperusahaan1.disabled =true;		
			document.frmParam.cariperusahaan1.value='';
		}else{
			document.frmParam.elements['employee.nik'].readOnly= true;
			document.frmParam.elements['employee.nik'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['employee.no_urut'].value = '';
			document.frmParam.elements['employee.no_urut'].disabled = true;
			document.frmParam.elements['employee.potongan'].value='';
			document.frmParam.elements['employee.potongan'].disabled = true;
			document.frmParam.elements['datausulan.mspr_discount'].disabled = true;
		}
			
		if (document.frmParam.elements['datausulan.flag_worksite'].value == '0')
		{
			document.frmParam.elements['pemegang.mspo_customer'].disabled = true;
			document.frmParam.elements['pemegang.mspo_customer'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['pemegang.mspo_customer'].value='';
			document.frmParam.btnperusahaan1.disabled = true;
			document.frmParam.cariperusahaan1.readOnly = true;
			document.frmParam.cariperusahaan1.style.backgroundColor ='#D4D4D4';
			document.frmParam.cariperusahaan1.value='';
			document.frmParam.elements['pemegang.nik'].disabled = true;
			document.frmParam.elements['pemegang.nik'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['pemegang.nik'].value='';
		}else{
			document.frmParam.elements['pemegang.mspo_customer'].disabled = false;
			document.frmParam.elements['pemegang.mspo_customer'].style.backgroundColor ='#FFFFFF';
			document.frmParam.cariperusahaan1.readOnly = false;
			document.frmParam.cariperusahaan1.style.backgroundColor ='#FFFFFF';
			document.frmParam.btnperusahaan1.disabled = false;
			document.frmParam.elements['pemegang.nik'].disabled = false;
			document.frmParam.elements['pemegang.nik'].style.backgroundColor ='#FFFFFF';
		}
		
		<c:if test="${cmd.datausulan.lspd_id gt 2}"> 
			document.frmParam.cariperusahaan1.disabled = true;
			document.frmParam.cariperusahaan1.style.backgroundColor ='#D4D4D4';
			document.frmParam.btnperusahaan1.disabled =true;		
			document.frmParam.mste_flag_el1.disabled = true;
			document.frmParam.mspo_pribadi1.disabled = true;
		</c:if>
	}
	
	//function pribadi
function pribadi_onClick(){

	if(document.frmParam.mspo_pribadi1.checked==true){
		document.frmParam.mspo_pribadi1.checked = true;
		document.frmParam.elements['pemegang.mspo_pribadi'].value = '1';
	}else{
		document.frmParam.mspo_pribadi1.checked = false;
		document.frmParam.elements['pemegang.mspo_pribadi'].value = '0';
	}
	
}
	
function karyawan_onClick()
{
	if(document.frmParam.mste_flag_el1.checked == true){
		document.frmParam.mste_flag_el1.checked = true;
		document.frmParam.cariperusahaan1.disabled = true;
		document.frmParam.cariperusahaan1.style.backgroundColor ='#D4D4D4';
		document.frmParam.btnperusahaan1.disabled =true;		
		document.frmParam.elements['datausulan.mste_flag_el'].value = '1';
		document.frmParam.cariperusahaan1.value='';
		ajaxSelectWithParam("080000000024",'select_company_ekal','perush','pemegang.mspo_customer','', 'COMPANY_ID', 'COMPANY_NAMA', '');
	}else{
		document.frmParam.mste_flag_el1.checked = false;
		document.frmParam.cariperusahaan1.disabled = false;
		document.frmParam.cariperusahaan1.style.backgroundColor ='#FFFFFF';
		document.frmParam.btnperusahaan1.disabled = false;		
		document.frmParam.elements['datausulan.mste_flag_el'].value = '0';
	}
}

	function next()
	{
		if(document.frmParam.mspo_pribadi1.checked==true){
			document.frmParam.elements['pemegang.mspo_pribadi'].value = '1';
		}else{
			document.frmParam.elements['pemegang.mspo_pribadi'].value = '0';
		}
		if(document.frmParam.mste_flag_el1.checked==true){
			document.frmParam.elements['datausulan.mste_flag_el'].value = '1';
		}else{
			document.frmParam.elements['datausulan.mste_flag_el'].value = '0';	
		}		
	}
	
	function next1()
	{
		if(document.frmParam.mspo_pribadi1.checked==true){
			document.frmParam.elements['pemegang.mspo_pribadi'].value = '1';
		}else{
			document.frmParam.elements['pemegang.mspo_pribadi'].value = '0';
		}
		if(document.frmParam.mste_flag_el1.checked==true){
			document.frmParam.elements['datausulan.mste_flag_el'].value = '1';
		}else{
			document.frmParam.elements['datausulan.mste_flag_el'].value = '0';	
		}		
	}	
	
// -->
</script>
<body bgcolor="ffffff" onload="Body_onload();" >
<XML ID=xmlData></XML>
     <form name="frmParam" method="post" >
 
<table border="0" width="100%" class="entry">
			<spring:bind path="cmd.datausulan.flag_as"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }"  >
                </spring:bind>
				<spring:bind path="cmd.datausulan.mspr_premium"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30">
               </spring:bind>
			   <spring:bind path="cmd.datausulan.flag_worksite"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30">
               </spring:bind>
               <spring:bind path="cmd.datausulan.indeks_validasi"> 
              <input type="hidden" name="${status.expression}" disabled
						value="${status.value }"  size="30">
               </spring:bind>
              <spring:bind path="cmd.datausulan.lspd_id"> 
              <input type="hidden" name="${status.expression}" disabled
						value="${status.value }"  size="30">
               </spring:bind>
	<tr>
		<th></th>
		<th>Nomor Registrasi (SPAJ) </th>
		<th colspan="5"><input type="reg_spaj"
			value="<elions:spaj nomor='${cmd.pemegang.reg_spaj}'/>"
			style='background-color :#D4D4D4' readOnly> &nbsp;&nbsp;&nbsp;&nbsp;
			<c:if test="${cmd.datausulan.flag_worksite_ekalife eq 1}"> Karyawan AJ Sinarmas</c:if>
		</th>
	</tr>
    <tr > 
       <th colspan="7" class="subtitle"  height="20"> <p align="center"><b> 
          <font face="Verdana" size="1" color="#FFFFFF">INFORMASI KHUSUS 
                KARYAWAN AJ Sinarmas SEBAGAI PEMEGANG POLIS</font></b> </p></th>
   </tr>
   <tr > 
      <th width="21"><b><font face="Verdana" size="1" color="#996600">1. 
      </font></b></th>
      <th width="216"><b><font face="Verdana" size="1" color="#996600">NIK 
      AJ Sinarmas <br>
      ( Khusus Excellink Karyawan)&nbsp; </font></b></th>
      <th colspan="5"><spring:bind path="cmd.employee.nik"> 
        <input type="text" name="${status.expression}"
		value="${status.value }"  size="50" maxlength="10" tabindex="7"
		 <c:if test="${ not empty status.errorMessage}">
			style='background-color :#FFE1FD'
		</c:if>>
        <font color="#CC3300">*</font> 
		</spring:bind></th>
  </tr>
  <tr > 
     <th width="21">&nbsp;</th>
	 <th width="216"><b><font face="Verdana" size="1" color="#996600">Nama 
        Karyawan&nbsp; </font></b></th>
     <th colspan="5"><spring:bind path="cmd.employee.nama"> 
        <input type="text" name="${status.expression}"
		value="${status.value }"  size="50" style='background-color :#D4D4D4'readOnly>
      </spring:bind></th>
  </tr>
  <tr > 
     <th width="21">&nbsp;</th>
     <th width="216"><b><font face="Verdana" size="1" color="#996600">Cabang 
        </font></b></th>
     <th colspan="5"><spring:bind path="cmd.employee.cabang"> 
        <input type="text" name="${status.expression}"
		value="${status.value }"  size="50" style='background-color :#D4D4D4'readOnly>
        </spring:bind></th>
  </tr>
  <tr > 
     <th width="21">&nbsp;</th>
     <th width="216"><b><font face="Verdana" size="1" color="#996600">Departemen</font></b></th>
     <th colspan="5"><spring:bind path="cmd.employee.dept"> 
        <input type="text" name="${status.expression}"
		value="${status.value }"  size="50" style='background-color :#D4D4D4'readOnly>
        </spring:bind></th>
  </tr>
  <tr > 
     <th width="21"><b><font face="Verdana" size="1" color="#996600">2</font><font face="Verdana" size="1" color="#996600">. 
     </font></b></th>
     <th width="216"><b><font face="Verdana" size="1" color="#996600">Premi 
     Ke</font></b></th>
     <th width="96"><spring:bind path="cmd.employee.no_urut"> 
        <input type="text" name="${status.expression}"
		value="${status.value }"  size="3" style='background-color :#D4D4D4'readOnly >
        </spring:bind></th>
     <th width="165"><b><font face="Verdana" size="1" color="#996600">Potongan</font> 
     </b></th>
     <th width="160"><spring:bind path="cmd.employee.potongan"> 
        <input type="text" name="${status.expression}" size="15" style='background-color :#D4D4D4'readOnly 
		value=<fmt:formatNumber type="number" value="${cmd.employee.potongan}"/> >
        </spring:bind></th>
     <th width="67"><b><font face="Verdana" size="1" color="#996600">Tanggal 
         Proses</font></b></th>
     <th width="261"> <spring:bind path="cmd.employee.tgl_proses"> 
          <script>('${status.expression}', '${status.value}', true);</script>
          </spring:bind> </th>
  </tr>
  <tr > 
      <th colspan="7" class="subtitle"  height="20"> <p align="center"><b> 
         <font face="Verdana" size="1" color="#FFFFFF">INFORMASI KHUSUS 
          PRODUK WORKSITE</font></b> </p></th>
  </tr>
  <tr > 
     <th width="21"><b><font face="Verdana" size="1" color="#996600">1. 
        </font></b></th>
     <th width="216"><b><font face="Verdana" size="1" color="#996600">Cari 
        perusahaan </font></b></th>
     <th colspan="5"> <input type="text" name="cariperusahaan1" onkeypress="if(event.keyCode==13){ document.frmParam.btnperusahaan1.click(); return false;}"> 
        <input type="button" name="btnperusahaan1" value="Cari"  onclick="ajaxSelectWithParam(document.frmParam.cariperusahaan1.value,'select_company','perush','pemegang.mspo_customer','', 'COMPANY_ID', 'COMPANY_NAMA', '');"> 
		<input type="checkbox" name="mste_flag_el1" class="noBorder" 
			value="${cmd.datausulan.mste_flag_el}"  size="50" onClick="karyawan_onClick();" tabindex="3"
		 <c:if test="${ not empty status.errorMessage}">
			style='background-color :#FFE1FD'
		</c:if>> 
       <spring:bind path="cmd.datausulan.mste_flag_el"> 
           <input type="hidden" name="${status.expression}"
				value="${cmd.datausulan.mste_flag_el}"  size="50" style='background-color :#D4D4D4'readOnly>
            <b><font face="Verdana" size="1" color="#996600">Karyawan AJ Sinarmas</font></b></spring:bind> 
<input type="checkbox" name="mspo_pribadi1" class="noBorder" 
						value="${cmd.pemegang.mspo_pribadi}"  size="30" onClick="pribadi_onClick();" tabindex="5"
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> 
              <spring:bind path="cmd.pemegang.mspo_pribadi"> 
              <input type="hidden" name="${status.expression}"
						value="${cmd.pemegang.mspo_pribadi}"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind> <font face="Verdana" size="1" color="#996600">Pribadi    </font>        
    </th>
  </tr>
  <tr > 
     <th width="21"><b><font face="Verdana" size="1" color="#996600"> </font></b></th>
     <th width="216"><b><font face="Verdana" size="1" color="#996600">Nama 
         perusahaan </font></b></th>
     <th colspan="5"><div id="perush"> 
         <select name="pemegang.mspo_customer"
		 <c:if test="${ not empty status.errorMessage}">
			style='background-color :#FFE1FD'
		</c:if>>
           <option value="${cmd.pemegang.mspo_customer}">${cmd.pemegang.mspo_customer_nama}</option>
           <c:if test="${ not empty status.errorMessage}"><a href="#"><img src="${path}/include/image/false.gif" width="11" height="11"  border="0" onMouseOver="showtip(this,event,'${status.errorMessage }')" onMouseOut="hidetip()" onClick="showtip(this,event,'${status.errorMessage }')"></a> 
           </c:if> 
        </select>
        <font color="#CC3300">*</font></div></th>
  </tr>
  <tr > 
     <th width="21"><b><font face="Verdana" size="1" color="#996600">2. 
     </font></b></th>
     <th width="216"><b><font face="Verdana" size="1" color="#996600">NIK 
         KARYAWAN </font></b></th>
     <th colspan="5"><spring:bind path="cmd.pemegang.nik"> 
         <input type="text" name="${status.expression}"
			value="${status.value }"  size="50" maxlength="10" tabindex="7"
		 <c:if test="${ not empty status.errorMessage}">
			style='background-color :#FFE1FD'
		</c:if>>
        <font color="#CC3300">*</font> 
        </spring:bind></th>
  </tr>
<tr > 
       <th colspan="7" class="subtitle"  height="20">
	<spring:bind path="cmd.datausulan.mspr_discount"> 
    <input type="hidden" name="${status.expression}" style='background-color :#D4D4D4'
		value="${status.value }"  size="50" maxlength="10" tabindex="7">
         </spring:bind>

          <input type="submit" value="Save" name="save"  onClick="next()">
          <input type="button" name="cancel" value="Cancel" onclick="window.close()";>
             </th>
          </tr> 
          <spring:bind path="cmd.*"> <c:if test="${not empty status.errorMessages}"> 
          <div id="error"> ERROR:<br>
            <c:forEach var="error" items="${status.errorMessages}"> - <c:out value="${error}" escapeXml="false" /> 
            <br />
            </c:forEach></div>
          </c:if> </spring:bind> 

</table>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>