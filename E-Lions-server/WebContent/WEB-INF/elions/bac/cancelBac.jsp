<%@ include file="/include/page/header.jsp"%>
<body style="height: 100%;">
	<div id="contents">
		<form method="post" name="frm_batal">
		<table class="entry">
			<tr>

				<td valign="top">Alasan Pembatalan</td>
				<td>
				<textarea name="textarea" cols="50" rows="5"></textarea>
				</td>
				
			</tr>
			<tr> 
        <td nowrap="nowrap" colspan="4">
		    <input type="submit" name="Ok" value="Save" >
		    <input type="button" name="Cancel" value="Cancel" >
        	
        	<c:if test="${not empty submitSuccess }">
	        	<div id="success">
	        		<script>
	        			alert("SPAJ berhasil dibatalkan !!!");
	        			window.close();
	        		</script>
		        	SPAJ berhasil dibatalkan !!!
	        	</div>
	        </c:if>	
  			<spring:bind path="cmd.*">
				<c:if test="${not empty status.errorMessages}">
					<div id="error">
						ERROR:<br>
							<c:forEach var="error" items="${status.errorMessages}">
										- <c:out value="${error}" escapeXml="false" />
								<br/>
							</c:forEach>
					</div>
				</c:if>									
			</spring:bind>
			
			<input type="hidden" name="flag" value="0" >
		</td>
      </tr>
	</table>
	</form>	
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>