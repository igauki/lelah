<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path }/include/js/default.js"></script>
<!-- Common Javascripts -->
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		$( ".radio" ).buttonset();
		$("#tabs").tabs();
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$(
			"input[title], select[title], textarea[title], button[title]")
			.qtip();

		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth : true,
			changeYear : true,
			dateFormat : "dd/mm/yy"
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass(
				"ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements

		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		$("#company").hide();
		$("#company1").hide();
		$("#company2").hide();
		if (parent) {
			if (parent.document) {
				var dZone = parent.document
						.getElementById('dZone');
				if (dZone)
					dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		$("#showPolis").click(function() {
			//var file=$("#file1").val();
			var dist = $("#dist2").val();
			var cmpny = $("#company2").val();
			var sts = $("#st_polis").val();
		});

		$("#company").click(
				function() {
					//kopi untuk disubmit (isinya, bukan valuenya)
					$("#company2").val(
							$("#company option:selected")
									.text());
				});
		$("#company1").click(
				function() {
					//kopi untuk disubmit (isinya, bukan valuenya)
					$("#company3").val(
							$("#company1 option:selected")
									.text());
				});
		$("#company2").click(
				function() {
					//kopi untuk disubmit (isinya, bukan valuenya)
					$("#company3").val(
							$("#company2 option:selected")
									.text());
				});
		var index = $('div#tabs li a').index(
				$('a[href="#tab-${showTab}"]').get(0));
		$('div#tabs').tabs({
			selected : index
		});

		var j = $('#customFields td').size() + 1;
		$(".addCF")
				.click(
						function() {
							$("#customFields")
									.append(
											'<tr valign="top"><th scope="row">'
													+ j
													+ '</th><th><input type="text" class="code" size="43" id="namaPT_' + j +'" name="namaPT_' + j +'" value="" placeholder="namaPT_" /></th><th><select name="selectProduk' + j +'"><option value="Asuransi Dasar">Asuransi Dasar</option><option value="Asuransi Penyakit Kritis">Asuransi Penyakit Kritis</option> <option value="Asuransi Kesehatan">Asuransi Kesehatan</option> <option value="Asuransi Kecelakaan">Asuransi Kecelakaan</option> <option value="Asuransi Lain Terkait Dengan Asuransi Jiwa">Asuransi Lain Terkait Dengan Asuransi Jiwa</option> </select></th><th><input type="text" class="code" id="up_' + j +'" name="up_' + j +'" value="" placeholder="up_" /></th><th><input type="text" class="code" id="tgl_' + j +'" name="tgl_' + j +'" value="" placeholder="tgl_" /></th><th><a href="javascript:void(0);" class="remCF">Remove</a></th></tr>');
							j++;
							$('#jml').val(j);
						});
		$(".remCF").live('click', function() {
			$(this).parent().parent().remove();
			j--;
			$('#jml').val(j);
		});

		var j2 = $('#customFields td').size() + 1;
		$(".addCF2")
				.click(
						function() {
							$("#customFields2")
									.append(
											'<tr valign="top"><th><select name="selectKategori' + j2 +'"><option value="Ayah">Ayah</option><option value="Ibu">Ibu</option> <option value="Saudara Laki2/ Perempuan">Saudara Laki2/ Perempuan</option></select></th><th><input type="text" class="code" id="umurCT_' + j2 +'" name="umurCT_' + j2 +'" value="" placeholder="umurCT" /></th><th><input type="text" class="code" id="kondisit_' + j2 +'" name="kondisit_' + j2 +'" value="" placeholder="kondisit" /></th><th><input type="text" class="code" id="penyebabt_' + j2 +'" name="penyebabt_' + j2 +'" value="" placeholder="penyebabt_" /></th><th><a href="javascript:void(0);" class="remCF2">Remove</a></th></tr>');
							j2++;
							$('#jml2').val(j2);
						});
		$(".remCF2").live('click', function() {
			$(this).parent().parent().remove();
			j2--;
			$('#jml2').val(j2);
		});

		var j3 = $('#customFields td').size() + 1;
		$(".addCF3")
				.click(
						function() {
							$("#customFields3")
									.append(
											'<tr valign="top"><th scope="row">'
													+ j3
													+ '</th><th><input type="text" class="code" size="37" id="namaPT2_' + j3 +'" name="namaPT2_' + j3 +'" value="" placeholder="namaPT2_" /></th><th><select name="selectProduk2' + j3 +'"><option value="Asuransi Dasar">Asuransi Dasar</option><option value="Asuransi Penyakit Kritis">Asuransi Penyakit Kritis</option> <option value="Asuransi Kesehatan">Asuransi Kesehatan</option> <option value="Asuransi Kecelakaan">Asuransi Kecelakaan</option> <option value="Asuransi Lain Terkait Dengan Asuransi Jiwa">Asuransi Lain Terkait Dengan Asuransi Jiwa</option> </select></th><th><input type="text" class="code" id="up2_' + j3 +'" name="up2_' + j3 +'" value="" placeholder="up2_" /></th><th><input type="text" class="code" id="tgl2_' + j3 +'" name="tgl2_' + j3 +'" value="" placeholder="tgl2_" /></th><th><input type="text" class="code" size="13" id="claim' + j3 +'" name="claim' + j3 +'" value="" placeholder="claim" /></th><th><a href="javascript:void(0);" class="remCF3">Remove</a></th></tr>');
							j3++;
							$('#jml3').val(j3);
						});
		$(".remCF3").live('click', function() {
			$(this).parent().parent().remove();
			j3--;
			$('#jml3').val(j3);
		});

		var j4 = $('#customFields td').size() + 1;
		$(".addCF4")
				.click(
						function() {
							$("#customFields4")
									.append(
											'<tr valign="top"><th scope="row">'
													+ j4
													+ '</th><th><input type="text" class="code" size="100" id="keterangansht_' + j4 +'" name="keterangansht_' + j4 +'" value="" placeholder="keterangansht_" /></th><th><a href="javascript:void(0);" class="remCF4">Remove</a></th></tr>');
							j4++;
							$('#jml4').val(j4);
						});
		$(".remCF4").live('click', function() {
			$(this).parent().parent().remove();
			j4--;
			$('#jml4').val(j4);
		});

		var j5 = $('#customFields td').size() + 1;
		$(".addCF5")
				.click(
						function() {
							$("#customFields5")
									.append(
											'<tr valign="top"><th><select name="selectKategoriP' + j5 +'"><option value="Ayah">Ayah</option><option value="Ibu">Ibu</option> <option value="Saudara Laki2/ Perempuan">Saudara Laki2/ Perempuan</option></select></th><th><input type="text" class="code" id="umurCP_' + j5 +'" name="umurCP_' + j5 +'" value="" placeholder="umurCP_" /></th><th><input type="text" class="code" id="kondisip_' + j5 +'" name="kondisip_' + j5 +'" value="" placeholder="kondisip_" /></th><th><input type="text" class="code" id="penyebabp_' + j5 +'" name="penyebabp_' + j5 +'" value="" placeholder="penyebabp_" /></th><th><a href="javascript:void(0);" class="remCF5">Remove</a></th></tr>');
							j5++;
							$('#jml5').val(j5);
						});
		$(".remCF5").live('click', function() {
			$(this).parent().parent().remove();
			j5--;
			$('#jml5').val(j5);
		});

		//alet untuk pesan
		var pesan = '${pesan}';
		var pesan2 = '${cmd.pesan}';
		
		if(pesan!=null && pesan!=''){
			alert(pesan);
		}
		
		if(pesan2!=null && pesan2!=''){
			alert(pesan2);
		}
	});
	
	function proses(flag){
		formPost.flag.value = flag; 
		formPost.flag2.value = flag; 
		formPost.submit();
	}
	
	function Body_onload(){
		document.getElementById("load").style.display = "none"; 
	}
</script>

<style type="text/css">
/* font utama */
body {
	font-family: Tahoma, Arial, Helvetica, sans-serif;
	font-size: 0.7em;
}

/* fieldset */
fieldset {
	margin-bottom: 1em;
	padding: 0.5em;
}

fieldset legend {
	width: 99%;
}

fieldset legend div {
	margin: 0.3em 0.5em;
}

fieldset .rowElem {
	margin-left: 0.5em;
	padding: 0.3em;
}

fieldset .rowElem label {
	margin-right: 0.4em;
	width: 12em;
	display: inline-block;
}

fieldset .rowElem .jtable {
	position: relative;
	left: 12.5em;
}

/* tanda bintang (*) untuk menandakan required field */
em {
	color: red;
	font-weight: bold;
}

/* agar semua datepicker align center, dan ukurannya fix */
.datepicker {
	text-align: center;
	width: 7em;
}

/* styling untuk client-side validation error message */
#formPost label.error {
	margin-left: 0.4em;
	color: red;
	font-size: 0.9em;
	font-style: italic;
}

/* styling untuk label khusus checkbox dan radio didalam rowElement */
fieldset .rowElem label.radioLabel {
	width: auto;
}

/* lebar untuk form elements */
.lebar {
	width: 24em;
}

/* untuk align center */
.tengah {
	text-align: center;
}

.ui-autocomplete {
	border-radius: 0px;
	margin-top: 330px;
	margin-left: 105px;;
}

/*Tes css*/
.datagrid table {
	border-collapse: collapse;
	text-align: left;
	width: 100%;
	background-color: #eee;
}

.datagrid {
	font: normal 12px/150% Arial, Helvetica, sans-serif;
	background: #fff;
	overflow: hidden;
	border: 1px solid #8C8C8C;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
}

.datagrid table td,.datagrid table th {
	padding: 3px 10px;
}

.datagrid table thead th {
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #8C8C8C
		), color-stop(1, #7D7D7D) );
	background: -moz-linear-gradient(center top, #8C8C8C 5%, #7D7D7D 100%);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#8C8C8C',
		endColorstr='#7D7D7D' );
	background-color: #8C8C8C;
	color: #FFFFFF;
	font-size: 15px;
	font-weight: bold;
	border-left: 1px solid #A3A3A3;
}

.datagrid table thead th:first-child {
	border: none;
}

.datagrid table tbody td {
	color: #000000;
	border-left: 1px solid #DBDBDB;
	font-size: 12px;
	font-weight: normal;
}

.datagrid table tbody .alt td {
	background: #DEDEDE;
	color: #000000;
}

.datagrid table tbody td:first-child {
	border-left: none;
}

.datagrid table tbody tr:last-child td {
	border-bottom: none;
}

/*BUTTON*/
.myButton {
	-moz-box-shadow:inset 0px 39px 0px -24px #e67a73;
	-webkit-box-shadow:inset 0px 39px 0px -24px #e67a73;
	box-shadow:inset 0px 39px 0px -24px #e67a73;
	background-color:#e4685d;
	-moz-border-radius:4px;
	-webkit-border-radius:4px;
	border-radius:4px;
	border:1px solid #ffffff;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:arial;
	font-size:15px;
	padding:6px 15px;
	text-decoration:none;
	text-shadow:0px 1px 0px #b23e35;
}
.myButton:hover {
	background-color:#eb675e;
}
.myButton:active {
	position:relative;
	top:1px;
}

#load
{
    background:url(opacity.png);
    width:100%;
    height:2000px;
    z-index:11;
    position:absolute;
    top:75px;
    left:0px;
    text-align:center;
}

</style>

<body onload="Body_onload();">
	<form id="formPost" name="formPost" method="post" target="">
			<input type="hidden" name="spaj" id="spaj" value="${cmd.spaj}">
			<input type="hidden" name="mspo_flag" id="mspo_flag" value="${cmd.mspo_flag}">
			<input type="hidden" name="jml" id="jml" value="0"> 
			<input type="hidden" name="jml2" id="jml2" value="0"> 
			<input type="hidden" name="jml3" id="jml3" value="0"> 
			<input type="hidden" name="jml4" id="jml4" value="0"> 
			<input type="hidden" name="jml5" id="jml5" value="0">
			<input type="hidden" name="flag" id="flag" value="${cmd.flag}">
			<input type="hidden" name="flag2" id="flag2" >
	<div id="tabs" style="min-width: 1250px; overflow: auto;">
			<fieldset class="ui-widget ui-widget-content">
				<legend class="ui-widget-header ui-corner-all">
				<c:choose>
						<c:when test="${cmd.mspo_flag eq \"4\" }">
							<div>Questionare Untuk ${cmd.spaj}</div>
						</c:when>
						<c:otherwise>
							<div>Questionare Untuk ${cmd.spaj}</div>
						    <div>Tertanggung - Pemegang - Data Kesehatan I - Data Kesehatan II (Harap Berurutan Dalam Menginput, Data Save Ada Di Halaman Data Kesehatan II)</div>
						</c:otherwise>
					</c:choose>
				</legend>
			</fieldset>
				<c:choose>
						<c:when test="${cmd.mspo_flag eq \"4\" }">
							<input type="button" class="myButton" name="Data Kesehatan SIO" id="Data Kesehatan SIO" value="Data Kesehatan SIO" disabled="disabled">
						</c:when>
						<c:otherwise>
							<input type="button" class="myButton" name="Tertanggung" id="Tertanggung" value="Tertanggung" onclick="proses (1);">
							<input type="button" class="myButton" name="Pemegang" id="Pemegang" value="Pemegang" onclick="proses (2);">
							<input type="button" class="myButton"  name="Data Kesehatan I" id="Data Kesehatan I" value="Data Kesehatan I" onclick="proses (3);">
							<input type="button" class="myButton"  name="Data Kesehatan II" id="Data Kesehatan II" value="Data Kesehatan II" onclick="proses (4);">
						</c:otherwise>
					</c:choose>
				<div id="load">
					<img src="${path}/include/image/loading.gif" alt="loading" /> <br />
					<br />
					<h3>Loading Please Wait....</h3>
				</div>
			<c:choose>
				<c:when test="${cmd.mspo_flag eq \"4\" }">
					<div id="tab-4">
						<fieldset class="ui-widget ui-widget-content">
							<div class="datagrid">
							<!-- <table class="ui-widget" style="background-color: #CCCCCC;width: 100%" border="1"> -->
							<table>
								<thead>
								<tr>
									<th>No</th>
									<th>Pertanyaan</th>
									<th>Calon Pemegang Polis</th>
									<th>Calon tertanggung</th>
								</tr>
								</thead>
								<tbody>
								<c:forEach items="${cmd.data_LQGSIO}" var="a" varStatus="s">
									<tr <c:if test="${(s.index+1)%2 eq 0 }"> class="alt"</c:if>>
										<td>${a.QUESTION_NUMBER}.</td>
										<td>${a.QUESTION} 
											<c:forEach items="${cmd.data_LQLSIO}" var="m">
												<input type="hidden" name="id_quest" id="id_quest" value="${m.QUESTION_ID}">
												<c:if test="${a.QUESTION_ID eq m.QUESTION_ID }">
													<c:if test="${m.OPTION_TYPE eq 0 }">
														<br><textarea rows="3" cols="150"
															style="width: 40%; text-transform: uppercase;"
															name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.ANSWER2}</textarea>
													</c:if>
													
													<c:if test="${m.OPTION_TYPE eq 3 }">
														<!-- belum ada -->
													</c:if>
		
													<c:if test="${m.OPTION_TYPE eq 4 }">
														<!-- belum ada -->
													</c:if>
												</c:if>
											</c:forEach>
										</td>
										<td>
											<span class="radio">
											<c:forEach items="${cmd.data_LQLSIO}" var="m">
												<c:if test="${a.QUESTION_ID eq m.QUESTION_ID }">
													<c:if test="${m.OPTION_TYPE eq 1 }">
														<c:if test="${m.OPTION_GROUP eq 1}">
															<c:if test="${m.OPTION_ORDER eq 1 }">
																<input type="radio" class="noBorder"
																	name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																	value="1" <c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																	id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																		<label for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label> 
											   		 		</c:if>
		
															<c:if test="${m.OPTION_ORDER eq 2 }">
																<input type="radio" class="noBorder"
																	name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																	value="0" 
																		<%-- <c:if test="${m.ANSWER2 eq 1}">checked</c:if> --%>
																		<c:choose>
																			<c:when test="${m.ANSWER2 eq 1}">checked</c:when>
																			<c:when test="${m.ANSWER2 eq null and (cmd.lspd_id eq 1 or cmd.lspd_id eq 27)}">checked</c:when>
																		</c:choose>
																	id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																		<label for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label> 
											   		 		</c:if>
		
														</c:if>
													</c:if>
													
													<c:if test="${m.OPTION_TYPE eq 4 }">
														<c:if test="${m.OPTION_GROUP eq 1}">
															<c:if test="${m.OPTION_ORDER eq 1 }">
																<input type="text" class="noBorder"
																	name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}"
																	value="${m.ANSWER2}" id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2} 
											   		 		</c:if>
		
															<c:if test="${m.OPTION_ORDER eq 2 }">
																<input type="text" class="noBorder"
																	name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}"
																	value="${m.ANSWER2}" id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2} 
											   		 		</c:if>
		
														</c:if>
													</c:if>
												</c:if>
											</c:forEach>
											</span>
										</td>
		
										<td>
											<span class="radio">
											<c:forEach items="${cmd.data_LQLSIO}" var="m">
												<c:if test="${a.QUESTION_ID eq m.QUESTION_ID }">
													<c:if test="${m.OPTION_TYPE eq 1 }">
														<c:if test="${m.OPTION_GROUP eq 2}">
															<c:if test="${m.OPTION_ORDER eq 1 }">
																<input type="radio" class="noBorder"
																	name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																	value="1" <c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																	id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																		<label for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label> 
												   		 	</c:if>
		
															<c:if test="${m.OPTION_ORDER eq 2 }">
																<input type="radio" class="noBorder"
																	name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																	value="0" 
																		<%-- <c:if test="${m.ANSWER2 eq 1}">checked</c:if> --%>
																		<c:choose>
																			<c:when test="${m.ANSWER2 eq 1}">checked</c:when>
																			<c:when test="${m.ANSWER2 eq null and (cmd.lspd_id eq 1 or cmd.lspd_id eq 27)}">checked</c:when>
																		</c:choose>
																	id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																		<label for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label> 
												   		 	</c:if>
		
														</c:if>
													</c:if>
													<c:if test="${m.OPTION_TYPE eq 4 }">
														<c:if test="${m.OPTION_GROUP eq 2}">
															<c:if test="${m.OPTION_ORDER eq 1 }">
																<input type="text" class="noBorder"
																	name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}"
																	value="${m.ANSWER2}" id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2} 
											   		 		</c:if>
		
															<c:if test="${m.OPTION_ORDER eq 2 }">
																<input type="text" class="noBorder"
																	name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}"
																	value="${m.ANSWER2}" id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2} 
											   		 		</c:if>
		
														</c:if>
													</c:if>
												</c:if>
											</c:forEach>
											</span>
										</td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
							</div>
						</fieldset>
					</div>
				</c:when>
				<c:otherwise>
					<c:if test="${cmd.flag eq 2}">
					 <div id="tab-1">
						<fieldset class="ui-widget ui-widget-content">
							<div class="datagrid">
							<table>
								<thead>
								<tr>
									<th>No</th>
									<th>Pertanyaan</th>
									<th>Pilihan Ya/Tidak</th> 
								</tr>
								</thead>
								<tbody>
								<c:forEach items="${cmd.data_LQGPP}" var="a" varStatus="s">
									<tr <c:if test="${(s.index+1)%2 eq 0 }"> class="alt"</c:if>>
										<td>${a.QUESTION_NUMBER}</td>
										<td>${a.QUESTION} 
											<c:forEach items="${cmd.data_LQLPP}" var="m">
												<c:if test="${a.QUESTION_ID eq m.QUESTION_ID }">
													<c:if test="${m.OPTION_TYPE eq 0 }">
														<c:if test="${m.QUESTION_ID eq 16 }">
															<c:if test="${m.OPTION_ORDER eq 1 }">
																<br> Jika "Ya" Mohon Sebutkan nama asuransi, nomor Polis dan besar uang pertanggunganya <br>
																<textarea rows="3" cols="150"
																	style="width: 45%; text-transform: uppercase;"
																	name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.ANSWER2}</textarea>
															</c:if>
		
															<c:if test="${m.OPTION_ORDER eq 2 }">
																<br>Jika "Tidak" Jelaskan <br>
																<textarea rows="3" cols="150"
																	style="width: 40%; text-transform: uppercase;"
																	name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.ANSWER2}</textarea>
															</c:if>
														</c:if>
		
														<c:if test="${m.QUESTION_ID eq 17 }">
															<c:if test="${m.OPTION_ORDER eq 1 }">
																<br>Jika "Ya" Mohon Sebutkan nama asuransi, nomor Polis dan besar uang pertanggunganya <br>
																<textarea rows="3" cols="150"
																	style="width: 45%; text-transform: uppercase;"
																	name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.ANSWER2}</textarea>
															</c:if>
		
															<c:if test="${m.OPTION_ORDER eq 2 }">
																<br>Jika "Tidak" Jelaskan <br>
																<textarea rows="3" cols="150"
																	style="width: 40%; text-transform: uppercase;"
																	name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.ANSWER2}</textarea>
															</c:if>
														</c:if>
		
													</c:if>
													<c:if test="${m.OPTION_TYPE eq 3 }">
														<table class="form-table" id="customFields" border="1">
															<tr valign="top">
																<th scope="row"><label for="customFieldName">Custom
																		Field</label>
																</th>
																<td><input type="text" class="code"
																	id="customFieldName" name="customFieldName[]" value=""
																	placeholder="Input Name" /> &nbsp; <input type="text"
																	class="code" id="customFieldValue"
																	name="customFieldValue[]" value=""
																	placeholder="Input Value" /> &nbsp; <a
																	href="javascript:void(0);" class="addCF">Add</a></td>
															</tr>
														</table>
													</c:if>
		
													<c:if test="${m.OPTION_TYPE eq 4 }">
														<c:if test="${m.QUESTION_ID eq 18 and  m.OPTION_ORDER eq 1}">
											      &nbsp;&nbsp; &nbsp; &nbsp;<input type="text" size="5"
																value="${m.ANSWER2}"
																name="${m.QUESTION_ID}_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}"></input>${m.OPTION2}
											   			 </c:if>
		
														<c:if test="${m.QUESTION_ID eq 18 and  m.OPTION_ORDER eq 2}">
											       &nbsp;&nbsp; &nbsp; &nbsp;<input type="text" size="7"
																value="${m.ANSWER2}"
																name="${m.QUESTION_ID}_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}"></input>${m.OPTION2}
											   			 </c:if>
													</c:if>
												</c:if>
											</c:forEach>
										</td>
										<td style="width: 120px;">
											<span class="radio">
											<c:forEach items="${cmd.data_LQLPP}" var="m">
												<c:if test="${a.QUESTION_ID eq m.QUESTION_ID }">
													<c:if test="${m.OPTION_TYPE eq 1 }">
		
														<c:if test="${m.OPTION_ORDER eq 1 }">
															<input type="radio" class="noBorder"
																name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																value="1" <c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																	<label for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label> 
									   		 			</c:if>
		
														<c:if test="${m.OPTION_ORDER eq 2 }">
															<input type="radio" class="noBorder"
																name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																value="0" 
																	<c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																	<c:choose>
																		<c:when test="${m.ANSWER2 eq 1}">checked</c:when>
																		<c:when test="${m.ANSWER2 eq null and (cmd.lspd_id eq 1 or cmd.lspd_id eq 27)}">checked</c:when>
																	</c:choose>
																id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																	<label for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label> 
									   		 			</c:if>
		
													</c:if>
		
													<c:if test="${m.OPTION_TYPE eq 2 }">
														<input type="checkbox" class="noBorder" name="chbox"
															value="0" id="chbox">
													</c:if>
		
													<c:if test="${m.OPTION_TYPE eq 5 }">date</c:if>
												</c:if>
											</c:forEach>
											</span>
										</td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
							</div>
						</fieldset>
					</div>
					</c:if>
					<c:if test="${cmd.flag eq 1}">
					<div id="tab-2">
						<fieldset class="ui-widget ui-widget-content">
							<div class="datagrid">
							<table>
								<thead>
								<tr>
									<th>No</th>
									<th>Pertanyaan</th>
									<th>Pilihan Ya/Tidak</th>
								</tr>
								</thead>
								<tbody>
								<c:forEach items="${cmd.data_LQGTT}" var="a" varStatus="s">
									<tr<c:if test="${(s.index+1)%2 eq 0 }"> class="alt"</c:if>>
										<td>${a.QUESTION_NUMBER}</td>
										<td>${a.QUESTION} <c:forEach items="${cmd.data_LQLTT}"
												var="m">
												<c:if test="${a.QUESTION_ID eq m.QUESTION_ID }">
													<c:if test="${m.OPTION_TYPE eq 0 }">
														<br>
														<textarea rows="3" cols="150"
															style="width: 40%; text-transform: uppercase;"
															name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.ANSWER2}</textarea>
													</c:if>
		
													<c:if test="${m.OPTION_TYPE eq 3 }">
														<c:if test="${m.QUESTION_ID eq 3 }">
															<div style="width: 1000px;overflow: auto;">
															<table class="displaytag" id="customFields" border="2">
																<tr style="background-color: #ccc;">
																	<th>No</th>
																	<th>Nama Perusahaan</th>
																	<th>Jenis Produk</th>
																	<th>Uang Pertanggungan</th>
																	<th>Tgl/Bln/Thn Penerbitan Polis</th>
																	<th><a href="javascript:void(0);" class="addCF">ADD</a></th>
																</tr>
		
																<c:if test="${not empty m.ANSWER2 }">
																	<c:forEach items="${m.ANSWER2 }" var="mn" varStatus="s">
		
																		<tr>
																			<c:set var="answerTable" value="${fn:split(mn, '~')}" />
																			<c:forEach items="${answerTable }" var="an">
																				<th><input readonly="readonly" type="text"
																					name="" id="" value="${an }" />
																				</th>
																			</c:forEach>
																		</tr>
																	</c:forEach>
		
																</c:if>
															</table>
															</div>
														</c:if>
		
														<c:if test="${m.QUESTION_ID eq 5 }">
															<div style="width: 1000px;overflow: auto;">
															<table class="displaytag" id="customFields3" border="2">
																<tr style="background-color: #ccc;">
																	<th>No</th>
																	<th>Nama Perusahaan</th>
																	<th>Jenis Klaim/Produk</th>
																	<th>Uang Pertanggungan</th>
																	<th>Tgl/Bln/Thn</th>
																	<th>Klaim dibayar/ditolak</th>
																	<th><a href="javascript:void(0);" class="addCF3">ADD</a>
																	</th>
																</tr>
																<c:if test="${not empty m.ANSWER2 }">
																	<c:forEach items="${m.ANSWER2 }" var="mn" varStatus="s">
																		<tr>
																			<c:set var="answerTable" value="${fn:split(mn, '~')}" />
																			<c:forEach items="${answerTable }" var="an">
																				<th><input type="text" readonly="readonly"
																					name="" id="" value="${an }" />
																				</th>
																			</c:forEach>
																		</tr>
																	</c:forEach>
																</c:if>
															</table>
															</div>
														</c:if>
													</c:if>
		
													<c:if test="${m.OPTION_TYPE eq 4 }">
														<c:if test="${m.QUESTION_ID eq 13 and  m.OPTION_ORDER eq 1}">
											     &nbsp;&nbsp; &nbsp; &nbsp;<input type="text" size="5"
																value="${m.ANSWER2}"
																name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}"></input>${m.OPTION2}
											   			 </c:if>
		
														<c:if test="${m.QUESTION_ID eq 13 and  m.OPTION_ORDER eq 2}">
											     &nbsp;&nbsp; &nbsp; &nbsp;<input type="text" size="6"
																value="${m.ANSWER2}"
																name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}"></input>${m.OPTION2}
											   			 </c:if>
		
														<c:if test="${m.QUESTION_ID eq 12}">
											     &nbsp;&nbsp; &nbsp; &nbsp;<input type="text" size="5"
																value="${m.ANSWER2}"
																name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}"></input>${m.OPTION2}
											   			 </c:if>
		
														<c:if test="${m.QUESTION_ID eq 15 and  m.OPTION_ORDER eq 1}">
											   &nbsp;&nbsp; &nbsp; &nbsp;<input type="text" size="5"
																value="${m.ANSWER2}"
																name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}"></input>${m.OPTION2}
											   			 </c:if>
		
														<c:if test="${m.QUESTION_ID eq 15 and  m.OPTION_ORDER eq 2}">
											     &nbsp;&nbsp; &nbsp; &nbsp;<input type="text" size="6"
																value="${m.ANSWER2}"
																name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}"></input>${m.OPTION2}
											   			 </c:if>
													</c:if>
												</c:if>
											</c:forEach>
										</td>
										<td style="width: 120px;">
											<span class="radio">
											<c:forEach items="${cmd.data_LQLTT}" var="m">
												<c:if test="${a.QUESTION_ID eq m.QUESTION_ID }">
													<c:if test="${m.OPTION_TYPE eq 1 }">
		
														<c:if test="${m.OPTION_ORDER eq 1 }">
															<input type="radio" class="noBorder"
																name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																value="1" <c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																	<label for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label> 
									   		 	</c:if>
		
														<c:if test="${m.OPTION_ORDER eq 2 }">
															<input type="radio" class="noBorder"
																name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																value="0" 
																	<%-- <c:if test="${m.ANSWER2 eq 1}">checked</c:if> --%>
																	<c:choose>
																		<c:when test="${m.ANSWER2 eq 1}">checked</c:when>
																		<c:when test="${m.ANSWER2 eq null and (cmd.lspd_id eq 1 or cmd.lspd_id eq 27)}">checked</c:when>
																	</c:choose>
																id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																	<label for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label>
									   		 	</c:if>
		
													</c:if>
		
													<c:if test="${m.OPTION_TYPE eq 2 }">
														<input type="checkbox" class="noBorder" name="chbox"
															value="0" id="chbox">
													</c:if>
		
													<c:if test="${m.OPTION_TYPE eq 5 }">date</c:if>
												</c:if>
											</c:forEach>
											</span>
										</td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
							</div>
						</fieldset>
					</div>
					</c:if>

					<c:if test="${cmd.flag eq 3}">
						<div id="tab-3">
							<fieldset class="ui-widget ui-widget-content">
								<div class="datagrid">
									<table>
										<thead>
											<tr>
												<th>No</th>
												<th>Pertanyaan</th>
												<th>Calon Pemegang Ya/Tidak</th>
												<th>Calon Tertanggung Ya/Tidak</th>
												<th>Calon Tertanggung I Ya/Tidak</th>
												<th>Calon Tertanggung II Ya/Tidak</th>
												<th>Calon Tertanggung III Ya/Tidak</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${cmd.data_LQGQS}" var="a" varStatus="s">
												<tr <c:if test="${(s.index+1)%2 eq 0 }"> class="alt"</c:if>>
													<td>${a.QUESTION_NUMBER}</td>
													<td>${a.QUESTION}</td>
													<td style="width: 120px;"><span class="radio">
															<c:forEach items="${cmd.data_LQLQS}" var="m">
																<c:if test="${a.QUESTION_ID eq m.QUESTION_ID }">
																	<c:if test="${m.OPTION_TYPE eq 1 }">
																		<c:if test="${m.OPTION_GROUP eq 1}">
																			<c:if test="${m.OPTION_ORDER eq 1 }">
																				<input type="radio" class="noBorder"
																					name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																					value="1"
																					<c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																					id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																				<label
																					for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label>
																			</c:if>

																			<c:if test="${m.OPTION_ORDER eq 2 }">
																				<input type="radio" class="noBorder"
																					name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																					value="0"
																					<c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																					<c:choose>
																			<c:when test="${m.ANSWER2 eq 1}">checked</c:when>
																			<c:when test="${m.ANSWER2 eq null and (cmd.lspd_id eq 1 or cmd.lspd_id eq 27)}">checked</c:when>
																		</c:choose>
																					id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																				<label
																					for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label>
																			</c:if>

																		</c:if>
																	</c:if>
																</c:if>
															</c:forEach> </span></td>

													<td style="width: 120px;"><span class="radio">
															<c:forEach items="${cmd.data_LQLQS}" var="m">
																<c:if test="${a.QUESTION_ID eq m.QUESTION_ID }">
																	<c:if test="${m.OPTION_TYPE eq 1 }">
																		<c:if test="${m.OPTION_GROUP eq 2}">
																			<c:if test="${m.OPTION_ORDER eq 1 }">
																				<input type="radio" class="noBorder"
																					name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																					value="1"
																					<c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																					id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																				<label
																					for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label>
																			</c:if>

																			<c:if test="${m.OPTION_ORDER eq 2 }">
																				<input type="radio" class="noBorder"
																					name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																					value="0"
																					<c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																					<c:choose>
																			<c:when test="${m.ANSWER2 eq 1}">checked</c:when>
																			<c:when test="${m.ANSWER2 eq null and (cmd.lspd_id eq 1 or cmd.lspd_id eq 27)}">checked</c:when>
																		</c:choose>
																					id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																				<label
																					for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label>
																			</c:if>

																		</c:if>
																	</c:if>
																</c:if>
															</c:forEach> </span></td>

													<td style="width: 120px;"><span class="radio">
															<c:forEach items="${cmd.data_LQLQS}" var="m">
																<c:if test="${a.QUESTION_ID eq m.QUESTION_ID }">
																	<c:if test="${m.OPTION_TYPE eq 1 }">
																		<c:if test="${m.OPTION_GROUP eq 3}">
																			<c:if test="${m.OPTION_ORDER eq 1 }">
																				<input type="radio" class="noBorder"
																					name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																					value="1"
																					<c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																					id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																				<label
																					for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label>
																			</c:if>

																			<c:if test="${m.OPTION_ORDER eq 2 }">
																				<input type="radio" class="noBorder"
																					name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																					value="0"
																					<c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																					<c:choose>
																			<c:when test="${m.ANSWER2 eq 1}">checked</c:when>
																			<c:when test="${m.ANSWER2 eq null and (cmd.lspd_id eq 1 or cmd.lspd_id eq 27) and (cmd.jml_ttg eq 1 or cmd.jml_ttg eq 2 or cmd.jml_ttg eq 3 or cmd.jml_ttg > 3)}">checked</c:when>
																		</c:choose>
																					id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																				<label
																					for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label>
																			</c:if>

																		</c:if>
																	</c:if>
																</c:if>
															</c:forEach> </span></td>

													<td style="width: 120px;"><span class="radio">
															<c:forEach items="${cmd.data_LQLQS}" var="m">
																<c:if test="${a.QUESTION_ID eq m.QUESTION_ID }">
																	<c:if test="${m.OPTION_TYPE eq 1 }">
																		<c:if test="${m.OPTION_GROUP eq 4}">
																			<c:if test="${m.OPTION_ORDER eq 1 }">
																				<input type="radio" class="noBorder"
																					name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																					value="1"
																					<c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																					id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																				<label
																					for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label>
																			</c:if>

																			<c:if test="${m.OPTION_ORDER eq 2 }">
																				<input type="radio" class="noBorder"
																					name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																					value="0"
																					<c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																					<c:choose>
																			<c:when test="${m.ANSWER2 eq 1}">checked</c:when>
																			<c:when test="${m.ANSWER2 eq null and (cmd.lspd_id eq 1 or cmd.lspd_id eq 27) and (cmd.jml_ttg eq 2 or cmd.jml_ttg eq 3 or cmd.jml_ttg > 3)}">checked</c:when>
																		</c:choose>
																					id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																				<label
																					for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label>
																			</c:if>

																		</c:if>
																	</c:if>
																</c:if>
															</c:forEach> </span></td>

													<td style="width: 120px;"><span class="radio">
															<c:forEach items="${cmd.data_LQLQS}" var="m">
																<c:if test="${a.QUESTION_ID eq m.QUESTION_ID }">
																	<c:if test="${m.OPTION_TYPE eq 1 }">
																		<c:if test="${m.OPTION_GROUP eq 5}">
																			<c:if test="${m.OPTION_ORDER eq 1 }">
																				<input type="radio" class="noBorder"
																					name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																					value="1"
																					<c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																					id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																				<label
																					for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label>
																			</c:if>

																			<c:if test="${m.OPTION_ORDER eq 2 }">
																				<input type="radio" class="noBorder"
																					name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																					value="0"
																					<c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																					<c:choose>
																			<c:when test="${m.ANSWER2 eq 1}">checked</c:when>
																			<c:when test="${m.ANSWER2 eq null and (cmd.lspd_id eq 1 or cmd.lspd_id eq 27) and (cmd.jml_ttg eq 3 or cmd.jml_ttg > 3)}">checked</c:when>
																		</c:choose>
																					id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																				<label
																					for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label>
																			</c:if>

																		</c:if>
																	</c:if>
																</c:if>
															</c:forEach> </span></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</fieldset>
							</div>
					</c:if>
					
					<c:if test="${cmd.flag eq 4}">
						<div id="tab-3">
							<fieldset class="ui-widget ui-widget-content">
								<div class="datagrid">
									<table>
										<thead>
											<tr>
												<th>No</th>
												<th>Pertanyaan</th>
												<th>Calon Pemegang Ya/Tidak</th>
												<th>Calon Tertanggung Ya/Tidak</th>
												<th>Calon Tertanggung I Ya/Tidak</th>
												<th>Calon Tertanggung II Ya/Tidak</th>
												<th>Calon Tertanggung III Ya/Tidak</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${cmd.data_LQGQS}" var="a" varStatus="s">
												<tr <c:if test="${(s.index+1)%2 eq 0 }"> class="alt"</c:if>>
													<td>${a.QUESTION_NUMBER}</td>
													<td>${a.QUESTION} <c:forEach items="${cmd.data_LQLQS}"
															var="m">
															<input type="hidden" name="id_quest" id="id_quest"
																value="${m.QUESTION_ID}">
															<c:if test="${a.QUESTION_ID eq m.QUESTION_ID }">
																<c:if test="${m.OPTION_TYPE eq 0 }">
																	<br>
																	<textarea rows="3" cols="150"
																		style="width: 40%; text-transform: uppercase;"
																		name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.ANSWER2}</textarea>
																</c:if>
																<c:if test="${m.OPTION_TYPE eq 3 }">
																	<c:if
																		test="${m.QUESTION_ID eq 56 or m.QUESTION_ID eq 155 and m.OPTION_ORDER eq 1}">
																		<div style="width: 430px;overflow: auto;">
																			<table class="displaytag" id="customFields2"
																				border="2">
																				<tr style="background-color: #ccc;">
																					<th>Calon Tertanggung</th>
																					<th>Umur</th>
																					<th>Keadaan</th>
																					<th>Penyebab(Untuk Yg Meninggal)</th>
																					<th><a href="javascript:void(0);"
																						class="addCF2">Add</a></th>
																				</tr>
																				<c:if test="${not empty m.ANSWER2 }">
																					<c:forEach items="${m.ANSWER2 }" var="mn"
																						varStatus="s">
																						<tr>
																							<c:set var="answerTable"
																								value="${fn:split(mn, '~')}" />
																							<c:forEach items="${answerTable }" var="an">
																								<th><input type="text" readonly="readonly"
																									name="" id="" value="${an }" /></th>
																							</c:forEach>
																						</tr>
																					</c:forEach>
																				</c:if>
																			</table>
																		</div>
																	</c:if>

																	<c:if
																		test="${m.QUESTION_ID eq 56 or m.QUESTION_ID eq 155 and m.OPTION_ORDER eq 2}">
																		<div style="width: 430px;overflow: auto;">
																			<table class="displaytag" id="customFields5"
																				border="2">
																				<tr style="background-color: #ccc;">
																					<th>Calon Pemegang</th>
																					<th>Umur</th>
																					<th>Keadaan</th>
																					<th>Penyebab(Untuk Yg Meninggal)</th>
																					<th><a href="javascript:void(0);"
																						class="addCF5">Add</a></th>
																				</tr>
																				<c:if test="${not empty m.ANSWER2 }">
																					<c:forEach items="${m.ANSWER2 }" var="mn"
																						varStatus="s">
																						<tr>
																							<c:set var="answerTable"
																								value="${fn:split(mn, '~')}" />
																							<c:forEach items="${answerTable }" var="an">
																								<th><input type="text" readonly="readonly"
																									name="" id="" value="${an }" /></th>
																							</c:forEach>
																						</tr>
																					</c:forEach>
																				</c:if>
																			</table>
																		</div>
																	</c:if>

																	<c:if
																		test="${m.QUESTION_ID eq 57 or m.QUESTION_ID eq 156}">
																		<div style="width: 430px;overflow: auto;">
																			<table class="displaytag" id="customFields4"
																				border="2">
																				<tr style="background-color: #ccc;">
																					<th>No</th>
																					<th>Keterangan Sehat (Jika "Ya", tulis nomor
																						dan jelaskan secara lengkap)</th>
																					<th><a href="javascript:void(0);"
																						class="addCF4">Add</a></th>
																				</tr>
																				<c:if test="${not empty m.ANSWER2 }">
																					<c:forEach items="${m.ANSWER2 }" var="mn"
																						varStatus="s">
																						<tr>
																							<c:set var="answerTable"
																								value="${fn:split(mn, '~')}" />
																							<c:forEach items="${answerTable }" var="an">
																								<th><input type="text" readonly="readonly"
																									name="" id="" value="${an }" /></th>
																							</c:forEach>
																						</tr>
																					</c:forEach>
																				</c:if>
																			</table>
																		</div>
																	</c:if>
																</c:if>

																<c:if test="${m.OPTION_TYPE eq 4 }">
																	<c:if
																		test="${m.QUESTION_ID eq 13 and  m.OPTION_ORDER eq 1}">
											     &nbsp;&nbsp; &nbsp; &nbsp;<input type="text" size="5"
																			value="${m.ANSWER2}"
																			name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}"></input>${m.OPTION2}
											   			 </c:if>

																	<c:if
																		test="${m.QUESTION_ID eq 13 and  m.OPTION_ORDER eq 2}">
											     &nbsp;&nbsp; &nbsp; &nbsp;<input type="text" size="6"
																			value="${m.ANSWER2}"
																			name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}"></input>${m.OPTION2}
											   			 </c:if>

																	<c:if
																		test="${m.QUESTION_ID eq 15 and  m.OPTION_ORDER eq 1}">
											   &nbsp;&nbsp; &nbsp; &nbsp;<input type="text" size="5"
																			value="${m.ANSWER2}"
																			name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}"></input>${m.OPTION2}
											   			 </c:if>

																	<c:if
																		test="${m.QUESTION_ID eq 15 and  m.OPTION_ORDER eq 2}">
											     &nbsp;&nbsp; &nbsp; &nbsp;<input type="text" size="6"
																			value="${m.ANSWER2}"
																			name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}"></input>${m.OPTION2}
											   			 </c:if>
																</c:if>
															</c:if>
														</c:forEach></td>
													<td style="width: 120px;"><span class="radio">
															<c:forEach items="${cmd.data_LQLQS}" var="m">
																<c:if test="${a.QUESTION_ID eq m.QUESTION_ID }">
																	<c:if test="${m.OPTION_TYPE eq 1 }">
																		<c:if test="${m.OPTION_GROUP eq 1}">
																			<c:if test="${m.OPTION_ORDER eq 1 }">
																				<input type="radio" class="noBorder"
																					name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																					value="1"
																					<c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																					id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																				<label
																					for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label>
																			</c:if>

																			<c:if test="${m.OPTION_ORDER eq 2 }">
																				<input type="radio" class="noBorder"
																					name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																					value="0"
																					<c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																					<c:choose>
																			<c:when test="${m.ANSWER2 eq 1}">checked</c:when>
																			<c:when test="${m.ANSWER2 eq null and (cmd.lspd_id eq 1 or cmd.lspd_id eq 27)}">checked</c:when>
																		</c:choose>
																					id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																				<label
																					for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label>
																			</c:if>

																		</c:if>
																	</c:if>
																</c:if>
															</c:forEach> </span></td>

													<td style="width: 120px;"><span class="radio">
															<c:forEach items="${cmd.data_LQLQS}" var="m">
																<c:if test="${a.QUESTION_ID eq m.QUESTION_ID }">
																	<c:if test="${m.OPTION_TYPE eq 1 }">
																		<c:if test="${m.OPTION_GROUP eq 2}">
																			<c:if test="${m.OPTION_ORDER eq 1 }">
																				<input type="radio" class="noBorder"
																					name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																					value="1"
																					<c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																					id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																				<label
																					for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label>
																			</c:if>

																			<c:if test="${m.OPTION_ORDER eq 2 }">
																				<input type="radio" class="noBorder"
																					name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																					value="0"
																					<c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																					<c:choose>
																			<c:when test="${m.ANSWER2 eq 1}">checked</c:when>
																			<c:when test="${m.ANSWER2 eq null and (cmd.lspd_id eq 1 or cmd.lspd_id eq 27)}">checked</c:when>
																		</c:choose>
																					id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																				<label
																					for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label>
																			</c:if>

																		</c:if>
																	</c:if>
																</c:if>
															</c:forEach> </span></td>

													<td style="width: 120px;"><span class="radio">
															<c:forEach items="${cmd.data_LQLQS}" var="m">
																<c:if test="${a.QUESTION_ID eq m.QUESTION_ID }">
																	<c:if test="${m.OPTION_TYPE eq 1 }">
																		<c:if test="${m.OPTION_GROUP eq 3}">
																			<c:if test="${m.OPTION_ORDER eq 1 }">
																				<input type="radio" class="noBorder"
																					name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																					value="1"
																					<c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																					id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																				<label
																					for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label>
																			</c:if>

																			<c:if test="${m.OPTION_ORDER eq 2 }">
																				<input type="radio" class="noBorder"
																					name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																					value="0"
																					<c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																					<c:choose>
																			<c:when test="${m.ANSWER2 eq 1}">checked</c:when>
																			<c:when test="${m.ANSWER2 eq null and (cmd.lspd_id eq 1 or cmd.lspd_id eq 27) and (cmd.jml_ttg eq 1 or cmd.jml_ttg eq 2 or cmd.jml_ttg eq 3 or cmd.jml_ttg > 3)}">checked</c:when>
																		</c:choose>
																					id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																				<label
																					for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label>
																			</c:if>

																		</c:if>
																	</c:if>
																</c:if>
															</c:forEach> </span></td>

													<td style="width: 120px;"><span class="radio">
															<c:forEach items="${cmd.data_LQLQS}" var="m">
																<c:if test="${a.QUESTION_ID eq m.QUESTION_ID }">
																	<c:if test="${m.OPTION_TYPE eq 1 }">
																		<c:if test="${m.OPTION_GROUP eq 4}">
																			<c:if test="${m.OPTION_ORDER eq 1 }">
																				<input type="radio" class="noBorder"
																					name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																					value="1"
																					<c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																					id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																				<label
																					for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label>
																			</c:if>

																			<c:if test="${m.OPTION_ORDER eq 2 }">
																				<input type="radio" class="noBorder"
																					name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																					value="0"
																					<c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																					<c:choose>
																			<c:when test="${m.ANSWER2 eq 1}">checked</c:when>
																			<c:when test="${m.ANSWER2 eq null and (cmd.lspd_id eq 1 or cmd.lspd_id eq 27) and (cmd.jml_ttg eq 2 or cmd.jml_ttg eq 3 or cmd.jml_ttg > 3)}">checked</c:when>
																		</c:choose>
																					id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																				<label
																					for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label>
																			</c:if>

																		</c:if>
																	</c:if>
																</c:if>
															</c:forEach> </span></td>

													<td style="width: 120px;"><span class="radio">
															<c:forEach items="${cmd.data_LQLQS}" var="m">
																<c:if test="${a.QUESTION_ID eq m.QUESTION_ID }">
																	<c:if test="${m.OPTION_TYPE eq 1 }">
																		<c:if test="${m.OPTION_GROUP eq 5}">
																			<c:if test="${m.OPTION_ORDER eq 1 }">
																				<input type="radio" class="noBorder"
																					name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																					value="1"
																					<c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																					id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																				<label
																					for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label>
																			</c:if>

																			<c:if test="${m.OPTION_ORDER eq 2 }">
																				<input type="radio" class="noBorder"
																					name="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}"
																					value="0"
																					<c:if test="${m.ANSWER2 eq 1}">checked</c:if>
																					<c:choose>
																			<c:when test="${m.ANSWER2 eq 1}">checked</c:when>
																			<c:when test="${m.ANSWER2 eq null and (cmd.lspd_id eq 1 or cmd.lspd_id eq 27) and (cmd.jml_ttg eq 3 or cmd.jml_ttg > 3)}">checked</c:when>
																		</c:choose>
																					id="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">
																				<label
																					for="${m.QUESTION_ID }_${m.OPTION_TYPE}_${m.OPTION_GROUP}_${m.OPTION_ORDER}">${m.OPTION2}</label>
																			</c:if>

																		</c:if>
																	</c:if>
																</c:if>
															</c:forEach> </span></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</fieldset>
							</div>
					</c:if>
				</c:otherwise>
			</c:choose>
			<c:if test="${cmd.flag eq 4}">
			<br>
			<div style="margin-left: 20px"><input type="button" name="Save" id="Save" value="Save" class="myButton" onClick="proses (5);"></div>
			<br>
			</c:if>
			<c:if test="${cmd.mspo_flag eq \"4\" }">
			<br><div style="margin-left: 20px"><input type="button" name="Save" id="Save" value="Save" class="myButton" onClick="proses (5);"></div><br>
			</c:if>
		</div>
	</form>
</body>
</html>