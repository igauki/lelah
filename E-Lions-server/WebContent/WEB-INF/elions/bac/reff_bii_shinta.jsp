<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css"
	HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script>
<!-- Common Javascripts -->
<script type="text/javascript"
	src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajax.js"></script>
<script type="text/javascript"
	src="${path }/dwr/interface/ajaxManager.js"></script>
<script type="text/javascript" src="${path }/dwr/engine.js"></script>
<script type="text/javascript" src="${path }/dwr/util.js"></script>
<script type="text/javascript">

	function sub(hasil){
		document.frmParam.status.value='insert';
		return confirm(hasil);
	}
	
	function cari_nama(id){
		frmParam.submit();
		
	}
	
	function awal(){
		document.frmParam.status.value='awal';
		if (document.frmParam.statussubmit.value=="1"){
			alert("Reff BSM/SMS sudah berhasil disimpan");
			document.frmParam.statussubmit.value="0";
		}
		
		if (document.frmParam.statussubmit.value=="5"){
			alert("Spaj ini sudah diaksep, tidak bisa diedit lagi.");
		}
	}
	
	function cari_reff(nilai){
	   var show_ajspusat= document.getElementById('show_ajspusat').value;
	   alert(document.frmParam.flag.value);
		if(nilai==1){ //cari agen penutup by nama
			if (document.frmParam.flag.value=="shinta"){
				//alert("shinta");
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama.value,2,show_ajspusat,'select_reff_shinta','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="simas"){
				//alert("simas");
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama.value,3,show_ajspusat,'select_reff_shinta','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="biiothers"){
				//alert("biiothers");
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama.value,12,show_ajspusat,'select_reff_shinta','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="syariah"){
				//alert("shinta");
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama.value,16,show_ajspusat,'select_reff_shinta','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="victoria"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama.value,20,show_ajspusat,'select_reff_shinta','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="harda"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama.value,25,show_ajspusat,'select_reff_shinta','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="shintacekbc"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama.value,2,show_ajspusat,'select_reff_shintacekBc','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="shintacekbcsyh"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama.value,16,show_ajspusat,'select_reff_shintacekBc','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bri"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama.value,42,show_ajspusat,'select_reff_shinta','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="btnsyh"){ //helpdesk [133346] produk baru 142-13 Smart Investment Protection
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama.value,43,show_ajspusat,'select_reff_shinta','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bjb"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama.value,44,show_ajspusat,'select_reff_shinta','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bjatim"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama.value,51,show_ajspusat,'select_reff_shinta','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bjatimsyh"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama.value,58,show_ajspusat,'select_reff_shinta','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bukopin"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama.value,50,show_ajspusat,'select_reff_shinta','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="btnsyariah"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama.value,56,show_ajspusat,'select_reff_shinta','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bukopinsyariah"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama.value,60,show_ajspusat,'select_reff_shinta','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="btn"){ //helpdesk [142003] produk baru Smart Platinum Link RPUL BEL (134-13)
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama.value,63,show_ajspusat,'select_reff_shinta','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}
		}else if(nilai==2){ //cari referral
			if (document.frmParam.flag.value=="shinta" || document.frmParam.flag.value=="shintacekbc" ){
				//alert("shinta");
				//select_reff_shinta2
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama2.value,2,show_ajspusat,'select_reff_shinta2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="simas"){
				//alert("simas");
				//select_reff_shinta2
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama2.value,3,show_ajspusat,'select_reff_shinta2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="biiothers"){
				//alert("biiothers");
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama2.value,12,show_ajspusat,'select_reff_shinta2','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="syariah" || document.frmParam.flag.value=="shintacekbcsyh"){
				//alert("shinta");
				//select_reff_shinta2
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama2.value,16,show_ajspusat,'select_reff_shinta2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="victoria"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama2.value,20,show_ajspusat,'select_reff_shinta2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="harda"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama2.value,25,show_ajspusat,'select_reff_shinta2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bri"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama2.value,42,show_ajspusat,'select_reff_shinta2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="btnsyh"){ //helpdesk [133346] produk baru 142-13 Smart Investment Protection
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama2.value,43,show_ajspusat,'select_reff_shinta2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bjb"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama2.value,44,show_ajspusat,'select_reff_shinta2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bjatim"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama2.value,51,show_ajspusat,'select_reff_shinta2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bjatimsyh"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama2.value,58,show_ajspusat,'select_reff_shinta2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bukopin"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama2.value,50,show_ajspusat,'select_reff_shinta2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="btnsyariah"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama2.value,56,show_ajspusat,'select_reff_shinta2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}
			else if (document.frmParam.flag.value=="bukopinsyariah"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama2.value,60,show_ajspusat,'select_reff_shinta2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="btn"){ //helpdesk [142003] produk baru Smart Platinum Link RPUL BEL (134-13)
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carinama2.value,63,show_ajspusat,'select_reff_shinta2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}
		}else if(nilai==3){ //cari penutup by kode
			if (document.frmParam.flag.value=="shinta"){
				//alert("shinta");
				//select_reff_shinta2
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode.value,2,show_ajspusat,'select_reff_shintabyagentcode','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="simas"){
				//alert("simas");
				//select_reff_shinta2
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode.value,3,show_ajspusat,'select_reff_shintabyagentcode','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="biiothers"){
				//alert("biiothers");
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode.value,12,show_ajspusat,'select_reff_shintabyagentcode','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="syariah"){
				//alert("shinta");
				//select_reff_shinta2
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode.value,16,show_ajspusat,'select_reff_shintabyagentcode','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="victoria"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode.value,20,show_ajspusat,'select_reff_shintabyagentcode','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="harda"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode.value,25,show_ajspusat,'select_reff_shintabyagentcode','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="shintacekbc"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode.value,2,show_ajspusat,'select_reff_shintacekBc2','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="shintacekbcsyh"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode.value,16,show_ajspusat,'select_reff_shintacekBc2','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bri"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode.value,42,show_ajspusat,'select_reff_shintabyagentcode','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="btnsyh"){ //helpdesk [133346] produk baru 142-13 Smart Investment Protection
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode.value,43,show_ajspusat,'select_reff_shintabyagentcode','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bjb"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode.value,44,show_ajspusat,'select_reff_shintabyagentcode','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bjatim"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode.value,51,show_ajspusat,'select_reff_shintabyagentcode','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bjatimsyh"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode.value,58,show_ajspusat,'select_reff_shintabyagentcode','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bukopin"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode.value,50,show_ajspusat,'select_reff_shintabyagentcode','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="btnsyariah"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode.value,56,show_ajspusat,'select_reff_shintabyagentcode','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bukopinsyariah"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode.value,60,show_ajspusat,'select_reff_shintabyagentcode','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="btn"){ //helpdesk [142003] produk baru Smart Platinum Link RPUL BEL (134-13)
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode.value,63,show_ajspusat,'select_reff_shintabyagentcode','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}
		}else if(nilai==4){ //cari referral by kode
			if (document.frmParam.flag.value=="shinta" || document.frmParam.flag.value=="shintacekbc"){
				//alert("shinta");
				//select_reff_shinta2
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode2.value,2,show_ajspusat,'select_reff_shintabyagentcode2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="simas"){
				//alert("simas");
				//select_reff_shinta2
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode2.value,3,show_ajspusat,'select_reff_shintabyagentcode2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="biiothers"){
				//alert("biiothers");
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode2.value,12,show_ajspusat,'select_reff_shintabyagentcode2','nama','lrb_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="syariah" || document.frmParam.flag.value=="shintacekbcsyh" ){
				//alert("shinta");
				//select_reff_shinta2
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode2.value,16,show_ajspusat,'select_reff_shintabyagentcode2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="victoria"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode2.value,20,show_ajspusat,'select_reff_shintabyagentcode2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="harda"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode2.value,25,show_ajspusat,'select_reff_shintabyagentcode2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bri"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode2.value,42,show_ajspusat,'select_reff_shintabyagentcode2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="btnsyh"){ //helpdesk [133346] produk baru 142-13 Smart Investment Protection
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode2.value,43,show_ajspusat,'select_reff_shintabyagentcode2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bjb"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode2.value,44,show_ajspusat,'select_reff_shintabyagentcode2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bjatim"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode2.value,51,show_ajspusat,'select_reff_shintabyagentcode2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bjatimsyh"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode2.value,58,show_ajspusat,'select_reff_shintabyagentcode2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bukopin"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode2.value,50,show_ajspusat,'select_reff_shintabyagentcode2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="btnsyariah"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode2.value,56,show_ajspusat,'select_reff_shintabyagentcode2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="bukopinsyariah"){
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode2.value,60,show_ajspusat,'select_reff_shintabyagentcode2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}else if (document.frmParam.flag.value=="btn"){ //helpdesk [142003] produk baru Smart Platinum Link RPUL BEL (134-13)
				ajaxSelectWithParamReffBiiShinta(document.frmParam.carikode2.value,63,show_ajspusat,'select_reff_shintabyagentcode2','nama2','reff_id','', 'LRB_ID', 'NAMA_REFF', 'cari_nama(this.options[this.selectedIndex].value);');
			}
		}
	}		

</script>
</head>
<BODY
	onload="awal(); resizeCenter(700,350); document.title='PopUp :: Input Informasi Agen Penutup dan Referral'; setupPanes('container1', 'tab1'); "
	style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li><a href="#" onClick="return showPane('pane1', this)"
				id="tab1">Input Informasi Agen Penutup dan Referral <c:choose>
						<c:when test="${empty sessionScope.currentUser.cab_bank}">
							<c:if test="${cmd.flag eq 'shinta' }">
									Konvensional
							</c:if>
							<c:if test="${cmd.flag eq 'syariah' }">
									Syariah
							</c:if>
						</c:when>
						<c:otherwise>

						</c:otherwise>
					</c:choose> </a></li>
		</ul>

		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form name="frmParam" method="post"
					onReset="return confirm('Reset Form Input?')">

					<fieldset>
						<legend>
							Reff For SPAJ:
							<elions:spaj nomor='${cmd.reg_spaj}' />
							(${cmd.flag})
						</legend>
						<table>

							<td valign="top"><spring:bind path="cmd.reg_spaj">
									<input type="hidden" name="${status.expression }"
										value="${status.value}" style='background-color :#D4D4D4'
										readOnly>
								</spring:bind> <spring:bind path="cmd.flag">
									<input type="hidden" name="${status.expression }"
										value="${status.value}" style='background-color :#D4D4D4'
										readOnly>
								</spring:bind> <spring:bind path="cmd.status">
									<input type="hidden" name="${status.expression}"
										value="${status.value }" size="30" maxlength="10" tabindex="7"
										style='background-color :#D4D4D4' readOnly>
								</spring:bind> <spring:bind path="cmd.statussubmit">
									<input type="hidden" name="${status.expression}"
										value="${status.value }" size="30" maxlength="10" tabindex="7"
										style='background-color :#D4D4D4' readOnly>
								</spring:bind> <input type="hidden" id="show_ajspusat"
								value="${show_ajspusat}" size="30" maxlength="10" tabindex="7"
								style='background-color :#D4D4D4' readOnly></td>

							<table>
								<tr>
									<td>
										<table class="entry2">
											<tr>
												<th colspan="2">INFORMASI AGEN PENUTUP</th>
											</tr>
											<tr>
												<th>Level</th>
												<td><spring:bind path="cmd.level_id">
														<input type="text" name="4" value="${status.value }"
															size="3" maxlength="3" tabindex="7"
															style='background-color :#D4D4D4' readOnly>
													</spring:bind></td>
											</tr>
											<tr>
												<th>Cari Agen Penutup(Nama)</th>
												<td><input type="text" value="${cmd.nama_reff}"
													size="27" name="carinama" onfocus="this.select();"
													onkeypress="if(event.keyCode==13){ document.frmParam.btncari1.click(); return false;}">
													<input type="button" name="btncari1" value="Cari"
													onclick="cari_reff(1);"></td>
											</tr>
											<tr>
												<th>Cari Agen Penutup(Kode)</th>
												<td><input type="text" value="${cmd.agent_code}"
													size="27" name="carikode" onfocus="this.select();"
													onkeypress="if(event.keyCode==13){ document.frmParam.btncari3.click(); return false;}">
													<input type="button" name="btncari3" value="Cari"
													onclick="cari_reff(3);"></td>
											</tr>
											<tr>
												<th>Kode Agen Penutup</th>
												<td>
													<div id="nama">
														<select name="lrb_id">
															<option value="${cmd.lrb_id}">${cmd.nama_reff}</option>
														</select>
													</div></td>
											<tr>
												<th>No A/C</th>
												<td><spring:bind path="cmd.no_rek">
														<input type="text" name="${status.expression}"
															value="${status.value }" size="36" maxlength="10"
															tabindex="7" style='background-color :#D4D4D4' readOnly>
													</spring:bind></td>
											</tr>
											<tr>
												<th>Cabang A/C</th>
												<td><spring:bind path="cmd.cab_rek">
														<input type="text" name="${status.expression}"
															value="${status.value }" size="36" maxlength="10"
															tabindex="7" style='background-color :#D4D4D4' readOnly>
													</spring:bind></td>
											</tr>
											<tr>
												<th>Atas Nama</th>
												<td><spring:bind path="cmd.atas_nama">
														<input type="text" name="${status.expression}"
															value="${status.value }" size="36" maxlength="10"
															tabindex="7" style='background-color :#D4D4D4' readOnly>
													</spring:bind></td>
											</tr>
											<tr>
												<th>Aktif</th>
												<td><spring:bind path="cmd.flag_aktif">
														<input type="hidden" name="${status.expression}"
															value="${status.value }" size="36" maxlength="10"
															tabindex="7" style='background-color :#D4D4D4' readOnly>
													</spring:bind> <spring:bind path="cmd.aktif">
														<input type="text" name="${status.expression}"
															value="${status.value }" size="36" maxlength="10"
															tabindex="7" style='background-color :#D4D4D4' readOnly>
													</spring:bind></td>
											</tr>
											<tr>
												<th>NPK</th>
												<td><spring:bind path="cmd.npk">
														<input type="text" name="${status.expression}"
															value="${status.value }" size="36" maxlength="10"
															tabindex="7" style='background-color :#D4D4D4' readOnly>
													</spring:bind> <spring:bind path="cmd.lcb_no">
														<input type="hidden" name="${status.expression}"
															value="${status.value }" size="36" maxlength="10"
															tabindex="7" style='background-color :#D4D4D4' readOnly>
													</spring:bind></td>
											</tr>
											<tr>
												<th>Info Agen BC</th>
												<td><spring:bind path="cmd.lrbj_id1">
														<label for="ya"> <input type="radio"
															class=noBorder name="${status.expression}" value="${status.value }"
															<c:if test="${not empty cmd.lrbj_id1}"> 
																			  checked
																		</c:if>
															id="ya" disabled>Agen BC </label>
														<label for="tidak"> <input type="radio"
															class=noBorder name="${status.expression}" value="${status.value }"
															<c:if test="${empty cmd.lrbj_id1}"> 
																				checked
																			</c:if>
															id="tidak" disabled>Bukan Agen BC </label>
															<input type="hidden" name="${status.expression}"
															value="${status.value }" size="36" maxlength="10"
															tabindex="7" style='background-color :#D4D4D4' readOnly>
													</spring:bind>
												</td>
											</tr>
										</table></td>
									<td>
										<table class="entry2">
											<tr>
												<th colspan="2">INFORMASI REFERRAL (Khusus BSM/SMS)</th>
											</tr>
											<tr>
												<th>Level</th>
												<td><spring:bind path="cmd.level_id2">
														<input type="text" name="4" value="${status.value }"
															size="3" maxlength="3" tabindex="7"
															style='background-color :#D4D4D4' readOnly>
													</spring:bind></td>
											</tr>
											<tr>
												<th>Cari Referral(Nama)</th>
												<td><input type="text" size="27"
													value="${cmd.nama_reff2}" name="carinama2"
													onfocus="this.select();"
													onkeypress="if(event.keyCode==13){ document.frmParam.btncari2.click(); return false;}">
													<input type="button" name="btncari2" value="Cari"
													onclick="cari_reff(2);"></td>
											</tr>
											<tr>
												<th>Cari Referal(Kode)</th>
												<td><input type="text" value="${cmd.agent_code2}"
													size="27" name="carikode2" onfocus="this.select();"
													onkeypress="if(event.keyCode==13){ document.frmParam.btncari4.click(); return false;}">
													<input type="button" name="btncari4" value="Cari"
													onclick="cari_reff(4);"></td>
											</tr>
											<tr>
												<th>Kode Referral</th>
												<td>
													<div id="nama2">
														<select name="reff_id">
															<option value="${cmd.reff_id}">${cmd.nama_reff2}</option>
														</select>
													</div></td>
											<tr>
												<th>No A/C</th>
												<td><spring:bind path="cmd.no_rek2">
														<input type="text" name="${status.expression}"
															value="${status.value }" size="36" maxlength="10"
															tabindex="7" style='background-color :#D4D4D4' readOnly>
													</spring:bind></td>
											</tr>
											<tr>
												<th>Cabang A/C</th>
												<td><spring:bind path="cmd.cab_rek2">
														<input type="text" name="${status.expression}"
															value="${status.value }" size="36" maxlength="10"
															tabindex="7" style='background-color :#D4D4D4' readOnly>
													</spring:bind></td>
											</tr>
											<tr>
												<th>Atas Nama</th>
												<td><spring:bind path="cmd.atas_nama2">
														<input type="text" name="${status.expression}"
															value="${status.value }" size="36" maxlength="10"
															tabindex="7" style='background-color :#D4D4D4' readOnly>
													</spring:bind></td>
											</tr>
											<tr>
												<th>Aktif</th>
												<td><spring:bind path="cmd.flag_aktif2">
														<input type="hidden" name="${status.expression}"
															value="${status.value }" size="36" maxlength="10"
															tabindex="7" style='background-color :#D4D4D4' readOnly>
													</spring:bind> <spring:bind path="cmd.aktif2">
														<input type="text" name="${status.expression}"
															value="${status.value }" size="36" maxlength="10"
															tabindex="7" style='background-color :#D4D4D4' readOnly>
													</spring:bind></td>
											</tr>
											<tr>
												<th>NPK</th>
												<td><spring:bind path="cmd.npk2">
														<input type="text" name="${status.expression}"
															value="${status.value }" size="36" maxlength="10"
															tabindex="7" style='background-color :#D4D4D4' readOnly>
													</spring:bind> <spring:bind path="cmd.lcb_no2">
														<input type="hidden" name="${status.expression}"
															value="${status.value }" size="36" maxlength="10"
															tabindex="7" style='background-color :#D4D4D4' readOnly>
													</spring:bind></td>
											</tr>
											<tr>
												<th>Info Agen BC</th>
												<td><spring:bind path="cmd.lrbj_id1_2">
														<label for="ya"> <input type="radio"
															class=noBorder name="${status.expression}" value="${cmd.lrbj_id1_2}"
															<c:if test="${not empty cmd.lrbj_id1_2}"> 
																			  checked
																		</c:if>
															id="ya" disabled>Agen BC </label>
														<label for="tidak"> <input type="radio"
															class=noBorder name="${status.expression}" value="${cmd.lrbj_id1_2}"
															<c:if test="${empty cmd.lrbj_id1_2}"> 
																				checked
																			</c:if>
															id="tidak" disabled>Bukan Agen BC </label>
															<input type="hidden" name="${status.expression}"
															value="${status.value }" size="36" maxlength="10"
															tabindex="7" style='background-color :#D4D4D4' readOnly>
													</spring:bind>
												</td>
											</tr>
											<c:if test="${not empty lcb_no_kk }">
												<tr>
													<th>Cabang KK</th>
													<td>
														<div id="nama">
															<select name="lcb_no_kk">
																<c:forEach items="${lcb_no_kk}" var="c">
																	<option
																		<c:if test="${cmd.lcb_no_kk eq c.LCB_NO}"> SELECTED </c:if>
																		value="${c.LCB_NO}">${c.NAMA_CABANG}</option>
																</c:forEach>
															</select>
														</div></td>
												</tr>
											</c:if>
										</table></td>
								</tr>
							</table>
							<div id="buttons">
								<input type="submit" value="Save" name="save"
									onclick="sub(this.value+'?');"> <input type="button"
									name="cancel" value="Cancel" onclick="window.close()";>
								<spring:bind path="cmd.*">
									<c:if test="${not empty status.errorMessages}">
										<div id="error">
											ERROR:<br>
											<c:forEach var="error" items="${status.errorMessages}"> - <c:out
													value="${error}" escapeXml="false" />
												<br />
											</c:forEach>
										</div>
									</c:if>
								</spring:bind>
							</div>
							</td>
							</tr>
						</table>
					</fieldset>


				</form>

			</div>
		</div>
	</div>

</body>
</html>