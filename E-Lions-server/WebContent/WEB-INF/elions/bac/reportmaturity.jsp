<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<meta HTTP-EQUIV="Expires" CONTENT="-1">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link href="${path}/include/image/eas.ico" rel="shortcut icon">
		<!--  -->
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<!--  -->
		<script type="text/javascript" src="${path }/include/js/default.js"></script>
		<!-- Ajax Related -->
		<script type="text/javascript" src="${path}/include/js/ajax.js"></script>
		<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script>
		<script type="text/javascript" src="${path }/dwr/engine.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>
		<script type="text/javascript">
			hideLoadingMessage();
			
			function tampil(pormat){
			
				asdf = '';
			
				if(document.formpost.pilih[0].checked) asdf = 'mpr_rate';
				else if(document.formpost.pilih[1].checked) asdf = 'mpr_deposit';
			
				var a = '${path}/report/bac.'+pormat+'?window=maturity_report' + 
					'&username=' + document.formpost.username.value +
					'&pilih=' + asdf +
					'&awal=' + document.formpost.awal.value +
					'&akhir=' + document.formpost.akhir.value +
					'&startDate=' + document.formpost.startDate.value +
					'&endDate=' + document.formpost.endDate.value;
				popWin(a, 600, 800);
			}
			
		</script>
	</head>
	<body onload="setupPanes('container1','tab1');" style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Maturity</a>
				</li>
			</ul>

			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form method="post" name="formpost">
						<fieldset style="text-align: left;">
							<legend>Parameter</legend>
							<table class="entry2" style="width: auto;">
								<tr>
									<th nowrap="nowrap" style="width: 120px;">User</th>
									<th class="left" colspan="2">
										<input type="text" id="username" name="username" value="${username}" disabled="disabled">
									</th>
								</tr>
								<tr>
									<th nowrap="nowrap">Pilihan</th>
									<th class="left">
										<c:forEach items="${daftarPilihan}" var="d" varStatus="s">
											<label for="pilih${s.index}"><input value="${d.key}" class="noBorder" type="radio" <c:if test="${pilih eq d.key}"> checked </c:if> name="pilih" id="pilih${s.index}">${d.value}</label><br/>
										</c:forEach>
									</th>
									<th class="left">
										<input style="text-align: right;" type="text" id="awal" name="awal" value="${awal}" size="10" > s/d
										<input style="text-align: right;" type="text" id="akhir" name="akhir" value="${akhir}" size="10" >								
									</th>
								</tr>
								<tr>
									<th nowrap="nowrap" style="width: 120px;">Tanggal Mulai</th>
									<th class="left" colspan="2">
										<script>inputDate('startDate', '${startDate}', false);</script> s/d
										<script>inputDate('endDate', '${endDate}', false);</script>
									</th>
								</tr>
								<tr>
									<th nowrap="nowrap"></th>
									<td colspan="2">
										<input type="button" name="pdf" value="Show PDF" onclick="tampil('pdf');">
										<input type="button" name="xls" value="Show XLS" onclick="tampil('xls');">
									</td>
								</tr>
							</table>
						</fieldset>	
					</form>						
				</div>
			</div>
		</div>
	</body>
</html>