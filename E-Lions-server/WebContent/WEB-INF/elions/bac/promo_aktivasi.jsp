<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	    $().ready(function() {
	    
	    var spaj = '${spaj}';
	    	  	    
		$("#tabs").tabs();
		$("#load").hide();
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol	 
		
		// user memilih cabang, maka tampilkan admin2nya
		$("#jen_promo").change(function() {
		$('#info_promo').empty();
		
		
		//alert(spaj);
		 var url = "${path}/bac/multi.htm?window=promo_aktiv&type=promovalid&jen_promo="+$('#jen_promo').val();
			$.getJSON(url, function(result) {
				
					$('#info_promo').text(result);
				
			});
			

		});
		
		// Aktivasi
		$("#btnAktiv").click(function() {
			$('#pesan').empty();		
		
			 var url = "${path}/bac/multi.htm?window=promo_aktiv&type=btnAktiv&jen_promo="+$('#jen_promo').val()+"&spajreff="+$('#spajreff').val()+"&spaj="+$('#spaj').val();
			$.getJSON(url, function(result) {
					alert(result);			
				
			});
		});
		
		// Aktivasi
		$("#btnCek").click(function() {
			$('#pesan').empty();		
		
			 var url = "${path}/bac/multi.htm?window=promo_aktiv&type=btnCek&jen_promo="+$('#jen_promo').val()+"&spajreff="+$('#spajreff').val()+"&spaj="+$('#spaj').val();
			$.getJSON(url, function(result) {
					alert(result);			
				
			});
		});
		
		$("#btnSearch").click(function() {
			var hp = $("#hp").val();
			var seri = $("#seri").val();
			var namapp = $("#namapp").val();
			if(hp ==null || hp==""){
			   	alert("Masukan no hp Terlebih Dahulu!");			   
				return false;
			}else if(seri ==null || seri==""){
				alert("Masukan no seri Terlebih Dahulu!");			   
				return false;
			}else if(namapp ==null || namapp==""){
				alert("Masukan nama pemegang Terlebih Dahulu!");			   
				return false;					
			}else{
			    $("#btnSearch").val("Loading....Please Wait...!");
				$("#btnSearch").hide();
				$("#main").fadeOut();
				$("#load").fadeIn();
				return true;
			};
		
		});
		
		
	});	
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }
</style>

<body>
<input id="spaj" name="spaj" type="hidden" value="${spaj}">
		
	 <div id="main">   
	 <form method="post" name="formpost" enctype="multipart/form-data">
							<fieldset class="ui-widget ui-widget-content">	
								<legend class="ui-widget-header ui-corner-all"><div><h5>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; AKTIVASI PROMO</h5></div></legend>
									&nbsp;&nbsp;
									<table class = "some" >	
									<tr>
									  <th>  Jenis Promo </th>
									  <td> :</td>
									  <td>
									    
										<select name="jen_promo" id="jen_promo" >
											<c:forEach var="c" items="${jn_promo}" varStatus="s">
												<option value="${c.key}">${c.value}</option>
											</c:forEach>
										</select>

									  </td>									  
									</tr>
									<tr>
									  <th>  Referensi SPAJ Utama</th>
									  <td> :</td>
									  <td>
									     <input type="text" name="spajreff" id="spajreff" size="20" <c:if test="${spajreff ne ''}">value="${spajreff}" </c:if>>					 					
									  </td>									  
									</tr>
												
									
									</table>
									&nbsp;
									<div class="rowElem"> 		
										<center> <button id="btnCek" title="Check SPAJ Utama" onclick="return false;">Check</button>									
										<button id="btnAktiv" title="Aktivasi" onclick="return false;">Aktivasi</button></center>	
									</div>	 	
									
																														
							</fieldset>
							
							  <span id="info_promo" style="color: red; font-size: 0.9em; font-style: italic;"></span>
									  
						</form>
			</div>	
<c:if test="${not empty pesan}">
	<script>alert('${pesan}');</script>
</c:if>		
</body>