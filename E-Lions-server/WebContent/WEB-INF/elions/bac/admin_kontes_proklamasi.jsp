<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
	<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta NAME="Description" CONTENT="EkaLife">
	<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<meta HTTP-EQUIV="Expires" CONTENT="-1">
	<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
	<link href="${path}/include/image/eas.ico" rel="shortcut icon">
	<!--  -->
	<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
	<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
	<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
	<!--  -->
	<script type="text/javascript" src="${path }/include/js/default.js"></script>
	<!-- Ajax Related -->
	<script type="text/javascript" src="${path}/include/js/ajax.js"></script>
	<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script>
	<script type="text/javascript" src="${path }/dwr/engine.js"></script>
	<script type="text/javascript" src="${path }/dwr/util.js"></script>
	
	<script type="text/javascript">
		hideLoadingMessage();
		
		function waitPreloadPage() { //DOM
			<c:if test='${not empty pesan}'>
	            alert('${pesan}');
	        </c:if>
			
			//calendar.set("tgl_lahir");
			
			if (document.getElementById){
				document.getElementById('prepage').style.visibility='hidden';
			}else{
				if (document.layers){ //NS4
					document.prepage.visibility = 'hidden';
				}
				else { //IE4
					document.all.prepage.style.visibility = 'hidden';
				}
			}
		}
		// End -->
	</script>
</head>

<body onload="waitPreloadPage();" >
<div id="prepage" style="position:absolute; font-family:arial; font-size:12; left:0px; top:0px; background-color:yellow; layer-background-color:white;"> 
	<table border="2" bordercolor="red"><tr><td style="color: red;"><b>Loading ... ... Please wait!</b></td></tr></table>
</div>
<form:form method="post" name="formpost" id="formpost" action="" commandName="command">
	<fieldset>
		<legend style="font-size: 12px; color: black; font-weight: bolder;">Kontes Proklamasi</legend>
		<table class="entry" style="text-transform: uppercase;" width="100%">
			<tr>
				<th>No</th>
				<th>Kode</th>
				<th>Nama</th>
				<th>Tgl Lahir</th>
				<th>Telp</th>
				<th>E-mail</th>
				<th>Alamat</th>
				<th>Marital Status</th>
				<th>Pendidikan</th>
				<th>Pekerjaan</th>
				<th>Video</th>
			</tr>
			<pg:paging url="${path}/bac/multi.htm?window=admin_kontes_proklamasi" pageSize="20">
			<c:forEach items="${data }" var="d" varStatus="s">
			<pg:item>
				<tr>
					<td>${s.index+1 }.</td>
					<td>${d.MASTER_LEAD_ID }</td>
					<td>${d.NAMA }</td>
					<td>${d.BDATE }</td>
					<td>${d.TELP }</td>
					<td>${d.EMAIL }</td>
					<td>${d.ALAMAT }</td>
					<td>${d.STATUS }</td>
					<td>${d.PENDIDIKAN }</td>
					<td>${d.PEKERJAAN }</td>
					<td><iframe width="140" height="105" src="${d.VIDEO }" frameborder="0" allowfullscreen></iframe></td>
				</tr>
			</pg:item>
			</c:forEach>
			<pg:index>
				  	<pg:page><%=thisPage%></pg:page> 
			</pg:index>
			</pg:paging>
		</table>
	</fieldset>
	
</form:form>
</body>
</html>	
