<%@ include file="/include/page/header.jsp"%><c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>
<script>

<spring:bind path="cmd.pemegang.jmlkyc"> 
	var jmlDaftarKyc=${status.value};
</spring:bind>

<spring:bind path="cmd.pemegang.jmlkyc2"> 
	var jmlDaftarKyc2=${status.value};
</spring:bind>

	function generateXML_sumberKyc( objParser, strID, strDesc ) {
		Optiondana = "";
		var collPosition = objParser.selectNodes( "//Position" );
		for( var i = 0; i < collPosition.length; i++ ) Optiondana += "<OPTION " + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
	}	
	
	function Body_onload() {

	var nameSelection
		xmlData.async = false;
		xmlData.src = "/E-Lions/xml/SUMBER_PENDANAAN.xml";
		xmlData.src = "/E-Lions/xml/SUMBER_PENGHASILAN.xml";
		generateXML_sumberKyc(xmlData, 'ID','DANA');

		document.frmParam.alamat_sementara.style.display="none";

		<c:if test="${not empty cmd.pemegang.keterangan_blanko_spaj}">
			if(confirm("Nomor Blanko yang dicantumkan sudah pernah digunakan untuk produk yang sama.\nTekan YES untuk melihat data sebelumnya, atau CANCEL untuk melanjutkan input.")){
				//FIXME : popup window diwsini
				popWin('${path}/uw/view.htm?p=v&c=c&showSPAJ=${cmd.pemegang.keterangan_blanko_spaj}', 500, 800);
			}
		</c:if>

		document.frmParam.elements['tertanggung.mste_age'].style.backgroundColor ='#D4D4D4';

		if (((document.frmParam.elements['tertanggung.mkl_tujuan'].value).toUpperCase()!=("Lain - Lain").toUpperCase()) && (document.frmParam.elements['tertanggung.mkl_tujuan'].value!=null) )
		{
			document.frmParam.elements['tertanggung.tujuana'].value='';
		}else{
			if ((document.frmParam.elements['tertanggung.tujuana'].value).toUpperCase()==("Lain - Lain").toUpperCase())
			{
				document.frmParam.elements['tertanggung.tujuana'].value='';
			}
		}
		
		if  (((document.frmParam.elements['tertanggung.mkl_pendanaan'].value).toUpperCase()!=("Lainnya").toUpperCase()) && (document.frmParam.elements['tertanggung.mkl_pendanaan'].value!=null))
		{
			document.frmParam.elements['tertanggung.danaa'].value='';
		}else{
			if  ((document.frmParam.elements['tertanggung.danaa'].value).toUpperCase()==("Lainnya").toUpperCase())
			{
				document.frmParam.elements['tertanggung.danaa'].value='';
			}
		}
		
		if  (((document.frmParam.elements['tertanggung.mkl_smbr_penghasilan'].value).toUpperCase() != ("Lainnya").toUpperCase()) && (document.frmParam.elements['tertanggung.mkl_smbr_penghasilan'].value!=null))
		{
//			document.frmParam.elements['tertanggung.danaa2'].value='';
			document.frmParam.elements['tertanggung.shasil'].value='';
		}else{
//			if ((document.frmParam.elements['tertanggung.danaa2'].value).toUpperCase() ==("Lainnya").toUpperCase())
			if ((document.frmParam.elements['tertanggung.shasil'].value).toUpperCase() ==("Lainnya").toUpperCase())
			{
//				document.frmParam.elements['tertanggung.danaa2'].value='';
				document.frmParam.elements['tertanggung.shasil'].value='';
			}
		}
		
		if  (((document.frmParam.elements['tertanggung.mkl_kerja'].value).toUpperCase()!=("Lainnya").toUpperCase()) &&(document.frmParam.elements['tertanggung.mkl_kerja'].value!=null))
		{
			document.frmParam.elements['tertanggung.kerjaa'].value='';
		}else{
			if  ((document.frmParam.elements['tertanggung.kerjaa'].value).toUpperCase()==("Lainnya").toUpperCase())
			{
				document.frmParam.elements['tertanggung.kerjaa'].value='';
			}
		}
		
		if (((document.frmParam.elements['tertanggung.mkl_kerja'].value).toUpperCase()!=("Karyawan Swasta").toUpperCase()) && (document.frmParam.elements['tertanggung.mkl_kerja'].value!=null))
		{
			document.frmParam.elements['tertanggung.kerjab'].value='';
		}else{
			if ((document.frmParam.elements['tertanggung.kerjab'].value).toUpperCase()==("Karyawan Swasta").toUpperCase())
			{
				document.frmParam.elements['tertanggung.kerjab'].value='';
			}
		}
				
		if  (((document.frmParam.elements['tertanggung.mkl_industri'].value).toUpperCase()!=("Lainnya").toUpperCase()) && (document.frmParam.elements['tertanggung.mkl_industri'].value!=null))
		{
			document.frmParam.elements['tertanggung.industria'].value='';
		}else{
			if ((document.frmParam.elements['tertanggung.industria'].value).toUpperCase()==("Lainnya").toUpperCase())
			{
				document.frmParam.elements['tertanggung.industria'].value='';
			}
		}
		
		document.frmParam.elements['addressbilling.kota_tgh'].value='${cmd.addressbilling.kota_tgh}';
		document.frmParam.elements['addressbilling.kota_tgh'].readOnly = true;
		document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D4D4D4';
		
		document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.alamat_sementara.value;
		document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
		document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D4D4D4';
		
		document.frmParam.elements['addressbilling.msap_zip_code'].value='${cmd.addressbilling.msap_zip_code}';
		document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
		document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D4D4D4';
		
		document.frmParam.elements['addressbilling.msap_area_code1'].value='${cmd.addressbilling.msap_area_code1}';
		document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
		document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D4D4D4';
		
		document.frmParam.elements['addressbilling.msap_phone1'].value='${cmd.addressbilling.msap_phone1}';
		document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
		document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D4D4D4';
		
		document.frmParam.elements['addressbilling.msap_area_code2'].value='${cmd.addressbilling.msap_area_code2}';
		document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;
		document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D4D4D4';
		
		document.frmParam.elements['addressbilling.msap_phone2'].value='${cmd.addressbilling.msap_phone2}';
		document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
		document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D4D4D4';
		
		document.frmParam.elements['addressbilling.msap_area_code3'].value='${cmd.addressbilling.msap_area_code3}';
		document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
		document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D4D4D4';
		
		document.frmParam.elements['addressbilling.msap_phone3'].value='${cmd.addressbilling.msap_phone3}';
		document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
		document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D4D4D4';				
		
		document.frmParam.elements['addressbilling.no_hp'].value='${cmd.addressbilling.no_hp}';
		document.frmParam.elements['addressbilling.no_hp'].readOnly = true;
		document.frmParam.elements['addressbilling.no_hp'].style.backgroundColor ='#D4D4D4';

		document.frmParam.elements['addressbilling.no_hp2'].value='${cmd.addressbilling.no_hp2}';
		document.frmParam.elements['addressbilling.no_hp2'].readOnly = true;
		document.frmParam.elements['addressbilling.no_hp2'].style.backgroundColor ='#D4D4D4';		
		
		document.frmParam.elements['addressbilling.e_mail'].value='${cmd.addressbilling.e_mail}';
		document.frmParam.elements['addressbilling.e_mail'].readOnly = true;
		document.frmParam.elements['addressbilling.e_mail'].style.backgroundColor ='#D4D4D4';
		
		document.frmParam.elements['addressbilling.msap_area_code_fax1'].value='${cmd.addressbilling.msap_area_code_fax1}';
		document.frmParam.elements['addressbilling.msap_area_code_fax1'].readOnly = true;
		document.frmParam.elements['addressbilling.msap_area_code_fax1'].style.backgroundColor ='#D4D4D4';		
		
		document.frmParam.elements['addressbilling.msap_fax1'].value='${cmd.addressbilling.msap_fax1}';
		document.frmParam.elements['addressbilling.msap_fax1'].readOnly = true;
		document.frmParam.elements['addressbilling.msap_fax1'].style.backgroundColor ='#D4D4D4';				
		firstRelationCheck()
		
		if (document.frmParam.document.frmParam.tanda_pp.value==null || document.frmParam.document.frmParam.tanda_pp.value=="")
		{
			document.frmParam.elements['tertanggung.mkl_dana_from'].value = '0';
			document.frmParam.elements['tertanggung.mkl_sumber_premi'].value = '0';
			document.frmParam.document.frmParam.tanda_pp.value = '0';
		}
		
		cpy(document.frmParam.tanda_pp.value);
		
		if (document.frmParam.document.frmParam.tanda_hub.value==null || document.frmParam.document.frmParam.tanda_hub.value=="")
		{
			document.frmParam.elements['tertanggung.mkl_sumber_premi'].value = '0';
			document.frmParam.document.frmParam.tanda_hub.value = '0';
		}
		
		cpy2(document.frmParam.tanda_hub.value);
	}
	
	function cpy(hasil){
		var relasi = '${cmd.pemegang.lsre_id}';
		document.frmParam.elements['tertanggung.mkl_dana_from'].value = hasil;
		document.frmParam.tanda_pp.value = hasil;
		if (document.frmParam.tanda_pp.value == '0'){
		// *alert(1);
			if ((relasi=='1')){
				document.frmParam.elements['tertanggung.lsre_id_premi'].disabled = true;
				document.frmParam.elements['tertanggung.lsre_id_premi'].style.backgroundColor ='#FFFFFF';
				document.getElementById("sumberPremi").disabled = true;
 			}else{
 				document.getElementById("sumberPremi").disabled = false;
 			}}}
 
	function cpy2(hasil2){
 		var relasi = '${cmd.pemegang.lsre_id}';
		document.frmParam.elements['tertanggung.mkl_sumber_premi'].value = hasil2;
		document.frmParam.tanda_hub.value = hasil2;
			if (document.frmParam.tanda_hub.value == '0'){
				if ((relasi=='1')){
					document.frmParam.elements['tertanggung.lsre_id_premi'].disabled = true;
 				}else{
 					document.frmParam.elements['tertanggung.lsre_id_premi'].disabled = false;
 			}}}
	
	function addRowDOM1 (tableID, jml) {
		flag_add1 = 1
   		var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
		var tmp = parseInt(document.getElementById("tableDanax").rows.length);
		var oRow = oTable.insertRow(oTable.rows.length);
        var oCells = oRow.cells;
		var i=tmp-1;
		var idx = jmlDaftarKyc + 1;
//		alert(idx);
		
		if (document.all) {
			var cell = oRow.insertCell(oCells.length);			
				cell.innerHTML = '';
			var cell = oRow.insertCell(oCells.length);			
				cell.innerHTML = '';
			var cell = oRow.insertCell(oCells.length);
				cell.innerHTML ="<td ><select name='listkyc.kyc_desc1"+idx+"'>"+Optiondana+"</select></td>";
			var cell = oRow.insertCell(oCells.length);
				cell.innerHTML = "<td ><input type=text name='listkyc.kyc_desc_x"+idx+"' value='' size=42 maxlength=50></td>";
			var cell = oRow.insertCell(oCells.length);
				cell.innerHTML ="<td ><input type=checkbox class=\"noBorder\" name=cek"+idx+" id=ck"+idx+"' ></td></tr>";
				jmlDaftarKyc = idx;
				}
			}
		function addRowDOM2 (tableID, jml) {
			flag_add2_x = 1
   			var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
			var tmp = parseInt(document.getElementById("tableDanax2").rows.length);
			var oRow = oTable.insertRow(oTable.rows.length);
        	var oCells = oRow.cells;
			var i=tmp-1;
			var idx = jmlDaftarKyc2 + 1;
//				alert(idx);
			if (document.all) {
				var cell = oRow.insertCell(oCells.length);			
					cell.innerHTML = '';
				var cell = oRow.insertCell(oCells.length);			
					cell.innerHTML = '';
				var cell = oRow.insertCell(oCells.length);
					cell.innerHTML ="<td ><select name='listkyc2.kyc_desc2"+idx+"'>"+Optiondana+"</select></td>";
				var cell = oRow.insertCell(oCells.length);
					cell.innerHTML = "<td ><input type=text name='listkyc2.kyc_desc2_x"+idx+"' value='' size=42 maxlength=50></td>";
				var cell = oRow.insertCell(oCells.length);
					cell.innerHTML ="<td ><input type=checkbox class=\"noBorder\" name=cek2"+idx+" id=ck2"+idx+"' ></td></tr>";
					jmlDaftarKyc2 = idx;
					}
				}		
	function deleteRowDOM1 (tableID, rowCount)
 		{ 
    	var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
    	var row=parseInt(document.getElementById("tableDanax").rows.length);
		var flag_row=0;
		var jumlah_cek=0;
		var idx = jmlDaftarKyc;

		if(row>0)
		{
			flag_row=0;
			for (var i=1;i<((parseInt(row)));i++)
			{
			if (eval("document.frmParam.elements['cek"+i+"'].checked"))
			{
				idx=idx-1;

				for (var k =i ; k<((parseInt(row))-1);k++)
				{
					eval(" document.frmParam.elements['listkyc.kyc_desc1"+k+"'].value =document.frmParam.elements['listkyc.kyc_desc1"+(k+1)+"'].value;");
					eval(" document.frmParam.elements['listkyc.kyc_desc_x"+k+"'].value =document.frmParam.elements['listkyc.kyc_desc_x"+(k+1)+"'].value;");  
					eval(" document.frmParam.elements['cek"+k+"'].checked = document.frmParam.elements['cek"+(k+1)+"'].checked;");
				}
				oTable.deleteRow(parseInt(document.getElementById("tableDanax").rows.length)-1);
				row=row-1;
				jmlDaftarKyc = idx;
				i=i-1;
			}
		}
		row=parseInt(document.getElementById("tableDanax").rows.length);
			if(row==1)	
				flag_add=0;
		
		}
	}
function deleteRowDOM2 (tableID, rowCount)
 	{ 
    var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
    var row=parseInt(document.getElementById("tableDanax2").rows.length);
	var flag_row=0;
	var jumlah_cek=0;
	var idx = jmlDaftarKyc2;

	if(row>0)
	{
		flag_row=0;
		for (var i=1;i<((parseInt(row)));i++)
		{
			if (eval("document.frmParam.elements['cek2"+i+"'].checked"))
			{
				idx=idx-1;

				for (var k =i ; k<((parseInt(row))-1);k++)
				{
					eval(" document.frmParam.elements['listkyc2.kyc_desc2"+k+"'].value =document.frmParam.elements['listkyc2.kyc_desc2"+(k+1)+"'].value;");
					eval(" document.frmParam.elements['listkyc2.kyc_desc2_x"+k+"'].value =document.frmParam.elements['listkyc2.kyc_desc2_x"+(k+1)+"'].value;");  
					eval(" document.frmParam.elements['cek2"+k+"'].checked = document.frmParam.elements['cek2"+(k+1)+"'].checked;");
				}
				oTable.deleteRow(parseInt(document.getElementById("tableDanax2").rows.length)-1);
				row=row-1;
				jmlDaftarKyc2 = idx;
				i=i-1;
			}
		}
		row=parseInt(document.getElementById("tableDanax2").rows.length);
			if(row==1)	
				flag_add2=0;
		
		}
	}
function cancel1()
{		
	var idx = jmlDaftarKyc;

	if  ((idx)!=null && (idx)!="")
	{
		if (idx >0)
			flag_add1=1;
	}
	if(flag_add1==1)
	{
		deleteRowDOM1('tableDanax', '1');
	}
}
function cancel2()
{		
	var idx = jmlDaftarKyc2;

	if  ((idx)!=null && (idx)!="")
	{
		if (idx >0)
			flag_add2_x=1;
	}
	if(flag_add2_x==1)
	{
		deleteRowDOM2('tableDanax2', '1');
	}
}
		
	function firstRelationCheck(){
	var relasi = '${cmd.pemegang.lsre_id}';

		 if  ( (relasi=='1') ) {
		 		document.frmParam.elements['tertanggung.nama_si'].readOnly = true;
		 		document.frmParam.elements['tertanggung.nama_si'].style.backgroundColor ='#D4D4D4';
		 		document.frmParam.elements['tertanggung.nama_anak1'].readOnly = true;
		 		document.frmParam.elements['tertanggung.nama_anak1'].style.backgroundColor ='#D4D4D4';
		 		document.frmParam.elements['tertanggung.nama_anak2'].readOnly = true;
		 		document.frmParam.elements['tertanggung.nama_anak2'].style.backgroundColor ='#D4D4D4';
			 	document.frmParam.elements['tertanggung.nama_anak3'].readOnly = true;
		 		document.frmParam.elements['tertanggung.nama_anak3'].style.backgroundColor ='#D4D4D4';
		 				 
				document.frmParam.elements['tertanggung.email'].readOnly = true;
				document.frmParam.elements['tertanggung.email'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.area_code_fax'].readOnly = true;
				document.frmParam.elements['tertanggung.area_code_fax'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.no_fax'].readOnly = true;
				document.frmParam.elements['tertanggung.no_fax'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['tertanggung.lti_id'].disabled = true;			
				document.frmParam.elements['tertanggung.lti_id'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.mcl_first'].readOnly = true;
				document.frmParam.elements['tertanggung.mcl_first'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.mcl_gelar'].readOnly = true;
				document.frmParam.elements['tertanggung.mcl_gelar'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.mspe_mother'].readOnly = true;
				document.frmParam.elements['tertanggung.mspe_mother'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.lside_id'].readOnly = true;
				document.frmParam.elements['tertanggung.lside_id'].disabled = true;			
				document.frmParam.elements['tertanggung.lside_id'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.mspe_no_identity'].readOnly = true;
				document.frmParam.elements['tertanggung.mspe_no_identity'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.lsne_id'].readOnly = true;
				document.frmParam.elements['tertanggung.lsne_id'].disabled = true;			
				document.frmParam.elements['tertanggung.lsne_id'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.mspe_place_birth'].readOnly = true;
				document.frmParam.elements['tertanggung.mspe_place_birth'].style.backgroundColor ='#D4D4D4';

				document.frmParam.elements['tertanggung.mspe_sts_mrt'].readOnly = true;
				document.frmParam.elements['tertanggung.mspe_sts_mrt'].disabled = true;			
				document.frmParam.elements['tertanggung.mspe_sts_mrt'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.lsag_id'].readOnly = true;
				document.frmParam.elements['tertanggung.lsag_id'].disabled = true;			
				document.frmParam.elements['tertanggung.lsag_id'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.mcl_agama'].disabled = true;			
				document.frmParam.elements['tertanggung.mcl_agama'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.lsed_id'].readOnly = true;
				document.frmParam.elements['tertanggung.lsed_id'].disabled= true;			
				document.frmParam.elements['tertanggung.lsed_id'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.alamat_rumah'].readOnly = true;
				document.frmParam.elements['tertanggung.alamat_rumah'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.kota_rumah'].readOnly = true;
				document.frmParam.elements['tertanggung.kota_rumah'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['tertanggung.kd_pos_rumah'].readOnly = true;
				document.frmParam.elements['tertanggung.kd_pos_rumah'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.area_code_rumah'].readOnly = true;
				document.frmParam.elements['tertanggung.area_code_rumah'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.telpon_rumah'].readOnly = true;
				document.frmParam.elements['tertanggung.telpon_rumah'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.area_code_rumah2'].readOnly = true;
				document.frmParam.elements['tertanggung.area_code_rumah2'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.telpon_rumah2'].readOnly = true;
				document.frmParam.elements['tertanggung.telpon_rumah2'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.alamat_kantor'].readOnly = true;
				document.frmParam.elements['tertanggung.alamat_kantor'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.kota_kantor'].readOnly = true;
				document.frmParam.elements['tertanggung.kota_kantor'].style.backgroundColor ='#D4D4D4';

				document.frmParam.elements['tertanggung.kd_pos_kantor'].readOnly = true;
				document.frmParam.elements['tertanggung.kd_pos_kantor'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.area_code_kantor'].readOnly = true;
				document.frmParam.elements['tertanggung.area_code_kantor'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.telpon_kantor'].readOnly = true;
				document.frmParam.elements['tertanggung.telpon_kantor'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.area_code_kantor2'].readOnly = true;				
				document.frmParam.elements['tertanggung.area_code_kantor2'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.telpon_kantor2'].readOnly = true;
				document.frmParam.elements['tertanggung.telpon_kantor2'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['tertanggung.mkl_kerja'].readOnly = true;
				document.frmParam.elements['tertanggung.mkl_kerja'].disabled = true;			
				document.frmParam.elements['tertanggung.mkl_kerja'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.kerjaa'].readOnly = true;
				document.frmParam.elements['tertanggung.kerjaa'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.kerjab'].readOnly = true;
				document.frmParam.elements['tertanggung.kerjab'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['tertanggung.mkl_industri'].readOnly = true;
				document.frmParam.elements['tertanggung.mkl_industri'].disabled = true;
				document.frmParam.elements['tertanggung.mkl_industri'].style.backgroundColor ='#D4D4D4';			
				document.frmParam.elements['tertanggung.industria'].readOnly = true;
				document.frmParam.elements['tertanggung.industria'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.mkl_tujuan'].readOnly = true;
				document.frmParam.elements['tertanggung.mkl_tujuan'].disabled= true;			
				document.frmParam.elements['tertanggung.mkl_tujuan'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.tujuana'].readOnly = true;
				document.frmParam.elements['tertanggung.tujuana'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.mkl_pendanaan'].readOnly = true;
				document.frmParam.elements['tertanggung.mkl_pendanaan'].disabled= true;			
				document.frmParam.elements['tertanggung.mkl_pendanaan'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.mkl_smbr_penghasilan'].readOnly = true;
				document.frmParam.elements['tertanggung.mkl_smbr_penghasilan'].disabled= true;			
				document.frmParam.elements['tertanggung.mkl_smbr_penghasilan'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.danaa'].readOnly = true;
				document.frmParam.elements['tertanggung.danaa'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.shasil'].readOnly = true;
				document.frmParam.elements['tertanggung.shasil'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.mkl_penghasilan'].readOnly = true;
				document.frmParam.elements['tertanggung.mkl_penghasilan'].disabled= true;			
				document.frmParam.elements['tertanggung.mkl_penghasilan'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.no_hp2'].readOnly = true;
				document.frmParam.elements['tertanggung.no_hp2'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.no_hp'].readOnly = true;
				document.frmParam.elements['tertanggung.no_hp'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['tertanggung.mkl_dana_from'].disabled = true;
				document.frmParam.elements['tertanggung.mkl_hasil_from'].disabled = true;
				document.frmParam.elements['tertanggung.mkl_sumber_premi'].disabled = true;
				document.frmParam.elements['tertanggung.lsre_id_premi'].readOnly = true;
				document.frmParam.elements['tertanggung.lsre_id_premi'].disabled = true;			
				document.frmParam.elements['tertanggung.lsre_id_premi'].style.backgroundColor ='#D4D4D4';
				document.getElementById("danaFrom").disabled = true;
				document.getElementById("sumberPremi").disabled = true;
				document.getElementById("hasilFrom").disabled = true;


				if('${cmd.pemegang.mspe_sex}' == '1'){
					document.frmParam.jenis_kelamin[0].checked=true;
					document.frmParam.elements['tertanggung.mspe_sex'].value == '1';
				}else{
					document.frmParam.jenis_kelamin[1].checked=true;
					document.frmParam.elements['tertanggung.mspe_sex'].value == '0';
				}
				document.frmParam.jenis_kelamin[0].disabled = true;
				document.frmParam.jenis_kelamin[1].disabled = true;
				document.frmParam.elements['tertanggung.mspe_sex'].value = '${cmd.pemegang.mspe_sex}';
			
			}
	}
		
</script>
<script language="JavaScript">
<!--
	if (!document.layers&&!document.all&&!document.getElementById)
		event="test"
		
	function showtip(current,e,text){
	
		if (document.all||document.getElementById){
			thetitle=text.split('<br>')
			if (thetitle.length>1){
				thetitles=''
				
			for (i=0;i<thetitle.length;i++)
				thetitles+=thetitle[i]
				
			current.title=thetitles
		}
		else
			current.title=text
	}
	
	else if (document.layers){
				document.tooltip.document.write('<layer bgColor="white" style="border:1px solid black;font-size:13px;">'+text+'</layer>')
				document.tooltip.document.close()
				document.tooltip.left=e.pageX+5
				document.tooltip.top=e.pageY+5
				document.tooltip.visibility="show"
			}
	}
	
	function hidetip(){
		if (document.layers)
			document.tooltip.visibility="hidden"
	}
	
		function next()
	{
		eval( " document.frmParam.elements['pemegang.indeks_halaman'].value = ((document.frmParam.hal.value)+1);");
			if(document.frmParam.jenis_kelamin[0].checked==true){
					document.frmParam.elements['tertanggung.mspe_sex'].value = '1';
			}else{
				document.frmParam.elements['tertanggung.mspe_sex'].value = '0';
			}	
			
	}

	function kelamin(hasil)
	{
		document.frmParam.elements['tertanggung.mspe_sex'].value = hasil;
	}
	
	function edit_onClick1(){
			with(document.frmParam){
			if(edit_tertanggung.checked==true){
				document.getElementById('edit_tertanggung').checked=true;
				document.getElementById('edit_tertanggung').value='1';
				elements['tertanggung.edit_tertanggung'].value = '1';
			}else{
				document.getElementById('edit_tertanggung').value='0';
				elements['tertanggung.edit_tertanggung'].value = '0';	
			}
		}
	} 
 
// -->
</script>


<body onLoad="Body_onload();">

<XML ID=xmlData></XML>
<form name="frmParam" method="post">
<table class="entry2">
	<tr  >
		<td>
			<input type="submit" name="_target0" value=" "  <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/pp1.jpg);" 
				accesskey="1" onmouseover="return overlib('Alt-1', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<c:choose><c:when test="${(sessionScope.currentUser.jn_bank eq '16')}">
			<input type="submit" name="_target1" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ttgbsim.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();"></c:when><c:otherwise>
			<input type="submit" name="_target1" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ttg1.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();"></c:otherwise></c:choose>				
			<input type="submit" name="_target2" value=" "  <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ddu1.jpg);"
				accesskey="3" onmouseover="return overlib('Alt-3', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target3" value=" "  <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/inv1.jpg);"
				accesskey="4" onmouseover="return overlib('Alt-4', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target4" value=" "  <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ag1.jpg);"
				accesskey="5" onmouseover="return overlib('Alt-5', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target5" value=" " <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/kf1.jpg);"
				accesskey="6" onmouseover="return overlib('Alt-6', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" name="dummy2" value=" "  <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/sb1.jpg);">
			<textarea cols="40" rows="7" name="alamat_sementara"    
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); ">${cmd.addressbilling.msap_address}</textarea>
		
</td>
	</tr>
	<tr>
		<td width="67%" align="left">

			<c:if test="${not empty cmd.pemegang.keterangan_blanko_spaj}">
				<div id="error">INFO:<br>
			  		${cmd.pemegang.keterangan_blanko}
				</div>
			</c:if>

		<spring:bind path="cmd.*">
			<c:if test="${not empty status.errorMessages }">
				<div id="error">INFO:<br>
				<c:forEach var="error" items="${status.errorMessages}">
														- <c:out value="${error}" escapeXml="false" />
					<br />
				</c:forEach></div>
			</c:if>
		</spring:bind>
		<table class="entry">
			<tr>
				<td colspan="6" style="text-align: center;">
				<input type="submit"
					name="_target${halaman-1}" value="Prev &laquo;" 
					onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input
					type="submit" name="_target${halaman+1}" value="Next &raquo;"
					onClick="next()" 
					onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="hidden" name="_page"
					value="${halaman}">
					<spring:bind path="cmd.pemegang.keterangan_blanko">
					<input type="hidden" name="${status.expression}"
						value="${status.value }" size="42" maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind>
	</td>
			</tr>
			<tr>
				<th colspan="6"><font size="1" face="Verdana" color="#CC3300">
				(
				<c:choose>
					<c:when test="${cmd.contactPerson.pic_jenis eq '1'}">
						Hanya diisi apabila PIC berbeda dengan calon Tertanggung
					</c:when>
					<c:otherwise>
						Hanya diisi apabila Pemegang Polis berbeda dengan calon Tertanggung
					</c:otherwise>
				</c:choose>
				)</font></th>
			</tr>
			<tr>
				
            <th class="subtitle" colspan="6">DATA CALON TERTANGGUNG </th>
			</tr>
			<tr>
				<th width="28">1.</th>
				<th width="280">Nama Lengkap <br>
				<span class="info">(sesuai dengan KTP / Identitas lain tanpa gelar)</span>
				</th>
				<th width="498">
				
				<select name="tertanggung.lti_id">
					<option value=""></option>
					<c:forEach var="l" items="${select_gelar}">
						<option
							<c:if test="${cmd.tertanggung.lti_id eq l.ID}"> SELECTED </c:if>
							value="${l.ID}">${l.GELAR}</option>
					</c:forEach>
				</select>				
				
				<spring:bind path="cmd.tertanggung.mcl_first">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind>
				<font color="#CC3300">*</font></th>
				<th width="171">Gelar</th>
				<th width="245"><spring:bind path="cmd.tertanggung.mcl_gelar">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="15">
				</spring:bind></th>
			</tr>
			<tr>
				<th>2.</th>
				<th>Nama Ibu Kandung</th>
				<th colspan="3"><spring:bind path="cmd.tertanggung.mspe_mother">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="50"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>3.</th>
				<th>Bukti Identitas</th>
				<th colspan="3"><select name="tertanggung.lside_id" >
					<c:forEach var="identitas" items="${select_identitas}">
						<option
							<c:if test="${cmd.tertanggung.lside_id eq identitas.ID}"> SELECTED </c:if>
							value="${identitas.ID}">${identitas.TDPENGENAL}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th></th>
				<th>No. KTP / Identitas lain</th>
				<th colspan="3"><spring:bind path="cmd.tertanggung.mspe_no_identity">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="50"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind>
				<font color="#CC3300">*</font></th>
			</tr>			
			<tr>
				<th>3a.</th>
				<th>Tanggal Kadaluarsa</th>
				<th colspan="3"><spring:bind path="cmd.tertanggung.mspe_no_identity_expired">
					<c:if test="${cmd.pemegang.lsre_id eq 1}">
						<script>inputDate('${status.expression}', '${status.value}', true);</script>
					</c:if>
					<c:if test="${cmd.pemegang.lsre_id ne 1}">
						<script>inputDate('${status.expression}', '${status.value}', false);</script>
					</c:if>
				</spring:bind><font color="#CC3300">*</font></th>
			</tr>			
			<tr>
				<th>4.</th>
				<th>Warga Negara</th>
				<th><select name="tertanggung.lsne_id" >
					<c:forEach var="negara" items="${select_negara}">
						<option
							<c:if test="${cmd.tertanggung.lsne_id eq negara.ID}"> SELECTED </c:if>
							value="${negara.ID}">${negara.NEGARA}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></th>
				<th>Umur Beasiswa</th>
				<th><spring:bind path="cmd.tertanggung.mspo_umur_beasiswa">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="3" maxlength="2"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind><font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th>5.</th>
				<th>Tanggal Lahir</th>
				<th><spring:bind path="cmd.tertanggung.mspe_date_birth">
					<c:if test="${cmd.pemegang.lsre_id eq 1}">
						<script>inputDate('${status.expression}', '${status.value}', true);</script>
					</c:if>
					<c:if test="${cmd.pemegang.lsre_id ne 1}">
						<script>inputDate('${status.expression}', '${status.value}', false);</script>
					</c:if>
				</spring:bind><font color="#CC3300">*</font></th>
				<th>Usia</th>
				<th><spring:bind path="cmd.tertanggung.mste_age">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="4" readOnly>
				</spring:bind> tahun</th>
			</tr>
			<tr>
				<th></th>
				<th>di</th>
				<th colspan="3"><spring:bind path="cmd.tertanggung.mspe_place_birth">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="30"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind><font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th>6.</th>
				<th>Jenis Kelamin</th>
				<th>
				<label for="cowok"> <input class="noBorder" type="radio"
						name="jenis_kelamin" value="1"  onClick="kelamin('1');"
						<c:if test="${cmd.tertanggung.mspe_sex eq 1 or cmd.tertanggung.mspe_sex eq null}" > 
              checked</c:if>
						id="cowok" >Pria </label>
			  <label for="cewek"> <input class="noBorder" type="radio"
						name="jenis_kelamin" value="0" onClick="kelamin('0');"
						<c:if test="${cmd.tertanggung.mspe_sex eq 0}" > 
              checked</c:if>
						id="cewek" >Wanita </label>
			  
			  <spring:bind path="cmd.tertanggung.mspe_sex">
					<input type="hidden" name="${status.expression}"
						value="${status.value }"  >
				</spring:bind>
			  <font color="#CC3300">*</font>
				</th>
				<th>Status</th>
				<th><select name="tertanggung.mspe_sts_mrt" >
					<c:forEach var="marital" items="${select_marital}">
						<option
							<c:if test="${cmd.tertanggung.mspe_sts_mrt eq marital.ID}"> SELECTED </c:if>
							value="${marital.ID}">${marital.MARITAL}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></th>
			</tr>
<tr>
				<th>a.</th>
				<th>Nama Suami/Istri</th>
				<th><spring:bind path="cmd.tertanggung.nama_si">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="30"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				<font color="#CC3300">*</font></spring:bind></th>
				<th>Tanggal Lahir</th>
				<th>				
				<c:if test="${cmd.pemegang.lsre_id eq 1}">
					<spring:bind path="cmd.tertanggung.tgllhr_si">
						<script>inputDate('${status.expression}', '${status.value}', true, '', 9);</script>
						<font color="#CC3300">*</font></spring:bind>
					</c:if>
					<c:if test="${cmd.pemegang.lsre_id ne 1}">
					<spring:bind path="cmd.tertanggung.tgllhr_si">
						<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
						<font color="#CC3300">*</font></spring:bind>
					</c:if></th>
			</tr>
<tr>
				<th>b.</th>
				<th>Nama Anak 1</th>
				<th><spring:bind path="cmd.tertanggung.nama_anak1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="50"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				<font color="#CC3300">*</font></spring:bind></th>
				<th>Tanggal Lahir</th>
				<th>
					<c:if test="${cmd.pemegang.lsre_id eq 1}">
					<spring:bind path="cmd.tertanggung.tgllhr_anak1">
						<script>inputDate('${status.expression}', '${status.value}', true, '', 9);</script>
						<font color="#CC3300">*</font></spring:bind>
					</c:if>
					<c:if test="${cmd.pemegang.lsre_id ne 1}">
					<spring:bind path="cmd.tertanggung.tgllhr_anak1">
						<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
						<font color="#CC3300">*</font></spring:bind>
					</c:if>
				</th>
			</tr>
<tr>
				<th>c.</th>
				<th>Nama Anak 2</th>
				<th><spring:bind path="cmd.tertanggung.nama_anak2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="50"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				<font color="#CC3300">*</font></spring:bind></th>
				<th>Tanggal Lahir</th>
				<th>				
				<c:if test="${cmd.pemegang.lsre_id eq 1}">
					<spring:bind path="cmd.tertanggung.tgllhr_anak2">
						<script>inputDate('${status.expression}', '${status.value}', true, '', 9);</script>
						<font color="#CC3300">*</font></spring:bind>
					</c:if>
					<c:if test="${cmd.pemegang.lsre_id ne 1}">
					<spring:bind path="cmd.tertanggung.tgllhr_anak2">
						<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
						<font color="#CC3300">*</font></spring:bind>
					</c:if></th>
			</tr>	
<tr>
				<th>d.</th>
				<th>Nama Anak 3</th>
				<th><spring:bind path="cmd.tertanggung.nama_anak3">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="50"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				<font color="#CC3300">*</font></spring:bind></th>
				<th>Tanggal Lahir</th>
				<th>				
				<c:if test="${cmd.pemegang.lsre_id eq 1}">
					<spring:bind path="cmd.tertanggung.tgllhr_anak3">
						<script>inputDate('${status.expression}', '${status.value}', true, '', 9);</script>
						<font color="#CC3300">*</font></spring:bind>
					</c:if>
					<c:if test="${cmd.pemegang.lsre_id ne 1}">
					<spring:bind path="cmd.tertanggung.tgllhr_anak3">
						<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
						<font color="#CC3300">*</font></spring:bind>
					</c:if>
					</th>
			</tr>				
			<tr>
				<script type="text/javascript">
					function agamaChange(flag){					
						if(flag=='6'){
							
							document.frmParam.elements['tertanggung.mcl_agama'].readOnly = false;
						}else{
							document.frmParam.elements['tertanggung.mcl_agama'].value="";
							document.frmParam.elements['tertanggung.mcl_agama'].readOnly = true;
						}
					}
				</script>
				<th>7.</th>
				<th>Agama</th>
				<th colspan="3"><select name="tertanggung.lsag_id" onchange="agamaChange(this.value);">
					<c:forEach var="agama" items="${select_agama}" >
						<option
							<c:if test="${cmd.tertanggung.lsag_id eq agama.ID}"> SELECTED </c:if>
							value="${agama.ID}">${agama.AGAMA}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th></th>
				<th>Lain - Lain, Sebutkan</th>
				<th colspan="3">					
					<spring:bind path="cmd.tertanggung.mcl_agama">
						<input type="text" name="${status.expression}"
							value="${status.value }" size="25" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
					</spring:bind>
				</th>
			</tr>
			<tr>
				<th>8.</th>
				<th>Pendidikan</th>
				<th colspan="3"><select name="tertanggung.lsed_id" >
					<c:forEach var="pendidikan" items="${select_pendidikan}">
						<option
							<c:if test="${cmd.tertanggung.lsed_id eq pendidikan.ID}"> SELECTED </c:if>
							value="${pendidikan.ID}">${pendidikan.PENDIDIKAN}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th rowspan="5">9. a.</th>
				<th rowspan="5">Alamat Rumah</th>
				<th rowspan="5"><spring:bind path="cmd.tertanggung.alamat_rumah">
					<textarea cols="33" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>${status.value }</textarea>
				</spring:bind><font color="#CC3300">*</font></th>
				<th>Kode Pos</th>
				<th><spring:bind path="cmd.tertanggung.kd_pos_rumah">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="10"\
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>Kota</th>
				<th>
				<spring:bind path="cmd.tertanggung.kota_rumah">
				<input type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
		    	<span id="indicator_rumah" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
			</spring:bind> 
				</th>
			</tr>
			<tr>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>No Telepon 1</th>
				<th><spring:bind path="cmd.tertanggung.area_code_rumah">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>><font color="#CC3300">*</font>
				</spring:bind> <spring:bind path="cmd.tertanggung.telpon_rumah">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind><font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th>No Telepon 2</th>
				<th><spring:bind path="cmd.tertanggung.area_code_rumah2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.tertanggung.telpon_rumah2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				<th>No HandPhone 1</th>
				<th><spring:bind path="cmd.tertanggung.no_hp">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20" 
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
				<th>Email</th>
				<th><spring:bind path="cmd.tertanggung.email">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="40" 
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				
            <th>No HandPhone 2</th>
				<th><spring:bind path="cmd.tertanggung.no_hp2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th rowspan="6">b.</th>
				<th rowspan="6">Alamat Kantor</th>
				<th rowspan="6"><spring:bind path="cmd.tertanggung.alamat_kantor">
					<textarea cols="33" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>${status.value }</textarea>
				</spring:bind></th>
				<th>Kode Pos</th>
				<th><spring:bind path="cmd.tertanggung.kd_pos_kantor">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="10"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>Kota</th>
				<th>
					<spring:bind path="cmd.tertanggung.kota_kantor">
				<input type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
		    	<span id="indicator_kantor" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
			</spring:bind> 		
				</th>
			</tr>
			<tr>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>No Telepon1</th>
				<th><spring:bind path="cmd.tertanggung.area_code_kantor">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.tertanggung.telpon_kantor">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>No Telepon 2</th>
				<th><spring:bind path="cmd.tertanggung.area_code_kantor2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.tertanggung.telpon_kantor2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>No Fax</th>
				<th><spring:bind path="cmd.tertanggung.area_code_fax">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.tertanggung.no_fax">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th rowspan="6">c.</th>
				<th rowspan="6">Alamat Penagihan / Korespondensi</th>
				<th rowspan="6"><spring:bind path="cmd.addressbilling.msap_address">
					<textarea cols="34" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); ">${status.value }</textarea>
				</spring:bind></th>
				<th>Kode Pos</th>
				<th><spring:bind path="cmd.addressbilling.msap_zip_code">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20">
				</spring:bind></th>
			</tr>
			<tr>
				<th>Kota</th>
				<th>
				<spring:bind path="cmd.addressbilling.kota_tgh">
					<input type="text" name="${status.expression}"
						value="${status.value }">
				</spring:bind>
				</th>
			</tr>
			<tr>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>No Telepon1</th>
				<th><spring:bind path="cmd.addressbilling.msap_area_code1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4">
				</spring:bind> <spring:bind path="cmd.addressbilling.msap_phone1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20">
				</spring:bind></th>
			</tr>
			<tr>
				<th>No Telepon 2</th>
				<th><spring:bind path="cmd.addressbilling.msap_area_code2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4">
				</spring:bind> <spring:bind path="cmd.addressbilling.msap_phone2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20">
				</spring:bind></th>
			</tr>
			<tr>
				<th>No Telepon 3</th>
				<th><spring:bind path="cmd.addressbilling.msap_area_code3">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4">
				</spring:bind> <spring:bind path="cmd.addressbilling.msap_phone3">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20">
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				<th>No HandPhone 1</th>
				<th><spring:bind path="cmd.addressbilling.no_hp">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind><br/><font color="#CC3300">* apabila tidak ada no hp, harap isi dengan 0000</font></th>
				<th>No Fax</th>
				<th><spring:bind path="cmd.addressbilling.msap_area_code_fax1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.addressbilling.msap_fax1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				<th>No HandPhone 2</th>
				<th><spring:bind path="cmd.addressbilling.no_hp2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
				<th>Email</th>
				<th><spring:bind path="cmd.addressbilling.e_mail">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="40"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind><br/><font color="#CC3300">* apabila tidak ada no hp, harap isi dengan 0000</font></th>
			</tr>
			<tr>
				<th>10.</th>
				<th>Tujuan Membeli Asuransi</th>
				<th colspan="3"><select name="tertanggung.mkl_tujuan" >
					<c:forEach var="tujuan" items="${select_tujuan}">
						<option
							<c:if test="${cmd.tertanggung.mkl_tujuan eq tujuan.ID}"> SELECTED </c:if>
							value="${tujuan.ID}">${tujuan.TUJUAN}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th></th>
				<th>Lain-Lain, Jelaskan</th>
				<th colspan="3"><spring:bind path="cmd.tertanggung.tujuana">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>11.</th>
				<th>Perkiraan Penghasilan Kotor Per Tahun</th>
				<th colspan="3">
					<select name="tertanggung.mkl_penghasilan">
						<c:forEach var="penghasilan" items="${select_penghasilan}">
							<option
								<c:if test="${cmd.tertanggung.mkl_penghasilan eq penghasilan.ID}"> SELECTED </c:if>
								value="${penghasilan.ID}">${penghasilan.PENGHASILAN}</option>
						</c:forEach>
					</select><font color="#CC3300">*</font>
				</th>
			</tr>
			<tr>
				<th>12.</th>
				<th>Sumber Pendanaan Pembelian Asuransi</th>
				<th colspan = "3" id = "danaFrom">
					<label for="sendiri2">
					<input type="radio" class=noBorder
							 name="tertanggung.mkl_dana_from" value="0" onClick="cpy('0');"
								<c:if test="${cmd.tertanggung.mkl_dana_from eq 0 or cmd.tertanggung.mkl_dana_from eq null}"> 
								   checked
								</c:if>
								 id="sendiri2">
					</label>Diri Sendiri
					<label for="gaksendiri2">
					<input type="radio" class=noBorder
							 name="tertanggung.mkl_dana_from" value="1" onClick="cpy('1');"
								<c:if test="${cmd.tertanggung.mkl_dana_from eq 1}"> 
									checked
								</c:if>
								id="gaksendiri2">
					</label>Bukan Diri Sendiri (Pembayar Premi bukan Pemegang Polis)<font color="#CC3300">*</font>
					<input type="hidden" name="tanda_pp" value='${cmd.pemegang.mkl_dana_from}'>
				</th>
			</tr>
			
			<tr>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th id = "sumberPremi"><font color="#CC3300">Apakah Pembayar Premi :</font>
					<label for="badan">
					<input type="radio" class=noBorder
							 name="tertanggung.mkl_sumber_premi" value="0" onClick="cpy2('0');"
								<c:if test="${cmd.pemegang.mkl_sumber_premi eq 0 or cmd.pemegang.mkl_sumber_premi eq null}"> 
								   checked
								</c:if>
								 id="badan">Bandan Hukum/Usaha
					</label>
					<label for="orang">
					<input type="radio" class=noBorder
							 name="tertanggung.mkl_sumber_premi" value="1" onClick="cpy2('1');"
								<c:if test="${cmd.pemegang.mkl_sumber_premi eq 1}"> 
									checked
								</c:if>
								id="orang">Perorangan
					</label>
					<input type="hidden" name="tanda_hub" value='${cmd.pemegang.mkl_sumber_premi}'>
				</th>
				<th colspan = "2" id = "lsreIdPremi">
					<select name="tertanggung.lsre_id_premi">
						<c:forEach var="pre" items="${select_relasi_premi}">
							<option
							<c:if test="${cmd.pemegang.lsre_id_premi eq pre.ID}"> SELECTED </c:if>
							value="${pre.ID}">${pre.RELATION}</option>
						</c:forEach>
					</select>
						
				</th>
			</tr>
			
			<tr>
				<th colspan = "5">
					<table id="test01" class="entry2">
					<tr>
						<th width = "19">&nbsp;</th>
						<th width = "258">&nbsp;</th>
						<th width = "464">
             			<c:choose>	
             				<c:when test="${cmd.pemegang.lsre_id eq '1'}">
             					<input name="btnadd1" type="button" id="btnadd1" value="ADD" disabled = "disabled" onClick="addRowDOM1('tableDanax', '1')"> &nbsp; 
             					<input name="btn_cancel1" type="button" id="btn_cancel1" value="DELETE" disabled = "disabled" onClick="cancel1()">
             				</c:when>	 
             				<c:otherwise>
             					<input name="btnadd1" type="button" id="btnadd1" value="ADD" onClick="addRowDOM1('tableDanax', '1')"> &nbsp; 
             					<input name="btn_cancel1" type="button" id="btn_cancel1" value="DELETE" onClick="cancel1()">
             				</c:otherwise>
						</c:choose>
								<input type="hidden" name="jmlDaftarKyc" value="0">
						
						</th>
						<th width = "320">&nbsp;</th>
						<th width="61">&nbsp;</th>
					</tr>
					</table>
				</th>
			</tr>		
			<tr>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th><font color="#CC3300">Pilih Sumber Pendanaan</font></th>
				<th colspan = "2"><font color="#CC3300">Lainnya, Jelaskan</font></th>
			</tr>					
			<tr>
				<th colspan = "5">
					<table id="tableDanax" class="entry2">
					<tr>
						<th width = "19">&nbsp;</th>
						<th width = "258">&nbsp;</th>
						<th width = "464">

						<select name="tertanggung.mkl_pendanaan">
							<c:forEach var="dana" items="${select_dana}">
								<option
									<c:if test="${cmd.tertanggung.mkl_pendanaan eq dana.ID}"> SELECTED </c:if>
									value="${dana.ID}">${dana.DANA}
								</option>
							</c:forEach>
						</select>
				 				<font color="#CC3300">*</font>
						</th>
						<th width = "320">
						<spring:bind path="cmd.tertanggung.danaa">
							<input type="text" name="${status.expression}"
								value="${status.value }" size="42" maxlength="100"
								<c:if test="${ not empty status.errorMessage}">
									style='background-color :#FFE1FD'
								</c:if>>
						</spring:bind>
						</th>
						<th width = "61">&nbsp;</th>
					</tr>
						
			<c:forEach var="listkyc" items="${cmd.pemegang.daftarKyc}" varStatus="status">
				<c:if test="${cmd.pemegang.lsre_id eq 1}">
					<tr>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>
							<select name="listkyc.kyc_desc1${status.index +1}" disabled = "disabled" style="background-color:#d4d4d4;">
								<c:forEach var="sdana" items="${select_dana}">
									<option 
										<c:if test="${listkyc.kyc_desc1 eq sdana.ID}"> SELECTED </c:if>
											value="${sdana.ID}">${sdana.DANA}
									</option>
								</c:forEach>
							</select>
						</th>
						<th>
							<input type="text" name='listkyc.kyc_desc_x${status.index +1}' value ='${listkyc.kyc_desc_x}' size="42" maxlength="60" disabled = "disabled" style="background-color:#d4d4d4;">
						</th>
						<th>
							<input type=checkbox class="noBorder" name="cek${status.index +1}" id= "ck${status.index +1}" disabled = "disabled" >
						</th>
					</tr>
				</c:if>
			</c:forEach>
						
					</table>
				</th>
			</tr>
			<tr>
				<th colspan = "5">&nbsp;</th>
			</tr>					
			<tr>
				<th>13.</th>
				<th>Sumber Penghasilan</th>
				<th colspan="3" id = "hasilFrom">
					<label for="sendiri3">
						<input type="radio" class=noBorder
							name="tertanggung.mkl_hasil_from" value="0"
							<c:if test="${cmd.pemegang.mkl_hasil_from eq 0 or cmd.pemegang.mkl_hasil_from eq null}"> 
									checked</c:if>
							id="sendiri3">
					</label>Diri Sendiri
					<label for="gaksendiri3">
						<input type="radio" class=noBorder
							name="tertanggung.mkl_hasil_from" value="1"
							<c:if test="${cmd.pemegang.mkl_hasil_from eq 1}"> 
									checked</c:if>
							id="gaksendiri3">
					</label>Bukan Diri Sendiri (Pembayar Premi bukan Pemegang Polis)<font color="#CC3300">*</font>
				</th>
			</tr>
			<tr>
				<th colspan = "5">
					<table id="test02" class="entry2">
					<tr>
						<th width = "19">&nbsp;</th>
						<th width = "258">&nbsp;</th>
						<th width = "464">

						<c:choose>	
             				<c:when test="${cmd.pemegang.lsre_id eq '1'}">
             					<input name="btnadd1" type="button" id="btnadd1" value="ADD" disabled = "disabled" onClick="addRowDOM2('tableDanax2', '1')"> &nbsp; 
             					<input name="btn_cancel1" type="button" id="btn_cancel1" value="DELETE" disabled = "disabled" onClick="cancel2()">
             				</c:when>	 
             				<c:otherwise>
             					<input name="btnadd1" type="button" id="btnadd1" value="ADD" onClick="addRowDOM2('tableDanax2', '1')"> &nbsp; 
             					<input name="btn_cancel1" type="button" id="btn_cancel1" value="DELETE" onClick="cancel2()">
             				</c:otherwise>
						</c:choose>
								<input type="hidden" name="jmlDaftarKyc2" value="0">
             						
						</th>
						<th width = "320">&nbsp;</th>
						<th width="61">&nbsp;</th>
					</tr>
					</table>
				</th>
			</tr>		
			<tr>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th><font color="#CC3300">Pilih Sumber Penghasilan</font></th>
				<th colspan = "2"><font color="#CC3300">Lainnya, Jelaskan</font></th>
			</tr>
			<tr>
				<th colspan = "5">
					<table id="tableDanax2" class="entry2">
						<tr>
							<th width = "19">&nbsp;</th>
							<th width = "258">&nbsp;</th>
							<th width = "464">
								<select name="tertanggung.mkl_smbr_penghasilan" >
									<c:forEach var="hasil" items="${select_hasil}">
										<option
											<c:if test="${cmd.tertanggung.mkl_smbr_penghasilan eq hasil.ID}"> SELECTED </c:if>
											value="${hasil.ID}">${hasil.DANA}
										</option>
									</c:forEach>
								</select>
									<font color="#CC3300">*</font>
							</th>
							<th width = "320">
									<spring:bind path="cmd.tertanggung.shasil">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="42" maxlength="100"
										<c:if test="${ not empty status.errorMessage}">
											style='background-color :#FFE1FD'
										</c:if>>
								</spring:bind>
							</th>
							<th width = "61">&nbsp;</th>
						</tr>
				<c:forEach var="listkyc2" items="${cmd.pemegang.daftarKyc2}" varStatus="status">
					<c:if test="${cmd.pemegang.lsre_id eq 1}">
						<tr>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th>
								<select name="listkyc2.kyc_desc2${status.index +1}" disabled = "disabled" style="background-color:#d4d4d4;">
									<c:forEach var="shasil" items="${select_hasil}">
										<option 
											<c:if test="${listkyc2.kyc_desc2 eq shasil.ID}"> SELECTED </c:if>
											value="${shasil.ID}">${shasil.DANA}
										</option>
									</c:forEach>
								</select>
							</th>
							<th>
								<input type="text" name='listkyc2.kyc_desc2_x${status.index +1}' value ='${listkyc2.kyc_desc2_x}' size="42" maxlength="60" disabled = "disabled" style="background-color:#d4d4d4;">
							</th>
							<th>
								<input type=checkbox class="noBorder" name="cek2${status.index +1}" id="ck2${status.index +1}" disabled = "disabled">
							</th>
						</tr> 
					</c:if>	
				</c:forEach>
					</table>
				</th>
			</tr>
			<tr>
				<th colspan = "5">&nbsp;</th>
			</tr>
			<tr>
				<th>14.</th>
				<th>Klasifikasi Pekerjaan</th>
				<th colspan="3"><select name="tertanggung.mkl_kerja" >
					<c:forEach var="kerja" items="${select_pekerjaan}">
						<option
							<c:if test="${cmd.tertanggung.mkl_kerja eq kerja.ID}"> SELECTED </c:if>
							value="${kerja.ID}">${kerja.KLASIFIKASI}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th></th>
				<th>Lain-Lain, Sebutkan</th>
				<th colspan="3"><spring:bind path="cmd.tertanggung.kerjaa">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				<th>
					Jabatan
					<!--  <br><span class="info">(Hanya diisi apabila Klasifikasi Pekerjaan sebagai KARYAWAN)</span> -->
				</th>
				<th colspan="3"><spring:bind path="cmd.tertanggung.kerjab">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>14.</th>
				<th>Klasifikasi Bidang Industri</th>
				<th colspan="3"><select name="tertanggung.mkl_industri"
					>
					<c:forEach var="industri" items="${select_industri}">
						<option
							<c:if test="${cmd.tertanggung.mkl_industri eq industri.ID}"> SELECTED </c:if>
							value="${industri.ID}">${industri.BIDANG}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th></th>
				<th>Lainnya, Sebutkan</th>
				<th colspan="3"><spring:bind path="cmd.tertanggung.industria">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th class="subtitle" colspan="5"></th>
			</tr>
			  <tr> 
            <td colspan="9"> 
			 <p align="left"><b> <font face="Verdana" size="2" color="#CC3300">Note : * Wajib diisi</font></b></p>
			</td>
          </tr>
          <c:if test="${cmd.tertanggung.lspd_id eq 2 or cmd.tertanggung.lspd_id eq 27 or cmd.tertanggung.lspd_id eq 209}">
         	<c:if test="${not empty cmd.pemegang.reg_spaj}">         	
	          <tr>          	 
	          	 <td colspan="2">
					 <input type="checkbox" name="edit_tertanggung" id="edit_tertanggung" class="noBorder" value ="${cmd.tertanggung.edit_tertanggung}" <c:if test="${cmd.tertanggung.edit_tertanggung eq 1}"> 
						checked</c:if> size="30" onClick="edit_onClick1();"> Telah dilakukan edit	
					 <spring:bind path="cmd.tertanggung.edit_tertanggung"> 
		          		<input type="hidden" name="${status.expression}" value="${cmd.tertanggung.edit_tertanggung}"  size="30" style='background-color :#D4D4D4'readOnly>
					 </spring:bind>   
	          	 </td>
				 <td colspan="7">
				 		<spring:bind path="cmd.tertanggung.kriteria_kesalahan">
							<select name="${status.expression}">
								<c:if test="${cmd.tertanggung.lspd_id ne 27}">
									<c:forEach var="a" items="${select_kriteria_kesalahan}">
										<option
											<c:if test="${cmd.tertanggung.kriteria_kesalahan eq a.KESALAHAN}"> SELECTED </c:if>
											value="${a.KESALAHAN}">${a.KESALAHAN}
										</option>
									</c:forEach>
								</c:if>
								<c:if test="${cmd.tertanggung.lspd_id eq 27}">
									<c:forEach var="a" items="${select_kriteria_kesalahan}">
										<c:if test="${a.ID ne 3 and a.ID ne 2}">
											<option
												<c:if test="${cmd.tertanggung.kriteria_kesalahan eq a.KESALAHAN}"> SELECTED </c:if>
												value="${a.KESALAHAN}">${a.KESALAHAN}
											</option>
										</c:if>
									</c:forEach>
								</c:if>
								
							</select>
						</spring:bind>
				 </td> 
	          </tr>
	          <tr>
	          	<td colspan="9">&nbsp;</td>
	          </tr>
          	</c:if>
          </c:if>
		  <tr>
				<td colspan="5" style="text-align: center;">
				<input type="hidden" name="hal" value="${halaman}">
				<spring:bind path="cmd.pemegang.indeks_halaman">
					<input type="hidden" name="${status.expression}"
						value="${halaman-1}" size="30">
				</spring:bind>				
				<input type="submit" name="_target${halaman-1}"
					value="Prev &laquo;" 
					accesskey="P" onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="submit"
					name="_target${halaman+1}" value="Next &raquo;" onClick="next()" 
					accesskey="N" onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
					<input
					type="hidden" name="_page" value="${halaman}">
				</td>
			</tr>
		</table>
		
		</td>
	</tr>
</table>
</form>
<ajax:autocomplete
				  source="tertanggung.kota_rumah"
				  target="tertanggung.kota_rumah"
				  baseUrl="${path}/servlet/autocomplete?s=tertanggung.kota_rumah&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_rumah"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />

<ajax:autocomplete
				  source="tertanggung.kota_kantor"
				  target="tertanggung.kota_kantor"
				  baseUrl="${path}/servlet/autocomplete?s=tertanggung.kota_kantor&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_kantor"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />
<script type="text/javascript">	
	agamaChange('${cmd.tertanggung.lsag_id}');
</script>
</body>
<%@ include file="/include/page/footer.jsp"%>