<%@ include file="/include/page/taglibs.jsp"%>

<table class="entry2">

	<tr>
		<th nowrap="nowrap">
			Informasi Medis
		</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select disabled>
				<c:forEach var="medis" items="${select_medis}">
				<option
					<c:if test="${cmd.dataUsulan.mste_medical eq medis.ID}"> SELECTED </c:if>
					value="${medis.ID}">${medis.MEDIS}</option>
				</c:forEach>
			</select>
			Nomor Seri
			<input type="text" value="${cmd.pemegang.mspo_no_blanko}" size="20" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">
			Nama Lengkap
		</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select name="pemegang.lti_id" disabled="disabled">
				<option value=""></option>
				<c:forEach var="l" items="${select_gelar}">
					<option
						<c:if test="${cmd.pemegang.lti_id eq l.ID}"> SELECTED </c:if>
						value="${l.ID}">${l.GELAR}</option>
				</c:forEach>
			</select>				
			<input type="text" value="${cmd.pemegang.mcl_first}" size="35" readonly>
			Gelar
			<input type="text" value="${cmd.pemegang.mcl_gelar}" size="12" readonly>
			Ibu Kandung
			<input type="text"	value="${cmd.pemegang.mspe_mother}" size="35" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">
			Bukti Identitas
		</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select disabled>
				<c:forEach var="identitas" items="${select_identitas}">
					<option
						<c:if test="${cmd.pemegang.lside_id eq identitas.ID}"> SELECTED </c:if>
						value="${identitas.ID}">${identitas.TDPENGENAL}</option>
				</c:forEach>
			</select>
			<input type="text" value="${cmd.pemegang.mspe_no_identity}" size="30" readonly>
			Warga Negara
			<select disabled>
				<c:forEach var="negara" items="${select_negara}">
					<option
						<c:if test="${cmd.pemegang.lsne_id eq negara.ID}"> SELECTED </c:if>
						value="${negara.ID}">${negara.NEGARA}</option>
				</c:forEach>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">
			Tempat/Tanggal Lahir
		</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<input type="text" value="${cmd.pemegang.mspe_place_birth}" size="25" readonly> / 
			<input type="text" value="<fmt:formatDate value="${cmd.pemegang.mspe_date_birth}" pattern="dd/MM/yyyy"/>" size="12" readonly>
			Usia
			<input type="text" value="${cmd.pemegang.mste_age}" size="3" readonly>
			Status
			<select disabled>
				<c:forEach var="marital" items="${select_marital}">
					<option
					<c:if test="${cmd.pemegang.mspe_sts_mrt eq marital.ID}"> SELECTED </c:if>
					value="${marital.ID}">${marital.MARITAL}</option>
				</c:forEach>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Jenis Kelamin</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<input type="text" value="<c:choose><c:when test="${cmd.pemegang.mspe_sex eq 1 or cmd.pemegang.mspe_sex eq null}">Pria</c:when><c:otherwise>Wanita</c:otherwise></c:choose>">
			Agama
			<select disabled>
				<c:forEach var="agama" items="${select_agama}">
					<option
					<c:if test="${cmd.pemegang.lsag_id eq agama.ID}"> SELECTED </c:if>
					value="${agama.ID}">${agama.AGAMA}</option>
				</c:forEach>
			</select>
			Pendidikan
			<select disabled>
				<c:forEach var="pendidikan" items="${select_pendidikan}">
				<option
					<c:if test="${cmd.pemegang.lsed_id eq pendidikan.ID}"> SELECTED </c:if>
					value="${pendidikan.ID}">${pendidikan.PENDIDIKAN}</option>
				</c:forEach>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Alamat Rumah</th>
		<th nowrap="nowrap" class="left">
			<textarea rows="2" cols="45" readonly>${cmd.pemegang.alamat_rumah}</textarea>
		</th>
		<th nowrap="nowrap" class="left">
			Kode Pos
			<input type="text" value="${cmd.pemegang.kd_pos_rumah}" size="9" readonly>
			Kota
			<input type="text" value="${cmd.pemegang.kota_rumah}" size="20" readonly>
			<br>
			No. Telepon
			<select style="width: 191px;" >
			   
				<OPTGROUP label="Telepon Rumah">
					<c:if test="${not empty cmd.pemegang.telpon_rumah}"><option>${cmd.pemegang.area_code_rumah} - ${cmd.pemegang.telpon_rumah}</option></c:if>
					<c:if test="${not empty cmd.pemegang.telpon_rumah2}"><option>${cmd.pemegang.area_code_rumah2} - ${cmd.pemegang.telpon_rumah2}</option></c:if>
				</OPTGROUP>
				 <OPTGROUP selected label="Handphone">
					<c:if  test="${not empty cmd.pemegang.no_hp}"><option selected >${cmd.pemegang.no_hp} </option></c:if>
					<c:if test="${not empty cmd.pemegang.no_hp2}"><option>${cmd.pemegang.no_hp2}</option></c:if>										
				</OPTGROUP>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Alamat Kantor</th>
		<th nowrap="nowrap" class="left">
			<textarea rows="2" cols="45" readonly>${cmd.pemegang.alamat_kantor}</textarea>
		</th>
		<th nowrap="nowrap" class="left">
			Kode Pos
			<input type="text" value="${cmd.pemegang.kd_pos_kantor}" size="9" readonly>
			Kota
			<input type="text" value="${cmd.pemegang.kota_kantor}" size="20" readonly>
			<br>
			No. Telepon
			<select style="width: 191px;">
				<OPTGROUP label="Telepon Kantor">
					<c:if test="${not empty cmd.pemegang.telpon_kantor}"><option>${cmd.pemegang.area_code_kantor} - ${cmd.pemegang.telpon_kantor}</option></c:if>
					<c:if test="${not empty cmd.pemegang.telpon_kantor2}"><option>${cmd.pemegang.area_code_kantor2} - ${cmd.pemegang.telpon_kantor2}</option></c:if>
				</OPTGROUP>
				<OPTGROUP label="Fax">
					<c:if test="${not empty cmd.pemegang.no_fax}"><option>${cmd.pemegang.area_code_fax} - ${cmd.pemegang.no_fax}</option></c:if>
				</OPTGROUP>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Alamat Penagihan</th>
		<th nowrap="nowrap" class="left">
			<textarea rows="2" cols="45" readonly>${cmd.addressbilling.msap_address}</textarea>
		</th>
		<th nowrap="nowrap" class="left">
			Kode Pos
			<input type="text" value="${cmd.addressbilling.msap_zip_code}" size="9" readonly>
			Kota
			<input type="text" value="${cmd.addressbilling.kota_tgh}" size="20" readonly>
			Negara
			<select disabled>
				<c:forEach var="negara" items="${select_negara}">
					<option
						<c:if test="${cmd.addressbilling.lsne_id eq negara.ID}"> SELECTED </c:if>
						value="${negara.ID}">${negara.NEGARA}</option>
				</c:forEach>
			</select>
			<br>
			No. Telepon
			<select style="width: 191px;" >
				<OPTGROUP label="No. Telepon">
					<c:if test="${not empty cmd.addressbilling.msap_phone1}"><option>${cmd.addressbilling.msap_area_code1} - ${cmd.addressbilling.msap_phone1}</option></c:if>
					<c:if test="${not empty cmd.addressbilling.msap_phone2}"><option>${cmd.addressbilling.msap_area_code2} - ${cmd.addressbilling.msap_phone2}</option></c:if>
					<c:if test="${not empty cmd.addressbilling.msap_phone3}"><option>${cmd.addressbilling.msap_area_code3} - ${cmd.addressbilling.msap_phone3}</option></c:if>
				</OPTGROUP>
				<OPTGROUP label="Handphone">
					<c:if test="${not empty cmd.addressbilling.no_hp}"><option>${cmd.addressbilling.no_hp}</option></c:if>
					<c:if test="${not empty cmd.addressbilling.no_hp2}"><option>${cmd.addressbilling.no_hp2}</option></c:if>
				</OPTGROUP>
				<OPTGROUP label="Fax">
					<c:if test="${not empty cmd.addressbilling.msap_fax1}"><option>${cmd.addressbilling.msap_area_code_fax1} - ${cmd.addressbilling.msap_fax1}</option></c:if>
				</OPTGROUP>
			</select>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Alamat E-mail</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<input type="text" value="${cmd.addressbilling.e_mail}" size="100" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Tujuan Membeli Asuransi</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select disabled style="width: 200px;">
				<c:forEach var="tujuan" items="${select_tujuan}">
					<option
						<c:if test="${cmd.pemegang.mkl_tujuan eq tujuan.ID}"> SELECTED </c:if>
						value="${tujuan.ID}">${tujuan.TUJUAN}</option>
				</c:forEach>
			</select>
			Lainnya
			<input style="width: 311px;" type="text" value="${cmd.pemegang.tujuana}" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Perkiraan Penghasilan<br>Kotor Per Tahun</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select disabled style="width: 200px;">
				<c:forEach var="penghasilan" items="${select_penghasilan}">
					<option
						<c:if test="${cmd.pemegang.mkl_penghasilan eq penghasilan.ID}"> SELECTED </c:if>
						value="${penghasilan.ID}">${penghasilan.PENGHASILAN}</option>
				</c:forEach>
			</select>
			Jabatan
			<input style="width: 309px;" tipe="text" value="${cmd.pemegang.kerjab}" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Sumber Pendanaan<br>Pembelian Asuransi</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select disabled style="width: 200px;">
				<c:forEach var="dana" items="${select_dana}">
					<option
						<c:if test="${cmd.pemegang.mkl_pendanaan eq dana.ID}"> SELECTED </c:if>
						value="${dana.ID}">${dana.DANA}</option>
				</c:forEach>
			</select>
			Lainnya
			<input style="width: 311px;" type="text" value="${cmd.pemegang.danaa}" readonly>
		</th>
	</tr>
				
	<tr>
		<th nowrap="nowrap">Klasifikasi Pekerjaan</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select disabled style="width: 200px;">
				<c:forEach var="kerja" items="${select_pekerjaan}">
					<option
						<c:if test="${cmd.pemegang.mkl_kerja eq kerja.ID}"> SELECTED </c:if>
						value="${kerja.ID}">${kerja.KLASIFIKASI}</option>
				</c:forEach>
			</select>
			Lainnya
			<input style="width: 311px;" type="text" value="${cmd.pemegang.kerjaa}" readonly>
		</th>
	</tr>
	
	<tr>
		<th nowrap="nowrap">Klasifikasi Bidang Industri</th>
		<th nowrap="nowrap" class="left" colspan="2">
			<select disabled style="width: 200px;">
				<c:forEach var="industri" items="${select_industri}">
					<option
						<c:if test="${cmd.pemegang.mkl_industri eq industri.ID}"> SELECTED </c:if>
						value="${industri.ID}">${industri.BIDANG}</option>
				</c:forEach>
			</select>
			Lainnya
			<input style="width: 311px;" type="text" value="${cmd.pemegang.industria}" readonly>
		</th>
	</tr>
		
	<tr>
		<th nowrap="nowrap" colspan="3">
			Hubungan Calon Pemegang Polis dengan Calon Tertanggung
			<select disabled>
				<c:forEach var="relasi" items="${select_relasi}">
					<option
						<c:if test="${cmd.pemegang.lsre_id eq relasi.ID}"> SELECTED </c:if>
						value="${relasi.ID}">${relasi.RELATION}</option>
				</c:forEach>
			</select>
		</th>
	</tr>
	
</table>