<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		$("#tabs").tabs();
		// (Qtip2) init tooltip untuk semua element menJenis Program : ggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
				
	  $("#select_data").click(function() {
			//kopi untuk disubmit (isinya, bukan valuenya)
			$("#namapsn").val($("#select_data option:selected").text());
		});
		
	});
	
	function buttonLinks(str){
		popWin('${path}/reports/konfirmPSN.pdf?spaj='+str, 700, 1200);
	}
	
	
</script>


<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

</style>

<body>
		<div id="tabs">
		<ul>
			<li><a href="#tab-1">Program Sosial Nasabah (SMiLe Religi) </a>
			</li>

		</ul>

		<div id="tab-1">
						<form id="formPost2" name="formPost2" method="post" target=""  >

				<fieldset class="ui-widget ui-widget-content">

					<table class="displaytag" cellpadding='0' cellspacing='0'
						border="1" align="left">
						<tr align="left">
							<th bgcolor="#cccccc">No SPAJ / Polis</th>
							<th><input type="text" value="${formatspaj}" size="25"
								readonly>
							</th>
						</tr>
						<tr align="left">
							<th bgcolor="#cccccc">Nama Pemegang Polis</th>
							<th><input type="text" value="${pemegang.mcl_first}"
								size="40" readonly>
							</th>
						</tr>
						<tr align="left">
							<th bgcolor="#cccccc">Nama Pihak Yang Diasuransikan (Calon
								Peserta)</th>
							<th><input type="text" value="${tertanggung.mcl_first}"
								size="40" readonly>
							</th>
						</tr>
						<tr align="left">
							<th bgcolor="#cccccc">Nama Produk</th>
							<th><input type="text" value="${dataUsulan.lsdbs_name}"
								size="40" readonly>
							</th>
						</tr>
					</table>
				</fieldset>
				<tr align="left">
					<th bgcolor="#cccccc">Jenis Program Sosial (SMiLe Religi) <select
						name="select_data" id="select_data">
							<option value="ALL">[PILIH JENIS PSN]</option>
							<c:forEach var="aset" items="${select_data}">
								<option <c:if test="${buka eq aset.ID_PSN}"> SELECTED </c:if>
									value="${aset.ID_PSN}">${aset.NAMA_PSN}</option>
							</c:forEach>
					</select> <%--  <select name="select_data" id="select_data">
														 <option value ="ALL">[PILIH JENIS PSN]</option>
				                       						 <c:forEach var="x" items="${select_data}">
				                       						 <c:if test="${buka eq x.ID_PSN}"> SELECTED </c:if>
				                         					 <option value="${x.ID_PSN}">${x.NAMA_PSN}
	                          			 				 </option>
				                       						 </c:forEach>
				                 					   </select> --%></th>
				</tr>
				<div class="rowElem">
					<input type="submit" name="showPolis" id="showPolis"
						value="Show Form PSN"> <input type="hidden" name="namapsn"
						id="namapsn" value="">
				</div>
				<br> <br>

				<c:if test="${buka eq '1'}">
					<fieldset class="ui-widget ui-widget-content">
						<table class="displaytag" cellpadding='0' cellspacing='0'
							border="1" align="left">
							<tr align="left">
								<th bgcolor="#cccccc">Tujuan Mengikuti Program Sosial
									Nasabah : <input type="text" value="${namapsn}" size="30"
									readonly>
								</th>
							</tr>
							<tr bgcolor="#cccccc" align="left">
								<th>Skema Nilai Polis :
								<th></th>
							</tr>
							<tr>
								<th><input type="checkbox" class="noBorder" name="chbox"
									value="0" id="chbox"> Wakaf Nilai Polis &nbsp; <input
									type="checkbox" class="noBorder" name="chbox" value="1"
									id="chbox"> Wakaf Wasiat</th>
							</tr>
							<tr align="left">
								<th bgcolor="#cccccc">Nilai Wakaf : <input type="text"
									size="25" maxlength="25" name="nominal">
								</th>
							</tr>
							<tr bgcolor="#cccccc" align="left">
								<th>Frekuensi Pelaksanaan Wakaf Tunai : (Pilih salah Satu)
								<th></th>
							</tr>
							<tr align="left">
								<th><input type="checkbox" class="noBorder" name="chbox2"
									value="0" id="chbox2"> Sekali Wakaf Tunai &nbsp;</th>
							</tr>
							<tr align="left">
								<th><input type="checkbox" class="noBorder" name="chbox2"
									value="1" id="chbox2">Regular Wakaf Tunai / Tahunan</th>
							</tr>

							<table class="displaytag" cellpadding='0' cellspacing='0'
								border="1" align="left">
								<td align="left" colspan="2">Lembaga Program Sosial Nasabah
									Yang Tertera :</td>
								<c:forEach items="${listLembaga}" var="d">
									<tr align="left">
										<td align="left" colspan="2"><input type="checkbox"
											class="noBorder" name="chbox3" value="${d.ID_LBG}"
											id="chbox3">
										</td>
									</tr>
									<tr align="left">
										<td nowrap align="left" width="120" bgcolor="#cccccc">Nama
											Lembaga :</td>
										<td nowrap align="left" width="200">${d.NAMA_DTL_LBG}</td>
									</tr>
									<tr align="left">
										<td nowrap align="left" width="140" bgcolor="#cccccc">Alamat</td>
										<td nowrap align="left" width="400">${d.ALAMAT_LBG}</td>
									</tr>
									<tr align="left">
										<td nowrap align="left" width="200" bgcolor="#cccccc">Rekening
											Bank</td>
										<td><select name="select_data" id="select_data"
											disabled="disabled">
												<c:forEach var="bank" items="${bang}">
													<option
														<c:if test="${d.LSBP_ID eq bank.LSBP_ID}"> SELECTED </c:if>
														value="${bank.LSBP_ID}">${bank.LSBP_NAMA}</option>
												</c:forEach>
										</select>
										</td>
									</tr>
									<tr align="left">
										<td nowrap align="left" width="70" bgcolor="#cccccc">No
											Rekening</td>
										<td nowrap align="left" width="100">${d.NO_REK}</td>
									</tr>
									</tr>
								</c:forEach>
							</table>
						</table>
					</fieldset>
				</c:if>

				<c:if test="${buka eq '2'}">
					<fieldset class="ui-widget ui-widget-content">
						<table class="displaytag" cellpadding='0' cellspacing='0'
							border="1" align="left">
							<tr align="left">
								<th bgcolor="#cccccc">Tujuan Mengikuti Program Sosial
									Nasabah : <input type="text" value="${namapsn}" size="30"
									readonly>
								</th>
							</tr>
							<tr align="left" bgcolor="#cccccc">
								<th>Skema Nilai Polis :
								<th></th>
							</tr>
							<tr align="left">
								<th><input type="checkbox" class="noBorder" name="chbox"
									value="0" id="chbox"> Nilai Zakat &nbsp;
							</tr>
							<tr align="left">
								<th bgcolor="#cccccc">Nilai Zakat: <input type="text"
									size="25" maxlength="25" name="nominal">
								</th>
							</tr>
							<tr align="left" bgcolor="#cccccc">
								<th>Frekuensi Pelaksanaan Zakat : (Pilih salah Satu)
								<th></th>
							</tr>
							<tr align="left">
								<th><input type="checkbox" class="noBorder" name="chbox2"
									value="0" id="chbox2"> Sekali Zakat &nbsp;</th>
							</tr>
							<tr align="left">
								<th><input type="checkbox" class="noBorder" name="chbox2"
									value="1" id="chbox2">Regular Zakat / Tahunan</th>
							</tr>

							<table class="displaytag" cellpadding='0' cellspacing='0'
								border="1" align="left">
								<td align="left" colspan="2">Lembaga Program Sosial Nasabah
									Yang Tertera :</td>
								<c:forEach items="${listLembaga}" var="d">
									<tr align="left">
										<td align="left" colspan="2"><input type="checkbox"
											class="noBorder" name="chbox3" value="${d.ID_LBG}"
											id="chbox3">
										</td>
									</tr>
									<tr align="left">
										<td nowrap align="left" width="120" bgcolor="#cccccc">Nama
											Lembaga :</td>
										<td nowrap align="left" width="200">${d.NAMA_DTL_LBG}</td>
									</tr>
									<tr align="left">
										<td nowrap align="left" width="140" bgcolor="#cccccc">Alamat</td>
										<td nowrap align="left" width="400">${d.ALAMAT_LBG}</td>
									</tr>
									<tr align="left">
										<td nowrap align="left" width="200" bgcolor="#cccccc">Rekening
											Bank</td>
										<td><select name="select_data" id="select_data"
											disabled="disabled">
												<c:forEach var="bank" items="${bang}">
													<option
														<c:if test="${d.LSBP_ID eq bank.LSBP_ID}"> SELECTED </c:if>
														value="${bank.LSBP_ID}">${bank.LSBP_NAMA}</option>
												</c:forEach>
										</select>
										</td>
									</tr>
									<tr align="left">
										<td nowrap align="left" width="70" bgcolor="#cccccc">No
											Rekening</td>
										<td nowrap align="left" width="100">${d.NO_REK}</td>
									</tr>
									</tr>
								</c:forEach>
							</table>
						</table>
					</fieldset>
				</c:if>

				<c:if test="${buka eq '3'}">
					<fieldset class="ui-widget ui-widget-content">
						<table class="displaytag" cellpadding='0' cellspacing='0'
							border="1" align="left">
							<tr align="left" bgcolor="#cccccc">
								<th>Tujuan Mengikuti Program Sosial Nasabah : <input
									type="text" value="${namapsn}" size="30" readonly>
								</th>
							</tr>
							<tr align="left" bgcolor="#cccccc">
								<th>Nilai Infaq Rp : <input type="text" size="25"
									maxlength="25" name="nominal">
								</th>
							</tr>
							<tr align="left" bgcolor="#cccccc">
								<th>Frekuensi Pelaksanaan Infaq : (Pilih salah Satu)
								<th></th>
							</tr>
							<tr align="left">
								<th><input type="checkbox" class="noBorder" name="chbox2"
									value="0" id="chbox2"> Sekali Infaq &nbsp;</th>
							</tr>
							<tr align="left">
								<th><input type="checkbox" class="noBorder" name="chbox2"
									value="1" id="chbox2">Regular Infaq / Tahunan</th>
							</tr>

							<table class="displaytag" cellpadding='0' cellspacing='0'
								border="1" align="left">
								<td align="left" colspan="2">Lembaga Program Sosial Nasabah
									Yang Tertera :</td>
								<c:forEach items="${listLembaga}" var="d">
									<tr align="left">
										<td align="left" colspan="2"><input type="checkbox"
											class="noBorder" name="chbox3" value="${d.ID_LBG}"
											id="chbox3">
										</td>
									</tr>
									<tr align="left">
										<td nowrap align="left" width="120" bgcolor="#cccccc">Nama
											Lembaga :</td>
										<td nowrap align="left" width="200">${d.NAMA_DTL_LBG}</td>
									</tr>
									<tr align="left">
										<td nowrap align="left" width="140" bgcolor="#cccccc">Alamat</td>
										<td nowrap align="left" width="400">${d.ALAMAT_LBG}</td>
									</tr>
									<tr align="left">
										<td nowrap align="left" width="200" bgcolor="#cccccc">Rekening
											Bank</td>
										<td><select name="select_data" id="select_data"
											disabled="disabled">
												<c:forEach var="bank" items="${bang}">
													<option
														<c:if test="${d.LSBP_ID eq bank.LSBP_ID}"> SELECTED </c:if>
														value="${bank.LSBP_ID}">${bank.LSBP_NAMA}</option>
												</c:forEach>
										</select>
										</td>
									</tr>
									<tr align="left">
										<td nowrap align="left" width="70" bgcolor="#cccccc">No
											Rekening</td>
										<td nowrap align="left" width="100">${d.NO_REK}</td>
									</tr>
									</tr>
								</c:forEach>
							</table>
						</table>
					</fieldset>
				</c:if>
				<c:if test="${buka eq '4'}">
					<fieldset class="ui-widget ui-widget-content">
						<table class="displaytag" cellpadding='0' cellspacing='0'
							border="1" align="left">
							<tr align="left" bgcolor="#cccccc">
								<th bgcolor="#cccccc">Tujuan Mengikuti Program Sosial
									Nasabah : <input type="text" value="${namapsn}" size="30"
									readonly>
								</th>
							</tr>
							<tr align="left">
								<th bgcolor="#cccccc">Nilai Donasi : <input type="text"
									size="25" maxlength="25" name="nominal">
								</th>
							</tr>
							<tr align="left">
								<th bgcolor="#cccccc">Frekuensi Pelaksanaan Donasi : (Pilih
									salah Satu)
								<th></th>
							</tr>
							<tr align="left">
								<th><input type="checkbox" class="noBorder" name="chbox2"
									value="0" id="chbox2"> Sekali Donasi &nbsp;</th>
							</tr>
							<tr align="left">
								<th><input type="checkbox" class="noBorder" name="chbox2"
									value="1" id="chbox2">Regular Donasi / Tahunan</th>
							</tr>

							<table class="displaytag" cellpadding='0' cellspacing='0'
								border="1" align="left">
								<td align="left" colspan="2">Lembaga Program Sosial Nasabah
									Yang Tertera :</td>
								<c:forEach items="${listLembaga}" var="d">
									<tr align="left">
										<td align="left" colspan="2"><input type="checkbox"
											class="noBorder" name="chbox3" value="${d.ID_LBG}"
											id="chbox3">
										</td>
									</tr>
									<tr align="left">
										<td nowrap align="left" width="120" bgcolor="#cccccc">Nama
											Lembaga :</td>
										<td nowrap align="left" width="200">${d.NAMA_DTL_LBG}</td>
									</tr>
									<tr align="left">
										<td nowrap align="left" width="140" bgcolor="#cccccc">Alamat</td>
										<td nowrap align="left" width="400">${d.ALAMAT_LBG}</td>
									</tr>
									<tr align="left">
										<td nowrap align="left" width="200" bgcolor="#cccccc">Rekening
											Bank</td>
										<td><select name="select_data" id="select_data"
											disabled="disabled">
												<c:forEach var="bank" items="${bang}">
													<option
														<c:if test="${d.LSBP_ID eq bank.LSBP_ID}"> SELECTED </c:if>
														value="${bank.LSBP_ID}">${bank.LSBP_NAMA}</option>
												</c:forEach>
										</select>
										</td>
									</tr>
									<tr align="left">
										<td nowrap align="left" width="70" bgcolor="#cccccc">No
											Rekening</td>
										<td nowrap align="left" width="100">${d.NO_REK}</td>
									</tr>
									</tr>
								</c:forEach>
							</table>
						</table>
					</fieldset>
				</c:if>
				<c:if test="${buka eq '5'}">
					<fieldset class="ui-widget ui-widget-content">
						<table class="displaytag" cellpadding='0' cellspacing='0'
							border="1" align="left">
							<tr align="left">
								<th colspan="2" bgcolor="#cccccc">Tujuan Mengikuti Program
									Sosial Nasabah : <input type="text" value="${namapsn}"
									size="30" readonly>
								</th>
							</tr>
							<tr align="left">
								<th colspan="2" bgcolor="#cccccc">Nilai Kurban : <input
									type="text" size="25" maxlength="25" name="nominal">
									Qty : <input type="text" size="5" maxlength="5" name="qty">
								</th>
							</tr>
							<tr align="left">
								<th colspan="2" bgcolor="#cccccc">Frekuensi Pelaksanaan
									Kurban : (Pilih salah Satu)
								<th></th>
							</tr>
							<tr align="left">
								<th colspan="2"><input type="checkbox" class="noBorder"
									name="chbox2" value="0" id="chbox2"> Sekali Kurban
									&nbsp;</th>
							</tr>
							<tr align="left">
								<th colspan="2"><input type="checkbox" class="noBorder"
									name="chbox2" value="1" id="chbox2">Regular Kurban /
									Tahunan</th>
							</tr>
							<tr>
								<th colspan="2" align="left" bgcolor="#cccccc">Nilai Kurban</th>
							</tr>
							<c:forEach items="${listItem}" var="d">
								<tr align="left">
									<td align="left" colspan="2"><input type="checkbox"
										class="noBorder" name="chbox4" value="${d.ID_ITEM}"
										id="chbox4">
									</td>
								</tr>
								<tr align="left">
									<td nowrap align="left" width="120" bgcolor="#cccccc">Nama
										Item :</td>
									<td nowrap align="left" width="200">${d.NAMA_ITEM}</td>
								</tr>
								<tr align="left">
									<td nowrap align="left" width="140" bgcolor="#cccccc">Harga
									</td>
									<td nowrap align="left" width="400">${d.HARGA_ITEM}</td>
								</tr>
								</tr>
							</c:forEach>
							<table class="displaytag" cellpadding='0' cellspacing='0'
								border="1" align="left">
								<td align="left" colspan="2">Lembaga Program Sosial Nasabah
									Yang Tertera :</td>
								<c:forEach items="${listLembaga}" var="d">
									<tr align="left">
										<td align="left" colspan="2"><input type="checkbox"
											class="noBorder" name="chbox3" value="${d.ID_LBG}"
											id="chbox3">
										</td>
									</tr>
									<tr align="left">
										<td nowrap align="left" width="120" bgcolor="#cccccc">Nama
											Lembaga :</td>
										<td nowrap align="left" width="200">${d.NAMA_DTL_LBG}</td>
									</tr>
									<tr align="left">
										<td nowrap align="left" width="140" bgcolor="#cccccc">Alamat</td>
										<td nowrap align="left" width="400">${d.ALAMAT_LBG}</td>
								</c:forEach>
							</table>
						</table>
					</fieldset>
				</c:if>
				<c:if test="${buka eq '6'}">
					<fieldset class="ui-widget ui-widget-content">
						<table class="displaytag" cellpadding='0' cellspacing='0'
							border="1" align="left">
							<tr align="left">
								<th colspan="2" bgcolor="#cccccc">Tujuan Mengikuti Program
									Sosial Nasabah(SMiLe Religi) : <input type="text"
									value="${namapsn}" size="30" readonly>
								</th>
							</tr>
							<tr align="left">
								<th colspan="2" bgcolor="#cccccc">Asumsi biaya : <input
									type="text" size="25" maxlength="25" name="nominal">
								</th>
							</tr>
							<tr align="left">
								<th colspan="2" bgcolor="#cccccc">Frekuensi Pembayaran
									(Pilih salah Satu)
								<th></th>
							</tr>
							<tr align="left">
								<th colspan="2"><input type="checkbox" class="noBorder"
									name="chbox2" value="0" id="chbox2">Sekaligus</th>
							</tr>
							<table class="displaytag" cellpadding='0' cellspacing='0'
								border="1" align="left">
								<td align="left" colspan="2">Jenis Program : <select
									name="select_id_item" id="select_id_item">
										<c:forEach var="item" items="${listItem}">
											<option value="${item.ID_ITEM}">${item.NAMA_ITEM}</option>
										</c:forEach>
								</select>

								<table class="displaytag" cellpadding='0' cellspacing='0'
									border="1" align="left">
									<td align="left" colspan="2">Lembaga Program Sosial
										Nasabah Yang Tertera :</td>
									<c:forEach items="${listLembaga}" var="d">
										<tr align="left">
											<td align="left" colspan="2"><input type="checkbox"
												class="noBorder" name="chbox3" value="${d.ID_LBG}"
												id="chbox3">
											</td>
										</tr>
										<tr align="left">
											<td nowrap align="left" width="120" bgcolor="#cccccc">Nama
												Lembaga :</td>
											<td nowrap align="left" width="200">${d.NAMA_DTL_LBG}</td>
										</tr>
										<tr align="left">
											<td nowrap align="left" width="140" bgcolor="#cccccc">Alamat</td>
											<td nowrap align="left" width="400">${d.ALAMAT_LBG}</td>
										</tr>
										<tr align="left">
											<td nowrap align="left" width="200" bgcolor="#cccccc">Rekening
												Bank</td>
											<td><select name="select_data" id="select_data"
												disabled="disabled">
													<c:forEach var="bank" items="${bang}">
														<option
															<c:if test="${d.LSBP_ID eq bank.LSBP_ID}"> SELECTED </c:if>
															value="${bank.LSBP_ID}">${bank.LSBP_NAMA}</option>
													</c:forEach>
											</select>
											</td>
										</tr>
										<tr align="left">
											<td nowrap align="left" width="70" bgcolor="#cccccc">No
												Rekening</td>
											<td nowrap align="left" width="100">${d.NO_REK}</td>
										</tr>
										</tr>
									</c:forEach>
								</table>
								</td>
							</table>
						</table>
					</fieldset>
				</c:if>
				<c:if test="${ not empty buka}">
					<div class="rowElem">
						<input type="submit" name="save" id="save" value="Save Proses PSN">
						<input type="button" value="Surat Kepesertaan PSN"
							name="surat_konfirm" onclick="buttonLinks('${formatspaj}');">
					</div>
				</c:if>
				<div class="rowElem">
					<c:forEach items="${pesanList}" var="d" varStatus="st">
						<c:choose>
							<c:when test="${d.sts eq 'FAILED'}">
								<font color="red">${st.index+1}. ${d.msg}</font>
							</c:when>
							<c:otherwise>
								<font color="blue">${st.index+1}. ${d.msg}</font>
							</c:otherwise>
						</c:choose>

						<br />
					</c:forEach>
				</div>	 			
	</form>						              		
  </div>
</div>		
 </body>
</html>


