<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript">
	hideLoadingMessage();
	
	function stat(){
		spaj = document.frmParam.spaj.value;
		if(spaj==''){
			alert('Harap cari SPAJ terlebih dahulu');
		}else{
			createLoadingMessage();
			document.getElementById('infoFrame').src='${path }/uw/status.htm?pos=1&spaj='+spaj;
		}
	}
	
	function disabledTransferButton(){
		var spajListDisabled = document.getElementById('spajListDisabledForTransfer').value;
		if( spajListDisabled.match(spaj) )
		{
			document.getElementById('transfer').disabled = false;
		}
		else 
		{
			document.getElementById('transfer').disabled = true;
		}
	}
	
	function buttonLinks(str){
		if(str=='cari'){
			spaj = document.frmParam.spaj.value;
			if(spaj==''){
				alert('Harap cari SPAJ terlebih dahulu');
			}else{
				var spajList4EditButton = document.getElementById('spajList4EditButton').value;
				var spajListDisabled = document.getElementById('spajListDisabledForTransfer').value;
				var ldeId = document.getElementById('ldeId').value;
				var jnBank = document.getElementById('jnBank').value;
				if(jnBank ==2)
				{
					if( ldeId == 11 )
					{
						document.getElementById('transfer').disabled = false;
					}
					else
					{
						if( spajListDisabled.match(spaj) )
						{
							document.getElementById('transfer').disabled = false;
						}
						else 
						{
							document.getElementById('transfer').disabled = true;
						}
					}
				}
				if( spajList4EditButton.match(spaj) )
				{
					alert('Maaf, Spaj tidak bisa diedit krn sudah diotorisasi / approved');
				}
				else
				{
					document.getElementById('infoFrame').src='${path}/bac/editdmtm.htm?data_baru=true&showSPAJ='+document.frmParam.spaj.value;
				}
				
			}
		}else{
			spaj = document.frmParam.spaj.value;
			if(spaj==''){
				alert('Harap cari SPAJ terlebih dahulu');
			}else{
			
				switch (str) {
				case "nik" :
					if  ((document.frmParam.kodebisnis.value == 138)  ||  (document.frmParam.kodebisnis.value == 140)  || (document.frmParam.kodebisnis.value == 141)  || (document.frmParam.kodebisnis.value == 148)  || (document.frmParam.kodebisnis.value == 149)  || (document.frmParam.kodebisnis.value == 156)  ||(document.frmParam.kodebisnis.value == 139) ||((document.frmParam.kodebisnis.value == 142 && document.frmParam.numberbisnis.value == 4) ) ||((document.frmParam.kodebisnis.value == 143 && document.frmParam.numberbisnis.value == 2) ) ||((document.frmParam.kodebisnis.value == 158 && document.frmParam.numberbisnis.value == 4) )  ||((document.frmParam.kodebisnis.value == 158 && document.frmParam.numberbisnis.value == 7) )){
						popWin('${path}/bac/nik.htm?sts=insert&spaj='+spaj, 500, 800);
					}else{
						alert('Nik tidak dapat diisi untuk polis ini');					
					}
					break;
				case "titipanpremi" :
					//validasi akses titipan premi, ada di controller titipan premi
					popWin('${path}/ttppremi/titipan_premi.htm?sts=insert&editSPAJ='+spaj, 500, 800);
					
					break;
				case "simas" :
					if (document.frmParam.kodebisnis.value == 161)
					{
						popWin('${path}/bac/editttgsimas.htm?sts=insert&showSPAJ='+spaj, 500, 800);
					}else{
						alert("Tertanggung Simas tidak perlu diisi untuk produk ini, proses selanjutnya adalah Transfer");
					}
					break;		
				case "hcp" :
					if (document.frmParam.kodebisnis.value != 161)
					{
						if(document.frmParam.kodebisnis.value==189 || document.frmParam.kodebisnis.value==183 || document.frmParam.kodebisnis.value==178 || document.frmParam.kodebisnis.value==193){
								popWin('${path}/bac/ttghcp.htm?sts=insert&showSPAJ='+spaj, 500, 800);
						}else {
							if (eval(document.frmParam.jml_peserta.value) > 0){
								popWin('${path}/bac/ttghcp.htm?sts=insert&showSPAJ='+spaj, 500, 800);
							}else{
								alert("Tidak bisa mengisi data Smart Medicare/Eka Sehat/HCP Family karena belum mengambil plan BASIC, Silahkan melakukan penginputan plan BASIC pada halaman penginputan spaj utama terlebih dahulu.");
							}
						}
					}else{
						alert("Produk ini tidak bisa mengisi data Smart Medicare/Eka Sehat/HCP Family");
					}
					break;		
				case "info" :
					document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj;
					break;		
				case "reffbii" :
					popWin('${path}/bac/reff_bank.htm?window=main&spaj='+spaj, 400, 700);
					break;
				case "reffdmtm" :
					popWin('${path}/bac/reff_bank.htm?window=reff_dmtm&spaj='+spaj, 400, 700);
					break;
				case "uwinfo" :
					document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=uwinfo&spaj='+spaj;
					break;
				case "aksepManual" :
					popWin('${path}/bac/pending.htm?spaj='+spaj, 250, 375);
					break;
				case "rekening" :
					popWin('${path}/uw/viewer.htm?window=rekening&spaj='+spaj, 400, 700);
					break;
				case "view" :
					popWin('${path}/uw/viewer.htm?window=viewerkontrol&spaj='+spaj, 400, 550);
					break;
				case "summary" :
					//popWin('${path}/bac/summary.htm', 500, 700);
					document.getElementById('infoFrame').src='${path}/bac/summary.htm';
					break;
				case "ttd" :
					popWin('${path}/bac/sign.htm?window=main&spaj='+spaj, ${panjang}, ${lebar});
					break;
				case "agen" :
					popWin('${path}/bac/agen.htm?spaj='+spaj, 360, 480);
					break;
				case "batalkan" :
					popWin('${path}/bac/cancelBac.htm?spaj='+spaj, 500, 700);
					break;
				case "checklist" :
					document.getElementById('infoFrame').src = '${path}/checklist.htm?lspd_id=1&reg_spaj='+spaj;
					break;
				case "editagen" :
					popWin('${path}/bac/editagenpenutup.htm?spaj='+spaj, 500, 700);
					break;
				case "kesehatan" :
					if  ((document.frmParam.kodebisnis.value == 155) || ((document.frmParam.kodebisnis.value == 158 && document.frmParam.numberbisnis.value == 5) ) )
					{
						popWin('${path}/bac/keterangan_kesehatan.htm?spaj='+spaj, 500, 700);
					}else{
						alert("Data Kesehatan hanya diinput untuk produk Platinum Save");
					}
					break;	
				//case "tglTerimaAdmin" :
					//popWin('${path }/uw/viewer.htm?window=editTglTrmKrmAdmin&spaj='+spaj+'&show=0', 200, 350);
				//break;
				case "tglTerimaSpaj" :
					popWin('${path }/uw/viewer.htm?window=editTglTrmKrmSpaj&spaj='+spaj+'&show=0', 150, 350);
				break;
				case "limit" :
					popWin('${path}/common/menu.htm?frame=view_limit_uw', 600, 800);
					break;
				case "simultan" :
					popWin('${path }/uw/viewer.htm?window=view_simultan', 600, 800);
					break;
				case "download_spaj" :
					spaj = document.frmParam.spaj.value;
					document.getElementById('infoFrame').src='${path}/bac/multi.htm?window=download_spaj&spaj='+spaj;
					break;		
				case "medis" :
					spaj = document.frmParam.spaj.value;
					document.getElementById('infoFrame').src='${path}/uw/medical_new.htm?spaj='+spaj;
					break;	
				case "questionare" :
					spaj = document.frmParam.spaj.value;
					document.getElementById('infoFrame').src='${path}/bac/questionareDMTM.htm?spaj='+spaj;
					break;					
				}
				
			}
		}
	}
	
	function trans(){
		spaj = document.frmParam.spaj.value;
		if(spaj==''){
			alert('Harap cari SPAJ terlebih dahulu');
		}else{
			//if (document.frmParam.jn_bank.value == 2 || document.frmParam.jn_bank.value == 3){
			//if (document.frmParam.jn_bank.value == 3){
			//	document.getElementById('infoFrame').src='${path}/bac/transferbactouwbanksinarmas.htm?spaj='+document.frmParam.spaj.value; 
			//}else{
			document.getElementById('infoFrame').src='${path}/bac/transferbactouw.htm?spaj='+document.frmParam.spaj.value; 			
			//}
		}
	}

	function awal(){
		cariregion(document.frmParam.spaj.value,'region');
		setFrameSize('infoFrame', 64);
	}
	
	function cariregion(spaj,nama)
	{
			var spajListDisabled = document.getElementById('spajListDisabledForTransfer').value;
			var ldeId = document.getElementById('ldeId').value;
			var jnBank = document.getElementById('jnBank').value;
			if( jnBank == 2 )
			{
				if( ldeId == 11 )
				{
					document.getElementById('transfer').disabled = false;
				}
				else
				{
					if( spajListDisabled.match(spaj) )
					{
						document.getElementById('transfer').disabled = false;
					}
					else 
					{
						document.getElementById('transfer').disabled = true;
					}
				}
			}
			ajaxManager.listcollect(spaj , nama,
			{callback:function(map) {
		
				DWRUtil.useLoadingMessage();
				document.frmParam.koderegion.value=map.LSRG_NAMA;
				document.frmParam.kodebisnis.value = map.LSBS_ID;
				document.frmParam.numberbisnis.value = map.LSDBS_NUMBER;
				document.frmParam.jml_peserta.value = map.jml_peserta;
				document.frmParam.cab_bank.value=map.CAB_BANK;
				document.frmParam.jn_bank.value=map.JN_BANK;
				document.frmParam.lus_id.value = map.LUS_ID;
			   },
			  timeout:15000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}

	function copyToClipboard(txt){
	
		if( window.clipboardData && clipboardData.setData ){
		
			clipboardData.setData("Text", txt);
		}
		else{
		 alert("Internet Explorer required"); 
		}
	}
	
	function run(){

		spaj = document.frmParam.spaj.value;
		if(spaj==''){
			alert('Harap cari SPAJ terlebih dahulu');
		}else{
			var shell = new ActiveXObject("WScript.Shell"); 
			var commandtoRun = "\\\\aplikasi\\lions\\secan4400f.exe";
		   
			if (shell){		
			 shell.run(commandtoRun); 
			} 
			else{ 
				alert("program or file doesn't exist on your system."); 
			}
		}

	}
	
	function copyAndRun(){ 
	 	txt = document.getElementById("spaj").value;
	 	copyToClipboard(txt);
	 	run();		
	}	
</script>
<body 
	onload="awal(); setFrameSize('infoFrame', 85);"
	onresize="setFrameSize('infoFrame', 85);" style="height: 100%;">

<form name="frmParam" method="post">
<div class="tabcontent">
<table class="entry2" style="width: 98.4%;">
	<tr>
		<th>SPAJ</th>
		<td>
			
			<input type="button" value="Input" name="info"
				onclick="document.getElementById('infoFrame').src='${path}/bac/editdmtm.htm?data_baru=true';" 
				accesskey="I" onmouseover="return overlib('Input SPAJ Baru', AUTOSTATUS, WRAP);" onmouseout="nd();"
				<c:if test="${sessionScope.currentUser.flag_approve eq 1}"> disabled="true" </c:if>>
			

		
			<select name="spaj" onChange="cariregion(this.options[this.selectedIndex].value,'region');" id="spaj">
				<option value="">[--- Silahkan Pilih/Cari SPAJ ---]</option>
				<c:forEach var="s" items="${daftarSPAJ}">
					<option value="${s.REG_SPAJ }" style="background-color: ${s.OTORISASI_BG};" 
						<c:if test="${s.REG_SPAJ eq param.spaj }">selected<c:set var="ada" value="ada" /></c:if>>${s.SPAJ_FORMATTED}
					- ${s.POLICY_FORMATTED }</option>
				</c:forEach>
			</select>
			
			<input type="button" value="Cari" name="search"
				onclick="popWin('${path}/uw/spajgutri.htm?posisi=1&win=bac&spajList4EditButton=${spajList4EditButton}', 350, 450);"
				accesskey="C" onmouseover="return overlib('Cari SPAJ', AUTOSTATUS, WRAP);" onmouseout="nd();">
				 <!-- &search=yes -->

			<input type="button" value="Edit" name="info" onclick="return buttonLinks('cari');" 
				accesskey="T" onmouseover="return overlib('Edit SPAJ', AUTOSTATUS, WRAP);" onmouseout="nd();"
				<c:if test="${sessionScope.currentUser.flag_approve eq 1}"> disabled="true" </c:if>>
				<!--${path }/uw/view.htm?showSPAJ='+document.frmParam.spaj.options[document.frmParam.spaj.selectedIndex].value;"> document.getElementById('infoFrame').src='${path}/bac/edit.htmshowSPAJ='+spaj; -->			

			<c:if test="${empty sessionScope.currentUser.cab_bank}">
				<input type="button" value="TTD" name="ttd"
					onclick="return buttonLinks('ttd');"
					accesskey="D" onmouseover="return overlib('Upload Tanda Tangan Nasabah', AUTOSTATUS, WRAP);" onmouseout="nd();">

				<input type="button" value="U/W Info" name="uwinfo"
					onclick="return buttonLinks('uwinfo');"
					accesskey="W" onmouseover="return overlib('U/W Info', AUTOSTATUS, WRAP);" onmouseout="nd();">
					
				<input type="button" value="Aksep Manual" name="aksepManual"
					onclick="return buttonLinks('aksepManual');"
					accesskey="A" onmouseover="return overlib('Aksep Manual (Worksite)', AUTOSTATUS, WRAP);" onmouseout="nd();">
					
				<input type="button" value="Agen" name="editagen"
					onclick="return buttonLinks('editagen');"
					accesskey="G" onmouseover="return overlib('Edit Kode Agen', AUTOSTATUS, WRAP);" onmouseout="nd();">
	
				<input type="button" value="Tgl Terima Spaj" name="tglTerima"
					onclick="return buttonLinks('tglTerimaSpaj');"
					accesskey="P" onmouseover="return overlib('Edit Tanggal Terima Spaj', AUTOSTATUS, WRAP);" onmouseout="nd();">

<!--				<input type="button" value="Tgl Terima Admin" name="tglAdmin"-->
<!--					onclick="return buttonLinks('tglTerimaAdmin'); "-->
<!--					accesskey="P" onmouseover="return overlib('Edit Tanggal Terima Admin', AUTOSTATUS, WRAP);" onmouseout="nd();">-->
					
				<input type="button" value="Scan" name="copySpaj"	onclick="copyAndRun();">
			</c:if>
			
		</td>
	</tr>
	<tr>
		<th rowspan="2">Proses</th>
		<td colspan="2">
			<input type="hidden" name="koderegion" >
			<input type="hidden" id="ldeId" value="${ldeId}" >
			
			<input type="hidden" name="kodebisnis" >
			<input type="hidden" name="numberbisnis" >
			<input type="hidden" name="jml_peserta">
			<input type="hidden" name="lus_id">
			<input type="hidden" name="cab_bank">
			<input type="hidden" name="jn_bank">
			<input type="hidden" name="user_id" value= "${user_id}">
			<input type="hidden" name="cabang_bank" value ="${cabang_bank}">
			<input type="button" value="Upload" name="upload" disabled="disabled" style="display: none;"
			onclick="document.getElementById('infoFrame').src='${path}/bac/upload.htm';" 
			accesskey="U" onmouseover="return overlib('Upload Polis Via Excel', AUTOSTATUS, WRAP);" onmouseout="nd();">
			
			<input type="button" value="Referral" name="reffbii"	onclick="return buttonLinks('reffbii');" 
				accesskey="R" onmouseover="return overlib('Insert Referral', AUTOSTATUS, WRAP);" onmouseout="nd();"
				<c:if test="${sessionScope.currentUser.flag_approve eq 1}"> disabled="true" </c:if>>
						
			<input type="button" value="Transfer" name="transfer" onclick="trans();" id="transfer"
				accesskey="F" onmouseover="return overlib('Transfer', AUTOSTATUS, WRAP);" onmouseout="nd();"
				<c:if test="${sessionScope.currentUser.jn_bank eq 3 and sessionScope.currentUser.flag_approve eq 0}"> disabled="true" </c:if>>
				
			
			<c:if test="${empty sessionScope.currentUser.cab_bank}">
				<input type="button" value="Titipan Premi" name="ttp" onclick="return buttonLinks('titipanpremi');"
					accesskey="M" onmouseover="return overlib('Titipan Premi', AUTOSTATUS, WRAP);" onmouseout="nd();">
					
				<input type="button" value="Rekening" name="rekening"
					onclick="return buttonLinks('rekening');"
					accesskey="K" onmouseover="return overlib('Rekening', AUTOSTATUS, WRAP);" onmouseout="nd();">
					
				<input type="button" value="View" name="view"
					onclick="return buttonLinks('view');"
					accesskey="V" onmouseover="return overlib('View', AUTOSTATUS, WRAP);" onmouseout="nd();">
					
				<input type="button" value="Summary" name="summary"
					onclick="return buttonLinks('summary');"
					accesskey="S" onmouseover="return overlib('Report Summary', AUTOSTATUS, WRAP);" onmouseout="nd();">
	
				<%--<input type="button" value="Batalkan" name="batalkan"
					onclick="return buttonLinks('batalkan');"
					accesskey="B" onmouseover="return overlib('Batalkan Polis', AUTOSTATUS, WRAP);" onmouseout="nd();">--%>
	
				 <input type="button" value="Kesehatan" name="kesehatan"
					onclick="return buttonLinks('kesehatan');"
					accesskey="B" onmouseover="return overlib('Keterangan Kesehatan', AUTOSTATUS, WRAP);" onmouseout="nd();">
		
				<input type="button" value="NIK" name="nik"	onclick="return buttonLinks('nik');" 
					accesskey="Y" onmouseover="return overlib('Edit Nik', AUTOSTATUS, WRAP);" onmouseout="nd();">
	
				<input type="button" value="SIMAS" name="simas"	onclick="return buttonLinks('simas');" 
					accesskey="H" onmouseover="return overlib('TTG Simas', AUTOSTATUS, WRAP);" onmouseout="nd();">
	
				<input type="button" value="SM/EKA SEHAT/HCP FAMILY" name="hcp"	onclick="return buttonLinks('hcp');" 
					accesskey="P" onmouseover="return overlib('HCP FAMILY', AUTOSTATUS, WRAP);" onmouseout="nd();">
	
			</c:if>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="button" value="Info" name="ucup"	onclick="return buttonLinks('info');" 
				 onmouseover="return overlib('Info Lengkap Dokumen Polis', AUTOSTATUS, WRAP);" onmouseout="nd();">			
				 			
			<c:if test="${empty sessionScope.currentUser.cab_bank}">
				<input type="button" value="Download SPAJ" name="download_spaj"	onclick="return buttonLinks('download_spaj');" 
					 onmouseover="return overlib('Download Formulir SPAJ', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="Reff DM/TM" name="reffdmtm"	onclick="return buttonLinks('reffdmtm');" 
					accesskey="R" onmouseover="return overlib('Insert Telemarketer', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="View Limit U/W" name="limit"	onclick="return buttonLinks('limit');" 
					accesskey="L" onmouseover="return overlib('View Limit U/W Individu', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="View Simultan" name="simultan"	onclick="return buttonLinks('simultan');" 
					 onmouseover="return overlib('View Simultan Polis', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="Status" name="stattt"	onclick="stat();" 
					 onmouseover="return overlib('Status (Further Requirement)', AUTOSTATUS, WRAP);" onmouseout="nd();">
			</c:if>
			
			<input type="button" value="Checklist" name="checklist"	onclick="return buttonLinks('checklist');" 
				 onmouseover="return overlib('Checklist Dokumen Polis', AUTOSTATUS, WRAP);" onmouseout="nd();"
				 <c:if test="${sessionScope.currentUser.flag_approve eq 1}"> disabled="true" </c:if>>
			<input name="btn_medis" type="button" value="Medis" onClick="return buttonLinks('medis');" accesskey="E" 
			     onmouseover="return overlib('View Medis', AUTOSTATUS, WRAP);" onmouseout="nd();">	 
			<input name="btn_questionare" type="button" value="Questionare" onClick="return buttonLinks('questionare');" accesskey="Q" 
			     onmouseover="return overlib('Questionare DMTM', AUTOSTATUS, WRAP);" onmouseout="nd();" disabled="disabled">
		</td>
	</tr>

	<tr>
		<td colspan="2">
			<c:choose>
				<c:when test="${sessionScope.currentUser.flag_approve eq 1}">
					<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
						width="100%" style="width: 100%;"> Please Wait... </iframe>
				</c:when>
				<c:otherwise>
					<iframe src="${path}/bac/editdmtm.htm" name="infoFrame" id="infoFrame"
						width="100%" style="width: 100%;"> Please Wait... </iframe>
				</c:otherwise>
			</c:choose>
			
<input type="hidden" value="${spajList}" id="spajListDisabledForTransfer" />
<input type="hidden" value="${spajList4EditButton}" id="spajList4EditButton" />
<input type="hidden" value="${jnBank}" id="jnBank" />
		</td>
	</tr>
</table>
</div>
</form>

</body>
<%@ include file="/include/page/footer.jsp"%>