<%@ include file="/include/page/header_mall.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>

<script>
	if('${status}' == 'input' && '${status_submit}' == 'baru')
	{
		alert('Selama belum ditransfer / approve, anda bisa melakukan perubahan data dengan menekan tombol edit');	
	}
	function buttonLinks(str){
		switch (str) {
			case "cari" : 
				popWin('${path}/pemegang.jsp?new=true', 800, 500);  //&search=yes
				break;
		 	case "titipanpremi" :
			if ((document.frmParam.cab_bank.value =="") )
			{
				spaj = document.frmParam.spaj.value;
				if(spaj=='')
				{
					alert('Harap cari SPAJ terlebih dahulu');
				}else{ 
					if (document.frmParam.kodebisnis.value == 157)
					{
						alert("Titipan Premi tidak perlu diisi untuk produk ini, proses selanjutnya adalah Transfer ke UW");
					}else{
						popWin('${path}/ttppremi/titipan_premi.htm?sts=insert&editSPAJ='+spaj, 500, 800);
					}
				}
			}else{
				alert("Produk ini tidak bisa mengisi titipan premi");
			}
				break;		
			case "newref" :
			//alert(document.frmParam.cab_bank.value);
		
				popWin('${path}/common/referral.htm?window=main_referral', 500, 800);// Sinarmas
				
			break;
			case "hcp" :
			spaj = document.frmParam.spaj.value;
					if (document.frmParam.kodebisnis.value != 161)
					{
						if(document.frmParam.kodebisnis.value==183 || document.frmParam.kodebisnis.value==189){
						//if(document.frmParam.kodebisnis.value==183 || document.frmParam.kodebisnis.value==189 || document.frmParam.kodebisnis.value==193){
								popWin('${path}/bac/ttghcp.htm?sts=insert&showSPAJ='+spaj, 500, 800);
						}else {
							if (eval(document.frmParam.jml_peserta.value) > 0){
								popWin('${path}/bac/ttghcp.htm?sts=insert&showSPAJ='+spaj, 500, 800);
							}else{
								alert("Tidak bisa mengisi data Eka Sehat/HCP Family karena belum mengambil plan BASIC, Silahkan melakukan penginputan plan BASIC pada halaman penginputan spaj utama terlebih dahulu.");
							}
						}
					}else{
						alert("Produk ini tidak bisa mengisi data Eka Sehat/HCP Family");
					}
					break;	
			case "simas" :
			spaj = document.frmParam.spaj.value;
				if (document.frmParam.kodebisnis.value == 161)
				{
					popWin('${path}/bac/editttgsimas.htm?sts=insert&showSPAJ='+spaj, 500, 800);
				}else{
					alert("Tertanggung Simas tidak perlu diisi untuk produk ini, proses selanjutnya adalah Transfer ke UW");
				}
			break;	
			}
	}

function cariregion(spaj,nama)
	{
	if (spaj !="")
	{
		var dok;
		if(self.opener)
			dok = self.opener.document;
		else
			dok = parent.document;
		
		var forminput;
		if(dok.formpost) forminput = dok.formpost;
		else if(dok.frmParam) forminput = dok.frmParam;
		addOptionToSelect(dok, forminput.spaj, spaj, spaj);
		
			ajaxManager.listcollect(spaj , nama,
			{callback:function(map) {
		
				DWRUtil.useLoadingMessage();
				document.frmParam.koderegion.value=map.LSRG_NAMA;
				document.frmParam.kodebisnis.value = map.LSBS_ID;
				document.frmParam.numberbisnis.value = map.LSDBS_NUMBER;
				document.frmParam.jml_peserta.value = map.jml_peserta;
				document.frmParam.cab_bank.value=map.CAB_BANK;
				document.frmParam.jn_bank.value=map.JN_BANK;
				document.frmParam.lus_id.value = map.LUS_ID;
			   },
			  timeout:15000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
}
</script>

<body bgcolor="ffffff" onLoad="cariregion('${nomorspaj}','region');">

	<XML ID=xmlData></XML>
	<form name="frmParam" method="post">
		<input type="hidden" name="koderegion">
		<input type="hidden" name="kodebisnis">
		<input type="hidden" name="validhcp">
		<input type="hidden" name="numberbisnis">
		<input type="hidden" name="jml_peserta">
		<input type="hidden" name="lus_id">
		<input type="hidden" name="cab_bank">
		<input type="hidden" name="jn_bank">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td width="67%" height="35" bgcolor="#FDFDFD" valign="bottom">
					<img border="0" src="${path}/include/image/pp1.jpg" height="27"
						width="99" alt="Pemegang Polis">
					<img border="0" src="${path}/include/image/ttg1.jpg" height="27"
						width="99" alt="Tertanggung">
					<img border="0" src="${path}/include/image/ddu1.jpg" height="27"
						width="99" alt="Usulan Asuransi">
					<img border="0" src="${path}/include/image/inv1.jpg" height="27"
						width="99" alt="Detail Investasi">
					<img border="0" src="${path}/include/image/ag1.jpg" height="27"
						width="99" alt="Detail Agen">
					<img border="0" src="${path}/include/image/kf1.jpg" height="27"
						width="99" alt="Konfirmasi">
					<img border="0" src="${path}/include/image/sb2.jpg" height="27"
						width="99" alt="Submit">
				</td>
			</tr>
		</table>
		<input type="hidden" name="spaj" value=${nomorspaj}>
		<c:if test="${status eq \"input\"}">
			<c:if test="${nomorspaj eq \"\"}">
				<table class="form_input" border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<th bgcolor="#DDDDDD">
							&nbsp;
						</th>
					</tr>
					<tr>
						<th>
							&nbsp;
						</th>
					</tr>
					<tr>
						<th class="subtitle" width="67%" rowspan="12" align="left" bgcolor="#800000">

							<p align="center">
								<font face="Verdana" size="2" color="#FFFFFF"> <br>
									<center>
										<b> Spaj tidak berhasil di submit. Silahkan dicoba kembali
											atau hubungi administrator. </b>
										<br>
										<input type="button" name="ref" value="Coba Kembali"
											onclick="parent.document.getElementById('infoFrame').src='${path}/bac/editmall.htm?jenis_pemegang_polis='+parent.document.getElementById('jenis_pemegang_polis').value;">
									</center> </font>
							</p>
						</th>
					</tr>
				</table>
			</c:if>
			<c:if test="${nomorspaj ne \"\" and empty sessionScope.currentUser.cab_bank}">
				<table class="form_input" border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<th bgcolor="#DDDDDD">
							&nbsp;
						</th>
					</tr>
					<tr>
						<th>
							&nbsp;
						</th>
					</tr>
					<tr>
						<th class="subtitle" width="67%" rowspan="12" align="left" bgcolor="#800000">

							<p align="center">
								<font face="Verdana" size="2" color="#FFFFFF"> <br>
									<center>
										<b> Spaj berhasil di submit. Silahkan catat nomor spaj : <elions:spaj
												nomor="${nomorspaj}" /> </b>
									</center> </font>
							</p>
						</th>
					</tr>
				</table>
				<table style="visibility: hidden;" border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td bgcolor="#800000">
							<p align="center">
								<font face="Verdana" size="2" color="#FFFFFF"> <br>
									<center>
										<b> Silahkan Menginput (1) Kuesioner (2) Print E-SPAJ, dan (3) Checklist Dokumen Polis</b>
									</center>
								</font>
							</p>
						</td>
					</tr>
					<tr>
						<td align="center" bgcolor="#800000">
							<input type="button" value="Input New Referral" name="newreff"
								onclick="return buttonLinks('newref');">
						</td>
					</tr>
					<tr>
						<td bgcolor="#800000">
							<p align="center">
								<font face="Verdana" size="2" color="#FFFFFF"> <br>
									<center>
										<b> Silahkan Menginput Data Rider HCP FAMILY</b>
								</font>
							</center>
							</font>
							</p>
						</td>
					</tr>
					<tr>
						<td align="center" bgcolor="#800000">
							<input type="button" value="HCP FAMILY" name="hcp"
								onclick="return buttonLinks('hcp');">
							<br>
						</td>
					</tr>
					<tr>
						<td bgcolor="#800000">
							<p align="center">
								<font face="Verdana" size="2" color="#FFFFFF"> <br>
									<center>
										<b> Silahkan Menginput Data Simas Sehat</b>
								</font>
							</center>
							</font>
							</p>
						</td>
					</tr>
					<tr>
						<td align="center" bgcolor="#800000">
							<input type="button" value="SIMAS" name="simas"
								onclick="return buttonLinks('simas');">
							<br>
							<br>
						</td>
					</tr>
				</table>

			</c:if>
			<c:if test="${kodebisnis eq 183 or kodebisnis eq 189 or kodebisnis eq 193 or jml_peserta ne 0 }">
				<script>
					parent.document.frmParam.kodebisnis.value = '${kodebisnis}';
				</script>
			</c:if>
			
			
			<c:if test="${nomorspaj ne \"\" and not empty sessionScope.currentUser.cab_bank}">
				<table class="form_input" border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<th bgcolor="#DDDDDD">
							&nbsp;
						</th>
					</tr>
					<tr>
						<th>
							&nbsp;
						</th>
					</tr>
					<tr>
						<th class="subtitle" width="67%" rowspan="12" align="left" bgcolor="#800000">

							<p align="center">
								<font face="Verdana" size="2" color="#FFFFFF"> <br>
									<center>
										<b> Spaj berhasil di submit. Silahkan catat nomor spaj : <elions:spaj
												nomor="${nomorspaj}" /> </b>
									</center> </font>
							</p>
						</th>
					</tr>
				</table>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td bgcolor="#800000">
							<p align="center">
								<font face="Verdana" size="2" color="#FFFFFF"> <br>
									<center>
										<b> Silahkan Menginput Refferal Bank (gunakan tombol diatas)</b>
									</center>
								</font>
							</p>
						</td>
					</tr>
					<tr>
						<td align="center" bgcolor="#800000">
							<input type="button" value="Input New Referral" name="newreff"
								onclick="return buttonLinks('newref');">
						</td>
					</tr>
				</table>

			</c:if>
		</c:if>
		<c:if test="${status eq \"edit\"}">
			<c:if test="${nomorspaj eq \"\"}">
				<table class="form_input" border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<th bgcolor="#DDDDDD">
							&nbsp;
						</th>
					</tr>
					<tr>
						<th>
							&nbsp;
						</th>
					</tr>
					<tr>
						<th  class="subtitle"  width="67%" rowspan="13" align="left" bgcolor="#800000">
							<p align="center">
								<font face="Verdana" size="2" color="#FFFFFF"> <br>
									<center>
										<b> Spaj tidak berhasil di edit. Silahkan dicoba kembali
											atau hubungi administrator. </b>
										<br>
										<input type="button" name="ref" value="Coba Kembali"
											onclick="history.go();">
									</center> </font>
							</p>
						</th>
					</tr>
					<tr></tr>
				</table>
			</c:if>
			<c:if test="${nomorspaj ne \"\"}">
				<table class="form_input" border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<th bgcolor="#DDDDDD">
							&nbsp;
						</th>
					</tr>
					<tr>
						<th>
							&nbsp;
						</th>
					</tr>
					<tr>
						<th  class="subtitle" width="67%" rowspan="12" align="left" bgcolor="#800000">

							<p align="center">
								<font face="Verdana" size="2" color="#FFFFFF"> <br>
									<center>
										<b> Spaj untuk nomor spaj : <elions:spaj
												nomor="${nomorspaj}" /> telah berhasil diedit.<br> <c:if
												test="${keterangan eq \"EDIT FUND SPAJ\"}">
												<p>
													<b>Spaj ini sudah di fund, tetapi telah dilakukan
														perubahan nilai dari fund tersebut</b>
												</p>
											</c:if> </b>
									</center> </font>
							</p>
						</th>
					</tr>
				</table>

				<c:if test="${lspd_id eq \"1\"}">
					<table style="visibility: hidden;" border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								&nbsp;
							</td>
						</tr>
						<c:if test="${empty sessionScope.currentUser.cab_bank}">
						<tr>
							<td bgcolor="#800000">
								<p align="center">
									<font face="Verdana" size="2" color="#FFFFFF"> <br>
										<center>
											<b> Silahkan Menginput Titipan Premi</b>
											<font face="Verdana" size="2" color="#FFFFFF"><b>dan
													Refferal Bank</b> (gunakan tombol diatas)</font>
										</center>
									</font>
								</p>
							</td>
						</tr>
						</c:if>
						<tr>
							<td align="center" bgcolor="#800000">
							</td>
						</tr>
						<c:if test="${empty sessionScope.currentUser.cab_bank}">
						<tr>
							<td bgcolor="#800000">
								<p align="center">
									<font face="Verdana" size="2" color="#FFFFFF"> <br>
										<center>
											<b> Silahkan Menginput Data Rider HCP FAMILY</b>
									</font>
								</center>
								</font>
								</p>
							</td>
						</tr>
						<tr>
							<td align="center" bgcolor="#800000">
								<input type="button" value="HCP FAMILY" name="hcp"
									onclick="return buttonLinks('hcp');">
								<br>
							</td>
						</tr>
						<tr>
							<td bgcolor="#800000">
								<p align="center">
									<font face="Verdana" size="2" color="#FFFFFF"> <br>
										<center>
											<b> Silahkan Menginput Data Simas Sehat</b>
									</font>
								</center>
								</font>
								</p>
							</td>
						</tr>
						<tr>
							<td align="center" bgcolor="#800000">
								<input type="button" value="SIMAS" name="simas"
									onclick="return buttonLinks('simas');">
								<br>
								<br>
							</td>
						</tr>
						</c:if>
					</table>
				</c:if>

			</c:if>
		</c:if>
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>
