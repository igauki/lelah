<%@ include file="/include/page/header.jsp"%>
<script>

	function mulai(){
		<c:if test="${not empty cmd.pesan}">
			alert('${cmd.pesan}');
		</c:if>
	}

	function konfirmasi(){
		if(confirm('${cmd.konfirmasi}')){
			return true;
		}
		return false;
	}
</script>
<body style="height: 100%;" onload="document.formpost.password.focus(); mulai();">
	<form method="post" name="formpost">
		<table class="entry2" style="width: auto;">
			<tr>
				<th>User Otorisasi</th>
				<td>
					<select name="otorotor">
						<option value="">-</option>
						<c:forEach var="d" items="${daftarOtor}">
							<option value="${d.key}">${d.value}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<th>Silahkan Masukkan Password Otorisasi Atasan / SPV Anda</th>
				<td>
					<input size="53" name="password" type="password" value="${password}">
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<input type="submit" name="save" value="Transfer" onclick="return konfirmasi();">

					<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
							<c:forEach var="error" items="${status.errorMessages}">
										- <c:out value="${error}" escapeXml="false" />
								<br />
								<script>errorMessages += '- ${error}\n';</script>
							</c:forEach></div>
						</c:if>									
					</spring:bind>
				</td>
			</tr>
		</table>
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>