<%@ include file="/include/page/header.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<%User currentUser = (User) session.getAttribute("currentUser");%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>
<script type="text/javascript" src="${path }/include/js/bac/pemegang.js"></script>
<script type="text/javascript" src="${path }/include/js/bac/pembayarpremi.js"></script>
<script>
// BEBERAPA JAVASCRIPT DIPINDAHKAN KE PEMEGANG.JS UNTUK MENGHINDARI ERROR EXCEEDING THE 65535 BYTES LIMIT.
// BEBERAPA JAVASCRIPT DIPINDAHKAN KE PEMBAYARPREMI.JS UNTUK MENGHINDARI ERROR EXCEEDING THE 65535 BYTES LIMIT.

var v_path = '${path}';
var v_tempat_kedudukan = '${cmd.pembayarPremi.tempat_kedudukan}';
var v_bidang_usaha_badan_hukum = '${cmd.pembayarPremi.bidang_usaha_badan_hukum}';
var v_tempat_lahir = '${cmd.pembayarPremi.tempat_lahir}';
	
	function Body_onload() {
	
	<c:if test="${not empty cmd.tertanggung.blacklist}">
			if(confirm("Ada Riwayat Blacklist Pada Tertanggung. \n Tekan OK untuk melihat data riwayat, atau CANCEL untuk melanjutkan input.")){
				//FIXME : popup window diwsini
				popWin('${path}/bac/multi.htm?window=view_attentionlist&blacklist=${cmd.tertanggung.blacklist}', 900, 850);
			}
	</c:if>
	
// 	var lsre_id_premi='${cmd.pembayarPremi.lsre_id_premi}';
		
// 		if (lsre_id_premi != '41'){
				
// 						if(lsre_id_premi == '40'){
// 									document.frmParam.elements['pembayarPremi.nama_pihak_ketiga'].value='${cmd.pemegang.mcl_first}';
// 									document.frmParam.elements['pembayarPremi.alamat_lengkap'].value='${cmd.pemegang.alamat_rumah}';
// 									document.frmParam.elements['pembayarPremi.kewarganegaraan'].value='${cmd.pemegang.lsne_id}';
// 									document.frmParam.elements['pembayarPremi.tempat_lahir'].value='${cmd.pemegang.mspe_place_birth}';
// 									document.frmParam.elements['pembayarPremi.area_code_3rd'].value='${cmd.pemegang.area_code_rumah}';
// 									document.frmParam.elements['pembayarPremi.telp_rumah'].value='${cmd.pemegang.telpon_rumah}';
// 									document.frmParam.elements['pembayarPremi.area_code_kantor_3rd'].value='${cmd.pemegang.area_code_kantor}';
// 									document.frmParam.elements['pembayarPremi.telp_kantor'].value='${cmd.pemegang.telpon_kantor}';
// 									document.frmParam.elements['pembayarPremi.pekerjaan'].value='${cmd.pemegang.mkl_kerja}';
// 									document.frmParam.elements['pembayarPremi.no_npwp'].value='${cmd.pemegang.mcl_npwp}';
// 									document.frmParam.elements['pembayarPremi.email'].value='${cmd.pemegang.email}';
// 									document.frmParam.elements['pembayarPremi.pekerjaan'].disabled=true;
// 						}else{
// 									document.frmParam.elements['pembayarPremi.nama_pihak_ketiga'].value='';
// 									document.frmParam.elements['pembayarPremi.alamat_lengkap'].value='';
// 									document.frmParam.elements['pembayarPremi.kewarganegaraan'].value='';
// 									document.frmParam.elements['pembayarPremi.tempat_lahir'].value='';
// 									document.frmParam.elements['pembayarPremi.mspe_date_birth_3rd'].value='';
// 									document.frmParam.elements['pembayarPremi.area_code_3rd'].value='';
// 									document.frmParam.elements['pembayarPremi.telp_rumah'].value='';
// 									document.frmParam.elements['pembayarPremi.area_code_kantor_3rd'].value='';
// 									document.frmParam.elements['pembayarPremi.telp_kantor'].value='';
// 									document.frmParam.elements['pembayarPremi.pekerjaan'].value='';
// 									document.frmParam.elements['pembayarPremi.no_npwp'].value='';
// 									document.frmParam.elements['pembayarPremi.email'].value='';
// 									document.frmParam.elements['pembayarPremi.pekerjaan'].disabled=false;
// 						}
				
// 						document.frmParam.elements['pembayarPremi.perusahaan'].disabled = true;	
// 						document.frmParam.elements['pembayarPremi.alamat_perusahaan'].value='';			
// 						document.frmParam.elements['pembayarPremi.alamat_perusahaan'].disabled = true;
// 						document.frmParam.elements['pembayarPremi.kdpos_perusahaan'].value='';
// 						document.frmParam.elements['pembayarPremi.kdpos_perusahaan'].disabled = true;
// 						document.frmParam.elements['pembayarPremi.prov_perusahaan'].value='';
// 						document.frmParam.elements['pembayarPremi.prov_perusahaan'].disabled = true;
// 						document.frmParam.elements['pembayarPremi.kota_perusahaan'].value='';
// 						document.frmParam.elements['pembayarPremi.kota_perusahaan'].disabled = true;
// 						document.frmParam.elements['pembayarPremi.area_code_perusahaan'].value='';
// 						document.frmParam.elements['pembayarPremi.area_code_perusahaan'].disabled = true;
// 						document.frmParam.elements['pembayarPremi.telp_perusahaan'].value='';
// 						document.frmParam.elements['pembayarPremi.telp_perusahaan'].disabled = true;
// 						document.frmParam.elements['pembayarPremi.area_code_fax_perusahaan'].value='';
// 						document.frmParam.elements['pembayarPremi.area_code_fax_perusahaan'].disabled = true;
// 						document.frmParam.elements['pembayarPremi.faks_perusahaan'].value='';
// 						document.frmParam.elements['pembayarPremi.faks_perusahaan'].disabled = true;
// 		}else{
						
// 						document.frmParam.elements['pembayarPremi.nama_pihak_ketiga'].value='${cmd.personal.mcl_first}';
// 						document.frmParam.elements['pembayarPremi.alamat_lengkap'].value='${cmd.pemegang.alamat_rumah}';
// 						document.frmParam.elements['pembayarPremi.kewarganegaraan'].value='${cmd.pemegang.lsne_id}';
// 						document.frmParam.elements['pembayarPremi.tempat_lahir'].value='${cmd.pemegang.mspe_place_birth}';
// 						document.frmParam.elements['pembayarPremi.area_code_3rd'].value='${cmd.pemegang.area_code_rumah}';
// 						document.frmParam.elements['pembayarPremi.telp_rumah'].value='${cmd.pemegang.telpon_rumah}';
// 						document.frmParam.elements['pembayarPremi.area_code_kantor_3rd'].value='${cmd.pemegang.area_code_kantor}';
// 						document.frmParam.elements['pembayarPremi.telp_kantor'].value='${cmd.pemegang.telpon_kantor}';
// 						document.frmParam.elements['pembayarPremi.pekerjaan'].value='${cmd.pemegang.mkl_kerja}';
						
// 						document.frmParam.elements['pembayarPremi.perusahaan'].disabled = false;	
// 						document.frmParam.elements['pembayarPremi.alamat_perusahaan'].value='';			
// 						document.frmParam.elements['pembayarPremi.alamat_perusahaan'].disabled = false;
// 						document.frmParam.elements['pembayarPremi.kdpos_perusahaan'].value='';
// 						document.frmParam.elements['pembayarPremi.kdpos_perusahaan'].disabled = false;
// 						document.frmParam.elements['pembayarPremi.prov_perusahaan'].value='';
// 						document.frmParam.elements['pembayarPremi.prov_perusahaan'].disabled = false;
// 						document.frmParam.elements['pembayarPremi.kota_perusahaan'].value='';
// 						document.frmParam.elements['pembayarPremi.kota_perusahaan'].disabled = false;
// 						document.frmParam.elements['pembayarPremi.area_code_perusahaan'].value='';
// 						document.frmParam.elements['pembayarPremi.area_code_perusahaan'].disabled = false;
// 						document.frmParam.elements['pembayarPremi.telp_perusahaan'].value='';
// 						document.frmParam.elements['pembayarPremi.telp_perusahaan'].disabled = false;
// 						document.frmParam.elements['pembayarPremi.area_code_fax_perusahaan'].value='';
// 						document.frmParam.elements['pembayarPremi.area_code_fax_perusahaan'].disabled = false;
// 						document.frmParam.elements['pembayarPremi.faks_perusahaan'].value='';
// 						document.frmParam.elements['pembayarPremi.faks_perusahaan'].disabled = false;
			
// 		}
	
	
	var frmParam = document.frmParam;
		body_onload_sub_1();
		frmParam.elements['pemegang.lus_id'].value=<%=currentUser.getLus_id()%>;
		frmParam.elements['pemegang.cbg_lus_id'].value='<%=currentUser.getLca_id()%>';
		body_onload_sub_2();
		var adaData = '${adaData}';
		var flag_spaj='${cmd.pemegang.mspo_flag_spaj}';
		//if(adaData != ''){
		//	if(!confirm('Ada data hasil input sebelumnya, apakah anda ingin menggunakan data ini?')){
		//		window.location = '${path}/bac/edit.htm?data_baru=1';
		//	}
		//}		
		var nameSelection
			xmlData.async = false;
			xmlData.src = "/E-Lions/xml/SUMBER_PENDANAAN.xml";
			xmlData.src = "/E-Lions/xml/SUMBER_PENGHASILAN.xml";
			generateXML_sumberKyc(xmlData, 'ID','DANA');
	var newvalue = '${cmd.addressbilling.tagih}';	
	if(newvalue!=""){
		document.getElementById('tgh').value=newvalue;		
		loaddata_penagihan();
	}else{
		document.getElementById('tgh').value='1';
		loaddata_penagihan();
	}
	

	if (flag_spaj==1){
		document.frmParam.elements['tertanggung.mste_no_vacc'].value='${cmd.tertanggung.mste_no_vacc}';		
	    document.frmParam.elements['tertanggung.mste_no_vacc'].readOnly =false;
		document.frmParam.elements['tertanggung.mste_no_vacc'].style.backgroundColor ='#FFFFFF';
	}else{
		document.frmParam.elements['tertanggung.mste_no_vacc'].value='';		
		document.frmParam.elements['tertanggung.mste_no_vacc'].readOnly = true;
		document.frmParam.elements['tertanggung.mste_no_vacc'].style.backgroundColor ='#D4D4D4';
		
	}
	
	if((frmParam.elements['pemegang.lus_id'].value=<%=currentUser.getLus_id()%>)=='2661'){//pemegang.mspo_nasabah_dcif
		document.frmParam.elements['pemegang.mspo_no_blanko'].readOnly = true;
		document.frmParam.elements['pemegang.mspo_no_blanko'].style.backgroundColor ='#D4D4D4';
		document.frmParam.elements['pemegang.mspo_nasabah_dcif'].readOnly = true;
		document.frmParam.elements['pemegang.mspo_nasabah_dcif'].style.backgroundColor ='#D4D4D4';
		document.frmParam.elements['no_pb'].readOnly = true;
		document.frmParam.elements['no_pb'].style.backgroundColor ='#D4D4D4';
		document.frmParam.elements['pemegang.sumber_id'].readOnly = true;
		document.frmParam.elements['pemegang.sumber_id'].style.backgroundColor ='#D4D4D4';
		}
	if (document.frmParam.document.frmParam.tanda_pp.value==null || document.frmParam.document.frmParam.tanda_pp.value==""){
			document.frmParam.elements['pemegang.mkl_dana_from'].value = '0';
			document.frmParam.elements['pemegang.mkl_sumber_premi'].value = '0';
			document.frmParam.document.frmParam.tanda_pp.value = '0';
		}
		
		cpy(document.frmParam.tanda_pp.value);
		
	if (document.frmParam.document.frmParam.tanda_hub.value==null || document.frmParam.document.frmParam.tanda_hub.value==""){
			document.frmParam.elements['pemegang.mkl_sumber_premi'].value = '0';
			document.frmParam.document.frmParam.tanda_hub.value = '0';
		}
		
		cpy2(document.frmParam.tanda_hub.value);	
		
	}
		
		
</script>
<script language="JavaScript">
<!--
	if (!document.layers&&!document.all&&!document.getElementById)
		event="test"
			function showtip(current,e,text){
			if (document.all||document.getElementById){
				thetitle=text.split('<br>')
				if (thetitle.length>1){thetitles=''
					for (i=0;i<thetitle.length;i++)
						thetitles+=thetitle[i]
					current.title=thetitles
				}else {current.title=text}
			}else if (document.layers){
				document.tooltip.document.write('<layer bgColor="white" style="border:1px solid black;font-size:13px;">'+text+'</layer>')
				document.tooltip.document.close()
				document.tooltip.left=e.pageX+5
				document.tooltip.top=e.pageY+5
				document.tooltip.visibility="show"
			}}
// -->
		
	//END OF specta_save_onClick()
</script>
<body onLoad="Body_onload(); document.frmParam.elements['datausulan.mste_medical'].focus();">
<XML ID=xmlData></XML>
<form name="frmParam" method="post">
<table class="entry2">
	<tr>
		<td><input type="submit" name="_target0" value=" " onclick="next1()"  <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/pp1.jpg);"
				accesskey="1" onmouseover="return overlib('Alt-1', AUTOSTATUS, WRAP);" onmouseout="nd();">			
			<c:choose><c:when test="${(sessionScope.currentUser.jn_bank eq '16')}">
			<input type="submit" name="_target1" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ttgbsim.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();"></c:when><c:otherwise>
				<input type="submit" name="_target1" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ttg1.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();"></c:otherwise></c:choose>
			<input type="submit" name="_target6" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ppremi2.jpg);"
				accesskey="7" onmouseover="return overlib('Alt-7', AUTOSTATUS, WRAP);" onmouseout="nd();">	
			<input type="submit" name="_target2" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ddu1.jpg);"
				accesskey="3" onmouseover="return overlib('Alt-3', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target3" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/inv1.jpg);"
				accesskey="4" onmouseover="return overlib('Alt-4', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target4" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ag1.jpg);"
				accesskey="5" onmouseover="return overlib('Alt-5', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target5" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/kf1.jpg);"
				accesskey="6" onmouseover="return overlib('Alt-6', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" name="dummy2" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/sb1.jpg);">
</td>
	</tr>
	<tr>
		<td width="67%" align="left" valign="top">
		<spring:bind path="cmd.*">
			<c:if test="${not empty status.errorMessages}">
				<div id="error">INFO:<br>
				<c:forEach var="error" items="${status.errorMessages}">
	- <c:out value="${error}" escapeXml="false" />
					<br />
				</c:forEach></div>
			</c:if>
		</spring:bind>	
		<table class="entry">
			<tr>
				<td colspan=5 height="20" style="text-align: center;">
					<%-- <input type="button"  value="Isi Data Pemegang Polis PT Guthrie" onclick="isi();" style="display:none;"> --%>
					<spring:bind path="cmd.datausulan.specta_save"> 
		              <input type="hidden" name="${status.expression}" value="${cmd.datausulan.specta_save}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>
					<input type="submit"
					name="_target${halaman-1}" value="Prev &laquo;" 
					onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
					<input type="submit" name="_target${halaman+1}" value="Next &raquo;" onClick="next()" 
						onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
					<input type="hidden" name="_page" value="${halaman}">
				</td>
			</tr>
			<tr>
				<tr>
				<th colspan=5 class="subtitle">DATA CALON PEMBAYAR PREMI</th>
			</tr>
			<tr>
				<th width="18">1.</th>
				<th width="287">Calon Pembayar Premi adalah<br>
				<span class="info">(Apabila Calon Pembayar Premi bukan perusahaan,maka langsung mengisi butir 5-8)</span>
				</th>
				<script type="text/javascript">
					function data_pembayar(flag){					
						if(flag!='41'){
							if(flag=='40'){
									document.frmParam.elements['pembayarPremi.nama_pihak_ketiga'].value='${cmd.pemegang.mcl_first}';
									document.frmParam.elements['pembayarPremi.alamat_lengkap'].value='${cmd.pemegang.alamat_rumah}';
									document.frmParam.elements['pembayarPremi.kewarganegaraan'].value='${cmd.pemegang.lsne_id}';
									document.frmParam.elements['pembayarPremi.tempat_lahir'].value='${cmd.pemegang.mspe_place_birth}';
									document.frmParam.elements['pembayarPremi.area_code_3rd'].value='${cmd.pemegang.area_code_rumah}';
									document.frmParam.elements['pembayarPremi.telp_rumah'].value='${cmd.pemegang.telpon_rumah}';
									document.frmParam.elements['pembayarPremi.area_code_kantor_3rd'].value='${cmd.pemegang.area_code_kantor}';
									document.frmParam.elements['pembayarPremi.telp_kantor'].value='${cmd.pemegang.telpon_kantor}';
									document.frmParam.elements['pembayarPremi.pekerjaan'].value='${cmd.pemegang.mkl_kerja}';
									document.frmParam.elements['pembayarPremi.no_npwp'].value='${cmd.pemegang.mcl_npwp}';
									document.frmParam.elements['pembayarPremi.email'].value='${cmd.pemegang.email}';
							}else if(flag=='1'){
									document.frmParam.elements['pembayarPremi.nama_pihak_ketiga'].value='${cmd.pemegang.mcl_first}';
									document.frmParam.elements['pembayarPremi.alamat_lengkap'].value='${cmd.pemegang.alamat_rumah}';
									document.frmParam.elements['pembayarPremi.kewarganegaraan'].value='${cmd.pemegang.lsne_id}';
									document.frmParam.elements['pembayarPremi.tempat_lahir'].value='${cmd.pemegang.mspe_place_birth}';
									document.frmParam.elements['pembayarPremi.area_code_3rd'].value='${cmd.pemegang.area_code_rumah}';
									document.frmParam.elements['pembayarPremi.telp_rumah'].value='${cmd.pemegang.telpon_rumah}';
									document.frmParam.elements['pembayarPremi.area_code_kantor_3rd'].value='${cmd.pemegang.area_code_kantor}';
									document.frmParam.elements['pembayarPremi.telp_kantor'].value='${cmd.pemegang.telpon_kantor}';
									document.frmParam.elements['pembayarPremi.pekerjaan'].value='${cmd.pemegang.mkl_kerja}';
									document.frmParam.elements['pembayarPremi.no_npwp'].value='${cmd.pemegang.mcl_npwp}';
									document.frmParam.elements['pembayarPremi.email'].value='${cmd.pemegang.email}';
							}else{
									document.frmParam.elements['pembayarPremi.perusahaan'].value='';
									document.frmParam.elements['pembayarPremi.alamat_perusahaan'].value='';
									
									document.frmParam.elements['pembayarPremi.nama_pihak_ketiga'].value='';
									document.frmParam.elements['pembayarPremi.alamat_lengkap'].value='';
									document.frmParam.elements['pembayarPremi.kewarganegaraan'].value='';
									document.frmParam.elements['pembayarPremi.tempat_lahir'].value='';
									document.frmParam.elements['pembayarPremi.mspe_date_birth_3rd'].value='';
									document.frmParam.elements['pembayarPremi.area_code_3rd'].value='';
									document.frmParam.elements['pembayarPremi.telp_rumah'].value='';
									document.frmParam.elements['pembayarPremi.area_code_kantor_3rd'].value='';
									document.frmParam.elements['pembayarPremi.telp_kantor'].value='';
									document.frmParam.elements['pembayarPremi.pekerjaan'].value='';
									document.frmParam.elements['pembayarPremi.no_npwp'].value='';
									document.frmParam.elements['pembayarPremi.email'].value='';
								}
						
							document.frmParam.elements['pembayarPremi.perusahaan'].disabled = true;	
							document.frmParam.elements['pembayarPremi.alamat_perusahaan'].value='';			
							document.frmParam.elements['pembayarPremi.alamat_perusahaan'].disabled = true;
							document.frmParam.elements['pembayarPremi.kdpos_perusahaan'].value='';
							document.frmParam.elements['pembayarPremi.kdpos_perusahaan'].disabled = true;
							document.frmParam.elements['pembayarPremi.prov_perusahaan'].value='';
							document.frmParam.elements['pembayarPremi.prov_perusahaan'].disabled = true;
							document.frmParam.elements['pembayarPremi.kota_perusahaan'].value='';
							document.frmParam.elements['pembayarPremi.kota_perusahaan'].disabled = true;
							document.frmParam.elements['pembayarPremi.area_code_perusahaan'].value='';
							document.frmParam.elements['pembayarPremi.area_code_perusahaan'].disabled = true;
							document.frmParam.elements['pembayarPremi.telp_perusahaan'].value='';
							document.frmParam.elements['pembayarPremi.telp_perusahaan'].disabled = true;
							document.frmParam.elements['pembayarPremi.area_code_fax_perusahaan'].value='';
							document.frmParam.elements['pembayarPremi.area_code_fax_perusahaan'].disabled = true;
							document.frmParam.elements['pembayarPremi.faks_perusahaan'].value='';
							document.frmParam.elements['pembayarPremi.faks_perusahaan'].disabled = true;
						}else{
								
							document.frmParam.elements['pembayarPremi.perusahaan'].value = '';	
							document.frmParam.elements['pembayarPremi.perusahaan'].disabled = false;	
							document.frmParam.elements['pembayarPremi.alamat_perusahaan'].value='';		
							document.frmParam.elements['pembayarPremi.alamat_perusahaan'].disabled = false;
							document.frmParam.elements['pembayarPremi.kdpos_perusahaan'].value='';
							document.frmParam.elements['pembayarPremi.kdpos_perusahaan'].disabled = false;
							document.frmParam.elements['pembayarPremi.prov_perusahaan'].value='';
							document.frmParam.elements['pembayarPremi.prov_perusahaan'].disabled = false;
							document.frmParam.elements['pembayarPremi.kota_perusahaan'].value='';
							document.frmParam.elements['pembayarPremi.kota_perusahaan'].disabled = false;
							document.frmParam.elements['pembayarPremi.area_code_perusahaan'].value='';
							document.frmParam.elements['pembayarPremi.area_code_perusahaan'].disabled = false;
							document.frmParam.elements['pembayarPremi.telp_perusahaan'].value='';
							document.frmParam.elements['pembayarPremi.telp_perusahaan'].disabled = false;
							document.frmParam.elements['pembayarPremi.area_code_fax_perusahaan'].value='';
							document.frmParam.elements['pembayarPremi.area_code_fax_perusahaan'].disabled = false;
							document.frmParam.elements['pembayarPremi.faks_perusahaan'].value='';
							document.frmParam.elements['pembayarPremi.faks_perusahaan'].disabled = false;
						}
					}
				</script>
				<th width="505"> <spring:bind path="cmd.pembayarPremi.lsre_id_premi">
						<select name="${status.expression}" onchange="data_pembayar(this.value);">
							<c:forEach var="d" items="${select_cpp}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
						<font color="#CC3300"></font>
					</spring:bind>
				</th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>2.</th>
				<th>Nama Perusahaan</th>
				<th colspan="3">
					<spring:bind path="cmd.pembayarPremi.perusahaan">
						<input type="text" name="${status.expression}"
							value="${status.value }" size="42" maxlength="50"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
					</spring:bind>
				</th>
			</tr>
		<tr>
			  <th rowspan="5">3.</th>
				<th rowspan="5">Alamat Perusahaan</th>
				<th rowspan="5"><spring:bind path="cmd.pembayarPremi.alamat_perusahaan">
					<textarea cols="40" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>${status.value }</textarea>
				</spring:bind></th>
				<th>Kode Pos</th>
				<th><spring:bind path="cmd.pembayarPremi.kdpos_perusahaan">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="5"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
			<th>Propinsi</th>
				<th><spring:bind path="cmd.pembayarPremi.prov_perusahaan">
					<select name="${status.expression}">
							<c:forEach var="d" items="${select_propinsi}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
				</spring:bind></th>
			</tr>
			<tr>
				<th>Kota</th>
				<th><spring:bind path="cmd.pembayarPremi.kota_perusahaan"><br>
				<input type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
		    	<span id="indicator_kantor" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
			</spring:bind>   
			<spring:bind path="cmd.contactPerson.kota_perusahaan">
					<input type="hidden" name="${status.expression }" id="${status.expression }" value="${status.value }"/>
				</spring:bind>
				</th>
			</tr>
				<tr>
				<th>No Telp Perusahaan</th>
				<th>
				<spring:bind path="cmd.pembayarPremi.area_code_perusahaan">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					</spring:bind>
				<spring:bind path="cmd.pembayarPremi.telp_perusahaan">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="15" maxlength="15"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>No Faksimili</th>
				<th><spring:bind path="cmd.pembayarPremi.area_code_fax_perusahaan">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.pembayarPremi.faks_perusahaan">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="15" maxlength="15"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind>
			</tr>
			<tr>
				<th>4.</th>
				<th>Bidang Usaha</th>
				<th><select name="pembayarPremi.bidang_usaha_pembayar_premi" >
					<c:forEach var="industri" items="${select_bidang}">
						<option
							<c:if test="${cmd.pembayarPremi.bidang_usaha_pembayar_premi eq industri.ID}"> SELECTED </c:if>
							value="${industri.ID}">${industri.BIDANG}</option>
					</c:forEach>
				</select>Lainnya,<spring:bind path="cmd.pembayarPremi.bidang_usaha_premi_lainnya">
						<input type="text" name="${status.expression}"
							value="${status.value }" size="32" maxlength="40"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
					</spring:bind></th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>5.</th>
				<th>Apakah Anda memiliki pekerjaan/usaha/bisnis lain  <br> diluar pekerjaan utama ?</th>
				<th><spring:bind path="cmd.pembayarPremi.mkl_kerja_other_radio">
					<label for="ya"> <input type="radio" class=noBorder
						name="${status.expression}" value="1"
						<c:if test="${cmd.pembayarPremi.mkl_kerja_other_radio eq 1 }"> 
									checked</c:if>
						id="ya">Ya </label>
					<label for="tidak"> <input type="radio" class=noBorder
						name="${status.expression}" value="0"
						<c:if test="${cmd.pembayarPremi.mkl_kerja_other_radio eq 0 or cmd.pembayarPremi.mkl_kerja_other_radio eq null}"> 
									checked</c:if>
						id="tidak">Tidak <font color="#CC3300"></font>
				</spring:bind> </label> 
				<br>
				Sebutkan jika ya
					<spring:bind path="cmd.pembayarPremi.mkl_kerja_other">
						<input type="text" name="${status.expression}"
							value="${status.value }" size="35" maxlength="30"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
					</spring:bind>
				</th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>6.</th>
				<th>Penghasilan dan sumber penghasilan Calon Pembayar Premi
				<span class="info"><br>(Dalam Rupiah Menggunakan Kurs Tengah BI)</span></th>
				<th></th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>A.</th>
				<th>Sumber pendapatan rutin per bulan</th>
				<th><spring:bind path="cmd.pembayarPremi.bulan_gaji">
					<input type="checkbox" name="bulan_gaji" class="noBorder" value="${cmd.pembayarPremi.bulan_gaji}"  size="30" onClick="b_gaji();"
						<c:if test="${status.value eq 'GAJI'}">checked="checked"</c:if>> 
					<input type="hidden" name="${status.expression}" value="${cmd.pembayarPremi.bulan_gaji}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Gaji
					<spring:bind path="cmd.pembayarPremi.bulan_penghasilan"> 
					<input type="checkbox" name="bulan_penghasilan" class="noBorder"  value="${cmd.pembayarPremi.bulan_penghasilan}"  size="30" onClick="b_penghasilan();"
		            		<c:if test="${status.value eq 'PENGHASILAN SUAMI/ISTRI'}">checked="checked"</c:if>> 
		             <input type="hidden" name="${status.expression}" value="${cmd.pembayarPremi.bulan_penghasilan}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind> Penghasilan Suami/Istri
					<spring:bind path="cmd.pembayarPremi.bulan_orangtua"> 
		            <input type="checkbox" name="bulan_orangtua" class="noBorder"  value="${cmd.pembayarPremi.bulan_orangtua}"  size="30" onClick="b_orangtua();"
					  		<c:if test="${status.value eq 'ORANG TUA'}">checked="checked"</c:if>> 
		             <input type="hidden" name="${status.expression}" value="${cmd.pembayarPremi.bulan_orangtua}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind> Orang Tua
					<spring:bind path="cmd.pembayarPremi.bulan_laba"> 
		            <input type="checkbox" name="bulan_laba" class="noBorder"  value="${cmd.pembayarPremi.bulan_laba}"  size="30" onClick="b_laba();" 
							 <c:if test="${status.value eq 'LABA PERUSAHAAN'}">checked="checked"</c:if>> 
					<input type="hidden" name="${status.expression}" value="${cmd.pembayarPremi.bulan_laba}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Laba Perusahaan
					<br>
					<spring:bind path="cmd.pembayarPremi.bulan_usaha"> 
		            <input type="checkbox" name="bulan_usaha" class="noBorder"  value="${cmd.pembayarPremi.bulan_usaha}"  size="30" onClick="b_usaha();"
									 <c:if test="${status.value eq 'HASIL USAHA'}">checked="checked"</c:if>> 
					<input type="hidden" name="${status.expression}" value="${cmd.pembayarPremi.bulan_usaha}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind> Hasil Usaha,<span class="info">sebutkan</span>
					<spring:bind path="cmd.pembayarPremi.bulan_usaha_note">
						<input type="text" name="${status.expression}"
							value="${status.value }" size="32" maxlength="40"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
					</spring:bind>
					<br>
					<spring:bind path="cmd.pembayarPremi.bulan_investasi"> 
		            <input type="checkbox" name="bulan_investasi" class="noBorder"  value="${cmd.pembayarPremi.bulan_investasi}"  size="30" onClick="b_investasi();"
						 <c:if test="${status.value eq 'HASIL INVESTASI'}">checked="checked"</c:if>> 
					<input type="hidden" name="${status.expression}" value="${cmd.pembayarPremi.bulan_investasi}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind> Hasil Investasi,<span class="info">sebutkan</span>
					<spring:bind path="cmd.pembayarPremi.bulan_investasi_note">
						<input type="text" name="${status.expression}"
							value="${status.value }" size="32" maxlength="40"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
					</spring:bind>
					<br>
					<spring:bind path="cmd.pembayarPremi.bulan_lainnya"> 
		            <input type="checkbox" name="bulan_lainnya" class="noBorder"  value="${cmd.pembayarPremi.bulan_lainnya}"  size="30" onClick="b_lainnya();"
							 <c:if test="${status.value eq 'LAINNYA'}">checked="checked"</c:if>> 
					<input type="hidden" name="${status.expression}" value="${cmd.pembayarPremi.bulan_lainnya}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind> Lainnya
					<spring:bind path="cmd.pembayarPremi.bulan_lainnya_note">
						<input type="text" name="${status.expression}"
							value="${status.value }" size="32" maxlength="40"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
					</spring:bind>
					</th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>B.</th>
				<th>Sumber pendapatan non rutin per tahun</th>
				<th><spring:bind path="cmd.pembayarPremi.tahun_bonus"> 
		            	<input type="checkbox" name="tahun_bonus" class="noBorder" value="${cmd.pembayarPremi.tahun_bonus}"  size="30" onClick="t_bonus();"
							 <c:if test="${status.value eq 'BONUS'}">checked="checked"</c:if>> 
						<input type="hidden" name="${status.expression}" value="${cmd.pembayarPremi.tahun_bonus}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Bonus
					<spring:bind path="cmd.pembayarPremi.tahun_komisi"> 
		           		<input type="checkbox" name="tahun_komisi" class="noBorder"  value="${cmd.pembayarPremi.tahun_komisi}"  size="30" onClick="t_komisi();"
							 <c:if test="${status.value eq 'KOMISI'}">checked="checked"</c:if>> 
						<input type="hidden" name="${status.expression}" value="${cmd.pembayarPremi.tahun_komisi}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Komisi
					<spring:bind path="cmd.pembayarPremi.tahun_aset"> 
		            	<input type="checkbox" name="tahun_aset" class="noBorder"  value="${cmd.pembayarPremi.tahun_aset}"  size="30" onClick="t_aset();"
							 <c:if test="${status.value eq 'PENJUALAN ASET'}">checked="checked"</c:if>> 
						<input type="hidden" name="${status.expression}" value="${cmd.pembayarPremi.tahun_aset}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Penjualan Aset
					<spring:bind path="cmd.pembayarPremi.tahun_hadiah"> 
		            	<input type="checkbox" name="tahun_hadiah" class="noBorder"  value="${cmd.pembayarPremi.tahun_hadiah}"  size="30" onClick="t_hadiah();"
							<c:if test="${status.value eq 'HADIAH/WARISAN'}">checked="checked"</c:if>> 
						<input type="hidden" name="${status.expression}" value="${cmd.pembayarPremi.tahun_hadiah}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Hadiah/Warisan
					<br>
					<spring:bind path="cmd.pembayarPremi.tahun_investasi"> 
		            	<input type="checkbox" name="tahun_investasi" class="noBorder"  value="${cmd.pembayarPremi.tahun_investasi}"  size="30" onClick="t_investasi();"
							 <c:if test="${status.value eq 'HASIL INVESTASI'}">checked="checked"</c:if>> 
						<input type="hidden" name="${status.expression}" value="${cmd.pembayarPremi.tahun_investasi}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Hasil Investasi
					<spring:bind path="cmd.pembayarPremi.tahun_investasi_note">
						<input type="text" name="${status.expression}"
							value="${status.value }" size="32" maxlength="40"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
					</spring:bind>
					<br>
					<spring:bind path="cmd.pembayarPremi.tahun_lainnya"> 
		        	<input type="checkbox" name="tahun_lainnya" class="noBorder"  value="${cmd.pembayarPremi.tahun_lainnya}"  size="30" onClick="t_lainnya();"
				    	<c:if test="${status.value eq 'LAINNYA'}">checked="checked"</c:if>> 
				    <input type="hidden" name="${status.expression}" value="${cmd.pembayarPremi.tahun_lainnya}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Lainnya
					<spring:bind path="cmd.pembayarPremi.tahun_lainnya_note">
						<input type="text" name="${status.expression}"
							value="${status.value }" size="32" maxlength="40"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
					</spring:bind></th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>C.</th>
				<th>Jumlah Total pendapatan rutin per bulan</th>
				<th><spring:bind path="cmd.pembayarPremi.total_rutin"><select name="${status.expression}">
							<c:forEach var="d" items="${select_rutin_bulan}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select></spring:bind></th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>D.</th>
				<th>Jumlah Total pendapatan non-rutin per tahun</th>
				<th><spring:bind path="cmd.pembayarPremi.total_non_rutin"><select name="${status.expression}">
							<c:forEach var="d" items="${select_non_tahun}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select></spring:bind></th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>7.</th>
				<th>Tujuan Pengajuan Asuransi<br>
				<span class="info"><br>(pilihan dapat lebih dari satu)</span></th>
				<th><spring:bind path="cmd.pembayarPremi.tujuan_proteksi"> 
		            	<input type="checkbox" name="tujuan_proteksi" class="noBorder" value="${cmd.pembayarPremi.tujuan_proteksi}"  size="30" onClick="proteksi();"
								<c:if test="${status.value eq 'PROTEKSI'}">checked="checked"</c:if>> 
						<input type="hidden" name="${status.expression}" value="${cmd.pembayarPremi.tujuan_proteksi}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Proteksi
					<spring:bind path="cmd.pembayarPremi.tujuan_pendidikan"> 
		            	<input type="checkbox" name="tujuan_pendidikan" class="noBorder"  value="${cmd.pembayarPremi.tujuan_pendidikan}"  size="30" onClick="pendidikan();"
								<c:if test="${status.value eq 'PENDIDIKAN'}">checked="checked"</c:if>> 
						<input type="hidden" name="${status.expression}" value="${cmd.pembayarPremi.tujuan_pendidikan}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Pendidikan
					<spring:bind path="cmd.pembayarPremi.tujuan_investasi"> 
		            	<input type="checkbox" name="tujuan_investasi" class="noBorder"  value="${cmd.pembayarPremi.tujuan_investasi}"  size="30" onClick="investasi();"
								<c:if test="${status.value eq 'INVESTASI'}">checked="checked"</c:if>> 
						<input type="hidden" name="${status.expression}" value="${cmd.pembayarPremi.tujuan_investasi}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Investasi
					<spring:bind path="cmd.pembayarPremi.tujuan_tabungan"> 
		            <input type="checkbox" name="tujuan_tabungan" class="noBorder"  value="${cmd.pembayarPremi.tujuan_tabungan}"  size="30" onClick="tabungan();"
								<c:if test="${status.value eq 'TABUNGAN'}">checked="checked"</c:if>> 
					<input type="hidden" name="${status.expression}" value="${cmd.pembayarPremi.tujuan_tabungan}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Tabungan
					<spring:bind path="cmd.pembayarPremi.tujuan_pensiun"> 
		            <input type="checkbox" name="tujuan_pensiun" class="noBorder"  value="${cmd.pembayarPremi.tujuan_pensiun}"  size="30" onClick="pensiun();"
								<c:if test="${status.value eq 'PENSIUN'}">checked="checked"</c:if>> 
					<input type="hidden" name="${status.expression}" value="${cmd.pembayarPremi.tujuan_pensiun}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Dana Pensiun
					<br>
					<spring:bind path="cmd.pembayarPremi.tujuan_lainnya"> 
		            <input type="checkbox" name="tujuan_lainnya" class="noBorder"  value="${cmd.pembayarPremi.tujuan_lainnya}"  size="30" onClick="lainnya();"
								<c:if test="${status.value eq 'LAINNYA'}">checked="checked"</c:if>> 
					<input type="hidden" name="${status.expression}" value="${cmd.pembayarPremi.tujuan_lainnya}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Lainnya
					<spring:bind path="cmd.pembayarPremi.tujuan_lainnya_note">
						<input type="text" name="${status.expression}"
							value="${status.value }" size="32" maxlength="40"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
					</spring:bind>
					</th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<tr>
				<th colspan=5 class="subtitle">DATA PIHAK KETIGA (Diisi apabila Calon Pembayar Premi bukan Calon Pemegang Polis)</th>
			</tr>
			<tr>
				<th>8.</th>
				<th>Apakah terdapat Pihak Ketiga yang :
				<br>(i)meminta Anda untuk mengajukan permintaan asuransi ini
				<br>(ii)menjadi pembayar premi; dan/atau
				<br>(iii)memiliki akses terhadap manfaat dari polis<span class="info"><br>Apabila jawaban "Ya" mohon menjawab pertanyaan ini:</span></th>
				<th><spring:bind path="cmd.pembayarPremi.ada_pihak_ketiga">
					<label for="ada"> <input type="radio" class=noBorder
						name="${status.expression}" value="1"
						<c:if test="${cmd.pembayarPremi.ada_pihak_ketiga eq 1}"> 
									checked</c:if>
						id="ada">Ya </label>
					<label for="belum"> <input type="radio" class=noBorder
						name="${status.expression}" value="0" 
						<c:if test="${cmd.pembayarPremi.ada_pihak_ketiga eq 0 or cmd.pembayarPremi.ada_pihak_ketiga eq null}"> 
									checked</c:if>
						id="belum">Tidak <font color="#CC3300"></font>
				</spring:bind> </label> 
				<br>
				Jenis Pihak Ketiga
				<spring:bind path="cmd.pembayarPremi.jenis_pihak_ketiga">
					<label for="individu"> <input type="radio" class=noBorder
						name="${status.expression}" value="1" onchange="disableData(this.value)" 
						<c:if test="${cmd.pembayarPremi.jenis_pihak_ketiga eq 1 or cmd.pembayarPremi.jenis_pihak_ketiga eq null}"> 
									checked</c:if>
						id="individu">Individu </label>
					<label for="badan"> <input type="radio" class=noBorder
						name="${status.expression}" value="0" onchange="disableData(this.value)"
						<c:if test="${cmd.pembayarPremi.jenis_pihak_ketiga eq 0}"> 
									checked</c:if>
						id="badan">Badan Hukum <font color="#CC3300"></font>
				</spring:bind> </label></th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>a.</th>
				<th>Nama Pihak Ketiga</th>
				<th>
					<spring:bind path="cmd.pembayarPremi.nama_pihak_ketiga">
						<input type="text" name="${status.expression}"
							value="${status.value }" size="35" maxlength="30"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
					</spring:bind>
				</th>
				<th></th>
				<th></th>
			</tr>
			<tr>
			  <th rowspan="3">b.</th>
				<th rowspan="3">Alamat</th>
				<th rowspan="3"><spring:bind path="cmd.pembayarPremi.alamat_lengkap">
					<textarea cols="40" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>${status.value }</textarea>
				</spring:bind></th>
				<th>Telpon Rumah</th>
				<th>
				<spring:bind path="cmd.pembayarPremi.area_code_3rd">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					</spring:bind>
				<spring:bind path="cmd.pembayarPremi.telp_rumah">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="15" maxlength="15"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
				<tr>
				<th>Telpon Kantor</th>
				<th>
				<spring:bind path="cmd.pembayarPremi.area_code_kantor_3rd">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					</spring:bind>
				<spring:bind path="cmd.pembayarPremi.telp_kantor">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="15" maxlength="15"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
				</tr>	
			<tr>
				<th>Alamat Email</th>
				<th><spring:bind path="cmd.pembayarPremi.email">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="25" maxlength="25"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>	
			</tr>
			<tr>
				<th></th>
				<th>Kewarganegaraan</th>
				<th><spring:bind path="cmd.pembayarPremi.kewarganegaraan">
						<select name="${status.expression}">
							<c:forEach var="d" items="${select_negara}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
						<font color="#CC3300"></font>
					</spring:bind></th>
				<th>Tanggal Pendirian<span class="info"><br>(jika berbentuk badan hukum)</span></th>
				<th><spring:bind path="cmd.pembayarPremi.mspe_date_birth_3rd_pendirian">
              			<script>inputDate('${status.expression}', '${status.value}', false);</script>
				</spring:bind></th></th>
			</tr>
			<tr>
				<th></th>
				<th></th>
				<th></th>
				<th>Tempat Kedudukan<span class="info"><br>(jika berbentuk badan hukum)</span></th>
				<th><spring:bind path="cmd.pembayarPremi.tempat_kedudukan">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="25" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>e.</th>
				<th>Tempat/Tgl/Lahir<span class="info"><br>(hanya jika individu)</span></th>
				<th><spring:bind path="cmd.pembayarPremi.tempat_lahir">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="15" maxlength="15"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>/
				</spring:bind>
				<spring:bind path="cmd.pembayarPremi.mspe_date_birth_3rd">
              			<script>inputDate('${status.expression}', '${status.value}', false);</script>
              	</spring:bind></th>
				<th>Bidang Usaha<span class="info"><br>(jika berbentuk badan hukum)</span></th>
				<th><select name="pembayarPremi.bidang_usaha_badan_hukum">
					<option value=""></option>
					<c:forEach var="l" items="${gelar}">
						<option
							<c:if test="${cmd.pembayarPremi.bidang_usaha_badan_hukum eq l.key}"> SELECTED </c:if>
							value="${l.key}">${l.value}</option>
					</c:forEach>
				</select></th>
			</tr>
			<tr>
				<th>f.</th>
				<th>Pekerjaan</th>
				<th colspan="3"><spring:bind path="cmd.pembayarPremi.pekerjaan">
						<select name="${status.expression}">
							<c:forEach var="d" items="${select_lst_pekerjaan}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
					</spring:bind></th>
			</tr>
			<tr>
				<th>G.</th>
				<th>Bidang / Jenis Pekerjaan</th>
				<th><spring:bind path="cmd.pembayarPremi.bidang_usaha_individu" >
					<select name="${status.expression}">
					<c:forEach var="industri" items="${select_bidang}">
						<option
							<c:if test="${cmd.pembayarPremi.bidang_usaha_individu eq industri.ID}"> SELECTED </c:if>
							value="${industri.ID}">${industri.BIDANG}</option>
					</c:forEach>
					</select></spring:bind>
				</select>Sebutkan Jika Lain-Lain<spring:bind path="cmd.pembayarPremi.bidang_usaha_individu_lain">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="35" maxlength="30"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>H.</th>
				<th>Jabatan/Pangkat/Golongan</th>
				<th><spring:bind path="cmd.pembayarPremi.jabatan">
					<select name="${status.expression}" 
						 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<c:forEach var="d" items="${select_jabatan}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
				</select>
				</spring:bind></th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>I.</th>
				<th>Instansi/Departemen</th>
				<th><spring:bind path="cmd.pembayarPremi.instansi">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="35" maxlength="30"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>J.</th>
				<th>No. NPWP</th>
				<th><spring:bind path="cmd.pembayarPremi.no_npwp">
						<input type="text" name="${status.expression}"
							value="${status.value }" size="35" maxlength="30"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
					</spring:bind></th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>K.</th>
				<th>Hubungan dengan Pemegang Polis</th>
				<th><spring:bind path="cmd.pembayarPremi.lsre_id_payer">
						<select name="${status.expression}">
							<c:forEach var="d" items="${select_pp}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
						<font color="#CC3300"></font>
					</spring:bind></th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>L.</th>
				<th>Sumber Dana</th>
				<th><spring:bind path="cmd.pembayarPremi.sumber_dana">
				<select name="${status.expression}">
					<c:forEach var="sumber" items="${select_sumber_dana}">
						<option
							<c:if test="${cmd.pembayarPremi.sumber_dana eq sumber.ID}"> SELECTED </c:if>
							value="${sumber.ID}">${sumber.DANA}</option>
					</c:forEach>
				</select></spring:bind>Sebutkan Jika Lain-Lain<spring:bind path="cmd.pembayarPremi.sumber_dana_lain">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="35" maxlength="30"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>M.</th>
				<th>Tujuan Penggunaan Dana</th>
				<th><spring:bind path="cmd.pembayarPremi.tujuan_dana">
				<select name="${status.expression}">
					<c:forEach var="dana" items="${select_tujuan_dana}">
						<option
							<c:if test="${cmd.pembayarPremi.tujuan_dana eq dana.ID}"> SELECTED </c:if>
							value="${dana.ID}">${dana.TUJUAN}</option>
					</c:forEach>
				</select></spring:bind>Sebutkan Jika Lain-Lain<spring:bind path="cmd.pembayarPremi.tujuan_dana_lain">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="35" maxlength="30"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
				<th></th>
				<th></th>
			</tr>
			<tr>
			    <th rowspan="2">9.</th>
				<th rowspan="2">Apabila tidak dapat memberikan informasi atas Pihak Ketiga,<br> mohon diberikan alasannya</th>
				<th rowspan="2"><spring:bind path="cmd.pembayarPremi.alasan">
					<textarea cols="40" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>${status.value }</textarea>
				</spring:bind></th>
					<th></th>
					<th></th>
				<tr>
					<th></th>
					<th></th>
				</tr>
			</tr>
			

			
			<c:if test="${cmd.datausulan.jenis_pemegang_polis == 1}">
				<!-- ada di "editpemegang_badanusaha.jsp" -->
			</c:if>
			<tr>
				<th class="subtitle" colspan="5"></th>
			</tr>		
            <tr> 
            <td colspan="9"> 
			 <p align="left"><b> <font face="Verdana" size="2" color="#CC3300">Note : * Wajib diisi</font></b></p>
			</td>
          </tr>	
          <c:if test="${cmd.tertanggung.lspd_id eq 2 or cmd.tertanggung.lspd_id eq 27 or cmd.tertanggung.lspd_id eq 209}">
          	<c:if test="${not empty cmd.pemegang.reg_spaj}">
	          <tr>          	 
	          	 <td colspan="2">
					 <input type="checkbox" name="edit_pemegang" id="edit_pemegang" class="noBorder" value ="${cmd.pemegang.edit_pemegang}" <c:if test="${cmd.pemegang.edit_pemegang eq 1}"> 
										checked</c:if> size="30" onClick="edit_onClick();"> Telah dilakukan edit
						<spring:bind path="cmd.pemegang.edit_pemegang"> 
			          		<input type="hidden" name="${status.expression}" value="${cmd.pemegang.edit_pemegang}"  size="30" style='background-color :#D4D4D4'readOnly>
						</spring:bind>	   
	          	 </td>
				 <td colspan="7">
				 		<spring:bind path="cmd.pemegang.kriteria_kesalahan">
							<select name="${status.expression}">
								<c:if test="${cmd.tertanggung.lspd_id ne 27}">
									<c:forEach var="a" items="${select_kriteria_kesalahan}">									
										<option
											<c:if test="${cmd.pemegang.kriteria_kesalahan eq a.KESALAHAN}"> SELECTED </c:if>
											value="${a.KESALAHAN}">${a.KESALAHAN}
										</option>									
									</c:forEach>
								</c:if>
								<c:if test="${cmd.tertanggung.lspd_id eq 27}">
									<c:forEach var="a" items="${select_kriteria_kesalahan}">
										<c:if test="${a.ID ne 3 and a.ID ne 2}">									
											<option
												<c:if test="${cmd.pemegang.kriteria_kesalahan eq a.KESALAHAN}"> SELECTED </c:if>
												value="${a.KESALAHAN}">${a.KESALAHAN}
											</option>
										</c:if>									
									</c:forEach>
								</c:if>
							</select>
						</spring:bind>
				 </td> 
	          </tr>
	          <tr>
	          	<td colspan="9">&nbsp;</td>
	          </tr>
          	</c:if>
          </c:if>
			<tr>
				<td colspan="5" style="text-align: center;">
				<input type="hidden" name="hal" value="${halaman}">
				<input type="hidden" name="_page" value="${halaman}">
				<spring:bind path="cmd.pemegang.indeks_halaman">
					<input type="hidden" name="${status.expression}"
						value="${halaman-1}" size="20">
				</spring:bind>
				<input type="submit" name="_target${halaman-1}" value="Prev &laquo;" 
					accesskey="P" onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="submit" name="_target${halaman+1}" value="Next &raquo;" onClick="next()" 
					accesskey="N" onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
					 
				 </td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</form>
<ajax:autocomplete
				  source="pembayarPremi.kota_perusahaan"
				  target="pembayarPremi.kota_perusahaan"
				  baseUrl="${path}/servlet/autocomplete?s=pembayarPremi.kota_perusahaan&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_kantor"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />
<script type="text/javascript">	
	if(frmParam.elements['datausulan.jenis_pemegang_polis'].value != 1){
		agamaChange('${cmd.pemegang.lsag_id}');
	}
</script>	  
</body>
<%@ include file="/include/page/footer.jsp"%>