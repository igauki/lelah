<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<meta HTTP-EQUIV="Expires" CONTENT="-1">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path}/include/css/default.css" media="screen">
		<link href="${path}/include/image/eas.ico" rel="shortcut icon">
		<!--  -->
		<link REL="Stylesheet" TYPE="text/css" HREF="${path}/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path}/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path}/include/js/jscalendar/calendar-setup.js"></script>
		<!--  -->
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<!-- Ajax Related -->
		<script type="text/javascript" src="${path}/include/js/ajax.js"></script>
		<script type="text/javascript" src="${path}/dwr/interface/ajaxManager.js"></script>
		<script type="text/javascript" src="${path}/dwr/engine.js"></script>
		<script type="text/javascript" src="${path}/dwr/util.js"></script>
		<script type="text/javascript">
			hideLoadingMessage();

			function awal(){

				<c:forEach items="${cmd.daftarTopup}" var="t" varStatus="s">
					<c:if test="${t.flag_new eq 1}">
						enableItems(${s.index});
						//if(document.formpost.elements['edit${s.index}']) document.formpost.elements['edit${s.index}'].click();
					</c:if>				
				</c:forEach>						
			
			}

			function buka(elemen){
				if(document.formpost.elements[elemen]){
					document.formpost.elements[elemen].readOnly = false;
					document.formpost.elements[elemen].setAttribute('class', '');
					document.formpost.elements[elemen].setAttribute('className', '');
				}
			}
			
			function tutup(elemen){
				if(document.formpost.elements[elemen]){
					document.formpost.elements[elemen].readOnly = true;
					document.formpost.elements[elemen].setAttribute('class', 'readOnly');
					document.formpost.elements[elemen].setAttribute('className', 'readOnly');
				}
			}
			
			function enableItems(indeks){
			<c:if test="${sessionScope.currentUser.jn_bank ne 3}">
				buka('daftarTopup['+indeks+'].no_trx10'); //no Trx
			</c:if>
				buka('daftarTopup['+indeks+'].msl_premi'); //premi
				buka('daftarTopup['+indeks+'].userotor');
				buka('daftarTopup['+indeks+'].passotor');
				enableDisableDate('daftarTopup['+indeks+'].msl_bdate'); //begdate
				document.formpost.elements['daftarTopup['+indeks+'].msl_mgi'].disabled = false; //mgi
				document.formpost.elements['daftarTopup['+indeks+'].msl_ro'].disabled = false; //jenis rollover
				if(document.formpost.elements['edit'+indeks]) document.formpost.elements['edit'+indeks].disabled = true;
				if(document.formpost.elements['bataledit'+indeks])document.formpost.elements['bataledit'+indeks].disabled = false;
				document.getElementById('bulanan1_'+indeks).disabled = false;
				if(document.getElementById('bulanan2_'+indeks))document.getElementById('bulanan2_'+indeks).disabled = false;
			}
			
			function disableItems(indeks){
				<c:if test="${sessionScope.currentUser.jn_bank ne 3}">
					buka('daftarTopup['+indeks+'].no_trx10'); //no Trx
				</c:if>
				tutup('daftarTopup['+indeks+'].msl_premi'); //premi
				tutup('daftarTopup['+indeks+'].userotor');
				tutup('daftarTopup['+indeks+'].passotor');
				enableDisableDate('daftarTopup['+indeks+'].msl_bdate'); //begdate
				document.formpost.elements['daftarTopup['+indeks+'].msl_mgi'].disabled = true; //mgi
				document.formpost.elements['daftarTopup['+indeks+'].msl_ro'].disabled = true; //jenis rollover
				if(document.formpost.elements['edit'+indeks]) document.formpost.elements['edit'+indeks].disabled = false;
				if(document.formpost.elements['bataledit'+indeks])document.formpost.elements['bataledit'+indeks].disabled = true;
				document.getElementById('bulanan1_'+indeks).disabled = true;
				if(document.getElementById('bulanan2_'+indeks))document.getElementById('bulanan2_'+indeks).disabled = true;
			}

			function konfirmasi(msl_no, lji_id, msl_tu_ke, mode_simpan, pesan){
				if(confirm(pesan)){
					document.formpost.elements['trans.simpan_msl_no'].value 	= msl_no; 
					document.formpost.elements['trans.simpan_lji_id'].value 	= lji_id;
					document.formpost.elements['trans.simpan_mode'].value 		= mode_simpan;
					document.formpost.elements['trans.simpan_msl_tu_ke'].value 		= msl_tu_ke;
					
					document.formpost.submit();
				}
			}
			
			function konfirmasi2(msl_no, lji_id, mode_simpan, pesan){
				if(confirm(pesan)){
					document.formpost.elements['trans.simpan_msl_no'].value 	= msl_no; 
					document.formpost.elements['trans.simpan_lji_id'].value 	= lji_id;
					document.formpost.elements['trans.simpan_mode'].value 		= mode_simpan;
					document.formpost.submit();
				}
			}
			
			
			
		</script>
	</head>
	<body onload="setupPanes('container1','tab1'); awal();" style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Otorisasi Topup</a>
				</li>
			</ul>

			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form:form id="formpost" name="formpost" commandName="cmd" cssStyle="text-align: left;">
						<fieldset>
							<legend>Data Polis</legend>
							<table class="entry2">
								<tr>
									<th nowrap="nowrap">No Polis / No Reg SPAJ:</th>
									<td colspan="5">
										<form:input path="reg_spaj" cssErrorClass="inpError"/> <input type="submit" name="show" value="Show">
										
										<form:hidden path="trans.simpan_msl_no"/>
										<form:hidden path="trans.simpan_lji_id"/>
										<form:hidden path="trans.simpan_mode"/>
										<form:hidden path="trans.simpan_msl_tu_ke"/>
										
									</td>
								</tr>
								<tr>
									<th nowrap="nowrap">Nama PP:</th>
									<td nowrap="nowrap">
										<form:input path="trans.mcl_first" readonly="true" cssClass="readOnly" size="40"/>
									</td>
									<th nowrap="nowrap">Produk:</th>
									<td nowrap="nowrap">
										<form:input path="trans.lsdbs_name" readonly="true" cssClass="readOnly" size="40"/>
									</td>
									<th nowrap="nowrap">Periode:</th>
									<td nowrap="nowrap">
										<form:input path="trans.mste_beg_date" readonly="true" cssClass="readOnly" size="12" />
										 s/d 
										<form:input path="trans.mste_end_date" readonly="true" cssClass="readOnly" size="12"/>
									</td>
								</tr>
								<tr>
									<th nowrap="nowrap">UP:</th>
									<td nowrap="nowrap">
										<input type="text" readonly="true" class="readOnly" size="5" value="${cmd.trans.lku_symbol}">
										<form:input path="trans.mspr_tsi" readonly="true" cssClass="readOnly" size="32" cssStyle="text-align: right;"/>
									</td>
									<th nowrap="nowrap">Premi:</th>
									<td nowrap="nowrap">
										<input type="text" readonly="true" class="readOnly" size="5" value="${cmd.trans.lku_symbol}">
										<form:input path="trans.mspr_premium" readonly="true" cssClass="readOnly" size="32" cssStyle="text-align: right;"/>
									</td>
									<th nowrap="nowrap"></th>
									<td nowrap="nowrap">
									</td>
								</tr>
								<tr>
									<td colspan="5">
										<spring:hasBindErrors name="cmd">
											<div id="error">
												<form:errors path="*" delimiter="<br>" />
											</div>
										</spring:hasBindErrors>
										
									</td>
								</tr>
							</table>
						</fieldset>
						<c:forEach items="${cmd.daftarTopup}" var="t" varStatus="s">
							<c:choose>
								<c:when test="${t.flag_new eq 0}">
									<fieldset>
										<legend>Transaksi Topup Ke-${t.msl_tu_ke}</legend>
								</c:when>
								<c:otherwise>
									<fieldset style="border-color: red;">
										<legend>Transaksi Topup Baru</legend>
								</c:otherwise>
							</c:choose>
							<c:if test='${cmd.daftarTopup[s.index].posisi eq 1}'>
									<script type="text/javascript">
										var result = (alert('${cmd.warning} Mau dilanjutkan?  JIKA ANDA TETAP INGIN MENYIMPAN/TRANSFER PADA LAYAR BERIKUTNYA SILAHKAN CLICK KEMBALI TOMBOL SIMPAN/TRANSFER'));
												
										if(result) { document.getElementById("proses").value = 1; 
										}else{
											document.getElementById('infoFrame').src='${path }/bac/topup.htm?spaj='+spaj;
										}
												
									</script>
	              								
              				</c:if>
								<table class="entry2">
									<tr>
										<c:if test="${sessionScope.currentUser.jn_bank ne 3}">
											<th>No Transaksi BSM<font class="error">*</font></th>
											<td >
												<form:input path="daftarTopup[${s.index}].no_trx" readonly="true" size="22" maxlength="20" cssClass="readOnly"  cssErrorClass="inpError" />
											</td>
										</c:if>
										<th>
											Nama Produk
										</th>
										<td colspan="2">
											<input type="text" size="50" class="readOnly" value="${nama_produk} <c:if test="${t.flag_bulanan eq \"1\"}">MANFAAT BULANAN</c:if>">
										</td>
										<th colspan="1" align="left" style="text-align: left" >
											
										</th>
									</tr>
									<tr>
										<th nowrap="nowrap">Jenis Investasi:</th>
										<td nowrap="nowrap">
											<form:input path="daftarTopup[${s.index}].lji_invest" readonly="true" cssClass="readOnly" size="30"/>
										</td>
										<th nowrap="nowrap">Deskripsi:</th>
										<td nowrap="nowrap">
											<form:input path="daftarTopup[${s.index}].msl_desc" readonly="true" cssClass="readOnly" size="30"/>
										</td>
										<th nowrap="nowrap">Tgl Input:</th>
										<td nowrap="nowrap">
											<form:input path="daftarTopup[${s.index}].msl_trans_date" readonly="true" cssClass="readOnly" size="12"/>
										</td>
									</tr>
									<tr>
										<th nowrap="nowrap">No. Registrasi:</th>
										<td nowrap="nowrap">
											<form:input path="daftarTopup[${s.index}].no_reg" readonly="true" cssClass="readOnly" size="30"/>
										</td>
										<th nowrap="nowrap">Premi Ke:</th>
										<td nowrap="nowrap">
											<form:input path="daftarTopup[${s.index}].msl_no" readonly="true" cssClass="readOnly" size="5"/>
											Top Up Ke:
											<form:input path="daftarTopup[${s.index}].msl_tu_ke" readonly="true" cssClass="readOnly" size="5"/>
										</td>
										<th nowrap="nowrap">Jumlah Top Up: <font class="error">*</font></th>
										<td nowrap="nowrap">
											<form:input path="daftarTopup[${s.index}].lku_symbol" readonly="true" cssClass="readOnly" size="5"/>
											<form:input path="daftarTopup[${s.index}].msl_premi" readonly="true" cssClass="readOnly" size="30" cssErrorClass="inpError"/>
										</td>
									</tr>
									<tr>
										<th nowrap="nowrap">Rate:</th>
										<td nowrap="nowrap">
											<form:input path="daftarTopup[${s.index}].msl_rate" readonly="true" cssClass="readOnly" size="5"/> %
											<%--
											<label for="flag_rate_${s.index}">
												<form:checkbox path="daftarTopup[${s.index}].flag_rate" value="1" id="flag_rate_${s.index}" cssClass="noBorder" disabled="true"/> Special Rate
											</label>
											--%>
										</td>
										<th nowrap="nowrap">MTI: <font class="error">*</font></th>
										<td nowrap="nowrap">
											<form:select path="daftarTopup[${s.index}].msl_mgi" disabled="true" cssErrorClass="inpError">
												<form:option value="" label="-" />
												<form:options items="${daftarMti}" itemValue="key" itemLabel="value" />
											</form:select>
										</td>
										<th nowrap="nowrap">Bunga:</th>
										<td nowrap="nowrap">
											<form:input path="daftarTopup[${s.index}].msl_bunga" readonly="true" cssClass="readOnly" size="30"/>
											<form:input path="daftarTopup[${s.index}].msl_hari" readonly="true" cssClass="readOnly" size="5"/> Hari
										</td>
									</tr>
									<tr>
										<th nowrap="nowrap">Periode MTI: <font class="error">*</font></th>
										<td nowrap="nowrap">
											<spring:bind path="cmd.daftarTopup[${s.index}].msl_bdate">
												<script>inputDate('${status.expression}', '${status.value}', true);</script>
											</spring:bind>
											 s/d 
											<form:input path="daftarTopup[${s.index}].msl_edate" readonly="true" cssClass="readOnly" size="12"/>
										</td>
										<th nowrap="nowrap">Jenis Rollover: <font class="error">*</font></th>
										<td nowrap="nowrap">
											<form:select path="daftarTopup[${s.index}].msl_ro" disabled="true" cssErrorClass="inpError">
												<form:option value="" label="-" />
												<form:options items="${daftarRo}" itemValue="key" itemLabel="value" />
											</form:select>
										</td>
										<th nowrap="nowrap">Tgl NAB:</th>
										<td nowrap="nowrap">
											<form:input path="daftarTopup[${s.index}].msl_tgl_nab" readonly="true" cssClass="readOnly" size="12"/>
											NAB:
											<form:input path="daftarTopup[${s.index}].msl_nab" readonly="true" cssClass="readOnly" size="12"/>
										</td>
									</tr>
									<tr>
										<th nowrap="nowrap">Biaya Investasi (%):</th>
										<td nowrap="nowrap">
											<form:input path="daftarTopup[${s.index}].msl_bp_rate" readonly="true" cssClass="readOnly" size="5"/> %
										</td>
										<th nowrap="nowrap">Tgl Input:</th>
										<td nowrap="nowrap">
											<form:input path="daftarTopup[${s.index}].msl_input_date" readonly="true" cssClass="readOnly" size="12"/>
										</td>
										<th nowrap="nowrap">Jumlah Unit:</th>
										<td nowrap="nowrap">
											<form:input path="daftarTopup[${s.index}].msl_unit" readonly="true" cssClass="readOnly" size="15"/>
										</td>
									</tr>
									<tr>
										<th nowrap="nowrap">Jenis Produk:</th>
										<td nowrap="nowrap" colspan="5">
											<label for="bulanan1_${s.index}">
												<form:radiobutton id="bulanan1_${s.index}" disabled="true" path="daftarTopup[${s.index}].flag_bulanan" cssClass="noBorder" value="0"/> Bukan Manfaat Bulanan
											</label>
											<label for="bulanan2_${s.index}">
												<form:radiobutton id="bulanan2_${s.index}" disabled="true" path="daftarTopup[${s.index}].flag_bulanan" cssClass="noBorder" value="1"/> Manfaat Bulanan
											</label>
										</td>
									</tr>
									<tr>
										<td nowrap="nowrap" colspan="6">
											<fieldset>
												<input type="button" name="otorisasi${s.index}" value="Otorisasi" onclick="return konfirmasi2('${t.msl_no}', '${t.lji_id}', 'otorisasi', 'Anda yakin Untuk mengotorisasi Top Up Ke-${t.msl_tu_ke}?');">
												<c:choose>
												<c:when test="${t.flag_new eq 0}">
													<input type="button" name="edit${s.index}" value="Edit" onclick="enableItems(${s.index});">
													<input type="button" name="bataledit${s.index}" value="Batalkan Edit" onclick="disableItems(${s.index});" disabled="disabled">
													<input type="button" name="simpan${s.index}" value="Simpan Topup" onclick="return konfirmasi('${t.msl_no}', '${t.lji_id}','${t.msl_tu_ke}', 'update', 'Anda yakin menyimpan Perubahan untuk Top Up Ke-${t.msl_tu_ke}? Harap pastikan periode MTI sesuai dengan tanggal RK.');">
													<input type="button" name="batal${s.index}" value="Hapus Topup" onclick="return konfirmasi('${t.msl_no}', '${t.lji_id}', '${t.msl_tu_ke}', 'delete', 'Anda yakin menghapus Top Up Ke-${t.msl_tu_ke}?');">
												</c:when>
												<c:otherwise>
													<input type="button" name="simpan${s.index}" value="Simpan Topup" onclick="return konfirmasi('${t.msl_no}', '${t.lji_id}', '${t.msl_tu_ke}', 'insert', 'Anda yakin menyimpan Top Up Baru? Harap pastikan periode MTI sesuai dengan tanggal RK.');">
												</c:otherwise>
											</c:choose>
											</fieldset>
										</td>
									</tr>
								</table>
							</fieldset>
						</c:forEach>
					</form:form>
				</div>
				
			</div>
			
		</div>
	</body>
	<script>
		<c:if test="${not empty param.pesan}">
			alert('${param.pesan}');
		</c:if>
	</script>
</html>