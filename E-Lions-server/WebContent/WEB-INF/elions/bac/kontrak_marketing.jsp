<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-blue.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript">
	var contextPath = "${path}";
</script>
<script type="text/javascript" src="${path }/include/js/calendarDateInput.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
<script type="text/javascript" src="${path }/include/js/default.js"></script>
<script type="text/javascript" src="${path }/dwr/util.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript">

	function getLeader(key){
		var a = document.getElementById('distribusi').value;
		var no_reg = '${no_reg}';
		if(no_reg!=null && no_reg!=''){
			window.location = '${path}/report/agency.htm?window=main&cekagen=2&distribusi='+a+'&nama='+key.replace('&','%26')+'&no_reg='+no_reg;
		}else{
			window.location = '${path}/report/agency.htm?window=main&cekagen=2&distribusi='+a+'&nama='+key.replace('&','%26');
		}
	}
	
	function cekLevel(){
	var a = document.getElementById("level").value;
	
	    if (a == "AGENCY DIRECTOR (Inkubator)" || a == "AGENCY DIRECTOR (Kantor Mandiri)") {
// 	        document.getElementById("leader2").disabled=true;	 
// 	        document.getElementById("leader2").value="PARLIN SIAHAAN";	       
	    } else {
	        document.getElementById("leader2").disabled=false;
	        document.getElementById("leader2").value="";	
	    }
	}	

	function cekAgen() {
	distribusi = document.getElementById('distribusi').value;
	window.location='${path}/report/agency.htm?window=main&cekagen=1&distribusi='+distribusi;
	}
	
	hideLoadingMessage();
	/*
	function viewReport(no){
		if(no==0){ //Summary Biasa
			window.location = '${path}/report/agency.htm?window=kontrak_agency';
		}else if(no==1){ //Summary Recurring
			window.location = '${path}/report/agency.htm?window=kontrak_hybrid';
		}else if(no==2){ //Summary Per Plan
			window.location = '${path}/report/agency.htm?window=kontrak_regional';
		}
	}*/
	
	function autofill(){
		
		var data = '${data}';
		
		
		if(data!=null && data!=''){
			
			var distribusi = '${reg_distribusi}';
			if(distribusi!='' && distribusi!=null){
				ubahStatus(distribusi);
			}
			
			document.getElementById("vp").value = '${reg_nama_atasan}';
			document.getElementById("namaAgent").value = '${reg_nama}';
			document.getElementById("ktp").value = '${reg_ktp}';
			document.getElementById("tlh").value = '${reg_tempat_lahir}';
			document.getElementById("_tglLahir").value = '${reg_tgl_lahir}';
			document.getElementById("tglLahir").value = '${reg_tgl_lahir}';
			document.getElementById("alamat").value = '${reg_alamat}';
			document.getElementById("cabang").value = '${reg_kota}';
			document.getElementById("_tglAwal").value = '${reg_tgl_kontrak}';
			document.getElementById("tglAwal").value = '${reg_tgl_kontrak}';
			
			//selected Regional
			var textToFind = '${reg_regional}';
			
			if(textToFind!='' && textToFind!=null){
				var dd = document.getElementById('jnsagen');
				for(var i = 0; i<dd.options.length; i++){
				  if(dd.options[i].text==textToFind){
					dd.selectedIndex = i;
					break;
				  }
				}
			}
		}
	}

	function ubahStatus(value) {
		
		document.getElementById("idDiv").innerHTML = ""
		input = document.createElement("select");
		input.setAttribute("name","level");
		input.setAttribute("id", "level");
		input.setAttribute("onchange", "ilang(window.document.formpost.distribusi.value, value)");
		var opti1;
		var data;
		
		document.getElementById("jenisLeader").innerHTML = "Atasan Pusat";
		document.getElementById("reg").innerHTML = "Agency";
		document.getElementById("regDesc").innerHTML = "";
		//var distribusi = document.getElementById('distribusi').value;
				
		if(value == "AGENCY" || value == "NEW AGENCY") {
			data = new Array("","AGENCY DIRECTOR (Inkubator)","AGENCY DIRECTOR (Kantor Mandiri)","AGENCY MANAGER","SALES MANAGER","SALES EXECUTIVE");

			//document.getElementById("jenisLeader").innerHTML = "Atasan Pusat";	
			//document.getElementById('atasanCab').innerHTML = "Atasan Cabang";
			//document.getElementById('atasanCabDesc').innerHTML = '<input type="text" name="vp" size="50" onfocus="this.select();" cssErrorClass="errField">';	
			
			var no_reg = '${no_reg}';
			if(no_reg!=null && no_reg!=''){
				window.location = '${path}/report/agency.htm?window=main&cekagen=1&distribusi='+value+'&no_reg='+no_reg;
			}else{
				window.location = '${path}/report/agency.htm?window=main&cekagen=1&distribusi='+value;
			}
		}
			else if(value == "HYBRID(ARTHAMAS)" || value == "HYBRID(AJS)") {
			data = new Array("","REGIONAL MANAGER","DISTRICT MANAGER","BRANCH MANAGER","SALES MANAGER","FINANCIAL CONSULTANT");
			//document.getElementById("jenisLeader").innerHTML = "Perwakilan Perusahaan";	
			//document.getElementById('reg').innerHTML = "Hybrid";
			//document.getElementById("regDesc").innerHTML = '<input type="text" name="regional" size="50" onfocus="this.select();">';
			
			var no_reg = '${no_reg}';
			if(no_reg!=null && no_reg!=''){
				window.location = '${path}/report/agency.htm?window=main&cekagen=1&distribusi='+value+'&no_reg='+no_reg;
			}else{
				window.location = '${path}/report/agency.htm?window=main&cekagen=1&distribusi='+value;
			}
		}
			else if(value == "REGIONAL") {
			data = new Array("","REGIONAL MANAGER","SENIOR BRANCH MANAGER","BRANCH MANAGER","UNIT MANAGER","MARKETING EXECUTIVE");
			
			//document.getElementById("jenisLeader").innerHTML = "Atasan Pusat";
			//document.getElementById('atasanCab').innerHTML = "Atasan Cabang";
			//document.getElementById('atasanCabDesc').innerHTML = '<input type="text" name="vp" size="50" onfocus="this.select();" cssErrorClass="errField">';					
			//document.getElementById("reg").innerHTML = "Regional";
			//document.getElementById("regDesc").innerHTML = '<input type="text" name="regional" size="50" onfocus="this.select();">';
			
			var no_reg = '${no_reg}';
			if(no_reg!=null && no_reg!=''){
				window.location = '${path}/report/agency.htm?window=main&cekagen=1&distribusi='+value+'&no_reg='+no_reg;
			}else{
				window.location = '${path}/report/agency.htm?window=main&cekagen=1&distribusi='+value;
			}
		}
			else if(value == "CORPORATE") {
			data = new Array("","EMPLOYEE BENEFIT GENERAL MANAGER", "TEAM LEADER", "CORPORATE ACCOUNT MANAGER", "EMPLOYEE BENEFIT FINANCIAL CONSULTANT");
			document.getElementById('atasanCab').innerHTML = "Atasan Cabang";
			document.getElementById('atasanCabDesc').innerHTML = '<input type="text" name="vp" size="50" onfocus="this.select();" cssErrorClass="errField">';					
			document.getElementById("jenisLeader").innerHTML = "Atasan Pusat";
		}
			else if(value == "AGENCY ARTHAMAS") {
				data = new Array("","AGENCY DIRECTOR (Inkubator)","AGENCY DIRECTOR (Kantor Mandiri)","AGENCY MANAGER","SALES MANAGER","SALES EXECUTIVE");
				
				var no_reg = '${no_reg}';
				if(no_reg!=null && no_reg!=''){
					window.location = '${path}/report/agency.htm?window=main&cekagen=1&distribusi='+value+'&no_reg='+no_reg;
				}else{
					window.location = '${path}/report/agency.htm?window=main&cekagen=1&distribusi='+value;
				}
		}
			else if(value == "CORPORATE / WORKSITE"){
				data = new Array("", "EBAM", "EBFE");
				
				var no_reg = '${no_reg}';
				if(no_reg!=null && no_reg!=''){
					window.location = '${path}/report/agency.htm?window=main&cekagen=1&distribusi='+value+'&no_reg='+no_reg;
				}else{
					window.location = '${path}/report/agency.htm?window=main&cekagen=1&distribusi='+value;
				}
		} else	if(value == "AKM WORKSITE") {
			data = new Array("","AD","AM-BM","SM","SE");
			
			var no_reg = '${no_reg}';
			if(no_reg!=null && no_reg!=''){
				window.location = '${path}/report/agency.htm?window=main&cekagen=1&distribusi='+value+'&no_reg='+no_reg;
			}else{
				window.location = '${path}/report/agency.htm?window=main&cekagen=1&distribusi='+value;
			}
		}
		
		// buat option baru
		for(var isi in data) {
			opti1 = document.createElement("option");
			opti1.setAttribute("value",data[isi]);
			opti1.appendChild(document.createTextNode(data[isi]));
			input.appendChild(opti1);			
		}
		//document.getElementById("idDiv").appendChild(input);
		//document.getElementById("regDesc").appendChild(input);
	}
	
	function ilang(value1, value2) {
		alert(value1 + " " + value2);
		if((value1 == "AGENCY" && value2 == "AGENCY DIRECTOR") || (value1 == "HYBRID(ARTHAMAS)" && value2 == "REGIONAL DIRECTOR") || (value1 == "HYBRID(AJS)" && value2 == "REGIONAL DIRECTOR") || (value1 ==  "REGIONAL" && value2 == "REGIONAL MANAGER") || (value1 == "CORPORATE" && value2 == "EMPLOYEE BENEFIT GENERAL MANAGER") || (value1 == "AGENCY ARTHAMAS" && value2 == "AGENCY DIRECTOR") || (value1 == "CORPORATE / WORKSITE") || (value1 == "NEW AGENCY" && value2 == "AGENCY DIRECTOR")) {
			document.getElementById("jenisLeader").innerHTML = "";
			document.getElementById("leaderDesc").style.visibility = "hidden";
			document.getElementById("leaderDesc").style = "visibility:hidden";
		}
		else if((value1 == "AGENCY" && value2 != "AGENCY DIRECTOR") || (value1 == "AGENCY ARTHAMAS" && value2 != "AGENCY DIRECTOR") || (value1 == "NEW AGENCY" && value2 != "AGENCY DIRECTOR")) {
			document.getElementById("jenisLeader").innerHTML = "Agency Direktur";
			document.getElementById("leaderDesc").style.visibility = "visible";
		}
		else if((value1 == "HYBRID(ARTHAMAS)" && value2 == "REGIONAL DIRECTOR")) {
			document.getElementById("jenisLeader").innerHTML = "Regional Direktur";
			document.getElementById("leaderDesc").style.visibility = "visible";
		}
		else if((value1 == "HYBRID(AJS)" && value2 == "REGIONAL DIRECTOR")) {
			document.getElementById("jenisLeader").innerHTML = "Regional Direktur";
			document.getElementById("leaderDesc").style.visibility = "visible";
		}
		else if((value1 == "REGIONAL" && value2 != "REGIONAL MANAGER")) {
			document.getElementById("jenisLeader").innerHTML = "Regional Manager";
			document.getElementById("leaderDesc").style.visibility = "visible";
		}
		else if((value1 == "CORPORATE" && value2 == "EMPLOYEE BENEFIT GENERAL MANAGER")) {
			document.getElementById("jenisLeader").innerHTML = "General Manager";
			document.getElementById("leaderDesc").style.visibility = "visible";
		}
		else if(value1 == "CORPORATE / WORKSITE"){
		}
	}
	
</script>
</head>
<BODY onload="document.title='Perjanjian Keagenan'; setupPanes('container1', 'tab1'); autofill();" style="height: 100%;">
	
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Perjanjian Keagenan</a>
			</li>
		</ul>
	
		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form method="post" name="formpost">
					<table class="entry2">
						<tr>
							<th style="width: 135px;">Kode/Nama Agen</th>
							<td>
								<input type="text" name="cari" value="${cari}" size="50" onfocus="this.select();">
							</td>
						</tr>
						<tr>
							<th>Tgl Lahir</th>
							<td>
								<script>inputDate('birthdate', '${birthdate}', false, '', 9);</script>
							</td>
						</tr>
						<tr>
							<th></th>
							<td>
								<input type="submit" name="btnCari" value="Cari">
								<input type="submit" name="btnInput" value="Baru">
							</td>
						</tr>
						<c:if test="${not empty errors}">
							<tr>
								<th></th>
								<td>
									<div id="error" style="text-transform: none;">ERROR:<br>
										<c:forEach var="err" items="${errors}">
											- <c:out value="${err}" escapeXml="false" />
											<br />
											<script>
												pesan += '${err}\n';
											</script>
										</c:forEach>
									</div>
								</td>
							</tr>
						</c:if>
						<c:if test="${not empty daftarAgen}">
							<tr>
								<th>Pilih Agen:</th>
								<td>
									<input type="hidden" name="tampil">
									<table class="simple" style="width: auto;">
										<thead>
											<tr>
												<th style="text-align: center;">Jenis</th>
												<th style="text-align: center;">Distribusi</th>
												<th style="text-align: center;">Jabatan</th>
												<th style="text-align: center;">Kode</th>
												<th style="text-align: center;">Nama</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="agen" items="${daftarAgen}" varStatus="stat">
												<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;" onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
													onclick="document.formpost.tampil.value='${agen.MSAG_ID}';document.formpost.submit();">
													<td style="text-align: center;">${agen.JENIS}</td>
													<td style="text-align: center;">${agen.DISTRIBUSI}</td>
													<td style="text-align: center;">${agen.JABATAN}</td>
													<td style="text-align: center;">${agen.MSAG_ID}</td>
													<td style="text-align: center;">${agen.MCL_FIRST}</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</td>
							</tr>
						</c:if>
						<c:if test="${not empty dataAgen}">
							<tr>
								<th>Data Agen:</th>
								<td>
									<fieldset>
										<table class="entry2">
											<tr>
												<th>Line Business</th>
												<td>${dataAgen[0].LSTB_NAME}</td>
											</tr>
											<tr>
												<th>Nama</th>
												<td>[${dataAgen[0].MSAG_ID}] ${dataAgen[0].MCL_FIRST}</td>
											</tr>
											<tr>
												<th>Posisi</th>
												<td>[${dataAgen[0].DISTRIBUSI}] ${dataAgen[0].JABATAN}</td>
											</tr>
											<tr>
												<th>Susunan Agen</th>
												<td>
													<table class="simple" style="width: auto;">
														<thead>
															<tr>
																<th style="text-align: center;">Jabatan</th>
																<th style="text-align: center;">Kode</th>
																<th style="text-align: center;">Nama</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach var="agen" items="${dataAgen}" varStatus="stat">
																<tr>
																	<td style="text-align: center;">${agen.JABATAN}</td>
																	<td style="text-align: center;">${agen.MSAG_ID}</td><input type="text" id="msag_id" name="msag_id" value="${dataAgen[0].MSAG_ID}">
																	<td style="text-align: center;">${agen.MCL_FIRST}</td>
																</tr>
															</c:forEach>
														</tbody>
													</table>
												</td>
											</tr>
											<tr>
												<th>Jenis Agen</th>
												<td>${dataAgen[0].MSAG_JN_AGEN}</td>
											</tr>
											<tr>
												<th>Region</th>
												<td>[${dataAgen[0].REGION}] ${dataAgen[0].LSRG_NAMA}</td>
											</tr>
											<tr>
												<th>Tanggal Kontrak</th>
												<td><fmt:formatDate value="${dataAgen[0].MSAG_BEG_DATE}" pattern="dd-MM-yyyy"/> s/d <fmt:formatDate value="${dataAgen[0].MSAG_END_DATE}" pattern="dd-MM-yyyy"/></td>
											</tr>
											<tr>
												<th>Tanggal Berlaku</th>
												<td><fmt:formatDate value="${dataAgen[0].MAGH_PROCESS_DATE}" pattern="dd-MM-yyyy"/></td>
											</tr>
											<tr>
												<th>Catatan</th>
												<td>
													<c:if test="${ empty dataAgen[0].MAGH_DESC}">
														<c:set var="dataAgen[0].MAGH_DESC" value="Cetak Polis"/>
													</c:if>
													<textarea rows="3" cols="60" name="catatan">${dataAgen[0].MAGH_DESC}</textarea>
												</td>
											</tr>
											<tr>
												<th></th>
												<td>
													<input type="submit" name="btnSave" value="Cetak Kontrak">
												</td>
											</tr>
										</table>
									</fieldset>
								</td>
							</tr>
						</c:if>
						<c:if test="${not empty redirect}">
								<tr>
									<th>Distribusi</th>
									<td>
										<select name="distribusi" id="distribusi" onchange="ubahStatus(value);" >
											<option></option>
<!-- 											<option value="NEW AGENCY" id="distribusi8">NEW AGENCY</option>-->
											<option value="AGENCY" id="distribusi1">AGENCY</option>											
											<option value="HYBRID(AJS)" id="distribusi3">HYBRID(AJS)</option>
											<option value="REGIONAL" id="distribusi4">REGIONAL</option>
<!-- 											<option value="AGENCY ARTHAMAS" id="distribusi5">AGENCY ARTHAMAS</option> -->
											<option value ="CORPORATE / WORKSITE" id = "distribusi6">CORPORATE / WORKSITE</option>
											<option value="AKM WORKSITE" id="distribusi7">AKM WORKSITE / INKUBATOR</option>
										</select>
									</td>
								</tr>
								<tr>
									<th></th>
									<td>
										<fieldset>
											<legend>Data Agen</legend>
											<table class="entry2">
												
													<tr>
														<th><span id="reg">Agency</span></th>
														<td>
															
																<div id="regDesc">
																	<select id="jnsagen" name="jnsagen" style="WIDTH :59%; <c:if test="${distribusi eq 'CORPORATE / WORKSITE'}">background-color:#B1AFAF;</c:if>" onchange="getLeader(value);">
																		<option></option>
																		<c:forEach items="${lstAgen}" var="x" varStatus="xt">
																			<option value="${x.KEY}~${x.VALUE}">${x.VALUE}</option>
																		</c:forEach>
																	</select>
																</div>
														</td>
													</tr>
													<tr>
														<th><span id="atasanCab">Atasan Cabang(AD/RM)</span></th>
														<td>
															<span id="atasanCabDesc">
															<%-- <input style="border-color:#6893B0;<c:if test="${distribusi eq 'HYBRID(ARTHAMAS)' or distribusi eq 'AGENCY ARTHAMAS' or distribusi eq 'CORPORATE / WORKSITE'}">background-color:#B1AFAF;</c:if>" type="text" name="vp" id="vp" size="50" onfocus="this.select();" cssErrorClass="errField" value="${vp}" <c:if test="${distribusi eq 'HYBRID(ARTHAMAS)' or distribusi eq 'AGENCY ARTHAMAS' or distribusi eq 'CORPORATE / WORKSITE'}">disabled="disabled"</c:if> ></span> --%>
															<!-- Permintaan Ibu Anna untuk Agency Arthamas field ini dihilangkan readonlynya -->
															<input style="border-color:#6893B0;<c:if test="${distribusi eq 'HYBRID(ARTHAMAS)' or distribusi eq 'CORPORATE / WORKSITE'}">background-color:#B1AFAF;</c:if>" type="text" name="vp" id="vp" size="50" onfocus="this.select();" cssErrorClass="errField" value="${vp}" <c:if test="${distribusi eq 'HYBRID(ARTHAMAS)' or distribusi eq 'CORPORATE / WORKSITE'}">disabled="disabled"</c:if> ></span>
														</td>
													</tr>
													<tr>
														<th>Nama Agen</th>
														<td><input style="border-color:#6893B0;" type="text" name="namaAgent" id="namaAgent" size="50" onfocus="this.select();" cssErrorClass="errField" value="${namaAgent}"></td>
													</tr>
													<tr>
														<th>Level</th>
														<td>
															<div id="idDiv">
																<select id="level" name="level" onChange="cekLevel();" style="width: 59%;" >
																	<c:forEach items="${lstLevel}" var="x" varStatus="xt">
																		<option value="${x.value}" <c:if test="${x.value eq maclevel}">selected="selected"</c:if>>${x.value}</option>		
																	</c:forEach>
																</select>
															</div>
														</td>
													</tr>
													<tr>
														<th>No Identitas (KTP)</th>
														<td><input style="border-color:#6893B0;" type="text" name="ktp" id="ktp" size="50" onfocus="this.select();" cssErrorClass="errField" value="${ktp}"></td>
													</tr>
													<tr>
														<th>Tempat / Tgl Lahir</th>
														<td>
															<input style="border-color:#6893B0;" type="text" id="tlh" name="tlh" size="20" onfocus="this.select();" cssErrorClass="errField" value="${tlh}">
															<script>inputDate('tglLahir', '${tglLahir}', false, '', 9);</script>
														</td>
													</tr>
													<tr>
														<th>Alamat</th>
<!--														<td><input style="border-color:#6893B0;" type="text" value="${alamat}" name="alamat" size="50" onfocus="this.select();" cssErrorClass="errField"></td>-->
														<td><textarea cols="40" rows="7" id="alamat" name="alamat" value="${alamat}" onfocus="this.select();" cssErrorClass="errField" style="border-color:#6893B0;"></textarea></td>
													</tr>
													<tr>
														<th>Tanggal Kontrak</th>
														<td>
															<script>inputDate('tglAwal', '${tglAwal}', false, '', 9);</script>
														</td>
													</tr>
													<tr>
														<th>Lokasi Cabang (Kota)</th>
														<td><input <c:if test="${distribusi eq 'CORPORATE / WORKSITE'}">disabled="disabled"</c:if>
															style="border-color:#6893B0; <c:if test="${distribusi eq 'CORPORATE / WORKSITE'}">background-color:#B1AFAF;</c:if>" type="text" value="${kota_cab}" name="cabang" id="cabang" size="50" onfocus="this.select();" cssErrorClass="errField"></td>
													</tr>
													<tr>
														<th><span id="jenisLeader2">Atasan Pusat</span></th>
														<td><span id="leaderDesc2">
															<input <c:if test="${distribusi eq 'HYBRID(ARTHAMAS)' or distribusi eq 'AGENCY ARTHAMAS'}">disabled="disabled"</c:if>
																style="border-color:#6893B0;<c:if test="${distribusi eq 'HYBRID(ARTHAMAS)' or distribusi eq 'AGENCY ARTHAMAS'}">background-color:#B1AFAF;</c:if>" type="text" id="leader2" value="${leader2}" name="leader2" size="50" onfocus="this.select();" cssErrorClass="errField"></span></td>
													</tr>
													<tr>
														<th>Jabatan Atasan Pusat</th>
														<td>
															<select name="jabatanpusat" id="jabatanpusat">
																<option></option>
																<option value="CHIEF AGENCY OFFICER" id="jabatanpusat1" <c:if test="${jabatanpusat eq \"CHIEF AGENCY OFFICER\"}">selected="selected"</c:if>>Chief Agency Officer</option>
																<option value="DIREKTUR" id="jabatanpusat2" <c:if test="${jabatanpusat eq \"DIREKTUR\"}">selected="selected"</c:if>>Direktur</option>
																<option value="REGIONAL SALES HEAD" id="jabatanpusat3" <c:if test="${jabatanpusat eq \"REGIONAL SALES HEAD\"}">selected="selected"</c:if>>Regional Sales Head</option>
																<option value="REGIONAL AGENCY DEVELOPMENT" id="jabatanpusat4" <c:if test="${jabatanpusat eq \"REGIONAL AGENCY DEVELOPMENT\"}">selected="selected"</c:if>>Regional Agency Development</option>
																<option value="SHARIA DIVISION HEAD" id="jabatanpusat5" <c:if test="${jabatanpusat eq \"SHARIA DIVISION HEAD\"}">selected="selected"</c:if>>Sharia Division Head</option>
															</select>
														</td>
													</tr>
													<tr>
														<th>No. NPWP</th>
														<td><input <c:if test="${distribusi ne 'CORPORATE / WORKSITE'}">disabled="disabled"</c:if>
														style="border-color:#6893B0; <c:if test="${distribusi ne 'CORPORATE / WORKSITE'}">background-color:#B1AFAF;</c:if>" type="text" name="npwp" id="npwp" size="50" onfocus="this.select();" cssErrorClass="errField" value="${npwp}"></td>
													</tr>
													<tr>
														<th>Status pajak per 1 Januari</th>
														<td><input <c:if test="${distribusi ne 'CORPORATE / WORKSITE'}">disabled="disabled"</c:if> 
														style="border-color:#6893B0; <c:if test="${distribusi ne 'CORPORATE / WORKSITE'}">background-color:#B1AFAF;</c:if>" type="text" name="pajak" id="pajak" size="50" onfocus="this.select();" cssErrorClass="errField" value="${pajak}"></td>
													</tr>
													<tr>
														<th></th>
														<td><input type="submit" name="btnSaveBaru" value="View Kontrak" cssErrorClass="errField"></td>
													</tr>
													<tr style="visibility: hidden;">
														<th><span id="jenisLeader">Atasan Pusat</span></th>
														<td><span id="leaderDesc"><input type="hidden" disabled="disabled" value="${leader}" name="leader" size="50" onfocus="this.select();" cssErrorClass="errField">IJ.SOEGENG WIBOWO</span></td>
													</tr>
													<tr>
														<td>
															<span class="info">*Semua kolom harus diisi, Tidak boleh kosong</span>
														</td>
													</tr>	
													<tr>
														<td>
															<span class="info">*Apabila atasan cabang(AD/RM) tidak ada, silakan diinput dengan tanda -</span>
														</td>
													</tr>							
											</table>
										</fieldset>
									</td>
								</tr>
						</c:if>
					</table>
				</form>
			</div>
		</div>
	</div>  
</body>
<c:if test="${not empty distribusi}">
	<script>
		var a = '${distribusi}';
		var b = '${reg}';
		var c = '${jenisLeader}';
		var d = '${atasanCab}';
		var e = '${agency}';
		//alert(e);
		//alert(document.getElementById('jnsagen').value);
		document.getElementById('distribusi').value = a;
		document.getElementById('reg').innerHTML = b;
		document.getElementById("jenisLeader").innerHTML = c;	
		document.getElementById('atasanCab').innerHTML = d;
		//document.getElementById('jnsagen').value = e;
		
		for(a=0;a<document.getElementById('jnsagen').length;a++) {
					if(document.getElementById('jnsagen')[a].value == e || document.getElementById('jnsagen')[a].innerHTML == e ) {
						document.getElementById('jnsagen')[a].selected = 'selected';
						break;
					}	
		}
		
	</script>
</c:if>


<%@ include file="/include/page/footer.jsp"%>