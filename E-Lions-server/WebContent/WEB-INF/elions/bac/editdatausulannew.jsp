<%@ include file="/include/page/header.jsp"%><c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path }/include/js/refund/refund_common.js"></script>
<script type="text/javascript" src="${path }/include/js/bac/datausulan.js"></script>

<script language="JavaScript">
// BEBERAPA JAVASCRIPT DIPINDAHKAN KE DATAUSULAN.JS UNTUK MENGHINDARI ERROR EXCEEDING THE 65535 BYTES LIMIT.

<!--
<spring:bind path="cmd.datausulan.jmlrider"> 
var mn=${status.value};
</spring:bind>  
var flag_add=0;
var flag_add1=0;

<spring:bind path="cmd.datausulan.jml_peserta"> 
var pesertax=${status.value};
</spring:bind>
var flag_add_x=0;
var flag_add1_x=0;

var varOptionrider;
var varOptionins;
var varOptionhub;
var varOptionlsne;
var OptionUp;
var OptionJP;
var OptionProdx;

var v_path = '${path}';
var v_jn_bank = '${sessionScope.currentUser.jn_bank}';
	
	function Body_onload() {
		
		var nameSelection;
		xmlData.async = false;
		document.frmParam.elements['datausulan.mspr_class'].readOnly=true;
		document.frmParam.elements['datausulan.mspr_class'].style.backgroundColor ='#D4D4D4';
		xmlData.src = "${path}/xml/PLANRIDER.xml";
		generateXML_rider(xmlData, 'BISNIS_ID','NAME');
		xmlData.src = "${path}/xml/INSRIDER.xml";
		generateXML_insrider( xmlData, 'ID','INSRIDER');
		xmlData.src = "/E-Lions/xml/PERSENUP.xml";
		generateXML_persentaseUp( xmlData, 'ID','PERSEN');
		xmlData.src = "${path}/xml/RELATION.xml";
		generateXML_penerima( xmlData, 'ID','RELATION');
		xmlData.src = "${path}/xml/FLAG_JENIS_PESERTA.xml";
		generateXML_flagJenisPeserta( xmlData, 'ID', 'JENIS');
		xmlData.src = "${path}/xml/PROD_KESEHATAN.xml";
		generateXML_flagJenisProd( xmlData, 'ID', 'PRODUK');
		xmlData.src = "${path}/xml/WARGANEGARA.xml";
		generateXML_negara( xmlData, 'ID', 'NEGARA');
		
		document.frmParam.elements['datausulan.mspo_pay_period'].style.backgroundColor ='#D4D4D4';
		document.frmParam.elements['datausulan.mspr_ins_period'].style.backgroundColor ='#D4D4D4';
		document.frmParam.elements['datausulan.jmlrider'].value='${cmd.datausulan.jmlrider}';
		document.frmParam.elements['datausulan.jml_peserta'].value='${cmd.datausulan.jml_peserta}';
		document.frmParam.kode_sementara.value='${cmd.datausulan.plan}';
		//document.frmParam.elements['datausulan.mste_beg_date'].style.backgroundColor ='#D4D4D4';
		
		if(document.frmParam.elements['datausulan.mste_flag_el'].value==1){
			document.frmParam.mste_flag_el1.checked = true;
		}
		
		if (document.frmParam.kode_sementara.value!="")
		{
			ProductConfig1(document.frmParam.elements['datausulan.lsbs_id'].value,document.frmParam.kode_sementara.value,'${sessionScope.currentUser.jn_bank}');
// 			<c:if test="${sessionScope.currentUser.jn_bank eq 0 or sessionScope.currentUser.jn_bank eq 1}">
// 				ProductConfigPlatinumBII(155);
// 			</c:if>
//  			<c:if test="${sessionScope.currentUser.jn_bank eq 16}">
//  				ProductConfigBsim(182);
 //			</c:if>
// 			<c:if test="${sessionScope.currentUser.jn_bank eq 3}">
// 				ProductConfigSekuritas(document.frmParam.elements['datausulan.lsbs_id'].value,document.frmParam.kode_sementara.value);
// 			</c:if>
			<c:if test="${sessionScope.currentUser.jn_bank eq 4}">
				ProductConfigAdminMall(document.frmParam.elements['datausulan.lsbs_id'].value,document.frmParam.kode_sementara.value);
			</c:if>
// 			<c:if test="${sessionScope.currentUser.lus_id eq 2661}">
// 				ProductConfigOnline(116);
// 			</c:if>
			perproduk1(document.frmParam.kode_sementara.value);
			if (document.frmParam.status.value.toUpperCase()==("edit").toUpperCase())
			{
				cek_rider_include(document.frmParam.kode_sementara.value);
			}
		}else{

			<c:if test="${sessionScope.currentUser.jn_bank eq 0 or sessionScope.currentUser.jn_bank eq 1}">
				ProductConfigPlatinumBII(155);
			</c:if>
// 			<c:if test="${sessionScope.currentUser.jn_bank eq 16}">
// 				ProductConfigBsim(182);
// 			</c:if>
			<c:if test="${sessionScope.currentUser.jn_bank eq 3}">
				ProductConfigSekuritas(142);
			</c:if>
			<c:if test="${sessionScope.currentUser.jn_bank eq 4}">
				ProductConfigAdminMall(143);
			</c:if>
			<c:if test="${sessionScope.currentUser.lus_id eq 2661}">
				ProductConfigOnline(116);
			</c:if>
		}
		var lsbs_id =document.frmParam.elements['datausulan.lsbs_id'].value;
		if(lsbs_id==142 || lsbs_id==143 || lsbs_id==144){
			document.getElementById('flagbul1').disabled = true;
			document.getElementById('flagbul2').disabled = true;
		}
		
		if (document.frmParam.document.frmParam.tanda_premi.value==null || document.frmParam.document.frmParam.tanda_premi.value=="")
		{
			document.frmParam.elements['datausulan.cara_premi'].value = '0';
			document.frmParam.document.frmParam.tanda_premi.value = '0';
		}
		
		cpy(document.frmParam.tanda_premi.value);
		if(document.getElementById('pakett').value==6 || document.getElementById('pakett').value==39 || document.getElementById('pakett').value==40){
			document.getElementById("spaj_pakett").style.visibility="visible";
		}else{
			document.getElementById("spaj_pakett").style.visibility="hidden";
			document.frmParam.elements['datausulan.spaj_paket'].value="";
		}
		if(document.getElementById("datausulan.lsbs_id").value == 212){
			document.getElementById("datausulan.mspr_ins_period").readOnly=false;
			document.getElementById("datausulan.mspr_ins_period").style.backgroundColor ='#FFFFFF';
			document.getElementById("datausulan.mspo_pay_period").readOnly=false;
			document.getElementById("datausulan.mspo_pay_period").style.backgroundColor ='#FFFFFF';
		}
	}

 
 
function next()
{
	eval( " document.frmParam.elements['pemegang.indeks_halaman'].value = ((document.frmParam.hal.value)+1);");
	document.frmParam.mn.value=mn;
	document.frmParam.pesertax.value = pesertax;
	
	if (eval(mn) >0 )
	{
	//alert( document.frmParam.elements['ride.mspr_tt1'].value);
	
		for (var k =1;k<=mn;k++)
		{
		      
				eval(" document.frmParam.elements['ride.mspr_tt1"+k+"'].value = document.frmParam.elements['ride.mspr_tt"+k+"'].value;");
				eval(" document.frmParam.elements['ride.plan_riderx"+k+"'].value =document.frmParam.elements['ride.plan_rider"+k+"'].value;");
				eval(" document.frmParam.elements['ride.persenUpx"+k+"'].value =document.frmParam.elements['ride.persenUp"+k+"'].value;");   
			if(document.frmParam.elements['ride.plan_rider'+k].value.substring(0,6) == '813~X4' || document.frmParam.elements['ride.plan_rider'+k].value.substring(0,6) == '813~X5' || document.frmParam.elements['ride.plan_rider'+k].value.substring(0,6) == '818~X3' || document.frmParam.elements['ride.plan_rider'+k].value.substring(0,6) == '818~X4'){
				if(document.frmParam.elements['ride.plan_rider'+k].value.substring(0,6) == '818~X4'){
					alert('Untuk rider ke '+k+' : Rider yang anda pilih adalah TERM RIDER 4 SINGLE (pembayaran sekaligus).');
				}else if(document.frmParam.elements['ride.plan_rider'+k].value.substring(0,6) == '818~X3'){
					alert('Untuk rider ke '+k+' : Rider yang anda pilih adalah TERM RIDER 4 (pembayaran setiap periode rollover).');
				}else if(document.frmParam.elements['ride.plan_rider'+k].value.substring(0,6) == '813~X5'){
					alert('Untuk rider ke '+k+' : Rider yang anda pilih adalah CI RIDER 4 SINGLE (pembayaran sekaligus).');
				}else {
					alert('Untuk rider ke '+k+' : Rider yang anda pilih adalah CI RIDER 4 (pembayaran setiap periode rollover).');
				}
			}
		}
	
	}
	
	if(eval(pesertax) > 0){
	alert( document.frmParam.elements['ptx.flag_jenis_peserta_x'].value);
		for(var z=1; z<=pesertax; z++){
			eval(" document.frmParam.elements['ptx.flag_jenis_peserta"+z+"'].value = document.frmParam.elements['ptx.flag_jenis_peserta"+z+"'].value;");
			eval(" document.frmParam.elements['ptx.lsre_id"+z+"'].value = document.frmParam.elements['ptx.lsre_id"+z+"'].value;");
			eval(" document.frmParam.elements['ptx.lsbs_id_plus"+z+"'].value = document.frmParam.elements['ptx.lsbs_id_plus"+z+"'].value;");
		}
	}

	var lsbs;
	if(document.frmParam.elements['datausulan.lsbs_id']){
		lsbs = document.frmParam.elements['datausulan.lsbs_id'].value;
	}
	var lsdbs;
	if(document.frmParam.elements['datausulan.plan']){
		lsdbs = document.frmParam.elements['datausulan.plan'].value.substring(5,6);
	}
	var convert = document.frmParam.elements['datausulan.convert'].value;
	var flag_simpol = document.frmParam.elements['datausulan.flag_simpol'].value;
	if(convert == 1){
		alert('Polis ini adalah SURRENDER ENDORSEMENT. Anda tidak bisa merubah Tanggal Mulai Pertanggungan dan Tanggal Rollover MGI');
	}
	if(flag_simpol == 1){
		alert('Polis Simas Power Link. Silahkan cek dahulu apakah orang ini sudah mempunyai Polis Simas Power Link sebelumnya.');
	}
	
	var jn_bank = '${sessionScope.currentUser.jn_bank}';
	if((lsbs == '164' || lsbs == '174') && (jn_bank != '2' && jn_bank !='3')){
		alert('Polis Stable Link. Silahkan cek dahulu apakah orang ini sudah mempunyai Polis Stable Link sebelumnya.');
		popWin(
			'${path}/bac/multi.htm?window=cek_sama&lku_id='+document.frmParam.elements['datausulan.lku_id'].value+
			'&pp_dob=<fmt:formatDate value="${cmd.pemegang.mspe_date_birth}" pattern="yyyyMMdd"/>'+
			'&pp_name=${cmd.pemegang.mcl_first}' +
			'&tt_dob=<fmt:formatDate value="${cmd.tertanggung.mspe_date_birth}" pattern="yyyyMMdd"/>'+
			'&tt_name=${cmd.tertanggung.mcl_first}', 600, 800);
	}
	
	if(lsbs == '186' && lsdbs == '3'){
		alert('Untuk produk SSP PRO, rekening Sumber Dana dan rekening SSP wajib diisi!')
	}
	
	if(lsbs==53 || lsbs == 54 || lsbs == '131' || lsbs == '132' || lsbs == '148'||lsbs == '183' || lsbs == '189'){
	//if(lsbs==53 || lsbs == 54 || lsbs == '131' || lsbs == '132' || lsbs == '148'||lsbs == '183' || lsbs == '189' ||lsbs == '193'){
		alert('Untuk produk KESEHATAN, ahli waris tidak perlu diisi!')
	}
	
	cekecek();
	
} 	
	
	
// -->
</script>
<script language="JavaScript">
<!--
	if (!document.layers&&!document.all&&!document.getElementById)
		event="test";
		 
// -->
</script>

<body bgcolor="ffffff" onLoad="Body_onload();">

<XML ID=xmlData></XML> 
<XML ID=xmlData1></XML> 
<XML ID=xmlData2></XML> 

<form name="frmParam" method="post" >
<table class="entry2">
  <tr> 
    <td> 
 			<input type="submit" name="_target0" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/pp1.jpg);"
				accesskey="1" onmouseover="return overlib('Alt-1', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<c:choose><c:when test="${(sessionScope.currentUser.jn_bank eq '16')}">
			<input type="submit" name="_target1" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ttgbsim.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();"></c:when><c:otherwise>
			<input type="submit" name="_target1" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ttg1.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();"></c:otherwise></c:choose>
			<input type="submit" name="_target1" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ppremi1.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-7', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target2" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ddu2.jpg);"
				accesskey="3" onmouseover="return overlib('Alt-3', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target3" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/inv1.jpg);"
				accesskey="4" onmouseover="return overlib('Alt-4', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target4" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ag1.jpg);"
				accesskey="5" onmouseover="return overlib('Alt-5', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target5" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/kf1.jpg);"
				accesskey="6" onmouseover="return overlib('Alt-6', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" name="dummy2" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/sb1.jpg);">		
      </td>
  </tr>
   <tr>
   

    <td> 
 									<spring:bind path="cmd.*">
										<c:if test="${not empty status.errorMessages}">
											<div id="error">
											INFO:<br>
											<c:forEach var="error" items="${status.errorMessages}">
														- <c:out value="${error}" escapeXml="false" />
												<br />
											</c:forEach></div>
										</c:if>									
									</spring:bind>     
        <table class="entry">
          <tr> 
            <td colspan="8" style="text-align: center;"> <input type="submit" name="_target${halaman-1}" value="Prev &laquo;" onClick="prev()" 
				onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();"> 
              <input type="submit" name="_target${halaman+1}" value="Next &raquo;" onClick="next()"
				onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();"> 
              <input type="hidden" name="_page" value="${halaman}"> </td>
          </tr>
          <tr> 
            <th class="subtitle" colspan="8">DATA PERTANGGUNGAN DAN PREMI ASURANSI
              <input type="hidden" name="tanggal_lahir_pp" value ="<fmt:formatDate value='${cmd.pemegang.mspe_date_birth}' pattern="yyyyMMdd"/> 
              "> <input type="hidden" name="tanggal_lahir_ttg" value ="<fmt:formatDate value='${cmd.tertanggung.mspe_date_birth}' pattern="yyyyMMdd"/> 
              "> </th>
          </tr>
          <tr  > 
            <th class="subtitle2"></th>
            <th class="subtitle2" colspan="7">JENIS ASURANSI POKOK 
              <input type="hidden" name="status" value="${cmd.pemegang.status}"></th>
          </tr>
          <tr  > 
            <th  width="13"></th>
            <th  width="256"> <p align="left"> Produk </th>
            <th colspan="6"><spring:bind path="cmd.datausulan.lsbs_id"> 
            	<select name="${status.expression }" 
            	<c:choose>
            		<c:when test="${sessionScope.currentUser.jn_bank eq 0 or sessionScope.currentUser.jn_bank eq 1}">
        		      onChange="ProductConfigPlatinumBII(this.options[this.selectedIndex].value)";
            		</c:when>
            		<c:when test="${sessionScope.currentUser.jn_bank eq 2}">
        		      onChange="ProductConfigBankSinarmas(this.options[this.selectedIndex].value)";
            		</c:when>
            		<c:when test="${sessionScope.currentUser.jn_bank eq 3}">
        		      onChange="ProductConfigSekuritas(this.options[this.selectedIndex].value)";
            		</c:when>
            		<c:when test="${sessionScope.currentUser.jn_bank eq 4}">
        		      onChange="ProductConfigAdminMall(this.options[this.selectedIndex].value)";
            		</c:when>
            		<c:when test="${sessionScope.currentUser.lus_id eq 2661}">
        		      onChange="ProductConfigOnline(this.options[this.selectedIndex].value)";
            		</c:when>
            		<c:when test="${sessionScope.currentUser.jn_bank eq 16}">
        		      onChange="ProductConfigBsim(this.options[this.selectedIndex].value)";
            		</c:when>
            		<c:otherwise>
		              onChange="ProductConfig(this.options[this.selectedIndex].value)";
            		</c:otherwise>
            	</c:choose>
			  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <option value="0">NONE</option>
                <c:forEach var="utama" items="${listprodukutama}"> <option value="${utama.lsbs_id}" 
						 <c:if test="${utama.lsbs_id eq cmd.datausulan.lsbs_id}">selected</c:if> >${utama.lsbs_name}</option> 
                </c:forEach> 
              </select>
			  <font color="#CC3300">*</font>
              </spring:bind> <input type="hidden" name="kode_sementara"> <spring:bind path="cmd.datausulan.tipeproduk"> 
              Jenis Produk : 
              <select name="${status.expression }" 
			  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <option value="" /> <c:forEach var="tipe" items="${listtipeproduk}"> <option value="${tipe.lstp_id}"
						   
                <c:if test="${cmd.datausulan.tipeproduk eq tipe.lstp_id}">selected</c:if> >${tipe.lstp_produk}</option> 
                </c:forEach> 
              </select>
              <font color="#CC3300">*</font>  </spring:bind> Kode Produk : <spring:bind path="cmd.datausulan.kodeproduk"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="5"  onChange="Prod(this.value)";
			<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
               </spring:bind> 
            </th>
          </tr>
          <tr  > 
            <th > </th>
            <th >Nama Produk </th>
            <th colspan="2"><font color="#CC3300" style="position:absolute;">*</font><div id="sideMenu" style="padding-left:10px;"> </div></th>
            <th colspan="3">Klas
            </th>
            <th> <spring:bind path="cmd.datausulan.mspr_class"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="5"   maxlength="2"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
              <font color="#CC3300">*</font> </spring:bind> 
           	</th>
          </tr>
           <tr>	
           		<th></th>
           		 <th>SPAJ Non Face to Face</th>
           		 <th colspan="2">
           		 	<select name="datausulan.covid_flag">
				 					<option value="0">DEFAULT</option>
				 					<c:forEach var="data" items="${select_face_to_face}"> 
				 						<option value="${data.FLAG_ID}" <c:if test="${data.FLAG_ID eq cmd.datausulan.covid_flag}">selected</c:if> >
				 							${data.LABEL_DESCRIPTION}
				 						</option> 
					                </c:forEach> 
				 		</select>
				 				
				 				
           		 </th>
           		  <th colspan="3">
           		  </th>
           		  <th>
           		  </th> 
           </tr>
          <tr>
          	<th></th>
          	<!-- START - kerjain helpdesk [137730] tambahin pilihan campaign -->
          	<th>Campaign</th>
          	<th colspan="2">
          		<spring:bind path="cmd.datausulan.campradio">
          			<c:choose>
	            		<c:when test="${status.value eq \"1\"}">
				 			<!-- 1 -->
			       			<label for="camp1"><input id="camp1" type="radio" class="noBorder" name="datausulan.campradio" onclick="showCampaign(value)" value="1" checked="checked"> Yes</label>
			 				<label for="camp2"><input id="camp2" type="radio" class="noBorder" name="datausulan.campradio" onclick="showCampaign(value)" value="0"> No</label>
				 			<div id="camplist" style="display:block; position:absolute;">
				 				<select name="datausulan.campaign_id">
				 					<option value="0~X0">NONE</option>
				 					<c:forEach var="data" items="${select_campaignid}"> 
				 						<option value="${data.MCP_CAMPAIGN_ID}" <c:if test="${data.MCP_CAMPAIGN_ID eq cmd.datausulan.campaign_id}">selected</c:if> >
				 							${data.MCP_CAMPAIGN_NAME}
				 						</option> 
					                </c:forEach> 
				 				</select>
			 				</div>
			 			</c:when>
	            		<c:when test="${status.value eq \"0\"}">
		            		<!-- 2 -->
			       			<label for="camp1"><input id="camp1" type="radio" class="noBorder" name="datausulan.campradio" onclick="showCampaign(value)" value="1"> Yes</label>
			 				<label for="camp2"><input id="camp2" type="radio" class="noBorder" name="datausulan.campradio" onclick="showCampaign(value)" value="0" checked="checked"> No</label>
		            		<div id="camplist" style="display:none; position:absolute;">
		            			<select name="datausulan.campaign_id">
				 					<option value="0~X0">NONE</option>
				 					<c:forEach var="data" items="${select_campaignid}"> 
				 						<option value="${data.MCP_CAMPAIGN_ID}" <c:if test="${data.MCP_CAMPAIGN_ID eq cmd.datausulan.campaign_id}">selected</c:if> >
				 							${data.MCP_CAMPAIGN_NAME}
				 						</option> 
					                </c:forEach> 
				 				</select>
			 				</div>
	            		</c:when>
            			<c:otherwise>
            				<!-- 3 -->
			       			<label for="camp1"><input id="camp1" type="radio" class="noBorder" name="datausulan.campradio" onclick="showCampaign(value)" value="1"> Yes</label>
			 				<label for="camp2"><input id="camp2" type="radio" class="noBorder" name="datausulan.campradio" onclick="showCampaign(value)" value="0" checked="checked"> No</label>
          					<div id="camplist" style="display:none; position:absolute;"></div>
            			</c:otherwise>
            		</c:choose>
          		</spring:bind>
			</th>
          	<!-- END - kerjain helpdesk [137730] tambahin pilihan campaign -->
          	<th colspan="3">
				<div id="PilihBatch" style="display:none;">
            		<span><font color="#CC3300">*</font>Batching</span>
            	</div>
			</th>
          	<th>
          		<div id="PilihBatch2" style="display:none;">
            		<spring:bind path="cmd.datausulan.pilih_batch"> <!-- tambah pilihan batch nonbatch untuk simas primelink (rider save). helpdesk [128634/128295] //Chandra -->
		            	<c:choose>
		            		<c:when test="${status.value eq \"Batch\"}">
		            			<label for="pb1"><input id="pb1" type="radio" class="noBorder" name="datausulan.pilih_batch" value="Batch" checked="checked"> Yes</label>
            					<label for="pb2"><input id="pb2" type="radio" class="noBorder" name="datausulan.pilih_batch" value="NonBatch"> No</label>
		            		</c:when>
		            		<c:when test="${status.value eq \"NonBatch\"}">
		            			<label for="pb1"><input id="pb1" type="radio" class="noBorder" name="datausulan.pilih_batch" value="Batch"> Yes</label>
            					<label for="pb2"><input id="pb2" type="radio" class="noBorder" name="datausulan.pilih_batch" value="NonBatch" checked="checked"> No</label>
		            		</c:when>
	            			<c:otherwise>
		            			<label for="pb1"><input id="pb1" type="radio" class="noBorder" name="datausulan.pilih_batch" value="Batch" checked="checked"> Yes</label>
            					<label for="pb2"><input id="pb2" type="radio" class="noBorder" name="datausulan.pilih_batch" value="NonBatch"> No</label>
	            			</c:otherwise>
	            		</c:choose>
            		</spring:bind>
            	</div>
          	</th>
          </tr>
          <tr>
          	 <th > </th>
          	 <th>Paket</th>
             <th width="29"> <div id="paketan"> </div>
             </th>
             <th  width="400">
             	Masukkan No SPAJ/ No Polis(Khusus Program SimasPrima-Simpol)
             </th>
	         <th colspan="3" width="100">
	         	<div id="spaj_pakett" style="visibility: hidden;" > 
	         		<spring:bind path="cmd.datausulan.spaj_paket">
						<input type="text" name="${status.expression}"
							value="${status.value }" maxlength="50"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
					</spring:bind>
<!--             		  <input type="text" name="spaj_paket" value="${cmd.datausulan.spaj_paket}" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>> -->
            	</div>
             </th>
            
            <th >
<!--             	<spring:bind path="cmd.datausulan.flag_paket">  -->
<!-- 	              <select name="${status.expression }" <c:if test="${ not empty status.errorMessage}"> style='background-color :#FFE1FD'</c:if>> -->
<!-- 	                <c:forEach items="${listPaket}" var="x"> -->
<!-- 	                	<option value="${x.key}" <c:if test="${cmd.datausulan.flag_paket eq x.key}">selected</c:if> >${x.value}</option>  -->
<!-- 	                </c:forEach>  -->
<!-- 	              </select> -->
<!--               	</spring:bind> -->
              	<input type ="hidden" id="pakett" name="pakett" value="${cmd.datausulan.flag_paket}"
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> 
            </th>
          </tr>
          <tr > 
            <th></th>
            <th>Khusus Stable Link / Stable Save</th>
            <th>
				<spring:bind path="cmd.powersave.flag_bulanan">
	            	<c:choose>
	            		<c:when test="${status.value ne 1}">
			            	<label for="flagbul1"><input id="flagbul1" name="${status.expression}" type="radio" class="noBorder" value="0" checked="checked">Biasa</label><br>
			            	<label for="flagbul2"><input id="flagbul2" name="${status.expression}" type="radio" class="noBorder" value="1">Manfaat Bulanan</label>
	            		</c:when>
	            		<c:otherwise>
			            	<label for="flagbul1"><input id="flagbul1" name="${status.expression}" type="radio" class="noBorder" value="0">Biasa</label><br>
			            	<label for="flagbul2"><input id="flagbul2" name="${status.expression}" type="radio" class="noBorder" value="1" checked="checked">Manfaat Bulanan</label>
	            		</c:otherwise>
	            	</c:choose>
	            </spring:bind>            
            </th>
            <th>
            	Spesial Case BP rate
				<spring:bind path="cmd.powersave.flag_special">
					<input disabled="disabled" type="checkbox" name="flag_special" class="noborder" value="0" onclick="spesial_onClick(this.checked);" 
            			<c:if test="${status.value eq 1}">checked="checked"</c:if>> 
					<input type="hidden" id="special" name="${status.expression}" value="${status.value}">
            	</spring:bind>
            	<br>
            	<input type="checkbox" name="mste_flag_el1" class="noBorder" 
						value="${cmd.datausulan.mste_flag_el}"  size="30" onClick="karyawan_onClick();"
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> 
	              <spring:bind path="cmd.datausulan.mste_flag_el"> 
		              <input type="hidden" name="${status.expression}"
								value="${cmd.datausulan.mste_flag_el}"  size="30" style='background-color :#D4D4D4'readOnly>
		              <b><font face="Verdana" size="1" color="#996600">Karyawan AJ Sinarmas</font></b>
	              </spring:bind> 
            </th>
            <th>Masa Pembayaran</th>
            <th colspan="3">
            	<input type="hidden" name="pay" value ="${cmd.datausulan.mspo_pay_period}"> 
				<spring:bind path="cmd.datausulan.mspo_pay_period">
					<input type="text" name="${status.expression}" value="${status.value }"  size="5" readOnly>
				</spring:bind>
				<div id="DDLSLP" style="display:none; position:absolute;"></div>
			</th>
          </tr>
          <tr>
          	<th></th>
            <th>Khusus Eka Sehat</th>
            <th colspan="2">
            	Plan Provider
				<spring:bind path="cmd.datausulan.mspo_provider">
					<input type="checkbox" name="mspo_provider" class="noborder" value="0" onclick="provider_onClick(this.checked);" 
            			<c:if test="${status.value eq 2}">checked="checked"</c:if>> 
					<input type="hidden" id="provider" name="${status.expression}" value="${status.value}">
            	</spring:bind>
            	<input type="hidden" id="usia_tt" name="usia_tt" value="${cmd.tertanggung.mste_age}">
            	<input type="hidden" id="lsdbs_number" name="lsdbs_number" value="${cmd.datausulan.lsdbs_number}">
            </th>
            <th colspan="4">
            	GMIT
				<spring:bind path="cmd.datausulan.mste_gmit">
					<input type="checkbox" name="mste_gmit" class="noborder" value="0" onclick="click_gmit(this.checked);" 
            			<c:if test="${status.value eq 1}">checked="checked"</c:if>> 
					<input type="hidden" id="id_gmit" name="${status.expression}" value="${status.value}">
            	</spring:bind>
            </th>            
          </tr>
          <tr > 
            <th></th>
            <th>Masa Pertanggungan</th>
            <th colspan="2">
            	<input type="hidden" name="ins" value ="${cmd.datausulan.mspr_ins_period}"> 
				<spring:bind path="cmd.datausulan.mspr_ins_period"> 
					<input type="text" name="${status.expression}" value="${status.value }"  size="5" readOnly >
				</spring:bind>
			</th>
            <th >Cuti Premi Setelah Tahun ke</th>
            <th colspan="3"> <spring:bind path="cmd.datausulan.mspo_installment"> 
              <input type="text" name="${status.expression}"
						value="${status.value}"  size="5"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if> >
              <font color="#CC3300">*</font> </spring:bind></th>
          </tr>
          <tr  > 
            <th></th>
            <th> Uang Pertanggungan 
              <input type ="hidden" name="kurss" value="${cmd.datausulan.kurs_p}"
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> 
            </th>
            <th width="29"> <div id="kursup"> </div></th>
			
			<th width="310">
				<spring:bind path="cmd.datausulan.mspr_tsi">
					<input type="text" name="${status.expression}" value="${status.value }" size="30" 
						onfocus="showForm( 'tsiHelper', 'true' );" onblur="showForm( 'tsiHelper', 'false' );" 
						onchange="this.value=formatCurrency( this.value );" onkeyup="showFormatCurrency('tsiHelper', this.value);"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
					<font color="#CC3300">*</font>
				</spring:bind>
				<br/>
				<input type="text" id="tsiHelper" disabled="disabled" style="display: none;" tabindex="-1"/>
			</th>
			
			<th> Premi Standar/Pokok <br> <span class="info">(sesuai dengan cara pembayaran 
              ) </span></th>
            <th width="26"><div id="kurspremi"> </div></th>

			<th colspan="2" width="324">
				<spring:bind path="cmd.datausulan.mspr_premium">
					<input type="text" name="${status.expression}" value="${status.value }" size="30" 
						onfocus="showForm( 'premiumHelper', 'true' );" onblur="showForm( 'premiumHelper', 'false' );" 
						onchange="this.value=formatCurrency( this.value );" onkeyup="showFormatCurrency('premiumHelper', this.value);"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
					<font color="#CC3300">*</font>
				</spring:bind>
				<br/>
				<input type="text" id="premiumHelper" disabled="disabled" style="display: none;" tabindex="-1"/>
			</th>

		</tr>
          <tr  > 
            <th></th>
            <th colspan="2">  
              <spring:bind path="cmd.datausulan.cara_premi">
					<label for="premi_biasa"> <input type="radio" class=noBorder
						name="${status.expression}" value="0" onClick="cpy('0');"
						<c:if test="${cmd.datausulan.cara_premi eq 0 or cmd.datausulan.cara_premi eq null}"> 
									checked</c:if>
						id="premi_biasa">ISI PREMI </label>
					<br>
					<label for="premi_persentase"> <input type="radio" class=noBorder
						name="${status.expression}" value="1" onClick="cpy('1');"
						<c:if test="${cmd.datausulan.cara_premi eq 1}"> 
									checked</c:if>
						id="premi_persentase">ISI TOTAL PREMI DAN PILIH KOMBINASI </label> 
				</spring:bind> 
					<input type="hidden" name="tanda_premi" value='${cmd.datausulan.cara_premi}'>
            </th>
            
            <th width="310">KOMBINASI : <spring:bind path="cmd.datausulan.kombinasi"> 
				<select name="${status.expression }" onChange="ganti(this.options[this.selectedIndex].value);"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					 <c:forEach var="kb" items="${select_kombinasi}"> <option value="${kb.ID}"
					<c:if test="${cmd.datausulan.kombinasi eq kb.ID}">selected</c:if> >${kb.KOMBINASI}</option> 
					</c:forEach> 
				  </select>
				  </spring:bind></th>
            <th> TOTAL PREMI <br>(Premi Pokok + Top Up):</th>

			<th width="324" colspan="3">
				<spring:bind path="cmd.datausulan.total_premi_kombinasi">
					<input type="text" name="${status.expression}" onChange="ganti(document.frmParam.tanda_premi.value); this.value=formatCurrency( this.value );" value="${status.value }" size="30" 
						onfocus="showForm( 'totpremiumHelper', 'true' );" onblur="showForm( 'totpremiumHelper', 'false' );" 
						onkeyup="showFormatCurrency('totpremiumHelper', this.value);"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
				</spring:bind>
				<br/>
				<input type="text" id="totpremiumHelper" disabled="disabled" style="display: none;" tabindex="-1"/>
			</th>
			
		</tr>
          <tr  > 
            <th  >2.</th>
            <th  > Cara Pembayaran Premi 
              <input type ="hidden" name="cb" value="${cmd.datausulan.lscb_id}"> 
            </th>
            <th colspan="6"> <div id="cara_bayar"> </div><font color="#CC3300">*</font>  </th>
          </tr>
          <tr  > 
            <th  >3.</th>
            <th  > Bentuk Pembayaran Premi </th>
            <th colspan="6" > <spring:bind path="cmd.datausulan.mste_flag_cc"> 
              <select name="${status.expression }" 
			  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <option value="" /> <c:forEach var="auto" items="${select_autodebet}"> <option value="${auto.ID}"
						   
                <c:if test="${cmd.datausulan.mste_flag_cc eq auto.ID}">selected</c:if> >${auto.AUTODEBET}</option> 
                </c:forEach> 
              </select>
              <font color="#CC3300">*</font>  </spring:bind> </th>
          </tr>
          <tr  > 
            <th  >4.</th>
            <th  colspan="3"> Mulai Berlaku Pertanggungan <span class="info">(DD/MM/YYYY)</span></th>
            <th colspan="4" > Akhir Berlaku Pertanggungan <span class="info">(DD/MM/YYYY)</span></th>
          </tr>
          <tr  > 
            <th ></th>
            <th colspan="3"> <spring:bind path="cmd.datausulan.mste_beg_date"> 
              <c:choose>
            		<c:when test="${cmd.datausulan.convert eq 1}">
            			<script>inputDate('${status.expression}', '${status.value}', true,'listtglpolis1();');</script>
            		</c:when>
              		<c:otherwise>
              			<script>inputDate('${status.expression}', '${status.value}', false,'listtglpolis1();');</script>
              		</c:otherwise>
              </c:choose>
              <font color="#CC3300">*</font>  </spring:bind> </th>
            <th colspan="4"> <spring:bind path="cmd.datausulan.mste_end_date"> 
              <script>inputDate('${status.expression}', '${status.value}', true);</script>
              </spring:bind> </th>
          </tr>
          <tr  > 
            <th >5.</th>
            <th colspan="7"><div align="left">Detail Khusus Horison</div></th>
          </tr>
          <tr  > 
            <th >&nbsp;</th>
            <th colspan="3"><div align="left">% Persentase DPLK</div></th>
            <th colspan="4"> <spring:bind path="cmd.pemegang.mste_pct_dplk"> 
				<select name="${status.expression }" 
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					 <c:forEach var="dplk" items="${select_dplk}"> <option value="${dplk.ID}"
					<c:if test="${cmd.pemegang.mste_pct_dplk eq dplk.ID}">selected</c:if> >${dplk.DPLK}</option> 
					</c:forEach> 
				  </select>
              <font color="#CC3300">*</font>  </spring:bind></th>
          </tr>
            <tr> 
            <th>6.</th>
            <th>No Virtual Account </th>
            <th colspan="6" >
              <input type="hidden" name="vacc" value ="${cmd.tertanggung.mste_no_vacc}"> 
				<spring:bind path="cmd.tertanggung.mste_no_vacc"> 
					<input type="text" name="${status.expression}" value="${status.value }"  size="20" readOnly 
					style="background-color: #D4D4D4"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>
					>
				</spring:bind>
             </th>
          </tr>          
          <tr> 
            <th class="subtitle2"></th>
            <th class="subtitle2" colspan="7">JENIS ASURANSI TAMBAHAN (RIDER) 
              <spring:bind path="cmd.datausulan.jmlrider"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30">
               </spring:bind> 
            </th>
          </tr>
          <tr> 
            <th class="subtitle2" colspan="8" style="text-align: center;"> <input name="btnadd1" type="button" id="btnadd1" value="ADD" onClick="addRowDOM1('tablerider', '1')"> 
              <input name="btn_cancel1" type="button" id="btn_cancel1" value="DELETE" " onClick="cancel1()"> 
              <input type="hidden" name="mn" value="0"> </th>
          </tr>
        </table>
         <spring:bind path="cmd.datausulan.convert"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="5">
               </spring:bind> 
        <div id="divrider"> 
          
        <table id="tablerider" class="entry">
          <tr  > 
            <th class="col">No. </th>
            <th class="col">Nama Produk<font color='#CC3300'>*</font></th>
            <th class="col"> Unit<font color='#CC3300'>*</font> </th>
            <th class="col"> Klas<font color='#CC3300'>*</font> </th>
            <th class="col">Persentase UP</th>
            <th class="col">Uang Pertanggungan </th>
			 <th class="col">Premi </th>
            <th class="col">Masa Per-<br>tanggungan</th>
            <th class="col">Mulai Berlaku Pertanggungan<br>
              <span class="info">(DD/MM/YYYY)</span></th>
            <th class="col">Akhir Berlaku Pertanggungan<br>
              <span class="info">(DD/MM/YYYY)</span></th>
            <th class="col">Akhir Pembayaran<br>
              <span class="info">(DD/MM/YYYY)</span></th>
            <th class="col" >Tertanggung<font color='#CC3300'>*</font></th>
            <th class="col">Rate</th>
            <th class="col"> 
              </th>
          </tr>
   
         <c:forEach items="${cmd.datausulan.daftaRider}" var="ride" varStatus="status"> 
          <tr  > 
            <td >${status.index +1}</td>
			<td>
							 
					<select name="ride.plan_rider${status.index +1}" <c:if test="${ride.flag_include eq 1}" >style='background-color :#D4D4D4' disabled</c:if>
					onchange="cekUpRiderDisabled(${status.index +1});">					
						<c:forEach var="rider" items="${select_rider}">						
							
								<option 
								<c:if test="${ride.plan_rider eq rider.plan}"> SELECTED</c:if>
								value="${rider.plan}">${rider.lsdbs_name}</option>
								

						</c:forEach>
						
					</select>
					
					<input type="hidden" name="ride.plan_riderx${status.index +1}" value="${ride.plan_rider}">

				<c:if test="${ride.lsbs_id eq 800}">	
					<c:if test="${ride.mspr_tsi_pa_a ne 0}"> <c:if test="${ride.mspr_tsi_pa_a ne null}"> Jenis Resiko A </c:if></c:if>
					<c:if test="${ride.mspr_tsi_pa_b ne 0}"> <c:if test="${ride.mspr_tsi_pa_b ne null}">- B </c:if></c:if>
					<c:if test="${ride.mspr_tsi_pa_c ne 0}"> <c:if test="${ride.mspr_tsi_pa_c ne null}"> - C </c:if></c:if>
					<c:if test="${ride.mspr_tsi_pa_d ne 0}"> <c:if test="${ride.mspr_tsi_pa_d ne null}"> - D </c:if></c:if>
					<c:if test="${ride.mspr_tsi_pa_m ne 0}"> <c:if test="${ride.mspr_tsi_pa_m ne null}"> - M </c:if></c:if>
				</c:if>
			</td>
           <td > 
              <input type="text" name='ride.mspr_unit${status.index +1}' value ='${ride.mspr_unit}' size="3" <c:if test="${ride.flag_include eq 1}">style='background-color :#D4D4D4' readOnly</c:if>  maxlength="2">
    	 	</td>
            <td ><input type="text" name='ride.mspr_class${status.index +1}' value ='${ride.mspr_class}' size="3" <c:if test="${ride.flag_include eq 1}">style='background-color :#D4D4D4' readOnly</c:if> maxlength="2"></td>
            <td nowrap>
				<select name="ride.persenUp${status.index +1}" <c:if test="${ride.flag_include eq 1}">style='background-color :#D4D4D4' disabled</c:if>>
					<c:forEach var="pUp" items="${select_persenUp}">
						<option 
							<c:if test="${ride.mspr_persen eq pUp.ID}"> SELECTED </c:if>
							value="${pUp.ID}">${pUp.PERSEN}</option>
					</c:forEach>
					</select>					
				<input type="hidden" name="ride.persenUpx${status.index +1}" value ="${ride.mspr_persen}">
			</td>
            <td ><input type="text" name='ride.mspr_tsi${status.index +1}' size="20" value =<fmt:formatNumber type='number' value='${ride.mspr_tsi}'/> style='background-color :#D4D4D4' readOnly></td> 
	       <td ><input type="text" name='ride.mspr_premium${status.index +1}' size="20" value =<fmt:formatNumber type='number' value='${ride.mspr_premium}'/> style='background-color :#D4D4D4' readOnly></td> 

	        <td ><input type="text" name='ride.mspr_ins_period${status.index +1}' value ='${ride.mspr_ins_period}'  size="5" style='background-color :#D4D4D4' readOnly></td>
            <td nowrap>
				<input type="text" name='mspr_beg_date${status.index +1}' value='<fmt:formatDate value="${ride.mspr_beg_date}" pattern="dd/MM/yyyy"/>'  size="15" style='background-color :#D4D4D4' readOnly>
			</td>
            <td nowrap>
				<input type="text" name='mspr_end_date${status.index +1}' value='<fmt:formatDate value="${ride.mspr_end_date}" pattern="dd/MM/yyyy"/>'  size="15" style='background-color :#D4D4D4' readOnly>
			</td>
            <td nowrap>
				<input type="text" name='mspr_end_pay${status.index +1 }' value='<fmt:formatDate value="${ride.mspr_end_pay}" pattern="dd/MM/yyyy"/>'  size="15" style='background-color :#D4D4D4' readOnly>
			</td>
						
            <td nowrap>
				<select name="ride.mspr_tt${status.index +1}" <c:if test="${ride.flag_include eq 1}">style='background-color :#D4D4D4' disabled</c:if>>
							<c:forEach var="insrider" items="${select_insrider}">
							<option 
							<c:if test="${ride.mspr_tt eq insrider.ID}"> SELECTED </c:if>
							value="${insrider.ID}">${insrider.INSRIDER}</option>
						</c:forEach>
					</select>					
			<input type="hidden" name="ride.mspr_tt1${status.index +1}" value ="${ride.mspr_tt}">
					
</td>
            <td ><input type="text" name='ride.mspr_rate${status.index +1}' value =<fmt:formatNumber type='number' value='${ride.mspr_rate}'/>  size="10" style='background-color :#D4D4D4' readOnly> </td>
            <td  > <input type=checkbox class="noBorder" name="cek${status.index +1}" id= "ck${status.index +1}" <c:if test="${ride.flag_include eq 1}">style='background-color :#D4D4D4' disabled</c:if>>
			<input type="hidden" name="ride.flag_include${status.index +1}" value='${ride.flag_include}' size="3" >
			</td>
          </tr>
          </c:forEach> 
        </table>
 </div>
 <table>
 	<tr><td>&nbsp;</td></tr>
 </table>
 <table class="entry" width = "100%">
 	<tr> 
            <th class="subtitle2"></th>
            <th class="subtitle2" colspan="7">PESERTA ASURANSI KESEHATAN
              <spring:bind path="cmd.datausulan.jml_peserta"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30">
               </spring:bind>  
            </th>
          </tr>
          <tr> 
            <th class="subtitle2" colspan="8" style="text-align: center;">
             	<input name="btnadd1x" type="button" id="btnadd1x" value="ADD"  tabindex="15"  onClick="addRowDOM1x('tableProd', '1')">  
             	<input name="btn_cancel1x" type="button" id="btn_cancel1x" value="DELETE" tabindex="16"  onClick="cancel1x()">			
              	<input type="hidden" name="pesertax"  value="0">
            </th>
          </tr>
	<table id="tableProd" class="entry">
            <tr > 
              	<th class="col" >No.</th>
              	<th class="col" >Nama <font color="#CC3300">*</font></th>
              	<th class="col" >Sex<font color="#CC3300">*</font></th>
              	<th class="col" >Tanggal Lahir <font color="#CC3300">*</font></th>
 	          	<th class="col" >Umur</th>
              	<th class="col" >Hubungan Dengan Pemegang Polis<font color="#CC3300">*</font></th>
              	<th class="col" >Tinggi Badan<font color="#CC3300">*</font></th>
              	<th class="col" >Berat Badan<font color="#CC3300">*</font></th>
              	<th class="col" >Kewarganegaraan<font color="#CC3300">*</font></th>
              	<th class="col" >Pekerjaan<font color="#CC3300">*</font></th>
              	<th class="col" >Jenis Produk<font color="#CC3300">*</font></th>
              	<th class="col" >Jenis rider<font color="#CC3300">*</font></th>
             	<th class="col"  >&nbsp;</th>
            </tr>
         <c:forEach items="${cmd.datausulan.daftarplus}" var="ptx" varStatus="status"> 
            <tr > 
             	<td>
              		<input type="text" name='ptx.no_urut${status.index +1}' value ='${ptx.no_urut}' size="2" maxlength="2" readOnly>
             	</td>
              	<td >
              		<input type="text" name='ptx.nama${status.index +1}' value ='${ptx.nama}' size="20" maxlength="100">
	           	</td>
	           	<td >
	           		<input type="radio" class=noBorder
						name='kelamin${status.index +1}' value="1"
						<c:if test="${ptx.kelamin eq 1}">  
									checked</c:if>  onClick="kelamin(${status.count},'1');"
							tabindex="11" id="cowok">Pria 
					<input type="radio" class=noBorder
						name='kelamin${status.index +1}' value="0"
						<c:if test="${ptx.kelamin eq 0}"> 
									checked</c:if> onClick="kelamin(${status.count},'0');"
						tabindex="12" id="cewek">Wanita
						<input type="hidden" name='ptx.kelamin${status.index +1}' value ='${ptx.kelamin}' size="3" maxlength="3" >
		       	</td>
		      	<td > 
              		<input type="text" name='tgllhr${status.index +1}' value='<fmt:formatDate value="${ptx.tgl_lahir}" pattern="dd"/>'  size="2" maxlength="2" >
					/<input type="text" name='blnhr${status.index +1}' value='<fmt:formatDate value="${ptx.tgl_lahir}" pattern="MM"/>'  size="2" maxlength="2" >
					/<input type="text" name='thnhr${status.index +1}' value='<fmt:formatDate value="${ptx.tgl_lahir}" pattern="yyyy"/>'  size="4" maxlength="4" >
					&nbsp;<input type="hidden" name='ptx.tgl_lahir${status.index +1}' value='<fmt:formatDate value="${ptx.tgl_lahir}" pattern="dd/MM/yyyy"/>'  size="5" >
              	</td>
              	<td>
              		<input type="text" name='ptx.umur${status.index +1}' value ='${ptx.umur}' size="3" maxlength="3" readOnly >
	          	</td>
              	<td>
                  	<select name="ptx.lsre_id${status.index +1}" >
						<c:forEach var="lsreIdPlus" items="${select_relasi}">
							<option 
							<c:if test="${ptx.lsre_id eq lsreIdPlus.ID}"> SELECTED </c:if>
							value="${lsreIdPlus.ID}">${lsreIdPlus.RELATION}</option>
						</c:forEach>
					</select>
	           	</td>
	           	<td >
              		<input type="text" name='ptx.tinggi${status.index +1}' value ='${ptx.tinggi}' size="2" maxlength="3">
	           	</td>
	           	<td>
              		<input type="text" name='ptx.berat${status.index+1}' value ='${ptx.berat}' size="2" maxlength="3">
	           	</td>
	           	<td>
                  	<select name="ptx.lsne${status.index +1}" >
						<c:forEach var="lsne" items="${select_warganegara}">
							<option 
							<c:if test="${ptx.lsne eq lsne.ID}"> SELECTED </c:if>
							value="${lsne.ID}">${lsne.NEGARA}</option>
						</c:forEach>
					</select>
	           	</td>
	           	<td >
              		<input type="text" name='ptx.kerja${status.index +1}' value ='${ptx.kerja}' size="20" maxlength="100">
	           	</td>
<!-- 	           	FIX -->
	           	<td>
                  	<select name="ptx.lsbs_id_plus${status.index +1}" >
						<c:forEach var="lsbsPlus" items="${select_prodKes}">
							<option 
							<c:if test="${ptx.lsbs_id_plus eq lsbsPlus.ID}"> SELECTED </c:if>
							value="${lsbsPlus.ID}">${lsbsPlus.PRODUK}</option>
						</c:forEach>
					</select>
	           	</td>
				<td>
					<select name="ptx.flag_jenis_peserta${status.index +1}">
						<c:forEach var="flagJenis" items="${select_flag_jp}">
							<option 
							<c:if test="${ptx.flag_jenis_peserta eq flagJenis.ID}"> SELECTED </c:if>
							value="${flagJenis.ID}">${flagJenis.JENIS}</option>
						</c:forEach>
					</select>
				</td>
	            <td ><input type=checkbox name="cek2${status.index +1}" id= "ck2${status.index +1}" class="noBorder"></td>
            </tr>
		</c:forEach>
	</table>
</table> 
<input type="hidden" name="hal" value="${halaman}">
<input type="hidden" name="flag_warning_autodebet" value="0">
			<spring:bind path="cmd.pemegang.indeks_halaman"> 
              <input type="hidden" name="${status.expression}"
						value="${halaman-1}"  size="30" >
              </spring:bind> 
		<table border="0" width="100%"  cellspacing="1" cellpadding="1" class="entry">
          <tr> 
            <td colspan="9"> 
			 <p align="left"><b> <font face="Verdana" size="2" color="#CC3300">Note : Untuk pecahan decimal menggunakan titik.
			 <br>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; * Wajib diisi</font></b></p>
			</td>
          </tr>
          <c:if test="${cmd.tertanggung.lspd_id eq 2 or cmd.tertanggung.lspd_id eq 27 or cmd.tertanggung.lspd_id eq 209}">
	          <c:if test="${not empty cmd.pemegang.reg_spaj}">
	          <tr>          	 
	          	 <td colspan="2">
					 <input type="checkbox" name="edit_dataUsulan" id="edit_dataUsulan" class="noBorder" value ="${cmd.datausulan.edit_dataUsulan}" <c:if test="${cmd.datausulan.edit_dataUsulan eq 1}"> 
						checked</c:if> size="30" onClick="edit_onClick();"> Telah dilakukan edit
					<spring:bind path="cmd.datausulan.edit_dataUsulan"> 
			          		<input type="hidden" name="${status.expression}" value="${cmd.datausulan.edit_dataUsulan}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>	   
	         	 </td>
				 <td colspan="7">
				 		<spring:bind path="cmd.datausulan.kriteria_kesalahan">
							<select name="${status.expression}">
								<c:forEach var="a" items="${select_kriteria_kesalahan}">
									<option
										<c:if test="${cmd.datausulan.kriteria_kesalahan eq a.KESALAHAN}"> SELECTED </c:if>
										value="${a.KESALAHAN}">${a.KESALAHAN}
									</option>
								</c:forEach>
							</select>
						</spring:bind>
				 </td> 
	          </tr>
	          <tr>
	          	<td colspan="9">&nbsp;</td>
	          </tr>
          	</c:if>
	      </c:if>
	          <tr> 
            <td  align="center" colspan="4" > 
              <div align="center">
                <input type="submit" name="_target${halaman-1}" value="Prev &laquo;" onClick="prev()" 
				accesskey="P" onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
                <input type="submit" name="_target${halaman+1}" value="Next &raquo;" onClick="next()" 
				accesskey="N" onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
                <input type="hidden" name="_page" value="${halaman}">
              </div>
            </td>
          </tr>
        </table>
  
</table>
</form>
</body>
<script>
<c:forEach items="${cmd.datausulan.daftaRider}" var="ride" varStatus="status"> 
	cekUpRiderDisabled(${status.index +1});
</c:forEach>
</script>
<%@ include file="/include/page/footer.jsp"%>