<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript">
	var pesan = '';
	
	function yuk(ayo){
		if(ayo=='simpan') if(!confirm('Simpan Telemarketer?')) return false;
		
		document.formpost.aksi.value = ayo;
		this.disabled=true;document.formpost.submit();
	}
	function ganti(a,b,c,d){
		document.formpost.tm_id.value = a;
		document.formpost.tm_code.value = b;
		document.formpost.spv_code.value = c;
		document.formpost.spv_name.value = d;
	}
</script>
</head>
<BODY onload="resizeCenter(550,400); document.title='PopUp :: Input Informasi Referral Bank'; setupPanes('container1', 'tab1'); document.formpost.cari.select(); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Input Informasi Telemarketer (KHUSUS PRODUK YANG DIJUAL DM/TM)</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" action="${path}/bac/reff_bank.htm" style="text-align: center;">
					<input type="hidden" name="window" value="reff_dmtm">
					<input type="hidden" name="aksi" value="">
					<input type="hidden" name="spaj" value="${cmd.spaj}">
					<fieldset>
						<legend>Telemarketer</legend>
						<table class="entry2">
							<tr>
								<th>ID</th>
								<td><input class="readOnly" readonly type="text" name="tm_id" size="8" value="${cmd.TM_ID}" maxlength="6"></td>
							</tr>
							<tr>
								<th>Telemarketer</th>
								<td><input class="readOnly" readonly type="text" name="tm_code" size="20" value="${cmd.TM_CODE}"></td>
							</tr>
							<tr>
								<th>Supervisor</th>
								<td><input class="readOnly" readonly type="text" name="spv_name" size="40" value="${cmd.SPV_NAME}">
								<input type="hidden" name="spv_code" size="8" value="${cmd.SPV_CODE}"></td>
							</tr>
						</table>
					</fieldset>
					<fieldset>
						<legend></legend>
						<table class="entry2">
							<tr>
								<th>Cari Telemarketer : </th>
								<td>
									<input type="text" value="${cmd.cari}" name="cari" size="40" onkeyup="if(event.keyCode==13) document.formpost.btnCari.click();">
									
									<input type="button" name="btnCari" value="Cari" onclick="yuk('cari');"
										accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">			
	
									<input type="button" name="btnSave" value="Simpan" onclick="yuk('simpan');"
										accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
				
									<input type="button" name="close" value="Tutup" onclick="window.close();"
										accesskey="T" onmouseover="return overlib('Alt-T', AUTOSTATUS, WRAP);" onmouseout="nd();">
				
								</td>
							</tr>
							<tr>
								<th></th>
								<td>
									<c:if test="${not empty cmd.daftarTelemarketer}">
										<table class="simple">
											<thead>
												<tr>
													<th class="center">ID</th>
													<th class="center">Telemarketer</th>
													<th class="center">Supervisor</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="u" items="${cmd.daftarTelemarketer}" varStatus="stat">
													<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
														onclick="ganti('${u.TM_ID}','${u.TM_CODE}','${u.SPV_CODE}','${u.SPV_NAME}');">
														<td>${u.TM_ID}</td>
														<td>${u.TM_CODE}</td>
														<td>${u.SPV_NAME}</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</c:if>
									<div id="success" style="text-transform: none;">PESAN:<br>
										- Anda dapat melakukan pencarian berdasarkan ID maupun NAMA TELEMARKETER<BR/>
										- Setelah memilih telemarketer, silahkan tekan tombol SIMPAN<BR/>
										<c:forEach var="err" items="${cmd.success}">
											- <c:out value="${err}" escapeXml="false" />
											<br />
											<script>
												pesan += '${err}\n';
											</script>
										</c:forEach>
									</div>
									<c:if test="${not empty cmd.error }">
										<div id="error" style="text-transform: none;">ERROR:<br>
											<c:forEach var="err" items="${cmd.error}">
												- <c:out value="${err}" escapeXml="false" />
												<br />
												<script>
													pesan += '${err}\n';
												</script>
											</c:forEach>
										</div>
									</c:if>
								</td>
							</tr>
						</table>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

</form>
</body>
<script>
	if(pesan != '') alert(pesan);
</script>
<%@ include file="/include/page/footer.jsp"%>