<%@ include file="/include/page/header.jsp"%><c:set var="path" value="${pageContext.request.contextPath}" />

<script language="JavaScript">
<!--
	function Body_onload() {
		var pesan = document.frmParam.elements['agen.pesan_ref'].value;
		if(pesan!=null && pesan!=''){
			alert(pesan);
		}
		
		var hd = document.frmParam.elements['pemegang.flag_hadiah'].value;
		if(hd==1){
			document.getElementById( 'ps_hadiah' ).style.display = 'block';
		}else{
			document.getElementById( 'ps_hadiah' ).style.display = 'none';
		}
	
		if (document.frmParam.elements['pemegang.mspo_pribadi'].value == '1')
		{
			document.frmParam.mspo_pribadi1.checked = true;
		}else{
			document.frmParam.mspo_pribadi1.checked = false;
		}
		if(document.frmParam.elements['currentUser.jn_bank'].value == '2' || document.frmParam.elements['currentUser.jn_bank'].value == '3' || document.frmParam.elements['currentUser.jn_bank'].value == '16'){
			document.frmParam.elements['agen.msag_id'].disabled = true;
			document.frmParam.elements['agen.msag_id'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['pemegang.mspo_leader'].disabled = true;
			document.frmParam.elements['pemegang.mspo_leader'].style.backgroundColor ='#D4D4D4';
		}else{
			document.frmParam.elements['agen.msag_id'].disabled = false;
			document.frmParam.elements['agen.msag_id'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['pemegang.mspo_leader'].disabled = false;
			document.frmParam.elements['pemegang.mspo_leader'].style.backgroundColor ='#FFFFFF';
		}
		
		if (document.frmParam.elements['pemegang.mspo_ref_bii'].value == '1')
		{
			document.frmParam.mspo_ref_bii1.checked = true;
			document.frmParam.elements['pemegang.mspo_ao'].disabled = false;
			document.frmParam.elements['pemegang.mspo_ao'].style.backgroundColor ='#FFFFFF';
		}else{
			document.frmParam.mspo_ref_bii1.checked = false;
			document.frmParam.elements['pemegang.mspo_ao'].disabled = true;
			document.frmParam.elements['pemegang.mspo_ao'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['pemegang.mspo_ao'].value='';
		}
		//MANTA
		if (document.frmParam.elements['broker.flagbroker'] == true || document.frmParam.elements['broker.lsb_id'].value != '')
		{
			document.frmParam.flagbroker1.checked = true;
			document.frmParam.elements['broker.lsb_id'].disabled = false;
			document.frmParam.elements['broker.lsb_id'].style.backgroundColor ='#FFFFFF';
			document.frmParam.caribank2.disabled = false;
			document.frmParam.caribank2.style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['broker.no_account'].disabled = false;
			document.frmParam.elements['broker.no_account'].style.backgroundColor ='#FFFFFF';
		}else{
			document.frmParam.flagbroker1.checked = false;
			document.frmParam.elements['broker.lsb_id'].disabled = true;
			document.frmParam.elements['broker.lsb_id'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['broker.lsb_id'].value='';
			document.frmParam.caribank2.disabled = true;
			document.frmParam.caribank2.style.backgroundColor ='#D4D4D4';
			document.frmParam.caribank2.value='';
			document.frmParam.elements['broker.no_account'].disabled = true;
			document.frmParam.elements['broker.no_account'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['broker.no_account'].value='';
			document.frmParam.elements['broker.lsb_nama'].value='';
		}
		<c:if test="${not empty cmd.datausulan.mste_flag_el}">document.getElementById('flag_karyawan').value = ${cmd.datausulan.mste_flag_el}</c:if>
		if(document.getElementById('flag_karyawan').value == '1'){
			document.getElementById('flagkrywn2').value=1;
			document.getElementById('flagkrywn2').checked=true;
			document.frmParam.cariperusahaan1.disabled = true;
			document.frmParam.cariperusahaan1.style.backgroundColor ='#D4D4D4';
			//document.frmParam.btnperusahaan1.disabled =true;		
			document.frmParam.cariperusahaan1.value='';
			ajaxSelectWithParam("080000000024",'select_company_ekal','perush','pemegang.mspo_customer','', 'COMPANY_ID', 'COMPANY_NAMA', '');
		}else if(document.getElementById('flag_karyawan').value == '2'){
			document.getElementById('flagkrywn3').value=2;
			document.getElementById('flagkrywn3').checked=true;
		}else if(document.getElementById('flag_karyawan').value == '3'){
			document.getElementById('flagkrywn4').value=3;
			document.getElementById('flagkrywn4').checked=true;
		}else if(document.getElementById('flag_karyawan').value == '4'){
			document.getElementById('flagkrywn5').value=4;
			document.getElementById('flagkrywn5').checked=true;
			document.frmParam.cariperusahaan1.disabled = true;
			document.frmParam.cariperusahaan1.style.backgroundColor ='#D4D4D4';
			document.frmParam.cariperusahaan1.value='';
			ajaxSelectWithParam("080000000024",'select_company_ekal','perush','pemegang.mspo_customer','', 'COMPANY_ID', 'COMPANY_NAMA', '');
		}else{
			document.getElementById('flagkrywn1').value=0;
			document.getElementById('flagkrywn1').checked=true;
			document.frmParam.cariperusahaan1.disabled = false;
			document.frmParam.cariperusahaan1.style.backgroundColor ='#FFFFFF';
			document.frmParam.btnperusahaan1.disabled = false;		
		}
		
		kotak();
		kotak1();
		kotak2();
		brok();
		
		if (document.frmParam.elements['datausulan.flag_as'].value=='2')
		{
			document.frmParam.elements['employee.nik'].readOnly = false;
			document.frmParam.elements['employee.nik'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['employee.no_urut'].value = '1';
			//document.frmParam.elements['employee.potongan'].value = document.frmParam.elements['datausulan.mspr_premium'].value;	
		}else{
			document.frmParam.elements['employee.nik'].readOnly = true;
			document.frmParam.elements['employee.nik'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['employee.no_urut'].value = '';
			document.frmParam.elements['employee.potongan'].value='';
		}
		
		if (document.frmParam.elements['datausulan.flag_worksite'].value == '0' && (document.frmParam.elements['mps_employee'].value != '1'))
		{
			document.frmParam.elements['pemegang.mspo_customer'].disabled = true;
			document.frmParam.elements['pemegang.mspo_customer'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['pemegang.mspo_customer'].value='';
			document.frmParam.btnperusahaan1.disabled = true;
			document.frmParam.cariperusahaan1.readOnly = true;
			document.frmParam.cariperusahaan1.style.backgroundColor ='#D4D4D4';
			document.frmParam.cariperusahaan1.value='';
			document.frmParam.elements['pemegang.nik'].disabled = true;
			document.frmParam.elements['pemegang.nik'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['pemegang.nik'].value='';
		}else{
			document.frmParam.elements['pemegang.mspo_customer'].disabled = false;
			document.frmParam.elements['pemegang.mspo_customer'].style.backgroundColor ='#FFFFFF';
			document.frmParam.cariperusahaan1.readOnly = false;
			document.frmParam.cariperusahaan1.style.backgroundColor ='#FFFFFF';
			document.frmParam.btnperusahaan1.disabled = false;
			document.frmParam.elements['pemegang.nik'].disabled = false;
			document.frmParam.elements['pemegang.nik'].style.backgroundColor ='#FFFFFF';
		}

	}
		
	//function pribadi
function pribadi_onClick(){

	if(document.frmParam.mspo_pribadi1.checked==true){
		document.frmParam.mspo_pribadi1.checked = true;
		document.frmParam.elements['pemegang.mspo_pribadi'].value = '1';
	}else{
		document.frmParam.mspo_pribadi1.checked = false;
		document.frmParam.elements['pemegang.mspo_pribadi'].value = '0';
	}
	
}

	//function pribadi
function ao_onClick(){
	if(document.frmParam.mspo_ref_bii1.checked == true){
		document.frmParam.mspo_ref_bii1.checked = true;
		document.frmParam.elements['pemegang.mspo_ao'].disabled = false;
		document.frmParam.elements['pemegang.mspo_ao'].style.backgroundColor ='#FFFFFF';
		document.frmParam.elements['pemegang.mspo_ref_bii'].value = '1';

	}else{
		document.frmParam.mspo_ref_bii1.checked = false;
		document.frmParam.elements['pemegang.mspo_ao'].disabled = true;
		document.frmParam.elements['pemegang.mspo_ao'].style.backgroundColor ='#D4D4D4';
		document.frmParam.elements['pemegang.mspo_ao'].value='';
		document.frmParam.elements['pemegang.mspo_ref_bii'].value = '0';
		document.frmParam.elements['pemegang.nama_ao'].value='';

	}
}

//MANTA - Function Flag Broker
function broker_onClick(){
	if(document.frmParam.flagbroker1.checked == true){
		document.frmParam.flagbroker1.checked = true;
		document.frmParam.elements['broker.lsb_id'].disabled = false;
		document.frmParam.elements['broker.lsb_id'].style.backgroundColor ='#FFFFFF';
		document.frmParam.caribank2.disabled = false;
		document.frmParam.caribank2.style.backgroundColor ='#FFFFFF';
		document.frmParam.elements['broker.no_account'].disabled = false;
		document.frmParam.elements['broker.no_account'].style.backgroundColor ='#FFFFFF';
		document.frmParam.elements['broker.flagbroker'].value = true;

	}else{
		document.frmParam.flagbroker1.checked = false;
		document.frmParam.elements['broker.lsb_id'].disabled = true;
		document.frmParam.elements['broker.lsb_id'].style.backgroundColor ='#D4D4D4';
		document.frmParam.elements['broker.lsb_id'].value='';
		document.frmParam.caribank2.disabled = true;
		document.frmParam.caribank2.style.backgroundColor ='#D4D4D4';
		document.frmParam.caribank2.value='';
		document.frmParam.elements['broker.no_account'].disabled = true;
		document.frmParam.elements['broker.no_account'].style.backgroundColor ='#D4D4D4';
		document.frmParam.elements['broker.no_account'].value='';
		
		document.frmParam.elements['broker.flagbroker'].value = false;
		document.frmParam.elements['broker.lsb_nama'].value='';
	}
}

function karyawan_onClick()
{

	if(document.getElementById('flagkrywn1').checked == true){
		document.frmParam.cariperusahaan1.disabled = true;
		document.frmParam.cariperusahaan1.style.backgroundColor ='#D4D4D4';
		document.frmParam.btnperusahaan1.disabled =true;		
		document.frmParam.elements['datausulan.mste_flag_el'].value = '1';
		document.frmParam.cariperusahaan1.value='';
		//alert("ok");
		//ajaxSelectWithParam('080000000024','select_namacompany','perush','pemegang.mspo_customer','', 'COMPANY_ID', 'COMPANY_NAMA', '');
		//ajaxSelectWithParam("080000000024",'select_namacompany','perush','pemegang.mspo_customer','', 'COMPANY_ID', 'COMPANY_NAMA', '');
		ajaxSelectWithParam("080000000024",'select_company_ekal','perush','pemegang.mspo_customer','', 'COMPANY_ID', 'COMPANY_NAMA', '');
		if(document.frmParam.elements['datausulan.lsbs_id'].value==164){
			alert("Untuk produk stable Link Karyawan, silakan dipastikan UP minimum Rp. 15.000.000,-, dan premi minimum Rp 5.000.000,-");
		}
		
		//ajaxSelectWithParam(document.frmParam.cariperusahaan1.value,'select_company','perush','pemegang.mspo_customer','', 'COMPANY_ID', 'COMPANY_NAMA', '');
	}else{
		document.frmParam.cariperusahaan1.disabled = false;
		document.frmParam.cariperusahaan1.style.backgroundColor ='#FFFFFF';
		document.frmParam.btnperusahaan1.disabled = false;		
		ajaxSelectWithParam("000000000000",'select_company_ekal','perush','pemegang.mspo_customer','', 'COMPANY_ID', 'COMPANY_NAMA', '');
	}

	/*if(document.frmParam.mste_flag_el1.checked == true){
		document.frmParam.mste_flag_el1.checked = true;
		document.frmParam.cariperusahaan1.disabled = true;
		document.frmParam.cariperusahaan1.style.backgroundColor ='#D4D4D4';
		document.frmParam.btnperusahaan1.disabled =true;		
		document.frmParam.elements['datausulan.mste_flag_el'].value = '1';
		document.frmParam.cariperusahaan1.value='';
		//alert("ok");
		//ajaxSelectWithParam('080000000024','select_namacompany','perush','pemegang.mspo_customer','', 'COMPANY_ID', 'COMPANY_NAMA', '');
		//ajaxSelectWithParam("080000000024",'select_namacompany','perush','pemegang.mspo_customer','', 'COMPANY_ID', 'COMPANY_NAMA', '');
		ajaxSelectWithParam("080000000024",'select_company_ekal','perush','pemegang.mspo_customer','', 'COMPANY_ID', 'COMPANY_NAMA', '');
		if(document.frmParam.elements['datausulan.lsbs_id'].value==164){
			alert("Untuk produk stable Link Karyawan, silakan dipastikan UP minimum Rp. 15.000.000,-, dan premi minimum Rp 5.000.000,-");
		}
		
		//ajaxSelectWithParam(document.frmParam.cariperusahaan1.value,'select_company','perush','pemegang.mspo_customer','', 'COMPANY_ID', 'COMPANY_NAMA', '');
	}else{
		document.frmParam.mste_flag_el1.checked = false;
		document.frmParam.cariperusahaan1.disabled = false;
		document.frmParam.cariperusahaan1.style.backgroundColor ='#FFFFFF';
		document.frmParam.btnperusahaan1.disabled = false;		
		document.frmParam.elements['datausulan.mste_flag_el'].value = '0';
	}
	*/
}

	function next()
	{

		eval( " document.frmParam.elements['pemegang.indeks_halaman'].value = ((document.frmParam.hal.value)+1);");

		if(document.frmParam.mspo_pribadi1.checked==true){
			document.frmParam.elements['pemegang.mspo_pribadi'].value = '1';
		}else{
			document.frmParam.elements['pemegang.mspo_pribadi'].value = '0';
		}
		
		if (document.frmParam.mspo_ref_bii1.checked == true)
		{
			document.frmParam.elements['pemegang.mspo_ref_bii'].value = '1';
		}else{
			document.frmParam.elements['pemegang.mspo_ref_bii'].value = '0';
		}		
		
		if (document.frmParam.flagbroker1.checked == true)
		{
			document.frmParam.elements['broker.flagbroker'].value = true;
		}else{
			document.frmParam.elements['broker.flagbroker'].value = false;
		}
			
	}
	
	function next1()
	{

		if(document.frmParam.mspo_pribadi1.checked==true){
			document.frmParam.elements['pemegang.mspo_pribadi'].value = '1';
		}else{
			document.frmParam.elements['pemegang.mspo_pribadi'].value = '0';
		}
		
		if (document.frmParam.mspo_ref_bii1.checked == true)
		{
			document.frmParam.elements['pemegang.mspo_ref_bii'].value = '1';
		}else{
			document.frmParam.elements['pemegang.mspo_ref_bii'].value = '0';
		}	
		
		if (document.frmParam.flagbroker1.checked == true)
		{
			document.frmParam.elements['broker.flagbroker'].value = true;
		}else{
			document.frmParam.elements['broker.flagbroker'].value = false;
		}	
			
	}	
	
	function kotak()
	{
		if (document.frmParam.elements['agen.msag_id'].value == "000000")
		{
			document.frmParam.elements['agen.mcl_first'].readOnly=false;
			document.frmParam.elements['agen.mcl_first'].style.backgroundColor ='#FFFFFF';
			
		}else{
			document.frmParam.elements['agen.mcl_first'].readOnly=true;
			document.frmParam.elements['agen.mcl_first'].style.backgroundColor ='#D4D4D4';		
		}
		if (document.frmParam.elements['agen.msag_id'].value != "")
		{
			listdataagen(document.frmParam.elements['agen.msag_id'].value, document.frmParam.elements['pemegang.lca_id'].value, document.frmParam.elements['pemegang.reg_spaj'].value);
		}
	}
	
	function kotak1()
	{
		if (document.frmParam.elements['pemegang.mspo_ao'].value != "")
		{
			listdataagenao(document.frmParam.elements['pemegang.mspo_ao'].value);
		}
	}
	
	function kotak2()
	{
		if (document.frmParam.elements['pemegang.mspo_leader'].value != "")
		{
			listdataagenleader(document.frmParam.elements['pemegang.mspo_leader'].value);
		}
	}
	
	function brok()
	{
		if (document.frmParam.elements['broker.lsb_id'].value != "")
		{
			listdatabroker(document.frmParam.elements['broker.lsb_id'].value);
		}
	}

 function cpy(hasil)
 {
	document.frmParam.elements['pemegang.mspo_jenis_terbit'].value = hasil;
 }	
		
	
// -->
</script>

<script language="JavaScript">
<!--
	if (!document.layers&&!document.all&&!document.getElementById)
		event="test"
		
	function showtip(current,e,text){
	
		if (document.all||document.getElementById){
			thetitle=text.split('<br>')
			if (thetitle.length>1){
				thetitles=''
				
			for (i=0;i<thetitle.length;i++)
				thetitles+=thetitle[i]
				
			current.title=thetitles
		}
		else
			current.title=text
	}
	
	else if (document.layers){
				document.tooltip.document.write('<layer bgColor="white" style="border:1px solid black;font-size:13px;">'+text+'</layer>')
				document.tooltip.document.close()
				document.tooltip.left=e.pageX+5
				document.tooltip.top=e.pageY+5
				document.tooltip.visibility="show"
			}
	}
	
	function hidetip(){
		if (document.layers)
			document.tooltip.visibility="hidden"
	}
			
	function CalcKeyCode(aChar) {
	  var character = aChar.substring(0,1);
	  var code = aChar.charCodeAt(0);
	  return code;
	}
	
	function checkNumber(val) {
	  var strPass = val.value;
	  var strLength = strPass.length;
	  var lchar = val.value.charAt((strLength) - 1);
	  var cCode = CalcKeyCode(lchar);
	
	  /* Check if the keyed in character is a number
	     do you want alphabetic UPPERCASE only ?
	     or lower case only just check their respective
	     codes and replace the 48 and 57 */
	
	  if (cCode < 48 || cCode > 57 ) {
	    var myNumber = val.value.substring(0, (strLength) - 1);
	    val.value = myNumber;
	  }
	  return false;
	}
	
	function getReferal(id_ref){
		var nama_p = document.frmParam.elements['pemegang.mcl_first'].value;
		var tgl_lahir_p = document.frmParam.elements['pemegang.mspe_date_birth'].value;
		var nama_t = document.frmParam.elements['tertanggung.mcl_first'].value;
		var tgl_lahir_t = document.frmParam.elements['tertanggung.mspe_date_birth'].value;
		
		ajaxManager.Id_Ref(id_ref, nama_p, tgl_lahir_p, nama_t, tgl_lahir_t,
		{callback:function(map) {
	
			DWRUtil.useLoadingMessage();
			document.frmParam.elements['agen.id_ref'].value = map.id_ref;
			document.frmParam.elements['agen.name_ref'].value = map.name_ref;
			document.frmParam.elements['agen.jenis_ref'].value = map.jenis_ref;
			var pesan = map.pesan;
			
			if(pesan!=null)alert(pesan);
		},
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		}); 
	}
	
	
	//var jmlhadiah = 0;
	<spring:bind path="cmd.pemegang.unit"> 
		var jmlhadiah=${status.value};
	</spring:bind>
	var flag_add1=0;
	
	function addRowDOM1 (tableID) {
		flag_add1=1;
   		var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
   		var tmp = 0;
   		var idx = null;
   		
		tmp = parseInt(document.getElementById("tableHadiah").rows.length);
		idx = jmlhadiah + 1;
   			
		var oRow = oTable.insertRow(oTable.rows.length);
        var oCells = oRow.cells;
		var i=tmp-1;
		var harga = document.getElementById('lh_harga').value;
		
		if (document.all) {
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td >"+idx+"</td>";
			
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td ><div id=hadiahmenu"+idx+"><input type=text name='hadiahharga"+idx+"' value='1' size='20'></div></td>";
			//<select name='hadiah.mh_no"+idx+"'><option>ALL</option></select>
			
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td ><input type=text name='hadiah.mh_quantity"+idx+"' value='1' size='3'></td>";         
			
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML ="<td ><input type=checkbox name=cek"+idx+" id=ck"+idx+"' class=\"noBorder\" ></td></tr>";
					
			var cell = oRow.insertCell(oCells.length);	
			jmlhadiah = idx;
			
			document.getElementById( 'jmlhadiah' ).value = jmlhadiah;
		};
		
		ajaxSelectHadiah(harga,'select_hadiah_ps', 'hadiahmenu'+idx, 'hadiah.mh_no'+idx,'hadiah.mh_no'+idx);
 	}
 	
 	function cancel1()
	{		
		var idx= null;
		idx = jmlhadiah;
		
		if  ( (idx)!="")
		{
			if (idx >0)
				flag_add1=1;
		}
		if(flag_add1==1)
		{
			deleteRowDOM1('tableHadiah');
		};
	}		
	
	function deleteRowDOM1 (tableID)
	 { 
		var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
	    var row=parseInt(document.getElementById("tableHadiah").rows.length);
		var flag_row=0;
		var jumlah_cek=0;
		var idx = jmlhadiah;
	
		if(row>0)
		{
			flag_row=0;
			for (var i=1;i<((parseInt(row)));i++)
			{
				if (eval("document.frmParam.elements['cek"+i+"'].checked"))
				{
					idx=idx-1;
					for (var k =i ; k<((parseInt(row))-1);k++)
					{
						eval(" document.frmParam.elements['hadiah.mh_no"+k+"'].value =document.frmParam.elements['hadiah.mh_no"+(k+1)+"'].value;");   
						eval(" document.frmParam.elements['hadiah.mh_quantity"+k+"'].value =document.frmParam.elements['hadiah.mh_quantity"+(k+1)+"'].value;"); 
						eval(" document.frmParam.elements['cek"+k+"'].checked = document.frmParam.elements['cek"+(k+1)+"'].checked;");
					}
					oTable.deleteRow(parseInt(document.getElementById("tableHadiah").rows.length)-1);
					row=row-1;
					//alert(mn);
					jmlhadiah = idx;
					//alert(mn);
					i=i-1;
					
					document.getElementById( 'jmlhadiah' ).value = jmlhadiah;
				}
			}
	
			row=parseInt(document.getElementById("tableHadiah").rows.length);
				if(row==1)	
					flag_add=0;
			
		}
	}
	
	function ajaxSelectHadiah(lh_harga,selectType, selectLocation, selectName, selectValue){
		
		ajaxManager.select_hadiah_ps(lh_harga,
			{callback:function(map) {
				DWRUtil.useLoadingMessage();
				if (map=='' || map==null){
					alert('Tidak ada data');
				}else{
					var text = '<select name="'+selectName+'">';
					for(var i=0 ; i<map.length ;i++){
						text += '<option value="'+map[i].LH_ID+'~'+map[i].LH_HARGA+'"';
						if(selectValue==map[i].LH_ID) text += ' selected ';
						text += '>'+map[i].LH_NAMA+' ('+(map[i].LH_HARGA).formatMoney(0, '.', ',')+')</option>';
					}
					text += '</select>';
					
					if(document.getElementById(selectLocation)){
						document.getElementById(selectLocation).innerHTML=text;
					}
				}
		   },
		  	timeout:60000,
		  	errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
		
	}
	
	Number.prototype.formatMoney = function(c, d, t){ 
		var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0; 
	   	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : ""); 
	 }; 
	
	function cek_hadiah(ncek){
		if(ncek==true){ 
			alert('Default hadiah telah dipilih');
		}else{
			alert('Silahkan pilih hadiah');
		}
		
		if(ncek) {
			document.getElementById('hadiah_standard').value = '1'; 
			document.getElementById( 'hadiah_2' ).style.display = 'none';
		} else {
			document.getElementById('hadiah_standard').value = '0';
			document.getElementById( 'hadiah_2' ).style.display = 'block';
		} 
	}
	
	function edit_onClick(){
			with(document.frmParam){
			if(edit_agen.checked==true){
				document.getElementById('edit_agen').checked=true;
				document.getElementById('edit_agen').value='1';
				elements['agen.edit_agen'].value = '1';
			}else{
				document.getElementById('edit_pemegang').value='0';
				elements['agen.edit_agen'].value = '0';	
			}
		}
	} 
		
// -->
</script>

<body bgcolor="ffffff" onLoad="Body_onload();">

 <XML ID=xmlData></XML>
<table border="0" width="100%" height="677" cellspacing="0" cellpadding="0" class="entry2">
  <tr > 
    <td width="67%" rowspan="5" align="left" valign="top"> 
      <form name="frmParam" method="post" >
        <table border="0" width="100%" cellspacing="0" cellpadding="0" height="47" class="entry">

  <tr> 
            <td width="67%" height="53" bgcolor="#FDFDFD"> 
 			<input type="submit" name="_target0" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/pp1.jpg);"
				accesskey="1" onmouseover="return overlib('Alt-1', AUTOSTATUS, WRAP);" onmouseout="nd();">				
			<c:choose><c:when test="${(sessionScope.currentUser.jn_bank eq '16')}">
			<input type="submit" name="_target1" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ttgbsim.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();"></c:when><c:otherwise>
			<input type="submit" name="_target1" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ttg1.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();"></c:otherwise></c:choose>			
			<input type="submit" name="_target1" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ppremi1.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target2" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ddu1.jpg);"
				accesskey="3" onmouseover="return overlib('Alt-3', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target3" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/inv1.jpg);"
				accesskey="4" onmouseover="return overlib('Alt-4', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target4" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ag2.jpg);"
				accesskey="5" onmouseover="return overlib('Alt-5', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target5" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/kf1.jpg);"
				accesskey="6" onmouseover="return overlib('Alt-6', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" name="dummy2" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/sb1.jpg);">		
              </td>
  </tr>
      <tr > 
    <td width="67%"  bgcolor="#DDDDDD" valign="top">&nbsp; 
  
     </td>

  </tr>
</table>

		<table border="0" width="100%"  cellspacing="0" cellpadding="0" class="entry">

          <tr >
			<td>		     
			<spring:bind path="cmd.*">
				<c:if test="${not empty status.errorMessages}">
				<div id="error">
				INFO:<br>
				<c:forEach var="error" items="${status.errorMessages}">
				- <c:out value="${error}" escapeXml="false" />
				<br />
				</c:forEach></div>
				</c:if>									
			</spring:bind> 
			</td>
			</tr><tr>
            <td  colspan="4" height="20"> 
              <div align="center">
                <input type="submit" name="_target${halaman-1}" value="Prev &laquo;"
				accesskey="P" onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
                <input type="submit" name="_target${halaman+1}" value="Next &raquo;" onClick="next()" 
				accesskey="N" onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
                <input type="hidden" name="_page" value="${halaman}">
              </div>
            </td>
          </tr>
          <tr >
            <td colspan="4" height="20">&nbsp;</td>
          </tr>
          <tr > 
            <th class="subtitle" colspan="4" height="20" > 
              <p align="center"><b> <font face="Verdana" size="1" color="#FFFFFF">E. 
                PERNYATAAN PENUTUP (DIISI OLEH PENUTUP)</font></b> 
				<spring:bind path="cmd.datausulan.flag_as"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }"  >
                </spring:bind>
                <spring:bind path="cmd.currentUser.jn_bank"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }"  >
                </spring:bind>
				<spring:bind path="cmd.pemegang.lca_id"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }"  >
                </spring:bind>
				<spring:bind path="cmd.datausulan.mspr_premium"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30">
               </spring:bind>
			   <spring:bind path="cmd.datausulan.flag_worksite"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30">
               </spring:bind>
               	<spring:bind path="cmd.pemegang.reg_spaj"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30">
               </spring:bind>
                <spring:bind path="cmd.powersave.mps_employee"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30">
               </spring:bind>
               <spring:bind path="cmd.datausulan.lsbs_id"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30">
               </spring:bind>	
            </th>
          </tr>
        </table>
        <table border="0" width="100%" cellspacing="0" cellpadding="0" class="entry">
          <tr > 
            <th ><b><font face="Verdana" size="1" color="#996600">1. </font></b></th>
            <th ><b><font color="#996600" size="1" face="Verdana">Tanggal SPAJ</font></b></th>
            <th colspan="5"> <spring:bind path="cmd.pemegang.mspo_spaj_date"> 
              <script>inputDate('${status.expression}', '${status.value}', false);</script>
             </spring:bind><spring:bind path="cmd.pemegang.mspo_ao"><font color="#CC3300">*</font> 
              </spring:bind> <spring:bind path="cmd.pemegang.mspo_spaj_date"><c:if test="${ not empty status.errorMessage}"> 
              </c:if> </spring:bind>
			</th>
          </tr>
          <tr > 
            <th width="21" ><b> <font face="Verdana" size="1" color="#996600">2</font><font face="Verdana" size="1" color="#996600">. 
              </font></b></th>
            <th width="216" ><b> <font face="Verdana" size="1" color="#996600">Kode 
              Regional</font></b></th>
            <th width="96"> <spring:bind path="cmd.agen.kode_regional"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="10" style='background-color :#D4D4D4'readOnly>
              </spring:bind> </th>
            <th width="165" ><b> <font face="Verdana" size="1" color="#996600">Nama 
              Regional</font></b></th>
            <th colspan="3" ><spring:bind path="cmd.agen.lsrg_nama"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="50" style='background-color :#D4D4D4'readOnly>
              </spring:bind></th>
          </tr>
          <tr > 
            <th width="21">&nbsp;</th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600"><c:if test="${cmd.datausulan.lsbs_id eq 120 }"> 
											Kode BAC</c:if>
											<c:if test="${cmd.datausulan.lsbs_id ne 120 }"> 
											Kode Penutup</c:if> </font><font face="Verdana" size="1"> </font></b>
                <a href="#" onclick="var msag_id=document.frmParam.elements['agen.msag_id'].value;popWin('${path}/bac/multi.htm?window=cek_struktur&msag_id='+msag_id, 350, 450);">
                    (cek struktur agen)
                </a>
              </th>
            <th width="96"><spring:bind path="cmd.agen.msag_id"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="10" onchange="kotak()" maxlength="6" 
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
              <font color="#CC3300">*</font> </spring:bind>
                <!--todo-->
              </th>
            <th width="165"><font size="1" face="Verdana"><b><font face="Verdana" size="1" color="#996600"><c:if test="${cmd.datausulan.lsbs_id eq 120 }"> 
											Nama BAC</c:if>
											<c:if test="${cmd.datausulan.lsbs_id ne 120 }"> 
											Nama Penutup</c:if> </font></b> </font>
              </th>
            <th colspan="3"> <spring:bind path="cmd.agen.mcl_first"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="50" maxlength="100" 
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
              </spring:bind> </th>
          </tr>
          <tr > 
            <th width="21">&nbsp;</th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">Kode Leader
              Penutup</font><font face="Verdana" size="1"> </font></b>
            <a href="#" onclick="var msag_id=document.frmParam.elements['agen.msag_id'].value;popWin('${path}/bac/multi.htm?window=cek_struktur&msag_id='+msag_id, 350, 450);">
                    (cek struktur agen)
                </a>
            </th>
            <th width="96"><spring:bind path="cmd.pemegang.mspo_leader"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="10" onchange="kotak2()" maxlength="6" 
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
              <font color="#CC3300">*</font> </spring:bind></th>
            <th colspan="3"> <spring:bind path="cmd.pemegang.nama_leader"> 
              <input type="hidden" name="${status.expression}"  
						value="${status.value }"  size="50" maxlength="100" 
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
              </spring:bind> </th>
            <th colspan="1">&nbsp;</th>
          </tr>
          <tr > 
            <th width="21"><b><font face="Verdana" size="1" color="#996600">3.</font></b></th>
            <th width="216"><b><font size="1" face="Verdana" color="#996600">AO 
              </font></b></th>
            <th width="96"> <input type="checkbox" name="mspo_ref_bii1" class="noBorder" 
						value="${cmd.pemegang.mspo_ref_bii}"  size="30" onClick="ao_onClick();" 
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> 
              <spring:bind path="cmd.pemegang.mspo_ref_bii"> 
              <input type="hidden" name="${status.expression}"
						value="${cmd.pemegang.mspo_ref_bii}"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind> </th>
            <th width="165"><b><font size="1" face="Verdana" color="#996600">Kode 
              AO </font></b></th>
            <th width="160"> <spring:bind path="cmd.pemegang.mspo_ao"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="10" onchange="kotak1()" maxlength="6" 
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
              <font color="#CC3300">*</font>  </spring:bind> </th>
            <th width="67"><b><font size="1" face="Verdana" color="#996600">Nama 
              AO </font></b></th>
            <th width="261"> <spring:bind path="cmd.pemegang.nama_ao"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30"  maxlength="6" style='background-color :#D4D4D4' readOnly   >
               </spring:bind> </th>
          </tr>
          <tr > 
            <th width="21"><b><font face="Verdana" size="1" color="#996600">4. 
              </font></b></th>
            <th width="216"><b><font size="1" face="Verdana" color="#996600">Pribadi</font></b> 
            </th>
            <th width="96"> <input type="checkbox" name="mspo_pribadi1" class="noBorder" 
						value="${cmd.pemegang.mspo_pribadi}"  size="30" onClick="pribadi_onClick();" 
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> 
              <spring:bind path="cmd.pemegang.mspo_pribadi"> 
              <input type="hidden" name="${status.expression}"
						value="${cmd.pemegang.mspo_pribadi}"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind> </th>
            <th width="165" colspan="4"> 
				<span class="info">* Untuk tutupan PRIBADI</span>
              <!--<b><font size="1" face="Verdana" color="#996600">Follow 
              Up</font></b> 
              <select name="pemegang.mspo_follow_up" >
                <c:forEach var="follow" items="${select_followup}"> <option 
							<c:if test="${cmd.pemegang.mspo_follow_up eq follow.ID}"> SELECTED </c:if>
							value="${follow.ID}">${follow.FOLLOWUP}</option> 
                </c:forEach> 
              </select>-->
            </th>
          </tr>
          <tr > 
            <th width="21"><b><font face="Verdana" size="1" color="#996600">5. 
              </font></b></th>
            <th width="216"><b><font size="1" face="Verdana" color="#996600">Kode 
              Regional Penagihan</font></b></th>
            <th width="96"> <input type="text" name="kode_regional" value="${cmd.addressbilling.region}"  size="10" style='background-color :#D4D4D4'readOnly> 
            </th>
            <th width="165"><b><font size="1" face="Verdana" color="#996600">Nama 
              Regional Penagihan</font></b></th>
            <th colspan="3"> <select name="addressbilling.region" 
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <c:forEach var="regional" items="${select_regional}"> <option 
							<c:if test="${cmd.addressbilling.region eq regional.key}"> SELECTED </c:if>
							value="${regional.key}">${regional.value}</option> 
                </c:forEach> </select> </th>
          </tr>
          
		<!-- MANTA -->
		<tr> 
			<th width="21"><b><font face="Verdana" size="1" color="#996600">6. </font></b></th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">Broker</font></b></th>
            <th width="96">
				<input type="checkbox" name="flagbroker1" class="noBorder" value="${cmd.broker.flagbroker}"  size="30" onClick="broker_onClick();" 
				<c:if test="${ not empty status.errorMessage}">
					style='background-color :#FFE1FD'
				</c:if>> 
				<spring:bind path="cmd.broker.flagbroker">
					<input type="hidden" name="${status.expression}" value="${cmd.broker.flagbroker}"  size="30" style='background-color :#D4D4D4'readOnly>
				</spring:bind>
			</th>
			<th colspan="4">&nbsp;</th>
		</tr>
		<tr>
			<th></th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">Kode Broker</font><font face="Verdana" size="1"> </font></b></th>
            <th width="96">
            	<spring:bind path="cmd.broker.lsb_id"> 
					<input type="text" name="${status.expression}"
						value="${status.value }"  size="10" onchange="brok()" maxlength="4" 
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
				</spring:bind>
			</th>	
            <th width="165"><font size="1" face="Verdana"><b><font face="Verdana" size="1" color="#996600">Nama Broker</font></b></font></th>
            <th colspan="3">
            	<spring:bind path="cmd.broker.lsb_nama"> 
					<input type="text" name="${status.expression}"
						value="${status.value }"  size="50" maxlength="100" style='background-color :#D4D4D4' readOnly>
            	</spring:bind>
            </th>
		</tr>
		<tr>          
			<th></th>
			<th width="216"><b><font face="Verdana" size="1" color="#996600">Cari Bank</font><font face="Verdana" size="1"> </font></b></th>
            <th height="20" colspan="1">
            	<input type="text" name="caribank2" onkeypress="if(event.keyCode==13){ document.frmParam.btncari2.click(); return false;}"> 
				<input type="button" name="btncari2" value="Cari" onclick="ajaxSelectWithParamMuamalat(document.frmParam.caribank2.value,'select_bank2','bank2','broker.lbn_id','', 'BANK_ID', 'BANK_NAMA', '','Silahkan pilih BANK','32',${cmd.datausulan.lsbs_id},${cmd.datausulan.lsdbs_number});">  
            </th>
          	<th height="20"><b><font face="Verdana" size="1" color="#996633">Bank&nbsp; 
				</font><font face="Verdana" size="1" color="#996600"> </font></b>
            </th>
            <th height="20" >
            	<div id="bank2"> 
					<select name="broker.lbn_id"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
	                	<option value="${cmd.broker.lbn_id}">${cmd.broker.lbn_nama}</option>
                	</select>
                	<font color="#CC3300">*</font>
                </div>
            </th>
            <th colspan="2">&nbsp;</th>
		</tr>
		<tr>          
			<th></th>
			<th width="216"><b><font face="Verdana" size="1" color="#996600">No Rekening Broker</font><font face="Verdana" size="1"> </font></b></th>
            <th colspan="5">
				<spring:bind path="cmd.broker.no_account"> 
					<input type="text" name="${status.expression}" value="${status.value }"  size="30" maxlength="12" 
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
               		<font color="#CC3300">*</font> 
				</spring:bind>
            </th>
		</tr>
		<tr> 
			<th width="21"><b><font face="Verdana" size="1" color="#996600">7. </font></b></th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">ID Sponsor</font></b></th>
			<th width="96">
				<spring:bind path="cmd.pemegang.mspo_id_sponsor"> 
              		<input type="text" name="${status.expression}" value="${status.value}" size="10"
				 	<c:if test="${not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind>
			</th>
			
            <th width="165"><b><font face="Verdana" size="1" color="#996600">ID Penempatan</font></b></th>
            <th colspan="3">
            	<spring:bind path="cmd.pemegang.mspo_id_place"> 
					<input type="text" name="${status.expression}" value="${status.value}" size="10" 
					<c:if test="${not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind>
			</th>
		</tr>
		
          <tr > 
            <th colspan="7" class="subtitle"  height="20"> <p align="center"><b> 
                <font face="Verdana" size="1" color="#FFFFFF">INFORMASI KHUSUS 
                KARYAWAN AJ Sinarmas SEBAGAI PEMEGANG POLIS</font></b> </p></th>
          </tr>
          <tr > 
            <th width="21"><b><font face="Verdana" size="1" color="#996600">1. 
              </font></b></th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">NIK 
              AJ Sinarmas <br>
              ( Khusus Excellink Karyawan)&nbsp; </font></b></th>
            <th colspan="5"><spring:bind path="cmd.employee.nik"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" maxlength="10" 
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
               <font color="#CC3300">*</font> 
 			</spring:bind></th>
          </tr>
          <tr > 
            <th width="21">&nbsp;</th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">Nama 
              Karyawan&nbsp; </font></b></th>
            <th colspan="5"><spring:bind path="cmd.employee.nama"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind></th>
          </tr>
          <tr > 
            <th width="21">&nbsp;</th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">Cabang 
              </font></b></th>
            <th colspan="5"><spring:bind path="cmd.employee.cabang"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind></th>
          </tr>
          <tr > 
            <th width="21">&nbsp;</th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">Departemen</font></b></th>
            <th colspan="5"><spring:bind path="cmd.employee.dept"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind></th>
          </tr>
          <tr > 
            <th width="21"><b><font face="Verdana" size="1" color="#996600">2</font><font face="Verdana" size="1" color="#996600">. 
              </font></b></th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">Premi 
              Ke</font></b></th>
            <th width="96">
              <input type="text" name="cmd.employee.no_urut" value="${cmd.employee.no_urut}" style="background-color :#D4D4D4" readonly="readonly"/>	
              </th>
            <th width="165"><b><font face="Verdana" size="1" color="#996600">Potongan</font> 
              </b></th>
            <th width="160">
              <input type="text" style="background-color :#D4D4D4"  readOnly name='cmd.employee.potongan' size="20" value =<fmt:formatNumber type="number" value="${cmd.employee.potongan}"/> >
              </th>
            <th width="67"><b><font face="Verdana" size="1" color="#996600">Tanggal 
              Proses</font></b></th>
            <th width="261"> <spring:bind path="cmd.employee.tgl_proses"> 
              <script>('${status.expression}', '${status.value}', true);</script>
              </spring:bind> </th>
          </tr>
          <tr > 
            <th colspan="7" class="subtitle"  height="20"> <p align="center"><b> 
                <font face="Verdana" size="1" color="#FFFFFF">INFORMASI KHUSUS 
                PRODUK WORKSITE, PERUSAHAAN BELI PRODUK INDIVIDU & KARYAWAN AJS</font></b> </p></th>
          </tr>
          <tr > 
            <th width="21"><b><font face="Verdana" size="1" color="#996600">1. 
              </font></b></th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">Cari perusahaan</font></b></th>
<!--            <th colspan="5"> <input type="text" name="cariperusahaan1" onkeypress="if(event.keyCode==13){ document.frmParam.btnperusahaan1.click(); return false;}"> -->
<!--              <input type="button" name="btnperusahaan1" value="Cari"  onclick="ajaxSelectWithParam(document.frmParam.cariperusahaan1.value,'select_company','perush','pemegang.mspo_customer','', 'COMPANY_ID', 'COMPANY_NAMA', '');"> -->
<!--				<input type="checkbox" name="mste_flag_el1" class="noBorder" -->
<!--						value="${cmd.datausulan.mste_flag_el}"  size="30" onClick="karyawan_onClick();" -->
<!--				 <c:if test="${ not empty status.errorMessage}">-->
<!--						style='background-color :#FFE1FD'-->
<!--					</c:if>> -->
<!--              <spring:bind path="cmd.datausulan.mste_flag_el"> -->
<!--              <input type="hidden" name="${status.expression}"-->
<!--						value="${cmd.datausulan.mste_flag_el}"  size="30" style='background-color :#D4D4D4'readOnly>-->
<!--              <b><font face="Verdana" size="1" color="#996600">Karyawan AJ Sinarmas</font></b></spring:bind> -->
<!--            </th>-->
            <th colspan="5">
            <input type="text" name="cariperusahaan1" onkeypress="if(event.keyCode==13){ document.frmParam.btnperusahaan1.click(); return false;}">
              <input type="button" name="btnperusahaan1" value="Cari"  onclick="ajaxSelectWithParam(document.frmParam.cariperusahaan1.value,'select_company','perush','pemegang.mspo_customer','', 'COMPANY_ID', 'COMPANY_NAMA', '');"> 
				<spring:bind path="cmd.datausulan.mste_flag_el">
			           	<label for="flagkrywn1"><input id="flagkrywn1" name="${status.expression}" type="radio" class="noBorder" value="0" onclick="karyawan_onClick();">Non Karyawan</label>
			           	<label for="flagkrywn2"><input id="flagkrywn2" name="${status.expression}" type="radio" class="noBorder" value="1" onclick="karyawan_onClick();">Karyawan AJS MSIG</label>
			            <label for="flagkrywn3"><input id="flagkrywn3" name="${status.expression}" type="radio" class="noBorder" value="2" onclick="karyawan_onClick();">Keluarga Karyawan AJS MSIG</label>
	           			<label for="flagkrywn4"><input id="flagkrywn4" name="${status.expression}" type="radio" class="noBorder" value="3" onclick="karyawan_onClick();">Teman Karyawan AJS MSIG</label>
	           			<label for="flagkrywn5"><input id="flagkrywn5" name="${status.expression}" type="radio" class="noBorder" value="4" onclick="karyawan_onClick();">Agent Berlisensi AJS MSIG</label>
	           			<input type="hidden" name="flag_karyawan" id="flag_karyawan"
						value="${cmd.datausulan.mste_flag_el}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>    
            </th>
          </tr>
          <tr > 
            <th width="21"><b><font face="Verdana" size="1" color="#996600"> </font></b></th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">Nama 
              perusahaan </font></b></th>
            <th colspan="5"><div id="perush"> 
                <select name="pemegang.mspo_customer"
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                  <option value="${cmd.pemegang.mspo_customer}">${cmd.pemegang.mspo_customer_nama}</option>
                  <c:if test="${ not empty status.errorMessage}"><a href="#"><img src="${path}/include/image/false.gif" width="11" height="11"  border="0" onMouseOver="showtip(this,event,'${status.errorMessage }')" onMouseOut="hidetip()" onClick="showtip(this,event,'${status.errorMessage }')"></a> 
                  </c:if> 
                </select>
                <font color="#CC3300">*</font></div></th>
          </tr>
          <tr > 
            <th width="21"><b><font face="Verdana" size="1" color="#996600">2. 
              </font></b></th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">NIK 
              KARYAWAN </font></b></th>
            <th colspan="5"><spring:bind path="cmd.pemegang.nik"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" maxlength="10" 
					 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
               <font color="#CC3300">*</font> 
               </spring:bind></th>
          </tr>
  			<spring:bind path="cmd.datausulan.mspr_discount"> 
              <input type="hidden" name="${status.expression}" style='background-color :#D4D4D4'
						value="${status.value }"  size="30" maxlength="10" >
              <c:if test="${ not empty status.errorMessage}"><a href="#"><img src="${path}/include/image/false.gif" width="11" height="11"  border="0" onMouseOver="showtip(this,event,'${status.errorMessage }')" onMouseOut="hidetip()" onClick="showtip(this,event,'${status.errorMessage }')"></a> 
              </c:if> </spring:bind>
          <tr  > 
        
        <tr > 
            <th colspan="7" class="subtitle"  height="20"> <p align="center"><b> 
                <font face="Verdana" size="1" color="#FFFFFF">KHUSUS MNC</font></b> </p>
            </th>
        </tr>
        <tr > 
            <tr > 
	            <th ><b><font face="Verdana" size="1" color="#996600">1. </font></b></th>
	            <th colspan="6"><b><font face="Verdana" size="1" color="#996600" >SPECIAL OFFER MNC :
            </tr> 
            <tr > 
	            <th ><b><font face="Verdana" size="1" color="#996600"> </font></b></th>
	            <th colspan="6">
	            <b><font face="Verdana" size="1" color="#996600">
		            <spring:bind path="cmd.tertanggung.mste_flag_special_offer">
							<label for="nonso"> <input type="radio" class=noBorder
								name="${status.expression}" value="0" onClick="cpy('0');"
								<c:if test="${cmd.tertanggung.mste_flag_special_offer eq 0 or cmd.tertanggung.mste_flag_special_offer eq null}"> 
											checked</c:if>
								id="nonso">Non Special Offer </label>
							<br>
							<label for="cashbackso"> <input type="radio" class=noBorder
								name="${status.expression}" value="1" onClick="cpy('1');"
								<c:if test="${cmd.tertanggung.mste_flag_special_offer eq 1}"> 
											checked</c:if>
							id="cashbackso">Lucky Draw</label> 
							<label for="additionalunitso"> <input type="radio" class=noBorder
								name="${status.expression}" value="2" onClick="cpy('2');"
								<c:if test="${cmd.tertanggung.mste_flag_special_offer eq 2}"> 
											checked</c:if>
							id="additionalunitso">Additional Unit</label> 
					</spring:bind> 
	            </font></b>
	            </th>
          	</tr>
        </tr>
        
        <!-- PROGRAM HADIAH STABLE SAVE -->
        <tr>
        	<th colspan="7">
        		<div id="ps_hadiah" style="display: none;">
        		<table width="100%">
        			<tr > 
			            <th colspan="7" class="subtitle"  height="20"> <p align="center"><b> 
			                <font face="Verdana" size="1" color="#FFFFFF">KHUSUS STABLE SAVE BERHADIAH</font></b> </p>
			            </th>
			        </tr>
			        <tr> 
			            <th class="subtitle" align="center" colspan="7"> 
			             	<input name="btnadd1" type="button" id="btnadd1" value="ADD" onClick="addRowDOM1('tableHadiah')"> &nbsp;&nbsp;&nbsp;&nbsp; 
			             	<input name="btn_cancel1" type="button" id="btn_cancel1" value="DELETE" onClick="cancel1()">			
			            	<input type="hidden" name="jmlhadiah" id="jmlhadiah"  value="${cmd.pemegang.unit}" >
			            	<input type="hidden" name="lh_harga" id="lh_harga"  value="${lh_harga }" >
			            	<spring:bind path="cmd.pemegang.flag_hadiah"> 
								<input type="hidden" name="${status.expression}" value="${status.value }"  size="2" >
						 	</spring:bind>
			            </th>
			        </tr>
			        <tr>
        				<th colspan="7">
        					<!-- <input type="checkbox" name="hadiah_standart" id="hadiah_standart" style="border: none;"> <em> *ceklist untuk memilih hadiah standart</em> -->
        					<spring:bind path="cmd.pemegang.flag_standard">
								<input type="checkbox" class="noborder" value="1" onclick="cek_hadiah(this.checked);"
			            			<c:if test="${status.value eq 1}">checked="checked"</c:if>> <em> *ceklist untuk memilih hadiah standart</em>
								<input type="hidden" id="hadiah_standard" name="${status.expression}" value="${status.value}">
			            	</spring:bind>
			            	<div id="hadiah_2" style="display: block;">
        					<table ID="tableHadiah" width="100%" border="0" cellspacing="1" cellpadding="1" class="entry">
				       			<tr > 
				         			<th class="subtitle2" style="text-align: center;"><font size="1"><b><font face="Verdana" color="#FFFFFF">No</font></b></font></th>
				       				<th class="subtitle2" style="text-align: center;"><font size="1"><b><font face="Verdana" color="#FFFFFF">Hadiah</font></b></font><font color="#CC3300">*</font></th>
				         			<th class="subtitle2" style="text-align: center;"> <font size="1"><b><font face="Verdana" color="#FFFFFF">Unit</font><font color="#CC3300">*</font></b></font></th>
									<th class="subtitle2" style="text-align: center;">Cek</th>
								</tr>
				    			<c:forEach items="${cmd.pemegang.daftar_hadiah}" var="hadiah" varStatus="status">
								<tr> 
					  				<th> ${status.count}</th>
					        		<th>
										<select name="hadiah.mh_no${status.index +1}">
											<c:forEach var="hdh" items="${select_hadiah}">
												<option 
													<c:if test="${hadiah.lh_id eq hdh.LH_ID}"> SELECTED </c:if>
													value="${hdh.LH_ID}~${hdh.LH_HARGA}">${hdh.LH_NAMA} (<fmt:formatNumber>${hdh.LH_HARGA}</fmt:formatNumber>)</option>
											</c:forEach>
										</select>
					   				</th>
									<th> 
										<input type="text" name="hadiah.mh_quantity${status.index +1}" value ="${hadiah.mh_quantity}" size="3" maxlength="3">
					        		</th>
					  				<th><input type=checkbox name="cek${status.index +1}" id= "ck${status.index +1}" class="noBorder" ></th>
					
								</tr>
								</c:forEach>		 
				  			</table>
				  			</div>
        				</th>
        			</tr>
        		</table>
        		</div>
  			</th>
        </tr>
        <!-- END PROGRAM HADIAH -->
        
        <tr > 
            <th colspan="7" class="subtitle"  height="20"> <p align="center"><b> 
                <font face="Verdana" size="1" color="#FFFFFF">PERNYATAAN DAN PERSETUJUAN</font></b> </p></th>
          </tr>
          <tr > 
          <tr > 
            <th ><b><font face="Verdana" size="1" color="#996600">1. </font></b></th>
            <th colspan="6"><b><font face="Verdana" size="1" color="#996600" >Menyetujui bahwa Polis akan diterbitkan dalam bentuk : <font color="#CC3300">*</font></font></b></th>
            
          </tr> 
          <tr > 
             <th ><b><font face="Verdana" size="1" color="#996600"> </font></b></th>
            <th colspan="6"><b><font face="Verdana" size="1" color="#996600">
            <spring:bind path="cmd.pemegang.mspo_jenis_terbit">
					<label for="hardcopy"> <input type="radio" class=noBorder
						name="${status.expression}" value="0" onClick="cpy('0');"
						<c:if test="${cmd.pemegang.mspo_jenis_terbit eq 0 or cmd.pemegang.mspo_jenis_terbit eq null}"> 
									checked</c:if>
						id="hardcopy">Hard Copy dengan membayar biaya polis sesuai dengan jenis produk yang diambil. </label>
					<br>
					<label for="softcopy"> <input type="radio" class=noBorder
						name="${status.expression}" value="1" onClick="cpy('1');"
						<c:if test="${cmd.pemegang.mspo_jenis_terbit eq 1}"> 
									checked</c:if>
						id="softcopy">Softcopy ke alamat e-mail.</label> 
				</spring:bind> 
            </font></b></th>
          </tr>  
          <!-- TAMBANG EMAS -->
          <tr > 
            <th colspan="7" class="subtitle"  height="20"> <p align="center"><b> 
                <font face="Verdana" size="1" color="#FFFFFF">REFERENSI</font></b> </p></th>
          </tr>
          <tr > 
            <th ><b><font face="Verdana" size="1" color="#996600">1.</font></b></th>
            <th><b><font face="Verdana" size="1" color="#996600" >ID Referral</font></b></th>
            <th colspan="5"> <spring:bind path="cmd.agen.id_ref"> 
              <input type="text" name="${status.expression}" value="${status.value }" size="12" onchange="getReferal(this.value);">
              </spring:bind>(isi dengan ID Referral atau Kode Konfirmasi)<em></em> </th>
          </tr>
          <tr > 
            <th ><b><font face="Verdana" size="1" color="#996600">2.</font></b></th>
            <th><b><font face="Verdana" size="1" color="#996600" >Nama Referral</font></b></th>
            <th colspan="5"> <spring:bind path="cmd.agen.name_ref"> 
              <input type="text" name="${status.expression}" value="${status.value }" style='background-color :#D4D4D4' readonly="readonly" size="50">
              </spring:bind> 
              <spring:bind path="cmd.agen.jenis_ref"> 
              <input type="hidden" name="${status.expression}" value="${status.value }" style='background-color :#D4D4D4' readonly="readonly" size="50">
              </spring:bind>
              <spring:bind path="cmd.pemegang.mcl_first"> 
              <input type="hidden" name="${status.expression}" value="${status.value }" style='background-color :#D4D4D4' readonly="readonly" size="50">
              </spring:bind>
              <spring:bind path="cmd.pemegang.mspe_date_birth"> 
              <input type="hidden" name="${status.expression}" value="${status.value }" style='background-color :#D4D4D4' readonly="readonly" size="50">
              </spring:bind>
              <spring:bind path="cmd.tertanggung.mcl_first"> 
              <input type="hidden" name="${status.expression}" value="${status.value }" style='background-color :#D4D4D4' readonly="readonly" size="50">
              </spring:bind>
              <spring:bind path="cmd.tertanggung.mspe_date_birth"> 
              <input type="hidden" name="${status.expression}" value="${status.value }" style='background-color :#D4D4D4' readonly="readonly" size="50">
              </spring:bind>
              <!-- Pesan Referensi -->
			  <spring:bind path="cmd.agen.pesan_ref"> 
			  <input type="hidden" name="${status.expression}" value="${status.value }"  size="20" >
			  </spring:bind>
			  <!-- End Pesan Referensi -->
            </th>
          </tr>
          <!-- END TAMBAHAN -->  
 			
            <th colspan="7" class="subtitle">&nbsp;</th>
          </tr>
		            <tr> 
            <td colspan="9"> 
			 <p align="left"><b> <font face="Verdana" size="2" color="#CC3300">Note : * Wajib diisi</font></b></p>
			</td>
          </tr>
          <c:if test="${cmd.tertanggung.lspd_id eq 2 or cmd.tertanggung.lspd_id eq 27 or cmd.tertanggung.lspd_id eq 209}">
	          <c:if test="${not empty cmd.pemegang.reg_spaj}">
	          <tr>
	          	 <td colspan="2">
	          	 	<input type="checkbox" class="noBorder" name="edit_agen" id="edit_agen" value="${cmd.agen.edit_agen}" <c:if test="${cmd.agen.edit_agen eq 1}"> 
						   checked </c:if> size="30" onClick="edit_onClick();"> Telah dilakukan edit
				   	<spring:bind path="cmd.agen.edit_agen"> 
	          			<input type="hidden" name="${status.expression}" value="${cmd.agen.edit_agen}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>
				 </td>
				 <td colspan="7">
				 	<spring:bind path="cmd.agen.kriteria_kesalahan">
							<select name="${status.expression}">
								<c:forEach var="a" items="${select_kriteria_kesalahan}">
									<option
										<c:if test="${cmd.agen.kriteria_kesalahan eq a.KESALAHAN}"> SELECTED </c:if>
										value="${a.KESALAHAN}">${a.KESALAHAN}
									</option>
								</c:forEach>
							</select>
					</spring:bind>
	          </tr>
	          <tr>
	          	<td colspan="9">&nbsp;</td>
	          </tr>
	          </c:if>
          </c:if>	
          <tr > 
            <td colspan="7"> <input type="hidden" name="hal" value="${halaman}"> 
              <spring:bind path="cmd.pemegang.indeks_halaman"> 
              <input type="hidden" name="${status.expression}"
						value="${halaman-1}"  size="30" >
              </spring:bind> </td>
          </tr>
          <tr > 
            <td colspan="7"> <div align="center"> 
                <input type="submit" name="_target${halaman-1}" value="Prev &laquo;" 
				accesskey="P" onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
                <input type="submit" name="_target${halaman+1}" value="Next &raquo;" onClick="next()" 
				accesskey="N" onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
                <input type="hidden" name="_page" value="${halaman}">
              </div></td>
          </tr>
        </table>
 

      </form>
    </td>

  </tr>
</table>
</body>
<%@ include file="/include/page/footer.jsp"%>