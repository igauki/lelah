<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		$("#tabs").tabs();
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
				
		$("#select_data").click(function() {
			//kopi untuk disubmit (isinya, bukan valuenya)
			$("#namapsn").val($("#select_data option:selected").text());
		});
		
	});
	
	function tampilkan(spajtemp){	
		with(self.opener.document){
			if(getElementById('kopiSPAJ')) getElementById('kopiSPAJ').value=spajtemp;
			if(getElementById('kopiSPAJ')) self.opener.document.getElementById('kopiSPAJ').value=spajtemp;
			getElementById('flag_upload3').value=3;
			self.opener.document.getElementById('flag_upload3').value=3;
			//alert('a');
			//window.location='${path}/bac/editspajnew.htm?kopiSPAJ='+spajtemp+'&flag_upload=3';
		}
		window.close();
	}
	
</script>


<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 100%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

</style>

<body >
	<c:choose>
		<c:when test="${open eq \"0\" }">
			<table class="displaytag" cellpadding='0' cellspacing='0' border="1" align="left">
				<tr>
					<th nowrap align="left" width="300" bgcolor="#b7b7b7">Nama </th>
					<th nowrap align="left" width="100" bgcolor="#b7b7b7">DOB</th>
					<th nowrap align="left" width="300" bgcolor="#b7b7b7">Alamat</th>
					<th nowrap align="left" width="300" bgcolor="#b7b7b7">Alasan</th>
					<th nowrap align="left" width="200" bgcolor="#b7b7b7">User Input</th>
				</tr>
				<c:forEach items="${blacklist}" var="d">
					<tr bgcolor="FFFFCC"
						onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"
						onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;">
						<td nowrap>${d.LBL_NAMA}</td>
						<td nowrap>${d.LBL_TGL_LAHIR}</td>
						<td nowrap><textarea rows="4" cols="200" style="width: 100%; text-transform: uppercase;" name="alamatPemegang">${d.LBL_ALAMAT}</textarea></td>
						<td nowrap><textarea rows="4" cols="200" style="width: 100%; text-transform: uppercase;" name="alamatPemegang">${d.LBL_ALASAN}</textarea></td>
						<td nowrap>${d.LUS_FULL_NAME}</td>
					</tr>
				</c:forEach>
			</table>
		</c:when>
		<c:otherwise>
			<div id="tabs">
				<ul>
					<li><a href="#tab-1">List Data Dari E-SPAJ GADGET</a>
					</li>
				</ul>
				<div id="tab-1">
					<form id="formPost2" name="formPost2" method="post" target="">
						<fieldset class="ui-widget ui-widget-content">
							<table class="displaytag" cellpadding='0' cellspacing='0' border="1" align="left">
								<tr>
									<th nowrap align="center" width="40" bgcolor="#b7b7b7">No</th>
									<th nowrap align="center" width="160" bgcolor="#b7b7b7">Tgl Submit ESPAJ</th>
									<th nowrap align="center" width="160" bgcolor="#b7b7b7">No ESPAJ/Gadget</th>
									<th nowrap align="center" width="200" bgcolor="#b7b7b7">Nama PP</th>
									<th nowrap align="center" width="200" bgcolor="#b7b7b7">Nama TT</th>
									<th nowrap align="center" width="100" bgcolor="#b7b7b7">Distribusi</th>
									<th nowrap align="center" width="150" bgcolor="#b7b7b7">No VA</th>
									<th nowrap align="center" width="100" bgcolor="#b7b7b7">Premi</th>
									<th nowrap align="center" width="110" bgcolor="#b7b7b7">Produk</th>
								</tr>
								<c:set var="xx" value="1" />
								<c:forEach items="${dbpolis}" var="d" varStatus="e">		
									<tr bgcolor="FFFFCC"
										onMouseOver="Javascript:this.bgColor='#FFFFFF';this.style.cursor='hand';return true;"
										onMouseOut="Javascript:this.bgColor='#FFFFCC';return true;"
										onClick="tampilkan('${d.NO_TEMP}');">
										<td nowrap>${e.index+1}</td>
										<td nowrap>${d.CREATED_DATE}</td>
										<td nowrap>${d.NO_TEMP}</td>
										<td nowrap>${d.PP_NAME}</td>
										<td nowrap>${d.TT_NAME}</td>
										<td nowrap>${d.DIST}</td>
										<td nowrap>${d.NO_VA}</td>
										<td nowrap align="right">${d.PREMI}</td>
										<td nowrap>${d.PRODUK}</td>
									</tr>
								</c:forEach>
							</table>
						</fieldset>
						<div class="rowElem">
							<c:forEach items="${pesanList}" var="d" varStatus="st">
								<c:choose>
									<c:when test="${d.sts eq 'FAILED'}">
										<font color="red">${st.index+1}. ${d.msg}</font>
									</c:when>
									<c:otherwise>
										<font color="blue">${st.index+1}. ${d.msg}</font>
									</c:otherwise>
								</c:choose>
								<br />
							</c:forEach>
						</div>
					</form>
				</div>
			</div>
		</c:otherwise>
	</c:choose>
</body>
</html>


