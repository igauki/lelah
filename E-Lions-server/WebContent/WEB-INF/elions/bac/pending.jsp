<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
</script>
<script type="text/javascript">
function cekStatus(flag){
	ajaxSelectWithParam(flag,'selectLstStatusAcceptSub','statusacceptsub','substatus','','SUB_ID','SUB_DESC','');
}
</script>

</head>
<BODY onload="document.title='PopUp :: Status Pending SPAJ'; setupPanes('container1', 'tab1');cekStatus(${status}); " style="height: 100%;">

	<c:choose>
		<c:when test="${not empty initialError}">
			<script>
				alert('${initialError}');
				window.close();
			</script>
		</c:when>
		<c:when test="${not empty sukses}">
			<script>
				alert('${sukses}');
				window.close();
			</script>
		</c:when>
		<c:otherwise>
		
			<div class="tab-container" id="container1">
				<ul class="tabs">
					<li>
						<a href="#" onClick="return showPane('pane1', this)" id="tab1">Akseptasi Manual (Khusus Produk Worksite)</a>
					</li>
				</ul>
		
				<div class="tab-panes">
		
					<div id="pane1" class="panes">
						<form method="post" name="formpost">
							<input type="hidden" name="spaj" value="${spaj}">
							
							<table class="entry2">
								<tr>
									<th>Status</th>
									<td style="text-transform: none;">
										<input id="status1" onclick="cekStatus('9');" type="radio" name="status" value="9" class="noBorder" <c:if test="${status eq 9}">checked</c:if>>
										<label for="status1" onmouseover="return overlib('Standar', AUTOSTATUS, WRAP);" onmouseout="nd();">Postponed</label>
										<input id="status2" onclick="cekStatus('2');" type="radio" name="status" value="2" class="noBorder" <c:if test="${status eq 2}">checked</c:if>>
										<label for="status2" onmouseover="return overlib('Tolak', AUTOSTATUS, WRAP);" onmouseout="nd();">Declined</label>
										<input id="status3" onclick="cekStatus('3');" type="radio" name="status" value="3" class="noBorder" <c:if test="${status eq 3}">checked</c:if>>
										<label for="status3" onmouseover="return overlib('Pending', AUTOSTATUS, WRAP);" onmouseout="nd();">Further Requirement</label>
									</td>
								</tr>
								<tr>
									<th>Sub Keterangan</th>
									<td>
										<div id="statusacceptsub">
											<select name="substatus">
							                  <option value="${substatus.SUB_ID}">${substatus.sub_desc}</option>
							                </select>
						                </div>
									</td>
								</tr>
								<tr>
									<th>Keterangan</th>
									<td>
										<textarea cols="48" rows="5" name="keterangan"
											tabindex="17" onkeyup="textCounter(this, 200); "
											onkeydown="textCounter(this, 200); ">${keterangan}</textarea>
									</td>
								</tr>
								<tr>
									<th></th>
									<td>
										<c:if test="${not empty error}">
											<div id="error">
											ERROR:<br>
											- ${error}
											</div>
										</c:if>	
										<input type="submit" name="save" value="Save" onclick="return confirm('Update Status Polis?');">
										<input type="button" name="close" value="Close" onclick="window.close();">
									</td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
		
		</c:otherwise>
	</c:choose>
</body>
<%@ include file="/include/page/footer.jsp"%>