<%@ include file="/include/page/header.jsp"%>
<style type="text/css">
	html,body{
	     overflow: hidden;
	}
</style>
<script>
	function cekSpaj(){
		if(trim(document.formpost.spaj.value)=='') {
			alert('Harap cari nomor SPAJ terlebih dahulu');
			popWin('${path}/bac/spaj.htm?posisi=-1', 350, 450);
		}else return true;
		
		return false;
	}
	
	function awal(){
		if (document.formpost.spaj.value != "")
		{
			cariregion(document.formpost.spaj.value,'region');
		}
	}

	function monyong(y){
		if('cari'==y) {
			popWin('${path}/bac/spaj.htm?posisi=-1&win=viewer&kata='+document.formpost.spaj.value, 350, 450);
		}else if('spaj'==y) {
			if(cekSpaj()){
				document.getElementById('infoFrame').src='${path}/bac/otorisasiInputSpajView.htm?p=v&showSPAJ='+document.formpost.spaj.value;
				document.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+document.formpost.spaj.value;
			}
		}
		else if('submitSpaj'==y){
			document.getElementById('infoFrame').src='${path}/bac/otorisasiInputSpajView.htm?p=v&showSPAJ='+document.formpost.spaj.value+'&submitSPAJ=yes';
		}
	}

	<c:if test="${not empty cmd.showCsfReminder}">
		popWin('${path}/bac/otorisasiInputSpajParent.HTM?window=csfreminder', 320, 700);
	</c:if>
	
	
	function cariregion(spaj,nama)
	{
			ajaxManager.listcollect(spaj , nama,
			{callback:function(map) {
		
				DWRUtil.useLoadingMessage();
				document.formpost.koderegion.value=map.LSRG_NAMA;
				document.formpost.numberbisnis.value = map.LSDBS_NUMBER;
				document.formpost.kodebisnis.value = map.LSBS_ID;
			   },
			  timeout:15000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
		
</script>
<body 
	onload="setFrameSize('infoFrame', 58); setFrameSize('docFrame', 58);" onresize="setFrameSize('infoFrame', 58); setFrameSize('docFrame', 58);" style="height: 100%;">
<form name="formpost" method="post">
<div class="tabcontent">
<table class="entry2" style="width: 40%;">
	<tr>
		<th style="width: 80px;">SPAJ</th>
		<th>
			<input type="hidden" name="koderegion" >
			<input type="hidden" name="kodebisnis" >
			<input type="hidden" name="numberbisnis" >
			<input type="text" size="15" readonly="readonly" class="readOnly" id="spaj" name="spaj" ></th>
		<td>
		
			<input type="button" value="List SPAJ" name="search"
				onclick="monyong('cari');" style="width: 100px;"
				accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">
		</td>
		
	</tr>
	<div style="display: none;">
		<input type="button" value="submit spaj" id="submitSpajController" name="submitSpajController" onclick="monyong('submitSpaj');"/>
	</div>
	<tr>
		<th>Polis</th>
		<th><input type="text" size="20" readonly="readonly" class="readOnly" id="polis" name="polis"></th>
		<td>
		
		</td>
	</tr>
	</table>
	<table class="entry2" style="width: 98%;">
	<tr>
		<td colspan="3">
		
			<table style="margin: 0 0 0 0; padding: 0 0 0 0; width: 100%;">
				<tr>
			<td>
								<spring:bind path="cmd.*">
								<iframe src="${path}/bac/spaj.htm?posisi=-1&win=viewer" name="infoFrame" id="infoFrame" width="100%"> Please Wait... </iframe>
								</spring:bind>
			</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</div>
</form>
</body>
<script>
</script>

<%@ include file="/include/page/footer.jsp"%>