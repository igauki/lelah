<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<script>
			/*
			popWin('${path}/bas/spaj.htm?window=form_spaj', 350, 450);
			showFrame('${path}/report/bas.htm?window=form_spaj');
			function showFrame(x){
				document.getElementById('infoFrame').src=x;
			}
			*/
			
			function checkKodeCabang(x){
			var kode = document.getElementById("kodecabang").value ;
				if(x.name=='check') {
						
						ajaxManager.cekKodeCabang(kode,
							{callback:function(result) {
								DWRUtil.useLoadingMessage();
								document.getElementById("thinfokode").innerHTML = result;
							   },
							  timeout:180000,
							  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
							});
				}
				
			}
			
			function tampilkan(btn){

				if(btn.name=='save') {
					
					if(document.getElementById('loginName').value == ''){
					alert('Login Name harus di isi !');	
					return false;
					}
					
					if(document.getElementById('fullName').value == ''){
					alert('Full Name harus di isi');	
					return false;
					}
					
					if(document.getElementById('levelList').value == 'inputerLvl'){
						if(document.getElementById('daftarSPV1').value == '' || document.getElementById('daftarSPV2').value == '' ){
						alert('Level Inputer harus mengisi SPV 1 dan SPV 2 ! Jika SPV tidak ditemukan didalam list, silahkan didaftarkan terlebih dahulu\n\n\n Daftar SPV yang ditampilkan berdasarkan cabang');	
						return false;
						}
					}
					
									
					if(!confirm('Simpan Perubahan?')) {
						return false;
					}
					
				} else if(btn.name=='cancel') {
					if(!confirm('Batalkan Permintaan?')) {
						return false;
					}
				} else if(btn.name=='received') {
					if(!confirm('Tandai permintaan sebagai RECEIVED?\nJumlah SPAJ akan bertambah secara otomatis di sistem. Lanjutkan?')) {
						return false;
					}
				} else if(btn.name=='show') {
					if(document.getElementById('lus_id').value=='') {
						alert('Silahkan pilih salah satu nama terlebih dahulu');				
						return false;
					}
				} else if(btn.name=='reset') {
					if(!confirm('Lakukan Pergantian Password?')) {
						return false;
					}
				}else if(btn.name=='new') {
					if(document.getElementById('cabangList').value=='') {
						alert('Silahkan Pilih Cabang terlebih dahulu!');	
						return false;
					}
				}else if(btn.name=='search') {
					if(document.getElementById('cabangList').value=='') {
						alert('Silahkan Pilih Cabang terlebih dahulu!');	
						return false;
					}
				}
					
				document.getElementById('submitMode').value = btn.name;
				btn.disabled=true;
				document.getElementById('formpost').submit();
				
			}
			
					
			hideLoadingMessage();
	 
					
			function change(obj) {
			
			var selectBox = obj;
    		var selected = selectBox.options[selectBox.selectedIndex].value;
   			var levela1 = document.getElementById("spv1");
   			var levela2 = document.getElementById("spv2");
   			var btncekspv = document.getElementById("btncekspv");
   			var buttonStat = document.getElementById("tombol").value;
   		//	alert(buttonStat);
			
			    if(selected == 'inputerLvl') {
			  		levela1.style.display = "block";
			        levela2.style.display = "block";
			        if(buttonStat == 3){
			        btncekspv.style.display = "block";
			        }
			    }
			    else {
					levela1.style.display = "none";
					levela2.style.display = "none";
					btncekspv.style.display = "none";
			    }
			}
			
			
			function perubahan(cabang_id){
		
				alert("Perubahan Cabang, harus melakukan perubahan/penambahan SPV untuk level Inputer ! \n\n Silahkan menggunakan tombol Cek SPV,  untuk menampilkan list user SPV berdasarkan cabang yang dipilih. ");
// 				ajaxManager.listSpv(cabang_id,
// 				{callback:function(user) {
			
// 					DWRUtil.useLoadingMessage();
// 					$("#loginName").val("aaaa");
// 					if(user.va)
// 					document.getElementById('loginName').value = "aaa";
// 				   },
// 				  timeout:15000,
// 				  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
// 				});
			}			
								

		</script>
	</head>

	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">
		
				<div class="tab-container" id="container1">
					<ul class="tabs">
						<li>
							<a href="#" onClick="return showPane('pane1', this)" id="tab1">Administrasi User ID</a>
							<a href="#" onClick="return showPane('pane2', this)" id="tab2">Cek Kode Cabang</a>
						</li>
					</ul>
			
					<div class="tab-panes">
						<div id="pane1" class="panes">
							<form:form commandName="cmd" id="formpost" method="post">
								<form:hidden id="submitMode" path="submitMode"/>
								<table class="entry2">
									<tr>
										<th style="vertical-align: top;" >
										
										<table><tr><td>							
										<select name="cabangList" id="cabangList" title="Pilih Salah satu" >
										<option value="" >Pilih Cabang</option>										
										<c:forEach var="x" items="${cmd.aksesCabang}">
											<option value="${x.lcb_no}"
												<c:if test="${x.lcb_no eq  cmd.lcb_no }">Selected</c:if>>
													${x.nama_cabang} ( ${x.lcb_no} - ${x.kode} )
											</option>
										</c:forEach>
										</select>
										</td></tr>	
										<tr><td align ="right">
											
											<input type="button" id="search" name="search" value="Search" style="width: 55px;" onclick="return tampilkan(this);">
											<input type="button" id="new" name="new" value="New" style="width: 55px;" onclick="return tampilkan(this);" >
										<br/>
										<br/>
										<br/>
										</td>
										</tr>
										<tr>
										<td align ="left"> 
										<input type="button" id="activeU" name="activeU" value="Active User" style="width: 100px;" onclick="return tampilkan(this);" <c:if test="${cmd.stat_Act eq 1}">disabled style="background-color: #E0E0E0"</c:if>>
										<input type="button" id="inactiveU" name="inactiveU" value="Inactive User" style="width: 100px;" onclick="return tampilkan(this);" <c:if test="${cmd.stat_Act eq 0}">disabled style="background-color: #E0E0E0"</c:if>>
										
										<input type="hidden" id="statusActiv" name="statusActiv" value="${cmd.stat_Act}">
										<input type="hidden" id="hiddenLus_id" name="hiddenLus_id" value="${cmd.lus_id}">
										<input type="hidden" id="hiddenCab_id" name="hiddenCab_id" value="${cmd.lcb_no}">
										<input type="hidden" id="tombol" name="tombol" value="${cmd.button}">
										
										</td>
										</tr>
										</table>	
										
										</th>
										<td style="vertical-align: top;" rowspan="2">
											<fieldset>
												<legend>Form Pengelolaan User ID</legend>
												<c:choose>
													<c:when test="${not empty cmd.detailUser}">
														<table class="entry2" border="0" >
														<tr>
														<td colspan = 2>
															&nbsp;
														</td> 
														</tr>
														<tr>
														<th width="100" align="left">LUS ID  </th>
														<td width="300">
															<input name="lusId" id="lusId" type="text" class="readOnly" readonly="true" size="15" value="${cmd.lus_id}"/>
															
															
														</td> 
														</tr>
														<tr>
														<th width="100" align="left">CABANG  </th>
														<td width="300">
														
														<select name="cabangPil" id="cabangPil" title="Pilih Salah satu" <c:if test="${ cmd.button eq 0 or cmd.button eq 1 }"> disabled="disabled" style="background-color: #E0E0E0"
														</c:if> onchange="perubahan(this.value)" >
																			
															<c:forEach var="x" items="${cmd.aksesCabang}">
																<option value="${x.lcb_no}"
																	<c:if test="${x.lcb_no eq  cmd.lcb_no_pil }">Selected</c:if>>
																		${x.nama_cabang} ( ${x.lcb_no} )
																</option>
															</c:forEach>
															</select>
														</td> 
														</tr>
														<tr>
														<th width="100" align="left">LOGIN NAME *  </th>
														<td width="300">
															<input type="text" name="loginName" id="loginName"  size="40"  value="${cmd.detailUser[0].lus_login_name}" 
															<c:if test="${ cmd.button eq 0 or cmd.button eq 3}"> readonly="readonly" style="background-color: #E0E0E0"
														</c:if>/>
														</td> 
														</tr>
														<tr>
														<th width="100" align="left">FULL NAME *  </th>
														<td width="300">
															<input type="text" name="fullName" id="fullName"  size="40" value="${cmd.detailUser[0].lus_full_name}" 
															<c:if test="${ cmd.button eq 0}">readonly="readonly" style="background-color: #E0E0E0"
														</c:if>/>
														</td> 
														</tr>
														<tr >
														<th width="100" align="left">LEVEL  </th>
														<td width="800">
															<table><tr>
														
															<td>
															<select name="levelList" id="levelList" title="Pilih Salah satu"  onchange="change(this)" 
															<c:if test="${ cmd.button eq 0}"> disabled="disabled" style="background-color: #E0E0E0"
														</c:if> >											
																<option value="inputerLvl" <c:if test="${(cmd.detailUser[0].valid_bank_1 ne null and cmd.detailUser[0].valid_bank_2 ne null and (cmd.button eq 0 or cmd.button eq 3) ) or (cmd.level eq 'inputerLvl' and cmd.button eq 1)}">selected="selected"</c:if>>Inputer</option>
																<option value="spv1lvl" <c:if test="${(cmd.detailUser[0].valid_bank_1 eq null and cmd.detailUser[0].valid_bank_2 eq null and (cmd.button eq 0 or cmd.button eq 3))}">selected="selected"</c:if>>SPV (A)</option>
																
															</select>
															</td><td>
															<input type="button" id="btncekspv" name="btncekspv" value="cek spv" style="width: 55px;"  onclick="return tampilkan(this);"  
															<c:if test="${(cmd.detailUser[0].valid_bank_1 eq null and cmd.detailUser[0].valid_bank_2 eq null and cmd.button eq 3 ) or (cmd.button ne 3) }">
															style="display: none;"</c:if>>
															</td></tr></table>	
														</td> 
														</tr>
														
																											
														<tr id = "spv1" <c:if test="${(cmd.detailUser[0].valid_bank_1 eq null and cmd.detailUser[0].valid_bank_2 eq null and (cmd.button eq 0 or cmd.button eq 3))}"> 
														 style="display: none;" 
														</c:if>>
														<th width="100" align="left">SPV 1 (A1)  </th>
														<td width="300">

														<select name="daftarSPV1" id="daftarSPV1" title="Pilih Salah satu" <c:if test="${ cmd.button eq 0}"> disabled="disabled" style="background-color: #E0E0E0"
														</c:if>>
														
														<option value="" >Pilih salah satu</option>		
														<c:forEach var="s" items="${cmd.daftarSpv1}">
															<option value="${s.lus_id}"
																<c:if test="${s.lus_id eq  cmd.spv1_name }">Selected</c:if>>
																	${s.lus_login_name}
															</option>
														</c:forEach>
																		
														</select>
																																										
														</td> 
														</tr>
														
														<tr id = "spv2" <c:if test="${(cmd.detailUser[0].valid_bank_1 eq null and cmd.detailUser[0].valid_bank_2 eq null and (cmd.button eq 0 or cmd.button eq 3))}"> 
														 style="display: none;" 
														</c:if>>
														<th width="100" align="left">SPV 2 (A2)  </th>
														<td width="300">

														<select name="daftarSPV2" id="daftarSPV2" title="Pilih Salah satu" <c:if test="${ cmd.button eq 0}"> disabled="disabled" style="background-color: #E0E0E0"
														</c:if>>
														<option value="" >Pilih salah satu</option>		
														<c:forEach var="s" items="${cmd.daftarSpv2}">
															<option value="${s.lus_id}"
																<c:if test="${s.lus_id eq  cmd.spv2_name }">Selected</c:if>>
																	${s.lus_login_name}
															</option>
														</c:forEach>
														
														</select>
														
																												
														</td> 
														</tr>
														
														<tr>
														<th width="100" align="left">EMAIL  </th>
														<td width="300">
															<input type="text" name="email" id="email"  size="40" value="${cmd.detailUser[0].lus_email}"
															<c:if test="${ cmd.button eq 0}"> readonly="readonly" style="background-color: #E0E0E0"
														</c:if>/>
														</td> 
														</tr>
														
														<tr>
														<th width="100" align="left">STATUS  </th>
														<td width="300">
															<input type="radio" name="status" id="ac" value="1" checked="checked"
																<c:if test="${cmd.detailUser[0].lus_active eq '1'}"> checked</c:if> 
																<c:if test="${ cmd.button eq 0}"> disabled="disabled" style="background-color: #E0E0E0"
														</c:if>/><b>ACTIVE</b> 
																&nbsp;&nbsp;&nbsp;&nbsp;
																<input type="radio" name="status" id="na" value="0"
																<c:if test="${cmd.detailUser[0].lus_active eq '0'}"> checked</c:if> 
																<c:if test="${ cmd.button eq 0}"> disabled="disabled" style="background-color: #E0E0E0"
																</c:if>
															/><b>NON ACTIVE</b>
										
														
														</td> 
														</tr>
														
														<tr>
														<td colspan = 2 >
															&nbsp;
														</td> 
														</tr>
														<tr>
														<td width="100"></td>
														<td width="270" colspan = 2 align = "center" >
															<input type="button" id="btnSave" name="save" value="save" onclick="return tampilkan(this);" <c:if test="${cmd.button eq 0}">disabled</c:if>>
															<input type="button" id="btnEdit" name="edit" value="edit" onclick="return tampilkan(this);" <c:if test="${cmd.button eq 1 or cmd.button eq 3}">disabled</c:if>>
															<input type="button" id="btnCancel" name="cancel" value="cancel" onclick="return tampilkan(this);" <c:if test="${cmd.button eq 0}">disabled</c:if>/>
														</td> 
														</tr>
														<tr>
														<td width="100"></td>
														<td width="270" colspan = 2 align = "center" >
															<input type="button" id="btnReset" name="reset" value="Reset Password" onclick="return tampilkan(this);" <c:if test="${cmd.button eq 1 or cmd.button eq 3}">disabled</c:if>/>
															
														</td> 
														</tr>
														
														
														</table>											
													</c:when>
													<c:otherwise>
														<div id="success">
															Silahkan pilih USER dengan cara memilih CABANG dan klik tombol SEARCH (Pencarian User berdasarkan cabang) / NEW (Pembuatan user id Baru) di sebelah kiri
														</div>
													</c:otherwise>
												</c:choose>
												
											</fieldset>									
										</td>										
									</tr>
									<tr>
										<th width="280" >
											<select id="lus_id" name="lus_id" size="${sessionScope.currentUser.comboBoxSize}
													" style="width: 270px;" onclick="document.getElementById('show').click();">
												
												<c:forEach var="d" items="${cmd.daftarUser}">
													<option value="${d.lus_id}" 
														<c:if test="${d.lus_id eq cmd.lus_id}"> selected </c:if>
													>${d.lus_full_name}</option>
												</c:forEach>
												
											</select>
											<br/>
											<input type="button" id="show" name="show" value="Show" style="width: 55px;" onclick="return tampilkan(this);">
										</th>
									</tr>
									
									
								</table>
							</form:form>
						</div>
					
					<div id="pane2" class="panes">
					<table>
					<tr><th colspan="3" align="center"><br></th></tr>
					<tr>
					<td>Kode Cabang :  </td>
					<td><input type="text" name="kodecabang" id="kodecabang"  size="10"/></td>
					<td><input type="button" id="check" name="check" value="Check"  onclick="return checkKodeCabang(this);" style="width: 55px;"> </td>
					</tr>
					<tr><th colspan="3" align="center"><br></th></tr>
					<tr><th colspan="3" align="center"> <p id="thinfokode"></p></th></tr>
					<tr><th colspan="3" align="center"><br></th></tr>
					<tr><th colspan="3" align="center"><br></th></tr>
					</table>
					</div>
											
					</div>
					
				</div>
			

	</body>
	<script>
		<c:if test="${not empty cmd.stat_sukses}">
			alert('${cmd.stat_sukses}');
		</c:if>
	</script>
</html>