<%@ include file="/include/page/header_jquery.jsp"%>
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (jQueryUI Tabs) init tab2 Utama
		$("#tabs").tabs();

		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title], label[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		
		// button icons
		$( "#btnSave" ).button({icons: {primary: "ui-icon-note"}});
		$( "#btnCancel" ).button({icons: {primary: "ui-icon-script"}});		
		
		// untuk semua textfield, onfocus maka select all text
		$("input:text, textarea, file").focus(function(){
		    this.select();
		});
		
		var pesan = '${pesan}';
		if(pesan != null && pesan != ''){
			alert(pesan);
		}
		
		$('#jn_surat').val('${sel_surat}');
		$('#polis').val('${polis}');
		
		$("#polis").change(function(){ 
            var url = "${path}/bac/multi.htm?window=request_surat&json=1&polis="+$('#polis').val();
			$.getJSON(url, function(result) {
				//$.each(result, function() {
					$('#span_pemegang').text(result);
				//});
			});
        });
        
        $("#jn_surat").change(function(){ 
        	$("#srt_text").val($("#jn_surat :selected").text());
        });
        
        
        $("#btnSave").click(function(){
        	if($('#jn_surat').val()=='' || $('#jn_surat').val()==null){
        		alert('Harap pilih jenis surat.');
        		return false;
        	} 
        	
        	if($('#polis').val()=='' || $('#polis').val()==null){
        		alert('Harap isi nomor polis.');
        		$('#polis').focus();
        		return false;
        	} 
        	
        	return true;
        });
        
        $("#btnCancel").click(function(){
        	$('#span_pemegang').text('');
        });
	});
	
	function getPolis(polis){
		$('#polis').val(polis);
		
		var url = "${path}/bac/multi.htm?window=request_surat&json=1&polis="+polis;
		$.getJSON(url, function(result) {
			//$.each(result, function() {
				$('#span_pemegang').text(result);
			//});
		});
	}
	
	/* function cari(){
		window.open('${path}/uw/spaj.htm?posisi=-1&win=viewer&kata=', 350, 450);
	} */
	
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 23em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 23.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 23em; }
	
	/* untuk align center */
	.tengah { text-align: center; }
	.kiri { text-align: left; }

	/* untuk tombol2 kecil di report */
	ul#icons {margin: 0; padding: 0;}
	ul#icons li {margin: 0 1px 0 0; position: relative; padding: 0px; cursor: pointer; float: left;  list-style: none;}
	ul#icons span.ui-icon {float: left; margin: 0px;}

	/* styling untuk server-side validation error */
	.errorField { border: 1px solid red; }
	.errorMessage { color: red; display: block;}
	.readonly { background-color: #CCC;}
	
	/* styling untuk table input */
	table.inputTable{
		width: 700px;
	}
	table.inputTable tr th{
		width: 150px;
	}
	
	/* styling untuk table info tanggal */
	div#infoTanggal{
		text-align: center;
		background-color: white;
		width: 345px;
		padding: 8px;
		border: 1px solid gray;
		position: fixed;
		left: 580px;
		top: 50px;
	}

</style>

<body>

	<form method="post" name="formpost" id="formpost" action="">
	<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>Request Print Surat</div></legend>
		
		<table class="inputTable">
			<tr>
				<th class="kiri">User Cabang</th>
				<td><input type="text" name="user_cabang" id="user_cabang" value="${user }" class="readonly" readonly="readonly"/></td>
			</tr>
			<tr>
				<th class="kiri">Jenis Surat</th>
				<td>
					<select id="jn_surat" name="jn_surat">
						<option value="">--Pilih--</option>
						<c:forEach items="${surat }" var="r" varStatus="s">
							<option value="${r.JENIS }">${r.NAMA }</option>
						</c:forEach>
					</select>
					<input type="hidden" name="srt_text" id="srt_text" value="${surat_text }"/>
				</td>
			</tr>
			<tr>
				<th class="kiri">Polis</th>
				<td>
					<input type="text" name="polis" id="polis" value=""/>
					<input type="submit" name="btnCari" id="btnCari" value="Cari" class="button"><em> *cari polis berdasarkan pemegang</em>
					<br><span id="span_pemegang" style="color: red; font-size: 0.9em; font-style: italic;"></span>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
					<input type="submit" name="btnSave" id="btnSave" title="Save" value="Save">
					<input type="reset" name="btnCancel" id="btnCancel" title="Cancel" value="Cancel">
				</td>
			</tr>
		</table>

	</fieldset>
	
	<c:if test="${not empty listPolis }">
		<br>
		<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>List Polis</div></legend>
		<br>
		<table border="1" cellpadding="2" cellspacing="0">
			<tr>
				<th>No. SPAJ</th>
				<th>No. Polis</th>
				<th>Produk</th>
				<th>Pemegang</th>
				<th>Tertanggung</th>
			</tr>
			<c:forEach items="${listPolis }" var="p" varStatus="s">
			<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	
					onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;" onclick="getPolis('${p.MSPO_POLICY_NO}');">
				<td align="center">${p.REG_SPAJ }</td>
				<td align="center">${p.MSPO_POLICY_NO }</td>
				<td align="center">${p.PRODUK }</td>
				<td align="center">${p.PEMEGANG }</td>
				<td align="center">${p.TERTANGGUNG }</td>
			</tr>
			</c:forEach>
		</table>
		</fieldset>
	</c:if>
	
	<c:if test="${not empty req_surat }">
		<br>
		<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>List Permintaan Yang Belum di Proses</div></legend>
		<br>
		<table border="1" cellpadding="2" cellspacing="0">
			<tr>
				<th>Tanggal</th>
				<th>No. Polis</th>
				<th>Surat</th>
				<th>User Req</th>
			</tr>
			<c:forEach items="${req_surat }" var="p" varStatus="s">
			<tr>
				<td align="center"><fmt:formatDate value="${p.TGL_REQ }" pattern="dd/MM/yyyy"/></td>
				<td align="center">${p.POLIS }</td>
				<td align="center">${p.JN_SURAT }</td>
				<td align="center">${p.USER_REQ }</td>
			</tr>
			</c:forEach>
		</table>
		</fieldset>
	</c:if>
	
	</form>
	
</body>
</html>