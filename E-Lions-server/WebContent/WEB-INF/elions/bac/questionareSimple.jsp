<%@ include file="/include/page/header.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<%User currentUser = (User) session.getAttribute("currentUser");%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>
<script type="text/javascript">
	function Body_onload() {	
		<c:if test="${not empty pesan}">
			alert('${pesan}');
			//window.location='${path }/bac/bac.htm';
		</c:if>
		
		//var berat = document.getElementById('medQuest.msadm_berat_berubah').value;
		var berat = '${cmd.medQuest.msadm_berat_berubah}';
		var sehat ='${cmd.medQuest.msadm_sehat}';
		var penyakit ='${cmd.medQuest.msadm_penyakit}';
		var medis ='${cmd.medQuest.msadm_medis}';
		var hamil ='${cmd.medQuest.msadm_pregnant}';
		
		
		if(berat=='0'){
			document.getElementById('flagberat_berubah2').value=0;
			document.getElementById('flagberat_berubah2').checked=true;
		}else{
			document.getElementById('flagberat_berubah1').value=1;
			document.getElementById('flagberat_berubah1').checked=true;
		}
		
		if(sehat=='0'){
			document.getElementById('flagsehat2').value=0;
			document.getElementById('flagsehat2').checked=true;
		}else{
			document.getElementById('flagsehat1').value=1;
			document.getElementById('flagsehat1').checked=true;
		}
		if(penyakit=='0'){
			document.getElementById('flagsakit2').value=0;
			document.getElementById('flagsakit2').checked=true;
		}else{
			document.getElementById('flagsakit1').value=1;
			document.getElementById('flagsakit1').checked=true;
		}
		if(medis=='0'){
			document.getElementById('flagmedis2').value=0;
			document.getElementById('flagmedis2').checked=true;
		}else{
			document.getElementById('flagmedis1').value=1;
			document.getElementById('flagmedis1').checked=true;
		}
		if(hamil=='0'){
			document.getElementById('flaghamil2').value=0;
			document.getElementById('flaghamil2').checked=true;
		}else{
			document.getElementById('flaghamil1').value=1;
			document.getElementById('flaghamil1').checked=true;
		}
	}
</script>
<body style="height: 100% " onLoad="Body_onload();">

<form name="frmParam" method="post">
<table class="entry2" style="width: 98.4%;">
	<tr>
				<td width="67%" align="left" valign="top" colspan="3">
		<spring:bind path="cmd.*">
			<c:if test="${not empty status.errorMessages}">
				<div id="error">ERROR:<br>
				<c:forEach var="error" items="${status.errorMessages}">
	- <c:out value="${error}" escapeXml="false" />
					<br />
				</c:forEach></div>
			</c:if>
		</spring:bind>	
				</td>
	</tr>
</table>
<center><h3><br>QUESTIONARE</h32></center>
<spring:bind path="cmd.medQuest.jenis_quest">
	<c:choose>
        <c:when test="${status.value eq \"NORMAL\"}">
			<table class="entry2">
				 <tr colspan ="5" align="left"> 
			            <th colspan ="-2">1.</th>
			            <th >a. Berat Badan/Kg <spring:bind path="cmd.medQuest.msadm_berat"> 
			              		<input type="text" name="${status.expression}" value="${status.value }"  size="3" maxlength="3" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
			             	</spring:bind><br><br><br>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tinggi Badan/Cm <spring:bind path="cmd.medQuest.msadm_tinggi"> 
			              		<input type="text" name="${status.expression}" value="${status.value }"  size="3" maxlength="3" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
			             	</spring:bind><br><br>b.  Apakah berat badan Anda berubah dalam 12 bulan terakhir? jika "YA", jelaskan berapa kg penurunan/kenaikannya dan jelaskan penyebabnya !<br><spring:bind path="cmd.medQuest.msadm_berubah_desc"> 
				              <textarea cols="100" rows="2" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
								 <c:if test="${ not empty status.errorMessage}">
										style='background-color :#FFE1FD'
									</c:if>>${status.value }</textarea>
				            </spring:bind><th><spring:bind path="cmd.medQuest.msadm_berat_berubah">
						           	<label for="flagberat_berubah1"><input id="flagberat_berubah1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest.msadm_berubah_desc',1);">Ya</label>
						           	<label for="flagberat_berubah2"><input id="flagberat_berubah2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest.msadm_berubah_desc',0);">Tidak</label>
				           			<input type="hidden" name="flagberat_berubah" id="flagberat_berubah"
									value="${cmd.medQuest.msadm_berat_berubah}"  size="30" style='background-color :#D4D4D4'readOnly>
				            </spring:bind>  
						</tr><th></th></tr>
			            </tr>
			            
			     <tr colspan ="5" align="left"> 
			            <th colspan ="-2">2.</th>
			            <th >Apakah anda sekarang dalam keadaan   sehat ? Jika "TIDAK", Mohon Dijelaskan !<br><spring:bind path="cmd.medQuest.msadm_sehat_desc"> 
				              <textarea cols="100" rows="2" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
								 <c:if test="${ not empty status.errorMessage}">
										style='background-color :#FFE1FD'
									</c:if>>${status.value }</textarea>
				            </spring:bind><th><spring:bind path="cmd.medQuest.msadm_sehat">
						           	<label for="flagsehat1"><input id="flagsehat1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_yes('medQuest.msadm_sehat_desc',1);">Ya</label>
						           	<label for="flagsehat2"><input id="flagsehat2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_yes('medQuest.msadm_sehat_desc',0);">Tidak</label>
				           			<input type="hidden" name="flagsehat" id="flagsehat"
									value="${cmd.medQuest.msadm_sehat}"  size="30" style='background-color :#D4D4D4'readOnly>
				            </spring:bind>  
						</tr><th></th></tr>
			        </tr>
			        
			 	<tr colspan ="5" align="left"> 
			            <th colspan ="-2">3.</th>
			            <th >Apakah Anda sedang atau pernah menderita, atau pernah diberitahu atau dalam konsultasi/perawatan/pengobatan/pengawasan medis sehubungan dengan salah satu atau beberapa penyakit/gangguan/gejala lainnya?
			jika "YA", jelaskan : nama penyakit, kapan, obat yang diberikan, fotokopi hasil pemeriksaan laboratorium, nama dan alamat dokter yang merawat !<br><spring:bind path="cmd.medQuest.msadm_penyakit_desc"> 
				              <textarea cols="100" rows="2" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
								 <c:if test="${ not empty status.errorMessage}">
										style='background-color :#FFE1FD'
									</c:if>>${status.value }</textarea>
				            </spring:bind><th><spring:bind path="cmd.medQuest.msadm_penyakit">
						           	<label for="flagsakit1"><input id="flagsakit1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest.msadm_penyakit_desc',1);">Ya</label>
						           	<label for="flagsakit2"><input id="flagsakit2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest.msadm_penyakit_desc',0);">Tidak</label>
				           			<input type="hidden" name="flagsakit" id="flagsakit"
									value="${cmd.medQuest.msadm_penyakit}"  size="30" style='background-color :#D4D4D4'readOnly>
				            </spring:bind> 
						</tr><th></th></tr>
			        </tr>        
			        
			       <tr colspan ="5" align="left"> 
			            <th colspan ="-2">4.</th>
			            <th >Apakah Anda sedang atau pernah menjalani konsultasi / rawat inap / operasi / biopsi / pemeriksaan laboratorium / rontgen / EKG / Treadmill / Echocardiography / USG / CT Scan / MRI atau pemeriksaan lainnya? Jika &quot;YA&quot;, jelaskan : pemeriksaan apa, kapan dilakukan, alasan dilakukan pemeriksaan tersebut, haslnya(lampirkan fotokopi hasil pemeriksaan)!<br>
			            <spring:bind path="cmd.medQuest.msadm_medis_desc"> 
				              <textarea cols="100" rows="2" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
								 <c:if test="${ not empty status.errorMessage}">
										style='background-color :#FFE1FD'
									</c:if>>${status.value }</textarea>
				            </spring:bind><th><spring:bind path="cmd.medQuest.msadm_medis">
						           	<label for="flagmedis1"><input id="flagmedis1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest.msadm_medis_desc',1);">Ya</label>
						           	<label for="flagmedis2"><input id="flagmedis2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest.msadm_medis_desc',0);">Tidak</label>
				           			<input type="hidden" name="flagmedis" id="flagmedis"
									value="${cmd.medQuest.msadm_medis}"  size="30" style='background-color :#D4D4D4'readOnly>
				            </spring:bind>
						</tr><th></th></tr>
			        </tr>        
			        
			         <tr colspan ="5" align="left"> 
			            <th colspan ="-2">5.</th>
			            <th >Pertanyaan Khusus untuk Wanita : Apakah saat ini Anda sedang hamil? Jika &quot;YA&quot;, Umur kehamilan___minggu. Kehamilan anak ke ?<br>
			           <spring:bind path="cmd.medQuest.msadm_pregnant_desc"> 
				              <textarea cols="100" rows="2" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
								 <c:if test="${ not empty status.errorMessage}">
										style='background-color :#FFE1FD'
									</c:if>>${status.value }</textarea>
				            </spring:bind> <th><spring:bind path="cmd.medQuest.msadm_pregnant">
						           	<label for="flaghamil1"><input id="flaghamil1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest.msadm_pregnant_desc',1);">Ya</label>
						           	<label for="flaghamil2"><input id="flaghamil2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest.msadm_pregnant_desc',0);">Tidak</label>
				           			<input type="hidden" name="flaghamil" id="flaghamil"
									value="${cmd.medQuest.msadm_pregnant}"  size="30" style='background-color :#D4D4D4'readOnly>
				            </spring:bind> 
						</tr><th></th></tr>
			        </tr>        
			</table>         		
        </c:when>
        <c:otherwise>
			<!-- helpdesk [148055] produk DMTM Dana Sejaterah 163 26-30 & Smile Sarjana 173 7-9, untuk UP > 200jt SIO ada tambah questionare -->
        	<table class="entry2">
				 <tr colspan ="5" align="left"> 
			            <th colspan ="-2">1.</th>
			            <th >a. Berat Badan/Kg <spring:bind path="cmd.medQuest.msadm_berat"> 
			              		<input type="text" name="${status.expression}" value="${status.value }"  size="3" maxlength="3" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
			             	</spring:bind><br>
							b. Tinggi Badan/Cm <spring:bind path="cmd.medQuest.msadm_tinggi">
								<input type="text" name="${status.expression}" value="${status.value }"  size="3" maxlength="3" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>	
						</th>
						<th></th>
				 </tr>
				 <tr colspan ="5" align="left" style="display:none;"> 
				 	<th colspan ="-2">2.</th>
			        <th>
			             	</spring:bind>Apakah berat badan Anda berubah dalam 12 bulan terakhir? jika "YA", jelaskan berapa kg penurunan/kenaikannya dan jelaskan penyebabnya !<br><spring:bind path="cmd.medQuest.msadm_berubah_desc"> 
				              <textarea cols="100" rows="2" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
								 <c:if test="${ not empty status.errorMessage}">
										style='background-color :#FFE1FD'
									</c:if>>${status.value }</textarea>
				            </spring:bind><th><spring:bind path="cmd.medQuest.msadm_berat_berubah">
						           	<label for="flagberat_berubah1"><input id="flagberat_berubah1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest.msadm_berubah_desc',1);">Ya</label>
						           	<label for="flagberat_berubah2"><input id="flagberat_berubah2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest.msadm_berubah_desc',0);">Tidak</label>
				           			<input type="hidden" name="flagberat_berubah" id="flagberat_berubah"
									value="${cmd.medQuest.msadm_berat_berubah}"  size="30" style='background-color :#D4D4D4'readOnly>
				            </spring:bind>
				     </th>  
				</tr>

			            
			     <tr colspan ="5" align="left" style="display:none;"> 
			            <th colspan ="-2">2.</th>
			            <th >Apakah anda sekarang dalam keadaan   sehat ? Jika "TIDAK", Mohon Dijelaskan !<br><spring:bind path="cmd.medQuest.msadm_sehat_desc"> 
				              <textarea cols="100" rows="2" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
								 <c:if test="${ not empty status.errorMessage}">
										style='background-color :#FFE1FD'
									</c:if>>${status.value }</textarea>
				            </spring:bind><th><spring:bind path="cmd.medQuest.msadm_sehat">
						           	<label for="flagsehat1"><input id="flagsehat1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_yes('medQuest.msadm_sehat_desc',1);">Ya</label>
						           	<label for="flagsehat2"><input id="flagsehat2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_yes('medQuest.msadm_sehat_desc',0);">Tidak</label>
				           			<input type="hidden" name="flagsehat" id="flagsehat"
									value="${cmd.medQuest.msadm_sehat}"  size="30" style='background-color :#D4D4D4'readOnly>
				            </spring:bind>  
						</tr><th></th></tr>
			        </tr>
			        
			 	<tr colspan ="5" align="left"> 
			            <th colspan ="-2">2.</th>
			            <th >Apakah Anda sedang atau pernah menderita, atau pernah diberitahu atau dalam konsultasi/perawatan/pengobatan/pengawasan medis sehubungan dengan salah satu atau beberapa penyakit/gangguan/gejala lainnya?
			jika "YA", jelaskan : nama penyakit, kapan, obat yang diberikan?<br><spring:bind path="cmd.medQuest.msadm_penyakit_desc"> 
				              <textarea cols="100" rows="2" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
								 <c:if test="${ not empty status.errorMessage}">
										style='background-color :#FFE1FD'
									</c:if>>${status.value }</textarea>
				            </spring:bind><th><spring:bind path="cmd.medQuest.msadm_penyakit">
						           	<label for="flagsakit1"><input id="flagsakit1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest.msadm_penyakit_desc',1);">Ya</label>
						           	<label for="flagsakit2"><input id="flagsakit2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest.msadm_penyakit_desc',0);">Tidak</label>
				           			<input type="hidden" name="flagsakit" id="flagsakit"
									value="${cmd.medQuest.msadm_penyakit}"  size="30" style='background-color :#D4D4D4'readOnly>
				            </spring:bind> 
						</tr>  
			        
			       <tr colspan ="5" align="left"> 
			            <th colspan ="-2">3.</th>
			            <th >Apakah Anda sedang atau pernah menjalani konsultasi / rawat inap / operasi / biopsi / pemeriksaan laboratorium / rontgen / EKG / Treadmill / Echocardiography / USG / CT Scan / MRI atau pemeriksaan lainnya? Jika &quot;YA&quot;, jelaskan : pemeriksaan apa, kapan dilakukan, alasan dilakukan pemeriksaan tersebut?<br>
			            <spring:bind path="cmd.medQuest.msadm_medis_desc"> 
				              <textarea cols="100" rows="2" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
								 <c:if test="${ not empty status.errorMessage}">
										style='background-color :#FFE1FD'
									</c:if>>${status.value }</textarea>
				            </spring:bind><th><spring:bind path="cmd.medQuest.msadm_medis">
						           	<label for="flagmedis1"><input id="flagmedis1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest.msadm_medis_desc',1);">Ya</label>
						           	<label for="flagmedis2"><input id="flagmedis2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest.msadm_medis_desc',0);">Tidak</label>
				           			<input type="hidden" name="flagmedis" id="flagmedis"
									value="${cmd.medQuest.msadm_medis}"  size="30" style='background-color :#D4D4D4'readOnly>
				            </spring:bind>
						</tr><th></th></tr>
			        </tr>        
			        
			         <tr colspan ="5" align="left"> 
			            <th colspan ="-2">4.</th>
			            <th >Pertanyaan Khusus untuk Wanita : Apakah saat ini Anda sedang hamil? Jika &quot;YA&quot;, Umur kehamilan___minggu.<br>
			           <spring:bind path="cmd.medQuest.msadm_pregnant_desc"> 
				              <textarea cols="100" rows="2" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 1); "
								 <c:if test="${ not empty status.errorMessage}">
										style='background-color :#FFE1FD'
									</c:if>>${status.value }</textarea>
				            </spring:bind> <th><spring:bind path="cmd.medQuest.msadm_pregnant">
						           	<label for="flaghamil1"><input id="flaghamil1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest.msadm_pregnant_desc',1);">Ya</label>
						           	<label for="flaghamil2"><input id="flaghamil2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest.msadm_pregnant_desc',0);">Tidak</label>
				           			<input type="hidden" name="flaghamil" id="flaghamil"
									value="${cmd.medQuest.msadm_pregnant}"  size="30" style='background-color :#D4D4D4'readOnly>
				            </spring:bind> 
						</tr><th></th></tr>
			        </tr>        
			</table> 
        </c:otherwise>
    </c:choose>
</spring:bind>
<center><input type="submit" name="btnSimpan" value="Simpan" ></center>
	<c:if test="${not empty pesan}">${pesan}</c:if>
</form>

</body>
<%@ include file="/include/page/footer.jsp"%>