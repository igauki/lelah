<%@ include file="/include/page/header.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<body onload="setupPanes('container1', 'tab1');">
	
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Upload SPAJ - Confirmation</a>
			</li>
		</ul>

		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form:form id="formpost" name="formpost" commandName="cmd" method="post">
					<table class="entry2">
						<tr>
<!--							<td colspan="2">-->
<!--								<fieldset>-->
<!--									<legend>Daftar Spaj</legend>-->
<!--									<display:table id="spaj" name="cmd.daftarSpaj" class="displaytag" style="width:auto;">-->
<!--										<display:column title="No." style="text-align: left; white-space:nowrap;">-->
<!--											${spaj_rowNum}.-->
<!--										</display:column>-->
<!--										<display:column property="pemegang.mcl_first" title="Nama" style="text-align: left; white-space:nowrap;" />-->
<!--										<display:column property="pemegang.mspe_date_birth" title="Tgl Lahir" format="{0, date, dd/MM/yyyy}" style="text-align: center; white-space:nowrap;" />-->
<!--										<display:column property="pemegang.mspe_place_birth" title="Tempat Lahir" style="text-align: center; white-space:nowrap;" />-->
<!--										<display:column title="Sex" style="text-align: center; white-space:nowrap;">-->
<!--											<c:if test="${spaj.pemegang.mspe_sex eq 0}" >Wanita</c:if>-->
<!--											<c:if test="${spaj.pemegang.mspe_sex eq 1}" >Pria</c:if>-->
<!--										</display:column>-->
<!--										<display:column property="address.alamat_rumah" title="Alamat" style="text-align: left; white-space:nowrap;" />-->
<!--										<display:column property="address.kd_pos_rumah" title="Kode Pos" style="text-align: center; white-space:nowrap;" />-->
<!--										<display:column property="address.kota_rumah" title="Kota" style="text-align: center; white-space:nowrap;" />-->
<!--										<display:column title="Telepon1" style="text-align: center; white-space:nowrap;">-->
<!--											${spaj.address.area_code_rumah}-${spaj.address.telpon_rumah}-->
<!--										</display:column>-->
<!--										<display:column title="Telepon2" style="text-align: center; white-space:nowrap;">-->
<!--											${spaj.address.area_code_rumah2}-${spaj.address.telpon_rumah2}-->
<!--										</display:column>-->
<!--										<c:set var="nestedName" value="cmd.daftarSpaj[${spaj_rowNum -1}].daftarBenefeciary" />-->
<!--										<display:column title="Ahli Waris">-->
<!--											<display:table name="${nestedName}" id="child${spaj_rowNum}" class="displaytag">-->
<!--												<display:column property="msaw_first" title="Nama" style="text-align: left; white-space:nowrap;"/>-->
<!--												<display:column property="msaw_birth" title="Tgl Lahir" format="{0, date, dd/MM/yyyy}" style="text-align: center; white-space:nowrap;" />-->
<!--												<display:column property="lsre_relation" title="Hubungan" style="text-align: center; white-space:nowrap;" />-->
<!--												<display:column property="msaw_persen" title="%" />-->
<!--											</display:table>-->
<!--										</display:column>-->
<!--									</display:table>-->
<!--								</fieldset>							-->
<!--							</td>-->
							<th style="width: auto">Jumlah Pengajuan SPAJ : </th>
							<td>${jumlahSpaj}</td>
						</tr>
						<tr>
							<th></th>
							<td>
								
				                <input type="submit" name="_target${halaman-1}" value="&laquo; Back" onclick="return confirm('Kembali ke halaman sebelumnya (anda harus melakukan upload kembali) ?');" 
									onmouseover="return overlib('Alt-B', AUTOSTATUS, WRAP);" onmouseout="nd();">
								
								<input type="submit" name="_finish" value="Save" onclick="return confirm('Penyimpanan SPAJ akan dilakukan. Pastikan data anda sudah benar.');"
									onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
								
								<input type="hidden" name="_page" value="${halaman}">
								
							</td>
						</tr>
					</table>
					
				</form:form>
			</div>
		</div>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>