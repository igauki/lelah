<%@ include file="/include/page/header.jsp"%>
<script>
</script>
<body style="height: 100%;" 
	onload="setupPanes('container1', 'tab1'); setFrameSize('docFrame', 170);" 
	onresize="setFrameSize('docFrame', 170);">

<div class="tab-container" id="container1">
	<ul class="tabs">
		<li><a href="#" onClick="return showPane('pane1', this)" id="tab1">Bank Sinarmas Reports</a></li>
	</ul>
	<div class="tab-panes">
		<div id="pane1" class="panes" style="text-align: left;">
			<form name="formpost" method="post">
			<table class="entry2" style="width: 100%;">
				<tr>
					<th>Report</th>
					<td>
						<select name="jenisReport">
							<c:set var="tmp" value="" />
							<c:forEach items="${daftarJenis}" var="d">
								<c:if test="${tmp ne d.value}">
									<c:set var="tmp" value="${d.value}" />
									<optgroup label="${tmp}">
								</c:if>
								<option value="${d.key}">${d.desc}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<th>Produk</th>
					<td>
						<select name="jenisProduk">
							<c:forEach items="${daftarProduk}" var="d">
								<option value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<th>Kurs</th>
					<td>
						<select name="jenisKurs">
							<c:forEach items="${daftarKurs}" var="d">
								<option value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<th>Format</th>
					<td>
						<select name="jenisFormat">
							<c:forEach items="${daftarFormat}" var="d">
								<option value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<th>Tanggal</th>
					<td>
						<script>inputDate('begDate', '${sysDate}', false);</script> s/d 
						<script>inputDate('endDate', '${sysDate}', false);</script>
						&nbsp;
						<input type="button" value="Show">
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<iframe src="" name="docFrame" id="docFrame"
							width="100%"  height="100%"> Please Wait... </iframe>
					</td>
				</tr>
			</table>
			</form>
		</div>
	</div>
</div>
</body>
<%@ include file="/include/page/footer.jsp"%>