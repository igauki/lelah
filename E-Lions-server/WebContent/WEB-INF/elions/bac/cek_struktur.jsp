<%@ include file="/include/page/header.jsp"%>

<%--
  User: samuel
  Date: May 6, 2010
  Time: 4:36:14 PM
--%>

<fieldset>
	<legend>Struktur</legend>
	<display:table id="baris" name="strukturList" class="displaytag" >
		<display:column property="JABATAN" title="JABATAN" style="text-align: center;"/>
		<display:column property="KODE_AGEN" title="KODE AGEN" style="text-align: left;" />
		<display:column property="NAMA" title="NAMA" style="text-align: left;" />
		<display:column property="NPWP" title="NPWP" style="text-align: center;" />
		<display:column property="BANK" title="BANK"  style="text-align: left;" />
		<display:column property="BEG_DATE" title="BEG DATE" style="text-align: center;" />
		<display:column property="END_DATE" title="END DATE" style="text-align: center;" />
		<display:column property="ACTIVE_DATE" title="ACTIVE DATE" style="text-align: center;" />
		<display:column property="JENIS_LISENSI" title="JENIS LISENSI" style="text-align: center;" />
		<display:column property="TGL_LISENSI_EXPIRED" title="TGL LISENSI EXPIRED" style="text-align: center;" />
	</display:table>
</fieldset>

<table align="center" width="100%">
	<tr>
		<td align="center" ><input type="button" name="btnClose" value="Close" onClick="javascript:window.close()"></td>
	</tr>
</table>
<%@ include file="/include/page/footer.jsp"%>