<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />

<c:if test="${not empty cmd.fileConfirm}">
	<script>
		if(!confirm('${cmd.fileConfirm}')) {
	         window.location="${path}/bac/editspajnew.htm?data_baru=true&refreshObject=1&flagAwal=1";
	    }
	</script>
</c:if>

<c:if test="${cmd.datausulan.jenis_pemegang_polis == 0}">
	<jsp:include page="editpemegang_individunew.jsp" >
		<jsp:param name="test" value="test"></jsp:param>
	</jsp:include>
</c:if>
<c:if test="${cmd.datausulan.jenis_pemegang_polis == 1}">
	<jsp:include page="editpemegang_badanusahanew.jsp" >
		<jsp:param name="test" value="test"></jsp:param>
	</jsp:include>
</c:if>
