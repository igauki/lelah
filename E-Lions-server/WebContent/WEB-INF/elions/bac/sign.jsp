<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script>
<script>
function cetak(){
	window.frames['download_spaj'].location = '${path}/bac/multi.htm?window=download_spaj&file=${fileSpaj}&x=${x}&y=${y}&f=${f}&spaj=${spaj}';
	return false;
}
</script>
</head>
<body onload="setupPanes('container1', 'tab1');">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Pemegang</a>
				<a href="#" onClick="return showPane('pane2', this)" id="tab2">Tertanggung</a>
				<a href="#" onClick="window.frames['signFrame'].location.reload(true);window.frames['signFrame2'].location.reload(true);return showPane('pane3', this)" id="tab3">Preview</a>
				<a href="#" onClick="return showPane('pane4', this)" id="tab4">Download Spaj</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<!-- imageFormat : jpg, png, gif -->
				<jsp:plugin type="applet" code="com/ekalife/applet/customersign/SignApplet.class"
						codebase="${path}/include/applets" name="signupload" align="middle" height="100%" width="100%">
					<jsp:params>
						<jsp:param name="spaj" value="${spaj}" />
						<jsp:param name="nono" value="0" />
						<jsp:param name="imageFormat" value="jpg" />
						<jsp:param name="fileName" value="sign" />
						<jsp:param name="path" value="http://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/sign" />
					</jsp:params>
					<jsp:fallback>Apabila anda mengalami kesulitan harap menghubungi EDP</jsp:fallback>
				</jsp:plugin>
			</div>

			<div id="pane2" class="panes">
				<!-- imageFormat : jpg, png, gif -->
				<jsp:plugin type="applet" code="com/ekalife/applet/customersign/SignApplet.class"
						codebase="${path}/include/applets" name="signupload" align="middle" height="100%" width="100%">
					<jsp:params>
						<jsp:param name="spaj" value="${spaj}" />
						<jsp:param name="nono" value="1" />
						<jsp:param name="imageFormat" value="jpg" />
						<jsp:param name="fileName" value="sign" />
						<jsp:param name="path" value="http://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/sign" />
					</jsp:params>
					<jsp:fallback>Apabila anda mengalami kesulitan harap menghubungi EDP</jsp:fallback>
				</jsp:plugin>
			</div>

			<div id="pane3" class="panes" style="height: 547px;">
				<table class="entry2">
					<tr>
						<th id="imageHolder">
							<iframe name="signFrame" src="${path}/sign?spaj=${spaj}&image=sign.jpg&no=0" width="100%" height="250px;"></iframe>
							<iframe name="signFrame2" src="${path}/sign?spaj=${spaj}&image=sign.jpg&no=1" width="100%" height="250px;"></iframe>
						</th>
					</tr>
					<tr>
						<th>
							<input type="button" value="Refresh" onclick="window.frames['signFrame'].location.reload(true)">
							<input type="button" value="Download SPAJ" onclick="cetak();return showPane('pane4', document.getElementById('tab4'));">
							<input type="button" value="Close" onclick="window.close();">
						</th>
					</tr>
				</table>
			</div>

			<div id="pane4" class="panes" >
				<table class="entry2">
					<tr>
						<th id="imageHolder">
							<iframe name="download_spaj" width="100%" height="500px;"></iframe>
						</th>
					</tr>
				</table>
			</div>

</body>
</html>