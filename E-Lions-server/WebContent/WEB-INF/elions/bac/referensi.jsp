<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
	<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta NAME="Description" CONTENT="EkaLife">
	<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<meta HTTP-EQUIV="Expires" CONTENT="-1">
	<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
	<link href="${path}/include/image/eas.ico" rel="shortcut icon">
	<!--  -->
	<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
	<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
	<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
	<!--  -->
	<script type="text/javascript" src="${path }/include/js/default.js"></script>
	<!-- Ajax Related -->
	<script type="text/javascript" src="${path}/include/js/ajax.js"></script>
	<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script>
	<script type="text/javascript" src="${path }/dwr/engine.js"></script>
	<script type="text/javascript" src="${path }/dwr/util.js"></script>
	<script type="text/javascript">
		hideLoadingMessage();
		
		function waitPreloadPage() { //DOM
			<c:if test="${not empty pesan}">
	            alert('${pesan}');
	        </c:if>
			
			//calendar.set("tgl_lahir");
			
			if (document.getElementById){
				document.getElementById('prepage').style.visibility='hidden';
			}else{
				if (document.layers){ //NS4
					document.prepage.visibility = 'hidden';
				}
				else { //IE4
					document.all.prepage.style.visibility = 'hidden';
				}
			}
		}
		// End -->
		
		function CalcKeyCode(aChar) {
		  var character = aChar.substring(0,1);
		  var code = aChar.charCodeAt(0);
		  return code;
		}
		
		function checkNumber(val) {
		  var strPass = val.value;
		  var strLength = strPass.length;
		  var lchar = val.value.charAt((strLength) - 1);
		  var cCode = CalcKeyCode(lchar);
		
		  /* Check if the keyed in character is a number
		     do you want alphabetic UPPERCASE only ?
		     or lower case only just check their respective
		     codes and replace the 48 and 57 */
		
		  if (cCode < 48 || cCode > 57 ) {
		    var myNumber = val.value.substring(0, (strLength) - 1);
		    val.value = myNumber;
		  }
		  return false;
		}
		
		function ValidateForm(){
			
			var nama=document.formpost.nama;
			var tgl_lahir=document.formpost.tgl_lahir;
			var no_telp=document.formpost.no_telp;
			var nama_ref=document.formpost.nama_ref;
			var email_ref=document.formpost.email_ref;
			var email_ref2=document.formpost.email_ref2;
			
			/*var cek = document.formpost.check.checked;
			var nama_ref2=document.formpost.nama_ref2;
			var tgl_lahir_ref2=document.formpost.tgl_lahir_ref2;
			var alamat_ref=document.formpost.alamat_ref;
			alert('validate2');
			if(cek==true){
				if((nama_ref2.value==null)||(nama_ref2.value=="")){
					alert("Harap isi Nama referral");
					nama_ref2.focus();
					return false;
				}
				if((tgl_lahir_ref2.value==null)||(tgl_lahir_ref2.value=="")){
					alert("Harap isi Tgl Lahir referral");
					tgl_lahir_ref2.focus();
					return false;
				}
				if((alamat_ref.value==null)||(alamat_ref.value=="")){
					alert("Harap isi Alamat referral");
					alamat_ref.focus();
					return false;
				}
			}*/
			/* if((nama_ref.value==null)||(nama_ref.value=="")){
				alert("Harap isi No Spaj atau No Polis terlebih dahulu");
				nama.focus();
				return false;
			} */
			if((nama.value==nama_ref.value)){
				alert("Nama referral dan nama referensi tidak boleh sama");
				nama.focus();
				return false;
			}
			if((nama.value==null)||(nama.value=="")){
				alert("Harap isi Nama yang di referensikan");
				nama.focus();
				return false;
			}
			if((tgl_lahir.value==null)||(tgl_lahir.value=="")){
				alert("Harap isi Tgl Lahir yang di referensikan");
				tgl_lahir.focus();
				return false;
			}
			if((no_telp.value==null)||(no_telp.value=="")){
				alert("Harap isi No Telp yang di referensikan");
				no_telp.focus();
				return false;
			} 
			if((email_ref2.value==null)||(email_ref2.value=="")||(email_ref2.value==" ")){
				alert("Email referral tidak ada harap input email referral terlebih dahulu.");
				email_ref.focus();
				return false;
			}
			return true;
		 }	
		 
		 function other_agen(div){
			if(div==0){
				document.formpost.elements['agen'].readOnly = false;
				document.formpost.elements['agen'].style.backgroundColor ='#FFF';
				document.formpost.elements['kd_agen'].readOnly = false;
				document.formpost.elements['kd_agen'].style.backgroundColor ='#FFF';
				
				document.formpost.elements['agen1'].value = '';
				document.formpost.elements['kd_agen1'].value = '';
				document.formpost.elements['cbng_agen1'].value = ''; 
				document.formpost.elements['cbng_agen1'].readOnly = true;
				
			}else{
				document.formpost.elements['agen'].readOnly = true;
				document.formpost.elements['agen'].value = '';
				document.formpost.elements['agen'].style.backgroundColor ='#D4D4D4';
				document.formpost.elements['kd_agen'].readOnly = true;
				document.formpost.elements['kd_agen'].value = '';
				document.formpost.elements['kd_agen'].style.backgroundColor ='#D4D4D4';
				document.formpost.elements['cbng_agen'].readOnly = true;
				document.formpost.elements['cbng_agen'].value = ''; 
				document.formpost.elements['cbng_agen'].style.backgroundColor ='#D4D4D4';
				
				document.formpost.elements['cbng_agen1'].value = document.formpost.cbng_agen2.value; 
				
				var e = document.getElementById("cmb_agen"); 
				var strUser = e.options[e.selectedIndex].text; 
				var valUser = e.options[e.selectedIndex].value; 
				document.formpost.elements['kd_agen1'].value = valUser;
				document.formpost.elements['agen1'].value = strUser;
			}
		}
		
		function jns(jn){
			if(jn==1){
				document.formpost.elements['cmb_agen'].disabled = true;
				document.formpost.elements['cmb_agen'].style.backgroundColor ='#D4D4D4';
				document.formpost.elements['agen'].readOnly = true;
				document.formpost.elements['agen'].value = '';
				document.formpost.elements['agen'].style.backgroundColor ='#D4D4D4';
				document.formpost.elements['kd_agen'].readOnly = true;
				document.formpost.elements['kd_agen'].value = '';
				document.formpost.elements['kd_agen'].style.backgroundColor ='#D4D4D4';
			}else{
				document.formpost.elements['cmb_agen'].disabled = false;
				document.formpost.elements['cmb_agen'].style.backgroundColor ='#FFF';
			}
		}
		
		function ck(value){
			var cek = value.checked;
			if(cek==true){
				document.getElementById('tidak').style.display = 'block';
				document.getElementById('ada').style.display = 'none';
				
				document.formpost.elements['agen'].readOnly = false;
				document.formpost.elements['agen'].style.backgroundColor ='#FFF';
				document.formpost.elements['kd_agen'].readOnly = false;
				document.formpost.elements['kd_agen'].style.backgroundColor ='#FFF';
				
				document.formpost.elements['agen1'].value = '';
				document.formpost.elements['kd_agen1'].value = '';
			}else{
				document.getElementById('ada').style.display = 'block';
				document.getElementById('tidak').style.display = 'none';
				
				document.formpost.elements['agen'].readOnly = true;
				document.formpost.elements['agen'].value = '';
				document.formpost.elements['agen'].style.backgroundColor ='#D4D4D4';
				document.formpost.elements['kd_agen'].readOnly = true;
				document.formpost.elements['kd_agen'].value = '';
				document.formpost.elements['kd_agen'].style.backgroundColor ='#D4D4D4';
			}
		}
		
		function editEmailRef(){
			var r = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"); 
			
			var nama = document.formpost.elements['nama_ref'].value;
			var tgl_lahir = document.formpost.elements['tgl_lahir_ref'].value;
			var email = document.formpost.elements['email_ref'].value;
			
			if(email.match(r)==null){
				alert('format email salah');
				document.formpost.elements['email_ref'].value ='';
				document.formpost.elements['email_ref2'].value ='';
			}else{
				//alert('ya');
				ajaxManager.editEmailRef(email,nama,tgl_lahir,
				{callback:function(map) {
			
					DWRUtil.useLoadingMessage();
					document.formpost.elements['email_ref'].value = map.email;
					document.formpost.elements['email_ref2'].value = map.email;
					
					var email1 = map.email;
					if(email==null){
					}else{
						document.formpost.elements['email_ref'].readOnly = true;
					}
					
					var pesan = map.pesan;
					
					if(pesan!=null)alert(pesan);
				},
				  timeout:180000,
				  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
				});  
			}
		}
		
		function getAgen(){
			
			var kd = document.formpost.elements['kd_agen'].value;
			
			ajaxManager.getAgen(kd,
			{callback:function(map) {
		
				DWRUtil.useLoadingMessage();
				
				if(map==null){
					alert("Kode Agen tidak ada");
				}else{
					document.formpost.elements['agen'].value = map.MCL_FIRST;
					document.formpost.elements['cbng_agen'].value = map.LSRG_NAMA;
					document.formpost.elements['email_agen'].value = map.EMAIL;
				}
			},
			  timeout:180000,
			  /* errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); } */
			  errorHandler:function(message) { alert("Kode Agen tidak ada"); }
			});   
			
		}
	</script>
</head>

<body onload="waitPreloadPage();other_agen(this);" >
<div id="prepage" style="position:absolute; font-family:arial; font-size:12; left:0px; top:0px; background-color:yellow; layer-background-color:white;"> 
	<table border="2" bordercolor="red"><tr><td style="color: red;"><b>Loading ... ... Please wait!</b></td></tr></table>
</div>
<br><center><h4>TAMBANG EMAS</h4>
<form method="post" name="formpost2" id="formpost2" action="">
	<table class="entry" style="width: 700px;">
		<tr>
			<td>
				<fieldset>
					<legend style="font-size: 12px; color: black; font-weight: bolder;">Input SPAJ/NO POLIS</legend>
					<table class="entry" style="text-transform: uppercase;">
						<tr>
							<th style="width: 150px;">SPAJ/No Polis/ID Referral</th>
							<td style="text-align: left;"><input type="text" name="spaj" id="spaj">
							<input type="Submit" name="btnCari" class="button" value="Cari"></td>
						</tr>
					</table>
				</fieldset>
			</td>
		</tr>
	</table>
</form>
<form method="post" name="formpost" id="formpost" action="" onsubmit="return ValidateForm()">
	<table class="entry" style="width: 700px;">
		<!-- <tr>
			<td>
				<input type="checkbox" name="check" id="check" style="border: none;" onclick="ck(this);">Belum ada nomor polis
			</td>
		</tr> -->
		<tr id="ada" style="display: block;">
			<td>
				<fieldset>
					<legend style="font-size: 12px; color: black; font-weight: bolder;">Referral</legend>
					<table class="entry" style="text-transform: uppercase;">
						<tr>
							<th style="width: 150px;">Nama</th>
							<td style="text-align: left;">${nama_ref}
								<input type="hidden" name="nama_ref" id="nama_ref" value="${nama_ref}">
								<input type="hidden" name="polis" id="polis" value="${polis}"></td>
								<input type="hidden" name="spaj2" id="spaj2" value="${spaj2}"></td>
						</tr>
						<tr>
							<th>Tgl. Lahir</th>
							<td style="text-align: left;"><fmt:formatDate value="${tgl_lahir_ref}" pattern="dd/MM/yyyy"/><input type="hidden" name="tgl_lahir_ref" id="tgl_lahir_ref" value="${tgl_lahir_ref}"></td>
						</tr>
						<c:choose>
							<c:when test="${email_ref ne \'\' }">
								<th>Email</th>
								<td style="text-align: left;"><input type="text" size="50" name="email_ref2" id="email_ref2" value="${email_ref}" readonly="readonly"></td>
							</c:when>
							<c:otherwise>
								<th>Email</th>
								<td style="text-align: left;">
								<input type="text" size="50" name="email_ref" id="email_ref" value="${email_ref}" >
								<input type="hidden" size="30" name="email_ref2" id="email_ref2" value="${email_ref}" >&nbsp;<input type="button" name="btnEdit" class="button" value="Simpan E-mail" onclick="editEmailRef();"></td>
							</c:otherwise>
						</c:choose>
					</table>
				</fieldset>
			</td>
		</tr>
		<tr id="tidak" style="display: none;">
			<td colspan="3">
				<fieldset>
					<legend style="font-size: 12px; color: black; font-weight: bolder;">Referal</legend>
					<table class="entry" style="text-transform: uppercase;">
						<tr>
					      <th>Nama</th>
					      <td><input type="text" name="nama_ref2" id="nama_ref2"></td>
					    </tr>
					    <tr>
					      <th>Tgl Lahir</th>
					      <td><script>inputDate('tgl_lahir_ref2', '', false);</script></td>
					    </tr>
					    <tr>
					      <th>Tempat Lahir</th>
					      <td><input type="text" name="tp_lahir_ref" id="tp_lahir_ref"></td>
					    </tr>
					    <tr>
					      <th>Alamat</th>
					      <td><textarea name="alamat_ref" id="alamat_ref" cols="40" rows="5"></textarea></td>
					    </tr>
					    <tr>
					      <th>Kota </th>
					      <td><input type="text" name="kota_ref" id="kota_ref"></td>
					    </tr>
					    <tr>
					      <th>Kode Pos</th>
					      <td><input type="text" name="kode_pos_ref" id="kode_pos_ref" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)"></td>
					    </tr>
					    <tr>
					      <th>No. Telp</th>
					      <td><input name="area_telp_ref" type="text" id="area_telp_ref" size="5" maxlength="5" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)">
					      -
					      <input type="text" name="telp_ref" id="telp_ref" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)"></td>
					    </tr>
					    <tr>
					      <th>No. Telp Kantor</th>
					      <td><input name="area_telp_kantor_ref" type="text" id="area_telp_kantor_ref" size="5" maxlength="5" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)">
					        -
					          <input type="text" name="telp_kantor_ref" id="telp_kantor_ref" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)"></td>
					    </tr>
					    <tr>
					      <th>No. HP</th>
					      <td><input type="text" name="hp_ref" id="hp_ref" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)"></td>
					    </tr>
					</table>
				</fieldset>
			</td>
		</tr>
		<tr>
			<td>
				<fieldset>
					<legend style="font-size: 12px; color: black; font-weight: bolder;">Input Data Yang Di Referensi</legend>
					<table class="entry" style="text-transform: uppercase;">
					  <tr>
					    <th>Jenis Referensi</th>
					    <td style="text-align: left;width: 350px;">
					    	<input type="radio" name="jenis" id="jenis1" value="0" style="border: none;" checked="checked" onclick="jns(this.value);">Nasabah
					    	<input type="radio" name="jenis" id="jenis2" value="1" style="border: none;" onclick="jns(this.value);">Agent
					    </td>
					    <th>ID Referral</th>
					    <th>${id_seller}</th>
					  </tr>
					  <tr>
					    <th style="width: 150px;">Nama (sesuai KTP)</th>
					    <td style="text-align: left;" colspan="3"><input type="text" name="nama" id="nama" style="text-transform: uppercase;"></td>
					  </tr>
					  <tr>
					    <th>Tgl. Lahir</th>
					    <td style="text-align: left;" colspan="3"><!-- <input type="text" width=100 height=100 name="tgl_lahir" id="tgl_lahir" readonly="readonly" style="text-transform: uppercase;"><em> * (dd/mm/yyyy)</em> -->
					    <script>inputDate('tgl_lahir', '', false);</script>
					    </td>
					  </tr>
					  <tr>
					    <th>No. Telp</th>
					    <td style="text-align: left;" colspan="3"><input type="text" name="no_telp" id="no_telp" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" style="text-transform: uppercase;"></td>
					  </tr>
					  <tr>
					    <th>Agen</th>
					    <td style="text-align: left;" colspan="3"><!-- <input type="text" name="no_telp" id="no_telp" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)"> -->
					    	<select name="cmb_agen" id="cmb_agen" onchange="other_agen(this.value);">
						        <c:forEach items="${agent}" var="r" varStatus="s">
						        	<option value="${r.KEY}">${r.VALUE}</option>
						        </c:forEach>
						        <option value="0">OTHERS</option>
						      </select><em> * Dipilih untuk jenis referensi nasabah</em>
					    </td>
					  </tr>
					  <tr>
					  	<th>Cabang</th>
					  	<td style="text-align: left;" colspan="3"><input type="text" size="40" name="cbng_agen1" id="cbng_agen1" style="background-color: #D4D4D4;text-transform: uppercase; " value="${lsrg }"></td>
					  </tr>
					  <tr>
					    <th>Other</th>
					    <td style="text-align: left;" colspan="3">
					    
					    	<table class="entry" style="text-transform: uppercase;width: 400px;">
							  <tr>
							  	<th>Kode Agen</th>
							    <td style="text-align: left;"><input type="text" size="8" name="kd_agen" id="kd_agen" readonly="readonly" style="background-color: #D4D4D4;text-transform: uppercase;" onchange="getAgen();"></td>
							    <th>Agen</th>
							    <td style="text-align: left;"><input type="text" size="40" name="agen" id="agen" readonly="readonly" style="background-color: #D4D4D4;text-transform: uppercase; "></td>
							  </tr>
							  <tr>
							    <th>Cabang</th>
							    <td style="text-align: left;" colspan="3"><input type="text" size="40" name="cbng_agen" id="cbng_agen" style="background-color: #D4D4D4;text-transform: uppercase; "></td>
							  </tr>
							</table>
					    
					  </tr>
					  <tr>
					    <td><input type="Submit" name="btnSimpan" class="button" value="Simpan">
					    	<input type="hidden" name="agen1" id="agen1">
					    	<input type="hidden" name="kd_agen1" id="kd_agen1">
					    	<input type="hidden" name="email_agen" id="email_agen">
					    	<input type="hidden" name="email_agen1" id="email_agen1" value="${email_agen}">
					    	<input type="hidden" name="cbng_agen2" id="cbng_agen2" value="${lsrg }">
					    </td>
					    <td>&nbsp;</td>
					  </tr>
					</table>
				</fieldset>
			</td>
		</tr>
		<tr>
			<td>
				<c:if test="${not empty daftar}">
				<fieldset>
					<legend style="font-size: 12px; color: black; font-weight: bolder;">Data Yang Di Referensi</legend>
					<table class="entry" style="text-transform: uppercase;">
					  <tr>
					    <th>No</th>
					    <th>Tanggal Pembuatan</th>
					    <th>Nama</th>
					    <th>Tgl. Lahir</th>
					    <th>No. Telp</th>
					    <th>Kode Agent</th>
					    <th>Nama Agent</th>
					    <th>Jenis Referensi</th>
					  </tr>
					  <c:forEach items="${daftar}" var="r" varStatus="s">
					  <tr>
					    <td>${r.ORDER_NUMBER}</td>
					    <td><fmt:formatDate value="${r.CREATE_DATE}" pattern="dd/MM/yyyy"/></td>
					    <td>${r.NAME}</td>
					    <td><fmt:formatDate value="${r.BIRTH_DATE}" pattern="dd/MM/yyyy"/></td>
					    <td>${r.CONTACT_NUMBER}</td>
					    <td>${r.MSAG_ID}<c:if test="${empty r.MSAG_ID}">-</c:if></td>
					    <td>${r.MSAG_NAME}<c:if test="${empty r.MSAG_NAME}">-</c:if></td>
					    <td>${r.FLAG}</td>
					  </tr>
					  </c:forEach>
					</table>
				</fieldset>
				</c:if>
			</td>
		</tr>
		<tr>
			<td>
				<c:if test="${not empty produk}">
				<fieldset>
					<legend style="font-size: 12px; color: black; font-weight: bolder;">Produk</legend>
					<table class="entry" style="text-transform: uppercase;">
					  <tr>
					    <th>Item</th>
					    <th>Point</th>
					  </tr>
					  <c:forEach items="${produk}" var="r" varStatus="s">
					  <tr>
					    <td>${r.NAMA_ITEM}</td>
					    <td>${r.NILAI_POINT}</td>
					  </tr>
					  </c:forEach>
					</table>
				</fieldset>
				</c:if>
			</td>
		</tr>
	</table>
</form>
</center>
</body>
</html>	
