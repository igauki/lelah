<%@ include file="/include/page/header.jsp"%><c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>
<script type="text/javascript" src="${path }/include/js/bac/tertanggung.js"></script>
<script>
// BEBERAPA JAVASCRIPT DIPINDAHKAN KE TERTANGGUNG.JS UNTUK MENGHINDARI ERROR EXCEEDING THE 65535 BYTES LIMIT.


var v_path = '${path}';
var v_relasi = '${cmd.pemegang.lsre_id}';
var v_p_sex = '${cmd.pemegang.mspe_sex}';


<spring:bind path="cmd.pemegang.jmlkyc"> 
	var jmlDaftarKyc=${status.value};
</spring:bind>

<spring:bind path="cmd.pemegang.jmlkyc2"> 
	var jmlDaftarKyc2=${status.value};
</spring:bind>


	function Body_onload() {

	var nameSelection
		xmlData.async = false;
		xmlData.src = "/E-Lions/xml/SUMBER_PENDANAAN.xml";
		xmlData.src = "/E-Lions/xml/SUMBER_PENGHASILAN.xml";
		generateXML_sumberKyc(xmlData, 'ID','DANA');

		document.frmParam.alamat_sementara.style.display="none";

		<c:if test="${not empty cmd.pemegang.keterangan_blanko_spaj}">
			if(confirm("Nomor Blanko yang dicantumkan sudah pernah digunakan untuk produk yang sama.\nTekan YES untuk melihat data sebelumnya, atau CANCEL untuk melanjutkan input.")){
				//FIXME : popup window diwsini
				popWin('${path}/uw/view.htm?p=v&c=c&showSPAJ=${cmd.pemegang.keterangan_blanko_spaj}', 500, 800);
			}
		</c:if>
		
		<c:if test="${not empty cmd.pemegang.blacklist}">
			if(confirm("Ada Riwayat Blacklist Pada Pemegang. \n Tekan OK untuk melihat data riwayat, atau CANCEL untuk melanjutkan input.")){
				//FIXME : popup window diwsini
				popWin('${path}/bac/multi.htm?window=view_attentionlist&blacklist=${cmd.pemegang.blacklist}&tgl_lahir=${cmd.pemegang.mspe_date_birth}', 900, 850);
			}
		</c:if>

		document.frmParam.elements['tertanggung.mste_age'].style.backgroundColor ='#D4D4D4';

		
		/* if  (((document.frmParam.elements['tertanggung.mkl_pendanaan'].value).toUpperCase()!=("Lainnya").toUpperCase()) && (document.frmParam.elements['tertanggung.mkl_pendanaan'].value!=null))
		{
			document.frmParam.elements['tertanggung.danaa'].value='';
		}else{
			if  ((document.frmParam.elements['tertanggung.danaa'].value).toUpperCase()==("Lainnya").toUpperCase())
			{
				document.frmParam.elements['tertanggung.danaa'].value='';
			}
		} */
		
		
		firstRelationCheck();
		
		if (document.frmParam.document.frmParam.tanda_pp.value==null || document.frmParam.document.frmParam.tanda_pp.value=="")
		{
			document.frmParam.elements['tertanggung.mkl_dana_from'].value = '0';
			document.frmParam.elements['tertanggung.mkl_sumber_premi'].value = '0';
			document.frmParam.document.frmParam.tanda_pp.value = '0';
		}
		
		cpy(document.frmParam.tanda_pp.value);
		
		if (document.frmParam.document.frmParam.tanda_hub.value==null || document.frmParam.document.frmParam.tanda_hub.value=="")
		{
			document.frmParam.elements['tertanggung.mkl_sumber_premi'].value = '0';
			document.frmParam.document.frmParam.tanda_hub.value = '0';
		}
		
		cpy2(document.frmParam.tanda_hub.value);
	}
	
		
</script>
<script language="JavaScript">
<!--
	if (!document.layers&&!document.all&&!document.getElementById)
		event="test";
		
 
// -->
</script>


<body onLoad="Body_onload();">

<XML ID=xmlData></XML>
<form name="frmParam" method="post">
<table class="entry2">
	<tr  >
		<td>
			<input type="submit" name="_target0" value=" "  <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/pp1.jpg);" 
				accesskey="1" onmouseover="return overlib('Alt-1', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<c:choose><c:when test="${(sessionScope.currentUser.jn_bank eq '16')}">
			<input type="submit" name="_target1" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ttgbsim.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();"></c:when><c:otherwise>
			<input type="submit" name="_target1" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ttg1.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();"></c:otherwise></c:choose>	
			<input type="submit" name="_target7" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ppremi1.jpg);"
				accesskey="7" onmouseover="return overlib('Alt-7', AUTOSTATUS, WRAP);" onmouseout="nd();">			
			<input type="submit" name="_target2" value=" "  <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ddu1.jpg);"
				accesskey="3" onmouseover="return overlib('Alt-3', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target3" value=" "  <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/inv1.jpg);"
				accesskey="4" onmouseover="return overlib('Alt-4', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target4" value=" "  <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ag1.jpg);"
				accesskey="5" onmouseover="return overlib('Alt-5', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target5" value=" " <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/kf1.jpg);"
				accesskey="6" onmouseover="return overlib('Alt-6', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" name="dummy2" value=" "  <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/sb1.jpg);">
			<textarea cols="40" rows="7" name="alamat_sementara"    
				onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); ">${cmd.addressbilling.msap_address}</textarea>
		
</td>
	</tr>
	<tr>
		<td width="67%" align="left">

			<c:if test="${not empty cmd.pemegang.keterangan_blanko_spaj}">
				<div id="error">INFO:<br>
			  		${cmd.pemegang.keterangan_blanko}
				</div>
			</c:if>

		<spring:bind path="cmd.*">
			<c:if test="${not empty status.errorMessages }">
				<div id="error">INFO:<br>
				<c:forEach var="error" items="${status.errorMessages}">
														- <c:out value="${error}" escapeXml="false" />
					<br />
				</c:forEach></div>
			</c:if>
		</spring:bind>
		<table class="entry">
			<tr>
				<td colspan="6" style="text-align: center;">
				<input type="submit"
					name="_target${halaman-1}" value="Prev &laquo;" 
					onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input
					type="submit" name="_target${halaman+1}" value="Next &raquo;"
					onClick="next()" 
					onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="hidden" name="_page"
					value="${halaman}">
					<spring:bind path="cmd.pemegang.keterangan_blanko">
					<input type="hidden" name="${status.expression}"
						value="${status.value }" size="42" maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind>
	</td>
			</tr>
			<tr>
				<th colspan="6"><font size="1" face="Verdana" color="#CC3300">
				(
				<c:choose>
					<c:when test="${cmd.contactPerson.pic_jenis eq '1'}">
						Hanya diisi apabila PIC berbeda dengan calon Tertanggung
					</c:when>
					<c:otherwise>
						Hanya diisi apabila Pemegang Polis berbeda dengan calon Tertanggung
					</c:otherwise>
				</c:choose>
				)</font></th>
			</tr>
			<tr>
				
            <th class="subtitle" colspan="6">DATA CALON TERTANGGUNG </th>
			</tr>
			<tr>
				<th width="28">1.</th>
				<th width="280">Nama Lengkap <br>
				<span class="info">(sesuai dengan KTP / Identitas lain tanpa gelar)</span>
				</th>
				<th width="498">
				
				<select name="tertanggung.lti_id">
					<option value=""></option>
					<c:forEach var="l" items="${select_gelar}">
						<option
							<c:if test="${cmd.tertanggung.lti_id eq l.ID}"> SELECTED </c:if>
							value="${l.ID}">${l.GELAR}</option>
					</c:forEach>
				</select>				
				
				<spring:bind path="cmd.tertanggung.mcl_first">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind>
				<font color="#CC3300">*</font></th>
				<th width="171">Gelar</th>
				<th width="245"><spring:bind path="cmd.tertanggung.mcl_gelar">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="15">
				</spring:bind></th>
			</tr>
			<tr>
				<th>2.</th>
				<th>Nama Alias <span class="info">(bila ada)</span></th>
				<th colspan="3"><spring:bind path="cmd.tertanggung.mcl_first_alias">
					<input type="text" name="${status.expression}" value="${status.value}" size="42" maxlength="50"/>
				</spring:bind></th>
			</tr>
			<tr>
				<th>3.</th>
				<th>Nama Ibu Kandung</th>
				<th colspan="3"><spring:bind path="cmd.tertanggung.mspe_mother">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="50"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>4.</th>
				<th>Bukti Identitas</th>
				<th colspan="3"><select name="tertanggung.lside_id" >
					<c:forEach var="identitas" items="${select_identitas}">
						<option
							<c:if test="${cmd.tertanggung.lside_id eq identitas.ID}"> SELECTED </c:if>
							value="${identitas.ID}">${identitas.TDPENGENAL}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th></th>
				<th>No. KTP / Identitas lain</th>
				<th colspan="3"><spring:bind path="cmd.tertanggung.mspe_no_identity">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="50"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind>
				<font color="#CC3300">*</font></th>
			</tr>			
			<tr>
				<th></th>
				<th>Tanggal Kadaluarsa</th>
				<th colspan="3"><spring:bind path="cmd.tertanggung.mspe_no_identity_expired">
					<c:if test="${cmd.pemegang.lsre_id eq 1}">
						<script>inputDate('${status.expression}', '${status.value}', true);</script>
					</c:if>
					<c:if test="${cmd.pemegang.lsre_id ne 1}">
						<script>inputDate('${status.expression}', '${status.value}', false);</script>
					</c:if>
				</spring:bind><font color="#CC3300">*</font></th>
			</tr>			
			<tr>
				<th>5.</th>
				<th>Warga Negara</th>
				<th><select name="tertanggung.lsne_id" >
					<c:forEach var="negara" items="${select_negara}">
						<option
							<c:if test="${cmd.tertanggung.lsne_id eq negara.ID}"> SELECTED </c:if>
							value="${negara.ID}">${negara.NEGARA}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></th>
				<th>Umur Beasiswa</th>
				<th><spring:bind path="cmd.tertanggung.mspo_umur_beasiswa">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="3" maxlength="2"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind><font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th>6.</th>
				<th>Apakah Anda warga negara Amerika Serika ? Pemegang Green Card ?</th>
				<th><spring:bind path="cmd.tertanggung.mcl_green_card">
					<label for="ya"> <input type="radio" class=noBorder
						name="${status.expression}" value="1"
						<c:if test="${cmd.tertanggung.mcl_green_card eq '1' or cmd.tertanggung.mcl_green_card eq null}"> 
									checked</c:if>
						id="ya">Ya </label>
					<label for="tidak"> <input type="radio" class=noBorder
						name="${status.expression}" value="0"
						<c:if test="${cmd.tertanggung.mcl_green_card eq '0'}"> 
									checked</c:if>
						id="tidak">Tidak <font color="#CC3300">*</font>
				</spring:bind> </label></th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>7.</th>
				<th>Tanggal Lahir</th>
				<th><spring:bind path="cmd.tertanggung.mspe_date_birth">
					<c:if test="${cmd.pemegang.lsre_id eq 1}">
						<script>inputDate('${status.expression}', '${status.value}', true);</script>
					</c:if>
					<c:if test="${cmd.pemegang.lsre_id ne 1}">
						<script>inputDate('${status.expression}', '${status.value}', false);</script>
					</c:if>
				</spring:bind><font color="#CC3300">*</font></th>
				<th>Usia</th>
				<th><spring:bind path="cmd.tertanggung.mste_age">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="4" readOnly>
				</spring:bind> tahun</th>
			</tr>
			<tr>
				<th></th>
				<th>di</th>
				<th colspan="3"><spring:bind path="cmd.tertanggung.mspe_place_birth">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="30"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind><font color="#CC3300">*</font></th>
			</tr>
				<tr>
				<th>8.</th>
				<th>Nama Lembaga/Perusahaan Tempat Bekerja</th>
				<th><spring:bind path="cmd.tertanggung.mcl_company_name">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="50"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind><font color="#CC3300">*</font></th>
				<th>Uraian Tugas</th>
				<th><spring:bind path="cmd.tertanggung.mkl_kerja_ket">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="40" maxlength="50"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				<th>Pekerjaan* dan Jabatan</th>
				<th colspan="3">
				<spring:bind path="cmd.tertanggung.mkl_kerja">
					<select name="${status.expression}"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
						<c:forEach var="d" items="${select_lst_pekerjaan}">
							<option <c:if test="${status.value eq d.key}"> SELECTED </c:if>
								value="${d.key}">${d.value}</option>
						</c:forEach>
					</select>
				</spring:bind><font color="#CC3300">*</font></th>	
			</tr>
			<tr>
				<th>9.</th>
				<th>Jenis Kelamin</th>
				<th>
				<label for="cowok"> <input class="noBorder" type="radio"
						name="jenis_kelamin" value="1"  onClick="kelamin('1');"
						<c:if test="${cmd.tertanggung.mspe_sex eq 1 or cmd.tertanggung.mspe_sex eq null}" > 
              checked</c:if>
						id="cowok" >Pria </label>
			  <label for="cewek"> <input class="noBorder" type="radio"
						name="jenis_kelamin" value="0" onClick="kelamin('0');"
						<c:if test="${cmd.tertanggung.mspe_sex eq 0}" > 
              checked</c:if>
						id="cewek" >Wanita </label>
			  
			  <spring:bind path="cmd.tertanggung.mspe_sex">
					<input type="hidden" name="${status.expression}"
						value="${status.value }"  >
				</spring:bind>
			  <font color="#CC3300">*</font>
				</th>
				<th>Status</th>
				<th><select name="tertanggung.mspe_sts_mrt" >
					<c:forEach var="marital" items="${select_marital}">
						<option
							<c:if test="${cmd.tertanggung.mspe_sts_mrt eq marital.ID}"> SELECTED </c:if>
							value="${marital.ID}">${marital.MARITAL}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></th>
			</tr>	
			<tr>
				<script type="text/javascript">
					function agamaChange(flag){					
						if(flag=='6'){
							
							document.frmParam.elements['tertanggung.mcl_agama'].readOnly = false;
						}else{
							document.frmParam.elements['tertanggung.mcl_agama'].value="";
							document.frmParam.elements['tertanggung.mcl_agama'].readOnly = true;
						}
					}
				</script>
				<th>10.</th>
				<th>Agama</th>
				<th colspan="3"><select name="tertanggung.lsag_id" onchange="agamaChange(this.value);">
					<c:forEach var="agama" items="${select_agama}" >
						<option
							<c:if test="${cmd.tertanggung.lsag_id eq agama.ID}"> SELECTED </c:if>
							value="${agama.ID}">${agama.AGAMA}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th></th>
				<th>Lain - Lain, Sebutkan</th>
				<th colspan="3">					
					<spring:bind path="cmd.tertanggung.mcl_agama">
						<input type="text" name="${status.expression}"
							value="${status.value }" size="25" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
					</spring:bind>
				</th>
			</tr>
			<tr>
				<th>11.</th>
				<th>Pendidikan</th>
				<th colspan="3"><select name="tertanggung.lsed_id" >
					<c:forEach var="pendidikan" items="${select_pendidikan}">
						<option
							<c:if test="${cmd.tertanggung.lsed_id eq pendidikan.ID}"> SELECTED </c:if>
							value="${pendidikan.ID}">${pendidikan.PENDIDIKAN}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th rowspan="5">12. a.</th>
				<th rowspan="5">Alamat Rumah<br><span class="info">(Wajib diisi sesuai KTP)</span></th>
				<th rowspan="5"><spring:bind path="cmd.tertanggung.alamat_rumah">
					<textarea cols="33" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>${status.value }</textarea>
				</spring:bind><font color="#CC3300">*</font></th>
				<th>Kode Pos</th>
				<th><spring:bind path="cmd.tertanggung.kd_pos_rumah">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="10"\
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>Kota</th>
				<th>
				<spring:bind path="cmd.tertanggung.kota_rumah">
				<input type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
		    	<span id="indicator_rumah" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
			</spring:bind> 
				</th>
			</tr>
			<tr>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>No Telepon 1</th>
				<th><spring:bind path="cmd.tertanggung.area_code_rumah">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>><font color="#CC3300">*</font>
				</spring:bind> <spring:bind path="cmd.tertanggung.telpon_rumah">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind><font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th>No Telepon 2</th>
				<th><spring:bind path="cmd.tertanggung.area_code_rumah2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.tertanggung.telpon_rumah2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				<th>No HandPhone 1</th>
				<th><spring:bind path="cmd.tertanggung.no_hp">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20" 
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
				<th>Email</th>
				<th><spring:bind path="cmd.tertanggung.email">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="40" 
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				
            <th>No HandPhone 2</th>
				<th><spring:bind path="cmd.tertanggung.no_hp2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th rowspan="6">b.</th>
				<th rowspan="6">Alamat Kantor</th>
				<th rowspan="6"><spring:bind path="cmd.tertanggung.alamat_kantor">
					<textarea cols="33" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>${status.value }</textarea>
				</spring:bind></th>
				<th>Kode Pos</th>
				<th><spring:bind path="cmd.tertanggung.kd_pos_kantor">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="10"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>Kota</th>
				<th>
					<spring:bind path="cmd.tertanggung.kota_kantor">
				<input type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
		    	<span id="indicator_kantor" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
			</spring:bind> 		
				</th>
			</tr>
			<tr>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>No Telepon1</th>
				<th><spring:bind path="cmd.tertanggung.area_code_kantor">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.tertanggung.telpon_kantor">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>No Telepon 2</th>
				<th><spring:bind path="cmd.tertanggung.area_code_kantor2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.tertanggung.telpon_kantor2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>No Fax</th>
				<th><spring:bind path="cmd.tertanggung.area_code_fax">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.tertanggung.no_fax">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
				<tr>
				<th rowspan="4">c.</th>
				<th rowspan="4">Alamat Tempat Tinggal<br><span class="info">(Wajib diisi hanya jika tidak sesuai KTP)</span></th>
				<th rowspan="4"><spring:bind path="cmd.tertanggung.alamat_tpt_tinggal">
					<textarea cols="34" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
						</c:if>
						>${status.value }</textarea>
				<font color="#CC3300">*</font></spring:bind></th>
				<th>Kode Pos</th>
				<th><spring:bind path="cmd.tertanggung.kd_pos_tpt_tinggal">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="10"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>Kota</th>
				<th ><spring:bind path="cmd.tertanggung.kota_tpt_tinggal"><br>
				<input type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
		    	<span id="indicator_rumah" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
				</spring:bind></td>
			</tr>
			<tr><th></th><th></th></tr>
			<tr><th></th><th></th></tr>

			<tr>
				<th></th>
				<th>No HandPhone 1</th>
				<th><spring:bind path="cmd.addressbilling.no_hp">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind><br/><font color="#CC3300">* apabila tidak ada no hp, harap isi dengan 0000</font></th>
				<th>No Fax</th>
				<th><spring:bind path="cmd.addressbilling.msap_area_code_fax1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.addressbilling.msap_fax1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				<th>No HandPhone 2</th>
				<th><spring:bind path="cmd.addressbilling.no_hp2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind><br/><font color="#CC3300">* apabila tidak ada no hp, harap isi dengan 0000</font></th>
				<th>Email</th>
				<th><spring:bind path="cmd.addressbilling.e_mail">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="40"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>13.</th>
				<th>Total Pendapatan Bersih Per Tahun</th>
				<th colspan="3">
					<select name="tertanggung.mkl_penghasilan">
						<c:forEach var="penghasilan" items="${select_penghasilan}">
							<option
								<c:if test="${cmd.tertanggung.mkl_penghasilan eq penghasilan.ID}"> SELECTED </c:if>
								value="${penghasilan.ID}">${penghasilan.PENGHASILAN}</option>
						</c:forEach>
					</select><font color="#CC3300">*</font>
				</th>
			</tr>
			<tr>
				<th>14.</th>
				<th>Sumber Pendanaan Pembelian Asuransi</th>
				<th colspan="3"><spring:bind path="cmd.tertanggung.bulan_gaji">
					<input type="checkbox" name="bulan_gaji_tt" class="noBorder" value="${cmd.tertanggung.bulan_gaji}"  size="30" onClick="tt_bulan();"
						<c:if test="${status.value eq 'GAJI'}">checked="checked"</c:if>> 
					<input type="hidden" name="${status.expression}" value="${cmd.tertanggung.bulan_gaji}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Gaji
					<spring:bind path="cmd.tertanggung.bulan_penghasilan"> 
					<input type="checkbox" name="bulan_penghasilan_tt" class="noBorder"  value="${cmd.tertanggung.bulan_penghasilan}"  size="30" onClick="tt_penghasilan();"
		            		<c:if test="${status.value eq 'TABUNGAN/DEPOSITO'}">checked="checked"</c:if>> 
		             <input type="hidden" name="${status.expression}" value="${cmd.tertanggung.bulan_penghasilan}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind> Tabungan/Deposito
					<spring:bind path="cmd.tertanggung.bulan_orangtua"> 
		            <input type="checkbox" name="warisan_tt" class="noBorder"  value="${cmd.tertanggung.bulan_orangtua}"  size="30" onClick="tt_warisan();"
					  		<c:if test="${status.value eq 'WARISAN'}">checked="checked"</c:if>> 
		             <input type="hidden" name="${status.expression}" value="${cmd.tertanggung.bulan_orangtua}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind> Warisan
					<spring:bind path="cmd.tertanggung.bulan_usaha"> 
		            <input type="checkbox" name="bulan_usaha_tt" class="noBorder"  value="${cmd.tertanggung.bulan_usaha}"  size="30" onClick="tt_usaha();"
									 <c:if test="${status.value eq 'HIBAH'}">checked="checked"</c:if>> 
					<input type="hidden" name="${status.expression}" value="${cmd.tertanggung.bulan_usaha}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Hibah
					<spring:bind path="cmd.tertanggung.bulan_lainnya"> 
		            <input type="checkbox" name="bulan_lainnya_tt" class="noBorder"  value="${cmd.tertanggung.bulan_lainnya}"  size="30" onClick="tt_lainnya();"
							 <c:if test="${status.value eq 'LAINNYA'}">checked="checked"</c:if>> 
					<input type="hidden" name="${status.expression}" value="${cmd.tertanggung.bulan_lainnya}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind> Lainnya
					<spring:bind path="cmd.tertanggung.bulan_lainnya_note">
						<input type="text" name="${status.expression}"
							value="${status.value }" size="32" maxlength="40"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
					</spring:bind>
			<%-- <tr>
				<th colspan = "5">
					<table id="tableDanax" class="entry2">
					<tr>
						<th width = "33">&nbsp;</th>
						<th width = "367">&nbsp;</th>
						<th width = "464">

						<select name="tertanggung.mkl_pendanaan">
							<c:forEach var="dana" items="${select_dana}">
								<option
									<c:if test="${cmd.tertanggung.mkl_pendanaan eq dana.ID}"> SELECTED </c:if>
									value="${dana.ID}">${dana.DANA}
								</option>
							</c:forEach>
						</select>
				 				<font color="#CC3300">*</font>
						</th>
						<th width = "320">
						<spring:bind path="cmd.tertanggung.danaa">
							<input type="text" name="${status.expression}"
								value="${status.value }" size="42" maxlength="100"
								<c:if test="${ not empty status.errorMessage}">
									style='background-color :#FFE1FD'
								</c:if>>
						</spring:bind>
						</th>
						<th width = "61">&nbsp;</th>
					</tr>
						
						
					</table>
				</th> --%>
			</tr>
			<tr>
				<th>15.</th>
				<th>Tujuan Membeli Asuransi</th>
				<th colspan="3"><%-- <select name="tertanggung.mkl_tujuan" >
					<c:forEach var="tujuan" items="${select_tujuan}">
						<option
							<c:if test="${cmd.tertanggung.mkl_tujuan eq tujuan.ID}"> SELECTED </c:if>
							value="${tujuan.ID}">${tujuan.TUJUAN}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font> --%>
				<spring:bind path="cmd.tertanggung.tujuan_proteksi"> 
		            	<input type="checkbox" name="tujuan_proteksi_tt" class="noBorder" value="${cmd.tertanggung.tujuan_proteksi}"  size="30" onClick="tt_proteksi();"
								<c:if test="${status.value eq 'PROTEKSI'}">checked="checked"</c:if>> 
						<input type="hidden" name="${status.expression}" value="${cmd.tertanggung.tujuan_proteksi}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Proteksi
					<spring:bind path="cmd.tertanggung.tujuan_investasi"> 
		            	<input type="checkbox" name="tujuan_investasi_tt" class="noBorder"  value="${cmd.tertanggung.tujuan_investasi}"  size="30" onClick="tt_investasi();"
								<c:if test="${status.value eq 'INVESTASI'}">checked="checked"</c:if>> 
						<input type="hidden" name="${status.expression}" value="${cmd.tertanggung.tujuan_investasi}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Investasi
					<spring:bind path="cmd.tertanggung.tujuan_lainnya"> 
		            <input type="checkbox" name="tujuan_lainnya_tt" class="noBorder"  value="${cmd.tertanggung.tujuan_lainnya}"  size="30" onClick="tttuj_lainnya();"
								<c:if test="${status.value eq 'LAINNYA'}">checked="checked"</c:if>> 
					<input type="hidden" name="${status.expression}" value="${cmd.tertanggung.tujuan_lainnya}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Lain-lain
				</th>
			</tr>
			<%-- <tr>
				<th></th>
				<th>Lain-Lain, Jelaskan</th>
				<th colspan="3"><spring:bind path="cmd.tertanggung.tujuana">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr> --%>
			<tr>
				<th colspan = "5">&nbsp;</th>
			</tr>
			<tr>
				<th class="subtitle" colspan="5"></th>
			</tr>
			  <tr> 
            <td colspan="9"> 
			 <p align="left"><b> <font face="Verdana" size="2" color="#CC3300">Note : * Wajib diisi</font></b></p>
			</td>
          </tr>
         <c:if test="${cmd.tertanggung.lspd_id eq 2 or cmd.tertanggung.lspd_id eq 27 or cmd.tertanggung.lspd_id eq 209}">
         	<c:if test="${not empty cmd.pemegang.reg_spaj}">         	
	          <tr>          	 
	          	 <td colspan="2">
					 <input type="checkbox" name="edit_tertanggung" id="edit_tertanggung" class="noBorder" value ="${cmd.tertanggung.edit_tertanggung}" <c:if test="${cmd.tertanggung.edit_tertanggung eq 1}"> 
						checked</c:if> size="30" onClick="edit_onClick1();"> Telah dilakukan edit	
					 <spring:bind path="cmd.tertanggung.edit_tertanggung"> 
		          		<input type="hidden" name="${status.expression}" value="${cmd.tertanggung.edit_tertanggung}"  size="30" style='background-color :#D4D4D4'readOnly>
					 </spring:bind>   
	          	 </td>
				 <td colspan="7">
				 		<spring:bind path="cmd.tertanggung.kriteria_kesalahan">
							<select name="${status.expression}">
								<c:forEach var="a" items="${select_kriteria_kesalahan}">
									<option
										<c:if test="${cmd.tertanggung.kriteria_kesalahan eq a.KESALAHAN}"> SELECTED </c:if>
										value="${a.KESALAHAN}">${a.KESALAHAN}
									</option>
								</c:forEach>
							</select>
						</spring:bind>
				 </td> 
	          </tr>
	          <tr>
	          	<td colspan="9">&nbsp;</td>
	          </tr>
          	</c:if>
          </c:if>
		  	<tr>
				<td colspan="5" style="text-align: center;">
					<input type="hidden" name="hal" value="${halaman}">
					<input type="hidden" name="_page" value="${halaman}">
					<spring:bind path="cmd.pemegang.indeks_halaman">
						<input type="hidden" name="${status.expression}"
							value="${halaman-1}" size="30">
					</spring:bind>				
					<input type="submit" name="_target${halaman-1}" value="Prev &laquo;" 
						accesskey="P" onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
					<input type="submit" name="_target${halaman+1}" value="Next &raquo;" onClick="next()" 
						accesskey="N" onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
				</td>
			</tr>
		</table>
		
		</td>
	</tr>
</table>
</form>
<ajax:autocomplete
				  source="tertanggung.kota_rumah"
				  target="tertanggung.kota_rumah"
				  baseUrl="${path}/servlet/autocomplete?s=tertanggung.kota_rumah&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_rumah"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />

<ajax:autocomplete
				  source="tertanggung.kota_kantor"
				  target="tertanggung.kota_kantor"
				  baseUrl="${path}/servlet/autocomplete?s=tertanggung.kota_kantor&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_kantor"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />
<ajax:autocomplete
				  source="tertanggung.kota_tpt_tinggal"
				  target="tertanggung.kota_tpt_tinggal"
				  baseUrl="${path}/servlet/autocomplete?s=tertanggung.kota_tpt_tinggal&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_rumah"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />
<script type="text/javascript">	
	agamaChange('${cmd.tertanggung.lsag_id}');
</script>
</body>
<%@ include file="/include/page/footer.jsp"%>