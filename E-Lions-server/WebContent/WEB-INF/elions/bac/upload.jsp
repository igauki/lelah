<%@ include file="/include/page/header.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<script>
	function submitForm(tombol, tipe){
		//tombol.disabled=true;
		document.formpost.mode.value=tipe;
		//document.formpost.submit();
		//if(tipe=='download') tombol.disabled=false;
		if(tipe=='upload') {
			return confirm('Anda yakin ingin upload dokumen?');
		}
	}
</script>
<body onload="setupPanes('container1', 'tab1');">
	
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Upload SPAJ</a>
			</li>
		</ul>

		<div class="tab-panes">
			<div id="pane1" class="panes">
				<form:form id="formpost" name="formpost" commandName="cmd" method="post" enctype="multipart/form-data">
					<form:hidden path="mode"/>
					<table class="entry2">
						<tr>
							<th style="width: 200px;">Download Form</th>
							<td>
								<form:select path="form">
									<form:options items="${daftarForm}" itemLabel="value" itemValue="key"/>
								</form:select>
								<input type="submit" name="_finish" value="Download" onclick="return submitForm(this, 'download');"/>
							</td>
						</tr>
						<tr>
							<th>Upload File <span class="info">* Max. 1 Mb (1024 Kb)</span></th>
							<td>
								<input type="file" name="file" />

								<input type="submit" name="_target${halaman+1}" onclick="return submitForm(this, 'upload');"
													value="Upload &raquo;" 
													onmouseover="return overlib('Alt-U', AUTOSTATUS, WRAP);" onmouseout="nd();">

								<input type="hidden" name="_page" value="${halaman}">
								
							</td>
						</tr>
						<tr>
							<th></th>
							<td>
								<spring:bind path="cmd.*">
									<c:choose>
										<c:when test="${not empty status.errorMessages}">
											<div id="error">
											ERROR:<br>
											<c:forEach var="error" items="${status.errorMessages}">
														- <c:out value="${error}" escapeXml="false" />
												<br />
											</c:forEach></div>
										</c:when>
										<c:otherwise>
											<div id="success" style="text-transform: none;">
												Cara Penggunaan: <br>
												1. Download dahulu file excel dengan menekan tombol DOWNLOAD<br>
												2. Isi data dengan lengkap pada file tersebut, lalu UPLOAD kembali file nya<br>
												3. Apabila ada kesalahan / kurang data, maka pesan error akan ditampilkan<br>
												<br>
												Informasi Tambahan: <br>
												1. Sebelum UPLOAD, harap lengkapi data pada file excel terlebih dahulu, khususnya: <br>
													&nbsp;&nbsp;&nbsp;&nbsp;- <strong>KODE_AGEN</strong> (kode agen penutup) <br>
													&nbsp;&nbsp;&nbsp;&nbsp;- <strong>BEG_DATE</strong> (tanggal mulai berlaku polis) <br>
													&nbsp;&nbsp;&nbsp;&nbsp;- <strong>TGL_SPAJ</strong> (tanggal spaj) <br>
													&nbsp;&nbsp;&nbsp;&nbsp;- <strong>BANK TTP</strong> (4 digit terakhir nomor rekening bank titipan premi) <br>
													&nbsp;&nbsp;&nbsp;&nbsp;- <strong>TGL RK TTP</strong> (tanggal RK titipan premi) <br>
													&nbsp;&nbsp;&nbsp;&nbsp;- <strong>TGL BAYAR TTP</strong> (tanggal pembayaran titipan premi) <br>
											</div>
										</c:otherwise>
									</c:choose>
								</spring:bind>
							</td>
						</tr>
					</table>
					
				</form:form>
			</div>
		</div>
	</div>
	
</body>
<%@ include file="/include/page/footer.jsp"%>