<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<script>
			var today = new Date();

			function info_awal(){
				var info="${cmd.info}";
				if(info=='1')
					setupPanes("container1", "tab7");
				else
					setupPanes("container1", "tab1");
				var suc="${cmd.suc}"
				
				if(suc=='1'){
					alert("Berhasil di simpan ${param.showSPAJ}");
					window.location='${path }/bac/otorisasiInputSpajView.htm?showSPAJ=${param.showSPAJ}';
				}
				
				document.body.style.visibility='visible';

				<c:if test="${not empty daftarPesan}">
					<c:forEach items="${daftarPesan}" var="d">
						alert('${d}');
					</c:forEach>
				</c:if>
			}
			
			function enabledAll()
			{
				for( i=1 ; i<6 ; i++){
					document.getElementById('tab'+i).disabled = false;
				}
			}
			
			function firstDisplayDisabledAll()
			{
				for( i=1 ; i<6 ; i++){
					document.getElementById('tab'+i).disabled = true;
				}
				document.getElementById('tab1').disabled = false;
			}
			
			function disabledAllWithoutMe(index)
			{		
				for( i=1 ; i<6 ; i++){
					if( parseInt(index) == i )
					{
						document.getElementById('tab'+i).disabled = false;
					}
					else
					{
						document.getElementById('tab'+i).disabled = true;
					}
				}
			}
			
			function next( temp, getUrl ){
				if( temp == null || temp == '' )
				{
					enabledAll();
					document.getElementById('temp').value = '2';
					document.getElementById('tab2').click();
					disabledAllWithoutMe('2');
				}
				else
				{
					if( temp == '5' )
					{
						
					}
					else
					{
						enabledAll();
						var tempValue = parseInt(temp) + 1;
						document.getElementById('temp').value = tempValue;
						document.getElementById('tab'+tempValue).click();
						disabledAllWithoutMe(tempValue);
					}
				}
			}
			
			function prev( temp, getUrl ){
				if( temp == null || temp == '' )
				{

				}
				else
				{
					if( temp == '1' )
					{
						
					}
					else
					{
						enabledAll();
						var tempValue = parseInt(temp) - 1;
						document.getElementById('temp').value = tempValue;
						document.getElementById('tab'+tempValue).click();
						disabledAllWithoutMe(tempValue);
					}
				}
			}
		</script>
	</head>
	<body style="height: 100%;text-align: center; visibility: hidden; ">
	<c:choose>
		<c:when test="${empty param.showSPAJ }">
			<div id="success">Silahkan gunakan menu diatas untuk memilih detail SPAJ</div>
		</c:when>
		<c:otherwise>
			<c:if test="${cmd.docPos.LSPD_ID eq 2 }">
				<div id="success">
					Proses Selanjutnya adalah ${cmd.proses}
				</div>
			</c:if>
	
			<fieldset>
				<table class="entry2">
					<tr>
						<th>No. Polis</th>
						<th><input readonly type=text size=20 value="<elions:polis nomor="${cmd.policyNo}"/>"></th>
						<th>No. SPAJ</th>
						<th><input readonly type=text size=15 value="<elions:spaj nomor="${param.showSPAJ}"/>"></th>
						<c:if test="${reins ne null }">
							<th>No. Reinstate</th>
							<th>
								<input type="text" value=" <elions:spaj nomor="${reins}"/>" size="15" readonly>
							</th>
						</c:if>
						<th>Pemegang</th>
						<th><input type="text" value="${cmd.pemegang.mcl_first}" size="35" readonly>
						</th>
						<c:if test="${infoNasabah ne null }">
							<th>Jenis Nasabah</th>
							<th>
								<input type="text" value="${infoNasabah}" size="15" readonly>
							</th>
						</c:if>
					</tr>
				</table>
				<table class="entry2">
					<tr>
						<th>Polis Lama (SURRENDER ENDORSEMENT)</th>
						<th><input readonly type=text size=20 value="${polisLama.MSPO_POLICY_NO_FORMAT}"></th>
					</tr>
				</table>
			</fieldset>
	
			<%-- <div id="success">Processing Time : ${selisih}</div> --%>
	
			<div class="tab-container" id="container1">
			<input type="hidden" id="temp"/>
			<input type="hidden" value="${successOrNo}" id="successOrNoSubmitSpaj"/>
			<input type="hidden" value="${alert}" id="alert"/>
			<input type="hidden" value="${cmd.msgInfo}" id="msgInfo"/>
				<ul class="tabs">
					<c:forEach items="${lsPanes}" var="x" varStatus="xt">
						<li>
							<c:choose>
								<c:when test="${(blacklistFont eq 'red' && ppf eq x.NAMES && deu eq 'uw') || (blacklistFont eq 'red' && terf eq x.NAMES && deu eq 'uw')}">
									<a href="#" onClick="return showPane('pane${xt.count}', this)" 
									onmouseover="return overlib('Alt-${xt.count}', AUTOSTATUS, WRAP);" onmouseout="nd();" readOnly = "true"
									onfocus="return showPane('pane${xt.count}', this)" accesskey="${xt.count}" id="tab${xt.count }"><font color="red">${x.NAMES}</font></a>
								</c:when>
								<c:otherwise>
									<a href="#" onClick="return showPane('pane${xt.count}', this)"  
									onmouseover="return overlib('Alt-${xt.count}', AUTOSTATUS, WRAP);" onmouseout="nd();" readOnly = "true"
									onfocus="return showPane('pane${xt.count}', this)" accesskey="${xt.count}" id="tab${xt.count }" style="">${x.NAMES}</a>
								</c:otherwise>
							</c:choose>
						
						</li>
					</c:forEach>	
				</ul>
				
			<ul class="tabs">
			<li>
			<a href="#" onclick="return prev(document.getElementById('temp').value, this)" onmouseout="nd();">PREV</a>
			&nbsp; &nbsp;
			<a href="#" onclick="return next(document.getElementById('temp').value, this)" onmouseout="nd();">NEXT</a>
			</li>
		 </ul>

				<div class="tab-panes">
					<c:forEach items="${lsPanes}" var="x" varStatus="xt">
						<div id="pane${xt.count}" class="panes">
							<jsp:include page="${x.PAGES}" flush="true" />
						</div>
					</c:forEach>	
				</div>
			</div>
		</c:otherwise>
	</c:choose>
		<div>
			<script>
				window.onload = function() { 
					setTimeout("info_awal()", 1);	
				};
	
				var today2 = new Date();
				window.status = ((today2.valueOf() - today.valueOf()) / 1000);
				firstDisplayDisabledAll();
			</script>
		</div>
	</body>
	
	<script>
	if( document.getElementById('msgInfo').value != null &&  document.getElementById('msgInfo').value != '' )
	{
	 	alert(document.getElementById('msgInfo').value);
	}
	if( document.getElementById('alert').value != null && document.getElementById('alert').value != '' )
	{
		alert(document.getElementById('alert').value);
	}

</script>
	<%@ include file="/include/page/footer.jsp"%>