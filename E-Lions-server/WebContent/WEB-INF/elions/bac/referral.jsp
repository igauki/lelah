<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
</script>
</head>
<BODY onload="resizeCenter(550,400); document.title='PopUp :: New Referral'; setupPanes('container1', 'tab1'); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">New Referral</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost">
					<table class="entry2">
						<tr>
							<th style="width: 115px;" rowspan="2">
								<select name="spaj" size="30" style="width: 110px;">
									<c:forEach var="s" items="${daftarSpaj}" varStatus="st">
										<option value="${s.REG_SPAJ }" style="background-color: ${s.BG};"
											<c:if test="${st.index eq 0}">selected</c:if>>${s.SPAJ_FORMATTED}
										</option>
									</c:forEach>
								</select>
								<br/>
								<input type="button" value="Cari" style="width: 110px;">
							</th>
							<td>
									<table class="entry2">
										<tr>
											<th>Cabang</th>
											<td>&nbsp;</td>							
											<td>&nbsp;</td>							
										</tr>
										<tr>
											<th>Cabang</th>
											<td>&nbsp;</td>							
											<td>&nbsp;</td>							
										</tr>
										<tr>
											<th colspan="3">
												Informasi Referral
											</th>
										</tr>
										<tr>
											<th colspan="3">
												Informasi Pemberi Lead
											</th>
										</tr>
											<th>Proses</th>
											<td>
												<input type="button" value="Info" 				onclick="go_to('info', 'uw/view.htm');"
													onmouseover="return overlib('Informasi Polis', AUTOSTATUS, WRAP);" onmouseout="nd();">
												<input type="button" value="Cover Letter" 	onclick="go_to('cover_letter', 'report/polis.htm?window=cover_letter');"
													onmouseover="return overlib('Cetak Cover Letter', AUTOSTATUS, WRAP);" onmouseout="nd();">
												<input type="button" value="E-mail" 			onclick="go_to('email', 'report/polis.htm?window=email');"
													onmouseover="return overlib('E-mail Softcopy', AUTOSTATUS, WRAP);" onmouseout="nd();">								
												<input type="button" value="Transfer" 		onclick="go_to('transfer', 'report/polis.htm?window=transfer_filling');"
													onmouseover="return overlib('Transfer ke Filling/Tanda Terima', AUTOSTATUS, WRAP);" onmouseout="nd();">
												<input type="button" value="Back" 		onclick="go_to('back', '');" disabled="disabled"
													onmouseover="return overlib('Kembali ke Checking Print Polis', AUTOSTATUS, WRAP);" onmouseout="nd();">
											</td>
										</tr>
									</table>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>

</form>
</body>
<%@ include file="/include/page/footer.jsp"%>