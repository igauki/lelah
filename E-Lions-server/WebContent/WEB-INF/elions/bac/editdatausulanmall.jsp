<%@ include file="/include/page/header_mall.jsp"%>
<link rel="Stylesheet" type="text/css" href="${path }/include/css/mallinsurance.css" media="screen">
<script type="text/javascript" src="${path }/include/js/refund/refund_common.js"></script>

<script language="JavaScript">
<!--
<spring:bind path="cmd.datausulan.jmlrider"> 
var mn=${status.value};
</spring:bind>  

<spring:bind path="cmd.datausulan.jml_peserta"> 
var pesertax=${status.value};
</spring:bind>
var flag_add_x=0;
var flag_add1_x=0;
			
var varOptionrider;
var varOptionins;
var varOptionhub;
var varOptionUp;
var flag_add=0;
var flag_add1=0;
var OptionUp;
var OptionJP;
var OptionProdx;

var lca_id=${sessionScope.currentUser.lca_id};
var cuti= '${cmd.datausulan.mspo_installment}';

	function spesial_onClick(ncek){
		if(ncek) {
			//document.getElementById('flag_special').value=1;
			document.getElementById('special').value = '1'; 
		} else {
			//document.getElementById('flag_special').value=0;
			document.getElementById('special').value = '0';
		} 
	}
	
	function provider_onClick(ncek){
		if(ncek) {
			//document.getElementById('flag_special').value=1;
			document.getElementById('provider').value = '2'; 
		} else {
			//document.getElementById('flag_special').value=0;
			document.getElementById('provider').value = '0';
		} 
	}

	function generateXML(intValue, objParser, nameSelection, strID, strDesc ,lbr,tgg,tab) {
		var strSelection;
		var strHTML = "<SELECT NAME='" + nameSelection  + "'  style='font-family: Verdana; width: " +lbr+"; height:"+tgg+"; font-size:8pt'>";
		var collPosition = objParser.selectNodes( "//Position" );
	//alert(intValue);
		for( var i = 0; i < collPosition.length; i++ ) {
			if ((intValue).toUpperCase() == (collPosition.item( i ).selectSingleNode(strID).text).toUpperCase())
				strSelection = "SELECTED";
			else
				strSelection = "";
			strHTML += "<OPTION " + strSelection + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
		}
		strHTML += "</SELECT>"
		//alert(strHTML);
		return strHTML;
	}	
	
	function generateXML_onChange(intValue, objParser, nameSelection, strID, strDesc ,lbr,tgg,tab) {
		var strSelection;
		var strHTML = "<SELECT NAME='" + nameSelection  + "'  style='font-family: Verdana; width: " +lbr+"; height:"+tgg+"; font-size:8pt'>";
		var collPosition = objParser.selectNodes( "//Position" );
	
		for( var i = 0; i < collPosition.length; i++ ) {
			if ((intValue) == (collPosition.item( i ).selectSingleNode(strID).text))
				strSelection = "SELECTED";
			else
				strSelection = "";
			strHTML += "<OPTION " + strSelection + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
		}
		strHTML += "</SELECT>"
		return strHTML;
	}	

function karyawan_onClick()
{
	if(document.frmParam.mste_flag_el1.checked == true){
		document.frmParam.mste_flag_el1.checked = true;
		document.frmParam.elements['datausulan.mste_flag_el'].value = '1';
	}else{
		document.frmParam.mste_flag_el1.checked = false;
		document.frmParam.elements['datausulan.mste_flag_el'].value = '0';
	}
}
	
//function xml rider
function generateXML_rider( objParser, strID, strDesc, varOption) {
	varOptionrider = "";
	
	if (window.ActiveXObject) {
		var collPosition = objParser.selectNodes( "//Position" );
		for( var i = 0; i < collPosition.length; i++ ) 
			varOptionrider += "<OPTION " + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
	}
	else if (document.implementation && document.implementation.createDocument) {
		var count = 0;
		var content = new Array(2);
		content[0] = new Array();
		
		var nodes = objParser.evaluate("//"+strID, objParser, null, XPathResult.ANY_TYPE, null);
		var result=nodes.iterateNext();
		while (result) {
		  content[0][count++] = result.childNodes[0].nodeValue;
		  result=nodes.iterateNext();
		}
		
		nodes = objParser.evaluate("//"+strDesc, objParser, null, XPathResult.ANY_TYPE, null);
		var result=nodes.iterateNext();
		count = 0;
		content[1] = new Array();
		while (result) {
			content[1][count++] = result.childNodes[0].nodeValue;
			result=nodes.iterateNext();
		}
		
		for(a=0;a<content[0].length;a++) {
			//document.write(content[0][a] + ' ' + content[1][a]);
			//document.write("<br />");
			varOptionrider += "<OPTION " + " VALUE='" + content[0][a] + "'>" + content[1][a] + "</OPTION>";	
		}
	}
	//var collPosition = objParser.selectNodes( "//Position" );
	//var collPosition = objParser.evaluate("//Position", objParser, null, XPathResult.ANY_TYPE,null);
	//for( var i = 0; i < collPosition.length; i++ ) 
	//varOptionrider += "<OPTION " + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
}	

function generateXML_persenUP( objParser, strID, strDesc ) {
	varOptionUp = "";
	
	if (window.ActiveXObject) {
		var collPosition = objParser.selectNodes( "//Position" );
		for( var i = 0; i < collPosition.length; i++ ) 
			varOptionUp += "<OPTION " + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
	}
	else if (document.implementation && document.implementation.createDocument) {
		var count = 0;
		var content = new Array(2);
		content[0] = new Array();
		
		var nodes = objParser.evaluate("//"+strID, objParser, null, XPathResult.ANY_TYPE, null);
		var result=nodes.iterateNext();
		while (result) {
		  content[0][count++] = result.childNodes[0].nodeValue;
		  result=nodes.iterateNext();
		}
		
		nodes = objParser.evaluate("//"+strDesc, objParser, null, XPathResult.ANY_TYPE, null);
		var result=nodes.iterateNext();
		count = 0;
		content[1] = new Array();
		while (result) {
			content[1][count++] = result.childNodes[0].nodeValue;
			result=nodes.iterateNext();
		}
		
		for(a=0;a<content[0].length;a++) {
			//document.write(content[0][a] + ' ' + content[1][a]);
			//document.write("<br />");
			varOptionUp += "<OPTION " + " VALUE='" + content[0][a] + "'>" + content[1][a] + "</OPTION>";	
		}
	}
}

function generateXML_penerima( objParser, strID, strDesc ) {
	varOptionhub = "";
	
	if (window.ActiveXObject) {
		var collPosition = objParser.selectNodes( "//Position" );
		for( var i = 0; i < collPosition.length; i++ ) 
			varOptionhub += "<OPTION " + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
	}
	else if (document.implementation && document.implementation.createDocument) {
		var count = 0;
		var content = new Array(2);
		content[0] = new Array();
		
		var nodes = objParser.evaluate("//"+strID, objParser, null, XPathResult.ANY_TYPE, null);
		var result=nodes.iterateNext();
		while (result) {
		  content[0][count++] = result.childNodes[0].nodeValue;
		  result=nodes.iterateNext();
		}
		
		nodes = objParser.evaluate("//"+strDesc, objParser, null, XPathResult.ANY_TYPE, null);
		var result=nodes.iterateNext();
		count = 0;
		content[1] = new Array();
		while (result) {
			content[1][count++] = result.childNodes[0].nodeValue;
			result=nodes.iterateNext();
		}
		
		for(a=0;a<content[0].length;a++) {
			//document.write(content[0][a] + ' ' + content[1][a]);
			//document.write("<br />");
			varOptionhub += "<OPTION " + " VALUE='" + content[0][a] + "'>" + content[1][a] + "</OPTION>";	
		}
	}
}

function generateXML_jenisPeserta( objParser, strID, strDesc ) {//xxx
	OptionJP = "";
	
	if (window.ActiveXObject) {
		var collPosition = objParser.selectNodes( "//Position" );
		for( var i = 0; i < collPosition.length; i++ ) 
			OptionJP += "<OPTION " + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
	}
	else if (document.implementation && document.implementation.createDocument) {
		var count = 0;
		var content = new Array(2);
		content[0] = new Array();
		
		var nodes = objParser.evaluate("//"+strID, objParser, null, XPathResult.ANY_TYPE, null);
		var result=nodes.iterateNext();
		while (result) {
		  content[0][count++] = result.childNodes[0].nodeValue;
		  result=nodes.iterateNext();
		}
		
		nodes = objParser.evaluate("//"+strDesc, objParser, null, XPathResult.ANY_TYPE, null);
		var result=nodes.iterateNext();
		count = 0;
		content[1] = new Array();
		while (result) {
			content[1][count++] = result.childNodes[0].nodeValue;
			result=nodes.iterateNext();
		}
		
		for(a=0;a<content[0].length;a++) {
			//document.write(content[0][a] + ' ' + content[1][a]);
			//document.write("<br />");
			OptionJP += "<OPTION " + " VALUE='" + content[0][a] + "'>" + content[1][a] + "</OPTION>";	
		}
	}
}

function generateXML_flagJenisProd( objParser, strID, strDesc ) {//xxx
	OptionProdx = "";
	
	if (window.ActiveXObject) {
		var collPosition = objParser.selectNodes( "//Position" );
		for( var i = 0; i < collPosition.length; i++ ) 
			OptionProdx += "<OPTION " + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
	}
	else if (document.implementation && document.implementation.createDocument) {
		var count = 0;
		var content = new Array(2);
		content[0] = new Array();
		
		var nodes = objParser.evaluate("//"+strID, objParser, null, XPathResult.ANY_TYPE, null);
		var result=nodes.iterateNext();
		while (result) {
		  content[0][count++] = result.childNodes[0].nodeValue;
		  result=nodes.iterateNext();
		}
		
		nodes = objParser.evaluate("//"+strDesc, objParser, null, XPathResult.ANY_TYPE, null);
		var result=nodes.iterateNext();
		count = 0;
		content[1] = new Array();
		while (result) {
			content[1][count++] = result.childNodes[0].nodeValue;
			result=nodes.iterateNext();
		}
		
		for(a=0;a<content[0].length;a++) {
			//document.write(content[0][a] + ' ' + content[1][a]);
			//document.write("<br />");
			OptionProdx += "<OPTION " + " VALUE='" + content[0][a] + "'>" + content[1][a] + "</OPTION>";	
		}
	}
}	
	
//function xml ins rider
function generateXML_insrider( objParser, strID, strDesc ) {
	varOptionins = "";
	
	if (window.ActiveXObject) {
		var collPosition = objParser.selectNodes( "//Position" );
		for( var i = 0; i < collPosition.length; i++ ) 
			//alert(collPosition.item( i ).selectSingleNode(strID).text);
			varOptionins += "<OPTION " + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
	}
	else if (document.implementation && document.implementation.createDocument) {
		var count = 0;
		var content = new Array(2);
		content[0] = new Array();
		
		var nodes = objParser.evaluate("//"+strID, objParser, null, XPathResult.ANY_TYPE, null);
		var result=nodes.iterateNext();
		while (result) {
		  content[0][count++] = result.childNodes[0].nodeValue;
		  result=nodes.iterateNext();
		}
		
		nodes = objParser.evaluate("//"+strDesc, objParser, null, XPathResult.ANY_TYPE, null);
		var result=nodes.iterateNext();
		count = 0;
		content[1] = new Array();
		while (result) {
			content[1][count++] = result.childNodes[0].nodeValue;
			result=nodes.iterateNext();
		}
		
		for(a=0;a<content[0].length;a++) {
			//document.write(content[0][a] + ' ' + content[1][a]);
			//document.write("<br />");
			varOptionins += "<OPTION " + " VALUE='" + content[0][a] + "'>" + content[1][a] + "</OPTION>";
		}
	}	
	//var collPosition = objParser.selectNodes( "//Position" );
	//for( var i = 0; i < collPosition.length; i++ ) varOptionins += "<OPTION " + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
}		
	
	function addRowDOM1x (tableID, jml) {
		flag_add1_x=1
   		var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
		var tmp = parseInt(document.getElementById("tableProd").rows.length);
		var oRow = oTable.insertRow(oTable.rows.length);
        var oCells = oRow.cells;
		var i=tmp-1;
		var idx = pesertax + 1;
	
// 		if (document.all) {
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td ><input type=text name=ptx.no_urut"+idx+" size=2 maxlength=3 value="+idx+" readOnly></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td ><input type=text name='ptx.nama"+idx+"' value='' size=30 maxlength=200></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td ><input type=radio class=noBorder name='kelamin"+idx+"' value=1 checked id=cowok onClick=kelamin("+idx+",'1');>Pria <input type=radio class=noBorder name='kelamin"+idx+"' value=0 id=cewek onClick=kelamin("+idx+",'0');>Wanita <input type=hidden name='ptx.kelamin"+idx+"' value ='1' size=3 maxlength=3></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td><input type=text name='tgllhr"+idx+"' value='' size=2 maxlength=2>/<input type=text name='blnhr"+idx+"' value='' size=2 maxlength=2>/<input type=text name='thnhr"+idx+"' value='' size=4 maxlength=4><input type=hidden name='ptx.tgl_lahir"+idx+"' value='' size=5></td>";         
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML ="<td><input type=text name='ptx.umur"+idx+"' value='0' size=3 maxlength=3 readOnly></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML =  "<td ><select name='ptx.lsre_id"+idx+"'>"+varOptionhub+"</select></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML =  "<td ><select name='ptx.lsbs_id_plus"+idx+"'>"+OptionProdx+"</select></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML =  "<td ><select name='ptx.flag_jenis_peserta"+idx+"'>"+OptionJP+"</select></td>";
			
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML ="<td ><input type=checkbox name=cek2"+idx+" id=ck2"+idx+"' class=\"noBorder\" ></td></tr>";
			var cell = oRow.insertCell(oCells.length);			
			cell.innerHTML = '';
			pesertax = idx;

// 		}
 	}
 	
	function cancel1x()
	{		
		var idx = pesertax;
	
		if  ((idx)!=null && (idx)!="")
		{
			if (idx >0)
				flag_add1_x=1;

		}
		if(flag_add1_x==1)
		{
			deleteRowDOM1x('tableProd', '1');

		}
	}	

	function deleteallplus (tableID,rowCount)
 	{ 
   		var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
   	 	var row=parseInt(document.getElementById("tableprod").rows.length);
		var flag_row_x=0;
		var jumlah_cek_x=0;
		var idx = pesertax;

		if(row>1)
		{
			for (var i=1;i<((parseInt(row)));i++)
			{
				oTable.deleteRow(parseInt(document.getElementById("tableprod").rows.length)-1);
			}
		}
		row=0;
		pesertax=0;
		document.frmParam.pesertax.value=0;
	}

function deleteRowDOM1x (tableID,rowCount)
 { 
    var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
    var row=parseInt(document.getElementById("tableProd").rows.length);
	var flag_row_x=0;
	var jumlah_cek_x=0;
	var idx = pesertax;
	if(row>0)
	{
		flag_row=0;
		for (var i=1;i<((parseInt(row)));i++)
		{
			if (eval("document.frmParam.elements['cek2"+i+"'].checked"))
			{
				idx=idx-1;
				for (var k =i ; k<((parseInt(row))-1);k++)
				{
					eval(" document.frmParam.elements['ptx.no_urut"+k+"'].value =k;");   
					eval(" document.frmParam.elements['ptx.nama"+k+"'].value =document.frmParam.elements['ptx.nama"+(k+1)+"'].value;"); 
					eval(" document.frmParam.elements['ptx.kelamin"+k+"'].value =document.frmParam.elements['ptx.kelamin"+(k+1)+"'].value;"); 
					eval(" document.frmParam.elements['kelamin"+k+"'].value =document.frmParam.elements['kelamin"+(k+1)+"'].value;"); 
					eval(" document.frmParam.elements['tgllhr"+k+"'].value = document.frmParam.elements['tgllhr"+(k+1)+"'].value;");	
					eval(" document.frmParam.elements['blnhr"+k+"'].value = document.frmParam.elements['blnhr"+(k+1)+"'].value;");			      
					eval(" document.frmParam.elements['thnhr"+k+"'].value = document.frmParam.elements['thnhr"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['ptx.tgl_lahir"+k+"'].value = document.frmParam.elements['ptx.tgl_lahir"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['ptx.umur"+k+"'].value = document.frmParam.elements['ptx.umur"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['ptx.lsre_id"+k+"'].value = document.frmParam.elements['ptx.lsre_id"+(k+1)+"'].value;");
					eval(" document.frmParam.elements['ptx.lsbs_id_plus"+k+"'].value = document.frmParam.elements['ptx.lsbs_id_plus"+(k+1)+"'].value;");
					eval(" document.frmParam.elements['ptx.flag_jenis_peserta"+k+"'].value = document.frmParam.elements['ptx.flag_jenis_peserta"+(k+1)+"'].value;");
					eval(" document.frmParam.elements['cek2"+k+"'].checked = document.frmParam.elements['cek2"+(k+1)+"'].checked;");
				}
				oTable.deleteRow(parseInt(document.getElementById("tableProd").rows.length)-1);
				row=row-1;
				pesertax = idx;
				i=i-1;
			}
		}
		row=parseInt(document.getElementById("tableProd").rows.length);
			if(row==1)	
				flag_add_x=0;
	}
}	
	
	function Body_onload() {
		var nameSelection
		xmlData.async = false;
		document.frmParam.elements['datausulan.mspr_class'].readOnly=true;
		document.frmParam.elements['datausulan.mspr_class'].style.backgroundColor ='#D4D4D4';
		
		generateXML_rider(loadXMLDoc("${path}/xml/PLANRIDER.xml"), 'BISNIS_ID','NAME');
		
// 		xmlData.src = "${path}/xml/FLAG_JENIS_PESERTA.xml";
// 		generateXML_flagJenisPeserta( xmlData, 'ID', 'JENIS');
// 		xmlData.src = "${path}/xml/PERSENUP.xml";
// 		generateXML_persentaseUp( xmlData, 'ID','PERSEN');
		//xmlData.src = "${path}/xml/INSRIDER.xml";
		//generateXML_insrider( xmlData, 'ID','INSRIDER');
// 		generateXML_insrider(loadXMLDoc("${path}/xml/FLAG_JENIS_PESERTA.xml"), 'ID', 'JENIS');
		generateXML_persenUP(loadXMLDoc("${path}/xml/PERSENUP.xml"), 'ID','PERSEN');
		generateXML_penerima(loadXMLDoc("${path}/xml/RELATION.xml"), 'ID','RELATION');
		generateXML_jenisPeserta(loadXMLDoc("${path}/xml/FLAG_JENIS_PESERTA.xml"), 'ID','JENIS');
		generateXML_flagJenisProd(loadXMLDoc("${path}/xml/PROD_KESEHATAN.xml"), 'ID','PRODUK');
		generateXML_insrider(loadXMLDoc("${path}/xml/INSRIDER.xml"), 'ID','INSRIDER');
		if(lca_id='58'){
			Prod(document.frmParam.elements['datausulan.lsbs_id'].value);
// 			alert(document.frmParam.elements['datausulan.lsbs_id'].value);
			if(${cmd.datausulan.jmlrider}>0){
				for (var k =1;k<=${cmd.datausulan.jmlrider};k++)
				{
					eval(" document.frmParam.elements['ride.plan_rider1"+k+"'].value = document.frmParam.elements['ride.plan_rider"+k+"'].value;");
					//eval(" document.frmParam.elements['ride.plan_rider1"+k+"'].value =document.frmParam.elements['ride.plan_rider"+k+"'].value;");  
				}
			}
		}

		document.frmParam.elements['datausulan.mspo_pay_period'].style.backgroundColor ='#D4D4D4';
		document.frmParam.elements['datausulan.mspr_ins_period'].style.backgroundColor ='#D4D4D4';
		document.frmParam.elements['datausulan.jmlrider'].value=${cmd.datausulan.jmlrider};
		document.frmParam.elements['datausulan.jml_peserta'].value=${cmd.datausulan.jml_peserta};
		document.frmParam.kode_sementara.value='${cmd.datausulan.plan}';
		//document.frmParam.elements['datausulan.mste_beg_date'].style.backgroundColor ='#D4D4D4';
		
		if (document.frmParam.kode_sementara.value!="")
		{
			ProductConfig1(document.frmParam.elements['datausulan.lsbs_id'].value,document.frmParam.kode_sementara.value);
// 			<c:if test="${sessionScope.currentUser.jn_bank eq 0 or sessionScope.currentUser.jn_bank eq 1}">
// 				ProductConfigPlatinumBII(155);
// 			</c:if>
// 			<c:if test="${sessionScope.currentUser.jn_bank eq 2}">
// 				ProductConfigBankSinarmas(120);
// 			</c:if>
// 			<c:if test="${sessionScope.currentUser.jn_bank eq 3}">
// 				ProductConfigSekuritas(document.frmParam.elements['datausulan.lsbs_id'].value,document.frmParam.kode_sementara.value);
// 			</c:if>
			document.frmParam.elements['datausulan.mspo_installment'].value=cuti;
			perproduk1(document.frmParam.kode_sementara.value);
			if (document.frmParam.status.value.toUpperCase()==("edit").toUpperCase())
			{
				cek_rider_include(document.frmParam.kode_sementara.value);
			}
		}else{

			<c:if test="${sessionScope.currentUser.jn_bank eq 0 or sessionScope.currentUser.jn_bank eq 1}">
				ProductConfigPlatinumBII(155);
			</c:if>
			<c:if test="${sessionScope.currentUser.jn_bank eq 2}">
				ProductConfigBankSinarmas(120);
			</c:if>
			<c:if test="${sessionScope.currentUser.jn_bank eq 3}">
				ProductConfigSekuritas(142);
			</c:if>
		
		}
		if (document.frmParam.elements['datausulan.cara_premi'].value==null || document.frmParam.elements['datausulan.cara_premi'].value=='')
		{
			document.frmParam.elements['datausulan.cara_premi'].value = '0';
			document.frmParam.tanda_premi.value = '0';
		}
		cpy(document.frmParam.tanda_premi.value);
	}

function addRowDOM1 (tableID, jml) {
		flag_add1=1
   		var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
		var tmp = parseInt(document.getElementById("tablerider").rows.length);
		var oRow = oTable.insertRow(oTable.rows.length);
        var oCells = oRow.cells;
		var i=tmp-1;
		var idx = mn + 1;
		ajaxSelectplan(document.frmParam.elements['datausulan.lsbs_id'].value, document.frmParam.elements['datausulan.plan'].value,'list_rider', 'ridermenu'+idx, 'ride.plan_rider'+idx,'ride.plan_rider'+idx , 'plan', 'lsdbs_name', 'cekUpRiderDisabled('+idx+')');
		

		//if (document.all) {
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td >"+idx+"</td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td ><div id=ridermenu"+idx+"> </div><input type=hidden name='ride.plan_rider1"+idx+"'></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td><input type=text name='ride.mspr_unit"+idx+"' value='0' size=3></td>";         
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML =  "<td ><input type=text name='ride.mspr_class"+idx+"' value='0' size=3></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML ="<td><select name='ride.persenUp"+idx+"'>"+varOptionUp+"</select> <input type=hidden name='ride.persenUpx"+idx+"'></td>";																											
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML ="<td><input type=text name='ride.mspr_tsi"+idx+"' style='background-color :#D4D4D4' readOnly> </td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML ="<td><input type=text name='ride.mspr_premium"+idx+"' style='background-color :#D4D4D4' readOnly> </td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML ="<td><input type=text name='ride.mspr_ins_period"+idx+"'   size=5 style='background-color :#D4D4D4' readOnly></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML ="<td><input type=text name='mspr_beg_date"+idx+"' value=''  size=15 style='background-color :#D4D4D4' readOnly></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML ="<td ><input type=text name='mspr_end_date"+idx+"' value=''  size=15 style='background-color :#D4D4D4' readOnly></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML ="<td ><input type=text name='mspr_end_pay"+idx+"' value=''  size=15 style='background-color :#D4D4D4' readOnly></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML ="<td><select name='ride.mspr_tt"+idx+"'>"+varOptionins+"</select> <input type=hidden name='ride.mspr_tt1"+idx+"'></td>";																											
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML ="<td><input type=text name='ride.mspr_rate"+idx+"' value='' size=5 style=background-color :#D4D4D4 readOnly> </td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML ="<td ><input type=checkbox class=\"noBorder\" name=cek"+idx+" id=ck"+idx+"' ><input type=hidden name'ride.flag_include"+idx+"' value=0 size=3></td></tr>";
			
			var cell = oRow.insertCell(oCells.length);			
			cell.innerHTML = '';
			mn = idx;
		
		//}


 }


					

function cekecek(){
	if(document.frmParam.elements['flag_warning_autodebet'].value == 1){
		alert('PERHATIAN!!! MOHON DIPERHATIKAN APAKAH BENAR CARA BAYAR TUNAI, KARENA SEHARUSNYA AUTODEBET!');
	}
}

function prev()
{
	document.frmParam.mn.value=mn;
	document.frmParam.pesertax.value=pesertax;
	//&& document.frmParam.status.value.toUpperCase()==("input").toUpperCase()
	if (eval(mn) >0 )
	{
		for (var k =1;k<=mn;k++)
		{
				eval(" document.frmParam.elements['ride.mspr_tt1"+k+"'].value = document.frmParam.elements['ride.mspr_tt"+k+"'].value;");
				eval(" document.frmParam.elements['ride.plan_rider1"+k+"'].value =document.frmParam.elements['ride.plan_rider"+k+"'].value;");
				eval(" document.frmParam.elements['ride.persenUpx"+k+"'].value =document.frmParam.elements['ride.persenUp"+k+"'].value;"); 
		}
	}
	if(eval(pesertax) > 0){
		for (var z=1; z<=pesertax; z++){
				eval(" document.frmParam.elements['ptx.flag_jenis_peserta"+z+"'].value = document.frmParam.elements['ptx.flag_jenis_peserta"+z+"'].value;");
				eval(" document.frmParam.elements['ptx.lsre_id"+z+"'].value = document.frmParam.elements['ptx.lsre_id"+z+"'].value;");
				eval(" document.frmParam.elements['ptx.lsbs_id_plus"+z+"'].value = document.frmParam.elements['ptx.lsbs_id_plus"+z+"'].value;");
		}
	}
	
	cekecek();
	
//	if (eval(jml_peserta) >0 )
//	{
//		for (var k =1;k<=mn;k++)
//		{
//			eval(" document.frmParam.elements['ttg.plan_rider1"+k+"'].value =document.frmParam.elements['ttg.plan_rider"+k+"'].value;");  
//			eval(" document.frmParam.elements['ttg.lspc_no1"+k+"'].value =document.frmParam.elements['ttg.lspc_no"+k+"'].value;");
//		}
//	}

} 
 
 
function next()
{
	eval( " document.frmParam.elements['pemegang.indeks_halaman'].value = ((document.frmParam.hal.value)+1);");
	document.frmParam.mn.value=mn;
	document.frmParam.pesertax.value = pesertax;
	
	if (eval(mn) >0 )
	{
	//alert( document.frmParam.elements['ride.mspr_tt1'].value);
		for (var k =1;k<=mn;k++)
		{
				eval(" document.frmParam.elements['ride.mspr_tt1"+k+"'].value = document.frmParam.elements['ride.mspr_tt"+k+"'].value;");
				eval(" document.frmParam.elements['ride.plan_rider1"+k+"'].value =document.frmParam.elements['ride.plan_rider"+k+"'].value;");
				eval(" document.frmParam.elements['ride.persenUpx"+k+"'].value =document.frmParam.elements['ride.persenUp"+k+"'].value;");     
			if(document.frmParam.elements['ride.plan_rider'+k].value.substring(0,6) == '813~X4' || document.frmParam.elements['ride.plan_rider'+k].value.substring(0,6) == '813~X5' || document.frmParam.elements['ride.plan_rider'+k].value.substring(0,6) == '818~X3' || document.frmParam.elements['ride.plan_rider'+k].value.substring(0,6) == '818~X4'){
				if(document.frmParam.elements['ride.plan_rider'+k].value.substring(0,6) == '818~X4'){
					alert('Untuk rider ke '+k+' : Rider yang anda pilih adalah TERM RIDER 4 SINGLE (pembayaran sekaligus).');
				}else if(document.frmParam.elements['ride.plan_rider'+k].value.substring(0,6) == '818~X3'){
					alert('Untuk rider ke '+k+' : Rider yang anda pilih adalah TERM RIDER 4 (pembayaran setiap periode rollover).');
				}else if(document.frmParam.elements['ride.plan_rider'+k].value.substring(0,6) == '813~X5'){
					alert('Untuk rider ke '+k+' : Rider yang anda pilih adalah CI RIDER 4 SINGLE (pembayaran sekaligus).');
				}else {
					alert('Untuk rider ke '+k+' : Rider yang anda pilih adalah CI RIDER 4 (pembayaran setiap periode rollover).');
				}
			}
		}
	
	}

	if(eval(pesertax) > 0){
	alert( document.frmParam.elements['ptx.flag_jenis_peserta_x'].value);
		for(var z=1; z<=pesertax; z++){
			eval(" document.frmParam.elements['ptx.flag_jenis_peserta"+z+"'].value = document.frmParam.elements['ptx.flag_jenis_peserta"+z+"'].value;");
			eval(" document.frmParam.elements['ptx.lsre_id"+z+"'].value = document.frmParam.elements['ptx.lsre_id"+z+"'].value;");
		}
	}
	
	var lsbs;
	if(document.frmParam.elements['datausulan.lsbs_id']){
		lsbs = document.frmParam.elements['datausulan.lsbs_id'].value;
	}
	var lsdbs;
	if(document.frmParam.elements['datausulan.plan']){
		lsdbs = document.frmParam.elements['datausulan.plan'].value.substring(5,6);
	}
	var convert = document.frmParam.elements['datausulan.convert'].value;
	if(convert == 1){
		alert('Polis ini adalah SURRENDER ENDORSEMENT. Anda tidak bisa merubah Tanggal Mulai Pertanggungan dan Tanggal Rollover MGI');
	}
	
	var jn_bank = '${sessionScope.currentUser.jn_bank}';
	if((lsbs == '164' || lsbs == '174') && (jn_bank != '2' && jn_bank !='3')){
		alert('Polis Stable Link. Silahkan cek dahulu apakah orang ini sudah mempunyai Polis Stable Link sebelumnya.');
		popWin(
			'${path}/bac/multi.htm?window=cek_sama&lku_id='+document.frmParam.elements['datausulan.lku_id'].value+
			'&pp_dob=<fmt:formatDate value="${cmd.pemegang.mspe_date_birth}" pattern="yyyyMMdd"/>'+
			'&pp_name=${cmd.pemegang.mcl_first}' +
			'&tt_dob=<fmt:formatDate value="${cmd.tertanggung.mspe_date_birth}" pattern="yyyyMMdd"/>'+
			'&tt_name=${cmd.tertanggung.mcl_first}', 600, 800);
	}
	
	if(lsbs == '186' && lsdbs == '3'){
		alert('Untuk produk SSP PRO, rekening Sumber Dana dan rekening SSP wajib diisi!')
	}
	
	if(lsbs == '183' || lsbs == '189' ||lsbs == '193' ){
		alert('Untuk produk EKA SEHAT, ahli waris tidak perlu diisi!')
	}
	
	cekecek();
	
} 	

function next1()
{
	document.frmParam.mn.value=mn;
	document.frmParam.pesertax.value=pesertax;

	if (eval(mn) >0 )
	{
	//alert( document.frmParam.elements['ride.mspr_tt1'].value);
		for (var k =1;k<=mn;k++)
		{
				eval(" document.frmParam.elements['ride.persenUpx"+k+"'].value =document.frmParam.elements['ride.persenUp"+k+"'].value;");

				eval(" document.frmParam.elements['ride.mspr_tt1"+k+"'].value = document.frmParam.elements['ride.mspr_tt"+k+"'].value;");
				eval(" document.frmParam.elements['ride.plan_rider1"+k+"'].value =document.frmParam.elements['ride.plan_rider"+k+"'].value;");
		}
	}
	
	if(eval(pesertax) > 0){
		for (var z=1; z<=pesertax; z++)
		{
				eval(" document.frmParam.elements['ptx.flag_jenis_peserta"+z+"'].value =document.frmParam.elements['ptx.flag_jenis_peserta"+z+"'].value;");
				eval(" document.frmParam.elements['ptx.lsre_id"+z+"'].value = document.frmParam.elements['ptx.lsre_id"+z+"'].value;");
				eval(" document.frmParam.elements['ptx.lsbs_id_plus"+z+"'].value = document.frmParam.elements['ptx.lsbs_id_plus"+z+"'].value;");
		}
	}

} 

function cancel1()
{		
	var idx = mn;

	if  ((idx)!=null && (idx)!="")
	{
		if (idx >0)
			flag_add1=1;
	}
	if(flag_add1==1)
	{
		deleteRowDOM1('tablerider', '1');
	}
}

function deleteallrider (tableID,rowCount)
 { 
   var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
    var row=parseInt(document.getElementById("tablerider").rows.length);
	var flag_row=0;
	var jumlah_cek=0;
	var idx = mn;

	if(row>1)
	{
		for (var i=1;i<((parseInt(row)));i++)
		{

				oTable.deleteRow(parseInt(document.getElementById("tablerider").rows.length)-1);
		}
	}
	row=0;
	mn=0;
	document.frmParam.mn.value=0;
}

function deleteRowDOM1 (tableID,rowCount)
 { 
    var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
    var row=parseInt(document.getElementById("tablerider").rows.length);
	var flag_row=0;
	var jumlah_cek=0;
	var idx = mn;

	if(row>0)
	{
		flag_row=0;
		//alert(row);
		for (var i=1;i<((parseInt(row)));i++)
		{

			if (eval("document.frmParam.elements['cek"+i+"'].checked"))
			{
				idx=idx-1;

				for (var k =i ; k<((parseInt(row))-1);k++)
				{

					eval(" document.frmParam.elements['ride.plan_rider"+k+"'].value =document.frmParam.elements['ride.plan_rider"+(k+1)+"'].value;");   
					eval(" document.frmParam.elements['ride.plan_rider1"+k+"'].value =document.frmParam.elements['ride.plan_rider"+(k+1)+"'].value;");  
					eval(" document.frmParam.elements['ride.mspr_unit"+k+"'].value =document.frmParam.elements['ride.mspr_unit"+(k+1)+"'].value;"); 
					eval(" document.frmParam.elements['ride.mspr_class"+k+"'].value = document.frmParam.elements['ride.mspr_class"+(k+1)+"'].value;");
					eval(" document.frmParam.elements['ride.persenUp"+k+"'].value = document.frmParam.elements['ride.persenUp"+(k+1)+"'].value;");
					eval(" document.frmParam.elements['ride.persenUpx"+k+"'].value = document.frmParam.elements['ride.persenUp"+(k+1)+"'].value;");
					eval(" document.frmParam.elements['ride.mspr_tsi"+k+"'].value = document.frmParam.elements['ride.mspr_tsi"+(k+1)+"'].value;");
					eval(" document.frmParam.elements['ride.mspr_premium"+k+"'].value = document.frmParam.elements['ride.mspr_premium"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['ride.mspr_ins_period"+k+"'].value = document.frmParam.elements['ride.mspr_ins_period"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['mspr_beg_date"+k+"'].value = document.frmParam.elements['mspr_beg_date"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['mspr_end_date"+k+"'].value = document.frmParam.elements['mspr_end_date"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['mspr_end_pay"+k+"'].value = document.frmParam.elements['mspr_end_pay"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['mspr_end_pay"+k+"'].value = document.frmParam.elements['mspr_end_pay"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['ride.mspr_tt"+k+"'].value = document.frmParam.elements['ride.mspr_tt"+(k+1)+"'].value;");
					eval(" document.frmParam.elements['ride.mspr_tt1"+k+"'].value = document.frmParam.elements['ride.mspr_tt"+(k+1)+"'].value;");
					eval(" document.frmParam.elements['ride.mspr_rate"+k+"'].value = document.frmParam.elements['ride.mspr_rate"+(k+1)+"'].value;");
					eval(" document.frmParam.elements['cek"+k+"'].checked = document.frmParam.elements['cek"+(k+1)+"'].checked;");
				}
				oTable.deleteRow(parseInt(document.getElementById("tablerider").rows.length)-1);
				row=row-1;
				//alert(mn);
				mn = idx;
				//alert(mn);
				i=i-1;
			}
		}


		row=parseInt(document.getElementById("tablerider").rows.length);
			if(row==1)	
				flag_add=0;
		
	}
}	

function cekUpRiderDisabled(nilai){
	if(document.frmParam.elements['ride.plan_rider'+nilai].value.substring(0,6) == '818~X2' || document.frmParam.elements['ride.plan_rider'+nilai].value.substring(0,6) == '813~X4' || document.frmParam.elements['ride.plan_rider'+nilai].value.substring(0,6) == '813~X5' || document.frmParam.elements['ride.plan_rider'+nilai].value.substring(0,6) == '818~X3' || document.frmParam.elements['ride.plan_rider'+nilai].value.substring(0,6) == '818~X4'){
		document.frmParam.elements['ride.mspr_tsi'+nilai].readOnly = false;
		document.frmParam.elements['ride.mspr_tsi'+nilai].style.backgroundColor ='#FFFFFF';
		//document.frmParam.elements['ride.mspr_tsi'+nilai].value = 
		//	document.frmParam.elements['datausulan.mspr_tsi'].value
	}else{
		document.frmParam.elements['ride.mspr_tsi'+nilai].readOnly = true;
		document.frmParam.elements['ride.mspr_tsi'+nilai].style.backgroundColor ='#D4D4D4';
	}
	
	if(document.frmParam.elements['ride.plan_rider'+nilai].value.substring(0,3) == '813'|| document.frmParam.elements['ride.plan_rider'+nilai].value.substring(0,3) == '830')
	{
		document.frmParam.elements['ride.persenUp'+nilai].disabled = false;
		document.frmParam.elements['ride.persenUp'+nilai].style.backgroundColor ='#FFFFFF';
	}else{
		document.frmParam.elements['ride.persenUp'+nilai].disabled = true;
		document.frmParam.elements['ride.persenUp'+nilai].style.backgroundColor ='#D4D4D4';
	}
}

function cek_tgl()
{
	if ((document.frmParam.kode_sementara.value=="")||(document.frmParam.kode_sementara.value=="0~X0") )
	{
		alert("Silahkan pilih produk dahulu baru menentukan tanggal mulai berlaku pertanggungan");
	}else{
		listtglpolis(document.frmParam.elements['datausulan.mste_beg_date'].value, document.frmParam.elements['datausulan.mspr_ins_period'].value,document.frmParam.elements['datausulan.plan'].value);
	}
}

 function cpy(hasil)
 {
	document.frmParam.elements['datausulan.cara_premi'].value = hasil;
	document.frmParam.tanda_premi.value=hasil;
	if (document.frmParam.tanda_premi.value == '0')
	{
		document.frmParam.elements['datausulan.kombinasi'].disabled = true;
		document.frmParam.elements['datausulan.kombinasi'].value ='';
		document.frmParam.elements['datausulan.total_premi_kombinasi'].readOnly= true;		
		document.frmParam.elements['datausulan.total_premi_kombinasi'].value = '';
		document.frmParam.elements['datausulan.total_premi_kombinasi'].style.backgroundColor ='#D4D4D4';
		document.frmParam.elements['datausulan.mspr_premium'].readOnly = false;		
		document.frmParam.elements['datausulan.mspr_premium'].style.backgroundColor ='#FFFFFF';
	}else{
		document.frmParam.elements['datausulan.kombinasi'].disabled = false;
		document.frmParam.elements['datausulan.total_premi_kombinasi'].readOnly = false;		
		document.frmParam.elements['datausulan.total_premi_kombinasi'].style.backgroundColor ='#FFFFFF';
		document.frmParam.elements['datausulan.mspr_premium'].readOnly = true;		
		document.frmParam.elements['datausulan.mspr_premium'].value = '';
		document.frmParam.elements['datausulan.mspr_premium'].style.backgroundColor ='#D4D4D4';
		
	}
 }	
	
 function ganti(hsl)
 {
 	if (document.frmParam.elements['datausulan.total_premi_kombinasi'].value=='' || document.frmParam.elements['datausulan.total_premi_kombinasi'].value == 0)
 	{
 		alert('Masukkan Total Premi dulu baru pilih Kombinasi.');
 		document.frmParam.elements['datausulan.kombinasi'].value='';
 	}else{
 		switch (document.frmParam.elements['datausulan.kombinasi'].value) {
 		case "A" : 
			document.frmParam.elements['datausulan.mspr_premium'].value = eval(document.frmParam.elements['datausulan.total_premi_kombinasi'].value);
			break;
		case "B" : 
			document.frmParam.elements['datausulan.mspr_premium'].value = 90/100*eval(document.frmParam.elements['datausulan.total_premi_kombinasi'].value);
			break;
 		case "C" : 
			document.frmParam.elements['datausulan.mspr_premium'].value = 80/100*eval(document.frmParam.elements['datausulan.total_premi_kombinasi'].value);
			break;
 		case "D" : 
			document.frmParam.elements['datausulan.mspr_premium'].value = 70/100*eval(document.frmParam.elements['datausulan.total_premi_kombinasi'].value);
			break;
		 case "E" : 
			document.frmParam.elements['datausulan.mspr_premium'].value = 60/100*eval(document.frmParam.elements['datausulan.total_premi_kombinasi'].value);
			break;
		 case "F" : 
			document.frmParam.elements['datausulan.mspr_premium'].value = 50/100*eval(document.frmParam.elements['datausulan.total_premi_kombinasi'].value);
			break;
		 case "G" : 
			document.frmParam.elements['datausulan.mspr_premium'].value = 40/100*eval(document.frmParam.elements['datausulan.total_premi_kombinasi'].value);
			break;
		 case "H" : 
			document.frmParam.elements['datausulan.mspr_premium'].value = 30/100*eval(document.frmParam.elements['datausulan.total_premi_kombinasi'].value);
			break;
		 case "I" : 
			document.frmParam.elements['datausulan.mspr_premium'].value = 20/100*eval(document.frmParam.elements['datausulan.total_premi_kombinasi'].value);
			break;
		 case "J" : 
			document.frmParam.elements['datausulan.mspr_premium'].value = 10/100*eval(document.frmParam.elements['datausulan.total_premi_kombinasi'].value);
			break;
		case "K" : 
			document.frmParam.elements['datausulan.mspr_premium'].value = 95/100*eval(document.frmParam.elements['datausulan.total_premi_kombinasi'].value);
			break;	
		case "L" : 
			document.frmParam.elements['datausulan.mspr_premium'].value = 85/100*eval(document.frmParam.elements['datausulan.total_premi_kombinasi'].value);
			break;		
		case "M" : 
			document.frmParam.elements['datausulan.mspr_premium'].value = 75/100*eval(document.frmParam.elements['datausulan.total_premi_kombinasi'].value);
			break;	
		case "N" : 
			document.frmParam.elements['datausulan.mspr_premium'].value = 65/100*eval(document.frmParam.elements['datausulan.total_premi_kombinasi'].value);
			break;	
		case "O" : 
			document.frmParam.elements['datausulan.mspr_premium'].value = 55/100*eval(document.frmParam.elements['datausulan.total_premi_kombinasi'].value);
			break;
		case "P" : 
			document.frmParam.elements['datausulan.mspr_premium'].value = 45/100*eval(document.frmParam.elements['datausulan.total_premi_kombinasi'].value);
			break;
		case "Q" : 
			document.frmParam.elements['datausulan.mspr_premium'].value = 35/100*eval(document.frmParam.elements['datausulan.total_premi_kombinasi'].value);
			break;
		case "R" : 
			document.frmParam.elements['datausulan.mspr_premium'].value = 25/100*eval(document.frmParam.elements['datausulan.total_premi_kombinasi'].value);
			break;								
		case "S" : 
			document.frmParam.elements['datausulan.mspr_premium'].value = 15/100*eval(document.frmParam.elements['datausulan.total_premi_kombinasi'].value);
			break;		
		case "T" : 
			document.frmParam.elements['datausulan.mspr_premium'].value = 5/100*eval(document.frmParam.elements['datausulan.total_premi_kombinasi'].value);
			break;			
 		}
 	}
 }
	
 function Prod(hasil)
{

	//deleteallrider('tablerider', '1');
	document.frmParam.elements['datausulan.lsbs_id'].value = hasil;
	ajaxSelectWithParam1(hasil,'select_detilprodukutamamall', 'sideMenu', 'datausulan.plan', 'cmd.datausulan.plan', 'plan', 'lsdbs_name', 'perproduk(this.options[this.selectedIndex].value);');
	eval(" document.frmParam.elements['datausulan.mspr_ins_period'].value ='';");			
	eval(" document.frmParam.elements['datausulan.mspo_pay_period'].value ='';");
	eval(" document.frmParam.elements['datausulan.mspo_installment'].value ='';");
}
 	
	
// -->
</script>
<script language="JavaScript">
<!--
	if (!document.layers&&!document.all&&!document.getElementById)
		event="test"
		
	function showtip(current,e,text){
	
		if (document.all||document.getElementById){
			thetitle=text.split('<br>')
			if (thetitle.length>1){
				thetitles=''
				
			for (i=0;i<thetitle.length;i++)
				thetitles+=thetitle[i]
				
			current.title=thetitles
		}
		else
			current.title=text
	}
	
	else if (document.layers){
				document.tooltip.document.write('<layer bgColor="white" style="border:1px solid black;font-size:13px;">'+text+'</layer>')
				document.tooltip.document.close()
				document.tooltip.left=e.pageX+5
				document.tooltip.top=e.pageY+5
				document.tooltip.visibility="show"
			}
	}
	
	function hidetip(){
		if (document.layers)
			document.tooltip.visibility="hidden"
	}
	
	function kelamin(k,hasil)
 	{
		eval(" document.frmParam.elements['ptx.kelamin"+k+"'].value = hasil;");
 	}
 
	 function hubungan(k,hasil)
	 {
	 	eval(" document.frmParam.elements['ptx.lsre_id"+k+"'].value = hasil;");
	 }
	 
	 function flagJPeserta(k,hasil)
	 {
	 	eval("document.frmParam.elements['ptx.flag_jenis_peserta"+k+"'].value = hasil;")
	 }
	 
	  function plan(k,hasil)
	 {
	 	eval(" document.frmParam.elements['ptx.plan_rider1"+k+"'].value = hasil;");
	 }
	 
	 function kecuali(k,hasil)
	 {
	 	eval(" document.frmParam.elements['ptx.lspc_no1"+k+"'].value = hasil;");
	 }
			
// -->
</script>

<body bgcolor="ffffff" onLoad="Body_onload();">

<XML ID=xmlData></XML> 
<XML ID=xmlData1></XML> 
<XML ID=xmlData2></XML> 

<form name="frmParam" method="post" >
<table class="form_input" style="font-size: 11px;" width="100%">
	<tr> 
		<td> 
 			<input type="submit" name="_target0" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/pp1.jpg);"
				accesskey="1" onmouseover="return overlib('Alt-1', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target1" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ttg1.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target2" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ddu2.jpg);"
				accesskey="3" onmouseover="return overlib('Alt-3', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target3" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/inv1.jpg);"
				accesskey="4" onmouseover="return overlib('Alt-4', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target4" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ag1.jpg);"
				accesskey="5" onmouseover="return overlib('Alt-5', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target5" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/kf1.jpg);"
				accesskey="6" onmouseover="return overlib('Alt-6', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" name="dummy2" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/sb1.jpg);">		
      </td>
  </tr>
   <tr>
    <td> 
 									<spring:bind path="cmd.*">
										<c:if test="${not empty status.errorMessages}">
											<div id="error">
											ERROR:<br>
											<c:forEach var="error" items="${status.errorMessages}">
														- <c:out value="${error}" escapeXml="false" />
												<br />
											</c:forEach></div>
										</c:if>									
									</spring:bind>     
        <table class="form_input" style="font-size: 11px;">
          <tr> 
            <td colspan="8" style="text-align: center;"> <input type="submit" name="_target${halaman-1}" value="Prev &laquo;" onClick="prev()" 
				onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();"> 
              <input type="submit" name="_target${halaman+1}" value="Next &raquo;" onClick="next()"
				onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();"> 
              <input type="hidden" name="_page" value="${halaman}"> </td>
          </tr>
          <tr> 
            <th class="subtitle" colspan="8">DATA PERTANGGUNGAN DAN PREMI ASURANSI 
              <input type="hidden" name="tanggal_lahir_pp" value ="<fmt:formatDate value='${cmd.pemegang.mspe_date_birth}' pattern="yyyyMMdd"/> 
              "> <input type="hidden" name="tanggal_lahir_ttg" value ="<fmt:formatDate value='${cmd.tertanggung.mspe_date_birth}' pattern="yyyyMMdd"/> 
              "> </th>
          </tr>
          <tr  > 
            <th class="sub1">&nbsp;</th>
            <th class="sub1" colspan="7">JENIS ASURANSI POKOK 
              <input type="hidden" name="status" value="${cmd.pemegang.status}"></th>
          </tr>
          <tr  > 
            <td  width="13">1.</td>
            <td  width="256"> <p align="left"> Produk </td>
            <td colspan="6"><spring:bind path="cmd.datausulan.lsbs_id"> 
            	<select name="${status.expression }"  onChange="ProductConfigMall(this.options[this.selectedIndex].value)";
            	<c:if test="${ not empty status.errorMessage}">
						class="read_only" 
				</c:if>>
                <option value="0">NONE</option>
                <c:forEach var="utama" items="${listprodukutama}"> <option value="${utama.lsbs_id}" 
						 <c:if test="${utama.lsbs_id eq cmd.datausulan.lsbs_id}">selected</c:if> >${utama.lsbs_name}</option> 
                </c:forEach> 
              </select>
			  <font color="#CC3300">*</font>
              </spring:bind> <input type="hidden" name="kode_sementara"> <spring:bind path="cmd.datausulan.tipeproduk"> 
              Jenis Produk : 
              <select name="${status.expression }" 
			  <c:if test="${ not empty status.errorMessage}">
						class="read_only" 
					</c:if>>
                <option value="" /> <c:forEach var="tipe" items="${listtipeproduk}"> <option value="${tipe.lstp_id}"
						   
                <c:if test="${cmd.datausulan.tipeproduk eq tipe.lstp_id}">selected</c:if> >${tipe.lstp_produk}</option> 
                </c:forEach> 
              </select>
              <font color="#CC3300">*</font>  </spring:bind> Kode Produk : <spring:bind path="cmd.datausulan.kodeproduk"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="5"  onChange="Prod(this.value)";
			<c:if test="${ not empty status.errorMessage}">
						class="read_only" 
					</c:if>>
               </spring:bind> 
            </td>
          </tr>
          <tr> 
            <td> </td>
            <td>Nama Produk </td>
            <td colspan="2"><div id="sideMenu"> </div><font color="#CC3300">*</font></td>
            <td>Klas</td>
            <td colspan="3" > <spring:bind path="cmd.datausulan.mspr_class"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="5"   maxlength="2"
				<c:if test="${ not empty status.errorMessage}">
						readOnly
					</c:if>>
              <font color="#CC3300">*</font> </spring:bind>
			</td>			
          </tr>
          <tr>
          	 <td> </td>
          	 <td>Paket</td>
             <td width="29"> <div id="paketan"> </div>
             </td>
<!--              <td  width="400"> -->
<!--              	Masukkan No SPAJ/ No Polis(Khusus Program SimasPrima-Simpol) -->
<!--              </td> -->
	         <td colspan="3" width="256">
	         	<div id="spaj_pakett" style="visibility: hidden;" > 
	         		<spring:bind path="cmd.datausulan.spaj_paket">
						<input type="text" name="${status.expression}"
							value="${status.value }" maxlength="50"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
					</spring:bind>
<!--             		  <input type="text" name="spaj_paket" value="${cmd.datausulan.spaj_paket}" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>> -->
            	</div>
             </td>
            
            <td >
<!--             	<spring:bind path="cmd.datausulan.flag_paket">  -->
<!-- 	              <select name="${status.expression }" <c:if test="${ not empty status.errorMessage}"> style='background-color :#FFE1FD'</c:if>> -->
<!-- 	                <c:forEach items="${listPaket}" var="x"> -->
<!-- 	                	<option value="${x.key}" <c:if test="${cmd.datausulan.flag_paket eq x.key}">selected</c:if> >${x.value}</option>  -->
<!-- 	                </c:forEach>  -->
<!-- 	              </select> -->
<!--               	</spring:bind> -->
              	<input type ="hidden" id="pakett" name="pakett" value="${cmd.datausulan.flag_paket}"
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> 
            </td>
          </tr>
          <tr > 
            <td></td>
           <td>Masa Pembayaran</td>
            <td colspan="2">
            	<input type="hidden" name="pay" value ="${cmd.datausulan.mspo_pay_period}"> 
				<spring:bind path="cmd.datausulan.mspo_pay_period">
					<input type="text" name="${status.expression}" value="${status.value }"  size="5" readOnly>
				</spring:bind>
			</td>
			<td colspan="4">&nbsp;</td>
          </tr>
          <tr > 
            <td></td>
            <td>Masa Pertanggungan</td>
            <td colspan="2">
            	<input type="hidden" name="ins" value ="${cmd.datausulan.mspr_ins_period}"> 
				<spring:bind path="cmd.datausulan.mspr_ins_period"> 
					<input type="text" name="${status.expression}" value="${status.value }"  size="5" readOnly >
				</spring:bind>
			</td>
            <td >Cuti Premi Setelah Tahun ke</td>
            <td colspan="3"> <spring:bind path="cmd.datausulan.mspo_installment"> 
              <input type="text" name="${status.expression}"
						value="${status.value}"  size="5"
				<c:if test="${ not empty status.errorMessage}">
						class="read_only" 
					</c:if> >
              <font color="#CC3300">*</font> </spring:bind></td>
          </tr>
          <tr  > 
            <td></td>
            <td> Uang Pertanggungan 
              <input type ="hidden" name="kurss" value="${cmd.datausulan.kurs_p}"
			 <c:if test="${ not empty status.errorMessage}">
						class="read_only" 
					</c:if>> 
            </td>
            <td width="29"> <div id="kursup"> </div></td>
			<td width="310">
				<spring:bind path="cmd.datausulan.mspr_tsi">
					<input type="text" name="${status.expression}" value="${status.value }" size="30" 
						onfocus="showForm( 'tsiHelper', 'true' );" onblur="showForm( 'tsiHelper', 'false' );" 
						onchange="this.value=formatCurrency( this.value );" onkeyup="showFormatCurrency('tsiHelper', this.value);"
						<c:if test="${ not empty status.errorMessage}">
							class="read_only" 
						</c:if>>
					<font color="#CC3300">*</font>
				</spring:bind>
				<br/>
				<input type="text" id="tsiHelper" disabled="disabled" style="display: none;" tabindex="-1"/>
			</td>
			
			<td> Premi Standar/Pokok <br> <span class="info">(sesuai dengan cara pembayaran 
              ) </span></td>
            <td width="26"><div id="kurspremi"> </div></td>

			<td colspan="2" width="324">
				<spring:bind path="cmd.datausulan.mspr_premium">
					<input type="text" name="${status.expression}" value="${status.value }" size="30" 
						onfocus="showForm( 'premiumHelper', 'true' );" onblur="showForm( 'premiumHelper', 'false' );" 
						onchange="this.value=formatCurrency( this.value );" onkeyup="showFormatCurrency('premiumHelper', this.value);"
						<c:if test="${ not empty status.errorMessage}">
							class="read_only" 
						</c:if>>
					<font color="#CC3300">*</font>
				</spring:bind>
				<br/>
				<input type="text" id="premiumHelper" disabled="disabled" style="display: none;" tabindex="-1"/>
			</td>

		</tr>
          <tr  > 
            <td></td>
            <td colspan="2">  
              <spring:bind path="cmd.datausulan.cara_premi">
					<label for="premi_biasa"> <input type="radio" class=noBorder
						name="${status.expression}" value="0" onClick="cpy('0');"
						<c:if test="${cmd.datausulan.cara_premi eq 0 or cmd.datausulan.cara_premi eq null}"> 
									checked</c:if>
						id="premi_biasa">ISI PREMI </label>
					<br>
					<label for="premi_persentase"> <input type="radio" class=noBorder
						name="${status.expression}" value="1" onClick="cpy('1');"
						<c:if test="${cmd.datausulan.cara_premi eq 1}"> 
									checked</c:if>
						id="premi_persentase">ISI TOTAL PREMI DAN PILIH KOMBINASI </label> 
				</spring:bind> 
					<input type="hidden" name="tanda_premi" value='${cmd.datausulan.cara_premi}'>
            </td>
            
            <td width="310">KOMBINASI : <spring:bind path="cmd.datausulan.kombinasi"> 
				<select name="${status.expression }" onChange="ganti(this.options[this.selectedIndex].value);"
				<c:if test="${ not empty status.errorMessage}">
						class="read_only" 
					</c:if>>
					 <c:forEach var="kb" items="${select_kombinasi}"> <option value="${kb.ID}"
					<c:if test="${cmd.datausulan.kombinasi eq kb.ID}">selected</c:if> >${kb.KOMBINASI}</option> 
					</c:forEach> 
				  </select>
				  </spring:bind></td>
            <td> TOTAL PREMI <br>(Premi Pokok + Top Up):</td>

			<td width="324" colspan="3">
				<spring:bind path="cmd.datausulan.total_premi_kombinasi">
					<input type="text" name="${status.expression}" onChange="ganti(document.frmParam.tanda_premi.value); this.value=formatCurrency( this.value );" value="${status.value }" size="30" 
						onfocus="showForm( 'totpremiumHelper', 'true' );" onblur="showForm( 'totpremiumHelper', 'false' );" 
						onkeyup="showFormatCurrency('totpremiumHelper', this.value);"
						<c:if test="${ not empty status.errorMessage}">
							class="read_only" 
						</c:if>>
				</spring:bind>
				<br/>
				<input type="text" id="totpremiumHelper" disabled="disabled" style="display: none;" tabindex="-1"/>
			</td>
			
		</tr>
          <tr  > 
            <td  >2.</td>
            <td  > Cara Pembayaran Premi 
              <input type ="hidden" name="cb" value="${cmd.datausulan.lscb_id}"> 
            </td>
            <td colspan="6"> <div id="cara_bayar"> </div><font color="#CC3300">*</font>  </td>
          </tr>
          <tr  > 
            <td  >3.</td>
            <td  colspan="3"> Mulai Berlaku Pertanggungan <span class="info">(DD/MM/YYYY)</span></td>
            <td colspan="4" > Akhir Berlaku Pertanggungan <span class="info">(DD/MM/YYYY)</span></td>
          </tr>
          <tr  > 
            <td ></td>
            <td colspan="3"> <spring:bind path="cmd.datausulan.mste_beg_date"> 
              <c:choose>
            		<c:when test="${cmd.datausulan.convert eq 1}">
            			<script>inputDate('${status.expression}', '${status.value}', true,'listtglpolis1();');</script>
            		</c:when>
              		<c:otherwise>
              			<script>inputDate('${status.expression}', '${status.value}', false,'listtglpolis1();');</script>
              		</c:otherwise>
              </c:choose>
              <font color="#CC3300">*</font>  </spring:bind> </td>
            <td colspan="4"> <spring:bind path="cmd.datausulan.mste_end_date"> 
              <script>inputDate('${status.expression}', '${status.value}', true);</script>
              </spring:bind> </td>
          </tr>
          <tr style="visibility: hidden;"> 
            <th class="sub1"></th>
            <th class="sub1" colspan="7">JENIS ASURANSI TAMBAHAN (RIDER) 
              <spring:bind path="cmd.datausulan.jmlrider"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30">
               </spring:bind> 
            </th>
          </tr>
          <tr> 
            <th class="sub1" colspan="8" style="text-align: center;"> <input name="btnadd1" type="button" id="btnadd1" value="ADD" onClick="addRowDOM1('tablerider', '1')"> 
              <input name="btn_cancel1" type="button" id="btn_cancel1" value="DELETE" " onClick="cancel1()"> 
              <input type="hidden" name="mn" value="0"> </th>
          </tr>
        </table>
         <spring:bind path="cmd.datausulan.convert"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="5">
               </spring:bind> 
        <div id="divrider"> 
          
        <table id="tablerider" class="result_table">
          <tr> 
            <th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">No.</th>
            <th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">Nama Produk<font color='#CC3300'>*</font></th>
            <th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">Unit<font color='#CC3300'>*</font> </th>
            <th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">Klas<font color='#CC3300'>*</font> </th>
            <th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">Persentase UP</th>
            <th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">Uang Pertanggungan</td>
			<th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">Premi</th>
            <th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">Masa Per-<br>tanggungan</th>
            <th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">
            	Mulai Berlaku Pertanggungan<br>
            	<span class="info">(DD/MM/YYYY)</span>
            </th>
            <th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">
            	Akhir Berlaku Pertanggungan<br>
            	<span class="info">(DD/MM/YYYY)</span>
            </th>
            <th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">
            	Akhir Pembayaran<br>
            	<span class="info">(DD/MM/YYYY)</span>
            </th>
            <th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">Tertanggung<font color='#CC3300'>*</font></th>
            <th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">Rate</th>
            <th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">&nbsp;</th>
          </tr>

         <c:forEach items="${cmd.datausulan.daftaRider}" var="ride" varStatus="status"> 
          <c:if test="${status.index mod 2 eq 1}"></c:if>
          <tr <c:choose>
          	  	<c:when test="${status.index mod 2 eq 0}"> style="odd"</c:when>
          		<c:when test="${status.index mod 2 eq 1}"> style="even"</c:when>
          	  </c:choose>> 
            <td >${status.index +1}</td>
			<td>
					<select name="ride.plan_rider${status.index +1}" <c:if test="${ride.flag_include eq 1 or cmd.pemegang.flag_dis eq 0}">style='background-color :#D4D4D4' disabled</c:if>
					onchange="cekUpRiderDisabled(${status.index +1});">
						<c:forEach var="rider" items="${select_rider}">
							<option 
							<c:if test="${ride.plan_rider eq rider.plan}"> SELECTED </c:if>
							value="${rider.plan}">${rider.lsdbs_name} </option>
						</c:forEach>
					</select>
					<input type="hidden" name="ride.plan_rider1${status.index +1}" value="${ride.plan_rider}">

				<c:if test="${ride.lsbs_id eq 800}">	
					<c:if test="${ride.mspr_tsi_pa_a ne 0}"> <c:if test="${ride.mspr_tsi_pa_a ne null}"> Jenis Resiko A </c:if></c:if>
					<c:if test="${ride.mspr_tsi_pa_b ne 0}"> <c:if test="${ride.mspr_tsi_pa_b ne null}">- B </c:if></c:if>
					<c:if test="${ride.mspr_tsi_pa_c ne 0}"> <c:if test="${ride.mspr_tsi_pa_c ne null}"> - C </c:if></c:if>
					<c:if test="${ride.mspr_tsi_pa_d ne 0}"> <c:if test="${ride.mspr_tsi_pa_d ne null}"> - D </c:if></c:if>
					<c:if test="${ride.mspr_tsi_pa_m ne 0}"> <c:if test="${ride.mspr_tsi_pa_m ne null}"> - M </c:if></c:if>
				</c:if>
			</td>
           <td > 
              <input type="text" name='ride.mspr_unit${status.index +1}' value ='${ride.mspr_unit}' size="3" <c:if test="${ride.flag_include eq 1 or cmd.pemegang.flag_dis eq 0}">style='background-color :#D4D4D4' </c:if>  maxlength="2">
    	 	</td>
            <td ><input type="text" name='ride.mspr_class${status.index +1}' value ='${ride.mspr_class}' size="3" maxlength="2"></td>
            <td>
				<select name="ride.persenUp${status.index +1}" <c:if test="${ride.lsbs_id ne 813}">style='background-color :#D4D4D4' disabled</c:if>>
					<c:forEach var="pUp" items="${select_persenUp}">
						<option 
							<c:if test="${ride.persenUp eq pUp.ID}"> SELECTED </c:if>
							value="${pUp.ID}">${pUp.PERSEN}</option>
					</c:forEach>
					</select>					
				<input type="hidden" name="ride.persenUpx${status.index +1}" value ="${ride.persenUp}">
			</td>            
            <td ><input type="text" name='ride.mspr_tsi${status.index +1}' size="20" style='background-color :#D4D4D4' readOnly value =<fmt:formatNumber type='number' value='${ride.mspr_tsi}'/> ></td> 
	       <td ><input type="text" name='ride.mspr_premium${status.index +1}' size="20" style='background-color :#D4D4D4' readOnly value =<fmt:formatNumber type='number' value='${ride.mspr_premium}'/> ></td> 

	        <td ><input type="text" name='ride.mspr_ins_period${status.index +1}' value ='${ride.mspr_ins_period}'  size="5" style='background-color :#D4D4D4' readOnly></td>
            <td nowrap>
				<input type="text" name='mspr_beg_date${status.index +1}' value='<fmt:formatDate value="${ride.mspr_beg_date}" pattern="dd/MM/yyyy"/>'  size="15" style='background-color :#D4D4D4' readOnly>
			</td>
            <td nowrap>
				<input type="text" name='mspr_end_date${status.index +1}' value='<fmt:formatDate value="${ride.mspr_end_date}" pattern="dd/MM/yyyy"/>'  size="15" style='background-color :#D4D4D4' readOnly>
			</td>
            <td nowrap>
				<input type="text" name='mspr_end_pay${status.index +1 }' value='<fmt:formatDate value="${ride.mspr_end_pay}" pattern="dd/MM/yyyy"/>'  size="15" style='background-color :#D4D4D4' readOnly>
			</td>
						
            <td nowrap>
				<select name="ride.mspr_tt${status.index +1}" <c:if test="${ride.flag_include eq 1}">style='background-color :#D4D4D4' disabled</c:if>>
							<c:forEach var="insrider" items="${select_insrider}">
							<option 
							<c:if test="${ride.mspr_tt eq insrider.ID}"> SELECTED </c:if>
							value="${insrider.ID}">${insrider.INSRIDER}</option>
						</c:forEach>
					</select>					
			<input type="hidden" name="ride.mspr_tt1${status.index +1}" value ="${ride.mspr_tt}">
			</td>
            <td ><input type="text" name='ride.mspr_rate${status.index +1}' size="10" style='background-color :#D4D4D4' readOnly value =<fmt:formatNumber type='number' value='${ride.mspr_rate}'/>  > </td>
            <td  > <input type=checkbox class="noBorder" name="cek${status.index +1}" id= "ck${status.index +1}" <c:if test="${ride.flag_include eq 1}">style='background-color :#D4D4D4' disabled</c:if>>
			<input type="hidden" name="ride.flag_include${status.index +1}" value='${ride.flag_include}' size="3" >
			</td>
          </tr>
          </c:forEach> 
        </table>
 </div>

 <table>
 	<tr><td>&nbsp;</td></tr>
 </table>
 <table class="form_input" width = "100%">
 	<tr> 
            <th style="font-size: 14px;height: 30px;padding: 5px 0px 5px 0px; ></th>
            <th style="font-size: 14px;height: 30px;padding: 5px 0px 5px 0px;" colspan="7">PESERTA ASURANSI KESEHATAN 
              <spring:bind path="cmd.datausulan.jml_peserta"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30">
               </spring:bind>  
            </th>
          </tr>
          <tr> 
            <th class="sub1" colspan="8" style="text-align: center;">
             	<input name="btnadd1x" type="button" id="btnadd1x" value="ADD"  tabindex="15"  onClick="addRowDOM1x('tableProd', '1')">  
             	<input name="btn_cancel1x" type="button" id="btn_cancel1x" value="DELETE" tabindex="16"  onClick="cancel1x()">			
              	<input type="hidden" name="pesertax"  value="0">
            </th>
          </tr>
	<table id="tableProd" class="result_table">
            <tr > 
              	<th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">No.</th>
              	<th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">Nama <font color="#CC3300">*</font></th>
              	<th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">Sex<font color="#CC3300">*</font></th>
              	<th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">Tanggal Lahir <font color="#CC3300">*</font></th>
 	          	<th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">Umur</th>
              	<th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">Hubungan Dengan Pemegang Polis<font color="#CC3300">*</font></th>
              	<th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">Jenis Produk Rider<font color="#CC3300">*</font></th>
              	<th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">Jenis rider<font color="#CC3300">*</font></th>
             	<th style="font-size: 12px;height: 30px;padding: 5px 0px 5px 0px;">&nbsp;</th>
            </tr>
         <c:forEach items="${cmd.datausulan.daftarplus}" var="ptx" varStatus="status"> 
            <tr > 
             	<td>
              		<input type="text" name='ptx.no_urut${status.index +1}' value ='${ptx.no_urut}' size="3" maxlength="3" readOnly>
             	</td>
              	<td >
              		<input type="text" name='ptx.nama${status.index +1}' value ='${ptx.nama}' size="30" maxlength="200">
	           	</td>
	           	<td >
	           		<input type="radio" class=noBorder
						name='kelamin${status.index +1}' value="1"
						<c:if test="${ptx.kelamin eq 1}">  
									checked</c:if>  onClick="kelamin(${status.count},'1');"
							tabindex="11" id="cowok">Pria 
					<input type="radio" class=noBorder
						name='kelamin${status.index +1}' value="0"
						<c:if test="${ptx.kelamin eq 0}"> 
									checked</c:if> onClick="kelamin(${status.count},'0');"
						tabindex="12" id="cewek">Wanita
						<input type="hidden" name='ptx.kelamin${status.index +1}' value ='${ptx.kelamin}' size="3" maxlength="3" >
		       	</td>
		      	<td > 
              		<input type="text" name='tgllhr${status.index +1}' value='<fmt:formatDate value="${ptx.tgl_lahir}" pattern="dd"/>'  size="2" maxlength="2" >
					/<input type="text" name='blnhr${status.index +1}' value='<fmt:formatDate value="${ptx.tgl_lahir}" pattern="MM"/>'  size="2" maxlength="2" >
					/<input type="text" name='thnhr${status.index +1}' value='<fmt:formatDate value="${ptx.tgl_lahir}" pattern="yyyy"/>'  size="4" maxlength="4" >
					&nbsp;<input type="hidden" name='ptx.tgl_lahir${status.index +1}' value='<fmt:formatDate value="${ptx.tgl_lahir}" pattern="dd/MM/yyyy"/>'  size="5" >
              	</td>
              	<td>
              		<input type="text" name='ptx.umur${status.index +1}' value ='${ptx.umur}' size="3" maxlength="3" readOnly >
	          	</td>
              	<td>
                  	<select name="ptx.lsre_id${status.index +1}" >
						<c:forEach var="lsreIdPlus" items="${select_relasi}">
							<option 
							<c:if test="${ptx.lsre_id eq lsreIdPlus.ID}"> SELECTED </c:if>
							value="${lsreIdPlus.ID}">${lsreIdPlus.RELATION}</option>
						</c:forEach>
					</select>
	           	</td>
	           	<td>
                  	<select name="ptx.lsbs_id_plus${status.index +1}" >
						<c:forEach var="lsbsPlus" items="${select_prodKes}">
							<option 
							<c:if test="${ptx.lsbs_id_plus eq lsbsPlus.ID}"> SELECTED </c:if>
							value="${lsbsPlus.ID}">${lsbsPlus.PRODUK}</option>
						</c:forEach>
					</select>
	           	</td>
				<td>
					<select name="ptx.flag_jenis_peserta${status.index +1}">
						<c:forEach var="flagJenis" items="${select_flag_jp}">
							<option 
							<c:if test="${ptx.flag_jenis_peserta eq flagJenis.ID}"> SELECTED </c:if>
							value="${flagJenis.ID}">${flagJenis.JENIS}</option>
						</c:forEach>
					</select>
				</td>
	            <td ><input type=checkbox name="cek2${status.index +1}" id= "ck2${status.index +1}" class="noBorder"></td>
            </tr>
		</c:forEach>
	</table>
</table>
 
<input type="hidden" name="hal" value="${halaman}">
<input type="hidden" name="flag_warning_autodebet" value="0">
			<spring:bind path="cmd.pemegang.indeks_halaman"> 
              <input type="hidden" name="${status.expression}"
						value="${halaman-1}"  size="30" >
              </spring:bind> 
		<table border="0" width="100%"  cellspacing="1" cellpadding="1" class="entry2">
          <tr> 
            <td colspan="9" style="color: #CC3300;font-weight: bold">Note : Untuk pecahan decimal menggunakan titik.
			 <br>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; * Wajib diisi</font></b></p>
			</td>
          </tr>		
          <tr> 
            <td  align="center" colspan="4" > 
              <div align="center">
                <input type="submit" name="_target${halaman-1}" value="Prev &laquo;" onClick="prev()" 
				accesskey="P" onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
                <input type="submit" name="_target${halaman+1}" value="Next &raquo;" onClick="next()" 
				accesskey="N" onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
                <input type="hidden" name="_page" value="${halaman}">
              </div>
            </td>
          </tr>
        </table>
  
</table>
</form>
</body>
<script>
//<c:forEach items="${cmd.datausulan.daftaRider}" var="ride" varStatus="status"> 
//	cekUpRiderDisabled(${status.index +1});
//</c:forEach>
</script>
<%@ include file="/include/page/footer.jsp"%>