<%@ include file="/include/page/header.jsp"%>
<script>

</script>
<body onload="setupPanes('container1', 'tab2');" style="height: 100%;">
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" id="tab1">1. Input SPAJ/Polis</a>
				<a href="#" onClick="return showPane('pane2', this)" id="tab2">2. Pilih Tagihan</a>
				<a href="#" id="tab3">3. Input Pembayaran</a>
				<a href="#" id="tab4">4. Selesai</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane2" class="panes">
				<c:if test="${not empty cmd.dataPolis}">
					<fieldset>
						<legend>Informasi Polis</legend>
						<table class="entry2">
							<tr>
								<th nowrap="nowrap" style="width: 140px;">
									Produk:
								</th>
								<th nowrap="nowrap" class="left">
									<input type="text" readonly="readonly" value="[${cmd.dataPolis.lsbs_id}-${cmd.dataPolis.lsdbs_number}] ${cmd.dataPolis.lsdbs_name}" size="60">
									No. Register SPAJ:
									<input type="text" readonly="readonly" value="${cmd.dataPolis.reg_spaj}" size="17">
									No. Polis:
									<input type="text" readonly="readonly" value="${cmd.dataPolis.mspo_policy_no_format}" size="23">
								</th>
							</tr>
							<tr>
								<th nowrap="nowrap">
									Pemegang:
								</th>
								<th nowrap="nowrap" class="left">
									<input type="text" readonly="readonly" value="${cmd.dataPolis.pemegang}" size="50">
									Tertanggung: 
									<input type="text" readonly="readonly" value="${cmd.dataPolis.tertanggung}" size="50">
								</th>
							</tr>
							<tr>
								<th nowrap="nowrap">
									Masa Berlaku Polis:
								</th>
								<th nowrap="nowrap" class="left">
									<input type="text" value="<fmt:formatDate value="${cmd.dataPolis.mste_beg_date}" pattern="dd/MM/yyyy"/>" size="12" readonly>
									s/d			
									<input type="text" value="<fmt:formatDate value="${cmd.dataPolis.mste_end_date}" pattern="dd/MM/yyyy"/>" size="12" readonly>
									Uang Pertanggungan:
									<input style="text-align: right;" type="text" value="<fmt:formatNumber value="${cmd.dataPolis.mspr_tsi}" type="currency" 
										currencySymbol="${cmd.dataPolis.lku_symbol} " maxFractionDigits="2" minFractionDigits="0" />" size="25" readonly>
									Premi:
									<input style="text-align: right;" type="text" value="<fmt:formatNumber value="${cmd.dataPolis.mspr_premium}" type="currency" 
										currencySymbol="${cmd.dataPolis.lku_symbol} " maxFractionDigits="2" minFractionDigits="0" />" size="25" readonly>
								</th>
							</tr>
						</table>
					</fieldset>
				</c:if>

				<fieldset>
					<legend>Daftar Billing Outstanding</legend>
					<form:form id="formpost" name="formpost" commandName="cmd">
					<table class="entry2">
						<tr>
							<th style="width: 300px;">Outstanding Billing yang ingin dibayar: </th>
							<td>
								<display:table id="billing" name="cmd.daftarBilling" class="displaytag" >
									<display:column title="Pilih <span class=\"error\">*</span>">
										<label for="pilih${billing_rowNum}"><form:checkbox cssClass="noBorder" path="daftarBilling[${billing_rowNum-1}].pilih" id="pilih${billing_rowNum}" value="1"/></label> 
									</display:column>
									<display:column property="msbi_tahun_ke" title="Tahun Ke"/>
									<display:column property="msbi_premi_ke" title="Premi Ke"/>
									<display:column property="msbi_beg_date" title="Periode Billing" format="{0, date, dd/MM/yyyy}"/>
									<display:column property="mspo_policy_no" title="No. Polis"/>
									<display:column property="msbi_stamp" title="Materai" format="{0, number, #,##0.00;(#,##0.00)}"/>
									<display:column property="lku_symbol" title="Mata Uang"/>
									<display:column property="biaya" title="Tagihan" format="{0, number, #,##0.00;(#,##0.00)}"/>
									<display:column property="bayar" title="Total Bayar" format="{0, number, #,##0.00;(#,##0.00)}"/>
									<display:column property="sisa" title="Sisa Bayar" format="{0, number, #,##0.00;(#,##0.00)}"/>
								</display:table>
							</td>
						</tr>
						<tr>
							<th></th>
							<td>
								<div id="success">Catatan: Silahkan pilih salah satu Billing diatas kemudian lanjutkan dengan menekan tombol NEXT</div>
								<spring:hasBindErrors name="cmd">
									<div id="error">
										ERROR:<br/>
										<form:errors path="*" />
									</div>	
								</spring:hasBindErrors>
								<input type="submit" name="_target0" value="&laquo; Prev" accesskey="P">
								<input type="submit" name="_target2" value="Next &raquo;" accesskey="N">
							</td>
						</tr>
					</table>
					</form:form>
				</fieldset>
			</div>
			
		</div>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>