<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript">
	var pesan = '';
	
	function yuk(ayo){
		if(ayo=='simpan') if(!confirm('Simpan Referall?')) return false;
		
		document.formpost.aksi.value = ayo;
		this.disabled=true;document.formpost.submit();
	}
	function ganti(a,b,c,d,e,f,g,h){
		document.formpost.kode.value = a;
		document.formpost.lead.value = b;
		document.formpost.nama.value = c;
		document.formpost.nm_lead.value = d;
		document.formpost.jab_lead.value = e;
		document.formpost.nama_ref.value = f;
		document.formpost.jab_ref.value = g;
		document.formpost.cabang.value = h;
	}
</script>
</head>
<BODY onload="resizeCenter(550,400); document.title='PopUp :: Input Informasi Referral Bank'; setupPanes('container1', 'tab1'); document.formpost.cari.select(); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Input Informasi Referral Bank</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" action="${path}/bac/reff_bank.htm" style="text-align: center;">
					<input type="hidden" name="window" value="reff_new">
					<input type="hidden" name="aksi" value="">
					<input type="hidden" name="spaj" value="${cmd.spaj}">
					<fieldset>
						<legend>Referral Bank</legend>
						<table class="entry2">
							<tr>
								<th>Kode Nasabah</th>
								<td><input class="readOnly" readonly type="text" name="lead" size="8" value="${cmd.LEAD}" maxlength="6"></td>
							</tr>
							<tr>
								<th>No Ref/Lead</th>
								<td><input class="readOnly" readonly type="text" name="kode" size="20" value="${cmd.KODE}"></td>
							</tr>
							<tr>
								<th>Nama</th>
								<td><input class="readOnly" readonly type="text" name="nama" size="40" value="${cmd.NAMA}"></td>
							</tr>
							<tr>
								<th>Nama Reff</th>
								<td><input class="readOnly" readonly type="text" name="nm_lead" size="40" value="${cmd.NM_LEAD}"></td>
							</tr>
							<tr>	
								<th>Jabatan</th>
								<td><input class="readOnly" readonly type="text" name="jab_lead" size="40" value="${cmd.JAB_LEAD}"></td>
							</tr>
							<tr>	
								<th>Atasan Langsung</th>
								<td><input class="readOnly" readonly type="text" name="nama_ref" size="40" value="${cmd.NAMA_REF}"></td>
									
							</tr>
							<tr>	
								<th>Jabatan</th>
								<td><input class="readOnly" readonly type="text" name="jab_ref" size="40" value="${cmd.JAB_REF}"></td>
							</tr>
							<tr>	
								<th>ASM</th>
								<td><input class="readOnly" readonly type="text" name="jab_ref" size="40" value="${cmd.LEADER_2}"></td>
							</tr>	
							<tr>
								<th>Cabang</th>
								<td><input class="readOnly" readonly type="text" name="cabang" size="40" value="${cmd.CABANG}"></td>
							</tr>
						</table>
					</fieldset>
					<fieldset>
						<legend></legend>
						<table class="entry2">
							<tr>
								<th>Cari Referral : </th>
								<td>
									<input type="text" value="${cmd.cari}" name="cari" size="40" onkeyup="if(event.keyCode==13) document.formpost.btnCari.click();">
									
									<input type="button" name="btnCari" value="Cari" onclick="yuk('cari');"
										accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">			
	
									<input type="button" name="btnSave" value="Simpan" onclick="yuk('simpan');"
										accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
				
									<input type="button" name="close" value="Tutup" onclick="window.close();"
										accesskey="T" onmouseover="return overlib('Alt-T', AUTOSTATUS, WRAP);" onmouseout="nd();">
				
								</td>
							</tr>
							<tr>
								<th></th>
								<td>
									<c:if test="${not empty cmd.daftarNasabah}">
										<table class="simple">
											<thead>
												<tr>
													<th class="center">Kode Nasabah</th>
													<th class="center">No. Ref/Lead</th>
													<th class="center">Nama</th>
													<th class="center">Nama Reff</th>
													<th class="center">Jabatan</th>
													<th class="center">Atasan Langsung</th>
													<th class="center">Jabatan</th>
													<th class="center">Cabang</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="u" items="${cmd.daftarNasabah}" varStatus="stat">
													<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
														onclick='ganti("${u.RESULT1}","${u.RESULT0}","${u.RESULT2}","${u.RESULT3}","${u.RESULT4}","${u.RESULT5}","${u.RESULT6}","${u.RESULT7}");'>
														<td>${u.RESULT1}</td>
														<td>${u.RESULT0}</td>
														<td>${u.RESULT2}</td>
														<td>${u.RESULT3}</td>
														<td>${u.RESULT4}</td>
														<td>${u.RESULT5}</td>
														<td>${u.RESULT6}</td>
														<td>${u.RESULT7}</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</c:if>
									<div id="success" style="text-transform: none;">PESAN:<br>
										- Anda dapat melakukan pencarian berdasarkan nomor lead maupun nama<BR/>
										- Setelah memilih referall, silahkan tekan tombol SIMPAN<BR/>
										<c:forEach var="err" items="${cmd.success}">
											- <c:out value="${err}" escapeXml="false" />
											<br />
											<script>
												pesan += '${err}\n';
											</script>
										</c:forEach>
									</div>
									<c:if test="${not empty cmd.error }">
										<div id="error" style="text-transform: none;">ERROR:<br>
											<c:forEach var="err" items="${cmd.error}">
												- <c:out value="${err}" escapeXml="false" />
												<br />
												<script>
													pesan += '${err}\n';
												</script>
											</c:forEach>
										</div>
									</c:if>
								</td>
							</tr>
						</table>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

</body>
<script>
	if(pesan != '') alert(pesan);
</script>
<%@ include file="/include/page/footer.jsp"%>