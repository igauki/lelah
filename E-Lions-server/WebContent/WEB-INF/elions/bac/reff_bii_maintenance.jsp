<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>

<script language="JavaScript">
<!--
	if (!document.layers&&!document.all&&!document.getElementById)
		event="test"
		
	function showtip(current,e,text){
	
		if (document.all||document.getElementById){
			thetitle=text.split('<br>')
			if (thetitle.length>1){
				thetitles=''
				
			for (i=0;i<thetitle.length;i++)
				thetitles+=thetitle[i]
				
			current.title=thetitles
		}
		else
			current.title=text
	}
	
	else if (document.layers){
				document.tooltip.document.write('<layer bgColor="white" style="border:1px solid black;font-size:13px;">'+text+'</layer>')
				document.tooltip.document.close()
				document.tooltip.left=e.pageX+5
				document.tooltip.top=e.pageY+5
				document.tooltip.visibility="show"
			}
	}
	
	function hidetip(){
		if (document.layers)
			document.tooltip.visibility="hidden"
	}
	
	function body_onload()
	{
		  var i;
		  var kunci = document.frmParam.kunci.value;
		  if (kunci==null)
		  {
		  	kunci =" ";
		  }

		  var text=" | ";
		  for(i = 1 ; i <= document.frmParam.jumlah_halaman.value ; i++)
		  {
		  		if (kunci.length==0)
				{	
					text = text + "<a href ='${path}/bac/reff_bii_maintenance.htm?halaman="+i+"'>"+ i +"</a> | ";
				}else{
					text = text + "<a href ='${path}/bac/reff_bii_maintenance.htm?halaman="+i+"&kunci="+kunci+"'>"+ i +"</a> | ";
				}
		  }
		document.getElementById("hal").innerHTML=text;
		if (document.frmParam.statussubmit.value=="1")
		{
			alert("Data sudah berhasil diedit");
			document.frmParam.statussubmit.value="0";
		}
	}	
	
	function add()
	{
		popWin('${path}/bac/addreffbii.htm', 500, 800);
	}
	
	function ubah(indeks)
	{
		document.frmParam.elements['daftarreffbii['+indeks+'].flag_edit'].value = "1";
	}
	
	function sub(hasil)
	{
		 var kunci = document.frmParam.kunci.value;
		 if (kunci.length==0)
		 {
		 	document.frmParam.kunci.value=" ";
		 }
		 if (kunci==null)
		 {
		 	document.frmParam.kunci.value=" ";
		 }
		return confirm(hasil);
	}
	
	function view()
	{
		document.frmParam.kunci.value=" "; 
		window.open('${path}/bac/reff_bii_maintenance.htm','_self');
	}

// -->
</script>
<body  onLoad="body_onload();">

<form name="frmParam" method="post" onReset="return confirm('Reset Form Input?')" >

  <span class="subtitle">Maintenance Referral Bank</span> 
  <table width="100%" >
			<tr>
				<td>Cari Nama Reff:
		  <spring:bind path="cmd.kunci"> 
              <input type="text" name="${status.expression}"
						value="${status.value}" size="30" maxlength="30" tabindex="1">
              </spring:bind>				

			<input type="button" name="Search" value="Search"  onClick="window.open('${path}/bac/reff_bii_maintenance.htm?kunci='+document.frmParam.kunci.value,'_self');"
				accesskey="E" onmouseover="return overlib('Alt-E', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" name="View" value="View All"  onClick="view();"
				accesskey="V" onmouseover="return overlib('Alt-V', AUTOSTATUS, WRAP);" onmouseout="nd();">
				</td>					
				</td>
			</tr>  
 	<tr>
      <td valign="top">
		  Halaman :
			  <spring:bind path="cmd.halaman_aktif"> 
              <input type="text" name="${status.expression}"
						value="${status.value}" size="3" maxlength="3"  readonly style='background-color :#D4D4D4'>
              </spring:bind>   
		  <div id="hal">
		  
		  </div>
		  <spring:bind path="cmd.jumlah_halaman"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value}" size="30" maxlength="30" tabindex="1">
              </spring:bind>
			<spring:bind path="cmd.statussubmit"> 
              <input type="hidden" name="${status.expression}" 
						value="${status.value}"  size="15" maxlength="15" tabindex="8">
              </spring:bind>			  
          <table class="entry" width="100%">
          <tr> 
            <th width="28">ID</th>
            <th width="246">NAMA REFF</th>
            <th width="269"> CABANG BII</th>
            <th width="73">NO A/C</th>
            <th width="132">CABANG A/C</th>
            <th width="128">ATAS NAMA</th>
            <th width="65">AKTIF</th>
            <th width="47">NPK</th>
          </tr>
		  <c:forEach items="${cmd.daftarreffbii}"  var="bii" varStatus="st"> 
          <tr> 
            <th>${st.count}</th>
            <th>
			<spring:bind path="cmd.daftarreffbii[${st.index}].lrb_id"> 
              <input type="hidden" name="${status.expression}" onChange="ubah(${st.index});"
						value="${status.value}" size="30" maxlength="30" tabindex="1" >
              </spring:bind> 
			<spring:bind path="cmd.daftarreffbii[${st.index}].nama_reff"> 
              <input type="text" name="${status.expression}" onChange="ubah(${st.index});"
						value="${status.value}" size="30" maxlength="30" tabindex="1" >
              </spring:bind> </th>
            <th> 
			<spring:bind path="cmd.daftarreffbii[${st.index}].lcb_no"> 
					<select name="${status.expression}" onChange="ubah(${st.index});">
						<c:forEach var="cabang" items="${daftarcabangbii}">
							<option value="${cabang.LCB_NO}" 
								<c:if test="${status.value eq cabang.LCB_NO}"> selected </c:if>>${cabang.NAMA_CABANG}</option>
						</c:forEach>
					</select>			
              </spring:bind></th>
            <th><spring:bind path="cmd.daftarreffbii[${st.index}].no_rek"> 
              <input type="text" name="${status.expression}" onChange="ubah(${st.index});"
						value="${status.value}"  size="30" maxlength="30" tabindex="3" >
              </spring:bind></th>
            <th><spring:bind path="cmd.daftarreffbii[${st.index}].cab_rek"> 
              <input type="text" name="${status.expression}" onChange="ubah(${st.index});"
						value="${status.value}" size="30" maxlength="30" tabindex="4" >
              </spring:bind></th>
            <th> <spring:bind path="cmd.daftarreffbii[${st.index}].atas_nama"> 
              <input type="text" name="${status.expression}" onChange="ubah(${st.index});"
						value="${status.value}"  size="30" maxlength="30" tabindex="5">
              </spring:bind> </th>
            <th>
			<spring:bind path="cmd.daftarreffbii[${st.index}].flag_aktif"> 
			<select name="${status.expression}" onChange="ubah(${st.index});">
					<option value="1" <c:if test="${status.value eq 1}">selected</c:if> >AKTIF</option>
					<option value="0" <c:if test="${status.value eq 0}">selected</c:if> >TIDAK AKTIF</option>
			</select>			
			 </spring:bind>
			 </th>
            <th>
			 <spring:bind path="cmd.daftarreffbii[${st.index}].npk"> 
              <input type="text" name="${status.expression}" onChange="ubah(${st.index});"
						value="${status.value}"  size="15" maxlength="15" tabindex="8">
              </spring:bind>
			  	<spring:bind path="cmd.daftarreffbii[${st.index}].hit_err"> 
              <input type="hidden" name="${status.expression}" onChange="ubah(${st.index});"
						value="${status.value}"  size="15" maxlength="15" tabindex="8">
              </spring:bind>
			 <spring:bind path="cmd.daftarreffbii[${st.index}].flag_edit"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value}"  size="15" maxlength="15" tabindex="8">
              </spring:bind>			  
			</th>
          </tr>
		 </c:forEach>
        </table>

    
        <div id="buttons" style="margin: 5px 0px 5px 0px;"> 
		  <input type="button" name="Add" value="Add" onclick="add();"
				accesskey="A" onmouseover="return overlib('Alt-A', AUTOSTATUS, WRAP);" onmouseout="nd();">
          <input type="submit" value="Save" name="save"  onclick="sub(this.value+'?');"
				accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
          <input type="button" name="cancel" value="Cancel" onclick="window.close()";
				accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">
		 <spring:bind path="cmd.*"> <c:if test="${not empty status.errorMessages}"> 
          <div id="error"> ERROR:<br>
            <c:forEach var="error" items="${status.errorMessages}"> - <c:out value="${error}" escapeXml="false" /> 
            <br />
            </c:forEach></div>
          </c:if> </spring:bind> </div>
      </td>
      </tr>
  
  </table>
</form>
</body>

<%@ include file="/include/page/footer.jsp"%>
