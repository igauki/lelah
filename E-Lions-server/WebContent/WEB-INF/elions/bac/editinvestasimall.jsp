<%@ include file="/include/page/header_mall.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<script language="JavaScript">

var jumlah_investasi = 34;

function brekele(ncek){
	if(ncek) alert('Perhatian! Dengan memilih breakable, maka akan digunakan rate terrendah untuk periode bersangkutan, dan jangan lupa menyertakan MEMO PERMINTAAN BREAKABLE dari Nasabah.');
	if(ncek) {
		document.getElementById('brek').value = '1'; 
	} else {
		document.getElementById('brek').value = '0';
	} 
}

function telemarket(ncek){
	if(ncek) {
		document.getElementById('tele').value = '1'; 
	} else {
		document.getElementById('tele').value = '0';
	} 
}

function setPersentaseRate(mgi){
	var lsbs_id = ${cmd.datausulan.lsbs_id};
	var lsdbs_number = ${cmd.datausulan.lsdbs_number};
	var flag_bulanan = ${cmd.powersave.flag_bulanan};
	var flag_special = ${cmd.powersave.flag_special};
	var begdate = document.frmParam.elements['powersave.begdate_topup'].value;
	var tahun = begdate.substr(6,4);
	var bulan = begdate.substr(3,2);
	var tanggal = begdate.substr(0,2);
	
	var sept10=new Date(2009,8,10);
	sept10.setHours(0,0,0,0);
	//sept10.setFullYear(2009,8,19); // 8 = september, dimulai dari 0 = januari sampai 11 = desember
	
	var begdate2=new Date(tahun,bulan-1,tanggal);
	begdate2.setHours(0,0,0,0);
	//begdate2.setFullYear(tahun,parseInt(bulan)-1,parseInt(tanggal)+1);
	
	if(lsbs_id == 164 || lsbs_id == 174){
		if(begdate2<sept10){
			xmlData.src = "${path}/xml/faktor_stable_"+lsdbs_number+".xml"; //1 = stable, 2 = stable BII
		}else {
			if(flag_special == 1) {
				xmlData.src = "${path}/xml/faktor_stable_"+lsdbs_number+".xml";
			}else{
				xmlData.src = "${path}/xml/faktor_stable_"+lsdbs_number+"_10sept.xml"; //1 = stable, 2 = stable BII
			}
		}
		if(flag_bulanan == 1) xmlData.src = "${path}/xml/faktor_stable_bulanan.xml";
		
		var collPosition = xmlData.selectNodes( "//Position" );
		for( var i = 0; i < collPosition.length; i++ ){
			var id_ = collPosition.item(i).selectSingleNode("ID").text;
			var faktor_ = collPosition.item(i).selectSingleNode("FAKTOR").text;
			if(mgi == id_ || (mgi >= 12 && id_ == 0)){
				document.frmParam.elements['powersave.msl_bp_rate'].value = faktor_;
				break;
			}
		}
	}
}

<!--
<spring:bind path="cmd.datausulan.jml_benef"> 
var jmlpenerima=${status.value};
</spring:bind>  
			
var varOptionhub ;
var varOptSex ;
var flag_add=0;
var flag_add1=0;
	//function xml rpenerima
	function generateXML_penerima( objParser, strID, strDesc ) {
		varOptionhub = "";
		
		if (window.ActiveXObject) {
			var collPosition = objParser.selectNodes( "//Position" );
			for( var i = 0; i < collPosition.length; i++ ) 
				varOptionhub += "<OPTION " + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
		}
		else if (document.implementation && document.implementation.createDocument) {
			var count = 0;
			var content = new Array(2);
			content[0] = new Array();
			
			var nodes = objParser.evaluate("//"+strID, objParser, null, XPathResult.ANY_TYPE, null);
			var result=nodes.iterateNext();
			while (result) {
			  content[0][count++] = result.childNodes[0].nodeValue;
			  result=nodes.iterateNext();
			}
			
			nodes = objParser.evaluate("//"+strDesc, objParser, null, XPathResult.ANY_TYPE, null);
			var result=nodes.iterateNext();
			count = 0;
			content[1] = new Array();
			while (result) {
				content[1][count++] = result.childNodes[0].nodeValue;
				result=nodes.iterateNext();
			}
			
			for(a=0;a<content[0].length;a++) {
				//document.write(content[0][a] + ' ' + content[1][a]);
				//document.write("<br />");
				varOptionhub += "<OPTION " + " VALUE='" + content[0][a] + "'>" + content[1][a] + "</OPTION>";
			}
		}	
	}

	function generateSex_Benef() {
		varOptSex = "";
		varOptSex += "<OPTION  VALUE='0'>Perempuan</OPTION>";
		varOptSex += "<OPTION  VALUE='1'>Laki-Laki</OPTION>";
	}
	
	
function kuasa_onClick(){
	if(document.frmParam.mrc_kuasa1.checked == true){
		document.frmParam.mrc_kuasa1.checked = true;
		document.frmParam.elements['rekening_client.mrc_kuasa'].value = '1';

	}else{
		document.frmParam.mrc_kuasa1.checked = false;
		document.frmParam.elements['rekening_client.mrc_kuasa'].value = '0';

	}
}	

function setInvestasiReadOnly(ke, isReadOnly){
	if(isReadOnly){
		document.frmParam.elements['investasiutama.daftarinvestasi['+ke+'].mdu_persen1'].value='';
		document.frmParam.elements['investasiutama.daftarinvestasi['+ke+'].mdu_persen1'].readOnly=true;	
		document.frmParam.elements['investasiutama.daftarinvestasi['+ke+'].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
		document.frmParam.elements['investasiutama.daftarinvestasi['+ke+'].mdu_jumlah1'].value='';
	}else{
		document.frmParam.elements['investasiutama.daftarinvestasi['+ke+'].mdu_persen1'].readOnly=false;	
		document.frmParam.elements['investasiutama.daftarinvestasi['+ke+'].mdu_persen1'].style.backgroundColor ='#FFFFFF';	
	}
}

	
	function Body_onload() {
	
		if(document.getElementById('alertEkaSehat').value==1){
// 			alert('Usia Peserta max 50th dan wajib melampirkan hasil general Check Up terakhir atau medis : LPK,Urin,darah rutin,SGPT,SGOT,GGT & cholesterol atas biaya sendiri');
		}
	
		if (document.frmParam.elements['rekening_client.mrc_kuasa'].value == '1')
		{
			document.frmParam.mrc_kuasa1.checked = true;
		}else{
			document.frmParam.mrc_kuasa1.checked = false;
		}

		document.frmParam.elements['powersave.msl_tgl_nab'].style.backgroundColor ='#D4D4D4';		
		document.frmParam.elements['powersave.msl_nab'].style.backgroundColor ='#D4D4D4';		

	
		var nameSelection
		var path
	//	xmlData.async = false;
		<c:choose>
			<c:when test="${empty sessionScope.currentUser.cab_bank}">
				//xmlData.src = "${path}/xml/RELATION.xml";
				path = "${path}/xml/RELATION.xml";
			</c:when>
			<c:otherwise>
				//xmlData.src = "${path}/xml/RELATION_BANCASS_BENEF.xml";
				path = "${path}/xml/RELATION_BANCASS_BENEF.xml";
			</c:otherwise>
		</c:choose>
		generateXML_penerima(loadXMLDoc(path), 'ID','RELATION');
		//generateXML_penerima( xmlData, 'ID','RELATION');
		generateSex_Benef();
		var flag_account=document.frmParam.elements['datausulan.flag_account'].value ;
		var flagbungasimponi = document.frmParam.elements['datausulan.isBungaSimponi'].value;
		var flagbonustahapan= document.frmParam.elements['datausulan.isBonusTahapan'].value;
		var flag_bao =  document.frmParam.elements['datausulan.flag_bao'].value;
		var kode_flag = document.frmParam.elements['datausulan.kode_flag'].value;
		if ( (flag_bao ==1) && ((flag_account==2)||(flag_account==3)) )
		{
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].disabled=false;
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].style.backgroundColor ='#FFFFFF';				
		}else{
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].value='0';	
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].disabled=true;
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].style.backgroundColor ='#D4D4D4';		
		}
		

		if ((document.frmParam.elements['datausulan.mste_flag_cc'].value=='1') || (document.frmParam.elements['datausulan.mste_flag_cc'].value=='2') || (flag_account== 3))
			{
				document.frmParam.elements['account_recur.mar_holder'].readOnly = false;
				document.frmParam.elements['account_recur.mar_holder'].style.backgroundColor ='#FFFFFF';
				if (document.frmParam.elements['account_recur.mar_holder'].value =='')
				{
					document.frmParam.elements['account_recur.mar_holder'].value = 	document.frmParam.elements['pemegang.mcl_first'].value;
				}				
				
				document.frmParam.elements['account_recur.lbn_id'].disabled = false;
				document.frmParam.elements['account_recur.flag_jn_tabungan'].disabled = false;
				document.frmParam.elements['account_recur.lbn_id'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.flag_jn_tabungan'].style.backgroundColor ='#FFFFFF';
				
				document.frmParam.elements['account_recur.mar_acc_no'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no'].style.backgroundColor ='#FFFFFF';
				/*tambahan setting dari Bertho untuk split mar_acc_no*/
				document.frmParam.elements['account_recur.mar_acc_no_split[0]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[0]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[1]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[1]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[2]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[2]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[3]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[3]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[4]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[4]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[5]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[5]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[6]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[6]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[7]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[7]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[8]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[8]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[9]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[9]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[10]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[10]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[11]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[11]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[12]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[12]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[13]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[13]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[14]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[14]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[15]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[15]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[16]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[16]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[17]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[17]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[18]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[18]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[19]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[19]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[20]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[20]'].style.backgroundColor ='#FFFFFF';
			
				<c:if test="${not empty cmd.account_recur.lbn_id}">
						ajaxPjngRek('${cmd.account_recur.lbn_id}', 2,'account_recur.mar_acc_no_split');							
				</c:if>
				
								/*end of setting*/
				document.frmParam.caribank2.readOnly = false;
				document.frmParam.caribank2.style.backgroundColor ='#FFFFFF';
				document.frmParam.btncari2.disabled = false;
		}else{
				document.frmParam.elements['account_recur.mar_holder'].readOnly = true;
				document.frmParam.elements['account_recur.mar_holder'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_holder'].value='';			
				document.frmParam.elements['account_recur.lbn_id'].disabled = true;
				document.frmParam.elements['account_recur.flag_jn_tabungan'].disabled = true;
				document.frmParam.elements['account_recur.lbn_id'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.lbn_id'].value='NULL';
				document.frmParam.elements['account_recur.flag_jn_tabungan'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no'].style.backgroundColor ='#D4D4D4';
				/*tambahan setting dari Bertho untuk split mar_acc_no*/
				
				document.frmParam.elements['account_recur.mar_acc_no_split[0]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[0]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[1]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[1]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[2]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[2]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[3]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[3]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[4]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[4]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[5]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[5]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[6]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[6]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[7]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[7]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[8]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[8]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[9]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[9]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[10]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[10]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[11]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[11]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[12]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[12]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[13]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[13]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[14]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[14]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[15]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[15]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[16]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[16]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[17]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[17]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[18]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[18]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[19]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[19]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[20]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[20]'].style.backgroundColor ='#D4D4D4';
				
								/*end of setting*/
				document.frmParam.elements['account_recur.mar_acc_no'].value='';
				document.frmParam.caribank2.readOnly = true;
				document.frmParam.caribank2.style.backgroundColor ='#D4D4D4';
				document.frmParam.caribank2.value='';
				document.frmParam.btncari2.disabled = true;
						
		}
		
		//alert(flag_account);
		
		if ((flag_account ==2)||(flag_account ==3))
		{
			document.frmParam.elements['rekening_client.mrc_no_ac'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac'].style.backgroundColor ='#FFFFFF';
			/*Tambahan dari Bertho untuk split rekening*/
			document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[20]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[20]'].style.backgroundColor ='#FFFFFF';
			
			<c:if test="${not empty cmd.rekening_client.lsbp_id}">
					ajaxPjngRek('${cmd.rekening_client.lsbp_id}', 1,'rekening_client.mrc_no_ac_split');
			</c:if>
		
		
			/*End of code*/
			document.frmParam.elements['rekening_client.mrc_nama'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_nama'].style.backgroundColor ='#FFFFFF';
			if (document.frmParam.elements['rekening_client.mrc_nama'].value =="")
			{
				document.frmParam.elements['rekening_client.mrc_nama'].value = document.frmParam.elements['pemegang.mcl_first'].value;
			}
			
			document.frmParam.elements['rekening_client.lsbp_id'].readOnly = false;
			document.frmParam.elements['rekening_client.lsbp_id'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_kota'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_kota'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.tgl_surat'].readOnly = false;
			document.frmParam.elements['rekening_client.tgl_surat'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.notes'].readOnly = false;
			document.frmParam.elements['rekening_client.notes'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_kuasa'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_kuasa'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_cabang'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_cabang'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_jenis'].disabled = false;
			document.frmParam.elements['rekening_client.mrc_jenis'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_kurs'].disabled = false;
			document.frmParam.elements['rekening_client.mrc_kurs'].style.backgroundColor ='#FFFFFF';
			document.frmParam.caribank1.readOnly = false;
			document.frmParam.caribank1.style.backgroundColor ='#FFFFFF';
			document.frmParam.btncari1.disabled = false;
		}else{
			document.frmParam.elements['rekening_client.mrc_no_ac'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac'].value = '';
			
			/*Tambahan dari Bertho untuk split rekening*/
			document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[20]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[20]'].value = '';
			
			document.frmParam.elements['rekening_client.mrc_nama'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_nama'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_nama'].value='';
			document.frmParam.elements['rekening_client.lsbp_id'].disabled = true;
			document.frmParam.elements['rekening_client.lsbp_id'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.lsbp_id'].value='NULL';
			document.frmParam.elements['rekening_client.mrc_kota'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_kota'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_kota'].value='';
			document.frmParam.elements['rekening_client.tgl_surat'].readOnly = true;
			document.frmParam.elements['rekening_client.tgl_surat'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.tgl_surat'].value='';			
			document.frmParam.elements['rekening_client.notes'].readOnly = true;
			document.frmParam.elements['rekening_client.notes'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.notes'].value='';
			document.frmParam.mrc_kuasa1.disabled = true;
			document.frmParam.mrc_kuasa1.checked = false;
			document.frmParam.elements['rekening_client.mrc_kuasa'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_kuasa'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_kuasa'].value='0';
			document.frmParam.elements['rekening_client.mrc_cabang'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_cabang'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_cabang'].value='';
			document.frmParam.elements['rekening_client.mrc_jenis'].disabled = true;
			document.frmParam.elements['rekening_client.mrc_jenis'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_jenis'].value='0';
			document.frmParam.elements['rekening_client.mrc_kurs'].disabled = true;
			document.frmParam.elements['rekening_client.mrc_kurs'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_kurs'].value='';			
			document.frmParam.caribank1.readOnly = true;
			document.frmParam.caribank1.style.backgroundColor ='#D4D4D4';
			document.frmParam.caribank1.value='';
			document.frmParam.btncari1.disabled = true;

		}
		
		var jumlah_fund = document.frmParam.elements['investasiutama.jmlh_invest'].value;
		
		if ((Number(kode_flag)==1) )
		{
				document.frmParam.elements['powersave.mps_jangka_inv'].disabled=false;
				document.frmParam.elements['powersave.mps_jangka_inv'].style.backgroundColor ='#FFFFFF';
				
				document.frmParam.elements['powersave.mps_roll_over'].disabled=false;
				document.frmParam.elements['powersave.mps_roll_over'].style.backgroundColor ='#FFFFFF';
				
				document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].value="";
				document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].disabled=true;
				document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].style.backgroundColor ='#D4D4D4';

				document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].value="";
				document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].disabled=true;
				document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].style.backgroundColor ='#D4D4D4';

				document.frmParam.elements['powersave.begdate_topup'].value="__/__/____";
				document.frmParam.elements['powersave.begdate_topup'].disabled=true;
				document.frmParam.elements['powersave.begdate_topup'].readOnly=true;
				document.frmParam.elements['powersave.begdate_topup'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['powersave.msl_bp_rate'].value="0";
				document.frmParam.elements['powersave.msl_bp_rate'].readOnly=true;
				document.frmParam.elements['powersave.msl_bp_rate'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.mspr_premium.value="";
				
				document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].value="";
				document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].readOnly=true;
				document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].style.backgroundColor ='#D4D4D4';

				document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].value="";
				document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].readOnly=true;
				document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].style.backgroundColor ='#D4D4D4';

				
				document.frmParam.elements['datausulan.total_premi_rider'].value="";
				document.frmParam.elements['investasiutama.total_premi_sementara'].value="";
				
				document.frmParam.elements['powersave.tarik_bunga'].value="0";
				document.frmParam.elements['powersave.tarik_bunga'].readOnly=true;
				document.frmParam.elements['powersave.tarik_bunga'].disabled=true;
				document.frmParam.elements['powersave.tarik_bunga'].style.backgroundColor ='#D4D4D4';
				
				tutup();
				jns_rate();
			}else{
				if ( Number(kode_flag)!=11 && Number(kode_flag)!=15  )
				{
					document.frmParam.elements['powersave.mps_jangka_inv'].value="";
					document.frmParam.elements['powersave.mps_jangka_inv'].disabled=true;
					document.frmParam.elements['powersave.mps_jangka_inv'].style.backgroundColor ='#D4D4D4';
					
					document.frmParam.elements['powersave.mps_rate'].value="";				
					document.frmParam.elements['powersave.mps_prm_interest'].value="";
					document.frmParam.elements['powersave.mps_prm_deposit'].value="";
					
					document.frmParam.elements['powersave.mps_roll_over'].value="";
					document.frmParam.elements['powersave.mps_roll_over'].disabled=true;
					document.frmParam.elements['powersave.mps_roll_over'].style.backgroundColor ='#D4D4D4';
					
					document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].disabled=false;
					document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].style.backgroundColor ='#FFFFFF';
	
					document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].disabled=false;
					document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].style.backgroundColor ='#FFFFFF';
	
					
					if (document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].value=="")
					{
						document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].value="0";
					}
					document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].readOnly=false;
					document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].style.backgroundColor ='#FFFFFF';
	
					if (document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].value=="")
					{
						document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].value="0";
					}
					document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].readOnly=false;
					document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].style.backgroundColor ='#FFFFFF';
					
					document.frmParam.elements['powersave.mps_rate'].readOnly=true;
					document.frmParam.elements['powersave.mps_rate'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['powersave.mps_rate'].value = "";
					document.frmParam.elements['powersave.mps_employee'].disabled=true;
					document.frmParam.elements['powersave.mps_employee'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['powersave.mps_employee'].value = "";				
					document.frmParam.elements['powersave.mpr_note'].readOnly=true;
					document.frmParam.elements['powersave.mpr_note'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['powersave.mpr_note'].value = "";
					document.frmParam.elements['powersave.mps_jenis_plan'].disabled=true;
					document.frmParam.elements['powersave.mps_jenis_plan'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['powersave.mps_jenis_plan'].value = "";
					
				document.frmParam.elements['powersave.begdate_topup'].value="__/__/____";
				document.frmParam.elements['powersave.begdate_topup'].disabled=true;
				document.frmParam.elements['powersave.begdate_topup'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['powersave.msl_bp_rate'].value="0";
				document.frmParam.elements['powersave.msl_bp_rate'].readOnly=true;
				document.frmParam.elements['powersave.msl_bp_rate'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['powersave.tarik_bunga'].value="0";
				document.frmParam.elements['powersave.tarik_bunga'].readOnly=true;
				document.frmParam.elements['powersave.tarik_bunga'].disabled=true;
				document.frmParam.elements['powersave.tarik_bunga'].style.backgroundColor ='#D4D4D4';
				}
			}	

		if ((Number(kode_flag)==1) || (Number(kode_flag)==0) )
		{
			document.frmParam.elements['datausulan.mste_flag_investasi'].value = '0';
			document.frmParam.elements['datausulan.mste_flag_investasi'][0].disabled=true;
			document.frmParam.elements['datausulan.mste_flag_investasi'][1].disabled=true;
		}

		switch (Number(kode_flag))
		{
			case 0: //tutup semua investasi
				for(q = 0; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);
			
				document.frmParam.elements['investasiutama.total_persen'].value="";
				
				document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].value="";
				document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].disabled=true;
				document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].style.backgroundColor ='#D4D4D4';

				document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].value="";
				document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].disabled=true;
				document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.mspr_premium.value="";
				
				document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].value="";
				document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].readOnly=true;
				document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].style.backgroundColor ='#D4D4D4';

				document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].value="";
				document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].readOnly=true;
				document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['datausulan.total_premi_rider'].value="";
				document.frmParam.elements['investasiutama.total_premi_sementara'].value="";				

				document.frmParam.elements['powersave.mps_jangka_inv'].value="";
				document.frmParam.elements['powersave.mps_jangka_inv'].disabled=true;
				document.frmParam.elements['powersave.mps_jangka_inv'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['powersave.mps_rate'].value="";				
				document.frmParam.elements['powersave.mps_prm_interest'].value="";
				document.frmParam.elements['powersave.mps_prm_deposit'].value="";
				
				document.frmParam.elements['powersave.mps_roll_over'].value="";
				document.frmParam.elements['powersave.mps_roll_over'].disabled=true;
				document.frmParam.elements['powersave.mps_roll_over'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['powersave.mps_rate'].readOnly=true;
				document.frmParam.elements['powersave.mps_rate'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['powersave.mps_rate'].value = "";
				document.frmParam.elements['powersave.mps_employee'].disabled=true;
				document.frmParam.elements['powersave.mps_employee'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['powersave.mps_employee'].value = "";				
				document.frmParam.elements['powersave.mpr_note'].readOnly=true;
				document.frmParam.elements['powersave.mpr_note'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['powersave.mpr_note'].value = "";
				document.frmParam.elements['powersave.mps_jenis_plan'].disabled=true;
				document.frmParam.elements['powersave.mps_jenis_plan'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['powersave.mps_jenis_plan'].value = "";				
				break;
			
			case 1: //tutup semua investasi (powersave)
				for(q = 0; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				document.frmParam.elements['investasiutama.total_persen'].value="";
				break;
			
			case 2: //buka fixed & dyna
				setInvestasiReadOnly(0, false);
				setInvestasiReadOnly(1, false);
				for(q = 2; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				break;			

			case 3: //buka aggresive
				for(q = 0; q <= 1; q++) setInvestasiReadOnly(q, true);
				setInvestasiReadOnly(2, false);
				for(q = 3; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				break;			
			
			case 4: //buka fixed & dyna & aggresive 
				setInvestasiReadOnly(0, false);
				setInvestasiReadOnly(1, false);
				setInvestasiReadOnly(2, false);
				for(q = 3; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				break;			
			
			case 5: //buka secured & dyna dollar
				for(q = 0; q <= 2; q++) setInvestasiReadOnly(q, true);
				setInvestasiReadOnly(3, false);
				setInvestasiReadOnly(4, false);
				for(q = 5; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				break;			
			
			case 6: //buka Syariah fixed & syariah dyna
				for(q = 0; q <= 4; q++) setInvestasiReadOnly(q, true);
				setInvestasiReadOnly(5, false);
				setInvestasiReadOnly(6, false);
				for(q = 7; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				break;			
		
			case 7: //excellink syariah - fixed, dina, aggre
				for(q = 0; q <= 4; q++) setInvestasiReadOnly(q, true);
				setInvestasiReadOnly(5, false);
				setInvestasiReadOnly(6, false);
				setInvestasiReadOnly(7, false);
				for(q = 8; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				break;			
			
			case 8: //excellink syariah dollar - secure, dina
				for(q = 0; q <= 7; q++) setInvestasiReadOnly(q, true);
				setInvestasiReadOnly(8, false);
				setInvestasiReadOnly(9, false);
				for(q = 10; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				break;			

			case 9: //arthalink, ekalink88, ekalink88+ - fixed, dina, aggre
	
				var lsbs = Number(document.frmParam.elements['datausulan.lsbs_id'].value);
				var lsdbs = Number(document.frmParam.elements['datausulan.lsdbs_number'].value);
				
				if(lsbs == 162 && lsdbs <= 4){ //arthalink
					for(q = 0; q <= 9; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(10, false);
					setInvestasiReadOnly(11, false);
					setInvestasiReadOnly(12, false);
					for(q = 13; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
					break;			
				}else if(lsbs == 162 && (lsdbs == 5 || lsdbs == 6)){ //ekalink 88
					for(q = 0; q <= 14; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(15, false);
					setInvestasiReadOnly(16, false);
					setInvestasiReadOnly(17, false);
					for(q = 18; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
					break;			
				}else if(lsbs == 162 && (lsdbs == 7 || lsdbs == 8)){ //ekalink 88+
					for(q = 0; q <= 22; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(23, false);
					setInvestasiReadOnly(24, false);
					setInvestasiReadOnly(25, false);
					for(q = 26; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
					break;			
				}
				break;
			
			case 10: //arthalink, ekalink88, ekalink88+ dollar - secure, dina
				var lsbs = document.frmParam.elements['datausulan.lsbs_id'].value;
				var lsdbs = document.frmParam.elements['datausulan.lsdbs_number'].value;
				
				if(lsbs == 162 && lsdbs <= 4){ //arthalink
					for(q = 0; q <= 12; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(13, false);
					setInvestasiReadOnly(14, false);
					for(q = 15; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
					break;			
				}else if(lsbs == 162 && (lsdbs == 5 || lsdbs == 6)){ //ekalink 88
					for(q = 0; q <= 17; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(18, false);
					setInvestasiReadOnly(19, false);
					for(q = 20; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
					break;			
				}else if(lsbs == 162 && (lsdbs == 7 || lsdbs == 8)){ //ekalink 88+
					for(q = 0; q <= 25; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(26, false);
					setInvestasiReadOnly(27, false);
					break;			
				}
				break;
														
			case 11: //stable link
				for(q = 0; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				document.frmParam.elements['investasiutama.total_persen'].value="100";

				if (document.frmParam.elements['datausulan.kurs_premi'].value == '01'){
					document.frmParam.elements['investasiutama.daftarinvestasi[21].mdu_persen1'].value="100";
					document.frmParam.elements['investasiutama.daftarinvestasi[21].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[21].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
					document.frmParam.elements['investasiutama.daftarinvestasi[22].mdu_persen1'].value="";
					document.frmParam.elements['investasiutama.daftarinvestasi[22].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[22].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				}else{
					document.frmParam.elements['investasiutama.daftarinvestasi[21].mdu_persen1'].value="";
					document.frmParam.elements['investasiutama.daftarinvestasi[21].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[21].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				
					document.frmParam.elements['investasiutama.daftarinvestasi[22].mdu_persen1'].value="100";
					document.frmParam.elements['investasiutama.daftarinvestasi[22].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[22].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				}
				
				var lsbs = document.frmParam.elements['datausulan.lsbs_id'].value;
				var lsdbs = document.frmParam.elements['datausulan.lsdbs_number'].value;
				
				if(lsbs == 164 && lsdbs == 2){ 
					document.frmParam.elements['powersave.msl_bp_rate'].readOnly=true;
					document.frmParam.elements['powersave.msl_bp_rate'].style.backgroundColor ='#D4D4D4';
				}else {
					document.frmParam.elements['powersave.msl_bp_rate'].readOnly=false;
				}
				
				if( lsbs==177||lsbs==207||lsbs==186){
					document.frmParam.elements['powersave.mps_jangka_inv'].disabled=true;
					document.frmParam.elements['powersave.mps_jangka_inv'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['powersave.msl_bp_rate'].readOnly=true;
					document.frmParam.elements['powersave.msl_bp_rate'].style.backgroundColor ='#D4D4D4';
				}
				
				
				
				document.frmParam.elements['investasiutama.total_persen'].value="100";
		
				break;		
				
			case 12: //investimax
			
				var lsbs = document.frmParam.elements['datausulan.lsbs_id'].value;
				var lsdbs = document.frmParam.elements['datausulan.lsdbs_number'].value;
				
				if(lsbs == 165 && lsdbs == 1){ //investimax 1
					for(q = 0; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
					document.frmParam.elements['investasiutama.daftarinvestasi[20].mdu_persen1'].value="100";
					document.frmParam.elements['investasiutama.daftarinvestasi[20].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[20].mdu_persen1'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['investasiutama.total_persen'].value="100";					
				}else{
					for(q = 0; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
					document.frmParam.elements['investasiutama.daftarinvestasi[28].mdu_persen1'].value="100";
					document.frmParam.elements['investasiutama.daftarinvestasi[28].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[28].mdu_persen1'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['investasiutama.total_persen'].value="100";					
				}	
				break;
			
			case 13: //amanah link syariah
				for(q = 0; q <= 5; q++) setInvestasiReadOnly(q, true);
				setInvestasiReadOnly(6, false);
				setInvestasiReadOnly(7, false);
				for(q = 8; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				break;			

			case 14: //muamalat - mabrur (153-5)
				for(q = 0; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].value=100; //set cepek untuk ekalink dinamic syariah fund	
					document.frmParam.elements['investasiutama.total_persen'].value="100";					
				break;
			case 15: //Stable link Syariah
				for(q = 0; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				document.frmParam.elements['investasiutama.total_persen'].value="100";

				if (document.frmParam.elements['datausulan.kurs_premi'].value == '01'){
					document.frmParam.elements['investasiutama.daftarinvestasi[29].mdu_persen1'].value="100";
					document.frmParam.elements['investasiutama.daftarinvestasi[29].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[29].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
					document.frmParam.elements['investasiutama.daftarinvestasi[30].mdu_persen1'].value="";
					document.frmParam.elements['investasiutama.daftarinvestasi[30].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[30].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				}else{
					document.frmParam.elements['investasiutama.daftarinvestasi[29].mdu_persen1'].value="";
					document.frmParam.elements['investasiutama.daftarinvestasi[29].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[29].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				
					document.frmParam.elements['investasiutama.daftarinvestasi[30].mdu_persen1'].value="100";
					document.frmParam.elements['investasiutama.daftarinvestasi[30].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[30].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				}
				document.frmParam.elements['investasiutama.total_persen'].value="100";
		
				break;		
		}
	
		
		<c:if test="${not empty daftarStable}">
		hideSelect('hidden'); 
		popup('popUpDiv');
		</c:if>
		
	}
	
	function jns_rate()
	{
		if ((document.frmParam.elements['datausulan.lsbs_id'].value=='158') &&( (document.frmParam.elements['datausulan.lsdbs_number'].value=='5') || (document.frmParam.elements['datausulan.lsdbs_number'].value=='8') || (document.frmParam.elements['datausulan.lsdbs_number'].value=='9') ))
		{
			if ((document.frmParam.elements['powersave.mps_jangka_inv'].value=='12') || (document.frmParam.elements['powersave.mps_jangka_inv'].value=='36'))
			{
				if ((document.frmParam.elements['powersave.mps_roll_over'].value=='')  || (document.frmParam.elements['powersave.mps_roll_over'].value=='1')) 
				{
					document.frmParam.elements['powersave.mps_roll_over'].value='2';
				}
			}
		}
				
		if ((document.frmParam.elements['datausulan.kode_flag'].value=='1') || (document.frmParam.elements['datausulan.kode_flag'].value=='11'))
		{	
			if (document.frmParam.elements['powersave.mps_jenis_plan'].value == "0")
			{
				document.frmParam.elements['powersave.mps_rate'].readOnly=true;
				document.frmParam.elements['powersave.mps_rate'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['powersave.mpr_note'].readOnly=true;
				document.frmParam.elements['powersave.mpr_note'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['powersave.mpr_note'].value = "";
				document.frmParam.elements['powersave.fee_based_income'].readOnly=true;
				document.frmParam.elements['powersave.fee_based_income'].style.backgroundColor ='#D4D4D4';
			}else{
				document.frmParam.elements['powersave.mps_rate'].readOnly=false;
				document.frmParam.elements['powersave.mps_rate'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['powersave.mpr_note'].readOnly=false;
				document.frmParam.elements['powersave.mpr_note'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['powersave.fee_based_income'].readOnly=false;
				document.frmParam.elements['powersave.fee_based_income'].style.backgroundColor ='#FFFFFF';
			}
		}
		
	}

 function sts(hasil)
 {
	document.frmParam.elements['datausulan.mste_flag_investasi'].value = hasil;
 }	
	
	function tutup()
	{
		if ((document.frmParam.elements['datausulan.kode_flag'].value=='1') || (document.frmParam.elements['datausulan.kode_flag'].value=='11'))
		{
			if (document.frmParam.elements['powersave.mps_roll_over'].value == '1')
			{

			}else{
				document.frmParam.elements['rekening_client.mrc_no_ac'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac'].style.backgroundColor ='#FFFFFF';
				
				/*Tambahan dari Bertho untuk split rekening*/
			document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[20]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[20]'].style.backgroundColor ='#FFFFFF';
			
			/*End of code*/
				
				document.frmParam.elements['rekening_client.mrc_nama'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_nama'].style.backgroundColor ='#FFFFFF';
				if (document.frmParam.elements['rekening_client.mrc_nama'].value =="")
				{
					document.frmParam.elements['rekening_client.mrc_nama'].value =  document.frmParam.elements['pemegang.mcl_first'].value;;
				}				
				
				document.frmParam.elements['rekening_client.lsbp_id'].readOnly = false;
				document.frmParam.elements['rekening_client.lsbp_id'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_kota'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_kota'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.tgl_surat'].readOnly = false;
				document.frmParam.elements['rekening_client.tgl_surat'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.notes'].readOnly = false;
				document.frmParam.elements['rekening_client.notes'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_kuasa'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_kuasa'].style.backgroundColor ='#FFFFFF';
				document.frmParam.mrc_kuasa1.disabled = false;			
				document.frmParam.elements['rekening_client.mrc_cabang'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_cabang'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_jenis'].disabled = false;
				document.frmParam.elements['rekening_client.mrc_jenis'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_kurs'].disabled = false;
				document.frmParam.elements['rekening_client.mrc_kurs'].style.backgroundColor ='#FFFFFF';
				document.frmParam.caribank1.readOnly = false;
				document.frmParam.caribank1.style.backgroundColor ='#FFFFFF';
				document.frmParam.btncari1.disabled = false;
			}
		}

		var flag_account = document.frmParam.elements['datausulan.flag_account'].value;
		var flag_bao = document.frmParam.elements['datausulan.flag_bao'].value;
		
		if ( (flag_bao ==1) && ((flag_account==2)||(flag_account==3)) )
		{
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].disabled=false;
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].style.backgroundColor ='#FFFFFF';				
		}else{
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].value='0';	
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].disabled=true;
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].style.backgroundColor ='#D4D4D4';		
		}

		if ((flag_account ==2)||(flag_account ==3))
		{
			document.frmParam.elements['rekening_client.mrc_no_ac'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac'].style.backgroundColor ='#FFFFFF';
			/*Tambahan dari Bertho untuk split rekening*/
			document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[20]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[20]'].style.backgroundColor ='#FFFFFF';
			
			<c:if test="${not empty cmd.rekening_client.lsbp_id}">
					ajaxPjngRek('${cmd.rekening_client.lsbp_id}', 1,'rekening_client.mrc_no_ac_split');
			</c:if>
		
		
			/*End of code*/
			document.frmParam.elements['rekening_client.mrc_nama'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_nama'].style.backgroundColor ='#FFFFFF';
			if (document.frmParam.elements['rekening_client.mrc_nama'].value =="")
			{
				document.frmParam.elements['rekening_client.mrc_nama'].value = document.frmParam.elements['pemegang.mcl_first'].value;
			}
			
			document.frmParam.elements['rekening_client.lsbp_id'].readOnly = false;
			document.frmParam.elements['rekening_client.lsbp_id'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_kota'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_kota'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.tgl_surat'].readOnly = false;
			document.frmParam.elements['rekening_client.tgl_surat'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.notes'].readOnly = false;
			document.frmParam.elements['rekening_client.notes'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_kuasa'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_kuasa'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_cabang'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_cabang'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_jenis'].disabled = false;
			document.frmParam.elements['rekening_client.mrc_jenis'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_kurs'].disabled = false;
			document.frmParam.elements['rekening_client.mrc_kurs'].style.backgroundColor ='#FFFFFF';
			document.frmParam.caribank1.readOnly = false;
			document.frmParam.caribank1.style.backgroundColor ='#FFFFFF';
			document.frmParam.btncari1.disabled = false;
		}else{
			document.frmParam.elements['rekening_client.mrc_no_ac'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac'].value = '';
			
			/*Tambahan dari Bertho untuk split rekening*/
			document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[20]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[20]'].value = '';
			
			document.frmParam.elements['rekening_client.mrc_nama'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_nama'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_nama'].value='';
			document.frmParam.elements['rekening_client.lsbp_id'].disabled = true;
			document.frmParam.elements['rekening_client.lsbp_id'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.lsbp_id'].value='NULL';
			document.frmParam.elements['rekening_client.mrc_kota'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_kota'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_kota'].value='';
			document.frmParam.elements['rekening_client.tgl_surat'].readOnly = true;
			document.frmParam.elements['rekening_client.tgl_surat'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.tgl_surat'].value='';			
			document.frmParam.elements['rekening_client.notes'].readOnly = true;
			document.frmParam.elements['rekening_client.notes'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.notes'].value='';
			document.frmParam.mrc_kuasa1.disabled = true;
			document.frmParam.mrc_kuasa1.checked = false;
			document.frmParam.elements['rekening_client.mrc_kuasa'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_kuasa'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_kuasa'].value='0';
			document.frmParam.elements['rekening_client.mrc_cabang'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_cabang'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_cabang'].value='';
			document.frmParam.elements['rekening_client.mrc_jenis'].disabled = true;
			document.frmParam.elements['rekening_client.mrc_jenis'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_jenis'].value='0';
			document.frmParam.elements['rekening_client.mrc_kurs'].disabled = true;
			document.frmParam.elements['rekening_client.mrc_kurs'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_kurs'].value='';			
			document.frmParam.caribank1.readOnly = true;
			document.frmParam.caribank1.style.backgroundColor ='#D4D4D4';
			document.frmParam.caribank1.value='';
			document.frmParam.btncari1.disabled = true;

		}

		if ((document.frmParam.elements['datausulan.mste_flag_cc'].value=='1') || (document.frmParam.elements['datausulan.mste_flag_cc'].value=='2') || (document.frmParam.elements['datausulan.flag_account'].value== 3))
		{
				document.frmParam.elements['account_recur.mar_holder'].readOnly = false;
				document.frmParam.elements['account_recur.mar_holder'].style.backgroundColor ='#FFFFFF';
				if (document.frmParam.elements['account_recur.mar_holder'].value =='')
				{
					document.frmParam.elements['account_recur.mar_holder'].value = 	document.frmParam.elements['pemegang.mcl_first'].value;
				}				
				
				document.frmParam.elements['account_recur.lbn_id'].disabled = false;
				document.frmParam.elements['account_recur.lbn_id'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.flag_jn_tabungan'].disabled = false;
				document.frmParam.elements['account_recur.flag_jn_tabungan'].style.backgroundColor ='#FFFFFF';
				
				document.frmParam.elements['account_recur.mar_acc_no'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no'].style.backgroundColor ='#FFFFFF';
				/*tambahan setting dari Bertho untuk split mar_acc_no*/
				document.frmParam.elements['account_recur.mar_acc_no_split[0]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[0]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[1]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[1]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[2]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[2]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[3]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[3]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[4]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[4]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[5]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[5]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[6]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[6]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[7]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[7]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[8]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[8]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[9]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[9]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[10]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[10]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[11]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[11]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[12]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[12]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[13]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[13]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[14]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[14]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[15]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[15]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[16]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[16]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[17]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[17]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[18]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[18]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[19]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[19]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['account_recur.mar_acc_no_split[20]'].readOnly = false;
				document.frmParam.elements['account_recur.mar_acc_no_split[20]'].style.backgroundColor ='#FFFFFF';
			
				<c:if test="${not empty cmd.account_recur.lbn_id}">
						ajaxPjngRek('${cmd.account_recur.lbn_id}', 2,'account_recur.mar_acc_no_split');							
				</c:if>
				
								/*end of setting*/
				document.frmParam.caribank2.readOnly = false;
				document.frmParam.caribank2.style.backgroundColor ='#FFFFFF';
				document.frmParam.btncari2.disabled = false;
		}else{
				document.frmParam.elements['account_recur.mar_holder'].readOnly = true;
				document.frmParam.elements['account_recur.mar_holder'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_holder'].value='';			
				document.frmParam.elements['account_recur.lbn_id'].disabled = true;
				document.frmParam.elements['account_recur.lbn_id'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.lbn_id'].value='NULL';
				document.frmParam.elements['account_recur.flag_jn_tabungan'].disabled = true;
				document.frmParam.elements['account_recur.flag_jn_tabungan'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no'].style.backgroundColor ='#D4D4D4';
				/*tambahan setting dari Bertho untuk split mar_acc_no*/
				
				document.frmParam.elements['account_recur.mar_acc_no_split[0]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[0]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[1]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[1]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[2]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[2]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[3]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[3]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[4]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[4]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[5]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[5]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[6]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[6]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[7]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[7]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[8]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[8]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[9]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[9]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[10]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[10]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[11]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[11]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[12]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[12]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[13]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[13]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[14]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[14]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[15]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[15]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[16]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[16]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[17]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[17]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[18]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[18]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[19]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[19]'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['account_recur.mar_acc_no_split[20]'].readOnly = true;
				document.frmParam.elements['account_recur.mar_acc_no_split[20]'].style.backgroundColor ='#D4D4D4';
				
								/*end of setting*/
				document.frmParam.elements['account_recur.mar_acc_no'].value='';
				document.frmParam.caribank2.readOnly = true;
				document.frmParam.caribank2.style.backgroundColor ='#D4D4D4';
				document.frmParam.caribank2.value='';
				document.frmParam.btncari2.disabled = true;
						
		}
	}
				
	function addRowDOM1 (tableID, jml) {
		flag_add1=1
   		var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
		var tmp = parseInt(document.getElementById("tableProd").rows.length);
		var oRow = oTable.insertRow(oTable.rows.length);
        var oCells = oRow.cells;
		var i=tmp-1;
		var idx = jmlpenerima + 1;
		//if (document.all) {
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td >"+idx+"</td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td ><input type=text name='benef.msaw_first"+idx+"' value='' size=30></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td><input type=text name='tgllhr"+idx+"' value='' size=3>/<input type=text name='blnhr"+idx+"' value='' size=3>/<input type=text name='thnhr"+idx+"' value='' size=5><input type=hidden name='msaw_birth"+idx+"' value='' size=5></td>";         
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML =  "<td ><select name='benef.msaw_sex"+idx+"'>"+varOptSex+"</select></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML =  "<td ><select name='benef.lsre_id"+idx+"'>"+varOptionhub+"</select></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML ="<td><input type=text name='benef.msaw_persen"+idx+"' value='' size=5 ></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML ="<td ><input type=checkbox name=cek"+idx+" id=ck"+idx+"' class=\"noBorder\" ></td></tr>";
			var cell = oRow.insertCell(oCells.length);			
			cell.innerHTML = '';
			jmlpenerima = idx;
		//}
 	}
 
 
	function next()
	{
		eval( " document.frmParam.elements['pemegang.indeks_halaman'].value = ((document.frmParam.hal.value)+1);");
		//alert(jmlpenerima);
		document.frmParam.jmlpenerima.value=jmlpenerima;
		//alert(document.frmParam.jmlpenerima.value);
	} 
	function prev()
	{
		document.frmParam.jmlpenerima.value=jmlpenerima;
	}	

	function cancel1()
	{		
		var idx = jmlpenerima;
	//(idx)!=null &&
		if  ( (idx)!="")
		{
			if (idx >0)
				flag_add1=1;
		}
		if(flag_add1==1)
		{
			deleteRowDOM1('tableProd', '1');
		}
	}		

function deleteRowDOM1 (tableID,rowCount)
 { 
    var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
    var row=parseInt(document.getElementById("tableProd").rows.length);
	var flag_row=0;
	var jumlah_cek=0;
	var idx = jmlpenerima;

	if(row>0)
	{
		flag_row=0;
		//alert(row);
		for (var i=1;i<((parseInt(row)));i++)
		{

			if (eval("document.frmParam.elements['cek"+i+"'].checked"))
			{
				idx=idx-1;

				for (var k =i ; k<((parseInt(row))-1);k++)
				{

					eval(" document.frmParam.elements['benef.msaw_first"+k+"'].value =document.frmParam.elements['benef.msaw_first"+(k+1)+"'].value;");   
					eval(" document.frmParam.elements['tgllhr"+k+"'].value =document.frmParam.elements['tgllhr"+(k+1)+"'].value;"); 
					eval(" document.frmParam.elements['blnhr"+k+"'].value = document.frmParam.elements['blnhr"+(k+1)+"'].value;");			      
					eval(" document.frmParam.elements['thnhr"+k+"'].value = document.frmParam.elements['thnhr"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['msaw_birth"+k+"'].value = document.frmParam.elements['msaw_birth"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['benef.lsre_id"+k+"'].value = document.frmParam.elements['benef.lsre_id"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['benef.msaw_persen"+k+"'].value = document.frmParam.elements['benef.msaw_persen"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['cek"+k+"'].checked = document.frmParam.elements['cek"+(k+1)+"'].checked;");
				}
				oTable.deleteRow(parseInt(document.getElementById("tableProd").rows.length)-1);
				row=row-1;
				//alert(jmlpenerima);
				jmlpenerima = idx;
				//alert(jmlpenerima);
				i=i-1;
			}
		}


		row=parseInt(document.getElementById("tableProd").rows.length);
			if(row==1)	
				flag_add=0;
		
	}
}

	if (!document.layers&&!document.all&&!document.getElementById)
		event="test"
		
	function showtip(current,e,text){
	
		if (document.all||document.getElementById){
			thetitle=text.split('<br>')
			if (thetitle.length>1){
				thetitles=''
				
			for (i=0;i<tdetitle.length;i++)
				thetitles+=thetitle[i]
				
			current.title=thetitles
		}
		else
			current.title=text
	}
	
	else if (document.layers){
				document.tooltip.document.write('<layer bgColor="white" style="border:1px solid black;font-size:13px;">'+text+'</layer>')
				document.tooltip.document.close()
				document.tooltip.left=e.pageX+5
				document.tooltip.top=e.pageY+5
				document.tooltip.visibility="show"
			}
	}
	
	function hidetip(){
		if (document.layers)
			document.tooltip.visibility="hidden"
	}
	
	function dofo(index, flag) {
		
		if(flag==1){
			var a="rekening_client.mrc_no_ac_split["+(index+1)+"]";		
			document.frmParam.elements[a].focus();
		}else{
			var a="account_recur.mar_acc_no_split["+(index+1)+"]";		
			document.frmParam.elements[a].focus();
		}
			
	}

	/*
	function hideSelect(disp){
		//FIXME
		document.frmParam.elements['datausulan.lsbs_id'].style.visibility = disp;
		document.frmParam.elements['datausulan.plan'].style.visibility = disp;
		document.frmParam.elements['datausulan.tipeproduk'].style.visibility = disp;
		document.frmParam.elements['datausulan.kombinasi'].style.visibility = disp;
		document.frmParam.elements['datausulan.mste_flag_cc'].style.visibility = disp;
		document.frmParam.elements['pemegang.mste_pct_dplk'].style.visibility = disp;
		document.frmParam.elements['datausulan.lku_id'].style.visibility = disp;
		document.frmParam.elements['datausulan.kurs_p'].style.visibility = disp;
		document.frmParam.elements['datausulan.lscb_id'].style.visibility = disp;
	}			
	*/
	
		

// -->
</script>



<body bgcolor="ffffff" onLoad="Body_onload();">

<c:if test="${not empty cmd.pesan}">
	<script>alert('${cmd.pesan}');</script>
</c:if>


<XML ID=xmlData></XML> 

 <form name="frmParam" method="post" >
<table border="0" width="100%" height="677" cellspacing="1" cellpadding="1" class="">
  <tr> 
    <td width="67%" height="35" bgcolor="#FDFDFD" valign="bottom"> 
 			<input type="submit" name="_target0" value=" " onclick=" prev()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/pp1.jpg);"
				accesskey="1" onmouseover="return overlib('Alt-1', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target1" value=" " onclick=" prev()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ttg1.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target2" value=" " onclick=" prev()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ddu1.jpg);"
				accesskey="3" onmouseover="return overlib('Alt-3', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target3" value=" " onclick=" prev()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/inv2.jpg);"
				accesskey="4" onmouseover="return overlib('Alt-4', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target4" value=" " onclick=" prev()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ag1.jpg);"
				accesskey="5" onmouseover="return overlib('Alt-5', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target5" value=" " onclick=" prev()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/kf1.jpg);"
				accesskey="6" onmouseover="return overlib('Alt-6', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" name="dummy2" value=" " onclick=" prev()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/sb1.jpg);">		
 	
    </td>
  </tr>
  <tr><td bgcolor="#DDDDDD">
  
  </td></tr>
  <tr> 
    <td width="67%" rowspan="5" align="left" valign="top"> 
<spring:bind path="cmd.*">
	<c:if test="${not empty status.errorMessages}">
	<div id="error">
	ERROR:<br>
	<c:forEach var="error" items="${status.errorMessages}">
	- <c:out value="${error}" escapeXml="false" />
	<br />
	</c:forEach></div>
	</c:if>									
</spring:bind>     
        <table class="form_input" style="font-size: 11px;">

          <tr> 
            <td  align="center" colspan="4"> 
              <div align="center">
                <input type="submit" name="_target${halaman-1}" value="Prev &laquo;" onClick="prev()" 
					onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
                <input type="submit" name="_target${halaman+1}" value="Next &raquo;" onClick="next()" 
					onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
                <input type="hidden" name="_page" value="${halaman}">
              </div>
            </td>
          </tr>
        </table>
        <table  class="form_input" style="font-size: 11px;width: 100%;" >
          <tr>  
            <th class="subtitle"  height="27" colspan="14" >DATA PRODUK INVESTASI
                <spring:bind path="cmd.datausulan.flag_bao"> 
                <input type="hidden" id="${status.expression}" name="${status.expression}"
						value="${status.value }" >
                </spring:bind> <spring:bind path="cmd.datausulan.kode_flag"> 
                <input type="hidden" id="${status.expression}"  name="${status.expression}"
						value="${status.value }"  >
                </spring:bind> <spring:bind path="cmd.datausulan.isBungaSimponi"> 
                <input type="hidden" id="${status.expression}" name="${status.expression}"
						value="${status.value }"  >
                </spring:bind> <spring:bind path="cmd.datausulan.isBonusTahapan"> 
                <input type="hidden" id="${status.expression}" name="${status.expression}"
						value="${status.value }"  >
                </spring:bind> <spring:bind path="cmd.datausulan.flag_account"> 
                <input type="hidden" id="${status.expression}" name="${status.expression}"
						value="${status.value }"  >
                </spring:bind> 
                <spring:bind path="cmd.datausulan.lsbs_id"> 
                <input type="hidden" id="${status.expression} name="${status.expression}"
						value="${status.value }"  >
                </spring:bind> 
                <spring:bind path="cmd.datausulan.lsdbs_number"> 
                <input type="hidden" id="${status.expression} name="${status.expression}"
						value="${status.value }"  >
                </spring:bind>
                <spring:bind path="cmd.pemegang.mcl_first"> 
                <input type="hidden" id="${status.expression}" name="${status.expression}"
						value="${status.value }"  size="30" >
                </spring:bind> 
                <spring:bind path="cmd.datausulan.kurs_premi"> 
                <input type="hidden" id="${status.expression}" name="${status.expression}"
						value="${status.value }"  size="30" >
                </spring:bind>
                <select style="visibility: hidden;" name="rekening_client.mrc_jn_nasabah" 
				 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
	                <c:forEach var="nsbh" items="${select_jenis_nasabah}"> <option 
						<c:if test="${cmd.rekening_client.mrc_jn_nasabah eq nsbh.ID}"> SELECTED </c:if>
						value="${nsbh.ID}">${nsbh.JENIS}</option> </c:forEach> </select> 
                <input type="hidden" id="alertEkaSehat" name="alertEkaSehat" value="${cmd.datausulan.alertEkaSehat}"></th>
          </tr>
          <tr  > 
            <td  height="20 " colspan="14" style="color: #CC3300;font-weight: bold">Detail Investasi Khusus Produk Unit Link</td>
          </tr>
          <tr> 
            <th class="sub1" height="20" width="2%">&nbsp;</th>
            <th class="sub1" colspan="13" height="20">Perhitungan Premi ( sesuai dengan cara pembayaran )</th>
          </tr>
          <tr  > 
            <td width="2%">&nbsp;</td>
            <td colspan="2" > <p align="left">Premi Pokok</td>
            <td > <input type="text" id="mspr_premium" name="mspr_premium"
						value=<fmt:formatNumber type="number" value="${cmd.datausulan.mspr_premium}"/> 
              size="30" style='background-color :#D4D4D4' readOnly> </td>
            <td colspan="11" >&nbsp;</td>
          </tr>
          <tr  > 
            <td width="2%">&nbsp;</td>
            <td colspan="2" >Premi Top Up Berkala</td>
            <td > <spring:bind path="cmd.investasiutama.daftartopup.premi_berkala"> 
              <input type="text" id="${status.expression}" name="${status.expression}"
						value="${status.value }"  size="30" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
             </spring:bind><font color="#CC3300">*</font></td>
            <td width="12%">Jenis Top Up</td>
            <td width="32%" colspan="10"> <select name="investasiutama.daftartopup.pil_berkala" 
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <c:forEach var="jnstopup" items="${select_jns_top_up}"> 
             	  <c:if test="${jnstopup.ID ne 2 }">
					 <option <c:if test="${cmd.investasiutama.daftartopup.pil_berkala eq jnstopup.ID}"> SELECTED </c:if> value="${jnstopup.ID}">${jnstopup.JENIS}</option> 
                  </c:if>
                </c:forEach> </select>
              <font color="#CC3300">*</font> </td>
          </tr>
       <tr  > 
            <td width="2%">&nbsp;</td>
            <td colspan="2" >Premi Top Up Tunggal</td>
            <td > <spring:bind path="cmd.investasiutama.daftartopup.premi_tunggal"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
             </spring:bind><font color="#CC3300">*</font></td>
            <td width="12%">Jenis Top Up</td>
            <td width="32%" colspan="10"> <select name="investasiutama.daftartopup.pil_tunggal" 
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <c:forEach var="jnstopup" items="${select_jns_top_up}"> 
                 <c:if test="${jnstopup.ID ne 1 }">
				   	<option <c:if test="${cmd.investasiutama.daftartopup.pil_tunggal eq jnstopup.ID}"> SELECTED </c:if> value="${jnstopup.ID}">${jnstopup.JENIS}</option> 
                 </c:if>
                </c:forEach> </select>
              <font color="#CC3300">*</font> </td>
          </tr>
          <tr  > 
            <td width="2%">&nbsp;</td>
            <td colspan="2" >Premi Tambahan</td>
            <td > <spring:bind path="cmd.datausulan.total_premi_rider"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" style='background-color :#D4D4D4'readOnly>
               </spring:bind> 
            </td>
            <td colspan="11" >&nbsp;</td>
          </tr>
          <tr  > 
            <td width="2%">&nbsp;</td>
            <td  colspan="3"> <hr> </td>
            <td colspan="11" >&nbsp;</td>
          </tr>
          <tr  > 
            <td width="2%">&nbsp;</td>
            <td colspan="2" >Jumlah</td>
            <td > <spring:bind path="cmd.investasiutama.total_premi_sementara"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" style='background-color :#D4D4D4'readOnly>
               </spring:bind> 
            </td>
            <td colspan="11" >&nbsp;</td>
          </tr>
		  <tr> 
            <th class="sub1" height="20" width="2%">&nbsp;</th>
            <c:choose>
            <c:when test="${cmd.datausulan.lsbs_id eq '186' and cmd.datausulan.lsdbs_number eq '3'}">
            	<th class="sub1" colspan="14" height="20">No. Rekening Sumber Dana</th>
            </c:when> <c:otherwise> 
	            <th class="sub1" colspan="14" height="20">No. Rekening Pemegang Polis Yang Digunakan untuk Transaksi (KHUSUS PRODUK INVESTASI)</th>
             </c:otherwise> </c:choose>
            
          </tr>
          <tr  > 
            <td  ></td>
            <td  > Bentuk Pembayaran Premi </td>
            <td  ></td>
            <td colspan="12" > <spring:bind path="cmd.datausulan.mste_flag_cc"> 
              <select id="${status.expression}" name="${status.expression }"  onchange="tutup();"
			  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <option value="" /> <c:forEach var="auto" items="${select_autodebet}"> <option value="${auto.ID}"
						   
                <c:if test="${cmd.datausulan.mste_flag_cc eq auto.ID}">selected</c:if> >${auto.AUTODEBET}</option> 
                </c:forEach> 
              </select>
              <font color="#CC3300">*</font>  </spring:bind> </td>
          </tr>
           <tr>          
          		<td></td>
          		<td width="15%"  height="20">Cari Bank</td>
	             <td></td>
	            <td  height="20"> <input type="text" name="caribank1" onkeypress="if(event.keyCode==13){ document.frmParam.btncari1.click(); return false;}"> 
	              <input type="button" name="btncari1" value="Cari" onclick="ajaxSelectWithParam1a(document.frmParam.caribank1.value,'select_bank1','bank1','rekening_client.lsbp_id','', 'BANK_ID', 'BANK_NAMA', 'ajaxPjngRek(this.value, 1,\'rekening_client.mrc_no_ac_split\')','Silahkan pilih BANK','3');"> 
	            </td>
             
          
            <td colspan="11"></td>
          </tr>
          <tr>          
          	<td></td>
          	 <td width="15%" height="20">Bank</td>
               <td></td>
            <td  height="20"> <div id="bank1"
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> 
                <select name="rekening_client.lsbp_id" >
                  <option value="${cmd.rekening_client.lsbp_id}">${cmd.rekening_client.lsbp_nama}</option>
                </select><font color="#CC3300">*</font>
                </div>
               
                </td>            
          
             <td width="15%" >Cabang</td>
            <td  height="20" colspan="10"><spring:bind path="cmd.rekening_client.mrc_cabang"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" maxlength="20" 
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
               </spring:bind></td>
          </tr>
           <tr  > 
            <td width="2%" >&nbsp;</td>
            <td colspan="2">Jenis Tabungan</td>
            <td width="24%"> <select name="rekening_client.mrc_jenis" 
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <c:forEach var="tab" items="${select_jenis_tabungan}"> <option 
							<c:if test="${cmd.rekening_client.mrc_jenis eq tab.ID}"> SELECTED </c:if>
							value="${tab.ID}">${tab.AUTODEBET}</option> </c:forEach> 
              </select>
              <font color="#CC3300">*</font> </td>
               <td width="15%" height="20">Kota</td>
            <td  height="20" colspan="10">     <div>
								<spring:bind path="cmd.rekening_client.mrc_kota"> 
					              <input type="text" name="${status.expression}"
											value="${status.value }"  size="30" maxlength="20" 
								 <c:if test="${ not empty status.errorMessage}">
											style='background-color :#FFE1FD'
										</c:if>>
					              </spring:bind>
				</div> </td>
          </tr>
<!--          <tr style="visibility: hidden;padding: inherit;" > -->
<!--            <td width="2%" >&nbsp;</td>-->
<!--            <td colspan="2">Jenis Nasabah</td>-->
<!--            <td> <select name="rekening_client.mrc_jn_nasabah" -->
<!--			 <c:if test="${ not empty status.errorMessage}">-->
<!--						style='background-color :#FFE1FD'-->
<!--					</c:if>>-->
<!--                <c:forEach var="nsbh" items="${select_jenis_nasabah}"> <option -->
<!--					<c:if test="${cmd.rekening_client.mrc_jn_nasabah eq nsbh.ID}"> SELECTED </c:if>-->
<!--					value="${nsbh.ID}">${nsbh.JENIS}</option> </c:forEach> </select> -->
<!--            </td>-->
<!--            -->
<!--            <td width="15%" ></td>-->
<!--            <td  height="20" colspan="10">&nbsp;</td>-->
<!--          </tr>-->
          <tr  > 
            <td width="2%"  height="20">&nbsp;</td>
            <td  height="20" colspan="2" >No. Rekening</td>
            <td  height="20" width="24%" colspan="12"><spring:bind path="cmd.rekening_client.mrc_no_ac"> 
              	<input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30" maxlength="20"
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>

					<input type="text" name="rekening_client.mrc_no_ac_split[0]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(0, 1);" value="${cmd.rekening_client.mrc_no_ac_split[0]}" 
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="rekening_client.mrc_no_ac_split[1]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(1, 1);" value="${cmd.rekening_client.mrc_no_ac_split[1]}" 
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="rekening_client.mrc_no_ac_split[2]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(2, 1);" value="${cmd.rekening_client.mrc_no_ac_split[2]}" 
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="rekening_client.mrc_no_ac_split[3]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(3, 1);" value="${cmd.rekening_client.mrc_no_ac_split[3]}" 
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="rekening_client.mrc_no_ac_split[4]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(4, 1);" value="${cmd.rekening_client.mrc_no_ac_split[4]}" 
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="rekening_client.mrc_no_ac_split[5]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(5, 1);" value="${cmd.rekening_client.mrc_no_ac_split[5]}" 
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="rekening_client.mrc_no_ac_split[6]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(6, 1);" value="${cmd.rekening_client.mrc_no_ac_split[6]}" 
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="rekening_client.mrc_no_ac_split[7]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(7, 1);" value="${cmd.rekening_client.mrc_no_ac_split[7]}" 
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="rekening_client.mrc_no_ac_split[8]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(8, 1);" value="${cmd.rekening_client.mrc_no_ac_split[8]}" 
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="rekening_client.mrc_no_ac_split[9]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(9, 1);" value="${cmd.rekening_client.mrc_no_ac_split[9]}" 
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="rekening_client.mrc_no_ac_split[10]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(10, 1);" value="${cmd.rekening_client.mrc_no_ac_split[10]}" 
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="rekening_client.mrc_no_ac_split[11]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(11, 1);" value="${cmd.rekening_client.mrc_no_ac_split[11]}" 
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="rekening_client.mrc_no_ac_split[12]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(12, 1);" value="${cmd.rekening_client.mrc_no_ac_split[12]}" 
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="rekening_client.mrc_no_ac_split[13]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(13, 1);" value="${cmd.rekening_client.mrc_no_ac_split[13]}" 
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="rekening_client.mrc_no_ac_split[14]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(14, 1);" value="${cmd.rekening_client.mrc_no_ac_split[14]}" 
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="rekening_client.mrc_no_ac_split[15]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(15, 1);" value="${cmd.rekening_client.mrc_no_ac_split[15]}" 
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="rekening_client.mrc_no_ac_split[16]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(16, 1);" value="${cmd.rekening_client.mrc_no_ac_split[16]}" 
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="rekening_client.mrc_no_ac_split[17]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(17, 1);" value="${cmd.rekening_client.mrc_no_ac_split[17]}" 
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="rekening_client.mrc_no_ac_split[18]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(18, 1);" value="${cmd.rekening_client.mrc_no_ac_split[18]}" 
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="rekening_client.mrc_no_ac_split[19]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(19, 1);" value="${cmd.rekening_client.mrc_no_ac_split[19]}" 
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="rekening_client.mrc_no_ac_split[20]" size="1"  maxlength="1" onfocus="select();" value="${cmd.rekening_client.mrc_no_ac_split[20]}" 
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">

					
				<font color="#CC3300">*</font>
              </spring:bind> 
              <select name="rekening_client.mrc_kurs" 
			   <c:if test="${ not empty status.errorMessage}" >
						style='background-color :#FFE1FD'
					</c:if>>
                <c:forEach var="kurs" items="${select_kurs}"> <option 
							<c:if test="${cmd.rekening_client.mrc_kurs eq kurs.ID}"> SELECTED </c:if>
							value="${kurs.ID}">${kurs.SYMBOL}</option> </c:forEach> 
              </select>
              <font color="#CC3300">*</font> </td>
                  
          </tr>       
          <tr  > 
            <td width="2%"  height="20">&nbsp;</td>
            <td  height="20" colspan="2" >Atas Nama</td>
            <td  height="20" width="24%"><spring:bind path="cmd.rekening_client.mrc_nama"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="35" maxlength="50" 
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> <font color="#CC3300">*</font>
              </spring:bind>
              <%-- <spring:bind path="cmd.pemegang.confNm"> 
              	<input type="hidden" id="${status.expression}" name="${status.expression}" value="${status.value }" onkeypress="alert(this.id)">
              </spring:bind>
              <c:if test='${cmd.pemegang.confNm eq \"tidak cocok\"}'>
	              	<script type="text/javascript">
	              		if(confirm('Nama Pemilik Rekening tidak sama dengan nama pemegang. Lanjutkan ?')) {
	              			document.getElementById('pemegang.confNm').value = "ok";
	              			frmParam.submit();
	              		}
	              	</script>
              </c:if> --%>
            </td>
              <td width="15%"  height="20"></td>
            <td  height="20" colspan="10"> 
            	&nbsp;
            </td>
          </tr>
         
          <tr  > 
            <td >&nbsp;</td>
            <td  valign="top">Memberi Kuasa
              <input type="checkbox" name="mrc_kuasa1" class="noBorder" 
						value="${cmd.rekening_client.mrc_kuasa}"  size="30" onClick="kuasa_onClick();" 
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
              <spring:bind path="cmd.rekening_client.mrc_kuasa"> 
              <input type="hidden" name="${status.expression}"
						value="${cmd.rekening_client.mrc_kuasa}"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind></td>
            <td  valign="top">Keterangan</td>
            <td> <spring:bind path="cmd.rekening_client.notes"> 
              <textarea cols="40" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>${status.value }</textarea>
               </spring:bind> </td>
            <td valign="top">Tanggal Surat</td>
            <td valign="top" colspan="10">
				<div style="vertical-align: top" >
							<spring:bind path="cmd.rekening_client.tgl_surat"> 
				              <c:choose> <c:when test="${cmd.datausulan.flag_bao eq '1' or cmd.datausulan.flag_account  eq '2'  or cmd.datausulan.flag_account eq '3'}"> 
				              <script>inputDate('${status.expression}', '${status.value}', false);</script>
				              </c:when> <c:otherwise> 
				              <script>inputDate('${status.expression}', '${status.value}', true);</script>
				              </c:otherwise> </c:choose> </spring:bind>
			</td>
          </tr>
          <tr  > 
            <th class="sub1" height="20" width="2%">&nbsp;</th>
            <c:choose>
            <c:when test="${cmd.datausulan.lsbs_id eq '186' and cmd.datausulan.lsdbs_number eq '3'}">
            	<td class="sub1" colspan="14" height="20">No. Rekening SSP</td>
            </c:when> <c:otherwise> 
	            <th class="sub1" colspan="14" height="20">No. Rekening Pemegang Polis Yang Digunakan untuk Transaksi (AUTODEBET)</th>
             </c:otherwise> </c:choose>
            
          </tr>
          <tr>          
          	<td></td>
          	 <td width="15%"  height="20">Cari Bank</td>
              <td></td>
            <td  height="20" colspan="1"> <input type="text" name="caribank2" onkeypress="if(event.keyCode==13){ document.frmParam.btncari2.click(); return false;}"> 
              <input type="button" name="btncari2" value="Cari" onclick="ajaxSelectWithParamMuamalat(document.frmParam.caribank2.value,'select_bank2','bank2','account_recur.lbn_id','', 'BANK_ID', 'BANK_NAMA', 'ajaxPjngRek(this.value, 2,\'account_recur.mar_acc_no_split\')','Silahkan pilih BANK','32',${cmd.datausulan.lsbs_id},${cmd.datausulan.lsdbs_number});">  
            </td>
            <td colspan="12">&nbsp;</td>
          </tr>
          <tr>          
          	<td></td>
          	<td height="20">Bank</td>
             <td></td>
            <td  height="20" > <div id="bank2"> 
                <select name="account_recur.lbn_id"
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                  <option value="${cmd.account_recur.lbn_id}">${cmd.account_recur.lbn_nama}</option>
                </select>
                <font color="#CC3300">*</font></div></td>
             <td  height="20" > <div id="bankMuamalat"> 
                <select name="account_recur.lbn_id2" disabled="disabled" 
                <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                  <option value="${cmd.account_recur.lbn_id}">${cmd.account_recur.nama}</option>
                </select>
                <font color="#CC3300">*</font></div></td>
            <td colspan="11"><input type="hidden" name="flag_muamalat" value="0"></td>
            
          </tr>
          
       <tr>          
          	<td></td>
          	<td height="20"><b><font face="Verdana" size="1" color="#996633">Jenis Rekening <%-- ${cmd.account_recur.flag_jn_tabungan} --%> &nbsp; 
              </font><font face="Verdana" size="1" color="#996600"> </font> </b>
             </td>
             <td></td>
             <td>
						<select name="account_recur.flag_jn_tabungan">
	                   	 <option <c:if test="${cmd.account_recur.flag_jn_tabungan eq '0'}">SELECTED</c:if> value="0">Regular</option> 
	           	       	 <option <c:if test="${cmd.account_recur.flag_jn_tabungan eq '1'}">SELECTED</c:if> value="1">Tabunganku</option> 
						</select> 
			 </td>
                <td colspan ="3"></td>
        </tr>
          
          <tr>
            <td width="2%"  height="20">&nbsp;</td>
            <td   height="20" colspan="2">No. Rekening</td>
            <td width="24%"  height="20" colspan="13"> 
            	<spring:bind path="cmd.account_recur.mar_acc_no"> 
          		  <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30" maxlength="20" 
				 	<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="account_recur.mar_acc_no_split[0]" size="1" maxlength="1" onfocus="select();" onkeyup="dofo(0, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[0]}">value="${cmd.account_recur.mar_acc_no_split[0]}"</c:if>  
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="account_recur.mar_acc_no_split[1]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(1, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[1]}">value="${cmd.account_recur.mar_acc_no_split[1]}"</c:if>   
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="account_recur.mar_acc_no_split[2]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(2, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[2]}">value="${cmd.account_recur.mar_acc_no_split[2]}"</c:if>   
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="account_recur.mar_acc_no_split[3]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(3, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[3]}">value="${cmd.account_recur.mar_acc_no_split[3]}"</c:if>   
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="account_recur.mar_acc_no_split[4]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(4, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[4]}">value="${cmd.account_recur.mar_acc_no_split[4]}"</c:if>   
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="account_recur.mar_acc_no_split[5]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(5, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[5]}">value="${cmd.account_recur.mar_acc_no_split[5]}"</c:if>  
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="account_recur.mar_acc_no_split[6]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(6, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[6]}">value="${cmd.account_recur.mar_acc_no_split[6]}"</c:if>  
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="account_recur.mar_acc_no_split[7]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(7, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[7]}">value="${cmd.account_recur.mar_acc_no_split[7]}"</c:if>  
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="account_recur.mar_acc_no_split[8]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(8, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[8]}">value="${cmd.account_recur.mar_acc_no_split[8]}"</c:if>  
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="account_recur.mar_acc_no_split[9]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(9, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[9]}">value="${cmd.account_recur.mar_acc_no_split[9]}"</c:if>  
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="account_recur.mar_acc_no_split[10]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(10, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[10]}">value="${cmd.account_recur.mar_acc_no_split[10]}"</c:if>  
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="account_recur.mar_acc_no_split[11]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(11, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[11]}">value="${cmd.account_recur.mar_acc_no_split[11]}"</c:if>  
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="account_recur.mar_acc_no_split[12]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(12, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[12]}">value="${cmd.account_recur.mar_acc_no_split[12]}"</c:if>  
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="account_recur.mar_acc_no_split[13]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(13, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[13]}">value="${cmd.account_recur.mar_acc_no_split[13]}"</c:if>  
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="account_recur.mar_acc_no_split[14]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(14, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[14]}">value="${cmd.account_recur.mar_acc_no_split[14]}"</c:if>  
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="account_recur.mar_acc_no_split[15]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(15, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[15]}">value="${cmd.account_recur.mar_acc_no_split[15]}"</c:if>  
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="account_recur.mar_acc_no_split[16]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(16, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[16]}">value="${cmd.account_recur.mar_acc_no_split[16]}"</c:if>  
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="account_recur.mar_acc_no_split[17]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(17, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[17]}">value="${cmd.account_recur.mar_acc_no_split[17]}"</c:if>  
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="account_recur.mar_acc_no_split[18]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(18, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[18]}">value="${cmd.account_recur.mar_acc_no_split[18]}"</c:if>  
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="account_recur.mar_acc_no_split[19]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(19, 2);"  <c:if test="${not empty cmd.account_recur.mar_acc_no_split[19]}">value="${cmd.account_recur.mar_acc_no_split[19]}"</c:if>  
					style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
					<input type="text" name="account_recur.mar_acc_no_split[20]" size="2"  maxlength="1" onfocus="select();"  <c:if test="${not empty cmd.account_recur.mar_acc_no_split[20]}">value="${cmd.account_recur.mar_acc_no_split[20]}"</c:if>  
					 style="width: 20px<c:if test="${ not empty status.errorMessage}">;background-color :#FFE1FD;</c:if>">
             </spring:bind>             		
             <font color="#CC3300">*</font>
             </td>           
          </tr>
          
          <tr  > 
            <td width="2%"  height="20">&nbsp;</td>
            <td  height="20" colspan="2">Atas Nama</td>
            <td width="24%"  height="20">
            	<spring:bind path="cmd.account_recur.mar_holder"> 
              		<input type="text" name="${status.expression}"
						   value="${status.value }"  size="35" maxlength="50" 
				    <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>><font color="#CC3300">*</font>
              </spring:bind>
            </td>
            <td width="15%"  height="20"></td>
            <td  height="20"  colspan="11">&nbsp; 
              </td>
          </tr>
          <tr  > 
          	 <td width="2%" >&nbsp;</td>
          	<td width="15%">Tanggal Expired (DD/MM/YYYY)</td>
            <td colspan="14"> <spring:bind path="cmd.account_recur.mar_expired"> <c:choose> 
              <c:when test="${cmd.datausulan.mste_flag_cc eq '1' or cmd.datausulan.mste_flag_cc  eq '2'  or cmd.datausulan.flag_account eq '3'}"> 
              <script>inputDate('${status.expression}', '${status.value}', false);</script>
              </c:when> <c:otherwise> 
              <script>inputDate('${status.expression}', '${status.value}', true);</script>
              </c:otherwise> </c:choose> </spring:bind><font color="#CC3300">*</font> </td>
<!--            <td style="visibility: hidden;" colspan="2">Tanggal Debet (DD/MM/YYYY)</td>-->
<!--            <td style="visibility: hidden;" width="24%"> <spring:bind path="cmd.pemegang.mste_tgl_recur"> -->
<!--              <c:choose> <c:when test="${cmd.datausulan.mste_flag_cc eq '1' or cmd.datausulan.mste_flag_cc  eq '2'  or cmd.datausulan.flag_account eq '3'}"> -->
<!--              <script>inputDate('${status.expression}', '${status.value}', false);</script>-->
<!--              </c:when> <c:otherwise> -->
<!--              <script>inputDate('${status.expression}', '${status.value}', true);</script>-->
<!--              </c:otherwise> </c:choose> </spring:bind> </td>-->
            
          </tr>
          <tr> 
            <td width="2%" >&nbsp;</td>
            <td colspan="2">Aktif</td>
            <td colspan="13">
            	<spring:bind path="cmd.account_recur.mar_active"> 
	                <select name="account_recur.mar_active"
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
	                  <option value="1" selected="selected">Aktif</option>
	                  <option value="0">Non Aktif</option>
	                </select>
				</spring:bind>
            </td>
          </tr>
<%--          <tr> --%>
<%--            <td width="2%" >&nbsp;</td>--%>
<%--            <td colspan="2"  ><b><font face="Verdana" size="1" color="#996600">Nomor Sinar Card (Khusus Produk BM)</font></b></td>--%>
<%--            <td colspan="4">--%>
<%--            	<spring:bind path="cmd.datausulan.mspo_nasabah_acc"> --%>
<%--					<input type="text" name="${status.expression}" value="${status.value }"  size="25" maxlength="16" --%>
<%--				 <c:if test="${ not empty status.errorMessage}">--%>
<%--						style='background-color :#FFE1FD'--%>
<%--					</c:if>><font color="#CC3300">*</font>--%>
<%--				</spring:bind>--%>
<%--            </td>--%>
<%--          </tr>--%>
          <tr>
            <th class="sub1" height="20" width="2%">&nbsp;</th>
            <th class="sub1" colspan="13" height="20">Dana Investasi dialokasikan : <font color="#CC3300">*</font></th>
          </tr><tr>
          <td  height="20" width="2%">&nbsp;</td>
            <td colspan="14" height="20">
            <spring:bind path="cmd.datausulan.mste_flag_investasi">
					<label for="langsung"> <input type="radio" class=noBorder 
						name="${status.expression}" value="0" onClick="sts('0');"
						<c:if test="${cmd.datausulan.mste_flag_investasi eq 0 or cmd.datausulan.mste_flag_investasi eq null}"> 
									checked</c:if>
						 id="langsung">Langsung setelah dana diterima di rekening yang ditentukan oleh PT. Asuransi Jiwa Sinarmas MSIG dan SPAJ telah diterima di bagian Underwriting. </label>
					<br>
					<label for="tidaklangsung"> <input type="radio" class=noBorder 
						name="${status.expression}" value="1" onClick="sts('1');"
						<c:if test="${cmd.datausulan.mste_flag_investasi eq 1}"> 
									checked</c:if>
						 id="tidaklangsung">Setelah permintaan asuransi disetujui oleh Bagian Underwriting dan Calon Pemegang Polis telah setuju serta membayar ekstra premi (jika ada).</label>
				</spring:bind>
             </td>
          </tr>   
		            <tr  > 
            <th class="sub1" height="20" width="2%">&nbsp;</th>
		              <th class="sub1" colspan="14" height="20">Jenis 
              Dana Investasi ( Isi % jenis Dana Investasi dengan kelipatan 20% atau 10 % tergantung produknya, 
              sehingga total 100% ) <spring:bind path="cmd.investasiutama.jmlh_invest"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30">
              </spring:bind></th>
          </tr>    
          <tr> 
            <th class="sub1" width="2%" rowspan="2">No.</th>
            <th class="sub1"  rowspan="2"  colspan="3">Type of Fund</th>
            <th class="sub1"  colspan="11">Fund Allocation</th>
          </tr>
          <tr> 
            <th class="sub1"  width="15%">Persen (%)</th>
            <th class="sub1" height="12" colspan="4">Jumlah</th>
          </tr>
          <c:forEach  items="${cmd.investasiutama.daftarinvestasi}" var="inv" varStatus="status"> 
	          <c:if test="${status.count lt 4}">
		          <tr  > 
		          	
			            <td width="2%" align="right"> <p align="right">${status.count}</td>
			            <td  align="right" colspan="3"> <div align="left"><spring:bind path="cmd.investasiutama.daftarinvestasi[${status.index}].lji_invest1"> 
			                <input type="text" name="${status.expression}"
									value="${status.value }"  size="50" style='background-color :#D4D4D4'readOnly>
			                </spring:bind> </div></td>
			            <td width="15%" align="right"> <div align="left"><spring:bind path="cmd.investasiutama.daftarinvestasi[${status.index}].mdu_persen1"> 
			                <input type="text" name="${status.expression}"
									value="${status.value }"  size="10" maxlength="3" 
							 <c:if test="${ not empty status.errorMessage}">
									style='background-color :#FFE1FD'
								</c:if>>
			               
			                </spring:bind><font color="#CC3300">*</font> </div></td>
			            <td align="right" colspan="4"> <div align="left"><spring:bind path="cmd.investasiutama.daftarinvestasi[${status.index}].mdu_jumlah1"> 
			                <input type="text" name="${status.expression}"
									value="${status.value }"  size="17" style='background-color :#D4D4D4' readOnly 
							 <c:if test="${ not empty status.errorMessage}">
									style='background-color :#FFE1FD'
								</c:if>>
			                 </spring:bind> <font color="#CC3300">*</font></div></td>
		           
		          </tr>
	          </c:if>
          </c:forEach> 
          <tr  > 
            <td width="2%" align="right">&nbsp;</td>
            <td colspan="3" align="right">Total :</td>
            <td width="15%" align="right" > <div align="left"><spring:bind path="cmd.investasiutama.total_persen"> 
                <input type="text" name="${status.expression}"
						value="${status.value }"  size="10" style='background-color :#D4D4D4' readOnly
				>
                 </spring:bind> 
              </div></td>
            <td width="2%" align="right" colspan="4">&nbsp;</td>
          </tr>
        </table>
        <table class="form_input" style="font-size: 11px;" ID="tablebiaya">
            <tr  > 
              
            <th width="5%" class="sub1">No.</th>
              <th width="35%" class="sub1">Jenis Biaya</th>
              <th width="24%" class="sub1">Jumlah</th>
              <th width="36%" class="sub1">Persen(%)</th>
            </tr>
			 <c:forEach items="${cmd.investasiutama.daftarbiaya}" var="biaya" varStatus="status"> 
			             <tr  > 
              <td width="5%" >${status.count}</td>
              <td width="35%" ><div align="left"><spring:bind path="cmd.investasiutama.daftarbiaya[${status.index}].ljb_biaya"> 
                <input type="text" name="${status.expression}"
						value="${status.value }"  size="50" style='background-color :#D4D4D4'readOnly>
                </spring:bind> </div></td>
              <td width="24%" ><div align="left"><spring:bind path="cmd.investasiutama.daftarbiaya[${status.index}].mbu_jumlah"> 
                <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" style='background-color :#D4D4D4'readOnly>
                </spring:bind> </div></td>
              <td width="36%" ><div align="left"><spring:bind path="cmd.investasiutama.daftarbiaya[${status.index}].mbu_persen"> 
                <input type="text" name="${status.expression}"
						value="${status.value }"  size="10" style='background-color :#D4D4D4'readOnly>
                </spring:bind> </div></td>
            </tr>
			</c:forEach>
          </table>
        
		<table class="form_input" style="font-size: 11px;">
          <tr> 
            <td>&nbsp;</td>
          </tr>
        </table>
		<table height="24" class="form_input" style="font-size: 11px;">
			<tr> 
				<td>Detail Investasi Khusus Produk PowerSave, StableLink, atau StableSave</td>
			</tr>
		</table>
        <table height="38" class="form_input" style="font-size: 11px;">
			<tr> 
				<th width="34" class="sub1"></th>
				<th width="1475" class="sub1">Jangka Waktu</th>
				<th width="1073" class="sub1">Bunga (%)</th>
				<th width="201" class="sub1">Tanggal Jatuh Tempo<br>(DD/MM/YYYY)</th>
				<th width="185" class="sub1">Jumlah Investasi</th>
				<th width="159" class="sub1">Jumlah Bunga</th>
				<th width="1276" class="sub1">Jenis Roll Over</th>
			</tr>
			<tr> 
				<td width="34">&nbsp;</td>
				<td width="1475">
					<select name="powersave.mps_jangka_inv"
							<c:if test="${not empty status.errorMessage}"> style='background-color :#FFE1FD' </c:if> 
							onchange="setPersentaseRate(this.value);">
                		<c:forEach var="jk" items="${select_jangka_invest}">
                			<option 
								<c:if test="${cmd.powersave.mps_jangka_inv eq jk.ID}"> SELECTED </c:if> value="${jk.ID}">${jk.JANGKAWAKTU}
							</option>
						</c:forEach> 
              		</select>
              		<font color="#CC3300">*</font>
              	</td>
				<td width="1073">
					<spring:bind path="cmd.powersave.mps_rate"> 
						<input type="text" name="${status.expression}" value="${status.value }"  size="10" maxlength="7"
							<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
					</spring:bind>
				</td>
				<td width="201">
					<spring:bind path="cmd.powersave.mps_batas_date"> 
						<script>inputDate('${status.expression}', '${status.value}', true);</script>
					</spring:bind>
				</td>
				<td width="185">
					<spring:bind path="cmd.powersave.mps_prm_deposit"> 
						<input type="text" name="${status.expression}" value="${status.value }"  size="15" style='background-color :#D4D4D4'readOnly>
					</spring:bind>
				</td>
				<td width="159">
					<spring:bind path="cmd.powersave.mps_prm_interest"> 
						<input type="text" name="${status.expression}" value="${status.value }"  size="15" style='background-color :#D4D4D4'readOnly>
					</spring:bind>
				</td>
				<td width="1276">
					<select name="powersave.mps_roll_over" onchange="tutup();"
			 				<c:if test="${ not empty status.errorMessage}"> style='background-color :#FFE1FD' </c:if>>
						<c:forEach var="roll" items="${select_rollover}"> 
							<option <c:if test="${cmd.powersave.mps_roll_over eq roll.ID}"> SELECTED </c:if> value="${roll.ID}">${roll.ROLLOVER}
							</option>
						</c:forEach> 
					</select>
					<font color="#CC3300">*</font> 
				</td>
			</tr>
			<tr> 
				<th class="sub1">&nbsp;</th>
				<th class="sub1">Jenis Bunga</th>
				<th colspan="2" class="sub1">No Memo</th>
				<th class="sub1" colspan="2">Fee Based Income (%)</th>
				<th class="sub1">Status Karyawan</th>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
					<select name="powersave.mps_jenis_plan" onChange="jns_rate();"
						<c:if test="${ not empty status.errorMessage}"> style='background-color :#FFE1FD' </c:if>>
						<c:forEach var="bg" items="${select_jenisbunga}">
						<option <c:if test="${cmd.powersave.mps_jenis_plan eq bg.ID}"> SELECTED </c:if>
							value="${bg.ID}">${bg.JENIS}</option>
						</c:forEach> 
					</select>
					<font color="#CC3300">*</font></td>
				<td colspan="2">
					<spring:bind path="cmd.powersave.mpr_note"> 
						<input type="text" name="${status.expression}" value="${status.value}" size="30" maxlength="100"
							<c:if test="${ not empty status.errorMessage}"> style='background-color :#FFE1FD' </c:if>>
              		</spring:bind> 
              		<spring:bind path="cmd.powersave.mpr_nett_tax"> 
						<input type="hidden" name="${status.expression}" value="${status.value}" size="30" maxlength="100">
             		</spring:bind>
             		<font color="#CC3300">*</font>
				</td>
				<td colspan="2">
					<spring:bind path="cmd.powersave.fee_based_income"> 
              			<input type="text" name="${status.expression}" value="${status.value }"  size="10" maxlength="7" style='visibility: hidden; background-color :#D4D4D4'readOnly
							<c:if test="${ not empty status.errorMessage}">
								style='background-color :#FFE1FD'
							</c:if>>
					</spring:bind> 				
				</td>
				<td>
					<select name="powersave.mps_employee" 
						<c:if test="${ not empty status.errorMessage}"> style='background-color :#FFE1FD' </c:if>>
						<c:forEach var="kr" items="${select_karyawan}">
							<option  <c:if test="${cmd.powersave.mps_employee eq kr.ID}"> SELECTED </c:if>
							value="${kr.ID}">${kr.KRY}</option> 
						</c:forEach> 
					</select>
					<font color="#CC3300">*</font>
				</td>
			</tr>
			<tr> 
				<th class="sub1">&nbsp;</th>
				<th class="sub1">Tanggal NAB</th>
				<th colspan="2" class="sub1">Nilai NAB</th>
				<th class="sub1" colspan="2">Begdate Top-Up</th>
				<th class="sub1">Persentase Rate BP (%)</th>
			</tr>
			<tr>
				<td></td>
				<td>
					<spring:bind path="cmd.powersave.msl_tgl_nab">
						<input type="text" name="${status.expression}" value="${status.value }"  size="30" maxlength="100" readOnly >
					</spring:bind>
				</td>
				<td colspan="2">
					<spring:bind path="cmd.powersave.msl_nab"> 
						<input type="text" name="${status.expression}" value="${status.value }"  size="30" maxlength="50" readOnly>
					</spring:bind>
				</td>
				<td colspan="2">
					<spring:bind path="cmd.powersave.begdate_topup">  
						<script>inputDate('${status.expression}', '${status.value}', false,'');</script>
              			<font color="#CC3300">*</font>
              		</spring:bind>
              	</td>
				<td>
					<spring:bind path="cmd.powersave.msl_bp_rate"> 
						<input type="text" name="${status.expression}" readonly="true" value="${status.value }"  size="10" maxlength="30" >%
					</spring:bind>
					<font color="#CC3300"> *</font>
				</td>
			</tr>   
			<tr> 
				<th class="sub1">&nbsp;</th>
				<th class="sub1">Penarikan Bunga</th>
				<th colspan="2" class="sub1">Breakable</th>
				<th class="sub1" colspan="2">Cara Bayar Rider</th>
				<th class="sub1">TELEMARKETING</th>
			</tr>
			<tr>
				<td></td>
				<td>
					<select name="powersave.tarik_bunga">
	                    <option <c:if test="${cmd.powersave.tarik_bunga eq 0}"> SELECTED </c:if>	value="0">0</option> 
	           	        <option <c:if test="${cmd.powersave.tarik_bunga eq 1}"> SELECTED </c:if>	value="1">1</option> 
	           	        <option <c:if test="${cmd.powersave.tarik_bunga eq 3}"> SELECTED </c:if>	value="3">3</option> 
					</select> bulan
				</td>
				<td colspan="2">
					<spring:bind path="cmd.powersave.mpr_breakable">
						<input type="checkbox" class="noborder" value="1" onclick="brekele(this.checked);"
            				<c:if test="${status.value eq 1}">checked="checked"</c:if>> 
						<input type="hidden" id="brek" name="${status.expression}" value="${status.value}"> Breakable
            		</spring:bind>
				</td>
              	<td colspan="2">
		          	<input class="noBorder" type="radio" <c:if test="${cmd.powersave.mpr_cara_bayar_rider eq 0}">checked</c:if> name="powersave.mpr_cara_bayar_rider" value="0"
		          	<c:if test="${cmd.powersave.mpr_cara_bayar_rider eq 2 or cmd.powersave.mpr_cara_bayar_rider eq 3}">disabled="disabled" </c:if>>Bayar Langsung<br>
		           	<input class="noBorder" type="radio" <c:if test="${cmd.powersave.mpr_cara_bayar_rider eq 1}">checked</c:if> name="powersave.mpr_cara_bayar_rider" value="1"
		           	<c:if test="${cmd.powersave.mpr_cara_bayar_rider eq 2 or cmd.powersave.mpr_cara_bayar_rider eq 3}">disabled="disabled" </c:if>>Potongan Bunga<br>
		           	<input class="noBorder" type="radio" <c:if test="${cmd.powersave.mpr_cara_bayar_rider eq 2}">checked</c:if> name="powersave.mpr_cara_bayar_rider" value="2"
		           	<c:if test="${cmd.powersave.mpr_cara_bayar_rider eq 0 or cmd.powersave.mpr_cara_bayar_rider eq 1}">disabled="disabled" </c:if>>Bayar Langsung Sekaligus<br>
		           	<input class="noBorder" type="radio" <c:if test="${cmd.powersave.mpr_cara_bayar_rider eq 3}">checked</c:if> name="powersave.mpr_cara_bayar_rider" value="3"
		           	<c:if test="${cmd.powersave.mpr_cara_bayar_rider eq 0 or cmd.powersave.mpr_cara_bayar_rider eq 1}">disabled="disabled" </c:if>>Potongan Bunga Sekaligus<br>
		        	<c:if test="${cmd.datausulan.flag_paymode_rider eq 1}">
			        	<spring:bind path="cmd.datausulan.lscb_id_rider">
			        		<select name="${status.expression}">
			        			 
			        			<option value="0" <c:if test="${status.value eq 0}"> selected="selected"</c:if>>SEKALIGUS</option>
			        			<option value="1" <c:if test="${status.value eq 1}"> selected="selected"</c:if>>TRIWULANAN</option>
			        			<option value="2" <c:if test="${status.value eq 2}"> selected="selected"</c:if>>SEMESTERAN</option>
			        			<option value="3" <c:if test="${status.value eq 3}"> selected="selected"</c:if>>TAHUNAN</option>
			        			<option value="6" <c:if test="${status.value eq 6}"> selected="selected"</c:if>>BULANAN</option>
			        		</select>
			        	</spring:bind>
		        	</c:if>
		        </td>
				<td>
					<spring:bind path="cmd.tertanggung.mste_flag_ref_telemarket">
						<input type="checkbox" class="noborder" value="1" onclick="telemarket(this.checked);" 
            				<c:if test="${status.value eq 1}">checked="checked"</c:if>> 
						<input type="hidden" id="tele" name="${status.expression}" value="${status.value}"> Via Telemarketing
            		</spring:bind>
				</td>
			</tr>               
		</table>
<%--        <table border="0" width="100%" cellspacing="1" cellpadding="1" height="24" class="entry">--%>
<%--          <tr> --%>
<%--            <td width="46%"> --%>
<%--              <p align="left"><b> <font face="Verdana" size="1" color="#CC3300">Detail --%>
<%--                Khusus Bank Sinarmas</font></b> --%>
<%--            </td>--%>
<%--            <td width="54%"><b><font face="Verdana" size="1" color="#CC3300">Detail --%>
<%--              Bonus Tahapan khusus Produk Simponi 8 dan Premium Saver </font></b></td>--%>
<%--          </tr>--%>
<%--        </table>--%>
<%--        <table border="0" width="100%" cellspacing="1" cellpadding="1" height="38" class="entry">--%>
<%--          <tr  > --%>
<%--            <td width="34"  class="sub1">&nbsp;</td>--%>
<%--            <td width="528" class="sub1"><b><font face="Verdana" size="1" color="#FFFFFF"> --%>
<%--              % Bunga --%>
<%--		  --%>
<%--			  </font></b></td>--%>
<%--            <td width="666" class="sub1"><b><font face="Verdana" size="1" color="#FFFFFF">% --%>
<%--              Bonus Tahapan --%>
<%--              </font></b></td>--%>
<%--          </tr>--%>
<%--          <tr  > --%>
<%--            <td width="34">&nbsp;</td>--%>
<%--            <td height="22" width="528"> --%>
<%--              <spring:bind path="cmd.pemegang.mspo_under_table"> --%>
<%--              <input type="text" name="${status.expression}"--%>
<%--						value="${status.value }"  size="30" maxlength="5"  >--%>
<%--              </spring:bind> <spring:bind path="cmd.pemegang.tgl_mspo_under_table"> --%>
<%--              <input type="text" name="${status.expression}"--%>
<%--						value="${status.value }"  size="15" maxlength="5"  >--%>
<%--               </spring:bind><font color="#CC3300">*</font> </td>--%>
<%--            <td height="22" width="666"> --%>
<%--               <spring:bind path="cmd.pemegang.bonus_tahapan"> --%>
<%--              <input type="text" name="${status.expression}"--%>
<%--						value="${status.value }"  size="30" maxlength="5" >--%>
<%--              </spring:bind> --%>
<%--            </td>--%>
<%--          </tr>--%>
<%--        </table>--%>

		<br>				
        <table height="27" class="form_input" style="font-size: 11px;">
          <tr> 
            <th class="subtitle"  width="100%"  height="27">D. DATA YANG DITUNJUK MENERIMA MANFAAT ASURANSI</th>
          </tr>
          <tr> 
            <th  class="subtitle" align="center"  > 
             <input name="btnadd1" type="button" id="btnadd1" value="ADD" onClick="addRowDOM1('tableProd', '1')"> &nbsp;&nbsp;&nbsp;&nbsp; 
             <input name="btn_cancel1" type="button" id="btn_cancel1" value="DELETE" onClick="cancel1()">			
              <input type="hidden" name="jmlpenerima"  value="0" >
            </th>
          </tr>
        </table>
          <table ID="tableProd" height="27" class="form_input" style="font-size: 11px;">
            <tr > 
              <th  class="sub1" >No</td>
              <th class="sub1" >Nama Lengkap <br>(Sesuai KTP/Akte Lahir)</th>
              <th class="sub1" > <font size="1">Tanggal Lahir <font color="#CC3300">*</font></th>
              <th class="sub1" >Jenis Kelamin <font color="#CC3300">*</font></th>
              <th class="sub1" >Hubungan Dengan Calon Tertanggung <font color="#CC3300">*</font></th>
              
            <th class="sub1" > <font size="1">Manfaat (%) <font color="#CC3300">*</font></th>
                <th class="sub1"  >&nbsp;</th>
  			<!--<td></td>-->
            </tr>
         <c:forEach items="${cmd.datausulan.daftabenef}" var="benef" varStatus="status"> 
            <tr > 
              <td > ${status.count}</td>
              <td >
			  	<input type="text" name='benef.msaw_first${status.index +1}' value ='${benef.msaw_first}' size="30" maxlength="60">
              </td>
	          <td > 
		          	<input type="text" name='tgllhr${status.index +1}' value='<fmt:formatDate value="${benef.msaw_birth}" pattern="dd"/>'  size="3" maxlength="2" style="width: 30px">
					/<input type="text" name='blnhr${status.index +1}' value='<fmt:formatDate value="${benef.msaw_birth}" pattern="MM"/>'  size="3" maxlength="2" style="width: 30px">
				/<input type="text" name='thnhr${status.index +1}' value='<fmt:formatDate value="${benef.msaw_birth}" pattern="yyyy"/>'  size="5" maxlength="4" style="width: 40px">
				<input type="hidden" name='msaw_birth${status.index +1}' value='<fmt:formatDate value="${benef.msaw_birth}" pattern="dd/MM/yyyy"/>'  size="5" readOnly>
              </td>
              <td>
					<select name="benef.msaw_sex${status.index +1}">
	                    <option <c:if test="${benef.msaw_sex eq 0}"> SELECTED </c:if>	value="0">Perempuan</option> 
	           	        <option <c:if test="${benef.msaw_sex eq 1}"> SELECTED </c:if>	value="1">Laki-Laki</option> 
					</select> 
			  </td>
              <td > 
			  		<select name="benef.lsre_id${status.index +1}">
						<c:forEach var="benefrl" items="${select_hubungan}">
							<option 
							<c:if test="${benef.lsre_id eq benefrl.ID}"> SELECTED </c:if>
							value="${benefrl.ID}">${benefrl.RELATION}</option>
						</c:forEach>
					</select>
              </td>
              <td>
			  	<input type="text" name='benef.msaw_persen${status.index +1}' value ='${benef.msaw_persen}' size="5" maxlength="5">
              </td>
                <td > <input type=checkbox name="cek${status.index +1}" id= "ck${status.index +1}" class="noBorder" ></td>
  
            </tr>
		</c:forEach>		 
		 
		  </table>
        
        <table ID="tableProd" class="form_input" style="font-size: 11px;">
          <tr> 
            <td colspan="9" style="color: #CC3300;font-weight: bold">Note : Untuk pecahan decimal menggunakan titik. * Wajib diisi</td>
          </tr>		
          <tr> 
            <td colspan="9"> <input type="hidden" name="hal" value="${halaman}">	
			<spring:bind path="cmd.pemegang.indeks_halaman"> 
              <input type="hidden" name="${status.expression}"
						value="${halaman-1}"  size="30" >
              </spring:bind>
			
			</td>
          </tr>
          <tr> 
            <td  align="center" colspan="9"> 
              <div align="center">
                <input type="submit" name="_target${halaman-1}" value="Prev &laquo;" onClick="prev()" 
				accesskey="p" onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
                <input type="submit" name="_target${halaman+1}" value="Next &raquo;" onClick="next()" 
				accesskey="N" onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
                <input type="hidden" name="_page" value="${halaman}">
              </div>
            </td>
          </tr>
        </table>
      <p>
    </td>
  </tr>
</table>
 </form>
</body>
<%@ include file="/include/page/footer.jsp"%>

