<%@ include file="/include/page/taglibs.jsp"%>

<fieldset>
	<legend>Agen</legend>
	
	<table class="entry2">
	
		<tr>
			<th nowrap="nowrap">
				Agen
			</th>
			<th nowrap="nowrap">
				<input type="text" value="${cmd.agen.MSAG_ID}" size="8" readonly>
				<input type="text" value="${cmd.agen.MCL_FIRST}" size="50" readonly>
			</th>
			<th nowrap="nowrap">
				Follow Up
			</th>
			<th nowrap="nowrap">
				<input type="text" value="${cmd.agen.FOLLOW_UP}" size="10" readonly>
			</th>
		</tr>
		
		<tr>
			<th nowrap="nowrap">
				Region
			</th>
			<th nowrap="nowrap">
				<input type="text" value="${cmd.agen.REGION_ID}" size="8" readonly>
				<input type="text" value="${cmd.agen.REGION_NAME}" size="50" readonly>
			</th>
			<th nowrap="nowrap">
				No. Blanko
			</th>
			<th nowrap="nowrap">
				<input type="text" value="${cmd.agen.MSPO_NO_BLANKO}" size="10" readonly>
			</th>
		</tr>
		
		<tr>
			<th nowrap="nowrap">
				<input type="checkbox" class="noBorder" <c:if test="${cmd.agen.MSPO_REF_BII eq 1}">checked</c:if> disabled> AO
			</th>
			<th nowrap="nowrap">
				<input type="text" value="${cmd.agen.MSPO_AO}" size="8" readonly>
				<input type="text" value="${cmd.agen.MCL_FIRST_AO}" size="50" readonly>
			</th>
			<th nowrap="nowrap">
				<input type="checkbox" class="noBorder" <c:if test="${cmd.agen.MSPO_PRIBADI eq 1}">checked</c:if> disabled> Pribadi
			</th>
			<th nowrap="nowrap">
				Tanggal SPAJ
				<input type="text" value="<fmt:formatDate value="${cmd.agen.MSPO_SPAJ_DATE}" pattern="dd/MM/yyyy"/>" size="12" readonly>
			</th>
		</tr>
		<tr>
			<th nowrap="nowrap">
				Email Agen
			</th>
			<th nowrap="nowrap">
				<input type="text" value="${cmd.agen.MSPE_EMAIL }" size="50" readonly>
			</th>
		</tr>
		
	</table>
	
	<c:if test="${not empty hadiah }">
		<table class="entry2">
			<tr>
				<th style="text-align: left;">Jenis Hadiah</th>
			</tr>
			<tr>
				<td>
					<c:forEach items="${hadiah }" var="h" varStatus="status">
						${status.index +1}. ${h.lh_nama } ${h.mh_quantity } unit<br>
					</c:forEach>
				</td>
			</tr>
		</table>
	</c:if>

</fieldset>
