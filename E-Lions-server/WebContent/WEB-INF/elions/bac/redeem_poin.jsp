<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
	<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta NAME="Description" CONTENT="EkaLife">
	<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<meta HTTP-EQUIV="Expires" CONTENT="-1">
	<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
	<link href="${path}/include/image/eas.ico" rel="shortcut icon">
	<!--  -->
	<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
	<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
	<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
	<!--  -->
	<script type="text/javascript" src="${path }/include/js/default.js"></script>
	<!-- Ajax Related -->
	<script type="text/javascript" src="${path}/include/js/ajax.js"></script>
	<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script>
	<script type="text/javascript" src="${path }/dwr/engine.js"></script>
	<script type="text/javascript" src="${path }/dwr/util.js"></script>
	
	<script type="text/javascript">
		hideLoadingMessage();
		
		function waitPreloadPage() { //DOM
			<c:if test='${not empty pesan}'>
	            alert('${pesan}');
	        </c:if>
			
			//calendar.set("tgl_lahir");
			
			if (document.getElementById){
				document.getElementById('prepage').style.visibility='hidden';
			}else{
				if (document.layers){ //NS4
					document.prepage.visibility = 'hidden';
				}
				else { //IE4
					document.all.prepage.style.visibility = 'hidden';
				}
			}
		}
		// End -->
		
		function getInfo(id_ref){
			ajaxManager.getInfoRef(id_ref,
			{callback:function(map) {
				DWRUtil.useLoadingMessage();
				document.formpost.elements['kd_ref'].value = map.kd_ref;
				document.formpost.elements['nama_ref'].value = map.nama_ref;
				document.formpost.elements['alamat'].value = map.alamat+" "+map.kota;
				var pos = map.kd_pos;
				if(pos==null)pos='';
				document.formpost.elements['kd_pos'].value = pos;
				/* document.formpost.elements['area_telp'].value = map.area_telp;
				document.formpost.elements['telp'].value = map.telp; */
				document.formpost.elements['poin_ref'].value = map.poin; 
				var pesan = map.pesan;
				
				if(pesan!=null)alert(pesan);
			},
			 timeout:180000,
			 errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		}); 
	} 
	</script>
</head>

<body onload="waitPreloadPage();" >
<div id="prepage" style="position:absolute; font-family:arial; font-size:12; left:0px; top:0px; background-color:yellow; layer-background-color:white;"> 
	<table border="2" bordercolor="red"><tr><td style="color: red;"><b>Loading ... ... Please wait!</b></td></tr></table>
</div>
<form:form method="post" name="formpost" id="formpost" action="${path}/bac/multi.htm?window=redeem_point" commandName="command">
	<fieldset>
		<legend style="font-size: 12px; color: black; font-weight: bolder;">Redeem Poin Tambang Emas</legend>
		<table class="entry" style="text-transform: uppercase;">
			  <tr>
			    <th>Kode Referral</th>
			    <td colspan="3"><form:input path="kd_ref" cssErrorClass="errorField" onchange="getInfo(this.value);"/>&nbsp;<em style="color: #ff0000"><form:errors path="kd_ref" cssClass="errorMessage"/></em>
			    	<input type="submit" name="btnSearch" id="btnSearch" value="Search">
			    </td>
			  </tr>
			  <tr>
			    <th>Nama Referral</th>
			    <td colspan="3"><form:input path="nama_ref" readonly="readonly" cssClass="readonly" size="80"/><!-- &nbsp;<em style="color: #ff0000"><form:errors path="nama_ref" cssClass="errorMessage"/></em> --></td>
			  </tr>
			  <tr>
			    <th>Jumlah point yang terkumpul</th>
			    <td colspan="3"><form:input path="poin_ref" readonly="readonly" cssClass="readonly"/><!-- &nbsp;<em style="color: #ff0000"><form:errors path="poin_ref" cssClass="errorMessage"/></em> --></td>
			  </tr>
			  <tr>
			    <th>Hadiah yang diambil</th>
			    <td>
			    	<form:select path="hadiah">
					<c:forEach items="${produk }" var="p">
						<form:option value="${p.ID_ITEM }~${p.NILAI_POINT }" label="${p.NAMA_ITEM }(${p.NILAI_POINT } poin)"/>
					</c:forEach>
					</form:select>
				</td>
			  </tr>
			  <tr>
			  	<th>Registrasi Hadiah</th>
			    <td><form:radiobutton id="reg_hadiah1" path="reg_hadiah" value="0" cssStyle="border:none;"/>Diambil di kantor pusat&nbsp;
			    	<form:radiobutton id="reg_hadiah2" path="reg_hadiah" value="1" cssStyle="border:none;"/>Dikirim
			    	&nbsp;<em style="color: #ff0000"><form:errors path="reg_hadiah" cssClass="errorMessage"/></em>
			    </td>
			  </tr>
			  <tr>
			    <th>Alamat Lengkap</th>
			    <td><form:textarea title="Alamat" path="alamat" cols="80" rows="3" cssErrorClass="errorField"/><br>&nbsp;<em style="color: #ff0000"><form:errors path="alamat" cssClass="errorMessage"/></em></td>
			  </tr>
			  <tr>
			  	<th>Kode Pos</th>
			    <td><form:input path="kd_pos" cssErrorClass="errorField"/>&nbsp;<em style="color: #ff0000"><form:errors path="kd_pos" cssClass="errorMessage"/></em></td>
			  </tr>
		</table>
		<input type="submit" name="btnSimpan" id="btnSimpan" value="Redeem">&nbsp;
		<input type="button" name="btnReset" id="btnReset" title="Reset" onClick="window.location='${path}/bac/multi.htm?window=redeem_point';" value="Reset">
	</fieldset>
	
	<c:if test="${not empty list }">
	<br>&nbsp;
	<fieldset>
	<legend style="font-size: 12px; color: black; font-weight: bolder;">Data</legend>
	<table class="entry" style="text-transform: uppercase;">
		<tr>
			<th>Tanggal</th>
			<th>Item</th>
			<th>Poin</th>
			<th>Registrasi</th>
			<th>User</th>
			<th>Status</th>
		</tr>
		<c:forEach items="${list }" var="l">
		<tr>
			<td><fmt:formatDate value="${l.TGL }" pattern="dd/MM/yyyy"/></td>
			<td>${l.NAMA_ITEM }</td>
			<td>${l.POIN }</td>
			<td>${l.REG_HADIAH }</td>
			<td>${l.USER_INPUT }</td>
			<td>${l.APPROVE }</td>
		</tr>
		</c:forEach>
	</table>
	</fieldset>
	</c:if>
</form:form>
</body>
</html>	
