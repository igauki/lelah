<%@ include file="/include/page/header.jsp"%>
<script>
	function awal(){
		var info='${cmd.error}'
		var pesan = '${pesanTambahan}';
		if(pesan != '') alert(pesan);
		var lspd = "${cmd.lspd_id}";
		var posisi = "Underwriting";
		if(lspd=="6"){
			posisi = "Print Polis";
		}
		
		if(lspd=="2" || lspd=="6" ){
			if(info=='0'){
				if("${proses_spaj}" == 'ditolak'){
					alert("${cmd.konfirmasi}");
					window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';
				}else if(confirm("${cmd.konfirmasi}")){
					if(confirm("Transfer Ke "+posisi+"? \n${cmd.to}")){
						formpost.submit();
					}else{
						window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';
					}
				}else{
					window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';
				}	
			}else if(info=='100'){
				if(confirm("Produk Unit Link Rekening Nasabah Belum Di Input \n Yakin Transfer?")){
					formpost.submit();
				}else{
					window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
				}	
			}	
		}else{
			if(info=='0'){
				if("${proses_spaj}" == 'ditolak'){
					alert("${cmd.konfirmasi}");
					window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';
				}else if(confirm("${cmd.konfirmasi}")){
					<c:choose>
						<c:when test="${not empty sessionScope.currentUser.cab_bank and sessionScope.currentUser.jn_bank ne 2}">
							if(confirm("Transfer Ke Print Polis? \n${cmd.to}")){
						</c:when>
						<c:when test="${not empty sessionScope.currentUser.cab_bank and sessionScope.currentUser.jn_bank eq 2}">
							if(confirm("Transfer Ke Underwriting? \n${cmd.to}")){
						</c:when>
						<c:otherwise>
							if(confirm("Transfer Ke Proses Checking? \n${cmd.to}")){
						</c:otherwise>
					</c:choose>
						formpost.submit();
					}else{
						window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';
					}	
				}else{
					window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';		
				}
			}else if(info=='100'){
				if(confirm("Produk Unit Link Rekening Nasabah Belum Di Input \n Yakin Transfer?")){
					formpost.submit();
				}else{
					window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';		
				}
			}
		}
	}
</script>
	


<body onload="awal();">
	<form name="formpost" method="post">
		<table>
			<tr>

				<c:if test="${not empty param.submitSuccess}">
					<td><div id="success">Silahkan Tunggu... Sedang dilakukan Proses Transfer...</div>
					</td>
				</c:if>	
			</tr>
			<tr>
			 <td>
		  			<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
								Informasi :<br>
									<c:forEach var="error" items="${status.errorMessages}">
												- <c:out value="${error}" escapeXml="false" />
										<br/>
										<c:if test="${error eq \"Nasabah telah memiliki polis kesehatan, cek riwayat klaim kesehatan\"}">
											<script type="text/javascript">
												alert('${error}');
											</script>	
										</c:if>	
									</c:forEach>
							</div>
						</c:if>									
					</spring:bind>
				</td>
			</tr>
		</table>
	</form>
</body>

<%@ include file="/include/page/footer.jsp"%>