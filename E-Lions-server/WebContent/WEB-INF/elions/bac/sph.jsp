<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen"> <!-- CSS untuk DatePicker Script (jscalendar) -->
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script><!-- DatePicker Script (jscalendar) -->
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script><!-- DatePicker Script (jscalendar) -->
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script><!-- DatePicker Script (jscalendar) -->
<script type="text/javascript">
	hideLoadingMessage();
	
	function cek(prod){
		if(prod == '') return;
		document.formpost.area.disabled = true;
		document.formpost.area2.disabled = true;
		document.formpost.cab_mayapada.disabled = true;
		document.formpost.cab_uob.disabled = true;
			
		if(prod == '142004'){ //MAYAPADA - MY SAVING INVESTA
			document.formpost.cab_mayapada.disabled = false;
		}else if(prod == '142005'){ //UOB - PRIVILEGE SAVE
			document.formpost.cab_uob.disabled = false;
		}else if(prod == '155002'){ //BII - PLATINUM, SPECTA, SMART
			document.formpost.area.disabled = false;			
		}else if(prod == '155001' || prod == '155003'){ //BII - PLATINUM, SPECTA, SMART
			document.formpost.area2.disabled = false;			
		}
	}
	
</script>
</head>
<BODY onload="setupPanes('container1', 'tab1'); cek('${produk}');" style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">E-Mail SPH</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost">
					<fieldset>
						<legend>SPH</legend>
						<table class="entry2">
							<tr>
								<th>Produk</th>
								<td>
									<c:forEach var="p" items="${listProduk}" varStatus="st">
										<label for="produk${p.lsbs}">
											<input onclick="cek('${p.lsbs}');" type="radio" id="produk${p.lsbs}" value="${p.lsbs}" name="produk" class="noBorder" <c:if test="${p.lsbs eq produk}">checked="true"</c:if>/>${p.nama}
										</label>
									</c:forEach>
								</td>
							</tr>
							<tr>
								<th>Area Branch Manager (BII)</th>
								<td>
									<select name="area">
										<c:set var="grup" value=""/>
										<c:forEach var="r" items="${listABM}" varStatus="st">
											<c:choose>
												<c:when test="${r.GRUP ne grup}">
													<c:set var="grup" value="${r.GRUP}"/>				 
													<OPTGROUP label="${r.GRUP}">
														<option value="${r.AREA}" <c:if test="${r.AREA eq area}">selected</c:if> >${r.NAMA_AREA}</option>
												</c:when>
												<c:otherwise>
													<option value="${r.AREA}" <c:if test="${r.AREA eq area}">selected</c:if> >${r.NAMA_AREA}</option>
												</c:otherwise>
											</c:choose>
											<c:if test="${r.GRUP ne grup}">
												<c:set var="grup" value="${r.GRUP}"/>				
												</OPTGROUP>
											</c:if>
										</c:forEach>
									</select> 
								</td>
							</tr>
							<tr>
								<th>Cabang Platinum Save dan Smart Invest (BII)</th>
								<td>
									<select name="area2">
										<c:set var="grup" value=""/>
										<c:forEach var="r" items="${listABM2}" varStatus="st">
											<c:choose>
												<c:when test="${r.GRUP ne grup}">
													<c:set var="grup" value="${r.GRUP}"/>				 
													<OPTGROUP label="${r.GRUP}">
														<option value="${r.AREA}" <c:if test="${r.AREA eq area}">selected</c:if> >${r.NAMA_AREA}</option>
												</c:when>
												<c:otherwise>
													<option value="${r.AREA}" <c:if test="${r.AREA eq area}">selected</c:if> >${r.NAMA_AREA}</option>
												</c:otherwise>
											</c:choose>
											<c:if test="${r.GRUP ne grup}">
												<c:set var="grup" value="${r.GRUP}"/>				
												</OPTGROUP>
											</c:if>
										</c:forEach>
									</select> 
								</td>
							</tr>
							<tr>
								<th>Cabang Mayapada</th>
								<td>
									<select name="cab_mayapada">
										<c:forEach var="r" items="${listMayapada}" varStatus="st">
											<option value="${r.LCB_NO}" <c:if test="${r.LCB_NO eq cab_mayapada}">selected</c:if> >${r.NAMA_CABANG}</option>
										</c:forEach>
									</select> 
								</td>
							</tr>
							<tr>
								<th>Cabang UOB</th>
								<td>
									<select name="cab_uob">
										<c:forEach var="r" items="${listUOB}" varStatus="st">
											<option value="${r.LCB_NO}" <c:if test="${r.LCB_NO eq cab_uob}">selected</c:if> >${r.NAMA_CABANG}</option>
										</c:forEach>
									</select> 
								</td>
							</tr>
							<tr>
								<th>Tanggal Jatuh Tempo</th>
								<td>
									<script>inputDate('startDate', '${startDate}', false);</script> s/d 
									<script>inputDate('endDate', '${endDate}', false);</script>
								</td>
							</tr>
							<tr>
								<th>Tanggal Pengajuan</th>
								<td>
									<script>inputDate('ajuDate', '${ajuDate}', false);</script>
								</td>
							</tr>
							<tr>
								<th></th>
								<td>
									<c:if test="${produk eq \"155001\" or produk eq \"155002\" or produk eq \"155003\"}">
										<span><input type="checkbox" name="setPass"  title="set password"> Passsword</span>
									</c:if>
									
									<input type="submit" name="tarik" value="Tarik">
								</td>
							</tr>
							<tr>
								<th></th>
								<td>
									<table class="simple">
										<thead>
											<tr>
												<td>No.</td>
												<td>SPAJ</td>
												<td>Polis</td>
											</tr>
										</thead>
										<tbody>
									         <c:forEach var="d" items="${sessionScope.daftarSpajSphEmail}" varStatus="st">
												<tr>
													<td nowrap="nowrap">${st.count}.</td>
													<td nowrap="nowrap">${d.REG_SPAJ}</td>
													<td nowrap="nowrap">${d.MSPO_POLICY_NO_FORMAT}</td>
												</tr>
									         </c:forEach>
								         </tbody>
									</table>
								</td>
							</tr>
						</table>
					</fieldset>
					<fieldset>
						<legend>E-mail</legend>
						<table class="entry2">
							<tr>
								<th>E-mail FROM</th>
								<td>
									<input type="text" size="100" name="emailfrom" value="${emailfrom}">
								</td>
							</tr>
							<tr>
								<th>E-mail TO</th>
								<td>
									<input type="text" size="100" name="emailto" value="${emailto}">
									<br/><span class="info">* Gunakan tanda titik koma ( ; ) untuk memasukkan lebih dari satu e-mail</span>
								</td>
							</tr>
							<tr>
								<th>E-mail CC</th>
								<td>
									<input type="text" size="100" name="emailcc" value="${emailcc}">
									<br/><span class="info">* Gunakan tanda titik koma ( ; ) untuk memasukkan lebih dari satu e-mail</span>
								</td>
							</tr>
							<tr>
								<th>E-mail Subject</th>
								<td>
									<input type="text" size="100" name="emailsubject" value="${emailsubject}">
								</td>
							</tr>
							<tr>
								<th>E-mail Message</th>
								<td>
									<textarea rows="15" cols="100" name="emailmessage">${emailmessage}</textarea>
								</td>
							</tr>
							<tr>
								<th></th>
								<td>
									<input type="submit" name="send" value="Send">
									<c:if test="${not empty message}">
										<div id="error">${message}</div>
									</c:if>
								</td>
							</tr>							
						</table>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</form>
</body>
<script>
var pesan = '${message}';
if(pesan != '') alert(pesan);
</script>
<%@ include file="/include/page/footer.jsp"%>