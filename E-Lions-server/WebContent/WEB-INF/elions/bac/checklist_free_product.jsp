<%@ include file="/include/page/header_jquery.jsp"%>
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path}/include/js/default.js"></script>

<script type="text/javascript">
	hideLoadingMessage();
	
	//Perintah untuk menjalankan jQuery setelah seluruh dokumen selesai di load.
	$().ready(function(){
	
		// (jQueryUI Tabs) init tab2 Utama
		$("#tabs").tabs();

		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title], label[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
//  		$(".opt1, .opt2").hide();
		
		$("#checkAll").change(function(){
	        if(this.checked == true){
	             $(".chbox").attr("checked", true);
	             $(".datarow").css("background-color", "yellow");
// 	             $(".opt1, .opt2").show();
	        }else{
	             $(".chbox").attr("checked", false);
	             $(".datarow").css("background-color", "white");
// 	             $(".opt1, .opt2").hide();
// 	             $(".opt1").val("1");
// 	             $(".opt2").val("");
	        }
    	});
    	
    	$("#btnApprove").click(function(){
    		var n = $(".chbox:checked").length;
    		if(n==0){
    			alert("Harap pilih data yang akan diproses!");
    			return false;
    		}else{
    			return true;
    		}
    	});
    	
    	$("#btnReject").click(function(){
    		var n = $(".chbox:checked").length;
    		if(n==0){
    			alert("Harap pilih data yang akan diproses!");
    			return false;
    		}else{
    			return true;
    		}
    	});
    	
	});
	
	function klikcheckbox(index){
		if(document.getElementById("check[" + index + "]").checked == true){
			document.getElementById("row[" + index + "]").style.backgroundColor = "yellow";
// 			document.getElementById("lsjb_id[" + index + "]").style.display = '';
// 			document.getElementById("flag_merchant[" + index + "]").style.display = '';
		}else{
			document.getElementById("row[" + index + "]").style.backgroundColor = "white";
// 			document.getElementById("lsjb_id[" + index + "]").style.display = 'none';
// 			document.getElementById("flag_merchant[" + index + "]").style.display = 'none';
// 			document.getElementById("lsjb_id[" + index + "]").value = "1";
// 			document.getElementById("flag_merchant[" + index + "]").value = "";
		}
	}
	
</script>

	<style type="text/css">
		/* font utama */
		body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }
	
		/* fieldset */
		fieldset { margin-bottom: 1em; padding: 0.5em; }
		fieldset legend { width: 99%; }
		fieldset legend div { margin: 0.3em 0.5em; }
		fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
		fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
		fieldset .rowElem .jtable { position: relative; left: 12.5em; }
	
		/* tanda bintang (*) untuk menandakan required field */
		em { color: red; font-weight: bold; }
	
		/* agar semua datepicker align center, dan ukurannya fix */
		.datepicker { text-align: center; width: 7em; }
	
		/* styling untuk client-side validation error message */
		#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }
	
		/* styling untuk label khusus checkbox dan radio didalam rowElement */
		fieldset .rowElem label.radioLabel { width: auto; }
		
		/* lebar untuk form elements */
		.lebar { width: 7em; }
		
		/* untuk align center */
		.tengah { text-align: center; }
		
		/* untuk warna kolom input */
		.warna { color: grey; }
			
		/* styling untuk server-side validation error */
		.errorField { border: 1px solid red; }
		.errorMessage { color: red; display: block;}
		
		#tab1 td, #tab1 th { padding: 2px; }
		
		#tab1 th { background-color: #eee; }
		
	</style>
	
	<body>
		<form:form method="post" name="formpost" id="formpost" commandName="cmd">
			<fieldset class="ui-widget ui-widget-content">
				<legend class="ui-widget-header ui-corner-all"><div>Proses Checklist Free Produk</div></legend>
				<br><br>
				
				<table id="tab3">
					<tr>
						<th colspan = 2>
						</th>						
						<th >
							<label> Kantor Wilayah 7</label>
						</th>
						<td >
							&nbsp;&nbsp;
						</td>
						<th >
							<label>Kantor Wilayah 11</label>
						</th>
						
					</tr>
					<tr>
						<td>
							<label><b>Jumlah SPAJ yang di Input</b></label>
						</td>
						<td>
							:
						</td>
						<td align="center"> ${cmd.jum_spaj_kw7_all}</td>
						<td >
							&nbsp;
						</td>						
						<td align="center">  ${cmd.jum_spaj_kw11_all}</td>
					</tr>
					<tr>
						<td>
							<label><b>Jumlah SPAJ yang di Approve</b></label>
						</td>
						<td>
							:
						</td>
						<td align="center"> ${cmd.jum_spaj_kw7_approve}</td>
						<td >
							&nbsp;
						</td>						
						<td align="center"> ${cmd.jum_spaj_kw11_approve}</td>
					</tr>
					<tr>
						<td>
							<label><b>Jumlah SPAJ yang di Tolak</b></label>
						</td>
						<td>
							:
						</td>
						<td align="center"> ${cmd.jum_spaj_kw7_reject}</td>
						<td >
							&nbsp;
						</td>						
						<td align="center"> ${cmd.jum_spaj_kw11_reject}</td>
					</tr>
					<tr>
						<td>
							<label><b>Jumlah SPAJ yang menunggu Approval</b></label>
						</td>
						<td>
							:
						</td>
						<td align="center"> ${cmd.jum_spaj_kw7_waiting}</td>
						<td >
							&nbsp;
						</td>						
						<td align="center">${cmd.jum_spaj_kw11_waiting}</td>
					</tr>
				</table>
				
				<table id="tab0">
					<tr>
						<td>
							<form:errors path="pesan" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<td><br></td>
					</tr>
				</table>
				<table class="displaytag" id="tab1">
					<tr>
						<th>
<!-- 							Check All<br> -->
<!-- 							<input type="checkbox" id="checkAll" style="border: none"> -->
						</th>
						<th nowrap>No</th>
						<th nowrap>Tgl Akseptasi (Inforce)</th>
						<th nowrap>SPAJ Utama</th>
						<th nowrap>Posisi SPAJ Utama</th>
						<th nowrap>SPAJ Free</th>
						<th nowrap>Posisi SPAJ Free</th>
						<th>Kurs</th>
						<th width="75px">APE</th>
						<th nowrap>Kantor Wilayah</th>
						<th nowrap>Cabang</th>
						<th nowrap>Status Promo</th>
					</tr>
					<c:forEach items="${cmd.listSpaj}" var="d" varStatus="st">
						<tr class="datarow" id="row[${st.index}]">
							<td class="center" <c:if test="${d.process_type eq 1 or d.process_type eq 2 }"> disabled </c:if>>
								<form:checkbox path="listSpaj[${st.index}].check" value="1" cssClass="chbox noBorder" id="check[${st.index}]"
									onclick="klikcheckbox('${st.index}')"/>
							</td>
							<td class="center" nowrap>${st.index+1}</td>
							<td class="left" nowrap>${d.tgl_transfer_uw_print}</td>
							<td class="left" nowrap>${d.spaj_utama}</td>
							<td class="left" nowrap>${d.posisi_spaj_utama}</td>
							<td class="left" nowrap>${d.spaj_free}</td>
							<td class="left" nowrap>${d.posisi_spaj_free}</td>
							<td class="left">
								<c:choose>
									<c:when test="${d.kurs eq '01'}">
										Rp
									</c:when>
									<c:otherwise>
										USD
									</c:otherwise>
								</c:choose>
							</td>
							<td>${d.nilai_ape}</td>
							<td class="left">${d.nama_kanwil}</td>
							<td class="left">${d.nama_cabang}</td>
							<td class="left">${d.status}</td>
						</tr>
					</c:forEach>
				</table>
				<br>
				<table id="tab2">
					<tr>
						<td>
							<input type="submit" name="btnReject" id="btnReject" title="Reject" value="Reject">
						</td>
						<td>
							<input type="submit" name="btnApprove" id="btnApprove" title="Approve" value="Approve">
						</td>
					</tr>
				</table>
			</fieldset>
		</form:form>
	</body>
	
</html>