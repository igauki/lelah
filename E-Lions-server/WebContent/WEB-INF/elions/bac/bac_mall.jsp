<%@ include file="/include/page/header_mall.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	hideLoadingMessage();
	
	function stat(){
		spaj = document.frmParam.spaj.value;
		if(spaj==''){
			alert('Harap cari SPAJ terlebih dahulu');
		}else{
			createLoadingMessage();
			document.getElementById('infoFrame').src='${path }/uw/status.htm?pos=1&spaj='+spaj;
		}
	}
	
	function disabledTransferButton(){
		var spajListDisabled = document.getElementById('spajListDisabledForTransfer').value;
		if( spajListDisabled.match(spaj) )
		{
			document.getElementById('transfer').disabled = false;
		}
		else 
		{
			document.getElementById('transfer').disabled = true;
		}
	}
	
	function buttonLinks(str){
		if(str=='cari'){
			spaj = document.frmParam.spaj.value;
			if(spaj==''){
				alert('Harap cari SPAJ terlebih dahulu');
			}else{
				var spajList4EditButton = document.getElementById('spajList4EditButton').value;
				var spajListDisabled = document.getElementById('spajListDisabledForTransfer').value;
				var ldeId = document.getElementById('ldeId').value;
				var jnBank = document.getElementById('jnBank').value;
				if(jnBank ==2)
				{
					if( ldeId == 11 )
					{
						document.getElementById('transfer').disabled = false;
					}
					else
					{
						if( spajListDisabled.match(spaj) )
						{
							document.getElementById('transfer').disabled = false;
						}
						else 
						{
							document.getElementById('transfer').disabled = true;
						}
					}
				}
				if( spajList4EditButton.match(spaj) )
				{
					alert('Maaf, Spaj tidak bisa diedit krn sudah diotorisasi / approved');
				}
				else
				{
					document.getElementById('infoFrame').src='${path}/bac/editmall.htm?data_baru=true&showSPAJ='+document.frmParam.spaj.value;
				}
				
			}
		}else{
			spaj = document.frmParam.spaj.value;
			if(spaj==''){
				alert('Harap cari SPAJ terlebih dahulu');
			}else{
			
				switch (str) {
				case "titipanpremi" :
					//validasi akses titipan premi, ada di controller titipan premi
					popWin('${path}/ttppremi/titipan_premi.htm?sts=insert&editSPAJ='+spaj, 500, 800);
					
					break;
				case "hcp" :
					if (document.frmParam.kodebisnis.value != 161)
					{
						ajaxManager.CountPeserta(spaj , 
							{callback:function(value) {
									DWRUtil.useLoadingMessage();
									if(value>0){
										alert("Sebelum mengisi data tertanggung tambahan, Silakan dipastikan terlebih dahulu data tertanggung utamanya sudah diisi dengan benar.");
										popWin('${path}/bac/ttghcp_mall.htm?sts=insert&showSPAJ='+spaj, 500, 800);
									}else{ 
										alert("Tidak bisa mengisi data Smart Medicare/Eka Sehat/HCP Family karena belum mengambil plan BASIC, Silahkan melakukan penginputan plan BASIC pada halaman penginputan spaj utama terlebih dahulu.");
										
									}	
							 	},
							  timeout:15000,
							  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
						});
						
						//if(document.frmParam.kodebisnis.value==189 || document.frmParam.kodebisnis.value==183 || document.frmParam.kodebisnis.value==178 || document.frmParam.kodebisnis.value==193){
						//		alert("Sebelum mengisi data tertanggung tambahan, Silakan dipastikan terlebih dahulu data tertanggung utamanya sudah diisi dengan benar.");
						//		popWin('${path}/bac/ttghcp_mall.htm?sts=insert&showSPAJ='+spaj, 500, 800);
						//}else {
						//	if (eval(document.frmParam.jml_peserta.value) > 0){
						//		alert("Sebelum mengisi data tertanggung tambahan, Silakan dipastikan terlebih dahulu data tertanggung utamanya sudah diisi dengan benar.");
						//		popWin('${path}/bac/ttghcp_mall.htm?sts=insert&showSPAJ='+spaj, 500, 800);
						//	}else{
						//		alert("Tidak bisa mengisi data Smart Medicare/Eka Sehat/HCP Family karena belum mengambil plan BASIC, Silahkan melakukan penginputan plan BASIC pada halaman penginputan spaj utama terlebih dahulu.");
						//	}
						//}
					}else{
						alert("Produk ini tidak bisa mengisi data Smart Medicare/Eka Sehat/HCP Family");
					}
					break;		
				case "info" :
					document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj;
					break;		
				case "reffbii" :
					if(confirm("Apakah anda akan input referral others BII?\nApabila anda adalah user BANK SINARMAS / ASURANSI SINARMAS / SINARMAS SEKURITAS, harap pilih CANCEL!")){
						//alert('${path}/bac/reff_bank.htm?window=main&reffothers=y&spaj='+spaj);
						popWin('${path}/bac/reff_bank.htm?window=main&reffothers=y&spaj='+spaj, 400, 700);
					}else{
						//alert('${path}/bac/reff_bank.htm?window=main&reffothers=n&spaj='+spaj);
						popWin('${path}/bac/reff_bank.htm?window=main&reffothers=n&spaj='+spaj, 400, 700);
					}
					break;
				case "uwinfo" :
					document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=uwinfo&spaj='+spaj;
					break;
				case "view" :
					popWin('${path}/uw/viewer.htm?window=viewerkontrol&spaj='+spaj, 400, 550);
					break;
				case "checklist" :
					document.getElementById('infoFrame').src = '${path}/checklist.htm?lspd_id=1&reg_spaj='+spaj;
					break;
				case "editagen" :
					popWin('${path}/bac/editagenpenutup.htm?spaj='+spaj, 500, 700);
					break;
				case "download_spaj" :
					spaj = document.frmParam.spaj.value;
					document.getElementById('infoFrame').src='${path}/bac/multi.htm?window=download_spaj&spaj='+spaj;
					break;		
				case "medis" :
					spaj = document.frmParam.spaj.value;
					document.getElementById('infoFrame').src='${path}/uw/medical_new.htm?spaj='+spaj;
					break;
				case "med_quest" :
					spaj = document.frmParam.spaj.value;
					document.getElementById('infoFrame').src='${path}/uw/medical_quest.htm?spaj='+spaj;
					break;	
				case "espaj" :
					spaj = document.frmParam.spaj.value;
					lus_id = document.frmParam.lus_id.value;
					ajaxManager.ValidateESpaj(spaj , lus_id,
							{callback:function(pesan) {
									DWRUtil.useLoadingMessage();
									if(pesan==''){
										window.open('${path}/reports/espaj.pdf?spaj='+spaj, 600, 800);
									}else{ 
										alert(pesan);
									}	
							 	},
							  timeout:15000,
							  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
						});
				}
				
			}
		}
	}

	
	function trans(){
		spaj = document.frmParam.spaj.value;
		if(spaj==''){
			alert('Harap cari SPAJ terlebih dahulu');
		}else{
			//if (document.frmParam.jn_bank.value == 2 || document.frmParam.jn_bank.value == 3){
			//if (document.frmParam.jn_bank.value == 3){
			//	document.getElementById('infoFrame').src='${path}/bac/transferbactouwbanksinarmas.htm?spaj='+document.frmParam.spaj.value; 
			//}else{
			document.getElementById('infoFrame').src='${path}/bac/transferbactouw.htm?spaj='+document.frmParam.spaj.value; 			
			//}
		}
	}

	function awal(){
		cariregion(document.frmParam.spaj.value,'region');
		setFrameSize('infoFrame', 64);
	}
	
	function cariregion(spaj,nama)
	{
			var spajListDisabled = document.getElementById('spajListDisabledForTransfer').value;
			var ldeId = document.getElementById('ldeId').value;
			var jnBank = document.getElementById('jnBank').value;
			if( jnBank == 2 )
			{
				if( ldeId == 11 )
				{
					document.getElementById('transfer').disabled = false;
				}
				else
				{
					if( spajListDisabled.match(spaj) )
					{
						document.getElementById('transfer').disabled = false;
					}
					else 
					{
						document.getElementById('transfer').disabled = true;
					}
				}
			}
			ajaxManager.listcollect(spaj , nama,
			{callback:function(map) {
		
				DWRUtil.useLoadingMessage();
				document.frmParam.koderegion.value=map.LSRG_NAMA;
				document.frmParam.kodebisnis.value = map.LSBS_ID;
				document.frmParam.numberbisnis.value = map.LSDBS_NUMBER;
				document.frmParam.jml_peserta.value = map.jml_peserta;
				document.frmParam.cab_bank.value=map.CAB_BANK;
				document.frmParam.jn_bank.value=map.JN_BANK;
				document.frmParam.lus_id.value = map.LUS_ID;
			   },
			  timeout:15000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}

	function copyToClipboard(txt){
	
		if( window.clipboardData && clipboardData.setData ){
		
			clipboardData.setData("Text", txt);
		}
		else{
		 alert("Internet Explorer required"); 
		}
	}
	
	function run(){

		spaj = document.frmParam.spaj.value;
		if(spaj==''){
			alert('Harap cari SPAJ terlebih dahulu');
		}else{
			var shell = new ActiveXObject("WScript.Shell"); 
			var commandtoRun = "\\\\aplikasi\\lions\\secan4400f.exe";
		   
			if (shell){		
			 shell.run(commandtoRun); 
			} 
			else{ 
				alert("program or file doesn't exist on your system."); 
			}
		}

	}
	
	function copyAndRun(){ 
	 	txt = document.getElementById("spaj").value;
	 	copyToClipboard(txt);
	 	run();		
	}	
</script>
<body 
	onload="awal(); setFrameSize('infoFrame', 85);"
	onresize="setFrameSize('infoFrame', 85);" style="height: 100%;">

<form name="frmParam" method="post">
<div class="tabcontent">
<table class="entry2" style="width: 98.4%;border: 0px;">
	<tr>
		<th>SPAJ</th>
		<td>
			<input type="hidden" name="jenis_pemegang_polis" id="jenis_pemegang_polis"  value=""/>
			<input type="button" value="Input" name="info" id="info"
				onclick="document.getElementById('infoFrame').src='${path}/bac/editmall.htm?data_baru=true&jenis_pemegang_polis='+document.getElementById('jenis_pemegang_polis').value;" 
				accesskey="I" onmouseover="return overlib('Input SPAJ Baru', AUTOSTATUS, WRAP);" onmouseout="nd();"
				<c:if test="${sessionScope.currentUser.flag_approve eq 1}"> disabled="true" </c:if>>
			

		
			<select name="spaj" onChange="cariregion(this.options[this.selectedIndex].value,'region');" id="spaj">
				<option value="">[--- Silahkan Pilih/Cari SPAJ ---]</option>
				<c:forEach var="s" items="${daftarSPAJ}">
					<option value="${s.REG_SPAJ }" style="background-color: ${s.OTORISASI_BG};" 
						<c:if test="${s.REG_SPAJ eq param.spaj }">selected<c:set var="ada" value="ada" /></c:if>>${s.SPAJ_FORMATTED}
					- ${s.POLICY_FORMATTED }</option>
				</c:forEach>
			</select>
			
			<input type="button" value="Cari" name="search"
				onclick="popWin('${path}/uw/spajgutri.htm?posisi=1&win=bacmall&spajList4EditButton=${spajList4EditButton}', 350, 450);"
				accesskey="C" onmouseover="return overlib('Cari SPAJ', AUTOSTATUS, WRAP);" onmouseout="nd();">
				 <!-- &search=yes -->

			<input type="button" value="Edit" name="info" onclick="return buttonLinks('cari');" 
				accesskey="T" onmouseover="return overlib('Edit SPAJ', AUTOSTATUS, WRAP);" onmouseout="nd();"
				<c:if test="${sessionScope.currentUser.jn_bank eq 2 and sessionScope.currentUser.flag_approve eq 1}"> disabled="true" </c:if>>
				<!--${path }/uw/view.htm?showSPAJ='+document.frmParam.spaj.options[document.frmParam.spaj.selectedIndex].value;"> document.getElementById('infoFrame').src='${path}/bac/edit.htmshowSPAJ='+spaj; -->			

			<c:if test="${empty sessionScope.currentUser.cab_bank}">
				<input type="hidden" value="U/W Info" name="uwinfo"
					onclick="return buttonLinks('uwinfo');"
					accesskey="W" onmouseover="return overlib('U/W Info', AUTOSTATUS, WRAP);" onmouseout="nd();">
					
			</c:if>
			
		</td>
	</tr>
	<tr>
		<th rowspan="2">Proses</th>
		<td colspan="2">
			<input type="hidden" name="koderegion" >
			<input type="hidden" id="ldeId" value="${ldeId}" >
			
			<input type="hidden" name="kodebisnis" >
			<input type="hidden" name="numberbisnis" >
			<input type="hidden" name="jml_peserta">
			<input type="hidden" name="lus_id">
			<input type="hidden" name="cab_bank">
			<input type="hidden" name="jn_bank">
			<input type="hidden" name="user_id" value= "${user_id}">
			<input type="hidden" name="cabang_bank" value ="${cabang_bank}">
			<input type="button" value="Upload" name="upload" disabled="disabled" style="display: none;"
			onclick="document.getElementById('infoFrame').src='${path}/bac/upload.htm';" 
			accesskey="U" onmouseover="return overlib('Upload Polis Via Excel', AUTOSTATUS, WRAP);" onmouseout="nd();">
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="button" value="Info" name="ucup"	onclick="return buttonLinks('info');" 
				 onmouseover="return overlib('Info Lengkap Dokumen Polis', AUTOSTATUS, WRAP);" onmouseout="nd();">			
				 			
			<input type="button" value="Checklist" name="checklist"	onclick="return buttonLinks('checklist');" 
				 onmouseover="return overlib('Checklist Dokumen Polis', AUTOSTATUS, WRAP);" onmouseout="nd();">
			 
			<input name="btn_med_quest" type="button" value="PERNYATAAN KESEHATAN" onClick="return buttonLinks('med_quest');" accesskey="Q" 
				 onmouseover="return overlib('Pernyataan Kesehatan', AUTOSTATUS, WRAP);" onmouseout="nd();" style="margin-top: 3px">
			
			<!--<input type="button" value="EKA SEHAT/HCP FAMILY" name="hcp"	onclick="return buttonLinks('hcp');" 
					accesskey="P" onmouseover="return overlib('HCP FAMILY', AUTOSTATUS, WRAP);" onmouseout="nd();"> -->
			
			<input name="btn_espaj" type="button" value="E-SPAJ" onClick="return buttonLinks('espaj');" accesskey="S" 
				 onmouseover="return overlib('E-SPAJ', AUTOSTATUS, WRAP);" onmouseout="nd();" style="margin-top: 3px">
			
			<input type="button" value="Transfer" name="transfer" onclick="trans();" id="transfer"
				accesskey="F" onmouseover="return overlib('Transfer', AUTOSTATUS, WRAP);" onmouseout="nd();"
				<c:if test="${sessionScope.currentUser.jn_bank eq 3 and sessionScope.currentUser.flag_approve eq 0}"> disabled="true" </c:if>>	 
		</td>
	</tr>

	<tr>
		<td colspan="2">
			<c:choose>
				<c:when test="${sessionScope.currentUser.flag_approve eq 1}">
					<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame" frameborder="0"
						width="100%" style="width: 100%;" height="2000px"> Please Wait... </iframe>
				</c:when>
				<c:otherwise>
					<iframe src="${path}/bac/editmall.htm?data_baru=true" name="infoFrame" id="infoFrame"
						 frameborder="0" width="100%" style="width: 100%;" height="2000px"> Please Wait... </iframe>
				</c:otherwise>
			</c:choose>
			
<input type="hidden" value="${spajList}" id="spajListDisabledForTransfer" />
<input type="hidden" value="${spajList4EditButton}" id="spajList4EditButton" />
<input type="hidden" value="${jnBank}" id="jnBank" />
		</td>
	</tr>
</table>
</div>
</form>

</body>
<%@ include file="/include/page/footer.jsp"%>