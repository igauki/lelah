<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<!-- DatePicker Script (jscalendar) -->
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	function cari(){
		if(trim(document.formpost.kata.value)=='') return false;
		else createLoadingMessage();	
	}
	
	function backToParent1(spaj, polis, region)
	{
			ajaxManager.listcollect(spaj , 'region',
			{callback:function(map) {
		
				DWRUtil.useLoadingMessage();
				document.formpost.koderegion.value=map.LSRG_NAMA;
				document.formpost.kodebisnis.value = map.LSBS_ID;
				document.formpost.numberbisnis.value = map.LSDBS_NUMBER;
				document.formpost.jml_peserta.value = map.jml_peserta;
				backToParent(spaj, polis, map.LSRG_NAMA,map.LSBS_ID,map.LSDBS_NUMBER,map.jml_peserta)
			   },
			  timeout:15000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
	function backToParent(spaj, polis, region,kodebisnis,numberbisnis,jml_peserta){
		var win = document.getElementById('win').value;
		var jn_bank = document.getElementById('jn_bank').value;
		//spaj = formatSpaj(spaj);
		//polis = formatPolis(polis);
		var retVal = spaj + " - " + polis;
		
		var dok;
		if(self.opener)
			dok = self.opener.document;
		else
			dok = parent.document;
		
		var forminput;
		if(dok.formpost) forminput = dok.formpost;
		else if(dok.frmParam) forminput = dok.frmParam;
		
		if('bac' == win){
			forminput.koderegion.value = region;
			forminput.kodebisnis.value = kodebisnis;
			forminput.numberbisnis.value = numberbisnis;
			forminput.jml_peserta.value = jml_peserta;
			addOptionToSelect(dok, forminput.spaj, retVal, spaj);
		}else if('komisi' == win){
			self.opener.ajaxPesan(spaj, 8);
		}else if('printpolis' == win){
			dok.getElementById('infoFrame').src='${path }/bac/otorisasiInputSpajView.htm?showSPAJ='+spaj;
		}else if('1' == win){
			if(self.opener)	{self.opener.ajaxPesan(spaj, -1);}
			else {parent.ajaxPesan(spaj, -1);}
		}else if('payment' == win){
			self.opener.ajaxPesan(spaj, 4);
		}else if('ttp' == win){
			//...
		}else if('uw' == win){
			forminput.koderegion.value = region;
			forminput.kodebisnis.value = kodebisnis;
			forminput.numberbisnis.value = numberbisnis;
			forminput.jml_peserta.value = jml_peserta;
			addOptionToSelect(dok, forminput.spaj, retVal, spaj);
		}
		if(forminput.spaj.type=='select-one'){
			addOptionToSelect(dok, forminput.spaj, retVal, spaj);
		}else
			dok.getElementById('spaj').value = spaj; 
		
		if(dok.getElementById('polis'))
			dok.getElementById('polis').value = polis;

		if( '3' == jn_bank && 'viewer' == win || '3' == jn_bank && 'viewerDanaSekuritas' == win )
		{
			dok.getElementById('infoFrame').src='${path}/bac/otorisasiInputSpajDanaSekuritasView.htm?p=v&showSPAJ='+spaj;
			//if(dok.getElementById('docFrame')) dok.getElementById('docFrame').src='${path}/common/util.htm?window=dokumen&spaj='+spaj;
			if(dok.getElementById('docFrame')) dok.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+spaj;
		}else if(('2'== jn_bank && 'viewer' == win)|| ('16'== jn_bank && 'viewer' == win))
		{
			dok.getElementById('infoFrame').src='${path}/bac/otorisasiInputSpajView.htm?p=v&showSPAJ='+spaj;
			//if(dok.getElementById('docFrame')) dok.getElementById('docFrame').src='${path}/common/util.htm?window=dokumen&spaj='+spaj;
			if(dok.getElementById('docFrame')) dok.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+spaj;
		}

		if('bac' == win){	
			dok.getElementById('infoFrame').src='${path}/bac/edit.htm?showSPAJ='+spaj; 
		}else if('uw' == win){
			if(dok.getElementById('docFrame')) dok.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+spaj;
			if(dok.getElementById('infoFrame')) dok.getElementById('infoFrame').src='${path }/bac/otorisasiInputSpajView.htm?showSPAJ='+spaj;			
		}else if('printpolis' == win){
			if(dok.getElementById('docFrame')) dok.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+spaj;			
		}
		
		if(self.opener) window.close();
	}

</script>
</head>
<BODY onload="resizeCenter(800, 600); document.title='PopUp :: Cari SPAJ'; setupPanes('container1', 'tab1'); " style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">List SPAJ</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" action="${path }/bac/spaj.htm" style="text-align: center;">
					<input type="hidden" name="win" id="win" value="${param.win}">
					<input type="hidden" name="posisi" value="${param.posisi}">
					<input type="hidden" name="jn_bank" id="jn_bank" value="${cmd.jn_bank}">
					<input type="hidden" name="koderegion" >
					<input type="hidden" name="kodebisnis" >
					<input type="hidden" name="numberbisnis" >
					<input type="hidden" name="jml_peserta">
					<table class="simple">
						<thead>
							<tr>
								<th style="text-align: center;">Distribusi</th>
								<th style="text-align: center;">No. SPAJ</th>
								<th style="text-align: center;">No. Polis</th>
								<th style="text-align: center;">Produk</th>
								<th style="text-align: center;">Pemegang Polis</th>
								<th style="text-align: center;">Tertanggung</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="spaj" items="${cmd.listSpaj}" varStatus="stat">
								<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
									onclick="backToParent1('${spaj.REG_SPAJ}', '${spaj.MSPO_POLICY_NO}' , '${spaj.LSRG_NAMA}');">
											<td>${spaj.DISTRIBUSI}</td>
											<td><elions:spaj nomor="${spaj.REG_SPAJ}"/></td>
											<td><elions:polis nomor="${spaj.MSPO_POLICY_NO}"/></td>
											<td>${spaj.LSDBS_NAME}</td>
											<td>${spaj.PEMEGANG}</td>
											<td>${spaj.TERTANGGUNG}</td>
								</tr>
								<c:set var="jml" value="${stat.count}"/>
								<c:if test="${stat.count eq 1}">
									<c:set var="v1" value="${spaj.REG_SPAJ}"/>
									<c:set var="v2" value="${spaj.MSPO_POLICY_NO}"/>
									<c:set var="v3" value="${spaj.LSRG_NAMA}"/>
								</c:if>
							</c:forEach>
						</tbody>
					</table>
					<br>
					<input type="hidden" name="flag" value="${cmd.flag }" >
					<input type="hidden" name="lssaId" value="${cmd.lssaId}" >
					<input type="hidden" name="lsspId" value="${cmd.lsspId}" >
					<input type="button" name="close" value="Close" onclick="window.close();"
						accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">

				</form>
			</div>
		</div>
	</div>

</form>
<c:if test="${jml eq 1}">
<script>backToParent1('${v1}', '${v2}' , '${v3}');</script>
</c:if>
</body>
<%@ include file="/include/page/footer.jsp"%>