<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript">
	hideLoadingMessage();
</script>
</head>
<BODY onload="resizeCenter(800,600); setupPanes('container1', 'tab1');" style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Cek Tertanggung</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" style="text-align: center;">
					<table class="entry2">
						<thead>
							<tr>
								<th>SPAJ</th>
								<th>Tertanggung</th>
								<th>Identitas</th>
								<th>Tgl Lahir</th>
								<th>Premi</th>
								<th>UP</th>
								<th>Produk</th>
								<th>Posisi Dokumen</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${daftar}" var="d">
								<tr>
									<td>${d.REG_SPAJ}</td>
									<td>${d.MCL_FIRST}</td>
									<td>${d.MSPE_NO_IDENTITY}</td>
									<td><fmt:formatDate value="${d.MSPE_DATE_BIRTH}" pattern="dd/MM/yyyy"/></td>
									<td><fmt:formatNumber value="${d.MSPR_PREMIUM}"/></td>
									<td><fmt:formatNumber value="${d.MSPR_TSI}"/></td>
									<td>[${d.LSBS_ID}-${d.LSDBS_NUMBER}] ${d.LSDBS_NAME}</td>
									<td>${d.LSPD_POSITION}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</form>
				<c:choose>
					<c:when test="${not empty daftar}">
						<div class="error">
							PERHATIAN! Ditemukan data dengan nama dan tanggal lahir sama.<br/> 
							Harap pastikan apakah orang ini sudah mempunyai Polis Stable Link sebelumnya.
						</div>
						<br/>
					</c:when>
					<c:otherwise>
						<div class="info">Data tidak ditemukan. Silahkan lanjutkan proses input.</div>
						<br/>
					</c:otherwise>
				</c:choose>
				<input type="button" value="Close" onclick="window.close();">
			</div>
		</div>
	</div>

</form>
</body>
</html>