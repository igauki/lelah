<%@ include file="/include/page/header_jquery.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />

<table class="entry">
	<tr>
	<th></th>
		<th>Nomor Registrasi (SPAJ) </th>
		<th><input type="reg_spaj"
			value="<elions:spaj nomor='${cmd.pemegang.reg_spaj}'/>"
			style='background-color :#D4D4D4' readOnly> &nbsp;&nbsp;&nbsp;&nbsp;
			<c:if test="${cmd.datausulan.flag_worksite_ekalife eq 1}"> Karyawan AJ Sinarmas</c:if>
			</th>
		<th>SPAJ</th>
		<th>
			<select name="pemegang.mste_spaj_asli">
				<option value="1" <c:if test="${cmd.pemegang.mste_spaj_asli eq 1}">selected="selected"</c:if>>ASLI</option>
				<option value="0" <c:if test="${cmd.pemegang.mste_spaj_asli eq 0}">selected="selected"</c:if>>FOTOKOPI/FAX</option>
			</select>
		</th>
	</tr>
	<tr>
		<th></th>
		<th>Medis</th>
		<th><select name="datausulan.mste_medical" >
			<c:forEach var="medis" items="${select_medis}">
				<option
					<c:if test="${cmd.datausulan.mste_medical eq medis.ID}"> SELECTED </c:if>
					value="${medis.ID}">${medis.MEDIS}</option>
			</c:forEach>
		</select><font color="#CC3300">*</font>
		</th>
		<th>Nomor Seri</th>
		<th><spring:bind path="cmd.pemegang.mspo_no_blanko">
			<input type="text" name="${status.expression}" <c:if test="${cmd.currentUser.lus_id eq \"2661\"}"> disabled </c:if>
				value="${status.value }" size="22" maxlength="21"
			<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
				<font color="#CC3300">*</font>
		</spring:bind></th>
	</tr>
	<tr>
		<th></th>
		<th>NO PB</th>
		<th ><spring:bind path="cmd.no_pb">
							<input type="text" name="${status.expression}"
								value="${status.value }" size="20" 
								maxlength="100"
			<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>
							>
							<font color="#CC3300">*</font>
						</spring:bind>
						</th>
		<th>NO CIF/ID MEMBER S.Prioritas</th>
		<th ><spring:bind path="cmd.pemegang.mspo_nasabah_dcif">
							<input type="text" name="${status.expression}"
								value="${status.value }" size="20" 
								maxlength="100"
			<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>
							>
							<font color="#CC3300">*</font>
						</spring:bind>
						</th>
	</tr>
	<c:if test="${cmd.currentUser.lca_id ne \"09\" or cmd.currentUser.lca_id ne \"01\"}">
	<tr>
		<th></th>
		<th>CARI SUMBER BISNIS</th>
		<th colspan="3">
			<input type="text" name="carisumber" onkeypress="if(event.keyCode==13){ document.frmParam.btnsumber.click(); return false;}"> 
            		<input type="button" name="btnsumber" value="Cari" onclick="ajaxSelectWithParam1a(document.frmParam.carisumber.value,'select_sumberBisnis','sumber','pemegang.sumber_id','', 'SUMBER_ID', 'NAMA_SUMBER', '','Silahkan pilih SUMBER BISNIS','3');"> 
		</th>
		
	</tr>
	<tr>
		<th></th><th></th>
		<th colspan="3">
			<div id="sumber"> 
				<select name="pemegang.sumber_id" 
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
					 </c:if>>
	                 <option value="${cmd.pemegang.sumber_id}">${cmd.pemegang.nama_sumber}</option>
	            </select>
            </div>
		</th>
	</tr>
	</c:if>
	<tr>
	
	<tr>
		<th colspan="5" class="subtitle">IDENTITAS CALON PEMEGANG POLIS (wajib diisi)</th>
	</tr>
	<tr>
		<th width="20">1.</th>
		<th width="280">Nama Lengkap<br>
			<span class="info">(sesuai dengan KTP / Identitas lain tanpa gelar)</span>
		</th>
		<th>
			<spring:bind path="cmd.pemegang.lti_id">
				<select  name="${status.expression}" >
					<c:forEach var="l" items="${select_gelar}">
						<option
							<c:if test="${status.value eq l.key}"> SELECTED </c:if>
							value="${l.key}">${l.value}</option>
					</c:forEach>
				</select>
			</spring:bind>			
			<spring:bind path="cmd.pemegang.mcl_first">
				<input type="text" name="${status.expression}" value="${status.value }" size="42" maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				<font color="#CC3300">*</font>
				<c:if test="${not empty status.errorMessage}">
					<div style="color: red">
						${status.errorMessage}
					</div>
				</c:if>
			</spring:bind>
		</th>
		<th width="155">Gelar &nbsp;</th>
		<th>
			<spring:bind path="cmd.pemegang.mcl_gelar">
				<input type="text" name="${status.expression}" value="${status.value }" size="20" maxlength="20">
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th >2.</th>
		<th>Nama Alias (bila ada)</th>
		<th colspan="3"><input type="text" id="alias" name="alias" /></th>
	</tr>
	<tr>
		<th >3.</th>
		<th>Nama Ibu Kandung</th>
		<th colspan="3">
			<spring:bind path="cmd.pemegang.mspe_mother">
				<input type="text" name="${status.expression}" value="${status.value }" size="42" maxlength="50">
				<font color="#CC3300">*</font>
				<c:if test="${not empty status.errorMessage}">
					<div style="color: red">
						${status.errorMessage}
					</div>
				</c:if>
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th >4.</th>
		<th>Kewarganegaraan</th>
		<th colspan="3">
			<spring:bind path="cmd.pemegang.lsne_id">
				<select name="${status.expression}">
					<c:forEach var="d" items="${select_negara}">
						<option
							<c:if test="${status.value eq d.key}"> SELECTED </c:if>
							value="${d.key}">${d.value}</option>
					</c:forEach>
				</select>
				<font color="#CC3300">*</font>
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th >5.</th>
		<th>Apakah Anda waga negara Amerika Serikat? Pemegang Green Card?</th>
		<th colspan="3">
			<input type="radio" class=noBorder name="green_card" id="green_card1" />Ya 
			<input type="radio" class=noBorder name="green_card" id="green_card2" />Tidak 
		</th>
	</tr>
	<tr>
		<th >6.</th>
		<th>Bukti Identitas</th>
		<th colspan="3">
			<spring:bind path="cmd.pemegang.lside_id">
				<select name="${status.expression}" onChange="bixx(this.value);">
					<c:forEach var="d" items="${select_identitas}">
						<option
							<c:if test="${status.value eq d.key}"> SELECTED </c:if>
							value="${d.key}">${d.value}</option>
					</c:forEach>
				</select>
				<font color="#CC3300">*</font>
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th >7.</th>
		<th>No. KTP/PASPOR/KITAS/SIM</th>
		<th>
			<spring:bind path="cmd.pemegang.mspe_no_identity">
				<input type="text" name="${status.expression}" value="${status.value }" size="42" maxlength="50"> 
				<font color="#CC3300">*</font>
				<c:if test="${not empty status.errorMessage}">
					<div style="color: red">
						${status.errorMessage}
					</div>
				</c:if>
			</spring:bind>
		</th>
		<th>Tanggal berlaku s/d</th>
		<th>
			<spring:bind path="cmd.pemegang.mspe_no_identity_expired">
				<input type="text" id="${status.expression}" name="${status.expression}" value="${status.value}" class="datepicker" readonly="readonly" />
				<font color="#CC3300">*</font>
				<c:if test="${not empty status.errorMessage}">
					<div style="color: red">
						${status.errorMessage}
					</div>
				</c:if>
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th >8.</th>
		<th>Jenis Kelamin</th>
		<th>
			<spring:bind path="cmd.pemegang.mspe_sex">
				<label for="cowok"> <input type="radio" class=noBorder
					name="${status.expression}" value="1"
					<c:if test="${cmd.pemegang.mspe_sex eq 1 or cmd.pemegang.mspe_sex eq null}"> 
								checked</c:if>
					id="cowok">Pria </label>
				<label for="cewek"> <input type="radio" class=noBorder
					name="${status.expression}" value="0"
					<c:if test="${cmd.pemegang.mspe_sex eq 0}"> 
								checked</c:if>
					id="cewek">Wanita <font color="#CC3300">*</font></label>
			</spring:bind>
		</th>
		<th>Status Pernikahan</th>
		<th>
			<spring:bind path="cmd.pemegang.mspe_sts_mrt">
				<select  name="${status.expression}" >
					<c:forEach var="marital" items="${select_marital}">
						<option
							<c:if test="${status.value eq marital.key}"> SELECTED </c:if>
							value="${marital.key}">${marital.value}</option>
					</c:forEach>
				</select>
			</spring:bind>
			<font color="#CC3300">*</font>
		</th>
	</tr>
	<tr>
		<th >9.</th>
		<th>Kota dan Negara Kelahiran, Tgl Lahir</th>
		<th>
			<spring:bind path="cmd.pemegang.mspe_place_birth">
				<input type="text" name="${status.expression}" value="${status.value }" size="42" maxlength="30">
				<font color="#CC3300">*</font>
				<c:if test="${ not empty status.errorMessage}">
					<div style="color: red">
						${status.errorMessage}
					</div>
				</c:if>
			</spring:bind>
			<spring:bind path="cmd.pemegang.mspe_date_birth">
				<input type="text" id="bdate_1" name="${status.expression}" value="${status.value}" class="datepicker" readonly="readonly" onchange="bdate1(this.value)" />
				<font color="#CC3300">*</font>
				<c:if test="${ not empty status.errorMessage}">
					<div style="color: red">
						${status.errorMessage}
					</div>
				</c:if>
			</spring:bind>
		</th>
		<th>Usia</th>
		<th>
			<spring:bind path="cmd.pemegang.mste_age">
				<input style='background-color :#D4D4D4' class ="age_1" type="dis" name="${status.expression}" value="${status.value }" 
				size="4" readOnly>
			</spring:bind>Tahun
		</th>
	</tr>
	<tr>
		<th >a.</th>
		<th>Nama Suami/Istri</th>
		<th>
			<spring:bind path="cmd.pemegang.nama_si">
				<input type="text" name="${status.expression}"
					value="${status.value }" size="42" maxlength="30"
				<c:if test="${ not empty status.errorMessage}">
					style='background-color :#FFE1FD'
				</c:if>>
			</spring:bind>
		</th>
		<th>Tanggal Lahir</th>
		<th>
			<spring:bind path="cmd.pemegang.tgllhr_si">
				<input type="text" id="${status.expression}" name="${status.expression}" value="${status.value}" class="datepicker" readonly="readonly" />
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th >b.</th>
		<th>Nama Anak 1</th>
		<th>
			<spring:bind path="cmd.pemegang.nama_anak1">
				<input type="text" name="${status.expression}"
					value="${status.value }" size="42" maxlength="50"
				<c:if test="${ not empty status.errorMessage}">
					style='background-color :#FFE1FD'
				</c:if>>
			</spring:bind>
		</th>
		<th>Tanggal Lahir</th>
		<th>
			<spring:bind path="cmd.pemegang.tgllhr_anak1">
				<input type="text" id="${status.expression}" name="${status.expression}" value="${status.value}" class="datepicker" readonly="readonly" />
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th >c.</th>
		<th>Nama Anak 2</th>
		<th>
			<spring:bind path="cmd.pemegang.nama_anak2">
				<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="50"
				<c:if test="${ not empty status.errorMessage}">
					style='background-color :#FFE1FD'
				</c:if>>
			</spring:bind>
		</th>
		<th>Tanggal Lahir</th>
		<th>
			<spring:bind path="cmd.pemegang.tgllhr_anak2">
				<input type="text" id="${status.expression}" name="${status.expression}" value="${status.value}" class="datepicker" readonly="readonly" />
			</spring:bind>
		</th>
	</tr>	
	<tr>
		<th >D.</th>
		<th>Nama Anak 3</th>
		<th><spring:bind path="cmd.pemegang.nama_anak3">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="50"
			<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
		<th>Tanggal Lahir</th>
		<th>
			<spring:bind path="cmd.pemegang.tgllhr_anak3">
				<input type="text" id="${status.expression}" name="${status.expression}" value="${status.value}" class="datepicker" readonly="readonly" />
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th >10.</th>
		<th>Agama</th>
		<th>
			<spring:bind path="cmd.pemegang.lsag_id">
				<select name="${status.expression}" onchange="agamaChange(this.value);">
					<c:forEach var="d" items="${select_agama}">
						<option
							<c:if test="${status.value eq d.key}"> SELECTED </c:if>
							value="${d.key}">${d.value}</option>
					</c:forEach>
				</select>
				<font color="#CC3300">*</font>
			</spring:bind>
		</th>
		<th>Lain-lain, sebutkan </th>
		<th>
			<spring:bind path="cmd.pemegang.mcl_agama">
				<input class='agama' type="dis" name="${status.expression}"
					value="${status.value }" size="25" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
					style='background-color :#FFE1FD'
				</c:if>>
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th >11.</th>
		<th>Pendidikan</th>
		<th colspan="3">
			<spring:bind path="cmd.pemegang.lsed_id">
				<select name="${status.expression}">
					<c:forEach var="d" items="${select_pendidikan}">
						<option
							<c:if test="${status.value eq d.key}"> SELECTED </c:if>
							value="${d.key}">${d.value}</option>
					</c:forEach>
				</select>
				<font color="#CC3300">*</font>
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th >12.</th>
		<th>Nama Lembaga/Perusahaan Tempat Bekerja</th>
		<th colspan="3">
		<%-- <spring:bind path="cmd.pemegang.mcl_first">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" 
				maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
					style='background-color :#FFE1FD'
				</c:if>>
			<font color="#CC3300">*</font>
		</spring:bind> --%>
		</th>
	</tr>
	<tr>
		<th >13.</th>
		<th>Klasifikasi Pekerjaan</th>
		<th>
		<spring:bind path="cmd.pemegang.mkl_kerja">
			<select style="width: 400px" name="${status.expression}" onChange="klas_kerja(this.value)">
				<option value=0>Lain-Lain</option>
				<c:forEach var="kerja" items="${select_pekerjaan}">
					<option
						<c:if test="${status.value eq kerja.key}"> SELECTED </c:if>
						value="${kerja.key}">${kerja.value}</option>
				</c:forEach>
			</select>
		</spring:bind>
		
		 <font color="#CC3300">*</font>
		</th>
		<th>Lain-Lain, Sebutkan</th>
		<th><spring:bind path="cmd.pemegang.kerjaa">
			<input id="lainKlasKerja" type="dis" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th >14.</th>
		<th title="* Jika Pemegang Polis memiliki lebih dari 1 (satu)pekerjaan mohon dicantumkan semua di formulir Perubahan dan atau Penambahan Data SPAJ">
		Pekerjaan* dan /Jabatan</th>
		<th><spring:bind path="cmd.pemegang.kerjab">
				<input type="text" name="${status.expression}"
					value="${status.value }" size="42" maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
					style='background-color :#FFE1FD'
					</c:if>	
				>
			</spring:bind>
		</th>
		<th>Uraian Tugas</th>
		<th>
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
				</c:if>	
			>
		</th>
	</tr>
	<tr>
		<th >15.</th>
		<th>Klasifikasi Bidang Industri</th>
		<th>
			<select name="pemegang.mkl_industri" onChange="bidIndustri1(this.value)" >
				<c:forEach var="industri" items="${select_industri}">
					<option
						<c:if test="${status.value eq industri.key}"> SELECTED </c:if>
						value="${industri.key}">${industri.value}</option>
				</c:forEach>
			</select>
		 <font color="#CC3300">*</font></th>
		<th>Lainnya, Sebutkan</th>
		<th><spring:bind path="cmd.pemegang.industria">
			<input type="dis" id='lainIndustri' name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th rowspan="7">16. a.</th>
		<th rowspan="7">Alamat Lembaga/Perusahaan Tempat Bekerja</th>
		<th rowspan="7">
			<spring:bind path="cmd.pemegang.alamat_kantor">
				<textarea id="almtKantor1" cols="70%" rows="12" name="${status.expression }">${status.value }</textarea>
				<c:if test="${not empty status.errorMessage}">
					<div style="color: red">
						${status.errorMessage}
					</div>
				</c:if>
				<font color="#CC3300">*</font></th>
			</spring:bind>
		</th>
		<th>Kecamatan</th>
		<th>
			<input id="KecamatanKantor1" type="text" name="${status.expression }" id="${status.expression }" value="${status.value }"
			<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</th>
	</tr>
	<tr>
		<th>Kota</th>
		<th><spring:bind path="cmd.pemegang.kota_kantor">
				<input id="kota1" type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
				<c:if test="${ not empty status.errorMessage}">
					style='background-color :#FFE1FD'
				</c:if>>
		    	<span id="indicator_kantor" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
				</spring:bind>
				<spring:bind path="cmd.contactPerson.kota_kantor">
				<input type="hidden" name="${status.expression }" id="${status.expression }" value="${status.value }"/>
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th>Negara</th>
		<th>
			<spring:bind path="cmd.addressbilling.lsne_id">
				<select id="negara1" name="${status.expression}">
					<c:forEach var="d" items="${select_negara}">
						<option
							<c:if test="${status.value eq d.key}"> SELECTED </c:if>
							value="${d.key}">${d.value}</option>
					</c:forEach>
				</select>
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th>Kode Pos</th>
		<th><spring:bind path="cmd.pemegang.kd_pos_kantor">
			<input id="kPos1" type="text" name="${status.expression}"
				value="${status.value }" size="20" maxlength="10"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
	<th>Telp Kantor 1</th>
	<th><spring:bind path="cmd.pemegang.area_code_kantor">
		<input id="tlp_kntr1a" type="text" name="${status.expression}"
			value="${status.value }" size="5" maxlength="4"
			<c:if test="${ not empty status.errorMessage}">
			style='background-color :#FFE1FD'
		</c:if>>
	</spring:bind> <spring:bind path="cmd.pemegang.telpon_kantor">
		<input id="tlp_kntr1b" type="text" name="${status.expression}"
			value="${status.value }" size="12" maxlength="20"
			<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th>Telp Kantor 2</th>
		<th><spring:bind path="cmd.pemegang.area_code_kantor2">
			<input id="tlp_kntr2a" type="text" name="${status.expression}"
			value="${status.value }" size="5" maxlength="4"
			<c:if test="${ not empty status.errorMessage}">
			style='background-color :#FFE1FD'
		</c:if>>
	</spring:bind> <spring:bind path="cmd.pemegang.telpon_kantor2">
		<input id="tlp_kntr2b" type="text" name="${status.expression}"
			value="${status.value }" size="12" maxlength="20"
			<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th>No Fax</th>
		<th><spring:bind path="cmd.pemegang.area_code_fax">
			<input id="fax1a" type="text" name="${status.expression}"
			value="${status.value }" size="5" maxlength="4"
			<c:if test="${ not empty status.errorMessage}">
			style='background-color :#FFE1FD'
		</c:if>>
	</spring:bind> <spring:bind path="cmd.pemegang.no_fax">
		<input id="fax1b" type="text" name="${status.expression}"
			value="${status.value }" size="12" maxlength="20"
			<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
			
	<tr>
		<th  rowspan="6">b.</th>
		<th rowspan="6">Alamat Rumah <br> <span class="info">(Wajib diisi sesuai KTP)</span></th>
		<th rowspan="6">
			<spring:bind path="cmd.pemegang.alamat_rumah">
				<textarea id="almtRmh1" cols="70%" rows="8" name="${status.expression }">${status.value }</textarea>
				<c:if test="${not empty status.errorMessage}">
					<div style="color: red">
						${status.errorMessage}
					</div>
				</c:if>
				<font color="#CC3300">*</font></th>
			</spring:bind>
		</th>
		<th>Kecamatan</th>
		<th>
			<input id="rmhkecamatan1" type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
			<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</th>
	</tr>
	<tr>
		<th>Kota</th>
		<th ><spring:bind path="cmd.pemegang.kota_rumah">
		<input id="kotaRmh1" type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
		<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
    	<span id="indicator_rumah" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
		</spring:bind>   
		
		<spring:bind path="cmd.contactPerson.kota_rumah">
			<input type="hidden" name="${status.expression }" id="${status.expression }" value="${status.value }"/>
		</spring:bind>
		</td>
	</tr>
	<tr>
		<th>Negara</th>
		<th>
			<spring:bind path="cmd.addressbilling.lsne_id">
				<select id="negaraRmh1" name="${status.expression}">
					<c:forEach var="d" items="${select_negara}">
						<option
							<c:if test="${status.value eq d.key}"> SELECTED </c:if>
							value="${d.key}">${d.value}</option>
					</c:forEach>
				</select>
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th>Kode Pos</th>
		<th><spring:bind path="cmd.pemegang.kd_pos_rumah">
			<input id="kPostRmh1" type="text" name="${status.expression}"
				value="${status.value }" size="20" maxlength="10"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>	
	</tr>
	<tr>
		<th>Telp Rumah 1</th>
		<th>
			<spring:bind path="cmd.pemegang.area_code_rumah">
				<input id="tlpRmh1a" type="text" name="${status.expression}"
					value="${status.value }" size="5" maxlength="4"
					<c:if test="${ not empty status.errorMessage}">
					style='background-color :#FFE1FD'
				</c:if>>
				</spring:bind> <spring:bind path="cmd.pemegang.telpon_rumah">
				<input id="tlpRmh1b" type="text" name="${status.expression}"
					value="${status.value }" size="12" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
					style='background-color :#FFE1FD'
				</c:if>>
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th>Telp Rumah 2</th>
		<th><spring:bind path="cmd.pemegang.area_code_rumah2">
			<input id="tlpRmh2a" type="text" name="${status.expression}"
				value="${status.value }" size="5" maxlength="4"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind> <spring:bind path="cmd.pemegang.telpon_rumah2">
			<input id="tlpRmh2b" type="text" name="${status.expression}"
				value="${status.value }" size="12" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th  rowspan="6">c.</th>
		<th rowspan="6">Alamat Tempat Tinggal <br>
		<span class="info">(Wajib diisi hanya jika tidak sesuai KTP)</span></th>
		<th rowspan="6">
			<!-- <spring:bind path="cmd.pemegang.alamat_rumah"> -->
			<textarea cols="70%" rows="8" name="${status.expression }"
				<c:if test="${ not empty status.errorMessage}">
					style='background-color :#FFE1FD'
				</c:if>
				>${status.value }
			</textarea>
		<!-- </spring:bind> -->
		</th>
		<th>Kecamatan</th>
		<th>
			<input type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
			<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</th>
	</tr>
	<tr>
		<th>Kota</th>
		<th >
		<!-- <spring:bind path="cmd.pemegang.kota_rumah"> -->
		<input type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
		<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
    	<span id="indicator_rumah" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
		<!-- </spring:bind> -->
		<!-- <spring:bind path="cmd.contactPerson.kota_rumah"> -->
			<input type="hidden" name="${status.expression }" id="${status.expression }" value="${status.value }"/>
		<!-- </spring:bind> -->
		</th>
	</tr>
	<tr>
		<th>Negara</th>
		<th>
			<spring:bind path="cmd.addressbilling.lsne_id">
				<select id="negaraRmh2" name="${status.expression}">
					<c:forEach var="d" items="${select_negara}">
						<option
							<c:if test="${status.value eq d.key}"> SELECTED </c:if>
							value="${d.key}">${d.value}</option>
					</c:forEach>
				</select>
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th>Kode Pos</th>
		<th>
		<!-- <spring:bind path="cmd.pemegang.kd_pos_rumah"> -->
			<input type="text" name="${status.expression}"
				value="${status.value }" size="20" maxlength="10"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		<!-- </spring:bind> -->
		</th>	
	</tr>
	<tr>
		<th>Telp Rumah 1</th>
		<th>
		<!-- <spring:bind path="cmd.pemegang.area_code_rumah"> -->
			<input type="text" name="${status.expression}"
				value="${status.value }" size="5" maxlength="4"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
				</c:if>>
			<!-- </spring:bind> <spring:bind path="cmd.pemegang.telpon_rumah"> -->
			<input type="text" name="${status.expression}"
				value="${status.value }" size="12" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
				</c:if>>		
		<!-- </spring:bind> -->
		</th>
	</tr>
	<tr>
		<th>Telp Rumah 2</th>
		<th>
		<!-- <spring:bind path="cmd.pemegang.area_code_rumah2"> -->
			<input type="text" name="${status.expression}"
				value="${status.value }" size="5" maxlength="4"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
				</c:if>>
		<!-- </spring:bind> --> 
		<!-- <spring:bind path="cmd.pemegang.telpon_rumah2"> -->
			<input type="text" name="${status.expression}"
				value="${status.value }" size="12" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
				</c:if>>
		<!-- </spring:bind> -->
		</th>
	</tr>
	<tr><th></th>
		<th>Handphone 1</th>
		<th>
			<spring:bind path="cmd.pemegang.no_hp">
				<input type="text" name="${status.expression}"
					value="${status.value }" size="42" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
					style='background-color :#FFE1FD'
					</c:if>>
			</spring:bind>
		<font color="#CC3300">*</font>
		</th>
		<th>Email</th>
		<th>
		<spring:bind path="cmd.pemegang.mspe_email">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="40" maxlength="50"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
				</c:if>>
		</spring:bind>
		</th>
	</tr>
	<tr><th></th>
		<th>Handphone 2</th>
		<th colspan="3">
		<spring:bind path="cmd.pemegang.no_hp2">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
				</c:if>>
		</spring:bind>
		</th>
	</tr>
	<tr>
		<th  colspan="5" class="subtitle" style="text-align: left;">Alamat Penagihan : 
		<spring:bind path="cmd.addressbilling.tagih">
			<select  id="tgh" name="${status.expression }">
				<option value="1">Lain - Lain</option>
				<option value="2">Alamat Rumah</option>
				<option value="3">Alamat Kantor</option>
				<option value="4">Alamat Tempat Tinggal</option>
			</select>
		 </spring:bind></th>
	</tr>
	<tr>
		<th  rowspan="7">d.</th>
		<th rowspan="7">Alamat Penagihan / Korespondensi</th>
		<th rowspan="7">
			<spring:bind path="cmd.addressbilling.msap_address">
				<textarea id="almtPenagihan" class='Penagiahan_1' cols="70" rows="11" name="${status.expression }">${status.value }</textarea>
				<c:if test="${ not empty status.errorMessage}">	
					<div style="color: red">
						${status.errorMessage}
					</div>
				</c:if>
				<font color="#CC3300">*</font>
			</spring:bind>
		</th>
		<th>Kecamatan</th>
		<th>
			<input id="kecamatanPenagihan" class="Penagiahan_1" type="dsl" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
			<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</th>
	</tr>
	<tr>
		<th>Kota</th>
		<th>
		<spring:bind path="cmd.addressbilling.kota_tgh">
		<input id="kota_penagihan" class="Penagiahan_1" type="dsl" name="${status.expression }" value="${status.value }"
		<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
    	 <span id="indicator_tagih" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
	</spring:bind>
		</th>
	</tr>
	<tr>
		<th>Negara</th>
		<th>
			<spring:bind path="cmd.addressbilling.lsne_id">
				<select id="negara_penagihan" class="Penagiahan_1" name="${status.expression}">
					<c:forEach var="d" items="${select_negara}">
						<option
							<c:if test="${status.value eq d.key}"> SELECTED </c:if>
							value="${d.key}">${d.value}</option>
					</c:forEach>
				</select>
				<font color="#CC3300">*</font>
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th>Kode Pos</th>
		<th><spring:bind path="cmd.addressbilling.msap_zip_code">
			<input id="kPos_penagihan" class='Penagiahan_1' type="dsl" name="${status.expression}"
				value="${status.value }" size="20" maxlength="10"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th>No Telepon1</th>
		<th><spring:bind path="cmd.addressbilling.msap_area_code1">
			<input id="tlp1a_penagihan" class="Penagiahan_1" type="dsl" name="${status.expression}"
				value="${status.value }" size="5" maxlength="4"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind> <spring:bind path="cmd.addressbilling.msap_phone1">
			<input id="tlp1b_penagihan" class="Penagiahan_1" type="dsl" name="${status.expression}"
				value="${status.value }" size="12" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		<font color="#CC3300">*</font></spring:bind></th>
	</tr>
	<tr>
		<th>No Telepon2</th>
		<th><spring:bind path="cmd.addressbilling.msap_area_code2">
			<input id="tlp2a_penagihan" class="Penagiahan_1" type="dsl" name="${status.expression}"
				value="${status.value }" size="5" maxlength="4"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind> <spring:bind path="cmd.addressbilling.msap_phone2">
			<input id="tlp2b_penagihan" class='Penagiahan_1' type="dsl" name="${status.expression}"
				value="${status.value }" size="12" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th>No Fax</th>
		<th><spring:bind path="cmd.addressbilling.msap_area_code_fax1">
			<input id="fax1a_penagihan" class="Penagiahan_1" type="dls" name="${status.expression}"
				value="${status.value }" size="5" maxlength="4"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind> <spring:bind path="cmd.addressbilling.msap_fax1">
			<input id="fax1b_penagihan" class="Penagiahan_1" type="dls" name="${status.expression}"
				value="${status.value }" size="12" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th></th>
		<th>Handphone 1</th>
		<th><spring:bind path="cmd.addressbilling.no_hp">
			<input id="hpa_penagihan" type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind><br/><font color="#CC3300">* apabila tidak ada no hp, harap isi dengan 0000</font></th>
		<th>Email</th>
		<th><spring:bind path="cmd.addressbilling.e_mail">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="40" maxlength="50"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th></th>
		<th>Handphone 2</th>
		<th colspan="3"><spring:bind path="cmd.addressbilling.no_hp2">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind><br/><font color="#CC3300">* apabila tidak ada no hp, harap isi dengan 0000</font></th>
		
	</tr>
	<tr><th colspan="5"></th></tr>
	<tr>
		<th >17.</th>
		<th>Tujuan Membeli Asuransi</th>
		<th>
			<spring:bind path="cmd.pemegang.mkl_tujuan">
				<select name="${status.expression}" onChange="tujuan1(this.value)">
					<c:forEach var="tujuan" items="${select_tujuan}">
						<option
							<c:if test="${status.value eq tujuan.key}"> SELECTED </c:if>
							value="${tujuan.key}">${tujuan.value}</option>
					</c:forEach>
				</select>
			</spring:bind>
		<font color="#CC3300">*</font></th>
		<th>Lain-Lain, Jelaskan</th>
		<th><spring:bind path="cmd.pemegang.tujuana">
			<input id="tujuanLain1" type="dis" name="${status.expression}"
				value="${status.value }" size="40" maxlength="90"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th >18.</th>
		<th>Perkiraan Penghasilan Kotor Per Tahun</th>
		<th colspan="3">
			<spring:bind path="cmd.pemegang.mkl_penghasilan">
				<select  name="${status.expression}" >
					<c:forEach var="penghasilan" items="${select_penghasilan}">
						<option
							<c:if test="${status.value eq penghasilan.key}"> SELECTED </c:if>
							value="${penghasilan.key}">${penghasilan.value}</option>
					</c:forEach>
				</select>
			</spring:bind>
		 	<font color="#CC3300">*</font>
		</th>
	</tr>
	<tr>
		<th >19.</th>
		<th>Sumber Pendanaan Pembelian Asuransi</th>
		<th colspan = "3">
			<spring:bind path="cmd.pemegang.mkl_dana_from">
			<label for="sendiri2">
			<input type="radio" class=noBorder
					 name="${status.expression}" value="0" onClick="cpy('0');"
						<c:if test="${cmd.pemegang.mkl_dana_from eq 0 or cmd.pemegang.mkl_dana_from eq null}"> 
						   checked
						</c:if>
						 id="sendiri2">Diri Sendiri
			</label>
			<label for="gaksendiri2">
			<input type="radio" class=noBorder
					 name="${status.expression}" value="1" onClick="cpy('1');"
						<c:if test="${cmd.pemegang.mkl_dana_from eq 1}"> 
							checked
						</c:if>
						id="gaksendiri2">Bukan Diri Sendiri (Pembayar Premi bukan Pemegang Polis)<font color="#CC3300">*</font>
			</label>
			</spring:bind>
			<input type="hidden" name="tanda_pp" value='${cmd.pemegang.mkl_dana_from}'>
		</th>
	</tr>
	
	<tr>
		<th></th>
		<th></th>
		<th id ="mklSumberPremi">
			<font color="#CC3300">Apakah Pembayar Premi :</font>
			<spring:bind path="cmd.pemegang.mkl_sumber_premi">
			<label for="badan">
			<input type="radio" class=noBorder
					 name="${status.expression}" value="0" onClick="cpy2('0');"
						<c:if test="${cmd.pemegang.mkl_sumber_premi eq 0 or cmd.pemegang.mkl_sumber_premi eq null}"> 
						   checked
						</c:if>
						 id="badan">Badan Hukum/Usaha
			</label>
			<label for="orang">
			<input type="radio" class=noBorder
					 name="${status.expression}" value="1" onClick="cpy2('1');"
						<c:if test="${cmd.pemegang.mkl_sumber_premi eq 1}"> 
							checked
						</c:if>
						id="orang">Perorangan
			</label>
			</spring:bind>
			<input type="hidden" name="tanda_hub" value='${cmd.pemegang.mkl_sumber_premi}'>
		</th>
		<th colspan = "2" id = "lsreIdPremi">
			<spring:bind path="cmd.pemegang.lsre_id_premi">
				<select  name="${status.expression}" >
					<c:forEach var="pre" items="${select_relasi_premi}">
						<option
							<c:if test="${status.value eq pre.key}"> SELECTED </c:if>
							value="${pre.key}">${pre.value}</option>
					</c:forEach>
				</select>
			</spring:bind> 	
		</th>
	</tr>
	<tr><th></th><th></th>
		<th colspan = "3" id = "addDel">
			<input name="btnadd1" type="button" id="btnadd1" value="ADD" onClick="addRowDOM1()"> &nbsp; 
         	<input name="btn_cancel1" type="button" id="btn_cancel1" value="DELETE" onClick="delRowDOM1()">
			<input type="hidden" name="jmlDaftarKyc" value="0">
		</th>
	</tr>			
	<tr><th></th>
		<th>&nbsp;</th>
		<th><font color="#CC3300">Pilih Sumber Pendanaan</font></th>
		<th colspan = "2"><font color="#CC3300">Lainnya, Jelaskan</font></th>
	</tr>					
		
	<tr>
		<th colspan = "5" >
			<table id="tableDanax" class="entry2">
				<tr>
					<th width = "17">&nbsp;</th>
					<th width = "255">&nbsp;</th>
					<th width = "390">
					<select name="pemegang.mkl_pendanaan">
						<c:forEach var="dana" items="${select_dana}">
							<option
								<c:if test="${cmd.pemegang.mkl_pendanaan eq dana.ID}"> SELECTED </c:if>
								value="${dana.ID}">${dana.DANA}
							</option>
						</c:forEach>
					</select>
			 				<font color="#CC3300">*</font>
					</th>
					<th width = "296">
					<spring:bind path="cmd.pemegang.danaa">
						<input type="text" name="${status.expression}"
							value="${status.value }" size="42" maxlength="100"
							<c:if test="${ not empty status.errorMessage}">
								style='background-color :#FFE1FD'
							</c:if>>
					</spring:bind>
					</th>
					<th width = "62">&nbsp;</th>
				</tr>
			</table>
		</th>
	</tr>

	
	<tr>
		<th colspan = "5">&nbsp;</th>
	</tr>					
	<tr>
		<th >20.</th>
		<th>Sumber Penghasilan</th>
		<th colspan="3">
			<spring:bind path="cmd.pemegang.mkl_hasil_from">
				<label for="sendiri3">
					<input type="radio" class=noBorder
						name="${status.expression}" value="0"
						<c:if test="${cmd.pemegang.mkl_hasil_from eq 0 or cmd.pemegang.mkl_hasil_from eq null}"> 
								checked</c:if>
						id="sendiri3">Diri Sendiri
				</label>
				<label for="gaksendiri3">
					<input type="radio" class=noBorder
						name="${status.expression}" value="1"
						<c:if test="${cmd.pemegang.mkl_hasil_from eq 1}"> 
								checked</c:if>
						id="gaksendiri3">Bukan Diri Sendiri (Pembayar Premi bukan Pemegang Polis)<font color="#CC3300">*</font>
				</label>
			</spring:bind>
		</th>
	</tr>
	
	<tr>
		<th></th>
		<th></th>
		<th colspan="3">
			<input name="btnadd1" type="button" id="btnadd1" value="ADD" onClick="addRowDOM2()"> &nbsp; 
         				<input name="btn_cancel1" type="button" id="btn_cancel1" value="DELETE" onClick="delRowDOM2()">
			<input type="hidden" name="jmlDaftarKyc2" value="0">
		</th>
	</tr>		
	<tr><th></th>
		<th></th>
		<th><font color="#CC3300">Pilih Sumber Penghasilan</font></th>
		<th colspan = "2"><font color="#CC3300">Lainnya, Jelaskan</font></th>
	</tr>
	<tr>
		<th colspan = "5">
		<table id="tableDanax2" class="entry2">
			<tr>
				<th width = "17">&nbsp;</th>
				<th width = "282">&nbsp;</th>
				<th width = "430">
					<select name="pemegang.mkl_smbr_penghasilan" >
						<c:forEach var="hasil" items="${select_hasil}">
							<option
								<c:if test="${cmd.pemegang.mkl_smbr_penghasilan eq hasil.ID}"> SELECTED </c:if>
								value="${hasil.ID}">${hasil.DANA}
							</option>
						</c:forEach>
					</select>
					<font color="#CC3300">*</font>
				</th>
				<th colspan = "2">
					<spring:bind path="cmd.pemegang.shasil">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="100"
						<c:if test="${ not empty status.errorMessage}">
								style='background-color :#FFE1FD'
							</c:if>>
					</spring:bind>
				</th>
				<th width = "62">&nbsp;</th>
			</tr>
		</table>
		</th>	
	<tr>
		<th colspan = "5">&nbsp;</th>
	</tr>
	<tr>
		<th >21.</th>
		<th>No NPWP</th>
		<th colspan="3">
			<!-- <spring:bind path="cmd.pemegang.shasil"> -->
				<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
				</c:if>>
			<!-- </spring:bind> -->
		</th>
	</tr>
	<tr>
		<th >22.</th>
		<th>Hubungan dengan Calon Tertanggung</th>
		<th>
			<!-- <spring:bind path="cmd.pemegang.mkl_tujuan"> -->
				<select id="slct_relation" name="${status.expression}" >
					<option value=0>Lain-Lain</option>
					<c:forEach var="relasi" items="${select_relasi}">
						<option
							<c:if test="${status.value eq relasi.key}"> SELECTED </c:if>
							value="${relasi.key}">${relasi.value}</option>
					</c:forEach>
				</select>
			<!-- </spring:bind>  -->
		</th>
		<th>Lain-Lain, Jelaskan</th>
		<th >
		<!-- <spring:bind path="cmd.pemegang.tujuana"> -->
			<input id="txt_relation" type="dis" name="${status.expression}"
				value="${status.value }" size="40" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		<!-- </spring:bind> -->
		</th>
	</tr>
	<tr>
		<th >23.</th>
		<th>Khusus untuk korespondensi rutin harap dikirimkan melalui (pilih satu satu)</th>
		<th colspan = "3">
			<input type="radio" class=noBorder name="${status.expression}" id="khusus_korepondensi1" />Email
			<input type="radio" class=noBorder name="${status.expression}" id="khusus_korepondensi2" />Kurir/Pos
		</th>
	</tr>
	<tr>
		<th >24.</th>
		<th>Polis dikirimkan melalui </th>
		<th colspan = "3">
			<input type="radio" class=noBorder name="${status.expression}" id="kirimPolis1" />Email
			<input type="radio" class=noBorder name="${status.expression}" id="kirimPolis2" />Kurir/Pos
		</th>
	</tr>
</table>
<style>
.textboxdisable
{
	border: 1px;
	border-color: #AAA;
	border-style: solid;
	background-color: #D9D9D9;
}
.textboxenable
{
	border: 1px;
	border-color: #AAA;
	border-style: solid;
	background-color: #FFF;
}
</style>
<!-- script perpage -->
<script type="text/javascript" src="${path }/include/js/bac/pemegang.js"></script>
<script type="text/javascript">
	var jenis =  "${cmd.datausulan.jenis_pemegang_polis}";
	if(jenis == 0){
		$(document).ready(function(){
			var tableEntry = $('.entry tr');
			tableEntry.find('input[type=text]').val('');
			tableEntry.find('textarea').val('');
		});
	}
	//set Agama
	function agamaChange(flag){				
		if(flag=='6'){
			$('.agama').removeClass("textboxdisable");
			$('.agama').removeAttr('disabled');
			$('.agama').addClass("textboxenable");
		}else{
			$('.agama').val('');
			$('.agama').removeClass("textboxenable");
			$('.agama').attr('disabled','disabled');
			$('.agama').addClass("textboxdisable");
		}
	}
	
	//set age from bdate
	var bdate = $('#bdate_1').val();
	if(bdate != null){
		$('.age_1').val(calculateAge(bdate));
	}else{
		$('.age_1').val('');
	}
	
	function calculateAge(birthday) {
	    var now = new Date();
	    var past = new Date(birthday);
	    var nowYear = now.getFullYear();
	    var pastYear = past.getFullYear();
	    var age = nowYear - pastYear;
	
	    return age;
	};
	function bdate1(bdate){
		$('.age_1').val(calculateAge(bdate));
	}
	
	//set alamat penagihan
	$('.Penagiahan_1').addClass("textboxenable");
	$('.Penagiahan_1').val("");
	$('#tgh').change(function(){
		var selAddPenagihan = $(this).find('option:selected').val();
		if(selAddPenagihan == '1'){
			$('.Penagiahan_1').val("");
			$('.Penagiahan_1').removeClass("textboxdisable");
			$('.Penagiahan_1').removeAttr('disabled');
			$('.Penagiahan_1').addClass("textboxenable");	
		}else if(selAddPenagihan == '2'){
			$('.Penagiahan_1').removeClass("textboxenable");
			$('.Penagiahan_1').attr('disabled','disabled');
			$('.Penagiahan_1').addClass("textboxdisable");
			
			var alamat = $('#almtRmh1').val();
			var kecamatan = $('#rmhkecamatan1').val();
			var kota = $('#kotaRmh1').val();
			var negara = $('#negaraRmh1').val();
			var kPost = $('#kPostRmh1').val();
			var tlp1a = $('#tlpRmh1a').val();
			var tlp1b = $('#tlpRmh1b').val();
			var tlp2a = $('#tlpRmh2a').val();
			var tlp2b = $('#tlpRmh2b').val();
			
			$('#almtPenagihan').val(alamat);
			$('#kecamatanPenagihan').val(kecamatan);
			$('#kPos_penagihan').val(kPost);
			$('#kota_penagihan').val(kota);
			$('#negara_penagihan').val(negara);
			$('#tlp1a_penagihan').val(tlp1a);
			$('#tlp1b_penagihan').val(tlp1b);
			$('#tlp2a_penagihan').val(tlp2a);
			$('#tlp2b_penagihan').val(tlp2b);
			$('#fax1a_penagihan').val('');
			$('#fax1b_penagihan').val('');
			
		}else if(selAddPenagihan == '3'){
				
			$('.Penagiahan_1').removeClass("textboxenable");
			$('.Penagiahan_1').attr('disabled','disabled');
			$('.Penagiahan_1').addClass("textboxdisable");
			
			var alamat = $('#almtKantor1').val();
			var kecamatan = $('#KecamatanKantor1').val();
			var kota = $('#kota1').val();
			var negara = $('#negara1').val();
			var kPost = $('#kPos1').val();
			var tlp1a = $('#tlp_kntr1a').val();
			var tlp1b = $('#tlp_kntr1b').val();
			var tlp2a = $('#tlp_kntr2a').val();
			var tlp2b = $('#tlp_kntr2b').val();
			var fax1a = $('#fax1a').val();
			var fax1b = $('#fax1b').val();

			$('#almtPenagihan').val(alamat);
			$('#kecamatanPenagihan').val(kecamatan);
			$('#kPos_penagihan').val(kPost);
			$('#kota_penagihan').val(kota);
			$('#negara_penagihan').val(negara);
			$('#tlp1a_penagihan').val(tlp1a);
			$('#tlp1b_penagihan').val(tlp1b);
			$('#tlp2a_penagihan').val(tlp2a);
			$('#tlp2b_penagihan').val(tlp2b);
			$('#fax1a_penagihan').val(fax1a);
			$('#fax1b_penagihan').val(fax1b);
		}
	});
	
	//Sumber Pendanaan Pembelian Asuransi 19
	$("#mklSumberPremi input:radio").attr('disabled',true);
	$("#mklSumberPremi").css('color','#AAA');
	$("#mklSumberPremi font").css('color','#AAA');
	$("#lsreIdPremi select").attr('disabled',true);
	$("#lsreIdPremi select").addClass("textboxdisable");
	$('#gaksendiri2').change(function(){
		$("#lsreIdPremi select").removeClass("textboxenable");
		$("#lsreIdPremi select").addClass("textboxdisable");
		$("#mklSumberPremi input:radio").removeAttr('disabled');
		$("#mklSumberPremi").css('color','');
		$("#mklSumberPremi font").css('color','');
		$("#lsreIdPremi select").attr('disabled',true);
		
	});
	$('#sendiri2').change(function(){
		$("#mklSumberPremi input:radio").attr('disabled',true);
		$("#mklSumberPremi").css('color','#AAA');
		$("#mklSumberPremi font").css('color','#AAA');
		$("#lsreIdPremi select").val('0');
		$("#lsreIdPremi select").attr('disabled',true);
		$("#badan").attr('checked', 'checked');
		$("#lsreIdPremi select").removeClass("textboxenable");
		$("#lsreIdPremi select").addClass("textboxdisable");
	});
	function cpy2(cond){
		if(cond == 0){
			$("#lsreIdPremi select").removeClass("textboxenable");
			$("#lsreIdPremi select").attr('disabled',true);
			$("#lsreIdPremi select").val('0');
			$("#lsreIdPremi select").addClass("textboxdisable");
		}else{
			$("#lsreIdPremi select").removeClass("textboxdisable");
			$("#lsreIdPremi select").removeAttr('disabled');
			$("#lsreIdPremi select").addClass("textboxenable");
		}
	}
	
	//menambah table utk no 19 dan 20
	function addRowDOM1(){
		var idx = '1';
		var tr = $('#tableDanax tbody tr:last-child');
		//var check = [];
		var check = $(tr).find('input[type=checkbox]').val();
		
		if(check != null){
			idx = parseInt(check) + 1;
		}
			
		var contentRow = '<tr class="check1_'+idx+'"><th>&nbsp;</th><th>&nbsp;</th>'
							+'<th>'
							+'<select name="listkyc.kyc_desc1'+idx+'">'
								+'<c:forEach var="sdana" items="${select_dana}">'
									+'<option<c:if test="${listkyc.kyc_desc1 eq sdana.ID}"> SELECTED </c:if> value="${sdana.ID}">${sdana.DANA}'
									+'</option>'
								+'</c:forEach>'
							+'</select>'
							+'</th>'
								+'<th>'
								+'<spring:bind path="cmd.pemegang.danaa">'
									+'<input type="text" name="listkyc.kyc_desc_x'+idx+'" value="${listkyc.kyc_desc_x}" size="42" maxlength="100">'
								+'</spring:bind>'
							+'</th>'
							+'<th><input class="noBorder" value="'+idx+'" type="checkbox" name="cek'+idx+'" id= "ck'+idx+'"></th>'
						  +'</tr>';
		var oRow = add_new_row('#tableDanax',contentRow);
	}
	function addRowDOM2(){
		var idx = '1';
		var tr = $('#tableDanax2 tbody tr:last-child');
		//var check = [];
		var check = $(tr).find('input[type=checkbox]').val();
		
		if(check != null){
			idx = parseInt(check) + 1;
		}
		
		var contentRow = '<tr class="check2_'+idx+'"><th>&nbsp;</th><th>&nbsp;</th>'
							+'<th>'
							+'<select name="listkyc2.kyc_desc2'+idx+'">'
								+'<c:forEach var="shasil" items="${select_sumberkyc}">'
									+'<option<c:if test="${listkyc2.kyc_desc2 eq shasil.ID}"> SELECTED </c:if> value="${shasil.ID}">${shasil.DANA}'
									+'</option>'
								+'</c:forEach>'
							+'</select>'
							+'</th>'
							+'<th colspan = "2">'
								+'<spring:bind path="cmd.pemegang.danaa">'
									+'<input type="text" name="listkyc2.kyc_desc2_x'+idx+'" value="${listkyc2.kyc_desc2_x}" size="42" maxlength="100">'
								+'</spring:bind>'
							+'</th>'
							+'<th><input class="noBorder" value="'+idx+'" type="checkbox" name="cek'+idx+'" id= "ck'+idx+'"></th>'
						 +'</tr>';
		var oRow = add_new_row('#tableDanax2',contentRow);
	}
	
	function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
    
    
    //Delete row 
    function delRowDOM1(){
    	var tr = $('#tableDanax tbody').children();
    	var check = $(tr).find('input[type=checkbox]:checked');
    	$.each( check, function( key, value ) {
    		idx = value.value;
    		$('.check1_'+idx).remove();
		}); 	
    }
    function delRowDOM2(){
    	var tr = $('#tableDanax2 tbody').children();
    	var check = $(tr).find('input[type=checkbox]:checked');
    	$.each( check, function( key, value ) {
    		idx = value.value;
    		$('.check2_'+idx).remove();
		});
	} 
	
	//ralasi untuk no 22
	$('#slct_relation').change(function(){
		var selRelation = $(this).find('option:selected').val();
		if(selRelation == 0){
			$('#txt_relation').removeClass("textboxdisable");
			$('#txt_relation').removeAttr('disabled');
			$('#txt_relation').addClass("textboxenable");
		}else{
			$('#txt_relation').val('');
			$('#txt_relation').removeClass("textboxenable");
			$('#txt_relation').attr('disabled','disabled');
			$('#txt_relation').addClass("textboxdisable");
		}
	});
	
	//Tujuan membeli asuransi no 17
	function tujuan1(id){
		if(id == 6){
			$('#tujuanLain1').removeClass("textboxdisable");
			$('#tujuanLain1').removeAttr('disabled');
			$('#tujuanLain1').addClass("textboxenable");
		}else{
			$('#tujuanLain1').val('');
			$('#tujuanLain1').removeClass("textboxenable");
			$('#tujuanLain1').attr('disabled','disabled');
			$('#tujuanLain1').addClass("textboxdisable");
		}
	}
	
	//Klasifikasi Pekerjaan 
	function klas_kerja(id){
		if(id == 0){
			$('#lainKlasKerja').removeClass("textboxdisable");
			$('#lainKlasKerja').removeAttr('disabled');
			$('#lainKlasKerja').addClass("textboxenable");
		}else{
			$('#lainKlasKerja').val('');
			$('#lainKlasKerja').removeClass("textboxenable");
			$('#lainKlasKerja').attr('disabled','disabled');
			$('#lainKlasKerja').addClass("textboxdisable");
		}
	}
	
	//Klasifikasi bid industri No 15
	function bidIndustri1(id){
		if(id == 8){
			$('#lainIndustri').removeClass("textboxdisable");
			$('#lainIndustri').removeAttr('disabled');
			$('#lainIndustri').addClass("textboxenable");
		}else{
			$('#lainIndustri').val('');
			$('#lainIndustri').removeClass("textboxenable");
			$('#lainIndustri').attr('disabled','disabled');
			$('#lainIndustri').addClass("textboxdisable");
		}
	}
	
	
</script>