<%@ include file="/include/page/header_jquery.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />

<table class="entry">
	<tr>
		<th colspan="5" class="subtitle">DATA CALON PEMBAYAR PREMI</th>
	</tr>
	<tr>
		<th width="18">1.</th>
		<th width="320">Calon Pembayar Premi adalah :<br>
			<span class="info">(Apabila Calon Pembayar Premi bukan perusahaan, maka langsung mengisi butir 5-8)</span>
		</th>
		<th colspan="3">
			<select name="${status.expression}">
					<c:forEach var="c" items="${select_calon_pembayar_premi}">
						<option
							<c:if test="${status.value eq c.key}"> SELECTED </c:if>
							value="${c.key}">${c.value}</option>
					</c:forEach>
			</select>
		</th>			
	</tr>
	<tr>
		<th width="18">2.</th>
		<th width="320">Nama Perusahaan </th>
		<th colspan="3">
			<spring:bind path="cmd.pembayarPremi.perusahaan">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
				</c:if> title="Nama Perusahaan Pembayar Premi">
			</spring:bind><em>*</em>
		</th>
	</tr>
	<tr>
		<th width="18">3.</th>
		<th width="320">Alamat Perusahaan </th>
		<th colspan="3">
			<spring:bind path="cmd.pembayarPremi.alamat_perusahaan">
			<textarea type="text" rows="5" cols="45" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
				</c:if> title="Alamat Perusahaan Pembayar Premi">
			</textarea>
			</spring:bind><em>*</em>
		</th>
	</tr>
	<tr>
		<th width="18">&nbsp;</th>
		<th width="320">Kota</th>
		<th>
			<spring:bind path="cmd.pembayarPremi.kota_perusahaan">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
				</c:if> title="Kota Pembayar Premi">
			</spring:bind><em>*</em>
		</th>		
		<th>Provinsi</th>
		<th>
			<spring:bind path="cmd.pembayarPremi.prov_perusahaan">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
				</c:if> title="Provinsi Pembayar Premi">
			</spring:bind><em>*</em>
		</th>		
	</tr>
	<tr>
		<th width="18">&nbsp;</th>
		<th width="320">No. Telp. Perusahaan</th>
		<th>
			<spring:bind path="cmd.pembayarPremi.telp_perusahaan">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
				</c:if> title="No. Telp Perusahaan Pembayar Premi">
			</spring:bind><em>*</em>
		</th>		
		<th>Kode Pos</th>
		<th>
			<spring:bind path="cmd.pembayarPremi.kdpos_perusahaan">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${not empty status.errorMessage}">
						style='background-color :#FFE1FD'
				</c:if> title="Kode Pos Pembayar Premi">
			</spring:bind><em>*</em>
		</th>		
	</tr>
	<tr>
		<th width="18">&nbsp;</th>
		<th width="320">No. Faksimili</th>
		<th colspan="3">
			<spring:bind path="cmd.pembayarPremi.faks_perusahaan">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
				</c:if> title="No. Faksimili Pembayar Premi">
			</spring:bind>
		</th>				
	</tr>
	<tr>
		<th width="18">4.</th>
		<th width="320">Bidang Usaha</th>
		<th colspan="3">
			<spring:bind path="cmd.pembayarPremi.usaha">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
				</c:if> title="Bidang Usaha Pembayar Premi">
			</spring:bind><em>*</em>
		</th>
	</tr>
	<tr>
		<th width="18">5.</th>
		<th width="320">Apakah Anda memiliki pekerjaan/usaha/bisnis lain di luar pekerjaan utama?</th>
		<th colspan="3">
			<input type="radio" class=noBorder name="green_card" id="green_card1" checked/>Ya, sebutkan apa saja
			<spring:bind path="cmd.pembayarPremi.usaha_lain">
				<input type="text" name="${status.expression}" 
					value="${status.value }" size="42" maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
					</c:if> title="Pembayar Premi memiliki pekerjaan/usaha/bisnis lain di luar pekerjaan utama">
			</spring:bind>
			<br><input type="radio" class=noBorder name="green_card" id="green_card1"/>Tidak
		</th>
	</tr>
	<tr>
		<th width="18">6.</th>
		<th width="320">Penghasilan dan sumber penghasilan Calon Pembayar Premi<br>
			<span class="info">Apabila penghasilan yang diperoleh adalah dalam mata uang asing, maka yang dicantumkan adalah ekuivalen Rupiah dengan menggunakan kurs tengah Bank Indonesia yang berlaku pada
				saat pengisian SPAJ ini.
			</span>
		</th>
		<th colspan="3">&nbsp;</th>
	</tr>
	<tr>
		<th colspan="5">
			<table id="TableAddPendapatan1" class="entry2">
				<tr>
					<th width="8">a.</th>
					<th width="370">Sumber pendapatan rutin per bulan <br>
						<span class="info">(pilihan dapat lebih dari satu)</span>
					</th>
					<th colspan="3">
						<select name="${status.expression}">
								<c:forEach var="c" items="${select_mkl_smbr_penghasilan}">
									<option
										<c:if test="${status.value eq c.key}"> SELECTED </c:if>
										value="${c.key}">${c.value}</option>
								</c:forEach>
						</select>		
					</th>
				</tr>
			</table>
		</th>
	</tr>
	<tr>
		<th></th>
		<th></th>
		<th colspan = "3" id = "addRows1">
			<input name="btn_add1" type="button" id="btn_add1" value="ADD" onClick="addRow1()"> &nbsp; 
         	<input name="btn_delete1" type="button" id="btn_delete1" value="DELETE" onClick="delRow1()">
			<input type="hidden" name="lpr_id" value="${cmd.pembayarPremi.lpr_id}">
		</th>
	</tr>
	<tr>
		<th colspan="5">
			<table id="TableAddPendapatan2" class="entry2">
				<tr>
					<th width="8">b.</th>
					<th width="370">Sumber pendap atan rutin per tahun <br>
						<span class="info">(pilihan dapat lebih dari satu)</span>
					</th>
					<th colspan="3">
						<select name="${status.expression}">
								<c:forEach var="c" items="${select_pendapatan_non_rutin}">
									<option
										<c:if test="${status.value eq c.key}"> SELECTED </c:if>
										value="${c.key}">${c.value}</option>
								</c:forEach>
						</select>		
					</th>
				</tr>
			</table>
		</th>
	</tr>
	<tr>
		<th></th>
		<th></th>
		<th colspan = "3" id = "addRows2">
			<input name="btn_add2" type="button" id="btn_add2" value="ADD" onClick="addRow2()"> &nbsp; 
         	<input name="btn_delete2" type="button" id="btn_delete2" value="DELETE" onClick="delRow2()">
			<input type="hidden" name="lpr_id" value="${cmd.pembayarPremi.lpnr_id}">
		</th>
	</tr>
	<tr>
		<th width="18">c.</th>
		<th width="320">Jumlah Total pendapatan rutin per bulan</th>
		<th colspan="3">
			<select name="${status.expression}">
					<c:forEach var="c" items="${select_mkl_penghasilan}">
						<option
							<c:if test="${status.value eq c.key}"> SELECTED </c:if>
							value="${c.key}">${c.value}</option>
					</c:forEach>
			</select>	
		</th>
	</tr>
	<tr>
		<th width="18">d.</th>
		<th width="320">Jumlah Total pendapatan non-rutin per tahun</th>
		<th colspan="3">
			<select name="${status.expression}">
					<c:forEach var="c" items="${select_mkl_penghasilan}">
						<option
							<c:if test="${status.value eq c.key}"> SELECTED </c:if>
							value="${c.key}">${c.value}</option>
					</c:forEach>
			</select>	
		</th>
	</tr>	
	<tr>
		<th colspan="5">
			<table id="TableAddPendapatan3" class="entry2">
				<tr>
					<th width="8">7.</th>
					<th width="370">Tujuan Pengajuan asuransi<br>
						<span class="info">(pilihan dapat lebih dari satu)</span>
					</th>
					<th colspan="3">
						<select name="${status.expression}">
								<c:forEach var="d" items="${select_mkl_tujuan}">
									<option
										<c:if test="${status.value eq d.key}"> SELECTED </c:if>
										value="${d.key}">${d.value}</option>
								</c:forEach>
						</select>
					</th>			
				</tr>
			</table>
		</th>
	</tr>
	<tr>
		<th></th>
		<th></th>
		<th colspan = "3" id = "addRows3">
			<input name="btn_add3" type="button" id="btn_add3" value="ADD" onClick="addRow3()"> &nbsp; 
         	<input name="btn_delete3" type="button" id="btn_delete3" value="DELETE" onClick="delRow3()">
			<input type="hidden" name="lpr_id" value="${cmd.pembayarPremi.lpnr_id}">
		</th>
	</tr>
	<tr>
		<th width="18">8.</th>
		<th width="320">Data Pihak Ketiga<br>
			<span class="info">(Diisi apabila Calon Pembayar Premi bukan Calon pembayarPremi Polis)</span>
		</th>
		<th colspan="3">&nbsp;</th>			
	</tr>
	<tr>
		<th width="18">&nbsp;</th>
		<th width="320">Apakah terdapat Pihak Ketiga yang: <br> (i) meminta Anda untuk mengajukan permintaan asuransi ini, <br> (ii) menjadi pembayar premi; dan/atau <br> (iii)
			memiliki akses terhadap manfaat dari Polis
		</th>
		<th colspan="3">
			<input type="radio" class=noBorder name="green_card" id="green_card1" checked/>Ya
			&nbsp;<input type="radio" class=noBorder name="green_card" id="green_card1"/>Tidak
			<br><br>
			<spring:bind path="cmd.pembayarPremi.usaha_lain">
				Sebutkan
				<input type="text" name="${status.expression}" 
					value="${status.value }" size="42" maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
					</c:if> title="Pembayar Premi memiliki pekerjaan/usaha/bisnis lain di luar pekerjaan utama">
			</spring:bind>
		</th>				
	</tr>
	<tr>
		<th width="18">&nbsp;</th>
		<th width="320">Apabila jawaban Ya mohon menjawab pertanyaan di bawah ini:<br>
			<span class="info">(Diisi apabila Calon Pembayar Premi bukan Calon pembayarPremi Polis)</span>
		</th>
		<th colspan="3"></th>			
	</tr>
	<tr>
		<th width="18">&nbsp;</th>
		<th width="320">Nama Pihak ketiga</th>
		<th colspan="3">
			<spring:bind path="cmd.pembayarPremi.nama_pihak_ketiga">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
				</c:if> title="Nama Pihak Ketiga Pembayar Premi">
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th width="18" rowspan="2">&nbsp;<br></th>
		<th width="320" rowspan="2">Alamat <br>
			<span class="info">Kecamatan/Kota/Kode Pos</span>
		</th>
		<th rowspan="2">
			<spring:bind path="cmd.pembayarPremi.alamat_lengkap">
			<textarea type="text" rows="5" cols="45" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
				</c:if> title="Alamat(Kecamatan/Kota/Kode Pos) Pembayar Premi">
			</textarea>
			</spring:bind>
		</th>
		<th>No. Telp. Rumah</th>
		<th>
			<spring:bind path="cmd.pembayarPremi.telp_rumah">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
				</c:if> title="No. Telp Rumah">
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th>No. Telp. Kantor</th>
		<th>
			<spring:bind path="cmd.pembayarPremi.telp_kantor">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
				</c:if> title="No. Telp Kantor">
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th width="18">&nbsp;</th>
		<th width="320">Kewarganegaraan</th>
		<th>
			<spring:bind path="cmd.pembayarPremi.kewarganegaraan">
				<select name="${status.expression}">
					<c:forEach var="r" items="${select_negara}">
						<option
							<c:if test="${status.value eq r.key}"> SELECTED </c:if>
							value="${r.key}">${r.value}</option>
					</c:forEach>
				</select>
			</spring:bind>
		</th>
		<th>Tanggal Pendirian <br>
			<span class="info">(jika berbentuk badan hukum)</span>
		</th>
		<th>
			<spring:bind path="cmd.pembayarPremi.tgl_pendirian">
				<input type="text" id="${status.expression}" name="${status.expression}" value="${status.value}" class="datepicker" readonly="readonly" title="Tanggal Pendirian"/>
			</spring:bind></th>
		</th>
	</tr>
	<tr>
		<th width="18">&nbsp;</th>
		<th width="320">Tempat/Tgl Lahir <br>
			<span class="info">(hanya jika individu)</span>
		</th>
		<th>
			<spring:bind path="cmd.pembayarPremi.tempat_lahir">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
				</c:if> title="Tempat Lahir Pembayaran Premi">
			</spring:bind> &nbsp;/&nbsp;			
			<spring:bind path="cmd.pembayarPremi.tgl_lahir">
				<input type="text" id="${status.expression}" name="${status.expression}" value="${status.value}" class="datepicker" readonly="readonly" title="Tanggal Lahir Pembayar Premi"/>
			</spring:bind>			
		</th>
		<th>Tempat Kedudukan <br>
			<span class="info">(jika berbentuk badan hukum)</span>
		</th>
		<th>
			<spring:bind path="cmd.pembayarPremi.tempat_kedudukan">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
				</c:if> title="Tempat Kedudukan Pembayaran Premi">
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th width="18">&nbsp;</th>
		<th width="320">Pekerjaan <br>
			<span class="info">(jika berbentuk badan hukum)</span>
		</th>
		<th colspan="3">
			<select name="${status.expression}">
					<c:forEach var="p" items="${select_pekerjaan}">
						<option
							<c:if test="${status.value eq p.key}"> SELECTED </c:if>
							value="${p.key}">${p.value}</option>
					</c:forEach>
			</select>
		</th>
	</tr>
	<tr>
		<th width="18">&nbsp;</th>
		<th width="320">Bidang Usaha <br>
			<span class="info">(jika berbentuk badan hukum)</span>
		</th>
		<th colspan="3">
			<spring:bind path="cmd.pembayarPremi.usaha_berbadan_hukum">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
				</c:if> title="Bidang Usaha Pembayar Premi">
			</spring:bind><em>*</em>
		</th>
	</tr>
	<tr>
		<th width="18">&nbsp;</th>
		<th width="320">Bidang / Jenis Pekerjaan : </th>
		<th colspan="3">
			<select name="${status.expression}">
					<c:forEach var="d" items="${select_grp_job}">
						<option
							<c:if test="${status.value eq d.key}"> SELECTED </c:if>
							value="${d.key}">${d.value}</option>
					</c:forEach>
			</select>
		</th>
	</tr>
	<tr>
		<th width="18">&nbsp;</th>
		<th width="320">Jabatan/Pangkat/Golongan</th>
		<th colspan="3">
			<select name="${status.expression}">
					<c:forEach var="j" items="${select_jabatan}">
						<option
							<c:if test="${status.value eq j.key}"> SELECTED </c:if>
							value="${j.key}">${j.value}</option>
					</c:forEach>
			</select>
		</th>
	</tr>
	<tr>
		<th width="18">&nbsp;</th>
		<th width="320">Instansi/Departemen</th>
		<th colspan="3">
			<spring:bind path="cmd.pembayarPremi.instansi">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
				</c:if> title="Instansi/Departmen Pembayar Premi">
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th width="18">&nbsp;</th>
		<th width="320">No. NPWP</th>
		<th colspan="3">
			<spring:bind path="cmd.pembayarPremi.no_npwp">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
				</c:if> title="No. NPWP Pembayar Premi">
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th width="18">&nbsp;</th>
		<th width="320">Hubungan dengan Pembayar Premi Polis</th>
		<th colspan="3">
			<select name="${status.expression}">
					<c:forEach var="r" items="${select_relation}">
						<option
							<c:if test="${status.value eq r.key}"> SELECTED </c:if>
							value="${r.key}">${r.value}</option>
					</c:forEach>
			</select>
		</th>
	</tr>
	<tr>
		<th width="18">&nbsp;</th>
		<th width="320">Sumber Dana</th>
		<th colspan="3">
			<spring:bind path="cmd.pembayarPremi.sumber_dana">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
				</c:if> title="Sumber Dana Pembayar Premi">
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th width="18">&nbsp;</th>
		<th width="320">Tujuan Penggunaan Dana</th>
		<th colspan="3">
			<spring:bind path="cmd.pembayarPremi.tujuan_dana">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
				</c:if> title="Tujuan Penggunaan Dana Pembayar Premi">
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th>&nbsp;</th>
		<th>Apabila tidak dapat memberikan informasi atas Pihak Ketiga, mohon diberikan alasannya</th>
		<th colspan="3">
			<spring:bind path="cmd.pembayarPremi.informasi">
			<textarea type="text" rows="5" cols="45" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
				</c:if> title="Alasan Pembayar Premi Tidak Memberikan Informasi atas Pihak Ketiga">
			</textarea>
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th>&nbsp;</th>
		<th colspan="4">
			<span class="info"><em>* Lampirkan pernyataan bahwa calon pembayarPremi Polis/tertanggung telah melakukan verifikasi atas kebenaran informasi Pihak Ketiga, dan identitas Pihak Ketiga perorangan yaitu fotokopi
				KTP/SIM/Paspor/KITAS sedangkan identitas Pihak Ketiga berbentuk perusahaan adalah fotokopi Anggaran Dasar, NPWP dan SIUP.</em></span>
		</th>
	</tr>	
	<tr>
		<th width="18">&nbsp;</th>
		<th width="320">Ditandatangani di :</th>
		<th>
			<spring:bind path="cmd.pembayarPremi.tempat_ttd">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
				</c:if> title="Tempat Tanda Tangan">
			</spring:bind><em>*</em>
		</th>
		<th>Pada Tanggal :</th>
		<th>
			<spring:bind path="cmd.pembayarPremi.tgl_ttd">
				<input type="text" id="${status.expression}" name="${status.expression}" value="${status.value}" class="datepicker" readonly="readonly" />
			</spring:bind><em>*</em>
		</th>
	</tr>
	<tr>
		 <th colspan="5" class="subtitle">&nbsp;</th>
	</tr>
	<tr>
		<th colspan="5"><em>* Wajib diisi</em></th>
	</tr>		
</table>
<script type="text/javascript">
	//add table untuk 6A, 6B
	function addRow1(){
		var idx = '1';
		var tr = $('#TableAddPendapatan1 tbody tr:last-child');
		var check = $(tr).find('input[type=checkbox]').val();
		
		if(check != null){
			idx = parseInt(check) + 1;
		}
			
		var contentRow = '<tr class="check1_'+idx+'"><th>&nbsp;</th><th>&nbsp;</th>'
								+'<th>'
									+'<select name="${status.expression}'+idx+'">'
									+'<c:forEach var="c" items="${select_mkl_smbr_penghasilan}">'
											+'<option<c:if test="${status.value eq c.key}"> SELECTED </c:if> value="${c.key}">${c.value}'
											+'</option>'
									+	'</c:forEach>'
									+'</select>'
								+'</th>'
								+'<th><input class="noBorder" value="'+idx+'" type="checkbox" name="cek'+idx+'" id= "ck'+idx+'"></th>'
						+'</tr>';
		var oRow = add_new_row('#TableAddPendapatan1',contentRow);
	}
	function addRow2(){
		var idx = '1';
		var tr = $('#TableAddPendapatan2 tbody tr:last-child');

		var check = $(tr).find('input[type=checkbox]').val();
		
		if(check != null){
			idx = parseInt(check) + 1;
		}
		
		var contentRow = '<tr class="check2_'+idx+'"><th>&nbsp;</th><th>&nbsp;</th>'
							+'<th>'
								+'<select name="${status.expression}'+idx+'">'
									+'<c:forEach var="c" items="${select_pendapatan_non_rutin}">'
										+'<option<c:if test="${status.value eq c.key}"> SELECTED </c:if> value="${c.key}">${c.value}'
										+'</option>'
									+'</c:forEach>'
								+'</select>'
							+'</th>'
							+'<th><input class="noBorder" value="'+idx+'" type="checkbox" name="cek'+idx+'" id= "ck'+idx+'"></th>'
						 +'</tr>';
		var oRow = add_new_row('#TableAddPendapatan2',contentRow);
	}
	function addRow3(){
		var idx = '1';
		var tr = $('#TableAddPendapatan3 tbody tr:last-child');

		var check = $(tr).find('input[type=checkbox]').val();
		
		if(check != null){
			idx = parseInt(check) + 1;
		}
		
		var contentRow = '<tr class="check2_'+idx+'"><th>&nbsp;</th><th>&nbsp;</th>'
							+'<th>'
								+'<select name="${status.expression}'+idx+'">'
									+'<c:forEach var="c" items="${select_mkl_tujuan}">'
										+'<option<c:if test="${status.value eq c.key}"> SELECTED </c:if> value="${c.key}">${c.value}'
										+'</option>'
									+'</c:forEach>'
								+'</select>'
							+'</th>'
							+'<th><input class="noBorder" value="'+idx+'" type="checkbox" name="cek'+idx+'" id= "ck'+idx+'"></th>'
						 +'</tr>';
		var oRow = add_new_row('#TableAddPendapatan3',contentRow);
	}
	
	function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
    
    
    //Delete row 
    function delRow1(){
    	var tr = $('#TableAddPendapatan1 tbody').children();
    	var check = $(tr).find('input[type=checkbox]:checked');
    	$.each( check, function( key, value ) {
    		idx = value.value;
    		$('.check1_'+idx).remove();
		}); 	
    }
    function delRow2(){
    	var tr = $('#TableAddPendapatan2 tbody').children();
    	var check = $(tr).find('input[type=checkbox]:checked');
    	$.each( check, function( key, value ) {
    		idx = value.value;
    		$('.check2_'+idx).remove();
		});
	} 
	 function delRow3(){
    	var tr = $('#TableAddPendapatan3 tbody').children();
    	var check = $(tr).find('input[type=checkbox]:checked');
    	$.each( check, function( key, value ) {
    		idx = value.value;
    		$('.check2_'+idx).remove();
		});
	} 

</script>
