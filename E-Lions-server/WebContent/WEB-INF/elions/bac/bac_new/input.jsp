<%@ include file="/include/page/header_jquery.jsp"%>
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<c:set var="path" value="${pageContext.request.contextPath}" />

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 23em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 23.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk button container */
	.buttons { text-align: center; margin: 1em; margin-top: 0em; }
	
	/* styling untuk label didalam tabel (contohnya untuk telepon) 
	label.tableElem { margin-right: 0.4em; width: 135px; display: block; float: left; }
	*/	

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }

</style>

<body >

	<%-- FIXME: nanti remove ini untuk development saja
	<script type="text/javascript"
	  src="http://jqueryui.com/themeroller/themeswitchertool/">
	</script>
	<div id="switcher"></div>
	--%>
	
	<form:form commandName="cmd" id="formpost" method="post" enctype="multipart/form-data">
	
	<table class="entry2" id="alternatecolor">
		<tr>
			<td colspan=5 height="20" style="text-align: center;">
				<input type="button"  value="Copy Data dari Spaj:" 
					onclick="return tanyaPsave(document.frmParam.kopiSPAJ.value,document.frmParam.elements['pemegang.flag_upload'].value);">
				<input type="text" id="kopiSPAJ" name="kopiSPAJ" value=${cmd.pemegang.reg_spaj}>
				<input type="button" value="Cari" name="searchkopi" onclick="popWin('${path}/uw/spajgutri.htm?win=edit', 350, 450); ">
				 	<!-- &search=yes -->
				<br>
				<input type="checkbox" name="specta_save1" class="noBorder"  value="${cmd.datausulan.specta_save}"  size="30" onClick="specta_save_onClick();"> SPECTA SAVE
				<spring:bind path="cmd.datausulan.specta_save"> 
	              <input type="hidden" name="${status.expression}" value="${cmd.datausulan.specta_save}"  size="30" style='background-color :#D4D4D4'readOnly>
				</spring:bind>
				<input type="checkbox" name="flag_upload" class="noBorder"  value="${cmd.pemegang.flag_upload}"  size="30" onClick="flag_upload_onClick();"> SPAJ UPLOAD
				<spring:bind path="cmd.pemegang.flag_upload"> 
	              <input type="hidden" name="${status.expression}" value="${cmd.pemegang.flag_upload}"  size="30" style='background-color :#D4D4D4'readOnly>
				</spring:bind>
				<br>
				PEMEGANG POLIS ADALAH BADAN HUKUM/USAHA :
				<select id = "jenis_pemegang" name="datausulan.jenis_pemegang_polis">
					<option value="0" <c:if test="${cmd.datausulan.jenis_pemegang_polis eq 0}">SELECTED</c:if>>TIDAK</option>
					<option value="1" <c:if test="${cmd.datausulan.jenis_pemegang_polis eq 1}">SELECTED</c:if>>YA</option>
				</select>
				
				<spring:bind path="cmd.datausulan.flag_worksite">
					<input type="hidden" name="${status.expression}" value="${status.value }" size="20">
				</spring:bind> 
				<spring:bind path="cmd.pemegang.cab_bank">
					<input type="hidden" name="${status.expression}" value="${status.value }" size="20">
				</spring:bind>
				<spring:bind path="cmd.pemegang.jn_bank">
					<input type="hidden" name="${status.expression}" value="${status.value }" size="20">
				</spring:bind>
				<spring:bind path="cmd.pemegang.lus_id">
					<input type="hidden" name="${status.expression}" value="${status.value }" size="20">
				</spring:bind>
				<spring:bind path="cmd.pemegang.cbg_lus_id">
					<input type="hidden" name="${status.expression}" value="${status.value }" size="20">
				</spring:bind>
				<br><br>
				<div class="buttons">
					<input type="button" title="Input SPAJ baru" id="New" value="New">
					<input id="btnSave" type="submit" title="Simpan SPAJ" value="Save">
				</div>
			</td>
		</tr>
		<tr>
			<td width="67%" align="left" valign="top">
				<%-- <spring:bind path="cmd.*">
					<c:if test="${not empty status.errorMessages}">
						<div id="error">ERROR:<br>
							<c:forEach var="error" items="${status.errorMessages}">
								- <c:out value="${error}" escapeXml="false" /><br />
							</c:forEach>
						</div>
					</c:if>
				</spring:bind> --%>	
			<c:if test="${cmd.pemegang.jn_bank eq 2}">
			 	<c:if test="${cmd.pemegang.rate_1 eq 0 }">
				  	<center><font size ="3" color="red">Rate Simas Prima per hari ini belum ada. Silahkan hubungi AO anda atau bagian Investment kami.</font></center>
				  	<script>alert('Rate Simas Prima per hari ini belum ada. Silahkan hubungi AO anda atau bagian Investment kami.');</script>
			 	</c:if>
			 	<c:if test="${cmd.pemegang.rate_2 eq 0 }">
				  	<center><font size ="3" color="red">Rate Simas Prima Manfaat Bulanan per hari ini belum ada. Silahkan hubungi AO anda atau bagian Investment kami.</font></center>
				  	<script>alert('Rate Simas Prima Manfaat Bulanan per hari ini belum ada. Silahkan hubungi AO anda atau bagian Investment kami.');</script>
			 	</c:if>
			  </c:if>
			</td>
		</tr>
	</table>
	
	<input id="submitType" name="submitType" type="hidden" value=${submitType}>
	<!-- Tabs -->
	<div id="tabs">
		<ul>
			<li><a href="#tabs-pp">Pemegang Polis</a></li>
			<li><a href="#tabs-ttg">Tertanggung</a></li>
			<li><a href="#tabs-prm">Pembayar Premi</a></li>
			<li><a href="#tabs-da">Detil Agent</a></li>
			<li><a href="#tabs-asu">Asuransi</a></li>
		</ul>
		<div id="tabs-pp">
			<c:choose>
			   <c:when test="${cmd.datausulan.jenis_pemegang_polis == 1}">
				   		<jsp:include page="input_pp_badanusaha.jsp" >
							<jsp:param name="path" value="${path }"></jsp:param>
						</jsp:include>
			   </c:when>
			   <c:otherwise>
			   		
				   		<jsp:include page="input_pp.jsp" >
						<jsp:param name="path" value="${path }"></jsp:param>
						</jsp:include>	
			   </c:otherwise> 
			</c:choose>
		</div>
		<div id="tabs-ttg">
			<jsp:include page="input_ttg.jsp" >
				<jsp:param name="path" value="${path }"></jsp:param>
			</jsp:include>
		</div>
		<div id="tabs-prm">
			<jsp:include page="input_prm.jsp" >
				<jsp:param name="path" value="${path }"></jsp:param>
			</jsp:include>
		</div>
		<div id="tabs-da">
			<jsp:include page="input_da.jsp" >
				<jsp:param name="path" value="${path }"></jsp:param>
			</jsp:include>
		</div> 
		<div id="tabs-asu">
			<!-- Pilih satu berdasarkan produk yang dipilih -->
			<jsp:include page="input_asu_ul.jsp" >
				<jsp:param name="path" value="${path }"></jsp:param>
			</jsp:include>
			
			<jsp:include page="input_asu_nul.jsp" >
				<jsp:param name="path" value="${path }"></jsp:param>
			</jsp:include>
			
			<jsp:include page="input_asu_simpol.jsp" >
				<jsp:param name="path" value="${path }"></jsp:param>
			</jsp:include>
			
			<jsp:include page="input_asu_sprima.jsp" >
				<jsp:param name="path" value="${path }"></jsp:param>
			</jsp:include>
			
			<jsp:include page="input_asu_psave.jsp" >
				<jsp:param name="path" value="${path }"></jsp:param>
			</jsp:include>
			
			<jsp:include page="input_asu_slinksatu.jsp" >
				<jsp:param name="path" value="${path }"></jsp:param>
			</jsp:include>
			
			<jsp:include page="input_asu_kcl.jsp" >
				<jsp:param name="path" value="${path }"></jsp:param>
			</jsp:include>
			
			<jsp:include page="input_asu_familyplan.jsp" >
				<jsp:param name="path" value="${path }"></jsp:param>
			</jsp:include>
		</div> 
		
		<div class="buttons">
			<input type="button" title="Input SPAJ baru" id="New" value="New">
			<input id="btnSave" type="submit" title="Simpan SPAJ" value="Save">
		</div>
	</div>

	</form:form>

</body>

<script type="text/javascript">
	$("#jenis_pemegang").change(function(){
		var jenis = $(this).find('option:selected').val();
		if(jenis == 1){
			var postData = {"test":"123"};
			$('#submitType').val($(this).find('option:selected').text());
			$('#formpost').submit();
		}else{
			$('#submitType').val($(this).find('option:selected').text());
			document.getElementById('formpost').submit();
		}
		
	});
	
	
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		// (jQueryUI Tabs) init tab2 Utama (Pemegang, Tertanggung, dll)
		$("#tabs").tabs();

		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI autocomplete) init semua field autocomplete
		var availableTags = [ "ActionScript", "AppleScript", "Asp", "BASIC",
				"C", "C++", "Clojure", "COBOL", "ColdFusion", "Erlang",
				"Fortran", "Groovy", "Haskell", "Java", "JavaScript", "Lisp",
				"Perl", "PHP", "Python", "Ruby", "Scala", "Scheme" ];
		
		$("#acSumberBisnis").autocomplete({
			source : availableTags
		});
		$("#acBankPusat").autocomplete({
			source : availableTags
		});
					
		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});
		$(".jtable tr").hover(function() {
			$(this).children("td").addClass("ui-state-hover");
		}, function() {
			$(this).children("td").removeClass("ui-state-hover");
		});
		//$(".jtable tr").click(function() {
		//	$(this).children("td").toggleClass("ui-state-highlight");
		//});
		
		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, input:reset, button").button(); //styling untuk semua tombol
		$( "#btnCopyDataSpaj" ).button({
            icons: {
                primary: "ui-icon-search"
            }
        });
		//$( ".radio" ).buttonset();
		//$( ".checkbox" ).buttonset();
		
		// (jQuery Form Plugin) rubah form jadi ajax
		var options = {
			target : '#output1',
			beforeSubmit : showRequest,
			success : showResponse
		};
		$('#formPost').ajaxForm(options); // bind form using 'ajaxForm'
		
		function showRequest(formData, jqForm, options) { // pre-submit callback 
		    var queryString = $.param(formData);
		    //alert('About to submit: \n\n' + queryString); 
		    return true; 
		}
		function showResponse(responseText, statusText, xhr, $form) { // post-submit callback 
		    alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + 
		        '\n\nThe output div should have already been updated with the responseText.'); 
		}
		
		<%--
		//theme switcher (FIXME untuk development saja)
		$('#switcher').themeswitcher();		
		--%>
		
		// (jQuery Validation Plugin) client-side validation untuk formPost on keyup and on submit
		$.validator.setDefaults({
			highlight: function(input) {
				$(input).addClass("ui-state-error");
			},
			unhighlight: function(input) {
				$(input).removeClass("ui-state-error");
			}
		});
		
		$("#formPost").validate({
			rules: {
				mspo_no_blanko: {
					required: true,
					minlength: 2
				},
				pemegang: "required"
			},
			messages: {
				mspo_no_blanko: {
					required: "Harap isi No Blanko",
					minlength: "No blanko minimal 2 karakter"
				},
				pemegang: "Harap isi nama Pemegang Polis"
			}
		});
		
		$('#New').click(function() {
    	      location.reload();
 
		});
	});
</script>
</html>