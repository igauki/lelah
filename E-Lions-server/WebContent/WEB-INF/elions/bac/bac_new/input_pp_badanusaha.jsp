<%@ include file="/include/page/header_jquery.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />

<table class="entry">
	<tr>
		<th></th>
		<th>Nomor Registrasi (SPAJ) </th>
		<th><input type="reg_spaj"
			value="<elions:spaj nomor='${cmd.pemegang.reg_spaj}'/>"
			style='background-color :#D4D4D4' readOnly> &nbsp;&nbsp;&nbsp;&nbsp;
			<c:if test="${cmd.datausulan.flag_worksite_ekalife eq 1}"> Karyawan AJ Sinarmas</c:if>
			</th>
		<th>SPAJ</th>
		<th>
			<select name="pemegang.mste_spaj_asli">
				<option value="1" <c:if test="${cmd.pemegang.mste_spaj_asli eq 1}">selected="selected"</c:if>>ASLI</option>
				<option value="0" <c:if test="${cmd.pemegang.mste_spaj_asli eq 0}">selected="selected"</c:if>>FOTOKOPI/FAX</option>
			</select>
		</th>
	</tr>
	<tr>
		<th></th>
		<th>Medis</th>
		<th>
			<select name="datausulan.mste_medical" >
			<c:forEach var="medis" items="${select_medis}">
				<option
					<c:if test="${cmd.datausulan.mste_medical eq medis.ID}"> SELECTED </c:if>
					value="${medis.ID}">${medis.MEDIS}</option>
			</c:forEach>
			</select>
			<font color="#CC3300">*</font>
		</th>
		<th>Nomor Seri</th>
		<th><spring:bind path="cmd.pemegang.mspo_no_blanko">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="20" maxlength="11"
			<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
				<font color="#CC3300">*</font>
		</spring:bind></th>
	</tr>
	<tr>
		<th></th>
		<th>NO PB</th>
		<th ><spring:bind path="cmd.no_pb">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="20" 
				maxlength="100"
			<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
			<font color="#CC3300">*</font>
		</spring:bind>
		</th>
		<th>NO CIF</th>
		<th ><spring:bind path="cmd.pemegang.mspo_nasabah_dcif">
							<input type="text" name="${status.expression}"
								value="${status.value }" size="20" 
								maxlength="100"
			<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
			<font color="#CC3300">*</font>
		</spring:bind>
		</th>
	</tr>
	<c:if test="${cmd.currentUser.lca_id ne \"09\" }">
		<tr>
			<th></th>
			<th>CARI SUMBER BISNIS</th>
			<th colspan="3">
				<input type="text" name="carisumber" onkeypress="if(event.keyCode==13){ document.frmParam.btnsumber.click(); return false;}"> 
             	<input type="button" name="btnsumber" value="Cari" onclick="ajaxSelectWithParam1a(document.frmParam.carisumber.value,'select_sumberBisnis','sumber','pemegang.sumber_id','', 'SUMBER_ID', 'NAMA_SUMBER', '','Silahkan pilih SUMBER BISNIS','3');"> 
			</th>
		</tr>
		<tr>
			<th></th>
			<th></th>
			<th colspan="3">
				<div id="sumber"> 
					<select name="pemegang.sumber_id" 
						 <c:if test="${ not empty status.errorMessage}">
								style='background-color :#FFE1FD'
						 </c:if>>
		                 <option value="${cmd.pemegang.sumber_id}">${cmd.pemegang.nama_sumber}</option>
		            </select>
	            </div>
			</th>
		</tr>
	</c:if>
	
	<tr>
	
		<tr>
			<th colspan=5 class="subtitle">DATA BADAN HUKUM / USAHA (PEMEGANG POLIS)</th>
		</tr>
	<tr><th width="18">1.</th>
		<th>Jenis & Nama Badan Hukum / Usaha
		</th>
		<th width="500">
			<select name="pemegang.lti_id">
				<option value=""></option>
				<c:forEach var="l" items="${select_gelar_bu}">
					<option
						<c:if test="${cmd.pemegang.lti_id eq l.key}"> SELECTED </c:if>
						value="${l.key}">${l.value}</option>
				</c:forEach>
			</select>				
			<spring:bind path="cmd.pemegang.mcl_first">
				<input type="text" name="${status.expression}"
					value="${status.value }" size="42" 
					maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
					style='background-color :#FFE1FD'
				</c:if>>
				<font color="#CC3300">*</font>
			</spring:bind>
		</th>
		<th colspan="2"></th>
	</tr>
	<tr>
		<th width="18">2.</th>
		<th>Anggaran Dasar No.</th>
		<th>
			<input type="text" name="${status.expression}" value="${status.value }" size="22" maxlength="100">
			Notaris <input type="text" name="${status.expression}" value="${status.value }" size="42" maxlength="100">
		</th>
		<th>Tanggal</th>
		<th>
			<input type="text" id="${status.expression}" name="${status.expression}" value="${status.value}" class="datepicker" readonly="readonly" />
		</th>
	</tr>
	<tr>
		<th width="18">3.</th>
		<th width="280" >No. SIUP
		</th>
		<th>
			<spring:bind path="cmd.personal.mpt_siup">
				<input type="text" name="${status.expression}"
					value="${status.value }" size="40" maxlength="35"
					<c:if test="${ not empty status.errorMessage}">
					style='background-color :#FFE1FD'
				</c:if>><font color="#CC3300">*</font>
			</spring:bind>
		</th>
		<th>Tanggal Terdaftar
		</th>
		<th>
			<spring:bind path="cmd.personal.tgl_siup">
				<input type="text" id="${status.expression}" name="${status.expression}" value="${status.value}" class="datepicker" readonly="readonly" />
				<font color="#CC3300">*</font></spring:bind>
		</th>	
	</tr>
	<tr>
		<th width="18">4.</th>
		<th>Klasifikasi Bidang Industri</th>
		<th>
			<select name="pemegang.mkl_industri" onChange="bidIndustri1(this.value)">
				<c:forEach var="industri" items="${select_industri}">
					<option
						<c:if test="${status.value eq industri.key}"> SELECTED </c:if>
						value="${industri.key}">${industri.value}</option>
				</c:forEach>
			</select>
			<font color="#CC3300">*</font>
		</th>
		<th>Lainnya, Sebutkan</th>
		<th colspan="3">
			<spring:bind path="cmd.pemegang.industria">
				<input type="dis" id='lainIndustri' name="${status.expression}"
					value="${status.value }" size="40" maxlength="50"
					<c:if test="${ not empty status.errorMessage}">
					style='background-color :#FFE1FD'
				</c:if>>
			</spring:bind>
		</th>
	</tr>
	
	<tr><th width="18">5.</th>
		<th>Tempat dan Tanggal Berdiri<br>
		</th>
		<th colspan="3">
		<spring:bind path="cmd.pemegang.mspe_place_birth">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="30"
			<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		<font color="#CC3300">*</font></spring:bind>	
						<spring:bind path="cmd.pemegang.mspe_date_birth">
			<input type="text" id="${status.expression}" name="${status.expression}" value="${status.value}" class="datepicker" readonly="readonly" />
			<font color="#CC3300">*</font></spring:bind>
		</th>
	</tr>
	<tr>
		<th rowspan="6">6.</th>
		<th rowspan="6">Alamat</th>
		<th rowspan="6"><spring:bind path="cmd.pemegang.alamat_rumah">
			<textarea cols="70" rows="11" name="${status.expression }"
				onkeyup="textCounter(this, 200); "
				onkeydown="textCounter(this, 200); "
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>${status.value }</textarea>
		<font color="#CC3300">*</font></spring:bind></th>
		<th>Kode Pos</th>
		<th><spring:bind path="cmd.pemegang.kd_pos_rumah">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="20" maxlength="10"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th>Kota</th>
		<th ><spring:bind path="cmd.pemegang.kota_rumah">
		<input type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
		<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
    	<span id="indicator_rumah" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
		</spring:bind>   
		<font color="#CC3300">*</font>
		<spring:bind path="cmd.pemegang.kota_kantor">
		<input type="hidden" name="${status.expression }" style="width: 1px;" id="${status.expression }" value="${status.value }" onfocus="this.select();"
		<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
    	<span id="indicator_kantor" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
		</spring:bind>  
		</th>
	</tr>
	<tr>
		<th>Telp 1</th>
		<th><spring:bind path="cmd.pemegang.area_code_rumah">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="5" maxlength="4"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind> <spring:bind path="cmd.pemegang.telpon_rumah">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="12" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th>Telp 2</th>
		<th><spring:bind path="cmd.pemegang.area_code_rumah2">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="5" maxlength="4"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind> <spring:bind path="cmd.pemegang.telpon_rumah2">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="12" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th>No. Fax</th>
		<th><spring:bind path="cmd.pemegang.area_code_fax">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="5" maxlength="4"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind> <spring:bind path="cmd.pemegang.no_fax">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="12" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th>Alamat E-mail</th>
		<th><spring:bind path="cmd.pemegang.email">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="40" maxlength="50"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th width="18">7.</th>
		<th>No. NPWP </th>
		<th><spring:bind path="cmd.personal.mpt_npwp">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="40" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>><font color="#CC3300">*</font>
			</spring:bind>
		</th>
		<th>Tanggal Terdaftar</th>
		<th>
			<spring:bind path="cmd.personal.tgl_npwp">
			<input type="text" id="${status.expression}" name="${status.expression}" value="${status.value}" class="datepicker" readonly="readonly" />
			<font color="#CC3300">*</font></spring:bind>
		</th>
	</tr>
	<tr>
		<th width="18">8.</th>
		<th>Tujuan Membeli Asuransi</th>
		<th>
			<spring:bind path="cmd.pemegang.mkl_tujuan">
				<select  name="${status.expression}" onChange="tujuanBeli(this.value)">
					<c:forEach var="tujuan" items="${select_tujuan}">
						<option
							<c:if test="${status.value eq tujuan.key}"> SELECTED </c:if>
							value="${tujuan.key}">${tujuan.value}</option>
					</c:forEach>
				</select>
			</spring:bind> 
		<font color="#CC3300">*</font>
		</th>
		<th>Lain-Lain, Jelaskan</th>
		<th>
			<spring:bind path="cmd.pemegang.tujuana">
				<input type="dis" id="lainTujuan" name="${status.expression}"
					value="${status.value}" size="42" maxlength="90"
					<c:if test="${ not empty status.errorMessage}">
					style='background-color :#FFE1FD'
					</c:if>>
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th width="18">9.</th>
		<th>Sumber Dana</th>
		<th><select name="pemegang.mkl_pendanaan" onChange="pendanaan(this.value)">
			<c:forEach var="dana" items="${select_dana_badan_usaha}">
				<option
					<c:if test="${status.value eq dana.key}"> SELECTED </c:if>
					value="${dana.key}">${dana.value}
				</option>
			</c:forEach>
			</select>
		 <font color="#CC3300">*</font>
		 </th>
		<th>Lainnya, Jelaskan</th>
		<th><spring:bind path="cmd.pemegang.danaa">
			<input type="dis" id="lainPendanaan" name="${status.expression}"
				value="${status.value }" size="42" maxlength="100"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind>
		</th>
	</tr>
	<tr>
		<th width="18">10.</th>
		<th>Aset</th>
		<th colspan="3"><select name="personal.mkl_aset"
			>
			<c:forEach var="aset" items="${select_aset}">
				<option
					<c:if test="${cmd.personal.mkl_aset eq aset.ID}"> SELECTED </c:if>
					value="${aset.ID}">${aset.ASET}</option>
			</c:forEach>
		</select>
		 <font color="#CC3300">*</font></th>
	</tr>
	<tr>
		<th width="18">11.</th>
		<th>Perkiraan Penghasilan Kotor Per Tahun</th>
		<th colspan="3">
			<spring:bind path="cmd.pemegang.mkl_penghasilan">
				<select  name="${status.expression}" >
					<c:forEach var="penghasilan" items="${select_penghasilan}">
						<option
							<c:if test="${status.value eq penghasilan.key}"> SELECTED </c:if>
							value="${penghasilan.key}">${penghasilan.value}</option>
					</c:forEach>
				</select>
			</spring:bind>
		 <font color="#CC3300">*</font></th>
	</tr>
	
	<tr>
		<th colspan=5 class="subtitle">DATA PIC
		 <select name="contactPerson.pic_jenis">
				<option value="1" <c:if test="${cmd.contactPerson.pic_jenis eq 1}">selected="selected"</c:if>>Penanggung jawab sesuai yang tercantum di AD</option>
				<option value="2" <c:if test="${cmd.contactPerson.pic_jenis eq 2}">selected="selected"</c:if>>Yang diberi kuasa</option>
			</select></th>
	</tr>
	<tr>
		<th width="18">12.</th>
		<th>Nama Lengkap<br>
		<span class="info">(sesuai dengan KTP / Identitas lain tanpa gelar)</span>
		</th>
		<th>
			<spring:bind path="cmd.pemegang.lti_id">
				<select  name="${status.expression}" >
					<c:forEach var="l" items="${select_gelar}">
						<option
							<c:if test="${status.value eq l.key}"> SELECTED </c:if>
							value="${l.key}">${l.value}</option>
					</c:forEach>
				</select>
			</spring:bind>			
			<spring:bind path="cmd.contactPerson.nama_lengkap">
				<input type="text" name="${status.expression}"
					value="${status.value }" size="42" 
					maxlength="100"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
				<font color="#CC3300">*</font>
			</spring:bind>
					</th>
		<th>Gelar &nbsp;</th>
		<th><spring:bind path="cmd.contactPerson.mcl_gelar">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="20" maxlength="20">
		</spring:bind></th>
	</tr>
	<tr>
		<th width="18">13.</th>
		<th>Nama Ibu Kandung</th>
		<th colspan="3"><spring:bind path="cmd.contactPerson.mspe_mother">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="50"
		<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'</c:if>>
		</spring:bind><font color="#CC3300">*</font></th>
	</tr>
	<tr>
		<th width="18">14.</th>
		<th>Jabatan</th>
		<th><spring:bind path="cmd.contactPerson.mpn_job_desc">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
		<th>Masa Kerja</th>
		<th><spring:bind path="cmd.contactPerson.lama_kerja">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="20" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th width="18">15.</th>
		<th>Bukti Identitas</th>
		<th colspan="3">
			<spring:bind path="cmd.contactPerson.lside_id">
				<select name="${status.expression}" onChange="bixx(this.value);">
					<c:forEach var="d" items="${select_identitas}">
						<option
							<c:if test="${status.value eq d.key}"> SELECTED </c:if>
							value="${d.key}">${d.value}</option>
					</c:forEach>
				</select>
				<font color="#CC3300">*</font>
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th>&nbsp;</th>
		<th>No. KTP / Identitas lain</th>
		<th><spring:bind path="cmd.contactPerson.no_identity">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="50"
			<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>	
		> <font color="#CC3300">*</font>
		</spring:bind></th>
		
		<th>Tanggal Kadaluarsa</th>
		<th id ="expired"><spring:bind path="cmd.contactPerson.no_identity_expired">
			<input type="text" id="${status.expression}" name="${status.expression}" value="${status.value}" class="datepicker" readonly="readonly" />
			<font color="#CC3300">*</font></spring:bind></th>
		<th colspan="2">&nbsp;</th>
	</tr>
	<tr>
		<th width="18">16.</th>
		<th>Warga Negara</th>
		<th colspan="3">
			<spring:bind path="cmd.contactPerson.lsne_id">
				<select name="${status.expression}">
					<c:forEach var="d" items="${select_negara}">
						<option
							<c:if test="${status.value eq d.key}"> SELECTED </c:if>
							value="${d.key}">${d.value}</option>
					</c:forEach>
				</select>
				<font color="#CC3300">*</font>
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th width="18">17.</th>
		<th>Kota dan Negara Kelahiran, Tgl Lahir</th>
		<th>
			<spring:bind path="cmd.contactPerson.place_birth">
				<input type="text" name="${status.expression}"
					value="${status.value }" size="42" maxlength="30"
				<c:if test="${ not empty status.errorMessage}">
					style='background-color :#FFE1FD'
				</c:if>>
				<font color="#CC3300">*</font>
			</spring:bind>
			<spring:bind path="cmd.contactPerson.date_birth">
				<input id="bdate_2" type="text" id="${status.expression}" name="${status.expression}" value="${status.value}" class="datepicker" readonly="readonly" onchange="bdate2(this.value)" />
				<font color="#CC3300">*</font>
			</spring:bind>
		</th>
		<th>Usia</th>
		<th><spring:bind path="cmd.contactPerson.mste_age">
			<input class="age_2" type="dis" name="${status.expression}"
				value="${status.value }" size="4" readOnly>
			</spring:bind> tahun
		</th>
	</tr>
	<tr>
		<th width="18">18.</th>
		<th>Jenis Kelamin</th>
		<th><spring:bind path="cmd.contactPerson.mste_sex">
			<label for="cowok"> <input type="radio" class=noBorder
				name="${status.expression}" value="1"
				<c:if test="${cmd.contactPerson.mste_sex eq 1 or cmd.contactPerson.mste_sex eq null}"> 
							checked</c:if>
				id="cowok">Pria </label>
			<label for="cewek"> <input type="radio" class=noBorder
				name="${status.expression}" value="0"
				<c:if test="${cmd.contactPerson.mste_sex eq 0}"> 
							checked</c:if>
				id="cewek">Wanita </label><font color="#CC3300">*</font>
		</spring:bind> </th>
		<th>Status</th>
		<th>
			<spring:bind path="cmd.pemegang.mspe_sts_mrt">
				<select  name="${status.expression}" >
					<c:forEach var="marital" items="${select_marital}">
						<option
							<c:if test="${status.value eq marital.key}"> SELECTED </c:if>
							value="${marital.key}">${marital.value}</option>
					</c:forEach>
				</select>
			</spring:bind>
			<font color="#CC3300">*</font>
		</th>
	</tr>
	<tr>
		<th>a.</th>
		<th>Nama Suami/Istri</th>
		<th><spring:bind path="cmd.contactPerson.nama_si">
			<input type="text" name="${status.expression}"
		value="${status.value }" size="42" maxlength="30"
			<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>></spring:bind></th>
		<th>Tanggal Lahir</th>
		<th><spring:bind path="cmd.contactPerson.tgllhr_si">
			<input type="text" id="${status.expression}" name="${status.expression}" value="${status.value}" class="datepicker" readonly="readonly" />
		</spring:bind></th>
	</tr>
	<tr>
		<th>b.</th>
		<th>Nama Anak 1</th>
		<th><spring:bind path="cmd.contactPerson.nama_anak1">
			<input type="text" name="${status.expression}"
		value="${status.value }" size="42" maxlength="30"
		<c:if test="${ not empty status.errorMessage}">
			style='background-color :#FFE1FD'
		</c:if>>
		</spring:bind></th>
		<th>Tanggal Lahir</th>
		<th><spring:bind path="cmd.contactPerson.tgllhr_anak1">
			<input type="text" id="${status.expression}" name="${status.expression}" value="${status.value}" class="datepicker" readonly="readonly" />
			</spring:bind></th>
	</tr>
	<tr>
		<th>c.</th>
		<th>Nama Anak 2</th>
		<th><spring:bind path="cmd.contactPerson.nama_anak2">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="30"
			<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
		<th>Tanggal Lahir</th>
		<th><spring:bind path="cmd.contactPerson.tgllhr_anak2">
		<input type="text" id="${status.expression}" name="${status.expression}" value="${status.value}" class="datepicker" readonly="readonly" />
			</spring:bind></th>
	</tr>	
	<tr>
		<th>d.</th>
		<th>Nama Anak 3</th>
		<th><spring:bind path="cmd.contactPerson.nama_anak3">
			<input type="text" name="${status.expression}"
		value="${status.value }" size="42" maxlength="30"
		<c:if test="${ not empty status.errorMessage}">
			style='background-color :#FFE1FD'
		</c:if>>
			</spring:bind></th>
		<th>Tanggal Lahir</th>
		<th><spring:bind path="cmd.contactPerson.tgllhr_anak3">
			<input type="text" id="${status.expression}" name="${status.expression}" value="${status.value}" class="datepicker" readonly="readonly" />
			</spring:bind></th>
	</tr>											
	<tr>
		<th width="18">19.</th>
		<th>Agama</th>
		<th>
		<spring:bind path="cmd.contactPerson.lsag_id">
			<select name="${status.expression}" onchange="agamaChange1(this.value);">
				<c:forEach var="d" items="${select_agama}">
					<option
						<c:if test="${status.value eq d.key}"> SELECTED </c:if>
						value="${d.key}">${d.value}</option>
				</c:forEach>
			</select>
			<font color="#CC3300">*</font>
		</spring:bind>
		</th>
		<th>Lain - Lain, Sebutkan</th>
		<th>					
			<spring:bind path="cmd.contactPerson.agama">
				<input type="dis" class="agama1" name="${status.expression}"
					value="${status.value }" size="25" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
					style='background-color :#FFE1FD'
				</c:if>>
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th width="18">20.</th>
		<th>Pendidikan</th>
		<th colspan="3">
			<spring:bind path="cmd.contactPerson.lsed_id">
				<select name="${status.expression}">
					<c:forEach var="d" items="${select_pendidikan}">
						<option
							<c:if test="${status.value eq d.key}"> SELECTED </c:if>
							value="${d.key}">${d.value}</option>
					</c:forEach>
				</select>
				<font color="#CC3300">*</font>
			</spring:bind>
		</th>
	</tr>
	<tr>
		<th rowspan="5">21. a.</th>
		<th rowspan="5">Alamat Rumah</th>
		<th rowspan="5"><spring:bind path="cmd.contactPerson.alamat_rumah">
			<textarea id="almtRmh1_bu" cols="70" rows="9" name="${status.expression }"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
				</c:if>
				>${status.value }</textarea>
		<font color="#CC3300">*</font></spring:bind></th>
		<th>Kode Pos</th>
		<th><spring:bind path="cmd.contactPerson.kd_pos_rumah">
			<input id="kPostRmh1_bu" type="text" name="${status.expression}"
				value="${status.value }" size="20" maxlength="10"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th>Kota</th>
		<th ><spring:bind path="cmd.contactPerson.kota_rumah">
		<input id="kotaRmh1" type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
		<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
    	<span id="indicator_rumah" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
		</spring:bind>   
		<font color="#CC3300">*</font>
		</th>
	</tr>
	<tr><th></th><th></th></tr>
	<tr>
		<th>Telp Rumah 1</th>
		<th><spring:bind path="cmd.contactPerson.area_code_rumah">
			<input id="tlpRmh1a_bu" type="text" name="${status.expression}"
				value="${status.value }" size="5" maxlength="4"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
			</spring:bind> <spring:bind path="cmd.contactPerson.telpon_rumah">
			<input id="tlpRmh1b_bu" type="text" name="${status.expression}"
				value="${status.value }" size="12" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		<font color="#CC3300">*</font></spring:bind></th>
	</tr>
	<tr>
		<th>Telp Rumah 2</th>
		<th><spring:bind path="cmd.contactPerson.area_code_rumah2">
			<input id="tlpRmh2a_bu" type="text" name="${status.expression}"
				value="${status.value }" size="5" maxlength="4"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind> <spring:bind path="cmd.contactPerson.telpon_rumah2">
			<input id="tlpRmh2b_bu" type="text" name="${status.expression}"
				value="${status.value }" size="12" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th></th>
		<th>Handphone 1</th>
		<th><spring:bind path="cmd.contactPerson.no_hp">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind> <font color="#CC3300">*</font></th>
		<th>Email</th>
		<th><spring:bind path="cmd.contactPerson.email">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="40" maxlength="50"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th></th>
		<th>Handphone 2</th>
		<th colspan="3"><spring:bind path="cmd.contactPerson.no_hp2">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th rowspan="6">b.</th>
		<th rowspan="6">Alamat Kantor</th>
		<th rowspan="6"><spring:bind path="cmd.contactPerson.alamat_kantor">
			<textarea id="almtKantor1_bu" cols="70" rows="11" name="${status.expression }"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>${status.value }</textarea>
		</spring:bind></th>
		<th>Kode Pos</th>
		<th><spring:bind path="cmd.contactPerson.kd_pos_kantor">
			<input id="kPos1_bu" type="text" name="${status.expression}"
				value="${status.value }" size="20" maxlength="10"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th>Kota</th>
		<th><spring:bind path="cmd.contactPerson.kota_kantor">
		<input id="kota1_bu" type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
		<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
    	<span id="indicator_kantor" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
	</spring:bind>   
		</th>
	</tr>
	<tr><th></th><th></th></tr>
	<tr>
		<th>Telp Kantor 1</th>
		<th><spring:bind path="cmd.contactPerson.area_code_kantor">
			<input id="tlp_kntr1a_bu" type="text" name="${status.expression}"
				value="${status.value }" size="5" maxlength="4"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind> <spring:bind path="cmd.contactPerson.telpon_kantor">
			<input id="tlp_kntr1b_bu" type="text" name="${status.expression}"
				value="${status.value }" size="12" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th>Telp Kantor 2</th>
		<th><spring:bind path="cmd.contactPerson.area_code_kantor2">
			<input id="tlp_kntr2a_bu" type="text" name="${status.expression}"
				value="${status.value }" size="5" maxlength="4"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind> <spring:bind path="cmd.contactPerson.telpon_kantor2">
			<input id="tlp_kntr2b_bu" type="text" name="${status.expression}"
				value="${status.value }" size="12" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th>No Fax</th>
		<th><spring:bind path="cmd.contactPerson.area_code_fax">
			<input id="fax1a_bu" type="text" name="${status.expression}"
				value="${status.value }" size="5" maxlength="4"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind> <spring:bind path="cmd.contactPerson.no_fax">
			<input id="fax1b_bu" type="text" name="${status.expression}"
				value="${status.value }" size="12" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th colspan="5" class="subtitle" style="text-align: left;">Alamat Penagihan : 
		<spring:bind path="cmd.addressbilling.tagih">
			<select id="tgh2" name="${status.expression}" onChange="data_penagihan(this.value)">
				<option value="1">Lain - Lain</option>
				<option value="2">Alamat Rumah</option>
				<option value="3">Alamat Kantor</option>
			</select>
		 </spring:bind></th>
	</tr>
	<tr>
		<th rowspan="8">c.</th>
		<th rowspan="8">Alamat Penagihan / <br>
		Korespondensi</th>
		<th rowspan="8">
			<spring:bind path="cmd.addressbilling.msap_address">
				<textarea id="almtPenagihan_bu" class='Penagiahan_2' cols="70" rows="15" name="${status.expression }"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>${status.value }</textarea>
			<font color="#CC3300">*</font>
			</spring:bind>
		</th>
		<th>Kode Pos</th>
		<th><spring:bind path="cmd.addressbilling.msap_zip_code">
			<input id="kPos_penagihan_bu" class='Penagiahan_2' type="dis" name="${status.expression}"
				value="${status.value }" size="20" maxlength="10"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th>Kota</th>
		<th>
		<spring:bind path="cmd.addressbilling.kota_tgh">
		<input id="kota_penagihan_bu" class='Penagiahan_2' type="dis" name="${status.expression }" value="${status.value }" onfocus="this.select();"
		<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
    	 <span id="indicator_tagih" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
	</spring:bind>
		</th>
	</tr>	
	<tr>
		<th>Negara</th>
		<th>
			<spring:bind path="cmd.addressbilling.lsne_id">
				<select id="negara_penagihan_bu" name="${status.expression}">
					<c:forEach var="d" items="${select_negara}">
						<option
							<c:if test="${status.value eq d.key}"> SELECTED </c:if>
							value="${d.key}">${d.value}</option>
					</c:forEach>
				</select>
				<font color="#CC3300">*</font>
			</spring:bind>
		</th>
	</tr>			
	<tr><th></th><th></th></tr>
	<tr>
		<th>No Telepon1</th>
		<th><spring:bind path="cmd.addressbilling.msap_area_code1">
			<input id="tlp1a_penagihan_bu" class='Penagiahan_2' type="dis" name="${status.expression}"
				value="${status.value }" size="5" maxlength="4"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind> <spring:bind path="cmd.addressbilling.msap_phone1">
			<input id="tlp1b_penagihan_bu" class='Penagiahan_2' type="dis" name="${status.expression}"
				value="${status.value }" size="12" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		<font color="#CC3300">*</font></spring:bind></th>
	</tr>
	<tr>
		<th>No Telepon2</th>
		<th><spring:bind path="cmd.addressbilling.msap_area_code2">
			<input id="tlp2a_penagihan_bu" class='Penagiahan_2' type="dis" name="${status.expression}"
				value="${status.value }" size="5" maxlength="4"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind> <spring:bind path="cmd.addressbilling.msap_phone2">
			<input id="tlp2b_penagihan_bu" class='Penagiahan_2' type="dis" name="${status.expression}"
				value="${status.value }" size="12" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th>No Telepon3</th>
		<th><spring:bind path="cmd.addressbilling.msap_area_code3">
			<input id="tlp3a_penagihan_bu" class='Penagiahan_2' type="dis" name="${status.expression}"
				value="${status.value }" size="5" maxlength="4"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind> <spring:bind path="cmd.addressbilling.msap_phone3">
			<input id="tlp3b_penagihan_bu" class='Penagiahan_2' type="dis" name="${status.expression}"
				value="${status.value }" size="12" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th>No Fax</th>
		<th><spring:bind path="cmd.addressbilling.msap_area_code_fax1">
			<input id="fax1a_penagihan" class='Penagiahan_2' type="dis" name="${status.expression}"
				value="${status.value }" size="5" maxlength="4"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind> <spring:bind path="cmd.addressbilling.msap_fax1">
			<input id="fax1b_penagihan" class='Penagiahan_2' type="dis" name="${status.expression}"
				value="${status.value }" size="12" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th></th>
		<th>Handphone 1</th>
		<th><spring:bind path="cmd.addressbilling.no_hp">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind><br/><font color="#CC3300">* apabila tidak ada no hp, harap isi dengan 0000</font></th>
		<th>Email</th>
		<th><spring:bind path="cmd.addressbilling.e_mail">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="40" maxlength="50"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind></th>
	</tr>
	<tr>
		<th></th>
		<th>Handphone 2</th>
		<th colspan="3"><spring:bind path="cmd.addressbilling.no_hp2">
			<input type="text" name="${status.expression}"
				value="${status.value }" size="42" maxlength="20"
				<c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
		</spring:bind><br/><font color="#CC3300">* apabila tidak ada no hp, harap isi dengan 0000</font></th>
	</tr>
	<tr>
		<th >22.</th>
		<th>Hubungan Calon Tertanggung dengan Calon Pemegang Polis (Badan Usaha)</th>
		<th colspan="3"><select name="pemegang.lsre_id" 
		 <c:if test="${ not empty status.errorMessage}">
				style='background-color :#FFE1FD'
			</c:if>>
			<c:forEach var="relasi" items="${select_relasi_badan_usaha}">
				<option
					<c:if test="${cmd.pemegang.lsre_id eq relasi.ID}"> SELECTED </c:if>
					value="${relasi.ID}">${relasi.RELATION}</option>
			</c:forEach>
		</select>
		 <font color="#CC3300">*</font></th>
	</tr>
	<tr>
		<th class="subtitle" colspan="5"></th>
	</tr>		
         <tr> 
         <td colspan="9"> 
	 		<p align="left"><b> <font face="Verdana" size="2" color="#CC3300">Note : * Wajib diisi</font></b></p>
		</td>
        </tr>	
	
</table>

<style>
.textboxdisable
{
	border: 1px;
	border-color: #AAA;
	border-style: solid;
	background-color: #D9D9D9;
}
.textboxenable
{
	border: 1px;
	border-color: #AAA;
	border-style: solid;
	background-color: #FFF;
}
</style>	

<script>
	var jenis =  ${cmd.datausulan.jenis_pemegang_polis};
	if(jenis == 1){
		var tableEntry = $('.entry tr');
		tableEntry.find('input[type=text]').val('');
		tableEntry.find('textarea').val('');
	}
	
	//set age from bdate
	function calculateAge(birthday) {
	    var now = new Date();
	    var past = new Date(birthday);
	    var nowYear = now.getFullYear();
	    var pastYear = past.getFullYear();
	    var age = nowYear - pastYear;
	
	    return age;
	};
	$('.age_2').addClass("textboxdisable");
	function bdate2(bdate){
		$('.age_2').val(calculateAge(bdate));
	}

	function agamaChange1(id){
		if(id == 6){
			$('.agama1').removeClass("textboxdisable");
			$('.agama1').removeAttr('disabled');
			$('.agama1').addClass("textboxenable");
		}else{
			$('.agama1').val('');
			$('.agama1').removeClass("textboxenable");
			$('.agama1').attr('disabled','disabled');
			$('.agama1').addClass("textboxdisable");
		}
	}
	
	//set alamat penagihan
	function data_penagihan(id){
		if(id == '1'){
			$('.Penagiahan_2').val("");
			$('.Penagiahan_2').removeClass("textboxdisable");
			$('.Penagiahan_2').removeAttr('disabled');
			$('.Penagiahan_2').addClass("textboxenable");	
		}else if(id == '2'){
			$('.Penagiahan_2').removeClass("textboxenable");
			$('.Penagiahan_2').attr('disabled','disabled');
			$('.Penagiahan_2').addClass("textboxdisable");
			
			var alamat = $('#almtRmh1_bu').val();
			var kecamatan = $('#rmhkecamatan1_bu').val();
			var kota = $('#kotaRmh1_bu').val();
			var negara = $('#negaraRmh1_bu').val();
			var kPost = $('#kPostRmh1_bu').val();
			var tlp1a = $('#tlpRmh1a_bu').val();
			var tlp1b = $('#tlpRmh1b_bu').val();
			var tlp2a = $('#tlpRmh2a_bu').val();
			var tlp2b = $('#tlpRmh2b_bu').val();
			
			$('#almtPenagihan_bu').val(alamat);
			$('#kecamatanPenagihan_bu').val(kecamatan);
			$('#kPos_penagihan_bu').val(kPost);
			$('#kota_penagihan_bu').val(kota);
			$('#negara_penagihan_bu').val(negara);
			$('#tlp1a_penagihan_bu').val(tlp1a);
			$('#tlp1b_penagihan_bu').val(tlp1b);
			$('#tlp2a_penagihan_bu').val(tlp2a);
			$('#tlp2b_penagihan_bu').val(tlp2b);
			$('#fax1a_penagihan_bu').val('');
			$('#fax1b_penagihan_bu').val('');
			
		}else if(id == '3'){
				
			$('.Penagiahan_2').removeClass("textboxenable");
			$('.Penagiahan_2').attr('disabled','disabled');
			$('.Penagiahan_2').addClass("textboxdisable");
			
			var alamat = $('#almtKantor1_bu').val();
			var kecamatan = $('#KecamatanKantor1_bu').val();
			var kota = $('#kota1_bu').val();
			var negara = $('#negara1_bu').val();
			var kPost = $('#kPos1_bu').val();
			var tlp1a = $('#tlp_kntr1a_bu').val();
			var tlp1b = $('#tlp_kntr1b_bu').val();
			var tlp2a = $('#tlp_kntr2a_bu').val();
			var tlp2b = $('#tlp_kntr2b_bu').val();
			var fax1a = $('#fax1a_bu').val();
			var fax1b = $('#fax1b_bu').val();

			$('#almtPenagihan_bu').val(alamat);
			$('#kecamatanPenagihan_bu').val(kecamatan);
			$('#kPos_penagihan_bu').val(kPost);
			$('#kota_penagihan_bu').val(kota);
			$('#negara_penagihan_bu').val(negara);
			$('#tlp1a_penagihan_bu').val(tlp1a);
			$('#tlp1b_penagihan_bu').val(tlp1b);
			$('#tlp2a_penagihan_bu').val(tlp2a);
			$('#tlp2b_penagihan_bu').val(tlp2b);
			$('#fax1a_penagihan_bu').val(fax1a);
			$('#fax1b_penagihan_bu').val(fax1b);
		}
	};
	
	//Klasifikasi bid industri No 4
	function bidIndustri1(id){
		if(id == 8){
			$('#lainIndustri').removeClass("textboxdisable");
			$('#lainIndustri').removeAttr('disabled');
			$('#lainIndustri').addClass("textboxenable");
		}else{
			$('#lainIndustri').val('');
			$('#lainIndustri').removeClass("textboxenable");
			$('#lainIndustri').attr('disabled','disabled');
			$('#lainIndustri').addClass("textboxdisable");
		}
	}
	function tujuanBeli(id){
		if(id == 6){
			$('#lainTujuan').removeClass("textboxdisable");
			$('#lainTujuan').removeAttr('disabled');
			$('#lainTujuan').addClass("textboxenable");
		}else{
			$('#lainTujuan').val('');
			$('#lainTujuan').removeClass("textboxenable");
			$('#lainTujuan').attr('disabled','disabled');
			$('#lainTujuan').addClass("textboxdisable");
		}
	}
	function pendanaan(id){
		if(id == 5){
			$('#lainPendanaan').removeClass("textboxdisable");
			$('#lainPendanaan').removeAttr('disabled');
			$('#lainPendanaan').addClass("textboxenable");
		}else{
			$('#lainPendanaan').val('');
			$('#lainPendanaan').removeClass("textboxenable");
			$('#lainPendanaan').attr('disabled','disabled');
			$('#lainPendanaan').addClass("textboxdisable");
		}
	}
	
</script>
		
