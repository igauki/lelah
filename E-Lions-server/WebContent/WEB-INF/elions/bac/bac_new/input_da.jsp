<%@ include file="/include/page/header_jquery.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<table class="entry">
 <tr > 
    <td width="67%" rowspan="5" align="left" valign="top"> 
      <form name="frmParam" method="post" >

        <table border="0" width="100%" cellspacing="0" cellpadding="0" class="entry">
          <tr > 
            <th ><b><font face="Verdana" size="1" color="#996600">1. </font></b></th>
            <th ><b><font color="#996600" size="1" face="Verdana">Tanggal SPAJ</font></b></th>
            <th colspan="5"> <spring:bind path="cmd.pemegang.mspo_spaj_date"> 
              <script>inputDate('${status.expression}', '${status.value}', false);</script>
             </spring:bind><spring:bind path="cmd.pemegang.mspo_ao"><font color="#CC3300">*</font> 
              </spring:bind> <spring:bind path="cmd.pemegang.mspo_spaj_date"><c:if test="${ not empty status.errorMessage}"> 
              </c:if> </spring:bind>
			</th>
          </tr>
          <tr > 
            <th width="21" ><b> <font face="Verdana" size="1" color="#996600">2</font><font face="Verdana" size="1" color="#996600">. 
              </font></b></th>
            <th width="216" ><b> <font face="Verdana" size="1" color="#996600">Kode 
              Regional</font></b></th>
            <th width="96"> <spring:bind path="cmd.agen.kode_regional"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="10" style='background-color :#D4D4D4'readOnly>
              </spring:bind> </th>
            <th width="165" ><b> <font face="Verdana" size="1" color="#996600">Nama 
              Regional</font></b></th>
            <th colspan="3" ><spring:bind path="cmd.agen.lsrg_nama"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="50" style='background-color :#D4D4D4'readOnly>
              </spring:bind></th>
          </tr>
          <tr > 
            <th width="21">&nbsp;</th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600"><c:if test="${cmd.datausulan.lsbs_id eq 120 }"> 
											Kode BAC</c:if>
											<c:if test="${cmd.datausulan.lsbs_id ne 120 }"> 
											Kode Penutup</c:if> </font><font face="Verdana" size="1"> </font></b>
                <a href="#" onclick="var msag_id=document.frmParam.elements['agen.msag_id'].value;popWin('${path}/bac/multi.htm?window=cek_struktur&msag_id='+msag_id, 350, 450);">
                    (cek struktur agen)
                </a>
              </th>
            <th width="96"><spring:bind path="cmd.agen.msag_id"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="10" onchange="kotak()" maxlength="6" 
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
              <font color="#CC3300">*</font> </spring:bind>
                <!--todo-->
              </th>
            <th width="165"><font size="1" face="Verdana"><b><font face="Verdana" size="1" color="#996600"><c:if test="${cmd.datausulan.lsbs_id eq 120 }"> 
											Nama BAC</c:if>
											<c:if test="${cmd.datausulan.lsbs_id ne 120 }"> 
											Nama Penutup</c:if> </font></b> </font>
              </th>
            <th colspan="3"> <spring:bind path="cmd.agen.mcl_first"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="50" maxlength="100" 
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
              </spring:bind> </th>
          </tr>
          <tr > 
            <th width="21">&nbsp;</th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">Kode Leader
              Penutup</font><font face="Verdana" size="1"> </font></b>
            <a href="#" onclick="var msag_id=document.frmParam.elements['agen.msag_id'].value;popWin('${path}/bac/multi.htm?window=cek_struktur&msag_id='+msag_id, 350, 450);">
                    (cek struktur agen)
                </a>
            </th>
            <th width="96"><spring:bind path="cmd.pemegang.mspo_leader"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="10" onchange="kotak2()" maxlength="6" 
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
              <font color="#CC3300">*</font> </spring:bind></th>
            <th colspan="3"> <spring:bind path="cmd.pemegang.nama_leader"> 
              <input type="hidden" name="${status.expression}"  
						value="${status.value }"  size="50" maxlength="100" 
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
              </spring:bind> </th>
            <th colspan="1">&nbsp;</th>
          </tr>
          <tr > 
            <th width="21"><b><font face="Verdana" size="1" color="#996600">3.</font></b></th>
            <th width="216"><b><font size="1" face="Verdana" color="#996600">AO 
              </font></b></th>
            <th width="96"> <input type="checkbox" name="mspo_ref_bii1" class="noBorder" 
						value="${cmd.pemegang.mspo_ref_bii}"  size="30" onClick="ao_onClick();" 
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> 
              <spring:bind path="cmd.pemegang.mspo_ref_bii"> 
              <input type="hidden" name="${status.expression}"
						value="${cmd.pemegang.mspo_ref_bii}"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind> </th>
            <th width="165"><b><font size="1" face="Verdana" color="#996600">Kode 
              AO </font></b></th>
            <th width="160"> <spring:bind path="cmd.pemegang.mspo_ao"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="10" onchange="kotak1()" maxlength="6" 
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
              <font color="#CC3300">*</font>  </spring:bind> </th>
            <th width="67"><b><font size="1" face="Verdana" color="#996600">Nama 
              AO </font></b></th>
            <th width="261"> <spring:bind path="cmd.pemegang.nama_ao"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30"  maxlength="6" style='background-color :#D4D4D4' readOnly   >
               </spring:bind> </th>
          </tr>
          <tr > 
            <th width="21"><b><font face="Verdana" size="1" color="#996600">4. 
              </font></b></th>
            <th width="216"><b><font size="1" face="Verdana" color="#996600">Pribadi</font></b> 
            </th>
            <th width="96"> <input type="checkbox" name="mspo_pribadi1" class="noBorder" 
						value="${cmd.pemegang.mspo_pribadi}"  size="30" onClick="pribadi_onClick();" 
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> 
              <spring:bind path="cmd.pemegang.mspo_pribadi"> 
              <input type="hidden" name="${status.expression}"
						value="${cmd.pemegang.mspo_pribadi}"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind> </th>
            <th width="165" colspan="4"> 
				<span class="info">* Untuk tutupan PRIBADI</span>
              <!--<b><font size="1" face="Verdana" color="#996600">Follow 
              Up</font></b> 
              <select name="pemegang.mspo_follow_up" >
                <c:forEach var="follow" items="${select_followup}"> <option 
							<c:if test="${cmd.pemegang.mspo_follow_up eq follow.ID}"> SELECTED </c:if>
							value="${follow.ID}">${follow.FOLLOWUP}</option> 
                </c:forEach> 
              </select>-->
            </th>
          </tr>
          <tr > 
            <th width="21"><b><font face="Verdana" size="1" color="#996600">5. 
              </font></b></th>
            <th width="216"><b><font size="1" face="Verdana" color="#996600">Kode 
              Regional Penagihan</font></b></th>
            <th width="96"> <input type="text" name="kode_regional" value="${cmd.addressbilling.region}"  size="10" style='background-color :#D4D4D4'readOnly> 
            </th>
            <th width="165"><b><font size="1" face="Verdana" color="#996600">Nama 
              Regional Penagihan</font></b></th>
            <th colspan="3"> <select name="addressbilling.region" 
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <c:forEach var="regional" items="${select_regional}"> <option 
							<c:if test="${cmd.addressbilling.region eq regional.key}"> SELECTED </c:if>
							value="${regional.key}">${regional.value}</option> 
                </c:forEach> </select> </th>
          </tr>
          
		<!-- MANTA -->
		<tr> 
			<th width="21"><b><font face="Verdana" size="1" color="#996600">6. </font></b></th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">Broker</font></b></th>
            <th width="96">
				<input type="checkbox" name="flagbroker1" class="noBorder" value="${cmd.broker.flagbroker}"  size="30" onClick="broker_onClick();" 
				<c:if test="${ not empty status.errorMessage}">
					style='background-color :#FFE1FD'
				</c:if>> 
				<spring:bind path="cmd.broker.flagbroker">
					<input type="hidden" name="${status.expression}" value="${cmd.broker.flagbroker}"  size="30" style='background-color :#D4D4D4'readOnly>
				</spring:bind>
			</th>
			<th colspan="4">&nbsp;</th>
		</tr>
		<tr>
			<th></th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">Kode Broker</font><font face="Verdana" size="1"> </font></b></th>
            <th width="96">
            	<spring:bind path="cmd.broker.lsb_id"> 
					<input type="text" name="${status.expression}"
						value="${status.value }"  size="10" onchange="brok()" maxlength="4" 
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
				</spring:bind>
			</th>	
            <th width="165"><font size="1" face="Verdana"><b><font face="Verdana" size="1" color="#996600">Nama Broker</font></b></font></th>
            <th colspan="3">
            	<spring:bind path="cmd.broker.lsb_nama"> 
					<input type="text" name="${status.expression}"
						value="${status.value }"  size="50" maxlength="100" style='background-color :#D4D4D4' readOnly>
            	</spring:bind>
            </th>
		</tr>
		<tr>          
			<th></th>
			<th width="216"><b><font face="Verdana" size="1" color="#996600">Cari Bank</font><font face="Verdana" size="1"> </font></b></th>
            <th height="20" colspan="1">
            	<input type="text" name="caribank2" onkeypress="if(event.keyCode==13){ document.frmParam.btncari2.click(); return false;}"> 
				<input type="button" name="btncari2" value="Cari" onclick="ajaxSelectWithParamMuamalat(document.frmParam.caribank2.value,'select_bank2','bank2','broker.lbn_id','', 'BANK_ID', 'BANK_NAMA', '','Silahkan pilih BANK','32',${cmd.datausulan.lsbs_id},${cmd.datausulan.lsdbs_number});">  
            </th>
          	<th height="20"><b><font face="Verdana" size="1" color="#996633">Bank&nbsp; 
				</font><font face="Verdana" size="1" color="#996600"> </font></b>
            </th>
            <th height="20" >
            	<div id="bank2"> 
					<select name="broker.lbn_id"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
	                	<option value="${cmd.broker.lbn_id}">${cmd.broker.lbn_nama}</option>
                	</select>
                	<font color="#CC3300">*</font>
                </div>
            </th>
            <th colspan="2">&nbsp;</th>
		</tr>
		<tr>          
			<th></th>
			<th width="216"><b><font face="Verdana" size="1" color="#996600">No Rekening Broker</font><font face="Verdana" size="1"> </font></b></th>
            <th colspan="5">
				<spring:bind path="cmd.broker.no_account"> 
					<input type="text" name="${status.expression}" value="${status.value }"  size="30" maxlength="12" 
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
               		<font color="#CC3300">*</font> 
				</spring:bind>
            </th>
		</tr>

          <tr > 
            <th colspan="7" class="subtitle"  height="20"> <p align="center"><b> 
                <font face="Verdana" size="1" color="#FFFFFF">INFORMASI KHUSUS 
                KARYAWAN AJ Sinarmas SEBAGAI PEMEGANG POLIS</font></b> </p></th>
          </tr>
          <tr > 
            <th width="21"><b><font face="Verdana" size="1" color="#996600">1. 
              </font></b></th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">NIK 
              AJ Sinarmas <br>
              ( Khusus Excellink Karyawan)&nbsp; </font></b></th>
            <th colspan="5"><spring:bind path="cmd.employee.nik"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" maxlength="10" 
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
               <font color="#CC3300">*</font> 
 			</spring:bind></th>
          </tr>
          <tr > 
            <th width="21">&nbsp;</th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">Nama 
              Karyawan&nbsp; </font></b></th>
            <th colspan="5"><spring:bind path="cmd.employee.nama"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind></th>
          </tr>
          <tr > 
            <th width="21">&nbsp;</th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">Cabang 
              </font></b></th>
            <th colspan="5"><spring:bind path="cmd.employee.cabang"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind></th>
          </tr>
          <tr > 
            <th width="21">&nbsp;</th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">Departemen</font></b></th>
            <th colspan="5"><spring:bind path="cmd.employee.dept"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind></th>
          </tr>
          <tr > 
            <th width="21"><b><font face="Verdana" size="1" color="#996600">2</font><font face="Verdana" size="1" color="#996600">. 
              </font></b></th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">Premi 
              Ke</font></b></th>
            <th width="96">
              <input type="text" name="cmd.employee.no_urut" value="${cmd.employee.no_urut}" style="background-color :#D4D4D4" readonly="readonly"/>	
              </th>
            <th width="165"><b><font face="Verdana" size="1" color="#996600">Potongan</font> 
              </b></th>
            <th width="160">
              <input type="text" style="background-color :#D4D4D4"  readOnly name='cmd.employee.potongan' size="20" value =<fmt:formatNumber type="number" value="${cmd.employee.potongan}"/> >
              </th>
            <th width="67"><b><font face="Verdana" size="1" color="#996600">Tanggal 
              Proses</font></b></th>
            <th width="261"> <spring:bind path="cmd.employee.tgl_proses"> 
              <script>('${status.expression}', '${status.value}', true);</script>
              </spring:bind> </th>
          </tr>
          <tr > 
            <th colspan="7" class="subtitle"  height="20"> <p align="center"><b> 
                <font face="Verdana" size="1" color="#FFFFFF">INFORMASI KHUSUS 
                PRODUK WORKSITE, PERUSAHAAN BELI PRODUK INDIVIDU & KARYAWAN AJS</font></b> </p></th>
          </tr>
          <tr > 
            <th width="21"><b><font face="Verdana" size="1" color="#996600">1. 
              </font></b></th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">Cari perusahaan</font></b></th>
<!--            <th colspan="5"> <input type="text" name="cariperusahaan1" onkeypress="if(event.keyCode==13){ document.frmParam.btnperusahaan1.click(); return false;}"> -->
<!--              <input type="button" name="btnperusahaan1" value="Cari"  onclick="ajaxSelectWithParam(document.frmParam.cariperusahaan1.value,'select_company','perush','pemegang.mspo_customer','', 'COMPANY_ID', 'COMPANY_NAMA', '');"> -->
<!--				<input type="checkbox" name="mste_flag_el1" class="noBorder" -->
<!--						value="${cmd.datausulan.mste_flag_el}"  size="30" onClick="karyawan_onClick();" -->
<!--				 <c:if test="${ not empty status.errorMessage}">-->
<!--						style='background-color :#FFE1FD'-->
<!--					</c:if>> -->
<!--              <spring:bind path="cmd.datausulan.mste_flag_el"> -->
<!--              <input type="hidden" name="${status.expression}"-->
<!--						value="${cmd.datausulan.mste_flag_el}"  size="30" style='background-color :#D4D4D4'readOnly>-->
<!--              <b><font face="Verdana" size="1" color="#996600">Karyawan AJ Sinarmas</font></b></spring:bind> -->
<!--            </th>-->
            <th colspan="5">
            <input type="text" name="cariperusahaan1" onkeypress="if(event.keyCode==13){ document.frmParam.btnperusahaan1.click(); return false;}">
              <input type="button" name="btnperusahaan1" value="Cari"  onclick="ajaxSelectWithParam(document.frmParam.cariperusahaan1.value,'select_company','perush','pemegang.mspo_customer','', 'COMPANY_ID', 'COMPANY_NAMA', '');"> 
				<spring:bind path="cmd.datausulan.mste_flag_el">
			           	<label for="flagkrywn1"><input id="flagkrywn1" name="${status.expression}" type="radio" class="noBorder" value="0" onclick="karyawan_onClick();">Non Karyawan</label>
			           	<label for="flagkrywn2"><input id="flagkrywn2" name="${status.expression}" type="radio" class="noBorder" value="1" onclick="karyawan_onClick();">Karyawan AJS MSIG</label>
			            <label for="flagkrywn3"><input id="flagkrywn3" name="${status.expression}" type="radio" class="noBorder" value="2" onclick="karyawan_onClick();">Keluarga Karyawan AJS MSIG</label>
	           			<label for="flagkrywn4"><input id="flagkrywn4" name="${status.expression}" type="radio" class="noBorder" value="3" onclick="karyawan_onClick();">Teman Karyawan AJS MSIG</label>
	           			<label for="flagkrywn5"><input id="flagkrywn5" name="${status.expression}" type="radio" class="noBorder" value="4" onclick="karyawan_onClick();">Agent Berlisensi AJS MSIG</label>
	           			<input type="hidden" name="flag_karyawan" id="flag_karyawan"
						value="${cmd.datausulan.mste_flag_el}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>    
            </th>
          </tr>
          <tr > 
            <th width="21"><b><font face="Verdana" size="1" color="#996600"> </font></b></th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">Nama 
              perusahaan </font></b></th>
            <th colspan="5"><div id="perush"> 
                <select name="pemegang.mspo_customer"
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                  <option value="${cmd.pemegang.mspo_customer}">${cmd.pemegang.mspo_customer_nama}</option>
                  <c:if test="${ not empty status.errorMessage}"><a href="#"><img src="${path}/include/image/false.gif" width="11" height="11"  border="0" onMouseOver="showtip(this,event,'${status.errorMessage }')" onMouseOut="hidetip()" onClick="showtip(this,event,'${status.errorMessage }')"></a> 
                  </c:if> 
                </select>
                <font color="#CC3300">*</font></div></th>
          </tr>
          <tr > 
            <th width="21"><b><font face="Verdana" size="1" color="#996600">2. 
              </font></b></th>
            <th width="216"><b><font face="Verdana" size="1" color="#996600">NIK 
              KARYAWAN </font></b></th>
            <th colspan="5"><spring:bind path="cmd.pemegang.nik"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" maxlength="10" 
					 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
               <font color="#CC3300">*</font> 
               </spring:bind></th>
          </tr>
  			<spring:bind path="cmd.datausulan.mspr_discount"> 
              <input type="hidden" name="${status.expression}" style='background-color :#D4D4D4'
						value="${status.value }"  size="30" maxlength="10" >
              <c:if test="${ not empty status.errorMessage}"><a href="#"><img src="${path}/include/image/false.gif" width="11" height="11"  border="0" onMouseOver="showtip(this,event,'${status.errorMessage }')" onMouseOut="hidetip()" onClick="showtip(this,event,'${status.errorMessage }')"></a> 
              </c:if> </spring:bind>
          <tr  > 
        
        <tr > 
            <th colspan="7" class="subtitle"  height="20"> <p align="center"><b> 
                <font face="Verdana" size="1" color="#FFFFFF">KHUSUS MNC</font></b> </p>
            </th>
        </tr>
        <tr > 
            <tr > 
	            <th ><b><font face="Verdana" size="1" color="#996600">1. </font></b></th>
	            <th colspan="6"><b><font face="Verdana" size="1" color="#996600" >SPECIAL OFFER MNC :
            </tr> 
            <tr > 
	            <th ><b><font face="Verdana" size="1" color="#996600"> </font></b></th>
	            <th colspan="6">
	            <b><font face="Verdana" size="1" color="#996600">
		            <spring:bind path="cmd.tertanggung.mste_flag_special_offer">
							<label for="nonso"> <input type="radio" class=noBorder
								name="${status.expression}" value="0" onClick="cpy('0');"
								<c:if test="${cmd.tertanggung.mste_flag_special_offer eq 0 or cmd.tertanggung.mste_flag_special_offer eq null}"> 
											checked</c:if>
								id="nonso">Non Special Offer </label>
							<br>
							<label for="cashbackso"> <input type="radio" class=noBorder
								name="${status.expression}" value="1" onClick="cpy('1');"
								<c:if test="${cmd.tertanggung.mste_flag_special_offer eq 1}"> 
											checked</c:if>
							id="cashbackso">Lucky Draw</label> 
							<label for="additionalunitso"> <input type="radio" class=noBorder
								name="${status.expression}" value="2" onClick="cpy('2');"
								<c:if test="${cmd.tertanggung.mste_flag_special_offer eq 2}"> 
											checked</c:if>
							id="additionalunitso">Additional Unit</label> 
					</spring:bind> 
	            </font></b>
	            </th>
          	</tr>
        </tr>
        
        <!-- PROGRAM HADIAH STABLE SAVE -->
        <tr>
        	<th colspan="7">
        		<div id="ps_hadiah" style="display: none;">
        		<table width="100%">
        			<tr > 
			            <th colspan="7" class="subtitle"  height="20"> <p align="center"><b> 
			                <font face="Verdana" size="1" color="#FFFFFF">KHUSUS STABLE SAVE BERHADIAH</font></b> </p>
			            </th>
			        </tr>
			        <tr> 
			            <th class="subtitle" align="center" colspan="7"> 
			             	<input name="btnadd1" type="button" id="btnadd1" value="ADD" onClick="addRowDOM1('tableHadiah')"> &nbsp;&nbsp;&nbsp;&nbsp; 
			             	<input name="btn_cancel1" type="button" id="btn_cancel1" value="DELETE" onClick="cancel1()">			
			            	<input type="hidden" name="jmlhadiah" id="jmlhadiah"  value="${cmd.pemegang.unit}" >
			            	<input type="hidden" name="lh_harga" id="lh_harga"  value="${lh_harga }" >
			            	<spring:bind path="cmd.pemegang.flag_hadiah"> 
								<input type="hidden" name="${status.expression}" value="${status.value }"  size="2" >
						 	</spring:bind>
			            </th>
			        </tr>
			        <tr>
        				<th colspan="7">
        					<!-- <input type="checkbox" name="hadiah_standart" id="hadiah_standart" style="border: none;"> <em> *ceklist untuk memilih hadiah standart</em> -->
        					<spring:bind path="cmd.pemegang.flag_standard">
								<input type="checkbox" class="noborder" value="1" onclick="cek_hadiah(this.checked);"
			            			<c:if test="${status.value eq 1}">checked="checked"</c:if>> <em> *ceklist untuk memilih hadiah standart</em>
								<input type="hidden" id="hadiah_standard" name="${status.expression}" value="${status.value}">
			            	</spring:bind>
			            	<div id="hadiah_2" style="display: block;">
        					<table ID="tableHadiah" width="100%" border="0" cellspacing="1" cellpadding="1" class="entry">
				       			<tr > 
				         			<th class="subtitle2" style="text-align: center;"><font size="1"><b><font face="Verdana" color="#FFFFFF">No</font></b></font></th>
				       				<th class="subtitle2" style="text-align: center;"><font size="1"><b><font face="Verdana" color="#FFFFFF">Hadiah</font></b></font><font color="#CC3300">*</font></th>
				         			<th class="subtitle2" style="text-align: center;"> <font size="1"><b><font face="Verdana" color="#FFFFFF">Unit</font><font color="#CC3300">*</font></b></font></th>
									<th class="subtitle2" style="text-align: center;">Cek</th>
								</tr>
				    			<c:forEach items="${cmd.pemegang.daftar_hadiah}" var="hadiah" varStatus="status">
								<tr> 
					  				<th> ${status.count}</th>
					        		<th>
										<select name="hadiah.mh_no${status.index +1}">
											<c:forEach var="hdh" items="${select_hadiah}">
												<option 
													<c:if test="${hadiah.lh_id eq hdh.LH_ID}"> SELECTED </c:if>
													value="${hdh.LH_ID}~${hdh.LH_HARGA}">${hdh.LH_NAMA} (<fmt:formatNumber>${hdh.LH_HARGA}</fmt:formatNumber>)</option>
											</c:forEach>
										</select>
					   				</th>
									<th> 
										<input type="text" name="hadiah.mh_quantity${status.index +1}" value ="${hadiah.mh_quantity}" size="3" maxlength="3">
					        		</th>
					  				<th><input type=checkbox name="cek${status.index +1}" id= "ck${status.index +1}" class="noBorder" ></th>
					
								</tr>
								</c:forEach>		 
				  			</table>
				  			</div>
        				</th>
        			</tr>
        		</table>
        		</div>
  			</th>
        </tr>
        <!-- END PROGRAM HADIAH -->
        
        <tr > 
            <th colspan="7" class="subtitle"  height="20"> <p align="center"><b> 
                <font face="Verdana" size="1" color="#FFFFFF">PERNYATAAN DAN PERSETUJUAN</font></b> </p></th>
          </tr>
          <tr > 
          <tr > 
            <th ><b><font face="Verdana" size="1" color="#996600">1. </font></b></th>
            <th colspan="6"><b><font face="Verdana" size="1" color="#996600" >Menyetujui bahwa Polis akan diterbitkan dalam bentuk : <font color="#CC3300">*</font></font></b></th>
            
          </tr> 
          <tr > 
             <th ><b><font face="Verdana" size="1" color="#996600"> </font></b></th>
            <th colspan="6"><b><font face="Verdana" size="1" color="#996600">
            <spring:bind path="cmd.pemegang.mspo_jenis_terbit">
					<label for="hardcopy"> <input type="radio" class=noBorder
						name="${status.expression}" value="0" onClick="cpy('0');"
						<c:if test="${cmd.pemegang.mspo_jenis_terbit eq 0 or cmd.pemegang.mspo_jenis_terbit eq null}"> 
									checked</c:if>
						id="hardcopy">Hard Copy dengan membayar biaya polis sesuai dengan jenis produk yang diambil. </label>
					<br>
					<label for="softcopy"> <input type="radio" class=noBorder
						name="${status.expression}" value="1" onClick="cpy('1');"
						<c:if test="${cmd.pemegang.mspo_jenis_terbit eq 1}"> 
									checked</c:if>
						id="softcopy">Softcopy ke alamat e-mail.</label> 
				</spring:bind> 
            </font></b></th>
          </tr>  
          <!-- TAMBANG EMAS -->
          <tr > 
            <th colspan="7" class="subtitle"  height="20"> <p align="center"><b> 
                <font face="Verdana" size="1" color="#FFFFFF">REFERENSI</font></b> </p></th>
          </tr>
          <tr > 
            <th ><b><font face="Verdana" size="1" color="#996600">1.</font></b></th>
            <th><b><font face="Verdana" size="1" color="#996600" >ID Referral</font></b></th>
            <th colspan="5"> <spring:bind path="cmd.agen.id_ref"> 
              <input type="text" name="${status.expression}" value="${status.value }" size="12" onchange="getReferal(this.value);">
              </spring:bind>(isi dengan ID Referral atau Kode Konfirmasi)<em></em> </th>
          </tr>
          <tr > 
            <th ><b><font face="Verdana" size="1" color="#996600">2.</font></b></th>
            <th><b><font face="Verdana" size="1" color="#996600" >Nama Referral</font></b></th>
            <th colspan="5"> <spring:bind path="cmd.agen.name_ref"> 
              <input type="text" name="${status.expression}" value="${status.value }" style='background-color :#D4D4D4' readonly="readonly" size="50">
              </spring:bind> 
              <spring:bind path="cmd.agen.jenis_ref"> 
              <input type="hidden" name="${status.expression}" value="${status.value }" style='background-color :#D4D4D4' readonly="readonly" size="50">
              </spring:bind>
              <spring:bind path="cmd.pemegang.mcl_first"> 
              <input type="hidden" name="${status.expression}" value="${status.value }" style='background-color :#D4D4D4' readonly="readonly" size="50">
              </spring:bind>
              <spring:bind path="cmd.pemegang.mspe_date_birth"> 
              <input type="hidden" name="${status.expression}" value="${status.value }" style='background-color :#D4D4D4' readonly="readonly" size="50">
              </spring:bind>
              <spring:bind path="cmd.tertanggung.mcl_first"> 
              <input type="hidden" name="${status.expression}" value="${status.value }" style='background-color :#D4D4D4' readonly="readonly" size="50">
              </spring:bind>
              <spring:bind path="cmd.tertanggung.mspe_date_birth"> 
              <input type="hidden" name="${status.expression}" value="${status.value }" style='background-color :#D4D4D4' readonly="readonly" size="50">
              </spring:bind>
              <!-- Pesan Referensi -->
			  <spring:bind path="cmd.agen.pesan_ref"> 
			  <input type="hidden" name="${status.expression}" value="${status.value }"  size="20" >
			  </spring:bind>
			  <!-- End Pesan Referensi -->
            </th>
          </tr>
          <!-- END TAMBAHAN -->  
 			
            <th colspan="7" class="subtitle">&nbsp;</th>
          </tr>
		            <tr> 
            <td colspan="9"> 
			 <p align="left"><b> <font face="Verdana" size="2" color="#CC3300">Note : * Wajib diisi</font></b></p>
			</td>
          </tr>
          <tr > 
            <td colspan="7"> <input type="hidden" name="hal" value="${halaman}"> 
              <spring:bind path="cmd.pemegang.indeks_halaman"> 
              <input type="hidden" name="${status.expression}"
						value="${halaman-1}"  size="30" >
              </spring:bind> </td>
          </tr>
        </table>
 

      </form>
    </td>

  </tr>
</table>
