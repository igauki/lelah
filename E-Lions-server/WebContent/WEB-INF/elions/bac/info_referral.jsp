<%@ include file="/include/page/taglibs.jsp"%>
<script>

	function submitSpaj(){
		if(confirm("Approved Spaj ?"))
		{
			parent.window.document.getElementById('submitSpajController').click();
		}
	}
</script>

<table class="entry2">
<tr>
								<td valign="top">
					
									<table>
										<tr>
											<td>
												<table class="entry2">
													<tr>
														<th colspan="2">INFORMASI AGEN PENUTUP</th>
													</tr>
													<tr>
														<th width="150" align="center">Level</th>
														<td width="250" >
															<input type="text" name="4" value="${cmd.referral.level_id}" size="3" maxlength="3" tabindex="7" style='background-color :#D4D4D4' readOnly>
														</td>
													</tr>
													<tr>
														<th>Agen Penutup</th>
														<td>
															<input type="text" name="no_rek" value="${cmd.referral.nama_reff}" size="36" maxlength="10" tabindex="7" style='background-color :#D4D4D4' readOnly>
														</td>
													<tr>
														<th>No A/C</th>
														<td>
															<input type="text" name="no_rek" value="${cmd.referral.no_rek}" size="36" maxlength="10" tabindex="7" style='background-color :#D4D4D4' readOnly>
														</td>
													</tr>
													<tr>
														<th>Cabang A/C</th>
														<td>
															<input type="text" name="cab_rek" value="${cmd.referral.cab_rek}" size="36" maxlength="10" tabindex="7" style='background-color :#D4D4D4' readOnly>
														</td>
													</tr>
													<tr>
														<th>Atas Nama</th>
														<td>
																<input type="text" name="atas_nama" value="${cmd.referral.atas_nama}" size="36" maxlength="10" tabindex="7" style='background-color :#D4D4D4' readOnly>
														</td>
													</tr>
													<tr>
														<th>Aktif</th>
														<td>
															<input type="hidden" name="flag_aktif" value="${cmd.referral.flag_aktif}" size="36" maxlength="10" tabindex="7" style='background-color :#D4D4D4' readOnly>
															<input type="text" name="aktif" value="${cmd.referral.aktif}" size="36" maxlength="10" tabindex="7" style='background-color :#D4D4D4' readOnly>
														</td>
													</tr>
													<tr>
														<th>NPK</th>
														<td>
															<input type="text" name="npk" value="${cmd.referral.npk}" size="36" maxlength="10" tabindex="7" style='background-color :#D4D4D4' readOnly>
															<input type="hidden" name="lcb_no" value="${cmd.referral.lcb_no}" size="36" maxlength="10" tabindex="7" style='background-color :#D4D4D4' readOnly>
														</td>
													</tr>
												</table>
											</td>
											<td>
												<table class="entry2">
													<tr>
														<th colspan="2">INFORMASI REFERRAL (Khusus Bank Sinarmas)</th>
													</tr>
													<tr>
														<th width="150" align="center">Level</th>
														<td width="250" >
															<input type="text" name="4" value="${cmd.referral.level_id2}" size="3" maxlength="3" tabindex="7" style='background-color :#D4D4D4' readOnly>
														</td>
													</tr>
													<tr>
														<th>Referral</th>
														<td>
															<input type="text" name="nama_reff2" value="${cmd.referral.nama_reff2}" size="36" maxlength="10" tabindex="7" style='background-color :#D4D4D4' readOnly>
														</td>
													</tr>
													<tr>
														<th>No A/C</th>
														<td>
															<input type="text" name="no_rek2" value="${cmd.referral.no_rek2}" size="36" maxlength="10" tabindex="7" style='background-color :#D4D4D4' readOnly>
														</td>
													</tr>
													<tr>
														<th>Cabang A/C</th>
														<td>
															<input type="text" name="cab_rek2" value="${cmd.referral.cab_rek2}" size="36" maxlength="10" tabindex="7" style='background-color :#D4D4D4' readOnly>
														</td>
													</tr>
													<tr>
														<th>Atas Nama</th>
														<td>
															<input type="text" name="atas_nama2" value="${cmd.referral.atas_nama2}" size="36" maxlength="10" tabindex="7" style='background-color :#D4D4D4' readOnly>
														</td>
													</tr>
													<tr>
														<th>Aktif</th>
														<td>
															<input type="hidden" name="flag_aktif2" value="${cmd.referral.flag_aktif2}" size="36" maxlength="10" tabindex="7" style='background-color :#D4D4D4' readOnly>
															<input type="text" name="aktif2" value="${cmd.referral.aktif2}" size="36" maxlength="10" tabindex="7" style='background-color :#D4D4D4' readOnly>
														</td>
													</tr>
													<tr>
														<th>NPK</th>
														<td>
															<input type="text" name="npk2" value="${cmd.referral.npk2}" size="36" maxlength="10" tabindex="7" style='background-color :#D4D4D4' readOnly>
															<input type="hidden" name="lcb_no2" value="${cmd.referral.lcb_no2}" size="36" maxlength="10" tabindex="7" style='background-color :#D4D4D4' readOnly>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
								</tr>
							</table>
							
							  <table class="entry2">
				      <tr> 
				        <th nowrap="nowrap">Rekening Billing<br><span style="color: red;">(Billing Account)</span></th>
				        <td colspan="3">
							<input class="readOnly" type="text" readonly="readonly" value="${cmd.virtual_account}" style="width: 336px;">
				        </td>        
				      </tr>
				      <tr> 
				        <th nowrap="nowrap">Bank <span style="color: red;">*</span></th>
				        <td colspan="3">
				        	<input type="hidden" name="spaj" value="${param.spaj}">
							<select name="lsbp_id" style="width: 338px;" tabindex="1" onchange="ajaxPjngRek(this.value, 1,'mrc_no_ac_split')" disabled="disabled">
								<option value=""></option>
								<c:forEach var="s" items="${cmd.bang}">
									<option value="${s.LSBP_ID}"
										<c:if test="${s.LSBP_ID eq cmd.rekening.lsbp_id}">selected</c:if>>${s.LSBP_NAMA}</option>
								</c:forEach>
							</select>        
				        </td>        
				      </tr>
				      <tr>
				      	<th nowrap="nowrap">No. Rekening <span style="color: red;">*</span></th>
				      	<td colspan="3">
				      		<%--NO REKENING DI SPLIT --%>
				      		<input type="text" name="mrc_no_ac_split[0]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(0, 1);"  value="${cmd.rekening.mrc_no_ac_split[0]}" tabindex="2" readOnly>
							<input type="text" name="mrc_no_ac_split[1]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(1, 1);"  value="${cmd.rekening.mrc_no_ac_split[1]}"  tabindex="3" readOnly>
							<input type="text" name="mrc_no_ac_split[2]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(2, 1);"  value="${cmd.rekening.mrc_no_ac_split[2]}"  tabindex="4" readOnly>
							<input type="text" name="mrc_no_ac_split[3]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(3, 1);"  value="${cmd.rekening.mrc_no_ac_split[3]}"  tabindex="5" readOnly>
							<input type="text" name="mrc_no_ac_split[4]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(4, 1);"  value="${cmd.rekening.mrc_no_ac_split[4]}"  tabindex="6" readOnly>
							<input type="text" name="mrc_no_ac_split[5]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(5, 1);"  value="${cmd.rekening.mrc_no_ac_split[5]}"  tabindex="7" readOnly>
							<input type="text" name="mrc_no_ac_split[6]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(6, 1);"  value="${cmd.rekening.mrc_no_ac_split[6]}"  tabindex="8" readOnly>
							<input type="text" name="mrc_no_ac_split[7]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(7, 1);"  value="${cmd.rekening.mrc_no_ac_split[7]}"  tabindex="9" readOnly>
							<input type="text" name="mrc_no_ac_split[8]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(8, 1);"  value="${cmd.rekening.mrc_no_ac_split[8]}"  tabindex="10" readOnly>
							<input type="text" name="mrc_no_ac_split[9]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(9, 1);"  value="${cmd.rekening.mrc_no_ac_split[9]}"  tabindex="11" readOnly>
							<input type="text" name="mrc_no_ac_split[10]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(10, 1);"  value="${cmd.rekening.mrc_no_ac_split[10]}" tabindex="12" readOnly>
							<input type="text" name="mrc_no_ac_split[11]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(11, 1);"  value="${cmd.rekening.mrc_no_ac_split[11]}"  tabindex="13" readOnly>
							<input type="text" name="mrc_no_ac_split[12]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(12, 1);"  value="${cmd.rekening.mrc_no_ac_split[12]}"  tabindex="14" readOnly>
							<input type="text" name="mrc_no_ac_split[13]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(13, 1);"  value="${cmd.rekening.mrc_no_ac_split[13]}" tabindex="15" readOnly>
							<input type="text" name="mrc_no_ac_split[14]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(14, 1);"  value="${cmd.rekening.mrc_no_ac_split[14]}" tabindex="16" readOnly>
							<input type="text" name="mrc_no_ac_split[15]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(15, 1);"  value="${cmd.rekening.mrc_no_ac_split[15]}" tabindex="17" readOnly>
							<input type="text" name="mrc_no_ac_split[16]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(16, 1);"  value="${cmd.rekening.mrc_no_ac_split[16]}"  tabindex="18" readOnly>
							<input type="text" name="mrc_no_ac_split[17]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(17, 1);"  value="${cmd.rekening.mrc_no_ac_split[17]}"  tabindex="19" readOnly>
							<input type="text" name="mrc_no_ac_split[18]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(18, 1);"  value="${cmd.rekening.mrc_no_ac_split[18]}"  tabindex="20" readOnly>
							<input type="text" name="mrc_no_ac_split[19]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(19, 1);" value="${cmd.rekening.mrc_no_ac_split[19]}"  tabindex="21" readOnly>
							<input type="text" name="mrc_no_ac_split[20]" size="1"  maxlength="1" onfocus="select();"  value="${cmd.rekening.mrc_no_ac_split[20]}"  tabindex="21" readOnly>
				      		
				      		<input type="hidden" name="mrc_no_ac" value="${cmd.rekening.mrc_no_ac}" style="width: 282px;">
				      		<input type="hidden" name="mrc_no_ac_lama" value="${cmd.rekening.mrc_no_ac_lama}">
							    <select name="mrc_kurs"  tabindex="22" disabled="disabled">
				                <c:forEach var="kurs" items="${cmd.select_kurs}"> <option 
											<c:if test="${cmd.rekening.mrc_kurs eq kurs.ID}"> SELECTED </c:if>
											value="${kurs.ID}">${kurs.SYMBOL}</option> 
				                </c:forEach> 
				               	</select>
				      	</td>
				      </tr>
				      <tr>
				      	<th nowrap="nowrap">Atas Nama <span style="color: red;">*</span></th>
				      	<td colspan="3"><input type="text" name="mrc_nama" value="${cmd.rekening.mrc_nama}"  tabindex="23" style="width: 336px;" readOnly></td>
				      </tr>
				      <tr>
				      	<th nowrap="nowrap">Cabang</th>
				      	<td><input type="text" name="mrc_cabang" value="${cmd.rekening.mrc_cabang}" size="20"  tabindex="24" readOnly></td>
				      	<th nowrap="nowrap">Kota</th>
				      	<td><input type="text" name="mrc_kota" value="${cmd.rekening.mrc_kota}" size="19"  tabindex="25" readOnly></td>
				      </tr>
				      
				    </table>
							
							<table class="entry2">
							<tr height="20">
								<td colspan="2">
								</td>
							</tr>
							<tr>
							<td align="center">
							<input type="submit" name="submitSpaj" value="Approved" onclick="submitSpaj()" align="left" id="submitSpaj">
							</td>
							</tr>

							</table>
