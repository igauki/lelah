<%@ include file="/include/page/header.jsp"%><c:set var="path" value="${pageContext.request.contextPath}" />
<table border="0" width="100%" cellspacing="0" cellpadding="0" class="entry">
          <tr >
            <th colspan="2" rowspan="2" class="subtitle" valign="middle"><strong>B. DATA KEGIATAN DAN KESEHATAN PRIBADI</strong></th>
            <th colspan="2" rowspan="2" class="subtitle" valign="middle"><strong>Pemegang Polis</strong></th>
            <th colspan="2" rowspan="2" class="subtitle" valign="middle"><strong>Tertanggung Utama</strong></th>
            <th colspan="10" class="subtitle"><strong>Tertanggung Tambahan</strong></th>
          </tr>
          <tr >
            <th colspan="2" class="subtitle">I</th>
            <th colspan="2" class="subtitle">II</th>
            <th colspan="2" class="subtitle">III</th>
            <th colspan="2" class="subtitle">IV</th>
            <th colspan="2" class="subtitle">V</th>
          </tr>
          <tr > 
            <th ><b><font face="Verdana" size="1" color="#000">1. </font></b></th>
            <th ><b><font color="#000" size="1" face="Verdana">Apakah Anda melakukan atau mempunyai rencana mengikuti olah raga atau kegiatan yang berisiko tinggi (seperti balap mobil/motor, menyelam, panjat tebing, terbang layang, bela diri, tinju, berkuda, ski es, mendaki gunung, bungee jumping dan sebagainya)? Jika &quot;YA&quot;, jelaskan!</font></b></th>
            <th><spring:bind path="cmd.medQuest.msadd_hobby">
			           	<label for="flaghobby1"><input id="flaghobby1" name="${status.expression}" type="radio" class="noBorder" value="1" >Ya</label>
			           	<label for="flaghobby2"><input id="flaghobby2" name="${status.expression}" type="radio" class="noBorder" value="0" >Tidak</label>
	           			<input type="hidden" name="flaghobby" id="flaghobby"
						value="${cmd.medQuest.msadd_hobby}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>   
			</th>
            <th colspan="-2">
            </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadd_hobby">
			           	<label for="flaghobby1_tertanggung"><input id="flaghobby1_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="1" >Ya</label>
			           	<label for="flaghobby2_tertanggung"><input id="flaghobby2_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="0" >Tidak</label>
	           			<input type="hidden" name="flaghobby_tertanggung" id="flaghobby_tertanggung"
						value="${cmd.medQuest_tertanggung.msadd_hobby}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th></th>
			<th><spring:bind path="cmd.medQuest_tambah.msadd_hobby">
			           	<label for="flaghobby1_tambah"><input id="flaghobby1_tambah" name="${status.expression}" type="radio" class="noBorder" value="1" >Ya</label>
			           	<label for="flaghobby2_tambah"><input id="flaghobby2_tambah" name="${status.expression}" type="radio" class="noBorder" value="0" >Tidak</label>
	           			<input type="hidden" name="flaghobby_tambah" id="flaghobby_tambah"
						value="${cmd.medQuest_tambah.msadd_hobby}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th></th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadd_hobby">
			           	<label for="flaghobby1_tambah2"><input id="flaghobby1_tambah2" name="${status.expression}" type="radio" class="noBorder" value="1" >Ya</label>
			           	<label for="flaghobby2_tambah2"><input id="flaghobby2_tambah2" name="${status.expression}" type="radio" class="noBorder" value="0" >Tidak</label>
	           			<input type="hidden" name="flaghobby_tambah2" id="flaghobby_tambah2"
						value="${cmd.medQuest_tambah2.msadd_hobby}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th></th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadd_hobby">
			           	<label for="flaghobby1_tambah3"><input id="flaghobby1_tambah3" name="${status.expression}" type="radio" class="noBorder" value="1" >Ya</label>
			           	<label for="flaghobby2_tambah3"><input id="flaghobby2_tambah3" name="${status.expression}" type="radio" class="noBorder" value="0" >Tidak</label>
	           			<input type="hidden" name="flaghobby_tambah3" id="flaghobby_tambah3"
						value="${cmd.medQuest_tambah3.msadd_hobby}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th></th>
			<th><spring:bind path="cmd.medQuest_tambah4.msadd_hobby">
			           	<label for="flaghobby1_tambah4"><input id="flaghobby1_tambah4" name="${status.expression}" type="radio" class="noBorder" value="1" >Ya</label>
			           	<label for="flaghobby2_tambah4"><input id="flaghobby2_tambah4" name="${status.expression}" type="radio" class="noBorder" value="0" >Tidak</label>
	           			<input type="hidden" name="flaghobby_tambah4" id="flaghobby_tambah4"
						value="${cmd.medQuest_tambah4.msadd_hobby}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th></th>
			<th><spring:bind path="cmd.medQuest_tambah5.msadd_hobby">
			           	<label for="flaghobby1_tambah5"><input id="flaghobby1_tambah5" name="${status.expression}" type="radio" class="noBorder" value="1" >Ya</label>
			           	<label for="flaghobby2_tambah5"><input id="flaghobby2_tambah5" name="${status.expression}" type="radio" class="noBorder" value="0" >Tidak</label>
	           			<input type="hidden" name="flaghobby_tambah5" id="flaghobby_tambah5"
						value="${cmd.medQuest_tambah5.msadd_hobby}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th></th>
          </tr>
          <tr > 
            <th ><b><font color="#000" size="1" face="Verdana">2.</font></b></th>
            <th ><b><font color="#000" size="1" face="Verdana">Apakah Anda biasa melakukan perjalanan / sebagai penumpang pada penerbangan yang tidak terjadwal tetap(unregularschedule airline)? Jika &quot;YA&quot;, jelaskan!</font></b></th>
            <th><spring:bind path="cmd.medQuest.msadd_flight">
			           	<label for="flagflight1"><input id="flagflight1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest.msadd_desc_flight',1);">Ya</label>
			           	<label for="flagflight2"><input id="flagflight2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest.msadd_desc_flight',0);">Tidak</label>
	           			<input type="hidden" name="flagflight" id="flagflight"
						value="${cmd.medQuest.msadd_flight}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>   
			</th>
            <th colspan="-2"><spring:bind path="cmd.medQuest.msadd_desc_flight"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind>
            </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadd_flight">
			           	<label for="flagflight1_tertanggung"><input id="flagflight1_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tertanggung.msadd_desc_flight',1);">Ya</label>
			           	<label for="flagflight2_tertanggung"><input id="flagflight2_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tertanggung.msadd_desc_flight',0);">Tidak</label>
	           			<input type="hidden" name="flagflight_tertanggung" id="flagflight_tertanggung"
						value="${cmd.medQuest_tertanggung.msadd_flight}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadd_desc_flight"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah.msadd_flight">
			           	<label for="flagflight1_tambah"><input id="flagflight1_tambah" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah.msadd_desc_flight',1);">Ya</label>
			           	<label for="flagflight2_tambah"><input id="flagflight2_tambah" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah.msadd_desc_flight',0);">Tidak</label>
	           			<input type="hidden" name="flagflight_tambah" id="flagflight_tambah"
						value="${cmd.medQuest_tambah.msadd_flight}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah.msadd_desc_flight"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadd_flight">
			           	<label for="flagflight1_tambah2"><input id="flagflight1_tambah2" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah2.msadd_desc_flight',1);">Ya</label>
			           	<label for="flagflight2_tambah2"><input id="flagflight2_tambah2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah2.msadd_desc_flight',0);">Tidak</label>
	           			<input type="hidden" name="flagflight_tambah2" id="flagflight_tambah2"
						value="${cmd.medQuest_tambah2.msadd_flight}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadd_desc_flight"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadd_flight">
			           	<label for="flagflight1_tambah3"><input id="flagflight1_tambah3" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah3.msadd_desc_flight',1);">Ya</label>
			           	<label for="flagflight2_tambah3"><input id="flagflight2_tambah3" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah3.msadd_desc_flight',0);">Tidak</label>
	           			<input type="hidden" name="flagflight_tambah3" id="flagflight_tambah3"
						value="${cmd.medQuest_tambah3.msadd_flight}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadd_desc_flight"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
	        <th><spring:bind path="cmd.medQuest_tambah4.msadd_flight">
			           	<label for="flagflight1_tambah4"><input id="flagflight1_tambah4" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah4.msadd_desc_flight',1);">Ya</label>
			           	<label for="flagflight2_tambah4"><input id="flagflight2_tambah4" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah4.msadd_desc_flight',0);">Tidak</label>
	           			<input type="hidden" name="flagflight_tambah4" id="flagflight_tambah4"
						value="${cmd.medQuest_tambah4.msadd_flight}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah4.msadd_desc_flight"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
	       <th><spring:bind path="cmd.medQuest_tambah5.msadd_flight">
			           	<label for="flagflight1_tambah5"><input id="flagflight1_tambah5" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah5.msadd_desc_flight',1);">Ya</label>
			           	<label for="flagflight2_tambah5"><input id="flagflight2_tambah5" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah5.msadd_desc_flight',0);">Tidak</label>
	           			<input type="hidden" name="flagflight_tambah5" id="flagflight_tambah5"
						value="${cmd.medQuest_tambah5.msadd_flight}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah5.msadd_desc_flight"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
          </tr>
          <tr > 
            <th ><b><font face="Verdana" size="1" color="#000">3. </font></b></th>
            <th ><b><font color="#000" size="1" face="Verdana">Apakah Anda mempunyai kebiasaan-kebiasaan ini? Bila &quot;YA&quot;, jelaskan!<br>
              a. Merokok___batang/hari, selama____tahun
            </font></b></th>
            <th><spring:bind path="cmd.medQuest.msadd_smoke">
			           	<label for="flagsmoke1"><input id="flagsmoke1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest.nsadd_many_cig',1);">Ya</label>
			           	<label for="flagsmoke2"><input id="flagsmoke2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest.nsadd_many_cig',0);">Tidak</label>
	           			<input type="hidden" name="flagsmoke" id="flagsmoke"
						value="${cmd.medQuest.msadd_smoke}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>   
			</th>
            <th colspan="-2"><spring:bind path="cmd.medQuest.nsadd_many_cig"> 
	             	<input type="text" name="${status.expression}" value="${status.value }"  size="2" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>> Batang/Hari
	            </spring:bind>
            </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadd_smoke">
			           	<label for="flagsmoke1_tertanggung"><input id="flagsmoke1_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tertanggung.nsadd_many_cig',1);">Ya</label>
			           	<label for="flagsmoke2_tertanggung"><input id="flagsmoke2_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tertanggung.nsadd_many_cig',0);">Tidak</label>
	           			<input type="hidden" name="flagsmoke_tertanggung" id="flagsmoke_tertanggung"
						value="${cmd.medQuest_tertanggung.msadd_smoke}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.nsadd_many_cig"> 
	             	<input type="text" name="${status.expression}" value="${status.value }"  size="2" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>> Batang/Hari
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah.msadd_smoke">
			           	<label for="flagsmoke1_tambah"><input id="flagsmoke1_tambah" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah.nsadd_many_cig',1);">Ya</label>
			           	<label for="flagsmoke2_tambah"><input id="flagsmoke2_tambah" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah.nsadd_many_cig',0);">Tidak</label>
	           			<input type="hidden" name="flagsmoke_tambah" id="flagsmoke_tambah"
						value="${cmd.medQuest_tambah.msadd_smoke}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah.nsadd_many_cig"> 
	             	<input type="text" name="${status.expression}" value="${status.value }"  size="2" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>> Batang/Hari
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadd_smoke">
			           	<label for="flagsmoke1_tambah2"><input id="flagsmoke1_tambah2" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah2.nsadd_many_cig',1);">Ya</label>
			           	<label for="flagsmoke2_tambah2"><input id="flagsmoke2_tambah2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah2.nsadd_many_cig',0);">Tidak</label>
	           			<input type="hidden" name="flagsmoke_tambah2" id="flagsmoke_tambah2"
						value="${cmd.medQuest_tambah2.msadd_smoke}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah2.nsadd_many_cig"> 
	             	<input type="text" name="${status.expression}" value="${status.value }"  size="2" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>> Batang/Hari
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadd_smoke">
			           	<label for="flagsmoke1_tambah3"><input id="flagsmoke1_tambah3" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah3.nsadd_many_cig',1);">Ya</label>
			           	<label for="flagsmoke2_tambah3"><input id="flagsmoke2_tambah3" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah3.nsadd_many_cig',0);">Tidak</label>
	           			<input type="hidden" name="flagsmoke_tambah3" id="flagsmoke_tambah3"
						value="${cmd.medQuest_tambah3.msadd_smoke}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
	        <th><spring:bind path="cmd.medQuest_tambah3.nsadd_many_cig"> 
	             	<input type="text" name="${status.expression}" value="${status.value }"  size="2" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>> Batang/Hari
	            </spring:bind></th>
	        <th><spring:bind path="cmd.medQuest_tambah4.msadd_smoke">
			           	<label for="flagsmoke1_tambah4"><input id="flagsmoke1_tambah4" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah4.nsadd_many_cig',1);">Ya</label>
			           	<label for="flagsmoke2_tambah4"><input id="flagsmoke2_tambah4" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah4.nsadd_many_cig',0);">Tidak</label>
	           			<input type="hidden" name="flagsmoke_tambah4" id="flagsmoke_tambah4"
						value="${cmd.medQuest_tambah4.msadd_smoke}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
	        <th><spring:bind path="cmd.medQuest_tambah4.nsadd_many_cig"> 
	             	<input type="text" name="${status.expression}" value="${status.value }"  size="2" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>> Batang/Hari
	            </spring:bind></th>
	        <th><spring:bind path="cmd.medQuest_tambah5.msadd_smoke">
			           	<label for="flagsmoke1_tambah5"><input id="flagsmoke1_tambah5" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah5.nsadd_many_cig',1);">Ya</label>
			           	<label for="flagsmoke2_tambah5"><input id="flagsmoke2_tambah5" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah5.nsadd_many_cig',0);">Tidak</label>
	           			<input type="hidden" name="flagsmoke_tambah5" id="flagsmoke_tambah5"
						value="${cmd.medQuest_tambah5.msadd_smoke}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
	        <th><spring:bind path="cmd.medQuest_tambah5.nsadd_many_cig"> 
	             	<input type="text" name="${status.expression}" value="${status.value }"  size="2" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>> Batang/Hari
	            </spring:bind></th>
          </tr>
          <tr > 
            <th >&nbsp;</th>
            <th ><b><font color="#000" size="1" face="Verdana">b. Minum minuman beralkohol(sebutkan jenisnya, jumlah konsumsi per hari dan berapa lama sudah mengkonsumsinya)</font></b></th>
            <th><spring:bind path="cmd.medQuest.msadd_drink_beer">
			           	<label for="flagdrink1"><input id="flagdrink1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest.msadd_drink_beer_desc',1);">Ya</label>
			           	<label for="flagdrink2"><input id="flagdrink2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest.msadd_drink_beer_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagdrink" id="flagdrink"
						value="${cmd.medQuest.msadd_drink_beer}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  
			</th>
            <th colspan="-2"><spring:bind path="cmd.medQuest.msadd_drink_beer_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind>
            </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadd_drink_beer">
			           	<label for="flagdrink1_tertanggung"><input id="flagdrink1_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tertanggung.msadd_drink_beer_desc',1);">Ya</label>
			           	<label for="flagdrink2_tertanggung"><input id="flagdrink2_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tertanggung.msadd_drink_beer_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagdrink_tertanggung" id="flagdrink_tertanggung"
						value="${cmd.medQuest_tertanggung.msadd_drink_beer}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadd_drink_beer_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah.msadd_drink_beer">
			           	<label for="flagdrink1_tambah"><input id="flagdrink1_tambah" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah.msadd_drink_beer_desc',1);">Ya</label>
			           	<label for="flagdrink2_tambah"><input id="flagdrink2_tambah" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah.msadd_drink_beer_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagdrink_tambah" id="flagdrink_tambah"
						value="${cmd.medQuest_tambah.msadd_drink_beer}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah.msadd_drink_beer_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadd_drink_beer">
			           	<label for="flagdrink1_tambah2"><input id="flagdrink1_tambah2" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah2.msadd_drink_beer_desc',1);">Ya</label>
			           	<label for="flagdrink2_tambah2"><input id="flagdrink2_tambah2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah2.msadd_drink_beer_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagdrink_tambah2" id="flagdrink_tambah2"
						value="${cmd.medQuest_tambah2.msadd_drink_beer}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadd_drink_beer_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadd_drink_beer">
			           	<label for="flagdrink1_tambah3"><input id="flagdrink1_tambah3" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah3.msadd_drink_beer_desc',1);">Ya</label>
			           	<label for="flagdrink2_tambah3"><input id="flagdrink2_tambah3" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah3.msadd_drink_beer_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagdrink_tambah3" id="flagdrink_tambah3"
						value="${cmd.medQuest_tambah3.msadd_drink_beer}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadd_drink_beer_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
	        <th><spring:bind path="cmd.medQuest_tambah4.msadd_drink_beer">
			           	<label for="flagdrink1_tambah4"><input id="flagdrink1_tambah4" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah4.msadd_drink_beer_desc',1);">Ya</label>
			           	<label for="flagdrink2_tambah4"><input id="flagdrink2_tambah4" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah4.msadd_drink_beer_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagdrink_tambah4" id="flagdrink_tambah3"
						value="${cmd.medQuest_tambah4.msadd_drink_beer}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah4.msadd_drink_beer_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
	       <th><spring:bind path="cmd.medQuest_tambah5.msadd_drink_beer">
			           	<label for="flagdrink1_tambah5"><input id="flagdrink1_tambah5" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah5.msadd_drink_beer_desc',1);">Ya</label>
			           	<label for="flagdrink2_tambah5"><input id="flagdrink2_tambah5" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah5.msadd_drink_beer_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagdrink_tambah5" id="flagdrink_tambah5"
						value="${cmd.medQuest_tambah5.msadd_drink_beer}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah5.msadd_drink_beer_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
          </tr>
          <tr > 
            <th >&nbsp;</th>
            <th ><b><font color="#000" size="1" face="Verdana">c.Menggunakan narkotika/obat terlarang/obat penenang(sebutkan nama obatnya,jumlah konsumsi perhari dan berapa lama sudah mengkonsumsinya)</font></b></th>
            <th><spring:bind path="cmd.medQuest.msadd_drugs">
			           	<label for="flagdrugs1"><input id="flagdrugs1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest.msadd_reason_drugs',1);">Ya</label>
			           	<label for="flagdrugs2"><input id="flagdrugs2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest.msadd_reason_drugs',0);">Tidak</label>
	           			<input type="hidden" name="flagdrugs" id="flagdrugs"
						value="${cmd.medQuest.msadd_drugs}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> 
			</th>
            <th colspan="-2"><spring:bind path="cmd.medQuest.msadd_reason_drugs"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind>
            </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadd_drugs">
			           	<label for="flagdrugs1_tertanggung"><input id="flagdrugs1_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tertanggung.msadd_reason_drugs',1);">Ya</label>
			           	<label for="flagdrugs2_tertanggung"><input id="flagdrugs2_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tertanggung.msadd_reason_drugs',0);">Tidak</label>
	           			<input type="hidden" name="flagdrugs_tertanggung" id="flagdrugs_tertanggung"
						value="${cmd.medQuest_tertanggung.msadd_drugs}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadd_reason_drugs"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah.msadd_drugs">
			           	<label for="flagdrugs1_tambah"><input id="flagdrugs1_tambah" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah.msadd_reason_drugs',1);">Ya</label>
			           	<label for="flagdrugs2_tambah"><input id="flagdrugs2_tambah" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah.msadd_reason_drugs',0);">Tidak</label>
	           			<input type="hidden" name="flagdrugs_tambah" id="flagdrugs_tambah"
						value="${cmd.medQuest_tambah.msadd_drugs}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah.msadd_reason_drugs"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadd_drugs">
			           	<label for="flagdrugs1_tambah2"><input id="flagdrugs1_tambah2" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah2.msadd_reason_drugs',1);">Ya</label>
			           	<label for="flagdrugs2_tambah2"><input id="flagdrugs2_tambah2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah2.msadd_reason_drugs',0);">Tidak</label>
	           			<input type="hidden" name="flagdrugs_tambah2" id="flagdrugs_tambah2"
						value="${cmd.medQuest_tambah2.msadd_drugs}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadd_reason_drugs"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadd_drugs">
			           	<label for="flagdrugs1_tambah3"><input id="flagdrugs1_tambah3" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah3.msadd_reason_drugs',1);">Ya</label>
			           	<label for="flagdrugs2_tambah3"><input id="flagdrugs2_tambah3" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah3.msadd_reason_drugs',0);">Tidak</label>
	           			<input type="hidden" name="flagdrugs_tambah3" id="flagdrugs_tambah3"
						value="${cmd.medQuest_tambah3.msadd_drugs}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadd_reason_drugs"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
	       <th><spring:bind path="cmd.medQuest_tambah4.msadd_drugs">
			           	<label for="flagdrugs1_tambah4"><input id="flagdrugs1_tambah4" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah4.msadd_reason_drugs',1);">Ya</label>
			           	<label for="flagdrugs2_tambah4"><input id="flagdrugs2_tambah4" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah4.msadd_reason_drugs',0);">Tidak</label>
	           			<input type="hidden" name="flagdrugs_tambah4" id="flagdrugs_tambah4"
						value="${cmd.medQuest_tambah4.msadd_drugs}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah4.msadd_reason_drugs"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
	        <th><spring:bind path="cmd.medQuest_tambah5.msadd_drugs">
			           	<label for="flagdrugs1_tambah5"><input id="flagdrugs1_tambah5" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah5.msadd_reason_drugs',1);">Ya</label>
			           	<label for="flagdrugs2_tambah5"><input id="flagdrugs2_tambah5" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah5.msadd_reason_drugs',0);">Tidak</label>
	           			<input type="hidden" name="flagdrugs_tambah5" id="flagdrugs_tambah5"
						value="${cmd.medQuest_tambah5.msadd_drugs}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah5.msadd_reason_drugs"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
          </tr>
          <tr > 
            <th ><b><font face="Verdana" size="1" color="#000">4.</font></b></th>
            <th ><b><font color="#000" size="1" face="Verdana">Apakah Anda sudah mempunyai atau sedang mengajukan atau pernah mengajukan Polis Asuransi Jiwa / Kecelakaan / Kesehatan baik di PT. Asuransi Jiwa Sinarmas MSIG maupun di perusahaan lainnya? Jika &quot;YA&quot;, sebutkan nama perusahaan asuransi, jenis asuransi, uang pertanggungan, tarif premi yang dikenakan(standar/ekstra Premi/ditunda/ditolak dan alasannya)!</font></b></th>
            <th><spring:bind path="cmd.medQuest.msadd_life_ins">
			           	<label for="flagins1"><input id="flagins1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest.msadd_life_ins_desc',1);">Ya</label>
			           	<label for="flagins2"><input id="flagins2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest.msadd_life_ins_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagins" id="flagins"
						value="${cmd.medQuest.msadd_life_ins}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> 
			</th>
            <th colspan="-2"><spring:bind path="cmd.medQuest.msadd_life_ins_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind>
            </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadd_life_ins">
			           	<label for="flagins1_tertanggung"><input id="flagins1_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tertanggung.msadd_life_ins_desc',1);">Ya</label>
			           	<label for="flagins2_tertanggung"><input id="flagins2_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tertanggung.msadd_life_ins_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagins_tertanggung" id="flagins_tertanggung"
						value="${cmd.medQuest_tertanggung.msadd_life_ins}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadd_life_ins_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah.msadd_life_ins">
			           	<label for="flagins1_tambah"><input id="flagins1_tambah" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah.msadd_life_ins_desc',1);">Ya</label>
			           	<label for="flagins2_tambah"><input id="flagins2_tambah" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah.msadd_life_ins_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagins_tambah" id="flagins_tambah"
						value="${cmd.medQuest_tambah.msadd_life_ins}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah.msadd_life_ins_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadd_life_ins">
			           	<label for="flagins1_tambah2"><input id="flagins1_tambah2" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah2.msadd_life_ins_desc',1);">Ya</label>
			           	<label for="flagins2_tambah2"><input id="flagins2_tambah2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah2.msadd_life_ins_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagins_tambah2" id="flagins_tambah2"
						value="${cmd.medQuest_tambah2.msadd_life_ins}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadd_life_ins_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadd_life_ins">
			           	<label for="flagins1_tambah3"><input id="flagins1_tambah3" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah3.msadd_life_ins_desc',1);">Ya</label>
			           	<label for="flagins2_tambah3"><input id="flagins2_tambah3" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah3.msadd_life_ins_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagins_tambah3" id="flagins_tambah3"
						value="${cmd.medQuest_tambah3.msadd_life_ins}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadd_life_ins_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
	       <th><spring:bind path="cmd.medQuest_tambah4.msadd_life_ins">
			           	<label for="flagins1_tambah4"><input id="flagins1_tambah4" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah4.msadd_life_ins_desc',1);">Ya</label>
			           	<label for="flagins2_tambah4"><input id="flagins2_tambah4" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah4.msadd_life_ins_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagins_tambah4" id="flagins_tambah4"
						value="${cmd.medQuest_tambah4.msadd_life_ins}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah4.msadd_life_ins_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
	         <th><spring:bind path="cmd.medQuest_tambah5.msadd_life_ins">
			           	<label for="flagins1_tambah5"><input id="flagins1_tambah5" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah5.msadd_life_ins_desc',1);">Ya</label>
			           	<label for="flagins2_tambah5"><input id="flagins2_tambah5" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah5.msadd_life_ins_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagins_tambah5" id="flagins_tambah5"
						value="${cmd.medQuest_tambah5.msadd_life_ins}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah5.msadd_life_ins_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
          </tr>
      </table>