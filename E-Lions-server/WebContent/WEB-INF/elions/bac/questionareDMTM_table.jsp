<%@ include file="/include/page/header.jsp"%><c:set var="path" value="${pageContext.request.contextPath}" />
<table border="0" width="100%" cellspacing="0" cellpadding="0" class="entry">
          <tr >
            <th colspan="2" rowspan="2" class="subtitle" valign="middle"><strong>A. DATA KESEHATAN<br>
(Data Kesehatan Tertanggung tambahan I, II, III, IV, V diisi, jika ada Tertanggung Tambahan Untuk Rider HCP Family atau Eka Sehat)</strong>
				<!-- <spring:bind path="cmd.pemegang.lca_id"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }"  >
                </spring:bind>
				<spring:bind path="cmd.datausulan.mspr_premium"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30">
               </spring:bind>
               	<spring:bind path="cmd.pemegang.reg_spaj"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30">
               </spring:bind>-->
               <spring:bind path="cmd.datausulan.jml_peserta"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30">
               </spring:bind>
               <spring:bind path="cmd.pemegang.lsre_id"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30">
               </spring:bind>
			</th>
            <th colspan="2" rowspan="2" class="subtitle" valign="middle"><strong>Pemegang Polis</strong></th>
            <th colspan="2" rowspan="2" class="subtitle" valign="middle"><strong>Tertanggung Utama</strong></th>
            <th colspan="10" class="subtitle"><strong>Tertanggung Tambahan</strong></th>
          </tr>
          <tr >
            <th colspan="2" class="subtitle">I</th>
            <th colspan="2" class="subtitle">II</th>
            <th colspan="2" class="subtitle">III</th>
            <th colspan="2" class="subtitle">VI</th>
            <th colspan="2" class="subtitle">V</th>
          </tr>
          <tr > 
            <th ><b><font face="Verdana" size="1" color="#000">1. </font></b></th>
            <th ><b><font color="#000" size="1" face="Verdana">a. Berat Badan/Kg<br><br><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tinggi Badan/Cm</font></b></th>
            <th> 
			</th>
            <th colspan="-2"><spring:bind path="cmd.medQuest.msadm_berat"> 
              		<input type="text" name="${status.expression}" value="${status.value }"  size="3" maxlength="3" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
             	</spring:bind><br><br>
             	<spring:bind path="cmd.medQuest.msadm_tinggi"> 
              		<input type="text" name="${status.expression}" value="${status.value }"  size="3" maxlength="3" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
             	</spring:bind>
            </th>
			<th>&nbsp;</th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadm_berat"> 
              		<input type="text" name="${status.expression}" value="${status.value }"  size="3" maxlength="3" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
             	</spring:bind><br><br>
             	<spring:bind path="cmd.medQuest_tertanggung.msadm_tinggi"> 
              		<input type="text" name="${status.expression}" value="${status.value }"  size="3" maxlength="3" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
             	</spring:bind></th>
			<th>&nbsp;</th>
			<th><spring:bind path="cmd.medQuest_tambah.msadm_berat"> 
              		<input type="text" name="${status.expression}" value="${status.value }"  size="3" maxlength="3" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
             	</spring:bind><br><br>
             	<spring:bind path="cmd.medQuest_tambah.msadm_tinggi"> 
              		<input type="text" name="${status.expression}" value="${status.value }"  size="3" maxlength="3" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
             	</spring:bind></th>
			<th>&nbsp;</th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadm_berat"> 
              		<input type="text" name="${status.expression}" value="${status.value }"  size="3" maxlength="3" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
             	</spring:bind><br><br>
             	<spring:bind path="cmd.medQuest_tambah2.msadm_tinggi"> 
              		<input type="text" name="${status.expression}" value="${status.value }"  size="3" maxlength="3" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
             	</spring:bind></th>
			<th>&nbsp;</th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadm_berat"> 
              		<input type="text" name="${status.expression}" value="${status.value }"  size="3" maxlength="3" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
             	</spring:bind><br><br>
             	<spring:bind path="cmd.medQuest_tambah3.msadm_tinggi"> 
              		<input type="text" name="${status.expression}" value="${status.value }"  size="3" maxlength="3" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
             	</spring:bind></th>
            <th>&nbsp;</th>
			<th><spring:bind path="cmd.medQuest_tambah4.msadm_berat"> 
              		<input type="text" name="${status.expression}" value="${status.value }"  size="3" maxlength="3" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
             	</spring:bind><br><br>
             	<spring:bind path="cmd.medQuest_tambah4.msadm_tinggi"> 
              		<input type="text" name="${status.expression}" value="${status.value }"  size="3" maxlength="3" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
             	</spring:bind></th>
             <th>&nbsp;</th>
			<th><spring:bind path="cmd.medQuest_tambah5.msadm_berat"> 
              		<input type="text" name="${status.expression}" value="${status.value }"  size="3" maxlength="3" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
             	</spring:bind><br><br>
             	<spring:bind path="cmd.medQuest_tambah5.msadm_tinggi"> 
              		<input type="text" name="${status.expression}" value="${status.value }"  size="3" maxlength="3" onkeyup="checkNumber(this)" onkeypress="checkNumber(this)" <c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
             	</spring:bind></th>
          </tr>
          <tr > 
            <th >&nbsp;</th>
            <th ><b><font color="#000" size="1" face="Verdana">b. Apakah berat badan Anda berubah dalam 12 bulan terakhir? jika "YA", jelaskan berapa kg penurunan/kenaikannya dan jelaskan penyebabnya !</font></b></th>
            <th><spring:bind path="cmd.medQuest.msadm_berat_berubah">
			           	<label for="flagberat_berubah1"><input id="flagberat_berubah1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest.msadm_berubah_desc',1);">Ya</label>
			           	<label for="flagberat_berubah2"><input id="flagberat_berubah2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest.msadm_berubah_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagberat_berubah" id="flagberat_berubah"
						value="${cmd.medQuest.msadm_berat_berubah}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  
			</th>
            <th colspan="-2">
            	<spring:bind path="cmd.medQuest.msadm_berubah_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> 
            </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadm_berat_berubah">
			           	<label for="flagberat_berubah1_tertanggung"><input id="flagberat_berubah1_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tertanggung.msadm_berubah_desc',1);">Ya</label>
			           	<label for="flagberat_berubah2_tertanggung"><input id="flagberat_berubah2_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tertanggung.msadm_berubah_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagberat_berubah_tertanggung" id="flagberat_berubah_tertanggung"
						value="${cmd.medQuest_tertanggung.msadm_berat_berubah}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadm_berubah_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah.msadm_berat_berubah">
			           	<label for="flagberat_berubah1_tambah"><input id="flagberat_berubah1_tambah" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah.msadm_berubah_desc',1);">Ya</label>
			           	<label for="flagberat_berubah2_tambah"><input id="flagberat_berubah2_tambah" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah.msadm_berubah_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagberat_berubah_tambah" id="flagberat_berubah_tambah"
						value="${cmd.medQuest_tambah.msadm_berat_berubah}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah.msadm_berubah_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadm_berat_berubah">
			           	<label for="flagberat_berubah1_tambah2"><input id="flagberat_berubah1_tambah2" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah2.msadm_berubah_desc',1);">Ya</label>
			           	<label for="flagberat_berubah2_tambah2"><input id="flagberat_berubah2_tambah2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah2.msadm_berubah_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagberat_berubah_tambah2" id="flagberat_berubah_tambah2"
						value="${cmd.medQuest_tambah2.msadm_berat_berubah}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadm_berubah_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadm_berat_berubah">
			           	<label for="flagberat_berubah1_tambah3"><input id="flagberat_berubah1_tambah3" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah3.msadm_berubah_desc',1);">Ya</label>
			           	<label for="flagberat_berubah2_tambah3"><input id="flagberat_berubah2_tambah3" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah3.msadm_berubah_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagberat_berubah_tambah3" id="flagberat_berubah_tambah3"
						value="${cmd.medQuest_tambah3.msadm_berat_berubah}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadm_berubah_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
	       <th><spring:bind path="cmd.medQuest_tambah4.msadm_berat_berubah">
			           	<label for="flagberat_berubah1_tambah4"><input id="flagberat_berubah1_tambah4" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah4.msadm_berubah_desc',1);">Ya</label>
			           	<label for="flagberat_berubah2_tambah4"><input id="flagberat_berubah2_tambah4" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah4.msadm_berubah_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagberat_berubah_tambah4" id="flagberat_berubah_tambah4"
						value="${cmd.medQuest_tambah4.msadm_berat_berubah}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah4.msadm_berubah_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
	        <th><spring:bind path="cmd.medQuest_tambah5.msadm_berat_berubah">
			           	<label for="flagberat_berubah1_tambah5"><input id="flagberat_berubah1_tambah5" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah5.msadm_berubah_desc',1);">Ya</label>
			           	<label for="flagberat_berubah2_tambah5"><input id="flagberat_berubah2_tambah5" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah5.msadm_berubah_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagberat_berubah_tambah5" id="flagberat_berubah_tambah5"
						value="${cmd.medQuest_tambah5.msadm_berat_berubah}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah5.msadm_berubah_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
          </tr>
          <tr > 
            <th ><b><font face="Verdana" size="1" color="#000">2. </font></b></th>
            <th ><b><font color="#000" size="1" face="Verdana">Apakah anda sekarang dalam keadaan   sehat ? Jika "TIDAK", Mohon Dijelaskan !</font></b></th>
            <th><spring:bind path="cmd.medQuest.msadm_sehat">
			           	<label for="flagsehat1"><input id="flagsehat1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_yes('medQuest.msadm_sehat_desc',1);">Ya</label>
			           	<label for="flagsehat2"><input id="flagsehat2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_yes('medQuest.msadm_sehat_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagsehat" id="flagsehat"
						value="${cmd.medQuest.msadm_sehat}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>   
			</th>
            <th colspan="-2"><spring:bind path="cmd.medQuest.msadm_sehat_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> 
            </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadm_sehat">
			           	<label for="flagsehat1_tertanggung"><input id="flagsehat1_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_yes('medQuest_tertanggung.msadm_sehat_desc',1);">Ya</label>
			           	<label for="flagsehat2_tertanggung"><input id="flagsehat2_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_yes('medQuest_tertanggung.msadm_sehat_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagsehat_tertanggung" id="flagsehat_tertanggung"
						value="${cmd.medQuest_tertanggung.msadm_sehat}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadm_sehat_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah.msadm_sehat">
			           	<label for="flagsehat1_tambah"><input id="flagsehat1_tambah" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_yes('medQuest_tambah.msadm_sehat_desc',1);">Ya</label>
			           	<label for="flagsehat2_tambah"><input id="flagsehat2_tambah" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_yes('medQuest_tambah.msadm_sehat_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagsehat_tambah" id="flagsehat_tambah"
						value="${cmd.medQuest_tambah.msadm_sehat}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tambah.msadm_sehat_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadm_sehat">
			           	<label for="flagsehat1_tambah2"><input id="flagsehat1_tambah2" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_yes('medQuest_tambah2.msadm_sehat_desc',1);">Ya</label>
			           	<label for="flagsehat2_tambah2"><input id="flagsehat2_tambah2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_yes('medQuest_tambah2.msadm_sehat_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagsehat_tambah2" id="flagsehat_tambah2"
						value="${cmd.medQuest_tambah2.msadm_sehat}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadm_sehat_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadm_sehat">
			           	<label for="flagsehat1_tambah3"><input id="flagsehat1_tambah3" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_yes('medQuest_tambah3.msadm_sehat_desc',1);">Ya</label>
			           	<label for="flagsehat2_tambah3"><input id="flagsehat2_tambah3" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_yes('medQuest_tambah3.msadm_sehat_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagsehat_tambah3" id="flagsehat_tambah3"
						value="${cmd.medQuest_tambah3.msadm_sehat}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadm_sehat_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
	       	<th><spring:bind path="cmd.medQuest_tambah4.msadm_sehat">
			           	<label for="flagsehat1_tambah4"><input id="flagsehat1_tambah4" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_yes('medQuest_tambah4.msadm_sehat_desc',1);">Ya</label>
			           	<label for="flagsehat2_tambah4"><input id="flagsehat2_tambah4" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_yes('medQuest_tambah4.msadm_sehat_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagsehat_tambah4" id="flagsehat_tambah4"
						value="${cmd.medQuest_tambah4.msadm_sehat}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tambah4.msadm_sehat_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
	         	<th><spring:bind path="cmd.medQuest_tambah5.msadm_sehat">
			           	<label for="flagsehat1_tambah5"><input id="flagsehat1_tambah5" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_yes('medQuest_tambah5.msadm_sehat_desc',1);">Ya</label>
			           	<label for="flagsehat2_tambah5"><input id="flagsehat2_tambah5" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_yes('medQuest_tambah5.msadm_sehat_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagsehat_tambah5" id="flagsehat_tambah5"
						value="${cmd.medQuest_tambah5.msadm_sehat}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tambah5.msadm_sehat_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
          </tr>
          <tr > 
            <th ><b><font face="Verdana" size="1" color="#000">3. </font></b></th>
            <th ><b><font color="#000" size="1" face="Verdana">Apakah Anda sedang atau pernah menderita, atau pernah diberitahu atau dalam konsultasi/perawatan/pengobatan/pengawasan medis sehubungan dengan salah satu atau beberapa penyakit/gangguan/gejala lainnya?

jika "YA", jelaskan : nama penyakit, kapan, obat yang diberikan, fotokopi hasil pemeriksaan laboratorium, nama dan alamat dokter yang merawat!</font></b></th>
            <th><spring:bind path="cmd.medQuest.msadm_penyakit">
			           	<label for="flagsakit1"><input id="flagsakit1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest.msadm_penyakit_desc',1);">Ya</label>
			           	<label for="flagsakit2"><input id="flagsakit2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest.msadm_penyakit_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagsakit" id="flagsakit"
						value="${cmd.medQuest.msadm_penyakit}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  
			</th>
            <th colspan="-2"><spring:bind path="cmd.medQuest.msadm_penyakit_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> 
            </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadm_penyakit">
			           	<label for="flagsakit1_tertanggung"><input id="flagsakit1_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tertanggung.msadm_penyakit_desc',1);">Ya</label>
			           	<label for="flagsakit2_tertanggung"><input id="flagsakit2_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tertanggung.msadm_penyakit_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagsakit_tertanggung" id="flagsakit_tertanggung"
						value="${cmd.medQuest_tertanggung.msadm_penyakit}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadm_penyakit_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah.msadm_penyakit">
			           	<label for="flagsakit1_tambah"><input id="flagsakit1_tambah" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah.msadm_penyakit_desc',1);">Ya</label>
			           	<label for="flagsakit2_tambah"><input id="flagsakit2_tambah" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah.msadm_penyakit_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagsakit_tambah" id="flagsakit_tambah"
						value="${cmd.medQuest_tambah.msadm_penyakit}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tambah.msadm_penyakit_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadm_penyakit">
			           	<label for="flagsakit1_tambah2"><input id="flagsakit1_tambah2" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah2.msadm_penyakit_desc',1);">Ya</label>
			           	<label for="flagsakit2_tambah2"><input id="flagsakit2_tambah2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah2.msadm_penyakit_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagsakit_tambah2" id="flagsakit_tambah2"
						value="${cmd.medQuest_tambah2.msadm_penyakit}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadm_penyakit_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadm_penyakit">
			           	<label for="flagsakit1_tambah3"><input id="flagsakit1_tambah3" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah3.msadm_penyakit_desc',1);">Ya</label>
			           	<label for="flagsakit2_tambah3"><input id="flagsakit2_tambah3" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah3.msadm_penyakit_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagsakit_tambah3" id="flagsakit_tambah3"
						value="${cmd.medQuest_tambah3.msadm_penyakit}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadm_penyakit_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
	       	<th><spring:bind path="cmd.medQuest_tambah4.msadm_penyakit">
			           	<label for="flagsakit1_tambah4"><input id="flagsakit1_tambah4" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah4.msadm_penyakit_desc',1);">Ya</label>
			           	<label for="flagsakit2_tambah4"><input id="flagsakit2_tambah4" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah4.msadm_penyakit_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagsakit_tambah4" id="flagsakit_tambah4"
						value="${cmd.medQuest_tambah4.msadm_penyakit}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tambah4.msadm_penyakit_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
	        	<th><spring:bind path="cmd.medQuest_tambah5.msadm_penyakit">
			           	<label for="flagsakit1_tambah5"><input id="flagsakit1_tambah5" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah5.msadm_penyakit_desc',1);">Ya</label>
			           	<label for="flagsakit2_tambah5"><input id="flagsakit2_tambah5" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah5.msadm_penyakit_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagsakit_tambah5" id="flagsakit_tambah5"
						value="${cmd.medQuest_tambah5.msadm_penyakit}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tambah5.msadm_penyakit_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
          </tr>
          <tr > 
            <th ><b><font face="Verdana" size="1" color="#000">4. </font></b></th>
            <th ><b><font color="#000" size="1" face="Verdana">a. Apakah Anda sedang atau pernah menjalani konsultasi / rawat inap / operasi / biopsi / pemeriksaan laboratorium / rontgen / EKG / Treadmill / Echocardiography / USG / CT Scan / MRI atau pemeriksaan lainnya? Jika &quot;YA&quot;, jelaskan : pemeriksaan apa, kapan dilakukan, alasan dilakukan pemeriksaan tersebut, haslnya(lampirkan fotokopi hasil pemeriksaan)!</font></b></th>
            <th><spring:bind path="cmd.medQuest.msadm_medis">
			           	<label for="flagmedis1"><input id="flagmedis1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest.msadm_medis_desc',1);">Ya</label>
			           	<label for="flagmedis2"><input id="flagmedis2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest.msadm_medis_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagmedis" id="flagmedis"
						value="${cmd.medQuest.msadm_medis}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>
			</th>
            <th colspan="-2"><spring:bind path="cmd.medQuest.msadm_medis_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> 
            </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadm_medis">
			           	<label for="flagmedis1_tertanggung"><input id="flagmedis1_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tertanggung.msadm_medis_desc',1);">Ya</label>
			           	<label for="flagmedis2_tertanggung"><input id="flagmedis2_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tertanggung.msadm_medis_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagmedis_tertanggung" id="flagmedis_tertanggung"
						value="${cmd.medQuest_tertanggung.msadm_medis}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadm_medis_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah.msadm_medis">
			           	<label for="flagmedis1_tambah"><input id="flagmedis1_tambah" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah.msadm_medis_desc',1);">Ya</label>
			           	<label for="flagmedis2_tambah"><input id="flagmedis2_tambah" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah.msadm_medis_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagmedis_tambah" id="flagmedis_tambah"
						value="${cmd.medQuest_tambah.msadm_medis}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah.msadm_medis_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadm_medis">
			           	<label for="flagmedis1_tambah2"><input id="flagmedis1_tambah2" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah2.msadm_medis_desc',1);">Ya</label>
			           	<label for="flagmedis2_tambah2"><input id="flagmedis2_tambah2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah2.msadm_medis_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagmedis_tambah2" id="flagmedis_tambah2"
						value="${cmd.medQuest_tambah2.msadm_medis}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadm_medis_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadm_medis">
			           	<label for="flagmedis1_tambah3"><input id="flagmedis1_tambah3" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah3.msadm_medis_desc',1);">Ya</label>
			           	<label for="flagmedis2_tambah3"><input id="flagmedis2_tambah3" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah3.msadm_medis_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagmedis_tambah3" id="flagmedis_tambah3"
						value="${cmd.medQuest_tambah3.msadm_medis}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadm_medis_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
	       <th><spring:bind path="cmd.medQuest_tambah4.msadm_medis">
			           	<label for="flagmedis1_tambah4"><input id="flagmedis1_tambah4" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah4.msadm_medis_desc',1);">Ya</label>
			           	<label for="flagmedis2_tambah4"><input id="flagmedis2_tambah4" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah4.msadm_medis_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagmedis_tambah4" id="flagmedis_tambah4"
						value="${cmd.medQuest_tambah4.msadm_medis}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah4.msadm_medis_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
	      <th><spring:bind path="cmd.medQuest_tambah5.msadm_medis">
			           	<label for="flagmedis1_tambah5"><input id="flagmedis1_tambah5" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah5.msadm_medis_desc',1);">Ya</label>
			           	<label for="flagmedis2_tambah5"><input id="flagmedis2_tambah5" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah5.msadm_medis_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagmedis_tambah5" id="flagmedis_tambah5"
						value="${cmd.medQuest_tambah5.msadm_medis}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah5.msadm_medis_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
          </tr>
          <tr > 
            <th >&nbsp;</th>
            <th ><b><font color="#000" size="1" face="Verdana">b. Apakah Anda sedang atau pernah menjalani pengobotan ahli jiwa / radiasi / kemoterapi / pengobatan tradisional / pengobatan alternatif, menerima transfusi darah atau ditolak untuk menjadi donor darah? Jika &quot;YA&quot;, jelaskan : kapan, berapa lama, dimana, pengobatan yang diberikan, alasan diberikan pengobatan!</font></b></th>
            <th><spring:bind path="cmd.medQuest.msadm_medis_alt">
			           	<label for="flagmedisalt1"><input id="flagmedisalt1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest.msadm_medis_alt_desc',1);">Ya</label>
			           	<label for="flagmedisalt2"><input id="flagmedisalt2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest.msadm_medis_alt_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagmedisalt" id="flagmedisalt"
						value="${cmd.medQuest.msadm_medis_alt}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  
			</th>
            <th colspan="-2"><spring:bind path="cmd.medQuest.msadm_medis_alt_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> 
            </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadm_medis_alt">
			           	<label for="flagmedisalt1_tertanggung"><input id="flagmedisalt1_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tertanggung.msadm_medis_alt_desc',1);">Ya</label>
			           	<label for="flagmedisalt2_tertanggung"><input id="flagmedisalt2_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tertanggung.msadm_medis_alt_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagmedisalt_tertanggung" id="flagmedisalt_tertanggung"
						value="${cmd.medQuest_tertanggung.msadm_medis_alt}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadm_medis_alt_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah.msadm_medis_alt">
			           	<label for="flagmedisalt1_tambah"><input id="flagmedisalt1_tambah" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah.msadm_medis_alt_desc',1);">Ya</label>
			           	<label for="flagmedisalt2_tambah"><input id="flagmedisalt2_tambah" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah.msadm_medis_alt_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagmedisalt_tambah" id="flagmedisalt_tambah"
						value="${cmd.medQuest_tambah.msadm_medis_alt}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tambah.msadm_medis_alt_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadm_medis_alt">
			           	<label for="flagmedisalt1_tambah2"><input id="flagmedisalt1_tambah2" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah2.msadm_medis_alt_desc',1);">Ya</label>
			           	<label for="flagmedisalt2_tambah2"><input id="flagmedisalt2_tambah2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah2.msadm_medis_alt_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagmedisalt_tambah2" id="flagmedisalt_tambah2"
						value="${cmd.medQuest_tambah2.msadm_medis_alt}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadm_medis_alt_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadm_medis_alt">
			           	<label for="flagmedisalt1_tambah3"><input id="flagmedisalt1_tambah3" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah3.msadm_medis_alt_desc',1);">Ya</label>
			           	<label for="flagmedisalt2_tambah3"><input id="flagmedisalt2_tambah3" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah3.msadm_medis_alt_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagmedisalt_tambah3" id="flagmedisalt_tambah3"
						value="${cmd.medQuest_tambah3.msadm_medis_alt}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadm_medis_alt_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
	       <th><spring:bind path="cmd.medQuest_tambah4.msadm_medis_alt">
			           	<label for="flagmedisalt1_tambah4"><input id="flagmedisalt1_tambah4" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah4.msadm_medis_alt_desc',1);">Ya</label>
			           	<label for="flagmedisalt2_tambah4"><input id="flagmedisalt2_tambah4" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah4.msadm_medis_alt_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagmedisalt_tambah4" id="flagmedisalt_tambah4"
						value="${cmd.medQuest_tambah4.msadm_medis_alt}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tambah4.msadm_medis_alt_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
	      <th><spring:bind path="cmd.medQuest_tambah5.msadm_medis_alt">
			           	<label for="flagmedisalt1_tambah5"><input id="flagmedisalt1_tambah5" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah5.msadm_medis_alt_desc',1);">Ya</label>
			           	<label for="flagmedisalt2_tambah5"><input id="flagmedisalt2_tambah5" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah5.msadm_medis_alt_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagmedisalt_tambah5" id="flagmedisalt_tambah5"
						value="${cmd.medQuest_tambah5.msadm_medis_alt}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tambah5.msadm_medis_alt_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
          </tr>
          <tr > 
            <th ><b><font face="Verdana" size="1" color="#000">5. </font></b></th>
            <th ><b><font color="#000" size="1" face="Verdana">Keterangan Keluarga<br>
Apakah ada anggota keluarga (orang tua, kakek/nenek, kakak/adik) yang pernah menderita / sedang menderita / meninggal dunia karena salah satu atau beberapa penyakit sebagai berikut : penyakit jantung, stroke, tekanan darah tinggi, TBC, kencing manis, penyakit ginjal, hepatitis, tumor/kanker, kelainan mental, penyakit keturunan lainnya? jika &quot;YA&quot;, jelaskan : hubungan keluarga, usia, nama penyakit yang diderita!</font></b></th>
            <th><spring:bind path="cmd.medQuest.msadm_family_sick">
			           	<label for="flagfamilysick1"><input id="flagfamilysick1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest.msadm_family_sick_desc',1);">Ya</label>
			           	<label for="flagfamilysick2"><input id="flagfamilysick2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest.msadm_family_sick_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagfamilysick" id="flagfamilysick"
						value="${cmd.medQuest.msadm_family_sick}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> 
			</th>
            <th colspan="-2"><spring:bind path="cmd.medQuest.msadm_family_sick_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind>  
            </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadm_family_sick">
			           	<label for="flagfamilysick1_tertanggung"><input id="flagfamilysick1_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tertanggung.msadm_family_sick_desc',1);">Ya</label>
			           	<label for="flagfamilysick2_tertanggung"><input id="flagfamilysick2_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tertanggung.msadm_family_sick_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagfamilysick_tertanggung" id="flagfamilysick_tertanggung"
						value="${cmd.medQuest_tertanggung.msadm_family_sick}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadm_family_sick_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah.msadm_family_sick">
			           	<label for="flagfamilysick1_tambah"><input id="flagfamilysick1_tambah" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah.msadm_family_sick_desc',1);">Ya</label>
			           	<label for="flagfamilysick2_tambah"><input id="flagfamilysick2_tambah" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah.msadm_family_sick_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagfamilysick_tambah" id="flagfamilysick_tambah"
						value="${cmd.medQuest_tambah.msadm_family_sick}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah.msadm_family_sick_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadm_family_sick">
			           	<label for="flagfamilysick1_tambah2"><input id="flagfamilysick1_tambah2" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah2.msadm_family_sick_desc',1);">Ya</label>
			           	<label for="flagfamilysick2_tambah2"><input id="flagfamilysick2_tambah2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah2.msadm_family_sick_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagfamilysick_tambah2" id="flagfamilysick_tambah2"
						value="${cmd.medQuest_tambah2.msadm_family_sick}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadm_family_sick_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadm_family_sick">
			           	<label for="flagfamilysick1_tambah3"><input id="flagfamilysick1_tambah3" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah3.msadm_family_sick_desc',1);">Ya</label>
			           	<label for="flagfamilysick2_tambah3"><input id="flagfamilysick2_tambah3" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah3.msadm_family_sick_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagfamilysick_tambah3" id="flagfamilysick_tambah3"
						value="${cmd.medQuest_tambah3.msadm_family_sick}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadm_family_sick_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
	      <th><spring:bind path="cmd.medQuest_tambah4.msadm_family_sick">
			           	<label for="flagfamilysick1_tambah4"><input id="flagfamilysick1_tambah4" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah4.msadm_family_sick_desc',1);">Ya</label>
			           	<label for="flagfamilysick2_tambah4"><input id="flagfamilysick2_tambah4" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah4.msadm_family_sick_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagfamilysick_tambah4" id="flagfamilysick_tambah4"
						value="${cmd.medQuest_tambah4.msadm_family_sick}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah4.msadm_family_sick_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
	      <th><spring:bind path="cmd.medQuest_tambah5.msadm_family_sick">
			           	<label for="flagfamilysick1_tambah5"><input id="flagfamilysick1_tambah5" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah5.msadm_family_sick_desc',1);">Ya</label>
			           	<label for="flagfamilysick2_tambah5"><input id="flagfamilysick2_tambah5" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah5.msadm_family_sick_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagfamilysick_tambah5" id="flagfamilysick_tambah5"
						value="${cmd.medQuest_tambah5.msadm_family_sick}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah5.msadm_family_sick_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
          </tr>
          <tr > 
            <th ><b><font face="Verdana" size="1" color="#000">6. </font></b></th>
            <th ><b><font color="#000" size="1" face="Verdana">Pertanyaan Khusus untuk Wanita<br>
a. Apakah saat ini Anda sedang hamil? Jika &quot;YA&quot;, Umur kehamilan___minggu. Kehamilan anak ke</font></b></th>
            <th><spring:bind path="cmd.medQuest.msadm_pregnant">
			           	<label for="flaghamil1"><input id="flaghamil1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest.msadm_pregnant_desc',1);">Ya</label>
			           	<label for="flaghamil2"><input id="flaghamil2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest.msadm_pregnant_desc',0);">Tidak</label>
	           			<input type="hidden" name="flaghamil" id="flaghamil"
						value="${cmd.medQuest.msadm_pregnant}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>    
			</th>
            <th colspan="-2"><spring:bind path="cmd.medQuest.msadm_pregnant_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> 
            </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadm_pregnant">
			           	<label for="flaghamil1_tertanggung"><input id="flaghamil1_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tertanggung.msadm_pregnant_desc',1);">Ya</label>
			           	<label for="flaghamil2_tertanggung"><input id="flaghamil2_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tertanggung.msadm_pregnant_desc',0);">Tidak</label>
	           			<input type="hidden" name="flaghamil_tertanggung" id="flaghamil_tertanggung"
						value="${cmd.medQuest_tertanggung.msadm_pregnant}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadm_pregnant_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah.msadm_pregnant">
			           	<label for="flaghamil1_tambah"><input id="flaghamil1_tambah" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah.msadm_pregnant_desc',1);">Ya</label>
			           	<label for="flaghamil2_tambah"><input id="flaghamil2_tambah" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah.msadm_pregnant_desc',0);">Tidak</label>
	           			<input type="hidden" name="flaghamil_tambah" id="flaghamil_tambah"
						value="${cmd.medQuest_tambah.msadm_pregnant}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tambah.msadm_pregnant_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadm_pregnant">
			           	<label for="flaghamil1_tambah2"><input id="flaghamil1_tambah2" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah2.msadm_pregnant_desc',1);">Ya</label>
			           	<label for="flaghamil2_tambah2"><input id="flaghamil2_tambah2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah2.msadm_pregnant_desc',0);">Tidak</label>
	           			<input type="hidden" name="flaghamil_tambah2" id="flaghamil_tambah2"
						value="${cmd.medQuest_tambah2.msadm_pregnant}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadm_pregnant_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadm_pregnant">
			           	<label for="flaghamil1_tambah3"><input id="flaghamil1_tambah3" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah3.msadm_pregnant_desc',1);">Ya</label>
			           	<label for="flaghamil2_tambah3"><input id="flaghamil2_tambah3" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah3.msadm_pregnant_desc',0);">Tidak</label>
	           			<input type="hidden" name="flaghamil_tambah3" id="flaghamil_tambah3"
						value="${cmd.medQuest_tambah3.msadm_pregnant}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadm_pregnant_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
	        <th><spring:bind path="cmd.medQuest_tambah4.msadm_pregnant">
			           	<label for="flaghamil1_tambah4"><input id="flaghamil1_tambah4" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah4.msadm_pregnant_desc',1);">Ya</label>
			           	<label for="flaghamil2_tambah4"><input id="flaghamil2_tambah4" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah4.msadm_pregnant_desc',0);">Tidak</label>
	           			<input type="hidden" name="flaghamil_tambah3" id="flaghamil_tambah4"
						value="${cmd.medQuest_tambah4.msadm_pregnant}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tambah4.msadm_pregnant_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
	        <th><spring:bind path="cmd.medQuest_tambah5.msadm_pregnant">
			           	<label for="flaghamil1_tambah5"><input id="flaghamil1_tambah5" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah5.msadm_pregnant_desc',1);">Ya</label>
			           	<label for="flaghamil2_tambah5"><input id="flaghamil2_tambah5" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah5.msadm_pregnant_desc',0);">Tidak</label>
	           			<input type="hidden" name="flaghamil_tambah5" id="flaghamil_tambah5"
						value="${cmd.medQuest_tambah5.msadm_pregnant}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind>  </th>
			<th><spring:bind path="cmd.medQuest_tambah5.msadm_pregnant_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
          </tr>
          <tr >
            <th >&nbsp;</th>
            <th ><b><font color="#000" size="1" face="Verdana">b. Apakah Anda pernah mengalami operasi caesar / keguguran / aborsi / kehamilan di luar kandungan / menderita kelainan payudara / gangguan menstruasi / endometriosis / gangguan atau penyakit saat kehamilan atau melahirkan atau kelainan alat reproduksi lainnya? Jika &quot;YA&quot;, jelaskan!</font></b></th>
            <th><spring:bind path="cmd.medQuest.msadm_abortion">
			           	<label for="flagabortion1"><input id="flagabortion1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest.msadm_abortion_desc',1);">Ya</label>
			           	<label for="flagabortion2"><input id="flagabortion2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest.msadm_abortion_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagabortion" id="flagabortion"
						value="${cmd.medQuest.msadm_abortion}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
            <th colspan="-2"><spring:bind path="cmd.medQuest.msadm_abortion_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind> </th>
            <th><spring:bind path="cmd.medQuest_tertanggung.msadm_abortion">
			           	<label for="flagabortion1_tertanggung"><input id="flagabortion1_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tertanggung.msadm_abortion_desc',1);">Ya</label>
			           	<label for="flagabortion2_tertanggung"><input id="flagabortion2_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tertanggung.msadm_abortion_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagabortion_tertanggung" id="flagabortion_tertanggung"
						value="${cmd.medQuest_tertanggung.msadm_abortion}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
            <th><spring:bind path="cmd.medQuest_tertanggung.msadm_abortion_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
            <th><spring:bind path="cmd.medQuest_tambah.msadm_abortion">
			           	<label for="flagabortion1_tambah"><input id="flagabortion1_tambah" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah.msadm_abortion_desc',1);">Ya</label>
			           	<label for="flagabortion2_tambah"><input id="flagabortion2_tambah" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah.msadm_abortion_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagabortion_tambah" id="flagabortion_tambah"
						value="${cmd.medQuest_tambah.msadm_abortion}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
            <th><spring:bind path="cmd.medQuest_tambah.msadm_abortion_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
            <th><spring:bind path="cmd.medQuest_tambah2.msadm_abortion">
			           	<label for="flagabortion1_tambah2"><input id="flagabortion1_tambah2" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah2.msadm_abortion_desc',1);">Ya</label>
			           	<label for="flagabortion2_tambah2"><input id="flagabortion2_tambah2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah2.msadm_abortion_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagabortion_tambah2" id="flagabortion_tambah2"
						value="${cmd.medQuest_tambah2.msadm_abortion}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
            <th><spring:bind path="cmd.medQuest_tambah2.msadm_abortion_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
            <th><spring:bind path="cmd.medQuest_tambah3.msadm_abortion">
			           	<label for="flagabortion1_tambah3"><input id="flagabortion1_tambah3" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah3.msadm_abortion_desc',1);">Ya</label>
			           	<label for="flagabortion2_tambah3"><input id="flagabortion2_tambah3" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah3.msadm_abortion_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagabortion_tambah3" id="flagabortion_tambah3"
						value="${cmd.medQuest_tambah3.msadm_abortion}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
            <th><spring:bind path="cmd.medQuest_tambah3.msadm_abortion_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
	        <th><spring:bind path="cmd.medQuest_tambah4.msadm_abortion">
			           	<label for="flagabortion1_tambah4"><input id="flagabortion1_tambah4" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah4.msadm_abortion_desc',1);">Ya</label>
			           	<label for="flagabortion2_tambah4"><input id="flagabortion2_tambah4" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah4.msadm_abortion_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagabortion_tambah4" id="flagabortion_tambah4"
						value="${cmd.medQuest_tambah4.msadm_abortion}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
            <th><spring:bind path="cmd.medQuest_tambah4.msadm_abortion_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
	         <th><spring:bind path="cmd.medQuest_tambah5.msadm_abortion">
			           	<label for="flagabortion1_tambah5"><input id="flagabortion1_tambah5" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah5.msadm_abortion_desc',1);">Ya</label>
			           	<label for="flagabortion2_tambah5"><input id="flagabortion2_tambah5" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah5.msadm_abortion_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagabortion_tambah5" id="flagabortion_tambah5"
						value="${cmd.medQuest_tambah5.msadm_abortion}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
            <th><spring:bind path="cmd.medQuest_tambah5.msadm_abortion_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
          </tr>
          <tr > 
            <th >&nbsp;</th>
            <th ><b><font color="#000" size="1" face="Verdana"> c. Apakah Anda pernah melakukan test Pap Smear / USG abdomen / mamografi? Bila &quot;YA&quot;, jelaskan : kapan dan bagaimana hasilnya(lampirkan fotokopi hasilnya)!</font></b></th>
            <th><spring:bind path="cmd.medQuest.msadm_usg">
			           	<label for="flagusg1"><input id="flagusg1" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest.msadm_usg_desc',1);">Ya</label>
			           	<label for="flagusg2"><input id="flagusg2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest.msadm_usg_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagusg" id="flagusg"
						value="${cmd.medQuest.msadm_usg}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> 
			</th>
            <th colspan="-2"><spring:bind path="cmd.medQuest.msadm_usg_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind>
            </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadm_usg">
			           	<label for="flagusg1_tertanggung"><input id="flagusg1_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tertanggung.msadm_usg_desc',1);">Ya</label>
			           	<label for="flagusg2_tertanggung"><input id="flagusg2_tertanggung" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tertanggung.msadm_usg_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagusg_tertanggung" id="flagusg_tertanggung"
						value="${cmd.medQuest_tertanggung.msadm_usg}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tertanggung.msadm_usg_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah.msadm_usg">
			           	<label for="flagusg1_tambah"><input id="flagusg1_tambah" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah.msadm_usg_desc',1);">Ya</label>
			           	<label for="flagusg2_tambah"><input id="flagusg2_tambah" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah.msadm_usg_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagusg_tambah" id="flagusg_tambah"
						value="${cmd.medQuest_tambah.msadm_usg}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah.msadm_usg_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadm_usg">
			           	<label for="flagusg1_tambah2"><input id="flagusg1_tambah2" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah2.msadm_usg_desc',1);">Ya</label>
			           	<label for="flagusg2_tambah2"><input id="flagusg2_tambah2" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah2.msadm_usg_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagusg_tambah2" id="flagusg_tambah2"
						value="${cmd.medQuest_tambah2.msadm_usg}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind> </th>
			<th><spring:bind path="cmd.medQuest_tambah2.msadm_usg_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadm_usg">
			           	<label for="flagusg1_tambah3"><input id="flagusg1_tambah3" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah3.msadm_usg_desc',1);">Ya</label>
			           	<label for="flagusg2_tambah3"><input id="flagusg2_tambah3" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah3.msadm_usg_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagusg_tambah3" id="flagusg_tambah3"
						value="${cmd.medQuest_tambah3.msadm_usg}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah3.msadm_usg_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
	       <th><spring:bind path="cmd.medQuest_tambah4.msadm_usg">
			           	<label for="flagusg1_tambah4"><input id="flagusg1_tambah4" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah4.msadm_usg_desc',1);">Ya</label>
			           	<label for="flagusg2_tambah4"><input id="flagusg2_tambah4" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah4.msadm_usg_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagusg_tambah4" id="flagusg_tambah4"
						value="${cmd.medQuest_tambah4.msadm_usg}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah4.msadm_usg_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
	        <th><spring:bind path="cmd.medQuest_tambah5.msadm_usg">
			           	<label for="flagusg1_tambah5"><input id="flagusg1_tambah5" name="${status.expression}" type="radio" class="noBorder" value="1" onClick="disablefield_no('medQuest_tambah5.msadm_usg_desc',1);">Ya</label>
			           	<label for="flagusg2_tambah5"><input id="flagusg2_tambah5" name="${status.expression}" type="radio" class="noBorder" value="0" onClick="disablefield_no('medQuest_tambah5.msadm_usg_desc',0);">Tidak</label>
	           			<input type="hidden" name="flagusg_tambah5" id="flagusg_tambah5"
						value="${cmd.medQuest_tambah3.msadm_usg}"  size="30" style='background-color :#D4D4D4'readOnly>
	            </spring:bind></th>
			<th><spring:bind path="cmd.medQuest_tambah5.msadm_usg_desc"> 
	              <textarea cols="15" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
	            </spring:bind></th>
          </tr>
      </table>
<!-- TABEL KEDUA -->
<jsp:include page="questionareDMTM_table2.jsp">
	<jsp:param name="username2" value="tes2"/>
</jsp:include>
