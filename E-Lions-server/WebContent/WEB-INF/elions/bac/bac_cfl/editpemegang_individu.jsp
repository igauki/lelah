<%@ include file="/include/page/header.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<%User currentUser = (User) session.getAttribute("currentUser");%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>
<script type="text/javascript" src="${path }/include/js/bac/pemegang.js"></script>
<script>
// BEBERAPA JAVASCRIPT DIPINDAHKAN KE PEMEGANG.JS UNTUK MENGHINDARI ERROR EXCEEDING THE 65535 BYTES LIMIT.
	function tanyaPsave(spaj){
		//uncomment yg dibawah ini
		/*if(confirm('Apakah Spaj ini merupakan konversi produk [Powersave / Simas Prima] ke [Stable Link / Simas Stabil Link] ?\nTekan OK bila YA, atau tekan CANCEL bila TIDAK.')){
			window.location='${path}/bac/edit.htm?isPsaveToSlink=true&kopiSPAJ='+document.frmParam.kopiSPAJ.value;
		}else{
			window.location='${path}/bac/edit.htm?kopiSPAJ='+document.frmParam.kopiSPAJ.value;
		}*/
		
		
		//if(confirm('Apakah Spaj ini merupakan konversi produk [Stable Link / Simas Stabil Link] ke [Powersave / Simas Prima] ?\nTekan OK bila YA, atau tekan CANCEL bila TIDAK.')){
		//	desc=prompt("Untuk Top Up ke-","");
		//	if(desc != null || desc != ''){
		//	window.location='${path}/bac/edit.htm?kopiSPAJ='+document.frmParam.kopiSPAJ.value+'&topupslinkke='+desc;
		//	}
		//}else{
		//	window.location='${path}/bac/edit.htm?kopiSPAJ='+document.frmParam.kopiSPAJ.value;
		//}
		
		window.location='${path}/bac/edit.htm?kopiSPAJ='+document.frmParam.kopiSPAJ.value;
		/*return false;*/
	}
	function Body_onload() {
		var frmParam = document.frmParam; 
		body_onload_sub_1();
		frmParam.elements['pemegang.lus_id'].value=<%=currentUser.getLus_id()%>;
		frmParam.elements['pemegang.cbg_lus_id'].value='<%=currentUser.getLca_id()%>';
		body_onload_sub_2();
		var adaData = '${adaData}';
		//if(adaData != ''){
		//	if(!confirm('Ada data hasil input sebelumnya, apakah anda ingin menggunakan data ini?')){
		//		window.location = '${path}/bac/edit.htm?data_baru=1';
		//	}
		//}		
	}
	function loaddata_penagihan()
	{
		if (document.frmParam.elements['addressbilling.tagih'].value == "1")
		{
			if (document.frmParam.elements['addressbilling.kota_tgh'].value=="")
			{document.frmParam.elements['addressbilling.kota_tgh'].value="";
			}else{document.frmParam.elements['addressbilling.kota_tgh'].value='${cmd.addressbilling.kota_tgh}';}
			document.frmParam.elements['addressbilling.kota_tgh'].disabled = false;
			document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['addressbilling.msap_address'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#FFFFFF';
			if ((document.frmParam.elements['addressbilling.msap_address'].value=="")||(document.frmParam.elements['addressbilling.msap_address'].value==""))
			{	document.frmParam.elements['addressbilling.msap_address'].value='';
			}else{document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.alamat_sementara.value;}
			document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#FFFFFF';						
			if ((document.frmParam.elements['addressbilling.msap_zip_code'].value=="")||(document.frmParam.elements['addressbilling.msap_zip_code'].value==""))
			{document.frmParam.elements['addressbilling.msap_zip_code'].value='';
			}else{document.frmParam.elements['addressbilling.msap_zip_code'].value='${cmd.addressbilling.msap_zip_code}';}			
			document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#FFFFFF';						
			if ((document.frmParam.elements['addressbilling.msap_area_code1'].value=="")||(document.frmParam.elements['addressbilling.msap_area_code1'].value==""))
			{document.frmParam.elements['addressbilling.msap_area_code1'].value='';
			}else{document.frmParam.elements['addressbilling.msap_area_code1'].value='${cmd.addressbilling.msap_area_code1}';}
			document.frmParam.elements['addressbilling.msap_phone1'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#FFFFFF';						
			if ((document.frmParam.elements['addressbilling.msap_phone1'].value=="")||(document.frmParam.elements['addressbilling.msap_phone1'].value==""))
			{document.frmParam.elements['addressbilling.msap_phone1'].value='';
			}else{document.frmParam.elements['addressbilling.msap_phone1'].value='${cmd.addressbilling.msap_phone1}';}
			document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#FFFFFF';						
			if ((document.frmParam.elements['addressbilling.msap_area_code2'].value=="")||(document.frmParam.elements['addressbilling.msap_area_code2'].value==""))
			{document.frmParam.elements['addressbilling.msap_area_code2'].value='';
			}else{document.frmParam.elements['addressbilling.msap_area_code2'].value='${cmd.addressbilling.msap_area_code2}';}
			document.frmParam.elements['addressbilling.msap_phone2'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#FFFFFF';						
			if ((document.frmParam.elements['addressbilling.msap_phone2'].value=="")||(document.frmParam.elements['addressbilling.msap_phone2'].value==""))
			{document.frmParam.elements['addressbilling.msap_phone2'].value='';
			}else{document.frmParam.elements['addressbilling.msap_phone2'].value='${cmd.addressbilling.msap_phone2}';}
			document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#FFFFFF';						
			document.frmParam.elements['addressbilling.msap_area_code3'].value='${cmd.addressbilling.msap_area_code3}';
			document.frmParam.elements['addressbilling.msap_phone3'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#FFFFFF';						
			document.frmParam.elements['addressbilling.msap_phone3'].value='${cmd.addressbilling.msap_phone3}';
		}else{
			if (document.frmParam.elements['addressbilling.tagih'].value=="2")
			{if (document.frmParam.elements['datausulan.jenis_pemegang_polis'].value == "1"){
					if (document.frmParam.elements['contactPerson.kota_rumah'].value == "")
					{document.frmParam.elements['addressbilling.kota_tgh'].value="";
					}else{document.frmParam.elements['addressbilling.kota_tgh'].value='${cmd.contactPerson.kota_rumah}';}
					document.frmParam.elements['addressbilling.kota_tgh'].disabled = true;
					document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.alamat_sementara1.value;
					document.frmParam.elements['addressbilling.msap_zip_code'].value='${cmd.contactPerson.kd_pos_rumah}';		
					document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_area_code1'].value='${cmd.contactPerson.area_code_rumah}';		
					document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone1'].value='${cmd.contactPerson.telpon_rumah}';
					document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_area_code2'].value='${cmd.contactPerson.area_code_rumah2}';		
					document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone2'].value='${cmd.contactPerson.telpon_rumah2}';
					document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D4D4D4';					
					document.frmParam.elements['addressbilling.msap_area_code3'].value='';		
					document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone3'].value='';
					document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D4D4D4';	
				}else{
					if (document.frmParam.elements['pemegang.kota_rumah'].value == "")
					{document.frmParam.elements['addressbilling.kota_tgh'].value="";
					}else{document.frmParam.elements['addressbilling.kota_tgh'].value='${cmd.pemegang.kota_rumah}';}
					document.frmParam.elements['addressbilling.kota_tgh'].disabled = true;
					document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.alamat_sementara1.value;
					document.frmParam.elements['addressbilling.msap_zip_code'].value='${cmd.pemegang.kd_pos_rumah}';		
					document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_area_code1'].value='${cmd.pemegang.area_code_rumah}';		
					document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone1'].value='${cmd.pemegang.telpon_rumah}';
					document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_area_code2'].value='${cmd.pemegang.area_code_rumah2}';		
					document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone2'].value='${cmd.pemegang.telpon_rumah2}';
					document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D4D4D4';					
					document.frmParam.elements['addressbilling.msap_area_code3'].value='';		
					document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone3'].value='';
					document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D4D4D4';	
				}
			}else{
				if (document.frmParam.elements['addressbilling.tagih'].value=="3")
				{if (document.frmParam.elements['datausulan.jenis_pemegang_polis'].value == "1"){
						if (document.frmParam.elements['contactPerson.kota_kantor'].value == "")
						{document.frmParam.elements['addressbilling.kota_tgh'].value="";
						}else{document.frmParam.elements['addressbilling.kota_tgh'].value='${cmd.contactPerson.kota_kantor}';}
						document.frmParam.elements['addressbilling.kota_tgh'].disabled = true;
						document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.alamat_sementara2.value;
						document.frmParam.elements['addressbilling.msap_zip_code'].value='${cmd.contactPerson.kd_pos_kantor}';		
						document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_area_code1'].value='${cmd.contactPerson.area_code_kantor}';		
						document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_phone1'].value='${cmd.contactPerson.telpon_kantor}';
						document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_area_code2'].value='${cmd.contactPerson.area_code_kantor2}';		
						document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_phone2'].value='${cmd.contactPerson.telpon_kantor2}';
						document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_area_code3'].value='';document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_phone3'].value='';document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D4D4D4';
					}else{
						if (document.frmParam.elements['pemegang.kota_kantor'].value == "")
						{document.frmParam.elements['addressbilling.kota_tgh'].value="";
						}else{document.frmParam.elements['addressbilling.kota_tgh'].value='${cmd.pemegang.kota_kantor}';}
						document.frmParam.elements['addressbilling.kota_tgh'].disabled = true;
						document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.alamat_sementara2.value;
						document.frmParam.elements['addressbilling.msap_zip_code'].value='${cmd.pemegang.kd_pos_kantor}';		
						document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_area_code1'].value='${cmd.pemegang.area_code_kantor}';		
						document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_phone1'].value='${cmd.pemegang.telpon_kantor}';
						document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_area_code2'].value='${cmd.pemegang.area_code_kantor2}';		
						document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_phone2'].value='${cmd.pemegang.telpon_kantor2}';document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_area_code3'].value='';document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_phone3'].value='';document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D4D4D4';					
				}	}	}	}   }
</script>
<script language="JavaScript">
<!--
	if (!document.layers&&!document.all&&!document.getElementById)
		event="test"
			function showtip(current,e,text){
			if (document.all||document.getElementById){
				thetitle=text.split('<br>')
				if (thetitle.length>1){thetitles=''
					for (i=0;i<thetitle.length;i++)
						thetitles+=thetitle[i]
					current.title=thetitles
				}else {current.title=text}
			}else if (document.layers){
				document.tooltip.document.write('<layer bgColor="white" style="border:1px solid black;font-size:13px;">'+text+'</layer>')
				document.tooltip.document.close()
				document.tooltip.left=e.pageX+5
				document.tooltip.top=e.pageY+5
				document.tooltip.visibility="show"
			}}
// -->
	function buttonLinks(str){
		if(str=='cari'){
			spaj = document.frmParam.spaj.value;
			if(spaj==''){alert('Harap cari SPAJ terlebih dahulu');
			}else{document.getElementById('infoFrame').src='${path}/bac/edit.htm?showSPAJ='+document.frmParam.spaj.value;
			}}}
	function resetPemegangPolis(){
		parent.document.getElementById('jenis_pemegang_polis').value = document.frmParam.elements['datausulan.jenis_pemegang_polis'].value;
		parent.document.getElementById('info').click();
	}
</script>
<body onLoad="Body_onload(); ">
<XML ID=xmlData></XML>
<form name="frmParam" method="post">
<table class="entry2">
	<tr>
		<td><input type="submit" name="_target0" value=" " onclick="next1()"  <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/pp2.jpg);"
				accesskey="1" onmouseover="return overlib('Alt-1', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target1" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ttg1.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target2" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ddu1.jpg);"
				accesskey="3" onmouseover="return overlib('Alt-3', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target3" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/inv1.jpg);"
				accesskey="4" onmouseover="return overlib('Alt-4', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target4" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ag1.jpg);"
				accesskey="5" onmouseover="return overlib('Alt-5', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target5" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/kf1.jpg);"
				accesskey="6" onmouseover="return overlib('Alt-6', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" name="dummy2" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/sb1.jpg);">
				<textarea cols="40" rows="7" name="alamat_sementara" onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); ">${cmd.addressbilling.msap_address}</textarea>
						<textarea cols="40" rows="7" name="alamat_sementara1" onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "><c:choose><c:when test="${cmd.datausulan.jenis_pemegang_polis == 1}">${cmd.contactPerson.alamat_rumah}</c:when><c:otherwise>${cmd.pemegang.alamat_rumah}</c:otherwise></c:choose></textarea>
						<textarea cols="40" rows="7" name="alamat_sementara2" onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "><c:choose><c:when test="${cmd.datausulan.jenis_pemegang_polis == 1}">${cmd.contactPerson.alamat_kantor}</c:when><c:otherwise>${cmd.pemegang.alamat_kantor}</c:otherwise></c:choose></textarea>
</td>
	</tr>
	<tr>
		<td width="67%" align="left" valign="top">
		<spring:bind path="cmd.*">
			<c:if test="${not empty status.errorMessages}">
				<div id="error">WARNING:<br>
				<c:forEach var="error" items="${status.errorMessages}">
	- <c:out value="${error}" escapeXml="false" />
					<br />
				</c:forEach></div>
			</c:if>
		</spring:bind>	
		<c:if test="${cmd.pemegang.jn_bank eq 2}">
			<%-- 
			<c:if test="${cmd.pemegang.rate_1 gt 0 }">
		  	<center><font size ="3" color="red">Rate PowerSave Bank Sinarmas per hari ini ada.</font></center>
		 	</c:if>
		 	--%>
		 	<c:if test="${cmd.pemegang.rate_1 eq 0 }">
		  	<center><font size ="3" color="red">Rate Simas Prima per hari ini belum ada.Mohon Maaf, Silakan dicoba kembali beberapa saat lagi.</font></center>
		  	<script>alert('Rate Simas Prima per hari ini belum ada.Mohon Maaf, Silakan dicoba kembali beberapa saat lagi.');</script>
		 	</c:if>
			<%-- 
		 	<c:if test="${cmd.pemegang.rate_2 gt 0 }">
		  	<center><font size ="3" color="red">Rate PowerSave Bank Sinarmas manfaat bulanan per hari ini ada.</font></center>
		 	</c:if>
		 	--%>
		 	<c:if test="${cmd.pemegang.rate_2 eq 0 }">
		  	<center><font size ="3" color="red">Rate Simas Prima Manfaat Bulanan per hari ini belum ada. Mohon Maaf, Silakan dicoba kembali beberapa saat lagi.</font></center>
		  	<script>alert('Rate Simas Prima Manfaat Bulanan per hari ini belum ada. Mohon Maaf, Silakan dicoba kembali beberapa saat lagi.');</script>
		 	</c:if>
		</c:if>
		<table class="entry">
			<tr>
				<td colspan=5 height="20" style="text-align: center;">
					<%-- <input type="button"  value="Isi Data Pemegang Polis PT Guthrie" onclick="isi();" style="display:none;"> --%>
					
					<spring:bind path="cmd.datausulan.specta_save"> 
		              <input type="hidden" name="${status.expression}" value="${cmd.datausulan.specta_save}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>
					
					<spring:bind path="cmd.datausulan.jenis_pemegang_polis"> 
		              <input type="hidden" name="${status.expression}" value="${cmd.datausulan.jenis_pemegang_polis}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>
					<spring:bind path="cmd.datausulan.flag_worksite">
						<input type="hidden" name="${status.expression}" value="${status.value }" size="20">
					</spring:bind> 
					<spring:bind path="cmd.pemegang.cab_bank">
						<input type="hidden" name="${status.expression}" value="${status.value }" size="20">
					</spring:bind>
					<spring:bind path="cmd.pemegang.jn_bank">
						<input type="hidden" name="${status.expression}" value="${status.value }" size="20">
					</spring:bind>
					<spring:bind path="cmd.pemegang.lus_id">
						<input type="hidden" name="${status.expression}" value="${status.value }" size="20">
					</spring:bind>
					<spring:bind path="cmd.pemegang.cbg_lus_id">
						<input type="hidden" name="${status.expression}" value="${status.value }" size="20">
					</spring:bind>
					
					<br>
					<input type="submit" name="_target${halaman+1}" value="Next &raquo;" onClick="next()" 
						onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
					<input type="hidden" name="_page" value="${halaman}">
				</td>
			</tr>
			<input type="hidden" name="carisumber" onkeypress="if(event.keyCode==13){ document.frmParam.btnsumber.click(); return false;}"> 
			<tr>
			<c:if test="${cmd.datausulan.jenis_pemegang_polis == 0}">
				<tr>
				<th colspan=5 class="subtitle">DATA CALON PEMEGANG POLIS</th>
			</tr>
			<tr>
				<th width="18">1.</th>
				<th width="287">Nama Lengkap<br>
				<span class="info">(sesuai dengan KTP / Identitas lain tanpa gelar)</span>
				</th>
				<th width="505">
				
				<select name="pemegang.lti_id">
					<option value=""></option>
					<c:forEach var="l" items="${select_gelar}">
						<option
							<c:if test="${cmd.pemegang.lti_id eq l.ID}"> SELECTED </c:if>
							value="${l.ID}">${l.GELAR}</option>
					</c:forEach>
				</select>				
								<spring:bind path="cmd.pemegang.mcl_first">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="42" 
										maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>
									>
									<font color="#CC3300">*</font>
								</spring:bind>
							</th>
				<th width="155">Gelar &nbsp;</th>
				<th width="257"><spring:bind path="cmd.pemegang.mcl_gelar">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="20">
				</spring:bind></th>
			</tr>
			<tr>
				<th>2.</th>
				<th>Nama Ibu Kandung</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.mspe_mother">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="50"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind><font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th>3.</th>
				<th>Bukti Identitas</th>
				<th colspan="3">
					<spring:bind path="cmd.pemegang.lside_id">
						<select name="${status.expression}">
							<c:forEach var="d" items="${select_identitas}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
						<font color="#CC3300">*</font>
					</spring:bind>
				</th>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<th>No. KTP / Identitas lain</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.mspe_no_identity">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="50"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>	
				> <font color="#CC3300">*</font>
				</spring:bind></th>
			</tr>
			<tr>
				<th>4.</th>
				<th>Warga Negara</th>
				<th colspan="3">
					<spring:bind path="cmd.pemegang.lsne_id">
						<select name="${status.expression}">
							<c:forEach var="d" items="${select_negara}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
						<font color="#CC3300">*</font>
					</spring:bind>
				</th>
			</tr>
			<tr>
				<th>5.</th>
				<th>Tanggal Lahir</th>
				<th><spring:bind path="cmd.pemegang.mspe_date_birth">
					<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
					<font color="#CC3300">*</font></spring:bind></th>
				<th>Usia</th>
				<th><spring:bind path="cmd.pemegang.mste_age">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="4" readOnly>
				</spring:bind> tahun</th>
			</tr>
			<tr>
				<th></th>
				<th>di</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.mspe_place_birth">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="30"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>
					>
				<font color="#CC3300">*</font></spring:bind></th>
			</tr>
			<tr>
				<th>6.</th>
				<th>Jenis Kelamin</th>
				<th><spring:bind path="cmd.pemegang.mspe_sex">
					<label for="cowok"> <input type="radio" class=noBorder
						name="${status.expression}" value="1"
						<c:if test="${cmd.pemegang.mspe_sex eq 1 or cmd.pemegang.mspe_sex eq null}"> 
									checked</c:if>
						id="cowok">Pria </label>
					<label for="cewek"> <input type="radio" class=noBorder
						name="${status.expression}" value="0"
						<c:if test="${cmd.pemegang.mspe_sex eq 0}"> 
									checked</c:if>
						id="cewek">Wanita <font color="#CC3300">*</font>
				</spring:bind> </label></th>
				<th>Status</th>
				<th><select name="pemegang.mspe_sts_mrt" >
					<c:forEach var="marital" items="${select_marital}">
						<option
							<c:if test="${cmd.pemegang.mspe_sts_mrt eq marital.ID}"> SELECTED </c:if>
							value="${marital.ID}">${marital.MARITAL}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></th>
			</tr>
<tr>
				<th>a.</th>
				<th>Nama Suami/Istri</th>
				<th><spring:bind path="cmd.pemegang.nama_si">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="30"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>></spring:bind></th>
				<th>Tanggal Lahir</th>
				<th><spring:bind path="cmd.pemegang.tgllhr_si">
					<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
					</spring:bind></th>
			</tr>
<tr>
				<th>b.</th>
				<th>Nama Anak 1</th>
				<th><spring:bind path="cmd.pemegang.nama_anak1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="30"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
				<th>Tanggal Lahir</th>
				<th><spring:bind path="cmd.pemegang.tgllhr_anak1">
					<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
					</spring:bind></th>
			</tr>
<tr>
				<th>c.</th>
				<th>Nama Anak 2</th>
				<th><spring:bind path="cmd.pemegang.nama_anak2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="30"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
				<th>Tanggal Lahir</th>
				<th><spring:bind path="cmd.pemegang.tgllhr_anak2">
					<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
					</spring:bind></th>
			</tr>	
<tr>
				<th>d.</th>
				<th>Nama Anak 3</th>
				<th><spring:bind path="cmd.pemegang.nama_anak3">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="30"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
				<th>Tanggal Lahir</th>
				<th><spring:bind path="cmd.pemegang.tgllhr_anak3">
					<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
					</spring:bind></th>
			</tr>											
			<tr>
				<th>7.</th>
				<th>Agama</th>
				<th colspan="3">
				<script type="text/javascript">
					function agamaChange(flag){					
						if(flag=='6'){						
							document.frmParam.elements['pemegang.mcl_agama'].readOnly = false;
						}else{
							document.frmParam.elements['pemegang.mcl_agama'].value="";
							document.frmParam.elements['pemegang.mcl_agama'].readOnly = true;
						}
					}
				</script>
					<spring:bind path="cmd.pemegang.lsag_id">
						<select name="${status.expression}" onchange="agamaChange(this.value);">
							<c:forEach var="d" items="${select_agama}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
						<font color="#CC3300">*</font>
					</spring:bind>
					
				</th>
			</tr>
			<tr>
				<th></th>
				<th>Lain - Lain, Sebutkan</th>
				<th colspan="3">					
					<spring:bind path="cmd.pemegang.mcl_agama">
						<input type="text" name="${status.expression}"
							value="${status.value }" size="25" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
					</spring:bind>
				</th>
			</tr>
			<tr>
				<th>8.</th>
				<th>Pendidikan</th>
				<th colspan="3">
					<spring:bind path="cmd.pemegang.lsed_id">
						<select name="${status.expression}">
							<c:forEach var="d" items="${select_pendidikan}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
						<font color="#CC3300">*</font>
					</spring:bind>
				</th>
			</tr>
			<tr>
				<th rowspan="5">9. a.</th>
				<th rowspan="5">Alamat Rumah</th>
				<th rowspan="5"><spring:bind path="cmd.pemegang.alamat_rumah">
					<textarea cols="40" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
						</c:if>
						>${status.value }</textarea>
				<font color="#CC3300">*</font></spring:bind></th>
				<th>Kode Pos</th>
				<th><spring:bind path="cmd.pemegang.kd_pos_rumah">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="10"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>
						> <font color="#CC3300">*</font>
				</spring:bind></th>
			</tr>
			<tr>
				<th>Kota</th>
				<th ><spring:bind path="cmd.pemegang.kota_rumah"><br>
				<input type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
		    	<span id="indicator_rumah" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
				</spring:bind>   
				<font color="#CC3300">*</font>
				<spring:bind path="cmd.contactPerson.kota_rumah">
					<input type="hidden" name="${status.expression }" id="${status.expression }" value="${status.value }"/>
				</spring:bind>
				</td>
			</tr>
			<tr>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>Telp Rumah 1</th>
				<th><spring:bind path="cmd.pemegang.area_code_rumah">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					</spring:bind> <spring:bind path="cmd.pemegang.telpon_rumah">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				<font color="#CC3300">*</font></spring:bind></th>
			</tr>
			<tr>
				<th>Telp Rumah 2</th>
				<th><spring:bind path="cmd.pemegang.area_code_rumah2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.pemegang.telpon_rumah2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				<th>Handphone 1</th>
				<th><spring:bind path="cmd.pemegang.no_hp">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <font color="#CC3300">*</font></th>
				<th>Email</th>
				<th><spring:bind path="cmd.pemegang.email">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="40" maxlength="50"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				<th>Handphone 2</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.no_hp2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th rowspan="6">b.</th>
				<th rowspan="6">Alamat Kantor</th>
				<th rowspan="6"><spring:bind path="cmd.pemegang.alamat_kantor">
					<textarea cols="40" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>${status.value }</textarea>
				</spring:bind></th>
				<th>Kode Pos</th>
				<th><spring:bind path="cmd.pemegang.kd_pos_kantor">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="10"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>Kota</th>
				<th><spring:bind path="cmd.pemegang.kota_kantor"><br>
				<input type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
		    	<span id="indicator_kantor" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
			</spring:bind>   
			<spring:bind path="cmd.contactPerson.kota_kantor">
					<input type="hidden" name="${status.expression }" id="${status.expression }" value="${status.value }"/>
				</spring:bind>
				</th>
			</tr>
			<tr>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>Telp Kantor 1</th>
				<th><spring:bind path="cmd.pemegang.area_code_kantor">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.pemegang.telpon_kantor">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>Telp Kantor 2</th>
				<th><spring:bind path="cmd.pemegang.area_code_kantor2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.pemegang.telpon_kantor2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>No Fax</th>
				<th><spring:bind path="cmd.pemegang.area_code_fax">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.pemegang.no_fax">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<br>
			<tr>
				<th colspan="5" class="subtitle" style="text-align: left;">Alamat Penagihan : <spring:bind path="cmd.addressbilling.tagih">
					<select name="${status.expression }" onChange="data_penagihan();"
						>
						<option value="2">Alamat Rumah</option>
						<option value="3">Alamat Kantor</option>
						<option value="1">Lain - Lain</option>
					</select>
				 </spring:bind></th>
			</tr>
			<tr>
				<th rowspan="6">c.</th>
				<th rowspan="6">Alamat Penagihan / <br>
				Korespondensi</th>
				<th rowspan="6"><spring:bind path="cmd.addressbilling.msap_address">
					<textarea cols="40" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>${status.value }</textarea>
				<font color="#CC3300">*</font></spring:bind></th>
				<th>Kode Pos</th>
				<th><spring:bind path="cmd.addressbilling.msap_zip_code">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="10"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>Kota</th>
				<th>
				<spring:bind path="cmd.addressbilling.kota_tgh"><br>
				<input type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
		    	 <span id="indicator_tagih" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
			</spring:bind>
				</th>
			</tr>
			<tr>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>No Telepon1</th>
				<th><spring:bind path="cmd.addressbilling.msap_area_code1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.addressbilling.msap_phone1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				<font color="#CC3300">*</font></spring:bind></th>
			</tr>
			<tr>
				<th>No Telepon2</th>
				<th><spring:bind path="cmd.addressbilling.msap_area_code2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.addressbilling.msap_phone2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>No Telepon3</th>
				<th><spring:bind path="cmd.addressbilling.msap_area_code3">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.addressbilling.msap_phone3">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				<th>Handphone 1</th>
				<th><spring:bind path="cmd.addressbilling.no_hp">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
				<th>No Fax</th>
				<th><spring:bind path="cmd.addressbilling.msap_area_code_fax1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.addressbilling.msap_fax1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				<th>Handphone 2</th>
				<th><spring:bind path="cmd.addressbilling.no_hp2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
				<th>Email</th>
				<th><spring:bind path="cmd.addressbilling.e_mail">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="40" maxlength="50"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>10.</th>
				<th>Tujuan Membeli Asuransi</th>
				<th colspan="3"><select name="pemegang.mkl_tujuan" >
					<c:forEach var="tujuan" items="${select_tujuan}">
						<option
							<c:if test="${cmd.pemegang.mkl_tujuan eq tujuan.ID}"> SELECTED </c:if>
							value="${tujuan.ID}">${tujuan.TUJUAN}</option>
					</c:forEach>
				</select> <font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th></th>
				<th>Lain-Lain, Jelaskan</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.tujuana">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="100"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>11.</th>
				<th>Perkiraan Penghasilan Kotor Per Tahun</th>
				<th colspan="3"><select name="pemegang.mkl_penghasilan"
					>
					<c:forEach var="penghasilan" items="${select_penghasilan}">
						<option
							<c:if test="${cmd.pemegang.mkl_penghasilan eq penghasilan.ID}"> SELECTED </c:if>
							value="${penghasilan.ID}">${penghasilan.PENGHASILAN}</option>
					</c:forEach>
				</select>
				 <font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th>12.</th>
				<th>Sumber Pendanaan Pembelian Asuransi</th>
				<th colspan="3"><select name="pemegang.mkl_pendanaan" >
					<c:forEach var="dana" items="${select_dana}">
						<option
							<c:if test="${cmd.pemegang.mkl_pendanaan eq dana.ID}"> SELECTED </c:if>
							value="${dana.ID}">${dana.DANA}</option>
					</c:forEach>
				</select>
				 <font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th></th>
				<th>Lainnya, Jelaskan</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.danaa">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="100"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>13.</th>
				<th>Sumber Penghasilan</th>
				<th colspan="3"><select name="pemegang.mkl_smbr_penghasilan" >
					<c:forEach var="dana" items="${select_dana}">
						<option
							<c:if test="${cmd.pemegang.mkl_smbr_penghasilan eq dana.ID}"> SELECTED </c:if>
							value="${dana.ID}">${dana.DANA}</option>
					</c:forEach>
				</select>
				 <font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th></th>
				<th>Lainnya, Jelaskan</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.danaa2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="100"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>14.</th>
				<th>Klasifikasi Pekerjaan</th>
				<th colspan="3"><select name="pemegang.mkl_kerja" >
					<c:forEach var="kerja" items="${select_pekerjaan}">
						<option
							<c:if test="${cmd.pemegang.mkl_kerja eq kerja.ID}"> SELECTED </c:if>
							value="${kerja.ID}">${kerja.KLASIFIKASI}</option>
					</c:forEach>
				</select>
				 <font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th></th>
				<th>Lain-Lain, Sebutkan</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.kerjaa">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="100"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				<th><!-- <br><span class="info">(Hanya diisi apabila Klasifikasi Pekerjaan sebagai KARYAWAN)</span> -->
				
					Jabatan
					</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.kerjab">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="100"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>15.</th>
				<th>Klasifikasi Bidang Industri</th>
				<th colspan="3"><select name="pemegang.mkl_industri" >
					<c:forEach var="industri" items="${select_industri}">
						<option
							<c:if test="${cmd.pemegang.mkl_industri eq industri.ID}"> SELECTED </c:if>
							value="${industri.ID}">${industri.BIDANG}</option>
					</c:forEach>
				</select>
				 <font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th></th>
				<th>Lainnya, Sebutkan</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.industria">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="100"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>16.</th>
				<th>Hubungan Calon Pemegang Polis dengan Calon Tertanggung</th>
				<th colspan="3"><select name="pemegang.lsre_id" 
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<c:forEach var="relasi" items="${select_relasi}">
						<option
							<c:if test="${cmd.pemegang.lsre_id eq relasi.ID}"> SELECTED </c:if>
							value="${relasi.ID}">${relasi.RELATION}</option>
					</c:forEach>
				</select>
				 <font color="#CC3300">*</font></th>
			</tr>
			</c:if>
			<c:if test="${cmd.datausulan.jenis_pemegang_polis == 1}">
				<!-- ada di "editpemegang_badanusaha.jsp" -->
			</c:if>
			<tr>
				<th class="subtitle" colspan="5"></th>
			</tr>		
            <tr> 
            <td colspan="9"> 
			 <p align="left"><b> <font face="Verdana" size="2" color="#CC3300">Note : * Wajib diisi</font></b></p>
			</td>
          </tr>	
			<tr>
				<td colspan="5" style="text-align: center;">
				<input type="hidden" name="hal" value="${halaman}">
				 <spring:bind path="cmd.pemegang.indeks_halaman">
					<input type="hidden" name="${status.expression}"
						value="${halaman-1}" size="20">
				 </spring:bind>
				<input type="submit" name="_target${halaman+1}"
					value="Next &raquo;" onClick="next()" 
				accesskey="N" onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
					 <input
					type="hidden" name="_page" value="${halaman}">
				 </td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</form>
<ajax:autocomplete
				  source="pemegang.kota_rumah"
				  target="pemegang.kota_rumah"
				  baseUrl="${path}/servlet/autocomplete?s=pemegang.kota_rumah&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_rumah"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />
<ajax:autocomplete
				  source="contactPerson.kota_rumah"
				  target="contactPerson.kota_rumah"
				  baseUrl="${path}/servlet/autocomplete?s=contactPerson.kota_rumah&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_rumah"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />
<ajax:autocomplete
				  source="contactPerson.kota_kantor"
				  target="contactPerson.kota_kantor"
				  baseUrl="${path}/servlet/autocomplete?s=contactPerson.kota_kantor&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_kantor"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />
<ajax:autocomplete
				  source="pemegang.kota_kantor"
				  target="pemegang.kota_kantor"
				  baseUrl="${path}/servlet/autocomplete?s=pemegang.kota_kantor&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_kantor"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />
	<ajax:autocomplete
				  source="addressbilling.kota_tgh"
				  target="addressbilling.kota_tgh"
				  baseUrl="${path}/servlet/autocomplete?s=addressbilling.kota_tgh&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_tagih"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />		

<script type="text/javascript">	
	if(frmParam.elements['datausulan.jenis_pemegang_polis'].value != 1){
		agamaChange('${cmd.pemegang.lsag_id}');
	}
</script>	  
</body>
<%@ include file="/include/page/footer.jsp"%>