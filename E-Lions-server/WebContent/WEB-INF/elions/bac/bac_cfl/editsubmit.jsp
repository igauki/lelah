<%@ include file="/include/page/header.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>

<script>

function cariregion(spaj,nama)
	{
	if (spaj !="")
	{
		var dok;
		if(self.opener)
			dok = self.opener.document;
		else
			dok = parent.document;
		
		var forminput;
		if(dok.formpost) forminput = dok.formpost;
		else if(dok.frmParam) forminput = dok.frmParam;
		addOptionToSelect(dok, forminput.spaj, spaj, spaj);
		
			ajaxManager.listcollect(spaj , nama,
			{callback:function(map) {
		
				DWRUtil.useLoadingMessage();
				document.frmParam.koderegion.value=map.LSRG_NAMA;
				document.frmParam.kodebisnis.value = map.LSBS_ID;
				document.frmParam.numberbisnis.value = map.LSDBS_NUMBER;
				document.frmParam.jml_peserta.value = map.jml_peserta;
				document.frmParam.cab_bank.value=map.CAB_BANK;
				document.frmParam.jn_bank.value=map.JN_BANK;
				document.frmParam.lus_id.value = map.LUS_ID;
			   },
			  timeout:15000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
}
</script>

<body bgcolor="ffffff" onLoad="cariregion('${nomorspaj}','region');">

	<XML ID=xmlData></XML>
	<form name="frmParam" method="post">
		<input type="hidden" name="koderegion">
		<input type="hidden" name="kodebisnis">
		<input type="hidden" name="validhcp">
		<input type="hidden" name="numberbisnis">
		<input type="hidden" name="jml_peserta">
		<input type="hidden" name="lus_id">
		<input type="hidden" name="cab_bank">
		<input type="hidden" name="jn_bank">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td width="67%" height="35" bgcolor="#FDFDFD" valign="bottom">
					<img border="0" src="${path}/include/image/pp1.jpg" height="27"
						width="99" alt="Pemegang Polis">
					<img border="0" src="${path}/include/image/ttg1.jpg" height="27"
						width="99" alt="Tertanggung">
					<img border="0" src="${path}/include/image/ddu1.jpg" height="27"
						width="99" alt="Usulan Asuransi">
					<img border="0" src="${path}/include/image/inv1.jpg" height="27"
						width="99" alt="Detail Investasi">
					<img border="0" src="${path}/include/image/ag1.jpg" height="27"
						width="99" alt="Detail Agen">
					<img border="0" src="${path}/include/image/kf1.jpg" height="27"
						width="99" alt="Konfirmasi">
					<img border="0" src="${path}/include/image/sb2.jpg" height="27"
						width="99" alt="Submit">
				</td>
			</tr>
		</table>
		<input type="hidden" name="spaj" value=${nomorspaj}>
		<c:if test="${status eq \"input\"}">
			<c:if test="${nomorspaj eq \"\"}">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td bgcolor="#DDDDDD">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td width="67%" rowspan="12" align="left" bgcolor="#800000">

							<p align="center">
								<font face="Verdana" size="2" color="#FFFFFF"> <br>
									<center>
										<b> Spaj tidak berhasil di submit. Silahkan dicoba kembali
											atau hubungi administrator. </b>
										<br>
										<input type="button" name="ref" value="Coba Kembali"
											onclick="parent.document.getElementById('infoFrame').src='${path}/bac/edit.htm>
									</center> </font>
							</p>
						</td>
					</tr>
				</table>
			</c:if>
			<c:if test="${nomorspaj ne \"\" and empty sessionScope.currentUser.cab_bank}">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td bgcolor="#DDDDDD">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td width="67%" rowspan="12" align="left" bgcolor="#800000">

							<p align="center">
								<font face="Verdana" size="2" color="#FFFFFF"> <br>
									<center>
										<b> Spaj berhasil di submit. Silahkan catat nomor spaj : <elions:spaj
												nomor="${nomorspaj}" /> </b>
									</center> </font>
							</p>
						</td>
					</tr>
				</table>
			</c:if>
		</c:if>
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>
