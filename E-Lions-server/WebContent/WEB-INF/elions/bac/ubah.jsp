<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type"
			content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<meta HTTP-EQUIV="Expires" CONTENT="-1">
		<link REL="Stylesheet" TYPE="text/css"
			HREF="${path }/include/css/default.css" media="screen">
		<link href="${path}/include/image/eas.ico" rel="shortcut icon">
		<!--  -->
		<link REL="Stylesheet" TYPE="text/css"
			HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css"
			media="screen">
		<script type="text/javascript"
			src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript"
			src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript"
			src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<!--  -->
		<script type="text/javascript" src="${path }/include/js/default.js"></script>
		<!-- Ajax Related -->
		<script type="text/javascript" src="${path}/include/js/ajax.js"></script>
		<script type="text/javascript"
			src="${path }/dwr/interface/ajaxManager.js"></script>
		<script type="text/javascript" src="${path }/dwr/engine.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>
		<script type="text/javascript">
			hideLoadingMessage();
		</script>
		<script type="text/javascript">
			function bukaTutup(flag){
							
				if(flag=='stopro'){
					if(document.getElementById(flag).checked){								
						document.getElementById("proses").disabled="";
					}else{				
						document.getElementById("proses").disabled="true";
					}
				}else if(flag=='rate'){
					if(document.getElementById(flag).checked){								
						document.getElementById(flag+"Change").readOnly="";
					}else{				
						document.getElementById(flag+"Change").value="";
						document.getElementById(flag+"Change").readOnly="true";
					}
				}else{
					
					if(document.getElementById(flag).checked){								
						document.getElementById(flag+"Change").disabled="";
					}else{				
						document.getElementById(flag+"Change").value=0;
						document.getElementById(flag+"Change").disabled="true";
					}
				}
			}
			
			function checkValidasi(flag){
				if(flag=='mgi'){
					var awal=document.getElementById("mgiAwal").value.replace(/^\s+|\s+$/g, '');
					var change=document.getElementById("mgiChange").value.replace(/^\s+|\s+$/g, '');
					
					if(awal==change){
						alert("WARNING : MGI Sama..!!!");
						document.getElementById("proses").disabled="true";
					}else{
						document.getElementById("proses").disabled="";
					}
				}else  if(flag=='ro'){
					var awal=document.getElementById("roAwal").value.replace(/^\s+|\s+$/g, '');
					var change=document.getElementById("roChange").value.replace(/^\s+|\s+$/g, '');
					
					if(awal==change){
						alert("WARNING : Roll Over Sama..!!!");
						document.getElementById("proses").disabled="true";
					}else{
						document.getElementById("proses").disabled="";
					}
				}else  if(flag=='rate'){
					var awal=document.getElementById("rateAwal").value.replace(/^\s+|\s+$/g, '');
					var change=document.getElementById("rateChange").value.replace(/^\s+|\s+$/g, '');
					
					if(awal==change){
						
						if(confirm("Rate Baru = Rate Lama Yakin Lanjut ??")){
							document.getElementById("proses").disabled="";							
						}else{
							document.getElementById("proses").disabled="true";							 
						}
					}else{
						document.getElementById("proses").disabled="";	
										 
					}
				}else{
					/*var proses=0;
					if(document.getElementById("mgi").checked){
						var awal=document.getElementById("mgiAwal").value.replace(/^\s+|\s+$/g, '');
						var change=document.getElementById("mgiChange").value.replace(/^\s+|\s+$/g, '');
						
						if(awal==change){
							alert("WARNING : MGI Sama..!!!");
							document.getElementById("proses").disabled="true";
						}else{
							if(document.getElementById("ro").checked){
								var awal=document.getElementById("roAwal").value.replace(/^\s+|\s+$/g, '');
								var change=document.getElementById("roChange").value.replace(/^\s+|\s+$/g, '');
								
								if(awal==change){
									alert("WARNING : Roll Over Sama..!!!");
									document.getElementById("proses").disabled="true";
								}else{
									if(document.getElementById("rate").checked){
										var awal=document.getElementById("rateAwal").value.replace(/^\s+|\s+$/g, '');
										var change=document.getElementById("rateChange").value.replace(/^\s+|\s+$/g, '');
										
										if(awal==change){
											
											if(confirm("Rate Baru = Rate Lama Yakin Lanjut ??")){
												document.getElementById("proses").disabled="";							
											}else{
												proses=1;							 
											}
										}else{
											proses=1;		 
										}
									}
								}
							}
						}
					}
					
					if(document.getElementById("stopro").checked && (document.getElementById("mgi").checked||document.getElementById("ro").checked||document.getElementById("rate").checked)){
						alert("Gagal Proses Data.~nProses -Stop Roll Over- Tdk Dapat digabung dengan proses lainnya.");
					}else{
						if(proses==1){
							if(confirm("Proses data..?")){
								window.location="${path}/bac/ubah.htm";					
							}else{
								alert("Proses dibatalkan.");						 
							}
						}else{
						}
					}*/
				}
			}
			
			function prosesAja(){				
				var selisih="${cmd.selisih}";
				
				var message="";
				if(confirm("Proses data...?")){
					if(selisih<1||selisih>14){
						if(selisih<1)message="Perubahan Sudah Lebih Dari Tgl Jatuh Tempo, Yakin Lanjutkan Proses ?";
						if(selisih>4)message="Perubahan Belum memasuki 14 hari sebelum jatuh tempo, Yakin Lanjutkan Proses ?";
						
						if(confirm(message)){
							document.getElementById("proses1").value="1";
							document.getElementById("formpost").submit();					
						}else{
							alert("Proses dibatalkan.");						 
						}
						
					}else{
						document.getElementById("proses1").value="1";
						document.getElementById("formpost").submit();					
					}
				}else{
					alert("Proses dibatalkan.");
				}
				
				
				
			}
			
			
			
			<%--function gantiBegDate(){
				document.getElementById("ganti").value="1";
				document.getElementById("formpost").submit();
			}--%>
		</script>

		<c:if test="${cmd.flagPinjaman gt 0}">
			<script type="text/javascript">
				alert("Pinjaman Konvensional Masih Aktif / Belum Paid !!!");
			</script>
		</c:if>

		<c:if test="${ not empty cmd.messageError}">
			<script type="text/javascript">
				alert("${cmd.messageError}");
			</script>
		</c:if>
		<c:if test="${not empty param.pesan}">
			<script>
				alert('${param.pesan}');
			</script>
		</c:if>
		
		
	</head>
	<body onload="<c:choose>
		<c:when test="${(not empty param.pesan) and (not empty cmd.listPowersaveUbah)}">
			setupPanes('container1','tab2');
		</c:when>
		<c:otherwise>
			setupPanes('container1','tab1');
		</c:otherwise>
	</c:choose>" style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Ubah
						Powersave</a>
				</li>
				<li>
					<a href="#" onClick="return showPane('pane2', this)" id="tab2">View
						Ubah Powersave</a>
				</li>
			</ul>

			<div class="tab-panes">

				<div id="pane1" class="panes">
					<form:form name="formpost" id="formpost" commandName="cmd"
						cssStyle="text-align: left;">
						<fieldset>
							<legend>
								Ubah Powersave
							</legend>
							<table class="entry2">
								<tr>
									<th nowrap="nowrap" style="width: 250px;">
										Nomor Polis / Nomor Register SPAJ:
									</th>
									<td>
										<form:input path="reg_spaj" />
									</td>
								</tr>
								<tr>
									<th nowrap="nowrap" style="width: 250px;">
										Nama Pemegang Polis:
									</th>
									<td>
										${cmd.mcl_first}
									</td>
								</tr>
								<tr>
									<th nowrap="nowrap" style="width: 250px;">
										Nomor Polis:
									</th>
									<td>
										${cmd.mspo_policy_no}
									</td>
								</tr>
								<tr>
									<th nowrap="nowrap" style="width: 250px;">
										Nomor SPAJ:
									</th>
									<td>
										${cmd.reg_spaj}
									</td>
								</tr>
								<tr>
									<th nowrap="nowrap"></th>
									<td>
										<input type="submit" name="show" value="Show">
										<spring:hasBindErrors name="cmd">
											<div id="error">
												<form:errors path="*" delimiter="<br>" />
											</div>
										</spring:hasBindErrors>
									</td>
								</tr>
							</table>
						</fieldset>

						<c:if test="${not empty cmd.powersaveProses}">
							<fieldset>
								<table class="entry2">
									<tr>
										<th colspan="2" style="font-size: 13pt;color: blue;">
											Tanggal Mulai Periode Selanjutnya :
											<fmt:formatDate value="${cmd.powersaveProses.mps_batas_date}" pattern="dd/MM/yyyy"/>
											<%--
											<form:input path="powersaveProses.mps_batas_date" cssErrorClass="inpError" onchange="gantiBegDate();"/>
											<input type="hidden" name="ganti">--%>
										</th>
									</tr>
									<tr>
										<th width="20%">
											<fieldset>
												<table>
													<tr>
														<td>
															<label for="mgi">
																<form:checkbox path="flagEditMGI" value="1" id="mgi"
																	onclick="bukaTutup('mgi');" cssErrorClass="inpError"/>
																Ubah MGI
															</label>
														</td>
													</tr>
													<tr>
														<td>
															<label for="ro">
																<form:checkbox path="flagEditRO" value="1" id="ro"
																	onclick="bukaTutup('ro');" cssErrorClass="inpError"/>
																Ubah Roll Over
															</label>
														</td>
													</tr>
													<tr>
														<td>
															<label for="rate">
																<form:checkbox path="flagEditRATE" value="1" id="rate"
																	onclick="bukaTutup('rate');" cssErrorClass="inpError"/>
																Ubah Bunga Special Plan
															</label>
														</td>
													</tr>
													<tr>
														<td>
															<label for="stopro">
																<form:checkbox path="flagStopRO" value="1" id="stopro"
																	onclick="bukaTutup('stopro');" cssErrorClass="inpError"/>
																Stop Roll Over
															</label>
														</td>
													</tr>
												</table>
											</fieldset>
										</th>
										<th align="left">
											<fieldset>
												<table>
													<tr>
														<td>
															MGI :
														</td>
														<td>
															<form:select path="powersaveProses.mps_jangka_inv"
																disabled="true" id="mgiAwal" cssErrorClass="inpError">
																<form:option value="0" label="" />
																<form:option value="1" label="1 Bln" />
																<form:option value="3" label="3 Bln" />
																<form:option value="6" label="6 Bln" />
																<form:option value="12" label="12 Bln" />
																<form:option value="24" label="24 Bln" />
																<form:option value="36" label="36 Bln" />
															</form:select>
														</td>
														<td>
															Ganti :
														</td>
														<td>
															<form:select path="mps_jangka_inv" id="mgiChange"
																disabled="true" onchange="checkValidasi('mgi');" cssErrorClass="inpError">
																<form:option value="0" label="" />
																<form:option value="1" label="1 Bln" />
																<form:option value="3" label="3 Bln" />
																<form:option value="6" label="6 Bln" />
																<form:option value="12" label="12 Bln" />
																<form:option value="24" label="24 Bln" />
																<form:option value="36" label="36 Bln" />
															</form:select>
														</td>
													</tr>
													<tr>
														<td>
															RO :
														</td>
														<td>
															<form:select path="powersaveProses.mps_roll_over"
																disabled="true" id="roAwal" cssErrorClass="inpError">
																<form:option value="0" label="" />
																<form:option value="1" label="Premi Deposit + Interest" />
																<form:option value="2" label="Premi Deposit" />
																<form:option value="3" label="Autobreak" />
															</form:select>
														</td>
														<td>
															Ganti :
														</td>
														<td>
															<form:select path="mps_roll_over" id="roChange"
																disabled="true" onchange="checkValidasi('ro');" cssErrorClass="inpError">
																<form:option value="0" label="" />
																<form:option value="1" label="Premi Deposit + Interest" />
																<form:option value="2" label="Premi Deposit" />
															</form:select>
														</td>
													</tr>
													<tr>
														<td>
															RATE :
														</td>
														<td>
															<form:input path="powersaveProses.mps_rate"
																readonly="true" id="rateAwal" cssErrorClass="inpError"/>
														</td>
														<td>
															Ganti :
														</td>
														<td>
															<form:input path="mps_rate" id="rateChange"
																readonly="true" onkeyup="checkValidasi('rate');" cssErrorClass="inpError"/>
														</td>
													</tr>
													<tr>
														<td>
															&nbsp;
														</td>
														<td>
														</td>
													</tr>

												</table>
											</fieldset>

										</th>

									</tr>
									<tr>
										<th colspan="2" align="left">
											Note :
											<br />
											<form:textarea path="powersaveUbah.mpu_note" cols="100"
												rows="10" />
										</th>
									</tr>
									<tr>
										<th colspan="2" align="left">
											<input type="button" value="proses" name="proses" id="proses"
												onclick="prosesAja();" disabled="disabled">
											<input type="button" value="view" name="view" id="view"
												onClick="return showPane('pane2', this)">
											
											<input type="hidden" name="proses1" id="proses1">
										</th>
									</tr>
								</table>
							</fieldset>
						</c:if>

					</form:form>
				</div>
				<div id="pane2" class="panes">
					<c:choose>
						<c:when test="${not empty cmd.listPowersaveUbah}">
							<table class="entry2" width="50%">
								<tr>
									<th nowrap="nowrap" style="width: 250px;">
										Nama Pemegang Polis:
									</th>
									<td>
										${cmd.mcl_first}
									</td>
								</tr>
								<tr>
									<th nowrap="nowrap" style="width: 250px;">
										Nomor Polis:
									</th>
									<td>
										${cmd.mspo_policy_no}
									</td>
								</tr>
								<tr>
									<th nowrap="nowrap" style="width: 250px;">
										Nomor SPAJ:
									</th>
									<td>
										${cmd.reg_spaj}
									</td>
								</tr>
								<tr>
									<th>
									</th>
									<td colspan="2" align="left" style="text-align: left;">
										<input type="button" value="Input Ubah Powersave" name="ubah"
												onClick="return showPane('pane1', this)">
									</td>
								</tr>
								<tr>
									<th>
									</th>
									<td colspan="1" align="left" style="text-align: left;">
										<table class="entry" border="1" cellpadding="0" cellspacing="0" width="70%">
											<tr>
												<th>
													No
												</th>
												<th>
													Beg Date MGI
												</th>
												<th>
													Tgl. Input
												</th>
												<th>
													Jenis
												</th>
												<th>
													Data Awal
												</th>
												<th>
													Data Baru
												</th>
												<th>
													Note
												</th>
											</tr>
											<c:forEach items="${cmd.listPowersaveUbah}" var="d"
												varStatus="s">
												<tr>
													<td>
														${s.count}&nbsp;
													</td>
													<td>
														<fmt:formatDate value="${d.mpu_tgl_awal}" pattern="dd/MM/yyyy"/>&nbsp;
													</td>
													<td>
														<fmt:formatDate value="${d.mpu_input}" pattern="dd/MM/yyyy"/>&nbsp;
													</td>
													<td>
														<c:choose>
															<c:when test="${d.mpu_jenis eq \"1\"}">
																Ganti MGI
															</c:when>
															<c:when test="${d.mpu_jenis eq \"2\"}">
																Ganti Roll Over
															</c:when>
															<c:when test="${d.mpu_jenis eq \"3\"}">
																Ganti Rate
															</c:when>
															<c:otherwise>
																Stop Roll Over
															</c:otherwise>
														</c:choose>
													</td>
													<td>
														
														<c:choose>
															<c:when test="${d.mpu_jenis eq \"1\"}">
																<fmt:formatNumber type="number">${d.mpu_awal}</fmt:formatNumber> bulan		
															</c:when>
															<c:when test="${d.mpu_jenis eq \"2\"}">
																<c:choose>
																	<c:when test="${d.mpu_awal eq \"1.0\"}">
																		Roll Over All
																	</c:when>
																	<c:when test="${d.mpu_awal eq \"2.0\"}">
																		Roll Over Premi Deposit
																	</c:when>
																	<c:otherwise>
																		Auto Break
																	</c:otherwise>
																</c:choose>
															</c:when>
															<c:when test="${d.mpu_jenis eq \"3\"}">
																<fmt:formatNumber>${d.mpu_awal}</fmt:formatNumber>%
															</c:when>
															<c:otherwise>
																
															</c:otherwise>
														</c:choose>&nbsp;
													</td>
													<td>
														
														<c:choose>
															<c:when test="${d.mpu_jenis eq \"1\"}">
																<fmt:formatNumber type="number">${d.mpu_akhir}</fmt:formatNumber> bulan		
															</c:when>
															<c:when test="${d.mpu_jenis eq \"2\"}">
																<c:choose>
																	<c:when test="${d.mpu_akhir eq \"1.0\"}">
																		Roll Over All
																	</c:when>
																	<c:when test="${d.mpu_akhir eq \"2.0\"}">
																		Roll Over Premi Deposit
																	</c:when>
																	<c:otherwise>
																		Auto Break
																	</c:otherwise>
																</c:choose>
															</c:when>
															<c:when test="${d.mpu_jenis eq \"3\"}">
																<fmt:formatNumber>${d.mpu_akhir}</fmt:formatNumber>%
															</c:when>
															<c:otherwise>
																
															</c:otherwise>
														</c:choose>&nbsp;

													</td>
													<td>
														${d.mpu_note}&nbsp;
													</td>
												</tr>
											</c:forEach>
										</table>
									</td>
								</tr>
							</table>
							
						</c:when>
						<c:otherwise>
							<table class="entry" width="100%">
							
								<tr>
									<th colspan="1"  align="center" style="text-align: center;">
										Tidak ada data perubahan Powersave
									</th>
								</tr>
								<tr>
									<th colspan="1"  align="center" style="text-align: center;" >
										<input type="button" value="Input Ubah Powersave" name="ubah"
												onClick="return showPane('pane1', this)">
									</th>
								</tr>
							</table>
						</c:otherwise>
					</c:choose>
				</div>
			</div>

		</div>
		
	

		<c:choose>
			<c:when test="${cmd.flagEditMGI eq \"1\"}">
				<script type="text/javascript">
					if(document.getElementById("mgiChange")) document.getElementById("mgiChange").disabled="";
				</script>
			</c:when>
			<c:otherwise>
				<script type="text/javascript">
					if(document.getElementById("mgiChange")) {
						document.getElementById("mgiChange").disabled="true";
						document.getElementById("mgiChange").value=0;
					}
				</script>
			</c:otherwise>
		</c:choose>

		<c:choose>
			<c:when test="${cmd.flagEditRO eq \"1\"}">
				<script type="text/javascript">
					if(document.getElementById("roChange")) document.getElementById("roChange").disabled="";
				</script>
			</c:when>
			<c:otherwise>
				<script type="text/javascript">
					if(document.getElementById("roChange")) {
						document.getElementById("roChange").disabled="true";
						document.getElementById("roChange").value=0;
					}
				</script>
			</c:otherwise>
		</c:choose>

		<c:choose>
			<c:when test="${cmd.flagEditRATE eq \"1\"}">
				<script type="text/javascript">
					if(document.getElementById("rateChange")){
						document.getElementById("rateChange").readOnly="";
					}
				</script>
			</c:when>
			<c:otherwise>
				<script type="text/javascript">
					if(document.getElementById("rateChange")){
						document.getElementById("rateChange").readOnly="true";
						document.getElementById("rateChange").value="";
					}
				</script>
			</c:otherwise>
		</c:choose>
		
	</body>

</html>
