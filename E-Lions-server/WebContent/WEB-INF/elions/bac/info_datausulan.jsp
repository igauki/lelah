<%@ include file="/include/page/taglibs.jsp"%>
<script>

	function submitSpaj(){
		if(confirm("Approved Spaj ?"))
		{
			parent.window.document.getElementById('submitSpajController').click();
		}
	}
</script>


<fieldset>
	<legend>Produk Utama</legend>
	<table class="entry2">
	
		<tr>
			<th nowrap="nowrap">
				Produk
			</th>
			<th nowrap="nowrap" class="left">
				<select disabled>
					<c:forEach var="lsdbs" items="${listlsdbs}">
						<option 
						<c:if test="${cmd.dataUsulan.lsdbs_number eq lsdbs.lsdbs_number}">selected</c:if> >[${lsdbs.lsbs_id}] ${lsdbs.lsdbs_name}</option> 
					</c:forEach> 
				</select>
				Jenis Produk : 
				<select disabled>
					<c:forEach var="tipe" items="${listtipeproduk}"> <option 
					<c:if test="${cmd.dataUsulan.tipeproduk eq tipe.lstp_id}">selected</c:if> >${tipe.lstp_produk}</option> 
					</c:forEach> 
				</select>
				Class
				<input type="text" value="${cmd.dataUsulan.mspr_class}" size="3" readonly>
			</th>
		</tr>
		<c:if test="${cmd.dataUsulan.flag_bulanan ne null}">
			<tr>
				<th nowrap="nowrap">
					Manfaat
				</th>
				<th nowrap="nowrap" class="left">
					<c:if test="${ cmd.dataUsulan.flag_bulanan eq 0}"> Bukan Manfaat bulanan </c:if>
					<c:if test="${ cmd.dataUsulan.flag_bulanan eq 1}">Manfaat bulanan </c:if>
					<span class="info">* Khusus Stable Link/Stable Save</span>
				</th>
			</tr>
		</c:if>
		<tr>
			<th nowrap="nowrap">
				Masa Berlaku Polis
			</th>
			<th nowrap="nowrap" class="left">
				<input type="text" value="<fmt:formatDate value="${cmd.dataUsulan.mste_beg_date}" pattern="dd/MM/yyyy"/>" size="12" readonly>
				s/d			
				<input type="text" value="<fmt:formatDate value="${cmd.dataUsulan.mste_end_date}" pattern="dd/MM/yyyy"/>" size="12" readonly>
				Lama Tanggung
				<input type="text" value="${cmd.dataUsulan.mspr_ins_period}" size="3" readonly>
				Cuti Premi
				<input type="text" value="${cmd.dataUsulan.mspo_installment}" size="3" readonly>
			</th>
		</tr>
		
		<tr>
			<th nowrap="nowrap">
				Uang Pertanggungan
			</th>
			<th nowrap="nowrap" class="left">
				<input style="text-align: right;" type="text" value="<fmt:formatNumber value="${cmd.dataUsulan.mspr_tsi}" type="currency" currencySymbol="${cmd.dataUsulan.lku_symbol} " maxFractionDigits="2" minFractionDigits="0" />" size="30" readonly>
				Premi
				<input style="text-align: right;" type="text" value="<fmt:formatNumber value="${cmd.dataUsulan.mspr_premium}" type="currency" currencySymbol="${cmd.dataUsulan.lku_symbol} " maxFractionDigits="2" minFractionDigits="0" />" size="30" readonly>
			</th>
		</tr>
		
		<tr>
			<th nowrap="nowrap">
				Lama Bayar
			</th>
			<th nowrap="nowrap" class="left">
				<input type="text" value="${cmd.dataUsulan.mspo_pay_period}" size="3" readonly>
				Cara Bayar
				<input type="text" value="${cmd.dataUsulan.lscb_pay_mode}" size="15" readonly>
				Tanggal Debet
				<input type="text" value="<fmt:formatDate value="${cmd.pemegang.mste_tgl_recur}" pattern="dd/MM/yyyy"/>" size="12" readonly>
			</th>
		</tr>
		<tr>
			<th nowrap="nowrap">Autodebet</th>
			<th nowrap="nowrap" class="left">
				<c:choose>
					<c:when test="${cmd.dataUsulan.mste_flag_cc eq 0}">None</c:when>
					<c:when test="${cmd.dataUsulan.mste_flag_cc eq 1}">Credit Card</c:when>
					<c:when test="${cmd.dataUsulan.mste_flag_cc eq 2}">Tabungan</c:when>
					<c:when test="${cmd.dataUsulan.mste_flag_cc eq 3}">Payroll</c:when>
					<c:when test="${cmd.dataUsulan.mste_flag_cc eq 8}">Penagihan ke Sekuritas</c:when>
				</c:choose>
			</th>
		</tr>
		
	</table>
</fieldset>

<c:choose>
	<c:when test="${not empty cmd.stableLink}">
		<display:table id="baris" name="cmd.stableLink" class="displaytag" requestURI="" export="true" pagesize="20">
			<%--<display:column property="REG_SPAJ" title="SPAJ"  sortable="true"/>--%>                                                                                       
			<display:column property="MSL_NO" title="No."  sortable="true"/>                                                                                           
			<display:column property="LJI_INVEST" title="Investasi"  sortable="true"/>                                                                                   
			<display:column property="ROLLOVER" title="Rollover"  sortable="true"/>                                                                                       
			<display:column property="LSPD_POSITION" title="Posisi"  sortable="true"/>                                                                             
			<display:column property="MSL_BDATE" title="BegDate" format="{0, date, dd/MM/yyyy}"  sortable="true"/>                                                      
			<display:column property="MSL_EDATE" title="EndDate" format="{0, date, dd/MM/yyyy}"  sortable="true"/>                                                      
			<display:column property="MSL_PREMI" title="Premi" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                           
			<display:column property="MSL_RATE" title="Rate" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                             
			<display:column property="MSL_MGI" title="MTI"  sortable="true"/>                                                                                         
			<%--<display:column property="msl_bunga" title="msl_bunga" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>--%>                                           
			<display:column property="MSL_TGL_NAB" title="Tgl NAB" format="{0, date, dd/MM/yyyy}"  sortable="true"/>                                                  
			<display:column property="MSL_NAB" title="NAB" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                               
			<display:column property="MSL_UNIT" title="Unit" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>                                             
			<%--<display:column property="msl_saldo_unit" title="msl_saldo_unit" format="{0, number, #,##0.00;(#,##0.00)}"  sortable="true"/>--%>                                 
			<display:column property="MSL_INPUT_DATE" title="Tgl Input" format="{0, date, dd/MM/yyyy}"  sortable="true"/>                                            
			<display:column property="LUS_LOGIN_NAME" title="User"  sortable="true"/>                                                                           
			<display:column property="MSL_DESC" title="Deskripsi"  sortable="true"/>                                                                                       
		</display:table>                                                                                                                                              			
	</c:when>
	<c:when test="${not empty cmd.powerSave}">
		<fieldset>
			<legend>Informasi Produk PowerSave</legend>
			<c:if test="${pin gt 0}">
				<span style="color: red; font-size: 16px;">PERHATIAN! POLIS INI SUDAH DILAKUKAN PENCAIRAN.</span><br>		
			</c:if>

			<display:table id="ps" name="cmd.powerSave" class="displaytag">
				<display:column title="Jangka Waktu" style="width: 100px; text-align: center;">
					<select disabled>
		                <c:forEach var="jk" items="${select_jangka_invest}">
		                	<option <c:if test="${ps.mps_jangka_inv eq jk.ID}"> SELECTED </c:if> value="${jk.ID}">${jk.JANGKAWAKTU}</option>
		                </c:forEach> 
					</select>
				</display:column>
				<display:column property="mps_rate" title="Bunga" format="{0, number, #,##0.000;(#,##0.000)}" />
				<display:column property="mps_batas_date" title="Tanggal Jatuh Tempo" format="{0, date, dd/MM/yyyy}"/>
				<display:column property="mps_prm_deposit" title="Jumlah Investasi" format="{0, number, #,##0.00;(#,##0.00)}" />
				<display:column property="mps_prm_interest" title="Jumlah Bunga" format="{0, number, #,##0.00;(#,##0.00)}" />
				<display:column title="Jenis Roll-over" style="width: 200px; text-align: center;">
					<select disabled>
						<c:forEach var="roll" items="${select_rollover}">
							<option <c:if test="${ps.mps_roll_over eq roll.ID}"> SELECTED </c:if> value="${roll.ID}">${roll.ROLLOVER}</option>
						</c:forEach> 
					</select>
				</display:column>
			</display:table>
			<display:table id="ps2" name="cmd.powerSave" class="displaytag">
				<display:column title="Jenis Bunga" style="width: 200px; text-align: center;">
					<select disabled>
						<c:forEach var="bg" items="${select_jenisbunga}">
							<option <c:if test="${ps2.mps_jenis_plan eq bg.ID}"> SELECTED </c:if> value="${bg.ID}">${bg.JENIS}</option>
						</c:forEach> 
					</select>
				</display:column>
				<display:column property="mpr_note" title="Nomor Memo" />
				<display:column property="mpr_breakable" title="Breakable" />
				<display:column title="Status Karyawan" style="width: 200px; text-align: center;">
					<select disabled>
						<c:forEach var="kr" items="${select_karyawan}">
							<option <c:if test="${ps2.mps_employee eq kr.ID}"> SELECTED </c:if> value="${kr.ID}">${kr.KRY}</option>
						</c:forEach> 
					</select>
				</display:column>
			</display:table>
		</fieldset>	
	</c:when>
</c:choose>

<c:if test="${not empty cmd.dataUsulan.daftaRider}">
	<fieldset>
		<legend>Rider</legend>
		<display:table id="produk" name="cmd.dataUsulan.daftaRider" class="displaytag">
			<display:column property="lsdbs_name" title="Produk" style="text-align: left;" />
			<display:column property="mspr_unit" title="Unit" />
			<display:column property="mspr_class" title="Class" />
			<display:column property="mspr_tsi" title="Uang Pertanggungan" format="{0, number, #,##0.00;(#,##0.00)}" />
			<display:column property="mspr_premium" title="Premi" format="{0, number, #,##0.00;(#,##0.00)}" total="true" />
			<display:column property="mspr_ins_period" title="Lama Tanggung" />
			<display:column property="mspr_beg_date" title="Awal Pertanggungan" format="{0, date, dd/MM/yyyy}" />
			<display:column property="mspr_end_date" title="Akhir Pertanggungan" format="{0, date, dd/MM/yyyy}" />
			<display:column property="mspr_end_pay" title="Akhir Bayar" format="{0, date, dd/MM/yyyy}" />
			<display:column property="mspr_extra" title="EM" format="{0, number, #,##0.00;(#,##0.00)}" />
			<display:column property="mspr_rate" title="Rate" format="{0, number, #,##0.00;(#,##0.00)}" />
		</display:table>
	</fieldset>
</c:if>

<c:if test="${not empty cmd.dataUsulan.daftarplus}">
	<fieldset>
		<legend>Peserta Tambahan</legend>
		<display:table id="plus" name="cmd.dataUsulan.daftarplus" class="displaytag">
		<display:column property="no_urut" title="Nomor" />
		<display:column property="nama" title="Nama" />
		<display:column property="tgl_lahir" title="Tanggal Lahir" format="{0, date, dd/MM/yyyy}"/>
		<display:column property="umur" title="Usia" />
		<display:column property="sex" title="Jenis Kelamin" />
		<display:column property="lsre_relation" title="Hubungan dengan Pemegang Polis" />
		<display:column property="jenis_peserta" title="Jenis Rider" />
		</display:table>
	</fieldset>
</c:if>

<c:if test="${not empty cmd.dataUsulan.daftabenef}">
	<fieldset>
		<legend>Penerima Manfaat</legend>
		<display:table id="benef" name="cmd.dataUsulan.daftabenef" class="displaytag">
			<display:column property="msaw_number" title="Nomor" />
			<display:column property="msaw_first" title="Nama" style="text-align: left;" />
			<display:column property="msaw_birth" title="Tanggal Lahir" format="{0, date, dd/MM/yyyy}" />
			<display:column property="lsre_relation" title="Hubungan dengan Calon Tertanggung" />
			<display:column property="msaw_persen" title="%" />
		</display:table>
	</fieldset>
</c:if>
<c:if test="${cmd.dataUsulan.worksite ne null}">
	<fieldset>
		<legend>Informasi Perusahaan Worksite</legend>
			<table class="entry2">
				<tr>
					<th>Nama Perusahaan :${cmd.dataUsulan.worksite.MCL_GELAR} ${cmd.dataUsulan.worksite.MCL_FIRST }</th>
				</tr>
				<tr>
					<th>NIK : ${cmd.dataUsulan.worksite.NIK }</th>
				</tr>
			</table>
	</fieldset>
</c:if>
<br />
<hr>
<br />
<c:choose>
<c:when test="${successOrNo eq 'yes'}">
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose> 

