<%@ include file="/include/page/header_mall.jsp"%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>
<script>
	
	function Body_onload() {
	
		document.frmParam.alamat_sementara.style.display="none";

		<c:if test="${not empty cmd.pemegang.keterangan_blanko_spaj}">
			if(confirm("Nomor Blanko yang dicantumkan sudah pernah digunakan untuk produk yang sama.\nTekan YES untuk melihat data sebelumnya, atau CANCEL untuk melanjutkan input.")){
				//FIXME : popup window diwsini
				popWin('${path}/uw/view.htm?p=v&c=c&showSPAJ=${cmd.pemegang.keterangan_blanko_spaj}', 500, 800);
			}
		</c:if>

		document.frmParam.elements['tertanggung.mste_age'].style.backgroundColor ='#D7D2D0';

		if (((document.frmParam.elements['tertanggung.mkl_tujuan'].value).toUpperCase()!=("Lain - Lain").toUpperCase()) && (document.frmParam.elements['tertanggung.mkl_tujuan'].value!=null) )
		{
			document.frmParam.elements['tertanggung.tujuana'].value='';
		}else{
			if ((document.frmParam.elements['tertanggung.tujuana'].value).toUpperCase()==("Lain - Lain").toUpperCase())
			{
				document.frmParam.elements['tertanggung.tujuana'].value='';
			}
		}
		
		if  (((document.frmParam.elements['tertanggung.mkl_pendanaan'].value).toUpperCase()!=("Lainnya").toUpperCase()) && (document.frmParam.elements['tertanggung.mkl_pendanaan'].value!=null))
		{
			document.frmParam.elements['tertanggung.danaa'].value='';
		}else{
			if  ((document.frmParam.elements['tertanggung.danaa'].value).toUpperCase()==("Lainnya").toUpperCase())
			{
				document.frmParam.elements['tertanggung.danaa'].value='';
			}
		}
		
		if  (((document.frmParam.elements['tertanggung.mkl_smbr_penghasilan'].value).toUpperCase() != ("Lainnya").toUpperCase()) && (document.frmParam.elements['tertanggung.mkl_smbr_penghasilan'].value!=null))
		{
			document.frmParam.elements['tertanggung.shasil'].value='';
		}else{
			if ((document.frmParam.elements['tertanggung.shasil'].value).toUpperCase() ==("Lainnya").toUpperCase())
			{
				document.frmParam.elements['tertanggung.shasil'].value='';
			}
		}
		
		if  (((document.frmParam.elements['tertanggung.mkl_kerja'].value).toUpperCase()!=("Lainnya").toUpperCase()) &&(document.frmParam.elements['tertanggung.mkl_kerja'].value!=null))
		{
			document.frmParam.elements['tertanggung.kerjaa'].value='';
		}else{
			if  ((document.frmParam.elements['tertanggung.kerjaa'].value).toUpperCase()==("Lainnya").toUpperCase())
			{
				document.frmParam.elements['tertanggung.kerjaa'].value='';
			}
		}
		
		if (((document.frmParam.elements['tertanggung.mkl_kerja'].value).toUpperCase()!=("Karyawan Swasta").toUpperCase()) && (document.frmParam.elements['tertanggung.mkl_kerja'].value!=null))
		{
			document.frmParam.elements['tertanggung.kerjab'].value='';
		}else{
			if ((document.frmParam.elements['tertanggung.kerjab'].value).toUpperCase()==("Karyawan Swasta").toUpperCase())
			{
				document.frmParam.elements['tertanggung.kerjab'].value='';
			}
		}
				
		if  (((document.frmParam.elements['tertanggung.mkl_industri'].value).toUpperCase()!=("Lainnya").toUpperCase()) && (document.frmParam.elements['tertanggung.mkl_industri'].value!=null))
		{
			document.frmParam.elements['tertanggung.industria'].value='';
		}else{
			if ((document.frmParam.elements['tertanggung.industria'].value).toUpperCase()==("Lainnya").toUpperCase())
			{
				document.frmParam.elements['tertanggung.industria'].value='';
			}
		}
		
		document.frmParam.elements['addressbilling.kota_tgh'].value='${cmd.addressbilling.kota_tgh}';
		document.frmParam.elements['addressbilling.kota_tgh'].readOnly = true;
		document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D7D2D0';
		
		document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.alamat_sementara.value;
		document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
		document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D7D2D0';
		
		document.frmParam.elements['addressbilling.msap_zip_code'].value='${cmd.addressbilling.msap_zip_code}';
		document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
		document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D7D2D0';
		
		document.frmParam.elements['addressbilling.msap_area_code1'].value='${cmd.addressbilling.msap_area_code1}';
		document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
		document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D7D2D0';
		
		document.frmParam.elements['addressbilling.msap_phone1'].value='${cmd.addressbilling.msap_phone1}';
		document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
		document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D7D2D0';
		
		document.frmParam.elements['addressbilling.msap_area_code2'].value='${cmd.addressbilling.msap_area_code2}';
		document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;
		document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D7D2D0';
		
		document.frmParam.elements['addressbilling.msap_phone2'].value='${cmd.addressbilling.msap_phone2}';
		document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
		document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D7D2D0';
		
		document.frmParam.elements['addressbilling.msap_area_code3'].value='${cmd.addressbilling.msap_area_code3}';
		document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
		document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D7D2D0';
		
		document.frmParam.elements['addressbilling.msap_phone3'].value='${cmd.addressbilling.msap_phone3}';
		document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
		document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D7D2D0';				
		
		document.frmParam.elements['addressbilling.no_hp'].value='${cmd.addressbilling.no_hp}';
		document.frmParam.elements['addressbilling.no_hp'].readOnly = true;
		document.frmParam.elements['addressbilling.no_hp'].style.backgroundColor ='#D7D2D0';

		document.frmParam.elements['addressbilling.no_hp2'].value='${cmd.addressbilling.no_hp2}';
		document.frmParam.elements['addressbilling.no_hp2'].readOnly = true;
		document.frmParam.elements['addressbilling.no_hp2'].style.backgroundColor ='#D7D2D0';		
		
		document.frmParam.elements['addressbilling.e_mail'].value='${cmd.addressbilling.e_mail}';
		document.frmParam.elements['addressbilling.e_mail'].readOnly = true;
		document.frmParam.elements['addressbilling.e_mail'].style.backgroundColor ='#D7D2D0';
		
		document.frmParam.elements['addressbilling.msap_area_code_fax1'].value='${cmd.addressbilling.msap_area_code_fax1}';
		document.frmParam.elements['addressbilling.msap_area_code_fax1'].readOnly = true;
		document.frmParam.elements['addressbilling.msap_area_code_fax1'].style.backgroundColor ='#D7D2D0';		
		
		document.frmParam.elements['addressbilling.msap_fax1'].value='${cmd.addressbilling.msap_fax1}';
		document.frmParam.elements['addressbilling.msap_fax1'].readOnly = true;
		document.frmParam.elements['addressbilling.msap_fax1'].style.backgroundColor ='#D7D2D0';				
		firstRelationCheck()
		
	}
	

	function firstRelationCheck(){
	var relasi = '${cmd.pemegang.lsre_id}';

		 if  ( (relasi=='1') ) {
		 		document.frmParam.elements['tertanggung.nama_si'].readOnly = true;
		 		document.frmParam.elements['tertanggung.nama_si'].style.backgroundColor ='#D7D2D0';
		 		document.frmParam.elements['tertanggung.nama_anak1'].readOnly = true;
		 		document.frmParam.elements['tertanggung.nama_anak1'].style.backgroundColor ='#D7D2D0';
		 		document.frmParam.elements['tertanggung.nama_anak2'].readOnly = true;
		 		document.frmParam.elements['tertanggung.nama_anak2'].style.backgroundColor ='#D7D2D0';
			 	document.frmParam.elements['tertanggung.nama_anak3'].readOnly = true;
		 		document.frmParam.elements['tertanggung.nama_anak3'].style.backgroundColor ='#D7D2D0';
		 				 
				document.frmParam.elements['tertanggung.email'].readOnly = true;
				document.frmParam.elements['tertanggung.email'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.area_code_fax'].readOnly = true;
				document.frmParam.elements['tertanggung.area_code_fax'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.no_fax'].readOnly = true;
				document.frmParam.elements['tertanggung.no_fax'].style.backgroundColor ='#D7D2D0';
				
				document.frmParam.elements['tertanggung.lti_id'].disabled = true;			
				document.frmParam.elements['tertanggung.lti_id'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.mcl_first'].readOnly = true;
				document.frmParam.elements['tertanggung.mcl_first'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.mcl_gelar'].readOnly = true;
				document.frmParam.elements['tertanggung.mcl_gelar'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.mspe_mother'].readOnly = true;
				document.frmParam.elements['tertanggung.mspe_mother'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.lside_id'].readOnly = true;
				document.frmParam.elements['tertanggung.lside_id'].disabled = true;			
				document.frmParam.elements['tertanggung.lside_id'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.mspe_no_identity'].readOnly = true;
				document.frmParam.elements['tertanggung.mspe_no_identity'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.lsne_id'].readOnly = true;
				document.frmParam.elements['tertanggung.lsne_id'].disabled = true;			
				document.frmParam.elements['tertanggung.lsne_id'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.mspe_place_birth'].readOnly = true;
				document.frmParam.elements['tertanggung.mspe_place_birth'].style.backgroundColor ='#D7D2D0';

				document.frmParam.elements['tertanggung.mspe_sts_mrt'].readOnly = true;
				document.frmParam.elements['tertanggung.mspe_sts_mrt'].disabled = true;			
				document.frmParam.elements['tertanggung.mspe_sts_mrt'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.lsag_id'].readOnly = true;
				document.frmParam.elements['tertanggung.lsag_id'].disabled = true;			
				document.frmParam.elements['tertanggung.lsag_id'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.mcl_agama'].disabled = true;			
				document.frmParam.elements['tertanggung.mcl_agama'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.lsed_id'].readOnly = true;
				document.frmParam.elements['tertanggung.lsed_id'].disabled= true;			
				document.frmParam.elements['tertanggung.lsed_id'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.alamat_rumah'].readOnly = true;
				document.frmParam.elements['tertanggung.alamat_rumah'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.kota_rumah'].readOnly = true;
				document.frmParam.elements['tertanggung.kota_rumah'].style.backgroundColor ='#D7D2D0';
				
				document.frmParam.elements['tertanggung.kd_pos_rumah'].readOnly = true;
				document.frmParam.elements['tertanggung.kd_pos_rumah'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.area_code_rumah'].readOnly = true;
				document.frmParam.elements['tertanggung.area_code_rumah'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.telpon_rumah'].readOnly = true;
				document.frmParam.elements['tertanggung.telpon_rumah'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.area_code_rumah2'].readOnly = true;
				document.frmParam.elements['tertanggung.area_code_rumah2'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.telpon_rumah2'].readOnly = true;
				document.frmParam.elements['tertanggung.telpon_rumah2'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.alamat_kantor'].readOnly = true;
				document.frmParam.elements['tertanggung.alamat_kantor'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.kota_kantor'].readOnly = true;
				document.frmParam.elements['tertanggung.kota_kantor'].style.backgroundColor ='#D7D2D0';

				document.frmParam.elements['tertanggung.kd_pos_kantor'].readOnly = true;
				document.frmParam.elements['tertanggung.kd_pos_kantor'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.area_code_kantor'].readOnly = true;
				document.frmParam.elements['tertanggung.area_code_kantor'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.telpon_kantor'].readOnly = true;
				document.frmParam.elements['tertanggung.telpon_kantor'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.area_code_kantor2'].readOnly = true;				
				document.frmParam.elements['tertanggung.area_code_kantor2'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.telpon_kantor2'].readOnly = true;
				document.frmParam.elements['tertanggung.telpon_kantor2'].style.backgroundColor ='#D7D2D0';				
				document.frmParam.elements['tertanggung.mkl_kerja'].readOnly = true;
				document.frmParam.elements['tertanggung.mkl_kerja'].disabled= true;			
				document.frmParam.elements['tertanggung.mkl_kerja'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.kerjaa'].readOnly = true;
				document.frmParam.elements['tertanggung.kerjaa'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.kerjab'].readOnly = true;
				document.frmParam.elements['tertanggung.kerjab'].style.backgroundColor ='#D7D2D0';				
				document.frmParam.elements['tertanggung.mkl_industri'].readOnly = true;
				document.frmParam.elements['tertanggung.mkl_industri'].style.backgroundColor ='#D7D2D0';			
				document.frmParam.elements['tertanggung.industria'].readOnly = true;
				document.frmParam.elements['tertanggung.industria'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.mkl_tujuan'].readOnly = true;
				document.frmParam.elements['tertanggung.mkl_tujuan'].disabled= true;			
				document.frmParam.elements['tertanggung.mkl_tujuan'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.tujuana'].readOnly = true;
				document.frmParam.elements['tertanggung.tujuana'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.mkl_pendanaan'].readOnly = true;
				document.frmParam.elements['tertanggung.mkl_pendanaan'].disabled= true;			
				document.frmParam.elements['tertanggung.mkl_pendanaan'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.mkl_smbr_penghasilan'].readOnly = true;
				document.frmParam.elements['tertanggung.mkl_smbr_penghasilan'].disabled= true;			
				document.frmParam.elements['tertanggung.mkl_smbr_penghasilan'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.danaa'].readOnly = true;
				document.frmParam.elements['tertanggung.danaa'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.shasil'].readOnly = true;
				document.frmParam.elements['tertanggung.shasil'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.mkl_penghasilan'].readOnly = true;
				document.frmParam.elements['tertanggung.mkl_penghasilan'].disabled= true;			
				document.frmParam.elements['tertanggung.mkl_penghasilan'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.no_hp2'].readOnly = true;
				document.frmParam.elements['tertanggung.no_hp2'].style.backgroundColor ='#D7D2D0';
				document.frmParam.elements['tertanggung.no_hp'].readOnly = true;
				document.frmParam.elements['tertanggung.no_hp'].style.backgroundColor ='#D7D2D0';

				if('${cmd.pemegang.mspe_sex}' == '1'){
					document.frmParam.jenis_kelamin[0].checked=true;
					document.frmParam.elements['tertanggung.mspe_sex'].value == '1';
				}else{
					document.frmParam.jenis_kelamin[1].checked=true;
					document.frmParam.elements['tertanggung.mspe_sex'].value == '0';
				}
				document.frmParam.jenis_kelamin[0].disabled = true;
				document.frmParam.jenis_kelamin[1].disabled = true;
				document.frmParam.elements['tertanggung.mspe_sex'].value = '${cmd.pemegang.mspe_sex}';
								
			}
	}
		
</script>
<script language="JavaScript">
<!--
	if (!document.layers&&!document.all&&!document.getElementById)
		event="test"
		
	function showtip(current,e,text){
	
		if (document.all||document.getElementById){
			thetitle=text.split('<br>')
			if (thetitle.length>1){
				thetitles=''
				
			for (i=0;i<thetitle.length;i++)
				thetitles+=thetitle[i]
				
			current.title=thetitles
		}
		else
			current.title=text
	}
	
	else if (document.layers){
				document.tooltip.document.write('<layer bgColor="white" style="border:1px solid black;font-size:13px;">'+text+'</layer>')
				document.tooltip.document.close()
				document.tooltip.left=e.pageX+5
				document.tooltip.top=e.pageY+5
				document.tooltip.visibility="show"
			}
	}
	
	function hidetip(){
		if (document.layers)
			document.tooltip.visibility="hidden"
	}
	
		function next()
	{
		eval( " document.frmParam.elements['pemegang.indeks_halaman'].value = ((document.frmParam.hal.value)+1);");
			if(document.frmParam.jenis_kelamin[0].checked==true){
					document.frmParam.elements['tertanggung.mspe_sex'].value = '1';
			}else{
				document.frmParam.elements['tertanggung.mspe_sex'].value = '0';
			}	
			
	}

 function kelamin(hasil)
 {
	document.frmParam.elements['tertanggung.mspe_sex'].value = hasil;
 }
 
// -->
</script>


<body onLoad="Body_onload();">

<XML ID=xmlData></XML>
<form name="frmParam" method="post">
<table class="" cellspacing="0" cellpadding="0" border="0">
	<tr  >
		<td>
			<input type="submit" name="_target0" value=" "  <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/pp1.jpg);" 
				accesskey="1" onmouseover="return overlib('Alt-1', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target1" value=" "  <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ttg2.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target2" value=" "  <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ddu1.jpg);"
				accesskey="3" onmouseover="return overlib('Alt-3', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target3" value=" "  <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/inv1.jpg);"
				accesskey="4" onmouseover="return overlib('Alt-4', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target4" value=" "  <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ag1.jpg);"
				accesskey="5" onmouseover="return overlib('Alt-5', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target5" value=" " <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/kf1.jpg);"
				accesskey="6" onmouseover="return overlib('Alt-6', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" name="dummy2" value=" "  <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/sb1.jpg);">
			<textarea cols="40" rows="7" name="alamat_sementara"    
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); ">${cmd.addressbilling.msap_address}</textarea>
		
</td>
	</tr>
	<tr>
		<td width="67%" align="left">

			<c:if test="${not empty cmd.pemegang.keterangan_blanko_spaj}">
				<div id="error">ERROR:<br>
			  		${cmd.pemegang.keterangan_blanko}
				</div>
			</c:if>

		<spring:bind path="cmd.*">
			<c:if test="${not empty status.errorMessages }">
				<div id="error">ERROR:<br>
				<c:forEach var="error" items="${status.errorMessages}">
														- <c:out value="${error}" escapeXml="false" />
					<br />
				</c:forEach></div>
			</c:if>
		</spring:bind>
		<!--<table class="" cellspacing="0" cellpadding="0" border="0">-->
		<table class="form_input" style="font-size: 11px;">
			<tr>
				<td colspan="5" style="text-align: center;">
				<input type="submit"
					name="_target${halaman-1}" value="Prev &laquo;" 
					onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input
					type="submit" name="_target${halaman+1}" value="Next &raquo;"
					onClick="next()" 
					onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="hidden" name="_page"
					value="${halaman}">
					<spring:bind path="cmd.pemegang.keterangan_blanko">
					<input type="hidden" name="${status.expression}"
						value="${status.value }" size="42" maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind>
				</td>
			</tr>
			<tr>
				<td colspan="5" style="color: #CC3300;font-weight: bold">
				(
				<c:choose>
					<c:when test="${cmd.contactPerson.pic_jenis eq '1'}">
						Hanya diisi apabila PIC berbeda dengan calon Tertanggung
					</c:when>
					<c:otherwise>
						Hanya diisi apabila Pemegang Polis berbeda dengan calon Tertanggung
					</c:otherwise>
				</c:choose>
				)</td>
			</tr>
			<tr>
				
            <th colspan="5">DATA CALON TERTANGGUNG </td>
			</tr>
			<tr>
				<td width="28">1.</td>
				<td width="280">Nama Lengkap <br>
				<span class="info">(sesuai dengan KTP / Identitas lain tanpa gelar)</span>
				</td>
				<td width="498">
				<select name="tertanggung.lti_id">
					<option value=""></option>
					<c:forEach var="l" items="${select_gelar}">
						<option
							<c:if test="${cmd.tertanggung.lti_id eq l.ID}"> SELECTED </c:if>
							value="${l.ID}">${l.GELAR}</option>
					</c:forEach>
				</select>				
				<spring:bind path="cmd.tertanggung.mcl_first">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind>
				<font color="#CC3300">*</font></td>
				<td width="171">Gelar</td>
				<td width="245"><spring:bind path="cmd.tertanggung.mcl_gelar">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="15">
				</spring:bind></td>
			</tr>
			<tr>
				<td>2.</td>
				<td>Nama Ibu Kandung</td>
				<td colspan="3"><spring:bind path="cmd.tertanggung.mspe_mother">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="50"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind></td>
			</tr>
			<tr>
				<td>3.</td>
				<td>Bukti Identitas</td>
				<td colspan="3"><select name="tertanggung.lside_id" >
					<c:forEach var="identitas" items="${select_identitas}">
						<option
							<c:if test="${cmd.tertanggung.lside_id eq identitas.ID}"> SELECTED </c:if>
							value="${identitas.ID}">${identitas.TDPENGENAL}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>No. KTP / Identitas lain</td>
				<td colspan="3"><spring:bind path="cmd.tertanggung.mspe_no_identity">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="50"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind>
				<font color="#CC3300">*</font></td>
			</tr>
			<tr>
				<td>3a.</td> 
				<td>Tanggal Kadaluarsa</td>
				<td colspan="3"><spring:bind path="cmd.tertanggung.mspe_no_identity_expired">
					<c:if test="${cmd.pemegang.lsre_id eq 1}">
						<script>inputDate('${status.expression}', '${status.value}', true);</script>
					</c:if>
					<c:if test="${cmd.pemegang.lsre_id ne 1}">
						<script>inputDate('${status.expression}', '${status.value}', false);</script>
					</c:if>
				</spring:bind><font color="#CC3300">*</font></td>
			</tr>		
			<tr>
				<td>4.</td>
				<td>Warga Negara</td>
				<td><select name="tertanggung.lsne_id" >
					<c:forEach var="negara" items="${select_negara}">
						<option
							<c:if test="${cmd.tertanggung.lsne_id eq negara.ID}"> SELECTED </c:if>
							value="${negara.ID}">${negara.NEGARA}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></td>
				<td>Umur Beasiswa</td>
				<td><spring:bind path="cmd.tertanggung.mspo_umur_beasiswa">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="3" maxlength="2"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind><font color="#CC3300">*</font></td>
			</tr>
			<tr>
				<td>5.</td>
				<td>Tanggal Lahir</td>
				<td><spring:bind path="cmd.tertanggung.mspe_date_birth">
					<c:if test="${cmd.pemegang.lsre_id eq 1}">
						<script>inputDate('${status.expression}', '${status.value}', true);</script>
					</c:if>
					<c:if test="${cmd.pemegang.lsre_id ne 1}">
						<script>inputDate('${status.expression}', '${status.value}', false);</script>
					</c:if>
				</spring:bind><font color="#CC3300">*</font></td>
				<td>Usia</td>
				<td><spring:bind path="cmd.tertanggung.mste_age">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="4" readOnly>
				</spring:bind> tahun</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>di</td>
				<td colspan="3"><spring:bind path="cmd.tertanggung.mspe_place_birth">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="30"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind><font color="#CC3300">*</font></td>
			</tr>
			<tr>
				<td>6.</td>
				<td>Jenis Kelamin</td>
				<td>
				<label for="cowok"> <input class="noBorder" type="radio"
						name="jenis_kelamin" value="1"  onClick="kelamin('1');"
						<c:if test="${cmd.tertanggung.mspe_sex eq 1 or cmd.tertanggung.mspe_sex eq null}" > 
              checked</c:if>
						id="cowok" >Pria </label>
			  <label for="cewek"> <input class="noBorder" type="radio"
						name="jenis_kelamin" value="0" onClick="kelamin('0');"
						<c:if test="${cmd.tertanggung.mspe_sex eq 0}" > 
              checked</c:if>
						id="cewek" >Wanita </label>
			  
			  <spring:bind path="cmd.tertanggung.mspe_sex">
					<input type="hidden" name="${status.expression}"
						value="${status.value }"  >
				</spring:bind>
			  <font color="#CC3300">*</font>
				</td>
				<td>Status</td>
				<td><select name="tertanggung.mspe_sts_mrt" >
					<c:forEach var="marital" items="${select_marital}">
						<option
							<c:if test="${cmd.tertanggung.mspe_sts_mrt eq marital.ID}"> SELECTED </c:if>
							value="${marital.ID}">${marital.MARITAL}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></td>
			</tr>
<tr>
				<td>a.</td>
				<td>Nama Suami/Istri</td>
				<td><spring:bind path="cmd.tertanggung.nama_si">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="30"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				<font color="#CC3300">*</font></spring:bind></td>
				<td>Tanggal Lahir</td>
				<td>				
				<c:if test="${cmd.pemegang.lsre_id eq 1}">
					<spring:bind path="cmd.tertanggung.tgllhr_si">
						<script>inputDate('${status.expression}', '${status.value}', true, '', 9);</script>
						<font color="#CC3300">*</font></spring:bind>
					</c:if>
					<c:if test="${cmd.pemegang.lsre_id ne 1}">
					<spring:bind path="cmd.tertanggung.tgllhr_si">
						<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
						<font color="#CC3300">*</font></spring:bind>
					</c:if></td>
			</tr>
<tr>
				<td>b.</td>
				<td>Nama Anak 1</td>
				<td><spring:bind path="cmd.tertanggung.nama_anak1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="50"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				<font color="#CC3300">*</font></spring:bind></td>
				<td>Tanggal Lahir</td>
				<td>
					<c:if test="${cmd.pemegang.lsre_id eq 1}">
					<spring:bind path="cmd.tertanggung.tgllhr_anak1">
						<script>inputDate('${status.expression}', '${status.value}', true, '', 9);</script>
						<font color="#CC3300">*</font></spring:bind>
					</c:if>
					<c:if test="${cmd.pemegang.lsre_id ne 1}">
					<spring:bind path="cmd.tertanggung.tgllhr_anak1">
						<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
						<font color="#CC3300">*</font></spring:bind>
					</c:if>
				</td>
			</tr>
<tr>
				<td>c.</td>
				<td>Nama Anak 2</td>
				<td><spring:bind path="cmd.tertanggung.nama_anak2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="50"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				<font color="#CC3300">*</font></spring:bind></td>
				<td>Tanggal Lahir</td>
				<td>				
				<c:if test="${cmd.pemegang.lsre_id eq 1}">
					<spring:bind path="cmd.tertanggung.tgllhr_anak2">
						<script>inputDate('${status.expression}', '${status.value}', true, '', 9);</script>
						<font color="#CC3300">*</font></spring:bind>
					</c:if>
					<c:if test="${cmd.pemegang.lsre_id ne 1}">
					<spring:bind path="cmd.tertanggung.tgllhr_anak2">
						<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
						<font color="#CC3300">*</font></spring:bind>
					</c:if></td>
			</tr>	
<tr>
				<td>d.</td>
				<td>Nama Anak 3</td>
				<td><spring:bind path="cmd.tertanggung.nama_anak3">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="50"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				<font color="#CC3300">*</font></spring:bind></td>
				<td>Tanggal Lahir</td>
				<td>				
				<c:if test="${cmd.pemegang.lsre_id eq 1}">
					<spring:bind path="cmd.tertanggung.tgllhr_anak3">
						<script>inputDate('${status.expression}', '${status.value}', true, '', 9);</script>
						<font color="#CC3300">*</font></spring:bind>
					</c:if>
					<c:if test="${cmd.pemegang.lsre_id ne 1}">
					<spring:bind path="cmd.tertanggung.tgllhr_anak3">
						<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
						<font color="#CC3300">*</font></spring:bind>
					</c:if>
					</td>
			</tr>				
			<tr>
				<script type="text/javascript">
					function agamaChange(flag){					
						if(flag=='6'){
							
							document.frmParam.elements['tertanggung.mcl_agama'].readOnly = false;
						}else{
							document.frmParam.elements['tertanggung.mcl_agama'].value="";
							document.frmParam.elements['tertanggung.mcl_agama'].readOnly = true;
						}
					}
				</script>
				<td>7.</td>
				<td>Agama</td>
				<td colspan="3"><select name="tertanggung.lsag_id" onchange="agamaChange(this.value);">
					<c:forEach var="agama" items="${select_agama}" >
						<option
							<c:if test="${cmd.tertanggung.lsag_id eq agama.ID}"> SELECTED </c:if>
							value="${agama.ID}">${agama.AGAMA}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Lain - Lain, Sebutkan</td>
				<td colspan="3">					
					<spring:bind path="cmd.tertanggung.mcl_agama">
						<input type="text" name="${status.expression}"
							value="${status.value }" size="25" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
							class='input_error'
						</c:if>>
					</spring:bind>
				</td>
			</tr>
			<tr>
				<td>8.</td>
				<td>Pendidikan</td>
				<td colspan="3"><select name="tertanggung.lsed_id" >
					<c:forEach var="pendidikan" items="${select_pendidikan}">
						<option
							<c:if test="${cmd.tertanggung.lsed_id eq pendidikan.ID}"> SELECTED </c:if>
							value="${pendidikan.ID}">${pendidikan.PENDIDIKAN}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></td>
			</tr>
			<tr>
				<td rowspan="4">9.<br/>a.</td>
				<td rowspan="4">Alamat Rumah</td>
				<td rowspan="4"><spring:bind path="cmd.tertanggung.alamat_rumah">
					<textarea cols="33" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>${status.value }</textarea>
				</spring:bind><font color="#CC3300">*</font></td>
				<td>Kode Pos</td>
				<td><spring:bind path="cmd.tertanggung.kd_pos_rumah">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="10"\
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind></td>
			</tr>
			<tr>
				<td>Kota</td>
				<td>
				<spring:bind path="cmd.tertanggung.kota_rumah">
				<input type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
				<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
		    	<span id="indicator_rumah" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
			</spring:bind> 
				</td>
			</tr>
			<!--<tr>
				<td></td>
				<td></td>
			</tr>-->
			<tr>
				<td>No Telepon 1</td>
				<td><spring:bind path="cmd.tertanggung.area_code_rumah">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>><font color="#CC3300">*</font>
				</spring:bind> <spring:bind path="cmd.tertanggung.telpon_rumah">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind><font color="#CC3300">*</font></td>
			</tr>
			<tr>
				<td>No Telepon 2</td>
				<td><spring:bind path="cmd.tertanggung.area_code_rumah2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.tertanggung.telpon_rumah2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>No HandPhone 1</td>
				<td><spring:bind path="cmd.tertanggung.no_hp">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20" 
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind></td>
				<td>Email</td>
				<td><spring:bind path="cmd.tertanggung.email">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="40" 
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				
            <td>No HandPhone 2</td>
				<td><spring:bind path="cmd.tertanggung.no_hp2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td rowspan="5">b.</td>
				<td rowspan="5">Alamat Kantor</td>
				<td rowspan="5"><spring:bind path="cmd.tertanggung.alamat_kantor">
					<textarea cols="33" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>${status.value }</textarea>
				</spring:bind></td>
				<td>Kode Pos</td>
				<td><spring:bind path="cmd.tertanggung.kd_pos_kantor">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="10"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind></td>
			</tr>
			<tr>
				<td>Kota</td>
				<td>
					<spring:bind path="cmd.tertanggung.kota_kantor">
				<input type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
				<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
		    	<span id="indicator_kantor" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
			</spring:bind> 		
				</td>
			</tr>
			<!--<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>-->
			<tr>
				<td>No Telepon1</td>
				<td><spring:bind path="cmd.tertanggung.area_code_kantor">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.tertanggung.telpon_kantor">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind></td>
			</tr>
			<tr>
				<td>No Telepon 2</td>
				<td><spring:bind path="cmd.tertanggung.area_code_kantor2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.tertanggung.telpon_kantor2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind></td>
			</tr>
			<tr>
				<td>No Fax</td>
				<td><spring:bind path="cmd.tertanggung.area_code_fax">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.tertanggung.no_fax">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind></td>
			</tr>
			<tr>
				<td rowspan="5">c.</td>
				<td rowspan="5">Alamat Penagihan / Korespondensi</td>
				<td rowspan="5"><spring:bind path="cmd.addressbilling.msap_address">
					<textarea cols="34" rows="7" name="${status.expression }" onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); ">${status.value }</textarea>
				</spring:bind></td>
				<td>Kode Pos</td>
				<td><spring:bind path="cmd.addressbilling.msap_zip_code">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20">
				</spring:bind></td>
			</tr>
			<tr>
				<td>Kota</td>
				<td>
				<spring:bind path="cmd.addressbilling.kota_tgh">
					<input type="text" name="${status.expression}"
						value="${status.value }">
				</spring:bind>
				</td>
			</tr>
			<!--<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>-->
			<tr>
				<td>No Telepon1</td>
				<td><spring:bind path="cmd.addressbilling.msap_area_code1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4">
				</spring:bind> <spring:bind path="cmd.addressbilling.msap_phone1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20">
				</spring:bind></td>
			</tr>
			<tr>
				<td>No Telepon 2</td>
				<td><spring:bind path="cmd.addressbilling.msap_area_code2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4">
				</spring:bind> <spring:bind path="cmd.addressbilling.msap_phone2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20">
				</spring:bind></td>
			</tr>
			<tr>
				<td>No Telepon 3</td>
				<td><spring:bind path="cmd.addressbilling.msap_area_code3">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4">
				</spring:bind> <spring:bind path="cmd.addressbilling.msap_phone3">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20">
				</spring:bind></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>No HandPhone 1</td>
				<td><spring:bind path="cmd.addressbilling.no_hp">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind></td>
				<td>No Fax</td>
				<td><spring:bind path="cmd.addressbilling.msap_area_code_fax1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.addressbilling.msap_fax1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>No HandPhone 2</td>
				<td><spring:bind path="cmd.addressbilling.no_hp2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind></td>
				<td>Email</td>
				<td><spring:bind path="cmd.addressbilling.e_mail">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="40"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind></td>
			</tr>
			<tr>
				<td>10.</td>
				<td>Tujuan Membeli Asuransi</td>
				<td colspan="3"><select name="tertanggung.mkl_tujuan" >
					<c:forEach var="tujuan" items="${select_tujuan}">
						<option
							<c:if test="${cmd.tertanggung.mkl_tujuan eq tujuan.ID}"> SELECTED </c:if>
							value="${tujuan.ID}">${tujuan.TUJUAN}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Lain-Lain, Jelaskan</td>
				<td colspan="3"><spring:bind path="cmd.tertanggung.tujuana">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind></td>
			</tr>
			<tr>
				<td>11.</td>
				<td>Perkiraan Penghasilan Kotor Per Tahun</td>
				<td colspan="3"><select name="tertanggung.mkl_penghasilan"
					>
					<c:forEach var="penghasilan" items="${select_penghasilan}">
						<option
							<c:if test="${cmd.tertanggung.mkl_penghasilan eq penghasilan.ID}"> SELECTED </c:if>
							value="${penghasilan.ID}">${penghasilan.PENGHASILAN}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></td>
			</tr>
			<tr>
				<td>12.</td>
				<td>Sumber Pendanaan Pembelian Asuransi</td>
				<td colspan="3"><select name="tertanggung.mkl_pendanaan"
					>
					<c:forEach var="dana" items="${select_dana}">
						<option
							<c:if test="${cmd.tertanggung.mkl_pendanaan eq dana.ID}"> SELECTED </c:if>
							value="${dana.ID}">${dana.DANA}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Lainnya, Jelaskan</td>
				<td colspan="3"><spring:bind path="cmd.tertanggung.danaa">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind></td>
			</tr>
			<tr>
				<td>13.</td>
				<td>Sumber Penghasilan</td>
				<td colspan="3"><select name="tertanggung.mkl_smbr_penghasilan" >
					<c:forEach var="dana" items="${select_dana}">
						<option
							<c:if test="${cmd.tertanggung.mkl_smbr_penghasilan eq dana.ID}"> SELECTED </c:if>
							value="${dana.ID}">${dana.DANA}</option>
					</c:forEach>
				</select>
				 <font color="#CC3300">*</font></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Lainnya, Jelaskan</td>
				<td colspan="3"><spring:bind path="cmd.tertanggung.shasil">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="100"
						<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind></td>
			</tr>
			<tr>
				<td>13.</td>
				<td>Klasifikasi Pekerjaan</td>
				<td colspan="3"><select name="tertanggung.mkl_kerja" >
					<c:forEach var="kerja" items="${select_pekerjaan}">
						<option
							<c:if test="${cmd.tertanggung.mkl_kerja eq kerja.ID}"> SELECTED </c:if>
							value="${kerja.ID}">${kerja.KLASIFIKASI}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Lain-Lain, Sebutkan</td>
				<td colspan="3"><spring:bind path="cmd.tertanggung.kerjaa">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
					Jabatan
					<!--  <br><span class="info">(Hanya diisi apabila Klasifikasi Pekerjaan sebagai KARYAWAN)</span> -->
				</td>
				<td colspan="3"><spring:bind path="cmd.tertanggung.kerjab">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind></td>
			</tr>
			<tr>
				<td>14.</td>
				<td>Klasifikasi Bidang Industri</td>
				<td colspan="3"><select name="tertanggung.mkl_industri"
					>
					<c:forEach var="industri" items="${select_industri}">
						<option
							<c:if test="${cmd.tertanggung.mkl_industri eq industri.ID}"> SELECTED </c:if>
							value="${industri.ID}">${industri.BIDANG}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Lainnya, Sebutkan</td>
				<td colspan="3"><spring:bind path="cmd.tertanggung.industria">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
					<c:if test="${ not empty status.errorMessage}">
						class='input_error'
					</c:if>>
				</spring:bind></td>
			</tr>
			<tr>
				<td class="subtitle" colspan="5">&nbsp;</td>
			</tr>
			  <tr> 
            <td colspan="5" style="color: #CC3300;font-weight: bold">Note : * Wajib diisi</td>
          </tr>
			<tr>
				<td colspan="5" style="text-align: center;">
				<input type="hidden" name="hal" value="${halaman}">
				<spring:bind path="cmd.pemegang.indeks_halaman">
					<input type="hidden" name="${status.expression}"
						value="${halaman-1}" size="30">
				</spring:bind>				
				<input type="submit" name="_target${halaman-1}"
					value="Prev &laquo;" 
					accesskey="P" onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="submit"
					name="_target${halaman+1}" value="Next &raquo;" onClick="next()" 
					accesskey="N" onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
					<input
					type="hidden" name="_page" value="${halaman}">
				</td>
			</tr>
		</table>
		
		</td>
	</tr>
</table>
</form>
<ajax:autocomplete
				  source="tertanggung.kota_rumah"
				  target="tertanggung.kota_rumah"
				  baseUrl="${path}/servlet/autocomplete?s=tertanggung.kota_rumah&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_rumah"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />

<ajax:autocomplete
				  source="tertanggung.kota_kantor"
				  target="tertanggung.kota_kantor"
				  baseUrl="${path}/servlet/autocomplete?s=tertanggung.kota_kantor&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_kantor"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />
<script type="text/javascript">	
	agamaChange('${cmd.tertanggung.lsag_id}');
</script>
</body>
<%@ include file="/include/page/footer.jsp"%>