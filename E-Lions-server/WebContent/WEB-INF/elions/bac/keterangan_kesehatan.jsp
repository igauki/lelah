<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>

<script>

	function Body_onload() {
		
		if (document.frmParam.msnm_keterangan.value == 'input' || document.frmParam.msnm_keterangan.value == 'edit')
		{
			alert('Data Kesehatan berhasil disimpan');
			document.frmParam.msnm_keterangan.value = "";
		}else{
			
		}
		if (document.frmParam.msnm_tumor.value == '1')
		{
			document.frmParam.msnm_tumor1.checked = true;
		}else{
			document.frmParam.msnm_tumor1.checked = false;
		}
		if (document.frmParam.msnm_asma.value == '1')
		{
			document.frmParam.msnm_asma1.checked = true;
		}else{
			document.frmParam.msnm_asma1.checked = false;
		}
		if (document.frmParam.msnm_diabetes.value == '1')
		{
			document.frmParam.msnm_diabetes1.checked = true;
		}else{
			document.frmParam.msnm_diabetes1.checked = false;
		}
		if (document.frmParam.msnm_lambung.value == '1')
		{
			document.frmParam.msnm_lambung1.checked = true;
		}else{
			document.frmParam.msnm_lambung1.checked = false;
		}
		if (document.frmParam.msnm_jantung.value == '1')
		{
			document.frmParam.msnm_jantung1.checked = true;
		}else{
			document.frmParam.msnm_jantung1.checked = false;
		}
		if (document.frmParam.msnm_pingsan.value == '1')
		{
			document.frmParam.msnm_pingsan1.checked = true;
		}else{
			document.frmParam.msnm_pingsan1.checked = false;
		}
		if (document.frmParam.msnm_darah_tinggi.value == '1')
		{
			document.frmParam.msnm_darah_tinggi1.checked = true;
		}else{
			document.frmParam.msnm_darah_tinggi1.checked = false;
		}
		if (document.frmParam.msnm_kelainan.value == '1')
		{
			document.frmParam.msnm_kelainan1.checked = true;
		}else{
			document.frmParam.msnm_kelainan1.checked = false;
		}
		if (document.frmParam.msnm_gila.value == '1')
		{
			document.frmParam.msnm_gila1.checked = true;
		}else{
			document.frmParam.msnm_gila1.checked = false;
		}
		if (document.frmParam.msnm_lain.value == '1')
		{
			document.frmParam.msnm_lain1.checked = true;
		}else{
			document.frmParam.msnm_lain1.checked = false;
		}
	
	}	
	
	function sub()
	{

		if(document.frmParam.msnm_tumor1.checked==true){
			document.frmParam.msnm_tumor.value = '1';
		}else{
			document.frmParam.msnm_tumor.value = '0';
		}
		
		if(document.frmParam.msnm_asma1.checked==true){
			document.frmParam.msnm_asma.value = '1';
		}else{
			document.frmParam.msnm_asma.value = '0';
		}
		
		if(document.frmParam.msnm_diabetes1.checked==true){
			document.frmParam.msnm_diabetes.value = '1';
		}else{
			document.frmParam.msnm_diabetes.value = '0';
		}
		
		if(document.frmParam.msnm_lambung1.checked==true){
			document.frmParam.msnm_lambung.value = '1';
		}else{
			document.frmParam.msnm_lambung.value = '0';
		}
		
		if(document.frmParam.msnm_jantung1.checked==true){
			document.frmParam.msnm_jantung.value = '1';
		}else{
			document.frmParam.msnm_jantung.value = '0';
		}
		
		if(document.frmParam.msnm_pingsan1.checked==true){
			document.frmParam.msnm_pingsan.value = '1';
		}else{
			document.frmParam.msnm_pingsan.value = '0';
		}
		
		if(document.frmParam.msnm_darah_tinggi1.checked==true){
			document.frmParam.msnm_darah_tinggi.value = '1';
		}else{
			document.frmParam.msnm_darah_tinggi.value = '0';
		}

		if(document.frmParam.msnm_kelainan1.checked==true){
			document.frmParam.msnm_kelainan.value = '1';
		}else{
			document.frmParam.msnm_kelainan.value = '0';
		}
		
		if(document.frmParam.msnm_gila1.checked==true){
			document.frmParam.msnm_gila.value = '1';
		}else{
			document.frmParam.msnm_gila.value = '0';
		}
		
		if(document.frmParam.msnm_lain1.checked==true){
			document.frmParam.msnm_lain.value = '1';
		}else{
			document.frmParam.msnm_lain.value = '0';
		}

		frmParam.submit();
	}
	
	function cek_onClick(karakter)
	{
		
		switch (karakter) {
			case "tumor" :
				if(document.frmParam.msnm_tumor1.checked==true){
					document.frmParam.msnm_tumor.value = '1';
				}else{
					document.frmParam.msnm_tumor.value = '0';
				}
			break;	
			case "asma" :
				if(document.frmParam.msnm_asma1.checked==true){
					document.frmParam.msnm_asma.value = '1';
				}else{
					document.frmParam.msnm_asma.value = '0';
				}
			break;
			case "diabetes" :
				if(document.frmParam.msnm_diabetes1.checked==true){
					document.frmParam.msnm_diabetes.value = '1';
				}else{
					document.frmParam.msnm_diabetes.value = '0';
				}
			break;
			case "lambung" :
				if(document.frmParam.msnm_lambung1.checked==true){
					document.frmParam.msnm_lambung.value = '1';
				}else{
					document.frmParam.msnm_lambung.value = '0';
				}
			break;
			case "jantung" :
				if(document.frmParam.msnm_jantung1.checked==true){
					document.frmParam.msnm_jantung.value = '1';
				}else{
					document.frmParam.msnm_jantung.value = '0';
				}
			break;
			case "pingsan" :
				if(document.frmParam.msnm_pingsan1.checked==true){
					document.frmParam.msnm_pingsan.value = '1';
				}else{
					document.frmParam.msnm_pingsan.value = '0';
				}
			break;
			case "darahtinggi" :
				if(document.frmParam.msnm_darah_tinggi1.checked==true){
					document.frmParam.msnm_darah_tinggi.value = '1';
				}else{
					document.frmParam.msnm_darah_tinggi.value = '0';
				}
			break;
			case "kelainan" :
				if(document.frmParam.msnm_kelainan1.checked==true){
					document.frmParam.msnm_kelainan.value = '1';
				}else{
					document.frmParam.msnm_kelainan.value = '0';
				}
			break;
			case "gila" :
				if(document.frmParam.msnm_gila1.checked==true){
					document.frmParam.msnm_gila.value = '1';
				}else{
					document.frmParam.msnm_gila.value = '0';
				}
			break;
			case "lain" :
				if(document.frmParam.msnm_lain1.checked==true){
					document.frmParam.msnm_lain.value = '1';
				}else{
					document.frmParam.msnm_lain.value = '0';
				}
			break;
		}			
		
	}
</script>
<script language="JavaScript">
<!--
	if (!document.layers&&!document.all&&!document.getElementById)
		event="test"
		
	function showtip(current,e,text){
	
		if (document.all||document.getElementById){
			thetitle=text.split('<br>')
			if (thetitle.length>1){
				thetitles=''
				
			for (i=0;i<thetitle.length;i++)
				thetitles+=thetitle[i]
				
			current.title=thetitles
		}
		else
			current.title=text
	}
	
	else if (document.layers){
				document.tooltip.document.write('<layer bgColor="white" style="border:1px solid black;font-size:13px;">'+text+'</layer>')
				document.tooltip.document.close()
				document.tooltip.left=e.pageX+5
				document.tooltip.top=e.pageY+5
				document.tooltip.visibility="show"
			}
	}
	
	function hidetip(){
		if (document.layers)
			document.tooltip.visibility="hidden"
	}

// -->
</script>

<body onLoad="Body_onload();" >
<XML ID=xmlData></XML>
	<form name="frmParam" method="post">
<table class="entry2">
	<tr>
		<td width="67%" align="left" valign="top">
		<spring:bind path="cmd.*">
			<c:if test="${not empty status.errorMessages}">
				<div id="error">ERROR:<br>
				<c:forEach var="error" items="${status.errorMessages}">
	 - <c:out value="${error}" escapeXml="false" />
					<br />
				</c:forEach></div>
			</c:if>
		</spring:bind>	
		<table class="entry">

			</tr>
			<tr>
				<th colspan="3" class="subtitle">KETERANGAN SEHAT CALON TERTANGGUNG
				<spring:bind path="cmd.msnm_keterangan">
					<input type="hidden" name="${status.expression}"
						value="${status.value }" size="42" tabindex="5" maxlength="50"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind>
				</th>
			</tr>
			<tr>
				<th width="18">&nbsp;</th>
				<th width="287">
				NO SPAJ </th>
				<th width="505">
				<input type="reg_spaj" value ="<elions:spaj nomor='${cmd.reg_spaj}'/>" style='background-color :#D4D4D4' readOnly>
			</th>
			</tr>
			<tr>
				<th width="18">1.</th>
				<th width="287">
				Apakah Anda sekarang dalam keadaan sehat? </th>
				<th width="505">
				<spring:bind path="cmd.msnm_sehat">
					<label for="ya"> <input type="radio" class=noBorder
						name="${status.expression}" value="1"
						<c:if test="${cmd.msnm_sehat eq 1 or cmd.msnm_sehat eq null}"> 
									checked</c:if>
						tabindex="11" id="ya">YA </label>&nbsp;&nbsp;&nbsp;&nbsp;
					<label for="tidak"> <input type="radio" class=noBorder
						name="${status.expression}" value="0"
						<c:if test="${cmd.msnm_sehat eq 0 }"> 
									checked</c:if>
						tabindex="12" id="tidak">TIDAK <font color="#CC3300">*</font></label>
				</spring:bind> 
			</th>
			</tr>
			<tr>
				<th> </th>
				<th>Jika "Tidak", Jelaskan</th>
				<th >
				<spring:bind path="cmd.msnm_alasan_sehat">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" tabindex="5" maxlength="50"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th width="18">2 a.</th>
				<th width="287">
				Apakah dalam 5 tahun terakhir Anda dirawat di rumah sakit / diperiksa dokter ? </th>
				<th width="505">
				<spring:bind path="cmd.msnm_perawatan">
					<label for="ya"> <input type="radio" class=noBorder
						name="${status.expression}" value="1"
						<c:if test="${cmd.msnm_perawatan eq 1 }"> 
									checked</c:if>
						tabindex="11" id="ya">YA </label>
						&nbsp;&nbsp;&nbsp;&nbsp;
					<label for="tidak"> <input type="radio" class=noBorder
						name="${status.expression}" value="0"
						<c:if test="${cmd.msnm_perawatan eq 0 or cmd.msnm_perawatan eq null}"> 
									checked</c:if>
						tabindex="12" id="tidak">TIDAK <font color="#CC3300">*</font></label>
				</spring:bind> 
							</th>
			</tr>
						<tr>
				<th width="18">  b.</th>
				<th width="287">
				Apakah dalam 5 tahun terakhir Anda dioperasi / dianjurkan untuk dioperasi ? </th>
				<th width="505">
				<spring:bind path="cmd.msnm_operasi">
					<label for="ya"> <input type="radio" class=noBorder
						name="${status.expression}" value="1"
						<c:if test="${cmd.msnm_operasi eq 1 }"> 
									checked</c:if>
						tabindex="11" id="ya">YA </label>
						&nbsp;&nbsp;&nbsp;&nbsp;
					<label for="tidak"> <input type="radio" class=noBorder
						name="${status.expression}" value="0"
						<c:if test="${cmd.msnm_operasi eq 0 or cmd.msnm_operasi eq null}"> 
									checked</c:if>
						tabindex="12" id="tidak">TIDAK <font color="#CC3300">*</font></label>
				</spring:bind> 
							</th>
			</tr>
			<tr>
				<th width="18">3 a.</th>
				<th width="287">
				Apakah berat badan Anda berubah dalam 12 bulan terakhir ini ? </th>
				<th width="505">
				<spring:bind path="cmd.msnm_berubah">
					<label for="ya"> <input type="radio" class=noBorder
						name="${status.expression}" value="1"
						<c:if test="${cmd.msnm_berubah eq 1 }"> 
									checked</c:if>
						tabindex="11" id="ya">YA </label>
						&nbsp;&nbsp;&nbsp;&nbsp;
					<label for="tidak"> <input type="radio" class=noBorder
						name="${status.expression}" value="0"
						<c:if test="${cmd.msnm_berubah eq 0 or cmd.msnm_berubah eq null}"> 
									checked</c:if>
						tabindex="12" id="tidak">TIDAK <font color="#CC3300">*</font></label>
				</spring:bind> 
							</th>
			</tr>
			<tr>
				<th width="18">&nbsp;</th>
				<th width="287">
				Turun / Naik ? </th>
				<th width="505">
				<spring:bind path="cmd.keadaan">
					<select name="${status.expression }" 
				  <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
	                <option value="0" <c:if test="${cmd.keadaan eq \"0\"}">
						selected
						</c:if>>None</option>
	                <option value="1" <c:if test="${cmd.keadaan eq \"1\"}">
						selected
						</c:if>>TURUN</option>
	                <option value="2" <c:if test="${cmd.keadaan eq \"2\"}">
						selected
						</c:if>>NAIK</option>
	              </select>
				</spring:bind>
				</th>
			</tr>
			<tr>
				<th width="18">&nbsp;</th>
				<th width="287">
				Banyaknya perubahan berat dalam  KG : </th>
				<th width="505">
				 <spring:bind path="cmd.msnm_banyak">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" tabindex="5" maxlength="5"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind>
							</th>
			</tr>
			<tr>
				<th width="18">&nbsp;</th>
				<th width="287">
				Jika "Ya" , Jelaskan </th>
				<th width="505">
				<spring:bind path="cmd.msnm_alasan_berat">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" tabindex="5" maxlength="50"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind>
							</th>
			</tr>
			<tr>
				<th width="18"> b.</th>
				<th width="287">
				Berat badan dan tinggi badan ?</th>
				<th width="505">
				<spring:bind path="cmd.msnm_berat"> Berat Badan  : 
				<input type="text" name="${status.expression}"
						value="${status.value }" size="5" tabindex="5" maxlength="50"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> 
				<br>
				<spring:bind path="cmd.msnm_tinggi"> Tinggi Badan : 
				<input type="text" name="${status.expression}"
						value="${status.value }" size="5" tabindex="5" maxlength="50"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind>
							</th>
			</tr>
			<tr>
				<th width="18">4.</th>
				<th width="287">
				Sebutkan hobi / jenis olahraga yang sering anda lakukan ? </th>
				<th width="505">
				<spring:bind path="cmd.msnm_hobi">
					<textarea cols="40" rows="7" name="${status.expression }"
						tabindex="17" onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
						</c:if>
						>${status.value }</textarea>
				<font color="#CC3300">*</font></spring:bind>
							</th>
			</tr>
		<tr>
				<th width="18">5 a.</th>
				<th width="287">
				Apakah Anda pernah menderita cacat ? </th>
				<th width="505">
				<spring:bind path="cmd.msnm_cacat">
					<label for="ya"> <input type="radio" class=noBorder
						name="${status.expression}" value="1"
						<c:if test="${cmd.msnm_cacat eq 1 }"> 
									checked</c:if>
						tabindex="11" id="ya">YA </label>
						&nbsp;&nbsp;&nbsp;&nbsp;
					<label for="tidak"> <input type="radio" class=noBorder
						name="${status.expression}" value="0"
						<c:if test="${cmd.msnm_cacat eq 0 or cmd.msnm_cacat eq null}"> 
									checked</c:if>
						tabindex="12" id="tidak">TIDAK <font color="#CC3300">*</font></label>
				</spring:bind> 
							</th>
			</tr>
			<tr>
				<th> </th>
				<th>Jika "Ya", Jelaskan</th>
				<th >
				<spring:bind path="cmd.msnm_alasan_cacat">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" tabindex="5" maxlength="50"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
		<tr>
				<th width="18" valign="top"> b.</th>
				<th width="287" valign="top">
				Apakah Anda pernah menderita penyakit di bawah ini ? </th>
				<th width="505">
				<input type="checkbox" name="msnm_tumor1" class="noBorder" 
						value="${cmd.msnm_tumor}"  size="30"  tabindex="3" onClick="cek_onClick('tumor');"
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> Tumor/Kanker
              <spring:bind path="cmd.msnm_tumor"> 
              <input type="hidden" name="${status.expression}"
						value="${cmd.msnm_tumor}"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind> 
				<input type="checkbox" name="msnm_asma1" class="noBorder" 
						value="${cmd.msnm_asma}"  size="30"  tabindex="3" onClick="cek_onClick('asma');"
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> TBC/Asma
              <spring:bind path="cmd.msnm_asma"> 
              <input type="hidden" name="${status.expression}"
						value="${cmd.msnm_asma}"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind> 
              <input type="checkbox" name="msnm_diabetes1" class="noBorder" onClick="cek_onClick('diabetes');" 
						value="${cmd.msnm_diabetes}"  size="30"  tabindex="3"
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> Kencing Manis
					<br>
              <spring:bind path="cmd.msnm_diabetes"> 
              <input type="hidden" name="${status.expression}"
						value="${cmd.msnm_diabetes}"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind>
              <input type="checkbox" name="msnm_lambung1" class="noBorder" onClick="cek_onClick('lambung');"
						value="${cmd.msnm_lambung}"  size="30"  tabindex="3"
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> Hati/Ginjal
              <spring:bind path="cmd.msnm_lambung"> 
              <input type="hidden" name="${status.expression}"
						value="${cmd.msnm_lambung}"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input type="checkbox" name="msnm_jantung1" class="noBorder" onClick="cek_onClick('jantung');"
						value="${cmd.msnm_jantung}"  size="30"  tabindex="3"
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> Jantung
              <spring:bind path="cmd.msnm_jantung"> 
              <input type="hidden" name="${status.expression}"
						value="${cmd.msnm_jantung}"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <input type="checkbox" name="msnm_pingsan1" class="noBorder" onClick="cek_onClick('pingsan');"
						value="${cmd.msnm_pingsan}"  size="30"  tabindex="3"
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> Stroke
              <spring:bind path="cmd.msnm_pingsan"> 
              <input type="hidden" name="${status.expression}"
						value="${cmd.msnm_pingsan}"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind>
              <br>
              <input type="checkbox" name="msnm_darah_tinggi1" class="noBorder" onClick="cek_onClick('darahtinggi');"
						value="${cmd.msnm_darah_tinggi}"  size="30"  tabindex="3"
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> Tekanan darah tinggi
              <spring:bind path="cmd.msnm_darah_tinggi"> 
              <input type="hidden" name="${status.expression}"
						value="${cmd.msnm_darah_tinggi}"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input type="checkbox" name="msnm_kelainan1" class="noBorder" onClick="cek_onClick('kelainan');"
						value="${cmd.msnm_kelainan}"  size="30"  tabindex="3"
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> Kelainan bawaan
              <spring:bind path="cmd.msnm_kelainan"> 
              <input type="hidden" name="${status.expression}"
						value="${cmd.msnm_kelainan}"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind>
              <br>
              <input type="checkbox" name="msnm_gila1" class="noBorder" onClick="cek_onClick('gila');"
						value="${cmd.msnm_gila}"  size="30"  tabindex="3"
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> Gangguan jiwa
              <spring:bind path="cmd.msnm_gila"> 
              <input type="hidden" name="${status.expression}"
						value="${cmd.msnm_gila}"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input type="checkbox" name="msnm_lain1" class="noBorder" onClick="cek_onClick('lain');"
						value="${cmd.msnm_lain}"  size="30"  tabindex="3"
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> Penyakit lainnya
              <spring:bind path="cmd.msnm_lain"> 
              <input type="hidden" name="${status.expression}"
						value="${cmd.msnm_lain}"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind>
            	</th>
			</tr>	
<tr>
				<th width="18">&nbsp;</th>
				<th width="287">
				Jika "Ya" , Jelaskan </th>
				<th width="505">
				<spring:bind path="cmd.msnm_desc_sakit">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" tabindex="5" maxlength="50"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind>
							</th>
			</tr>					
            <tr> 
            <td colspan="3" > 
			 <p align="center"><input type="submit" value="Save" name="save"  onclick="sub();"></p>
			</td>
          </tr>	
            <tr> 
            <td colspan="3" > 
			 <p align="left"><b> <font face="Verdana" size="2" color="#CC3300">Note : * Wajib diisi</font></b></p>
			</td>
          </tr>	
			
		</table>

		</td>
	</tr>
</table>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>
