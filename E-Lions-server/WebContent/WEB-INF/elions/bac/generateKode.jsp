<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
	<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta NAME="Description" CONTENT="EkaLife">
	<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<meta HTTP-EQUIV="Expires" CONTENT="-1">
	<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
	<link href="${path}/include/image/eas.ico" rel="shortcut icon">
	<!--  -->
	<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
	<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
	<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
	<!--  -->
	<script type="text/javascript" src="${path }/include/js/default.js"></script>
	<!-- Ajax Related -->
	<script type="text/javascript" src="${path}/include/js/ajax.js"></script>
	<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script>
	<script type="text/javascript" src="${path }/dwr/engine.js"></script>
	<script type="text/javascript" src="${path }/dwr/util.js"></script>
	<script type="text/javascript">
		hideLoadingMessage();
		
		function waitPreloadPage() { //DOM
			<c:if test='${not empty pesan}'>
	            alert('${pesan}');
	        </c:if>
			
			//calendar.set("tgl_lahir");
			
			if (document.getElementById){
				document.getElementById('prepage').style.visibility='hidden';
			}else{
				if (document.layers){ //NS4
					document.prepage.visibility = 'hidden';
				}
				else { //IE4
					document.all.prepage.style.visibility = 'hidden';
				}
			}
		}
		// End -->
	</script>
</head>

<body onload="waitPreloadPage();" >
<div id="prepage" style="position:absolute; font-family:arial; font-size:12; left:0px; top:0px; background-color:yellow; layer-background-color:white;"> 
	<table border="2" bordercolor="red"><tr><td style="color: red;"><b>Loading ... ... Please wait!</b></td></tr></table>
</div>
<form method="post" name="formpost" id="formpost" action="">
	<table class="entry">
		<tr>
			<td>
				<fieldset>
					<legend style="font-size: 12px; color: black; font-weight: bolder;">Generate Kode</legend>
					<table class="entry" style="text-transform: uppercase;">
						<tr>
							<th style="width: 150px;">Kode</th>
							<td style="text-align: left;"><input type="text" name="kode" id="kode" readonly="readonly" value="${kode }"></td>
						</tr>
						<tr>
							<td colspan="2">
								<input type="Submit" name="btnGenerate" class="button" value="Generate">
								<input type="Submit" name="btnSimpan" class="button" value="Simpan">
							</td>
						</tr>
					</table>
				</fieldset>
			</td>
		</tr>
	</table>
</form>
</body>
</html>	
