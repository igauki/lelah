<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<meta HTTP-EQUIV="Expires" CONTENT="-1">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link href="${path}/include/image/eas.ico" rel="shortcut icon">
		<!--  -->
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<!--  -->
		<script type="text/javascript" src="${path }/include/js/default.js"></script>
		<!-- Ajax Related -->
		<script type="text/javascript" src="${path}/include/js/ajax.js"></script>
		<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script>
		<script type="text/javascript" src="${path }/dwr/engine.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>
		<script type="text/javascript">
			hideLoadingMessage();
		</script>
	</head>
	<body onload="setupPanes('container1','tab1');" style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Daftar Input Pencairan</a>
				</li>
			</ul>

			<div class="tab-panes">

				<div id="pane1" class="panes">
					<form:form commandName="cmd" name="formpost">
						<fieldset style="text-align: left;">
							<legend>Parameter</legend>
							<table class="entry2" style="width: auto;">
								<tr>
									<th nowrap="nowrap" style="width: 200px;">Posisi</th>
									<td>
										<select name="posisi" id="posisi">
											<c:forEach items="${daftarPosisi}" var="d">
												<option value="${d.key}"
												<c:if test="${posisi eq d.key}"> selected </c:if>
												>${d.value}</option>
											</c:forEach>
										</select>
										<input type="button" name="cari" value="Cari" onclick="window.location='${path}/bac/daftarcair.htm?posisi='+document.getElementById('posisi').value;">
									</td>
								</tr>
							</table>
						</fieldset>	
						
						<table class="displaytag">
							<thead>
								<tr>
									<th></th>
									<th>Cabang</th>
									<th>No Polis</th>
									<th>Produk</th>
									<th>Pemegang Polis</th>
									<th>Premi Ke</th>
									<th>Rollover</th>
									<th>Kurs</th>
									<th>(%)</th>
									<th>Tgl Deposit</th>
									<th>Tgl Jatuh Tempo</th>
									<th>Tgl Cair</th>
									<c:if test="${not empty boleh and posisi eq 2}">
										<th style="background-color: #FFFFCC;">Rubah Tgl Cair</th>
										<th style="background-color: #FFFFCC;">Keterangan Perubahan</th>
									</c:if>
									<th>Deposit</th>
									<th>Bunga</th>
									<th>Tambahan Hari</th>
									<th>Tambahan Manfaat</th>
									<th>Total Bunga</th>
									<th>Bonus Performance</th>
									<th>Total Nilai Tunai</th>
									<th>Tgl Input</th>
									<th>User Input</th>
									<th>Tgl Proses</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${cmd.daftarPremi}" var="p" varStatus="st">
									<c:choose>
										<c:when test="${not empty p.warna}"><tr style="background-color: #${p.warna};"></c:when>
										<c:otherwise><tr></c:otherwise>
									</c:choose>
										<td style="white-space: nowrap;"><form:checkbox cssClass="noBorder" path="daftarPremi[${st.index}].centang" value="true"/></td>
										<td style="white-space: nowrap;">${p.nama_cabang}</td>
										<td style="white-space: nowrap;">${p.mspo_policy_no_format}</td>
										<td style="white-space: nowrap;">${p.lsdbs_name}</td>
										<td style="white-space: nowrap;">${p.pemegang}</td>
										<td style="white-space: nowrap;">
											<c:choose>
												<c:when test="${p.mpc_tu_ke gt 0}">Premi Top-Up Ke ${p.mpc_tu_ke}</c:when>
												<c:otherwise>Premi Pokok</c:otherwise>
											</c:choose>
										</td>
										<td style="white-space: nowrap;">
											<c:choose>
												<c:when test="${p.mpc_ro eq 1}">ROLLOVER ALL</c:when>
												<c:when test="${p.mpc_ro eq 2}">ROLLOVER PREMI</c:when>
												<c:when test="${p.mpc_ro eq 3}">AUTOBREAK</c:when>
											</c:choose>
										</td>
										<td style="white-space: nowrap;">${p.lku_symbol}</td>
										<td style="white-space: nowrap;"><fmt:formatNumber value="${p.mpc_rate}" maxFractionDigits="2" minFractionDigits="0" />%</td>
										<td style="white-space: nowrap;"><fmt:formatDate value="${p.mpc_bdate}" pattern="dd/MM/yyyy"/></td>
										<td style="white-space: nowrap;"><fmt:formatDate value="${p.mpc_edate}" pattern="dd/MM/yyyy"/></td>
										<td style="white-space: nowrap;"><fmt:formatDate value="${p.mpc_cair}" pattern="dd/MM/yyyy"/></td>
										 <c:if test="${not empty boleh and posisi eq 2}">
											<td style="white-space: nowrap;">
												<spring:bind path="cmd.daftarPremi[${st.index}].mpc_cair_baru">
													<script>inputDate('${status.expression}', '${status.value}', false);</script>
												</spring:bind>
											</td>										
											<td style="white-space: nowrap;"><form:input path="daftarPremi[${st.index}].mpc_desc" cssErrorClass="inpError"/></td>
										</c:if>
										<td style="white-space: nowrap;"><fmt:formatNumber value="${p.mpc_premi}" maxFractionDigits="2" minFractionDigits="2" /></td>
										<td style="white-space: nowrap;"><fmt:formatNumber value="${p.mpc_bunga}" maxFractionDigits="2" minFractionDigits="2" /></td>
										<td style="white-space: nowrap;">${p.mpc_hari} Hari</td>
										<td style="white-space: nowrap;"><fmt:formatNumber value="${p.mpc_tambah - p.mpc_kurang}" maxFractionDigits="2" minFractionDigits="2" /></td>
										<td style="white-space: nowrap;"><fmt:formatNumber value="${p.mpc_bunga + p.mpc_tambah - p.mpc_kurang}" maxFractionDigits="2" minFractionDigits="2" /></td>
										<td style="white-space: nowrap;"><fmt:formatNumber value="${p.mpc_bp}" maxFractionDigits="2" minFractionDigits="2" /></td>
										<td style="white-space: nowrap;"><fmt:formatNumber value="${p.mpc_premi + p.total_bunga}" maxFractionDigits="2" minFractionDigits="2" /></td>
										<td style="white-space: nowrap;"><fmt:formatDate value="${p.tgl_input}" pattern="dd/MM/yyyy"/></td>
										<td style="white-space: nowrap;">${p.lus_full_name}</td>
										<td style="white-space: nowrap;"><fmt:formatDate value="${p.tgl_proses}" pattern="dd/MM/yyyy"/></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<div id="success" align="left" style="text-align: left; text-transform: none;">
							<strong>INFORMASI:</strong>
							<br/>- Warna Merah menandakan Polis sudah lewat / sama dengan Tanggal Jatuh Tempo
							<br/>- Posisi 1 : Permohonan Pencairan Polis sudah diinput.
							<br/>- Posisi 2 : Permohonan Pencairan Polis sedang dalam proses approval oleh Kantor Pusat BSM.
							<br/>- Posisi 3 : Permohonan Pencairan Polis sedang dalam proses oleh AJS.
							<br/>- Posisi 5 : Permohonan Pencairan Polis sudah selesai diproses oleh AJS, dan akan muncul dalam REPORT DAFTAR PEMBAYARAN.
						</div>
						<br/>
						<c:if test="${posisi lt 3}">
							<input type="submit" name="transfer" value="Transfer" onclick="return confirm('Lakukan transfer untuk data yang dipilih? \nData dengan Jatuh Tempo T-2 keatas akan ditransfer ke Kantor Pusat, lainnya akan ditransfer langsung ke AJS untuk diproses.');">
						</c:if>
						<spring:hasBindErrors name="cmd">
							<div id="error">
								<form:errors path="*" delimiter="<br>" />
							</div>
						</spring:hasBindErrors>
						
					</form:form>
				</div>
			</div>
			
		</div>

	</body>
	<script>
		<c:if test="${not empty param.pesan}">
			alert('${param.pesan}');
		</c:if>
	</script>
</html>