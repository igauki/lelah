<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<meta HTTP-EQUIV="Expires" CONTENT="-1">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link href="${path}/include/image/eas.ico" rel="shortcut icon">
		<!--  -->
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<!--  -->
		<script type="text/javascript" src="${path }/include/js/default.js"></script>
		<!-- Ajax Related -->
		<script type="text/javascript" src="${path}/include/js/ajax.js"></script>
		<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script>
		<script type="text/javascript" src="${path }/dwr/engine.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>
		<script type="text/javascript">
			hideLoadingMessage();
			
			function tampil(pormat){
			
				asdf = '';
			
				if(document.formpost.tanggal[0].checked) asdf = 'mpc_bdate';
				else if(document.formpost.tanggal[1].checked) asdf = 'mpc_edate';
				else if(document.formpost.tanggal[2].checked) asdf = 'mpc_cair';
				else if(document.formpost.tanggal[3].checked) asdf = 'tgl_input';
			
				var a = '${path}/report/bac.'+pormat+'?window=daftar_pembayaran_report' + 
					'&produk=' + document.formpost.produk.value +
					'&cabang=' + document.formpost.cabang.value +
					'&tanggal=' + asdf +
					'&cnc=' + document.formpost.cnc.value +
					'&startDate=' + document.formpost.startDate.value +
					'&endDate=' + document.formpost.endDate.value;
				popWin(a, 768, 1024);
			}
			
		</script>
	</head>
	<body onload="setupPanes('container1','tab1');" style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Daftar Pembayaran</a>
				</li>
			</ul>

			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form method="post" name="formpost">
						<fieldset style="text-align: left;">
							<legend>Parameter</legend>
							<table class="entry2" style="width: auto;">
								<tr>
									<th nowrap="nowrap" style="width: 120px;">Jenis</th>
									<th class="left" colspan="2">
										<select name="cnc">
											<c:forEach items="${daftarReport}" var="d">
												<option value="${d.key}">${d.value}</option>
											</c:forEach>
										</select>
									</th>
								</tr>
								<tr>
									<th nowrap="nowrap" style="width: 120px;">Cabang</th>
									<th class="left" colspan="2">
										<select name="cabang">
											<c:forEach items="${daftarCabang}" var="d">
												<option value="${d.key}"
												<c:if test="${cabang eq d.key}"> selected </c:if>
												>${d.value}</option>
											</c:forEach>
										</select>
									</th>
								</tr>
								<tr>
									<th nowrap="nowrap">Produk</th>
									<th class="left" colspan="2">
										<select name="produk">
											<c:forEach items="${daftarProduk}" var="d">
												<option value="${d.key}"
												<c:if test="${produk eq d.key}"> selected </c:if>
												>${d.value}</option>
											</c:forEach>
										</select>
									</th>
								</tr>
								<tr>
									<th nowrap="nowrap">Tanggal</th>
									<th class="left">
										<c:forEach items="${daftarTanggal}" var="d" varStatus="s">
											<label for="tanggal${s.index}"><input value="${d.key}" class="noBorder" type="radio" <c:if test="${tanggal eq d.key}"> checked </c:if> name="tanggal" id="tanggal${s.index}">${d.value}</label><br/>
										</c:forEach>
									</th>
									<th class="left">
										<script>inputDate('startDate', '${startDate}', false);</script> s/d
										<script>inputDate('endDate', '${endDate}', false);</script>									
									</th>
								</tr>
								<tr>
									<th nowrap="nowrap"></th>
									<td colspan="2">
										<input type="button" name="pdf" value="Show PDF" onclick="tampil('pdf');">
										<!-- <input type="button" name="xls" value="Show XLS" onclick="tampil('xls');"> -->
									</td>
								</tr>
							</table>
						</fieldset>	
					</form>						
				</div>
			</div>
		</div>
	</body>
</html>