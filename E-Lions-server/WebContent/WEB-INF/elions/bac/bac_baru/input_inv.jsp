<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ include file="/include/page/header_bacbaru.jsp"%>
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<c:set var="path" value="${pageContext.request.contextPath}" />



	<%-- FIXME: nanti remove ini untuk development saja
	<script type="text/javascript"
	  src="http://jqueryui.com/themeroller/themeswitchertool/">
	</script>
	<div id="switcher"></div>
	--%>
	
	<form method="post" id="formPost" name="formPost">
		<!-- @@ Fieldset Button Page -->
		<input id="submitType" name="submitType" type="hidden" value=${submitType}>
		<input type="submit" name="_target0" value=" " 
				class="bacNavigation" style="background-image: url(${path}/include/image/ddu1.jpg);">
		<input type="submit" name="_target1" value=" "
				class="bacNavigation" style="background-image: url(${path}/include/image/inv2.jpg);">
		<input type="submit" name="_target2" value=" "
				class="bacNavigation" style="background-image: url(${path}/include/image/pp1.jpg);">
		<input type="submit" name="_target3" value=" "
				class="bacNavigation" style="background-image: url(${path}/include/image/ttg1.jpg);">
		<input type="submit" name="_target4" value=" "
				class="bacNavigation" style="background-image: url(${path}/include/image/ag1.jpg);">
		<input type="submit" name="_target5" value=" "
				class="bacNavigation" style="background-image: url(${path}/include/image/kf1.jpg);">
		<input type="button" name="dummy2" value=" "
				class="bacNavigation" style="background-image: url(${path}/include/image/sb1.jpg);">
		
		<div style="text-align: center;">
			</br>				
			<input type="submit" id="prevsubmit" name="_target${halaman-1}"
				value="Prev &laquo;" >
			<input type="submit" id="nowsubmit" name="_target${halaman+1}"
				value="Next &raquo;">
		</div>
			
		<input type="hidden" name="hal" value="${halaman}">
		<input type="hidden" name="_page" value="${halaman}">
		
		<!-- Tabs -->
<!-- 		<div id="tabs"> -->
<!-- 			<ul id="divtabs"> -->
<!-- 				<li id="tab1" value="0"><a href="#tab-asu" name="_target0" >Asuransi</a></li> -->
<!-- 				<li id="tab2" value="1"><a href="#tab-inv" name="_target1">Detail Investasi & Ahli Waris</a></li> -->
<!-- 				<li id="tab3" value="2"><a href="#tab-pp" name="_target2" >Pemegang Polis</a></li> -->
<!-- 			</ul> -->
			
<!-- 			<div id="tab-inv"  > -->
				
				<!-- @@ Fieldset Rekening Autodebet -->			
				<fieldset class="ui-widget ui-widget-content">
					<legend class="ui-widget-header ui-corner-all"><div>Rekening / Kartu Kredit Pemegang Polis (yang digunakan untuk Pembayaran Premi Lanjutan / Autodebet)</div></legend>
					
					<div class="rowElem">
						<label>Bank:<em>*</em></label>
						<input id="acBankPusat" type="text" title="Nama bank">
					</div>
					<div class="rowElem">
						<label>No Rekening:<em>*</em></label>
						<input type="text" title="Nomor rekening">
					</div>
					<div class="rowElem">
						<label>Atas Nama:<em>*</em></label>
						<input type="text" title="Nama pemegang rekening">
					</div>
					<div class="rowElem">
						<label>Tanggal Debet:<em>*</em></label>
						<input type="text" class="datepicker" title="Tanggal pendebetan Rekening / Kartu Kredit">
					</div>
					<div class="rowElem">
						<label>Tanggal Expired CC:</label>
						<input type="text" class="datepicker" title="Tanggal expired kartu kredit">
					</div>
					<div class="rowElem">
						<label>Aktif:</label>
						<select title="Aktif / Non aktif"><option>Aktif</option></select>
					</div>
					
				</fieldset>
				
				<!-- @@ Fieldset Produk Unit Link -->
				<fieldset class="ui-widget ui-widget-content">
					<legend class="ui-widget-header ui-corner-all"><div>Khusus Produk Unit Link</div></legend>
				
					<div class="rowElem">
						<label>Perhitungan Premi<br>(sesuai dengan cara pembayaran):<em>*</em></label>
						<table class="jtable">
							<tr>
								<th>Jenis Premi</th>
								<th>Jumlah</th>
								<th>Kombinasi Premi (%)</th>
							</tr>
							<tr>
								<td><select title="Jenis premi Pokok / Topup"><option>Premi Pokok</option></select></td>
								<td><input type="text" title="Nominal premi"></td>
								<td><input type="text" title="Persentase kombinasi Premi Pokok (PP) dan Premi Topup Berkala (PTB) sesuai Proposal"> %</td>
							</tr>
							<tr>
								<td><select title="Jenis premi Pokok / Topup"><option>Premi Topup Berkala</option></select></td>
								<td><input type="text" title="Nominal premi"></td>
								<td><input type="text" title="Persentase kombinasi Premi Pokok (PP) dan Premi Topup Berkala (PTB) sesuai Proposal"> %</td>
							</tr>
							<tr>
								<th>Total:</th>
								<td><input type="text" title="Total nominal premi"></td>
								<td><input type="text" title="Total persentase kombinasi Premi Pokok (PP) dan Premi Topup Berkala (PTB) sesuai Proposal"> %</td>
							</tr>
						</table>		
					</div>
				
					<div class="rowElem">
						<label>Jenis Dana Investasi:<em>*</em></label>
						<table class="jtable">
							<tr>
								<th rowspan="2" colspan="2">Jenis Dana Investasi</th>
								<th colspan="5">Nama Fund (%)</th>
							</tr>
							<tr>
								<th>Excellink</th>
								<th>Excellink Syariah</th>
								<th>Artha Link</th>
								<th>Ekalink</th>
								<th>Ekalink Super</th>
							</tr>
							<tr>
								<td>Rp.</td>
								<td>Fixed Income Fund</td>
								<td><input type="text" title="Fund" size="2"></td>
								<td><input type="text" title="Fund" size="2"></td>
								<td><input type="text" title="Fund" size="2"></td>
								<td><input type="text" title="Fund" size="2"></td>
								<td><input type="text" title="Fund" size="2"></td>
							</tr>
							<tr>
								<td>Rp.</td>
								<td>Dynamic Fund</td>
								<td><input type="text" title="Fund" size="2"></td>
								<td><input type="text" title="Fund" size="2"></td>
								<td><input type="text" title="Fund" size="2"></td>
								<td><input type="text" title="Fund" size="2"></td>
								<td><input type="text" title="Fund" size="2"></td>
							</tr>
							<tr>
								<td>Rp.</td>
								<td>Aggresive Fund</td>
								<td><input type="text" title="Fund" size="2"></td>
								<td><input type="text" title="Fund" size="2"></td>
								<td><input type="text" title="Fund" size="2"></td>
								<td><input type="text" title="Fund" size="2"></td>
								<td><input type="text" title="Fund" size="2"></td>
							</tr>
							<tr>
								<td>USD.</td>
								<td>Secure Dollar Income Fund</td>
								<td><input type="text" title="Fund" size="2"></td>
								<td><input type="text" title="Fund" size="2"></td>
								<td><input type="text" title="Fund" size="2"></td>
								<td><input type="text" title="Fund" size="2"></td>
								<td><input type="text" title="Fund" size="2"></td>
							</tr>
							<tr>
								<td>USD.</td>
								<td>Dynamic Dollar Fund</td>
								<td><input type="text" title="Fund" size="2"></td>
								<td><input type="text" title="Fund" size="2"></td>
								<td><input type="text" title="Fund" size="2"></td>
								<td><input type="text" title="Fund" size="2"></td>
								<td><input type="text" title="Fund" size="2"></td>
							</tr>
						</table>
						
						<table class="jtable">
							<tr>
								<th rowspan="2">Jenis Dana Investasi</th>
								<th colspan="2">Nama Fund (%)</th>
							</tr>
							<tr>
								<th>Stable Fund</th>
								<th>Stable Link Syariah Fund</th>
							</tr>
							<tr>
								<td>Rp.</td>
								<td><input type="text" title="Fund" size="2"></td>
								<td><input type="text" title="Fund" size="2"></td>
							</tr>
							<tr>
								<td>USD</td>
								<td><input type="text" title="Fund" size="2"></td>
								<td><input type="text" title="Fund" size="2"></td>
							</tr>
						</table>
						
						<table class="jtable">
							<tr>
								<th>Jenis Dana Investasi</th>
								<th>Persentase Fund (%)</th>
							</tr>
							<tr>
								<td>BPPI Plus Fund-1</td>
								<td><input type="text" title="Fund" size="2"></td>
							</tr>
							<tr>
								<td>BPPI Plus Fund-2</td>
								<td><input type="text" title="Fund" size="2"></td>
							</tr>
						</table>	
					</div>
				
					<div class="rowElem">
						<label>Dana Investasi Dialokasikan:<em>*</em></label>
						<input type="radio" id="radioDid1" name="did" title="Langsung setelah dana diterima di rekening yang ditentukan dan SPAJ telah diterima di bagian Underwriting"><label class="radioLabel" for="radioDid1">Langsung Setelah Dana Diterima</label>
						<input type="radio" id="radioDid2" name="did" title="Setelah SPAJ disetujui oleh Underwriting dan Extra Premi sudah dibayar (jika ada)"><label class="radioLabel" for="radioDid2">Setelah Akseptasi</label>
					</div>
				
				</fieldset>
				
				<!-- @@ Fieldset Produk Save (Powersave, Stablelink, Stablesave) -->
				<fieldset class="ui-widget ui-widget-content">
					<legend class="ui-widget-header ui-corner-all"><div>Khusus Produk Investasi (Powersave, Platinum Save, Stable Link, Stable Save, Progressive Save)</div></legend>
				
					<div class="rowElem">
						<label>Jangka Waktu:<em>*</em></label>
						<select title="Masa Garansi Investasi (MGI) / Masa Target Investasi (MTI)"><option>---Silahkan Pilih---</option></select>
					</div>
					<div class="rowElem">
						<label>Bunga (%):<em>*</em></label>
						<select title="Jenis bunga"><option>Normal</option></select>
						<input type="text" title="Persentase bunga (%)" size="5"> %
					</div>
					<div class="rowElem">
						<label>Tanggal Jatuh Tempo:</label>
						<input class="ui-state-disabled" disabled="disabled" type="text" class="datepicker" title="Tanggal jatuh tempo rollover">
					</div>
					<div class="rowElem">
						<label>Jumlah Investasi:</label>
						<input class="ui-state-disabled" disabled="disabled" type="text" title="Jumlah dana yang diinvestasikan">
					</div>
					<div class="rowElem">
						<label>Jumlah Bunga:</label>
						<input class="ui-state-disabled" disabled="disabled" type="text" title="Jumlah bunga">
					</div>
					<div class="rowElem">
						<label>Jenis Rollover:</label>
						<select title="Jenis rollover">
							<option title="Memperpanjang nilai polis">Rollover Nilai Tunai (All)</option>
							<option title="Memperpanjang Premi saja dan mengambil hasil investasi">Rollover Premi</option>
							<option title="Mengambil nilai tunai">Autobreak</option>
						</select>
					</div>
					<div class="rowElem">
						<label>Nomor Memo:<em>*</em></label>
						<input type="text" title="Harus diisi apabila jenis bunga SPECIAL CASE">
					</div>
					<div class="rowElem">
						<label>Tanggal NAB:</label>
						<input type="text" title="Tanggal NAB (khusus Stable Link)">
					</div>
					<div class="rowElem">
						<label>Nilai NAB:</label>
						<input type="text" title="Nilai NAB (khusus Stable Link)">
					</div>
					<div class="rowElem">
						<label>Begdate Topup:</label>
						<input type="text" title="Tanggal mulai Topup (khusus Stable Link)">
					</div>
					<div class="rowElem">
						<label>Persentase Rate BP:</label>
						<input type="text" title="Persentase rate Bonus Prestasi (khusus Stable Link)">
					</div>
					<%-- FIXME perlu gak
					<div class="rowElem">
						<label>Penarikan Bunga:</label>
						<select title="Penarikan bunga"><option>---Silahkan pilih---</option></select>
					</div>
					<div class="rowElem">
						<label>Breakable:</label>
						<input type="checkbox" title="Breakable" id="brekele"><label class="radioLabel" for="brekele">Breakable</label>
					</div>
					<div class="rowElem">
						<label>Cara Bayar Rider:</label>
						<input type="radio" id="radioCBR1" name="cbr" title="?"><label class="radioLabel" for="radioCBR1">Bayar Langsung</label>
						<input type="radio" id="radioCBR2" name="cbr" title="?"><label class="radioLabel" for="radioCBR2">Potongan Bunga</label>
						<input type="radio" id="radioCBR3" name="cbr" title="?"><label class="radioLabel" for="radioCBR3">Bayar Langsung Sekaligus</label>
						<input type="radio" id="radioCBR4" name="cbr" title="?"><label class="radioLabel" for="radioCBR4">Potongan Bunga Sekaligus</label>
					</div>
					<div class="rowElem">
						<label>Telemarketing:</label>
						<input type="checkbox" title="Via telemarketing" id="vt"><label class="radioLabel" for="vt">Via Telemarketing</label>
					</div>
					--%>	
					
				</fieldset>
<!-- 			</div> -->
<!-- 		</div> -->
	
	</form>
</body>

