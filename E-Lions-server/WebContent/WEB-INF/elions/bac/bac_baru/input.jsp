<%@ include file="/include/page/header_jquery.jsp"%>
<%@ include file="/include/page/header.jsp"%>

<c:set var="path" value="${pageContext.request.contextPath}" />

<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		// (jQueryUI Tabs) init tab2 Utama (Pemegang, Tertanggung, dll)
		$("#tabs").tabs();

		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI autocomplete) init semua field autocomplete
		var availableTags = [ "ActionScript", "AppleScript", "Asp", "BASIC",
				"C", "C++", "Clojure", "COBOL", "ColdFusion", "Erlang",
				"Fortran", "Groovy", "Haskell", "Java", "JavaScript", "Lisp",
				"Perl", "PHP", "Python", "Ruby", "Scala", "Scheme" ];
		
		$("#acSumberBisnis").autocomplete({
			source : availableTags
		});
		$("#acBankPusat").autocomplete({
			source : availableTags
		});
					
		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});
		$(".jtable tr").hover(function() {
			$(this).children("td").addClass("ui-state-hover");
		}, function() {
			$(this).children("td").removeClass("ui-state-hover");
		});
		//$(".jtable tr").click(function() {
		//	$(this).children("td").toggleClass("ui-state-highlight");
		//});
		
		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, input:reset, button").button(); //styling untuk semua tombol
		$( "#btnCopyDataSpaj" ).button({
            icons: {
                primary: "ui-icon-search"
            }
        });
		//$( ".radio" ).buttonset();
		//$( ".checkbox" ).buttonset();
		
		// (jQuery Form Plugin) rubah form jadi ajax
		var options = {
			target : '#output1',
			beforeSubmit : showRequest,
			success : showResponse
		};
		$('#formPost').ajaxForm(options); // bind form using 'ajaxForm'
		
		function showRequest(formData, jqForm, options) { // pre-submit callback 
		    var queryString = $.param(formData);
		    alert('About to submit: \n\n' + queryString); 
		    return true; 
		}
		function showResponse(responseText, statusText, xhr, $form) { // post-submit callback 
		    alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + 
		        '\n\nThe output div should have already been updated with the responseText.'); 
		}
		
		<%--
		//theme switcher (FIXME untuk development saja)
		$('#switcher').themeswitcher();		
		--%>
		
		// (jQuery Validation Plugin) client-side validation untuk formPost on keyup and on submit
		$.validator.setDefaults({
			highlight: function(input) {
				$(input).addClass("ui-state-error");
			},
			unhighlight: function(input) {
				$(input).removeClass("ui-state-error");
			}
		});
		
		$("#formPost").validate({
			rules: {
				mspo_no_blanko: {
					required: true,
					minlength: 2
				},
				pemegang: "required"
			},
			messages: {
				mspo_no_blanko: {
					required: "Harap isi No Blanko",
					minlength: "No blanko minimal 2 karakter"
				},
				pemegang: "Harap isi nama Pemegang Polis"
			}
		});
		
		$('#New').click(function() {
//  				alert("new nih");
    	      location.reload();
 
		});
		
		//
		
	});
				
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 23em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 23.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk button container */
	.buttons { text-align: center; margin: 1em; margin-top: 0em; }
	
	/* styling untuk label didalam tabel (contohnya untuk telepon) 
	label.tableElem { margin-right: 0.4em; width: 135px; display: block; float: left; }
	*/	

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }

</style>

<body >

	<%-- FIXME: nanti remove ini untuk development saja
	<script type="text/javascript"
	  src="http://jqueryui.com/themeroller/themeswitchertool/">
	</script>
	<div id="switcher"></div>
	--%>
	
	<form method="post" id="formPost" name="formPost">
	<input id="submitType" name="submitType" type="hidden" value=${submitType}>
	<!-- Tabs -->
	<div id="tabs">
		<ul>
			<li><a href="#tab-asu"  id="tab-asu">Asuransi</a></li>
			<li><a href="#tab-inv"  id="tab-inv">Detail Investasi & Ahli Waris</a></li>
<!-- 			<li><a href="#tab-pp" onClick="showPane('pane3', this)" id="tab-pp">Pemegang Polis</a></li> -->
<!-- 			<li><a href="#tab-prm" name="_target3">Pembayar Premi</a></li> -->
<!-- 			<li><a href="#tab-ttg" name="_target4">Tertanggung</a></li> -->
<!-- 			<li><a href="#tab-agen" name="_target5">Agen Penutup</a></li> -->
<!-- 			<li><a href="#tab-konf" name="_target6">Konfirmasi</a></li> -->
		</ul>
		
		<!-- Tab Asuransi -->
		<div id="tab-asu"  >
			<jsp:include page="input_asu.jsp" >
				<jsp:param name="test" value="test"></jsp:param>
			</jsp:include>				
		</div>
			
		<!-- Tab Investasi -->
		<div id="tab-inv">
			<jsp:include page="input_inv.jsp" >
				<jsp:param name="test" value="test"></jsp:param>
			</jsp:include>				
		</div>
		
		<!-- Tab Pemegang & Tertanggung -->
		<div id="tab-pp" >
			<jsp:include page="input_pp.jsp">
				<jsp:param name="test" value="test"></jsp:param>
			</jsp:include>				
		</div>

		<!-- Tab Pembayar Premi -->
		<div id="tab-prm">
			<jsp:include page="input_prm.jsp" >
				<jsp:param name="test" value="test"></jsp:param>
			</jsp:include>				
		</div>
		
		<!-- Tab Tertanggung -->
		<div id="tab-ttg">
			<jsp:include page="input_ttg.jsp" >
				<jsp:param name="test" value="test"></jsp:param>
			</jsp:include>				
		</div>

		<!-- Tab Agen Penutup -->
		<div id="tab-agen">
			<jsp:include page="input_agen.jsp" >
				<jsp:param name="test" value="test"></jsp:param>
			</jsp:include>				
		</div>
			
		<!-- Tab Konfirmasi -->
		<div id="tab-konf">
			<jsp:include page="input_konf.jsp" >
				<jsp:param name="test" value="test"></jsp:param>
			</jsp:include>				
		</div>

		<!-- Buttons -->
		<div class="buttons">
			<input type="button" title="Input SPAJ baru" id="New" value="New">
			<input id="btnSave" type="submit" title="Simpan SPAJ" value="Save">
		</div>
		
	</div>

	</form>

</body>
</html>