<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header_bacbaru.jsp"%>

<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<script type="text/javascript">
	$().ready(function() {
		$("#lsbs_id").change(function() {
			//show kode product
			$("#kode_produk").val(this.value);
			//show Dropdown of sub product
			$("#plan_sub").empty();
			$("#masa_tanggung").val('');
			$("#masa_bayar").val('');
			$("#cuti_premi").val('');
			var url = "${path}/common/json.htm?window=json&type=getSubProduk&paramA=" +$("#kode_produk").val();;
			$.getJSON(url, function(result) {
				$("<option/>").val("0~X0").html("--SILAKAN PILIH NAMA PRODUK--").appendTo("#plan_sub");
				$.each(result, function() {
					$("<option/>").val(this.plan).html('' + this.lsdbs_name + ' ' ).appendTo("#plan_sub");
					if($("#hidden_plan_sub").val() != ''){
						$("#plan_sub").val($("#hidden_plan_sub").val());
					}
				});
			});
		});
		
		//set Value
		$("#plan_sub").change(function() {
			$("#hidden_plan_sub").val($("#plan_sub").val());
			$("#plan_sub").attr('selected', 'selected');
		});
		
		
		$("#formPost").validate({
			rules: {
				lsbs_id: "required",
				plan_utama: "required",
			},
			messages: {
				lsbs_id: "Harap isi No produk",
				plan_utama: "Harap pilih Plan Porduk"
			}
		});
		
		if($("#lsbs_id").val() != ''){
			$("#lsbs_id").change();
			
		}
		
	});
</script>

<c:set var="path" value="${pageContext.request.contextPath}" />

<body>
	
	<form method="post" id="formPost" name="formPost">
		
		<!-- @@ Fieldset Button Page -->
		<input id="submitType" name="submitType" type="hidden" value=${submitType}>
		<input type="submit" name="_target0" value=" " 
				class="bacNavigation" style="background-image: url(${path}/include/image/ddu2.jpg);">
		<input type="submit" name="_target1" value=" "
				class="bacNavigation" style="background-image: url(${path}/include/image/inv1.jpg);">
		<input type="submit" name="_target2" value=" "
				class="bacNavigation" style="background-image: url(${path}/include/image/pp1.jpg);">
		<input type="submit" name="_target3" value=" "
				class="bacNavigation" style="background-image: url(${path}/include/image/ttg1.jpg);">
		<input type="submit" name="_target4" value=" "
				class="bacNavigation" style="background-image: url(${path}/include/image/ag1.jpg);">
		<input type="submit" name="_target5" value=" "
				class="bacNavigation" style="background-image: url(${path}/include/image/kf1.jpg);">
		<input type="button" name="dummy2" value=" "
				class="bacNavigation" style="background-image: url(${path}/include/image/sb1.jpg);">
		
		<div style="text-align: center;">
			</br>				
			<input type="submit" id="nowsubmit" name="_target${halaman+1}"
					value="Next &raquo;" >
		</div>
			
		<input type="hidden" name="hal" value="${halaman}">
		<input type="hidden" name="_page" value="${halaman}">
		
		<!-- Tabs -->
<!-- 		<div id="tabs"> -->
<!-- 			<ul id="divtabs"> -->
<!-- 				<li id="tab1" value="0"><a href="#tab-asu"  >Asuransi</a></li> -->
<!-- 				<li id="tab2" value="1"><a href="#tab-inv">Detail Investasi & Ahli Waris</a></li> -->
<!-- 				<li id="tab3" value="2"><a href="#tab-pp">Pemegang Polis</a></li> -->
<!-- 			</ul> -->
			
<!-- 			<div id="tab-asu"> -->
			
				<!-- @@ Fieldset Data Produk Utama -->
				<fieldset class="ui-widget ui-widget-content">
					<legend class="ui-widget-header ui-corner-all"><div>Asuransi Pokok</div></legend>
					
					<div class="rowElem">
						<label>Produk:<em>*</em></label>
						
						<input type="text"  name="datausulan.kodeproduk" id="kode_produk" required="required" title="Kode Produk" size="3" value="${cmd.datausulan.lsbs_id}" >
						
						<select name="datausulan.lsbs_id" id="lsbs_id"  title="Silahkan pilih Produk Utama">
							<option value="">-- Silahkan pilih Produk Utama --</option>
							<c:forEach var="plan_v" items="${listprodukutama}" varStatus="plan_vs">
								<option value="${plan_v.lsbs_id}" <c:if test="${plan_v.lsbs_id eq cmd.datausulan.lsbs_id}">selected</c:if>>${plan_v.lsbs_name}</option>
							</c:forEach>
						</select>
						<select name="datausulan.plan" id="plan_sub" title="Silahkan pilih Sub Produk">
						<input type="hidden" id="hidden_plan_sub" value="${cmd.datausulan.plan}">
						</select>
					</div>
					<div class="rowElem">
						<label>Jenis Produk:<em>*</em></label>
						<select name="jn_produk" id="jn_produk" title="Silahkan pilih Jenis produk">
							<option>-- Silahkan pilih Jenis Produk --</option>
							<c:forEach var="jn_produk_v" items="${listtipeproduk}" varStatus="jn_produk_vs">
								<option value="${jn_produk_v.lsbs_id}">${jn_produk_v.lstp_produk}</option>
							</c:forEach>
						</select>
					</div>
					<div class="rowElem">
						<label>Klas:<em>*</em></label>
						<spring:bind path="cmd.datausulan.mspr_class">
							<input type="text"  name="${status.expression}" id="lsbs_id" required="required" title="Kode Produk" size="3" value="${cmd.datausulan.mspr_class}" >
						</spring:bind>
					</div>
					<div class="rowElem">
						<label>Masa Pertanggungan:</label>
						<input class="ui-state-disabled" type="text" name="datausulan.mspr_ins_period" id="masa_tanggung" disabled="disabled" title="Masa pertanggungan (tahun)" size="3" value="${cmd.datausulan.mspr_ins_period}" > tahun
					</div>
					<div class="rowElem">
						<label>Masa Pembayaran Premi:</label>
						<input class="ui-state-disabled" type="text" name="datausulan.mspo_pay_period" id="masa_bayar" disabled="disabled" title="Masa pembayaran premi (tahun)" size="3" value="${cmd.datausulan.mspo_pay_period}" > tahun
					</div>
					<div class="rowElem">
						<label>Cuti Premi :</label>
						Pada Tahun ke <input type="text" name="datausulan.mspo_installment" id="cuti_premi" title="Cuti Premi" size="3" value="${cmd.datausulan.mspo_installment}" >
					</div>
					<div class="rowElem">
						<label>Uang Pertanggungan:<em>*</em></label>
						<select title="Mata uang"><option>---</option></select>
						<input required="required" type="text" title="Uang pertanggungan (UP / TSI)">
					</div>
					<div class="rowElem">
						<label>Total Premi Standar:<em>*</em></label>
						<select title="Mata uang"><option>---</option></select>
						<input type="text" title="Total premi standar (sesuai dengan cara pembayaran)">
					</div>
					<div class="rowElem">
						<label>Kombinasi Premi:<em>*</em></label>
						<select title="Kombinasi premi (%) pokok dan top-up (khusus produk Unit Link)"><option>--Kombinasi Premi--</option></select>
					</div>
					<div class="rowElem">
						<label>Cara Pembayaran Premi:<em>*</em></label>
						<select title="Cara pembayaran premi"><option>--Cara Pembayaran Premi--</option></select>
					</div>
					<div class="rowElem">
						<label>Bentuk Pembayaran Premi:<em>*</em></label>
						<select title="Bentuk pembayaran premi"><option>--Bentuk Pembayaran Premi--</option></select>
					</div>
					<div class="rowElem">
						<label>Periode Pertanggungan:<em>*</em></label>
						<input type="text" class="datepicker" title="Tanggal mulai berlaku pertanggungan"> s/d 
						<input type="text" class="ui-state-disabled datepicker" disabled="disabled" title="Tanggal akhir berlaku pertanggungan">
					</div>
				
				</fieldset>
				
				<!-- @@ Fieldset Data Rider -->
				<fieldset class="ui-widget ui-widget-content">
					<legend class="ui-widget-header ui-corner-all"><div>Asuransi Tambahan (Rider)</div></legend>
				
					<table class="jtable">
						<tr>
							<th>No</th>
							<th>Produk</th>
							<th>UP</th>
							<th>Premi</th>
							<th colspan="2">Masa / Periode Pertanggungan</th>
							<th>Akhir Pembayaran</th>
							<th>Tertanggung</th>
							<th>Rate</th>
						</tr>
						<tr>
							<td>1</td>
							<td>
								<select title="Rider yang dipilih"><option>--- Silahkan Pilih ---</option></select>
								<label for="unit">Unit:</label><input type="text" title="Unit" id="unit" size="2">
								<label for="klas">Klas:</label><input type="text" title="Klas" id="klas" size="2">
							</td>
							<td><input type="text" title="Uang pertanggungan (UP/TSI)"></td>
							<td><input type="text" title="Premi"></td>
							<td><input type="text" title="Masa pertanggungan (tahun)" size="3"></td>
							<td>
								<input type="text" class="ui-state-disabled datepicker" disabled="disabled" class="datepicker" title="Tanggal mulai berlaku pertanggungan"> s/d 
								<input type="text" class="ui-state-disabled datepicker" disabled="disabled" title="Tanggal akhir berlaku pertanggungan">
							</td>
							<td><input type="text" class="datepicker" title="Tanggal akhir pembayaran"></td>
							<td><select title="Tertanggung"><option>---Tertanggung---</option></select></td>
							<td><input type="text" size="5" title="Rate rider"></td>
						</tr>
					</table>
				
				</fieldset>
				
				<!-- @@ Fieldset Data Tambahan -->
				<fieldset class="ui-widget ui-widget-content">
					<legend class="ui-widget-header ui-corner-all"><div>Data Tambahan (Khusus Produk Tertentu)</div></legend>
				
					<div class="rowElem">
						<label>Khusus Specta Save:</label>
						<input type="checkbox" title="Produk Specta Save (BII)" id="specta"><label class="radioLabel" for="specta">Produk Specta Save (BII)</label>
					</div>
					<div class="rowElem">
						<label>Khusus Stable Link / Stable Save:</label>
						<input type="radio" id="radioS1" name="slss" title="Bukan manfaat bulanan"><label class="radioLabel" for="radioS1">Bukan Manfaat Bulanan</label>
						<input type="radio" id="radioS2" name="slss" title="Manfaat bulanan"><label class="radioLabel" for="radioS2">Manfaat Bulanan</label>
					</div>
					<div class="rowElem">
						<!-- FIXME gabungkan dengan checkbox yang karyawan EL-SMG di bagian rollover powersave -->
						<label>Khusus Karyawan AJS:</label>
						<input type="checkbox" title="Pemegang Polis merupakan karyawan AJS" id="kary"><label class="radioLabel" for="kary">Pemegang Polis merupakan karyawan AJS</label>
					</div>
					<div class="rowElem">
						<label>BP Rate:</label>
						<input type="checkbox" title="Special case BP rate" id="bpr"><label class="radioLabel" for="bpr">Special Case BP Rate</label>
					</div>
					<div class="rowElem">
						<label>Khusus Eka Sehat:</label>
						<input type="checkbox" title="Plan provider" id="prov"><label class="radioLabel" for="prov">Plan Provider</label>
					</div>
					<div class="rowElem">
						<label>Khusus Horison:</label>
						<select title="Persentase (%) DPLK"><option>--Persentase (%) DPLK--</option></select>
					</div>
					<div class="rowElem">
						<label>Khusus Produk BMI:</label>
						<input type="text" title="Nomor Sinar Card" id="sinarcard"><label class="radioLabel" for="sinarcard">(Nomor Sinar Card)</label>
					</div>
					<div class="rowElem">
						<label>Khusus Bank Sinarmas:</label>
						<input type="text" title="Persen (%) bunga">
					</div>
					<div class="rowElem">
						<label>Khusus Produk Simponi 8 dan Premium Saver:</label>
						<input type="text" title="Persen (%) bonus tahapan">
					</div>
				
				</fieldset>	
<!-- 			</div> -->
			
<!-- 		</div> -->
	</form>
</body>

