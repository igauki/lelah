<%@ include file="/include/page/header_bacbaru.jsp"%>
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">

<c:set var="path" value="${pageContext.request.contextPath}" />

<form method="post" id="formPost" name="formPost">

	<!-- @@ Fieldset Button Page -->
		<input id="submitType" name="submitType" type="hidden" value=${submitType}>
		<input type="submit" name="_target0" value=" " 
				class="bacNavigation" style="background-image: url(${path}/include/image/ddu1.jpg);">
		<input type="submit" name="_target1" value=" "
				class="bacNavigation" style="background-image: url(${path}/include/image/inv1.jpg);">
		<input type="submit" name="_target2" value=" "
				class="bacNavigation" style="background-image: url(${path}/include/image/pp2.jpg);">
		<input type="submit" name="_target3" value=" "
				class="bacNavigation" style="background-image: url(${path}/include/image/ttg1.jpg);">
		<input type="submit" name="_target4" value=" "
				class="bacNavigation" style="background-image: url(${path}/include/image/ag1.jpg);">
		<input type="submit" name="_target5" value=" "
				class="bacNavigation" style="background-image: url(${path}/include/image/kf1.jpg);">
		<input type="button" name="dummy2" value=" "
				class="bacNavigation" style="background-image: url(${path}/include/image/sb1.jpg);">
		
		<div style="text-align: center;">
			</br>				
			<input type="submit" id="prevsubmit" name="_target${halaman-1}"
				value="Prev &laquo;" >
			<input type="submit" id="nowsubmit" name="_target${halaman+1}"
				value="Next &raquo;">
		</div>
			
		<input type="hidden" name="hal" value="${halaman}">
		<input type="hidden" name="_page" value="${halaman}">
	
	<!-- @@ Fieldset Data Pendukung -->
	<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>Form SPAJ</div></legend>
		
		<div class="rowElem">
			<label>Copy Data dari SPAJ:</label>
			<button id="btnCopyDataSpaj" title="Fitur untuk copy data dari SPAJ lain yang pernah diinput sebelumnya">Cari</button>
		</div>
		<div class="rowElem">
			<label>Nomor Seri / Blanko:<em>*</em></label>
			<input name="mspo_no_blanko" type="text" title="Nomor Blanko SPAJ" value="123456">
		</div>
		<div class="rowElem">
			<label>Nomor Register SPAJ:</label>
			<input class="ui-state-disabled" disabled="disabled" type="text" title="Nomor Register SPAJ (dikeluarkan otomatis oleh sistem)" value="60201212345">
		</div>
		<div class="rowElem">
			<label>Nomor Polis:</label>
			<input class="ui-state-disabled" disabled="disabled" type="text" title="Nomor Polis (dikeluarkan otomatis oleh sistem setelah AKSEPTASI)" value="60200201212345">
		</div>
		<div class="rowElem">
			<label>Medis:<em>*</em></label>
			<select title="Medis / Non medis"><option>Tanpa pemeriksaan kesehatan</option></select>
		</div>
		<div class="rowElem">
			<label>Dokumen SPAJ:<em>*</em></label>
			<select title="SPAJ asli / fotokopi"><option>Asli</option></select>
		</div>
		<div class="rowElem">
			<label>Nomor PB / CIF:<em>*</em></label>
			<input type="text" title="Nomor Pemindahbukuan (Khusus Bank Sinarmas)">
			<input type="text" title="Nomor CIF (Khusus Bank Sinarmas)">
		</div>
		<div class="rowElem">
			<label>Sumber Bisnis:</label>
			<input id="acSumberBisnis" type="text" title="Input perusahaan Grup Sinarmas yang merupakan sumber bisnis SPAJ ini (Khusus Cross-Selling)">
		</div>
		
	</fieldset>
	
	<%--
	<table class="jtable" style="width: 100%;">
		<tr>
			<th style="width: 20%;">A. Data</th>
			<th style="width: 40%;">Pemegang Polis</th>
			<th style="width: 40%;">
				Tertanggung<br>
				<select title="Hubungan / Relasi Calon Pemegang Polis dengan Calon Tertanggung">
					<option>--- Silahkan Pilih ---</option>
				</select>
			</th>
		</tr>
		<tr>
			<td>Pemegang Polis Badan Hukum / Usaha:<em>*</em></td>
			<td colspan="2">
				<select title="Apakah Pemegang Polis merupakan badan hukum / usaha?"><option>Tidak</option></select>
			</td>
		</tr>
		<tr>
			<td>Nama Lengkap:<em>*</em></td>
			<td>
				<select title="Gelar"><option>---</option></select>
				<input type="text" title="Nama lengkap Pemegang Polis sesuai Bukti Identitas yang disertakan">
				<input type="text" size="5" title="Gelar">
			</td>
			<td>
				<select title="Gelar"><option>---</option></select>
				<input type="text" title="Nama lengkap Tertanggung sesuai Bukti Identitas yang disertakan">
				<input type="text" size="5" title="Gelar">
			</td>
		</tr>
		<tr>
			<td>Nama Ibu Kandung:<em>*</em></td>
			<td><input type="text" title="Nama gadis ibu kandung dari Pemegang Polis"></td>
			<td><input type="text" title="Nama gadis ibu kandung dari Tertanggung"></td>
		</tr>
		<tr>
			<td>Bukti Identitas:<em>*</em>
			</td>
			<td>
				<select title="Bukti Identitas Pemegang Polis yang valid"><option>-----</option></select>
				<input type="text" title="Nomor bukti identitas Pemegang Polis">
			</td>
			<td>
				<select title="Bukti Identitas Pemegang Polis yang valid"><option>-----</option></select>
				<input type="text" title="Nomor bukti identitas Tertanggung">
			</td>
		</tr>
		<tr>
			<td>Warga Negara:<em>*</em></td>
			<td>
				<select title="Status warga negara Pemegang Polis"><option>-- Silahkan Pilih --</option></select>
			</td>
			<td>
				<select title="Status warga negara Tertanggung"><option>-- Silahkan Pilih --</option></select>
			</td>
		</tr>
		<tr>
			<td>Tempat / Tanggal Lahir:<em>*</em></td>
			<td>
				<input type="text" title="Kota/Negara kelahiran Pemegang Polis">
				<input type="text" class="datepicker" title="Tanggal lahir Pemegang Polis">
			</td>
			<td>
				<input type="text" title="Kota/Negara kelahiran Tertanggung">
				<input type="text" class="datepicker" title="Tanggal lahir Tertanggung">
			</td>
		</tr>
		<tr>
			<td>Jenis Kelamin:<em>*</em></td>
			<td>
				<select title="Jenis kelamin Pemegang Polis"><option>-----</option></select>
			</td>
			<td>
				<select title="Jenis kelamin Tertanggung"><option>-----</option></select>
			</td>
		</tr>
		<tr>
			<td>Status:<em>*</em></td>
			<td>
				<select title="Status pernikahan Pemegang Polis"><option>-- Silahkan Pilih --</option></select>
			</td>
			<td>
				<select title="Status pernikahan Tertanggung"><option>-- Silahkan Pilih --</option></select>
			</td>
		</tr>
		<tr>
			<td>Keluarga:</td>
			<td>
				<table class="jtable">
					<tr>
						<th>No</th>
						<th colspan="2">Nama</th>
						<th>Tgl Lahir</th>
					</tr>
					<tr>
						<td>1</td>
						<td>Suami / Istri</td>
						<td><input type="text" title="Nama"></td>
						<td><input type="text" class="datepicker" title="Tanggal lahir"></td>
					</tr>
					<tr>
						<td>2</td>
						<td>Anak 1</td>
						<td><input type="text" title="Nama"></td>
						<td><input type="text" class="datepicker" title="Tanggal lahir"></td>
					</tr>
					<tr>
						<td>3</td>
						<td>Anak 2</td>
						<td><input type="text" title="Nama"></td>
						<td><input type="text" class="datepicker" title="Tanggal lahir"></td>
					</tr>
				</table>
			</td>
			<td>
				<table class="jtable">
					<tr>
						<th>No</th>
						<th colspan="2">Nama</th>
						<th>Tgl Lahir</th>
					</tr>
					<tr>
						<td>1</td>
						<td>Suami / Istri</td>
						<td><input type="text" title="Nama"></td>
						<td><input type="text" class="datepicker" title="Tanggal lahir"></td>
					</tr>
					<tr>
						<td>2</td>
						<td>Anak 1</td>
						<td><input type="text" title="Nama"></td>
						<td><input type="text" class="datepicker" title="Tanggal lahir"></td>
					</tr>
					<tr>
						<td>3</td>
						<td>Anak 2</td>
						<td><input type="text" title="Nama"></td>
						<td><input type="text" class="datepicker" title="Tanggal lahir"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>Agama:<em>*</em></td>
			<td><select title="Agama Pemegang Polis"><option>-- Silahkan Pilih --</option></select></td>
			<td><select title="Agama Tertanggung"><option>-- Silahkan Pilih --</option></select></td>
		</tr>
			<td>Pendidikan:<em>*</em></td>
			<td><select title="Tingkat pendidikan Pemegang Polis"><option>-- Silahkan Pilih --</option></select></td>
			<td><select title="Tingkat pendidikan Tertanggung"><option>-- Silahkan Pilih --</option></select></td>
		</tr>
		<tr>
			<td>Alamat Rumah:<em>*</em></td>
			<td>
				<textarea rows="4" cols="50" title="Alamat lengkap rumah Pemegang Polis sesuai Bukti Identitas yang disertakan"></textarea>
				<br>
				<label>Kota:<em>*</em></label>
				<input type="text" title="Kota" size="20">
				<label>Kode Pos:<em>*</em></label>
				<input type="text" title="Kode pos" size="6">
				<br>
				<label class="tableElem">Telpon Rumah 1:<em>*</em></label>
				<input type="text" title="Telpon rumah 1" size="3">
				<input type="text" title="Telpon rumah 1">
				<br>
				<label class="tableElem">Telpon Rumah 2:</label>
				<input type="text" title="Telpon rumah 2" size="3">
				<input type="text" title="Telpon rumah 2">
				<br>
				<label class="tableElem">Handphone 1:<em>*</em></label>
				<input type="text" title="Handphone 1">
				<br>
				<label class="tableElem">Handphone 2:</label>
				<input type="text" title="Handphone 2">
				<br>
				<label class="tableElem">Email:<em>*</em></label>
				<input type="text" title="Alamat email (TIDAK case sensitive, TANPA spasi)">
			</td>
			<td>
				<textarea rows="4" cols="50" title="Alamat lengkap rumah Tertanggung sesuai Bukti Identitas yang disertakan"></textarea>
				<br>
				<label>Kota:<em>*</em></label>
				<input type="text" title="Kota" size="20">
				<label>Kode Pos:<em>*</em></label>
				<input type="text" title="Kode pos" size="6">
				<br>
				<label class="tableElem">Telpon Rumah 1:<em>*</em></label>
				<input type="text" title="Telpon rumah 1" size="3">
				<input type="text" title="Telpon rumah 1">
				<br>
				<label class="tableElem">Telpon Rumah 2:</label>
				<input type="text" title="Telpon rumah 2" size="3">
				<input type="text" title="Telpon rumah 2">
				<br>
				<label class="tableElem">Handphone 1:<em>*</em></label>
				<input type="text" title="Handphone 1">
				<br>
				<label class="tableElem">Handphone 2:</label>
				<input type="text" title="Handphone 2">
				<br>
				<label class="tableElem">Email:<em>*</em></label>
				<input type="text" title="Alamat email (TIDAK case sensitive, TANPA spasi)">
			</td>
		</tr>
		<tr>
			<td>Alamat Kantor:</td>
			<td>
				<textarea rows="4" cols="50" title="Alamat lengkap kantor Pemegang Polis"></textarea>
				<br>
				<label>Kota:</label>
				<input type="text" title="Kota" size="20">
				<label>Kode Pos:</label>
				<input type="text" title="Kode pos" size="6">
				<br>
				<label class="tableElem">Telpon Kantor 1:</label>
				<input type="text" title="Telpon kantor 1" size="3">
				<input type="text" title="Telpon kantor 1">
				<br>
				<label class="tableElem">Telpon Kantor 2:</label>
				<input type="text" title="Telpon kantor 2" size="3">
				<input type="text" title="Telpon kantor 2">
				<br>
				<label class="tableElem">Fax:</label>
				<input type="text" title="Fax">
			</td>
			<td>
				<textarea rows="4" cols="50" title="Alamat lengkap kantor Tertanggung"></textarea>
				<br>
				<label>Kota:</label>
				<input type="text" title="Kota" size="20">
				<label>Kode Pos:</label>
				<input type="text" title="Kode pos" size="6">
				<br>
				<label class="tableElem">Telpon Kantor 1:</label>
				<input type="text" title="Telpon kantor 1" size="3">
				<input type="text" title="Telpon kantor 1">
				<br>
				<label class="tableElem">Telpon Kantor 2:</label>
				<input type="text" title="Telpon kantor 2" size="3">
				<input type="text" title="Telpon kantor 2">
				<br>
				<label class="tableElem">Fax:</label>
				<input type="text" title="Fax">
			</td>
		</tr>
		<tr>
			<td>Alamat Penagihan:<em>*</em></td>
			<td colspan="2">
				<select title="Silahkan pilih salah satu opsi">
					<option>-- Silahkan pilih --</option>
					<option>Sama dengan ALAMAT RUMAH</option>
					<option>Sama dengan ALAMAT KANTOR</option>
					<option>Lainnya</option>
				</select>
				<br>
				<textarea rows="4" cols="50" title="Alamat lengkap penagihan / korespondensi"></textarea>
				<br>
				<label>Kota:</label>
				<input type="text" title="Kota" size="20">
				<label>Kode Pos:</label>
				<input type="text" title="Kode pos" size="6">
				<br>
				<label class="tableElem">Telpon 1:<em>*</em></label>
				<input type="text" title="Telpon 1" size="3">
				<input type="text" title="Telpon 1">
				<br>
				<label class="tableElem">Telpon 2:</label>
				<input type="text" title="Telpon 2" size="3">
				<input type="text" title="Telpon 2">
				<br>
				<label class="tableElem">Telpon 3:</label>
				<input type="text" title="Telpon 3" size="3">
				<input type="text" title="Telpon 3">
				<br>
				<label class="tableElem">Handphone 1:</label>
				<input type="text" title="Handphone 1">
				<br>
				<label class="tableElem">Handphone 2:</label>
				<input type="text" title="Handphone 2">
				<br>
				<label class="tableElem">Fax:</label>
				<input type="text" title="Fax">
				<br>
				<label class="tableElem">Email:</label>
				<input type="text" title="Alamat email (TIDAK case sensitive, TANPA spasi)">
			</td>
		</tr>
		<tr>
			<td>Tujuan Membeli Asuransi:<em>*</em></td>
			<td>
				<select title="Tujuan membeli asuransi Pemegang Polis"><option>-----</option></select>
				<input type="text" title="Harus diisi apabila dipilih LAINNYA">
			</td>
			<td>
				<select title="Tujuan membeli asuransi Tertanggung"><option>-----</option></select>
				<input type="text" title="Harus diisi apabila dipilih LAINNYA">
			</td>
		</tr>
		<tr>
			<td>Perkiraan Penghasilan Kotor per Tahun:<em>*</em></td>
			<td>
				<select title="Perkiraan penghasilan kotor per tahun Pemegang Polis"><option>-----</option></select>
				<input type="text" title="Harus diisi apabila dipilih LAINNYA">
			</td>
			<td>
				<select title="Perkiraan penghasilan kotor per tahun Tertanggung"><option>-----</option></select>
				<input type="text" title="Harus diisi apabila dipilih LAINNYA">
			</td>
		</tr>
		<tr>
			<td>Sumber Pendanaan Pembelian Asuransi:<em>*</em></td>
			<td>
				<select title="Sumber pendanaan pembelian asuransi Pemegang Polis"><option>-----</option></select>
				<input type="text" title="Harus diisi apabila dipilih LAINNYA">
			</td>
			<td>
				<select title="Sumber pendanaan pembelian asuransi Tertanggung"><option>-----</option></select>
				<input type="text" title="Harus diisi apabila dipilih LAINNYA">
			</td>
		</tr>
		<tr>
			<td>Sumber Penghasilan:<em>*</em></td>
			<td>
				<select title="Sumber penghasilan Pemegang Polis"><option>-----</option></select>
				<input type="text" title="Harus diisi apabila dipilih LAINNYA">
			</td>
			<td>
				<select title="Sumber penghasilan Tertanggung"><option>-----</option></select>
				<input type="text" title="Harus diisi apabila dipilih LAINNYA">
			</td>
		</tr>
		<tr>
			<td>Klasifikasi Pekerjaan:<em>*</em></td>
			<td>
				<select title="Klasifikasi pekerjaan Pemegang Polis"><option>-----</option></select>
				<input type="text" title="Harus diisi apabila dipilih LAINNYA">
			</td>
			<td>
				<select title="Klasifikasi pekerjaan Tertanggung"><option>-----</option></select>
				<input type="text" title="Harus diisi apabila dipilih LAINNYA">
			</td>
		</tr>
		<tr>
			<td>Jabatan Pekerjaan:<em>*</em></td>
			<td><input type="text" title="Jabatan pada pekerjaan Pemegang Polis"></td>
			<td><input type="text" title="Jabatan pada pekerjaan Tertanggung"></td>
		</tr>
		<tr>
			<td>Klasifikasi Bidang Industri:<em>*</em></td>
			<td>
				<select title="Klasifikasi bidang industri Pemegang Polis"><option>-----</option></select>
				<input type="text" title="Harus diisi apabila dipilih LAINNYA">
			</td>
			<td>
				<select title="Klasifikasi bidang industri Tertanggung"><option>-----</option></select>
				<input type="text" title="Harus diisi apabila dipilih LAINNYA">
			</td>
		</tr>
	</table>
	--%>
	
	<!-- @@ Fieldset Data Pemegang -->
	
	<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>Pemegang Polis</div></legend>
	
		<div class="rowElem">
			<label>Pemegang Polis Badan Hukum / Usaha:<em>*</em></label>
			<select title="Apakah Pemegang Polis merupakan badan hukum / usaha?"><option>Tidak</option></select>
		</div>
		<div class="rowElem">
			<label>Nama Lengkap:<em>*</em></label>
			<select title="Gelar"><option>---</option></select>
			<input name="pemegang" type="text" title="Nama lengkap Pemegang Polis sesuai Bukti Identitas yang disertakan">
			<input type="text" size="5" title="Gelar">
		</div>
		<div class="rowElem">
			<label>Nama Ibu Kandung:<em>*</em></label>
			<input type="text" title="Nama gadis ibu kandung dari Pemegang Polis">
		</div>
		<div class="rowElem">
			<label>Bukti Identitas:<em>*</em></label>
			<select title="Bukti Identitas Pemegang Polis yang valid"><option>-----</option></select>
			<input type="text" title="Nomor bukti identitas Pemegang Polis">
		</div>
		<div class="rowElem">
			<label>Warga Negara:<em>*</em></label>
			<select title="Status warga negara Pemegang Polis"><option>-- Silahkan Pilih --</option></select>
		</div>	
		<div class="rowElem">
			<label>Tempat / Tanggal Lahir:<em>*</em></label>
			<input type="text" title="Kota/Negara kelahiran Pemegang Polis">
			<input type="text" class="datepicker" title="Tanggal lahir Pemegang Polis">
		</div>
		<div class="rowElem">
			<label>Jenis Kelamin:<em>*</em></label>
			<select title="Jenis kelamin Pemegang Polis"><option>-----</option></select>
		</div>
		<div class="rowElem">
			<label>Status:<em>*</em></label>
			<select title="Status pernikahan Pemegang Polis"><option>-- Silahkan Pilih --</option></select>
		</div>
		<div class="rowElem">
			<label>Keluarga:</label>
			<table class="jtable">
				<tr>
					<th>No</th>
					<th colspan="2">Nama</th>
					<th>Tgl Lahir</th>
				</tr>
				<tr>
					<td>1</td>
					<td>Suami / Istri</td>
					<td><input type="text" title="Nama"></td>
					<td><input type="text" class="datepicker" title="Tanggal lahir"></td>
				</tr>
				<tr>
					<td>2</td>
					<td>Anak 1</td>
					<td><input type="text" title="Nama"></td>
					<td><input type="text" class="datepicker" title="Tanggal lahir"></td>
				</tr>
				<tr>
					<td>3</td>
					<td>Anak 2</td>
					<td><input type="text" title="Nama"></td>
					<td><input type="text" class="datepicker" title="Tanggal lahir"></td>
				</tr>
			</table>
		</div>
		<div class="rowElem">
			<label>Agama:<em>*</em></label>
			<select title="Agama Pemegang Polis"><option>-- Silahkan Pilih --</option></select>
		</div>
		<div class="rowElem">
			<label>Pendidikan:<em>*</em></label>
			<select title="Tingkat pendidikan Pemegang Polis"><option>-- Silahkan Pilih --</option></select>
		</div>
			
	</fieldset>
	
	<!-- @@ Fieldset Data Alamat Rumah Pemegang -->
	<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>Alamat Rumah</div></legend>
	
		<div class="rowElem">
			<label>Alamat Rumah:<em>*</em></label>
			<textarea rows="4" cols="50" title="Alamat lengkap rumah Pemegang Polis sesuai Bukti Identitas yang disertakan"></textarea>
		</div>
		<div class="rowElem">
			<label>Kota:<em>*</em></label>
			<input type="text" title="Kota">
		</div>
		<div class="rowElem">
			<label>Kode Pos:<em>*</em></label>
			<input type="text" title="Kode pos">
		</div>
		<div class="rowElem">
			<label>Telpon Rumah 1:<em>*</em></label>
			<input type="text" title="Telpon rumah 1" size="3">
			<input type="text" title="Telpon rumah 1">
		</div>
		<div class="rowElem">
			<label>Telpon Rumah 2:</label>
			<input type="text" title="Telpon rumah 2" size="3">
			<input type="text" title="Telpon rumah 2">
		</div>
		<div class="rowElem">
			<label>Handphone 1:<em>*</em></label>
			<input type="text" title="Handphone 1">
		</div>
		<div class="rowElem">
			<label>Handphone 2:</label>
			<input type="text" title="Handphone 2">
		</div>
		<div class="rowElem">
			<label>Email:<em>*</em></label>
			<input type="text" title="Alamat email (TIDAK case sensitive, TANPA spasi)">
		</div>	
		
	</fieldset>		
	
	<!-- @@ Fieldset Data Alamat Kantor Pemegang -->			
	<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>Alamat Kantor</div></legend>
	
		<div class="rowElem">
			<label>Alamat Kantor:</label>
			<textarea rows="4" cols="50" title="Alamat lengkap kantor Pemegang Polis"></textarea>
		</div>
		<div class="rowElem">
			<label>Kota:</label>
			<input type="text" title="Kota">
		</div>
		<div class="rowElem">
			<label>Kode Pos:</label>
			<input type="text" title="Kode pos">
		</div>
		<div class="rowElem">
			<label>Telpon Kantor 1:</label>
			<input type="text" title="Telpon kantor 1" size="3">
			<input type="text" title="Telpon kantor 1">
		</div>
		<div class="rowElem">
			<label>Telpon Kantor 2:</label>
			<input type="text" title="Telpon kantor 2" size="3">
			<input type="text" title="Telpon kantor 2">
		</div>
		<div class="rowElem">
			<label>Fax:</label>
			<input type="text" title="Fax">
		</div>
		
	</fieldset>
		
	<!-- @@ Fieldset Data Alamat Penagihan / Korespondensi (Billing Address) -->			
	<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>Alamat Penagihan / Korespondensi (Billing Address)</div></legend>
	
		<div class="rowElem">
			<label>Silahkan Pilih:<em>*</em></label>
			<select title="Silahkan pilih salah satu opsi">
				<option>-- Silahkan pilih --</option>
				<option>Sama dengan ALAMAT RUMAH</option>
				<option>Sama dengan ALAMAT KANTOR</option>
				<option>Lainnya</option>
			</select>
		</div>
		<div class="rowElem">
			<label>Alamat Penagihan:</label>
			<textarea rows="4" cols="60" title="Alamat lengkap penagihan / korespondensi"></textarea>
		</div>
		<div class="rowElem">
			<label>Kota:</label>
			<input type="text" title="Kota">
		</div>
		<div class="rowElem">
			<label>Kode Pos:</label>
			<input type="text" title="Kode pos">
		</div>
		<div class="rowElem">
			<label>Telpon 1:<em>*</em></label>
			<input type="text" title="Telpon 1" size="3">
			<input type="text" title="Telpon 1">
		</div>
		<div class="rowElem">
			<label>Telpon 2:</label>
			<input type="text" title="Telpon 2" size="3">
			<input type="text" title="Telpon 2">
		</div>
		<div class="rowElem">
			<label>Telpon 3:</label>
			<input type="text" title="Telpon 3" size="3">
			<input type="text" title="Telpon 3">
		</div>
		<div class="rowElem">
			<label>Handphone 1:</label>
			<input type="text" title="Handphone 1">
		</div>
		<div class="rowElem">
			<label>Handphone 2:</label>
			<input type="text" title="Handphone 2">
		</div>
		<div class="rowElem">
			<label>Fax:</label>
			<input type="text" title="Fax">
		</div>
		<div class="rowElem">
			<label>Email:</label>
			<input type="text" title="Alamat email (TIDAK case sensitive, TANPA spasi)">
		</div>
			
	</fieldset>
		
	<!-- @@ Fieldset KYC (Know Your Customer) -->			
	<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>KYC (Know Your Customer)</div></legend>
	
		<div class="rowElem">
			<label>Tujuan Membeli Asuransi:<em>*</em></label>
			<select title="Tujuan membeli asuransi Pemegang Polis"><option>-----</option></select>
			<input type="text" title="Harus diisi apabila dipilih LAINNYA">
		</div>
		<div class="rowElem">
			<label>Perkiraan Penghasilan Kotor per Tahun:<em>*</em></label>
			<select title="Perkiraan penghasilan kotor per tahun Pemegang Polis"><option>-----</option></select>
			<input type="text" title="Harus diisi apabila dipilih LAINNYA">
		</div>
		<div class="rowElem">
			<label>Sumber Pendanaan Pembelian Asuransi:<em>*</em></label>
			<select title="Sumber pendanaan pembelian asuransi Pemegang Polis"><option>-----</option></select>
			<input type="text" title="Harus diisi apabila dipilih LAINNYA">
		</div>
		<div class="rowElem">
			<label>Sumber Penghasilan:<em>*</em></label>
			<select title="Sumber penghasilan Pemegang Polis"><option>-----</option></select>
			<input type="text" title="Harus diisi apabila dipilih LAINNYA">
		</div>
		<div class="rowElem">
			<label>Klasifikasi Pekerjaan:<em>*</em></label>
			<select title="Klasifikasi pekerjaan Pemegang Polis"><option>-----</option></select>
			<input type="text" title="Harus diisi apabila dipilih LAINNYA">
		</div>
		<div class="rowElem">
			<label>Jabatan Pekerjaan:<em>*</em></label>
			<input type="text" title="Jabatan pada pekerjaan Pemegang Polis">
		</div>
		<div class="rowElem">
			<label>Klasifikasi Bidang Industri:<em>*</em></label>
			<select title="Klasifikasi bidang industri Pemegang Polis"><option>-----</option></select>
			<input type="text" title="Harus diisi apabila dipilih LAINNYA">
		</div>
		
	</fieldset>
		
	<!-- @@ Fieldset Rekening Pemegang Polis -->			
	<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>Rekening Pemegang Polis (yang digunakan untuk Pembayaran Manfaat)</div></legend>
	
		<div class="rowElem">
			<label>Bank:<em>*</em></label>
			<input id="acBankPusat" type="text" title="Nama bank">
		</div>
		<div class="rowElem">
			<label>Cabang:<em>*</em></label>
			<input type="text" title="Cabang bank">
		</div>
		<div class="rowElem">
			<label>Kota:<em>*</em></label>
			<input type="text" title="Kota">
		</div>
		<div class="rowElem">
			<label>No Rekening:<em>*</em></label>
			<input type="text" title="Nomor rekening">
		</div>
		<div class="rowElem">
			<label>Atas Nama:<em>*</em></label>
			<input type="text" title="Nama pemegang rekening">
		</div>
		<div class="rowElem">
			<label>Jenis Tabungan:</label>
			<input type="text" title="Jenis tabungan">
		</div>
		<div class="rowElem">
			<label>Jenis Nasabah:</label>
			<input type="text" title="Jenis nasabah">
		</div>
		<hr>
		<div class="rowElem">
			<label>Memberi Kuasa:</label>
			<input type="checkbox">
		</div>
		<div class="rowElem">
			<label>Keterangan:</label>
			<textarea rows="3" cols="40" title="Diisi bila nama pada rekening berbeda dengan nama Pemegang Polis"></textarea>
		</div>
		<div class="rowElem">
			<label>Tanggal Surat:</label>
			<input type="text" class="datepicker" title="Diisi bila nama pada rekening berbeda dengan nama Pemegang Polis">
		</div>
		
	</fieldset>

</form>