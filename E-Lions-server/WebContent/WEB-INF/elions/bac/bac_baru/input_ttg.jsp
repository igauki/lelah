<%@ include file="/include/page/header_bacbaru.jsp"%>
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">

<c:set var="path" value="${pageContext.request.contextPath}" />

<form method="post" id="formPost" name="formPost">

<!-- @@ Fieldset Button Page -->
	<input id="submitType" name="submitType" type="hidden" value=${submitType}>
	<input type="submit" name="_target0" value=" " 
			class="bacNavigation" style="background-image: url(${path}/include/image/ddu1.jpg);">
	<input type="submit" name="_target1" value=" "
			class="bacNavigation" style="background-image: url(${path}/include/image/inv1.jpg);">
	<input type="submit" name="_target2" value=" "
			class="bacNavigation" style="background-image: url(${path}/include/image/pp1.jpg);">
	<input type="submit" name="_target3" value=" "
			class="bacNavigation" style="background-image: url(${path}/include/image/ttg2.jpg);">
	<input type="submit" name="_target4" value=" "
			class="bacNavigation" style="background-image: url(${path}/include/image/ag1.jpg);">
	<input type="submit" name="_target5" value=" "
			class="bacNavigation" style="background-image: url(${path}/include/image/kf1.jpg);">
	<input type="button" name="dummy2" value=" "
			class="bacNavigation" style="background-image: url(${path}/include/image/sb1.jpg);">
	
	<div style="text-align: center;">
		</br>				
		<input type="submit" id="prevsubmit" name="_target${halaman-1}"
			value="Prev &laquo;" >
		<input type="submit" id="nowsubmit" name="_target${halaman+1}"
			value="Next &raquo;">
	</div>
		
	<input type="hidden" name="hal" value="${halaman}">
	<input type="hidden" name="_page" value="${halaman}">

	<!-- @@ Relasi Pemegang dgn Tertanggung -->			
	<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>Hubungan / Relasi Calon Pemegang Polis dengan Calon Tertanggung</div></legend>
	
		<div class="rowElem">
			<label>Hubungan / Relasi:<em>*</em></label>
			<select title="Hubungan / Relasi Calon Pemegang Polis dengan Calon Tertanggung">
				<option>--- Silahkan Pilih ---</option>
			</select>
		</div>
	
	</fieldset>	
	
	<!-- @@ Fieldset Data Tertanggung -->
	
	<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>Tertanggung</div></legend>
	
		<div class="rowElem">
			<label>Nama Lengkap:<em>*</em></label>
			<select title="Gelar"><option>---</option></select>
			<input type="text" title="Nama lengkap Tertanggung sesuai Bukti Identitas yang disertakan">
			<input type="text" size="5" title="Gelar">
		</div>
		<div class="rowElem">
			<label>Nama Ibu Kandung:<em>*</em></label>
			<input type="text" title="Nama gadis ibu kandung dari Tertanggung">
		</div>
		<div class="rowElem">
			<label>Bukti Identitas:<em>*</em></label>
			<select title="Bukti Identitas Tertanggung yang valid"><option>-----</option></select>
			<input type="text" title="Nomor bukti identitas Tertanggung">
		</div>
		<div class="rowElem">
			<label>Warga Negara:<em>*</em></label>
			<select title="Status warga negara Tertanggung"><option>-- Silahkan Pilih --</option></select>
		</div>	
		<div class="rowElem">
			<label>Tempat / Tanggal Lahir:<em>*</em></label>
			<input type="text" title="Kota/Negara kelahiran Tertanggung">
			<input type="text" class="datepicker" title="Tanggal lahir Tertanggung">
		</div>
		<div class="rowElem">
			<label>Jenis Kelamin:<em>*</em></label>
			<select title="Jenis kelamin Tertanggung"><option>-----</option></select>
		</div>
		<div class="rowElem">
			<label>Status:<em>*</em></label>
			<select title="Status pernikahan Tertanggung"><option>-- Silahkan Pilih --</option></select>
		</div>
		<div class="rowElem">
			<label>Keluarga:</label>
			<table class="jtable">
				<tr>
					<th>No</th>
					<th colspan="2">Nama</th>
					<th>Tgl Lahir</th>
				</tr>
				<tr>
					<td>1</td>
					<td>Suami / Istri</td>
					<td><input type="text" title="Nama"></td>
					<td><input type="text" class="datepicker" title="Tanggal lahir"></td>
				</tr>
				<tr>
					<td>2</td>
					<td>Anak 1</td>
					<td><input type="text" title="Nama"></td>
					<td><input type="text" class="datepicker" title="Tanggal lahir"></td>
				</tr>
				<tr>
					<td>3</td>
					<td>Anak 2</td>
					<td><input type="text" title="Nama"></td>
					<td><input type="text" class="datepicker" title="Tanggal lahir"></td>
				</tr>
			</table>
		</div>
		<div class="rowElem">
			<label>Agama:<em>*</em></label>
			<select title="Agama Tertanggung"><option>-- Silahkan Pilih --</option></select>
		</div>
		<div class="rowElem">
			<label>Pendidikan:<em>*</em></label>
			<select title="Tingkat pendidikan Tertanggung"><option>-- Silahkan Pilih --</option></select>
		</div>
			
	</fieldset>
	
	<!-- @@ Fieldset Data Alamat Rumah Tertanggung -->
	<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>Alamat Rumah</div></legend>
	
		<div class="rowElem">
			<label>Alamat Rumah:<em>*</em></label>
			<textarea rows="4" cols="50" title="Alamat lengkap rumah Tertanggung sesuai Bukti Identitas yang disertakan"></textarea>
		</div>
		<div class="rowElem">
			<label>Kota:<em>*</em></label>
			<input type="text" title="Kota">
		</div>
		<div class="rowElem">
			<label>Kode Pos:<em>*</em></label>
			<input type="text" title="Kode pos">
		</div>
		<div class="rowElem">
			<label>Telpon Rumah 1:<em>*</em></label>
			<input type="text" title="Telpon rumah 1" size="3">
			<input type="text" title="Telpon rumah 1">
		</div>
		<div class="rowElem">
			<label>Telpon Rumah 2:</label>
			<input type="text" title="Telpon rumah 2" size="3">
			<input type="text" title="Telpon rumah 2">
		</div>
		<div class="rowElem">
			<label>Handphone 1:<em>*</em></label>
			<input type="text" title="Handphone 1">
		</div>
		<div class="rowElem">
			<label>Handphone 2:</label>
			<input type="text" title="Handphone 2">
		</div>
		<div class="rowElem">
			<label>Email:<em>*</em></label>
			<input type="text" title="Alamat email (TIDAK case sensitive, TANPA spasi)">
		</div>	
		
	</fieldset>		
	
	<!-- @@ Fieldset Data Alamat Kantor Tertanggung -->			
	<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>Alamat Kantor</div></legend>
	
		<div class="rowElem">
			<label>Alamat Kantor:</label>
			<textarea rows="4" cols="50" title="Alamat lengkap kantor Tertanggung"></textarea>
		</div>
		<div class="rowElem">
			<label>Kota:</label>
			<input type="text" title="Kota">
		</div>
		<div class="rowElem">
			<label>Kode Pos:</label>
			<input type="text" title="Kode pos">
		</div>
		<div class="rowElem">
			<label>Telpon Kantor 1:</label>
			<input type="text" title="Telpon kantor 1" size="3">
			<input type="text" title="Telpon kantor 1">
		</div>
		<div class="rowElem">
			<label>Telpon Kantor 2:</label>
			<input type="text" title="Telpon kantor 2" size="3">
			<input type="text" title="Telpon kantor 2">
		</div>
		<div class="rowElem">
			<label>Fax:</label>
			<input type="text" title="Fax">
		</div>
		
	</fieldset>
		
	<!-- @@ Fieldset KYC (Know Your Customer) -->			
	<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>KYC (Know Your Customer)</div></legend>
	
		<div class="rowElem">
			<label>Tujuan Membeli Asuransi:<em>*</em></label>
			<select title="Tujuan membeli asuransi Tertanggung"><option>-----</option></select>
			<input type="text" title="Harus diisi apabila dipilih LAINNYA">
		</div>
		<div class="rowElem">
			<label>Perkiraan Penghasilan Kotor per Tahun:<em>*</em></label>
			<select title="Perkiraan penghasilan kotor per tahun Tertanggung"><option>-----</option></select>
			<input type="text" title="Harus diisi apabila dipilih LAINNYA">
		</div>
		<div class="rowElem">
			<label>Sumber Pendanaan Pembelian Asuransi:<em>*</em></label>
			<select title="Sumber pendanaan pembelian asuransi Tertanggung"><option>-----</option></select>
			<input type="text" title="Harus diisi apabila dipilih LAINNYA">
		</div>
		<div class="rowElem">
			<label>Sumber Penghasilan:<em>*</em></label>
			<select title="Sumber penghasilan Tertanggung"><option>-----</option></select>
			<input type="text" title="Harus diisi apabila dipilih LAINNYA">
		</div>
		<div class="rowElem">
			<label>Klasifikasi Pekerjaan:<em>*</em></label>
			<select title="Klasifikasi pekerjaan Tertanggung"><option>-----</option></select>
			<input type="text" title="Harus diisi apabila dipilih LAINNYA">
		</div>
		<div class="rowElem">
			<label>Jabatan Pekerjaan:<em>*</em></label>
			<input type="text" title="Jabatan pada pekerjaan Tertanggung">
		</div>
		<div class="rowElem">
			<label>Klasifikasi Bidang Industri:<em>*</em></label>
			<select title="Klasifikasi bidang industri Tertanggung"><option>-----</option></select>
			<input type="text" title="Harus diisi apabila dipilih LAINNYA">
		</div>
		
	</fieldset>
	
	<!-- @@ Data Calon Tertanggung Tambahan -->			
	<fieldset class="ui-widget ui-widget-content">
		<legend class="ui-widget-header ui-corner-all"><div>Tertanggung Tambahan (Khusus untuk Eka Sehat dan HCP Family)</div></legend>
		
		<table class="jtable">
			<tr>
				<th>No</th>
				<th>Nama</th>
				<th>Relasi</th>
				<th>Sex</th>
				<th>Tgl Lahir</th>
				<th>Umur</th>
				<th>Premi</th>
				<th>Produk</th>
			</tr>
			<tr>
				<td>1</td>
				<td><input type="text" title="Nama tertanggung tambahan"></td>
				<td><input type="text" title="Relasi tertanggung tambahan dengan Pemegang Polis"></td>
				<td>
					<input type="radio" id="radio1" name="sex"><label for="radio1" title="Laki-laki">L</label>
					<input type="radio" id="radio2" name="sex"><label for="radio2" title="Perempuan">P</label>
				</td>
				<td><input type="text" class="datepicker" title="Tanggal lahir tertanggung tambahan"></td>
				<td><input type="text" title="Umur tertanggung tambahan" size="3" disabled="disabled"></td>
				<td><input type="text" title="Premi tertanggung tambahan"></td>
				<td><input type="text" title="Produk tertanggung tambahan"></td>
			</tr>
		</table>
		
	</fieldset>
	
</form>
