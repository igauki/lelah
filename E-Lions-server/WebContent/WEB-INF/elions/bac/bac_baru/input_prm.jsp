<!-- @@ Relasi Pemegang dgn Pembayar Premi -->			
<fieldset class="ui-widget ui-widget-content">
	<legend class="ui-widget-header ui-corner-all"><div>Hubungan / Relasi Calon Pemegang Polis dengan Pembayar Premi</div></legend>

	<div class="rowElem">
		<label>Hubungan / Relasi:<em>*</em></label>
		<select title="Hubungan / Relasi Calon Pemegang Polis dengan Pembayar Premi">
			<option>--- Silahkan Pilih ---</option>
		</select>
	</div>

</fieldset>	

<!-- @@ Fieldset Data Pembayar premi -->

<fieldset class="ui-widget ui-widget-content">
	<legend class="ui-widget-header ui-corner-all"><div>Pembayar Premi</div></legend>

	<div class="rowElem">
		<label>Pembayar Premi Badan Hukum / Usaha:<em>*</em></label>
		<select title="Apakah Pembayar Premi merupakan badan hukum / usaha?"><option>Tidak</option></select>
	</div>
	<div class="rowElem">
		<label>Nama Lengkap:<em>*</em></label>
		<select title="Gelar"><option>---</option></select>
		<input name="Pembayar premi" type="text" title="Nama lengkap Pembayar Premi sesuai Bukti Identitas yang disertakan">
		<input type="text" size="5" title="Gelar">
	</div>
	<div class="rowElem">
		<label>Nama Ibu Kandung:<em>*</em></label>
		<input type="text" title="Nama gadis ibu kandung dari Pembayar Premi">
	</div>
	<div class="rowElem">
		<label>Bukti Identitas:<em>*</em></label>
		<select title="Bukti Identitas Pembayar Premi yang valid"><option>-----</option></select>
		<input type="text" title="Nomor bukti identitas Pembayar Premi">
	</div>
	<div class="rowElem">
		<label>Warga Negara:<em>*</em></label>
		<select title="Status warga negara Pembayar Premi"><option>-- Silahkan Pilih --</option></select>
	</div>	
	<div class="rowElem">
		<label>Tempat / Tanggal Lahir:<em>*</em></label>
		<input type="text" title="Kota/Negara kelahiran Pembayar Premi">
		<input type="text" class="datepicker" title="Tanggal lahir Pembayar Premi">
	</div>
	<div class="rowElem">
		<label>Jenis Kelamin:<em>*</em></label>
		<select title="Jenis kelamin Pembayar Premi"><option>-----</option></select>
	</div>
	<div class="rowElem">
		<label>Status:<em>*</em></label>
		<select title="Status pernikahan Pembayar Premi"><option>-- Silahkan Pilih --</option></select>
	</div>
	<div class="rowElem">
		<label>Keluarga:</label>
		<table class="jtable">
			<tr>
				<th>No</th>
				<th colspan="2">Nama</th>
				<th>Tgl Lahir</th>
			</tr>
			<tr>
				<td>1</td>
				<td>Suami / Istri</td>
				<td><input type="text" title="Nama"></td>
				<td><input type="text" class="datepicker" title="Tanggal lahir"></td>
			</tr>
			<tr>
				<td>2</td>
				<td>Anak 1</td>
				<td><input type="text" title="Nama"></td>
				<td><input type="text" class="datepicker" title="Tanggal lahir"></td>
			</tr>
			<tr>
				<td>3</td>
				<td>Anak 2</td>
				<td><input type="text" title="Nama"></td>
				<td><input type="text" class="datepicker" title="Tanggal lahir"></td>
			</tr>
		</table>
	</div>
	<div class="rowElem">
		<label>Agama:<em>*</em></label>
		<select title="Agama Pembayar Premi"><option>-- Silahkan Pilih --</option></select>
	</div>
	<div class="rowElem">
		<label>Pendidikan:<em>*</em></label>
		<select title="Tingkat pendidikan Pembayar Premi"><option>-- Silahkan Pilih --</option></select>
	</div>
		
</fieldset>

<!-- @@ Fieldset Data Alamat Rumah Pembayar premi -->
<fieldset class="ui-widget ui-widget-content">
	<legend class="ui-widget-header ui-corner-all"><div>Alamat Rumah</div></legend>

	<div class="rowElem">
		<label>Alamat Rumah:<em>*</em></label>
		<textarea rows="4" cols="50" title="Alamat lengkap rumah Pembayar Premi sesuai Bukti Identitas yang disertakan"></textarea>
	</div>
	<div class="rowElem">
		<label>Kota:<em>*</em></label>
		<input type="text" title="Kota">
	</div>
	<div class="rowElem">
		<label>Kode Pos:<em>*</em></label>
		<input type="text" title="Kode pos">
	</div>
	<div class="rowElem">
		<label>Telpon Rumah 1:<em>*</em></label>
		<input type="text" title="Telpon rumah 1" size="3">
		<input type="text" title="Telpon rumah 1">
	</div>
	<div class="rowElem">
		<label>Telpon Rumah 2:</label>
		<input type="text" title="Telpon rumah 2" size="3">
		<input type="text" title="Telpon rumah 2">
	</div>
	<div class="rowElem">
		<label>Handphone 1:<em>*</em></label>
		<input type="text" title="Handphone 1">
	</div>
	<div class="rowElem">
		<label>Handphone 2:</label>
		<input type="text" title="Handphone 2">
	</div>
	<div class="rowElem">
		<label>Email:<em>*</em></label>
		<input type="text" title="Alamat email (TIDAK case sensitive, TANPA spasi)">
	</div>	
	
</fieldset>		

<!-- @@ Fieldset KYC (Know Your Customer) -->			
<fieldset class="ui-widget ui-widget-content">
	<legend class="ui-widget-header ui-corner-all"><div>KYC (Know Your Customer)</div></legend>

	<div class="rowElem">
		<label>Tujuan Membeli Asuransi:<em>*</em></label>
		<select title="Tujuan membeli asuransi Pembayar Premi"><option>-----</option></select>
		<input type="text" title="Harus diisi apabila dipilih LAINNYA">
	</div>
	<div class="rowElem">
		<label>Perkiraan Penghasilan Kotor per Tahun:<em>*</em></label>
		<select title="Perkiraan penghasilan kotor per tahun Pembayar Premi"><option>-----</option></select>
		<input type="text" title="Harus diisi apabila dipilih LAINNYA">
	</div>
	<div class="rowElem">
		<label>Sumber Pendanaan Pembelian Asuransi:<em>*</em></label>
		<select title="Sumber pendanaan pembelian asuransi Pembayar Premi"><option>-----</option></select>
		<input type="text" title="Harus diisi apabila dipilih LAINNYA">
	</div>
	<div class="rowElem">
		<label>Sumber Penghasilan:<em>*</em></label>
		<select title="Sumber penghasilan Pembayar Premi"><option>-----</option></select>
		<input type="text" title="Harus diisi apabila dipilih LAINNYA">
	</div>
	<div class="rowElem">
		<label>Klasifikasi Pekerjaan:<em>*</em></label>
		<select title="Klasifikasi pekerjaan Pembayar Premi"><option>-----</option></select>
		<input type="text" title="Harus diisi apabila dipilih LAINNYA">
	</div>
	<div class="rowElem">
		<label>Jabatan Pekerjaan:<em>*</em></label>
		<input type="text" title="Jabatan pada pekerjaan Pembayar Premi">
	</div>
	<div class="rowElem">
		<label>Klasifikasi Bidang Industri:<em>*</em></label>
		<select title="Klasifikasi bidang industri Pembayar Premi"><option>-----</option></select>
		<input type="text" title="Harus diisi apabila dipilih LAINNYA">
	</div>
	
</fieldset>