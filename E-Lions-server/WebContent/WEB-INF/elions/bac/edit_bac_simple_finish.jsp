<%@ include file="/include/page/header.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>

<script>
	if('${status}' == 'input' && '${status_submit}' == 'baru')
	{
		alert('Selama belum ditransfer / approve, anda bisa melakukan perubahan data dengan menekan tombol edit');	
	}
	

function cariregion(spaj,nama)
	{
	if (spaj !="")
	{
		var dok;
		if(self.opener)
			dok = self.opener.document;
		else
			dok = parent.document;
		
		var forminput;
		if(dok.formpost) forminput = dok.formpost;
		else if(dok.frmParam) forminput = dok.frmParam;
		addOptionToSelect(dok, forminput.spaj, spaj, spaj);
		
			/*ajaxManager.listcollect(spaj , nama,
			{callback:function(map) {
		
				DWRUtil.useLoadingMessage();
				document.frmParam.koderegion.value=map.LSRG_NAMA;
				document.frmParam.kodebisnis.value = map.LSBS_ID;
				document.frmParam.numberbisnis.value = map.LSDBS_NUMBER;
				document.frmParam.jml_peserta.value = map.jml_peserta;
				document.frmParam.cab_bank.value=map.CAB_BANK;
				document.frmParam.jn_bank.value=map.JN_BANK;
				document.frmParam.lus_id.value = map.LUS_ID;
			   },
			  timeout:15000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});*/
	}
}
</script>

<body style="height: 100%;" onLoad="cariregion('${nomorspaj}','region');">

<form name="frmParam" method="post">
<div class="tabcontent">
<table class="entry2" style="width: 98.4%;">

	<tr>
		<td colspan="3">
			<c:if test="${not empty err}">
			<div id="error">ERROR:<br>
			${err}
			</div>
			</c:if>
		</td>
	</tr>
</table>
		<input type="hidden" name="spaj" value=${nomorspaj}>
		<c:if test="${status eq \"input\"}">
			<c:if test="${nomorspaj eq \"\"}">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td bgcolor="#DDDDDD">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td width="67%" rowspan="12" align="left" bgcolor="#800000">

							<p align="center">
								<font face="Verdana" size="2" color="#FFFFFF"> <br>
									<center>
										<b> Spaj tidak berhasil di submit. Silahkan dicoba kembali
											atau hubungi administrator. </b>
										<br>
										<input type="button" name="ref" value="Coba Kembali"
											onclick="parent.document.getElementById('infoFrame').src='${path}/bac/editsimple.htm>
									</center> </font>
							</p>
						</td>
					</tr>
				</table>
			</c:if>
			<c:if test="${nomorspaj ne \"\" and empty sessionScope.currentUser.cab_bank}">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td bgcolor="#DDDDDD">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td width="67%" rowspan="12" align="left" bgcolor="#800000">

							<p align="center">
								<font face="Verdana" size="2" color="#FFFFFF"> <br>
									<center>
										<b> Spaj berhasil di edit. Silahkan catat nomor spaj : <elions:spaj
												nomor="${nomorspaj}" /> </b>
									</center> </font>
							</p>
						</td>
					</tr>
				</table>
			</c:if>
		</c:if>
		<c:if test="${status eq \"edit\"}">
			<c:if test="${nomorspaj eq \"\"}">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td bgcolor="#DDDDDD">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td width="67%" rowspan="12" align="left" bgcolor="#800000">

							<p align="center">
								<font face="Verdana" size="2" color="#FFFFFF"> <br>
									<center>
										<b> Spaj tidak berhasil di edit. Silahkan dicoba kembali
											atau hubungi administrator. </b>
										<br>
										<input type="button" name="ref" value="Coba Kembali"
											onclick="parent.document.getElementById('infoFrame').src='${path}/bac/editsimple.htm>
									</center> </font>
							</p>
						</td>
					</tr>
				</table>
			</c:if>
			<c:if test="${nomorspaj ne \"\" and empty sessionScope.currentUser.cab_bank}">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td bgcolor="#DDDDDD">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td width="67%" rowspan="12" align="left" bgcolor="#800000">

							<p align="center">
								<font face="Verdana" size="2" color="#FFFFFF"> <br>
									<center>
										<b> Spaj berhasil di edit. Silahkan catat nomor spaj : <elions:spaj
												nomor="${nomorspaj}" /> </b>
									</center> </font>
							</p>
						</td>
					</tr>
				</table>
			</c:if>
		</c:if>
		</div>
	</form>

</body>
<%@ include file="/include/page/footer.jsp"%>
