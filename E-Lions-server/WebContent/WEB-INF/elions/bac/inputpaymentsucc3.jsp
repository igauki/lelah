<%@ include file="/include/page/header.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<script>

	function copyData(v_payment_id, v_mode){
		var formpost = document.getElementById('formpost');
		if(formpost){
			if(formpost.mode) formpost.mode.value = v_mode;
			if(formpost.mspa_payment_id) formpost.mspa_payment_id.value = v_payment_id;
			if(formpost.elements['_target3']) formpost.elements['_target3'].click();
		}
	}

</script>
<body onload="setupPanes('container1', 'tab3');" style="height: 100%;">
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" id="tab1">1. Input SPAJ/Polis</a>
				<a href="#" id="tab2">2. Pilih Tagihan</a>
				<a href="#" onClick="return showPane('pane3', this)" id="tab3">3. Input Pembayaran</a>
				<a href="#" id="tab4">4. Selesai</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane3" class="panes">
				<form:form id="formpost" commandName="cmd">
				<fieldset>
					<legend>History Pembayaran</legend>
					<table class="displaytag">
						<thead>
							<tr>
								<th>Ke</th>
								<th>Tipe</th>
								<th>Pembayaran</th>
								<th>Kurs</th>
								<th>Tgl Bayar</th>
								<th>Tgl RK</th>
								<th>Cara Bayar</th>
								<th>Rekening AJS</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${cmd.daftarPayment}" var="payment" varStatus="st">
								<!-- bedanya diwarna saja koq -->
								<c:choose>
									<c:when test="${payment.mspa_payment_id eq cmd.mspa_payment_id}">
										<tr style="background-color: #FFFFCC">
											<td >${payment.ke}</td>
											<td>${payment.tipe}</td>
											<td>${payment.lku_symbol} <fmt:formatNumber value="${payment.mstp_value}" currencySymbol="${payment.lku_symbol}" maxFractionDigits="2" minFractionDigits="2"/></td>
											<td><fmt:formatNumber value="${payment.mspa_nilai_kurs}" maxFractionDigits="0" minFractionDigits="0"/></td>
											<td><fmt:formatDate value="${payment.mspa_pay_date}" pattern="dd/MM/yyyy"/></td>
											<td><fmt:formatDate value="${payment.mspa_date_book}" pattern="dd/MM/yyyy"/></td>
											<td>${payment.lsjb_type}</td>
											<td>${payment.lre_acc_no} [${payment.lku_rek}] ${payment.lsbp_nama} ${payment.lbn_nama}</td>
										</tr>
									</c:when>
									<c:otherwise>
										<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;" onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
											onclick="copyData('${payment.mspa_payment_id}', 'update');">
											<td>${payment.ke}</td>
											<td>${payment.tipe}</td>
											<td>${payment.lku_symbol} <fmt:formatNumber value="${payment.mstp_value}" currencySymbol="${payment.lku_symbol}" maxFractionDigits="2" minFractionDigits="2"/></td>
											<td><fmt:formatNumber value="${payment.mspa_nilai_kurs}" maxFractionDigits="0" minFractionDigits="0"/></td>
											<td><fmt:formatDate value="${payment.mspa_pay_date}" pattern="dd/MM/yyyy"/></td>
											<td><fmt:formatDate value="${payment.mspa_date_book}" pattern="dd/MM/yyyy"/></td>
											<td>${payment.lsjb_type}</td>
											<td>${payment.lre_acc_no} [${payment.lku_rek}] ${payment.lsbp_nama} ${payment.lbn_nama}</td>
										</tr>
									</c:otherwise>
								</c:choose>
							</c:forEach>
							<tr>
								<td colspan="8" style="text-align: left;">
									<input type="button" value="Input Pembayaran Baru..." onclick="copyData('', 'insert');">
									<form:hidden path="mode"/>
									<form:hidden path="mspa_payment_id"/>
								</td>
							</tr>
						</tbody>
					</table>
					<div id="success">Catatan: ${catatan}</div>
				</fieldset>
				<c:if test="${not empty cmd.mode}">
					<fieldset>
						<legend>BANK AJ SINARMAS</legend>
						<table class="entry2" style="width: auto;">
							<tr>
								<th style="width: 150px;">Rekening AJS: <span class="error">*</span></th>
								<th style="width: 600px;">
									<form:select cssErrorClass="inpError" path="payment.lsrek_id">
										<form:option value=""/>
										<form:options items="${daftarRekeningAJS}" itemValue="desc" itemLabel="value"/>
									</form:select>
								</th>
							</tr>
							<tr>
								<th>Tgl RK: <span class="error">*</span></th>
								<th>
									<spring:bind path="cmd.payment.mspa_date_book">
										<script>inputDate('${status.expression}', '${status.value}', false);</script>
									</spring:bind>							
									No Pre: 
									<input type="text" class="readOnly" readonly="readonly" value="${cmd.payment.mspa_no_pre}">
									No Voucher: 
									<input type="text" class="readOnly" readonly="readonly" value="${cmd.payment.mspa_no_voucher}">
							</tr>
						</table>
					</fieldset>
					<fieldset>
						<legend>PEMBAYARAN</legend>
						<table class="entry2" style="width: auto;">
							<tr>
								<th style="width: 150px;">Tgl Bayar: <span class="error">*</span></th>
								<th style="width: 600px;">
									<spring:bind path="cmd.payment.pay_date">
										<script>inputDate('${status.expression}', '${status.value}', false);</script>
									</spring:bind>							
									Cara Bayar: <span class="error">*</span>
									<spring:bind path="cmd.payment.lsjb_id">
										<select name="${status.expression}">
											<c:forEach items="${daftarPaymentType}" var="p">
												<option value="${p.LSJB_ID}"
													<c:if test="${status.value eq p.LSJB_ID}"></c:if>
												>${p.LSJB_TYPE}</option>
											</c:forEach>
										</select>
									</spring:bind>
									Ref Polis No: 
									<form:input path="payment.mspa_old_policy"/>
								</th>
							</tr>
							<tr>
								<th>Mata Uang: <span class="error">*</span></th>
								<th>
									<form:select path="payment.lku_id" cssErrorClass="inpError">
										<form:option value=""/>
										<form:option value="01" label="Rp."/>
										<form:option value="02" label="US$"/>
									</form:select>
									Jumlah Bayar: <span class="error">*</span>
									<form:input path="payment.mspa_payment" cssErrorClass="inpError"/>
									Kurs: 
									<form:input path="payment.mspa_nilai_kurs" cssClass="readOnly" readonly="true"/>
								</th>
							</tr>						
							<tr>
								<th>Keterangan:</th>
								<th>
									<form:textarea path="payment.mspa_desc" cols="105" rows="5"/>
								</th>							
							</tr>
						</table>
					</fieldset>
					<fieldset>
						<legend>DETAIL PEMBAYARAN</legend>
						<table class="entry2" style="width: auto;">
							<tr>
								<th style="width: 150px;">Client Bank: <span class="error">*</span></th>
								<th style="width: 600px;">
									<spring:bind path="cmd.payment.client_bank">
										<select name="${status.expression}">
											<option value="" />
											<c:forEach var="bank" items="${listBankPusat}">
												<option value="${bank.LSBP_ID}"
													<c:if test="${bank.LSBP_ID eq status.value}">selected</c:if>
												>${bank.LSBP_NAMA }</option>
											</c:forEach>
										</select>
									</spring:bind>
									No Giro/Cek/Credit Card: 
									<form:input path="payment.mspa_no_rek"/>
								</th>
							</tr>
							<tr>
								<th>Tgl Jatuh Tempo:</th>
								<th>
									<spring:bind path="cmd.payment.mspa_due_date">	
										<script>inputDate('${status.expression}', '${status.value}', false);</script>
									</spring:bind>
								</th>
							</tr>						
						</table>
					</fieldset>
				</c:if>
				<fieldset>
					<table class="entry2">
						<tr>
							<td>
								<spring:hasBindErrors name="cmd">
									<div id="error">
										ERROR:<br>
										<form:errors path="*" />
									</div>
								</spring:hasBindErrors>
								<input type="submit" name="_target1" value="&laquo; Prev" accesskey="P">
								<input type="submit" name="_finish" value="Save &raquo;" accesskey="S" <c:if test="${empty cmd.mode}"> style="visibility: hidden;" </c:if>>
								<input type="submit" name="_target3" style="visibility: hidden;">
							</td>
						</tr>
					</table>
				</fieldset>
				</form:form>
			</div>
			
		</div>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>