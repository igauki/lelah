<%@ include file="/include/page/header_jquery.jsp"%>
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		$("#tabs").tabs();
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		$("#Ok").hide();
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
	  	$("#status").change(function() {
			var x=$("#status").val();			
			$("#substatus").empty();		
			if(x ==15 || x==16){
			 if(x!=15){
			 	$("#Ok").show();$("#Send").hide();$("#emailAd").hide();$("#emailTo").hide();
			 }else{
			 		$("#Ok").hide();$("#Send").show();$("#emailAd").show();$("#emailTo").show();
			 }
			  var url = "${path }/uw/uw.htm?window=emailAuto&jsonStatus=1&l="+x;
			   $.getJSON(url, function(result) {
			   				
					$.each(result, function() {
						$("<option/>").val(this.SUB_ID).html(this.SUB_DESC).appendTo("#substatus");
					});
					$("#substatus option:first").attr('selected','selected');
				});	
			
			}
			
		});
		
		
	   function split( val ) {
		
 			return val.split( /;\s*/ );
		}
		function extractLast( term ) {
		   
			return split( term).pop();
			
		}		
		
 		$( "#emailTo" )
//		don't navigate away from the field on tab when selecting an item
 		.bind( "keydown", function( event ) {
 			if ( event.keyCode === $.ui.keyCode.TAB &&
 			$( this ).data( "ui-autocomplete" ).menu.active ) {
 				event.preventDefault();
 			}
 		})
 		.autocomplete({
 			source: function( request, response ) {
 			var url = "${path }/uw/uw.htm?window=emailAuto&json=1&l="+$( "#emailTo" ).val();			
 			$.getJSON( url, {			
  				term: extractLast( request.term )			
 			}, response );
 		},
		search: function() {
		//custom minLength
 		var term = extractLast( this.value );		
 		if ( term.length < 2 ) {
 		return false;
 		}
 		},
 		focus: function() {
//		prevent value inserted on focus
 		return false;
 		},
 		select: function( event, ui ) {
 		var terms = split( this.value );
			
//		remove the current input
		terms.pop();
//		add the selected item
		terms.push( ui.item.key );
//		add placeholder to get the comma-and-space at the end
 		terms.push( "" );
 		this.value = terms.join( ";" );
 		z=+1;
		return false;
		}
		
		
 		}) ;
 		
 		
		
				
		
	});
	
	function simpan(){
		frm_batal.flagEmail.value="0";
		frm_batal.submit();	
	
	}
	
	function kirim(){

		frm_batal.flagEmail.value="1";
		frm_batal.submit();	
	}
	
	
</script>
<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }
	
	.ui-autocomplete {
    
    border-radius: 0px;
    margin-top: 330px;
    margin-left: 105px;;
	}

</style>

<body style="height: 100%;">
	<div id="contents">
		<form method="post" name="frm_batal" enctype="multipart/form-data">
		<fieldset class="ui-widget ui-widget-content">
	    <legend class="ui-widget-header ui-corner-all"><div>Status SPAJ</div></legend>
		<table class="ui-widget " style="background-color: #CCCCCC;width: 100%" >
			<tr>
				<input type="hidden" readOnly name="spaj" value="${cmd.spaj}" >
				<th>Tanggal</th>
				<th>Akseptor</th>
				<th>Status</th>
				<th>Keterangan</th>
			</tr>
			<c:forEach var="inv" items="${cmd.lsPosition}" varStatus="xt">
				<tr>
					<td><input type="text" size="22" readOnly name="tanggal" value="${inv.MSPS_DATE}" ></td>
					<td><input type="text" readOnly name="lus id"
						<c:forEach var="user" items="${lsUser}">
							<c:if test="${ (inv.LUS_ID eq user.LUS_ID) }">
							 value="${user.LUS_LOGIN_NAME}"
							 </c:if>
						</c:forEach>	 
						></td>
					<td>

					<c:choose>
						<c:when test="${ xt.index ne (inv.SIZE)}">
							<c:forEach var="status" items="${lsStatusAccept}">
								<c:if test="${ (inv.LSSA_ID_BAS eq status.LSSA_ID)}">${status.STATUS_ACCEPT}</c:if>							
							</c:forEach>
							<br>
							${inv.SUB_DESC}
						</c:when>
						<c:otherwise>
							<select name="status" id="status">
								<option />
								<c:forEach var="status" items="${lsStatusAccept}">
									<option value="${status.LSSA_ID}"
										<c:if test="${ (inv.LSSA_ID_BAS eq status.LSSA_ID) 
										 }">selected</c:if>
									> ${status.STATUS_ACCEPT } </option>
								</c:forEach>
							</select>
							<c:if test="${inv.LSSA_ID_BAS eq 15 or inv.LSSA_ID_BAS eq 16}">	
								<br>
								<div id="statusacceptsub">
								                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
									<select name="substatus" id="substatus" >
					                 <c:forEach var="c" items="${lsSubStatusAccept}" varStatus="s">
										<option value="${c.SUB_ID}">${c.SUB_DESC}</option>
					 				</c:forEach> 
					                </select>
				                </div>
							</c:if>
						</c:otherwise>
					</c:choose>
					
					
					</td>
					<td><textarea name="textarea" cols="60" rows="4" onkeyup="textCounter(this, 450); "
						onkeydown="textCounter(this, 450); "
							<c:choose>
	       						<c:when test="${xt.index ne inv.SIZE}" > 
									readOnly
								</c:when>
							</c:choose>
						>${inv.MSPS_DESC}</textarea>
<!-- 						<c:if test="${ (inv.LSSA_ID_BAS eq 15) && xt.index ne inv.SIZE}"> -->
<!-- 							 <input type="button"  name="btnEdit" value="Edit" title="Ubah keterangan" onClick="ubah(${xt.index} );"> -->
<!--  						</c:if> -->
					</td>
				</tr>
			</c:forEach>				
		</table>
	</fieldset>
	<fieldset class="ui-widget ui-widget-content">
	<legend class="ui-widget-header ui-corner-all"><div>Attachment</div></legend>
	<table width="60%" class="entry2">
        <tr>
				<td colspan="4">
						Attachment : <input type="file" name="file1" id="file1" size="48" value="${file1}">
				</td>
		</tr>
			<tr>			
				<th id="emailAd">Email Admin (Tambahan To)</th>
				<td>				
					<input size="100" id ="emailTo" type="text" name="emailTo" value="" >				  
				</td>
				
			</tr> 

		<tr>
			<td colspan="4">
				<div align="center">
					  <input type="hidden" name="flagEmail" value="0">	
					   <input type="hidden" name="brs" >
					   <input type="hidden" name="ket" >
					   <input type="hidden" name="temp">
					   <input type="button" id="Ok" value="Save" onClick="simpan();" >
					   <input type="button" id="Send" value="Save & Send Email"  onclick="kirim();" >
				</div>
			</td>
		</tr>
		<tr> 
	        <td nowrap="nowrap" colspan="4">
			    <input type="hidden" name="flag" value="0">
	        	<c:if test="${not empty submitSuccess} ">
		        	<div id="success">
			        	<input type="hidden" name="flag" value="0">
			        	Berhasil Disimpan
		        	</div>
		        </c:if>	
	  			<spring:bind path="cmd.*">
					<c:if test="${not empty status.errorMessages}">
						<div id="error">
			        		Pesan:<br>
								<c:forEach var="error" items="${status.errorMessages}">
											 <c:out value="${error}" escapeXml="false" />
									<br/>
								</c:forEach>
						</div>
					</c:if>									
				</spring:bind>
			</td>
   		  </tr>
	</table>
	</fieldset>
	</form>	
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>