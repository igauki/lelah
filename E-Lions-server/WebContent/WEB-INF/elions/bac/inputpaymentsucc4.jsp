<%@ include file="/include/page/header.jsp"%>
<script>

</script>
<body onload="setupPanes('container1', 'tab4');" style="height: 100%;">
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" id="tab1">1. Input SPAJ/Polis</a>
				<a href="#" id="tab2">2. Pilih Tagihan</a>
				<a href="#" id="tab3">3. Input Pembayaran</a>
				<a href="#" onClick="return showPane('pane4', this)" id="tab4">4. Selesai</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane4" class="panes">
				<fieldset>
					<legend>Selesai</legend>
					<div id="success" style="text-transform: none; text-align: left;">
						${result}<br/>
						Note: Silahkan cek ulang inputan Anda dengan kembali ke menu INPUT PAYMENT SUCCESSIVE
					</div>
					<input type="button" value="&laquo; Kembali ke INPUT PAYMENT SUCCESSIVE" onclick="window.location='${path}/bac/inputpaymentsucc.htm';">
				</fieldset>
			</div>
			
		</div>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>