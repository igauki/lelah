<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
	<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta NAME="Description" CONTENT="EkaLife">
	<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<meta HTTP-EQUIV="Expires" CONTENT="-1">
	<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
	<link href="${path}/include/image/eas.ico" rel="shortcut icon">
	<!--  -->
	<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
	<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
	<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
	<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
	<!--  -->
	<script type="text/javascript" src="${path }/include/js/default.js"></script>
	<!-- Ajax Related -->
	<script type="text/javascript" src="${path}/include/js/ajax.js"></script>
	<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script>
	<script type="text/javascript" src="${path }/dwr/engine.js"></script>
	<script type="text/javascript" src="${path }/dwr/util.js"></script>
	
	<script type="text/javascript">
		hideLoadingMessage();
		
		function waitPreloadPage() { //DOM
			<c:if test='${not empty warn}'>
	            alert('${warn}');
	        </c:if>
			
			//calendar.set("tgl_lahir");
			
			if (document.getElementById){
				document.getElementById('prepage').style.visibility='hidden';
			}else{
				if (document.layers){ //NS4
					document.prepage.visibility = 'hidden';
				}
				else { //IE4
					document.all.prepage.style.visibility = 'hidden';
				}
			}
		}
		// End -->
		
		var fieldName='chbox';

		function selectall(){
		  var i=document.formpost.elements.length;
		  var e=document.formpost.elements;
		  var name=new Array();
		  var value=new Array();
		  var j=0;
		  for(var k=0;k<i;k++)
		  {
		    if(document.formpost.elements[k].name==fieldName)
		    {
		      if(document.formpost.elements[k].checked==true){
		        value[j]=document.formpost.elements[k].value;
		        j++;
		      }
		    }
		  }
		  checkSelect();
		}
		
		function selectCheck(obj)
		{
		 var i=document.formpost.elements.length;
		  for(var k=0;k<i;k++)
		  {
		    if(document.formpost.elements[k].name==fieldName)
		    {
		      document.formpost.elements[k].checked=obj;
		    }
		  }
		  selectall();
		}
		
		function selectallMe()
		{
		  if(document.formpost.allCheck.checked==true)
		  {
		   selectCheck(true);
		  }
		  else
		  {
		    selectCheck(false);
		  }
		}
		
		function checkSelect()
		{
		 var i=document.formpost.elements.length;
		 var berror=true;
		  for(var k=0;k<i;k++)
		  {
		    if(document.formpost.elements[k].name==fieldName)
		    {
		      if(document.formpost.elements[k].checked==false)
		      {
		        berror=false;
		        break;
		      }
		    }
		  }
		  if(berror==false)
		  {
		    document.formpost.allCheck.checked=false;
		  }
		  else
		  {
		    document.formpost.allCheck.checked=true;
		  }
		}
	</script>
</head>

<body onload="waitPreloadPage();" >
<div id="prepage" style="position:absolute; font-family:arial; font-size:12; left:0px; top:0px; background-color:yellow; layer-background-color:white;"> 
	<table border="2" bordercolor="red"><tr><td style="color: red;"><b>Loading ... ... Please wait!</b></td></tr></table>
</div>
<c:if test="${not empty warn } }">
	${warn }
</c:if>
<form method="post" name="formpost" id="formpost" action="" commandName="command">
	<fieldset>
		<legend style="font-size: 12px; color: black; font-weight: bolder;">Redeem Poin Tambang Emas</legend>
		<table class="entry" style="text-transform: uppercase;">
			<tr>
				<th><input type="checkbox" name="allCheck" onclick="selectallMe()" style="border: none"></th>
				<th>Tanggal</th>
				<th>Id Referral</th>
				<th>Nama</th>
				<th>Item</th>
				<th>Poin</th>
				<th>Registrasi</th>
				<th>User</th>
				<th>Status</th>
			</tr>
			<c:forEach items="${list }" var="l">
			<tr>
				<td align="center"><input name="chbox" type="checkbox" id="chbox" value="${l.ID_REDEEM}" style="border: none"></td>
				<td><fmt:formatDate value="${l.TGL }" pattern="dd/MM/yyyy"/></td>
				<td>${l.ID_SELLER }</td>
				<td>${l.NAMA }</td>
				<td>${l.NAMA_ITEM }</td>
				<td>${l.POIN }</td>
				<td>${l.REG_HADIAH }</td>
				<td>${l.USER_INPUT }</td>
				<td>${l.APPROVE }</td>
			</tr>
			</c:forEach>
		</table>
		<input type="submit" name="btnApprove" id="btnApprove" value="Approve">&nbsp;
		<input type="button" name="btnReset" id="btnReset" title="Reset" onClick="window.location='${path}/bac/multi.htm?window=redeem_proses';" value="Reset">
	</fieldset>
</form>
</body>
</html>	
