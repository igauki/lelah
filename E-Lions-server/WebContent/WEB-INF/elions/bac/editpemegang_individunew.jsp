<%@ include file="/include/page/header.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<%User currentUser = (User) session.getAttribute("currentUser");%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>
<script type="text/javascript" src="${path }/include/js/bac/pemegang.js"></script>
<script type="text/javascript" src="${path }/include/js/bac/pemegang2.js"></script>
<script type="text/javascript" src="${path }/include/js/bac/pemegang3.js"></script>
<script>
	/**
	  * MANTA
	  * Javascript dipindahkan ke file .js untuk menghindari error "EXCEEDING THE 65535 BYTES LIMIT"
	  * Penggunaan literal string tidak berfungsi di file .js
	  * Literal tersebut harus ditampung terlebih dahulu nilainya ke dalam variabel
	**/
	
	<spring:bind path="cmd.pemegang.jmlkyc"> 
		var jmlDaftarKyc = '${status.value}';
	</spring:bind>
	var Optiondana;
	var flag_add = 0;
	var flag_add1 = 0;
		
	<spring:bind path="cmd.pemegang.jmlkyc2">
		var jmlDaftarKyc2 = '${status.value}';
	</spring:bind>
	var Optionhasil;
	var flag_add2 = 0;
	var flag_add2_x = 0;
		
	var v_mspo_no_blanko = '${cmd.pemegang.mspo_no_blanko}';
	var v_mspo_nasabah_dcif = '${cmd.pemegang.mspo_nasabah_dcif}';
	var v_mcl_first_alias = '${cmd.pemegang.mcl_first_alias}';
	var v_nama_suami = '${cmd.pemegang.nama_suami}';
	var v_tgl_suami = '${cmd.pemegang.tgl_suami}';
	var v_usia_suami = '${cmd.pemegang.usia_suami}';
	var v_pekerjaan_suami = '${cmd.pemegang.pekerjaan_suami}';
	var v_jabatan_suami = '${cmd.pemegang.jabatan_suami}';
	var v_perusahaan_suami = '${cmd.pemegang.perusahaan_suami}';
	var v_bidang_suami = '${cmd.pemegang.bidang_suami}';
	var v_npwp_suami = '${cmd.pemegang.npwp_suami}';
	var v_penghasilan_suami = '${cmd.pemegang.penghasilan_suami}';
	var v_nama_ayah = '${cmd.pemegang.nama_ayah}';
	var v_tgl_ayah = '${cmd.pemegang.tgl_ayah}';
	var v_usia_ayah = '${cmd.pemegang.usia_ayah}';
	var v_pekerjaan_ayah = '${cmd.pemegang.pekerjaan_ayah}';
	var v_jabatan_ayah = '${cmd.pemegang.jabatan_ayah}';
	var v_perusahaan_ayah = '${cmd.pemegang.perusahaan_ayah}';
	var v_bidang_ayah = '${cmd.pemegang.bidang_ayah}';
	var v_npwp_ayah = '${cmd.pemegang.npwp_ayah}';
	var v_penghasilan_ayah = '${cmd.pemegang.penghasilan_ayah}';
	var v_nama_ibu = '${cmd.pemegang.nama_ibu}';
	var v_tgl_ibu = '${cmd.pemegang.tgl_ibu}';
	var v_usia_ibu = '${cmd.pemegang.usia_ibu}';
	var v_pekerjaan_ibu = '${cmd.pemegang.pekerjaan_ibu}';
	var v_jabatan_ibu = '${cmd.pemegang.jabatan_ibu}';
	var v_perusahaan_ibu = '${cmd.pemegang.perusahaan_ibu}';
	var v_bidang_ibu = '${cmd.pemegang.bidang_ibu}';
	var v_npwp_ibu = '${cmd.pemegang.npwp_ibu}';
	var v_penghasilan_ibu = '${cmd.pemegang.penghasilan_ibu}';
	
	var v_ad_kota_tgh = '${cmd.addressbilling.kota_tgh}';
	var v_ad_msap_zip_code = '${cmd.addressbilling.msap_zip_code}';
	var v_ad_lsne_id = '${cmd.addressbilling.lsne_id}';
	var v_ad_msap_area_code1 = '${cmd.addressbilling.msap_area_code1}';
	var v_ad_msap_phone1 = '${cmd.addressbilling.msap_phone1}';
	var v_ad_msap_area_code2 = '${cmd.addressbilling.msap_area_code2}';
	var v_ad_msap_phone2 = '${cmd.addressbilling.msap_phone2}';
	var v_ad_msap_area_code3 = '${cmd.addressbilling.msap_area_code3}';
	var v_ad_msap_phone3 = '${cmd.addressbilling.msap_phone3}';
	
	var v_cp_kota_rumah = '${cmd.contactPerson.kota_rumah}';
	var v_cp_kd_pos_rumah = '${cmd.contactPerson.kd_pos_rumah}';
	var v_cp_lsne_id = '${cmd.contactPerson.lsne_id}';
	var v_cp_area_code_rumah = '${cmd.contactPerson.area_code_rumah}';
	var v_cp_telpon_rumah = '${cmd.contactPerson.telpon_rumah}';
	var v_cp_area_code_rumah2 = '${cmd.contactPerson.area_code_rumah2}';
	var v_cp_telpon_rumah2 = '${cmd.contactPerson.telpon_rumah2}';
	var v_cp_kota_kantor = '${cmd.contactPerson.kota_kantor}';
	var v_cp_kd_pos_kantor = '${cmd.contactPerson.kd_pos_kantor}';
	var v_cp_area_code_kantor = '${cmd.contactPerson.area_code_kantor}';
	var v_cp_telpon_kantor = '${cmd.contactPerson.telpon_kantor}';
	var v_cp_area_code_kantor2 = '${cmd.contactPerson.area_code_kantor2}';
	var v_cp_telpon_kantor2 = '${cmd.contactPerson.telpon_kantor2}';
	
	var v_pp_no_hp = '${cmd.pemegang.no_hp}';
	var v_pp_no_hp2 = '${cmd.pemegang.no_hp2}';
	var v_pp_kota_rumah = '${cmd.pemegang.kota_rumah}';
	var v_pp_kd_pos_rumah = '${cmd.pemegang.kd_pos_rumah}';
	var v_pp_lsne_id = '${cmd.pemegang.lsne_id}';
	var v_pp_area_code_rumah = '${cmd.pemegang.area_code_rumah}';
	var v_pp_telpon_rumah = '${cmd.pemegang.telpon_rumah}';
	var v_pp_area_code_rumah2 = '${cmd.pemegang.area_code_rumah2}';
	var v_pp_telpon_rumah2 = '${cmd.pemegang.telpon_rumah2}';
	var v_pp_kota_kantor = '${cmd.pemegang.kota_kantor}';
	var v_pp_kd_pos_kantor = '${cmd.pemegang.kd_pos_kantor}';
	var v_pp_area_code_kantor = '${cmd.pemegang.area_code_kantor}';
	var v_pp_telpon_kantor = '${cmd.pemegang.telpon_kantor}';
	var v_pp_area_code_kantor2 = '${cmd.pemegang.area_code_kantor2}';
	var v_pp_telpon_kantor2 = '${cmd.pemegang.telpon_kantor2}';


	function Body_onload() {
		var frmParam = document.frmParam;
		body_onload_sub_1();
		var flag_special_case = '${cmd.flag_special_case}';
		if(flag_special_case==1){
			document.getElementById('special_case').value = '1';
		}else{
			document.getElementById('special_case').value = '0';
			document.getElementById('flag_special_case').value = '0';
		}
		frmParam.elements['pemegang.lus_id'].value=<%=currentUser.getLus_id()%>;
		frmParam.elements['pemegang.cbg_lus_id'].value='<%=currentUser.getLca_id()%>';
		body_onload_sub_2();
		
		var adaData = '${adaData}';
		var flag_spaj = '${cmd.pemegang.mspo_flag_spaj}';
		var nameSelection;
		xmlData.async = false;
		xmlData.src = "/E-Lions/xml/SUMBER_PENDANAAN.xml";
		xmlData.src = "/E-Lions/xml/SUMBER_PENGHASILAN.xml";
		generateXML_sumberKyc(xmlData, 'ID','DANA');
		var newvalue = '${cmd.addressbilling.tagih}';	
		if(newvalue!=""){
			document.getElementById('tgh').value=newvalue;		
			loaddata_penagihan();
		}else{
			document.getElementById('tgh').value='1';
			loaddata_penagihan();
		}
	
		if((frmParam.elements['pemegang.lus_id'].value=<%=currentUser.getLus_id()%>)=='2661'){//pemegang.mspo_nasabah_dcif
			document.frmParam.elements['pemegang.mspo_no_blanko'].readOnly = true;
			document.frmParam.elements['pemegang.mspo_no_blanko'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['pemegang.mspo_nasabah_dcif'].readOnly = true;
			document.frmParam.elements['pemegang.mspo_nasabah_dcif'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['no_pb'].readOnly = true;
			document.frmParam.elements['no_pb'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['pemegang.sumber_id'].readOnly = true;
			document.frmParam.elements['pemegang.sumber_id'].style.backgroundColor ='#D4D4D4';
		}
		if (document.frmParam.document.frmParam.tanda_pp.value==null || document.frmParam.document.frmParam.tanda_pp.value==""){
			document.frmParam.elements['pemegang.mkl_dana_from'].value = '0';
			document.frmParam.elements['pemegang.mkl_sumber_premi'].value = '0';
			document.frmParam.document.frmParam.tanda_pp.value = '0';
		}
		cpy(document.frmParam.tanda_pp.value);
		
		if (document.frmParam.document.frmParam.tanda_hub.value==null || document.frmParam.document.frmParam.tanda_hub.value==""){
			document.frmParam.elements['pemegang.mkl_sumber_premi'].value = '0';
			document.frmParam.document.frmParam.tanda_hub.value = '0';
		}
		cpy2(document.frmParam.tanda_hub.value);	
	}
	
	function tanyaPsave(spaj, flag_upload){
		window.location='${path}/bac/editspajnew.htm?kopiSPAJ='+document.frmParam.kopiSPAJ.value+'&flag_upload='+document.frmParam.elements['pemegang.flag_upload'].value;
	}
</script>

<body onLoad="Body_onload(); document.frmParam.elements['datausulan.mste_medical'].focus();">
<XML ID=xmlData></XML>
<form name="frmParam" method="post">
<table class="entry2">
	<tr>
		<td><input type="submit" name="_target0" value=" " onclick="next1()"  <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/pp2.jpg);"
				accesskey="1" onmouseover="return overlib('Alt-1', AUTOSTATUS, WRAP);" onmouseout="nd();">			
			<c:choose><c:when test="${(sessionScope.currentUser.jn_bank eq '16')}">
			<input type="submit" name="_target1" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ttgbsim.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();"></c:when><c:otherwise>
				<input type="submit" name="_target1" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ttg1.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();"></c:otherwise></c:choose>
			<input type="submit" name="_target7" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ppremi1.jpg);"
				accesskey="7" onmouseover="return overlib('Alt-7', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target2" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ddu1.jpg);"
				accesskey="3" onmouseover="return overlib('Alt-3', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target3" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/inv1.jpg);"
				accesskey="4" onmouseover="return overlib('Alt-4', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target4" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ag1.jpg);"
				accesskey="5" onmouseover="return overlib('Alt-5', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target5" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/kf1.jpg);"
				accesskey="6" onmouseover="return overlib('Alt-6', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" name="dummy2" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/sb1.jpg);">
			<textarea cols="40" rows="7" name="alamat_sementara" onkeyup="textCounter(this, 200); "
				onkeydown="textCounter(this, 200); ">${cmd.addressbilling.msap_address}</textarea>
			<textarea cols="40" rows="7" name="alamat_sementara1" onkeyup="textCounter(this, 200); "
				onkeydown="textCounter(this, 200); "><c:choose><c:when test="${cmd.datausulan.jenis_pemegang_polis == 1}">${cmd.contactPerson.alamat_rumah}</c:when><c:otherwise>${cmd.pemegang.alamat_rumah}</c:otherwise></c:choose></textarea>
			<textarea cols="40" rows="7" name="alamat_sementara2" onkeyup="textCounter(this, 200); "
				onkeydown="textCounter(this, 200); "><c:choose><c:when test="${cmd.datausulan.jenis_pemegang_polis == 1}">${cmd.contactPerson.alamat_kantor}</c:when><c:otherwise>${cmd.pemegang.alamat_kantor}</c:otherwise></c:choose></textarea>
</td>
	</tr>
	<tr>
		<td width="67%" align="left" valign="top">
		<spring:bind path="cmd.*">
			<c:if test="${not empty status.errorMessages}">
				<div id="error">INFO:<br>
				<c:forEach var="error" items="${status.errorMessages}">
	- <c:out value="${error}" escapeXml="false" />
					<br />
				</c:forEach></div>
			</c:if>
		</spring:bind>	
		<c:if test="${cmd.pemegang.jn_bank eq 2}">
			<%-- 
			<c:if test="${cmd.pemegang.rate_1 gt 0 }">
		  	<center><font size ="3" color="red">Rate PowerSave Bank Sinarmas per hari ini ada.</font></center>
		 	</c:if>
		 	--%>
		 	<c:if test="${cmd.pemegang.rate_1 eq 0 }">
		  	<center><font size ="3" color="red">Rate Simas Prima per hari ini belum ada. Silahkan hubungi AO anda atau bagian Investment kami.</font></center>
		  	<script>alert('Rate Simas Prima per hari ini belum ada. Silahkan hubungi AO anda atau bagian Investment kami.');</script>
		 	</c:if>
			<%-- 
		 	<c:if test="${cmd.pemegang.rate_2 gt 0 }">
		  	<center><font size ="3" color="red">Rate PowerSave Bank Sinarmas manfaat bulanan per hari ini ada.</font></center>
		 	</c:if>
		 	--%>
		 	<c:if test="${cmd.pemegang.rate_2 eq 0 }">
		  	<center><font size ="3" color="red">Rate Simas Prima Manfaat Bulanan per hari ini belum ada. Silahkan hubungi AO anda atau bagian Investment kami.</font></center>
		  	<script>alert('Rate Simas Prima Manfaat Bulanan per hari ini belum ada. Silahkan hubungi AO anda atau bagian Investment kami.');</script>
		 	</c:if>
		  </c:if>
		<table class="entry">
			<tr>
				<td colspan=5 height="20" style="text-align: center;">
					<%-- <input type="button"  value="Isi Data Pemegang Polis PT Guthrie" onclick="isi();" style="display:none;"> --%>
					
					<input type="button"  value="Copy Data dari Spaj:" 
						onclick="return tanyaPsave(document.frmParam.kopiSPAJ.value,document.frmParam.elements['pemegang.flag_upload'].value);">
					<input type="text" id="kopiSPAJ" name="kopiSPAJ" value=${cmd.pemegang.reg_spaj}>
					<input type="button" value="Cari" name="searchkopi"
						onclick="popWin('${path}/uw/spajgutri.htm?win=edit', 350, 450); ">
			<c:if test="${sessionScope.currentUser.lde_id ne 01 or sessionScope.currentUser.lde_id ne \"01\"}">
			<input name="btn_gadget" type="button" value="ESPAJ GADGET" onclick="popWin('${path}/bac/multi.htm?window=viewDataFromGadget', 650, 1320);" accesskey="E" 
			     onmouseover="return overlib('View Data Dari Gadget', AUTOSTATUS, WRAP);" onmouseout="nd();">		 
		     </c:if> 
					 	<!-- &search=yes -->
					<br>
					Copy Data SPAJ Dari:
					<input type="checkbox" name="specta_save1" class="noBorder"  value="${cmd.datausulan.specta_save}"  size="30" onClick="specta_save_onClick();"> SPECTA SAVE
					<spring:bind path="cmd.datausulan.specta_save"> 
		              <input type="hidden" name="${status.expression}" value="${cmd.datausulan.specta_save}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>
					<input type="checkbox" name="flag_upload" class="noBorder"  value="${cmd.pemegang.flag_upload}"  size="30" onClick="flag_upload_onClick();"> SPAJ UPLOAD
					<spring:bind path="cmd.pemegang.flag_upload"> 
		              <input type="hidden" name="${status.expression}" value="${cmd.pemegang.flag_upload}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>
					  <input type="checkbox" name="flag_upload2" class="noBorder"  size="30" onClick="flag_idProp_onClick();"> ID PROPOSAL
				      <input type="checkbox" name="flag_upload3" class="noBorder" <c:if test="${cmd.pemegang.flag_upload eq 3}"> 
					   checked</c:if> id="flag_upload3" size="30" onClick="flag_idProp_onClick();"> GADGET(E-SPAJ)
					<br>
					
					
					<input type="checkbox" name="special_case" class="noBorder"<c:if test="${cmd.flag_special_case eq '1'}"> 
					   checked</c:if> id="special_case" size="30" onClick="konfirm();">Special Case
					 <spring:bind path="cmd.flag_special_case">  
		              	<input type="hidden" name="${status.expression}"  id="flag_special_case" value="${cmd.flag_special_case}"  size="30" style='background-color :#D4D4D4'readOnly>
					   </spring:bind> 
					   <br>
					PEMEGANG POLIS ADALAH BADAN HUKUM/USAHA : 
					<select name="datausulan.jenis_pemegang_polis" onchange="resetPemegangPolis();">
						<option value="0" <c:if test="${cmd.datausulan.jenis_pemegang_polis eq 0}">selected="selected"</c:if>>TIDAK</option>
						<option value="1" <c:if test="${cmd.datausulan.jenis_pemegang_polis eq 1}">selected="selected"</c:if>>YA</option>
					</select>
					<spring:bind path="cmd.datausulan.flag_worksite">
						<input type="hidden" name="${status.expression}" value="${status.value }" size="20">
					</spring:bind> 
					<spring:bind path="cmd.pemegang.cab_bank">
						<input type="hidden" name="${status.expression}" value="${status.value }" size="20">
					</spring:bind>
					<spring:bind path="cmd.pemegang.jn_bank">
						<input type="hidden" name="${status.expression}" value="${status.value }" size="20">
					</spring:bind>
					<spring:bind path="cmd.pemegang.lus_id">
						<input type="hidden" name="${status.expression}" value="${status.value }" size="20">
					</spring:bind>
					<spring:bind path="cmd.pemegang.cbg_lus_id">
						<input type="hidden" name="${status.expression}" value="${status.value }" size="20">
					</spring:bind>
					
					<br>
					<input type="submit" name="_target${halaman+1}" value="Next &raquo;" onClick="next()" 
						onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
					<input type="hidden" name="_page" value="${halaman}">
				</td>
			</tr>
			<tr>
				<th></th>
				<th>Nomor Registrasi (SPAJ) </th>
				<th><input type="reg_spaj"
					value="<elions:spaj nomor='${cmd.pemegang.reg_spaj}'/>"
					style='background-color :#D4D4D4' readOnly> &nbsp;&nbsp;&nbsp;&nbsp;
					<c:if test="${cmd.datausulan.flag_worksite_ekalife eq 1}"> Karyawan AJ Sinarmas</c:if>
					</th>
				<th>SPAJ</th>
				<th>
					<select name="pemegang.mste_spaj_asli">
						<option value="1" <c:if test="${cmd.pemegang.mste_spaj_asli eq 1}">selected="selected"</c:if>>ASLI</option>
						<option value="0" <c:if test="${cmd.pemegang.mste_spaj_asli eq 0}">selected="selected"</c:if>>FOTOKOPI/FAX</option>
					</select>
				</th>
			</tr>
			<tr>
				<th></th>
				<th>Medis</th>
				<th><select name="datausulan.mste_medical" >
					<c:forEach var="medis" items="${select_medis}">
						<option
							<c:if test="${cmd.datausulan.mste_medical eq medis.ID}"> SELECTED </c:if>
							value="${medis.ID}">${medis.MEDIS}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font>
				</th>
				<th>Nomor Seri</th> 
				<th><spring:bind path="cmd.pemegang.mspo_no_blanko">
					<input type="text" name="${status.expression}" <c:if test="${cmd.currentUser.lus_id eq \"2661\"}"> disabled="disabled" style='background-color :#FFE1FD' </c:if>
						value="${status.value }" size="25" maxlength="30"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
						<font color="#CC3300">*</font>
				</spring:bind>
				<!-- generate format baru untuk nomor virtual account -->
				<select name="pemegang.jenis_va">
					<option value="VA_BARU" <c:if test="${cmd.pemegang.jenis_va eq 'VA_BARU'}">selected="selected"</c:if>>VA BARU</option>
					<option value="VA_LAMA" <c:if test="${cmd.pemegang.jenis_va eq 'VA_LAMA'}">selected="selected"</c:if>>VA LAMA</option>
				</select>
				</th>
			</tr>
			
			<c:if test="${sessionScope.currentUser.lde_id eq '11'}">
				<tr> <!-- NANA ADDED -->
					<th></th>
					<th>Vip Flag</th>
					<th><select name="datausulan.flag_vip">
						<c:forEach var="data" items="${select_medis_desc}"> 
					 		<option 
					 			<c:if test="${ cmd.datausulan.flag_vip eq data.FLAG_ID }"> SELECTED </c:if> 
					 			value="${data.FLAG_ID}">${data.LABEL_DESCRIPTION}		
					 		</option>
						</c:forEach> 
						</select>
					</th>
					<th></th>
					<th></th>
				</tr>  <!-- NANA END -->
			</c:if>
				
			<tr>
				<th></th>
				<th>NO PB</th>
				<th ><spring:bind path="cmd.no_pb">
									<input type="text" name="${status.expression}" <c:if test="${cmd.currentUser.lus_id eq \"2661\" or cmd.pemegang.mspo_flag_spaj eq 4 or cmd.pemegang.mspo_flag_spaj eq 5 }"> disabled="disabled" style='background-color :#FFE1FD' </c:if>
										value="${status.value }" size="20" 
										maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>
									>
									<font color="#CC3300">*</font>
								</spring:bind>
								</th>
				<th>NO CIF/ID MEMBER S.Prioritas</th>
				<th ><spring:bind path="cmd.pemegang.mspo_nasabah_dcif">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="20" 
										maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
									<font color="#CC3300">*</font>
								</spring:bind>
				</th>
				
			</tr>
<!-- 			<tr> -->
<!-- 				<th></th> -->
<!-- 				<th></th> -->
<!-- 				<th></th> -->
<!-- 				<th>No.Virtual Account</th> -->
<!-- 				<th ><spring:bind path="cmd.tertanggung.mste_no_vacc"> -->
<!-- 									<input type="text" name="${status.expression}" <%-- <c:if test="${cmd.pemegang.mspo_flag_spaj eq 4 or cmd.pemegang.mspo_flag_spaj eq 5}">disabled="disabled" style='background-color :#FFE1FD'</c:if> --%> -->
<!-- 										value="${status.value }" size="20"  -->
<!-- 										maxlength="16" -->
<!-- 					<c:if test="${ not empty status.errorMessage}"> -->
<!-- 						style='background-color :#FFE1FD' -->
<!-- 					</c:if> <c:if test="${ cmd.pemegang.mspo_flag_spaj eq 0}"> -->
<!-- 						disabled -->
<!-- 						style='background-color :#D4D4D4'					 -->
<!-- 					</c:if>> -->
<!-- 									<font color="#CC3300">*</font> -->
<!-- 								</spring:bind> -->
<!-- 								</th>	 -->
<!-- 			</tr>			 -->
			<tr>
				<th></th>
				<th></th>
				<th></th>
				<th>Jenis Form SPAJ</th>
				<th>
					<select name="pemegang.mspo_flag_spaj" onchange="disableVacc(this.value)"> 
						<%-- <option value="0" <c:if test="${cmd.pemegang.mspo_flag_spaj eq 0}">selected="selected"</c:if>>SPAJ LAMA</option>
						<option value="1" <c:if test="${cmd.pemegang.mspo_flag_spaj eq 1}">selected="selected"</c:if>>SPAJ BARU</option> --%>
						<option value="2" <c:if test="${cmd.pemegang.mspo_flag_spaj eq 2}">selected="selected"</c:if>>SPAJ TERBARU</option>
						<option value="3" <c:if test="${cmd.pemegang.mspo_flag_spaj eq 3}">selected="selected"</c:if>>SPAJ FULL MARET 2015</option>
						<option value="4" <c:if test="${cmd.pemegang.mspo_flag_spaj eq 4}">selected="selected"</c:if>>SPAJ SIO</option>
						<option value="5" <c:if test="${cmd.pemegang.mspo_flag_spaj eq 5}">selected="selected"</c:if>>SPAJ GIO</option>
					</select>
				</th>				
			</tr>
			
			<!-- Full Auto Sales Start -->
			<tr>
				<th></th>
				<th></th>
				<th></th>
				<th>Full Auto Sales</th>
				<th>
					<c:choose>
						<c:when test="${cmd.datausulan.enable_full_autosales eq 4}">
							<spring:bind path="cmd.datausulan.full_autosales">
								<input type="checkbox" name="full_autosales" class="noBorder" value="${cmd.datausulan.full_autosales}"  
									size="30" onClick="fullautosales_onClick();"
									<c:if test="${status.value eq '4'}">checked="checked"</c:if>> 
								<input type="hidden" name="${status.expression}" value="${cmd.datausulan.full_autosales}"  
								size="30" style='background-color :#D4D4D4'readOnly>
							</spring:bind>
						</c:when> 
						<c:otherwise>
							<spring:bind path="cmd.datausulan.full_autosales">
								<input disabled="disabled" type="checkbox" name="full_autosales" class="noBorder" value="${cmd.datausulan.full_autosales}"  
									size="30" onClick="fullautosales_onClick();"
									<c:if test="${status.value eq '4'}">checked="checked"</c:if>> 
								<input type="hidden" name="${status.expression}" value="${cmd.datausulan.full_autosales}"  
								size="30" style='background-color :#D4D4D4'readOnly>
							</spring:bind>
						</c:otherwise> 
					</c:choose> 
				</th>
				
			</tr>
			<!-- Full Auto Sales End -->
					
			<c:if test="${cmd.currentUser.lca_id ne \"09\" or cmd.currentUser.lca_id ne \"01\"}">
				<tr>
					<th></th>
					<th>CARI SUMBER BISNIS</th>
					<th colspan="3">
						<input type="text" name="carisumber" onkeypress="if(event.keyCode==13){ document.frmParam.btnsumber.click(); return false;}"> 
	              		<input type="button" name="btnsumber" value="Cari" onclick="ajaxSelectWithParam1a(document.frmParam.carisumber.value,'select_sumberBisnis','sumber','pemegang.sumber_id','', 'SUMBER_ID', 'NAMA_SUMBER', '','Silahkan pilih SUMBER BISNIS','3');"> 
					</th>
					
				</tr>
				<tr>
					<th></th>
					<th></th>
					<th colspan="3">
						<div id="sumber"> 
							<select name="pemegang.sumber_id" 
								 <c:if test="${ not empty status.errorMessage}">
										style='background-color :#FFE1FD'
								 </c:if>>
				                 <option value="${cmd.pemegang.sumber_id}">${cmd.pemegang.nama_sumber}</option>
				            </select>
			            </div>
					</th>
				</tr>
			</c:if>
			<tr>
			<c:if test="${cmd.datausulan.jenis_pemegang_polis == 0}">
				<tr>
				<th colspan=5 class="subtitle">DATA CALON PEMEGANG POLIS</th>
			</tr>
			<tr>
				<th width="18">1.</th>
				<th width="287">Nama Lengkap<br>
				<span class="info">(sesuai dengan KTP / Identitas lain tanpa gelar)</span>
				</th>
				<th width="505">
				
				<select name="pemegang.lti_id">
					<option value=""></option>
					<c:forEach var="l" items="${select_gelar}">
						<option
							<c:if test="${cmd.pemegang.lti_id eq l.ID}"> SELECTED </c:if>
							value="${l.ID}">${l.GELAR}</option>
					</c:forEach>
				</select>				
								<spring:bind path="cmd.pemegang.mcl_first">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="42" 
										maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>
									>
									<font color="#CC3300">*</font>
								</spring:bind>
							</th>
				<th width="155">Gelar &nbsp;</th>
				<th width="257"><spring:bind path="cmd.pemegang.mcl_gelar">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="20">
				</spring:bind></th>
			</tr>
			<tr>
				<th>2.</th>
				<th>Nama Alias 	<span class="info">(bila ada)</span></th>
				<th colspan="3"><spring:bind path="cmd.pemegang.mcl_first_alias">
					<input type="text" name="${status.expression}" <c:if test="${cmd.currentUser.lus_id eq \"2661\" or cmd.pemegang.mspo_flag_spaj eq 4 or cmd.pemegang.mspo_flag_spaj eq 5 }"> disabled="disabled" style='background-color :#FFE1FD' </c:if> value="${status.value}" size="42" maxlength="50"/>
				</spring:bind></th>
			</tr>
			<tr>
				<th>3.</th>
				<th>Nama Ibu Kandung</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.mspe_mother">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="50"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind><font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th>4.</th>
				<th>Bukti Identitas</th>
				<th colspan="3">
					<spring:bind path="cmd.pemegang.lside_id">
						<select name="${status.expression}" onChange="bixx(this.value);">
							<c:forEach var="d" items="${select_identitas}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
						<font color="#CC3300">*</font>
					</spring:bind>
				</th>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<th>No. KTP / Identitas lain</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.mspe_no_identity">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="50"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>	
				> <font color="#CC3300">*</font>
				</spring:bind></th>
			</tr>
			<tr>
				<th>4a.</th>
				<th>Tanggal Kadaluarsa</th>
				<th id ="expired"><spring:bind path="cmd.pemegang.mspe_no_identity_expired">
					<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
					<font color="#CC3300">*</font></spring:bind></th>
				<th colspan="2">&nbsp;</th>
			</tr>
			<tr>
				<th>5.</th>
				<th>Warga Negara</th>
				<th colspan="3">
					<spring:bind path="cmd.pemegang.lsne_id">
						<select name="${status.expression}">
							<c:forEach var="d" items="${select_negara}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
						<font color="#CC3300">*</font>
					</spring:bind>
				</th>
			</tr>
			<tr>
				<th>6.</th>
				<th>Apakah Anda warga negara Amerika Serika ? Pemegang Green Card ?</th>
				<th><spring:bind path="cmd.pemegang.mcl_green_card">
					<label for="ya"> <input type="radio" class=noBorder
						name="${status.expression}" value="1"
						<c:if test="${cmd.pemegang.mcl_green_card eq 1 or cmd.pemegang.mcl_green_card eq null}"> 
									checked</c:if>
						id="ya">Ya </label>
					<label for="tidak"> <input type="radio" class=noBorder
						name="${status.expression}" value="0"
						<c:if test="${cmd.pemegang.mcl_green_card eq 0}"> 
									checked</c:if>
						id="tidak">Tidak <font color="#CC3300"></font>
				</spring:bind> </label></th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>7.</th>
				<th>Tanggal Lahir</th>
				<th><spring:bind path="cmd.pemegang.mspe_date_birth">
					<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
					<font color="#CC3300">*</font></spring:bind></th>
				<th>Usia</th>
				<th><spring:bind path="cmd.pemegang.mste_age">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="4">
				</spring:bind> tahun</th>
			</tr>
		
			<tr>
				<th></th>
				<th>di</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.mspe_place_birth">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="30"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>
					>
				<font color="#CC3300">*</font></spring:bind></th>
			</tr>
				<tr>
				<th>8.</th>
				<th>Nama Lembaga/Perusahaan Tempat Bekerja</th>
				<th><spring:bind path="cmd.pemegang.mcl_company_name">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="50"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind><font color="#CC3300">*</font></th>
				<th>Uraian Tugas</th>
				<th><spring:bind path="cmd.pemegang.mkl_kerja_ket">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="40" maxlength="50"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				<th>Pekerjaan* dan Jabatan</th>
				<th colspan="3">
				<spring:bind path="cmd.pemegang.mkl_kerja">
					<select name="${status.expression}" 
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
						<c:forEach var="d" items="${select_lst_pekerjaan}">
							<option <c:if test="${status.value eq d.key}"> SELECTED </c:if>
								value="${d.key}">${d.value}</option>
						</c:forEach>
					</select>
				</spring:bind><font color="#CC3300">*</font></th>	
			</tr>
			<tr>
				<th>9.</th>
				<th>Jenis Kelamin</th>
				<th><spring:bind path="cmd.pemegang.mspe_sex">
					<label for="cowok"> <input type="radio" class=noBorder
						name="${status.expression}" value="1"
						<c:if test="${cmd.pemegang.mspe_sex eq 1 or cmd.pemegang.mspe_sex eq null}"> 
									checked</c:if>
						id="cowok">Pria </label>
					<label for="cewek"> <input type="radio" class=noBorder
						name="${status.expression}" value="0"
						<c:if test="${cmd.pemegang.mspe_sex eq 0}"> 
									checked</c:if>
						id="cewek">Wanita <font color="#CC3300">*</font>
				</spring:bind> </label></th>
				<th>Status</th>
				<th><select name="pemegang.mspe_sts_mrt" >
					<c:forEach var="marital" items="${select_marital}">
						<option
							<c:if test="${cmd.pemegang.mspe_sts_mrt eq marital.ID}"> SELECTED </c:if>
							value="${marital.ID}">${marital.MARITAL}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></th>
			</tr>								
			<tr>
				<th>10.</th>
				<th>Agama</th>
				<th colspan="3">
				<script type="text/javascript">
					function agamaChange(flag){					
						if(flag=='6'){						
							document.frmParam.elements['pemegang.mcl_agama'].readOnly = false;
						}else{
							document.frmParam.elements['pemegang.mcl_agama'].value="";
							document.frmParam.elements['pemegang.mcl_agama'].readOnly = true;
						}
					}
				</script>
					<spring:bind path="cmd.pemegang.lsag_id">
						<select name="${status.expression}" onchange="agamaChange(this.value);">
							<c:forEach var="d" items="${select_agama}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
						<font color="#CC3300">*</font>
					</spring:bind>
					
				</th>
			</tr>
			<tr>
				<th></th>
				<th>Lain - Lain, Sebutkan</th>
				<th colspan="3">					
					<spring:bind path="cmd.pemegang.mcl_agama">
						<input type="text" name="${status.expression}"
							value="${status.value }" size="25" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
					</spring:bind>
				</th>
			</tr>
			<tr>
			<th rowspan="6">11. a.</th>
				<th rowspan="6">Alamat Kantor</th>
				<th rowspan="6"><spring:bind path="cmd.pemegang.alamat_kantor">
					<textarea cols="40" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>${status.value }</textarea>
				</spring:bind></th>
				<th>Kode Pos</th>
				<th><spring:bind path="cmd.pemegang.kd_pos_kantor">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="10"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>Kota</th>
				<th><spring:bind path="cmd.pemegang.kota_kantor"><br>
				<input type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
		    	<span id="indicator_kantor" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
			</spring:bind>   
			<spring:bind path="cmd.contactPerson.kota_kantor">
					<input type="hidden" name="${status.expression }" id="${status.expression }" value="${status.value }"/>
				</spring:bind>
				</th>
			</tr>
			<tr><th></th><th></th></tr>
			<tr>
				<th>Telp Kantor 1</th>
				<th><spring:bind path="cmd.pemegang.area_code_kantor">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.pemegang.telpon_kantor">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>Telp Kantor 2</th>
				<th><spring:bind path="cmd.pemegang.area_code_kantor2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.pemegang.telpon_kantor2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>No Fax</th>
				<th><spring:bind path="cmd.pemegang.area_code_fax">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.pemegang.no_fax">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
			
				<th rowspan="5">b.</th>
				<th rowspan="5">Alamat Rumah<br><span class="info">(Wajib diisi sesuai KTP)</span></th>
				<th rowspan="5"><spring:bind path="cmd.pemegang.alamat_rumah">
					<textarea cols="40" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>${status.value }</textarea>
					</spring:bind><font color="#CC3300">*</font></th>
				<th>Kode Pos</th>
				<th><spring:bind path="cmd.pemegang.kd_pos_rumah">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="10"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>
						> <font color="#CC3300">*</font>
				</spring:bind></th>
			</tr>
			<tr>
				<th>Kota</th>
				<th ><spring:bind path="cmd.pemegang.kota_rumah"><br>
				<input type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
		    	<span id="indicator_rumah" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
				</spring:bind>   
				<font color="#CC3300">*</font>
				<spring:bind path="cmd.contactPerson.kota_rumah">
					<input type="hidden" name="${status.expression }" id="${status.expression }" value="${status.value }"/>
				</spring:bind>
				</td>
			</tr>
			<tr>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>Telp Rumah 1</th>
				<th><spring:bind path="cmd.pemegang.area_code_rumah">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					</spring:bind> <spring:bind path="cmd.pemegang.telpon_rumah">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				<font color="#CC3300">*</font></spring:bind></th>
			</tr>
			<tr>
				<th>Telp Rumah 2</th>
				<th><spring:bind path="cmd.pemegang.area_code_rumah2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.pemegang.telpon_rumah2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				<th></th>
				<th></th>
				<th>Handphone 1</th>
				<th><spring:bind path="cmd.pemegang.no_hp">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th></th>
				<th>No Npwp <font color="#CC3300">*</font></th>
				<th><spring:bind path="cmd.pemegang.mcl_npwp">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="40" maxlength="50"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
				<th>Handphone 2</th>
				<th><spring:bind path="cmd.pemegang.no_hp2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th rowspan="4">c.</th>
				<th rowspan="4">Alamat Tempat Tinggal<br><span class="info">(Wajib diisi hanya jika tidak sesuai KTP)</span></th>
				<th rowspan="4"><spring:bind path="cmd.pemegang.alamat_tpt_tinggal">
					<textarea cols="40" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
						</c:if>
						>${status.value }</textarea>
				<font color="#CC3300"></font></spring:bind></th>
				<th>Kode Pos</th>
				<th><spring:bind path="cmd.pemegang.kd_pos_tpt_tinggal">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="10"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>Kota</th>
				<th ><spring:bind path="cmd.pemegang.kota_tpt_tinggal"><br>
				<input type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
		    	<span id="indicator_rumah" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
				</spring:bind></td>
			</tr>
			<tr><th></th><th></th></tr>
			<tr><th></th><th></th></tr>
			<br>
			<tr>
				<th  colspan="5" class="subtitle" style="text-align: left;">Alamat Penagihan : <spring:bind path="cmd.addressbilling.tagih">
					<select  id="tgh" name="${status.expression }" onChange="data_penagihan();">
						<option value="2" <c:if test="${cmd.addressbilling.tagih eq 2}"> SELECTED </c:if>>Alamat Rumah</option>
						<option value="3" <c:if test="${cmd.addressbilling.tagih eq 3}"> SELECTED </c:if>>Alamat Kantor</option>
						<option value="1" <c:if test="${cmd.addressbilling.tagih eq 1}"> SELECTED </c:if>>Lain - Lain</option>
						<option value="4" <c:if test="${cmd.addressbilling.tagih eq 4}"> SELECTED </c:if>>Alamat Tempat Tinggal</option>
					</select>
				 </spring:bind></th>
			</tr>
			<tr>
				<th rowspan="7">d.</th>
				<th rowspan="7">Alamat Penagihan / <br>
				Korespondensi</th>
				<th rowspan="7"><spring:bind path="cmd.addressbilling.msap_address">
					<textarea cols="40" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>${status.value }</textarea>
				<font color="#CC3300">*</font></spring:bind></th>
				<th>Kode Pos</th>
				<th><spring:bind path="cmd.addressbilling.msap_zip_code">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="10"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>Kota</th>
				<th>
				<spring:bind path="cmd.addressbilling.kota_tgh"><br>
				<input type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
		    	 <span id="indicator_tagih" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
			</spring:bind>
				</th>
			</tr>
<!-- 			hattori -->
			<tr>
				<th>Negara</th>
				<th>
					<spring:bind path="cmd.addressbilling.lsne_id">
						<select name="${status.expression}">
							<c:forEach var="d" items="${select_negara}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
						<font color="#CC3300">*</font>
					</spring:bind>
				</th>
			</tr>
			<tr>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>No Telepon1</th>
				<th><spring:bind path="cmd.addressbilling.msap_area_code1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.addressbilling.msap_phone1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				<font color="#CC3300">*</font></spring:bind></th>
			</tr>
			<tr>
				<th>No Telepon2</th>
				<th><spring:bind path="cmd.addressbilling.msap_area_code2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.addressbilling.msap_phone2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>No Telepon3</th>
				<th><spring:bind path="cmd.addressbilling.msap_area_code3">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.addressbilling.msap_phone3">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				<th>Handphone 1</th>
				<th><spring:bind path="cmd.addressbilling.no_hp">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind><br/><font color="#CC3300">* apabila tidak ada no hp, harap isi dengan 0000</font></th>
				<th>No Fax</th>
				<th><spring:bind path="cmd.addressbilling.msap_area_code_fax1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.addressbilling.msap_fax1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				<th>Handphone 2</th>
				<th><spring:bind path="cmd.addressbilling.no_hp2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind><br/><font color="#CC3300">* apabila tidak ada no hp, harap isi dengan 0000</font></th>
				<th>Email</th>
				<th><spring:bind path="cmd.addressbilling.e_mail">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="40" maxlength="50"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>12.</th>
				<th>Khusus untuk korespondensi rutin harap dikirimkan melalui (pilih salah satu)</th>
				<th><spring:bind path="cmd.pemegang.mspo_korespondensi">
					<label for="email"> <input type="radio" class=noBorder
						name="${status.expression}" value="1"
						<c:if test="${cmd.pemegang.mspo_korespondensi eq 1 or cmd.pemegang.mspo_korespondensi eq null}"> 
									checked</c:if>
						id="email">Email </label>
					<label for="kurir"> <input type="radio" class=noBorder
						name="${status.expression}" value="0"
						<c:if test="${cmd.pemegang.mspo_korespondensi eq 0}"> 
									checked</c:if>
						id="kurir">Kurir/Pos <font color="#CC3300">*</font>
				</spring:bind> </label></th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>13.</th>
				<th>Polis dikirimkan melalui </th>
				<th><spring:bind path="cmd.pemegang.mspo_jenis_terbit">
					<label for="email1"> <input type="radio" class=noBorder
						name="${status.expression}" value="1"
						<c:if test="${cmd.pemegang.mspo_jenis_terbit eq 1 or cmd.pemegang.mspo_jenis_terbit eq null}"> 
									checked</c:if>
						id="email1">Email </label>
					<label for="kurir1"> <input type="radio" class=noBorder
						name="${status.expression}" value="0"
						<c:if test="${cmd.pemegang.mspo_jenis_terbit eq 0}"> 
									checked</c:if>
						id="kurir1">Kurir/Pos <font color="#CC3300">*</font>
				</spring:bind> </label></th>
				<th></th>
				<th></th>
			</tr>
			<tr>
				<th>14.</th>
				<th>Total Pendapatan Bersih/Tahun</th>
				<th colspan="3">
					<select name="pemegang.mkl_penghasilan">
						<c:forEach var="penghasilan" items="${select_penghasilan}">
							<option
							<c:if test="${cmd.pemegang.mkl_penghasilan eq penghasilan.ID}"> SELECTED </c:if>
							value="${penghasilan.ID}">${penghasilan.PENGHASILAN}</option>
						</c:forEach>
					</select>
				 	<font color="#CC3300">*</font>
				</th>
			</tr>
			<tr>
				<th>15.</th>
				<th>Sumber Pendanaan Pembelian Asuransi</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.bulan_gaji">
					<input type="checkbox" name="bulan_gaji_pp" class="noBorder" value="${cmd.pemegang.bulan_gaji}"  size="30" onClick="pp_bulan();"
						<c:if test="${status.value eq 'GAJI'}">checked="checked"</c:if>> 
					<input type="hidden" name="${status.expression}" value="${cmd.pemegang.bulan_gaji}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Gaji
					<spring:bind path="cmd.pemegang.bulan_penghasilan"> 
					<input type="checkbox" name="bulan_penghasilan_pp" class="noBorder"  value="${cmd.pemegang.bulan_penghasilan}"  size="30" onClick="pp_penghasilan();"
		            		<c:if test="${status.value eq 'TABUNGAN/DEPOSITO'}">checked="checked"</c:if>> 
		             <input type="hidden" name="${status.expression}" value="${cmd.pemegang.bulan_penghasilan}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind> Tabungan/Deposito
					<spring:bind path="cmd.pemegang.bulan_orangtua"> 
		            <input type="checkbox" name="warisan" class="noBorder"  value="${cmd.pemegang.bulan_orangtua}"  size="30" onClick="pp_warisan();"
					  		<c:if test="${status.value eq 'WARISAN'}">checked="checked"</c:if>> 
		             <input type="hidden" name="${status.expression}" value="${cmd.pemegang.bulan_orangtua}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind> Warisan
					<spring:bind path="cmd.pemegang.bulan_usaha"> 
		            <input type="checkbox" name="bulan_usaha_pp" class="noBorder"  value="${cmd.pemegang.bulan_usaha}"  size="30" onClick="pp_usaha();"
									 <c:if test="${status.value eq 'HIBAH'}">checked="checked"</c:if>> 
					<input type="hidden" name="${status.expression}" value="${cmd.pemegang.bulan_usaha}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Hibah <spring:bind path="cmd.pemegang.bulan_lainnya"> 
		            <input type="checkbox" name="bulan_lainnya_pp" class="noBorder"  value="${cmd.pemegang.bulan_lainnya}"  size="30" onClick="pp_lainnya();"
							 <c:if test="${status.value eq 'LAINNYA'}">checked="checked"</c:if>> 
					<input type="hidden" name="${status.expression}" value="${cmd.pemegang.bulan_lainnya}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind> Lainnya
					<spring:bind path="cmd.pemegang.bulan_lainnya_note">
						<input type="text" name="${status.expression}"
							value="${status.value }" size="32" maxlength="40"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
					</spring:bind>
			</tr> 
				<tr>
				<th>16.</th>
				<th>Tujuan Membeli Asuransi</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.tujuan_proteksi"> 
		            	<input type="checkbox" name="tujuan_proteksi_pp" class="noBorder" value="${cmd.pemegang.tujuan_proteksi}"  size="30" onClick="pp_proteksi();"
								<c:if test="${status.value eq 'PROTEKSI'}">checked="checked"</c:if>> 
						<input type="hidden" name="${status.expression}" value="${cmd.pemegang.tujuan_proteksi}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Proteksi
					<spring:bind path="cmd.pemegang.tujuan_investasi"> 
		            	<input type="checkbox" name="tujuan_investasi_pp" class="noBorder"  value="${cmd.pemegang.tujuan_investasi}"  size="30" onClick="pp_investasi();"
								<c:if test="${status.value eq 'INVESTASI'}">checked="checked"</c:if>> 
						<input type="hidden" name="${status.expression}" value="${cmd.pemegang.tujuan_investasi}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Investasi
					<spring:bind path="cmd.pemegang.tujuan_lainnya"> 
		            <input type="checkbox" name="tujuan_lainnya_pp" class="noBorder"  value="${cmd.pemegang.tujuan_lainnya}"  size="30" onClick="pptuj_lainnya();"
								<c:if test="${status.value eq 'LAINNYA'}">checked="checked"</c:if>> 
					<input type="hidden" name="${status.expression}" value="${cmd.pemegang.tujuan_lainnya}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>Lain-lain
					</th>
			</tr>
			<tr>
				<th>17.</th>
				<th>Hubungan Calon Pemegang Polis dengan Calon Tertanggung</th>
				<th colspan="3"><select name="pemegang.lsre_id" 
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<c:forEach var="relasi" items="${select_relasi}">
						<option
							<c:if test="${cmd.pemegang.lsre_id eq relasi.ID}"> SELECTED </c:if>
							value="${relasi.ID}">${relasi.RELATION}</option>
					</c:forEach>
				</select>
				 <font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th>18.</th>
				<th>Pemegang Polis adalah</th>
				<th colspan="3"><select name="pemegang.pemegang_polis" onchange="disableDataKeluarga(this.value)">
						<option value="4" <c:if test="${cmd.pemegang.pemegang_polis eq 4}">selected="selected"</c:if>>   </option>
						<option value="0" <c:if test="${cmd.pemegang.pemegang_polis eq 0}">selected="selected"</c:if>>Ibu Rumah Tangga</option>
						<option value="1" <c:if test="${cmd.pemegang.pemegang_polis eq 1}">selected="selected"</c:if>>Pelajar</option>
						<option value="2" <c:if test="${cmd.pemegang.pemegang_polis eq 2}">selected="selected"</c:if>>Lainnya</option>
				</select></th>
			</tr>
			<th colspan=5 class="subtitle">DATA SUAMI(JIKA CALON PEMEGANG POLIS ADALAH IBU RUMAH TANGGA)</th>
			<tr>
			<th></th>
				<th>Nama Suami</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.nama_suami">
					<input type="text" name="${status.expression}" value="${status.value}" size="40" maxlength="35" 
					<c:if test="${cmd.pemegang.mspo_flag_spaj eq 4 or cmd.pemegang.mspo_flag_spaj eq 5}">disabled="disabled" style='background-color :#FFE1FD'</c:if>/>
				</spring:bind></th>
			</tr>
			<th></th>
				<th>Tanggal Lahir Suami</th>
				<th><spring:bind path="cmd.pemegang.tgl_suami">
              			<script>inputDate('${status.expression}', '${status.value}', false);</script>
              	</spring:bind></th>
				<th>Usia</th>
				<th><spring:bind path="cmd.pemegang.usia_suami">
					<input type="text" name="${status.expression}" value="${status.value}" size="3" maxlength="3" readOnly/>
				</spring:bind></th>
			</tr>
			<th></th>
				<th>Pekerjaan Suami</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.pekerjaan_suami">
					<select name="${status.expression}">
							<c:forEach var="d" items="${select_lst_pekerjaan}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
				</spring:bind></th>
			</tr>
			<th></th>
				<th>Jabatan Suami</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.jabatan_suami">
					<select name="${status.expression}" >
					<c:forEach var="d" items="${select_jabatan}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
				</select></spring:bind></th>
			</tr>
			<th></th>
				<th>Nama Perusahaan Tempat Suami Bekerja</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.perusahaan_suami">
					<input type="text" name="${status.expression}" value="${status.value}" size="40" maxlength="35"/>
				</spring:bind></th>
			</tr>
			<th></th>
				<th>Bidang Usaha Suami</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.bidang_suami">
						<select name="${status.expression}">
							<c:forEach var="d" items="${select_bidang_info_nasabah}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
				</spring:bind></th>
			</tr>
			<th></th>
				<th>No. NPWP</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.npwp_suami">
					<input type="text" name="${status.expression}" value="${status.value}" size="20" maxlength="15"/>
				</spring:bind></th>
			</tr>
				<th></th>
				<th>Penghasilan Suami Per Tahun</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.penghasilan_suami">
						<select name="${status.expression}">
							<c:forEach var="d" items="${select_penghasilan}">
								<option
									<c:if test="${status.value eq d.ID}"> SELECTED </c:if>
									value="${d.ID}">${d.PENGHASILAN}</option>
							</c:forEach>
						</select>
						<font color="#CC3300"></font>
					</spring:bind></th>
			</tr>
			<tr></tr>
			<th colspan=5 class="subtitle">DATA ORANG TUA(JIKA CALON PEMEGANG POLIS ADALAH PELAJAR)</th>
			<tr>
			<th></th>
				<th><span class="info">(Data Ayah)</span></th>
				<th colspan="3"></th>
			</tr>
			<tr>
			<th></th>
				<th>Nama Ayah</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.nama_ayah">
					<input type="text" name="${status.expression}" value="${status.value}" size="40" maxlength="35"/>
				</spring:bind></th>
			</tr>
			<th></th>
				<th>Tanggal Lahir Ayah</th>
				<th><spring:bind path="cmd.pemegang.tgl_ayah">
					<script>inputDate('${status.expression}', '${status.value}', false);</script>
				</spring:bind></th>
				<th>Usia</th>
				<th><spring:bind path="cmd.pemegang.usia_ayah">
					<input type="text" name="${status.expression}" value="${status.value}" size="3" maxlength="3" readOnly/>
				</spring:bind></th>
			</tr>
			<th></th>
				<th>Pekerjaan Ayah</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.pekerjaan_ayah">
					<select name="${status.expression}">
							<c:forEach var="d" items="${select_lst_pekerjaan}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
				</spring:bind></th>
			</tr>
			<th></th>
				<th>Jabatan Ayah</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.jabatan_ayah">
				<select name="${status.expression}" 
					 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					 </c:if>>
					<c:forEach var="d" items="${select_jabatan}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
				</select></spring:bind></th>
			</tr>
			<th></th>
				<th>Nama Perusahaan Tempat Ayah Bekerja</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.perusahaan_ayah">
					<input type="text" name="${status.expression}" value="${status.value}" size="40" maxlength="35"/>
				</spring:bind></th>
			</tr>
			<th></th>
				<th>Bidang Usaha Ayah</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.bidang_ayah">
						<select name="${status.expression}">
							<c:forEach var="d" items="${select_bidang_info_nasabah}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
				</spring:bind></th>
			</tr>
			<th></th>
				<th>No. NPWP</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.npwp_ayah">
					<input type="text" name="${status.expression}" value="${status.value}" size="20" maxlength="15"/>
				</spring:bind></th>
			</tr>
				<th></th>
				<th>Penghasilan Ayah Per Tahun</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.penghasilan_ayah">
					<select name="${status.expression}">
							<c:forEach var="d" items="${select_penghasilan}">
								<option
									<c:if test="${status.value eq d.ID}"> SELECTED </c:if>
									value="${d.ID}">${d.PENGHASILAN}</option>
							</c:forEach>
						</select>
						<font color="#CC3300"></font>
				</spring:bind></th>
			</tr>
			<tr></tr>
			<tr>
			<th></th>
				<th><span class="info">(Data Ibu)</span></th>
				<th colspan="3"></th>
			</tr>
			<tr>
			<th></th>
				<th>Nama Ibu</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.nama_ibu">
					<input type="text" name="${status.expression}" value="${status.value}" size="40" maxlength="35"/>
				</spring:bind></th>
			</tr>
			<th></th>
				<th>Tanggal Lahir Ibu</th>
				<th><spring:bind path="cmd.pemegang.tgl_ibu">
					<script>inputDate('${status.expression}', '${status.value}', false);</script>
				</spring:bind></th>
				<th>Usia</th>
				<th><spring:bind path="cmd.pemegang.usia_ibu">
					<input type="text" name="${status.expression}" value="${status.value}" size="3" maxlength="3" readonly/>
				</spring:bind></th>
			</tr>
			<th></th>
				<th>Pekerjaan Ibu</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.pekerjaan_ibu">
					<select name="${status.expression}">
							<c:forEach var="d" items="${select_lst_pekerjaan}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
				</spring:bind></th>
			</tr>
			<th></th>
				<th>Jabatan Ibu</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.jabatan_ibu">
				<select name="${status.expression}" 
					 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<c:forEach var="d" items="${select_jabatan}">
							<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
				</select></spring:bind></th>
			</tr>
			<th></th>
				<th>Nama Perusahaan Tempat Ibu Bekerja</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.perusahaan_ibu">
					<input type="text" name="${status.expression}" value="${status.value}" size="40" maxlength="35"/>
				</spring:bind></th>
			</tr>
			<th></th>
				<th>Bidang Usaha Ibu</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.bidang_ibu">
						<select name="${status.expression}">
							<c:forEach var="d" items="${select_bidang_info_nasabah}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
				</spring:bind></th>
			</tr>
			<th></th>
				<th>No. NPWP</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.npwp_ibu">
					<input type="text" name="${status.expression}" value="${status.value}" size="20" maxlength="15"/>
				</spring:bind></th>
			</tr>
				<th></th>
				<th>Penghasilan Ibu Per Tahun</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.penghasilan_ibu">
						<select name="${status.expression}">
							<c:forEach var="d" items="${select_penghasilan}">
								<option
									<c:if test="${status.value eq d.ID}"> SELECTED </c:if>
									value="${d.ID}">${d.PENGHASILAN}</option>
							</c:forEach>
						</select>
						<font color="#CC3300"></font>
				</spring:bind></th>
			</tr>
			<tr></tr>
			</c:if>
			<c:if test="${cmd.datausulan.jenis_pemegang_polis == 1}">
				<!-- ada di "editpemegang_badanusaha.jsp" -->
			</c:if>
			<tr>
				<th class="subtitle" colspan="5"></th>
			</tr>		
            <tr> 
            <td colspan="9"> 
			 <p align="left"><b> <font face="Verdana" size="2" color="#CC3300">Note : * Wajib diisi</font></b></p>
			</td>
          </tr>	
          <c:if test="${cmd.tertanggung.lspd_id eq 2 or cmd.tertanggung.lspd_id eq 27 or cmd.tertanggung.lspd_id eq 209}">
          	<c:if test="${not empty cmd.pemegang.reg_spaj}">
	          <tr>          	 
	          	 <td colspan="2">
					 <input type="checkbox" name="edit_pemegang" id="edit_pemegang" class="noBorder" value ="${cmd.pemegang.edit_pemegang}" <c:if test="${cmd.pemegang.edit_pemegang eq 1}"> 
										checked</c:if> size="30" onClick="edit_onClick();"> Telah dilakukan edit
						<spring:bind path="cmd.pemegang.edit_pemegang"> 
			          		<input type="hidden" name="${status.expression}" value="${cmd.pemegang.edit_pemegang}"  size="30" style='background-color :#D4D4D4'readOnly>
						</spring:bind>	   
	          	 </td>
				 <td colspan="7">
				 		<spring:bind path="cmd.pemegang.kriteria_kesalahan">
							<select name="${status.expression}">
								<c:forEach var="a" items="${select_kriteria_kesalahan}">
									<option
										<c:if test="${cmd.pemegang.kriteria_kesalahan eq a.KESALAHAN}"> SELECTED </c:if>
										value="${a.KESALAHAN}">${a.KESALAHAN}
									</option>
								</c:forEach>
							</select>
						</spring:bind>
				 </td> 
	          </tr>
	          <tr>
	          	<td colspan="9">&nbsp;</td>
	          </tr>
          	</c:if>
          </c:if>
			<tr>
				<td colspan="5" style="text-align: center;">
					<input type="hidden" name="hal" value="${halaman}">
					<input type="hidden" name="_page" value="${halaman}">
				 	<spring:bind path="cmd.pemegang.indeks_halaman">
						<input type="hidden" name="${status.expression}"
							value="${halaman-1}" size="20">
				 	</spring:bind>
					<input type="submit" name="_target${halaman+1}"
						value="Next &raquo;" onClick="next()" 
						accesskey="N" onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</form>
<ajax:autocomplete
				  source="pemegang.kota_rumah"
				  target="pemegang.kota_rumah"
				  baseUrl="${path}/servlet/autocomplete?s=pemegang.kota_rumah&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_rumah"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />
<ajax:autocomplete
				  source="contactPerson.kota_rumah"
				  target="contactPerson.kota_rumah"
				  baseUrl="${path}/servlet/autocomplete?s=contactPerson.kota_rumah&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_rumah"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />
<ajax:autocomplete
				  source="contactPerson.kota_kantor"
				  target="contactPerson.kota_kantor"
				  baseUrl="${path}/servlet/autocomplete?s=contactPerson.kota_kantor&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_kantor"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />
<ajax:autocomplete
				  source="pemegang.kota_kantor"
				  target="pemegang.kota_kantor"
				  baseUrl="${path}/servlet/autocomplete?s=pemegang.kota_kantor&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_kantor"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />
	<ajax:autocomplete
				  source="addressbilling.kota_tgh"
				  target="addressbilling.kota_tgh"
				  baseUrl="${path}/servlet/autocomplete?s=addressbilling.kota_tgh&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_tagih"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />	
	<ajax:autocomplete
				  source="pemegang.kota_tpt_tinggal"
				  target="pemegang.kota_tpt_tinggal"
				  baseUrl="${path}/servlet/autocomplete?s=pemegang.kota_tpt_tinggal&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_rumah"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />		

<script type="text/javascript">	
	if(frmParam.elements['datausulan.jenis_pemegang_polis'].value != 1){
		agamaChange('${cmd.pemegang.lsag_id}');
	}
</script>	  
</body>
<%@ include file="/include/page/footer.jsp"%>