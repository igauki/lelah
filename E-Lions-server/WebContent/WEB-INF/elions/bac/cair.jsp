<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<meta HTTP-EQUIV="Expires" CONTENT="-1">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link href="${path}/include/image/eas.ico" rel="shortcut icon">
		<!--  -->
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<!--  -->
		<script type="text/javascript" src="${path }/include/js/default.js"></script>
		<!-- Ajax Related -->
		<script type="text/javascript" src="${path}/include/js/ajax.js"></script>
		<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script>
		<script type="text/javascript" src="${path }/dwr/engine.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>
		<script type="text/javascript">
			hideLoadingMessage();
			
			function rubah(nc, jnc){
				document.getElementById(jnc).disabled = !nc.checked;
			}
		
			////////////////////
			
			var detik = 60;
		
			var reloadTimer = function(options) {
				var seconds = options.seconds || 0, logoutURL = options.logoutURL, message = options.message;
		
				this.start = function() {
					setTimeout(function() {
						alert(message);
						window.location.href = logoutURL;
					}, seconds * 1000);
				}
				return this;
			};
			
			var timer = reloadTimer({
				seconds : detik,
				logoutURL : '${path}/bac/cair.htm',
				message : 'Sesi input pencairan telah berakhir. Harap lakukan input pencairan dalam ' + detik + ' detik.'
			});
			timer.start();
			
			function tampilTimeout(){
				document.getElementById("kapan").innerHTML = (detik--);
			} 
		
</script>
	</head>
	<body onload="setupPanes('container1','tab1'); tampilTimeout(); setInterval('tampilTimeout()', 1000);" style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Pencairan</a>
				</li>
			</ul>

			<div class="tab-panes">

				<div id="pane1" class="panes">
					<div style="text-transform: none;font-size: 1.5em; color: RED;">PERHATIAN! Menu ini akan timeout dalam&nbsp;<span id="kapan"></span>&nbsp;detik</div>
					
					<form:form name="formpost" commandName="cmd" cssStyle="text-align: left;">
						<fieldset>
							<c:choose>
								<c:when test="${sessionScope.currentUser.jn_bank eq 2 }">
									<legend>Pencairan Simas Prima / Simas Stabil Link  </legend>
								</c:when>
								<c:when test="${sessionScope.currentUser.jn_bank eq 3}">
									<legend>Pencairan Danamas Prima / Stable Link Sekuritas </legend>
								</c:when>
								<c:when test="${sessionScope.currentUser.jn_bank eq 16 }">
									<legend>Pencairan PowerSave Syariah BSM  </legend>
								</c:when>
								<c:otherwise>
									<legend>Pencairan PowerSave / Stable Link  </legend>
								</c:otherwise>
							</c:choose>
							<table class="entry2">
								<tr>
									<th nowrap="nowrap" style="width: 250px;">Nomor Polis / Nomor Register SPAJ:</th>
									<td>
										<form:input path="reg_spaj"/>&nbsp;<input type="submit" name="show" value="Show">
									</td>
								</tr>
								<tr>
									<th nowrap="nowrap"></th>
									<td>
										<spring:hasBindErrors name="cmd">
											<div id="error">
												<strong>PERHATIAN!</strong><br/>
												<form:errors path="*" delimiter="<br>" />
											</div>
										</spring:hasBindErrors>
									</td>
								</tr>
							</table>
						</fieldset>
						<c:if test="${not empty cmd.powersaveCair}">
							<fieldset>
								<legend>Informasi Polis</legend>
								<table class="entry2">
									<tr>
										<th nowrap="nowrap" style="width: 140px;">
											Produk:
										</th>
										<th nowrap="nowrap" class="left">
											<input type="text" readonly="readonly" value="[${cmd.powersaveCair.lsbs_id}-${cmd.powersaveCair.lsdbs_number}] ${cmd.powersaveCair.lsdbs_name}" size="60">
											No. Register SPAJ:
											<input type="text" readonly="readonly" value="${cmd.powersaveCair.reg_spaj}" size="17">
											No. Polis:
											<input type="text" readonly="readonly" value="${cmd.powersaveCair.mspo_policy_no_format}" size="23">
										</th>
									</tr>
									<tr>
										<th nowrap="nowrap">
											Pemegang:
										</th>
										<th nowrap="nowrap" class="left">
											<input type="text" readonly="readonly" value="${cmd.powersaveCair.pemegang}" size="50">
											Tertanggung: 
											<input type="text" readonly="readonly" value="${cmd.powersaveCair.tertanggung}" size="50">
										</th>
									</tr>
									<tr>
										<th nowrap="nowrap">
											Masa Berlaku Polis:
										</th>
										<th nowrap="nowrap" class="left">
											<input type="text" value="<fmt:formatDate value="${cmd.powersaveCair.mste_beg_date}" pattern="dd/MM/yyyy"/>" size="12" readonly>
											s/d			
											<input type="text" value="<fmt:formatDate value="${cmd.powersaveCair.mste_end_date}" pattern="dd/MM/yyyy"/>" size="12" readonly>
											Uang Pertanggungan:
											<input style="text-align: right;" type="text" value="<fmt:formatNumber value="${cmd.powersaveCair.mspr_tsi}" type="currency" 
												currencySymbol="${cmd.powersaveCair.lku_symbol} " maxFractionDigits="2" minFractionDigits="0" />" size="25" readonly>
											Premi:
											<input style="text-align: right;" type="text" value="<fmt:formatNumber value="${cmd.powersaveCair.mspr_premium}" type="currency" 
												currencySymbol="${cmd.powersaveCair.lku_symbol} " maxFractionDigits="2" minFractionDigits="0" />" size="25" readonly>
										</th>
									</tr>
									<tr>
										<th nowrap="nowrap">
											Info Rekening:
										</th>
										<th nowrap="nowrap" class="left">
											<div id="success">
												${cmd.powersaveCair.mpc_note}
											</div>
										</th>
									</tr>
								</table>
							</fieldset>
						</c:if>
						<c:if test="${not empty cmd.daftarPremi}">
							<fieldset style="border-color: red">
								<legend>Informasi Rollover Terakhir</legend>
								
								<table class="displaytag">
									<thead>
										<tr>
											<th title="Silahkan Pilih Minimal 1 Transaksi">Pilih</th>
											<th title="Nomor Registrasi Pencairan">No Reg</th>
											<th title="Jenis Transaksi Premi Pokok / Top-Up">Info</th>
											<th title="Masa Garansi Investasi / Masa Target Investasi">MGI</th>
											<th title="Persentase Rate yang Diberikan">(%)</th>
											<th title="Jenis Rollover (ALL/PREMI/AUTOBREAK)">RO</th>
											<th title="Tanggal Deposit">Tgl Deposit</th>
											<th title="Tanggal Jatuh Tempo">Tgl JT</th>
											<th title="Tanggal Cair Aktual">Tgl Cair</th>
											<th title="Pencairan Non-Cash">Non Cash</th>
											<th title="Jenis Non-Cash">Jenis Non Cash</th>
											<th title="Nominal Deposit">Deposit</th>
											<th title="Bunga yang diperoleh">Bunga</th>
											<th title="Tambahan hari bunga yang diperoleh apabila JT pada hari libur">Tambahan Hari</th>
											<th title="Tambahan manfaat bunga harian">Tambahan Manfaat</th>
											<th title="Bonus Performance (Khusus Stabil Link)">BP</th>
											<th title="Total Bunga yang diperoleh">Total Bunga</th>
											<th title="Total Seluruh Nominal (Nilai Tunai)">Total NT</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${cmd.daftarPremi}" var="p" varStatus="st">
											<tr style="background-color: ${p.warna}; ">
												<td><form:checkbox cssClass="noBorder" path="daftarPremi[${st.index}].centang" value="true"/></td>
												<td>${p.mpc_reg}</td>
												<td>
													<c:choose>
														<c:when test="${p.mpc_tu_ke eq 0}">Premi Pokok</c:when>
														<c:otherwise>Top-Up ke ${p.mpc_tu_ke}</c:otherwise>
													</c:choose>
												</td>
												<td>${p.mgi} Bln</td>
												<td><fmt:formatNumber value="${p.mpc_rate}" maxFractionDigits="2" minFractionDigits="0" />%</td>
												<td>
													<c:choose>
														<c:when test="${p.mpc_ro eq 1}">ROLLOVER ALL</c:when>
														<c:when test="${p.mpc_ro eq 2}">ROLLOVER PREMI</c:when>
														<c:when test="${p.mpc_ro eq 3}">AUTOBREAK</c:when>
													</c:choose>
												</td>
												<td><fmt:formatDate value="${p.mpc_bdate}" pattern="dd/MM/yyyy"/></td>
												<td><fmt:formatDate value="${p.mpc_edate}" pattern="dd/MM/yyyy"/></td>
												<td><fmt:formatDate value="${p.mpc_cair}" pattern="dd/MM/yyyy"/></td>
												<td><form:checkbox cssClass="noBorder" path="daftarPremi[${st.index}].mpc_noncash" value="0" onclick="rubah(this, 'jnc${st.index}');"/></td>
												<td>
													<c:choose>
														<c:when test="${cmd.daftarPremi[st.index].mpc_noncash eq 1}">
															<form:select id="jnc${st.index}" path="daftarPremi[${st.index}].mpc_jenis_noncash" items="${jenisNoncash}" itemValue="key" itemLabel="value" disabled="true"/>
														</c:when>
														<c:otherwise>
															<form:select id="jnc${st.index}" path="daftarPremi[${st.index}].mpc_jenis_noncash" items="${jenisNoncash}" itemValue="key" itemLabel="value" disabled="false"/>
														</c:otherwise>
													</c:choose>
												</td>
												<td><fmt:formatNumber value="${p.mpc_premi}" maxFractionDigits="2" minFractionDigits="2" /></td>
												<td><fmt:formatNumber value="${p.mpc_bunga}" maxFractionDigits="2" minFractionDigits="2" /></td>
												<td>${p.mpc_hari} Hari</td>
												<td><fmt:formatNumber value="${p.mpc_tambah - p.mpc_kurang}" maxFractionDigits="2" minFractionDigits="2" /></td>
												<td><fmt:formatNumber value="${p.mpc_bp}" maxFractionDigits="2" minFractionDigits="2" /></td>
												<td><fmt:formatNumber value="${p.total_bunga}" maxFractionDigits="2" minFractionDigits="2" /></td>
												<td>
													<fmt:formatNumber value="${p.mpc_premi + p.total_bunga}" maxFractionDigits="2" minFractionDigits="2" /><br/>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
								<span class="info">* Silahkan pilih/centang rollover yang ingin dicairkan. Apabila pencairan non cash, harap pilih/centang kolom NON CASH, dan pilih jenis NON CASH.</span>
							</fieldset>
							<fieldset>
								<legend>INFORMASI WARNA PADA KOLOM "INFORMASI ROLLOVER TERAKHIR"</legend>
								<br/>
								<input type="button" value="Belum Pernah Diinput" style="background-color: #ffffff; ">
								&nbsp;<img src="${path}/include/image/arrow.gif">&nbsp;
								<input type="button" value="SUDAH DI-INPUT" style="background-color: #ffffcc; ">
								&nbsp;<img src="${path}/include/image/arrow.gif">&nbsp;
								<c:if test="${sessionScope.currentUser.jn_bank eq 2}">
									<input type="button" value="Approval KP BSM" style="background-color: #ccffcc; ">
									&nbsp;<img src="${path}/include/image/arrow.gif">&nbsp;
								</c:if>
								<input type="button" value="Dalam Proses AJS" style="background-color: #ffccff; ">
								<c:if test="${sessionScope.currentUser.jn_bank eq 2  || sessionScope.currentUser.jn_bank eq 16}">
									<span class="info">
									<br> * Input Pencairan hanya bisa dilakukan paling cepat H-7 dan paling lambat H-3 dari TANGGAL JATUH TEMPO.
									<br> * Apabila input dilakukan pada H-2, H-1, atau H-0 dari TANGGAL JATUH TEMPO, maka memerlukan APPROVAL KP BSM ${cmd.kategori_company}.
									<br> * Untuk Penginputan Pencairan Harus Dilakukan Maksimal H-3 Dari Jatuh Tempo Agar Mendapatkan Tanggal Aktual Cair.
									<br> * Harap Segera Transfer Posisi Ke Proses AJS jika sudah diotorisasi, agar tidak terjadi keterlambatan Pencairan.
									</span>
								</c:if>
							</fieldset>
							<fieldset>
								<legend>INFORMASI Kelengkapan Dokumen</legend>
								<table>
									<tr>
										<td><label for="a1"><form:checkbox id="a1" cssClass="noBorder" path="daftarPremi[0].mpc_ktp" value="1"/>KTP</label></td>
										<td><label for="a3"><form:checkbox id="a3" cssClass="noBorder" path="daftarPremi[0].mpc_sph" value="1"/>SPH (Surat Pinjaman Polis - Asli + Meterai)</label></td>
										<td><label for="a5"><form:checkbox id="a5" cssClass="noBorder" path="daftarPremi[0].mpc_formrek" value="1"/>Form Perubahan Nomor Rekening</label></td>
									</tr>
									<tr>
										<td><label for="a2"><form:checkbox id="a2" cssClass="noBorder" path="daftarPremi[0].mpc_spt" value="1"/>SPT (Surat Perintah Transfer - Asli)</label></td>
										<td><label for="a4"><form:checkbox id="a4" cssClass="noBorder" path="daftarPremi[0].mpc_polis" value="1"/>Polis Asli</label></td>
									</tr>
								</table>
								<span class="info">* Silahkan pilih/centang kelengkapan dokumen.</span>
							</fieldset>
							<c:if test="${sessionScope.currentUser.jn_bank eq 2}">
								<fieldset>
								<legend>Username dan Password Otorisasi</legend>
								Username:<input name="userotor" type="text" size="15"> 
								Password:<input name="passotor" type="password" size="15">
								<br><span class="info">* Silahkan masukkan USERNAME dan PASSWORD dari Spv/Pincab untuk melanjutkan input pencairan</span>
								</fieldset>
							</c:if>
							<fieldset>
							<input type="submit" style="width: 150px;" value="Simpan" name="save" onclick="return confirm('Lakukan input pencairan?');" title="Lakukan Input Pencairan">
							<input type="submit" style="width: 150px;" value="Hapus" name="delete" onclick="return confirm('Hapus input pencairan?');" title="Lakukan Penghapusan Pencairan">
							</fieldset>
						</c:if>
					</form:form>
				</div>
			</div>
			
		</div>

	</body>
	<script>
		<c:if test="${not empty param.pesan}">
			alert('${param.pesan}');
		</c:if>
	</script>
</html>