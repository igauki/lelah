<%@ include file="/include/page/header.jsp"%>
<script language="JavaScript">
<!--
<spring:bind path="cmd.datausulan.jml_benef"> 
	var jmlpenerima=${status.value};
</spring:bind>  
		
var varOptionhub ;
var flag_add=0;
var flag_add1=0; 

function generateXML_penerima( objParser, strID, strDesc ) {
	varOptionhub = "";
	var collPosition = objParser.selectNodes( "//Position" );
	for( var i = 0; i < collPosition.length; i++ )  
		varOptionhub += "<OPTION " + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
}
//END OF generateXML_penerima()

function kuasa_onClick(){
	with(document.frmParam){
		if(mrc_kuasa1.checked == true){
			mrc_kuasa1.checked = true;
			elements['rekening_client.mrc_kuasa'].value = '1';
		}else{
			mrc_kuasa1.checked = false;
			elements['rekening_client.mrc_kuasa'].value = '0';
		}
	}
}
//END OF kuasa_onClick()

function Body_onload() {
	with(document.frmParam){
		if (elements['rekening_client.mrc_kuasa'].value == '1') {
			mrc_kuasa1.checked = true;
		}else{
			mrc_kuasa1.checked = false;
		}
		
		if (elements['account_recur.flag_set_auto'].value == '1') {
			tgl_debet.checked = true;
		}else{
			tgl_debet.checked = false;
		}
		elements['powersave.msl_tgl_nab'].style.backgroundColor ='#D4D4D4';		
		elements['powersave.msl_nab'].style.backgroundColor ='#D4D4D4';		
		//
		var nameSelection
		xmlData.async = false;
		<c:choose>
			<c:when test="${empty sessionScope.currentUser.cab_bank}">
				xmlData.src = "${path}/xml/RELATION.xml";
			</c:when>
			<c:otherwise>
				xmlData.src = "${path}/xml/RELATION_BANCASS_BENEF.xml";
			</c:otherwise>
		</c:choose>
		generateXML_penerima( xmlData, 'ID','RELATION');
		//
		var flag_account = elements['datausulan.flag_account'].value ;
		var flagbungasimponi = elements['datausulan.isBungaSimponi'].value;
		var flagbonustahapan= elements['datausulan.isBonusTahapan'].value;
		var flag_bao =  elements['datausulan.flag_bao'].value;
		var kode_flag = elements['datausulan.kode_flag'].value;
		//
		if ( flag_bao ==1 && (flag_account==2 || flag_account==3) ){
			elements['rekening_client.mrc_jn_nasabah'].disabled=false;
			elements['rekening_client.mrc_jn_nasabah'].style.backgroundColor ='#FFFFFF';				
		}else{
			elements['rekening_client.mrc_jn_nasabah'].value='0';	
			elements['rekening_client.mrc_jn_nasabah'].disabled=true;
			elements['rekening_client.mrc_jn_nasabah'].style.backgroundColor ='#D4D4D4';		
		}
		//
		if (flagbungasimponi==1){
			elements['pemegang.mspo_under_table'].readOnly = true;
			elements['pemegang.mspo_under_table'].style.backgroundColor ='#D4D4D4';
			elements['pemegang.tgl_mspo_under_table'].readOnly = true;
			elements['pemegang.tgl_mspo_under_table'].style.backgroundColor ='#D4D4D4';
		}else{
			elements['pemegang.mspo_under_table'].readOnly = true;
			elements['pemegang.mspo_under_table'].style.backgroundColor ='#D4D4D4';
			elements['pemegang.mspo_under_table'].value='';
			elements['pemegang.tgl_mspo_under_table'].readOnly = true;
			elements['pemegang.tgl_mspo_under_table'].style.backgroundColor ='#D4D4D4';
			elements['pemegang.tgl_mspo_under_table'].value='';
		}
		//
		if (flagbonustahapan == 1){
			elements['pemegang.bonus_tahapan'].readOnly = false;
			elements['pemegang.bonus_tahapan'].style.backgroundColor ='#FFFFFF';
		}else{
			elements['pemegang.bonus_tahapan'].readOnly = true;
			elements['pemegang.bonus_tahapan'].style.backgroundColor ='#D4D4D4';
			elements['pemegang.bonus_tahapan'].value='';	
		}
		//	
		if ((elements['datausulan.mste_flag_cc'].value=='1') || (elements['datausulan.mste_flag_cc'].value=='2') || (flag_account== 3)){
			elements['account_recur.mar_holder'].readOnly = false;
			elements['account_recur.mar_holder'].style.backgroundColor ='#FFFFFF';
			if (elements['account_recur.mar_holder'].value =='')
			{
				elements['account_recur.mar_holder'].value = 	elements['pemegang.mcl_first'].value;
			}				
			
			elements['account_recur.lbn_id'].disabled = false;
			elements['account_recur.lbn_id'].style.backgroundColor ='#FFFFFF';
			elements['account_recur.mar_acc_no'].readOnly = false;
			elements['account_recur.mar_acc_no'].style.backgroundColor ='#FFFFFF';
			caribank2.readOnly = false;
			caribank2.style.backgroundColor ='#FFFFFF';
			btncari2.disabled = false;
		}else{
			elements['account_recur.mar_holder'].readOnly = true;
			elements['account_recur.mar_holder'].style.backgroundColor ='#D4D4D4';
			elements['account_recur.mar_holder'].value='';			
			elements['account_recur.lbn_id'].disabled = true;
			elements['account_recur.lbn_id'].style.backgroundColor ='#D4D4D4';
			elements['account_recur.lbn_id'].value='NULL';
			elements['account_recur.mar_acc_no'].readOnly = true;
			elements['account_recur.mar_acc_no'].style.backgroundColor ='#D4D4D4';
			elements['account_recur.mar_acc_no'].value='';
			caribank2.readOnly = true;
			caribank2.style.backgroundColor ='#D4D4D4';
			caribank2.value='';
			btncari2.disabled = true;
		}
		//
		if (flag_account ==2 || flag_account ==3){
			elements['rekening_client.mrc_no_ac'].readOnly = false;
			elements['rekening_client.mrc_no_ac'].style.backgroundColor ='#FFFFFF';
			elements['rekening_client.mrc_nama'].readOnly = false;
			elements['rekening_client.mrc_nama'].style.backgroundColor ='#FFFFFF';
			if (elements['rekening_client.mrc_nama'].value ==""){
				elements['rekening_client.mrc_nama'].value = elements['pemegang.mcl_first'].value;
			}
			elements['rekening_client.lsbp_id'].readOnly = false;
			elements['rekening_client.lsbp_id'].style.backgroundColor ='#FFFFFF';
			elements['rekening_client.mrc_kota'].readOnly = false;
			elements['rekening_client.mrc_kota'].style.backgroundColor ='#FFFFFF';
			elements['rekening_client.tgl_surat'].readOnly = false;
			elements['rekening_client.tgl_surat'].style.backgroundColor ='#FFFFFF';
			elements['rekening_client.notes'].readOnly = false;
			elements['rekening_client.notes'].style.backgroundColor ='#FFFFFF';
			elements['rekening_client.mrc_kuasa'].readOnly = false;
			elements['rekening_client.mrc_kuasa'].style.backgroundColor ='#FFFFFF';
			elements['rekening_client.mrc_cabang'].readOnly = false;
			elements['rekening_client.mrc_cabang'].style.backgroundColor ='#FFFFFF';
			elements['rekening_client.mrc_jenis'].disabled = false;
			elements['rekening_client.mrc_jenis'].style.backgroundColor ='#FFFFFF';
			elements['rekening_client.mrc_kurs'].disabled = false;
			elements['rekening_client.mrc_kurs'].style.backgroundColor ='#FFFFFF';
			caribank1.readOnly = false;
			caribank1.style.backgroundColor ='#FFFFFF';
			btncari1.disabled = false;
		}else{
			elements['rekening_client.mrc_no_ac'].readOnly = true;
			elements['rekening_client.mrc_no_ac'].style.backgroundColor ='#D4D4D4';
			elements['rekening_client.mrc_no_ac'].value = '';
			elements['rekening_client.mrc_nama'].readOnly = true;
			elements['rekening_client.mrc_nama'].style.backgroundColor ='#D4D4D4';
			elements['rekening_client.mrc_nama'].value='';
			elements['rekening_client.lsbp_id'].disabled = true;
			elements['rekening_client.lsbp_id'].style.backgroundColor ='#D4D4D4';
			elements['rekening_client.lsbp_id'].value='NULL';
			elements['rekening_client.mrc_kota'].readOnly = true;
			elements['rekening_client.mrc_kota'].style.backgroundColor ='#D4D4D4';
			elements['rekening_client.mrc_kota'].value='';
			elements['rekening_client.tgl_surat'].readOnly = true;
			elements['rekening_client.tgl_surat'].style.backgroundColor ='#D4D4D4';
			elements['rekening_client.tgl_surat'].value='';			
			elements['rekening_client.notes'].readOnly = true;
			elements['rekening_client.notes'].style.backgroundColor ='#D4D4D4';
			elements['rekening_client.notes'].value='';
			mrc_kuasa1.disabled = true;
			mrc_kuasa1.checked = false;
			elements['rekening_client.mrc_kuasa'].readOnly = true;
			elements['rekening_client.mrc_kuasa'].style.backgroundColor ='#D4D4D4';
			elements['rekening_client.mrc_kuasa'].value='0';
			elements['rekening_client.mrc_cabang'].readOnly = true;
			elements['rekening_client.mrc_cabang'].style.backgroundColor ='#D4D4D4';
			elements['rekening_client.mrc_cabang'].value='';
			elements['rekening_client.mrc_jenis'].disabled = true;
			elements['rekening_client.mrc_jenis'].style.backgroundColor ='#D4D4D4';
			elements['rekening_client.mrc_jenis'].value='0';
			elements['rekening_client.mrc_kurs'].disabled = true;
			elements['rekening_client.mrc_kurs'].style.backgroundColor ='#D4D4D4';
			elements['rekening_client.mrc_kurs'].value='';			
			caribank1.readOnly = true;
			caribank1.style.backgroundColor ='#D4D4D4';
			caribank1.value='';
			btncari1.disabled = true;
		}
		//
	}
	
	//FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME 

/*0 - tutup
1 - Power save
2 - buka fixed & dyna
3 - buka aggresive
4 - buka fixed & dyna & aggresive
5 - buka secured & dyna dollar
6 - buka Syariah fixed & syariah dyna		*/		
		var jumlah_fund = document.frmParam.elements['investasiutama.jmlh_invest'].value;
		if ((Number(kode_flag)==1) )
		{
				document.frmParam.elements['powersave.mps_jangka_inv'].disabled=false;
				document.frmParam.elements['powersave.mps_jangka_inv'].style.backgroundColor ='#FFFFFF';
				
				document.frmParam.elements['powersave.mps_roll_over'].disabled=false;
				document.frmParam.elements['powersave.mps_roll_over'].style.backgroundColor ='#FFFFFF';
				
				document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].value="";
				document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].disabled=true;
				document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].style.backgroundColor ='#D4D4D4';

				document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].value="";
				document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].disabled=true;
				document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].style.backgroundColor ='#D4D4D4';

				document.frmParam.elements['powersave.begdate_topup'].value="__/__/____";
				document.frmParam.elements['powersave.begdate_topup'].disabled=true;
				document.frmParam.elements['powersave.begdate_topup'].readOnly=true;
				document.frmParam.elements['powersave.begdate_topup'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['powersave.msl_bp_rate'].value="0";
				document.frmParam.elements['powersave.msl_bp_rate'].readOnly=true;
				document.frmParam.elements['powersave.msl_bp_rate'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.mspr_premium.value="";
				
				document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].value="";
				document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].readOnly=true;
				document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].style.backgroundColor ='#D4D4D4';

				document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].value="";
				document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].readOnly=true;
				document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].style.backgroundColor ='#D4D4D4';

				
				document.frmParam.elements['datausulan.total_premi_rider'].value="";
				document.frmParam.elements['investasiutama.total_premi_sementara'].value="";
				
				document.frmParam.elements['powersave.tarik_bunga'].value="0";
				document.frmParam.elements['powersave.tarik_bunga'].readOnly=true;
				document.frmParam.elements['powersave.tarik_bunga'].disabled=true;
				document.frmParam.elements['powersave.tarik_bunga'].style.backgroundColor ='#D4D4D4';
				
				tutup();
				jns_rate();
			}else{
				if ((Number(kode_flag)!=11))
				{
					document.frmParam.elements['powersave.mps_jangka_inv'].value="";
					document.frmParam.elements['powersave.mps_jangka_inv'].disabled=true;
					document.frmParam.elements['powersave.mps_jangka_inv'].style.backgroundColor ='#D4D4D4';
					
					document.frmParam.elements['powersave.mps_rate'].value="";				
					document.frmParam.elements['powersave.mps_prm_interest'].value="";
					document.frmParam.elements['powersave.mps_prm_deposit'].value="";
					
					document.frmParam.elements['powersave.mps_roll_over'].value="";
					document.frmParam.elements['powersave.mps_roll_over'].disabled=true;
					document.frmParam.elements['powersave.mps_roll_over'].style.backgroundColor ='#D4D4D4';
					
					document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].disabled=false;
					document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].style.backgroundColor ='#FFFFFF';
	
					document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].disabled=false;
					document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].style.backgroundColor ='#FFFFFF';
	
					
					if (document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].value=="")
					{
						document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].value="0";
					}
					document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].readOnly=false;
					document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].style.backgroundColor ='#FFFFFF';
	
					if (document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].value=="")
					{
						document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].value="0";
					}
					document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].readOnly=false;
					document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].style.backgroundColor ='#FFFFFF';
					
					document.frmParam.elements['powersave.mps_rate'].readOnly=true;
					document.frmParam.elements['powersave.mps_rate'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['powersave.mps_rate'].value = "";
					document.frmParam.elements['powersave.mps_employee'].disabled=true;
					document.frmParam.elements['powersave.mps_employee'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['powersave.mps_employee'].value = "";				
					document.frmParam.elements['powersave.mpr_note'].readOnly=true;
					document.frmParam.elements['powersave.mpr_note'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['powersave.mpr_note'].value = "";
					document.frmParam.elements['powersave.mps_jenis_plan'].disabled=true;
					document.frmParam.elements['powersave.mps_jenis_plan'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['powersave.mps_jenis_plan'].value = "";
					
				document.frmParam.elements['powersave.begdate_topup'].value="__/__/____";
				document.frmParam.elements['powersave.begdate_topup'].disabled=true;
				document.frmParam.elements['powersave.begdate_topup'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['powersave.msl_bp_rate'].value="0";
				document.frmParam.elements['powersave.msl_bp_rate'].readOnly=true;
				document.frmParam.elements['powersave.msl_bp_rate'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['powersave.tarik_bunga'].value="0";
				document.frmParam.elements['powersave.tarik_bunga'].readOnly=true;
				document.frmParam.elements['powersave.tarik_bunga'].disabled=true;
				document.frmParam.elements['powersave.tarik_bunga'].style.backgroundColor ='#D4D4D4';
				}
			}	

		if ((Number(kode_flag)==1) || (Number(kode_flag)==0) )
		{
			document.frmParam.elements['datausulan.mste_flag_investasi'].value = '0';
			document.frmParam.elements['datausulan.mste_flag_investasi'][0].disabled=true;
			document.frmParam.elements['datausulan.mste_flag_investasi'][1].disabled=true;
		}

		switch (Number(kode_flag))
		{
			case 0:
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[7].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[7].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[7].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[7].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[8].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[8].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[8].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[8].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[9].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[9].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[9].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[9].mdu_jumlah1'].value="";	
				
				document.frmParam.elements['investasiutama.daftarinvestasi[10].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[10].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[10].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[10].mdu_jumlah1'].value="";	
				
				document.frmParam.elements['investasiutama.daftarinvestasi[11].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[11].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[11].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[11].mdu_jumlah1'].value="";		
				
				document.frmParam.elements['investasiutama.daftarinvestasi[12].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[12].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[12].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[12].mdu_jumlah1'].value="";		
				
				document.frmParam.elements['investasiutama.daftarinvestasi[13].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[13].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[13].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[13].mdu_jumlah1'].value="";		
				
				document.frmParam.elements['investasiutama.daftarinvestasi[14].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[14].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[14].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[14].mdu_jumlah1'].value="";													
				
				document.frmParam.elements['investasiutama.daftarinvestasi[15].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[15].mdu_persen1'].readOnly=true;	
				document.frmParam.elements['investasiutama.daftarinvestasi[15].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				document.frmParam.elements['investasiutama.daftarinvestasi[15].mdu_jumlah1'].value="";		
				
				document.frmParam.elements['investasiutama.daftarinvestasi[16].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[16].mdu_persen1'].readOnly=true;	
				document.frmParam.elements['investasiutama.daftarinvestasi[16].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				document.frmParam.elements['investasiutama.daftarinvestasi[16].mdu_jumlah1'].value="";	
				
				document.frmParam.elements['investasiutama.daftarinvestasi[17].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[17].mdu_persen1'].readOnly=true;	
				document.frmParam.elements['investasiutama.daftarinvestasi[17].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				document.frmParam.elements['investasiutama.daftarinvestasi[17].mdu_jumlah1'].value="";	
				
				document.frmParam.elements['investasiutama.daftarinvestasi[18].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[18].mdu_persen1'].readOnly=true;	
				document.frmParam.elements['investasiutama.daftarinvestasi[18].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				document.frmParam.elements['investasiutama.daftarinvestasi[18].mdu_jumlah1'].value="";	
				
				document.frmParam.elements['investasiutama.daftarinvestasi[19].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[19].mdu_persen1'].readOnly=true;	
				document.frmParam.elements['investasiutama.daftarinvestasi[19].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				document.frmParam.elements['investasiutama.daftarinvestasi[19].mdu_jumlah1'].value="";	
				
				document.frmParam.elements['investasiutama.daftarinvestasi[20].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[20].mdu_persen1'].readOnly=true;	
				document.frmParam.elements['investasiutama.daftarinvestasi[20].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				document.frmParam.elements['investasiutama.daftarinvestasi[20].mdu_jumlah1'].value="";	
				
				document.frmParam.elements['investasiutama.total_persen'].value="";
				
				document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].value="";
				document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].disabled=true;
				document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].style.backgroundColor ='#D4D4D4';

				document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].value="";
				document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].disabled=true;
				document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.mspr_premium.value="";
				
				document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].value="";
				document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].readOnly=true;
				document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].style.backgroundColor ='#D4D4D4';

				document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].value="";
				document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].readOnly=true;
				document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['datausulan.total_premi_rider'].value="";
				document.frmParam.elements['investasiutama.total_premi_sementara'].value="";				

				document.frmParam.elements['powersave.mps_jangka_inv'].value="";
				document.frmParam.elements['powersave.mps_jangka_inv'].disabled=true;
				document.frmParam.elements['powersave.mps_jangka_inv'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['powersave.mps_rate'].value="";				
				document.frmParam.elements['powersave.mps_prm_interest'].value="";
				document.frmParam.elements['powersave.mps_prm_deposit'].value="";
				
				document.frmParam.elements['powersave.mps_roll_over'].value="";
				document.frmParam.elements['powersave.mps_roll_over'].disabled=true;
				document.frmParam.elements['powersave.mps_roll_over'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['powersave.mps_rate'].readOnly=true;
				document.frmParam.elements['powersave.mps_rate'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['powersave.mps_rate'].value = "";
				document.frmParam.elements['powersave.mps_employee'].disabled=true;
				document.frmParam.elements['powersave.mps_employee'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['powersave.mps_employee'].value = "";				
				document.frmParam.elements['powersave.mpr_note'].readOnly=true;
				document.frmParam.elements['powersave.mpr_note'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['powersave.mpr_note'].value = "";
				document.frmParam.elements['powersave.mps_jenis_plan'].disabled=true;
				document.frmParam.elements['powersave.mps_jenis_plan'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['powersave.mps_jenis_plan'].value = "";				
	
				document.frmParam.elements['investasiutama.daftarinvestasi[21].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[21].mdu_persen1'].readOnly=true;	
				document.frmParam.elements['investasiutama.daftarinvestasi[21].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				document.frmParam.elements['investasiutama.daftarinvestasi[22].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[22].mdu_persen1'].readOnly=true;	
				document.frmParam.elements['investasiutama.daftarinvestasi[22].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				
			break;
			
			case 1:
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_jumlah1'].value="";
			
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_jumlah1'].value="";
					
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[7].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[7].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[7].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[7].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[8].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[8].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[8].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[8].mdu_jumlah1'].value="";
				
				document.frmParam.elements['investasiutama.daftarinvestasi[9].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[9].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[9].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[9].mdu_jumlah1'].value="";	
				
				document.frmParam.elements['investasiutama.daftarinvestasi[10].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[10].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[10].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[10].mdu_jumlah1'].value="";	
				
				document.frmParam.elements['investasiutama.daftarinvestasi[11].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[11].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[11].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[11].mdu_jumlah1'].value="";		
				
				document.frmParam.elements['investasiutama.daftarinvestasi[12].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[12].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[12].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[12].mdu_jumlah1'].value="";		
				
				document.frmParam.elements['investasiutama.daftarinvestasi[13].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[13].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[13].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[13].mdu_jumlah1'].value="";		
				
				document.frmParam.elements['investasiutama.daftarinvestasi[14].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[14].mdu_persen1'].readOnly=true;
				document.frmParam.elements['investasiutama.daftarinvestasi[14].mdu_persen1'].style.backgroundColor ='#D4D4D4';				
				document.frmParam.elements['investasiutama.daftarinvestasi[14].mdu_jumlah1'].value="";			
				
				document.frmParam.elements['investasiutama.daftarinvestasi[15].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[15].mdu_persen1'].readOnly=true;	
				document.frmParam.elements['investasiutama.daftarinvestasi[15].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				document.frmParam.elements['investasiutama.daftarinvestasi[15].mdu_jumlah1'].value="";		
					
				document.frmParam.elements['investasiutama.daftarinvestasi[16].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[16].mdu_persen1'].readOnly=true;	
				document.frmParam.elements['investasiutama.daftarinvestasi[16].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				document.frmParam.elements['investasiutama.daftarinvestasi[16].mdu_jumlah1'].value="";	
					
				document.frmParam.elements['investasiutama.daftarinvestasi[17].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[17].mdu_persen1'].readOnly=true;	
				document.frmParam.elements['investasiutama.daftarinvestasi[17].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				document.frmParam.elements['investasiutama.daftarinvestasi[17].mdu_jumlah1'].value="";	
				
				document.frmParam.elements['investasiutama.daftarinvestasi[18].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[18].mdu_persen1'].readOnly=true;	
				document.frmParam.elements['investasiutama.daftarinvestasi[18].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				document.frmParam.elements['investasiutama.daftarinvestasi[18].mdu_jumlah1'].value="";	
				
				document.frmParam.elements['investasiutama.daftarinvestasi[19].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[19].mdu_persen1'].readOnly=true;	
				document.frmParam.elements['investasiutama.daftarinvestasi[19].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				document.frmParam.elements['investasiutama.daftarinvestasi[19].mdu_jumlah1'].value="";	
	
				document.frmParam.elements['investasiutama.daftarinvestasi[20].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[20].mdu_persen1'].readOnly=true;	
				document.frmParam.elements['investasiutama.daftarinvestasi[20].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				document.frmParam.elements['investasiutama.daftarinvestasi[20].mdu_jumlah1'].value="";	
				
				document.frmParam.elements['investasiutama.total_persen'].value="";

				document.frmParam.elements['investasiutama.daftarinvestasi[21].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[21].mdu_persen1'].readOnly=true;	
				document.frmParam.elements['investasiutama.daftarinvestasi[21].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				document.frmParam.elements['investasiutama.daftarinvestasi[22].mdu_persen1'].value="";
				document.frmParam.elements['investasiutama.daftarinvestasi[22].mdu_persen1'].readOnly=true;	
				document.frmParam.elements['investasiutama.daftarinvestasi[22].mdu_persen1'].style.backgroundColor ='#D4D4D4';	

			break;
			
			case 9:
				//EKALINK 88 RUPIAH
				if (document.frmParam.elements['datausulan.lsbs_id'].value == 162 && document.frmParam.elements['datausulan.lsdbs_number'].value > 4){
					document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].value="";
					document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].readOnly=true;
					document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].style.backgroundColor ='#D4D4D4';			
					
					document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].value="";
					document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].readOnly=true;
					document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].style.backgroundColor ='#D4D4D4';					
					
					document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].value="";
					document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].readOnly=true;
					document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].style.backgroundColor ='#D4D4D4';						
				
					document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].readOnly=false;
					document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].style.backgroundColor ='#FFFFFF';				
					
					document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].readOnly=false;
					document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].style.backgroundColor ='#FFFFFF';				
					
					document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].readOnly=false;
					document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].style.backgroundColor ='#FFFFFF';	
				
				//ARTHALINK RUPIAH
				}else{
					document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].readOnly=false;
					document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].style.backgroundColor ='#FFFFFF';				
					
					document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].readOnly=false;
					document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].style.backgroundColor ='#FFFFFF';				
					
					document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].readOnly=false;
					document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].style.backgroundColor ='#FFFFFF';				
				
					document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].value="";
					document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].readOnly=true;
					document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].style.backgroundColor ='#D4D4D4';			
					
					document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].value="";
					document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].readOnly=true;
					document.frmParam.elements['investasiutama.daftarinvestasi[4].mdu_persen1'].style.backgroundColor ='#D4D4D4';					
					
					document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].value="";
					document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].readOnly=true;
					document.frmParam.elements['investasiutama.daftarinvestasi[5].mdu_persen1'].style.backgroundColor ='#D4D4D4';					
				}	
			break;		
			
			case 10:
				//EKALINK 88 DOLLAR
				if (document.frmParam.elements['datausulan.lsbs_id'].value == 162 && document.frmParam.elements['datausulan.lsdbs_number'].value > 4){
					document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].value="";
					document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].readOnly=true;
					document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].style.backgroundColor ='#D4D4D4';			
				
					document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].value="";
					document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].readOnly=true;
					document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].style.backgroundColor ='#D4D4D4';			
				
				
					document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].readOnly=false;
					document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].style.backgroundColor ='#FFFFFF';				
					
					document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].readOnly=false;
					document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].style.backgroundColor ='#FFFFFF';				
					
				//ARTHALINK DOLLAR
				}else{
					document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].value="";
					document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].readOnly=true;
					document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].style.backgroundColor ='#D4D4D4';			
					
					document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].value="";
					document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].readOnly=true;
					document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].style.backgroundColor ='#D4D4D4';					
					
					document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].readOnly=false;
					document.frmParam.elements['investasiutama.daftarinvestasi[2].mdu_persen1'].style.backgroundColor ='#FFFFFF';				
			
					document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].readOnly=false;
					document.frmParam.elements['investasiutama.daftarinvestasi[3].mdu_persen1'].style.backgroundColor ='#FFFFFF';
				}	
			break;
																			
			case 11:
				document.frmParam.elements['investasiutama.total_persen'].value="100";
				//STABLE LINK RUPIAH
				if (document.frmParam.elements['datausulan.kurs_premi'].value == '01'){
					document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].value="100";
					document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
					
					document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].value="";
					document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				//STABLE LINK DOLLAR
				}else{
					document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].value="";
					document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[0].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				
					document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].value="100";
					document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[1].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				}
			break;		
		}
		
	}
	
	function jns_rate()
	{
		if ((document.frmParam.elements['datausulan.lsbs_id'].value=='158') &&( (document.frmParam.elements['datausulan.lsdbs_number'].value=='5') || (document.frmParam.elements['datausulan.lsdbs_number'].value=='8') || (document.frmParam.elements['datausulan.lsdbs_number'].value=='9') ))
		{
			if ((document.frmParam.elements['powersave.mps_jangka_inv'].value=='12') || (document.frmParam.elements['powersave.mps_jangka_inv'].value=='36'))
			{
				if ((document.frmParam.elements['powersave.mps_roll_over'].value=='')  || (document.frmParam.elements['powersave.mps_roll_over'].value=='1')) 
				{
					document.frmParam.elements['powersave.mps_roll_over'].value='2';
				}
			}
		}
				
		if ((document.frmParam.elements['datausulan.kode_flag'].value=='1') || (document.frmParam.elements['datausulan.kode_flag'].value=='11'))
		{	
			if (document.frmParam.elements['powersave.mps_jenis_plan'].value == "0")
			{
				document.frmParam.elements['powersave.mps_rate'].readOnly=true;
				document.frmParam.elements['powersave.mps_rate'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['powersave.mpr_note'].readOnly=true;
				document.frmParam.elements['powersave.mpr_note'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['powersave.mpr_note'].value = "";
			}else{
				document.frmParam.elements['powersave.mps_rate'].readOnly=false;
				document.frmParam.elements['powersave.mps_rate'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['powersave.mpr_note'].readOnly=false;
				document.frmParam.elements['powersave.mpr_note'].style.backgroundColor ='#FFFFFF';
			}
		}

<%--
<c:if test="${sessionScope.currentUser.jn_bank eq 0 or sessionScope.currentUser.jn_bank eq 1}">
		document.frmParam.caribank1.value = 'BII';
		document.frmParam.caribank1.readOnly = true;
		document.frmParam.caribank1.style.backgroundColor ='#FFFFFF';
		document.frmParam.btncari1.click();
		document.frmParam.btncari1.disabled = true;

		document.frmParam.caribank2.value = 'BII';
		document.frmParam.caribank2.readOnly = true;
		document.frmParam.caribank2.style.backgroundColor ='#FFFFFF';
		document.frmParam.btncari2.click();
		document.frmParam.btncari2.disabled = true;
</c:if>
		
<c:if test="${sessionScope.currentUser.jn_bank eq 2}">
		document.frmParam.caribank1.value = 'BANK SINAR MAS';
		document.frmParam.caribank1.readOnly = true;
		document.frmParam.caribank1.style.backgroundColor ='#FFFFFF';
		document.frmParam.btncari1.click();
		document.frmParam.btncari1.disabled = true;

		document.frmParam.caribank2.value = 'BANK SINAR MAS';
		document.frmParam.caribank2.readOnly = true;
		document.frmParam.caribank2.style.backgroundColor ='#FFFFFF';
		document.frmParam.btncari2.click();
		document.frmParam.btncari2.disabled = true;
</c:if>
--%>		
	}

 function sts(hasil)
 {
	document.frmParam.elements['datausulan.mste_flag_investasi'].value = hasil;
 }	
	
	function tutup()
	{
		if ((document.frmParam.elements['datausulan.kode_flag'].value=='1') || (document.frmParam.elements['datausulan.kode_flag'].value=='11'))
		{
			if (document.frmParam.elements['powersave.mps_roll_over'].value == '1')
			{

			}else{
				document.frmParam.elements['rekening_client.mrc_no_ac'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_nama'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_nama'].style.backgroundColor ='#FFFFFF';
				if (document.frmParam.elements['rekening_client.mrc_nama'].value =="")
				{
					document.frmParam.elements['rekening_client.mrc_nama'].value =  document.frmParam.elements['pemegang.mcl_first'].value;;
				}				
				
				document.frmParam.elements['rekening_client.lsbp_id'].readOnly = false;
				document.frmParam.elements['rekening_client.lsbp_id'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_kota'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_kota'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.tgl_surat'].readOnly = false;
				document.frmParam.elements['rekening_client.tgl_surat'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.notes'].readOnly = false;
				document.frmParam.elements['rekening_client.notes'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_kuasa'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_kuasa'].style.backgroundColor ='#FFFFFF';
				document.frmParam.mrc_kuasa1.disabled = false;			
				document.frmParam.elements['rekening_client.mrc_cabang'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_cabang'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_jenis'].disabled = false;
				document.frmParam.elements['rekening_client.mrc_jenis'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_kurs'].disabled = false;
				document.frmParam.elements['rekening_client.mrc_kurs'].style.backgroundColor ='#FFFFFF';
				document.frmParam.caribank1.readOnly = false;
				document.frmParam.caribank1.style.backgroundColor ='#FFFFFF';
				document.frmParam.btncari1.disabled = false;
			}
		}
	}
				
	function addRowDOM1 (tableID, jml) {
		flag_add1=1
   		var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
		var tmp = parseInt(document.getElementById("tableProd").rows.length);
		var oRow = oTable.insertRow(oTable.rows.length);
        var oCells = oRow.cells;
		var i=tmp-1;
		var idx = jmlpenerima + 1;
	
		if (document.all) {
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td >"+idx+"</td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td ><input type=text name='benef.msaw_first"+idx+"' value='' size=30></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML = "<td><input type=text name='tgllhr"+idx+"' value='' size=3>/<input type=text name='blnhr"+idx+"' value='' size=3>/<input type=text name='thnhr"+idx+"' value='' size=5><input type=hidden name='msaw_birth"+idx+"' value='' size=5></td>";         
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML =  "<td ><select name='benef.lsre_id"+idx+"'>"+varOptionhub+"</select></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML ="<td><input type=text name='benef.msaw_persen"+idx+"' value='' size=5 ></td>";
			var cell = oRow.insertCell(oCells.length);
			cell.innerHTML ="<td ><input type=checkbox name=cek"+idx+" id=ck"+idx+"' class=\"noBorder\" ></td></tr>";
			var cell = oRow.insertCell(oCells.length);			
			cell.innerHTML = '';
			jmlpenerima = idx;
		}
 	}
 
 
	function next()
	{
		eval( " document.frmParam.elements['pemegang.indeks_halaman'].value = ((document.frmParam.hal.value)+1);");
		//alert(jmlpenerima);
		document.frmParam.jmlpenerima.value=jmlpenerima;
		//alert(document.frmParam.jmlpenerima.value);
	} 
	function prev()
	{
		document.frmParam.jmlpenerima.value=jmlpenerima;
	}	

	function cancel1()
	{		
		var idx = jmlpenerima;
	//(idx)!=null &&
		if  ( (idx)!="")
		{
			if (idx >0)
				flag_add1=1;
		}
		if(flag_add1==1)
		{
			deleteRowDOM1('tableProd', '1');
		}
	}		

function deleteRowDOM1 (tableID,rowCount)
 { 
    var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
    var row=parseInt(document.getElementById("tableProd").rows.length);
	var flag_row=0;
	var jumlah_cek=0;
	var idx = jmlpenerima;

	if(row>0)
	{
		flag_row=0;
		//alert(row);
		for (var i=1;i<((parseInt(row)));i++)
		{

			if (eval("document.frmParam.elements['cek"+i+"'].checked"))
			{
				idx=idx-1;

				for (var k =i ; k<((parseInt(row))-1);k++)
				{

					eval(" document.frmParam.elements['benef.msaw_first"+k+"'].value =document.frmParam.elements['benef.msaw_first"+(k+1)+"'].value;");   
					eval(" document.frmParam.elements['tgllhr"+k+"'].value =document.frmParam.elements['tgllhr"+(k+1)+"'].value;"); 
					eval(" document.frmParam.elements['blnhr"+k+"'].value = document.frmParam.elements['blnhr"+(k+1)+"'].value;");			      
					eval(" document.frmParam.elements['thnhr"+k+"'].value = document.frmParam.elements['thnhr"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['msaw_birth"+k+"'].value = document.frmParam.elements['msaw_birth"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['benef.lsre_id"+k+"'].value = document.frmParam.elements['benef.lsre_id"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['benef.msaw_persen"+k+"'].value = document.frmParam.elements['benef.msaw_persen"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['cek"+k+"'].checked = document.frmParam.elements['cek"+(k+1)+"'].checked;");
				}
				oTable.deleteRow(parseInt(document.getElementById("tableProd").rows.length)-1);
				row=row-1;
				//alert(jmlpenerima);
				jmlpenerima = idx;
				//alert(jmlpenerima);
				i=i-1;
			}
		}


		row=parseInt(document.getElementById("tableProd").rows.length);
			if(row==1)	
				flag_add=0;
		
	}
}

	if (!document.layers&&!document.all&&!document.getElementById)
		event="test"
		
	function showtip(current,e,text){
	
		if (document.all||document.getElementById){
			thetitle=text.split('<br>')
			if (thetitle.length>1){
				thetitles=''
				
			for (i=0;i<thetitle.length;i++)
				thetitles+=thetitle[i]
				
			current.title=thetitles
		}
		else
			current.title=text
	}
	
	else if (document.layers){
				document.tooltip.document.write('<layer bgColor="white" style="border:1px solid black;font-size:13px;">'+text+'</layer>')
				document.tooltip.document.close()
				document.tooltip.left=e.pageX+5
				document.tooltip.top=e.pageY+5
				document.tooltip.visibility="show"
			}
	}
	
	function hidetip(){
		if (document.layers)
			document.tooltip.visibility="hidden"
	}
		

// -->
</script>



<body bgcolor="ffffff" onLoad="Body_onload();">
2=42200700436
<br/>
KODE_FLAG = ${cmd.datausulan.kode_flag}
<XML ID=xmlData></XML> 
 <form name="frmParam" method="post" >
<table border="0" width="100%" height="677" cellspacing="1" cellpadding="1" class="entry2">
  <tr> 
    <td width="67%" height="35" bgcolor="#FDFDFD" valign="bottom"> 
 			<input type="submit" name="_target0" value=" " onclick=" prev()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/pp1.jpg);"
				accesskey="1" onmouseover="return overlib('Alt-1', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target1" value=" " onclick=" prev()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ttg1.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target2" value=" " onclick=" prev()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ddu1.jpg);"
				accesskey="3" onmouseover="return overlib('Alt-3', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target3" value=" " onclick=" prev()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/inv2.jpg);"
				accesskey="4" onmouseover="return overlib('Alt-4', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target4" value=" " onclick=" prev()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ag1.jpg);"
				accesskey="5" onmouseover="return overlib('Alt-5', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target5" value=" " onclick=" prev()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/kf1.jpg);"
				accesskey="6" onmouseover="return overlib('Alt-6', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" name="dummy2" value=" " onclick=" prev()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/sb1.jpg);">		
    </td>
  </tr>
  <tr><td bgcolor="#DDDDDD">
  
  </td></tr>
  <tr> 
    <td width="67%" rowspan="5" align="left" valign="top"> 
<spring:bind path="cmd.*">
	<c:if test="${not empty status.errorMessages}">
	<div id="error">
	ERROR:<br>
	<c:forEach var="error" items="${status.errorMessages}">
	- <c:out value="${error}" escapeXml="false" />
	<br />
	</c:forEach></div>
	</c:if>									
</spring:bind>     
        <table border="0" width="100%" cellspacing="1" cellpadding="1"  class="entry">

          <tr> 
            <td  align="center" colspan="4"> 
              <div align="center">
                <input type="submit" name="_target${halaman-1}" value="Prev &laquo;" onClick="prev()" tabindex="19"
					onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
                <input type="submit" name="_target${halaman+1}" value="Next &raquo;" onClick="next()" tabindex="18"
					onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
                <input type="hidden" name="_page" value="${halaman}">
              </div>
            </td>
          </tr>
        </table>
        <table border="0" width="100%" cellspacing="1" cellpadding="1" height="47" class="entry">
          <tr> 
            <th class="subtitle"  height="27" colspan="9"> <p align="center"><b> 
                <font color="#FFFFFF" size="2" face="Verdana">DATA PRODUK INVESTASI</font></b> 
                <spring:bind path="cmd.datausulan.flag_bao"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }" >
                </spring:bind> <spring:bind path="cmd.datausulan.kode_flag"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }"  >
                </spring:bind> <spring:bind path="cmd.datausulan.isBungaSimponi"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }"  >
                </spring:bind> <spring:bind path="cmd.datausulan.isBonusTahapan"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }"  >
                </spring:bind> <spring:bind path="cmd.datausulan.mste_flag_cc"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }"  >
                </spring:bind> <spring:bind path="cmd.datausulan.flag_account"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }"  >
                </spring:bind> 
                <spring:bind path="cmd.datausulan.lsbs_id"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }"  >
                </spring:bind> 
                <spring:bind path="cmd.datausulan.lsdbs_number"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }"  >
                </spring:bind>
                <spring:bind path="cmd.pemegang.mcl_first"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30" tabindex="1">
                </spring:bind> 
                <spring:bind path="cmd.datausulan.kurs_premi"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30" tabindex="1">
                </spring:bind></th>
          </tr>
          <tr  > 
            <th  height="20 " colspan="9"> <p align="left"><b> <font face="Verdana" size="1" color="#CC3300">Detail 
                Investasi Khusus Produk Unit Link</font></b></p></th>
          </tr>
          <tr  > 
            <th class="subtitle2" height="20" width="2%"><b> <font face="Verdana" size="1" color="#FFFFFF">5. 
              a. </font></b></th>
            <th class="subtitle2" colspan="5" height="20"> <b> <font face="Verdana" size="1" color="#FFFFFF">Perhitungan 
              Premi ( sesuai dengan cara pembayaran )</font></b> </th>
          </tr>
          <tr  > 
            <th width="2%">&nbsp;</th>
            <th colspan="2" > <p align="left"><b><font face="Verdana" size="1" color="#996600">Premi 
                Pokok</font></b><font size="1" face="Verdana"> </font> </th>
            <th > <input type="text" name="mspr_premium"
						value=<fmt:formatNumber type="number" value="${cmd.datausulan.mspr_premium}"/> 
              size="30" style='background-color :#D4D4D4' readOnly> </th>
            <th colspan="2" >&nbsp;</th>
          </tr>
          <tr  > 
            <th width="2%">&nbsp;</th>
            <th colspan="2" ><b><font face="Verdana" size="1" color="#996600">Premi 
              Top Up Berkala</font></b></th>
            <th > <spring:bind path="cmd.investasiutama.daftartopup.premi_berkala"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" tabindex="1"  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
             </spring:bind><font color="#CC3300">*</font></th>
            <th width="12%"> <div align="right"><b><font face="Verdana" size="1" color="#996600">Jenis 
                Top Up </font></b> </div></th>
            <th width="32%"> <select name="investasiutama.daftartopup.pil_berkala" tabindex="2" 
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <c:forEach var="jnstopup" items="${select_jns_top_up}"> 
             	  <c:if test="${jnstopup.ID ne 2 }">
					 <option <c:if test="${cmd.investasiutama.daftartopup.pil_berkala eq jnstopup.ID}"> SELECTED </c:if> value="${jnstopup.ID}">${jnstopup.JENIS}</option> 
                  </c:if>
                </c:forEach> </select>
              <font color="#CC3300">*</font> </th>
          </tr>
       <tr  > 
            <th width="2%">&nbsp;</th>
            <th colspan="2" ><b><font face="Verdana" size="1" color="#996600">Premi 
              Top Up Tunggal</font></b></th>
            <th > <spring:bind path="cmd.investasiutama.daftartopup.premi_tunggal"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" tabindex="1"  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
             </spring:bind><font color="#CC3300">*</font></th>
            <th width="12%"> <div align="right"><b><font face="Verdana" size="1" color="#996600">Jenis 
                Top Up </font></b> </div></th>
            <th width="32%"> <select name="investasiutama.daftartopup.pil_tunggal" tabindex="2" 
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <c:forEach var="jnstopup" items="${select_jns_top_up}"> 
                 <c:if test="${jnstopup.ID ne 1 }">
				   	<option <c:if test="${cmd.investasiutama.daftartopup.pil_tunggal eq jnstopup.ID}"> SELECTED </c:if> value="${jnstopup.ID}">${jnstopup.JENIS}</option> 
                 </c:if>
                </c:forEach> </select>
              <font color="#CC3300">*</font> </th>
          </tr>
          <tr  > 
            <th width="2%">&nbsp;</th>
            <th colspan="2" ><b><font face="Verdana" size="1" color="#996600">Premi 
              Tambahan </font></b></th>
            <th > <spring:bind path="cmd.datausulan.total_premi_rider"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" style='background-color :#D4D4D4'readOnly>
               </spring:bind> 
            </th>
            <th colspan="2" >&nbsp;</th>
          </tr>
          <tr  > 
            <th width="2%">&nbsp;</th>
            <th  colspan="3"> <hr> </th>
            <th colspan="2" >&nbsp;</th>
          </tr>
          <tr>
            <th width="2%">&nbsp;</th>
            <th colspan="2" ><b><font face="Verdana" size="1" color="#996600">Jumlah 
              </font></b></th>
            <th > <spring:bind path="cmd.investasiutama.total_premi_sementara"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" style='background-color :#D4D4D4'readOnly>
               </spring:bind> 
            </th>
            <th colspan="2" >&nbsp;</th>
          </tr>
          <tr  > 
            <th class="subtitle2" height="20" width="2%"><b><font face="Verdana" size="1" color="#FFFFFF">&nbsp;&nbsp;b.</font></b></th>
            <th class="subtitle2" colspan="5" height="20"> <b> <font face="Verdana" size="1" color="#FFFFFF">No. 
              Rekening Pemegang Polis Yang Digunakan untuk Transaksi (KHUSUS PRODUK 
              INVESTASI)</font></b> </th>
          </tr>
          <tr  > 
            <th width="2%"  height="20">&nbsp;</th>
            <th  height="20" colspan="2" ><b><font face="Verdana" size="1" color="#996633">No. 
              Rekening&nbsp; </font></b><b><font size="1" face="Verdana"> </font> 
              </b></th>
            <th  height="20" width="24%"><spring:bind path="cmd.rekening_client.mrc_no_ac"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" maxlength="20" tabindex="3"
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>><font color="#CC3300">*</font>
              </spring:bind> 
              <select name="rekening_client.mrc_kurs"
			   <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <c:forEach var="kurs" items="${select_kurs}"> <option 
							<c:if test="${cmd.rekening_client.mrc_kurs eq kurs.ID}"> SELECTED </c:if>
							value="${kurs.ID}">${kurs.SYMBOL}</option> </c:forEach> 
              </select>
              <font color="#CC3300">*</font> </th>
            <th width="15%"  height="20"><font size="1" face="Verdana"><b><font face="Verdana" size="1" color="#996633"> 
              </font></b> </font><b> <font size="1" face="Verdana"><b><font face="Verdana" size="1" color="#996633">Cari 
              Bank&nbsp;</font></b></font><font face="Verdana" size="1" color="#996633"> 
              </font></b> </th>
            <th  height="20"> <input type="text" name="caribank1" onkeypress="if(event.keyCode==13){ document.frmParam.btncari1.click(); return false;}"> 
              <input type="button" name="btncari1" value="Cari" onclick="ajaxSelectWithParam(document.frmParam.caribank1.value,'select_bank1','bank1','rekening_client.lsbp_id','', 'BANK_ID', 'BANK_NAMA', '');"> 
            </th>
          </tr>
          <tr  > 
            <th width="2%"  height="20">&nbsp;</th>
            <th  height="20" colspan="2" ><b><font face="Verdana" size="1" color="#996633">Atas 
              Nama </font></b><b><font size="1" face="Verdana"> </font> </b></th>
            <th  height="20" width="24%"><spring:bind path="cmd.rekening_client.mrc_nama"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="35" maxlength="50" tabindex="4"
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> <font color="#CC3300">*</font>
              </spring:bind></th>
            <th width="15%"  height="20"><font size="1" face="Verdana"> <b><font face="Verdana" size="1" color="#996633">Bank&nbsp; 
              </font></b></font></th>
            <th  height="20"> <div id="bank1"
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> 
                <select name="rekening_client.lsbp_id">
                  <option value="${cmd.rekening_client.lsbp_id}">${cmd.rekening_client.lsbp_nama}</option>
                </select><font color="#CC3300">*</font>
                </div></th>
          </tr>
          <tr  > 
            <th width="2%" >&nbsp;</th>
            <th colspan="2"  ><b><font face="Verdana" size="1" color="#996633">Jenis 
              Tabungan </font></b> </th>
            <th width="24%"> <select name="rekening_client.mrc_jenis" tabindex="5"
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <c:forEach var="tab" items="${select_jenis_tabungan}"> <option 
							<c:if test="${cmd.rekening_client.mrc_jenis eq tab.ID}"> SELECTED </c:if>
							value="${tab.ID}">${tab.AUTODEBET}</option> </c:forEach> 
              </select>
              <font color="#CC3300">*</font> </th>
            <th width="15%" ><font size="1" face="Verdana"><b><font face="Verdana" size="1" color="#996633">Cabang 
              </font></b> </font><b><font size="1" face="Verdana"> </font> </b><font size="1" face="Verdana">&nbsp; 
              </font></th>
            <th  height="20"><spring:bind path="cmd.rekening_client.mrc_cabang"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" maxlength="20" tabindex="8"
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
               </spring:bind></th>
          </tr>
          <tr  > 
            <th width="2%" >&nbsp;</th>
            <th colspan="2"  ><b><font face="Verdana" size="1" color="#996633">Jenis 
              Nasabah </font></b></th>
            <th> <select name="rekening_client.mrc_jn_nasabah" tabindex="6"
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <c:forEach var="nsbh" items="${select_jenis_nasabah}"> <option 
					<c:if test="${cmd.rekening_client.mrc_jn_nasabah eq nsbh.ID}"> SELECTED </c:if>
					value="${nsbh.ID}">${nsbh.JENIS}</option> </c:forEach> </select> 
            </th>
            <th><b><font face="Verdana" size="1" color="#996633">Kota </font></b><b> 
              </b><font size="1" face="Verdana">&nbsp; </font></th>
            <th ><spring:bind path="cmd.rekening_client.mrc_kota"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" maxlength="20" tabindex="9"
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
              </spring:bind></th>
          </tr>
          <tr  > 
            <th >&nbsp;</th>
            <th  valign="top"><b><font face="Verdana" size="1" color="#996633">Memberi 
              Kuasa 
              <input type="checkbox" name="mrc_kuasa1" class="noBorder" 
						value="${cmd.rekening_client.mrc_kuasa}"  size="30" onClick="kuasa_onClick();" tabindex="3"
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
              <spring:bind path="cmd.rekening_client.mrc_kuasa"> 
              <input type="hidden" name="${status.expression}"
						value="${cmd.rekening_client.mrc_kuasa}"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind> &nbsp;&nbsp;&nbsp;&nbsp; </font></b></th>
            <th  valign="top"><b><font face="Verdana" size="1" color="#996633">Keterangan 
              </font></b></th>
            <th> <spring:bind path="cmd.rekening_client.notes"> 
              <textarea cols="40" rows="7" name="${status.expression }"
						tabindex="17" onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>${status.value }</textarea>
               </spring:bind> </th>
            <th valign="top"><b><font color="#996633" size="1" face="Verdana">Tanggal 
              Surat</font></b></th>
            <th valign="top"><spring:bind path="cmd.rekening_client.tgl_surat"> 
              <c:choose> <c:when test="${cmd.datausulan.flag_bao eq '1' or cmd.datausulan.flag_account  eq '2'  or cmd.datausulan.flag_account eq '3'}"> 
              <script>inputDate('${status.expression}', '${status.value}', false);</script>
              </c:when> <c:otherwise> 
              <script>inputDate('${status.expression}', '${status.value}', true);</script>
              </c:otherwise> </c:choose> </spring:bind> </th>
          </tr>
          <tr  > 
            <th class="subtitle2" height="20" width="2%"><b><font face="Verdana" size="1" color="#FFFFFF">&nbsp;&nbsp;c.&nbsp;</font></b></th>
            <th class="subtitle2" colspan="5" height="20"> <b> <font face="Verdana" size="1" color="#FFFFFF">No. 
              Rekening Pemegang Polis Yang Digunakan untuk Transaksi (AUTODEBET)</font></b> 
            </th>
          </tr>
          <tr  > 
            <th width="2%"  height="20">&nbsp;</th>
            <th   height="20" colspan="2"><b> <font face="Verdana" size="1" color="#996633">No. 
              Rekening&nbsp; </font></b></th>
            <th width="24%"  height="20"><spring:bind path="cmd.account_recur.mar_acc_no"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" maxlength="20" tabindex="10"
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
             </spring:bind><font color="#CC3300">*</font></th>
            <th width="15%"  height="20"><b> <font face="Verdana" size="1" color="#996600">Cari 
              &nbsp;</font><font face="Verdana" size="1" color="#996633">Bank&nbsp; 
              </font><font face="Verdana" size="1" color="#996600"> </font> <font face="Verdana" size="1" color="#996633"> 
              </font></b></th>
            <th  height="20" > <input type="text" name="caribank2" onkeypress="if(event.keyCode==13){ document.frmParam.btncari2.click(); return false;}"> 
              <input type="button" name="btncari2" value="Cari" onclick="ajaxSelectWithParam(document.frmParam.caribank2.value,'select_bank2','bank2','account_recur.lbn_id','', 'BANK_ID', 'BANK_NAMA', '');"> 
            </th>
          </tr>
          <tr  > 
            <th width="2%"  height="20">&nbsp;</th>
            <th  height="20" colspan="2"><b> <font face="Verdana" size="1" color="#996633">Atas 
              Nama </font></b></th>
            <th width="24%"  height="20"><spring:bind path="cmd.account_recur.mar_holder"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="35" maxlength="50" tabindex="11"
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>><font color="#CC3300">*</font>
              </spring:bind></th>
            <th width="15%"  height="20"><b><font face="Verdana" size="1" color="#996600">&nbsp;</font><font face="Verdana" size="1" color="#996633">Bank&nbsp; 
              </font><font face="Verdana" size="1" color="#996600"> </font> </b></th>
            <th  height="20" > <div id="bank2"> 
                <select name="account_recur.lbn_id"
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                  <option value="${cmd.account_recur.lbn_id}">${cmd.account_recur.lbn_nama}</option>
                </select>
                <font color="#CC3300">*</font></div></th>
          </tr>
          <tr  > 
            <th width="2%" >&nbsp;</th>
            <th colspan="2"  ><b><font face="Verdana" size="1" color="#996600">Tanggal 
              Debet <br>
              (DD/MM/YYYY) </font></b></th>
            <th width="24%"> <spring:bind path="cmd.pemegang.mste_tgl_recur"> 
              <c:choose> <c:when test="${cmd.datausulan.mste_flag_cc eq '1' or cmd.datausulan.mste_flag_cc  eq '2'  or cmd.datausulan.flag_account eq '3'}"> 
              <script>inputDate('${status.expression}', '${status.value}', false);</script>
              </c:when> <c:otherwise> 
              <script>inputDate('${status.expression}', '${status.value}', true);</script>
              </c:otherwise> </c:choose> </spring:bind> </th>
            <th width="15%"><b> <font face="Verdana" size="1" color="#996633">Tanggal 
              Valid<br>
              (DD/MM/YYYY) </font></b></th>
            <th > <spring:bind path="cmd.account_recur.mar_expired"> <c:choose> 
              <c:when test="${cmd.datausulan.mste_flag_cc eq '1' or cmd.datausulan.mste_flag_cc  eq '2'  or cmd.datausulan.flag_account eq '3'}"> 
              <script>inputDate('${status.expression}', '${status.value}', false);</script>
              </c:when> <c:otherwise> 
              <script>inputDate('${status.expression}', '${status.value}', true);</script>
              </c:otherwise> </c:choose> </spring:bind><font color="#CC3300">*</font> </th>
          </tr>
            <th class="subtitle2" height="20" width="2%"><b><font face="Verdana" size="1" color="#FFFFFF">&nbsp;&nbsp;&nbsp;d.</font></b></th>
            <th class="subtitle2" colspan="5" height="20"> <b> <font face="Verdana" size="1" color="#FFFFFF">Dana Investasi dialokasikan : <font color="#CC3300">*</font></font></b> 
            </th>
          </tr><tr>
          <th  height="20" width="2%"><b><font face="Verdana" size="1" color="#FFFFFF">&nbsp;&nbsp;&nbsp;</font></b></th>
            <th colspan="5" height="20">
            <b><font face="Verdana" size="1" color="#996600">
            <spring:bind path="cmd.datausulan.mste_flag_investasi">
					<label for="langsung"> <input type="radio" class=noBorder
						name="${status.expression}" value="0" onClick="sts('0');"
						<c:if test="${cmd.datausulan.mste_flag_investasi eq 0 or cmd.datausulan.mste_flag_investasi eq null}"> 
									checked</c:if>
						tabindex="11" id="langsung">Langsung setelah dana diterima di rekening yang ditentukan oleh PT. Asuransi Jiwa Sinarmas MSIG dan SPAJ telah diterima di bagian Underwriting. </label>
					<br>
					<label for="tidaklangsung"> <input type="radio" class=noBorder
						name="${status.expression}" value="1" onClick="sts('1');"
						<c:if test="${cmd.datausulan.mste_flag_investasi eq 1}"> 
									checked</c:if>
						tabindex="12" id="tidaklangsung">Setelah permintaan asuransi disetujui oleh Bagian Underwriting dan Calon Pemegang Polis telah setuju serta membayar ekstra premi (jika ada).</label> </font></b> 
				</spring:bind>
             </th>
          </tr>   
		            <tr  > 
            <th class="subtitle2" height="20" width="2%"><b><font face="Verdana" size="1" color="#FFFFFF">&nbsp;&nbsp;&nbsp;e.</font></b></th>
         
		              <th class="subtitle2" colspan="5" height="20"> <b> <font face="Verdana" size="1" color="#FFFFFF">Jenis 
              Dana Investasi ( Isi % jenis Dana Investasi dengan kelipatan 20% atau 10 % tergantung produknya, 
              sehingga total 100% ) <spring:bind path="cmd.investasiutama.jmlh_invest"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30">
              </spring:bind> </font></b> </th>
          </tr>    
          <tr  > 
            <th class="subtitle2" width="2%" rowspan="2" ><b> <font face="Verdana" size="1" color="#ffffff">No.</font></b></th>
            <th class="subtitle2"  rowspan="2"  colspan="3"><b> <font face="Verdana" size="1" color="#ffffff">Type 
              of Fund</font></b></th>
            <th class="subtitle2"  colspan="2" > <p align="center"><b> <font face="Verdana" size="1" color="#ffffff">Fund 
                Allocation</font></b> </th>
          </tr>
          <tr  > 
            <th class="subtitle2"  width="15%" > <p align="left"><b> <font face="Verdana" size="1" color="#ffffff">Persen 
                (%) </font></b> </th>
            <th class="subtitle2"  height="12"  > <p align="left"><b> <font size="1" face="Verdana" color="#ffffff">Jumlah</font></b> 
            </th>
          </tr>
          
		<c:forEach items="${cmd.investasiutama.daftarinvestasi}" var="inv" varStatus="status"> 
			<tr> 
				<th width="2%" align="right">
					<p align="right"><b><font face="Verdana" size="1">${status.count}</font></b>
				</th>
				<th align="right" colspan="3">
					<div align="left">
						<spring:bind path="cmd.investasiutama.daftarinvestasi[${status.index}].lji_invest1"> 
	                		<input type="text" name="${status.expression}" value="${status.value }"  size="50" style='background-color :#D4D4D4' readOnly>
	                	</spring:bind>
	                </div>
               </th>
				<th width="15%" align="right">
					<div align="left">
						<spring:bind path="cmd.investasiutama.daftarinvestasi[${status.index}].mdu_persen1"> 
							<input type="text" name="${status.expression}" value="${status.value }"  size="10" maxlength="3" tabindex="13"
								<c:if test="${ not empty status.errorMessage}"> style='background-color :#FFE1FD' </c:if>>
						</spring:bind>
						<font color="#CC3300">*</font>
					</div>
				</th>
				<th align="right">
					<div align="left">
						<spring:bind path="cmd.investasiutama.daftarinvestasi[${status.index}].mdu_jumlah1">
                			<input type="text" name="${status.expression}" value="${status.value }"  size="17" style='background-color :#D4D4D4' readOnly 
								<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
						</spring:bind>
						<font color="#CC3300">*</font>
					</div>
				</th>
			</tr>
		</c:forEach> 
			
          <tr  > 
            <th width="2%" align="right">&nbsp;</th>
            <th colspan="3" align="right"><b><font face="Verdana" size="1" color="#996600">Total 
              : </font></b></th>
            <th width="15%" align="right" > <div align="left"><spring:bind path="cmd.investasiutama.total_persen"> 
                <input type="text" name="${status.expression}"
						value="${status.value }"  size="10" style='background-color :#D4D4D4' readOnly
				>
                 </spring:bind> 
              </div></th>
            <th width="2%" align="right">&nbsp;</th>
          </tr>
        </table>
      
          <table border="0" width="100%" cellspacing="1" cellpadding="1"  ID="tablebiaya" class="entry">
            <tr  > 
              
            <th width="5%" class="subtitle2" ><b><font face="Verdana" size="1" color="#ffffff">No.</font><font face="Verdana" size="1" ></font></b></th>
              <th width="35%" class="subtitle2" ><b> <font face="Verdana" size="1"  color="#ffffff">Jenis 
                Biaya</font></b></th>
              <th width="24%" class="subtitle2" ><b> <font face="Verdana" size="1" color="#ffffff">Jumlah</font></b></th>
              <th width="36%" class="subtitle2" ><b> <font face="Verdana" size="1" color="#ffffff">Persen(%)</font></b></th>
            </tr>
			 <c:forEach items="${cmd.investasiutama.daftarbiaya}" var="biaya" varStatus="status"> 
			             <tr  > 
              <th width="5%" ><b><font face="Verdana" size="1" >${status.count}</font></b></th>
              <th width="35%" ><div align="left"><spring:bind path="cmd.investasiutama.daftarbiaya[${status.index}].ljb_biaya"> 
                <input type="text" name="${status.expression}"
						value="${status.value }"  size="50" style='background-color :#D4D4D4'readOnly>
                </spring:bind> </div></th>
              <th width="24%" ><div align="left"><spring:bind path="cmd.investasiutama.daftarbiaya[${status.index}].mbu_jumlah"> 
                <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" style='background-color :#D4D4D4'readOnly>
                </spring:bind> </div></th>
              <th width="36%" ><div align="left"><spring:bind path="cmd.investasiutama.daftarbiaya[${status.index}].mbu_persen"> 
                <input type="text" name="${status.expression}"
						value="${status.value }"  size="10" style='background-color :#D4D4D4'readOnly>
                </spring:bind> </div></th>
            </tr>
			</c:forEach>
          </table>
        
        <table border="0" width="100%" cellspacing="1" cellpadding="1" class="entry">
          <tr> 
            <th>&nbsp;</th>
          </tr>
        </table>
        <table border="0" width="100%" cellspacing="1" cellpadding="1" height="24" class="entry" >
          <tr> 
            <th> 
              <p align="left"><b> <font face="Verdana" size="1" color="#CC3300">Detail 
                Investasi Khusus Produk Power Save</font></b> 
            </th>
          </tr>
        </table>
        <table border="0" width="100%" cellspacing="1" cellpadding="1" height="38" class="entry">
          <tr  > 
            <th width="34" class="subtitle2"><b><font face="Verdana" size="1" color="#FFFFFF">6. 
              </font></b></th>
            <th width="1475" class="subtitle2"><b> <font face="Verdana" size="1" color="#FFFFFF">Jangka 
              Waktu </font></b></th>
            <th width="1073" class="subtitle2"><b> <font face="Verdana" size="1" color="#FFFFFF">Bunga 
              (%)</font></b></th>
            <th width="201" class="subtitle2"><b> <font face="Verdana" size="1" color="#FFFFFF">Tanggal 
              Jatuh Tempo<br>
              (DD/MM/YYYY)</font></b></th>
            <th width="185" class="subtitle2"><b> <font face="Verdana" size="1" color="#FFFFFF">Jumlah 
              Investasi</font></b></th>
            <th width="159" class="subtitle2"><b> <font face="Verdana" size="1" color="#FFFFFF">Jumlah 
              Bunga</font></b></th>
            <th  width="1276" class="subtitle2"><b> <font face="Verdana" size="1" color="#FFFFFF">Jenis 
              Roll Over </font></b></th>
          </tr>
          <tr  > 
            <th width="34" height="22">&nbsp;</th>
            <th width="1475" height="22"> <select name="powersave.mps_jangka_inv" tabindex="14"
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <c:forEach var="jk" items="${select_jangka_invest}"> <option 
							<c:if test="${cmd.powersave.mps_jangka_inv eq jk.ID}"> SELECTED </c:if>
							value="${jk.ID}">${jk.JANGKAWAKTU}</option> </c:forEach> 
              </select>
              <font color="#CC3300">*</font> </th>
            <th width="1073" height="22"><spring:bind path="cmd.powersave.mps_rate"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="10" maxlength="7"
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
              </spring:bind> 
            </th>
            <th width="201" height="22"> <spring:bind path="cmd.powersave.mps_batas_date"> 
              <script>inputDate('${status.expression}', '${status.value}', true);</script>
              </spring:bind> </th>
            <th width="185" height="22"><spring:bind path="cmd.powersave.mps_prm_deposit"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="15" style='background-color :#D4D4D4'readOnly>
               </spring:bind></th>
            <th width="159" height="22"><spring:bind path="cmd.powersave.mps_prm_interest"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="15" style='background-color :#D4D4D4'readOnly>
              </spring:bind></th>
            <th height="22" width="1276"> <select name="powersave.mps_roll_over" tabindex="15" onchange="tutup();"
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <c:forEach var="roll" items="${select_rollover}"> <option 
							<c:if test="${cmd.powersave.mps_roll_over eq roll.ID}"> SELECTED </c:if>
							value="${roll.ID}">${roll.ROLLOVER}</option> </c:forEach> 
              </select>
              <font color="#CC3300">*</font> </th>
          </tr>
          <tr > 
            <th height="22" class="subtitle2">&nbsp;</th>
            <th height="22" class="subtitle2"><b><font face="Verdana" size="1" color="#FFFFFF">Jenis 
              Bunga </font></b></th>
            <th height="22" colspan="2" class="subtitle2"><b><font face="Verdana" size="1" color="#FFFFFF">No 
              Memo </font></b></th>
           
            <th height="22" class="subtitle2"><b></b></th>
            <th height="22" class="subtitle2">&nbsp;</th>
            <th height="22" class="subtitle2"><b><font face="Verdana" size="1" color="#FFFFFF">Status 
              Karyawan </font></b></th>
          </tr>
          <tr  >
            <th height="22">&nbsp;</th>
            <th height="22">			<select name="powersave.mps_jenis_plan" tabindex="14" onChange="jns_rate();"
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <c:forEach var="bg" items="${select_jenisbunga}"> <option 
							<c:if test="${cmd.powersave.mps_jenis_plan eq bg.ID}"> SELECTED </c:if>
							value="${bg.ID}">${bg.JENIS}</option> </c:forEach> 
            	  </select> <font color="#CC3300">*</font></th>
            <th height="22" colspan="2">
			<spring:bind path="cmd.powersave.mpr_note"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" maxlength="100"
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
              </spring:bind> <spring:bind path="cmd.powersave.mpr_nett_tax"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30" maxlength="100">
             </spring:bind><font color="#CC3300">*</font></th>
			   <th height="22">
	
			</th>

            <th height="22">&nbsp;</th>
            <th height="22">
			<select name="powersave.mps_employee" tabindex="14" 
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <c:forEach var="kr" items="${select_karyawan}"> <option 
							<c:if test="${cmd.powersave.mps_employee eq kr.ID}"> SELECTED </c:if>
							value="${kr.ID}">${kr.KRY}</option> </c:forEach> 
            	  </select>
              <font color="#CC3300">*</font> </th>
          </tr>
<tr  >
            <th height="22"></th>
            <th height="22">Tanggal NAB :<br>
            <spring:bind path="cmd.powersave.msl_tgl_nab"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" maxlength="100" readOnly >
             </spring:bind></th>
            <th height="22" colspan="2">Nilai NAB : <br>
            <spring:bind path="cmd.powersave.msl_nab"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" maxlength="50" readOnly>
             </spring:bind>	</th>
              <th height="22" colspan="2">Begdate TOPUP<br>
              <spring:bind path="cmd.powersave.begdate_topup">  
              <script>inputDate('${status.expression}', '${status.value}', false,'');</script>
              <font color="#CC3300">*</font>  </spring:bind></th>

		            <th height="22" >Persentase Rate :<br>
				<spring:bind path="cmd.powersave.msl_bp_rate"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="10" maxlength="30" >%
             </spring:bind><font color="#CC3300"> *
			 </th>
          </tr>      
<tr  >
            <th height="22"></th>
            <th height="22">Penarikan Bunga<br>
             <select name="powersave.tarik_bunga" tabindex="14" >
                    <option <c:if test="${cmd.powersave.tarik_bunga eq 0}"> SELECTED </c:if>	value="0">0</option> 
           	        <option <c:if test="${cmd.powersave.tarik_bunga eq 1}"> SELECTED </c:if>	value="1">1</option> 
           	        <option <c:if test="${cmd.powersave.tarik_bunga eq 3}"> SELECTED </c:if>	value="3">3</option> 
           	 </select> bulan
            </th>
            <th height="22" colspan="2"></th>
            <th height="22" colspan="2"></th>
            <th height="22" > </th>
          </tr>               
                    
        </table>
        <table border="0" width="100%" cellspacing="1" cellpadding="1" height="24" class="entry">
          <tr> 
            <th width="46%"> 
              <p align="left"><b> <font face="Verdana" size="1" color="#CC3300">Detail 
                Khusus Bank Sinarmas</font></b> 
            </th>
            <th width="54%"><b><font face="Verdana" size="1" color="#CC3300">Detail 
              Bonus Tahapan khusus Produk Simponi 8 dan Premium Saver </font></b></th>
          </tr>
        </table>
        <table border="0" width="100%" cellspacing="1" cellpadding="1" height="38" class="entry">
          <tr  > 
            <th width="34"  class="subtitle2">&nbsp;</th>
            <th width="528" class="subtitle2"><b><font face="Verdana" size="1" color="#FFFFFF"> 
              % Bunga 
		  
			  </font></b></th>
            <th width="666" class="subtitle2"><b><font face="Verdana" size="1" color="#FFFFFF">% 
              Bonus Tahapan 
              </font></b></th>
          </tr>
          <tr  > 
            <th width="34">&nbsp;</th>
            <th height="22" width="528"> 
              <spring:bind path="cmd.pemegang.mspo_under_table"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" maxlength="5"  tabindex="16">
              </spring:bind> <spring:bind path="cmd.pemegang.tgl_mspo_under_table"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="15" maxlength="5"  tabindex="16">
               </spring:bind><font color="#CC3300">*</font> </th>
            <th height="22" width="666"> 
               <spring:bind path="cmd.pemegang.bonus_tahapan"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" maxlength="5" tabindex="17">
              </spring:bind> 
            </th>
          </tr>
        </table>

		<br>				
        <table border="0" width="100%" cellspacing="1" cellpadding="1" height="27"  class="entry">
          <tr> 
            <th class="subtitle"  width="101%"  height="27"> 
              <p align="center"><b> <font color="#FFFFFF" size="2" face="Verdana">D. 
                DATA YANG DITUNJUK MENERIMA MANFAAT ASURANSI </font></b> 
            </th>
          </tr>
          <tr> 
            <th  class="subtitle" align="center"  > 
             <input name="btnadd1" type="button" id="btnadd1" value="ADD" " tabindex="15"  onClick="addRowDOM1('tableProd', '1')"> &nbsp;&nbsp;&nbsp;&nbsp; 
             <input name="btn_cancel1" type="button" id="btn_cancel1" value="DELETE" " tabindex="16"  onClick="cancel1()">			
              <input type="hidden" name="jmlpenerima"  value="0" >
            </th>
          </tr>
        </table>
          <table ID="tableProd" width="100%" border="0" cellspacing="1" cellpadding="1" class="entry">
            <tr > 
              <th  class="subtitle2" > <font size="1"><b><font face="Verdana" color="#FFFFFF">No</font></b></font></th>
              <th class="subtitle2" > <font size="1"><b><font face="Verdana" color="#FFFFFF">Nama 
                Lengkap 
				<br>(Sesuai KTP/Akte Lahir)
                </font></b></font><font color="#CC3300">*</font></th>
              <th class="subtitle2" > <font size="1"><b><font face="Verdana" color="#FFFFFF">Tanggal 
                Lahir </font><font color="#CC3300">*</font></b></font><font size="1"><b><font face="Verdana" color="#FFFFFF">
               </font></b></font></th>
              <th class="subtitle2" > <font size="1"><b><font face="Verdana" color="#FFFFFF">Hubungan 
                Dengan Calon Tertanggung </font></b></font><font color="#CC3300">*</font></th>
              
            <th class="subtitle2" > <font size="1"><b><font face="Verdana" color="#FFFFFF">Manfaat 
              (%) </font></b></font><font color="#CC3300">*</font></th>
                <th class="subtitle2"  > <font size="1"><b><font face="Verdana" color="#FFFFFF">&nbsp; </font></b></font></th>
  
            </tr>
         <c:forEach items="${cmd.datausulan.daftabenef}" var="benef" varStatus="status"> 
            <tr > 
              <th > ${status.count}</th>
              <th >
			  <input type="text" name='benef.msaw_first${status.index +1}' value ='${benef.msaw_first}' size="30" maxlength="60">
              </th>
              <th > <input type="text" name='tgllhr${status.index +1}' value='<fmt:formatDate value="${benef.msaw_birth}" pattern="dd"/>'  size="3" maxlength="2">
			/<input type="text" name='blnhr${status.index +1}' value='<fmt:formatDate value="${benef.msaw_birth}" pattern="MM"/>'  size="3" maxlength="2">
		/<input type="text" name='thnhr${status.index +1}' value='<fmt:formatDate value="${benef.msaw_birth}" pattern="yyyy"/>'  size="5" maxlength="4">
		<input type="hidden" name='msaw_birth${status.index +1}' value='<fmt:formatDate value="${benef.msaw_birth}" pattern="dd/MM/yyyy"/>'  size="5" readOnly>
              </th>
              <th > 
			  <select name="benef.lsre_id${status.index +1}">
						<c:forEach var="benefrl" items="${select_hubungan}">
							<option 
							<c:if test="${benef.lsre_id eq benefrl.ID}"> SELECTED </c:if>
							value="${benefrl.ID}">${benefrl.RELATION}</option>
						</c:forEach>
					</select>
               </th>
              <th>
			  <input type="text" name='benef.msaw_persen${status.index +1}' value ='${benef.msaw_persen}' size="5" maxlength="5">
               </th>
                <th > <input type=checkbox name="cek${status.index +1}" id= "ck${status.index +1}" class="noBorder" ></th>
  
            </tr>
		</c:forEach>		 
		 
		  </table>
        
        <table border="0" width="100%" cellspacing="1" cellpadding="1" class="entry2">
          <tr> 
            <td colspan="9"> 
			 <p align="left"><b> <font face="Verdana" size="2" color="#CC3300">Note : Untuk pecahan decimal menggunakan titik.
			 <br>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; * Wajib diisi</font></b></p>
			</td>
          </tr>		
          <tr> 
            <td colspan="9"> <input type="hidden" name="hal" value="${halaman}">	
			<spring:bind path="cmd.pemegang.indeks_halaman"> 
              <input type="hidden" name="${status.expression}"
						value="${halaman-1}"  size="30" >
              </spring:bind>
			
			</td>
          </tr>
          <tr> 
            <td  align="center" colspan="4"> 
              <div align="center">
                <input type="submit" name="_target${halaman-1}" value="Prev &laquo;" onClick="prev()" tabindex="19"
				accesskey="p" onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
                <input type="submit" name="_target${halaman+1}" value="Next &raquo;" onClick="next()" tabindex="18"
				accesskey="N" onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
                <input type="hidden" name="_page" value="${halaman}">
              </div>
            </td>
          </tr>
        </table>
      <p>
    </td>
  </tr>
</table>
 </form>
</body>
<%@ include file="/include/page/footer.jsp"%>