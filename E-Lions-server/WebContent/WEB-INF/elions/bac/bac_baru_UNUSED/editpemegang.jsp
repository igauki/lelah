<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%><%@ page import="com.ekalife.elions.model.User"%><%User currentUser = (User) session.getAttribute("currentUser");%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="Description" content="EkaLife">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="-1">
<link rel="Stylesheet" type="text/css" href="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">

<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>

<script type="text/javascript">
<!--
	function kyc(kombo, teks, lain){
		with(document.frmParam){
			var ket = elements[kombo].options[elements[kombo].selectedIndex].text;
			if (ket.toUpperCase() != lain) {
				elements[teks].value='';
			}else if ((elements[teks].value).toUpperCase()==lain) {
				elements[teks].value='';
			}
		}
	}
	//END OF kyc()

	function togel(elemen, truefalse, bgkolor, valu){
		with(document.frmParam) {
			elements[elemen].value = valu;
			elements[elemen].readOnly = truefalse;
			elements[elemen].style.backgroundColor = bgkolor;
		}
	}
	//END OF togel()

	function Body_onload() {
		with(document.frmParam){
			if (elements['datausulan.specta_save'].value == '1') {
				specta_save1.checked = true;
			}else{
				specta_save1.checked = false;
			}
		
			elements['pemegang.lus_id'].value=<%=currentUser.getLus_id()%>;
			elements['pemegang.cbg_lus_id'].value=<%=currentUser.getLca_id()%>;
			elements['pemegang.mste_age'].style.backgroundColor ='#D4D4D4';
			elements['pemegang.mste_age'].readOnly = true;
			
			kyc('pemegang.mkl_tujuan', 'pemegang.tujuana', 'LAIN - LAIN');
			kyc('pemegang.mkl_pendanaan', 'pemegang.danaa', 'LAINNYA');
			kyc('pemegang.mkl_smbr_penghasilan', 'pemegang.danaa2', 'LAINNYA');
			kyc('pemegang.mkl_kerja', 'pemegang.kerjaa', 'LAINNYA');
			kyc('pemegang.mkl_kerja', 'pemegang.kerjab', 'KARYAWAN');
			kyc('pemegang.mkl_industri', 'pemegang.industria', 'LAINNYA');
			
			elements['addressbilling.tagih'].value='1';
		}
		addressbilling_onchange(true);
	}
	//END OF Body_onload()

	function addressbilling_onchange(isOnLoad) {
		with(document.frmParam) {
			//Lain-Lain
			 if (elements['addressbilling.tagih'].value == "1") {
			 	if(!isOnLoad){
				 	togel('addressbilling.msap_address', false, '#FFFFFF', '');
				 	togel('addressbilling.msap_zip_code', false, '#FFFFFF', '');
				 	togel('addressbilling.kota_tgh', false, '#FFFFFF', '');
				 	togel('addressbilling.msap_area_code1', false, '#FFFFFF', '');
				 	togel('addressbilling.msap_phone1', false, '#FFFFFF', '');
				 	togel('addressbilling.msap_area_code2', false, '#FFFFFF', '');
				 	togel('addressbilling.msap_phone2', false, '#FFFFFF', '');
				 	togel('addressbilling.msap_area_code3', false, '#FFFFFF', '');
				 	togel('addressbilling.msap_phone3', false, '#FFFFFF', '');
				 	togel('addressbilling.no_hp', false, '#FFFFFF', '');
				 	togel('addressbilling.no_hp2', false, '#FFFFFF', '');
				 	togel('addressbilling.msap_area_code_fax1', false, '#FFFFFF', '');
				 	togel('addressbilling.msap_fax1', false, '#FFFFFF', '');
				 	togel('addressbilling.e_mail', false, '#FFFFFF', '');
				 }
			//Rumah
			}else if (elements['addressbilling.tagih'].value=="2") {
			 	togel('addressbilling.msap_address', true, '#D4D4D4', elements['pemegang.alamat_rumah'].value);
			 	togel('addressbilling.msap_zip_code', true, '#D4D4D4', elements['pemegang.kd_pos_rumah'].value);
			 	togel('addressbilling.kota_tgh', true, '#D4D4D4', elements['pemegang.kota_rumah'].value);
			 	togel('addressbilling.msap_area_code1', true, '#D4D4D4', elements['pemegang.area_code_rumah'].value);
			 	togel('addressbilling.msap_phone1', true, '#D4D4D4', elements['pemegang.telpon_rumah'].value);
			 	togel('addressbilling.msap_area_code2', true, '#D4D4D4', elements['pemegang.area_code_rumah2'].value);
			 	togel('addressbilling.msap_phone2', true, '#D4D4D4', elements['pemegang.telpon_rumah2'].value);
			 	togel('addressbilling.msap_area_code3', true, '#D4D4D4', '');
			 	togel('addressbilling.msap_phone3', true, '#D4D4D4', '');
			 	togel('addressbilling.no_hp', true, '#D4D4D4', elements['pemegang.no_hp'].value);
			 	togel('addressbilling.no_hp2', true, '#D4D4D4', elements['pemegang.no_hp2'].value);
			 	togel('addressbilling.msap_area_code_fax1', true, '#D4D4D4', '');
			 	togel('addressbilling.msap_fax1', true, '#D4D4D4', '');
			 	togel('addressbilling.e_mail', true, '#D4D4D4', elements['pemegang.email'].value);
			//Kantor
			}else if (elements['addressbilling.tagih'].value=="3") {
			 	togel('addressbilling.msap_address', true, '#D4D4D4', elements['pemegang.alamat_kantor'].value);
			 	togel('addressbilling.msap_zip_code', true, '#D4D4D4', elements['pemegang.kd_pos_kantor'].value);
			 	togel('addressbilling.kota_tgh', true, '#D4D4D4', elements['pemegang.kota_kantor'].value);
			 	togel('addressbilling.msap_area_code1', true, '#D4D4D4', elements['pemegang.area_code_kantor'].value);
			 	togel('addressbilling.msap_phone1', true, '#D4D4D4', elements['pemegang.telpon_kantor'].value);
			 	togel('addressbilling.msap_area_code2', true, '#D4D4D4', elements['pemegang.area_code_kantor'].value);
			 	togel('addressbilling.msap_phone2', true, '#D4D4D4', elements['pemegang.telpon_kantor'].value);
			 	togel('addressbilling.msap_area_code3', true, '#D4D4D4', '');
			 	togel('addressbilling.msap_phone3', true, '#D4D4D4', '');
			 	togel('addressbilling.no_hp', true, '#D4D4D4', '');
			 	togel('addressbilling.no_hp2', true, '#D4D4D4', '');
			 	togel('addressbilling.msap_area_code_fax1', true, '#D4D4D4', elements['pemegang.area_code_fax'].value);
			 	togel('addressbilling.msap_fax1', true, '#D4D4D4', elements['pemegang.no_fax'].value);
			 	togel('addressbilling.e_mail', true, '#D4D4D4', '');
			}
		}
	}	
	//END OF addressbilling_onchange()

	function next(tarrrget) {
		createLoadingMessage();
		if(!tarrrget) document.frmParam.elements['pemegang.indeks_halaman'].value = (document.frmParam.elements['hal'].value) + 1;
		specta_save_onClick();
	}
	//END OF next()
	
	function isi() {	
		with(document.frmParam) {
			elements['pemegang.mcl_first'].value = 'PT. GUTHRIE  PECCONINA  INDONESIA';
			elements['pemegang.mcl_gelar'].value='';
			elements['pemegang.mspe_mother'].value='-';
			elements['pemegang.lside_id'].value='0';
			elements['pemegang.mspe_no_identity'].value='-';
			elements['pemegang.lsne_id'].value='1';
			elements['pemegang.mspe_date_birth'].value='13/10/1995';
			elements['pemegang.mste_age'].value='12';
			elements['pemegang.mspe_place_birth'].value='PALEMBANG';
			elements['pemegang.mspe_sex'].value='1';
			elements['pemegang.mspe_sex'][0].checked=true;
			elements['pemegang.mspe_sts_mrt'].value='1';
			elements['pemegang.lsag_id'].value='1';	
			elements['pemegang.lsed_id'].value='4';	
			elements['pemegang.alamat_rumah'].value='KOMP. ILIR BARAT PERMAI BLOK D-1 NO.23-24, 24 ILIR';	
			elements['pemegang.kd_pos_rumah'].value='';	
			elements['pemegang.kota_rumah'].value='PALEMBANG';	
			elements['pemegang.area_code_rumah'].value='-';	
			elements['pemegang.telpon_rumah'].value='-';	
			elements['pemegang.area_code_rumah2'].value='';		
			elements['pemegang.telpon_rumah2'].value='';		
			elements['pemegang.no_hp'].value='';		
			elements['pemegang.email'].value='';		
			elements['pemegang.no_hp2'].value='';		
			elements['pemegang.alamat_kantor'].value='KOMP. ILIR BARAT PERMAI BLOK D-1 NO.23-24, 24 ILIR';		
			elements['pemegang.kd_pos_kantor'].value='';		
			elements['pemegang.kota_kantor'].value='PALEMBANG';		
			elements['pemegang.area_code_kantor'].value='-';		
			elements['pemegang.telpon_kantor'].value='-';		
			elements['pemegang.area_code_kantor2'].value='';		
			elements['pemegang.telpon_kantor2'].value='';		
			elements['pemegang.area_code_fax'].value='';		
			elements['pemegang.no_fax'].value='';		
			elements['addressbilling.tagih'].value='2';		
			elements['addressbilling.msap_address'].value='KOMP. ILIR BARAT PERMAI BLOK D-1 NO.23-24, 24 ILIR';		
			elements['addressbilling.msap_zip_code'].value='';		
			elements['addressbilling.kota_tgh'].value='PALEMBANG';		
			elements['addressbilling.msap_area_code1'].value='-';		
			elements['addressbilling.msap_phone1'].value='-';		
			elements['addressbilling.msap_area_code2'].value='';		
			elements['addressbilling.msap_phone2'].value='';		
			elements['addressbilling.msap_area_code3'].value='';		
			elements['addressbilling.msap_phone3'].value='';		
			elements['addressbilling.no_hp'].value='';		
			elements['addressbilling.msap_area_code_fax1'].value='';		
			elements['addressbilling.msap_fax1'].value='';		
			elements['addressbilling.no_hp2'].value='';		
			elements['addressbilling.e_mail'].value='';		
			elements['pemegang.mkl_tujuan'].value='LAIN - LAIN';		
			elements['pemegang.tujuana'].value='';		
			elements['pemegang.mkl_penghasilan'].value='<= RP. 10 JUTA';		
			elements['pemegang.mkl_pendanaan'].value='LAINNYA';		
			elements['pemegang.mkl_kerja'].value='LAINNYA';		
			elements['pemegang.kerjaa'].value='';		
			elements['pemegang.kerjab'].value='';		
			elements['pemegang.mkl_industri'].value='LAINNYA';		
			elements['pemegang.industria'].value='';		
			elements['pemegang.lsre_id'].value='6';		
		}
	}
	//END OF isi()

	function specta_save_onClick(){
		with(document.frmParam) {
			if(specta_save1.checked==true){
				elements['datausulan.specta_save'].value = '1';
			}else{
				elements['datausulan.specta_save'].value = '0';
			}
		}
	}
	//END OF specta_save_onClick()
// -->
</script>
</head>

<body onload="Body_onload(); setupPanes('container1', 'tab1'); document.frmParam.elements['datausulan.mste_medical'].focus();">
<XML ID=xmlData></XML>
<form:form commandName="cmd" name="frmParam" id="frmParam" method="post">

	<!-- Hidden Fields -->
		<c:choose>
			<c:when test="${cmd.pemegang.reg_spaj eq null}"><c:set var="disableFlag" value="disabled"/></c:when>
			<c:otherwise><c:set var="disableFlag" value=""/></c:otherwise>
		</c:choose>
		
		<input type="submit" id="_target0" name="_target0" onclick="next('_target0')" style="display: none;" ${disableFlag}>
		<input type="submit" id="_target1" name="_target1" onclick="next('_target1')" style="display: none;" ${disableFlag}>
		<input type="submit" id="_target2" name="_target2" onclick="next('_target2')" style="display: none;" ${disableFlag}>
		<input type="submit" id="_target3" name="_target3" onclick="next('_target3')" style="display: none;" ${disableFlag}>
		<input type="submit" id="_target4" name="_target4" onclick="next('_target4')" style="display: none;" ${disableFlag}>
		<input type="submit" id="_target5" name="_target5" onclick="next('_target5')" style="display: none;" ${disableFlag}>
	
		<form:hidden path="datausulan.specta_save"/>
		<form:hidden path="datausulan.flag_worksite"/>
		<form:hidden path="pemegang.cab_bank"/>
		<form:hidden path="pemegang.jn_bank"/>
		<form:hidden path="pemegang.lus_id"/>
		<form:hidden path="pemegang.cbg_lus_id"/>
		
		<textarea cols="40" rows="7" name="alamat_sementara" style="display:none;">${cmd.addressbilling.msap_address}</textarea>
		<textarea cols="40" rows="7" name="alamat_sementara1" style="display:none;">${cmd.pemegang.alamat_rumah}</textarea>
		<textarea cols="40" rows="7" name="alamat_sementara2" style="display:none;">${cmd.pemegang.alamat_kantor}</textarea>
		
		<input type="hidden" name="_page" value="${halaman}">		
		<input type="hidden" name="hal" value="${halaman}">
		 <spring:bind path="cmd.pemegang.indeks_halaman">
			<input type="hidden" name="${status.expression}" value="${halaman-1}">
		 </spring:bind>
	<!-- End of Hidden Fields -->

	<div id="contents">
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li><a href="#" onclick="return showPane('pane1', this);" id="tab1" class="tab-active" >Pemegang</a></li>
				<li><a href="#" onclick="document.getElementById('_target1').click(); return false;">Tertanggung</a></li>
				<li><a href="#" onclick="document.getElementById('_target2').click(); return false;">Asuransi</a></li>
				<li><a href="#" onclick="document.getElementById('_target3').click(); return false;">Investasi</a></li>
				<li><a href="#" onclick="document.getElementById('_target4').click(); return false;">Agen</a></li>
				<li><a href="#" onclick="document.getElementById('_target5').click(); return false;">Konfirmasi</a></li>
				<li><a href="#" onclick="return false;">Finish</a></li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<table class="entry2">
						<spring:bind path="cmd.*">
							<c:if test="${not empty status.errorMessages}">
								<tr>
									<td colspan="7">
										<div id="error">ERROR:<br>
										<c:forEach var="error" items="${status.errorMessages}">
											- <c:out value="${error}" escapeXml="false" />
											<br />
										</c:forEach>
										</div>
									</td>
								</tr>
							</c:if>
						</spring:bind>	
						<tr>
							<th colspan="7">
								<input type="submit" name="_target${halaman+1}" value="Next &raquo;" onclick="next()" accesskey="N" title="Alt-N">
								<input type="button"  value="Isi Data PP Guthrie" onclick="isi();" title="Alt-N">
								<label for="specta_save1">
									<input type="checkbox" id="specta_save1" name="specta_save1" class="noBorder" value="${cmd.datausulan.specta_save}" onclick="specta_save_onClick();"> 
									SPECTA SAVE							
								</label>
							</th>
						</tr>
						<tr>
							<th colspan="7" style="background-color: #D0D0D0">A. Data Calon Pemegang Polis</th>
						</tr>
						<tr>
							<th class="left"></th>
							<th>Nomor Registrasi (SPAJ)</th>
							<td class="nowrap">
								<input type="reg_spaj" value="<elions:spaj nomor='${cmd.pemegang.reg_spaj}'/>" class="readOnly" readonly="true">
								<c:if test="${cmd.datausulan.flag_worksite_ekalife eq 1}"><span class="info">Karyawan AJ Sinarmas</span></c:if>
							</td>
							<th>SPAJ</th>
							<td class="nowrap">
								<form:select path="pemegang.mste_spaj_asli" cssStyle="width: 130px;">
									<form:option value="1" label="ASLI"/>
									<form:option value="0" label="FOTOKOPI/FAX"/>
								</form:select>
							</td>
							<th>Nomor Seri</th>
							<td class="nowrap">
								<form:input path="pemegang.mspo_no_blanko" size="24" maxlength="10" cssErrorClass="inpError"/>
								<font class="error">*</font>
							</td>
						</tr>
						<tr>
							<th class="left"></th>
							<th>Medis</th>
							<td class="nowrap">
								<form:select path="datausulan.mste_medical" cssStyle="width: 110px;">
									<form:option value="0" label="NON MEDIS"/>
									<form:option value="1" label="MEDIS"/>
								</form:select>
								<font class="error">*</font>
							</td>
							<th>No CIF</th>
							<td class="nowrap">
								<form:input path="pemegang.mspo_nasabah_dcif" size="24" maxlength="100" cssErrorClass="inpError"/>
								<font class="error">*</font>
								<br/><span class="info">(Khusus Input Bank)</span>
							</td>
							<th>No PB</th>
							<td class="nowrap">
								<form:input path="pemegang.no_pb" size="24" maxlength="100" cssErrorClass="inpError"/>
								<font class="error">*</font>
								<br/><span class="info">(Khusus Input Bank)</span>
							</td>
						</tr>
						<tr>
							<th colspan="7" style="background-color: #D0D0D0">&nbsp;</th>
						</tr>
						<tr>
							<th class="left">1.</th>
							<th>
								Nama Lengkap<br>
								<span class="info">(sesuai Identitas, Tanpa Gelar)</span>
							</th>
							<td class="nowrap">
								<form:input path="pemegang.mcl_first" size="41" maxlength="100" cssErrorClass="inpError"/>
								<font class="error">*</font>
							</td>
							<th>Gelar</th>
							<td class="nowrap"><form:input path="pemegang.mcl_gelar" size="24" maxlength="15"/></td>
							<th colspan="2"/>
						</tr>
						<tr>
							<th class="left">2.</th>
							<th>Nama Ibu Kandung</th>
							<td class="nowrap">
								<form:input path="pemegang.mspe_mother" size="41" maxlength="50" cssErrorClass="inpError"/>
								<font class="error">*</font>
							</td>
							<th colspan="4"/>
						</tr>
						<tr>
							<th class="left">3.</th>
							<th>Identitas</th>
							<td class="nowrap">
								<form:select path="pemegang.lside_id" cssStyle="width: 160px;" items="${select_identitas}" itemValue="key" itemLabel="value" />
								<font class="error">*</font>
							</td>
							<th>No. Identitas</th>
							<td class="nowrap">
								<form:input path="pemegang.mspe_no_identity" size="24" maxlength="50" cssErrorClass="inpError"/>
								<font class="error">*</font>
							</td>
							<th colspan="2"/>
						</tr>
						<tr>
							<th class="left">4.</th>
							<th>Warga Negara</th>
							<td class="nowrap">
								<form:select path="pemegang.lsne_id" cssStyle="width: 160px;" items="${select_negara}" itemValue="key" itemLabel="value" />
								<font class="error">*</font>
							</td>
							<th colspan="4"/>
						</tr>
						<tr>
							<th class="left">5.</th>
							<th>Tgl Lahir</th>
							<td class="nowrap">
								<form:input path="pemegang.mspe_date_birth" maxlength="10" size="12" cssErrorClass="inpError"/>
								<font class="error">*</font> <font class="info">(dd/mm/yyyy)</font>
							</td>
							<th>Lahir Di</th>
							<td class="nowrap">
								<form:input path="pemegang.mspe_place_birth" size="24" maxlength="30" cssErrorClass="inpError"/>
								<font class="error">*</font>
							</td>
							<th>Usia</th>
							<td class="nowrap"><form:input path="pemegang.mste_age" size="4"/> Tahun</td><th></th>
						</tr>
						<tr>
							<th class="left">6.</th>
							<th>Jenis Kelamin</th>
							<td class="nowrap">
								<label for="cowok">
									<form:radiobutton path="pemegang.mspe_sex" id="cowok" value="1" cssClass="noBorder"/> Pria
								</label>
								<label for="cewek">
									<form:radiobutton path="pemegang.mspe_sex" id="cewek" value="0" cssClass="noBorder"/> Wanita
								</label>
								<font class="error">*</font>
							</td>
							<th>Status</th>
							<td class="nowrap">						
								<form:select path="pemegang.mspe_sts_mrt" cssStyle="width: 130px;">
									<form:option value="1" label="BELUM MENIKAH"/>
									<form:option value="2" label="MENIKAH"/>
									<form:option value="3" label="JANDA"/>
									<form:option value="4" label="DUDA"/>
								</form:select>
								<font class="error">*</font>
							</td>
							<th colspan="2"/>
						</tr>
						<tr>
							<th class="left">7.</th>
							<th>Agama</th>
							<td class="nowrap">							
								<form:select path="pemegang.lsag_id" cssStyle="width: 160px;" items="${select_agama}" itemValue="key" itemLabel="value" />
								<font class="error">*</font>
							</td>
							<th colspan="4"/>
						</tr>
						<tr>
							<th class="left">8.</th>
							<th>Pendidikan</th>
							<td class="nowrap">							
								<form:select path="pemegang.lsed_id" cssStyle="width: 160px;" items="${select_pendidikan}" itemValue="key" itemLabel="value" />
								<font class="error">*</font>
							</td>
							<th colspan="4"/>
						</tr>
						<tr>
							<th class="left" rowspan="4">9.a</th>
							<th rowspan="4">Alamat Rumah</th>
							<td class="nowrap" rowspan="4">
								<form:textarea path="pemegang.alamat_rumah" cols="40" rows="5" onkeyup="textCounter(this, 200);" onkeydown="textCounter(this, 200);" cssErrorClass="inpError"/>
								<font class="error">*</font>
							</td>
							<th>Kode Pos</th>
							<td class="nowrap">
								<form:input path="pemegang.kd_pos_rumah" size="24" maxlength="10" cssErrorClass="inpError"/>
								<font class="error">*</font>
							</td>
							<th>Kota</th>
							<td class="nowrap">
								<form:input path="pemegang.kota_rumah" id="pemegang.kota_rumah" size="24" onfocus="this.select();" cssErrorClass="inpError"/>
						    	<span id="indicator_rumah" style="display:none;"><img src="${path}/include/image/indicator.gif" alt=""/></span>
								<font class="error">*</font>
							</td>
						</tr>
						<tr>
							<th class="nowrap">Telp Rumah 1</th>
							<td class="nowrap">
								<form:input path="pemegang.area_code_rumah" size="5" maxlength="4" cssErrorClass="inpError"/>
								<form:input path="pemegang.telpon_rumah" size="16" maxlength="24" cssErrorClass="inpError"/>
								<font class="error">*</font>
							</td>
							<th>Telp Rumah 2</th>
							<td class="nowrap">
								<form:input path="pemegang.area_code_rumah2" size="5" maxlength="4" cssErrorClass="inpError"/>
								<form:input path="pemegang.telpon_rumah2" size="16" maxlength="24" cssErrorClass="inpError"/>
							</td>
						</tr>
						<tr>
							<th>Handphone 1</th>
							<td class="nowrap">
								<form:input path="pemegang.no_hp" size="24" maxlength="20" cssErrorClass="inpError"/>
								<font class="error">*</font>
							</td>
							<th>Handphone 2</th>
							<td class="nowrap">
								<form:input path="pemegang.no_hp2" size="24" maxlength="20" cssErrorClass="inpError"/>
							</td>
						</tr>
						<tr>
							<th>Email</th>
							<td class="nowrap" colspan="2">
								<form:input path="pemegang.email" size="42" maxlength="50" cssErrorClass="inpError"/>
							</td>
							<th colspan="1"/>
						</tr>
						<tr>
							<th class="left" rowspan="3">9.b</th>
							<th rowspan="3">Alamat Kantor</th>
							<td class="nowrap" rowspan="3">
								<form:textarea path="pemegang.alamat_kantor" cols="40" rows="5" onkeyup="textCounter(this, 200);" onkeydown="textCounter(this, 200);" cssErrorClass="inpError"/>
							</td>
							<th>Kode Pos</th>
							<td class="nowrap">
								<form:input path="pemegang.kd_pos_kantor" size="24" maxlength="10" cssErrorClass="inpError"/>
							</td>
							<th>Kota</th>
							<td class="nowrap">
								<form:input path="pemegang.kota_kantor" size="24" id="pemegang.kota_kantor" onfocus="this.select();" cssErrorClass="inpError"/>
						    	<span id="indicator_kantor" style="display:none;"><img src="${path}/include/image/indicator.gif" alt=""/></span>
							</td>
						</tr>
						<tr>
							<th>Telp Kantor 1</th>
							<td class="nowrap">
								<form:input path="pemegang.area_code_kantor" size="5" maxlength="4" cssErrorClass="inpError"/>
								<form:input path="pemegang.telpon_kantor" size="16" maxlength="20" cssErrorClass="inpError"/>
							</td>
							<th>Telp Kantor 2</th>
							<td class="nowrap">
								<form:input path="pemegang.area_code_kantor2" size="5" maxlength="4" cssErrorClass="inpError"/>
								<form:input path="pemegang.telpon_kantor2" size="16" maxlength="20" cssErrorClass="inpError"/>
							</td>
						</tr>
						<tr>
							<th>Fax</th>
							<td class="nowrap">
								<form:input path="pemegang.area_code_fax" size="5" maxlength="4" cssErrorClass="inpError"/>
								<form:input path="pemegang.no_fax" size="16" maxlength="20" cssErrorClass="inpError"/>
							</td>
							<th colspan="2"/>
						</tr>
						<tr>
							<th class="left" rowspan="5">9.c</th>
							<th rowspan="5">Alamat Penagihan / Korespondensi</th>
							<td class="nowrap" rowspan="5">
								<form:select path="addressbilling.tagih" onchange="addressbilling_onchange(false);">
									<form:option value="2" label="ALAMAT RUMAH"/>
									<form:option value="3" label="ALAMAT KANTOR"/>
									<form:option value="1" label="LAIN-LAIN"/>
								</form:select><br/>
								<form:textarea path="addressbilling.msap_address" cols="40" rows="6" onkeyup="textCounter(this, 200);" onkeydown="textCounter(this, 200);" cssErrorClass="inpError"/>
								<font class="error">*</font>
							</td>
							<th>Kode Pos</th>
							<td class="nowrap">
								<form:input path="addressbilling.msap_zip_code" size="24" maxlength="10" cssErrorClass="inpError"/>
								<font class="error">*</font>
							</td>
							<th>Kota</th>
							<td class="nowrap">
								<form:input path="addressbilling.kota_tgh" id="addressbilling.kota_tgh" size="24" onfocus="this.select();" cssErrorClass="inpError"/>
						    	<span id="indicator_tagih" style="display:none;"><img src="${path}/include/image/indicator.gif" alt=""/></span>
								<font class="error">*</font>
							</td>
						</tr>
						<tr>
							<th>Telp 1</th>
							<td class="nowrap">
								<form:input path="addressbilling.msap_area_code1" size="5" maxlength="4" cssErrorClass="inpError"/>
								<form:input path="addressbilling.msap_phone1" size="16" maxlength="20" cssErrorClass="inpError"/>
							</td>
							<th>Handphone 1</th>
							<td class="nowrap">
								<form:input path="addressbilling.no_hp" size="24" maxlength="20" cssErrorClass="inpError"/>
							</td>
						</tr>
						<tr>
							<th>Telp 2</th>
							<td class="nowrap">
								<form:input path="addressbilling.msap_area_code2" size="5" maxlength="4" cssErrorClass="inpError"/>
								<form:input path="addressbilling.msap_phone2" size="16" maxlength="20" cssErrorClass="inpError"/>
							</td>
							<th>Handphone 2</th>
							<td class="nowrap">
								<form:input path="addressbilling.no_hp2" size="24" maxlength="20" cssErrorClass="inpError"/>
							</td>
						</tr>
						<tr>
							<th>Telp 3</th>
							<td class="nowrap">
								<form:input path="addressbilling.msap_area_code3" size="5" maxlength="4" cssErrorClass="inpError"/>
								<form:input path="addressbilling.msap_phone3" size="16" maxlength="20" cssErrorClass="inpError"/>
							</td>
							<th>Fax</th>
							<td class="nowrap">
								<form:input path="addressbilling.msap_area_code_fax1" size="5" maxlength="4" cssErrorClass="inpError"/>
								<form:input path="addressbilling.msap_fax1" size="16" maxlength="20" cssErrorClass="inpError"/>
							</td>
						</tr>
						<tr>
							<th>Email</th>
							<td class="nowrap" colspan="2">
								<form:input path="addressbilling.e_mail" size="42" maxlength="50" cssErrorClass="inpError"/>
							</td>
							<th colspan="1"/>
						</tr>
						<tr>
							<th colspan="7" style="background-color: #D0D0D0">Know Your Customer</th>
						</tr>
						<tr>
							<th>10.</th>
							<th>Tujuan Membeli Asuransi</th>
							<td class="nowrap">
								<form:select path="pemegang.mkl_tujuan" cssStyle="width: 220px;">
									<form:option value="LAIN - LAIN" label="LAIN - LAIN"/>
									<form:option value="INVESTASI" label="INVESTASI"/>
									<form:option value="PERLINDUNGAN HARI TUA" label="PERLINDUNGAN HARI TUA"/>
									<form:option value="PERLINDUNGAN KELUARGA" label="PERLINDUNGAN KELUARGA"/>
									<form:option value="PERLINDUNGAN KESEHATAN" label="PERLINDUNGAN KESEHATAN"/>
									<form:option value="PERLINDUNGAN PENDIDIKAN" label="PERLINDUNGAN PENDIDIKAN"/>
									<form:option value="PERLINDUNGAN KECELAKAAN" label="PERLINDUNGAN KECELAKAAN"/>
								</form:select>
								<font class="error">*</font>
							</td>
							<th>Lain-Lain</th>
							<td class="nowrap" colspan="2">
								<form:input path="pemegang.tujuana" size="42" maxlength="100" cssErrorClass="inpError"/>
							</td>
							<th/>
						</tr>
						<tr>
							<th>11.</th>
							<th>Penghasilan Kotor<br/>Per Tahun</th>
							<td class="nowrap">
								<form:select path="pemegang.mkl_penghasilan" cssStyle="width: 220px;">
									<form:option value="<= RP. 10 JUTA" label="<= RP. 10 JUTA"/>
									<form:option value="> RP. 10 JUTA - RP. 50 JUTA" label="> RP. 10 JUTA - RP. 50 JUTA"/>
									<form:option value="> RP. 50 JUTA - RP. 100 JUTA" label="> RP. 50 JUTA - RP. 100 JUTA"/>
									<form:option value="> RP. 100 JUTA - RP. 300 JUTA" label="> RP. 100 JUTA - RP. 300 JUTA"/>
									<form:option value="> RP. 300 JUTA - RP. 500 JUTA" label="> RP. 300 JUTA - RP. 500 JUTA"/>
									<form:option value="> RP. 500 JUTA" label="> RP. 500 JUTA"/>
								</form:select>
								 <font class="error">*</font>
							 </td>
							<th colspan="4"/>
						</tr>
						<tr>
							<th>12.</th>
							<th>Sumber Pendanaan<br/>Pembelian Asuransi</th>
							<td class="nowrap">
								<form:select path="pemegang.mkl_pendanaan" cssStyle="width: 220px;">
									<form:option value="LAINNYA" label="LAINNYA"/>
									<form:option value="GAJI" label="GAJI"/>
									<form:option value="HASIL INVESTASI" label="HASIL INVESTASI"/>
									<form:option value="HASIL USAHA" label="HASIL USAHA"/>
									<form:option value="WARISAN" label="WARISAN"/>
								</form:select>
							 	<font class="error">*</font>
							 </td>
							<th>Lain-Lain</th>
							<td class="nowrap" colspan="2">
								<form:input path="pemegang.danaa" size="42" maxlength="100" cssErrorClass="inpError"/>
							</td>
							<th/>
						</tr>
						<tr>
							<th>13.</th>
							<th>Sumber Penghasilan</th>
							<td class="nowrap">
								<form:select path="pemegang.mkl_smbr_penghasilan" cssStyle="width: 220px;">
									<form:option value="LAINNYA" label="LAINNYA"/>
									<form:option value="GAJI" label="GAJI"/>
									<form:option value="HASIL INVESTASI" label="HASIL INVESTASI"/>
									<form:option value="HASIL USAHA" label="HASIL USAHA"/>
									<form:option value="WARISAN" label="WARISAN"/>
								</form:select>
								 <font class="error">*</font>
							</td>
							<th>Lain-Lain</th>
							<td class="nowrap" colspan="2">
								<form:input path="pemegang.danaa2" size="42" maxlength="100" cssErrorClass="inpError"/>
							</td>
							<th/>
						</tr>
						<tr>
							<th>14.</th>
							<th>Klasifikasi Pekerjaan</th>
							<td class="nowrap">
								<form:select path="pemegang.mkl_kerja" cssStyle="width: 220px;">
									<form:option value="LAINNYA" label="LAINNYA"/>
									<form:option value="BURUH" label="BURUH"/>
									<form:option value="IBU RUMAH TANGGA" label="IBU RUMAH TANGGA"/>
									<form:option value="JASA" label="JASA"/>
									<form:option value="KARYAWAN" label="KARYAWAN"/>
									<form:option value="PELAJAR" label="PELAJAR"/>
									<form:option value="PEMASARAN" label="PEMASARAN"/>
									<form:option value="PEMILIK/PENGUSAHA" label="PEMILIK/PENGUSAHA"/>
									<form:option value="PROFESIONAL" label="PROFESIONAL"/>
								</form:select>
								 <font class="error">*</font>
							</td>
							<th>Lain-Lain</th>
							<td class="nowrap" colspan="2">
								<form:input path="pemegang.kerjaa" size="42" maxlength="100" cssErrorClass="inpError"/>
							</td>
							<th/>
						</tr>
						<tr>
							<th></th>
							<th colspan="2"><span class="info">(Silahkan isi JABATAN hanya apabila klasifikasi pekerjaan KARYAWAN)</span></th>
							<th>Jabatan</th>
							<td class="nowrap" colspan="2">
								<form:input path="pemegang.kerjab" size="42" maxlength="100" cssErrorClass="inpError"/>
							</td>
							<th/>
						</tr>
						<tr>
							<th>15.</th>
							<th>Klasifikasi<br/>Bidang Industri</th>
							<td class="nowrap">
								<form:select path="pemegang.mkl_industri" cssStyle="width: 220px;">
									<form:option value="LAINNYA" label="LAINNYA"/>
									<form:option value="JASA KEUANGAN" label="JASA KEUANGAN"/>
									<form:option value="KONSTRUKSI" label="KONSTRUKSI"/>
									<form:option value="MANUFAKTUR" label="MANUFAKTUR"/>
									<form:option value="PEMERINTAHAN" label="PEMERINTAHAN"/>
									<form:option value="PERDAGANGAN" label="PERDAGANGAN"/>
									<form:option value="PERTAMBANGAN" label="PERTAMBANGAN"/>
									<form:option value="PERTANIAN" label="PERTANIAN"/>
								</form:select>
								<font class="error">*</font>
							</td>
							<th>Lain-lain</th>
							<td class="nowrap" colspan="2">
								<form:input path="pemegang.industria" size="42" maxlength="100" cssErrorClass="inpError"/>
							</td>
							<th/>
						</tr>
						<tr>
							<th colspan="7" style="background-color: #D0D0D0">Data Keluarga</th>
						</tr>
						<tr>
							<th>a.</th>
							<th>Nama Suami/Istri</th>
							<td class="nowrap">
								<form:input path="pemegang.nama_si" size="42" maxlength="30" cssErrorClass="inpError"/>
							</td>
							<th>Tgl Lahir</th>
							<td class="nowrap">
								<form:input path="pemegang.tgllhr_si" size="12" maxlength="10" cssErrorClass="inpError"/>
								<font class="info">(dd/mm/yyyy)</font>
							</td>
							<th colspan="2"/>
						</tr>
						<tr>
							<th>b.</th>
							<th>Nama Anak 1</th>
							<td class="nowrap">
								<form:input path="pemegang.nama_anak1" size="42" maxlength="30" cssErrorClass="inpError"/>
							</td>
							<th>Tgl Lahir</th>
							<td class="nowrap">
								<form:input path="pemegang.tgllhr_anak1" maxlength="10" size="12" cssErrorClass="inpError"/>
								<font class="info">(dd/mm/yyyy)</font>
							</td>
							<th colspan="2"/>
						</tr>
						<tr>
							<th>c.</th>
							<th>Nama Anak 2</th>
							<td class="nowrap">
								<form:input path="pemegang.nama_anak2" size="42" maxlength="30" cssErrorClass="inpError"/>
							</td>
							<th>Tgl Lahir</th>
							<td class="nowrap">
								<form:input path="pemegang.tgllhr_anak2" maxlength="10" size="12" cssErrorClass="inpError"/>
								<font class="info">(dd/mm/yyyy)</font>
							</td>
							<th colspan="2"/>
						</tr>
						<tr>
							<th>d.</th>
							<th>Nama Anak 3</th>
							<td class="nowrap">
								<form:input path="pemegang.nama_anak3" size="42" maxlength="30" cssErrorClass="inpError"/>
							</td>
							<th>Tgl Lahir</th>
							<td class="nowrap">
								<form:input path="pemegang.tgllhr_anak3" maxlength="10" size="12" cssErrorClass="inpError"/>
								<font class="info">(dd/mm/yyyy)</font>
							</td>
							<th colspan="2"/>
						</tr>
						<tr>
							<th>16.</th>
							<th colspan="2">Hubungan Calon Pemegang Polis dengan Calon Tertanggung</th>
							<td class="nowrap" colspan="3">
								<select name="pemegang.lsre_id" <c:if test="${ not empty status.errormessage}"> style='background-color :#ffe1fd'</c:if> style="width: 280px;">
									<c:forEach var="relasi" items="${select_relasi}">
										<option <c:if test="${cmd.pemegang.lsre_id eq relasi.id}"> selected </c:if>
											value="${relasi.ID}">${relasi.RELATION}</option>
									</c:forEach>
								</select>
								 <font class="error">*</font>
							 </td>
							 <th/>
						</tr>
						<tr>
							<th colspan="7" style="background-color: #D0D0D0">&nbsp;</th>
						</tr>
						<tr>
							<th colspan="7">
								<input type="submit" name="_target${halaman+1}" value="Next &raquo;" onclick="next()" accesskey="N" title="Alt-N">
							</th>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</form:form>
<script>
	hideLoadingMessage();
	<c:if test='${cmd.pemegang.jn_bank eq 2}'>
	 	<c:if test="${cmd.pemegang.rate_1 eq 0 }">
			alert('Rate Simas Prima per hari ini belum ada. Mohon hubungi IT Department PT. Asuransi Jiwa Sinarmas MSIG. Terima kasih.');
	 	</c:if>
	 	<c:if test="${cmd.pemegang.rate_2 eq 0 }">
			alert('Rate Simas Prima Manfaat Bulanan per hari ini belum ada. Mohon hubungi IT Department PT. Asuransi Jiwa Sinarmas MSIG. Terima kasih.');
	 	</c:if>
	</c:if>
</script>
<ajax:autocomplete
				  source="pemegang.kota_rumah"
				  target="pemegang.kota_rumah"
				  baseUrl="${path}/servlet/autocomplete?s=pemegang.kota_rumah&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_rumah"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />
<ajax:autocomplete
				  source="pemegang.kota_kantor"
				  target="pemegang.kota_kantor"
				  baseUrl="${path}/servlet/autocomplete?s=pemegang.kota_kantor&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_kantor"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />
<ajax:autocomplete
				  source="addressbilling.kota_tgh"
				  target="addressbilling.kota_tgh"
				  baseUrl="${path}/servlet/autocomplete?s=addressbilling.kota_tgh&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_tagih"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />			  
</body>
</html>