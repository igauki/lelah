<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%><%@ page import="com.ekalife.elions.model.User"%><%User currentUser = (User) session.getAttribute("currentUser");%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="Description" content="EkaLife">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="-1">
<link rel="Stylesheet" type="text/css" href="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">

<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>

<script type="text/javascript">
<!--
	function kyc(kombo, teks, lain){
		with(document.frmParam){
			var ket = elements[kombo].options[elements[kombo].selectedIndex].text;
			if (ket.toUpperCase() != lain) {
				elements[teks].value='';
			}else if ((elements[teks].value).toUpperCase()==lain) {
				elements[teks].value='';
			}
		}
	}
	//END OF kyc()
	
	function togel(elemen, kolor, readonlyORdisabled, trueORfalse){
		with(document.frmParam){
			if(readonlyORdisabled) elements[elemen].readOnly = trueORfalse;
			else elements[elemen].disabled = trueORfalse;
	 		elements[elemen].style.backgroundColor = kolor;
		}		
	}
	
	function Body_onload() {
		document.frmParam.elements['tertanggung.mste_age'].style.backgroundColor ='#D4D4D4';
		kyc('tertanggung.mkl_tujuan', 'tertanggung.tujuana', 'LAIN - LAIN');
		kyc('tertanggung.mkl_pendanaan', 'tertanggung.danaa', 'LAINNYA');
		kyc('tertanggung.mkl_smbr_penghasilan', 'tertanggung.danaa2', 'LAINNYA');
		kyc('tertanggung.mkl_kerja', 'tertanggung.kerjaa', 'LAINNYA');
		kyc('tertanggung.mkl_kerja', 'tertanggung.kerjab', 'KARYAWAN');
		kyc('tertanggung.mkl_industri', 'tertanggung.industria', 'LAINNYA');
		
		//apabila relasi diri sendiri
		var relasi = '${cmd.pemegang.lsre_id}';
		with(document.frmParam){
			if(relasi=='1'){
				togel('tertanggung.nama_si', '#D4D4D4', true, true);
				togel('tertanggung.nama_anak1', '#D4D4D4', true, true);
				togel('tertanggung.nama_anak2', '#D4D4D4', true, true);
				togel('tertanggung.nama_anak3', '#D4D4D4', true, true);
				togel('tertanggung.tgllhr_si', '#D4D4D4', true, true);
				togel('tertanggung.tgllhr_anak1', '#D4D4D4', true, true);
				togel('tertanggung.tgllhr_anak2', '#D4D4D4', true, true);
				togel('tertanggung.tgllhr_anak3', '#D4D4D4', true, true);
				togel('tertanggung.email', '#D4D4D4', true, true);
				togel('tertanggung.area_code_fax', '#D4D4D4', true, true);
				togel('tertanggung.no_fax', '#D4D4D4', true, true);
				togel('tertanggung.mspe_date_birth', '#D4D4D4', true, true);
				togel('tertanggung.mcl_first', '#D4D4D4', true, true);
				togel('tertanggung.mcl_gelar', '#D4D4D4', true, true);
				togel('tertanggung.mspe_mother', '#D4D4D4', true, true);
				togel('tertanggung.mspe_no_identity', '#D4D4D4', true, true);
				togel('tertanggung.mspe_place_birth', '#D4D4D4', true, true);
				togel('tertanggung.lside_id', '#D4D4D4', false, true);
				togel('tertanggung.lsne_id', '#D4D4D4', false, true);
				togel('tertanggung.mspe_sts_mrt', '#D4D4D4', false, true);
				togel('tertanggung.lsag_id', '#D4D4D4', false, true);
				togel('tertanggung.lsed_id', '#D4D4D4', false, true);
				togel('tertanggung.alamat_rumah', '#D4D4D4', true, true);
				togel('tertanggung.kota_rumah', '#D4D4D4', true, true);
				togel('tertanggung.kd_pos_rumah', '#D4D4D4', true, true);
				togel('tertanggung.area_code_rumah', '#D4D4D4', true, true);
				togel('tertanggung.telpon_rumah', '#D4D4D4', true, true);
				togel('tertanggung.area_code_rumah2', '#D4D4D4', true, true);
				togel('tertanggung.telpon_rumah2', '#D4D4D4', true, true);
				togel('tertanggung.alamat_kantor', '#D4D4D4', true, true);
				togel('tertanggung.kota_kantor', '#D4D4D4', true, true);
				togel('tertanggung.kd_pos_kantor', '#D4D4D4', true, true);
				togel('tertanggung.area_code_kantor', '#D4D4D4', true, true);
				togel('tertanggung.telpon_kantor', '#D4D4D4', true, true);
				togel('tertanggung.area_code_kantor2', '#D4D4D4', true, true);				
				togel('tertanggung.telpon_kantor2', '#D4D4D4', true, true);
				togel('tertanggung.mkl_kerja', '#D4D4D4', false, true);
				togel('tertanggung.kerjaa', '#D4D4D4', true, true);
				togel('tertanggung.kerjab', '#D4D4D4', true, true);
				togel('tertanggung.mkl_industri', '#D4D4D4', false, true);			
				togel('tertanggung.industria', '#D4D4D4', true, true);
				togel('tertanggung.mkl_tujuan', '#D4D4D4', false, true);			
				togel('tertanggung.tujuana', '#D4D4D4', true, true);
				togel('tertanggung.mkl_pendanaan', '#D4D4D4', false, true);			
				togel('tertanggung.mkl_smbr_penghasilan', '#D4D4D4', false, true);
				togel('tertanggung.danaa', '#D4D4D4', true, true);
				togel('tertanggung.danaa2', '#D4D4D4', true, true);
				togel('tertanggung.mkl_penghasilan', '#D4D4D4', false, true);			
				togel('tertanggung.no_hp2', '#D4D4D4', true, true);
				togel('tertanggung.no_hp', '#D4D4D4', true, true);
				
				elements['tertanggung.mspe_sex'].value = '${cmd.pemegang.mspe_sex}';
				elements['tertanggung.mspe_sex'][0].disabled = true;
				elements['tertanggung.mspe_sex'][1].disabled = true;
			}
		}
	}

	function next() {
		createLoadingMessage();
		with(document.frmParam){
			elements['pemegang.indeks_halaman'].value = (elements['hal'].value) + 1;
		}
	}

	function kelamin(hasil) {
		document.frmParam.elements['tertanggung.mspe_sex'].value = hasil;
	}
-->	
</script>

<body onLoad="Body_onload(); setupPanes('container1', 'tab1'); ">
<XML ID=xmlData></XML>

<form:form commandName="cmd" name="frmParam" id="frmParam" method="post">

	<!-- Hidden Fields -->
		<c:choose>
			<c:when test="${cmd.pemegang.reg_spaj eq null}"><c:set var="disableFlag" value="disabled"/></c:when>
			<c:otherwise><c:set var="disableFlag" value=""/></c:otherwise>
		</c:choose>
		
		<input type="submit" id="_target0" name="_target0" onclick="next('_target0')" style="display: none;" ${disableFlag}>
		<input type="submit" id="_target1" name="_target1" onclick="next('_target1')" style="display: none;" ${disableFlag}>
		<input type="submit" id="_target2" name="_target2" onclick="next('_target2')" style="display: none;" ${disableFlag}>
		<input type="submit" id="_target3" name="_target3" onclick="next('_target3')" style="display: none;" ${disableFlag}>
		<input type="submit" id="_target4" name="_target4" onclick="next('_target4')" style="display: none;" ${disableFlag}>
		<input type="submit" id="_target5" name="_target5" onclick="next('_target5')" style="display: none;" ${disableFlag}>
	
		<form:hidden path="pemegang.keterangan_blanko"/>
		<input type="hidden" name="_page" value="${halaman}">		
		<input type="hidden" name="hal" value="${halaman}">
		 <spring:bind path="cmd.pemegang.indeks_halaman">
			<input type="hidden" name="${status.expression}" value="${halaman-1}">
		 </spring:bind>
	<!-- End of Hidden Fields -->

	<div id="contents">
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li><a href="#" onclick="document.getElementById('_target0').click(); return false;">Pemegang</a></li>
				<li><a href="#" onclick="return showPane('pane1', this);" id="tab1" class="tab-active" >Tertanggung</a></li>
				<li><a href="#" onclick="document.getElementById('_target2').click(); return false;">Asuransi</a></li>
				<li><a href="#" onclick="document.getElementById('_target3').click(); return false;">Investasi</a></li>
				<li><a href="#" onclick="document.getElementById('_target4').click(); return false;">Agen</a></li>
				<li><a href="#" onclick="document.getElementById('_target5').click(); return false;">Konfirmasi</a></li>
				<li><a href="#" onclick="return false;">Finish</a></li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<c:if test="${not empty cmd.pemegang.keterangan_blanko and cmd.pemegang.keterangan_blanko ne \"NONE\"}">
						<div id="error">INFORMASI: ${cmd.pemegang.keterangan_blanko}</div>
					</c:if>
					<c:if test="${cmd.pemegang.cek_blanko eq 1 and cmd.pemegang.reg_spaj eq null}">
						<div id="error">NO BLANKO YANG DICANTUMKAN SUDAH PERNAH DIGUNAKAN UNTUK PRODUK YANG SAMA</div>
					</c:if>
					<table class="entry2">
						<spring:bind path="cmd.*">
							<c:if test="${not empty status.errorMessages}">
								<tr>
									<td colspan="7">
										<div id="error">ERROR:<br>
										<c:forEach var="error" items="${status.errorMessages}">
											- <c:out value="${error}" escapeXml="false" />
											<br />
										</c:forEach>
										</div>
									</td>
								</tr>
							</c:if>
						</spring:bind>	
						<tr>
							<th colspan="7">
								<input type="submit" name="_target${halaman-1}" value="&laquo; Prev" onclick="createLoadingMessage();" accesskey="P" title="Alt-P">
								<input type="submit" name="_target${halaman+1}" value="Next &raquo;" onclick="next()" accesskey="N" title="Alt-N">
							</th>
						</tr>
						<tr>
							<th colspan="7" style="background-color: #D0D0D0">
								B. Data Calon Tertanggung
								<br/><span class="info">(Hanya diisi apabila Pemegang Polis berbeda dengan calon Tertanggung)</span>
							</th>
						</tr>
						<tr>
							<th class="left">1.</th>
							<th>
								Nama Lengkap<br>
								<span class="info">(sesuai Identitas, Tanpa Gelar)</span>
							</th>
							<td class="nowrap">
								<form:input path="tertanggung.mcl_first" size="41" maxlength="100" cssErrorClass="inpError"/>
								<font class="error">*</font>
							</td>
							<th>Gelar</th>
							<td class="nowrap"><form:input path="tertanggung.mcl_gelar" size="24" maxlength="15"/></td>
							<th colspan="2"/>
						</tr>
						<tr>
							<th class="left">2.</th>
							<th>Nama Ibu Kandung</th>
							<td class="nowrap">
								<form:input path="tertanggung.mspe_mother" size="41" maxlength="50" cssErrorClass="inpError"/>
								<font class="error">*</font>
							</td>
							<th colspan="4"/>
						</tr>
						<tr>
							<th class="left">3.</th>
							<th>Identitas</th>
							<td class="nowrap">
								<form:select path="tertanggung.lside_id" cssStyle="width: 160px;" items="${select_identitas}" itemValue="key" itemLabel="value" />
								<font class="error">*</font>
							</td>
							<th>No. Identitas</th>
							<td class="nowrap">
								<form:input path="tertanggung.mspe_no_identity" size="24" maxlength="50" cssErrorClass="inpError"/>
								<font class="error">*</font>
							</td>
							<th colspan="2"/>
						</tr>
						<tr>
							<th class="left">4.</th>
							<th>Warga Negara</th>
							<td class="nowrap">
								<form:select path="tertanggung.lsne_id" cssStyle="width: 160px;" items="${select_negara}" itemValue="key" itemLabel="value" />
								<font class="error">*</font>
							</td>
							<th>Umur Beasiswa</th>
							<td class="nowrap"><form:input path="tertanggung.mspo_umur_beasiswa" size="4" maxlength="2"/> Tahun</td><th></th>
							<th colspan="2"/>
						</tr>
						<tr>
							<th class="left">5.</th>
							<th>Tgl Lahir</th>
							<td class="nowrap">
								<form:input path="tertanggung.mspe_date_birth" maxlength="10" size="12" cssErrorClass="inpError"/>
								<font class="error">*</font> <font class="info">(dd/mm/yyyy)</font>
							</td>
							<th>Lahir Di</th>
							<td class="nowrap">
								<form:input path="tertanggung.mspe_place_birth" size="24" maxlength="30" cssErrorClass="inpError"/>
								<font class="error">*</font>
							</td>
							<th>Usia</th>
							<td class="nowrap"><form:input path="tertanggung.mste_age" size="4" readonly="true"/> Tahun</td><th></th>
						</tr>
						<tr>
							<th class="left">6.</th>
							<th>Jenis Kelamin</th>
							<td class="nowrap">
								<label for="cowok">
									<form:radiobutton path="tertanggung.mspe_sex" id="cowok" value="1" cssClass="noBorder"/> Pria
								</label>
								<label for="cewek">
									<form:radiobutton path="tertanggung.mspe_sex" id="cewek" value="0" cssClass="noBorder"/> Wanita
								</label>
								<font class="error">*</font>
							</td>
							<th>Status</th>
							<td class="nowrap">							
								<form:select path="tertanggung.mspe_sts_mrt" cssStyle="width: 130px;">
									<form:option value="1" label="BELUM MENIKAH"/>
									<form:option value="2" label="MENIKAH"/>
									<form:option value="3" label="JANDA"/>
									<form:option value="4" label="DUDA"/>
								</form:select>
								<font class="error">*</font>
							</td>
							<th colspan="2"/>
						</tr>
						<tr>
							<th class="left">7.</th>
							<th>Agama</th>
							<td class="nowrap">							
								<form:select path="tertanggung.lsag_id" cssStyle="width: 160px;" items="${select_agama}" itemValue="key" itemLabel="value" />
								<font class="error">*</font>
							</td>
							<th colspan="4"/>
						</tr>
						<tr>
							<th class="left">8.</th>
							<th>Pendidikan</th>
							<td class="nowrap">							
								<form:select path="tertanggung.lsed_id" cssStyle="width: 160px;" items="${select_pendidikan}" itemValue="key" itemLabel="value" />
								<font class="error">*</font>
							</td>
							<th colspan="4"/>
						</tr>
						<tr>
							<th class="left" rowspan="4">9.a</th>
							<th rowspan="4">Alamat Rumah</th>
							<td class="nowrap" rowspan="4">
								<form:textarea path="tertanggung.alamat_rumah" cols="40" rows="5" onkeyup="textCounter(this, 200);" onkeydown="textCounter(this, 200);" cssErrorClass="inpError"/>
								<font class="error">*</font>
							</td>
							<th>Kode Pos</th>
							<td class="nowrap">
								<form:input path="tertanggung.kd_pos_rumah" size="24" maxlength="10" cssErrorClass="inpError"/>
								<font class="error">*</font>
							</td>
							<th>Kota</th>
							<td class="nowrap">
								<form:input path="tertanggung.kota_rumah" id="tertanggung.kota_rumah" size="24" onfocus="this.select();" cssErrorClass="inpError"/>
						    	<span id="indicator_rumah" style="display:none;"><img src="${path}/include/image/indicator.gif" alt=""/></span>
								<font class="error">*</font>
							</td>
						</tr>
						<tr>
							<th class="nowrap">Telp Rumah 1</th>
							<td class="nowrap">
								<form:input path="tertanggung.area_code_rumah" size="5" maxlength="4" cssErrorClass="inpError"/>
								<form:input path="tertanggung.telpon_rumah" size="16" maxlength="24" cssErrorClass="inpError"/>
								<font class="error">*</font>
							</td>
							<th>Telp Rumah 2</th>
							<td class="nowrap">
								<form:input path="tertanggung.area_code_rumah2" size="5" maxlength="4" cssErrorClass="inpError"/>
								<form:input path="tertanggung.telpon_rumah2" size="16" maxlength="24" cssErrorClass="inpError"/>
							</td>
						</tr>
						<tr>
							<th>Handphone 1</th>
							<td class="nowrap">
								<form:input path="tertanggung.no_hp" size="24" maxlength="20" cssErrorClass="inpError"/>
								<font class="error">*</font>
							</td>
							<th>Handphone 2</th>
							<td class="nowrap">
								<form:input path="tertanggung.no_hp2" size="24" maxlength="20" cssErrorClass="inpError"/>
							</td>
						</tr>
						<tr>
							<th>Email</th>
							<td class="nowrap" colspan="2">
								<form:input path="tertanggung.email" size="42" maxlength="50" cssErrorClass="inpError"/>
							</td>
							<th colspan="1"/>
						</tr>
						<tr>
							<th class="left" rowspan="3">9.b</th>
							<th rowspan="3">Alamat Kantor</th>
							<td class="nowrap" rowspan="3">
								<form:textarea path="tertanggung.alamat_kantor" cols="40" rows="5" onkeyup="textCounter(this, 200);" onkeydown="textCounter(this, 200);" cssErrorClass="inpError"/>
							</td>
							<th>Kode Pos</th>
							<td class="nowrap">
								<form:input path="tertanggung.kd_pos_kantor" size="24" maxlength="10" cssErrorClass="inpError"/>
							</td>
							<th>Kota</th>
							<td class="nowrap">
								<form:input path="tertanggung.kota_kantor" size="24" id="tertanggung.kota_kantor" onfocus="this.select();" cssErrorClass="inpError"/>
						    	<span id="indicator_kantor" style="display:none;"><img src="${path}/include/image/indicator.gif" alt=""/></span>
							</td>
						</tr>
						<tr>
							<th>Telp Kantor 1</th>
							<td class="nowrap">
								<form:input path="tertanggung.area_code_kantor" size="5" maxlength="4" cssErrorClass="inpError"/>
								<form:input path="tertanggung.telpon_kantor" size="16" maxlength="20" cssErrorClass="inpError"/>
							</td>
							<th>Telp Kantor 2</th>
							<td class="nowrap">
								<form:input path="tertanggung.area_code_kantor2" size="5" maxlength="4" cssErrorClass="inpError"/>
								<form:input path="tertanggung.telpon_kantor2" size="16" maxlength="20" cssErrorClass="inpError"/>
							</td>
						</tr>
						<tr>
							<th>Fax</th>
							<td class="nowrap">
								<form:input path="tertanggung.area_code_fax" size="5" maxlength="4" cssErrorClass="inpError"/>
								<form:input path="tertanggung.no_fax" size="16" maxlength="20" cssErrorClass="inpError"/>
							</td>
							<th colspan="2"/>
						</tr>
						<tr>
							<th colspan="7" style="background-color: #D0D0D0">Know Your Customer</th>
						</tr>
						<tr>
							<th>10.</th>
							<th>Tujuan Membeli Asuransi</th>
							<td class="nowrap">
								<form:select path="tertanggung.mkl_tujuan" cssStyle="width: 220px;">
									<form:option value="LAIN-LAIN" label="LAIN-LAIN"/>
									<form:option value="INVESTASI" label="INVESTASI"/>
									<form:option value="PERLINDUNGAN HARI TUA" label="PERLINDUNGAN HARI TUA"/>
									<form:option value="PERLINDUNGAN KELUARGA" label="PERLINDUNGAN KELUARGA"/>
									<form:option value="PERLINDUNGAN KESEHATAN" label="PERLINDUNGAN KESEHATAN"/>
									<form:option value="PERLINDUNGAN PENDIDIKAN" label="PERLINDUNGAN PENDIDIKAN"/>
									<form:option value="PERLINDUNGAN KECELAKAAN" label="PERLINDUNGAN KECELAKAAN"/>
								</form:select>
								<font class="error">*</font>
							</td>
							<th>Lain-Lain</th>
							<td class="nowrap" colspan="2">
								<form:input path="tertanggung.tujuana" size="42" maxlength="100" cssErrorClass="inpError"/>
							</td>
							<th/>
						</tr>
						<tr>
							<th>11.</th>
							<th>Penghasilan Kotor<br/>Per Tahun</th>
							<td class="nowrap">
								<form:select path="tertanggung.mkl_penghasilan" cssStyle="width: 220px;">
									<form:option value="<= RP. 10 JUTA" label="<= RP. 10 JUTA"/>
									<form:option value="> RP. 10 JUTA - RP. 50 JUTA" label="> RP. 10 JUTA - RP. 50 JUTA"/>
									<form:option value="> RP. 50 JUTA - RP. 100 JUTA" label="> RP. 50 JUTA - RP. 100 JUTA"/>
									<form:option value="> RP. 100 JUTA - RP. 300 JUTA" label="> RP. 100 JUTA - RP. 300 JUTA"/>
									<form:option value="> RP. 300 JUTA - RP. 500 JUTA" label="> RP. 300 JUTA - RP. 500 JUTA"/>
									<form:option value="> RP. 500 JUTA" label="> RP. 500 JUTA"/>
								</form:select>
								 <font class="error">*</font>
							 </td>
							<th colspan="4"/>
						</tr>
						<tr>
							<th>12.</th>
							<th>Sumber Pendanaan<br/>Pembelian Asuransi</th>
							<td class="nowrap">
								<form:select path="tertanggung.mkl_pendanaan" cssStyle="width: 220px;">
									<form:option value="LAINNYA" label="LAINNYA"/>
									<form:option value="GAJI" label="GAJI"/>
									<form:option value="HASIL INVESTASI" label="HASIL INVESTASI"/>
									<form:option value="HASIL USAHA" label="HASIL USAHA"/>
									<form:option value="WARISAN" label="WARISAN"/>
								</form:select>
							 	<font class="error">*</font>
							 </td>
							<th>Lain-Lain</th>
							<td class="nowrap" colspan="2">
								<form:input path="tertanggung.danaa" size="42" maxlength="100" cssErrorClass="inpError"/>
							</td>
							<th/>
						</tr>
						<tr>
							<th>13.</th>
							<th>Sumber Penghasilan</th>
							<td class="nowrap">
								<form:select path="tertanggung.mkl_smbr_penghasilan" cssStyle="width: 220px;">
									<form:option value="LAINNYA" label="LAINNYA"/>
									<form:option value="GAJI" label="GAJI"/>
									<form:option value="HASIL INVESTASI" label="HASIL INVESTASI"/>
									<form:option value="HASIL USAHA" label="HASIL USAHA"/>
									<form:option value="WARISAN" label="WARISAN"/>
								</form:select>
								 <font class="error">*</font>
							</td>
							<th>Lain-Lain</th>
							<td class="nowrap" colspan="2">
								<form:input path="tertanggung.danaa2" size="42" maxlength="100" cssErrorClass="inpError"/>
							</td>
							<th/>
						</tr>
						<tr>
							<th>14.</th>
							<th>Klasifikasi Pekerjaan</th>
							<td class="nowrap">
								<form:select path="tertanggung.mkl_kerja" cssStyle="width: 220px;">
									<form:option value="LAINNYA" label="LAINNYA"/>
									<form:option value="BURUH" label="BURUH"/>
									<form:option value="IBU RUMAH TANGGA" label="IBU RUMAH TANGGA"/>
									<form:option value="JASA" label="JASA"/>
									<form:option value="KARYAWAN" label="KARYAWAN"/>
									<form:option value="PELAJAR" label="PELAJAR"/>
									<form:option value="PEMASARAN" label="PEMASARAN"/>
									<form:option value="PEMILIK/PENGUSAHA" label="PEMILIK/PENGUSAHA"/>
									<form:option value="PROFESIONAL" label="PROFESIONAL"/>
								</form:select>
								 <font class="error">*</font>
							</td>
							<th>Lain-Lain</th>
							<td class="nowrap" colspan="2">
								<form:input path="tertanggung.kerjaa" size="42" maxlength="100" cssErrorClass="inpError"/>
							</td>
							<th/>
						</tr>
						<tr>
							<th></th>
							<th colspan="2"><span class="info">(Silahkan isi JABATAN hanya apabila klasifikasi pekerjaan KARYAWAN)</span></th>
							<th>Jabatan</th>
							<td class="nowrap" colspan="2">
								<form:input path="tertanggung.kerjab" size="42" maxlength="100" cssErrorClass="inpError"/>
							</td>
							<th/>
						</tr>
						<tr>
							<th>15.</th>
							<th>Klasifikasi<br/>Bidang Industri</th>
							<td class="nowrap">
								<form:select path="tertanggung.mkl_industri" cssStyle="width: 220px;">
									<form:option value="LAINNYA" label="LAINNYA"/>
									<form:option value="JASA KEUANGAN" label="JASA KEUANGAN"/>
									<form:option value="KONSTRUKSI" label="KONSTRUKSI"/>
									<form:option value="MANUFAKTUR" label="MANUFAKTUR"/>
									<form:option value="PEMERINTAHAN" label="PEMERINTAHAN"/>
									<form:option value="PERDAGANGAN" label="PERDAGANGAN"/>
									<form:option value="PERTAMBANGAN" label="PERTAMBANGAN"/>
									<form:option value="PERTANIAN" label="PERTANIAN"/>
								</form:select>
								<font class="error">*</font>
							</td>
							<th>Lain-lain</th>
							<td class="nowrap" colspan="2">
								<form:input path="tertanggung.industria" size="42" maxlength="100" cssErrorClass="inpError"/>
							</td>
							<th/>
						</tr>
						<tr>
							<th colspan="7" style="background-color: #D0D0D0">Data Keluarga</th>
						</tr>
						<tr>
							<th>a.</th>
							<th>Nama Suami/Istri</th>
							<td class="nowrap">
								<form:input path="tertanggung.nama_si" size="42" maxlength="30" cssErrorClass="inpError"/>
							</td>
							<th>Tgl Lahir</th>
							<td class="nowrap">
								<form:input path="tertanggung.tgllhr_si" size="12" maxlength="10" cssErrorClass="inpError"/>
								<font class="info">(dd/mm/yyyy)</font>
							</td>
							<th colspan="2"/>
						</tr>
						<tr>
							<th>b.</th>
							<th>Nama Anak 1</th>
							<td class="nowrap">
								<form:input path="tertanggung.nama_anak1" size="42" maxlength="30" cssErrorClass="inpError"/>
							</td>
							<th>Tgl Lahir</th>
							<td class="nowrap">
								<form:input path="tertanggung.tgllhr_anak1" maxlength="10" size="12" cssErrorClass="inpError"/>
								<font class="info">(dd/mm/yyyy)</font>
							</td>
							<th colspan="2"/>
						</tr>
						<tr>
							<th>c.</th>
							<th>Nama Anak 2</th>
							<td class="nowrap">
								<form:input path="tertanggung.nama_anak2" size="42" maxlength="30" cssErrorClass="inpError"/>
							</td>
							<th>Tgl Lahir</th>
							<td class="nowrap">
								<form:input path="tertanggung.tgllhr_anak2" maxlength="10" size="12" cssErrorClass="inpError"/>
								<font class="info">(dd/mm/yyyy)</font>
							</td>
							<th colspan="2"/>
						</tr>
						<tr>
							<th>d.</th>
							<th>Nama Anak 3</th>
							<td class="nowrap">
								<form:input path="tertanggung.nama_anak3" size="42" maxlength="30" cssErrorClass="inpError"/>
							</td>
							<th>Tgl Lahir</th>
							<td class="nowrap">
								<form:input path="tertanggung.tgllhr_anak3" maxlength="10" size="12" cssErrorClass="inpError"/>
								<font class="info">(dd/mm/yyyy)</font>
							</td>
							<th colspan="2"/>
						</tr>
						<tr>
							<th colspan="7" style="background-color: #D0D0D0">&nbsp;</th>
						</tr>
						<tr>
							<th colspan="7">
								<input type="submit" name="_target${halaman-1}" value="&laquo; Prev" onclick="createLoadingMessage();" accesskey="P" title="Alt-P">
								<input type="submit" name="_target${halaman+1}" value="Next &raquo;" onclick="next()" accesskey="N" title="Alt-N">
							</th>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	
</form:form>
<ajax:autocomplete
				  source="tertanggung.kota_rumah"
				  target="tertanggung.kota_rumah"
				  baseUrl="${path}/servlet/autocomplete?s=tertanggung.kota_rumah&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_rumah"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />

<ajax:autocomplete
				  source="tertanggung.kota_kantor"
				  target="tertanggung.kota_kantor"
				  baseUrl="${path}/servlet/autocomplete?s=tertanggung.kota_kantor&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_kantor"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />
</body>
<%@ include file="/include/page/footer.jsp"%>