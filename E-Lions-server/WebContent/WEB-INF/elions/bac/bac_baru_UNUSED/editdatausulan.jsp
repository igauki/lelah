<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen"> <!-- CSS untuk DatePicker Script (jscalendar) -->
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script><!-- DatePicker Script (jscalendar) -->
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script><!-- DatePicker Script (jscalendar) -->
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script><!-- DatePicker Script (jscalendar) -->
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript">
<!--
<spring:bind path="cmd.datausulan.jmlrider"> 
	var mn=${status.value};
</spring:bind>  

var varOptionrider;
var varOptionins;
var varOptionhub;
var flag_add=0;
var flag_add1=0;

function generateXML(intValue, objParser, nameSelection, strID, strDesc ,lbr,tgg,tab) {
	var strSelection;
	var strHTML = "<SELECT NAME=" + nameSelection  + "  style=font-family: Verdana; width: " +lbr+"; height:"+tgg+"; font-size:8pt tabindex="+tab+ ">";
	var collPosition = objParser.selectNodes( "//Position" );
	//
	for( var i = 0; i < collPosition.length; i++ ) {
		if ((intValue).toUpperCase() == (collPosition.item( i ).selectSingleNode(strID).text).toUpperCase())
			strSelection = "SELECTED";
		else
			strSelection = "";
		strHTML += "<OPTION " + strSelection + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
	}
	strHTML += "</SELECT>"
	//
	return strHTML;
}	
	
function generateXML_onChange(intValue, objParser, nameSelection, strID, strDesc ,lbr,tgg,tab) {
	var strSelection;
	var strHTML = "<SELECT NAME=" + nameSelection  + "  style=font-family: Verdana; width: " +lbr+"; height:"+tgg+"; font-size:8pt tabindex="+tab+ ">";
	var collPosition = objParser.selectNodes( "//Position" );

	for( var i = 0; i < collPosition.length; i++ ) {
		if ((intValue) == (collPosition.item( i ).selectSingleNode(strID).text))
			strSelection = "SELECTED";
		else
			strSelection = "";
		strHTML += "<OPTION " + strSelection + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
	}
	strHTML += "</SELECT>"
	return strHTML;
}	
	
//function xml rider
function generateXML_rider( objParser, strID, strDesc ) {
	varOptionrider = "";
	var collPosition = objParser.selectNodes( "//Position" );
	for( var i = 0; i < collPosition.length; i++ ) varOptionrider += "<OPTION " + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
}	
	
//function xml ins rider
function generateXML_insrider( objParser, strID, strDesc ) {
	varOptionins = "";
	var collPosition = objParser.selectNodes( "//Position" );
	for( var i = 0; i < collPosition.length; i++ ) varOptionins += "<OPTION " + " VALUE='" + collPosition.item( i ).selectSingleNode(strID).text + "'>" + collPosition.item( i ).selectSingleNode(strDesc).text + "</OPTION>";
}		
	
function Body_onload() {
	with(document.frmParam){
		xmlData.async = false;
		elements['datausulan.mspr_class'].readOnly=true;
		elements['datausulan.mspr_class'].style.backgroundColor ='#D4D4D4';
		xmlData.src = "${path}/xml/PLANRIDER.xml";
		generateXML_rider(xmlData, 'BISNIS_ID','NAME');
		xmlData.src = "${path}/xml/INSRIDER.xml";
		generateXML_insrider( xmlData, 'ID','INSRIDER');
		//	
		elements['datausulan.mspo_pay_period'].style.backgroundColor ='#D4D4D4';
		elements['datausulan.mspr_ins_period'].style.backgroundColor ='#D4D4D4';
		elements['datausulan.jmlrider'].value=${cmd.datausulan.jmlrider};
		kode_sementara.value='${cmd.datausulan.plan}';
		//elements['datausulan.mste_beg_date'].style.backgroundColor ='#D4D4D4';
		
		if (kode_sementara.value!="") {
			ProductConfig1(elements['datausulan.lsbs_id'].value,kode_sementara.value);
			perproduk1(kode_sementara.value);
			if (status.value.toUpperCase()==("edit").toUpperCase()){
				cek_rider_include(kode_sementara.value);
			}
		}else{
			<c:if test="${sessionScope.currentUser.jn_bank eq 0 or sessionScope.currentUser.jn_bank eq 1}">
				ProductConfigPlatinumBII(155);
			</c:if>
			<c:if test="${sessionScope.currentUser.jn_bank eq 2}">
				ProductConfigBankSinarmas(142);
			</c:if>
			<c:if test="${sessionScope.currentUser.jn_bank eq 3}">
				ProductConfigSekuritas(elements['datausulan.lsbs_id'].value,kode_sementara.value);
			</c:if>
		}
		if (tanda_premi.value==null || tanda_premi.value==""){
			elements['datausulan.cara_premi'].value = '0';
			tanda_premi.value = '0';
		}
		cpy(tanda_premi.value);
	}
}

function addRowDOM1 (tableID, jml) {
	flag_add1=1
  	var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
	var tmp = parseInt(document.getElementById("tablerider").rows.length);
	var oRow = oTable.insertRow(oTable.rows.length);
       var oCells = oRow.cells;
	var i=tmp-1;
	var idx = mn + 1;
	ajaxSelectplan(document.frmParam.elements['datausulan.lsbs_id'].value,'list_rider', 'ridermenu'+idx, 'ride.plan_rider'+idx,'ride.plan_rider'+idx , 'plan', 'lsdbs_name', '');
	
	if (document.all) {
		var cell = oRow.insertCell(oCells.length);
		cell.innerHTML = "<td >"+idx+"</td>";
		var cell = oRow.insertCell(oCells.length);
		cell.innerHTML = "<td ><div id=ridermenu"+idx+"> </div><input type=hidden name='ride.plan_rider1"+idx+"'></td>";
		var cell = oRow.insertCell(oCells.length);
		cell.innerHTML = "<td><input type=text name='ride.mspr_unit"+idx+"' value='0' size=3 maxlength=2></td>";         
		var cell = oRow.insertCell(oCells.length);
		cell.innerHTML =  "<td ><input type=text name='ride.mspr_class"+idx+"' value='0' size=3 maxlength=2></td>";
		var cell = oRow.insertCell(oCells.length);
		cell.innerHTML ="<td><input type=text name='ride.mspr_tsi"+idx+"' size=15 style=background-color :#D4D4D4 readOnly> </td>";
		var cell = oRow.insertCell(oCells.length);
		cell.innerHTML ="<td><input type=text name='ride.mspr_premium"+idx+"' size=15 style=background-color :#D4D4D4 readOnly> </td>";
		var cell = oRow.insertCell(oCells.length);
		cell.innerHTML ="<td><input type=text name='ride.mspr_ins_period"+idx+"'   size=5 style=background-color :#D4D4D4 readOnly></td>";
		var cell = oRow.insertCell(oCells.length);
		cell.innerHTML ="<td><input type=text name='mspr_beg_date"+idx+"' value=''  size=12 style=background-color :#D4D4D4 readOnly></td>";
		var cell = oRow.insertCell(oCells.length);
		cell.innerHTML ="<td ><input type=text name='mspr_end_date"+idx+"' value=''  size=12 style=background-color :#D4D4D4 readOnly></td>";
		var cell = oRow.insertCell(oCells.length);
		cell.innerHTML ="<td ><input type=text name='mspr_end_pay"+idx+"' value=''  size=12 style=background-color :#D4D4D4 readOnly></td>";
		var cell = oRow.insertCell(oCells.length);
		cell.innerHTML ="<td><select name='ride.mspr_tt"+idx+"'>"+varOptionins+"</select> <input type=hidden name='ride.mspr_tt1"+idx+"'></td>";																											
		var cell = oRow.insertCell(oCells.length);
		cell.innerHTML ="<td><input type=text name='ride.mspr_rate"+idx+"' value='' size=5 style=background-color :#D4D4D4 readOnly> </td>";
		var cell = oRow.insertCell(oCells.length);
		cell.innerHTML ="<td ><input type=checkbox class=\"noBorder\" name=cek"+idx+" id=ck"+idx+"' ><input type=hidden name'ride.flag_include"+idx+"' value=0 size=3></td></tr>";
		
		var cell = oRow.insertCell(oCells.length);			
		cell.innerHTML = '';
		mn = idx;
	}
 }

function prev(){
	createLoadingMessage();
	document.frmParam.mn.value=mn;
	if (eval(mn) >0 ){
		for (var k =1;k<=mn;k++)	{
			eval(" document.frmParam.elements['ride.mspr_tt1"+k+"'].value = document.frmParam.elements['ride.mspr_tt"+k+"'].value;");
			eval(" document.frmParam.elements['ride.plan_rider1"+k+"'].value =document.frmParam.elements['ride.plan_rider"+k+"'].value;");  
		}
	}
} 
 
function next(tarrrget){
	createLoadingMessage();
	if(!tarrrget) eval( " document.frmParam.elements['pemegang.indeks_halaman'].value = ((document.frmParam.hal.value)+1);");
	document.frmParam.mn.value=mn;
	if (eval(mn) >0 ){
		for (var k =1;k<=mn;k++)	{
			eval(" document.frmParam.elements['ride.mspr_tt1"+k+"'].value = document.frmParam.elements['ride.mspr_tt"+k+"'].value;");
			eval(" document.frmParam.elements['ride.plan_rider1"+k+"'].value =document.frmParam.elements['ride.plan_rider"+k+"'].value;");  
		}
	}
} 	

function cancel1(){		
	var idx = mn;
	if  ((idx)!=null && (idx)!=""){
		if (idx >0)
			flag_add1=1;
	}
	if(flag_add1==1){
		deleteRowDOM1('tablerider', '1');
	}
}

function deleteallrider (tableID,rowCount){ 
   var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
    var row=parseInt(document.getElementById("tablerider").rows.length);
	var flag_row=0;
	var jumlah_cek=0;
	var idx = mn;

	if(row>1){
		for (var i=1;i<((parseInt(row)));i++)	{
			oTable.deleteRow(parseInt(document.getElementById("tablerider").rows.length)-1);
		}
	}
	row=0;
	mn=0;
	document.frmParam.mn.value=0;
}

function deleteRowDOM1 (tableID,rowCount){ 
    var oTable = document.all ? document.all[tableID] : document.getElementById(tableID);
    var row=parseInt(document.getElementById("tablerider").rows.length);
	var flag_row=0;
	var jumlah_cek=0;
	var idx = mn;

	if(row>0){
		flag_row=0;
		for (var i=1;i<((parseInt(row)));i++)	{
			if (eval("document.frmParam.elements['cek"+i+"'].checked")){
				idx=idx-1;
				for (var k =i ; k<((parseInt(row))-1);k++)	{
					eval(" document.frmParam.elements['ride.plan_rider"+k+"'].value =document.frmParam.elements['ride.plan_rider"+(k+1)+"'].value;");   
					eval(" document.frmParam.elements['ride.plan_rider1"+k+"'].value =document.frmParam.elements['ride.plan_rider"+(k+1)+"'].value;");  
					eval(" document.frmParam.elements['ride.mspr_unit"+k+"'].value =document.frmParam.elements['ride.mspr_unit"+(k+1)+"'].value;"); 
					eval(" document.frmParam.elements['ride.mspr_class"+k+"'].value = document.frmParam.elements['ride.mspr_class"+(k+1)+"'].value;");			      
					eval(" document.frmParam.elements['ride.mspr_tsi"+k+"'].value = document.frmParam.elements['ride.mspr_tsi"+(k+1)+"'].value;");
					eval(" document.frmParam.elements['ride.mspr_premium"+k+"'].value = document.frmParam.elements['ride.mspr_premium"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['ride.mspr_ins_period"+k+"'].value = document.frmParam.elements['ride.mspr_ins_period"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['mspr_beg_date"+k+"'].value = document.frmParam.elements['mspr_beg_date"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['mspr_end_date"+k+"'].value = document.frmParam.elements['mspr_end_date"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['mspr_end_pay"+k+"'].value = document.frmParam.elements['mspr_end_pay"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['mspr_end_pay"+k+"'].value = document.frmParam.elements['mspr_end_pay"+(k+1)+"'].value;");			
					eval(" document.frmParam.elements['ride.mspr_tt"+k+"'].value = document.frmParam.elements['ride.mspr_tt"+(k+1)+"'].value;");
					eval(" document.frmParam.elements['ride.mspr_tt1"+k+"'].value = document.frmParam.elements['ride.mspr_tt"+(k+1)+"'].value;");
					eval(" document.frmParam.elements['ride.mspr_rate"+k+"'].value = document.frmParam.elements['ride.mspr_rate"+(k+1)+"'].value;");
					eval(" document.frmParam.elements['cek"+k+"'].checked = document.frmParam.elements['cek"+(k+1)+"'].checked;");
				}
				oTable.deleteRow(parseInt(document.getElementById("tablerider").rows.length)-1);
				row=row-1;
				mn = idx;
				i=i-1;
			}
		}
		row=parseInt(document.getElementById("tablerider").rows.length);
		if(row==1) flag_add=0;
	}
}	

function cek_tgl(){
	with(document.frmParam){
		if ((kode_sementara.value=="")||(kode_sementara.value=="0~X0") ){
			alert("Silahkan pilih produk dahulu baru menentukan tanggal mulai berlaku pertanggungan");
		}else{
			listtglpolis(elements['datausulan.mste_beg_date'].value, elements['datausulan.mspr_ins_period'].value,elements['datausulan.plan'].value);
		}
	}
}

 function cpy(hasil){
 	with(document.frmParam){
		elements['datausulan.cara_premi'].value = hasil;
		tanda_premi.value=hasil;
		if (tanda_premi.value == '0'){
			elements['datausulan.kombinasi'].disabled = true;
			elements['datausulan.kombinasi'].value ='';
			elements['datausulan.total_premi_kombinasi'].readOnly= true;		
			elements['datausulan.total_premi_kombinasi'].value = '';
			elements['datausulan.total_premi_kombinasi'].style.backgroundColor ='#D4D4D4';
			elements['datausulan.mspr_premium'].readOnly = false;		
			elements['datausulan.mspr_premium'].style.backgroundColor ='#FFFFFF';
		}else{
			elements['datausulan.kombinasi'].disabled = false;
			elements['datausulan.total_premi_kombinasi'].readOnly = false;		
			elements['datausulan.total_premi_kombinasi'].style.backgroundColor ='#FFFFFF';
			elements['datausulan.mspr_premium'].readOnly = true;		
			elements['datausulan.mspr_premium'].value = '';
			elements['datausulan.mspr_premium'].style.backgroundColor ='#D4D4D4';
		}
 	}
 }	
	
 function ganti(hsl){
 	with(document.frmParam){
	 	if (elements['datausulan.total_premi_kombinasi'].value=='' || elements['datausulan.total_premi_kombinasi'].value == 0){
	 		alert('Masukkan Total Premi dulu baru pilih Kombinasi.');
	 		elements['datausulan.kombinasi'].value='';
	 	}else{
	 		switch (elements['datausulan.kombinasi'].value) {
	 		case "A" : 
				elements['datausulan.mspr_premium'].value = eval(elements['datausulan.total_premi_kombinasi'].value);
				break;
			case "B" : 
				elements['datausulan.mspr_premium'].value = 90/100*eval(elements['datausulan.total_premi_kombinasi'].value);
				break;
	 		case "C" : 
				elements['datausulan.mspr_premium'].value = 80/100*eval(elements['datausulan.total_premi_kombinasi'].value);
				break;
	 		case "D" : 
				elements['datausulan.mspr_premium'].value = 70/100*eval(elements['datausulan.total_premi_kombinasi'].value);
				break;
			 case "E" : 
				elements['datausulan.mspr_premium'].value = 60/100*eval(elements['datausulan.total_premi_kombinasi'].value);
				break;
			 case "F" : 
				elements['datausulan.mspr_premium'].value = 50/100*eval(elements['datausulan.total_premi_kombinasi'].value);
				break;
			 case "G" : 
				elements['datausulan.mspr_premium'].value = 40/100*eval(elements['datausulan.total_premi_kombinasi'].value);
				break;
			 case "H" : 
				elements['datausulan.mspr_premium'].value = 30/100*eval(elements['datausulan.total_premi_kombinasi'].value);
				break;
			 case "I" : 
				elements['datausulan.mspr_premium'].value = 20/100*eval(elements['datausulan.total_premi_kombinasi'].value);
				break;
			 case "J" : 
				elements['datausulan.mspr_premium'].value = 10/100*eval(elements['datausulan.total_premi_kombinasi'].value);
				break;
			case "K" : 
				elements['datausulan.mspr_premium'].value = 95/100*eval(elements['datausulan.total_premi_kombinasi'].value);
				break;	
			case "L" : 
				elements['datausulan.mspr_premium'].value = 85/100*eval(elements['datausulan.total_premi_kombinasi'].value);
				break;		
			case "M" : 
				elements['datausulan.mspr_premium'].value = 75/100*eval(elements['datausulan.total_premi_kombinasi'].value);
				break;	
			case "N" : 
				elements['datausulan.mspr_premium'].value = 65/100*eval(elements['datausulan.total_premi_kombinasi'].value);
				break;	
			case "O" : 
				elements['datausulan.mspr_premium'].value = 55/100*eval(elements['datausulan.total_premi_kombinasi'].value);
				break;
			case "P" : 
				elements['datausulan.mspr_premium'].value = 45/100*eval(elements['datausulan.total_premi_kombinasi'].value);
				break;
			case "Q" : 
				elements['datausulan.mspr_premium'].value = 35/100*eval(elements['datausulan.total_premi_kombinasi'].value);
				break;
			case "R" : 
				elements['datausulan.mspr_premium'].value = 25/100*eval(elements['datausulan.total_premi_kombinasi'].value);
				break;								
			case "S" : 
				elements['datausulan.mspr_premium'].value = 15/100*eval(elements['datausulan.total_premi_kombinasi'].value);
				break;		
			case "T" : 
				elements['datausulan.mspr_premium'].value = 5/100*eval(elements['datausulan.total_premi_kombinasi'].value);
				break;			
	 		}
	 	}
 	}
 }
	
 function Prod(hasil){
	with(document.frmParam){
		deleteallrider('tablerider', '1');
		elements['datausulan.lsbs_id'].value = hasil;
		ajaxSelectWithParam1(hasil,'select_detilprodukutama', 'sideMenu', 'datausulan.plan', 'cmd.datausulan.plan', 'plan', 'lsdbs_name', 'perproduk(this.options[this.selectedIndex].value);');
		elements['datausulan.mspr_ins_period'].value ='';
		elements['datausulan.mspo_pay_period'].value ='';
		elements['datausulan.mspo_installment'].value ='';
	}
}
// -->
</script>
<body bgcolor="ffffff" onLoad="Body_onload(); setupPanes('container1', 'tab1');">
<XML ID=xmlData></XML>
<XML ID=xmlData1></XML>
<XML ID=xmlData2></XML>

<form:form commandName="cmd" name="frmParam" id="frmParam" method="post">

	<!-- Hidden Fields -->
		<c:choose>
			<c:when test="${cmd.pemegang.reg_spaj eq null}"><c:set var="disableFlag" value="disabled"/></c:when>
			<c:otherwise><c:set var="disableFlag" value=""/></c:otherwise>
		</c:choose>
		
		<input type="submit" id="_target0" name="_target0" onclick="next('_target0')" style="display: none;" ${disableFlag}>
		<input type="submit" id="_target1" name="_target1" onclick="next('_target1')" style="display: none;" ${disableFlag}>
		<input type="submit" id="_target2" name="_target2" onclick="next('_target2')" style="display: none;" ${disableFlag}>
		<input type="submit" id="_target3" name="_target3" onclick="next('_target3')" style="display: none;" ${disableFlag}>
		<input type="submit" id="_target4" name="_target4" onclick="next('_target4')" style="display: none;" ${disableFlag}>
		<input type="submit" id="_target5" name="_target5" onclick="next('_target5')" style="display: none;" ${disableFlag}>
	
		<input type="hidden" name="_page" value="${halaman}">
		<input type="hidden" name="hal" value="${halaman}">
		<spring:bind path="cmd.pemegang.indeks_halaman">
			<input type="hidden" name="${status.expression}" value="${halaman-1}">
		</spring:bind>

		<input type="hidden" name="tanggal_lahir_pp" value="<fmt:formatDate value='${cmd.pemegang.mspe_date_birth}' pattern="yyyyMMdd"/>">
		<input type="hidden" name="tanggal_lahir_ttg" value="<fmt:formatDate value='${cmd.tertanggung.mspe_date_birth}' pattern="yyyyMMdd"/>">
	
		<input type="hidden" name="status" value="${cmd.pemegang.status}">
		<input type="hidden" name="kode_sementara">
		<input type="hidden" name="ins" value="${cmd.datausulan.mspr_ins_period}">
		<input type="hidden" name="pay" value="${cmd.datausulan.mspo_pay_period}">
		<input type="hidden" name="kurss" value="${cmd.datausulan.kurs_p}">

		<input type="hidden" name="tanda_premi" value='${cmd.datausulan.cara_premi}'>
		<input type="hidden" name="cb" value="${cmd.datausulan.lscb_id}">

		<form:hidden path="datausulan.jmlrider"/>
		<input type="hidden" name="mn" value="0">
	<!-- End of Hidden Fields -->

	<div id="contents">
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li><a href="#" onclick="document.getElementById('_target0').click(); return false;">Pemegang</a></li>
				<li><a href="#" onclick="document.getElementById('_target1').click(); return false;">Tertanggung</a></li>
				<li><a href="#" onclick="return showPane('pane1', this);" id="tab1" class="tab-active" >Asuransi</a></li>
				<li><a href="#" onclick="document.getElementById('_target3').click(); return false;">Investasi</a></li>
				<li><a href="#" onclick="document.getElementById('_target4').click(); return false;">Agen</a></li>
				<li><a href="#" onclick="document.getElementById('_target5').click(); return false;">Konfirmasi</a></li>
				<li><a href="#" onclick="return false;">Finish</a></li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<table class="entry2">
						<spring:bind path="cmd.*">
							<c:if test="${not empty status.errorMessages}">
								<tr>
									<td colspan="7">
										<div id="error">ERROR:<br>
										<c:forEach var="error" items="${status.errorMessages}">
											- <c:out value="${error}" escapeXml="false" />
											<br />
										</c:forEach>
										</div>
									</td>
								</tr>
							</c:if>
						</spring:bind>	
						<tr>
							<th colspan="7">
								<input type="submit" name="_target${halaman-1}" value="&laquo; Prev" onclick="prev()" accesskey="P" title="Alt-P">
								<input type="submit" name="_target${halaman+1}" value="Next &raquo;" onclick="next()" accesskey="N" title="Alt-N">
							</th>
						</tr>
						<tr>
							<th colspan="7" style="background-color: #D0D0D0">A. Data Pertanggungan dan Premi Asuransi</th>
						</tr>
						<tr>
							<th colspan="7" style="background-color: #D0D0D0">Jenis Asuransi Pokok</th>
						</tr>
						<tr>
							<th>1.</th>
							<th>Produk</th>
							<td>
								<spring:bind path="cmd.datausulan.lsbs_id">
									<c:choose>
										<c:when test="${sessionScope.currentUser.jn_bank eq 0 or sessionScope.currentUser.jn_bank eq 1}">
											<select name="${status.expression }" onChange="ProductConfigPlatinumBII(this.options[this.selectedIndex].value)" tabindex="1"
											<c:if test="${ not empty status.errorMessage}">class="inpError"</c:if>>
										</c:when>
										<c:when test="${sessionScope.currentUser.jn_bank eq 2}">
											<select name="${status.expression }" onChange="ProductConfigBankSinarmas(this.options[this.selectedIndex].value)" tabindex="1"
											<c:if test="${ not empty status.errorMessage}">class="inpError"</c:if>>
										</c:when>
										<c:when test="${sessionScope.currentUser.jn_bank eq 3}">
											<select name="${status.expression }" onChange="ProductConfigSekuritas(this.options[this.selectedIndex].value)" tabindex="1"
											<c:if test="${ not empty status.errorMessage}">class="inpError"</c:if>>
										</c:when>
										<c:otherwise>
											<select name="${status.expression }" onChange="ProductConfig(this.options[this.selectedIndex].value)" tabindex="1"
											<c:if test="${ not empty status.errorMessage}">class="inpError"</c:if>>
										</c:otherwise>
									</c:choose>
									<option value="0">NONE</option>
									<c:forEach var="utama" items="${listprodukutama}">
										<option value="${utama.lsbs_id}"
											<c:if test="${utama.lsbs_id eq cmd.datausulan.lsbs_id}">selected</c:if>>
											${utama.lsbs_name}
										</option>
									</c:forEach>
									</select>
									<font class="error">*</font>
								</spring:bind>							
							</td>
							<th>Jenis Produk</th>
							<td>
								<form:select path="datausulan.tipeproduk" cssStyle="width: 160px;" items="${listtipeproduk}" itemValue="key" itemLabel="value" />
								<font class="error">*</font>
							</td>
							<th>Kode Produk</th>
							<td>
								<form:input path="datausulan.kodeproduk" size="3" maxlength="3" onchange="Prod(this.value)" cssErrorClass="inpError"/>
							</td>
						</tr>
						<tr>
							<th></th>
							<th>Nama Produk</th>
							<td>
								<div id="sideMenu" style="display: inline;"></div>
								<font class="error">*</font>
							</td>
							<th>Klas</th>
							<td>
								<form:input path="datausulan.mspr_class" size="3" maxlength="2" cssErrorClass="inpError"/>
								<font class="error">*</font>
							</td>							
							<th>Cuti Premi</th>
							<td>
								<form:input path="datausulan.mspo_installment" size="3" cssErrorClass="inpError"/>
								<font class="error">*</font>
							</td>
						</tr>
						<tr>
							<th></th>
							<th>Uang Pertanggungan</th>
							<td>
								<div id="kursup" style="display: inline">
								</div>
								<form:input path="datausulan.mspr_tsi" size="25" cssErrorClass="inpError"/>
								<font class="error">*</font>
							</td>
							<th>Lama Tanggung</th>
							<td>
								<form:input path="datausulan.mspr_ins_period" size="3" readonly="true"/>
							</td>
							<th>Lama Bayar</th>
							<td>
								<form:input path="datausulan.mspo_pay_period" size="3" readonly="true"/>
							</th>
						</tr>
						<tr>
							<th colspan="7" style="background-color: #D0D0D0">Premi</th>
						</tr>
						<tr>
							<th></th>
							<th style="text-align: left;" colspan="2">
								<label for="premi_biasa">
									<form:radiobutton id="premi_biasa" path="datausulan.cara_premi" value="0" onclick="cpy('0');" cssClass="noBorder" cssErrorClass="inpError"/>
									Isi Premi
								</label>
								<span class="info">(sesuai dengan cara pembayaran)</span>
							</th>
							<th>
								Premi<br/>Standar/Pokok
							</th>
							<td colspan="2">
								<div id="kurspremi" style="display: inline">
								</div>
								<form:input path="datausulan.mspr_premium" size="25" cssErrorClass="inpError" />
								<font class="error">*</font>
							</td>						
							<th></th>
						</tr>
						<tr>
							<th></th>
							<th style="text-align: left;" colspan="2">
								<label for="premi_persentase">
									<form:radiobutton id="premi_persentase" path="datausulan.cara_premi" value="1" onclick="cpy('1');" cssClass="noBorder" cssErrorClass="inpError"/>
									Isi Total Premi dan Pilih Kombinasi
									<span class="info">(Premi Pokok + Top-Up)</span>
								</label>
							</th>
							<th>
								Total Premi
							</th>
							<td colspan="3">
								<form:input path="datausulan.total_premi_kombinasi" size="23" cssErrorClass="inpError" onchange="ganti(document.frmParam.tanda_premi.value);"/>
								<spring:bind path="cmd.datausulan.kombinasi">
									<select name="${status.expression }" tabindex="7" onChange="ganti(this.options[this.selectedIndex].value);"
										<c:if test="${ not empty status.errorMessage}">class="inpError"</c:if>>
										<c:forEach var="kb" items="${select_kombinasi}">
											<option value="${kb.ID}" <c:if test="${cmd.datausulan.kombinasi eq kb.ID}">selected</c:if>>
												${kb.KOMBINASI}
											</option>
										</c:forEach>
									</select>
								</spring:bind>
							</td>
						</tr>
						<tr>
							<th>2.</th>
							<th>Cara<br/>Pembayaran Premi</th>
							<td>
								<div id="cara_bayar" style="display: inline">
								</div>
								<font class="error">*</font>
							</td>
							<th>Bentuk<br/>Pembayaran Premi</th>
							<td>
								<spring:bind path="cmd.datausulan.mste_flag_cc">
									<select name="${status.expression }" tabindex="7" <c:if test="${ not empty status.errorMessage}">class="inpError"</c:if>>
										<option value="" />
										<c:forEach var="auto" items="${select_autodebet}">
											<option value="${auto.ID}" <c:if test="${cmd.datausulan.mste_flag_cc eq auto.ID}">selected</c:if>>
												${auto.AUTODEBET}
											</option>
										</c:forEach>
									</select>
									<font class="error">*</font>
								</spring:bind>
							</td>
							<th colspan="2"></th>
						</tr>
						<tr>
							<th>3.</th>
							<th>Mulai Berlaku Pertanggungan</th>
							<td>
								<spring:bind path="cmd.datausulan.mste_beg_date">
									<script>inputDate('${status.expression}', '${status.value}', false,'listtglpolis1();');</script>
								</spring:bind>
								<font class="error">*</font>
								<span class="info">(DD/MM/YYYY)</span>
							</td>
							<th>Akhir Berlaku Pertanggungan</th>
							<td colspan="2">
								<spring:bind path="cmd.datausulan.mste_end_date">
									<script>inputDate('${status.expression}', '${status.value}', true);</script>
								</spring:bind>
								<font class="error">*</font>
								<span class="info">(DD/MM/YYYY)</span>
							</td>
							<th></th>
						</tr>
						<tr>
							<th>4.</th>
							<th>% Persentase DPLK</th>
							<td>
								<spring:bind path="cmd.pemegang.mste_pct_dplk">
									<select name="${status.expression }" tabindex="7" <c:if test="${ not empty status.errorMessage}">class="inpError"</c:if>>
										<c:forEach var="dplk" items="${select_dplk}">
											<option value="${dplk.ID}" <c:if test="${cmd.pemegang.mste_pct_dplk eq dplk.ID}">selected</c:if>>
												${dplk.DPLK}
											</option>
										</c:forEach>
									</select>
									<font class="error">*</font>
									<span class="info">(Khusus produk Horison)</span>
								</spring:bind>
							</td>
							<th colspan="4"></th>
						</tr>
						<tr>
							<th colspan="7" style="background-color: #D0D0D0">
								Jenis Asuransi Tambahan (Rider)
								<input name="btnadd1" type="button" id="btnadd1" value="ADD" onClick="addRowDOM1('tablerider', '1')">
								<input name="btn_cancel1" type="button" id="btn_cancel1" value="DELETE" onClick="cancel1()">
							</th>
						</tr>
						<tr>
							<td colspan="7">
								<table id="tablerider" class="displaytag">
									<tr>
										<th>No.</th>
										<th>Nama Produk <font class="error">*</font></th>
										<th>Unit <font class="error">*</font></th>
										<th>Klas <font class="error">*</font></th>
										<th>UP</th>
										<th>Premi</th>
										<th>
											Masa Per-
											<br>
											tanggungan
										</th>
										<th>
											Mulai Berlaku Pertanggungan
											<span class="info">(DD/MM/YYYY)</span>
										</th>
										<th>
											Akhir Berlaku Pertanggungan
											<span class="info">(DD/MM/YYYY)</span>
										</th>
										<th>
											Akhir Pembayaran
											<span class="info">(DD/MM/YYYY)</span>
										</th>
										<th>Tertanggung <font class="error">*</font></th>
										<th>Rate</th>
										<th>
										</th>
									</tr>
									<c:forEach items="${cmd.datausulan.daftaRider}" var="ride" varStatus="status">
										<tr>
											<td>${status.index +1}</td>
											<td>
												<select name="ride.plan_rider${status.index +1}"
													<c:if test="${ride.flag_include eq 1}">style='background-color :#D4D4D4' disabled</c:if>>
													<c:forEach var="rider" items="${select_rider}">
														<option
															<c:if test="${ride.plan_rider eq rider.BISNIS_ID}"> SELECTED </c:if>
															value="${rider.BISNIS_ID}">
															${rider.NAME}
														</option>
													</c:forEach>
												</select>
												<input type="hidden" name="ride.plan_rider1${status.index +1}" value="${ride.plan_rider}">
												<c:if test="${ride.lsbs_id eq 800}">
													<c:if test="${ride.mspr_tsi_pa_a ne 0}">
														<c:if test="${ride.mspr_tsi_pa_a ne null}"> Jenis Resiko A </c:if>
													</c:if>
													<c:if test="${ride.mspr_tsi_pa_b ne 0}">
														<c:if test="${ride.mspr_tsi_pa_b ne null}">- B </c:if>
													</c:if>
													<c:if test="${ride.mspr_tsi_pa_c ne 0}">
														<c:if test="${ride.mspr_tsi_pa_c ne null}"> - C </c:if>
													</c:if>
													<c:if test="${ride.mspr_tsi_pa_d ne 0}">
														<c:if test="${ride.mspr_tsi_pa_d ne null}"> - D </c:if>
													</c:if>
													<c:if test="${ride.mspr_tsi_pa_m ne 0}">
														<c:if test="${ride.mspr_tsi_pa_m ne null}"> - M </c:if>
													</c:if>
												</c:if>
											</td>
											<td>
												<input type="text" name='ride.mspr_unit${status.index +1}' value='${ride.mspr_unit}' size="3"
													<c:if test="${ride.flag_include eq 1}">style='background-color :#D4D4D4' readOnly</c:if> maxlength="2">
											</td>
											<td>
												<input type="text" name='ride.mspr_class${status.index +1}' value='${ride.mspr_class}' size="3"
													<c:if test="${ride.flag_include eq 1}">style='background-color :#D4D4D4' readOnly</c:if> maxlength="2">
											</td>
											<td>
												<input type="text" name='ride.mspr_tsi${status.index +1}' size="15" value=<fmt:formatNumber type='number' value='${ride.mspr_tsi}'/>
													style='background-color :#D4D4D4' readOnly>
											</td>
											<td>
												<input type="text" name='ride.mspr_premium${status.index +1}' size="15" value=<fmt:formatNumber type='number' value='${ride.mspr_premium}'/>
													style='background-color :#D4D4D4' readOnly>
											</td>
											<td>
												<input type="text" name='ride.mspr_ins_period${status.index +1}'
													value='${ride.mspr_ins_period}' size="5" style='background-color :#D4D4D4' readOnly>
											</td>
											<td nowrap>
												<input type="text" name='mspr_beg_date${status.index +1}'
													value='<fmt:formatDate value="${ride.mspr_beg_date}" pattern="dd/MM/yyyy"/>'
													size="12" style='background-color :#D4D4D4' readOnly>
											</td>
											<td nowrap>
												<input type="text" name='mspr_end_date${status.index +1}'
													value='<fmt:formatDate value="${ride.mspr_end_date}" pattern="dd/MM/yyyy"/>'
													size="12" style='background-color :#D4D4D4' readOnly>
											</td>
											<td nowrap>
												<input type="text" name='mspr_end_pay${status.index +1 }'
													value='<fmt:formatDate value="${ride.mspr_end_pay}" pattern="dd/MM/yyyy"/>'
													size="12" style='background-color :#D4D4D4' readOnly>
											</td>
											<td nowrap>
												<select name="ride.mspr_tt${status.index +1}"
													<c:if test="${ride.flag_include eq 1}">style='background-color :#D4D4D4' disabled</c:if>>
													<c:forEach var="insrider" items="${select_insrider}">
														<option <c:if test="${ride.mspr_tt eq insrider.ID}"> SELECTED </c:if> value="${insrider.ID}">
															${insrider.INSRIDER}
														</option>
													</c:forEach>
												</select>
												<input type="hidden" name="ride.mspr_tt1${status.index +1}" value="${ride.mspr_tt}">
											</td>
											<td>
												<input type="text" name='ride.mspr_rate${status.index +1}' value=<fmt:formatNumber type='number' value='${ride.mspr_rate}'/>
													size="10" style='background-color :#D4D4D4' readOnly>
											</td>
											<td>
												<input type=checkbox class="noBorder" name="cek${status.index +1}" id="ck${status.index +1}"
													<c:if test="${ride.flag_include eq 1}">style='background-color :#D4D4D4' disabled</c:if>>
												<input type="hidden" name="ride.flag_include${status.index +1}" value='${ride.flag_include}' size="3">
											</td>
										</tr>
										</c:forEach>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="7">
									<font class="error">Note: Untuk pecahan decimal, harap menggunakan titik.</font>
								</td>
							</tr>
							<tr>
								<th colspan="7" style="background-color: #D0D0D0">&nbsp;</th>
							</tr>
							<tr>
								<th colspan="7">
									<input type="submit" name="_target${halaman-1}" value="&laquo; Prev" onclick="prev()" accesskey="P" title="Alt-P">
									<input type="submit" name="_target${halaman+1}" value="Next &raquo;" onclick="next()" accesskey="N" title="Alt-N">
								</th>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>

</form:form>
<script>
	hideLoadingMessage();
</script>
</body>
<%@ include file="/include/page/footer.jsp"%>
