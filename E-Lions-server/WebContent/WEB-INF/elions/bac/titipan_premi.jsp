<%@ include file="/include/page/header.jsp"%>
<%@ page import="com.ekalife.elions.model.User" %>

<script type="text/javascript">
	function buttonLinks(str){
			window.open("${path}/uw/uw.htm?window=showValidasiTtd&spaj="+document.frmParam.reg_spaj.value,"","toolbar=no, width=400, height=150, resizable=no, scrollbar=no");
//  		if(str =="print"){
//  			if(confirm('Apakah tanda tangan sudah dicek dan sesuai dengan bukti identitas ?')){
//  				popWinToolbar("../report/uw.pdf?window=titipanpremi&show=pdf&user="+document.frmParam.user.value+"&spaj="+document.frmParam.reg_spaj.value+"&ttdjenis="+1, 500, 800);
//  			}else{
//				popWinToolbar("../../ReportServer/?rs=BAC/Suratpermohonanterbitpolis&new=true&1spaj="+document.frmParam.reg_spaj.value+"&cetak=true&1user="+document.frmParam.user.value, 500, 800);	
//  				popWinToolbar("../report/uw.pdf?window=titipanpremi&show=pdf&user="+document.frmParam.user.value+"&spaj="+document.frmParam.reg_spaj.value+"&ttdjenis="+0, 500, 800);	
//  			}		
//  		}
	}
	
	function ubah(elm, nama, _nama, nilai){
		if(elm.value.length==5){
			//ajaxSelectWithParam(elm.value, 'selectBankEkaLife', _nama, nama, nilai, 'LSREK_ID', 'NAMA', '');
			ajaxSelectWithParam(elm.value, 'selectBankEkaLife2', _nama, nama, nilai, 'LSREK_ID', 'NAMA', '');
			if(elm.value == "00000"){
				ajaxSelectWithParam2(document.frmParam.submitType.value,'0','selectPayType0','carabayar','cara_bayar', "${cmd.cara_bayar}", 'LSJB_ID', 'LSJB_TYPE', 'merchantDisplay(this.value)');
			}else{
				ajaxSelectWithParam2(document.frmParam.submitType.value,'1','selectPayType0','carabayar','cara_bayar', "${cmd.cara_bayar}", 'LSJB_ID', 'LSJB_TYPE', 'merchantDisplay(this.value)');
			}
		//document.frmParam.temporary.value = document.frmParam.ucup.value ;
		}
	}

	function ubah_awal(elm, nama, _nama, nilai){
		ajaxSelectWithParam(elm.value, 'selectBankEkaLife2', _nama, nama, nilai, 'LSREK_ID', 'NAMA', '');
		if(elm.value == ""){
			ajaxSelectWithParam2(document.frmParam.submitType.value,'0','selectPayType0','carabayar','cara_bayar', "${cmd.cara_bayar}", 'LSJB_ID', 'LSJB_TYPE', 'merchantDisplay(this.value)');
		}else{
 			ajaxSelectWithParam2(document.frmParam.submitType.value,'1','selectPayType0','carabayar','cara_bayar', "${cmd.cara_bayar}", 'LSJB_ID', 'LSJB_TYPE', 'merchantDisplay(this.value)');
		}
		document.frmParam.ucup.value = document.frmParam.temporary.value;
	}
	
	function pp(nopol,np){
		var bn = document.frmParam.ucup.value;
		var nopol = nopol.value;
		if(bn.length==5){
			if(bn="00000"){
				ajaxManager.listnamapp(nopol,
					{callback:function(m){
						DWRUtil.useLoadingMessage();		
						np.value= m.nama_pemegang;	
						
						if(m.nama_pemegang=="") alert("Polis tidak terdaftar!");
					},
					timeout:180000,
					errorHandler:function(message){
						alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
				});
			}
		}
	}
	
	function getdataRkCc(noSpaj){
		var spaj = noSpaj.value;
		ajaxManager.listdataRkCc(spaj,
			{callback:function(data){
				DWRUtil.useLoadingMessage();
					
				var lima = data.norek_ajs.substr(data.norek_ajs.length-5);
				document.frmParam.ucup.value = lima;
				document.frmParam.ucup.onkeyup();
				var flag = "";
				if( data.flag_tg == 0 )
				{
					flag = "Tunggal";
				}
				else if( data.flag_tg == 1 )
				{
					flag = "Gabungan";
				}
				else
				{
					flag = "";
				}
				document.frmParam.tipe.value = flag;
				document.frmParam._tgl_rk.value = data.tgl_rk;
				document.frmParam.tgl_rk.value = data.tgl_rk;
				document.frmParam.keterangan.value = 'CEK IBANK (No Transaksi ' + data.no_trx + ')';	
				document.frmParam.no_trx.value = data.no_trx;
				document.frmParam.kurs.value = data.kurs;
				document.frmParam.jml_bayar.value = data.jml_bayar;
				document.frmParam.nilai_kurs.value = data.nilai_kurs;
			},
			timeout:180000,
			errorHandler:function(message){
				if(message == "null"){
					alert("Data Rekening Kartu Kredit Tidak Ada");
				}else{
					alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah.");
				}
			}
		});
		//alert("Nilai = "+document.frmParam.mste_flag_cc.value);
		//if(document.frmParam.data_cc.value == '1')
		ajaxSelectWithParam2(document.frmParam.submitType.value,'3','selectPayType0','carabayar','cara_bayar', "${cmd.cara_bayar}", 'LSJB_ID', 'LSJB_TYPE', 'merchantDisplay(this.value)');
	}
	
	function getdataRkVa(noSpaj){
		var spaj = noSpaj.value;
		ajaxManager.listdataVirtualAccount(spaj,
			{callback:function(data){
				DWRUtil.useLoadingMessage();
				var premi_sisa= data.premi_sisa;
				if(data.jml_bayar<= premi_sisa){
					var lima = data.norek_ajs.substr(data.norek_ajs.length-5);
					document.frmParam.ucup.value = lima;
					document.frmParam.ucup.onkeyup();
					var flag = "";
					if( data.flag_tg == 0 )
					{
						flag = "Tunggal";
					}
					else if( data.flag_tg == 1 )
					{
						flag = "Gabungan";
					}
					else
					{
						flag = "";
					}
					document.frmParam.tipe.value = flag;
					document.frmParam._tgl_rk.value = data.tgl_rk;
					document.frmParam.tgl_rk.value = data.tgl_rk;
					document.frmParam.keterangan.value = 'CEK IBANK (No Transaksi ' + data.no_trx + ')';	
					document.frmParam.no_trx.value = data.no_trx;
					document.frmParam.kurs.value = data.kurs;
					document.frmParam.jml_bayar.value = data.jml_bayar;
					document.frmParam.nilai_kurs.value = data.nilai_kurs;
					
				}else{
					alert ('Saldo ibank ini tidak cukup');
					return;
				}
			},
			timeout:180000,
			errorHandler:function(message){
				if(message == "null"){
					alert("Data Virtual Account Untuk SPAJ Ini Tidak Ada");
				}else{
					alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah.");
				}
			}
		});
	}
	
	function setPremi(parameter){
		var spaj = document.frmParam.reg_spaj.value;
		ajaxManager.getPremiPokokTopup(spaj,parameter,
			{callback:function(data){
				DWRUtil.useLoadingMessage();
				if(data.premi != null) document.getElementById("premi").value = data.premi;
// 	 			document.frmParam.msdp_flag_topup.value = data.flagtopup;
				document.getElementById("flag_tu").value = data.flagtopup;
			},
			timeout:180000,
			errorHandler:function(message){
				if(message == "null"){
					alert("Data Tidak Ada");
				}else{
					alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah.");
				}
			}
		});
	}
	
	function merchantDisplay(param){
		if(param == 3){
			document.getElementById("list_merchant").style.display = "";
		}else{
			document.getElementById("list_merchant").style.display = "none";
			document.frmParam.msdp_flag_merchant.value = "";
		}
	}
</script>

<script type="text/javascript">

	if(!document.layers&&!document.all&&!document.getElementById)
		event="test";
		
	function showtip(current,e,text){
		if(document.all||document.getElementById){
			thetitle=text.split('<br>')
			if(thetitle.length>1){
				thetitles=''
				
				for(i=0;i<thetitle.length;i++)
				thetitles+=thetitle[i]
				
				current.title=thetitles
			}
		else
			current.title=text
		}
	
		else if (document.layers){
			document.tooltip.document.write('<layer bgColor="white" style="border:1px solid black;font-size:13px;">'+text+'</layer>')
			document.tooltip.document.close()
			document.tooltip.left=e.pageX+5
			document.tooltip.top=e.pageY+5
			document.tooltip.visibility="show"
		}
	}
	
	function hidetip(){
		if(document.layers)
			document.tooltip.visibility="hidden"
	}
	
	function Body_onload(){
		getkurs(document.frmParam.kurs.value, document.frmParam.tgl_rk.value,'<%=request.getAttribute("tglsekarang")%>', document.frmParam.kurs, document.frmParam.nilai_kurs);	
		document.getElementById("load").style.display = "none"; 
// 		alert(document.frmParam.bk.value);
		if(document.frmParam.statussubmit.value == "1"){
			alert("Proses input data titipan premi telah berhasil dilakukan.");
			document.frmParam.statussubmit.value = "0";
		}else if (document.frmParam.statussubmit.value == "2"){
			alert("Proses edit data titipan premi telah berhasil dilakukan.");
			document.frmParam.statussubmit.value = "0";	
		}else if (document.frmParam.statussubmit.value == "3"){
			alert("Proses delete data titipan premi telah berhasil dilakukan.");
			document.frmParam.statussubmit.value = "0";	
		}

		if(document.frmParam.bk.value=="0"){
			document.frmParam.ucup.value="00000";
		}

		if(document.frmParam.ucup.value !=""){
			if(document.frmParam.ucup.value.length==5){
				if(document.frmParam.ucup.value == "00000"){
					ajaxSelectWithParam2("${cmd.submitType}",'0','selectPayType0','carabayar','cara_bayar', document.frmParam.cara_bayar_sementara.value, 'LSJB_ID', 'LSJB_TYPE', 'merchantDisplay(this.value)');
				}else{
					ajaxSelectWithParam2("${cmd.submitType}",'1','selectPayType0','carabayar','cara_bayar', document.frmParam.cara_bayar_sementara.value, 'LSJB_ID', 'LSJB_TYPE', 'merchantDisplay(this.value)');
				}
				//alert("");
				/*if ((document.frmParam.stss.value=="INSERT") || (document.frmParam.stss.value=="EDIT") )
				{
					enableDisableAll(document.frmParam, false);
				}else{
					enableDisableAll(document.frmParam, true);
				}*/
						
			}
		}

		if(document.getElementById("cara_bayar_sementara").value == 3){
			document.getElementById("list_merchant").style.display = "";
		}else{
			document.getElementById("list_merchant").style.display = "none";
			document.frmParam.msdp_flag_merchant.value = "";
		}

		var pesanError = '${pesanError}';
		if(pesanError != ''){
			alert(pesanError);
			window.close();
		}
	}
</script>

<style type="text/css">
	#load
	{
	    background:url(opacity.png);
	    width:100%;
	    height:2000px;
	    z-index:11;
	    position:absolute;
	    top:75px;
	    left:0px;
	    text-align:center;
	}
</style>

<body onload="Body_onload();">
	<% User currentUser = (User)session.getAttribute("currentUser");%>
	<div id="load">   
		<img  src="${path}/include/image/loading.gif" alt="loading" />
        <br /><br />
		<h3>Loading Please Wait....</h3>       
    </div>
 	<div id="content">
		<form name="frmParam" method="post" onReset="return confirm('Reset Form Input?')">
			<input type="hidden" name="user" value="<%=currentUser.getName()%>">
			<input type="hidden" name="userid" value="<%=currentUser.getLus_id()%>">
			<input type="hidden" name="premi" id="premi" value="${premi}">
			<input type="hidden" name="flag_tu" id="flag_tu" value="${cmd.msdp_flag_topup}">
<!-- 			<spring:bind path="cmd.msdp_flag_topup">  -->
<!-- 				<input type="hidden" name="${status.expression}" value='${status.value}' style='background-color :#D4D4D4'> -->
<!-- 			</spring:bind> -->
	  		<span class="subtitle">Titipan Premi</span> 
	  		<table width="83%" >
				<c:if test="${not empty freepayment}"> 
			    	<tr>
			    		<td></td>
			    		<td>
							<div id="error">
								SPAJ ini merupakan bagian dari promo Free SMiLe Medical.<br>
								Silahkan input pembayaran dengan memilih cara bayar Promotion Budget.<br>
								Jumlah premi yang ditanggung adalah sebesar ${freepayment}.
							</div>
						</td>
			    	</tr>
				</c:if>
		    	<tr> 
		      		<td valign="top"> 
		        		<table class="simple">
			          		<thead> 
					        	<tr> 
					            	<th colspan="3">Titipan Ke</th>
					          	</tr>
			          		</thead>
			          		<tbody>
			          		<c:forEach var="pke" items="${premike}" varStatus="st">
					          	<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	
												onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
												onClick="window.location='${path }/ttppremi/titipan_premi.htm?editSPAJ=${pke.REG_SPAJ}&ke=${pke.MSDP_NUMBER}&sts=update';"> 
					            	<td align="right" colspan="3"> ${st.count} </td>
					          	</tr>
			          		</c:forEach>
			          		</tbody> 
		        		</table>
						<div class="info">* Click for Detail</div>
					</td>
					
					<td valign="top"> 
						<fieldset>
		        			<legend>Bank AJ Sinarmas</legend>
					 		No Spaj : 
					 		<input type="kode_spaj" id="noSpaj" value ="<elions:spaj nomor='${cmd.reg_spaj}'/>" style='background-color :#D4D4D4' readOnly>
							<spring:bind path="cmd.reg_spaj"> 
								<input type="hidden" name="${status.expression}" value='${status.value }' style='background-color :#D4D4D4' readOnly>
							</spring:bind>
		        	 		Jenis Nasabah :  
		        	 		<spring:bind path="cmd.to"> 
								<input type="text" name="${status.expression}" value='${status.value }' size="35" style='background-color :#D4D4D4' readOnly>			
							</spring:bind>
							No Blanko : 
							<input type="text" readonly="readonly" style="background-color: #D4D4D4;" value="${blankon}">
							<spring:bind path="cmd.submitType"> 
								<input type="hidden" name="${status.expression}" value='${status.value }' style='background-color :#D4D4D4' readOnly>
							</spring:bind>
							<spring:bind path="cmd.no_ke"> 
								<input type="hidden" name="${status.expression}" value='${status.value }' style='background-color :#D4D4D4' readOnly>
							</spring:bind>
							<spring:bind path="cmd.statussubmit"> 
			              		<input type="hidden" name="${status.expression}"
										value="${status.value }"  size="30" maxlength="10" tabindex="7"  style='background-color :#D4D4D4' readOnly>
			              	</spring:bind>
					
		          			<table class="entry" width="717">
					            <tr> 
					              	<th width="137">Nama Bank <br>(5 digit terakhir)</th>
					              	<th colspan="3"><input type="hidden" name="bk" value="${cmd.bank}">
									  	<spring:bind path="cmd.bank"> 
											<input type="hidden" name="temporary" value = "${cmd.kode}" >
											<input id="ucup" type="text" value="${cmd.acc_no}" size="5" maxlength="5" onkeyup="ubah(this, '${status.expression}', '_${status.expression}', '${status.value}');" >
											<div id="_${status.expression}">
												<select name="${status.expression}" ></select>
											</div>
											<c:if test="${not empty cmd.kode}">
												<script>
													ubah_awal(document.frmParam.ucup, '${status.expression}', '_${status.expression}', '${cmd.acc_no}');
												</script>
											</c:if>
						              	</spring:bind>
					              	</th>
					            </tr>
					            <tr> 
						            <th width="137">Tanggal RK</th>
						            <th width="260">
										<spring:bind path="cmd.tgl_rk"> 
							            	<script>inputDate('${status.expression}', '${status.value}', false);</script>
							              	<c:if test="${ not empty status.errorMessage}"><a href="#"><img src="${path}/include/image/false.gif" width="11" height="11"  border="0" onMouseOver="showtip(this,event,'${status.errorMessage }')" onMouseOut="hidetip()" onClick="showtip(this,event,'${status.errorMessage }')"></a> 
							              	</c:if>			  
										</spring:bind> 
								 	</th>
					              
					            	<th width="121">No. KTPP</th>
					              	<th width="256">
						              	<spring:bind path="cmd.no_kttp"> 
						                <input type="text" name="${status.expression }"
												value="${status.value }">
						             	<c:if test="${ not empty status.errorMessage}"><a href="#"><img src="${path}/include/image/false.gif" width="11" height="11"  border="0" onMouseOver="showtip(this,event,'${status.errorMessage }')" onMouseOut="hidetip()" onClick="showtip(this,event,'${status.errorMessage }')"></a> 
						              	</c:if>						
						                </spring:bind>
					                </th>
					            </tr>
					            <tr>
					            	<th colspan="2">
										<input type="button" id="rktgl" value="Tarik Data Berdasarkan Tanggal RK" onclick="popWin('${path}/uw/uw.htm?window=drek&startDate='+document.frmParam.tgl_rk.value+'&endDate='+document.frmParam.tgl_rk.value+'&noSpaj='+document.frmParam.reg_spaj.value+'&startNominal=${premi}'+'&endNominal=${premi}'+'&flagHalaman=1', 600, 999);">           	
					            	</th>
					            	<th width="121">Tipe</th>
					              	<th width="256">
						              	<spring:bind path="cmd.tipe"> 
						                	<input type="text" name="${status.expression }"
													value="${status.value }"  style='background-color :#D4D4D4' readOnly>
						             		<c:if test="${ not empty status.errorMessage}"><a href="#"><img src="${path}/include/image/false.gif" width="11" height="11"  border="0" onMouseOver="showtip(this,event,'${status.errorMessage }')" onMouseOut="hidetip()" onClick="showtip(this,event,'${status.errorMessage }')"></a> 
						              		</c:if>						
						                </spring:bind>
						            </th>
					            </tr>
					            <tr>
					            	<th colspan="2">
					            		<input type="button" id="rkcc" value="Tarik Data Berdasarkan CC" onClick="getdataRkCc(noSpaj)" >
					            	</th>
					            	<th colspan="2">
					            		<input type="button" id="rkva" value="Tarik Data Berdasarkan VA" onClick="getdataRkVa(noSpaj)" >
					            	</th>
					            </tr>
					            <tr>
					                <th>User Input</th>
					                <th colspan="3">
					                    <spring:bind path="cmd.lus_login_name">
					                        <input type="text" name="${status.expression}" style="background-color: #d4d4d4" readOnly 
					                            	value="${status.value}">
					                    </spring:bind>
					                </th>
					            </tr>
		          			</table>
					        <table class="entry" width="717">
					            <tr> 
					              	<th width="79">Tanggal Bayar</th>
					              	<th colspan="5">
								  		<spring:bind path="cmd.tgl_bayar"> 
					              			<script>inputDate('${status.expression}', '${status.value}', false);</script>
					              			<c:if test="${ not empty status.errorMessage}"><a href="#"><img src="${path}/include/image/false.gif" width="11" height="11"  border="0" onMouseOver="showtip(this,event,'${status.errorMessage }')" onMouseOut="hidetip()" onClick="showtip(this,event,'${status.errorMessage }')"></a> 
					              			</c:if>			  
										</spring:bind> 			  
									</th>
					              	<th width="136">Jenis TTP</th>
					              	<th width="214">
						              	<spring:bind path="cmd.jenis_ttp"> 
						                	<select name="jenis_ttp">
						                  		<option value="1">Titipan Premi</option>
						                	</select>
						                </spring:bind>
						            </th>
					            </tr>
		            			<tr> 
									<th width="79">Cara Bayar</th>
						            <th colspan="5"><input type="hidden" id="cara_bayar_sementara" name="cara_bayar_sementara" value ="${cmd.cara_bayar}">
										<spring:bind path="cmd.cara_bayar"> 
											<div id="carabayar">
											</div>
											<c:if test="${ not empty status.errorMessage}"><a href="#"><img src="${path}/include/image/false.gif" width="11" height="11"  border="0" onMouseOver="showtip(this,event,'${status.errorMessage }')" onMouseOut="hidetip()" onClick="showtip(this,event,'${status.errorMessage }')"></a> 
						              		</c:if>
						                </spring:bind>				
									</th>
						            <th width="136" >Ref Polis No<br><br>Nama Pemegang Polis</th>
						            <th width="214">
						              	<spring:bind path="cmd.ref_polis_no"> 
							                <input id="nopol" type="text" name="${status.expression }" value="${status.value }" onchange="pp(nopol,np)">
											<c:if test="${ not empty status.errorMessage}"><a href="#">
												<img src="${path}/include/image/false.gif" width="11" height="11"  border="0" onMouseOver="showtip(this,event,'${status.errorMessage }')" onMouseOut="hidetip()" onClick="showtip(this,event,'${status.errorMessage }')"></a> 
											</c:if>						
						                </spring:bind><br>
						              	<spring:bind path="cmd.mcl_first"> 
						              		<input id="np" type="text" name="${status.expression}" value="${cmd.mcl_first}" style='background-color :#D4D4D4' readOnly>
						                	<c:if test="${ not empty cmd.ref_polis_no}">
												<script>
													pp(nopol,np);
												</script>
											</c:if>             								
										</spring:bind>
		              				</th>
		            			</tr>
		            			
					            <tr id="list_merchant"> 
									<th width="79">Merchant</th>
					              	<th colspan="5">
						              	<spring:bind path="cmd.msdp_flag_merchant">
						                	<select name="${status.expression}">
						                  		<option value="" />
							                  	<c:forEach var="fee" items="${listMerchant}">
							                  		<option value="${fee.ID_MERCHANT}"
														<c:if test="${fee.ID_MERCHANT eq status.value}">selected</c:if>>${fee.NAMA}
								                  	</option>
							                  	</c:forEach>
						                	</select>
						             		<c:if test="${ not empty status.errorMessage}"><a href="#"><img src="${path}/include/image/false.gif" width="11" height="11"  border="0" onMouseOver="showtip(this,event,'${status.errorMessage }')" onMouseOut="hidetip()" onClick="showtip(this,event,'${status.errorMessage }')"></a> 
						              		</c:if>
						              	</spring:bind>
					              	</th>
						            <th width="136" ></th>
						            <th width="214"></th>
					            </tr>
					            
					            <tr style="display: ${jenisDisplay}"> 
									<th width="79">Jenis Transaksi </th>
					              	<th colspan="5">
						              	<spring:bind path="cmd.jenis">
						                	<select name="${status.expression}" onchange="setPremi(this.value)">
						                  		<option value="" />
							                  	<c:forEach var="jenis" items="${cmd.jenisTransaksiList}">
							                  		<option value="${jenis.tahunKeAndPremiKe}"
							                  			<c:if test="${jenis.tahunKeAndPremiKe eq status.value}">selected</c:if>>${jenis.descr}
							                  		</option>
							                  	</c:forEach>
						                	</select>
						             		<c:if test="${ not empty status.errorMessage}"><a href="#"><img src="${path}/include/image/false.gif" width="11" height="11"  border="0" onMouseOver="showtip(this,event,'${status.errorMessage }')" onMouseOut="hidetip()" onClick="showtip(this,event,'${status.errorMessage }')"></a> 
						              		</c:if>
						              	</spring:bind>
					              	</th>
						            <th width="136" ></th>
						            <th width="214"></th>
					            </tr>
				            	<tr> 
						            <th width="79">Jumlah Bayar</th>
						            <th colspan="5">
							            <spring:bind path="cmd.kurs"> 
					                		<select name="${status.expression }" onChange="getkurs(this.value, tgl_rk.value,'<%=request.getAttribute("tglsekarang")%>', document.frmParam.kurs, document.frmParam.nilai_kurs)">
							                  	<option value="" />
							                  	<c:forEach var="pke" items="${listKurs}">
								                  	<option value="${pke.LKU_ID }"
														<c:if test="${pke.LKU_ID eq status.value}">selected</c:if>>${pke.LKU_SYMBOL }
								                  	</option>
							                  	</c:forEach>
						                	</select>
				                		</spring:bind>
				                		<spring:bind path="cmd.jml_bayar"> 
							                <input type="text" name="${status.expression }"
													value="${status.value }">
							             	<c:if test="${ not empty status.errorMessage}"><a href="#"><img src="${path}/include/image/false.gif" width="11" height="11"  border="0" onMouseOver="showtip(this,event,'${status.errorMessage }')" onMouseOut="hidetip()" onClick="showtip(this,event,'${status.errorMessage }')"></a> 
							              	</c:if>						
				                		</spring:bind>
				                	</th>
						            <th width="136">Nilai Kurs</th>
						            <th width="214">
						            	<spring:bind path="cmd.nilai_kurs"> 
							                <input type="text" name="${status.expression }"
													value="${status.value }" disabled="disabled">
											<c:if test="${ not empty status.errorMessage}"><a href="#"><img src="${path}/include/image/false.gif" width="11" height="11"  border="0" onMouseOver="showtip(this,event,'${status.errorMessage }')" onMouseOut="hidetip()" onClick="showtip(this,event,'${status.errorMessage }')"></a> 
							              	</c:if>
						                </spring:bind>
				                	</th>
				            	</tr>
		            			<tr> 
						            <th width="79">Keterangan</th>
						            <th colspan="7">
						            	<spring:bind path="cmd.keterangan"> 
							                <textarea cols="64" rows="5" name="${status.expression }">${status.value }</textarea>
							                <font class=error>${status.errorMessage }</font>
						                </spring:bind>
										<spring:bind path="cmd.no_trx">
											<br/><input type="text" name="${status.expression}" value="${status.value}" readonly="readonly" class="readOnly" width="10">
										</spring:bind>
					              	</th>
		            			</tr>
		          			</table>
		        		</fieldset>
				        <fieldset> 
					        <legend>Detail Pembayaran</legend>
			          		<table class="entry" width="715">
					            <tr> 
					              	<th width="259">Client Bank</th>
					              	<th width="444">
					              		<spring:bind path="cmd.client_bank"> 
					                		<select name="${status.expression }">
						                  		<option value="" />
							                  	<c:forEach var="bank" items="${listBankPusat}">
							                  		<option value="${bank.LSBP_ID}"
													<c:if test="${bank.LSBP_ID eq status.value}">selected</c:if>>${bank.LSBP_NAMA}</option>
												</c:forEach>
					                		</select>
					             			<c:if test="${ not empty status.errorMessage}"><a href="#"><img src="${path}/include/image/false.gif" width="11" height="11"  border="0" onMouseOver="showtip(this,event,'${status.errorMessage }')" onMouseOut="hidetip()" onClick="showtip(this,event,'${status.errorMessage }')"></a> 
					              			</c:if>
					                	</spring:bind>
					                </th>
					            </tr>
					            <tr> 
						            <th width="259">No Giro / Cek / Credit Card</th>
						            <th width="444">
							            <spring:bind path="cmd.no_rek"> 
						                	<input type="text" name="${status.expression }"
													value="${status.value }">
						             		<c:if test="${ not empty status.errorMessage}"><a href="#"><img src="${path}/include/image/false.gif" width="11" height="11"  border="0" onMouseOver="showtip(this,event,'${status.errorMessage }')" onMouseOut="hidetip()" onClick="showtip(this,event,'${status.errorMessage }')"></a> 
						              		</c:if>						
						                </spring:bind>
					                </th>
					            </tr>
					            <tr> 
					              	<th width="259">Tanggal Jatuh Tempo</th>
					              	<th width="444">
					 		   			<spring:bind path="cmd.tgl_jatuh_tempo"> 
					              			<script>inputDate('${status.expression}', '${status.value}', false);</script>
										  	<c:if test="${ not empty status.errorMessage}"><a href="#"><img src="${path}/include/image/false.gif" width="11" height="11"  border="0" onMouseOver="showtip(this,event,'${status.errorMessage }')" onMouseOut="hidetip()" onClick="showtip(this,event,'${status.errorMessage }')"></a> 
							              	</c:if>
										</spring:bind> 			  
									</th>
					            </tr>
					            <tr> 
					              	<th width="259">Total Pembayaran</th>
					              	<th width="444">
						              	<spring:bind path="cmd.total_bayar"> 
						                	<input type="text" name="${status.expression }"
													value="${status.value }" disabled="disabled">
						                </spring:bind>
					                </th>
					            </tr>
		          			</table>
		        		</fieldset>
		        			
						<div id="buttons" style="margin: 5px 0px 5px 0px;"> 
							<c:forEach var="dok" items="${userOut}" >							
									<c:if test="${currentUser.getLus_id() == dok.LUS_ID}">
										<input type="button" value="Add" disabled="true">
										<input type="reset" value="Reset Form" disabled="true">
										<input type="button" value="Edit" disabled="true">
										<input type="submit" value="Save" disabled="true">
										<input type="button" value="Cancel" disabled="true">
										<input type="submit" value="Delete" disabled="true">
										<c:set var="ussr" value="1"/>
									</c:if>
							</c:forEach>
							
							<c:if test="${ussr != 1}">
								<input type="button" name="add" value="Add" onclick="addklik();">
								<!--"showHideMenu('hideAble');changeCaption(this, 'Add Payment', 'Cancel Payment');enableDisable(document.frmParam.edit); enableDisable(document.frmParam.rst);enableDisable(document.frmParam.save);enableDisableAll(document.frmParam, false);window.location='${path }/ttppremi/titipan_premi.htm?sts=insert&editSPAJ='+document.frmParam.reg_spaj.value">-->
								<input type="reset" name="rst" value="Reset Form" disabled="true">
								<input type="button" name="edit" value="Edit" onclick="editklik();">
								<!--"enableDisable(document.frmParam.add);enableDisable(document.frmParam.edit); enableDisable(document.frmParam.rst);enableDisable(document.frmParam.save);enableDisableAll(document.frmParam, false);" disabled="true">-->
								<input type="submit" value="Save" name="save" id="save" onclick="return saveklik();" disabled="true">
								<input type="button" name="cancel" value="Cancel" onclick="cancelklik();">
								<input type="submit" name="delete" value="Delete" onclick="return deleteklik();">
								<!--"showHideMenu('hideAble');enableDisable(document.frmParam.edit); enableDisable(document.frmParam.rst);enableDisable(document.frmParam.save);enableDisableAll(document.frmParam, false)">-->
							</c:if>
							
							<input type="button" value="Permohonan Terbit Polis" name="print" onclick="return buttonLinks('print');" > 
							<spring:bind path="cmd.*">
								<c:if test="${not empty status.errorMessages}"> 
									<div id="error"> PERHATIAN:<br>
										<c:forEach var="error" items="${status.errorMessages}"> - <c:out value="${error}" escapeXml="false" /> 
											<br />
										</c:forEach>
									</div>
								</c:if>
							</spring:bind>
						</div>		          	
		      		</td>
		      	</tr>
			</table>
		</form>
	</div> 
</body>

<script>
	function alert1(){
// 		alert("Flag TU " + document.getElementById("flag_tu").value + " dan " + "MSDPTU " + document.frmParam.msdp_flag_topup.value);
		alert("Flag TU " + document.getElementById("flag_tu").value);
	}
	
	function saveklik(){
		document.getElementById("load").style.display = 'block';
	    document.getElementById("content").style.display = 'none';
		return confirm('Simpan ?');	
	}
	
	function addklik(){
		enableDisableAll(document.frmParam, false);
		document.frmParam.add.disabled=false;
		document.frmParam.rst.disabled=true;
		document.frmParam.edit.disabled=true;
		document.frmParam.save.disabled=false;
		document.frmParam.cancel.disabled=false;
		document.frmParam.nilai_kurs.readOnly=true;
		document.frmParam.total_bayar.readOnly=true;
		window.location='${path }/ttppremi/titipan_premi.htm?sts=insert&editSPAJ='+document.frmParam.reg_spaj.value;
	}
	
	function deleteklik(){
		document.frmParam.submitType.value="DELETE";
		enableDisableAll(document.frmParam, false);
		return confirm('DELETE ?');
		//window.location='${path }/ttppremi/titipan_premi.htm?sts=DELETE&editSPAJ='+document.frmParam.reg_spaj.value+'&ke='+document.frmParam.no_ke.value;
	
	}
	
	function editklik(){
		var temp_nilai_trx = document.frmParam.jml_bayar.value;
		temp_nilai_trx = temp_nilai_trx.replace(/,/g,"");
		enableDisableAll(document.frmParam, false);
		document.frmParam.add.disabled=true;
		document.frmParam.rst.disabled=true;
		document.frmParam.edit.disabled=true;
		document.frmParam.save.disabled=false;
		document.frmParam.cancel.disabled=false;
		document.frmParam.submitType.value="EDIT";
		document.frmParam.nilai_kurs.readOnly=true;
		document.frmParam.total_bayar.readOnly=true;
		document.getElementById("ucup").setAttribute("disabled", "disabled");
		document.getElementById("_bank").setAttribute("disabled", "disabled");
		document.getElementById("_tgl_rk").setAttribute("disabled", "disabled");
		document.getElementById("img_tgl_rk").setAttribute("disabled", "disabled");
		document.getElementById("rktgl").setAttribute("disabled", "disabled");
		document.getElementById("rkcc").setAttribute("disabled", "disabled");
		document.getElementById("rkva").setAttribute("disabled", "disabled");
	}
	
	function cancelklik(){
		enableDisableAll(document.frmParam, true);
		document.frmParam.add.disabled=false;
		document.frmParam.rst.disabled=true;
		document.frmParam.edit.disabled=false;
		document.frmParam.save.disabled=true;
		document.frmParam.cancel.disabled=true;
		document.frmParam.submitType.value="UPDATE";
		document.frmParam.nilai_kurs.readOnly=true;
		document.frmParam.total_bayar.readOnly=true;
	}

	a = '${cmd.submitType}';

	<spring:bind path="cmd.*">
		errCount = ${status.errors.errorCount};
	</spring:bind>
	
	document.frmParam.nilai_kurs.readOnly=true;
	document.frmParam.total_bayar.readOnly=true;
	
	if (a==""){
		enableDisableAll(document.frmParam, true);
		document.frmParam.add.disabled=false;
		document.frmParam.cancel.disabled=true;
	}else{
		if(a=='insert'){
			enableDisableAll(document.frmParam, false);
			document.frmParam.add.disabled=true;
			document.frmParam.rst.disabled=false;
			document.frmParam.edit.disabled=true;
			document.frmParam.save.disabled=false;
			document.frmParam.cancel.disabled=false;
		}else{
			if(a=='update'){
				if(errCount>0){
					enableDisableAll(document.frmParam, false);
					document.frmParam.add.disabled=true;
					document.frmParam.rst.disabled=true;
					document.frmParam.edit.disabled=true;
					document.frmParam.save.disabled=false;
					document.frmParam.cancel.disabled=false;					
				}else{
					enableDisableAll(document.frmParam, true);
					document.frmParam.add.disabled=false;
					document.frmParam.rst.disabled=true;
					document.frmParam.edit.disabled=false;
					document.frmParam.save.disabled=true;
					document.frmParam.cancel.disabled=true;
				}
			}else{
				if(a=='edit'){
					if(errCount>0){
						enableDisableAll(document.frmParam, false);
						document.frmParam.add.disabled=true;
						document.frmParam.rst.disabled=true;
						document.frmParam.edit.disabled=true;
						document.frmParam.save.disabled=false;
						document.frmParam.cancel.disabled=false;
					}else{
						enableDisableAll(document.frmParam, true);
						document.frmParam.add.disabled=false;
						document.frmParam.rst.disabled=true;
						document.frmParam.edit.disabled=true;
						document.frmParam.save.disabled=true;
						document.frmParam.cancel.disabled=true;
					}
				}else{
					if(a=='cancel'){
						enableDisableAll(document.frmParam, true);
						document.frmParam.add.disabled=true;
						document.frmParam.rst.disabled=true;
						document.frmParam.edit.disabled=true;
						document.frmParam.save.disabled=true;
						document.frmParam.cancel.disabled=true;
						alert("Ini Autodebet. Tidak Perlu Titipan Premi !!!");
					}
				}
			}
		}
	}
	
</script>

<%@ include file="/include/page/footer.jsp"%>
