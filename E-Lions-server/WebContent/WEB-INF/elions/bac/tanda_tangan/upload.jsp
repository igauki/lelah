<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script>
</head>
<body onload="setupPanes('container1', 'tab2');">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" id="tab1">Pemegang</a>
				<a href="#" onClick="return showPane('pane2', this)" id="tab2">Tertanggung</a>
			</li>
		</ul>
		<div class="tab-panes">
			<div id="pane2" class="panes">
				<!-- imageFormat : jpg, png, gif -->
				<jsp:plugin type="applet" code="com/ekalife/applet/customersign/SignApplet.class"
						codebase="${path}/include/applets" name="signupload" align="middle" height="100%" width="100%">
					<jsp:params>
						<jsp:param name="spaj" value="${spaj}" />
						<jsp:param name="nono" value="0" />
						<jsp:param name="imageFormat" value="jpg" />
						<jsp:param name="fileName" value="sign" />
						<jsp:param name="path" value="http://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/sign" />
					</jsp:params>
					<jsp:fallback>Apabila anda mengalami kesulitan harap menghubungi EDP</jsp:fallback>
				</jsp:plugin>
			</div>
		</div>
	</div>
	
</body>
</html>