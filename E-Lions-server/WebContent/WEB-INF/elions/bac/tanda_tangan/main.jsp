<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script>
<script>
function cetak(){
	window.frames['download_spaj'].location = '${path}/bac/multi.htm?window=download_spaj&file=${fileSpaj}&x=${x}&y=${y}&f=${f}&spaj=${spaj}';
	return false;
}
</script>
<script>
	function signPreview(mstt_id){
		document.getElementById('signPreviewFrame').src = '${path}/bac/sign.htm?window=preview&mstt_id='+mstt_id;
	}
	
	function flagUpdate(mstt_id){
		var txtSPAJ=document.getElementById('txtSPAJ'+mstt_id).value;
		if(txtSPAJ!=""){
			document.getElementById('hdFlagUpdate'+mstt_id).value='1';
		}else{
			document.getElementById('hdFlagUpdate'+mstt_id).value='';
		}
	}
	
	function download(spaj, flag){		
			
		if (flag==1){
			document.getElementById("download_spaj").src='http://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/bac/multi.htm?window=download_spaj&spaj='+spaj;
		}else{
			var f= document.getElementById("hdF"+spaj).value;
			var x= document.getElementById("hdX"+spaj).value;
			var y= document.getElementById("hdY"+spaj).value;
			var file= document.getElementById("hdFile"+spaj).value;
				window.frames['download_spaj'].location='http://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/bac/multi.htm?window=download_spaj&spaj='+spaj+'&f='+f+'&x='+x+'&y='+y+'&file='+file;
		}
		
		return showPane('pane6', tab3);
	}
</script>

<!--<c:if test="${download1 eq \"1\"}">-->
<!--	<script>-->
<!--		document.getElementById('download_spaj').src = 'http://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/bac/multi.htm?window=download_spaj&file=${file}&x=${x}&y=${y}&f=${f}&spaj=${spaj};-->
<!--	</script>-->
<!--</c:if>-->
</head>
<c:choose>
	<c:when test="${empty daftarTtg}">
		<body onload="setupPanes('container1', 'tab2');">
	</c:when>
	<c:when  test="${download1 eq \"1\"}">
	<body onload="setupPanes('container1', 'tab3');"></c:when>
	<c:otherwise>
		<body onload="setupPanes('container1', 'tab1');">
	</c:otherwise>
</c:choose>



	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane5', this)"id="tab1">List</a>
				<a href="#" onClick="return showPane('pane1', this)" id="tab2">Upload</a>
				<a href="#" onClick="download('',1);" id="tab3">Download SPAJ</a>
		
<%--<a href="#" onClick="return showPane('pane1', this)" id="tab1">List</a>
				<a href="#" id="tab2">Upload</a>
				<a href="#" id="tab3">Download SPAJ</a>
	--%>	</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<!-- imageFormat : jpg, png, gif -->
				<jsp:plugin type="applet" code="com/ekalife/applet/customersign/SignApplet.class"
						codebase="${path}/include/applets" name="signupload" align="middle" height="100%" width="100%">
					<jsp:params>
						<jsp:param name="spaj" value="${spaj}" />
						<jsp:param name="nono" value="0" />
						<jsp:param name="imageFormat" value="jpg" />
						<jsp:param name="fileName" value="sign" />
						<jsp:param name="path" value="http://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/sign" />
					</jsp:params>
					<jsp:fallback>Apabila anda mengalami kesulitan harap menghubungi EDP</jsp:fallback>
				</jsp:plugin>
			</div>

			<div id="pane2" class="panes" style="height: 600px;">

	<!-- imageFormat : jpg, png, gif -->
				<jsp:plugin type="applet" code="com/ekalife/applet/customersign/SignApplet.class"
						codebase="${path}/include/applets" name="signupload" align="middle" height="100%" width="100%">
					<jsp:params>
						<jsp:param name="spaj" value="${spaj}" />
						<jsp:param name="nono" value="1" />
						<jsp:param name="imageFormat" value="gif" />
						<jsp:param name="fileName" value="sign" />
						<jsp:param name="path" value="http://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/sign" />
					</jsp:params>
					<jsp:fallback>Apabila anda mengalami kesulitan harap menghubungi EDP</jsp:fallback>
				</jsp:plugin>				
			</div>
			
			<div id="pane5" class="panes">
				<c:choose>
					<c:when test="${empty daftarTtg}">
					<br>					<br>					<br>					<br>
						<font style="font-size: 12px;font-weight: bolder">Silahkan Upload Dahulu Tanda Tangan</font>
					</c:when>
					<c:otherwise>
						<form method="post" name="formpost" style="text-align: center;" enctype="multipart/form-data">
						<table>
							<tr>
								<td>
									<table width="100%" height="100%">
										<tr align="center">
											<td style="font-size: 12pt; font-weight: bolder">
												List Tanda Tangan							
											</td>
										</tr>
										<tr align="center">
											<td style="font-size: 12pt; font-weight: bolder">
												<br>
												<input type="submit" name="save" value="Save">
												<input type="button" name="upload" value="Upload Tanda Tangan" onclick="return showPane('pane1', this)">
												<input type="button" name="refreshList" value="Refresh List" onclick="history.go()">						
											</td>
										</tr>
										<c:if test="${not empty errorMsg}">
											<tr >
												<td>
													<table border="1" bordercolor="RED" style="background-color: yellow;color: red;" width="100%">
														<tr>
															<td>
																<ul><b>Error Message :</b>
																<c:forEach items="${errorMsg}" var="e">
																	<li>${e}
																</c:forEach>
																</ul>
															</td>
														</tr>
													</table>
													
													
												</td>
											</tr>
										</c:if>
										<tr align="center">
											<td>				
												<table  border="1" style="border-color: black;border-style: solid;"  cellspacing="0" bordercolor="BLACK">
													<tr style="font-size: 9pt">
														<th >
															No.
														</th>
														<th>
															ID. TTD
														</th>
														<th>
															SPAJ
														</th>
														
														<th>
															Nama
														</th>
														<th>
															Created Date
														</th>	
														<th>
															
														</th>									
													</tr>
													<c:forEach items="${daftarTtg}" var="TTD" varStatus="status">
													
													<tr>
													
														<td >
															${status.count}.
														</td>
														<td align="center">
															${TTD.MSTT_ID}
														</td>
														<td align="center">
															<c:choose>
																<c:when test="${empty TTD.REG_SPAJ}">
																	<input type="text" size="13" maxlength="12" name="txtSPAJ${TTD.MSTT_ID }" id="txtSPAJ${TTD.MSTT_ID}" onchange="flagUpdate(${TTD.MSTT_ID })" >
																	
																</c:when>
																<c:otherwise>
																	${TTD.REG_SPAJ}
																</c:otherwise>
															</c:choose>
															<input type="hidden" name="hdFlagUpdate${TTD.MSTT_ID }" id="hdFlagUpdate${TTD.MSTT_ID}"  value="">
														</td>
														<%-- <td align="center">
															<c:choose>
																<c:when test="${empty TTD.mcl_id}">
																	<input type="text" size="12" maxlength="11" name="txtMCLID${TTD.mstt_id }">
																</c:when>
																<c:otherwise>
																	${TTD.mcl_id}
																</c:otherwise>
															</c:choose>
				
														</td>--%>
														<td align="center">
															${TTD.MSTT_NAMA}
														</td>
														<td align="center">
															<fmt:formatDate value="${TTD.CREATE_DT}" pattern="dd/MM/yyyy"/>
														</td>
														<td>
	
															<input type="button" name="preview" value="View TTD" onclick="signPreview(${TTD.MSTT_ID });">
															<c:if test="${not empty TTD.REG_SPAJ}">
																<input type="button" name="btnDownload" value="SPAJ" onclick="download('${TTD.REG_SPAJ}','2');">
																<c:forEach items="${daftarf}" var="D">
																	<c:if test="${TTD.REG_SPAJ eq D.spaj}">			
																		<input type="hidden" name="hdF${TTD.REG_SPAJ}" id="hdF${TTD.REG_SPAJ}"  value="${D.fontSize}">
																		<input type="hidden" name="hdX${TTD.REG_SPAJ}" id="hdX${TTD.REG_SPAJ}" value="${D.x}">
																		<input type="hidden" name="hdY${TTD.REG_SPAJ}" id="hdY${TTD.REG_SPAJ}" value="${D.y}">
																		<input type="hidden" name="hdFile${TTD.REG_SPAJ}" id="hdFile${TTD.REG_SPAJ}" value="${D.key}">
																	</c:if>
																</c:forEach>																
															</c:if>
														</td>
														
													
													</tr>
													
													</c:forEach>
												</table>
											</td>
										</tr>						
									</table>
								</td>
								<td align="center" valign="middle">
									<table width="100%" height="100%">
										<tr align="center">
											<td style="font-size: 12pt; font-weight: bolder">
												Preview Tanda Tangan 					
											</td>
										</tr>
										<tr align="center">
											<td style="font-size: 12pt; font-weight: bolder">
												<br>
																		
											</td>
										</tr>
										<tr align="center">	
											<td>
												<iframe name="signPreviewFrame" id="signPreviewFrame" src="" width="350px" height="250px;"></iframe>
											</td>
										</tr>
									</table>
								</td>
							</tr>						
						</table>
						
					</form>
					</c:otherwise>
				</c:choose>
				
			</div>
	<%-- 
			<div id="pane3" class="panes" style="height: 547px;">
				<table class="entry2">
					<tr>
						<th id="imageHolder">
							<iframe name="signFrame" src="${path}/sign?spaj=${spaj}&image=sign.jpg&no=0" width="100%" height="250px;"></iframe>
							<iframe name="signFrame2" src="${path}/sign?spaj=${spaj}&image=sign.jpg&no=1" width="100%" height="250px;"></iframe>
						</th>
					</tr>
					<tr>
						<th>
							<input type="button" value="Refresh" onclick="window.frames['signFrame'].location.reload(true)">
							<input type="button" value="Download SPAJ" onclick="cetak();return showPane('pane4', document.getElementById('tab4'));">
							<input type="button" value="Close" onclick="window.close();">
						</th>
					</tr>
				</table>
			</div>

			<div id="pane4" class="panes" >
				<table class="entry2">
					<tr>
						<th id="imageHolder">
							<iframe name="download_spaj" width="100%" height="500px;" src="${path}/bac/multi.htm?window=download_spaj"></iframe>
						</th>
					</tr>
				</table>
			</div>
			--%>
			<div id="pane6" class="panes" >
				<table class="entry2">					
					<tr>
						<th >
							<iframe name="download_spaj" id="download_spaj" width="100%" height="500px;" src="${path}/bac/multi.htm?window=download_spaj"></iframe>
						</th>
					</tr>
				</table>
			</div>
	
</body>
</html>