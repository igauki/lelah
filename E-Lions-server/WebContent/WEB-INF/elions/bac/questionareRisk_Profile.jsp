<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="pg" uri="/WEB-INF/tlds/taglib139.tld"%>

<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	    $().ready(function() {
	    
	    var spaj = '${spaj}';
	    	  	    
		$("#tabs").tabs();
		$("#load").hide();
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol	 
		
		// user memilih cabang, maka tampilkan admin2nya
		$("#jen_promo").change(function() {
		$('#info_promo').empty();
		
				

		});
		
		
		// SAVE
		$("#btnSave").click(function() {
			var selectedVal = "";
			var selectedanswer1 = $("input[type='radio'][name='quest1']:checked");
			var selectedanswer2 = $("input[type='radio'][name='quest7']:checked");
			var selectedanswer3 = $("input[type='radio'][name='quest12']:checked");
			var selectedanswer4 = $("input[type='radio'][name='quest18']:checked");
			var selectedanswer5 = $("input[type='radio'][name='quest23']:checked");
			var selectedanswer6 = $("input[type='radio'][name='quest28']:checked");
			var selectedanswer7 = $("input[type='radio'][name='quest34']:checked");
			var selectedanswer8 = $("input[type='radio'][name='quest40']:checked");
			var selectedanswer9 = $("input[type='radio'][name='quest45']:checked");
			var selectedanswer10 = $("input[type='radio'][name='quest51']:checked");
			
			var listanswer1 = selectedanswer1.val() +";"+ selectedanswer2.val() + ";"+selectedanswer3.val()+";"+selectedanswer4.val()+";"+selectedanswer5.val();
			var listanswer2 = selectedanswer6.val()+";"+selectedanswer7.val()+";"+selectedanswer8.val()+";"+selectedanswer9.val()+";"+selectedanswer10.val();
			var listanswer = listanswer1+";"+listanswer2;
			
			if (selectedanswer1.length > 0 && selectedanswer2.length > 0 
				&& selectedanswer3.length > 0 && selectedanswer4.length > 0 
				&& selectedanswer5.length > 0 && selectedanswer6.length > 0
				&& selectedanswer7.length > 0 && selectedanswer8.length > 0
				&& selectedanswer9.length > 0 && selectedanswer10.length > 0   ) {
// 			    selectedVal = selectedanswer10.val();
// 			    alert(selectedVal);
// 				alert($('#spaj').val());
// 				alert(listanswer);
				var url = "${path}/bac/multi.htm?window=profile_risk&spaj=" +$('#spaj').val() + "&viewonly=0&type=save&listanswer="+listanswer;
				$.getJSON(url, function(result) {
						alert(result.pesan);
						$("#tans1").val(result.tans1);
						$("#tans7").val(result.tans7);	
						$("#tans12").val(result.tans12);	
						$("#tans18").val(result.tans18);	
						$("#tans23").val(result.tans23);	
						$("#tans28").val(result.tans28);	
						$("#tans34").val(result.tans34);	
						$("#tans40").val(result.tans40);	
						$("#tans45").val(result.tans45);	
						$("#tans51").val(result.tans51);	
						$("#tdscore").text(result.score);		
						$("#tdtinkrisk").text(result.tinkrisk);
						$("#tdriskproduk").text(result.riskproduk);
						$("#tdprofilrisk").text(result.profilrisk);
				});
			
			}else{
				alert("Mohon melengkapi data terlebih dahulu");
				
			}	
				
		
		});
		
				
		
	});	
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }
	
	#thskor { width: 100px; background-color: #4CAF50; color: white;}
	#tdscore { color: green; font-weight: bold;}
	#tdtinkrisk { color: green; font-weight: bold;}
	#tdriskproduk { color: green; font-weight: bold;}
	#tdprofilrisk { color: green; font-weight: bold; text-align: left;}
	#pesanerror { color: red; font-weight: bold; text-align: left; font-size:30;}
</style>

<body>
<c:choose>
<c:when test="${pesanerror ne null}">
 <p id="pesanerror" name="pesanerror">${pesanerror}</p>
</c:when>
<c:otherwise>
<input id="spaj" name="spaj" type="hidden" value="${spaj}">
<input type ="hidden" id = "viewonly" name = "viewonly" value ="${viewonly}"  >

		
	 <div id="main">   
	 <form method="post" name="formpost" enctype="multipart/form-data">
							<fieldset class="ui-widget ui-widget-content">	
								<legend class="ui-widget-header ui-corner-all"><div><h3 align ="center">&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; PROFILE RISIKO NASABAH</h3></div></legend>
									&nbsp;&nbsp;
									
									<table border="1">
									<tr>
									<th  id="thskor" style="width: 100px;"><b>SKOR TOTAL</b></th>
									<th  id="thskor" style="width: 150px;"><b>TINGKAT RISIKO</b></th>
									<th  id="thskor" style="width: 220px;"><b>RISIKO PRODUK YANG SESUAI</b></th>
									<th  id="thskor" style="width: 800px;"><b>PROFILE RISIKO INVESTOR</b></th>
									</tr>
									
									<tr>

									<td align="center"> <p id="tdscore">${score}</p></td>
									<td align="center"> <p id="tdtinkrisk">${tinkrisk}</p></td>
									<td align="center"> <p id="tdriskproduk">${riskproduk}</p></td>
									<td align="center"> <p id="tdprofilrisk">${profilrisk}</p></td>
									</tr>
									</table>
									
									
									
									<table class = "some" >	
									<br>
									<br>
									
									<tr>
											<td><b>1. Apa pendidikan terakhir Anda ? </b> <br><br>
												<input type="radio" name="quest1" id="ans1a" value="2" <c:if test="${ans1 eq '2'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> SD atau sederajat (2) 																
												<input type="radio" name="quest1" id="ans1b" value="3" <c:if test="${ans1 eq '3'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> SMP atau sederajat (4)
												<input type="radio" name="quest1" id="ans1c" value="4" <c:if test="${ans1 eq '4'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> SMU atau sederajat (6)
												<input type="radio" name="quest1" id="ans1d" value="5" <c:if test="${ans1 eq '5'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> D3/S1 atau sederajat (8)
											    <input type="radio" name="quest1" id="ans1e" value="6" <c:if test="${ans1 eq '6'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Pasca Sarjana (10)
											
												<input type ="hidden" id = "ans1" name = "ans1" value ="${ans1}"  >
												
											</td>
											<td style="width: 100px;">&nbsp;</td>
											<th align="center" valign="middle"> <b><font size="4"><input type="text" id = "tans1" name = "tans1" value = "${tans1}" disabled width="20" style="width: 50px;"> </font></b> </th>
									</tr>
									
									<tr>
											<td><br><br><b>2. Berapa tahun pengalaman investasi yang Anda miliki pada investasi saham/reksadana ?</b> <br><br>
												<input type="radio" name="quest7" id="ans7a" value="8" <c:if test="${ans7 eq '8'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> < 1 tahun (2)<br>																
												<input type="radio" name="quest7" id="ans7b" value="9" <c:if test="${ans7 eq '9'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> 1-5 tahun (4)<br>
												<input type="radio" name="quest7" id="ans7c" value="10" <c:if test="${ans7 eq '10'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> 6-10 tahun (6)<br>
												<input type="radio" name="quest7" id="ans7d" value="11" <c:if test="${ans7 eq '11'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> > 10 tahun (8)
											    
											
												<input type ="hidden" id = "ans7" name = "ans7" value ="${ans7}"  >
											</td>
											<td style="width: 100px;">&nbsp;</td>
											<th align="center" valign="middle"> <b><font size="4"><input type="text" id = "tans7" name = "tans7" value = "${tans7}" disabled width="20" style="width: 50px;"> </font></b> </th>
									</tr>
									
									<tr>
											<td><br><br><b>3. Dalam berinvestasi, apakah tujuan investasi Anda saat ini ?</b> <br><br>
												<input type="radio" name="quest12" id="ans12a" value="13" <c:if test="${ans12 eq '13'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Keamanan dana sebagai tujuan paling penting (2)<br>																
												<input type="radio" name="quest12" id="ans12b" value="14" <c:if test="${ans12 eq '14'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Keamanan dana sebagai tujuan penting (4)<br>		
												<input type="radio" name="quest12" id="ans12c" value="15" <c:if test="${ans12 eq '15'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Keseimbangan antara keamanan dan pengembangan dana (6)<br>
												<input type="radio" name="quest12" id="ans12d" value="16" <c:if test="${ans12 eq '16'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Pengembangan dana adalah penting (8)<br>		
											    <input type="radio" name="quest12" id="ans12e" value="17" <c:if test="${ans12 eq '17'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Pengembangan dana adalah paling penting (10)	
											
												<input type ="hidden" id = "ans12" name = "ans12" value ="${ans12}"  >
											</td>
											<td style="width: 100px;">&nbsp;</td>
											<th align="center" valign="middle"> <b><font size="4"><input type="text" id = "tans12" name = "tans12" value = "${tans12}" disabled width="20" style="width: 50px;"> </font></b> </th>
									</tr>

									<tr>
											<td><br><br><b>4. Dalam kondisi darurat, jumlah dana cadangan yang Anda miliki saat ini dapat menutupi biaya bulanan normal
														    Anda selama berapa bulan ?</b> <br>
												<input type="radio" name="quest18" id="ans18a" value="19" <c:if test="${ans18 eq '19'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> < 3 bulan (2)<br>																
												<input type="radio" name="quest18" id="ans18b" value="20" <c:if test="${ans18 eq '20'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> 3-6 bulan (4)<br>		
												<input type="radio" name="quest18" id="ans18c" value="21" <c:if test="${ans18 eq '21'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> 7-9 bulan (6)<br>
												<input type="radio" name="quest18" id="ans18d" value="22" <c:if test="${ans18 eq '22'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> > 9 bulan (8)<br>		
											    											
												<input type ="hidden" id = "ans18" name = "ans18" value ="${ans18}"  >
											</td>
											<td style="width: 100px;">&nbsp;</td>
											<th align="center" valign="middle"> <b><font size="4"><input type="text" id = "tans18" name = "tans18" value = "${tans18}" disabled width="20" style="width: 50px;"> </font></b> </th>
									</tr>
									
									<tr>
											<td><br><br><b>5. Kapankah Anda berencana untuk pensiun ?</b> <br><br>
												<input type="radio" name="quest23" id="ans23a" value="24" <c:if test="${ans23 eq '24'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Dalam waktu 5 tahun (2)<br>																
												<input type="radio" name="quest23" id="ans23b" value="25" <c:if test="${ans23 eq '25'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> 6-10 tahun (4)<br>		
												<input type="radio" name="quest23" id="ans23c" value="26" <c:if test="${ans23 eq '26'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> 11-15 tahun (6)<br>
												<input type="radio" name="quest23" id="ans23d" value="27" <c:if test="${ans23 eq '27'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Sekurang-kurangnya 16 tahun lagi (8)<br>	
											    											
												<input type ="hidden" id = "ans23" name = "ans23" value ="${ans23}"  >
											</td>
											<td style="width: 100px;">&nbsp;</td>
											<th align="center" valign="middle"> <b><font size="4"><input type="text" id = "tans23" name = "tans23" value = "${tans23}" disabled width="20" style="width: 50px;"> </font></b> </th>
									</tr>
									
									<tr>
											<td><br><br><b>6. Secara umum,berapa persen dari pendapatan bulanan Anda yang dapat dialokasikan untuk investasi
																dan/atau unit link ?</b> <br><br>
												<input type="radio" name="quest28" id="ans28a" value="29" <c:if test="${ans28 eq '29'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> 0% (2)<br>																
												<input type="radio" name="quest28" id="ans28b" value="30" <c:if test="${ans28 eq '30'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> 1%-10% (4)<br>		
												<input type="radio" name="quest28" id="ans28c" value="31" <c:if test="${ans28 eq '31'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> 11%-20% (6)<br>
												<input type="radio" name="quest28" id="ans28d" value="32" <c:if test="${ans28 eq '32'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> 21%-30% (8)<br>	
											    <input type="radio" name="quest28" id="ans28e" value="33" <c:if test="${ans28 eq '33'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> > 30% (10)<br>	
																										
												<input type ="hidden" id = "ans28" name = "ans28" value ="${ans28}"  >
											</td>
											<td style="width: 100px;">&nbsp;</td>
											<th align="center" valign="middle"> <b><font size="4"><input type="text" id = "tans28" name = "tans28" value = "${tans28}" disabled width="20" style="width: 50px;"> </font></b> </th>
									</tr>
									
									<tr>
											<td><br><br><b>7. Manakah di antara pertanyaan ini yang paling menggambarkan perasaan dan sikap Anda terhadap risiko ?</b> <br><br>
												<input type="radio" name="quest34" id="ans34a" value="35" <c:if test="${ans34 eq '35'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Saya tidak dapat menerima risiko sekecil apapun (2)<br>																
												<input type="radio" name="quest34" id="ans34b" value="36" <c:if test="${ans34 eq '36'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Saya sedapat mungkin menghindari risiko, namun masih dapat menerima risiko yang minimal (4)<br>		
												<input type="radio" name="quest34" id="ans34c" value="37" <c:if test="${ans34 eq '37'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Saya akan coba mengimbangi antara risiko dan hasil investasi (6)<br>
												<input type="radio" name="quest34" id="ans34d" value="38" <c:if test="${ans34 eq '38'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Saya bersedia menerima risiko lebih tinggi asalkan sepadan dengan hasil investasi yang didapat (8)<br>	
											    <input type="radio" name="quest34" id="ans34e" value="39" <c:if test="${ans34 eq '39'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Risiko tidak pernah menghalangi saya dalam mengambil keputusan. Saya hanya berfokus pada hasil investasi (10)<br>	
																										
												<input type ="hidden" id = "ans34" name = "ans34" value ="${ans34}"  >
											</td>
											<td style="width: 100px;">&nbsp;</td>
											<th align="center" valign="middle"> <b><font size="4"><input type="text" id = "tans34" name = "tans34" value = "${tans34}" disabled width="20" style="width: 50px;"> </font></b> </th>
									</tr>
									
									<tr>
											<td><br><br><b>8. Jenis investasi apakah yang Anda miliki saat ini ?</b> <br><br>
												<input type="radio" name="quest40" id="ans40a" value="41" <c:if test="${ans40 eq '41'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Tabungan dan deposito di bank (2)<br>																
												<input type="radio" name="quest40" id="ans40b" value="42" <c:if test="${ans40 eq '42'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Tabungan, deposito di bank, dan valuta asing (4)<br>		
												<input type="radio" name="quest40" id="ans40c" value="43" <c:if test="${ans40 eq '43'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Tabungan, deposito di bank, valuta asing, dan obligasi (6)<br>
												<input type="radio" name="quest40" id="ans40d" value="44" <c:if test="${ans40 eq '44'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Tabungan, deposito di bank, valuta asing, obligasi, saham, dan derivative (8)<br>	
											  												
												<input type ="hidden" id = "ans40" name = "ans40" value ="${ans40}"  >
											</td>
											<td style="width: 100px;">&nbsp;</td>
											<th align="center" valign="middle"> <b><font size="4"><input type="text" id = "tans40" name = "tans40" value = "${tans40}" disabled width="20" style="width: 50px;"> </font></b> </th>
									</tr>
									
									<tr>
											<td><br><br><b>9. Apakah yang Anda lakukan jika saham Anda turun 10%-20% ?</b> <br><br>
												<input type="radio" name="quest45" id="ans45a" value="46" <c:if test="${ans45 eq '46'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Saya akan menjualnya (2)<br>																
												<input type="radio" name="quest45" id="ans45b" value="47" <c:if test="${ans45 eq '47'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Saya akan mengubah ke jenis investasi yang lebih aman (4)<br>		
												<input type="radio" name="quest45" id="ans45c" value="48" <c:if test="${ans45 eq '48'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Fluktuasi harga itu biasa, saya akan menunggu sebelum saya melakukan perubahan (6)<br>
												<input type="radio" name="quest45" id="ans45d" value="49" <c:if test="${ans45 eq '49'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Saya akan melanjutkan investasi jangka panjang dan mempertahankan kombinasi aset saya (8)<br>	
											  	<input type="radio" name="quest45" id="ans45e" value="50" <c:if test="${ans45 eq '50'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Saya akan menambah jumlah saham saya pada saat harga saham turun (10)<br>
																										
												<input type ="hidden" id = "ans45" name = "ans45" value ="${ans45}"  >
											</td>
											<td style="width: 100px;">&nbsp;</td>
											<th align="center" valign="middle"> <b><font size="4"><input type="text" id = "tans45" name = "tans45" value = "${tans45}" disabled width="20" style="width: 50px;"> </font></b> </th>
									</tr>
									
									<tr>
											<td><br><br><b>10. Apa yang Anda harapkan dari hasil investasi Anda ?</b> <br><br>
												<input type="radio" name="quest51" id="ans51a" value="52" <c:if test="${ans51 eq '52'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Tidak ada penurunan sama sekali (2)<br>																
												<input type="radio" name="quest51" id="ans51b" value="53" <c:if test="${ans51 eq '53'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Setidaknya sama dengan tingkat inflasi (4)<br>		
												<input type="radio" name="quest51" id="ans51c" value="54" <c:if test="${ans51 eq '54'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Diatas rata-rata tingkat inflasi (6)<br>
												<input type="radio" name="quest51" id="ans51d" value="55" <c:if test="${ans51 eq '55'}"> checked="checked" </c:if> <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>
															/> Jauh di atas tingkat inflasi (8)<br>	
											  																											
												<input type ="hidden" id = "ans51" name = "ans51" value ="${ans51}"  >
											</td>
											<td style="width: 100px;">&nbsp;</td>
											<th align="center" valign="middle"> <b><font size="4"><input type="text" id = "tans51" name = "tans51" value = "${tans51}" disabled width="20" style="width: 50px;"> </font></b> </th>
									</tr>
									
									
									</table>
									&nbsp;
									<div class="rowElem"> 		
										<center> 
										<button id="btnSave" onclick="return false;" <c:if test="${viewonly eq '1'}">disabled = "disabled"</c:if>>SAVE</button>									
										</center>	
									</div>	 	
									
																														
							</fieldset>
							
						</form>
			</div>	
<c:if test="${not empty pesan}">
	<script>alert('${pesan}');</script>
</c:if>		
</c:otherwise>
</c:choose>

</body>