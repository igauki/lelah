<%@ include file="/include/page/header.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<%User currentUser = (User) session.getAttribute("currentUser");%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>
<script type="text/javascript" src="${path }/include/js/bac/pemegang.js"></script>
<script>
// BEBERAPA JAVASCRIPT DIPINDAHKAN KE PEMEGANG.JS UNTUK MENGHINDARI ERROR EXCEEDING THE 65535 BYTES LIMIT.

	function edit_onClick(){
			with(document.frmParam){
			if(edit_pemegang.checked==true){
				document.getElementById('edit_pemegang').checked=true;
				document.getElementById('edit_pemegang').value='1';
				elements['pemegang.edit_pemegang'].value = '1';
			}else{
				document.getElementById('edit_pemegang').value='0';
				elements['pemegang.edit_pemegang'].value = '0';	
			}
		}
	}
	function tanyaPsave(spaj){
		//uncomment yg dibawah ini
		/*if(confirm('Apakah Spaj ini merupakan konversi produk [Powersave / Simas Prima] ke [Stable Link / Simas Stabil Link] ?\nTekan OK bila YA, atau tekan CANCEL bila TIDAK.')){
			window.location='${path}/bac/edit.htm?isPsaveToSlink=true&kopiSPAJ='+document.frmParam.kopiSPAJ.value;
		}else{
			window.location='${path}/bac/edit.htm?kopiSPAJ='+document.frmParam.kopiSPAJ.value;
		}*/
		//comment yg dibawah ini
		window.location='${path}/bac/edit.htm?kopiSPAJ='+document.frmParam.kopiSPAJ.value;
		/*return false;*/
	}
	function Body_onload() {
		var frmParam = document.frmParam; 
		body_onload_sub_1();
		frmParam.elements['pemegang.lus_id'].value=<%=currentUser.getLus_id()%>;
		frmParam.elements['pemegang.cbg_lus_id'].value='<%=currentUser.getLca_id()%>';
		body_onload_sub_2();
		var adaData = '${adaData}';
		var  flag_special_case = '${cmd.flag_special_case}';
		if(flag_special_case==1){
					
			document.getElementById('special_case').value = '1';
								
		}else{
			document.getElementById('special_case').value = '0';
			document.getElementById('flag_special_case').value = '0';
		}
		//if(adaData != ''){
		//	if(!confirm('Ada data hasil input sebelumnya, apakah anda ingin menggunakan data ini?')){
		//		window.location = '${path}/bac/edit.htm?data_baru=1';
		//	}
		//}		
	}
	function loaddata_penagihan()
	{
		if (document.frmParam.elements['addressbilling.tagih'].value == "1")
		{
			if (document.frmParam.elements['addressbilling.kota_tgh'].value=="")
			{document.frmParam.elements['addressbilling.kota_tgh'].value="";
			}else{document.frmParam.elements['addressbilling.kota_tgh'].value='${cmd.addressbilling.kota_tgh}';}
			document.frmParam.elements['addressbilling.kota_tgh'].disabled = false;
			document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['addressbilling.msap_address'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#FFFFFF';
			if ((document.frmParam.elements['addressbilling.msap_address'].value=="")||(document.frmParam.elements['addressbilling.msap_address'].value==""))
			{	document.frmParam.elements['addressbilling.msap_address'].value='';
			}else{document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.alamat_sementara.value;}
			document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#FFFFFF';						
			if ((document.frmParam.elements['addressbilling.msap_zip_code'].value=="")||(document.frmParam.elements['addressbilling.msap_zip_code'].value==""))
			{document.frmParam.elements['addressbilling.msap_zip_code'].value='';
			}else{document.frmParam.elements['addressbilling.msap_zip_code'].value='${cmd.addressbilling.msap_zip_code}';}			
			
			document.frmParam.elements['addressbilling.lsne_id'].readOnly = false;
			document.frmParam.elements['addressbilling.lsne_id'].style.backgroundColor ='#FFFFFF';						
			if ((document.frmParam.elements['addressbilling.lsne_id'].value=="")||(document.frmParam.elements['addressbilling.lsne_id'].value==""))
			{document.frmParam.elements['addressbilling.lsne_id'].value='';
			}else{document.frmParam.elements['addressbilling.lsne_id'].value='${cmd.addressbilling.lsne_id}';}
			
			document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#FFFFFF';						
			if ((document.frmParam.elements['addressbilling.msap_area_code1'].value=="")||(document.frmParam.elements['addressbilling.msap_area_code1'].value==""))
			{document.frmParam.elements['addressbilling.msap_area_code1'].value='';
			}else{document.frmParam.elements['addressbilling.msap_area_code1'].value='${cmd.addressbilling.msap_area_code1}';}
			document.frmParam.elements['addressbilling.msap_phone1'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#FFFFFF';						
			if ((document.frmParam.elements['addressbilling.msap_phone1'].value=="")||(document.frmParam.elements['addressbilling.msap_phone1'].value==""))
			{document.frmParam.elements['addressbilling.msap_phone1'].value='';
			}else{document.frmParam.elements['addressbilling.msap_phone1'].value='${cmd.addressbilling.msap_phone1}';}
			document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#FFFFFF';						
			if ((document.frmParam.elements['addressbilling.msap_area_code2'].value=="")||(document.frmParam.elements['addressbilling.msap_area_code2'].value==""))
			{document.frmParam.elements['addressbilling.msap_area_code2'].value='';
			}else{document.frmParam.elements['addressbilling.msap_area_code2'].value='${cmd.addressbilling.msap_area_code2}';}
			document.frmParam.elements['addressbilling.msap_phone2'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#FFFFFF';						
			if ((document.frmParam.elements['addressbilling.msap_phone2'].value=="")||(document.frmParam.elements['addressbilling.msap_phone2'].value==""))
			{document.frmParam.elements['addressbilling.msap_phone2'].value='';
			}else{document.frmParam.elements['addressbilling.msap_phone2'].value='${cmd.addressbilling.msap_phone2}';}
			document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#FFFFFF';						
			document.frmParam.elements['addressbilling.msap_area_code3'].value='${cmd.addressbilling.msap_area_code3}';
			document.frmParam.elements['addressbilling.msap_phone3'].readOnly = false;
			document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#FFFFFF';						
			document.frmParam.elements['addressbilling.msap_phone3'].value='${cmd.addressbilling.msap_phone3}';
// 			document.frmParam.elements['addressbilling.no_hp'].value='${cmd.pemegang.no_hp}';
// 			document.frmParam.elements['addressbilling.no_hp2'].value='${cmd.pemegang.no_hp2}';
		}else{
			if (document.frmParam.elements['addressbilling.tagih'].value=="2")
			{if (document.frmParam.elements['datausulan.jenis_pemegang_polis'].value == "1"){
					if (document.frmParam.elements['contactPerson.kota_rumah'].value == "")
					{document.frmParam.elements['addressbilling.kota_tgh'].value="";
					}else{document.frmParam.elements['addressbilling.kota_tgh'].value='${cmd.contactPerson.kota_rumah}';}
					document.frmParam.elements['addressbilling.kota_tgh'].disabled = true;
					document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.alamat_sementara1.value;
					document.frmParam.elements['addressbilling.msap_zip_code'].value='${cmd.contactPerson.kd_pos_rumah}';		
					document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D4D4D4';
					
					document.frmParam.elements['addressbilling.lsne_id'].value='${cmd.contactPerson.lsne_id}';		
					document.frmParam.elements['addressbilling.lsne_id'].readOnly = true;
					document.frmParam.elements['addressbilling.lsne_id'].style.backgroundColor ='#D4D4D4';
					
					document.frmParam.elements['addressbilling.msap_area_code1'].value='${cmd.contactPerson.area_code_rumah}';		
					document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone1'].value='${cmd.contactPerson.telpon_rumah}';
					document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_area_code2'].value='${cmd.contactPerson.area_code_rumah2}';		
					document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone2'].value='${cmd.contactPerson.telpon_rumah2}';
					document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D4D4D4';					
					document.frmParam.elements['addressbilling.msap_area_code3'].value='';		
					document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone3'].value='';
					document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D4D4D4';	
// 					document.frmParam.elements['addressbilling.no_hp'].value='${cmd.pemegang.no_hp}';
// 					document.frmParam.elements['addressbilling.no_hp2'].value='${cmd.pemegang.no_hp2}';
				}else{
					if (document.frmParam.elements['pemegang.kota_rumah'].value == "")
					{document.frmParam.elements['addressbilling.kota_tgh'].value="";
					}else{document.frmParam.elements['addressbilling.kota_tgh'].value='${cmd.pemegang.kota_rumah}';}
					document.frmParam.elements['addressbilling.kota_tgh'].disabled = true;
					document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.alamat_sementara1.value;
					document.frmParam.elements['addressbilling.msap_zip_code'].value='${cmd.pemegang.kd_pos_rumah}';		
					document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D4D4D4';
					
					document.frmParam.elements['addressbilling.lsne_id'].value='${cmd.pemegang.lsne_id}';		
					document.frmParam.elements['addressbilling.lsne_id'].readOnly = true;
					document.frmParam.elements['addressbilling.lsne_id'].style.backgroundColor ='#D4D4D4';					
					
					document.frmParam.elements['addressbilling.msap_area_code1'].value='${cmd.pemegang.area_code_rumah}';		
					document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone1'].value='${cmd.pemegang.telpon_rumah}';
					document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_area_code2'].value='${cmd.pemegang.area_code_rumah2}';		
					document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone2'].value='${cmd.pemegang.telpon_rumah2}';
					document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D4D4D4';					
					document.frmParam.elements['addressbilling.msap_area_code3'].value='';		
					document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['addressbilling.msap_phone3'].value='';
					document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
					document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D4D4D4';	
// 					document.frmParam.elements['addressbilling.no_hp'].value='${cmd.pemegang.no_hp}';
// 					document.frmParam.elements['addressbilling.no_hp2'].value='${cmd.pemegang.no_hp2}';
				}
			}else{
				if (document.frmParam.elements['addressbilling.tagih'].value=="3")
				{if (document.frmParam.elements['datausulan.jenis_pemegang_polis'].value == "1"){
						if (document.frmParam.elements['contactPerson.kota_kantor'].value == "")
						{document.frmParam.elements['addressbilling.kota_tgh'].value="";
						}else{document.frmParam.elements['addressbilling.kota_tgh'].value='${cmd.contactPerson.kota_kantor}';}
						document.frmParam.elements['addressbilling.kota_tgh'].disabled = true;
						document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.alamat_sementara2.value;
						document.frmParam.elements['addressbilling.msap_zip_code'].value='${cmd.contactPerson.kd_pos_kantor}';		
						document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D4D4D4';
						
						document.frmParam.elements['addressbilling.lsne_id'].value='${cmd.contactPerson.lsne_id}';		
						document.frmParam.elements['addressbilling.lsne_id'].readOnly = true;
						document.frmParam.elements['addressbilling.lsne_id'].style.backgroundColor ='#D4D4D4';
						
						document.frmParam.elements['addressbilling.msap_area_code1'].value='${cmd.contactPerson.area_code_kantor}';		
						document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_phone1'].value='${cmd.contactPerson.telpon_kantor}';
						document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_area_code2'].value='${cmd.contactPerson.area_code_kantor2}';		
						document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_phone2'].value='${cmd.contactPerson.telpon_kantor2}';
						document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_area_code3'].value='';document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_phone3'].value='';document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D4D4D4';
// 						document.frmParam.elements['addressbilling.no_hp'].value='${cmd.pemegang.no_hp}';
// 						document.frmParam.elements['addressbilling.no_hp2'].value='${cmd.pemegang.no_hp2}';
					}else{
						if (document.frmParam.elements['pemegang.kota_kantor'].value == "")
						{document.frmParam.elements['addressbilling.kota_tgh'].value="";
						}else{document.frmParam.elements['addressbilling.kota_tgh'].value='${cmd.pemegang.kota_kantor}';}
						document.frmParam.elements['addressbilling.kota_tgh'].disabled = true;
						document.frmParam.elements['addressbilling.kota_tgh'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_address'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_address'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_address'].value=document.frmParam.alamat_sementara2.value;
						document.frmParam.elements['addressbilling.msap_zip_code'].value='${cmd.pemegang.kd_pos_kantor}';		
						document.frmParam.elements['addressbilling.msap_zip_code'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_zip_code'].style.backgroundColor ='#D4D4D4';
						
						document.frmParam.elements['addressbilling.lsne_id'].value='${cmd.pemegang.lsne_id}';		
						document.frmParam.elements['addressbilling.lsne_id'].readOnly = true;
						document.frmParam.elements['addressbilling.lsne_id'].style.backgroundColor ='#D4D4D4';						
						
						document.frmParam.elements['addressbilling.msap_area_code1'].value='${cmd.pemegang.area_code_kantor}';		
						document.frmParam.elements['addressbilling.msap_area_code1'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_area_code1'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_phone1'].value='${cmd.pemegang.telpon_kantor}';
						document.frmParam.elements['addressbilling.msap_phone1'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_phone1'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_area_code2'].value='${cmd.pemegang.area_code_kantor2}';		
						document.frmParam.elements['addressbilling.msap_area_code2'].readOnly = true;document.frmParam.elements['addressbilling.msap_area_code2'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_phone2'].value='${cmd.pemegang.telpon_kantor2}';document.frmParam.elements['addressbilling.msap_phone2'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_phone2'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_area_code3'].value='';document.frmParam.elements['addressbilling.msap_area_code3'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_area_code3'].style.backgroundColor ='#D4D4D4';
						document.frmParam.elements['addressbilling.msap_phone3'].value='';document.frmParam.elements['addressbilling.msap_phone3'].readOnly = true;
						document.frmParam.elements['addressbilling.msap_phone3'].style.backgroundColor ='#D4D4D4';		
// 						document.frmParam.elements['addressbilling.no_hp'].value='${cmd.pemegang.no_hp}';
// 						document.frmParam.elements['addressbilling.no_hp2'].value='${cmd.pemegang.no_hp2}';			
				}	}	}	}   }
</script>
<script language="JavaScript">
<!--
	if (!document.layers&&!document.all&&!document.getElementById)
		event="test"
			function showtip(current,e,text){
			if (document.all||document.getElementById){
				thetitle=text.split('<br>')
				if (thetitle.length>1){thetitles=''
					for (i=0;i<thetitle.length;i++)
						thetitles+=thetitle[i]
					current.title=thetitles
				}else {current.title=text}
			}else if (document.layers){
				document.tooltip.document.write('<layer bgColor="white" style="border:1px solid black;font-size:13px;">'+text+'</layer>')
				document.tooltip.document.close()
				document.tooltip.left=e.pageX+5
				document.tooltip.top=e.pageY+5
				document.tooltip.visibility="show"
			}}
// -->
    function konfirm(){
		 var pass;
			with(document.frmParam) {           
             
				if(special_case.value==1){					
							document.getElementById('special_case').checked=false;
							elements['special_case'].value = '0';
							document.getElementById('flag_special_case').value='0';
							return false;
				
				}else{
					if(confirm('Apakah Anda yakin mau input/edit SPAJ special case?Jika tidak pilih cancel, jika iya pilih Ok dan pastikan data yang diinput sesuai proposal')) {
							 pass=prompt(' harap masukkan password untuk special case spaj!');
							 if(pass=="Msig2015"){
							 	special_case.checked==true;
								elements['special_case'].value = '1';
								document.getElementById('flag_special_case').value='1';
								return true;
							 }else{
							 	alert('Password Salah');
							 	document.getElementById('special_case').checked=false;
								elements['special_case'].value = '0';
								document.getElementById('flag_special_case').value='0';
								return false;	
							 };
							 
					}else{
							
						document.getElementById('special_case').checked=false;
								elements['special_case'].value = '0';
								document.getElementById('flag_special_case').value='0';
								return false;	
					};					
					
				};
			};
	} 
	function bixx(flag){
		if(flag=='9'){						
			document.getElementById("expired").disabled = true;
		}else{
			document.getElementById("expired").disabled = false;
		}
	}

	function buttonLinks(str){
		if(str=='cari'){
			spaj = document.frmParam.spaj.value;
			if(spaj==''){alert('Harap cari SPAJ terlebih dahulu');
			}else{document.getElementById('infoFrame').src='${path}/bac/edit.htm?showSPAJ='+document.frmParam.spaj.value;
			}}}
	function resetPemegangPolis(){
		parent.document.getElementById('jenis_pemegang_polis').value = document.frmParam.elements['datausulan.jenis_pemegang_polis'].value;
		parent.document.getElementById('info').click();
	}
</script>
<body onLoad="Body_onload(); document.frmParam.elements['datausulan.mste_medical'].focus();">
<XML ID=xmlData></XML>
<form name="frmParam" method="post">
<table class="entry2">
	<tr>
		<td><input type="submit" name="_target0" value=" " onclick="next1()"  <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/pp2.jpg);"
				accesskey="1" onmouseover="return overlib('Alt-1', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target1" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ttg1.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target2" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ddu1.jpg);"
				accesskey="3" onmouseover="return overlib('Alt-3', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target3" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/inv1.jpg);"
				accesskey="4" onmouseover="return overlib('Alt-4', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target4" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ag1.jpg);"
				accesskey="5" onmouseover="return overlib('Alt-5', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target5" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/kf1.jpg);"
				accesskey="6" onmouseover="return overlib('Alt-6', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" name="dummy2" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/sb1.jpg);">
				<textarea cols="40" rows="7" name="alamat_sementara" onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); ">${cmd.addressbilling.msap_address}</textarea>
						<textarea cols="40" rows="7" name="alamat_sementara1" onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "><c:choose><c:when test="${cmd.datausulan.jenis_pemegang_polis == 1}">${cmd.contactPerson.alamat_rumah}</c:when><c:otherwise>${cmd.pemegang.alamat_rumah}</c:otherwise></c:choose></textarea>
						<textarea cols="40" rows="7" name="alamat_sementara2" onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "><c:choose><c:when test="${cmd.datausulan.jenis_pemegang_polis == 1}">${cmd.contactPerson.alamat_kantor}</c:when><c:otherwise>${cmd.pemegang.alamat_kantor}</c:otherwise></c:choose></textarea>
</td>
	</tr>
	<tr>
		<td width="67%" align="left" valign="top">
		<spring:bind path="cmd.*">
			<c:if test="${not empty status.errorMessages}">
				<div id="error">INFO:<br>
				<c:forEach var="error" items="${status.errorMessages}">
	- <c:out value="${error}" escapeXml="false" />
					<br />
				</c:forEach></div>
			</c:if>
		</spring:bind>	
		<c:if test="${cmd.pemegang.jn_bank eq 2}">
			<%-- 
			<c:if test="${cmd.pemegang.rate_1 gt 0 }">
		  	<center><font size ="3" color="red">Rate PowerSave Bank Sinarmas per hari ini ada.</font></center>
		 	</c:if>
		 	--%>
		 	<c:if test="${cmd.pemegang.rate_1 eq 0 }">
		  	<center><font size ="3" color="red">Rate Simas Prima per hari ini belum ada. Silahkan hubungi AO anda atau bagian Investment kami.</font></center>
		  	<script>alert('Rate Simas Prima per hari ini belum ada. Silahkan hubungi AO anda atau bagian Investment kami.');</script>
		 	</c:if>
			<%-- 
		 	<c:if test="${cmd.pemegang.rate_2 gt 0 }">
		  	<center><font size ="3" color="red">Rate PowerSave Bank Sinarmas manfaat bulanan per hari ini ada.</font></center>
		 	</c:if>
		 	--%>
		 	<c:if test="${cmd.pemegang.rate_2 eq 0 }">
		  	<center><font size ="3" color="red">Rate Simas Prima Manfaat Bulanan per hari ini belum ada. Silahkan hubungi AO anda atau bagian Investment kami.</font></center>
		  	<script>alert('Rate Simas Prima Manfaat Bulanan per hari ini belum ada. Silahkan hubungi AO anda atau bagian Investment kami.');</script>
		 	</c:if>
		  </c:if>
		<table class="entry">
			<tr>
				<td colspan=5 height="20" style="text-align: center;">
					<%-- <input type="button"  value="Isi Data Pemegang Polis PT Guthrie" onclick="isi();" style="display:none;"> --%>
					
					Copy Data SPAJ Dari:
					<input type="checkbox" name="specta_save1" class="noBorder"  value="${cmd.datausulan.specta_save}"  size="30" onClick="specta_save_onClick();"> SPECTA SAVE
					<spring:bind path="cmd.datausulan.specta_save"> 
		              <input type="hidden" name="${status.expression}" value="${cmd.datausulan.specta_save}"  size="30" style='background-color :#D4D4D4'readOnly>
					</spring:bind>
					<br>
					Masukkan ID:<input type="text" id="kopiSPAJ" name="kopiSPAJ" value=${cmd.pemegang.reg_spaj}>
					<input type="button"  value="Copy" 
						onclick="return tanyaPsave(document.frmParam.kopiSPAJ.value);">
<!-- 					<input type="button" value="Cari" name="searchkopi" -->
<!-- 						onclick="popWin('${path}/uw/spajgutri.htm?win=edit', 350, 450); "> -->
					 	<!-- &search=yes -->
					
					<br>
					<input type="checkbox" name="special_case" class="noBorder"<c:if test="${cmd.flag_special_case eq '1'}"> 
					   checked</c:if> id="special_case" size="30" onClick="konfirm();">SPecial Case
					 <spring:bind path="cmd.flag_special_case">  
		              	<input type="hidden" name="${status.expression}"  id="flag_special_case" value="${cmd.flag_special_case}"  size="30" style='background-color :#D4D4D4'readOnly>
					   </spring:bind> 
					<br>   
					PEMEGANG POLIS ADALAH BADAN HUKUM/USAHA : 
					<select name="datausulan.jenis_pemegang_polis" onchange="resetPemegangPolis();">
						<option value="0" <c:if test="${cmd.datausulan.jenis_pemegang_polis eq 0}">selected="selected"</c:if>>TIDAK</option>
						<option value="1" <c:if test="${cmd.datausulan.jenis_pemegang_polis eq 1}">selected="selected"</c:if>>YA</option>
					</select>
					<spring:bind path="cmd.datausulan.flag_worksite">
						<input type="hidden" name="${status.expression}" value="${status.value }" size="20">
					</spring:bind> 
					<spring:bind path="cmd.pemegang.cab_bank">
						<input type="hidden" name="${status.expression}" value="${status.value }" size="20">
					</spring:bind>
					<spring:bind path="cmd.pemegang.jn_bank">
						<input type="hidden" name="${status.expression}" value="${status.value }" size="20">
					</spring:bind>
					<spring:bind path="cmd.pemegang.lus_id">
						<input type="hidden" name="${status.expression}" value="${status.value }" size="20">
					</spring:bind>
					<spring:bind path="cmd.pemegang.cbg_lus_id">
						<input type="hidden" name="${status.expression}" value="${status.value }" size="20">
					</spring:bind>
					
					<br>
					<input type="submit" name="_target${halaman+1}" value="Next &raquo;" onClick="next()" 
						onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
					<input type="hidden" name="_page" value="${halaman}">
				</td>
			</tr>
			<tr>
				<th></th>
				<th>Nomor Registrasi (SPAJ) </th>
				<th><input type="reg_spaj"
					value="<elions:spaj nomor='${cmd.pemegang.reg_spaj}'/>"
					style='background-color :#D4D4D4' readOnly> &nbsp;&nbsp;&nbsp;&nbsp;
					<c:if test="${cmd.datausulan.flag_worksite_ekalife eq 1}"> Karyawan AJ Sinarmas</c:if>
					</th>
				<th>SPAJ</th>
				<th>
					<select name="pemegang.mste_spaj_asli">
						<option value="1" <c:if test="${cmd.pemegang.mste_spaj_asli eq 1}">selected="selected"</c:if>>ASLI</option>
						<option value="0" <c:if test="${cmd.pemegang.mste_spaj_asli eq 0}">selected="selected"</c:if>>FOTOKOPI/FAX</option>
					</select>
				</th>
			</tr>
			<tr>
				<th></th>
				<th>Medis</th>
				<th><select name="datausulan.mste_medical" >
					<c:forEach var="medis" items="${select_medis}">
						<option
							<c:if test="${cmd.datausulan.mste_medical eq medis.ID}"> SELECTED </c:if>
							value="${medis.ID}">${medis.MEDIS}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font>
				</th>
				<th>Nomor Seri</th>
				<th><spring:bind path="cmd.pemegang.mspo_no_blanko">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="12"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
						<font color="#CC3300">*</font>
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				<th>NO PB</th>
				<th ><spring:bind path="cmd.no_pb">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="20" 
										maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>
									>
									<font color="#CC3300">*</font>
								</spring:bind>
								</th>
				<th>NO CIF</th>
				<th ><spring:bind path="cmd.pemegang.mspo_nasabah_dcif">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="20" 
										maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>
									>
									<font color="#CC3300">*</font>
								</spring:bind>
								</th>
			</tr>
			<tr>
				<th></th>
				<th></th>
				<th></th>
				<th>No.Virtual Account</th>
				<th ><spring:bind path="cmd.tertanggung.mste_no_vacc">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="20" 
										maxlength="16"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
									<font color="#CC3300">*</font>
								</spring:bind>
								</th>	
			</tr>			
			<tr>
				<th></th>
				<th></th>
				<th></th>
				<th>Jenis Form SPAJ</th>
				<th>
					<select name="pemegang.mspo_flag_spaj"> 
						<option value="0" <c:if test="${cmd.pemegang.mspo_flag_spaj eq 0}">selected="selected"</c:if>>SPAJ LAMA</option>
						<option value="1" <c:if test="${cmd.pemegang.mspo_flag_spaj eq 1}">selected="selected"</c:if>>SPAJ BARU</option>
					</select>
				</th>		
			</tr>
			<c:if test="${cmd.currentUser.lca_id ne \"09\" }">
				<tr>
					<th></th>
					<th>CARI SUMBER BISNIS</th>
					<th colspan="3">
						<input type="text" name="carisumber" onkeypress="if(event.keyCode==13){ document.frmParam.btnsumber.click(); return false;}"> 
	              		<input type="button" name="btnsumber" value="Cari" onclick="ajaxSelectWithParam1a(document.frmParam.carisumber.value,'select_sumberBisnis','sumber','pemegang.sumber_id','', 'SUMBER_ID', 'NAMA_SUMBER', '','Silahkan pilih SUMBER BISNIS','3');"> 
					</th>
					
				</tr>
				<tr>
					<th></th>
					<th></th>
					<th colspan="3">
						<div id="sumber"> 
							<select name="pemegang.sumber_id" 
								 <c:if test="${ not empty status.errorMessage}">
										style='background-color :#FFE1FD'
								 </c:if>>
				                 <option value="${cmd.pemegang.sumber_id}">${cmd.pemegang.nama_sumber}</option>
				            </select>
			            </div>
					</th>
				</tr>
			</c:if>
			<tr>
			<c:if test="${cmd.datausulan.jenis_pemegang_polis == 0}">
				<!-- ada di "editpemegang_individu.jsp" -->
			</c:if>
			<c:if test="${cmd.datausulan.jenis_pemegang_polis == 1}">
				<tr>
				<th colspan=5 class="subtitle">DATA BADAN HUKUM / USAHA (PEMEGANG POLIS)</th>
			</tr>
			<tr><th width="18">1.</th>
				<th width="287">Jenis & Nama Badan Hukum / Usaha<br>
				</th>
				<th width="505">
				
				<select name="pemegang.lti_id">
					<option value=""></option>
					<c:forEach var="l" items="${gelar}">
						<option
							<c:if test="${cmd.pemegang.lti_id eq l.key}"> SELECTED </c:if>
							value="${l.key}">${l.value}</option>
					</c:forEach>
				</select>				
								<spring:bind path="cmd.pemegang.mcl_first">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="42" 
										maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>
									>
									<font color="#CC3300">*</font>
								</spring:bind>
				</th>
				<th width="155" colspan="2"></th>
			</tr>
			<tr>
				<th width="18" rowspan="2">2.</th>
				<th width="287" >No. SIUP
				</th>
				<th width="505" >
				
				<spring:bind path="cmd.personal.mpt_siup">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="23" maxlength="35"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>><font color="#CC3300">*</font>
				</spring:bind>	
							<th width="155">No. NPWP
				<th width="257"><spring:bind path="cmd.personal.mpt_npwp">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>><font color="#CC3300">*</font>
				</spring:bind>
				</th>
			</tr>
			<tr>
				<th width="287" >Tanggal Terdaftar
				</th>
				<th width="505" >
					<spring:bind path="cmd.personal.tgl_siup">
					<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
					<font color="#CC3300">*</font></spring:bind>
							</th>
							<th width="155">Tanggal Terdaftar</th>
				<th width="257">
					<spring:bind path="cmd.personal.tgl_npwp">
					<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
					<font color="#CC3300">*</font></spring:bind>
				</th>
			</tr>
			<tr><th width="18">3.</th>
				<th width="287">Tempat dan Tanggal Berdiri<br>
				</th>
				<th width="505" colspan="3">
				
				<spring:bind path="cmd.pemegang.mspe_place_birth">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="30"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				<font color="#CC3300">*</font></spring:bind>	
								<spring:bind path="cmd.pemegang.mspe_date_birth">
					<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
					<font color="#CC3300">*</font></spring:bind>
				</th>
			</tr>
			<tr>
				<th rowspan="6">4.</th>
				<th rowspan="6">Alamat</th>
				<th rowspan="6"><spring:bind path="cmd.pemegang.alamat_rumah">
					<textarea cols="40" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>${status.value }</textarea>
				<font color="#CC3300">*</font></spring:bind></th>
				<th>Kode Pos</th>
				<th><spring:bind path="cmd.pemegang.kd_pos_rumah">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="10"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>Kota</th>
				<th ><spring:bind path="cmd.pemegang.kota_rumah"><br>
				<input type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
		    	<span id="indicator_rumah" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
				</spring:bind>   
				<font color="#CC3300">*</font>
				<spring:bind path="cmd.pemegang.kota_kantor"><br>
				<input type="hidden" name="${status.expression }" style="width: 1px;" id="${status.expression }" value="${status.value }" onfocus="this.select();"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
		    	<span id="indicator_kantor" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
				</spring:bind>  
				</th>
			</tr>
			<tr>
				<th>Telp 1</th>
				<th><spring:bind path="cmd.pemegang.area_code_rumah">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.pemegang.telpon_rumah">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>Telp 2</th>
				<th><spring:bind path="cmd.pemegang.area_code_rumah2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.pemegang.telpon_rumah2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>No. Fax</th>
				<th><spring:bind path="cmd.pemegang.area_code_fax">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.pemegang.no_fax">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>Alamat E-mail</th>
				<th><spring:bind path="cmd.pemegang.email">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="40" maxlength="50"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>5.</th>
				<th>Tujuan Membeli Asuransi</th>
				<th colspan="3"><select name="pemegang.mkl_tujuan" >
					<c:forEach var="tujuan" items="${select_tujuan}">
						<option
							<c:if test="${cmd.pemegang.mkl_tujuan eq tujuan.ID}"> SELECTED </c:if>
							value="${tujuan.ID}">${tujuan.TUJUAN}</option>
					</c:forEach>
				</select> <font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th></th>
				<th>Lain-Lain, Jelaskan</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.tujuana">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="100"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>6.</th>
				<th>Sumber Dana</th>
				<th colspan="3"><select name="pemegang.mkl_pendanaan" >
					<c:forEach var="dana" items="${select_dana_badan_usaha}">
						<option
							<c:if test="${cmd.pemegang.mkl_pendanaan eq dana.ID}"> SELECTED </c:if>
							value="${dana.ID}">${dana.DANA}</option>
					</c:forEach>
				</select>
				 <font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th></th>
				<th>Lainnya, Jelaskan</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.danaa">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="100"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>7.</th>
				<th>Aset</th>
				<th colspan="3"><select name="personal.mkl_aset"
					>
					<c:forEach var="aset" items="${select_aset}">
						<option
							<c:if test="${cmd.personal.mkl_aset eq aset.ID}"> SELECTED </c:if>
							value="${aset.ID}">${aset.ASET}</option>
					</c:forEach>
				</select>
				 <font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th>8.</th>
				<th>Perkiraan Penghasilan Kotor Per Tahun</th>
				<th colspan="3"><select name="pemegang.mkl_penghasilan"
					>
					<c:forEach var="penghasilan" items="${select_penghasilan}">
						<option
							<c:if test="${cmd.pemegang.mkl_penghasilan eq penghasilan.ID}"> SELECTED </c:if>
							value="${penghasilan.ID}">${penghasilan.PENGHASILAN}</option>
					</c:forEach>
				</select>
				 <font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th>9.</th>
				<th>Klasifikasi Bidang Industri</th>
				<th colspan="3"><select name="pemegang.mkl_industri" >
					<c:forEach var="industri" items="${bidangUsaha}">
						<option
							<c:if test="${cmd.pemegang.mkl_industri eq industri.value}"> SELECTED </c:if>
							value="${industri.value}">${industri.value}</option>
					</c:forEach>
				</select>
				 <font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th></th>
				<th>Lainnya, Sebutkan</th>
				<th colspan="3"><spring:bind path="cmd.pemegang.industria">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="100"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind>
				</th>
			</tr>
			<br>
			<tr>
				<th colspan=5 class="subtitle">DATA PIC
				 <select name="contactPerson.pic_jenis">
						<option value="1" <c:if test="${cmd.contactPerson.pic_jenis eq 1}">selected="selected"</c:if>>Penanggung jawab sesuai yang tercantum di AD</option>
						<option value="2" <c:if test="${cmd.contactPerson.pic_jenis eq 2}">selected="selected"</c:if>>Yang diberi kuasa</option>
					</select></th>
			</tr>
			<tr>
				<th width="18">10.</th>
				<th width="287">Nama Lengkap<br>
				<span class="info">(sesuai dengan KTP / Identitas lain tanpa gelar)</span>
				</th>
				<th width="505">
				<select name="contactPerson.lti_id">
					<option value=""></option>
					<option value="15" <c:if test="${cmd.contactPerson.lti_id eq 15}">selected="selected"</c:if>>H.</option>
					<option value="16" <c:if test="${cmd.contactPerson.lti_id eq 16}">selected="selected"</c:if>>HJ.</option>
				</select>				
								<spring:bind path="cmd.contactPerson.nama_lengkap">
									<input type="text" name="${status.expression}"
										value="${status.value }" size="42" 
										maxlength="100"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>><font color="#CC3300">*</font>
								</spring:bind>
							</th>
				<th width="155">Gelar &nbsp;</th>
				<th width="257"><spring:bind path="cmd.contactPerson.mcl_gelar">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="20">
				</spring:bind></th>
			</tr>
			<tr>
				<th>11.</th>
				<th>Nama Ibu Kandung</th>
				<th colspan="3"><spring:bind path="cmd.contactPerson.mspe_mother">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="50"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'</c:if>>
				</spring:bind><font color="#CC3300">*</font></th>
			</tr>
			<tr>
				<th>12.</th>
				<th>Jabatan</th>
				<th><spring:bind path="cmd.contactPerson.mpn_job_desc">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
				<th>Masa Kerja</th>
				<th><spring:bind path="cmd.contactPerson.lama_kerja">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>13.</th>
				<th>Bukti Identitas</th>
				<th colspan="3">
					<spring:bind path="cmd.contactPerson.lside_id">
						<select name="${status.expression}" onChange="bixx(this.value);">
							<c:forEach var="d" items="${select_identitas}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
						<font color="#CC3300">*</font>
					</spring:bind>
				</th>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<th>No. KTP / Identitas lain</th>
				<th colspan="3"><spring:bind path="cmd.contactPerson.no_identity">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="50"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>	
				> <font color="#CC3300">*</font>
				</spring:bind></th>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<th>Tanggal Kadaluarsa</th>
				<th id ="expired"><spring:bind path="cmd.contactPerson.no_identity_expired">
					<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
					<font color="#CC3300">*</font></spring:bind></th>
				<th colspan="2">&nbsp;</th>
			</tr>
			<tr>
				<th>14.</th>
				<th>Warga Negara</th>
				<th colspan="3">
					<spring:bind path="cmd.contactPerson.lsne_id">
						<select name="${status.expression}">
							<c:forEach var="d" items="${select_negara}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
						<font color="#CC3300">*</font>
					</spring:bind>
				</th>
			</tr>
			<tr>
				<th>15.</th>
				<th>Tanggal Lahir</th>
				<th><spring:bind path="cmd.contactPerson.date_birth">
					<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
					<font color="#CC3300">*</font></spring:bind></th>
				<th>Usia</th>
				<th><spring:bind path="cmd.contactPerson.mste_age">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="4" readOnly>
				</spring:bind> tahun</th>
			</tr>
			<tr>
				<th></th>
				<th>di</th>
				<th colspan="3"><spring:bind path="cmd.contactPerson.place_birth">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="30"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				<font color="#CC3300">*</font></spring:bind></th>
			</tr>
			<tr>
				<th>16.</th>
				<th>Jenis Kelamin</th>
				<th><spring:bind path="cmd.contactPerson.mste_sex">
					<label for="cowok"> <input type="radio" class=noBorder
						name="${status.expression}" value="1"
						<c:if test="${cmd.contactPerson.mste_sex eq 1 or cmd.contactPerson.mste_sex eq null}"> 
									checked</c:if>
						id="cowok">Pria </label>
					<label for="cewek"> <input type="radio" class=noBorder
						name="${status.expression}" value="0"
						<c:if test="${cmd.contactPerson.mste_sex eq 0}"> 
									checked</c:if>
						id="cewek">Wanita </label><font color="#CC3300">*</font>
				</spring:bind> </th>
				<th>Status</th>
				<th><select name="contactPerson.sts_mrt" >
					<c:forEach var="marital" items="${select_marital}">
						<option
							<c:if test="${cmd.contactPerson.sts_mrt eq marital.ID}"> SELECTED </c:if>
							value="${marital.ID}">${marital.MARITAL}</option>
					</c:forEach>
				</select><font color="#CC3300">*</font></th>
			</tr>
<tr>
				<th>a.</th>
				<th>Nama Suami/Istri</th>
				<th><spring:bind path="cmd.contactPerson.nama_si">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="30"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>></spring:bind></th>
				<th>Tanggal Lahir</th>
				<th><spring:bind path="cmd.contactPerson.tgllhr_si">
					<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
					</spring:bind></th>
			</tr>
<tr>
				<th>b.</th>
				<th>Nama Anak 1</th>
				<th><spring:bind path="cmd.contactPerson.nama_anak1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="30"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
				<th>Tanggal Lahir</th>
				<th><spring:bind path="cmd.contactPerson.tgllhr_anak1">
					<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
					</spring:bind></th>
			</tr>
<tr>
				<th>c.</th>
				<th>Nama Anak 2</th>
				<th><spring:bind path="cmd.contactPerson.nama_anak2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="30"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
				<th>Tanggal Lahir</th>
				<th><spring:bind path="cmd.contactPerson.tgllhr_anak2">
					<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
					</spring:bind></th>
			</tr>	
<tr>
				<th>d.</th>
				<th>Nama Anak 3</th>
				<th><spring:bind path="cmd.contactPerson.nama_anak3">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="30"
					<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
				<th>Tanggal Lahir</th>
				<th><spring:bind path="cmd.contactPerson.tgllhr_anak3">
					<script>inputDate('${status.expression}', '${status.value}', false, '', 9);</script>
					</spring:bind></th>
			</tr>											
			<tr>
				<th>17.</th>
				<th>Agama</th>
				<th colspan="3">
				<script type="text/javascript">
					function agamaChange(flag){					
						if(flag=='6'){	document.frmParam.elements['contactPerson.agama'].readOnly = false;
						}else{
							document.frmParam.elements['contactPerson.agama'].value="";
							document.frmParam.elements['contactPerson.agama'].readOnly = true;}
					}
				</script>
					<spring:bind path="cmd.contactPerson.lsag_id">
						<select name="${status.expression}" onchange="agamaChange(this.value);">
							<c:forEach var="d" items="${select_agama}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
						<font color="#CC3300">*</font>
					</spring:bind>
					
				</th>
			</tr>
			<tr>
				<th></th>
				<th>Lain - Lain, Sebutkan</th>
				<th colspan="3">					
					<spring:bind path="cmd.contactPerson.agama">
						<input type="text" name="${status.expression}"
							value="${status.value }" size="25" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
					</spring:bind>
				</th>
			</tr>
			<tr>
				<th>18.</th>
				<th>Pendidikan</th>
				<th colspan="3">
					<spring:bind path="cmd.contactPerson.lsed_id">
						<select name="${status.expression}">
							<c:forEach var="d" items="${select_pendidikan}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
						<font color="#CC3300">*</font>
					</spring:bind>
				</th>
			</tr>
			<tr>
				<th rowspan="5">19. a.</th>
				<th rowspan="5">Alamat Rumah</th>
				<th rowspan="5"><spring:bind path="cmd.contactPerson.alamat_rumah">
					<textarea cols="40" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
						</c:if>
						>${status.value }</textarea>
				<font color="#CC3300">*</font></spring:bind></th>
				<th>Kode Pos</th>
				<th><spring:bind path="cmd.contactPerson.kd_pos_rumah">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="10"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>Kota</th>
				<th ><spring:bind path="cmd.contactPerson.kota_rumah"><br>
				<input type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
		    	<span id="indicator_rumah" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
				</spring:bind>   
				<font color="#CC3300">*</font>
				</th>
			</tr>
			<tr><th></th><th></th></tr>
			<tr>
				<th>Telp Rumah 1</th>
				<th><spring:bind path="cmd.contactPerson.area_code_rumah">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					</spring:bind> <spring:bind path="cmd.contactPerson.telpon_rumah">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				<font color="#CC3300">*</font></spring:bind></th>
			</tr>
			<tr>
				<th>Telp Rumah 2</th>
				<th><spring:bind path="cmd.contactPerson.area_code_rumah2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.contactPerson.telpon_rumah2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				<th>Handphone 1</th>
				<th><spring:bind path="cmd.contactPerson.no_hp">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <font color="#CC3300">*</font></th>
				<th>Email</th>
				<th><spring:bind path="cmd.contactPerson.email">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="40" maxlength="50"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				<th>Handphone 2</th>
				<th colspan="3"><spring:bind path="cmd.contactPerson.no_hp2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th rowspan="6">b.</th>
				<th rowspan="6">Alamat Kantor</th>
				<th rowspan="6"><spring:bind path="cmd.contactPerson.alamat_kantor">
					<textarea cols="40" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>${status.value }</textarea>
				</spring:bind></th>
				<th>Kode Pos</th>
				<th><spring:bind path="cmd.contactPerson.kd_pos_kantor">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="10"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>Kota</th>
				<th><spring:bind path="cmd.contactPerson.kota_kantor"><br>
				<input type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
		    	<span id="indicator_kantor" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
			</spring:bind>   
				</th>
			</tr>
			<tr><th></th><th></th></tr>
			<tr>
				<th>Telp Kantor 1</th>
				<th><spring:bind path="cmd.contactPerson.area_code_kantor">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.contactPerson.telpon_kantor">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>Telp Kantor 2</th>
				<th><spring:bind path="cmd.contactPerson.area_code_kantor2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.contactPerson.telpon_kantor2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>No Fax</th>
				<th><spring:bind path="cmd.contactPerson.area_code_fax">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.contactPerson.no_fax">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th colspan="5" class="subtitle" style="text-align: left;">Alamat Penagihan : <spring:bind path="cmd.addressbilling.tagih">
					<select name="${status.expression }" onChange="data_penagihan();"
						>
						<option value="2">Alamat Rumah</option>
						<option value="3">Alamat Kantor</option>
						<option value="1">Lain - Lain</option>
					</select>
				 </spring:bind></th>
			</tr>
			<tr>
				<th rowspan="7">c.</th>
				<th rowspan="7">Alamat Penagihan / <br>
				Korespondensi</th>
				<th rowspan="6"><spring:bind path="cmd.addressbilling.msap_address">
					<textarea cols="40" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>${status.value }</textarea>
				<font color="#CC3300">*</font></spring:bind></th>
				<th>Kode Pos</th>
				<th><spring:bind path="cmd.addressbilling.msap_zip_code">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="20" maxlength="10"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>Kota</th>
				<th>
				<spring:bind path="cmd.addressbilling.kota_tgh"><br>
				<input type="text" name="${status.expression }" id="${status.expression }" value="${status.value }" onfocus="this.select();"
				<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
		    	 <span id="indicator_tagih" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
			</spring:bind>
				</th>
			</tr>	
			<tr>
				<th>Negara</th>
				<th>
					<spring:bind path="cmd.addressbilling.lsne_id">
						<select name="${status.expression}">
							<c:forEach var="d" items="${select_negara}">
								<option
									<c:if test="${status.value eq d.key}"> SELECTED </c:if>
									value="${d.key}">${d.value}</option>
							</c:forEach>
						</select>
						<font color="#CC3300">*</font>
					</spring:bind>
				</th>
			</tr>			
			<tr><th></th><th></th></tr>
			<tr>
				<th>No Telepon1</th>
				<th><spring:bind path="cmd.addressbilling.msap_area_code1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.addressbilling.msap_phone1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				<font color="#CC3300">*</font></spring:bind></th>
			</tr>
			<tr>
				<th>No Telepon2</th>
				<th><spring:bind path="cmd.addressbilling.msap_area_code2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.addressbilling.msap_phone2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>No Telepon3</th>
				<th><spring:bind path="cmd.addressbilling.msap_area_code3">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.addressbilling.msap_phone3">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				<th>Handphone 1</th>
				<th><spring:bind path="cmd.addressbilling.no_hp">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind><br/><font color="#CC3300">* apabila tidak ada no hp, harap isi dengan 0000</font></th>
				<th>No Fax</th>
				<th><spring:bind path="cmd.addressbilling.msap_area_code_fax1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="5" maxlength="4"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind> <spring:bind path="cmd.addressbilling.msap_fax1">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="12" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th></th>
				<th>Handphone 2</th>
				<th><spring:bind path="cmd.addressbilling.no_hp2">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="42" maxlength="20"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind><br/><font color="#CC3300">* apabila tidak ada no hp, harap isi dengan 0000</font></th>
				<th>Email</th>
				<th><spring:bind path="cmd.addressbilling.e_mail">
					<input type="text" name="${status.expression}"
						value="${status.value }" size="40" maxlength="50"
						<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
				</spring:bind></th>
			</tr>
			<tr>
				<th>20.</th>
				<th>Hubungan Calon Tertanggung dengan Calon Pemegang Polis (Badan Usaha)</th>
				<th colspan="3"><select name="pemegang.lsre_id" 
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<c:forEach var="relasi" items="${select_relasi_badan_usaha}">
						<option
							<c:if test="${cmd.pemegang.lsre_id eq relasi.ID}"> SELECTED </c:if>
							value="${relasi.ID}">${relasi.RELATION}</option>
					</c:forEach>
				</select>
				 <font color="#CC3300">*</font></th>
			</tr>
			</c:if>
			<tr>
				<th class="subtitle" colspan="5"></th>
			</tr>		
            <tr> 
            <td colspan="9"> 
			 <p align="left"><b> <font face="Verdana" size="2" color="#CC3300">Note : * Wajib diisi</font></b></p>
			</td>
          </tr>
          <c:if test="${cmd.tertanggung.lspd_id eq 2 or cmd.tertanggung.lspd_id eq 27 or cmd.tertanggung.lspd_id eq 209}">
          	<c:if test="${not empty cmd.pemegang.reg_spaj}">	
	          <tr>
	          	 <td colspan="2">
	          	 		<input type="checkbox" class="noBorder" name="edit_pemegang" value="${cmd.pemegang.edit_pemegang}" <c:if test="${cmd.pemegang.edit_pemegang eq 1}"> 
										checked</c:if> size="30" onClick="edit_onClick();"> Telah dilakukan edit
						<spring:bind path="cmd.pemegang.edit_pemegang"> 
					          		<input type="hidden" name="${status.expression}" value="${cmd.pemegang.edit_pemegang}"  size="30" style='background-color :#D4D4D4'readOnly>
						</spring:bind>
				 </td>	
				 <td colspan="7">
				 	<select name="pemegang.edit_pemegang" >
						<c:if test="${cmd.tertanggung.lspd_id ne 27}">
							<c:forEach var="a" items="${select_kriteria_kesalahan}">									
								<option
									<c:if test="${cmd.pemegang.kriteria_kesalahan eq a.KESALAHAN}"> SELECTED </c:if>
									value="${a.KESALAHAN}">${a.KESALAHAN}
								</option>									
							</c:forEach>
						</c:if>
						<c:if test="${cmd.tertanggung.lspd_id eq 27}">
							<c:forEach var="a" items="${select_kriteria_kesalahan}">
								<c:if test="${a.ID ne 3 and a.ID ne 2}">									
									<option
										<c:if test="${cmd.pemegang.kriteria_kesalahan eq a.KESALAHAN}"> SELECTED </c:if>
										value="${a.KESALAHAN}">${a.KESALAHAN}
									</option>
								</c:if>									
							</c:forEach>
						</c:if>
					</select>
				</td> 
	          </tr>
	          <tr>
	          	<td colspan="9">&nbsp;</td>
	          </tr>
	        </c:if>
          </c:if>
			<tr>
				<td colspan="5" style="text-align: center;">
				<input type="hidden" name="hal" value="${halaman}">
				 <spring:bind path="cmd.pemegang.indeks_halaman">
					<input type="hidden" name="${status.expression}"
						value="${halaman-1}" size="20">
				 </spring:bind>
				<input type="submit" name="_target${halaman+1}"
					value="Next &raquo;" onClick="next()" 
				accesskey="N" onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
					 <input
					type="hidden" name="_page" value="${halaman}">
				 </th>
			</tr>
		</table>
		</td>
	</tr>
</table>
</form>
<ajax:autocomplete
				  source="pemegang.kota_rumah"
				  target="pemegang.kota_rumah"
				  baseUrl="${path}/servlet/autocomplete?s=pemegang.kota_rumah&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_rumah"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />
<ajax:autocomplete
				  source="contactPerson.kota_rumah"
				  target="contactPerson.kota_rumah"
				  baseUrl="${path}/servlet/autocomplete?s=contactPerson.kota_rumah&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_rumah"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />
<ajax:autocomplete
				  source="contactPerson.kota_kantor"
				  target="contactPerson.kota_kantor"
				  baseUrl="${path}/servlet/autocomplete?s=contactPerson.kota_kantor&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_kantor"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />
<ajax:autocomplete
				  source="pemegang.kota_kantor"
				  target="pemegang.kota_kantor"
				  baseUrl="${path}/servlet/autocomplete?s=pemegang.kota_kantor&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_kantor"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />
	<ajax:autocomplete
				  source="addressbilling.kota_tgh"
				  target="addressbilling.kota_tgh"
				  baseUrl="${path}/servlet/autocomplete?s=addressbilling.kota_tgh&q=wilayah"
				  className="autocomplete"
				  indicator="indicator_tagih"
				  minimumCharacters="2"
				  parser="new ResponseXmlToHtmlListParser()" />		

<script type="text/javascript">	
	if(frmParam.elements['datausulan.jenis_pemegang_polis'].value != 1){
		agamaChange('${cmd.pemegang.lsag_id}');
	}
</script>	  
</body>
<%@ include file="/include/page/footer.jsp"%>