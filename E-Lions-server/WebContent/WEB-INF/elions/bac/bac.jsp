<%@ include file="/include/page/header.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	hideLoadingMessage();
	
	function balikKeBac(){
		alert ("Halaman Akan Kembali Ke BAC");
	  	window.location='${path }/bac/bac.htm';
	}
	
	function stat(){
		spaj = document.frmParam.spaj.value;
		if(spaj==''){
			alert('Harap cari SPAJ terlebih dahulu');
		}else{
			createLoadingMessage();
			document.getElementById('infoFrame').src='${path }/uw/status.htm?pos=1&spaj='+spaj;
		}
	}
	
	function disabledTransferButton(){
		var spajListDisabled = document.getElementById('spajListDisabledForTransfer').value;
		if( spajListDisabled.match(spaj) )
		{
			//document.getElementById('transfer').disabled = false;
		}
		else 
		{
			//document.getElementById('transfer').disabled = true;
		}
	}
	
	function buttonLinks(str){
		if(str=='cari'){
			spaj = document.frmParam.spaj.value;
			if(spaj==''){
				alert('Harap cari SPAJ terlebih dahulu');
			}else{
				var spajList4EditButton = document.getElementById('spajList4EditButton').value;
				var spajListDisabled = document.getElementById('spajListDisabledForTransfer').value;
				var ldeId = document.getElementById('ldeId').value;
				var jnBank = document.getElementById('jnBank').value;
				if(jnBank ==2)
				{
					if( ldeId == 11 )
					{
						//document.getElementById('transfer').disabled = false;
					}
					else
					{
						if( spajListDisabled.match(spaj) )
						{
							//document.getElementById('transfer').disabled = false;
						}
						else 
						{
							//document.getElementById('transfer').disabled = true;
						}
					}
				}
				if( spajList4EditButton.match(spaj) )
				{
					alert('Maaf, Spaj tidak bisa diedit krn sudah diotorisasi / approved');
				}
				else
				{
					document.getElementById('infoFrame').src='${path}/bac/multi.htm?window=testRedirect&spaj='+document.frmParam.spaj.value;
				}
				
			}
		}else{
			spaj = document.frmParam.spaj.value;
			if(spaj=='' && str!='cekphone'){
				alert('Harap cari SPAJ terlebih dahulu');
			}else{
			
				switch (str) {
				case "nik" :
					if  ((document.frmParam.kodebisnis.value == 138)  ||  (document.frmParam.kodebisnis.value == 140)  || (document.frmParam.kodebisnis.value == 141)  || (document.frmParam.kodebisnis.value == 148)  || (document.frmParam.kodebisnis.value == 149)  || (document.frmParam.kodebisnis.value == 156)  ||(document.frmParam.kodebisnis.value == 139) ||((document.frmParam.kodebisnis.value == 142 && document.frmParam.numberbisnis.value == 4) ) ||((document.frmParam.kodebisnis.value == 143 && document.frmParam.numberbisnis.value == 2) ) ||((document.frmParam.kodebisnis.value == 158 && document.frmParam.numberbisnis.value == 4) )  ||((document.frmParam.kodebisnis.value == 158 && document.frmParam.numberbisnis.value == 7) )){
						popWin('${path}/bac/nik.htm?sts=insert&spaj='+spaj, 500, 800);
					}else{
						alert('Nik tidak dapat diisi untuk polis ini');					
					}
					break;
				case "titipanpremi" :
					//validasi akses titipan premi, ada di controller titipan premi
					popWin('${path}/ttppremi/titipan_premi.htm?sts=insert&editSPAJ='+spaj, 500, 800);
					
					break;
				case "simas" :
					if (document.frmParam.kodebisnis.value == 161)
					{
						popWin('${path}/bac/editttgsimas.htm?sts=insert&showSPAJ='+spaj, 500, 800);
					}else{
						alert("Tertanggung Simas tidak perlu diisi untuk produk ini, proses selanjutnya adalah Transfer");
					}
					break;		
				case "hcp" :
					if (document.frmParam.kodebisnis.value != 161)
					{
						ajaxManager.CountPeserta(spaj , 
							{callback:function(value) {
									DWRUtil.useLoadingMessage();
									if(value>0){
										alert("Sebelum mengisi data tertanggung tambahan, Silakan dipastikan terlebih dahulu data tertanggung utamanya sudah diisi dengan benar.");
										popWin('${path}/bac/ttghcp.htm?sts=insert&showSPAJ='+spaj, 500, 800);
									}else{ 
										alert("Tidak bisa mengisi data Smart Medicare/Eka Sehat/HCP Family karena belum mengambil plan BASIC, Silahkan melakukan penginputan plan BASIC pada halaman penginputan spaj utama terlebih dahulu.");
										
									}	
							 	},
							  timeout:15000,
							  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
						});
						
						//if(document.frmParam.kodebisnis.value==189 || document.frmParam.kodebisnis.value==183 || document.frmParam.kodebisnis.value==178 || document.frmParam.kodebisnis.value==193){
						//		popWin('${path}/bac/ttghcp.htm?sts=insert&showSPAJ='+spaj, 500, 800);
						//}else {
						//	if (eval(document.frmParam.jml_peserta.value) > 0){
						//		popWin('${path}/bac/ttghcp.htm?sts=insert&showSPAJ='+spaj, 500, 800);
						//	}else{
						//		alert("Tidak bisa mengisi data Smart Medicare/Eka Sehat/HCP Family karena belum mengambil plan BASIC, Silahkan melakukan penginputan plan BASIC pada halaman penginputan spaj utama terlebih dahulu.");
						//	}
						//}
					}else{
						alert("Produk ini tidak bisa mengisi data Smart Medicare/Eka Sehat/HCP Family");
					}
					break;		
				case "info" :
					document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj;
					break;		
				case "multiview" : 
					popWin('${path}/common/util.htm?window=multi_doc_list&spaj='+spaj, 600, 900);
					break;	
				case "reffbii" :
					if(confirm("Apakah anda akan input referral others BII?\nApabila anda adalah user/spaj ini dari BANK SINARMAS / ASURANSI SINARMAS / SINARMAS SEKURITAS/ BANK SINARMAS SYARIAH / BANK VICTORIA / BANK HARDA / BANK BRI / BANK BJB / BANK JATIM / BANK BUKOPIN / BANK BUKOPIN SYARIAH, harap pilih CANCEL!")){
						//alert('${path}/bac/reff_bank.htm?window=main&reffothers=y&spaj='+spaj);
						popWin('${path}/bac/reff_bank.htm?window=main&reffothers=y&spaj='+spaj, 600, 700);
					}else{
						//alert('${path}/bac/reff_bank.htm?window=main&reffothers=n&spaj='+spaj);
						popWin('${path}/bac/reff_bank.htm?window=main&reffothers=n&spaj='+spaj, 600, 700);
					}
					break;
				case "reffdmtm" :
					popWin('${path}/bac/reff_bank.htm?window=reff_dmtm&spaj='+spaj, 400, 700);
					break;
				case "spvdmtm" :
					popWin('${path}/bac/reff_bank.htm?window=spv_dmtm&spaj='+spaj, 400, 700);
					break;	
				case "uwinfo" :
					document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=uwinfo&spaj='+spaj;
					break;
// 				case "aksepManual" :
// 					popWin('${path}/bac/pending.htm?spaj='+spaj, 250, 375);
// 					break;
				case "rekening" :
					popWin('${path}/uw/viewer.htm?window=rekening&spaj='+spaj, 400, 700);
					break;
				case "view" :
					popWin('${path}/uw/viewer.htm?window=viewerkontrol&spaj='+spaj, 400, 550);
					break;
				case "summary" :
					//popWin('${path}/bac/summary.htm', 500, 700);
					document.getElementById('infoFrame').src='${path}/bac/summary.htm';
					break;
				case "ttd" :
					popWin('${path}/bac/sign.htm?window=main&spaj='+spaj, '${panjang}', '${lebar}');
					break;
				case "agen" :
					popWin('${path}/bac/agen.htm?spaj='+spaj, 360, 480);
					break;
				case "batalkan" :
					popWin('${path}/bac/cancelBac.htm?spaj='+spaj, 500, 700);
					break;
				case "checklist" :
					document.getElementById('infoFrame').src = '${path}/checklist.htm?lspd_id=1&reg_spaj='+spaj;
					break;
				case "editagen" :
					popWin('${path}/bac/editagenpenutup.htm?spaj='+spaj, 500, 700);
					break;
				case "kesehatan" :
					if  ((document.frmParam.kodebisnis.value == 155) || ((document.frmParam.kodebisnis.value == 158 && document.frmParam.numberbisnis.value == 5) ) )
					{
						popWin('${path}/bac/keterangan_kesehatan.htm?spaj='+spaj, 500, 700);
					}else{
						alert("Data Kesehatan hanya diinput untuk produk Platinum Save");
					}
					break;	
				case "tglTerimaAdmin" :
					//popWin('${path }/uw/viewer.htm?window=editTglTrmKrmAdmin&spaj='+spaj+'&show=0', 200, 350);
					popWin('${path }/uw/viewer.htm?window=editTglTrmKrmSpaj&spaj='+spaj+'&show=4', 150, 350);
				break;
				case "tglTerimaSpaj" :
					popWin('${path }/uw/viewer.htm?window=editTglTrmKrmSpaj&spaj='+spaj+'&show=0', 150, 350);
				break;
				case "limit" :
					popWin('${path}/common/menu.htm?frame=view_limit_uw', 600, 800);
					break;
				case "simultan" :
					popWin('${path }/uw/viewer.htm?window=view_simultan', 600, 800);
					break;
				case "download_spaj" :
					spaj = document.frmParam.spaj.value;
					document.getElementById('infoFrame').src='${path}/bac/multi.htm?window=download_spaj&spaj='+spaj;
					break;		
				case "medis" :
					spaj = document.frmParam.spaj.value;
					document.getElementById('infoFrame').src='${path}/uw/medical_new.htm?spaj='+spaj;
					break;
				case "med_quest" :
					spaj = document.frmParam.spaj.value;
					document.getElementById('infoFrame').src='${path}/uw/medical_quest.htm?spaj='+spaj;
					break;	
				case "e-spajonline" :
// 					if (document.frmParam.quest.value > 0){
// 						popWin('${path}/reports/espajonline.pdf?spaj='+spaj, 700, 1200);}
// 					else{
// 						alert("- Harap Isi QUESTIONARE Terlebih Dahulu");
// 					}
// 					break;
					if (document.frmParam.quest.value > 0){
						ajaxManager.listcollect(document.frmParam.spaj.value , 'region',
						{callback:function(map) {
					
							DWRUtil.useLoadingMessage();
							document.frmParam.koderegion.value=map.LSRG_NAMA;
							document.frmParam.kodebisnis.value = map.LSBS_ID;
							document.frmParam.numberbisnis.value = map.LSDBS_NUMBER;
							document.frmParam.jml_peserta.value = map.jml_peserta;
							document.frmParam.cab_bank.value=map.CAB_BANK;
							document.frmParam.jn_bank.value=map.JN_BANK;
							document.frmParam.lus_id.value = map.LUS_ID;
							document.frmParam.quest.value = map.quest;
							document.frmParam.syariah.value = map.syariah;
																		
						if (document.frmParam.syariah.value == 1){
							popWin('${path}/reports/espajonlinesyariah.pdf?spaj='+spaj, 700, 1200);
						} else{
							popWin('${path}/reports/espajonline.pdf?spaj='+spaj, 700, 1200);
						}
						   	},
						  	timeout:15000,
						  	errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
						});
					}else{
						alert("- Harap Isi QUESTIONARE Terlebih Dahulu");
					}
					break;
//				case "autoaccept" :
//					spaj = document.frmParam.spaj.value;
//					document.getElementById('infoFrame').src='${path}/bac/multi.htm?window=autoAccept&no_reg='+spaj+'&flag_reg=1';
//					break;	
				case "konfirmasiagency" :
// 					if (document.frmParam.quest.value >0){
// 						popWin('${path}/reports/eAgency.pdf?spaj='+spaj, 700, 1200);}
// 					else{
// 						alert("-Harap Isi QUESTIONARE Terlebih Dahulu ");
// 					}
// 					break;
					if (document.frmParam.quest.value > 0){
						ajaxManager.listcollect(document.frmParam.spaj.value , 'region',
						{callback:function(map) {
					
							DWRUtil.useLoadingMessage();
							document.frmParam.koderegion.value=map.LSRG_NAMA;
							document.frmParam.kodebisnis.value = map.LSBS_ID;
							document.frmParam.numberbisnis.value = map.LSDBS_NUMBER;
							document.frmParam.jml_peserta.value = map.jml_peserta;
							document.frmParam.cab_bank.value=map.CAB_BANK;
							document.frmParam.jn_bank.value=map.JN_BANK;
							document.frmParam.lus_id.value = map.LUS_ID;
							document.frmParam.quest.value = map.quest;
							document.frmParam.syariah.value = map.syariah;
																		
						if (document.frmParam.syariah.value == 1){
							popWin('${path}/reports/ekonfirmasisyariah.pdf?spaj='+spaj, 700, 1200);
						} else{
							popWin('${path}/reports/eAgency.pdf?spaj='+spaj, 700, 1200);
						}
						   	},
						  	timeout:15000,
						  	errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
						});
					}else{
						alert("-Harap Isi QUESTIONARE Terlebih Dahulu");
					}
					break;
				case "questionare" :
				spaj = document.frmParam.spaj.value;
						/* if (document.frmParam.quest.value >0)
						{
						alert("- Questionaire Medis Hanya Bisa Diproses/Diinput 1x . ");}
						else{
							document.getElementById('infoFrame').src='${path}/bac/questionarenew.htm?spaj='+spaj;
						} */
					document.getElementById('infoFrame').src='${path}/bac/questionarenew.htm?flag=1&spaj='+spaj;
					/* document.getElementById('infoFrame').src='${path}/bac/questionareDMTM.htm?spaj='+spaj; */
					break; 
				case "autoDebet"	:
					spaj = document.frmParam.spaj.value;
					if(confirm("Apakah Anda yakin akan menjalankan Proses AutoDebet Premi Pertama?")){
						document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=autoDebetPPertama&spaj='+spaj;
					}
					break;	
				/* case "cam" :
					document.getElementById('infoFrame').src='${path}/uw/endorsenonmaterial.htm?window=cam&spaj='+spaj;
				break; */
				case "referensi" :
					//spaj = document.frmParam.spaj.value;
					document.getElementById('infoFrame').src='${path}/bac/multi.htm?window=referensi';
					break;	
				case "psn" :
					//spaj = document.frmParam.spaj.value;
					if  ((document.frmParam.kodebisnis.value == 152)   ||  (document.frmParam.kodebisnis.value == 153) ||  (document.frmParam.kodebisnis.value == 160)  || (document.frmParam.kodebisnis.value == 166)  || (document.frmParam.kodebisnis.value == 199)  || (document.frmParam.kodebisnis.value == 200)  || (document.frmParam.kodebisnis.value == 202)){
					document.getElementById('infoFrame').src='${path}/bac/multi.htm?window=psn&spaj='+spaj;
					}else{
					alert("Hanya Untuk Produk UNIT LINK SYARIAH Yang Mengambil PROGRAM SOSIAL NASABAH(SMiLe RELIGI)");					
					}
					break;		
				case "upload_nb" :
					spaj = document.frmParam.spaj.value;
					document.getElementById('infoFrame').src='${path}/uw/upload_nb.htm?reg_spaj='+spaj;
					break;		
				case "upload_nb_exist" :
					spaj = document.frmParam.spaj.value;
					document.getElementById('infoFrame').src='${path}/uw/upload_exist.htm?reg_spaj='+spaj;
					break;
				case "surat_konfirm" :
					spaj = document.frmParam.spaj.value;
					popWin('${path}/reports/konfirmSyrh.pdf?spaj='+spaj, 700, 1200);
					break;	
				case "questionare_dmtm" :
						popWin('${path}/bac/questionareSimple.htm?spaj='+spaj+'&show=4', 400, 700);
						break;
				case "status" :
						spaj = document.frmParam.spaj.value;
						document.getElementById('infoFrame').src='${path}/bac/status_bas.htm?spaj='+spaj;
						break;
				case "view_file_gadget" :
						spaj = document.frmParam.spaj.value;
						document.getElementById('infoFrame').src='${path}/common/util.htm?window=file_gadget&spaj=' + spaj;
						break;
				case "cekphone" :	
						popWin('${path}/uw/uw.htm?window=ceknohp', 400, 700);
						break;
				case "promo_aktiv" :		
// 						document.getElementById('infoFrame').src='${path}/bac/multi.htm?window=promo_aktiv';
// 						break;
						popWin('${path}/bac/multi.htm?window=promo_aktiv&spaj=' + spaj, 250, 700);
						break;
				case "profile_risk" :		
 						document.getElementById('infoFrame').src='${path}/bac/multi.htm?window=profile_risk&spaj=' + spaj + '&viewonly=0';
// 						break;
					//	popWin('${path}/bac/multi.htm?window=profile_risk&spaj=' + spaj + '&viewonly=0', 800, 1200);
						break;
						
				}				
			}
		}
	}
	
	function trans(){
		spaj = document.frmParam.spaj.value;
		if(spaj==''){
			alert('Harap cari SPAJ terlebih dahulu');
		}else{
			//if (document.frmParam.jn_bank.value == 2 || document.frmParam.jn_bank.value == 3){
			//if (document.frmParam.jn_bank.value == 3){
			//	document.getElementById('infoFrame').src='${path}/bac/transferbactouwbanksinarmas.htm?spaj='+document.frmParam.spaj.value; 
			//}else{
			document.getElementById('infoFrame').src='${path}/bac/transferbactouw.htm?spaj='+document.frmParam.spaj.value; 			
			//}
		}
	}

	function awal(){
		cariregion(document.frmParam.spaj.value,'region');
		setFrameSize('infoFrame', 64);
		spajAwal(0);
	}
	
	function cariregion(spaj,nama)
	{
			var spajListDisabled = document.getElementById('spajListDisabledForTransfer').value;
			var ldeId = document.getElementById('ldeId').value;
			var jnBank = document.getElementById('jnBank').value;
			if( jnBank == 2)
			{
				if( ldeId == 11 )
				{
					//document.getElementById('transfer').disabled = false;
				}
				else
				{
					if( spajListDisabled.match(spaj) )
					{
						//document.getElementById('transfer').disabled = false;
					}
					else 
					{
						//document.getElementById('transfer').disabled = true;
					}
				}
			}
			ajaxManager.listcollect(spaj , nama,
			{callback:function(map) {
		
				DWRUtil.useLoadingMessage();
				document.frmParam.koderegion.value=map.LSRG_NAMA;
				document.frmParam.kodebisnis.value = map.LSBS_ID;
				document.frmParam.numberbisnis.value = map.LSDBS_NUMBER;
				document.frmParam.jml_peserta.value = map.jml_peserta;
				document.frmParam.cab_bank.value=map.CAB_BANK;
				document.frmParam.jn_bank.value=map.JN_BANK;
				document.frmParam.lus_id.value = map.LUS_ID;
				document.frmParam.quest.value = map.quest;
				document.frmParam.syariah.value = map.syariah;
				
			   },
			  timeout:15000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}

	function copyToClipboard(txt){
	
		if( window.clipboardData && clipboardData.setData ){
		
			clipboardData.setData("Text", txt);
		}
		else{
		 alert("Internet Explorer required"); 
		}
	}
	
	function run(){

		spaj = document.frmParam.spaj.value;
		if(spaj==''){
			alert('Harap cari SPAJ terlebih dahulu');
		}else{
			var shell = new ActiveXObject("WScript.Shell"); 
			var commandtoRun = "\\\\aplikasi\\lions\\secan4400f.exe";
		   
			if (shell){		
			 shell.run(commandtoRun); 
			} 
			else{ 
				alert("program or file doesn't exist on your system."); 
			}
		}

	}
	
	function copyAndRun(){ 
	 	txt = document.getElementById("spaj").value;
	 	copyToClipboard(txt);
	 	run();		
	}	
	
	function spajAwal(nilai){
		if(confirm("Apakah anda ingin menginput spaj lama?Jika iya tekan OK\nJika ingin menginput spaj Agustus 2014 / SIO / GIO / FULL MARET 2015 tekan CANCEL")){
			nilai = 1;
			document.getElementById('infoFrame').src='${path}/bac/edit.htm?data_baru=true&flagAwal='+nilai+'&jenis_pemegang_polis='+document.getElementById('jenis_pemegang_polis').value;
		}else{
			nilai = 1;
			document.getElementById('infoFrame').src='${path}/bac/editspajnew.htm?data_baru=true&flagAwal='+nilai+'&jenis_pemegang_polis='+document.getElementById('jenis_pemegang_polis').value;
		}
		
// 		nilai = 1;
// 		document.getElementById('infoFrame').src='${path}/bac/editspajnew.htm?data_baru=true&flagAwal='+nilai+'&jenis_pemegang_polis='+document.getElementById('jenis_pemegang_polis').value;
	}
</script>
<body 
	onload="awal(); setFrameSize('infoFrame', 85);"
	onresize="setFrameSize('infoFrame', 85);" style="height: 100%;">

<form name="frmParam" method="post">
<div class="tabcontent">
<table class="entry2" style="width: 98.4%;">
	<tr>
		<th>SPAJ</th>
		<td>
			<input type="hidden" name="jenis_pemegang_polis" id="jenis_pemegang_polis"  value=""/>
			
			<input type="button" value="Refresh" name="refresh" onclick="document.frames['infoFrame'].location.reload()" style="background-color: yellow;">
			
			<input type="button" value="Input" name="info" id="info"
				onclick="spajAwal(1)" 
				accesskey="I" onmouseover="return overlib('Input SPAJ Baru', AUTOSTATUS, WRAP);" onmouseout="nd();"
				<c:if test="${sessionScope.currentUser.flag_approve eq 1}"> disabled="true" </c:if>>

			<%-- FIXME: Yusuf 24 jul 2011, enable bila sudah selesai redesign menu input spaj --%>
			<c:if test="${sessionScope.currentUser.lus_id eq 574}">
				<input type="button" value="Input2" onclick="document.getElementById('infoFrame').src='${path}/bac/editnew.htm';">
			</c:if>
			
			<select name="spaj" onChange="cariregion(this.options[this.selectedIndex].value,'region');" id="spaj">
				<option value="">[--- Silahkan Pilih/Cari SPAJ ---]</option>
				<c:forEach var="s" items="${daftarSPAJ}">
					<option value="${s.REG_SPAJ }" style="background-color: ${s.OTORISASI_BG};" 
						<c:if test="${s.REG_SPAJ eq param.spaj }">selected<c:set var="ada" value="ada" /></c:if>>${s.SPAJ_FORMATTED}
					- ${s.POLICY_FORMATTED }</option>
				</c:forEach>
			</select>
			
			<input type="button" value="Cari" name="search"
				onclick="popWin('${path}/uw/spajgutri.htm?posisi=1&win=bac&spajList4EditButton=${spajList4EditButton}', 350, 450);"
				accesskey="C" onmouseover="return overlib('Cari SPAJ', AUTOSTATUS, WRAP);" onmouseout="nd();">
				 <!-- &search=yes -->

			<input type="button" value="Edit" name="info" onclick="return buttonLinks('cari');" 
				accesskey="T" onmouseover="return overlib('Edit SPAJ', AUTOSTATUS, WRAP);" onmouseout="nd();"
				<c:if test="${sessionScope.currentUser.jn_bank eq 2 and sessionScope.currentUser.flag_approve eq 1}"> disabled="true" </c:if>>
				<!--${path }/uw/view.htm?showSPAJ='+document.frmParam.spaj.options[document.frmParam.spaj.selectedIndex].value;"> document.getElementById('infoFrame').src='${path}/bac/edit.htmshowSPAJ='+spaj; -->			

			<c:if test="${empty sessionScope.currentUser.cab_bank and sessionScope.currentUser.jn_bank ne 4 and sessionScope.currentUser.lus_id ne 2661}">
				<input type="button" value="TTD" name="ttd"
					onclick="return buttonLinks('ttd');"
					accesskey="D" onmouseover="return overlib('Upload Tanda Tangan Nasabah', AUTOSTATUS, WRAP);" onmouseout="nd();">

				<input type="button" value="U/W Info" name="uwinfo"
					onclick="return buttonLinks('uwinfo');"
					accesskey="W" onmouseover="return overlib('U/W Info', AUTOSTATUS, WRAP);" onmouseout="nd();">
					
<!-- 				<input type="button" value="Aksep Manual" name="aksepManual"
 					onclick="return buttonLinks('aksepManual');"
 					accesskey="A" onmouseover="return overlib('Aksep Manual (Worksite)', AUTOSTATUS, WRAP);" onmouseout="nd();">
-->
					
				<input type="button" value="Agen" name="editagen"
					onclick="return buttonLinks('editagen');"
					accesskey="G" onmouseover="return overlib('Edit Kode Agen', AUTOSTATUS, WRAP);" onmouseout="nd();">
	
				<!-- <input type="button" value="Tgl Terima Spaj" name="tglTerima"
					onclick="return buttonLinks('tglTerimaSpaj');"
					accesskey="P" onmouseover="return overlib('Edit Tanggal Terima Spaj', AUTOSTATUS, WRAP);" onmouseout="nd();"> -->

				<input type="button" value="Tgl Terima Admin" name="tglAdmin"
					onclick="return buttonLinks('tglTerimaAdmin'); "
					accesskey="P" onmouseover="return overlib('Edit Tanggal Terima Admin', AUTOSTATUS, WRAP);" onmouseout="nd();">
					
				<%--<input type="button" value="Scan" name="copySpaj"	onclick="copyAndRun();">--%>
				
				<input type="button" value="Upload Scan" name="upload_nb"	
				onclick="buttonLinks('upload_nb');">
				
				<input type="button" value="Copy Scan" name="upload_nb_exist"	
				onclick="buttonLinks('upload_nb_exist');">
				
				<input type="button" value="Cek Agen" name="cekAgen" 
				onclick="popWin('${path}/uw/viewer.htm?window=cek_agen', 350, 600);">
				
				<input type="button" value="autoDebet" name="autoDebet" 
				onclick="return buttonLinks('autoDebet');">
				
				<%--<input type="button" value="Validasi Cam" name="cam"
					onclick="return buttonLinks('cam'); "
					accesskey="P" onmouseover="return overlib('Capture With Cam', AUTOSTATUS, WRAP);" onmouseout="nd();">--%>
			</c:if>
			
			<c:if test="${sessionScope.currentUser.jn_bank eq 4 and sessionScope.currentUser.lus_id ne 2661}">
				<input type="button" value="U/W Info" name="uwinfo"
					onclick="return buttonLinks('uwinfo');"
					accesskey="W" onmouseover="return overlib('U/W Info', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<!-- <input type="button" value="Tgl Terima Spaj" name="tglTerima"
					onclick="return buttonLinks('tglTerimaSpaj');"
					accesskey="P" onmouseover="return overlib('Edit Tanggal Terima Spaj', AUTOSTATUS, WRAP);" onmouseout="nd();"> -->
				<input type="button" value="Tgl Terima Admin" name="tglAdmin"
					onclick="return buttonLinks('tglTerimaAdmin'); "
					accesskey="P" onmouseover="return overlib('Edit Tanggal Terima Admin', AUTOSTATUS, WRAP);" onmouseout="nd();">
			</c:if>
			
		</td>
	</tr>
	<tr>
		<th rowspan="2">Proses</th>
		<td colspan="2">
			<input type="hidden" name="koderegion" >
			<input type="hidden" id="ldeId" value="${ldeId}" >
			
			<input type="hidden" name="kodebisnis" >
			<input type="hidden" name="numberbisnis" >
			<input type="hidden" name="jml_peserta">
			<input type="hidden" name="lus_id">
			<input type="hidden" name="quest">
			<input type="hidden" name="syariah">
			<input type="hidden" name="cab_bank">
			<input type="hidden" name="jn_bank">
			<input type="hidden" name="user_id" value= "${user_id}">
			<input type="hidden" name="cabang_bank" value ="${cabang_bank}">
			<input type="button" value="Upload" name="upload" disabled="disabled" style="display: none;"
			onclick="document.getElementById('infoFrame').src='${path}/bac/upload.htm';" 
			accesskey="U" onmouseover="return overlib('Upload Polis Via Excel', AUTOSTATUS, WRAP);" onmouseout="nd();">
			
			<c:if test="${sessionScope.currentUser.jn_bank ne 4 and sessionScope.currentUser.lus_id ne 2661}">
			<input type="button" value="Referral" name="reffbii"	onclick="return buttonLinks('reffbii');" 
				accesskey="R" onmouseover="return overlib('Insert Referral', AUTOSTATUS, WRAP);" onmouseout="nd();"
				<c:if test="${sessionScope.currentUser.jn_bank eq 2 and sessionScope.currentUser.flag_approve eq 1}"> disabled="true" </c:if>>
			</c:if>		
			<c:if test="${sessionScope.currentUser.lus_id ne 2661}">	
			<input type="button" value="Transfer" name="transfer" onclick="trans();" id="transfer"
				accesskey="F" onmouseover="return overlib('Transfer', AUTOSTATUS, WRAP);" onmouseout="nd();"
				<c:if test="${sessionScope.currentUser.jn_bank eq 3 and sessionScope.currentUser.flag_approve eq 0 and sessionScope.currentUser.lus_id ne 2661}"> disabled="true" </c:if>
				</c:if>
				
			
			<c:if test="${empty sessionScope.currentUser.cab_bank and sessionScope.currentUser.jn_bank ne 4 and sessionScope.currentUser.lus_id ne 2661}">
				<%--<input type="button" value="Titipan Premi" name="ttp" onclick="return buttonLinks('titipanpremi');"
					accesskey="M" onmouseover="return overlib('Titipan Premi', AUTOSTATUS, WRAP);" onmouseout="nd();"> --%>
					
				<input type="button" value="Rekening" name="rekening"
					onclick="return buttonLinks('rekening');"
					accesskey="K" onmouseover="return overlib('Rekening', AUTOSTATUS, WRAP);" onmouseout="nd();">
					
				<input type="button" value="View" name="view"
					onclick="return buttonLinks('view');"
					accesskey="V" onmouseover="return overlib('View', AUTOSTATUS, WRAP);" onmouseout="nd();">
					
				<input type="button" value="Summary" name="summary"
					onclick="return buttonLinks('summary');"
					accesskey="S" onmouseover="return overlib('Report Summary', AUTOSTATUS, WRAP);" onmouseout="nd();">
					
				<%--<input type="button" value="Titipan Premi" name="ttp" onclick="return buttonLinks('titipanpremi');"
					accesskey="M" onmouseover="return overlib('Titipan Premi', AUTOSTATUS, WRAP);" onmouseout="nd();">--%>
	
				<%--<input type="button" value="Batalkan" name="batalkan"
					onclick="return buttonLinks('batalkan');"
					accesskey="B" onmouseover="return overlib('Batalkan Polis', AUTOSTATUS, WRAP);" onmouseout="nd();">--%>
	
				 <input type="button" value="Kesehatan" name="kesehatan"
					onclick="return buttonLinks('kesehatan');"
					accesskey="B" onmouseover="return overlib('Keterangan Kesehatan', AUTOSTATUS, WRAP);" onmouseout="nd();">
		
	
<%-- 				<input type="button" value="X" name="simas"	onclick="return buttonLinks('simas');"  --%>
<%-- 					accesskey="H" onmouseover="return overlib('?', AUTOSTATUS, WRAP);" onmouseout="nd();"> --%>
	
<%-- 				<input type="button" value="X" name="hcp"	onclick="return buttonLinks('hcp');"  --%>
<%-- 					accesskey="P" onmouseover="return overlib('?', AUTOSTATUS, WRAP);" onmouseout="nd();"> --%>
	
			</c:if>
			<c:if test="${empty sessionScope.currentUser.cab_bank and sessionScope.currentUser.jn_bank ne 4 and empty sessionScope.currentUser.cab_bank and sessionScope.currentUser.jn_bank ne 4 and sessionScope.currentUser.lus_id ne 2661}">	
			<input type="button" value="NIK" name="nik"	onclick="return buttonLinks('nik');" 
					accesskey="Y" onmouseover="return overlib('Edit Nik', AUTOSTATUS, WRAP);" onmouseout="nd();"></c:if>
			     
		    <input name="btn_questionare" type="button" value="Questionare" onClick="return buttonLinks('questionare');" accesskey="Q" 
			     	onmouseover="return overlib('Pengisian Questionare', AUTOSTATUS, WRAP);" onmouseout="nd();">
		<c:if test="${sessionScope.currentUser.lus_id eq 2661 or sessionScope.currentUser.lde_id eq 11 or sessionScope.currentUser.lde_id eq \"01\"}">	
				<input type="button" value="E-SPAJ ONLINE" name="spajonline"	onclick="return buttonLinks('e-spajonline');" 
					accesskey="Y" onmouseover="return overlib('SPAJ ONLINE', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="E-KONFIRMASI" name="konfirmasiagency"	onclick="return buttonLinks('konfirmasiagency');" 
					accesskey="Y" onmouseover="return overlib('KONFIRMASI AGENCY', AUTOSTATUS, WRAP);" onmouseout="nd();"></c:if>
				<input name="profile_risiko_nasabah" type="button" value="Profile Risiko Nasabah" onClick="return buttonLinks('profile_risk');" accesskey="E" 
			     onmouseover="return overlib('Aktivasi Promo', AUTOSTATUS, WRAP);" onmouseout="nd();">	
			<%--<input type="button" value="Auto Accept" name="autoaccept"	onclick="return buttonLinks('autoaccept');" 
				accesskey="" onmouseover="return overlib('Auto Accept', AUTOSTATUS, WRAP);" onmouseout="nd();">--%>
				
			     
			    <%--  <input type="button" value="Titipan Premi" name="ttp" onclick="return buttonLinks('titipanpremi');"
					accesskey="M" onmouseover="return overlib('Titipan Premi', AUTOSTATUS, WRAP);" onmouseout="nd();"> --%>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="button" value="Info" name="ucup"	onclick="return buttonLinks('info');" 
				 onmouseover="return overlib('Info Lengkap Dokumen Polis', AUTOSTATUS, WRAP);" onmouseout="nd();">			
				 			
			<c:if test="${empty sessionScope.currentUser.cab_bank and sessionScope.currentUser.jn_bank ne 4 and sessionScope.currentUser.lus_id ne 2661}">
				<input type="button" value="Download SPAJ" name="download_spaj"	onclick="return buttonLinks('download_spaj');" 
					 onmouseover="return overlib('Download Formulir SPAJ', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="Reff DM/TM" name="reffdmtm"	onclick="return buttonLinks('reffdmtm');" 
					accesskey="R" onmouseover="return overlib('Insert Telemarketer', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="SPV DM/TM" name="reffdmtm"	onclick="return buttonLinks('spvdmtm');" 
					accesskey="R" onmouseover="return overlib('Insert SPV Telemarketer', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="View Limit U/W" name="limit"	onclick="return buttonLinks('limit');" 
					accesskey="L" onmouseover="return overlib('View Limit U/W Individu', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="View Simultan" name="simultan"	onclick="return buttonLinks('simultan');" 
					 onmouseover="return overlib('View Simultan Polis', AUTOSTATUS, WRAP);" onmouseout="nd();">
				<input type="button" value="Status" name="stattt"	onclick="stat();" 
					 onmouseover="return overlib('Status (Further Requirement)', AUTOSTATUS, WRAP);" onmouseout="nd();">
			</c:if>
			<c:if test="${sessionScope.currentUser.lus_id ne 2661}">	
				<input type="button" value="Checklist" name="checklist"	onclick="return buttonLinks('checklist');" 
				 onmouseover="return overlib('Checklist Dokumen Polis', AUTOSTATUS, WRAP);" onmouseout="nd();"
				 <c:if test="${sessionScope.currentUser.jn_bank eq 2 and sessionScope.currentUser.jn_bank eq 16 and sessionScope.currentUser.flag_approve eq 1 and sessionScope.currentUser.lus_id ne 2661}"> disabled="true" </c:if>>
				<input type="button" value="autoDebet" name="autoDebet" 
				onclick="return buttonLinks('autoDebet');">
				<input type="button" value="Upload Scan" name="upload_nb"	
				onclick="buttonLinks('upload_nb');">
				 <c:if test="${sessionScope.currentUser.jn_bank eq 16}">
				<input type="button" value="Surat Konfirmasi" name="surat_konfirm" onclick="buttonLinks('surat_konfirm');"></c:if>
			</c:if>
			<c:if test="${sessionScope.currentUser.jn_bank ne 2 and sessionScope.currentUser.jn_bank ne 16 and sessionScope.currentUser.jn_bank ne 3 and sessionScope.currentUser.jn_bank ne 4  and sessionScope.currentUser.lus_id ne 2661}">
			<input name="btn_medis" type="button" value="Medis" onClick="return buttonLinks('medis');" accesskey="E" 
			     onmouseover="return overlib('View Medis', AUTOSTATUS, WRAP);" onmouseout="nd();">		
			 
			 <input name="btn_questionare" type="button" value="Pengisian Kuisioner" onClick="return buttonLinks('questionare_dmtm');"
			     onmouseover="return overlib('Pengisian Questionare DMTM', AUTOSTATUS, WRAP);" onmouseout="nd();">
			 <input name="psn" type="button" value="SMiLe Religi" onClick="return buttonLinks('psn');"
			     onmouseover="return overlib('SMiLe Religi', AUTOSTATUS, WRAP);" onmouseout="nd();">
			 <input type="button" value="View Agen" name="view_agen" onclick="document.getElementById('infoFrame').src='${path}/bac/multi.htm?window=cek_struktur_view';" onmouseover="return overlib('View Struktur Agen', AUTOSTATUS, WRAP);" onmouseout="nd();">
			 
			 <input name="btn_status" type="button" value="Status Further BAS" 			onClick="return buttonLinks('status');"
			      onmouseover="return overlib('Status Akseptasi Bas', AUTOSTATUS, WRAP);" onmouseout="nd();"> 
			 <input type="button" value="view document pendukung" name="view_document_pendukung" 
		 		  onclick="popWin('${path}/common/util.htm?window=file_gadget', 600, 800);" onmouseover="return overlib('View Document Pendukung', AUTOSTATUS, WRAP);" onmouseout="nd();" />   
		 	 <c:if test="${sessionScope.currentUser.lus_id eq '133' or sessionScope.currentUser.lus_id eq '500' or sessionScope.currentUser.lus_id eq '516' or sessionScope.currentUser.lus_id eq '3177'
		 	 or sessionScope.currentUser.lus_id eq '690' or sessionScope.currentUser.lus_id eq '3041' or sessionScope.currentUser.lus_id eq '3732' or sessionScope.currentUser.lus_id eq '3725' or sessionScope.currentUser.lus_id eq '4386'}">
		 	 <input type="button" value="MultiView" name="MultiView"	onclick="return buttonLinks('multiview');" 
				 onmouseover="return overlib('Dokumen Polis', AUTOSTATUS, WRAP);" onmouseout="nd();"></c:if>
			<input name="cekphone" type="button" value="CHECK NO HP" onClick="return buttonLinks('cekphone');" accesskey="E" 
			     onmouseover="return overlib('View Medis', AUTOSTATUS, WRAP);" onmouseout="nd();">	
			<c:if  test ="${ sessionScope.currentUser.lde_id eq 29 or sessionScope.currentUser.lde_id eq \"01\"}">	
			 <input name="promo_aktiv" type="button" value="Promo Activation" onClick="return buttonLinks('promo_aktiv');" accesskey="E" 
			     onmouseover="return overlib('Aktivasi Promo', AUTOSTATUS, WRAP);" onmouseout="nd();">	 
			 </c:if>
		  </c:if> 
		  
				
			<!--<input name="btn_med_quest" type="button" value="PERNYATAAN KESEHATAN" onClick="return buttonLinks('med_quest');" accesskey="Q" 
			     onmouseover="return overlib('Pernyataan Kesehatan', AUTOSTATUS, WRAP);" onmouseout="nd();" style="margin-top: 3px">--> 
		</td>
	</tr>

	<tr>
		<td colspan="2">
			<c:choose>
				<c:when test="${sessionScope.currentUser.flag_approve eq 1}">
					<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
						width="100%" style="width: 100%;"> Please Wait... </iframe>
				</c:when>
				<c:otherwise>
					<iframe src="${path}/bac/edit.htm?data_baru=true" name="infoFrame" id="infoFrame"
						width="100%" style="width: 100%;"> Please Wait... </iframe>
				</c:otherwise>
			</c:choose>
			
<input type="hidden" value="${spajList}" id="spajListDisabledForTransfer" />
<input type="hidden" value="${spajList4EditButton}" id="spajList4EditButton" />
<input type="hidden" value="${jnBank}" id="jnBank" />
		</td>
	</tr>
</table>
</div>
</form>

</body>
<%@ include file="/include/page/footer.jsp"%>