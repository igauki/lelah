<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script>
			function cetak(key, urut,spaj){
				x = document.getElementById('x'+urut).value;
				y = document.getElementById('y'+urut).value;
				f = document.getElementById('f'+urut).value;
				window.location = '${path}/bac/multi.htm?window=download_spaj&file='+key+'&x='+x+'&y='+y+'&f='+f+'&spaj='+spaj;
				return false;
			}
		</script>
	</head>
	<body style="height: 100%;" onload="setupPanes('container1', 'tab1');">
<form name="frmParam" method="post">
		<fieldset>
			<table class="entry2">
				<tr>
					<th>Download Formulir SPAJ</th>
				</tr>
			</table>
		</fieldset>

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a id="tab1" href="#" onClick="return showPane('pane1', this)" onfocus="return showPane('pane1', this)">Formulir SPAJ</a>
					<a id="tab2" href="#" onClick="return showPane('pane2', this)" onfocus="return showPane('pane2', this)">History</a>
				</li>
			</ul>

			<div class="tab-panes">
			
				<div id="pane1" class="panes">
					<table class="entry2" style="width: auto;">
						<tr>
							<th>Daftar Form SPAJ <input type="reg_spaj" value="<elions:spaj nomor='${spaj}'/>" style='background-color :#D4D4D4' readOnly> :
							</th>
							<td>
								<table class="displaytag">
									<tr>
										<th>No.</th>
										<th>Form</th>
										<th>X</th>
										<th>Y</th>
										<th>Font</th>
										<th>Tanggal Update</th>
									</tr>
									<c:forEach items="${daftar}" var="f" varStatus="st">
										<tr>
											<td style="text-align: left;">${st.count}.</td>
											<td style="text-align: left;"><a href="#" onclick="return cetak('${f.key}', ${st.count},'${spaj}');">${f.key}</a></td>
											<td><input id="x${st.count}" type="text" size="5" value="${f.x}"/></td>
											<td><input id="y${st.count}" type="text" size="5" value="${f.y}"/></td>
											<td><input id="f${st.count}" type="text" size="5" value="${f.fontSize}"/></td>
											<td>${f.value}</td>
										</tr>
									</c:forEach>
									
								</table>
							</td>
						</tr>
					</table>
				</div>
				
				<div id="pane2" class="panes">
					<table class="entry2" style="width: auto;">
						<tr>
							<th>History Blanko SPAJ:</th>
							<td>
								<table class="displaytag">
									<tr>
										<th>No.</th>
										<th>No. Blanko</th>
										<th>Waktu</th>
										<th>Dokumen</th>
									</tr>
									<c:forEach items="${hist}" var="h" varStatus="st">
										<tr>
											<td style="text-align: center;">${st.count}.</td>
											<td style="text-align: center;">${h.NO_BLANKO}</td>
											<td style="text-align: center;">${h.TGL_CETAK}</td>
											<td style="text-align: left;">${h.NAMA_SPAJ}</td>
										</tr>
									</c:forEach>
								</table>
							</td>
						</tr>
					</table>
				</div>
				
			</div>

		</div>
</form>
<%@ include file="/include/page/footer.jsp"%>