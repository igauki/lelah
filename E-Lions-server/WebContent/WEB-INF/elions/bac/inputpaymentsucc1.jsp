<%@ include file="/include/page/header.jsp"%>
<script>

</script>
<body onload="setupPanes('container1', 'tab1');" style="height: 100%;">
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">1. Input SPAJ/Polis</a>
				<a href="#" id="tab2">2. Pilih Tagihan</a>
				<a href="#" id="tab3">3. Input Pembayaran</a>
				<a href="#" id="tab4">4. Selesai</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<fieldset>
					<form:form id="formpost" name="formpost" commandName="cmd">
					<table class="entry2">
						<tr>
							<th style="width: 300px;">Silahkan masukkan nomor SPAJ / Polis : <span class="error">*</span></th>
							<td>
								<form:input path="reg_spaj" size="20" maxlength="14" cssErrorClass="inpError" onfocus="this.select();"/>
							</td>
						</tr>
						<tr>
							<th></th>
							<td>
								<spring:hasBindErrors name="cmd">
									<div id="error">
										ERROR:<br/>
										<form:errors path="*" />
									</div>	
								</spring:hasBindErrors>
								<input type="submit" name="_target0" value="&laquo; Prev" disabled="disabled" accesskey="P">
								<input type="submit" name="_target1" value="Next &raquo;" accesskey="N">
							</td>
						</tr>
					</table>
					</form:form>
				</fieldset>
			</div>
			
		</div>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>