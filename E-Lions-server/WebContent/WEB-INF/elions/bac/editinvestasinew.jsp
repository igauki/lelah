<%@ include file="/include/page/header.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${path }/include/js/refund/refund_common.js"></script>
<script type="text/javascript" src="${path }/include/js/bac/investasinew.js"></script>
<script>
// BEBERAPA JAVASCRIPT DIPINDAHKAN KE INVESTASINEW.JS UNTUK MENGHINDARI ERROR EXCEEDING THE 65535 BYTES LIMIT.

var jumlah_investasi = '${cmd.investasiutama.jmlh_invest}';
var v_path = '${path}';
var v_lsbs_id = '${cmd.datausulan.lsbs_id}';
var v_lsdbs_number = '${cmd.datausulan.lsdbs_number}';	
var v_flag_bulanan = '${cmd.powersave.flag_bulanan}';	
var v_flag_special = '${cmd.powersave.flag_special}';	

	
	<spring:bind path="cmd.datausulan.jml_benef"> 
	var jmlpenerima=${status.value};
	</spring:bind>  
	
	<spring:bind path="cmd.tertanggung.jml_dth"> 
	var jmldth=${status.value};
	</spring:bind> 
	
	var varOptionlsne;
	var varOptionhub;
	var varOptSex;
	var flag_add=0;
	var flag_add1=0;
	
	
	
	
	
	function setInvestasiReadOnly(ke, isReadOnly){
		var flag_upload = '${cmd.pemegang.flag_upload}';
		<c:if test="${not empty cmd.pemegang.flag_upload}">	
		/* 	if(ke == 62){
			alert(ke);
			alert(isReadOnly);
			alert("value"+document.frmParam.elements['investasiutama.daftarinvestasi['+ke+'].mdu_persen1'].value);
			}; */
			if(isReadOnly){
			 	if(flag_upload==3){
					document.frmParam.elements['investasiutama.daftarinvestasi['+ke+'].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi['+ke+'].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
					document.frmParam.elements['investasiutama.daftarinvestasi['+ke+'].mdu_jumlah1'].value='';
			 	}else{
				 	document.frmParam.elements['investasiutama.daftarinvestasi['+ke+'].mdu_persen1'].value='';	
					document.frmParam.elements['investasiutama.daftarinvestasi['+ke+'].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi['+ke+'].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
					document.frmParam.elements['investasiutama.daftarinvestasi['+ke+'].mdu_jumlah1'].value='';
			 	}
			}else{
				
				document.frmParam.elements['investasiutama.daftarinvestasi['+ke+'].mdu_persen1'].readOnly=false;	
				document.frmParam.elements['investasiutama.daftarinvestasi['+ke+'].mdu_persen1'].style.backgroundColor ='#FFFFFF';	
			} 
		</c:if>
	}

	function Body_onload() {
		var lsbs_id = '${cmd.datausulan.lsbs_id}';
		var lsdbs_number = '${cmd.datausulan.lsdbs_number}';
		if(lsbs_id==153 && (lsdbs_number==6 || lsdbs_number==7)){
			if(document.getElementById('mste_dth').value==1) {
				document.getElementById('btnadd2').disabled=false;
				document.getElementById('btn_cancel2').disabled=false;
			} else {
				document.getElementById('btnadd2').disabled=true;
				document.getElementById('btn_cancel2').disabled=true;
			}
		}
		
	    var jn_bank = '${sessionScope.currentUser.jn_bank}';
	    
	    if(jn_bank==2 || jn_bank==16) {	 
	   		 document.frmParam.elements['powersave.mps_jenis_plan'].value = '0';
        	 document.frmParam.elements['powersave.mps_jenis_plan'].readOnly = true;
        	 document.frmParam.elements['powersave.mps_jenis_plan'].disabled = true;
			 document.frmParam.elements['powersave.mps_jenis_plan'].style.backgroundColor = '#D4D4D4';				      
        }
		if(document.getElementById('alertEkaSehat').value==1){
// 			alert('Usia Peserta max 50th dan wajib melampirkan hasil general Check Up terakhir atau medis : LPK,Urin,darah rutin,SGPT,SGOT,GGT & cholesterol atas biaya sendiri');
		}
		
		if(document.frmParam.elements['rekening_client.mrc_kuasa'].value == '1'){	
			document.frmParam.mrc_kuasa1.checked = true;
		}else{
			document.frmParam.mrc_kuasa1.checked = false;
		}
		
		if(document.frmParam.elements['account_recur.flag_set_auto'].value == '1'){
			document.frmParam.tgl_debet.checked = true;
		}else{
			document.frmParam.tgl_debet.checked = false;
		}
		
		document.frmParam.elements['powersave.msl_tgl_nab'].style.backgroundColor = '#D4D4D4';		
		document.frmParam.elements['powersave.msl_nab'].style.backgroundColor = '#D4D4D4';		
		
		var nameSelection;
		xmlData.async = false;
		<c:choose>
			<c:when test="${empty sessionScope.currentUser.cab_bank}">
				xmlData.src = "${path}/xml/RELATION.xml";
			</c:when>
			<c:otherwise>
				xmlData.src = "${path}/xml/RELATION_BANCASS_BENEF.xml";
			</c:otherwise>
		</c:choose>
		generateXML_penerima( xmlData, 'ID','RELATION');
		generateSex_Benef();
		xmlData.src = "${path}/xml/WARGANEGARA.xml";
		generateXML_warganegara( xmlData, 'ID','NEGARA');
		var flag_account = document.frmParam.elements['datausulan.flag_account'].value ;
		var flagbungasimponi = document.frmParam.elements['datausulan.isBungaSimponi'].value;
		var flagbonustahapan = document.frmParam.elements['datausulan.isBonusTahapan'].value;
		var flag_bao = document.frmParam.elements['datausulan.flag_bao'].value;
		var kode_flag = document.frmParam.elements['datausulan.kode_flag'].value;
		if( (flag_bao ==1) && ((flag_account==2)||(flag_account==3)) ){
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].disabled=false;
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].style.backgroundColor ='#FFFFFF';				
		}else{
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].value='0';	
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].disabled=true;
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].style.backgroundColor ='#D4D4D4';		
		}
		
		if(flagbungasimponi==1){
			document.frmParam.elements['pemegang.mspo_under_table'].readOnly = true;
			document.frmParam.elements['pemegang.mspo_under_table'].style.backgroundColor = '#D4D4D4';
			document.frmParam.elements['pemegang.tgl_mspo_under_table'].readOnly = true;
			document.frmParam.elements['pemegang.tgl_mspo_under_table'].style.backgroundColor = '#D4D4D4';
		}else{
			document.frmParam.elements['pemegang.mspo_under_table'].readOnly = true;
			document.frmParam.elements['pemegang.mspo_under_table'].style.backgroundColor = '#D4D4D4';
			document.frmParam.elements['pemegang.mspo_under_table'].value = '';
			document.frmParam.elements['pemegang.tgl_mspo_under_table'].readOnly = true;
			document.frmParam.elements['pemegang.tgl_mspo_under_table'].style.backgroundColor = '#D4D4D4';
			document.frmParam.elements['pemegang.tgl_mspo_under_table'].value = '';
		}

		if(flagbonustahapan == 1){
			document.frmParam.elements['pemegang.bonus_tahapan'].readOnly = false;
			document.frmParam.elements['pemegang.bonus_tahapan'].style.backgroundColor = '#FFFFFF';
		}else{
			document.frmParam.elements['pemegang.bonus_tahapan'].readOnly = true;
			document.frmParam.elements['pemegang.bonus_tahapan'].style.backgroundColor = '#D4D4D4';
			document.frmParam.elements['pemegang.bonus_tahapan'].value = '';	
		}		

		if ((document.frmParam.elements['datausulan.mste_flag_cc'].value=='1') || (document.frmParam.elements['datausulan.mste_flag_cc'].value=='2') ||
			(document.frmParam.elements['datausulan.mste_flag_cc'].value=='9') || (flag_account== 3)){
			
			document.frmParam.elements['account_recur.mar_holder'].readOnly = false;
			document.frmParam.elements['account_recur.mar_holder'].style.backgroundColor = '#FFFFFF';
			document.frmParam.elements['account_recur.flag_autodebet_nb'].disabled = false;
			document.frmParam.elements['account_recur.flag_autodebet_nb'].style.backgroundColor = '#FFFFFF';
			if (document.frmParam.elements['account_recur.mar_holder'].value==''){
				document.frmParam.elements['account_recur.mar_holder'].value = document.frmParam.elements['pemegang.mcl_first'].value;
			}				
			
			document.frmParam.elements['account_recur.lbn_id'].disabled = false;
			document.frmParam.elements['account_recur.flag_jn_tabungan'].disabled = false;
			document.frmParam.elements['account_recur.lbn_id'].style.backgroundColor = '#FFFFFF';
			document.frmParam.elements['account_recur.flag_jn_tabungan'].style.backgroundColor = '#FFFFFF';
			
			document.frmParam.elements['account_recur.mar_acc_no'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no'].style.backgroundColor = '#FFFFFF';
			/*tambahan setting dari Bertho untuk split mar_acc_no*/
			document.frmParam.elements['account_recur.mar_acc_no_split[0]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[0]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[1]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[1]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[2]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[2]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[3]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[3]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[4]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[4]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[5]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[5]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[6]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[6]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[7]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[7]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[8]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[8]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[9]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[9]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[10]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[10]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[11]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[11]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[12]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[12]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[13]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[13]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[14]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[14]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[15]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[15]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[16]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[16]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[17]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[17]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[18]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[18]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[19]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[19]'].style.backgroundColor ='#FFFFFF';
		
			<c:if test="${not empty cmd.account_recur.lbn_id}">
				var flagCC = document.frmParam.elements['datausulan.mste_flag_cc'].value;
				
				if(flagCC!=1){
						ajaxPjngRek('${cmd.account_recur.lbn_id}', 2,'account_recur.mar_acc_no_split');	
				}else{
				    for(var i=0;i<16;i++){	
						document.frmParam.elements['account_recur.mar_acc_no_split'+'['+i+']'].readOnly = false;
						document.frmParam.elements['account_recur.mar_acc_no_split'+'['+i+']'].style.backgroundColor ='#FFFFFF';
					}
				    for(var i=16;i<20;i++){	
						document.frmParam.elements['account_recur.mar_acc_no_split'+'['+i+']'].readOnly = true;
						document.frmParam.elements['account_recur.mar_acc_no_split'+'['+i+']'].style.backgroundColor ='#D4D4D4';
					}
				}							
			</c:if>
			
			/*end of setting*/
			document.frmParam.caribank2.readOnly = false;
			document.frmParam.caribank2.style.backgroundColor ='#FFFFFF';
			document.frmParam.btncari2.disabled = false;
		}else{
			document.frmParam.elements['account_recur.mar_holder'].readOnly = true;
			document.frmParam.elements['account_recur.mar_holder'].style.backgroundColor = '#D4D4D4';
			document.frmParam.elements['account_recur.mar_holder'].value='';
			document.frmParam.elements['account_recur.flag_autodebet_nb'].disabled = true;
			document.frmParam.elements['account_recur.flag_autodebet_nb'].style.backgroundColor = '#D4D4D4';
			document.frmParam.elements['account_recur.flag_autodebet_nb'].value = '0';			
			document.frmParam.elements['account_recur.lbn_id'].disabled = true;
			document.frmParam.elements['account_recur.flag_jn_tabungan'].disabled = true;
			document.frmParam.elements['account_recur.lbn_id'].style.backgroundColor = '#D4D4D4';
			document.frmParam.elements['account_recur.lbn_id'].value='NULL';
			document.frmParam.elements['account_recur.flag_jn_tabungan'].style.backgroundColor = '#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no'].style.backgroundColor = '#D4D4D4';
			/*tambahan setting dari Bertho untuk split mar_acc_no*/
			
			document.frmParam.elements['account_recur.mar_acc_no_split[0]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[0]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[1]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[1]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[2]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[2]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[3]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[3]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[4]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[4]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[5]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[5]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[6]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[6]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[7]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[7]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[8]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[8]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[9]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[9]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[10]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[10]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[11]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[11]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[12]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[12]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[13]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[13]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[14]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[14]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[15]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[15]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[16]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[16]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[17]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[17]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[18]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[18]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[19]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[19]'].style.backgroundColor ='#D4D4D4';
			
			/*end of setting*/
			document.frmParam.elements['account_recur.mar_acc_no'].value = '';
			document.frmParam.caribank2.readOnly = true;
			document.frmParam.caribank2.style.backgroundColor = '#D4D4D4';
			document.frmParam.caribank2.value = '';
			document.frmParam.btncari2.disabled = true;
		}
		
		//alert(flag_account);
		
		if ((flag_account ==2)||(flag_account ==3))
		{
			document.frmParam.elements['rekening_client.mrc_no_ac'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac'].style.backgroundColor = '#FFFFFF';
			/*Tambahan dari Bertho untuk split rekening*/
			document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].style.backgroundColor ='#FFFFFF';
			
			<c:if test="${not empty cmd.rekening_client.lsbp_id}">
// 				var flagCC = document.frmParam.elements['datausulan.mste_flag_cc'].value;
				
// 				if(flagCC!=1){
						ajaxPjngRek('${cmd.rekening_client.lsbp_id}', 1, 'rekening_client.mrc_no_ac_split');
// 				}else{
// 				    for(var i=0;i<16;i++){	
// 						document.frmParam.elements['rekening_client.mar_acc_no_split'+'['+i+']'].readOnly = false;
// 						document.frmParam.elements['rekening_client.mar_acc_no_split'+'['+i+']'].style.backgroundColor ='#FFFFFF';
// 					}
// 				    for(var i=16;i<20;i++){	
// 						document.frmParam.elements['rekening_client.mrc_no_ac_split'+'['+i+']'].readOnly = true;
// 						document.frmParam.elements['rekening_client.mrc_no_ac_split'+'['+i+']'].style.backgroundColor ='#D4D4D4';
// 					}
// 				}						
			</c:if>
		
			/*End of code*/
			document.frmParam.elements['rekening_client.mrc_nama'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_nama'].style.backgroundColor ='#FFFFFF';
			if(document.frmParam.elements['rekening_client.mrc_nama'].value ==""){
				document.frmParam.elements['rekening_client.mrc_nama'].value = document.frmParam.elements['pemegang.mcl_first'].value;
			}
			
			document.frmParam.elements['rekening_client.lsbp_id'].readOnly = false;
			document.frmParam.elements['rekening_client.lsbp_id'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_kota'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_kota'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.tgl_surat'].readOnly = false;
			document.frmParam.elements['rekening_client.tgl_surat'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.notes'].readOnly = false;
			document.frmParam.elements['rekening_client.notes'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_kuasa'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_kuasa'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_cabang'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_cabang'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_jenis'].disabled = false;
			document.frmParam.elements['rekening_client.mrc_jenis'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_kurs'].disabled = false;
			document.frmParam.elements['rekening_client.mrc_kurs'].style.backgroundColor ='#FFFFFF';
			document.frmParam.caribank1.readOnly = false;
			document.frmParam.caribank1.style.backgroundColor ='#FFFFFF';
			document.frmParam.btncari1.disabled = false;
		}else{
			document.frmParam.elements['rekening_client.mrc_no_ac'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac'].value = '';
			
			/*Tambahan dari Bertho untuk split rekening*/
			document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].value = '';
			
			document.frmParam.elements['rekening_client.mrc_nama'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_nama'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_nama'].value='';
			document.frmParam.elements['rekening_client.lsbp_id'].disabled = true;
			document.frmParam.elements['rekening_client.lsbp_id'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.lsbp_id'].value='NULL';
			document.frmParam.elements['rekening_client.mrc_kota'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_kota'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_kota'].value='';
			document.frmParam.elements['rekening_client.tgl_surat'].readOnly = true;
			document.frmParam.elements['rekening_client.tgl_surat'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.tgl_surat'].value='';			
			document.frmParam.elements['rekening_client.notes'].readOnly = true;
			document.frmParam.elements['rekening_client.notes'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.notes'].value='';
			document.frmParam.mrc_kuasa1.disabled = true;
			document.frmParam.mrc_kuasa1.checked = false;
			document.frmParam.elements['rekening_client.mrc_kuasa'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_kuasa'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_kuasa'].value='0';
			document.frmParam.elements['rekening_client.mrc_cabang'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_cabang'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_cabang'].value='';
			document.frmParam.elements['rekening_client.mrc_jenis'].disabled = true;
			document.frmParam.elements['rekening_client.mrc_jenis'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_jenis'].value='0';
			document.frmParam.elements['rekening_client.mrc_kurs'].disabled = true;
			document.frmParam.elements['rekening_client.mrc_kurs'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_kurs'].value='';			
			document.frmParam.caribank1.readOnly = true;
			document.frmParam.caribank1.style.backgroundColor ='#D4D4D4';
			document.frmParam.caribank1.value='';
			document.frmParam.btncari1.disabled = true;
		}
		
		var jumlah_fund = document.frmParam.elements['investasiutama.jmlh_invest'].value;
		
		if((Number(kode_flag)==1) ){
			document.frmParam.elements['powersave.mps_jangka_inv'].disabled=false;
			document.frmParam.elements['powersave.mps_jangka_inv'].style.backgroundColor ='#FFFFFF';
			
			document.frmParam.elements['powersave.mps_roll_over'].disabled=false;
			document.frmParam.elements['powersave.mps_roll_over'].style.backgroundColor ='#FFFFFF';
			
			document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].value="";
			document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].disabled=true;
			document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].style.backgroundColor ='#D4D4D4';

			document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].value="";
			document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].disabled=true;
			document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].style.backgroundColor ='#D4D4D4';

			document.frmParam.elements['powersave.begdate_topup'].value="__/__/____";
			document.frmParam.elements['powersave.begdate_topup'].disabled=true;
			document.frmParam.elements['powersave.begdate_topup'].readOnly=true;
			document.frmParam.elements['powersave.begdate_topup'].style.backgroundColor ='#D4D4D4';
			
			document.frmParam.elements['powersave.msl_bp_rate'].value="0";
			document.frmParam.elements['powersave.msl_bp_rate'].readOnly=true;
			document.frmParam.elements['powersave.msl_bp_rate'].style.backgroundColor ='#D4D4D4';
			
			document.frmParam.mspr_premium.value="";
			
			document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].value="";
			document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].readOnly=true;
			document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].style.backgroundColor ='#D4D4D4';

			document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].value="";
			document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].readOnly=true;
			document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].style.backgroundColor ='#D4D4D4';

			
			document.frmParam.elements['datausulan.total_premi_rider'].value="";
			document.frmParam.elements['investasiutama.total_premi_sementara'].value="";
			
			document.frmParam.elements['powersave.tarik_bunga'].value="0";
			document.frmParam.elements['powersave.tarik_bunga'].readOnly=true;
			document.frmParam.elements['powersave.tarik_bunga'].disabled=true;
			document.frmParam.elements['powersave.tarik_bunga'].style.backgroundColor ='#D4D4D4';
			
			tutup();
			jns_rate();
		}else{
			if( Number(kode_flag)!=11 && Number(kode_flag)!=15 && Number(kode_flag)!=16 ){
				document.frmParam.elements['powersave.mps_jangka_inv'].value="";
				document.frmParam.elements['powersave.mps_jangka_inv'].disabled=true;
				document.frmParam.elements['powersave.mps_jangka_inv'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['pemegang.mspo_spaj_bundle'].value="";
				document.frmParam.elements['pemegang.mspo_spaj_bundle'].disabled=true;
				document.frmParam.elements['pemegang.mspo_spaj_bundle'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['powersave.mps_rate'].value="";				
				document.frmParam.elements['powersave.mps_prm_interest'].value="";
				document.frmParam.elements['powersave.mps_prm_deposit'].value="";
				
				document.frmParam.elements['powersave.mps_roll_over'].value="";
				document.frmParam.elements['powersave.mps_roll_over'].disabled=true;
				document.frmParam.elements['powersave.mps_roll_over'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].disabled=false;
				document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].style.backgroundColor ='#FFFFFF';

				document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].disabled=false;
				document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].style.backgroundColor ='#FFFFFF';

				
				if (document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].value==""){
					document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].value="0";
				}
				document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].readOnly=false;
				document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].style.backgroundColor ='#FFFFFF';

				if (document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].value==""){
					document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].value="0";
				}
				document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].readOnly=false;
				document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].style.backgroundColor ='#FFFFFF';
				
				document.frmParam.elements['powersave.mps_rate'].readOnly=true;
				document.frmParam.elements['powersave.mps_rate'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['powersave.mps_rate'].value = "";
				document.frmParam.elements['powersave.mps_employee'].disabled=true;
				document.frmParam.elements['powersave.mps_employee'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['powersave.mps_employee'].value = "";				
				document.frmParam.elements['powersave.mpr_note'].readOnly=true;
				document.frmParam.elements['powersave.mpr_note'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['powersave.mpr_note'].value = "";
				document.frmParam.elements['powersave.mps_jenis_plan'].disabled=true;
				document.frmParam.elements['powersave.mps_jenis_plan'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['powersave.mps_jenis_plan'].value = "";
				
				document.frmParam.elements['powersave.begdate_topup'].value="__/__/____";
				document.frmParam.elements['powersave.begdate_topup'].disabled=true;
				document.frmParam.elements['powersave.begdate_topup'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['powersave.msl_bp_rate'].value="0";
				document.frmParam.elements['powersave.msl_bp_rate'].readOnly=true;
				document.frmParam.elements['powersave.msl_bp_rate'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['powersave.tarik_bunga'].value="0";
				document.frmParam.elements['powersave.tarik_bunga'].readOnly=true;
				document.frmParam.elements['powersave.tarik_bunga'].disabled=true;
				document.frmParam.elements['powersave.tarik_bunga'].style.backgroundColor ='#D4D4D4';					
			}
		}

		if ((Number(kode_flag)==1) || (Number(kode_flag)==0) ){
			document.frmParam.elements['datausulan.mste_flag_investasi'].value = '0';
			document.frmParam.elements['datausulan.mste_flag_investasi'][0].disabled=true;
			document.frmParam.elements['datausulan.mste_flag_investasi'][1].disabled=true;
		}		
		
		//alert(kode_flag);
	
		switch (Number(kode_flag)){
			case 0: //tutup semua investasi
				for(var q = 0; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);
			
				document.frmParam.elements['investasiutama.total_persen'].value="";
				
				document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].value="";
				document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].disabled=true;
				document.frmParam.elements['investasiutama.daftartopup.pil_tunggal'].style.backgroundColor ='#D4D4D4';

				document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].value="";
				document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].disabled=true;
				document.frmParam.elements['investasiutama.daftartopup.pil_berkala'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.mspr_premium.value="";
				
				document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].value="";
				document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].readOnly=true;
				document.frmParam.elements['investasiutama.daftartopup.premi_tunggal'].style.backgroundColor ='#D4D4D4';

				document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].value="";
				document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].readOnly=true;
				document.frmParam.elements['investasiutama.daftartopup.premi_berkala'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['datausulan.total_premi_rider'].value="";
				document.frmParam.elements['investasiutama.total_premi_sementara'].value="";				

				document.frmParam.elements['powersave.mps_jangka_inv'].value="";
				document.frmParam.elements['powersave.mps_jangka_inv'].disabled=true;
				document.frmParam.elements['powersave.mps_jangka_inv'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['powersave.mps_rate'].value="";				
				document.frmParam.elements['powersave.mps_prm_interest'].value="";
				document.frmParam.elements['powersave.mps_prm_deposit'].value="";
				
				document.frmParam.elements['powersave.mps_roll_over'].value="";
				document.frmParam.elements['powersave.mps_roll_over'].disabled=true;
				document.frmParam.elements['powersave.mps_roll_over'].style.backgroundColor ='#D4D4D4';
				
				document.frmParam.elements['powersave.mps_rate'].readOnly=true;
				document.frmParam.elements['powersave.mps_rate'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['powersave.mps_rate'].value = "";			
				document.frmParam.elements['powersave.mps_employee'].disabled=true;
				document.frmParam.elements['powersave.mps_employee'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['powersave.mps_employee'].value = "";				
				document.frmParam.elements['powersave.mpr_note'].readOnly=true;
				document.frmParam.elements['powersave.mpr_note'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['powersave.mpr_note'].value = "";
				document.frmParam.elements['powersave.mps_jenis_plan'].disabled=true;
				document.frmParam.elements['powersave.mps_jenis_plan'].style.backgroundColor ='#D4D4D4';
				document.frmParam.elements['powersave.mps_jenis_plan'].value = "";
				break;
			
			case 1: //tutup semua investasi (powersave)
				for(var q = 0; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				document.frmParam.elements['investasiutama.total_persen'].value="";
				break;
			
			case 2: //buka fixed & dyna
				setInvestasiReadOnly(0, false);
				setInvestasiReadOnly(1, false);
				for(var q = 2; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				break;			

			case 3: //buka aggresive
				for(var q = 0; q <= 1; q++) setInvestasiReadOnly(q, true);
				setInvestasiReadOnly(2, false);
				for(var q = 3; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				break;			
			
			case 4: //buka fixed & dyna & aggresive
				var lsbs = document.frmParam.elements['datausulan.lsbs_id'].value;
				var lsdbs = document.frmParam.elements['datausulan.lsdbs_number'].value;	
			    
			   if((lsbs==120 && (lsdbs>=19 && lsdbs<=21)) || (lsbs==134 && lsdbs==9) || (lsbs==220 && lsdbs==3))	{		
			     	for(var q = 0; q <=33; q++) setInvestasiReadOnly(q, true);		
				    setInvestasiReadOnly(34, false);
					setInvestasiReadOnly(35, false);
					setInvestasiReadOnly(36, false);				
					for(var q = 37; q < 62; q++) setInvestasiReadOnly(q, true);	
					setInvestasiReadOnly(62, false);	
					for(var q = 63; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);
							
					break;	
				}else if((lsbs==120 &&(lsdbs>=22 && lsdbs<=24)) || (lsbs==134 && (lsdbs==5 || lsdbs==13))){ //helpdesk [142003] produk baru Smart Platinum Link RPUL BEL (134-13)
			
					setInvestasiReadOnly(0, false);
					setInvestasiReadOnly(1, false);
					setInvestasiReadOnly(2, false);	
					for(var q = 3; q <=33; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(34, false);
					setInvestasiReadOnly(35, false);
					setInvestasiReadOnly(36, false);				
					for(var q = 37; q <= 59; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(60, false);
					setInvestasiReadOnly(61, true);
					setInvestasiReadOnly(62, false);
					for(var q = 63; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);
					break;	
					
				}else if(lsbs==215 && lsdbs==2){
					for(var q = 0; q <= 4; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(5, false);
					setInvestasiReadOnly(6, false);
					setInvestasiReadOnly(7, false);
					for(var q = 8; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);
					break;			
				}else if(lsbs==217 && lsdbs==2){
					for(var q = 0; q <= 1; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(2, false);
					for(var q = 3; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);
					break;			
				}else if ((lsbs==134 && lsdbs==11) || (lsbs==190 &&(lsdbs>=5 && lsdbs<=6))
							|| (lsbs==140 && (lsdbs>=1 && lsdbs<=2))
							|| (lsbs==138 && lsdbs==4) ){
				    setInvestasiReadOnly(0, false);
					setInvestasiReadOnly(1, false);
					setInvestasiReadOnly(2, false);	
					for(var q = 3; q <=59; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(60, false);
					setInvestasiReadOnly(61, true);
					setInvestasiReadOnly(62, false);
					setInvestasiReadOnly(63, true);
					break;			
				}
				//helpdesk 126900, Penambahan 3 Fund Simas Pada Produk UL Bank Bukopin dan Agency - Chandra - Start
				else if((lsbs==190 && (lsdbs==3 || lsdbs==4)) || //smile link 99 single/reguler
					(lsbs==116 && (lsdbs==3 || lsdbs==4)) || //smile link 88 single/reguler
					(lsbs==118 && (lsdbs==3 || lsdbs==4)) || //smile link 88 single/reguler (AS)
					(lsbs==134 && lsdbs==6) || //smile link proasset
					(lsbs==217 && lsdbs == 1)){ //smile link pro 100
					setInvestasiReadOnly(1, false);
					setInvestasiReadOnly(2, false);
					setInvestasiReadOnly(3, false);				
					for(var q = 3; q < 34; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(35, false);
					setInvestasiReadOnly(36, false);
					setInvestasiReadOnly(37, false);
					for(var q = 37; q < 60; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(60, false);		
					setInvestasiReadOnly(61, true);	
					setInvestasiReadOnly(62, false);					
					for(var q = 63; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);		
					break;	
				}
				//helpdesk 126900, Penambahan 3 Fund Simas Pada Produk UL Bank Bukopin dan Agency - Chandra - End
				else if(lsbs==190 && (lsdbs==1 || lsdbs==2)){ //smile link bridge single/reguler
					setInvestasiReadOnly(0, false);
					setInvestasiReadOnly(1, false);
					setInvestasiReadOnly(2, false);				
					//helpdesk 126900, Penambahan 3 Fund Simas Pada Produk UL Bank Bukopin dan Agency - Chandra - Start
					for(var q = 3; q < 34; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(35, false);
					setInvestasiReadOnly(36, false);
					setInvestasiReadOnly(37, false);
					for(var q = 37; q < 62; q++) setInvestasiReadOnly(q, true);
					//helpdesk 126900, Penambahan 3 Fund Simas Pada Produk UL Bank Bukopin dan Agency - Chandra - End
					setInvestasiReadOnly(62, false);					
					for(var q = 63; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);		
					break;	
				}else if((lsbs==220 && lsdbs==4) || (lsbs==134 && lsdbs==12)){ //bukopin smile protection/insurance
					setInvestasiReadOnly(1, false);
					setInvestasiReadOnly(2, false);
					setInvestasiReadOnly(3, false);					
					//helpdesk 126900, Penambahan 3 Fund Simas Pada Produk UL Bank Bukopin dan Agency - Chandra - Start
					for(var q = 3; q < 34; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(35, false);
					setInvestasiReadOnly(36, false);
					setInvestasiReadOnly(37, false);
					for(var q = 37; q < 60; q++) setInvestasiReadOnly(q, true);
					//helpdesk 126900, Penambahan 3 Fund Simas Pada Produk UL Bank Bukopin dan Agency - Chandra - End
					setInvestasiReadOnly(60, false);		
					setInvestasiReadOnly(61, true);	
					setInvestasiReadOnly(62, false);					
					for(var q = 63; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);		
					break;	
				}else if(lsbs==134 && lsdbs==10){ //helpdesk 125061, request untuk simas primelink hanya buka 1 fund - Chandra
					for(var q = 0; q < 36; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(37, false);
					for(var q = 37; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);		
					break;
				}
				//helpdesk 125227, Penambahan 3 Fund Simas Pada Produk UL - Chandra - Start
				else if((lsbs==190 && (lsdbs==7 || lsdbs==8))){
					setInvestasiReadOnly(1, false);
					setInvestasiReadOnly(2, false);
					setInvestasiReadOnly(3, false);
					for(var q = 3; q < 34; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(35, false);
					setInvestasiReadOnly(36, false);
					setInvestasiReadOnly(37, false);
					for(var q = 37; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);		
					break;
				}else if((lsbs==220 &&(lsdbs==1 || lsdbs==2)) || 
					(lsbs==134 && (lsdbs==7 || lsdbs==8))){
					setInvestasiReadOnly(1, false);
					setInvestasiReadOnly(2, false);
					setInvestasiReadOnly(3, false);	
					for(var q = 3; q < 34; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(35, false);
					setInvestasiReadOnly(36, false);
					setInvestasiReadOnly(37, false);
					for(var q = 37; q < 60; q++) setInvestasiReadOnly(q, true);	
					setInvestasiReadOnly(60, false);
					setInvestasiReadOnly(61, true);
					setInvestasiReadOnly(62, false);
					setInvestasiReadOnly(63, true);
					break;			
				}
				//helpdesk 125227, Penambahan 3 Fund Simas Pada Produk UL - Chandra - End
				else{
				    setInvestasiReadOnly(0, false);
					setInvestasiReadOnly(1, false);
					setInvestasiReadOnly(2, false);				
					for(var q = 3; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);			
					break;	
				}
						
			
			case 5: //buka secured & dyna dollar
				for(var q = 0; q <= 2; q++) setInvestasiReadOnly(q, true);
				setInvestasiReadOnly(3, false);
				setInvestasiReadOnly(4, false);
				for(var q = 5; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				break;			
			
			case 6: //buka Syariah fixed & syariah dyna
				for(var q = 0; q <= 4; q++) setInvestasiReadOnly(q, true);
				setInvestasiReadOnly(5, false);
				setInvestasiReadOnly(6, false);
				for(var q = 7; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				break;			
		
			case 7: //excellink syariah - fixed, dina, aggre
				var lsbs = document.frmParam.elements['datausulan.lsbs_id'].value;
				var lsdbs = document.frmParam.elements['datausulan.lsdbs_number'].value;	
			    
				if((lsbs==202 &&(lsdbs>=4 && lsdbs<=6)) || (lsbs==215 && (lsdbs>=1 && lsdbs<=2)) || (lsbs==200 && (lsdbs>=3 && lsdbs<=4))
				 		|| (lsbs==218 && lsdbs==1) || (lsbs==199 && (lsdbs>=1 && lsdbs<=2)) || (lsbs==224 && (lsdbs==1)) 
				 		|| (lsbs==153 && ((lsdbs>=3 && lsdbs<=4) || (lsdbs>=6 && lsdbs<=7))) ){
					    for(var q = 0; q <= 4; q++) setInvestasiReadOnly(q, true);		
					    setInvestasiReadOnly(5, false);
						setInvestasiReadOnly(6, false);
						setInvestasiReadOnly(7, false);		
						for(var q = 8; q <= 60; q++) setInvestasiReadOnly(q, true);
						setInvestasiReadOnly(61, false);
						for(var q = 62; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);
						break;			
				}else if(lsbs==200 && lsdbs>=7){ // Ultimate Syariah hanya di buka ExcellinkAgressiveSyariah Fund 
					    for(var q = 0; q <= 6; q++) setInvestasiReadOnly(q, true);		
					    setInvestasiReadOnly(7, false);		
						for(var q = 8; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);
						break;			
				}else if(lsbs==215 && lsdbs==4){ //helpdesk [133899], produk B Smile Insurance Syariah 215-4
					for(var q = 0; q <= 4; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(5, false);
					setInvestasiReadOnly(6, false);
					setInvestasiReadOnly(7, false);
					/*for(var q = 8; q < 57; q++) setInvestasiReadOnly(q, true);	
					setInvestasiReadOnly(58, false);
					setInvestasiReadOnly(59, false);*/	
					for(var q = 8; q <= 59; q++) setInvestasiReadOnly(q, true);	//helpdesk [136533] [URGENT] tutp sementara Fund Produk Syariah untuk produk bukopin syariah
					setInvestasiReadOnly(60, true);
					setInvestasiReadOnly(61, false);
					for(var q = 62; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);	
					break;
				}else if(lsbs==224 && lsdbs==3){ //helpdesk [133899], produk B Smile Protection Syariah 224-3
					for(var q = 0; q <= 4; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(5, false);
					setInvestasiReadOnly(6, false);
					setInvestasiReadOnly(7, false);
					/*for(var q = 8; q < 34; q++) setInvestasiReadOnly(q, true);	
					setInvestasiReadOnly(35, false);
					setInvestasiReadOnly(36, false);
					for(var q = 37; q < 61; q++) setInvestasiReadOnly(q, true);*/
					for(var q = 8; q < 61; q++) setInvestasiReadOnly(q, true); //helpdesk [136533] [URGENT] tutp sementara Fund Produk Syariah untuk produk bukopin syariah
					for(var q = 62; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);	
					break;
				}else{
						for(var q = 0; q <= 4; q++) setInvestasiReadOnly(q, true);
						setInvestasiReadOnly(5, false);
						setInvestasiReadOnly(6, false);
						setInvestasiReadOnly(7, false);
						for(var q = 8; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);
						break;			
				}
			case 8: //excellink syariah dollar - secure, dina
				for(var q = 0; q <= 7; q++) setInvestasiReadOnly(q, true);
				setInvestasiReadOnly(8, false);
				setInvestasiReadOnly(9, false);
				for(var q = 10; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				break;			

			case 9: //arthalink, ekalink88, ekalink88+ - fixed, dina, aggre
	
				var lsbs = Number(document.frmParam.elements['datausulan.lsbs_id'].value);
				var lsdbs = Number(document.frmParam.elements['datausulan.lsdbs_number'].value);
				
				if(lsbs == 162 && lsdbs <= 4){ //arthalink
					for(var q = 0; q <= 9; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(10, false);
					setInvestasiReadOnly(11, false);
					setInvestasiReadOnly(12, false);
					for(var q = 13; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
					break;			
				}else if(lsbs == 162 && (lsdbs == 5 || lsdbs == 6)){ //ekalink 88
					for(var q = 0; q <= 14; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(15, false);
					setInvestasiReadOnly(16, false);
					setInvestasiReadOnly(17, false);
					for(var q = 18; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
					break;			
				}else if(lsbs == 162 && (lsdbs == 7 || lsdbs == 8)){ //ekalink 88+
					for(var q = 0; q <= 22; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(23, false);
					setInvestasiReadOnly(24, false);
					setInvestasiReadOnly(25, false);
					for(var q = 26; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
					break;			
				}
				break;
			
			case 10: //arthalink, ekalink88, ekalink88+ dollar - secure, dina
				var lsbs = document.frmParam.elements['datausulan.lsbs_id'].value;
				var lsdbs = document.frmParam.elements['datausulan.lsdbs_number'].value;
				
				if(lsbs == 162 && lsdbs <= 4){ //arthalink
					for(var q = 0; q <= 12; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(13, false);
					setInvestasiReadOnly(14, false);
					for(var q = 15; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
					break;			
				}else if(lsbs == 162 && (lsdbs == 5 || lsdbs == 6)){ //ekalink 88
					for(var q = 0; q <= 17; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(18, false);
					setInvestasiReadOnly(19, false);
					for(var q = 20; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
					break;			
				}else if(lsbs == 162 && (lsdbs == 7 || lsdbs == 8)){ //ekalink 88+
					for(var q = 0; q <= 25; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(26, false);
					setInvestasiReadOnly(27, false);
					break;			
				}
				break;
														
			case 11: //stable link
				for(var q = 0; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				document.frmParam.elements['investasiutama.total_persen'].value="100";

				if (document.frmParam.elements['datausulan.kurs_premi'].value == '01'){
					document.frmParam.elements['investasiutama.daftarinvestasi[21].mdu_persen1'].value="100";
					document.frmParam.elements['investasiutama.daftarinvestasi[21].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[21].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
					document.frmParam.elements['investasiutama.daftarinvestasi[22].mdu_persen1'].value="";
					document.frmParam.elements['investasiutama.daftarinvestasi[22].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[22].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				}else{
					document.frmParam.elements['investasiutama.daftarinvestasi[21].mdu_persen1'].value="";
					document.frmParam.elements['investasiutama.daftarinvestasi[21].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[21].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
					document.frmParam.elements['investasiutama.daftarinvestasi[22].mdu_persen1'].value="100";
					document.frmParam.elements['investasiutama.daftarinvestasi[22].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[22].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				}
				
				var lsbs = document.frmParam.elements['datausulan.lsbs_id'].value;
				var lsdbs = document.frmParam.elements['datausulan.lsdbs_number'].value;
				
				if(lsbs == 164 && lsdbs == 2 ){ 
					document.frmParam.elements['powersave.msl_bp_rate'].readOnly=true;
					document.frmParam.elements['powersave.msl_bp_rate'].style.backgroundColor ='#D4D4D4';
				}else {
					document.frmParam.elements['powersave.msl_bp_rate'].readOnly=false;
				}
				
				if( lsbs==177||lsbs==207||lsbs==186){
					document.frmParam.elements['powersave.mps_jangka_inv'].disabled=true;
					document.frmParam.elements['powersave.mps_jangka_inv'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['powersave.msl_bp_rate'].readOnly=true;
					document.frmParam.elements['powersave.msl_bp_rate'].style.backgroundColor ='#D4D4D4';
				}
				
				document.frmParam.elements['investasiutama.total_persen'].value="100";
		
				break;		
				
			case 12: //investimax
			
				var lsbs = document.frmParam.elements['datausulan.lsbs_id'].value;
				var lsdbs = document.frmParam.elements['datausulan.lsdbs_number'].value;
				
				if(lsbs == 165 && lsdbs == 1){ //investimax 1
					for(var q = 0; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
					document.frmParam.elements['investasiutama.daftarinvestasi[20].mdu_persen1'].value="100";
					document.frmParam.elements['investasiutama.daftarinvestasi[20].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[20].mdu_persen1'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['investasiutama.total_persen'].value="100";					
				}else{
					for(var q = 0; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
					document.frmParam.elements['investasiutama.daftarinvestasi[28].mdu_persen1'].value="100";
					document.frmParam.elements['investasiutama.daftarinvestasi[28].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[28].mdu_persen1'].style.backgroundColor ='#D4D4D4';
					document.frmParam.elements['investasiutama.total_persen'].value="100";					
				}	
				break;
			
			case 13: //amanah link syariah
				for(var q = 0; q <= 5; q++) setInvestasiReadOnly(q, true);
				setInvestasiReadOnly(6, false);
				setInvestasiReadOnly(7, false);
				for(var q = 8; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				break;			

			case 14: //muamalat - mabrur (153-5)
				for(var q = 0; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				document.frmParam.elements['investasiutama.daftarinvestasi[6].mdu_persen1'].value=100; //set cepek untuk ekalink dinamic syariah fund	
					document.frmParam.elements['investasiutama.total_persen'].value="100";					
				break;
			case 15: //Stable link Syariah
				for(var q = 0; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				document.frmParam.elements['investasiutama.total_persen'].value="100";

				if (document.frmParam.elements['datausulan.kurs_premi'].value == '01'){
					document.frmParam.elements['investasiutama.daftarinvestasi[29].mdu_persen1'].value="100";
					document.frmParam.elements['investasiutama.daftarinvestasi[29].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[29].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
					document.frmParam.elements['investasiutama.daftarinvestasi[30].mdu_persen1'].value="";
					document.frmParam.elements['investasiutama.daftarinvestasi[30].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[30].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				}else{
					document.frmParam.elements['investasiutama.daftarinvestasi[29].mdu_persen1'].value="";
					document.frmParam.elements['investasiutama.daftarinvestasi[29].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[29].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				
					document.frmParam.elements['investasiutama.daftarinvestasi[30].mdu_persen1'].value="100";
					document.frmParam.elements['investasiutama.daftarinvestasi[30].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[30].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				}
				document.frmParam.elements['investasiutama.total_persen'].value="100";
		
				break;	
			case 16: //stable link 2
				for(var q = 0; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);				
				document.frmParam.elements['investasiutama.total_persen'].value="100";

				if (document.frmParam.elements['datausulan.kurs_premi'].value == '01'){
					document.frmParam.elements['investasiutama.daftarinvestasi[33].mdu_persen1'].value="100";
					document.frmParam.elements['investasiutama.daftarinvestasi[33].mdu_persen1'].readOnly=true;	
					document.frmParam.elements['investasiutama.daftarinvestasi[33].mdu_persen1'].style.backgroundColor ='#D4D4D4';	
				}
				
				var lsbs = document.frmParam.elements['datausulan.lsbs_id'].value;
				var lsdbs = document.frmParam.elements['datausulan.lsdbs_number'].value;
				
				if(lsbs == 164 && lsdbs == 11){ 
					document.frmParam.elements['powersave.msl_bp_rate'].readOnly=true;
					document.frmParam.elements['powersave.msl_bp_rate'].style.backgroundColor ='#D4D4D4';
				}
				document.frmParam.elements['investasiutama.total_persen'].value="100";
				break;	
			
			case 17: //buka Simas Stabile Fund, Simas Balance Fund , Simas Equity Fund
				for(var q = 0; q <= 40; q++) setInvestasiReadOnly(q, true);
				setInvestasiReadOnly(41, false);
				setInvestasiReadOnly(42, false);
				setInvestasiReadOnly(43, false);
				for(var q = 44; q < 46 ; q++) setInvestasiReadOnly(q, true);
				setInvestasiReadOnly(46, false);
				setInvestasiReadOnly(47, false);
				setInvestasiReadOnly(48, false);
			    for(var q = 49; q < jumlah_investasi ; q++) setInvestasiReadOnly(q, true); 
				/* for(q = 0; q <= 40; q++) setInvestasiReadOnly(q, true);
				setInvestasiReadOnly(41, false);
				setInvestasiReadOnly(42, false);
				setInvestasiReadOnly(43, false);
				for(q = 44; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true); */
				break;
			
			case 18: //Simas Stabile Dollar Fund , Simas Balance Dollar Fund, Excellink Stabile Dollar Fund , Excellink Balance Dollar Fund
				if((lsbs==134 && lsdbs==9) || (lsbs==220 && lsdbs==3)){
					for(var q = 0; q <= 43; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(44, false);
					setInvestasiReadOnly(45, false);			
					for(var q = 46; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);			
					break;	
				}else{
					for(var q = 0; q <= 43; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(44, false);
					setInvestasiReadOnly(45, false);
					for(var q = 46; q < 55; q++) setInvestasiReadOnly(q, true);
					setInvestasiReadOnly(55, false);
					setInvestasiReadOnly(56, false);
					for(var q = 57; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);
					break;
				}
				
			case 19: //buka Simas Stabile Fund, Simas Balance Fund , Simas Equity Fund
				for(var q = 0; q <= 48; q++) setInvestasiReadOnly(q, true);
				setInvestasiReadOnly(49, false);
				setInvestasiReadOnly(50, false);
				setInvestasiReadOnly(51, false);
				setInvestasiReadOnly(52, false);
				setInvestasiReadOnly(53, false);
				setInvestasiReadOnly(54, false);
				for(var q = 55; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);
				/* for(q = 0; q <= 40; q++) setInvestasiReadOnly(q, true);
				setInvestasiReadOnly(41, false);
				setInvestasiReadOnly(42, false);
				setInvestasiReadOnly(43, false);
				for(q = 44; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true); */
				break;
				
			case 20: //buka Excellink dan Simas Fund Syariah (Fixed, Dynamic, dan Aggressive)
				for(var q = 0; q <= 4; q++) setInvestasiReadOnly(q, true);
				setInvestasiReadOnly(5, false);
				setInvestasiReadOnly(6, false);
				setInvestasiReadOnly(7, false);
				for(var q = 8; q <= 56; q++) setInvestasiReadOnly(q, true);
				setInvestasiReadOnly(57, false);
				setInvestasiReadOnly(58, false);
				setInvestasiReadOnly(59, false);
				for(var q = 60; q < jumlah_investasi; q++) setInvestasiReadOnly(q, true);
				break;
		}
	
		<c:if test="${not empty daftarStable}">
			hideSelect('hidden'); 
			popup('popUpDiv');
		</c:if>
	}
	



	
	function tutup(){
		if ((document.frmParam.elements['datausulan.kode_flag'].value=='1') || (document.frmParam.elements['datausulan.kode_flag'].value=='11') ||
			(document.frmParam.elements['datausulan.kode_flag'].value=='16')){
			if(document.frmParam.elements['powersave.mps_roll_over'].value == '1')
			{

			}else{
				document.frmParam.elements['rekening_client.mrc_no_ac'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac'].style.backgroundColor ='#FFFFFF';
				
				/*Tambahan dari Bertho untuk split rekening*/
				document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].style.backgroundColor ='#FFFFFF';
			
				/*End of code*/
				document.frmParam.elements['rekening_client.mrc_nama'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_nama'].style.backgroundColor ='#FFFFFF';
				if (document.frmParam.elements['rekening_client.mrc_nama'].value =="")
				{
					document.frmParam.elements['rekening_client.mrc_nama'].value =  document.frmParam.elements['pemegang.mcl_first'].value;;
				}				
				
				document.frmParam.elements['rekening_client.lsbp_id'].readOnly = false;
				document.frmParam.elements['rekening_client.lsbp_id'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_kota'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_kota'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.tgl_surat'].readOnly = false;
				document.frmParam.elements['rekening_client.tgl_surat'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.notes'].readOnly = false;
				document.frmParam.elements['rekening_client.notes'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_kuasa'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_kuasa'].style.backgroundColor ='#FFFFFF';
				document.frmParam.mrc_kuasa1.disabled = false;			
				document.frmParam.elements['rekening_client.mrc_cabang'].readOnly = false;
				document.frmParam.elements['rekening_client.mrc_cabang'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_jenis'].disabled = false;
				document.frmParam.elements['rekening_client.mrc_jenis'].style.backgroundColor ='#FFFFFF';
				document.frmParam.elements['rekening_client.mrc_kurs'].disabled = false;
				document.frmParam.elements['rekening_client.mrc_kurs'].style.backgroundColor ='#FFFFFF';
				document.frmParam.caribank1.readOnly = false;
				document.frmParam.caribank1.style.backgroundColor ='#FFFFFF';
				document.frmParam.btncari1.disabled = false;
			}
		}

		var flag_account = document.frmParam.elements['datausulan.flag_account'].value;
		var flag_bao = document.frmParam.elements['datausulan.flag_bao'].value;
		
		if ( (flag_bao ==1) && ((flag_account==2)||(flag_account==3)) ){
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].disabled=false;
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].style.backgroundColor ='#FFFFFF';				
		}else{
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].value='0';	
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].disabled=true;
			document.frmParam.elements['rekening_client.mrc_jn_nasabah'].style.backgroundColor ='#D4D4D4';		
		}

		if ((flag_account ==2)||(flag_account ==3)){
			document.frmParam.elements['rekening_client.mrc_no_ac'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac'].style.backgroundColor ='#FFFFFF';
			/*Tambahan dari Bertho untuk split rekening*/
			document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].style.backgroundColor ='#FFFFFF';
			
			<c:if test="${not empty cmd.rekening_client.lsbp_id}">
// 				var flagCC = document.frmParam.elements['datausulan.mste_flag_cc'].value;
				
// 				if(flagCC!=1){
					ajaxPjngRek('${cmd.rekening_client.lsbp_id}', 1,'rekening_client.mrc_no_ac_split');	
// 				}else{
// 				    for(var i=0;i<16;i++){	
// 						document.frmParam.elements['rekening_client.mrc_no_ac_split'+'['+i+']'].readOnly = false;
// 						document.frmParam.elements['rekening_client.mrc_no_ac_split'+'['+i+']'].style.backgroundColor ='#FFFFFF';
// 					}
// 				    for(var i=16;i<20;i++){	
// 						document.frmParam.elements['rekening_client.mrc_no_ac_split'+'['+i+']'].readOnly = true;
// 						document.frmParam.elements['rekening_client.mrc_no_ac_split'+'['+i+']'].style.backgroundColor ='#D4D4D4';
// 					}
// 				}				
			</c:if>
		
			/*End of code*/
			document.frmParam.elements['rekening_client.mrc_nama'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_nama'].style.backgroundColor ='#FFFFFF';
			if (document.frmParam.elements['rekening_client.mrc_nama'].value ==""){
				document.frmParam.elements['rekening_client.mrc_nama'].value = document.frmParam.elements['pemegang.mcl_first'].value;
			}
			
			document.frmParam.elements['rekening_client.lsbp_id'].readOnly = false;
			document.frmParam.elements['rekening_client.lsbp_id'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_kota'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_kota'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.tgl_surat'].readOnly = false;
			document.frmParam.elements['rekening_client.tgl_surat'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.notes'].readOnly = false;
			document.frmParam.elements['rekening_client.notes'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_kuasa'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_kuasa'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_cabang'].readOnly = false;
			document.frmParam.elements['rekening_client.mrc_cabang'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_jenis'].disabled = false;
			document.frmParam.elements['rekening_client.mrc_jenis'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['rekening_client.mrc_kurs'].disabled = false;
			document.frmParam.elements['rekening_client.mrc_kurs'].style.backgroundColor ='#FFFFFF';
			document.frmParam.caribank1.readOnly = false;
			document.frmParam.caribank1.style.backgroundColor ='#FFFFFF';
			document.frmParam.btncari1.disabled = false;
		}else{
			document.frmParam.elements['rekening_client.mrc_no_ac'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac'].value = '';
			
			/*Tambahan dari Bertho untuk split rekening*/
			document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[0]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[1]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[2]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[3]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[4]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[5]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[6]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[7]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[8]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[9]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[10]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[11]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[12]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[13]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[14]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[15]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[16]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[17]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[18]'].value = '';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_no_ac_split[19]'].value = '';
			
			document.frmParam.elements['rekening_client.mrc_nama'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_nama'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_nama'].value='';
			document.frmParam.elements['rekening_client.lsbp_id'].disabled = true;
			document.frmParam.elements['rekening_client.lsbp_id'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.lsbp_id'].value='NULL';
			document.frmParam.elements['rekening_client.mrc_kota'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_kota'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_kota'].value='';
			document.frmParam.elements['rekening_client.tgl_surat'].readOnly = true;
			document.frmParam.elements['rekening_client.tgl_surat'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.tgl_surat'].value='';			
			document.frmParam.elements['rekening_client.notes'].readOnly = true;
			document.frmParam.elements['rekening_client.notes'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.notes'].value='';
			document.frmParam.mrc_kuasa1.disabled = true;
			document.frmParam.mrc_kuasa1.checked = false;
			document.frmParam.elements['rekening_client.mrc_kuasa'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_kuasa'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_kuasa'].value='0';
			document.frmParam.elements['rekening_client.mrc_cabang'].readOnly = true;
			document.frmParam.elements['rekening_client.mrc_cabang'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_cabang'].value='';
			document.frmParam.elements['rekening_client.mrc_jenis'].disabled = true;
			document.frmParam.elements['rekening_client.mrc_jenis'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_jenis'].value='0';
			document.frmParam.elements['rekening_client.mrc_kurs'].disabled = true;
			document.frmParam.elements['rekening_client.mrc_kurs'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['rekening_client.mrc_kurs'].value='';			
			document.frmParam.caribank1.readOnly = true;
			document.frmParam.caribank1.style.backgroundColor ='#D4D4D4';
			document.frmParam.caribank1.value='';
			document.frmParam.btncari1.disabled = true;

		}

		if ((document.frmParam.elements['datausulan.mste_flag_cc'].value=='1') || (document.frmParam.elements['datausulan.mste_flag_cc'].value=='2') ||
			(document.frmParam.elements['datausulan.mste_flag_cc'].value=='9') || (document.frmParam.elements['datausulan.flag_account'].value== 3)){
			document.frmParam.elements['account_recur.mar_holder'].readOnly = false;
			document.frmParam.elements['account_recur.mar_holder'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.flag_autodebet_nb'].disabled = false;
			document.frmParam.elements['account_recur.flag_autodebet_nb'].style.backgroundColor ='#FFFFFF';
			if (document.frmParam.elements['account_recur.mar_holder'].value ==''){
				document.frmParam.elements['account_recur.mar_holder'].value = 	document.frmParam.elements['pemegang.mcl_first'].value;
			}				
			
			document.frmParam.elements['account_recur.lbn_id'].disabled = false;
			document.frmParam.elements['account_recur.lbn_id'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.flag_jn_tabungan'].disabled = false;
			document.frmParam.elements['account_recur.flag_jn_tabungan'].style.backgroundColor ='#FFFFFF';
			
			document.frmParam.elements['account_recur.mar_acc_no'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no'].style.backgroundColor ='#FFFFFF';
			/*tambahan setting dari Bertho untuk split mar_acc_no*/
			document.frmParam.elements['account_recur.mar_acc_no_split[0]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[0]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[1]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[1]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[2]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[2]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[3]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[3]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[4]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[4]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[5]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[5]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[6]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[6]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[7]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[7]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[8]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[8]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[9]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[9]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[10]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[10]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[11]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[11]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[12]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[12]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[13]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[13]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[14]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[14]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[15]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[15]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[16]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[16]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[17]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[17]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[18]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[18]'].style.backgroundColor ='#FFFFFF';
			document.frmParam.elements['account_recur.mar_acc_no_split[19]'].readOnly = false;
			document.frmParam.elements['account_recur.mar_acc_no_split[19]'].style.backgroundColor ='#FFFFFF';
		
			<c:if test="${not empty cmd.account_recur.lbn_id}">
				var flagCC = document.frmParam.elements['datausulan.mste_flag_cc'].value;
				
				if(flagCC!=1){
					ajaxPjngRek('${cmd.account_recur.lbn_id}', 2,'account_recur.mar_acc_no_split');	
				}else{
				    for(var i=0;i<16;i++){	
						document.frmParam.elements['account_recur.mar_acc_no_split'+'['+i+']'].readOnly = false;
						document.frmParam.elements['account_recur.mar_acc_no_split'+'['+i+']'].style.backgroundColor ='#FFFFFF';
					}
				    for(var i=16;i<20;i++){	
						document.frmParam.elements['account_recur.mar_acc_no_split'+'['+i+']'].readOnly = true;
						document.frmParam.elements['account_recur.mar_acc_no_split'+'['+i+']'].style.backgroundColor ='#D4D4D4';
					}
				}									
			</c:if>
			
			/*end of setting*/
			document.frmParam.caribank2.readOnly = false;
			document.frmParam.caribank2.style.backgroundColor ='#FFFFFF';
			document.frmParam.btncari2.disabled = false;
		}else{
			document.frmParam.elements['account_recur.mar_holder'].readOnly = true;
			document.frmParam.elements['account_recur.mar_holder'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_holder'].value='';			
			document.frmParam.elements['account_recur.flag_autodebet_nb'].disabled = true;
			document.frmParam.elements['account_recur.flag_autodebet_nb'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.flag_autodebet_nb'].value='0';	
			document.frmParam.elements['account_recur.lbn_id'].disabled = true;
			document.frmParam.elements['account_recur.lbn_id'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.lbn_id'].value='NULL';
			document.frmParam.elements['account_recur.flag_jn_tabungan'].disabled = true;
			document.frmParam.elements['account_recur.flag_jn_tabungan'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no'].style.backgroundColor ='#D4D4D4';
			/*tambahan setting dari Bertho untuk split mar_acc_no*/
			
			document.frmParam.elements['account_recur.mar_acc_no_split[0]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[0]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[1]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[1]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[2]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[2]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[3]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[3]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[4]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[4]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[5]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[5]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[6]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[6]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[7]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[7]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[8]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[8]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[9]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[9]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[10]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[10]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[11]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[11]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[12]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[12]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[13]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[13]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[14]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[14]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[15]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[15]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[16]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[16]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[17]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[17]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[18]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[18]'].style.backgroundColor ='#D4D4D4';
			document.frmParam.elements['account_recur.mar_acc_no_split[19]'].readOnly = true;
			document.frmParam.elements['account_recur.mar_acc_no_split[19]'].style.backgroundColor ='#D4D4D4';
			
			/*end of setting*/
			document.frmParam.elements['account_recur.mar_acc_no'].value='';
			document.frmParam.caribank2.readOnly = true;
			document.frmParam.caribank2.style.backgroundColor ='#D4D4D4';
			document.frmParam.caribank2.value='';
			document.frmParam.btncari2.disabled = true;
		}
	}
				
	if(!document.layers&&!document.all&&!document.getElementById) event="test";
	

</script>



<body bgcolor="ffffff" onLoad="Body_onload();">

<c:if test="${not empty cmd.pesan}">
	<script>alert('${cmd.pesan}');</script>
</c:if>


<XML ID=xmlData></XML> 

 <form name="frmParam" method="post" >
<table border="0" width="100%" height="677" cellspacing="1" cellpadding="1" class="entry2">
  <tr> 
    <td width="67%" height="35" bgcolor="#FDFDFD" valign="bottom"> 
 			<input type="submit" name="_target0" value=" " onclick=" prev()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/pp1.jpg);"
				accesskey="1" onmouseover="return overlib('Alt-1', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<c:choose><c:when test="${(sessionScope.currentUser.jn_bank eq '16')}">
			<input type="submit" name="_target1" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ttgbsim.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();"></c:when><c:otherwise>
			<input type="submit" name="_target1" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ttg1.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();"></c:otherwise></c:choose>				
			<input type="submit" name="_target1" value=" " onclick="next1()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ppremi1.jpg);"
				accesskey="2" onmouseover="return overlib('Alt-2', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target2" value=" " onclick=" prev()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ddu1.jpg);"
				accesskey="3" onmouseover="return overlib('Alt-3', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target3" value=" " onclick=" prev()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/inv2.jpg);"
				accesskey="4" onmouseover="return overlib('Alt-4', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target4" value=" " onclick=" prev()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/ag1.jpg);"
				accesskey="5" onmouseover="return overlib('Alt-5', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="submit" name="_target5" value=" " onclick=" prev()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/kf1.jpg);"
				accesskey="6" onmouseover="return overlib('Alt-6', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" name="dummy2" value=" " onclick=" prev()" <c:if test="${cmd.pemegang.reg_spaj eq null}"> disabled </c:if>
				class="bacNavigation" style="background-image: url(${path}/include/image/sb1.jpg);">		
 	
    </td>
  </tr>
  <tr><td bgcolor="#DDDDDD">
  
  </td></tr>
  <tr> 
    <td width="67%" rowspan="5" align="left" valign="top"> 
<spring:bind path="cmd.*">
	<c:if test="${not empty status.errorMessages}">
	<div id="error">
	INFO:<br>
	<c:forEach var="error" items="${status.errorMessages}">
	- <c:out value="${error}" escapeXml="false" />
	<br />
	</c:forEach></div>
	</c:if>									
</spring:bind>     
        <table border="0" width="100%" cellspacing="1" cellpadding="1"  class="entry">

          <tr> 
            <td  align="center" colspan="4"> 
              <div align="center">
                <input type="submit" name="_target${halaman-1}" value="Prev &laquo;" onClick="prev()" 
					onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
                <input type="submit" name="_target${halaman+1}" value="Next &raquo;" onClick="next()" 
					onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
                <input type="hidden" name="_page" value="${halaman}">
              </div>
            </td>
          </tr>
        </table>
        <table border="0" width="100%" cellspacing="1" cellpadding="1" height="47" class="entry">
          <tr> 
            <th class="subtitle"  height="27" colspan="9"> <p align="center"><b> 
                <font color="#FFFFFF" size="2" face="Verdana">DATA PRODUK INVESTASI</font></b> 
                <spring:bind path="cmd.datausulan.flag_bao"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }" >
                </spring:bind> <spring:bind path="cmd.datausulan.kode_flag"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }"  >
                </spring:bind> <spring:bind path="cmd.datausulan.isBungaSimponi"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }"  >
                </spring:bind> <spring:bind path="cmd.datausulan.isBonusTahapan"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }"  >
                </spring:bind> <spring:bind path="cmd.datausulan.flag_account"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }"  >
                </spring:bind> 
                <spring:bind path="cmd.datausulan.lsbs_id"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }"  >
                </spring:bind> 
                <spring:bind path="cmd.datausulan.lsdbs_number"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }"  >
                </spring:bind>
                <spring:bind path="cmd.pemegang.mcl_first"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30" >
                </spring:bind> 
                <spring:bind path="cmd.datausulan.kurs_premi"> 
                <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30" >
                </spring:bind></th>
                <input type="hidden" id="alertEkaSehat" name="alertEkaSehat" value="${cmd.datausulan.alertEkaSehat}"></th>
          </tr>
          <tr  > 
            <th  height="20 " colspan="9"> <p align="left"><b> <font face="Verdana" size="1" color="#CC3300">Detail 
                Investasi Khusus Produk Unit Link</font></b></p></th>
          </tr>
          <tr  > 
            <th class="subtitle2" height="20" width="2%"><b> <font face="Verdana" size="1" color="#FFFFFF"></font></b></th>
            <th class="subtitle2" colspan="5" height="20"> <b> <font face="Verdana" size="1" color="#FFFFFF">Perhitungan 
              Premi ( sesuai dengan cara pembayaran )</font></b> </th>
          </tr>
          <tr  > 
            <th width="2%">&nbsp;</th>
            <th colspan="2" > <p align="left"><b><font face="Verdana" size="1" color="#996600">Premi 
                Pokok</font></b><font size="1" face="Verdana"> </font> </th>
            <th > <input type="text" name="mspr_premium"
						value=<fmt:formatNumber type="number" value="${cmd.datausulan.mspr_premium}"/> 
              size="30" style='background-color :#D4D4D4' readOnly> </th>
            <th colspan="2" >&nbsp;</th>
          </tr>
          <tr  > 
            <th width="2%">&nbsp;</th>
            <th colspan="2" ><b><font face="Verdana" size="1" color="#996600">Premi 
              Top Up Berkala</font></b></th>
            <th > <spring:bind path="cmd.investasiutama.daftartopup.premi_berkala"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
             </spring:bind><font color="#CC3300">*</font></th>
            <th width="12%"> <div align="right"><b><font face="Verdana" size="1" color="#996600">Jenis 
                Top Up </font></b> </div></th>
            <th width="32%"> <select name="investasiutama.daftartopup.pil_berkala" 
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <c:forEach var="jnstopup" items="${select_jns_top_up}"> 
             	  <c:if test="${jnstopup.ID ne 2 }">
					 <option <c:if test="${cmd.investasiutama.daftartopup.pil_berkala eq jnstopup.ID}"> SELECTED </c:if> value="${jnstopup.ID}">${jnstopup.JENIS}</option> 
                  </c:if>
                </c:forEach> </select>
              <font color="#CC3300">*</font> </th>
          </tr>
       <tr  > 
            <th width="2%">&nbsp;</th>
            <th colspan="2" ><b><font face="Verdana" size="1" color="#996600">Premi 
              Top Up Tunggal</font></b></th>
            <th > <spring:bind path="cmd.investasiutama.daftartopup.premi_tunggal"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
             </spring:bind><font color="#CC3300">*</font></th>
            <th width="12%"> <div align="right"><b><font face="Verdana" size="1" color="#996600">Jenis 
                Top Up </font></b> </div></th>
            <th width="32%"> <select name="investasiutama.daftartopup.pil_tunggal" 
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <c:forEach var="jnstopup" items="${select_jns_top_up}"> 
                 <c:if test="${jnstopup.ID ne 1 }">
				   	<option <c:if test="${cmd.investasiutama.daftartopup.pil_tunggal eq jnstopup.ID}"> SELECTED </c:if> value="${jnstopup.ID}">${jnstopup.JENIS}</option> 
                 </c:if>
                </c:forEach> </select>
              <font color="#CC3300">*</font> </th>
          </tr>
          <tr  > 
            <th width="2%">&nbsp;</th>
            <th colspan="2" ><b><font face="Verdana" size="1" color="#996600">Premi 
              Tambahan </font></b></th>
            <th > <spring:bind path="cmd.datausulan.total_premi_rider"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" style='background-color :#D4D4D4'readOnly>
               </spring:bind> 
            </th>
            <th colspan="2" >&nbsp;</th>
          </tr>
          <tr  > 
            <th width="2%">&nbsp;</th>
            <th  colspan="3"> <hr> </th>
            <th colspan="2" >&nbsp;</th>
          </tr>
          <tr  > 
            <th width="2%">&nbsp;</th>
            <th colspan="2" ><b><font face="Verdana" size="1" color="#996600">Jumlah 
              </font></b></th>
            <th > <spring:bind path="cmd.investasiutama.total_premi_sementara"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" style='background-color :#D4D4D4'readOnly>
               </spring:bind> 
            </th>
            <th colspan="2" >&nbsp;</th>
          </tr>
          <tr  > 
            <th class="subtitle2" height="20" width="2%"><b><font face="Verdana" size="1" color="#FFFFFF">&nbsp;&nbsp;</font></b></th>
            <c:choose>
            <c:when test="${cmd.datausulan.lsbs_id eq '186' and cmd.datausulan.lsdbs_number eq '3'}">
            	<th class="subtitle2" colspan="5" height="20"> <b> <font face="Verdana" size="1" color="#FFFFFF">No. 
	              Rekening Sumber Dana</font></b> 
	            </th>
            </c:when> <c:otherwise> 
	            <th class="subtitle2" colspan="5" height="20"> <b> <font face="Verdana" size="1" color="#FFFFFF">No. 
              Rekening Pemegang Polis Yang Digunakan untuk Transaksi (KHUSUS PRODUK 
              INVESTASI)</font></b> </th>
             </c:otherwise> </c:choose>
            
          </tr>
          <tr  > 
            <th  ></th>
            <th  > Bentuk Pembayaran Premi </th>
            <th  ></th>
            <th colspan="6" > <spring:bind path="cmd.datausulan.mste_flag_cc"> 
              <select name="${status.expression }"  onchange="tutup();"
			  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <option value="" /> <c:forEach var="auto" items="${select_autodebet}"> <option value="${auto.ID}"
						   
                <c:if test="${cmd.datausulan.mste_flag_cc eq auto.ID}">selected</c:if> >${auto.AUTODEBET}</option> 
                </c:forEach> 
              </select>
              <font color="#CC3300">*</font>  </spring:bind> </th>
          </tr>
           <tr>          
          		<th></th>
          		<th width="15%"  height="20"><font size="1" face="Verdana"><b><font face="Verdana" size="1" color="#996633"> 
	              </font></b> </font><b> <font size="1" face="Verdana"><b><font face="Verdana" size="1" color="#996633">Cari 
	              Bank&nbsp;</font></b></font><font face="Verdana" size="1" color="#996633"> 
	              </font></b> </th>
	             <th></th>
	            <th  height="20"> <input type="text" name="caribank1" onkeypress="if(event.keyCode==13){ document.frmParam.btncari1.click(); return false;}"> 
	              <input type="button" name="btncari1" value="Cari" onclick="ajaxSelectWithParam1a(document.frmParam.caribank1.value,'select_bank1','bank1','rekening_client.lsbp_id','', 'BANK_ID', 'BANK_NAMA', 'ajaxPjngRek(this.value, 1,\'rekening_client.mrc_no_ac_split\')','Silahkan pilih BANK','3');"> 
	            </th>
             
          
            <th colspan="3"></th>
          </tr>
          <tr>          
          	<th></th>
          	 <th width="15%"  height="20"><font size="1" face="Verdana"> <b><font face="Verdana" size="1" color="#996633">Bank&nbsp; 
              </font></b></font></th>
               <th></th>
            <th  height="20"> <div id="bank1"
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> 
                <select name="rekening_client.lsbp_id" >
                  <option value="${cmd.rekening_client.lsbp_id}">${cmd.rekening_client.lsbp_nama}</option>
                </select><font color="#CC3300">*</font>
                </div>
               
                </th>            
          
             <th width="15%" ><font size="1" face="Verdana"><b><font face="Verdana" size="1" color="#996633">Cabang 
              </font></b> </font><b><font size="1" face="Verdana"> </font> </b><font size="1" face="Verdana">&nbsp; 
              </font></th>
            <th  height="20"><spring:bind path="cmd.rekening_client.mrc_cabang"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" maxlength="20" 
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
               </spring:bind></th>
               <th></th>
          </tr>
           <tr  > 
            <th width="2%" >&nbsp;</th>
            <th colspan="2"  ><b><font face="Verdana" size="1" color="#996633">Jenis 
              Tabungan </font></b> </th>
            <th width="24%"> <select name="rekening_client.mrc_jenis" 
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <c:forEach var="tab" items="${select_jenis_tabungan}"> <option 
							<c:if test="${cmd.rekening_client.mrc_jenis eq tab.ID}"> SELECTED </c:if>
							value="${tab.ID}">${tab.AUTODEBET}</option> </c:forEach> 
              </select>
              <font color="#CC3300">*</font> </th>
               <th width="15%"  height="20"><font size="1" face="Verdana"> <b><font face="Verdana" size="1" color="#996633">Kota 
              </font></b></font>
               </th>
            <th  height="20">     <div>
								<spring:bind path="cmd.rekening_client.mrc_kota"> 
					              <input type="text" name="${status.expression}"
											value="${status.value }"  size="30" maxlength="20" 
								 <c:if test="${ not empty status.errorMessage}">
											style='background-color :#FFE1FD'
										</c:if>>
					              </spring:bind>
				</div> </th>
          </tr>
          <tr  > 
            <th width="2%" >&nbsp;</th>
            <th colspan="2"  ><b><font face="Verdana" size="1" color="#996633">Jenis 
              Nasabah </font></b></th>
            <th> <select name="rekening_client.mrc_jn_nasabah" 
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                <c:forEach var="nsbh" items="${select_jenis_nasabah}"> <option 
					<c:if test="${cmd.rekening_client.mrc_jn_nasabah eq nsbh.ID}"> SELECTED </c:if>
					value="${nsbh.ID}">${nsbh.JENIS}</option> </c:forEach> </select> 
            </th>
            
            <th width="15%" ></th>
            <th  height="20"></th>
          </tr>
          <tr  > 
            <th width="2%"  height="20">&nbsp;</th>
            <th  height="20" colspan="2" ><b><font face="Verdana" size="1" color="#996633">No. 
              Rekening&nbsp; </font></b><b><font size="1" face="Verdana"> </font> 
              </b>					</th>
            <th  height="20" width="24%" colspan="4"><spring:bind path="cmd.rekening_client.mrc_no_ac"> 
              	<input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30" maxlength="20"
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>

					<input type="text" name="rekening_client.mrc_no_ac_split[0]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(0, 1);" value="${cmd.rekening_client.mrc_no_ac_split[0]}" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="rekening_client.mrc_no_ac_split[1]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(1, 1);" value="${cmd.rekening_client.mrc_no_ac_split[1]}" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="rekening_client.mrc_no_ac_split[2]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(2, 1);" value="${cmd.rekening_client.mrc_no_ac_split[2]}" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="rekening_client.mrc_no_ac_split[3]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(3, 1);" value="${cmd.rekening_client.mrc_no_ac_split[3]}" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="rekening_client.mrc_no_ac_split[4]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(4, 1);" value="${cmd.rekening_client.mrc_no_ac_split[4]}" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="rekening_client.mrc_no_ac_split[5]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(5, 1);" value="${cmd.rekening_client.mrc_no_ac_split[5]}" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="rekening_client.mrc_no_ac_split[6]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(6, 1);" value="${cmd.rekening_client.mrc_no_ac_split[6]}" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="rekening_client.mrc_no_ac_split[7]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(7, 1);" value="${cmd.rekening_client.mrc_no_ac_split[7]}" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="rekening_client.mrc_no_ac_split[8]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(8, 1);" value="${cmd.rekening_client.mrc_no_ac_split[8]}" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="rekening_client.mrc_no_ac_split[9]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(9, 1);" value="${cmd.rekening_client.mrc_no_ac_split[9]}" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="rekening_client.mrc_no_ac_split[10]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(10, 1);" value="${cmd.rekening_client.mrc_no_ac_split[10]}" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="rekening_client.mrc_no_ac_split[11]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(11, 1);" value="${cmd.rekening_client.mrc_no_ac_split[11]}" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="rekening_client.mrc_no_ac_split[12]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(12, 1);" value="${cmd.rekening_client.mrc_no_ac_split[12]}" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="rekening_client.mrc_no_ac_split[13]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(13, 1);" value="${cmd.rekening_client.mrc_no_ac_split[13]}" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="rekening_client.mrc_no_ac_split[14]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(14, 1);" value="${cmd.rekening_client.mrc_no_ac_split[14]}" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="rekening_client.mrc_no_ac_split[15]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(15, 1);" value="${cmd.rekening_client.mrc_no_ac_split[15]}" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="rekening_client.mrc_no_ac_split[16]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(16, 1);" value="${cmd.rekening_client.mrc_no_ac_split[16]}" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="rekening_client.mrc_no_ac_split[17]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(17, 1);" value="${cmd.rekening_client.mrc_no_ac_split[17]}" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="rekening_client.mrc_no_ac_split[18]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(18, 1);" value="${cmd.rekening_client.mrc_no_ac_split[18]}" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="rekening_client.mrc_no_ac_split[19]" size="1"  maxlength="1" onfocus="select();" value="${cmd.rekening_client.mrc_no_ac_split[19]}" <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>

					
				<font color="#CC3300">*</font>
              </spring:bind> 
              <select name="rekening_client.mrc_kurs" 
			   <c:if test="${ not empty status.errorMessage}" >
						style='background-color :#FFE1FD'
					</c:if>>
                <c:forEach var="kurs" items="${select_kurs}"> <option 
							<c:if test="${cmd.rekening_client.mrc_kurs eq kurs.ID}"> SELECTED </c:if>
							value="${kurs.ID}">${kurs.SYMBOL}</option> </c:forEach> 
              </select>
              <font color="#CC3300">*</font> </th>
                  
          </tr>       
          <tr  > 
            <th width="2%"  height="20">&nbsp;</th>
            <th  height="20" colspan="2" ><b><font face="Verdana" size="1" color="#996633">Atas 
              Nama </font></b><b><font size="1" face="Verdana"> </font> </b></th>
            <th  height="20" width="24%"><spring:bind path="cmd.rekening_client.mrc_nama"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="35" maxlength="60" 
			 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>> <font color="#CC3300">*</font>
              </spring:bind>
              <%-- <spring:bind path="cmd.pemegang.confNm"> 
              	<input type="hidden" id="${status.expression}" name="${status.expression}" value="${status.value }" onkeypress="alert(this.id)">
              </spring:bind>
              <c:if test='${cmd.pemegang.confNm eq \"tidak cocok\"}'>
	              	<script type="text/javascript">
	              		if(confirm('Nama Pemilik Rekening tidak sama dengan nama pemegang. Lanjutkan ?')) {
	              			document.getElementById('pemegang.confNm').value = "ok";
	              			frmParam.submit();
	              		}
	              	</script>
              </c:if> --%>
            </th>
              <th width="15%"  height="20"></th>
            <th  height="20"> 
            
            </th>
          </tr>
         
          <tr  > 
            <th >&nbsp;</th>
            <th  valign="top"><b><font face="Verdana" size="1" color="#996633">Memberi 
              Kuasa 
              <input type="checkbox" name="mrc_kuasa1" class="noBorder" 
						value="${cmd.rekening_client.mrc_kuasa}"  size="30" onClick="kuasa_onClick();" 
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
              <spring:bind path="cmd.rekening_client.mrc_kuasa"> 
              <input type="hidden" name="${status.expression}"
						value="${cmd.rekening_client.mrc_kuasa}"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind> &nbsp;&nbsp;&nbsp;&nbsp; </font></b></th>
            <th  valign="top"><b><font face="Verdana" size="1" color="#996633">Keterangan 
              </font></b></th>
            <th> <spring:bind path="cmd.rekening_client.notes"> 
              <textarea cols="40" rows="7" name="${status.expression }"
						onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); "
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>${status.value }</textarea>
               </spring:bind> </th>
            <th valign="top"><b><font color="#996633" size="1" face="Verdana">
            	
            	<div>Tanggal Surat</div>

            	</font></b></th>
            <th valign="top"><b><font color="#996633" size="1" face="Verdana">
            	
				<div style="vertical-align: top" >
							<spring:bind path="cmd.rekening_client.tgl_surat"> 
				              <c:choose> <c:when test="${cmd.datausulan.flag_bao eq '1' or cmd.datausulan.flag_account  eq '2'  or cmd.datausulan.flag_account eq '3'}"> 
				              <script>inputDate('${status.expression}', '${status.value}', false);</script>
				              </c:when> <c:otherwise> 
				              <script>inputDate('${status.expression}', '${status.value}', true);</script>
				              </c:otherwise> </c:choose> </spring:bind>
				</div>
            	</font></b></th>
           	  </th>
          </tr>
          <tr  > 
            <th class="subtitle2" height="20" width="2%"><b><font face="Verdana" size="1" color="#FFFFFF">&nbsp;&nbsp;</font></b></th>
            <c:choose>
            <c:when test="${cmd.datausulan.lsbs_id eq '186' and cmd.datausulan.lsdbs_number eq '3'}">
            	<th class="subtitle2" colspan="5" height="20"> <b> <font face="Verdana" size="1" color="#FFFFFF">No. 
	              Rekening SSP</font></b> 
	            </th>
            </c:when> <c:otherwise> 
	            <th class="subtitle2" colspan="5" height="20"> <b> <font face="Verdana" size="1" color="#FFFFFF">No. 
	              Rekening Pemegang Polis Yang Digunakan untuk Transaksi (AUTODEBET)</font></b> 
	            </th>
             </c:otherwise> </c:choose>
            
          </tr>
          <tr>          
          	<th></th>
          	 <th width="15%"  height="20"><b> <font face="Verdana" size="1" color="#996600">Cari 
              &nbsp;</font><font face="Verdana" size="1" color="#996633">Bank&nbsp; 
              </font><font face="Verdana" size="1" color="#996600"> </font> <font face="Verdana" size="1" color="#996633"> 
              </font></b></th>
              <th></th>
             <c:choose> 
             	<c:when test="${cmd.datausulan.mste_flag_cc eq '1'}">
            		<th  height="20" colspan="1"> <input type="text" name="caribank2" onkeypress="if(event.keyCode==13){ document.frmParam.btncari2.click(); return false;}"> 
              			<input type="button" name="btncari2" value="Cari" onclick="ajaxSelectWithParam1a(document.frmParam.caribank2.value,'select_bankcredit','bank2','account_recur.lbn_id','', 'BANK_ID', 'BANK_NAMA', '','Silahkan pilih BANK','32',${cmd.datausulan.lsbs_id},${cmd.datausulan.lsdbs_number});">  
            		</th>
            	</c:when>
            	<c:when test="${(cmd.datausulan.lsbs_id eq 120 and (cmd.datausulan.lsdbs_number eq 22 or cmd.datausulan.lsdbs_number eq 23 or cmd.datausulan.lsdbs_number eq 24)) }">
            		<th  height="20" colspan="1"> <input type="text" name="caribank2" onkeypress="if(event.keyCode==13){ document.frmParam.btncari2.click(); return false;}"> 
              			<input type="button" name="btncari2" value="Cari" onclick="ajaxSelectWithParam1a(document.frmParam.caribank2.value,'select_bank3','bank2','account_recur.lbn_id','', 'BANK_ID', 'BANK_NAMA', 'ajaxPjngRek(this.value, 2,\'account_recur.mar_acc_no_split\')','Silahkan pilih BANK','32',${cmd.datausulan.lsbs_id},${cmd.datausulan.lsdbs_number});">  
            		</th>
            	</c:when>
            	<c:when test="${(cmd.datausulan.lsbs_id eq 163 and (cmd.datausulan.lsdbs_number > 5 or cmd.datausulan.lsdbs_number < 11 )) }">
            		<th  height="20" colspan="1"> <input type="text" name="caribank2" onkeypress="if(event.keyCode==13){ document.frmParam.btncari2.click(); return false;}"> 
              			<input type="button" name="btncari2" value="Cari" onclick="ajaxSelectWithParam1a(document.frmParam.caribank2.value,'select_bank3','bank2','account_recur.lbn_id','', 'BANK_ID', 'BANK_NAMA', 'ajaxPjngRek(this.value, 2,\'account_recur.mar_acc_no_split\')','Silahkan pilih BANK','32',${cmd.datausulan.lsbs_id},${cmd.datausulan.lsdbs_number});">  
            		</th>
            	</c:when>
            	<c:otherwise>
            		<th  height="20" colspan="1"> <input type="text" name="caribank2" onkeypress="if(event.keyCode==13){ document.frmParam.btncari2.click(); return false;}"> 
              			<input type="button" name="btncari2" value="Cari" onclick="ajaxSelectWithParam1a(document.frmParam.caribank2.value,'select_bank2','bank2','account_recur.lbn_id','', 'BANK_ID', 'BANK_NAMA', 'ajaxPjngRek(this.value, 2,\'account_recur.mar_acc_no_split\')','Silahkan pilih BANK','32',${cmd.datausulan.lsbs_id},${cmd.datausulan.lsdbs_number});">  
            		</th>          
            	</c:otherwise>
            </c:choose>
            <th colspan="3"></th>
          </tr>
          <tr>          
          	<th></th>
          	<th height="20"><b><font face="Verdana" size="1" color="#996633">Bank&nbsp; 
              </font><font face="Verdana" size="1" color="#996600"> </font> </b>
             </th>
             <th></th>
            <th  height="20" > <div id="bank2"> 
                <select name="account_recur.lbn_id"
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                  <option value="${cmd.account_recur.lbn_id}">${cmd.account_recur.lbn_nama}</option>
                </select>
                <font color="#CC3300">*</font></div></th>
             <th  height="20" > <div id="bankMuamalat"> 
                <select name="account_recur.lbn_id2" disabled="disabled" 
                <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                  <option value="${cmd.account_recur.lbn_id}">${cmd.account_recur.nama}</option>
                </select>
                <font color="#CC3300">*</font></div></th>
            <th colspan="3"><input type="hidden" name="flag_muamalat" value="0"></th>
            
          </tr>
          
       <tr>          
          	<th></th>
          	<th height="20"><b><font face="Verdana" size="1" color="#996633">Jenis Rekening <%-- ${cmd.account_recur.flag_jn_tabungan} --%> &nbsp; 
              </font><font face="Verdana" size="1" color="#996600"> </font> </b>
             </th>
             <th></th>
             <th>
				<select name="account_recur.flag_jn_tabungan">
	            	<option <c:if test="${cmd.account_recur.flag_jn_tabungan eq '0'}">SELECTED</c:if> value="0">Regular</option> 
	           	    <option <c:if test="${cmd.account_recur.flag_jn_tabungan eq '1'}">SELECTED</c:if> value="1">Tabunganku</option> 
				</select> 
			 </th>
                <th colspan ="3"></th>
        </tr>
          
          <tr> 
            <th width="2%"  height="20">&nbsp;</th>
            <th   height="20" colspan="2"><b> <font face="Verdana" size="1" color="#996633">No. 
              Rekening&nbsp; </font></b></th>
            <th width="24%"  height="20" colspan="3"> 
            	<spring:bind path="cmd.account_recur.mar_acc_no"> 
          		  <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30" maxlength="20" 
				 	<c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="account_recur.mar_acc_no_split[0]" size="1"    maxlength="1" onfocus="select();" onkeyup="dofo(0, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[0]}">value="${cmd.account_recur.mar_acc_no_split[0]}"</c:if>  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="account_recur.mar_acc_no_split[1]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(1, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[1]}">value="${cmd.account_recur.mar_acc_no_split[1]}"</c:if>   <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="account_recur.mar_acc_no_split[2]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(2, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[2]}">value="${cmd.account_recur.mar_acc_no_split[2]}"</c:if>   <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="account_recur.mar_acc_no_split[3]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(3, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[3]}">value="${cmd.account_recur.mar_acc_no_split[3]}"</c:if>   <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="account_recur.mar_acc_no_split[4]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(4, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[4]}">value="${cmd.account_recur.mar_acc_no_split[4]}"</c:if>   <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="account_recur.mar_acc_no_split[5]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(5, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[5]}">value="${cmd.account_recur.mar_acc_no_split[5]}"</c:if>  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="account_recur.mar_acc_no_split[6]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(6, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[6]}">value="${cmd.account_recur.mar_acc_no_split[6]}"</c:if>  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="account_recur.mar_acc_no_split[7]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(7, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[7]}">value="${cmd.account_recur.mar_acc_no_split[7]}"</c:if>  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="account_recur.mar_acc_no_split[8]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(8, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[8]}">value="${cmd.account_recur.mar_acc_no_split[8]}"</c:if>  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="account_recur.mar_acc_no_split[9]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(9, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[9]}">value="${cmd.account_recur.mar_acc_no_split[9]}"</c:if>  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="account_recur.mar_acc_no_split[10]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(10, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[10]}">value="${cmd.account_recur.mar_acc_no_split[10]}"</c:if>  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="account_recur.mar_acc_no_split[11]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(11, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[11]}">value="${cmd.account_recur.mar_acc_no_split[11]}"</c:if>  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="account_recur.mar_acc_no_split[12]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(12, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[12]}">value="${cmd.account_recur.mar_acc_no_split[12]}"</c:if>  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="account_recur.mar_acc_no_split[13]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(13, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[13]}">value="${cmd.account_recur.mar_acc_no_split[13]}"</c:if>  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="account_recur.mar_acc_no_split[14]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(14, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[14]}">value="${cmd.account_recur.mar_acc_no_split[14]}"</c:if>  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="account_recur.mar_acc_no_split[15]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(15, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[15]}">value="${cmd.account_recur.mar_acc_no_split[15]}"</c:if>  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="account_recur.mar_acc_no_split[16]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(16, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[16]}">value="${cmd.account_recur.mar_acc_no_split[16]}"</c:if>  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="account_recur.mar_acc_no_split[17]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(17, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[17]}">value="${cmd.account_recur.mar_acc_no_split[17]}"</c:if>  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="account_recur.mar_acc_no_split[18]" size="1"  maxlength="1" onfocus="select();" onkeyup="dofo(18, 2);" <c:if test="${not empty cmd.account_recur.mar_acc_no_split[18]}">value="${cmd.account_recur.mar_acc_no_split[18]}"</c:if>  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					<input type="text" name="account_recur.mar_acc_no_split[19]" size="1"  maxlength="1" onfocus="select();"  <c:if test="${not empty cmd.account_recur.mar_acc_no_split[19]}">value="${cmd.account_recur.mar_acc_no_split[19]}"</c:if>  <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
					
             </spring:bind>             		
             <font color="#CC3300">*</font>
             </th>           
          </tr>
          
          <tr  > 
            <th width="2%"  height="20">&nbsp;</th>
            <th  height="20" colspan="2"><b> <font face="Verdana" size="1" color="#996633">Atas 
              Nama </font></b></th>
            <th width="24%"  height="20">
            	<spring:bind path="cmd.account_recur.mar_holder"> 
              		<input type="text" name="${status.expression}"
						   value="${status.value }"  size="35" maxlength="50" 
				    <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>><font color="#CC3300">*</font>
              </spring:bind>
            </th>
            <th width="15%"  height="20"></th>
            <th  height="20" > 
              </th>
          </tr>
          <tr  > 
            <th width="2%" >&nbsp;</th>
            <th colspan="2"  ><b><font face="Verdana" size="1" color="#996600">Tanggal 
              Debet <br>
              (DD/MM/YYYY) </font></b></th>
            <th width="24%"> <spring:bind path="cmd.pemegang.mste_tgl_recur"> 
             <input type="checkbox" name="tgl_debet" class="noBorder" 
						value="${cmd.account_recur.flag_set_auto}"  size="30" onClick="tgl_debet_onClick();" 
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
			   <spring:bind path="cmd.account_recur.flag_set_auto"> 
              <input type="hidden" name="${status.expression}"
						value="${cmd.account_recur.flag_set_auto}"  size="30" style='background-color :#D4D4D4'readOnly>
              </spring:bind> 
              <c:choose> <c:when test="${cmd.datausulan.mste_flag_cc eq '1' or cmd.datausulan.mste_flag_cc  eq '2' or cmd.datausulan.mste_flag_cc  eq '9' or cmd.datausulan.flag_account eq '3'}"> 
              <script>inputDate('${status.expression}', '${status.value}', false);</script>
              </c:when> <c:otherwise> 
              <script>inputDate('${status.expression}', '${status.value}', true);</script>
              </c:otherwise> </c:choose> </spring:bind> </th>
            <th width="15%"><b> <font face="Verdana" size="1" color="#996633">Tanggal 
              Valid<br>
              (DD/MM/YYYY) </font></b></th>
            <th > <spring:bind path="cmd.account_recur.mar_expired"> <c:choose> 
              <c:when test="${cmd.datausulan.mste_flag_cc eq '1' or cmd.datausulan.mste_flag_cc  eq '2' or cmd.datausulan.mste_flag_cc  eq '9' or cmd.datausulan.flag_account eq '3'}"> 
              <script>inputDate('${status.expression}', '${status.value}', false);</script>
              </c:when> <c:otherwise> 
              <script>inputDate('${status.expression}', '${status.value}', true);</script>
              </c:otherwise> </c:choose> </spring:bind><font color="#CC3300">*</font> </th>
          </tr>
          <tr> 
            <th width="2%" >&nbsp;</th>
            <th colspan="2"><b><font face="Verdana" size="1" color="#996600">Aktif</font></b></th>
            <th colspan="4">
            	<spring:bind path="cmd.account_recur.mar_active"> 
	                <select name="account_recur.mar_active"
					 <c:if test="${ not empty status.errorMessage}">
							style='background-color :#FFE1FD'
						</c:if>>
	                  <option value="1" selected="selected">Aktif</option>
	                  <option value="0">Non Aktif</option>
	                </select>
				</spring:bind>
            </th>
          </tr>
          <tr> 
            <th width="2%" >&nbsp;</th>
            <th colspan="2"  ><b><font face="Verdana" size="1" color="#996600">Nomor Sinar Card (Khusus Produk BM)</font></b></th>
            <th colspan="4">
            	<spring:bind path="cmd.datausulan.mspo_nasabah_acc"> 
					<input type="text" name="${status.expression}" value="${status.value }"  size="25" maxlength="16" 
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>><font color="#CC3300">*</font>
				</spring:bind>
            </th>
          </tr>
       <tr>          
          	<th></th>
          	<th height="20"><b><font face="Verdana" size="1" color="#996633">Auto Debet Premi Pertama
              </font><font face="Verdana" size="1" color="#996600"> </font> </b>
             </th>
             <th>&nbsp;</th>
             <th>
				<select name="account_recur.flag_autodebet_nb">
	              <option <c:if test="${cmd.account_recur.flag_autodebet_nb eq '0'}">SELECTED</c:if> value="0">Tidak</option> 
	           	  <option <c:if test="${cmd.account_recur.flag_autodebet_nb eq '1'}">SELECTED</c:if> value="1">Ya</option> 
				</select> 
			 </th>
                <th colspan ="3"></th>
        </tr>
          <tr>
            <th class="subtitle2" height="20" width="2%"><b><font face="Verdana" size="1" color="#FFFFFF">&nbsp;&nbsp;&nbsp;</font></b></th>
            <th class="subtitle2" colspan="5" height="20"> <b> <font face="Verdana" size="1" color="#FFFFFF">Dana Investasi dialokasikan : <font color="#CC3300">*</font></font></b> 
            </th>
          </tr>
          <tr>
          	<th  height="20" width="2%"><b><font face="Verdana" size="1" color="#FFFFFF">&nbsp;&nbsp;&nbsp;</font></b></th>
	            <th colspan="5" height="20">
	            <b><font face="Verdana" size="1" color="#996600">
	            <spring:bind path="cmd.datausulan.mste_flag_investasi">
					<label for="langsung"> <input type="radio" class=noBorder 
						name="${status.expression}" value="0" onClick="sts('0');"
						<c:if test="${cmd.datausulan.mste_flag_investasi eq 0 or cmd.datausulan.mste_flag_investasi eq null}"> 
									checked</c:if>
						 id="langsung">Langsung setelah dana diterima di rekening yang ditentukan oleh PT. Asuransi Jiwa Sinarmas MSIG dan SPAJ telah diterima di bagian Underwriting. </label>
					<br>
					<label for="tidaklangsung"> <input type="radio" class=noBorder 
						name="${status.expression}" value="1" onClick="sts('1');"
						<c:if test="${cmd.datausulan.mste_flag_investasi eq 1}"> 
									checked</c:if>
						 id="tidaklangsung">Setelah permintaan asuransi disetujui oleh Bagian Underwriting dan Calon Pemegang Polis telah setuju serta membayar ekstra premi (jika ada).</label> </font></b> 
				</spring:bind>
             </th>
          </tr>  
		  <tr  > 
            <th class="subtitle2" height="20" width="2%"><b><font face="Verdana" size="1" color="#FFFFFF">&nbsp;&nbsp;&nbsp;</font></b></th>
         
		              <th class="subtitle2" colspan="5" height="20"> <b> <font face="Verdana" size="1" color="#FFFFFF">Jenis 
              Dana Investasi ( Isi % jenis Dana Investasi dengan kelipatan 20% atau 10 % tergantung produknya, 
              sehingga total 100% ) <spring:bind path="cmd.investasiutama.jmlh_invest"> 
              <input type="hidden" name="${status.expression}"
						value="${status.value }"  size="30">
              </spring:bind> </font></b> </th>
          </tr>    
          <tr  > 
            <th class="subtitle2" width="2%" rowspan="2" ><b> <font face="Verdana" size="1" color="#ffffff">No.</font></b></th>
            <th class="subtitle2"  rowspan="2"  colspan="3"><b> <font face="Verdana" size="1" color="#ffffff">Type 
              of Fund</font></b></th>
            <th class="subtitle2"  colspan="2" > <p align="center"><b> <font face="Verdana" size="1" color="#ffffff">Fund 
                Allocation</font></b> </th>
          </tr>
          <tr  > 
            <th class="subtitle2"  width="15%" > <p align="left"><b> <font face="Verdana" size="1" color="#ffffff">Persen 
                (%) </font></b> </th>
            <th class="subtitle2"  height="12"  > <p align="left"><b> <font size="1" face="Verdana" color="#ffffff">Jumlah</font></b> 
            </th>
          </tr>
          <c:forEach items="${cmd.investasiutama.daftarinvestasi}" var="inv" varStatus="status"> 
          <tr  > 
            <th width="2%" align="right"> <p align="right"><b><font face="Verdana" size="1">${status.count}</font></b></th>
            <th  align="right" colspan="3"> <div align="left"><spring:bind path="cmd.investasiutama.daftarinvestasi[${status.index}].lji_invest1"> 
                <input type="text" name="${status.expression}"
						value="${status.value }"  size="50" style='background-color :#D4D4D4'readOnly>
                </spring:bind> </div></th>
            <th width="15%" align="right"> <div align="left">
           
            <spring:bind path="cmd.investasiutama.daftarinvestasi[${status.index}].mdu_persen1"> 
                <input type="text" name="${status.expression}"
						value="${status.value }"  size="10" maxlength="3" 
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
               
                </spring:bind><font color="#CC3300">*</font> </div></th>
            <th align="right" > <div align="left">
            <spring:bind path="cmd.investasiutama.daftarinvestasi[${status.index}].mdu_jumlah1"> 
                <input type="text" name="${status.expression}"
						value="${status.value }"  size="17" style='background-color :#D4D4D4' readOnly 
				 <c:if test="${ not empty status.errorMessage}">
						style='background-color :#FFE1FD'
					</c:if>>
                 </spring:bind> <font color="#CC3300">*</font></div></th>
          </tr>
          </c:forEach> 
          <tr  > 
            <th width="2%" align="right">&nbsp;</th>
            <th colspan="3" align="right"><b><font face="Verdana" size="1" color="#996600">Total 
              : </font></b></th>
            <th width="15%" align="right" > <div align="left"><spring:bind path="cmd.investasiutama.total_persen"> 
                <input type="text" name="${status.expression}"
						value="${status.value }"  size="10" style='background-color :#D4D4D4' readOnly
				>
                 </spring:bind> 
              </div></th>
            <th width="2%" align="right">&nbsp;</th>
          </tr>
        </table>
          <table border="0" width="100%" cellspacing="1" cellpadding="1"  ID="tablebiaya" class="entry">
            <tr  > 
              
            <th width="5%" class="subtitle2" ><b><font face="Verdana" size="1" color="#ffffff">No.</font><font face="Verdana" size="1" ></font></b></th>
              <th width="35%" class="subtitle2" ><b> <font face="Verdana" size="1"  color="#ffffff">Jenis 
                Biaya</font></b></th>
              <th width="24%" class="subtitle2" ><b> <font face="Verdana" size="1" color="#ffffff">Jumlah</font></b></th>
              <th width="36%" class="subtitle2" ><b> <font face="Verdana" size="1" color="#ffffff">Persen(%)</font></b></th>
            </tr>
			 <c:forEach items="${cmd.investasiutama.daftarbiaya}" var="biaya" varStatus="status"> 
			             <tr  > 
              <th width="5%" ><b><font face="Verdana" size="1" >${status.count}</font></b></th>
              <th width="35%" ><div align="left"><spring:bind path="cmd.investasiutama.daftarbiaya[${status.index}].ljb_biaya"> 
                <input type="text" name="${status.expression}"
						value="${status.value }"  size="50" style='background-color :#D4D4D4'readOnly>
                </spring:bind> </div></th>
              <th width="24%" ><div align="left"><spring:bind path="cmd.investasiutama.daftarbiaya[${status.index}].mbu_jumlah"> 
                <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" style='background-color :#D4D4D4'readOnly>
                </spring:bind> </div></th>
              <th width="36%" ><div align="left"><spring:bind path="cmd.investasiutama.daftarbiaya[${status.index}].mbu_persen"> 
                <input type="text" name="${status.expression}"
						value="${status.value }"  size="10" style='background-color :#D4D4D4'readOnly>
                </spring:bind> </div></th>
            </tr>
			</c:forEach>
          </table>
        <c:if test="${cmd.datausulan.lsbs_id eq 153 and (cmd.datausulan.lsdbs_number eq 6 or cmd.datausulan.lsdbs_number eq 7)}" > 
        	<table border="0" width="100%" cellspacing="1" cellpadding="1"  ID="tabledth" class="entry">
	          	<tr>
		            <th colspan="6"> 
						<p align="left">
						<b><font face="Verdana" size="1" color="#CC3300">Dana Talangan Haji</font></b> 
	            	</th>
		      	</tr>
		      	<tr>
		      		<th colspan="6">
			      		<spring:bind path="cmd.tertanggung.mste_dth">
							<input type="checkbox" class="noborder" value="1" onclick="dth_onclick(this.checked);" 
		            			<c:if test="${status.value eq 1}">checked="checked"</c:if>> 
							<input type="hidden" id="mste_dth" name="${status.expression}" value="${status.value}">Menggunakan Dana Talangan Haji
		            	</spring:bind>
	            	</th>
		      	</tr>  
		      	<tr> 
		        	<th colspan="6" width="60%" class="subtitle" align="center"> 
		            	<input disabled="disabled" name="btnadd2" type="button" id="btnadd2" value="ADD" onClick="addRowDOM1('tabledth', '2')"> &nbsp;&nbsp;&nbsp;&nbsp; 
		            	<input disabled="disabled" name="btn_cancel2" type="button" id="btn_cancel2" value="DELETE" onClick="cancel1('2')">			
		            	<input type="hidden" id="jmldth" name="jmldth" value="0" >
		            </th>
		            <th width="40%"></th>
		        </tr>
		        <tr > 
	            	<th width="5%" class="subtitle2" ><b><font face="Verdana" size="1" color="#ffffff">No.</font><font face="Verdana" size="1" ></font></b></th>
	            	<th width="15%" class="subtitle2" > 
	              		<font size="1">
	              			<b>
	              				<font face="Verdana" color="#FFFFFF">Jumlah Penarikan(Rp.) </font>
	              				<font color="#CC3300">*</font>
	              			</b>
	              		</font>
		        	</th>
	            	<th width="15%" class="subtitle2" > 
	            		<font size="1">
	            			<b>
	            				<font face="Verdana" color="#FFFFFF">Tanggal Penarikan</font>
	            			</b>
	            		</font>
	            		<font color="#CC3300">*</font>
	                </th>
	             	<th width="30%" class="subtitle2"> 
	             		<font size="1">
	             			<b>
	             				<font face="Verdana" color="#FFFFFF">Keterangan </font>
	             			</b>
	             		</font>
	                </th>
	                <th width="1%" class="subtitle2"  >
	                	<font size="1">
	                		<b>
	                			<font face="Verdana" color="#FFFFFF">&nbsp; </font>
	                		</b>
	                	</font>
	                </th>
	                <th></th>
	            </tr>
	            <c:forEach items="${cmd.tertanggung.daftarDth}" var="dth" varStatus="status"> 
		            <tr > 
		            	<th > ${status.count}</th>
		            	<th >
					  		<input type="text" name='dth.jumlah${status.index +1}' value =<fmt:formatNumber type='number' value='${dth.jumlah}'/> size="30" maxlength="60">
		              	</th>
			          	<th > 
				          	<input type="text" name='tgltarik${status.index +1}' value='<fmt:formatDate value="${dth.tgl_penarikan}" pattern="dd"/>'  size="3" maxlength="2">
							/<input type="text" name='blntarik${status.index +1}' value='<fmt:formatDate value="${dth.tgl_penarikan}" pattern="MM"/>'  size="3" maxlength="2">
							/<input type="text" name='thntarik${status.index +1}' value='<fmt:formatDate value="${dth.tgl_penarikan}" pattern="yyyy"/>'  size="5" maxlength="4">
							<input type="hidden" name='tgl_penarikan${status.index +1}' value='<fmt:formatDate value="${dth.tgl_penarikan}" pattern="dd/MM/yyyy"/>'  size="5" readOnly>
		              	</th>
		              	<th>
					  		<input type="text" name='dth.keterangan${status.index +1}' value ='${dth.keterangan}' size="50" maxlength="100">
		              	</th>
		                <th > 
		                	<input type=checkbox name="cek1${status.index +1}" id= "ck1${status.index +1}" class="noBorder" >
		                </th>
		  
		            </tr>
				</c:forEach>
          	</table>
        </c:if>
		<table border="0" width="100%" cellspacing="1" cellpadding="1" class="entry">
          <tr> 
            <th>&nbsp;</th>
          </tr>
        </table>
		<table border="0" width="100%" cellspacing="1" cellpadding="1" height="24" class="entry" >
			<tr> 
				<th> 
					<p align="left">
					<b><font face="Verdana" size="1" color="#CC3300">Detail Investasi Khusus Produk PowerSave, StableLink, atau StableSave</font></b> 
	            </th>
			</tr>
		</table>
        <table border="0" width="100%" cellspacing="1" cellpadding="1" height="38" class="entry">
			<tr> 
				<th width="34" class="subtitle2"></th>
				<th width="1475" class="subtitle2">Jangka Waktu</th>
				<th width="1073" class="subtitle2">Bunga (%)</th>
				<th width="201" class="subtitle2">Tanggal Jatuh Tempo<br>(DD/MM/YYYY)</th>
				<th width="185" class="subtitle2">Jumlah Investasi</th>
				<th width="159" class="subtitle2">Jumlah Bunga</th>
				<th width="1276" class="subtitle2">Jenis Roll Over</th>
			</tr>
			<tr> 
				<th width="34">&nbsp;</th>
				<th width="1475">
					<select name="powersave.mps_jangka_inv"
							<c:if test="${not empty status.errorMessage}"> style='background-color :#FFE1FD' </c:if> 
							onchange="setPersentaseRate(this.value);">
                		<c:forEach var="jk" items="${select_jangka_invest}">
                			<option 
								<c:if test="${cmd.powersave.mps_jangka_inv eq jk.ID}"> SELECTED </c:if> value="${jk.ID}">${jk.JANGKAWAKTU}
							</option>
						</c:forEach> 
              		</select>
              		<font color="#CC3300">*</font>
              	</th>
				<th width="1073">
					<spring:bind path="cmd.powersave.mps_rate"> 
						<input type="text" name="${status.expression}" value="${status.value }"  size="10" maxlength="7"
							<c:if test="${ not empty status.errorMessage}">style='background-color :#FFE1FD'</c:if>>
					</spring:bind>
					<br/>
					<spring:bind path="cmd.pemegang.mspo_spaj_bundle">
					  SPAJ Paket(Power Save Dan Unit LINK)
						<input type="text" name="${status.expression}" value="${status.value }"  size="20" >
					</spring:bind>
					
				</th>
				<th width="201">
					<spring:bind path="cmd.powersave.mps_batas_date"> 
						<script>inputDate('${status.expression}', '${status.value}', true);</script>
					</spring:bind>
				</th>
				<th width="185">
					<spring:bind path="cmd.powersave.mps_prm_deposit"> 
						<input type="text" name="${status.expression}" value="${status.value }"  size="15" style='background-color :#D4D4D4'readOnly>
					</spring:bind>
					
				</th>
				<th width="159">
					<spring:bind path="cmd.powersave.mps_prm_interest"> 
						<input type="text" name="${status.expression}" value="${status.value }"  size="15" style='background-color :#D4D4D4'readOnly>
					</spring:bind>
				</th>
				<th width="1276">
					<select name="powersave.mps_roll_over" onchange="tutup();"
			 				<c:if test="${ not empty status.errorMessage}"> style='background-color :#FFE1FD' </c:if>>
						 <c:forEach var="roll" items="${select_rollover}"> 
							<option <c:if test="${cmd.powersave.mps_roll_over eq roll.ID}"> SELECTED </c:if> value="${roll.ID}">${roll.ROLLOVER}
							</option>
						</c:forEach> 
					</select>
					<font color="#CC3300">*</font> 
				</th>
			</tr>
			<tr> 
				<th class="subtitle2">&nbsp;</th>
				<th class="subtitle2">Jenis Bunga</th>
				<th colspan="2" class="subtitle2">No Memo</th>
				<th class="subtitle2" colspan="2">Fee Based Income (%)</th>
				<th class="subtitle2">Status Karyawan</th>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<th>
					<select name="powersave.mps_jenis_plan" onChange="jns_rate();"
						<c:if test="${ not empty status.errorMessage}"> style='background-color :#FFE1FD' </c:if>>
						<c:forEach var="bg" items="${select_jenisbunga}">
						<option <c:if test="${cmd.powersave.mps_jenis_plan eq bg.ID}"> SELECTED </c:if>
							value="${bg.ID}">${bg.JENIS}</option>
						</c:forEach> 
					</select>
					<font color="#CC3300">*</font></th>
				<th colspan="2">
					<spring:bind path="cmd.powersave.mpr_note"> 
						<input type="text" name="${status.expression}" value="${status.value}" size="30" maxlength="100"
							<c:if test="${ not empty status.errorMessage}"> style='background-color :#FFE1FD' </c:if>>
              		</spring:bind> 
              		<spring:bind path="cmd.powersave.mpr_nett_tax"> 
						<input type="hidden" name="${status.expression}" value="${status.value}" size="30" maxlength="100">
             		</spring:bind>
             		<font color="#CC3300">*</font>
				</th>
				<th colspan="2">
					<spring:bind path="cmd.powersave.fee_based_income"> 
              			<input type="text" name="${status.expression}" value="${status.value }"  size="10" maxlength="7" style='visibility: hidden; background-color :#D4D4D4'readOnly
							<c:if test="${ not empty status.errorMessage}">
								style='background-color :#FFE1FD'
							</c:if>>
					</spring:bind> 				
				</th>
				<th>
					<select name="powersave.mps_employee" 
						<c:if test="${ not empty status.errorMessage}"> style='background-color :#FFE1FD' </c:if>>
						<c:forEach var="kr" items="${select_karyawan}">
							<option  <c:if test="${cmd.powersave.mps_employee eq kr.ID}"> SELECTED </c:if>
							value="${kr.ID}">${kr.KRY}</option> 
						</c:forEach> 
					</select>
					<font color="#CC3300">*</font>
				</th>
			</tr>
			<tr> 
				<th class="subtitle2">&nbsp;</th>
				<th class="subtitle2">Tanggal NAB</th>
				<th colspan="2" class="subtitle2">Nilai NAB</th>
						<th class="subtitle2" colspan="2">Begdate Top-Up</th>
				<th class="subtitle2">Persentase Rate BP (%)</th>
			</tr>
			<tr>
				<th></th>
				<th>
					<spring:bind path="cmd.powersave.msl_tgl_nab">
						<input type="text" name="${status.expression}" value="${status.value }"  size="30" maxlength="100" readOnly >
					</spring:bind>
				</th>
				<th colspan="2">
					<spring:bind path="cmd.powersave.msl_nab"> 
						<input type="text" name="${status.expression}" value="${status.value }"  size="30" maxlength="50" readOnly>
					</spring:bind>
				</th>
				<th colspan="2">
					<spring:bind path="cmd.powersave.begdate_topup">  
						<script>inputDate('${status.expression}', '${status.value}', false,'');</script>
              			<font color="#CC3300">*</font>
              		</spring:bind>
              	</th>
				<th>
					<spring:bind path="cmd.powersave.msl_bp_rate"> 
						<input type="text" name="${status.expression}" readonly="true" value="${status.value }"  size="10" maxlength="30" >%
					</spring:bind>
					<font color="#CC3300"> *</font>
				</th>
			</tr>   
			<tr> 
				<th class="subtitle2">&nbsp;</th>
				<th class="subtitle2">Penarikan Bunga</th>
				<th colspan="2" class="subtitle2">Breakable</th>
				<th class="subtitle2" colspan="2">Cara Bayar Rider</th>
				<th class="subtitle2">TELEMARKETING</th>
			</tr>
			<tr>
				<th></th>
				<th>
					<select name="powersave.tarik_bunga">
	                    <option <c:if test="${cmd.powersave.tarik_bunga eq 0}"> SELECTED </c:if>	value="0">0</option> 
	           	        <option <c:if test="${cmd.powersave.tarik_bunga eq 1}"> SELECTED </c:if>	value="1">1</option> 
	           	        <option <c:if test="${cmd.powersave.tarik_bunga eq 3}"> SELECTED </c:if>	value="3">3</option> 
					</select> bulan
				</th>
				<th colspan="2">
					<spring:bind path="cmd.powersave.mpr_breakable">
						<input type="checkbox" class="noborder" value="1" onclick="brekele(this.checked);"
            				<c:if test="${status.value eq 1}">checked="checked"</c:if>> 
						<input type="hidden" id="brek" name="${status.expression}" value="${status.value}"> Breakable
            		</spring:bind>
				</th>
              	<th colspan="2">
		          	<input class="noBorder" type="radio" <c:if test="${cmd.powersave.mpr_cara_bayar_rider eq 0}">checked</c:if> name="powersave.mpr_cara_bayar_rider" value="0" onclick="cek_cb_rider(this.value);"
		          	<c:if test="${cmd.powersave.mpr_cara_bayar_rider eq 2 or cmd.powersave.mpr_cara_bayar_rider eq 3}">disabled="disabled" </c:if>>Bayar Di Depan/Langsung<br>
		           	<input class="noBorder" type="radio" <c:if test="${cmd.powersave.mpr_cara_bayar_rider eq 1}">checked</c:if> name="powersave.mpr_cara_bayar_rider" value="1" onclick="cek_cb_rider(this.value);"
		           	<c:if test="${cmd.powersave.mpr_cara_bayar_rider eq 2 or cmd.powersave.mpr_cara_bayar_rider eq 3}">disabled="disabled" </c:if>>Potongan Bunga<br>
<!--		           	<input class="noBorder" type="radio" <c:if test="${cmd.powersave.mpr_cara_bayar_rider eq 2}">checked</c:if> name="powersave.mpr_cara_bayar_rider" value="2" onclick="cek_cb_rider(this.value);"-->
<!--		           	<c:if test="${cmd.powersave.mpr_cara_bayar_rider eq 0 or cmd.powersave.mpr_cara_bayar_rider eq 1}">disabled="disabled" </c:if>>Bayar Di Depan/Langsung Sekaligus<br>-->
<!--		           	<input class="noBorder" type="radio" <c:if test="${cmd.powersave.mpr_cara_bayar_rider eq 3}">checked</c:if> name="powersave.mpr_cara_bayar_rider" value="3" onclick="cek_cb_rider(this.value);"-->
<!--		           	<c:if test="${cmd.powersave.mpr_cara_bayar_rider eq 0 or cmd.powersave.mpr_cara_bayar_rider eq 1}">disabled="disabled" </c:if>>Potongan Bunga Sekaligus<br>-->
		        	<c:if test="${cmd.datausulan.flag_paymode_rider eq 1}">
			        	<div id="cb_rider">
			        		<spring:bind path="cmd.datausulan.lscb_id_rider">
				        		<select name="${status.expression}">
<!--				        			<option value="0" <c:if test="${status.value eq 0}"> selected="selected"</c:if>>SEKALIGUS</option>-->
				        			<option value="1" <c:if test="${status.value eq 1}"> selected="selected"</c:if>>TRIWULANAN</option>
				        			<option value="2" <c:if test="${status.value eq 2}"> selected="selected"</c:if>>SEMESTERAN</option>
				        			<option value="3" <c:if test="${status.value eq 3}"> selected="selected"</c:if>>TAHUNAN</option>
				        			<option value="6" <c:if test="${status.value eq 6}"> selected="selected"</c:if>>BULANAN</option>
				        		</select>
				        	</spring:bind>
			        	</div>
		        	</c:if>
		        </th>
				<th>
					<spring:bind path="cmd.tertanggung.mste_flag_ref_telemarket">
						<input type="checkbox" class="noborder" value="1" onclick="telemarket(this.checked);" 
            				<c:if test="${status.value eq 1}">checked="checked"</c:if>> 
						<input type="hidden" id="tele" name="${status.expression}" value="${status.value}"> Via Telemarketing
            		</spring:bind>
				</th>
			</tr>               
		</table>
		<table border="0" width="100%" cellspacing="1" cellpadding="1" height="24" class="entry" >
			<tr> 
				<th width="100%"> 
					<b><font face="Verdana" size="1" color="#CC3300">Power Save Manfaat Bulanan</font></b> 
	            </th>
			</tr>
		</table>
		
        <table border="0" width="100%" cellspacing="1" cellpadding="1" height="24" class="entry">
          <tr> 
            <th width="46%"> 
              <p align="left"><b> <font face="Verdana" size="1" color="#CC3300">Detail 
                Khusus Bank Sinarmas</font></b> 
            </th>
            <th width="54%"><b><font face="Verdana" size="1" color="#CC3300">Detail 
              Bonus Tahapan khusus Produk Simponi 8 dan Premium Saver </font></b></th>
          </tr>
        </table>
        <table border="0" width="100%" cellspacing="1" cellpadding="1" height="38" class="entry">
          <tr  > 
            <th width="34"  class="subtitle2">&nbsp;</th>
            <th width="528" class="subtitle2"><b><font face="Verdana" size="1" color="#FFFFFF"> 
              % Bunga 
		  
			  </font></b></th>
            <th width="666" class="subtitle2"><b><font face="Verdana" size="1" color="#FFFFFF">% 
              Bonus Tahapan 
              </font></b></th>
          </tr>
          <tr  > 
            <th width="34">&nbsp;</th>
            <th height="22" width="528"> 
              <spring:bind path="cmd.pemegang.mspo_under_table"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" maxlength="5"  >
              </spring:bind> <spring:bind path="cmd.pemegang.tgl_mspo_under_table"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="15" maxlength="5"  >
               </spring:bind><font color="#CC3300">*</font> </th>
            <th height="22" width="666"> 
               <spring:bind path="cmd.pemegang.bonus_tahapan"> 
              <input type="text" name="${status.expression}"
						value="${status.value }"  size="30" maxlength="5" >
              </spring:bind> 
            </th>
          </tr>
          <tr>
          	<th width="666" class="subtitle2" colspan="3"><b><font face="Verdana" size="1" color="#FFFFFF">
              Nasabah MAU menandatangani SPH
              </font></b></th>
          </tr>
          <tr>
          	<th colspan="3">
				<spring:bind path="cmd.datausulan.mspo_flag_sph">
					<input type="checkbox" class="noborder" value="1" onclick="brekele2(this.checked);"
            			<c:if test="${status.value eq 1}">checked="checked"</c:if>> 
					<input type="hidden" id="brek2" name="${status.expression}" value="${status.value}"> Mau
            	</spring:bind>
			</th>
          </tr>
        </table>

		<br>				
		<table border="0" width="100%" cellspacing="1" cellpadding="1" height="27"  class="entry">
			<tr> 
				<th class="subtitle"  width="101%"  height="27"> 
					<p align="center"><b> <font color="#FFFFFF" size="2" face="Verdana">D. 
					DATA YANG DITUNJUK MENERIMA MANFAAT ASURANSI </font></b> 
            	</th>
			</tr>
			<tr> 
				<th class="subtitle" align="center"  > 
					<input name="btnadd1" type="button" id="btnadd1" value="ADD" onClick="addRowDOM1('tableProd', '1')"> &nbsp;&nbsp;&nbsp;&nbsp; 
					<input name="btn_cancel1" type="button" id="btn_cancel1" value="DELETE" onClick="cancel1('1')">			
					<input type="hidden" name="jmlpenerima" id="jmlpenerima" value="0" >
					<input type="hidden" name="jmlbaris" id="jmlbaris"  >
            	</th>
			</tr>
		</table>
		<table id="tableProd" width="100%" border="0" cellspacing="1" cellpadding="1" class="entry">
            <tr> 
				<th class="subtitle2"> <font size="1"><b><font face="Verdana" color="#FFFFFF">No</font></b></font></th>
				<th class="subtitle2"> <font size="1"><b><font face="Verdana" color="#FFFFFF">Nama Lengkap<br>(Sesuai KTP/Akte Lahir)</font></b></font>
					<font color="#CC3300">*</font>
				</th>
				<th class="subtitle2"> <font size="1"><b><font face="Verdana" color="#FFFFFF">Tanggal Lahir</font>
					<font color="#CC3300">*</font></b></font><font size="1"><b><font face="Verdana" color="#FFFFFF"></font></b></font>
				</th>
				<th class="subtitle2"> <font size="1"><b><font face="Verdana" color="#FFFFFF">Jenis Kelamin</font></b></font>
					<font color="#CC3300">*</font></th>
				<th class="subtitle2"> <font size="1"><b><font face="Verdana" color="#FFFFFF">Hubungan Dengan Calon Tertanggung</font></b></font>
					<font color="#CC3300">*</font>
				</th>
				<th class="subtitle2"> <font size="1"><b><font face="Verdana" color="#FFFFFF">Manfaat (%)</font></b></font>
					<font color="#CC3300">*</font>
				</th>
				<th class="subtitle2"> <font size="1"><b><font face="Verdana" color="#FFFFFF">Kewarganegaraan</font></b></font>
					<font color="#CC3300">*</font>
				</th>
				<th class="subtitle2"> <font size="1"><b><font face="Verdana" color="#FFFFFF">Instruksi Khusus</font></b>
					</font><font color="#CC3300">*</font>
				</th>
                <th class="subtitle2"> <font size="1"><b><font face="Verdana" color="#FFFFFF">&nbsp; </font></b></font></th>
  				<th></th>
            </tr>
			<c:forEach items="${cmd.datausulan.daftabenef}" var="benef" varStatus="status"> 
				<tr> 
					<th>${status.count}</th>
					<th>
						<input type="text" id='benef.msaw_first${status.index +1}' name='benef.msaw_first${status.index +1}' value="${benef.msaw_first}" size="30" maxlength="60">
					</th>
				<th> 
					<input type="text" id='tgllhr${status.index +1}' name='tgllhr${status.index +1}' value='<fmt:formatDate value="${benef.msaw_birth}" pattern="dd"/>'  size="3" maxlength="2">
					/<input type="text" id='blnhr${status.index +1}' name='blnhr${status.index +1}' value='<fmt:formatDate value="${benef.msaw_birth}" pattern="MM"/>'  size="3" maxlength="2">
					/<input type="text" id='thnhr${status.index +1}' name='thnhr${status.index +1}' value='<fmt:formatDate value="${benef.msaw_birth}" pattern="yyyy"/>'  size="5" maxlength="4">
					<input type="hidden" id='msaw_birth${status.index +1}' name='msaw_birth${status.index +1}' value='<fmt:formatDate value="${benef.msaw_birth}" pattern="dd/MM/yyyy"/>'  size="5" readOnly>
				</th>
				<th>
					<select id='benef.msaw_sex${status.index +1}' name='benef.msaw_sex${status.index +1}'>
	                    <option <c:if test="${benef.msaw_sex eq 0}"> SELECTED </c:if> value="0">Perempuan</option> 
	           	        <option <c:if test="${benef.msaw_sex eq 1}"> SELECTED </c:if> value="1">Laki-Laki</option> 
					</select> 
				</th>
				<th> 
			  		<select id='benef.lsre_id${status.index +1}' name='benef.lsre_id${status.index +1}'>
						<c:forEach var="benefrl" items="${select_hubungan}">
							<option <c:if test="${benef.lsre_id eq benefrl.ID}"> SELECTED </c:if>
								value="${benefrl.ID}">${benefrl.RELATION}</option>
						</c:forEach>
					</select>
				</th>
				<th>
					<input type="text" id='benef.msaw_persen${status.index +1}' name='benef.msaw_persen${status.index +1}' value="${benef.msaw_persen}" size="5" maxlength="5">
				</th>
                <th> 
			  		<select id='benef.lsne_id${status.index +1}' name='benef.lsne_id${status.index +1}'>
						<c:forEach var="benelsne" items="${select_warganegara}">
							<option <c:if test="${benef.lsne_id eq benelsne.ID}"> SELECTED </c:if>
								value="${benelsne.ID}">${benelsne.NEGARA}</option>
						</c:forEach>
					</select>
				</th> 
				<th>
			  		<input type="text" id='benef.msaw_ket${status.index +1}' name='benef.msaw_ket${status.index +1}' value="${benef.msaw_ket}" size="60" maxlength="200">
				</th>
                <th>
                	<input type=checkbox id='cek${status.index +1}' name='cek${status.index +1}' class="noBorder" >
                </th>
			</tr>
		</c:forEach>		 
	</table>
        
        <table border="0" width="100%" cellspacing="1" cellpadding="1" class="entry2">
          <tr> 
            <td colspan="9"> 
			 <p align="left"><b> <font face="Verdana" size="2" color="#CC3300">Note : Untuk pecahan decimal menggunakan titik.
			 <br>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; * Wajib diisi</font></b></p>
			</td>
          </tr>
          <c:if test="${cmd.tertanggung.lspd_id eq 2 or cmd.tertanggung.lspd_id eq 27 or cmd.tertanggung.lspd_id eq 209}">
	          <c:if test="${not empty cmd.pemegang.reg_spaj}">
		          <tr>
		          	 <td colspan="2">
		          	 	<input type="checkbox" class="noBorder" name="edit_investasi" id="edit_investasi" value="${cmd.investasiutama.edit_investasi}" <c:if test="${cmd.investasiutama.edit_investasi eq 1}"> 
							   checked </c:if> size="30" onClick="edit_onClick();"> Telah dilakukan edit
						<spring:bind path="cmd.investasiutama.edit_investasi"> 
				          		<input type="hidden" name="${status.expression}" value="${cmd.investasiutama.edit_investasi}"  size="30" style='background-color :#D4D4D4'readOnly>
						</spring:bind>	
					 </td>
					 <td colspan="7">
					 	<spring:bind path="cmd.investasiutama.kriteria_kesalahan">
								<select name="${status.expression}">
									<c:forEach var="a" items="${select_kriteria_kesalahan}">
										<option
											<c:if test="${cmd.investasiutama.kriteria_kesalahan eq a.KESALAHAN}"> SELECTED </c:if>
											value="${a.KESALAHAN}">${a.KESALAHAN}
										</option>
									</c:forEach>
								</select>
							</spring:bind>
					</td> 
		          </tr>
		          <tr>
		          	<td colspan="9">&nbsp;</td>
		          </tr>
	          </c:if>
          </c:if>		
          <tr> 
            <td colspan="9"> <input type="hidden" name="hal" value="${halaman}">	
			<spring:bind path="cmd.pemegang.indeks_halaman"> 
              <input type="hidden" name="${status.expression}"
						value="${halaman-1}"  size="30" >
              </spring:bind>
			
			</td>
          </tr>
          <tr> 
            <td  align="center" colspan="4"> 
              <div align="center">
                <input type="submit" name="_target${halaman-1}" value="Prev &laquo;" onClick="prev()" 
				accesskey="p" onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
                <input type="submit" name="_target${halaman+1}" value="Next &raquo;" onClick="next()" 
				accesskey="N" onmouseover="return overlib('Alt-N', AUTOSTATUS, WRAP);" onmouseout="nd();">
                <input type="hidden" name="_page" value="${halaman}">
              </div>
            </td>
          </tr>
        </table>
      <p>
    </td>
  </tr>
</table>
 </form>
</body>
<%@ include file="/include/page/footer.jsp"%>

