<%@ include file="/include/page/header.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	hideLoadingMessage();

	function trans(){
		spaj = document.frmParam.spaj.value;
		if(spaj==''){
			alert('Harap cari SPAJ terlebih dahulu');
		}else{
			//if (document.frmParam.jn_bank.value == 2 || document.frmParam.jn_bank.value == 3){
			//if (document.frmParam.jn_bank.value == 3){
			//	document.getElementById('infoFrame').src='${path}/bac/transferbactouwbanksinarmas.htm?spaj='+document.frmParam.spaj.value; 
			//}else{
			document.getElementById('infoFrame').src='${path}/bac/transferbactouw.htm?spaj='+document.frmParam.spaj.value; 			
			//}
		}
	}

	function awal(){
		//cariregion(document.frmParam.spaj.value,'region');
		setFrameSize('infoFrame', 64);
	}

	function buttonLinks(str){
		if(str=='cari'){
			spaj = document.frmParam.spaj.value;
			if(spaj==''){
				alert('Harap cari SPAJ terlebih dahulu');
			}else{
				document.getElementById('infoFrame').src='${path}/bac/editsimple.htm?data_baru=true&showSPAJ='+document.frmParam.spaj.value;
			}
		}else{
			spaj = document.frmParam.spaj.value;
			if(spaj==''){
				alert('Harap cari SPAJ terlebih dahulu');
			}else{
			
				switch (str) {
				
					case "info" :
						document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj;
						break;		
					case "uwinfo" :
						document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=uwinfo&spaj='+spaj;
						break;
					case "checklist" :
						document.getElementById('infoFrame').src = '${path}/checklist.htm?lspd_id=1&reg_spaj='+spaj;
						break;
					case "tglTerimaAdmin" :
						//popWin('${path }/uw/viewer.htm?window=editTglTrmKrmAdmin&spaj='+spaj+'&show=0', 200, 350);
						popWin('${path }/uw/viewer.htm?window=editTglTrmKrmSpaj&spaj='+spaj+'&show=4', 150, 350);
						break;
					case "questionare" :
						popWin('${path}/bac/questionareSimple.htm?spaj='+spaj+'&show=4', 400, 700);
						break;
					case "reffbii" :
						if(confirm("Apakah anda akan input referral others BII?\nApabila anda adalah user BANK SINARMAS / ASURANSI SINARMAS / SINARMAS SEKURITAS/BANK SINARMAS SYARIAH, harap pilih CANCEL!")){
							//alert('${path}/bac/reff_bank.htm?window=main&reffothers=y&spaj='+spaj);
							popWin('${path}/bac/reff_bank.htm?window=main&reffothers=y&spaj='+spaj, 400, 700);
						}else{
							//alert('${path}/bac/reff_bank.htm?window=main&reffothers=n&spaj='+spaj);
							popWin('${path}/bac/reff_bank.htm?window=main&reffothers=n&spaj='+spaj, 400, 700);
						}
						break;
				}
				
			}
		}
	}


</script>
<body 
	onload="awal(); setFrameSize('infoFrame', 85);"
	onresize="setFrameSize('infoFrame', 85);" style="height: 100%;">

<form name="frmParam" method="post">
<div class="tabcontent">
<table class="entry2" style="width: 98.4%;">
	<tr>
		<th>SPAJ</th>
		<td>
			<input type="button" value="Refresh" name="refresh" onclick="document.location.reload()" style="background-color: yellow;">
			
			<input type="button" value="Input" name="info" id="info"
				onclick="document.getElementById('infoFrame').src='${path}/bac/editsimple.htm?data_baru=true;'" 
				accesskey="I" onmouseover="return overlib('Input SPAJ Baru', AUTOSTATUS, WRAP);" onmouseout="nd();"
				<c:if test="${sessionScope.currentUser.flag_approve eq 1}"> disabled="true" </c:if>>
			
			<select name="spaj" id="spaj">
				<option value="">[--- Silahkan Pilih/Cari SPAJ ---]</option>
				<c:forEach var="s" items="${daftarSPAJ}">
					<option value="${s.REG_SPAJ }" style="background-color: ${s.OTORISASI_BG};" 
						<c:if test="${s.REG_SPAJ eq param.spaj }">selected<c:set var="ada" value="ada" /></c:if>>${s.SPAJ_FORMATTED}
					- ${s.POLICY_FORMATTED }</option>
				</c:forEach>
			</select>
			
			<input type="button" value="Cari" name="search"
                onclick="popWin('${path}/bac/caribacsimple.htm?posisi=1&win=bac&spajList4EditButton=${spajList4EditButton}', 350, 450);"
                accesskey="C" onmouseover="return overlib('Cari SPAJ', AUTOSTATUS, WRAP);" onmouseout="nd();">
			
			<input type="button" value="Edit" name="info" onclick="return buttonLinks('cari');" 
				accesskey="T" onmouseover="return overlib('Edit SPAJ', AUTOSTATUS, WRAP);" onmouseout="nd();"
				<c:if test="${sessionScope.currentUser.jn_bank eq 2 and sessionScope.currentUser.flag_approve eq 1}"> disabled="true" </c:if> />
				
			<input type="button" value="Info" name="ucup"	onclick="return buttonLinks('info');" 
				 onmouseover="return overlib('Info Lengkap Dokumen Polis', AUTOSTATUS, WRAP);" onmouseout="nd();">
				 
			<input type="button" value="U/W Info" name="uwinfo"
				onclick="return buttonLinks('uwinfo');"
				accesskey="W" onmouseover="return overlib('U/W Info', AUTOSTATUS, WRAP);" onmouseout="nd();">			
				
		</td>
	</tr>
	<tr>
		<th>Proses</th>
		<td >
			<input type="button" value="Questionare" name="questionare"
				onclick="return buttonLinks('questionare'); "
				accesskey="P" onmouseover="return overlib('Questionare', AUTOSTATUS, WRAP);" onmouseout="nd();" />
				
			<input type="button" value="Checklist" name="checklist"	onclick="return buttonLinks('checklist');" 
				 onmouseover="return overlib('Checklist Dokumen Polis', AUTOSTATUS, WRAP);" onmouseout="nd();"
				 <c:if test="${sessionScope.currentUser.jn_bank eq 2 and sessionScope.currentUser.jn_bank eq 16 and sessionScope.currentUser.flag_approve eq 1 and sessionScope.currentUser.lus_id ne 2661}"> disabled="true" </c:if>>
				
			<input type="button" value="Tgl Terima Admin" name="tglAdmin"
				onclick="return buttonLinks('tglTerimaAdmin'); "
				accesskey="P" onmouseover="return overlib('Edit Tanggal Terima Admin', AUTOSTATUS, WRAP);" onmouseout="nd();" />
					
			<c:if test="${sessionScope.currentUser.jn_bank ne 4 and sessionScope.currentUser.lus_id ne 2661}">
				<input type="button" value="Referral" name="reffbii"	onclick="return buttonLinks('reffbii');" 
				accesskey="R" onmouseover="return overlib('Insert Referral', AUTOSTATUS, WRAP);" onmouseout="nd();"
				<c:if test="${sessionScope.currentUser.jn_bank eq 2 and sessionScope.currentUser.flag_approve eq 1}"> disabled="true" </c:if>>
			</c:if>		
					
			<c:if test="${sessionScope.currentUser.lus_id ne 2661}">	
				<input type="button" value="Transfer" name="transfer" onclick="trans();" id="transfer"
				accesskey="F" onmouseover="return overlib('Transfer', AUTOSTATUS, WRAP);" onmouseout="nd();"
				<c:if test="${sessionScope.currentUser.jn_bank eq 3 and sessionScope.currentUser.flag_approve eq 0 and sessionScope.currentUser.lus_id ne 2661}"> disabled="true" </c:if> />
			</c:if>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<c:choose>
				<c:when test="${sessionScope.currentUser.flag_approve eq 1}">
					<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
						width="100%" style="width: 100%;"> Please Wait... </iframe>
				</c:when>
				<c:otherwise>
					<iframe src="${path}/bac/editsimple.htm?data_baru=true" name="infoFrame" id="infoFrame"
						width="100%" style="width: 100%;"> Please Wait... </iframe>
				</c:otherwise>
			</c:choose>
			
			<input type="hidden" value="${spajList}" id="spajListDisabledForTransfer" />
			<input type="hidden" value="${spajList4EditButton}" id="spajList4EditButton" />
			<input type="hidden" value="${jnBank}" id="jnBank" />
		</td>
	</tr>
</table>
</div>
</form>

</body>
<%@ include file="/include/page/footer.jsp"%>