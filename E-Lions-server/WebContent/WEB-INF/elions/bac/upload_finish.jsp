<%@ include file="/include/page/header.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<body onload="setupPanes('container1', 'tab1');">
	
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Upload SPAJ - Finished</a>
			</li>
		</ul>

		<div class="tab-panes">
			<div id="pane1" class="panes">
			
				<div id="success" style="text-transform: none;">
					Upload SPAJ Berhasil <br>
					Silahkan cek kembali data yang anda masukkan
				</div>
				
				<fieldset>
					<legend>Daftar Spaj yang berhasil di-upload</legend>
					<display:table id="spaj" name="cmd.daftarSpaj" class="displaytag" style="width:auto;">
						<display:column title="No." style="text-align: left; white-space:nowrap;">
							${spaj_rowNum}.
						</display:column>
						<display:column property="polis.reg_spaj" title="SPAJ" style="text-align: center; white-space:nowrap;" />
						<display:column property="pemegang.mcl_first" title="Nama" style="text-align: left; white-space:nowrap;" />
						<display:column property="pemegang.mspe_date_birth" title="Tgl Lahir" format="{0, date, dd/MM/yyyy}" style="text-align: center; white-space:nowrap;" />
					</display:table>
				</fieldset>							

				<input type="button" value="&laquo; Back" onclick="if(confirm(''))window.location='${path}/bac/upload.htm';">
				<input type="button" value="Print" onclick="window.print();">
			</div>
		</div>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>