<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript">
	hideLoadingMessage();
	
	function viewReport(no){
		if(no==0){ //Summary Biasa
			window.location = '${path}/report/bac.htm?window=summary_biasa';
		}else if(no==1){ //Summary Recurring
			window.location = '${path}/report/bac.htm?window=summary_recurring';
		}else if(no==2){ //Summary Per Plan
			window.location = '${path}/report/bac.htm?window=summary_per_plan ';
		}else if(no==3){ //Summary Biasa guthrie
			window.location =  '${path}/report/bac.htm?window=summary_biasa_guthrie';
		}
	}
</script>
</head>
<BODY onload="document.title='PopUp :: Print Summary'; setupPanes('container1', 'tab1'); " style="height: 100%;">
	
	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Print Summary</a>
			</li>
		</ul>
	
		<div class="tab-panes">
			<div id="pane1" class="panes">
				<table class="entry2">
					<tr>
						<th>Jenis Report : </th>
						<td>
							<c:forEach var="p" items="${daftar}" varStatus="st">
								<input class="noBorder" type="radio" name="pilih" value="${p.key}" id="pilih${st.count}"
									onclick="viewReport(${p.key});">
								<label for="pilih${st.count}">${p.value}</label><br/>
							</c:forEach>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>