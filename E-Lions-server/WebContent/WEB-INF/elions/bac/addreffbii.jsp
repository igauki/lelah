<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>

<script language="JavaScript">
<!--
	if (!document.layers&&!document.all&&!document.getElementById)
		event="test"
		
	function showtip(current,e,text){
	
		if (document.all||document.getElementById){
			thetitle=text.split('<br>')
			if (thetitle.length>1){
				thetitles=''
				
			for (i=0;i<thetitle.length;i++)
				thetitles+=thetitle[i]
				
			current.title=thetitles
		}
		else
			current.title=text
	}
	
	else if (document.layers){
				document.tooltip.document.write('<layer bgColor="white" style="border:1px solid black;font-size:13px;">'+text+'</layer>')
				document.tooltip.document.close()
				document.tooltip.left=e.pageX+5
				document.tooltip.top=e.pageY+5
				document.tooltip.visibility="show"
			}
	}
	
	function hidetip(){
		if (document.layers)
			document.tooltip.visibility="hidden"
	}

	function body_onload()
	{
		if (document.frmParam.statussubmit.value=="1")
		{
			alert("Data sudah berhasil disimpan");
			document.frmParam.statussubmit.value="0";
		}
	}	
	
	function sub(hasil)
	{
		return confirm(hasil);
	}

// -->
</script>
<body  onLoad="body_onload();">

		
<form name="frmParam" method="post" onReset="return confirm('Reset Form Input?')" >
  <span class="subtitle">Add Referral BII</span> 
  <table width="100%" >
 	<tr>
      <td valign="top">
			<spring:bind path="cmd.statussubmit"> 
              <input type="hidden" name="${status.expression}" 
						value="${status.value}"  size="15" maxlength="15" tabindex="8">
              </spring:bind>			  
          <table class="entry" width="100%">
          <tr> 
            <th width="132">NAMA REFF </th>
            <th width="1091"> <spring:bind path="cmd.lrb_id"> 
              <input type="hidden" name="${status.expression}" value="${status.value}" size="30" maxlength="30" tabindex="1" >
              </spring:bind> 
			  <spring:bind path="cmd.nama_reff"> 
              	<input type="text" name="${status.expression}" value="${status.value}" size="30" maxlength="30" tabindex="1" >
              </spring:bind></th>
          </tr>
          <tr> 
            <th>CABANG BII</th>
            <th>
			<spring:bind path="cmd.lcb_no"> 
              <select name="${status.expression}">
                <c:forEach var="cabang" items="${daftarcabangbii}"> <option value="${cabang.LCB_NO}" 
								<c:if test="${status.value eq cabang.LCB_NO}"> selected </c:if>>${cabang.NAMA_CABANG}</option> 
                </c:forEach> 
              </select>
              </spring:bind></th>
          </tr>
          <tr> 
            <th>NO A/C</th>
            <th><spring:bind path="cmd.no_rek"> 
              <input type="text" name="${status.expression}" value="${status.value}"  size="30" maxlength="30" tabindex="3" >
              </spring:bind></th>
          </tr>
          <tr> 
            <th>CABANG A/C</th>
            <th><spring:bind path="cmd.cab_rek"> 
              <input type="text" name="${status.expression}" value="${status.value}" size="30" maxlength="30" tabindex="4" >
              </spring:bind></th>
          </tr>
          <tr> 
            <th>ATAS NAMA</th>
            <th><spring:bind path="cmd.atas_nama"> 
              <input type="text" name="${status.expression}" value="${status.value}"  size="30" maxlength="30" tabindex="5">
              </spring:bind></th>
          </tr>
          <tr> 
            <th>AKTIF</th>
            <th><spring:bind path="cmd.flag_aktif"> 
              <select name="${status.expression}"><option value="1"  
                <c:if test="${status.value eq 1}">selected</c:if> >AKTIF</option> 
				<option value="0" <c:if test="${status.value eq 0}">selected</c:if> >TIDAK AKTIF</option> 
              </select>
              </spring:bind></th>
          </tr>
          <tr> 
            <th>NPK</th>
            <th><spring:bind path="cmd.npk"> 
              <input type="text" name="${status.expression}" value="${status.value}"  size="30" maxlength="15" tabindex="8">
              </spring:bind> <br>
			</th>
          </tr>
        </table>

    
        <div id="buttons" style="margin: 5px 0px 5px 0px;"> 
          <input type="submit" value="Save" name="save"  onclick="sub(this.value+'?');">
          <input type="button" name="cancel" value="Cancel" onclick="window.close()";>
		  <input class="button" type="button" name="tutup" value="Close" onclick="window.close();">
          <spring:bind path="cmd.*"> <c:if test="${not empty status.errorMessages}"> 
          <div id="error"> ERROR:<br>
            <c:forEach var="error" items="${status.errorMessages}"> - <c:out value="${error}" escapeXml="false" /> 
            <br />
            </c:forEach></div>
          </c:if> </spring:bind> </div>
      </td>
      </tr>
  
  </table>
</form>
</body>

<%@ include file="/include/page/footer.jsp"%>
