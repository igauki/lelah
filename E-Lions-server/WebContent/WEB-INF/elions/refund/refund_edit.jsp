<%--
/**********************************************************************
 * Program History
 *
 * Project Name      	: E-Lions
 * Function Id         	:
 * Program Name   		: refund_edit
 * Description         	:
 * Environment      	: Java  1.5.0_06
 * Author               : samuel
 * Version              : 1.0
 * Creation Date    	: Oct 13, 2008 4:58:36 PM
 *
 * Update history   Re-fix date      Person in charge      Description
 *
 *
 *
 * Copyright(C) 2008-Asuransi Jiwa Sinarmas. All Rights Reserved.
 ***********************************************************************/
--%>

<%@ include file="/include/page/header.jsp"%>

<script type="text/javascript" src="${path }/include/js/refund/refund_common.js"></script>
<script type="text/javascript" src="${path }/include/js/overlib.js"></script>
<style type="text/css">
    html, body {
    }

    }

</style>

<script type="text/javascript">

	function showtip2(current,e,text){
	  if (document.all&&document.readyState=="complete"){
	  	document.getElementById('keterangan').style.color = 'red'
	  	document.getElementById('keterangan').style.fontSize = '12'
	    document.getElementById('keterangan').style.fontWeight = 'bold'
	    document.all.tooltip2.innerHTML=text
	    document.all.tooltip2.style.pixelLeft=event.clientX+document.body.scrollLeft+10
	    document.all.tooltip2.style.pixelTop=event.clientY+document.body.scrollTop+10
	    document.all.tooltip2.style.visibility="visible"
		}
	  else if (document.layers){
	    document.tooltip2.document.nstip.document.write('<b>'+text+'</b>')
	    document.tooltip2.document.nstip.document.close()
	    document.tooltip2.document.nstip.left=0
	    currentscroll=setInterval("scrolltip()",100)
	    document.tooltip2.left=e.pageX+10
	    document.tooltip2.top=e.pageY+10
	    document.tooltip2.visibility="show"
		}
	}
	function hidetip2(){
		document.getElementById('keterangan').style.color = 'blue'
		document.getElementById('keterangan').style.fontSize = '10'
	    document.getElementById('keterangan').style.fontWeight = 'normal'
	  	if (document.all)
	    document.all.tooltip2.style.visibility="hidden"
	    else if (document.layers){
	    clearInterval(currentscroll)
	    document.tooltip2.visibility="hidden"
		}
	}

    function triggerEvent( theEvent )
    {
        document.forms[ 0 ].theEvent.value = theEvent;
        checkEvent();
    }

    function checkEvent()
    {
        var theEvent = document.forms[ 0 ].theEvent.value;
        var tindakanGantiPlan = '4';
        var tindakanCd = document.getElementById('editForm.tindakanCd').value;
        if( theEvent == 'onPressButtonNext' )
        {
            if( tindakanCd == tindakanRefundPremi && document.getElementById('statusUnitId').value == 'sudah' && document.getElementById('posisiNo').value < 2)
            {
                submitForm( PREVIEW_EDIT_JSP );
            }
            else if( tindakanCd == tindakanRefundPremi && document.getElementById('statusUnitId').value == 'belum' && document.getElementById('sumMtuUnit').value == 0 )
            {
            	if( document.getElementById('posisiNo').value < 2 )
            	{
	            	submitForm( PREVIEW_EDIT_SETORAN );
            	}
            	else
            	{
            		submitForm( PREVIEW_LAMP_1_JSP );
            	}
            }
            else if( tindakanCd == tindakanRefundPremi && document.getElementById('statusUnitId').value == 'belum' || tindakanCd == tindakanRefundPremi && document.getElementById('statusUnitId').value == '' || tindakanCd == tindakanRefundPremi && document.getElementById('posisiNo').value > 0)
            {
                submitForm( PREVIEW_LAMP_1_JSP );
            }
            else if( tindakanCd == tindakanGantiTertanggung )
            {
                submitForm( PREVIEW_LAMP_3_JSP );
            }
            else if( tindakanCd == tindakanGantiPlan )
            {
                submitForm( PREVIEW_LAMP_3_JSP );
            }
            else
            {
                submitForm( REFUND_EDIT );
            }
        }
        else if( theEvent == 'onBlurSpaj' )
        {
          //  if( tindakanCd == tindakanRefundPremi || tindakanCd == tindakanGantiPlan ||  tindakanCd == tindakanGantiTertanggung )
           // {
                submitForm( REFUND_EDIT );
            //}
        }
        else if(theEvent == 'onPressButtonAddLampiran')
        {
        	if(document.getElementById('addLampiran').value != '' )
        	{
				submitForm( REFUND_EDIT );
        	}
        }
        else if( theEvent == 'onBlurNewSpaj' )
        {
           if( tindakanCd == tindakanGantiPlan ||  tindakanCd == tindakanGantiTertanggung )
            {
                submitForm( REFUND_EDIT );
            }
        }
        else if( theEvent == 'onChangeTindakanCd' || theEvent == 'onPressButtonSendPhysicalDoc')
        {
            submitForm( REFUND_EDIT );
        }
        else if( theEvent == 'onChangeHasUnitFlag' )
        {
            submitForm( REFUND_EDIT );
        }
        else if( theEvent == 'onPressButtonPreviewSuratBatal' )
        {
            submitForm( REFUND_EDIT );
        }
        else if( theEvent == 'onPressButtonBatalkanSpaj' )
        {
            var isOk = window.confirm( "Anda yakin untuk membatalkan SPAJ?" )
            if( isOk )submitForm( REFUND_EDIT );
        }
        else if( theEvent == 'onPressButtonSaveDraft' )
        {
            var isOk = window.confirm( "Anda yakin untuk save draft?" )
            if( isOk )submitForm( REFUND_EDIT );
        }
        else if( theEvent == 'onPressButtonBack' )
        {
            submitForm( REFUND_LOOK_UP );
        }
        else if( theEvent == 'onChangeMerchantCd' )
        {
            submitForm( REFUND_EDIT );
        }
        
    }
    
    function deleteLampiran(index)
    {
    	document.getElementById('indexDelete').value = index;
    	submitForm( REFUND_EDIT );
    }

    function updateAlasan()
    {
        var alasanCd = document.getElementById('editForm.alasanCd').value;
        triggerEvent("onChangeAlasanCd");
        if( alasanCd == alasanLain2 )
        {
            document.getElementById('editForm.alasan').value = '';
            document.getElementById('editForm.alasanDisplayId').style.display = '';
            document.getElementById('editForm.alasan').focus();
        }
        else
        {
            document.getElementById('editForm.alasan').value = showLabelOfDropDown('editForm.alasanCd');
            document.getElementById('editForm.alasanDisplayId').style.display = 'none';
        }
        submitForm( REFUND_EDIT );
    }
    
    /*function checkBoxBasedIndex( index )
    {
    	if(document.getElementById('checkBox'+index).checked == true)
    	{
    		document.getElementById('checkBox'+index).checked = false;
    	}
    	else if(document.getElementById('checkBox'+index).checked == false )
    	{
    		document.getElementById('checkBox'+index).checked = true;
    	}
    }
    */
    
</script>

<body style="height: 100%;" onload="setupPanes('container1', 'tab1'); ">

<div class="tab-container" id="container1">

<ul class="tabs">
    <li>
        <a href="#" onClick="return showPane('pane1', this)" id="tab1">Pembatalan Polis ${pageSubTitle}</a>
    </li>
</ul>

<div class="tab-panes">
<div id="pane1" class="panes" style="">
<form:form id="formpost" commandName="cmd" name="">

                    <input type="hidden" name="theEvent" id="theEvent"/>
                    <input type="hidden" name="indexDelete" id="indexDelete"/>
					<form:hidden path="editForm.downloadFlag" id="downloadFlag"/>
					
                    <form:hidden path="editForm.alasanDisplay"/>
                    <form:hidden path="editForm.spajBaruDisplay"/>

                    <form:hidden path="editForm.biayaAdminDisplay"/>
                    <form:hidden path="editForm.biayaMedisDisplay"/>
                    <form:hidden path="editForm.biayaLainDisplay"/>

                    <form:hidden path="editForm.atasNamaDisplay"/>
                    <form:hidden path="editForm.norekDisplay"/>
                    <form:hidden path="editForm.namaBankDisplay"/>
                    <form:hidden path="editForm.cabangBankDisplay"/>
                    <form:hidden path="editForm.kotaBankDisplay"/>
                    <form:hidden path="editForm.mtuUnit" id="sumMtuUnit"/>

                    <form:hidden path="editForm.posisiNo" id="posisiNo"/>

                    <table style="width: 100%;">
                        <tr><td colspan="2"><input type="text" id="textMsg" name="textMsg" style="font-weight: bold; border: thin; width: 15cm; background-color: #F7F7F7;" disabled="disabled" /></td></tr>
                        <tr id="editForm.totalPremiDikembalikanDisplayId" style="display: ${cmd.editForm.totalPremiDikembalikanDisplay}">
                            <td style="text-align: left; width: 150px;">
                                No. Reg SPAJ / No Polis
                            </td>
                            <td style="text-align: left; width: 400px;">
                                : ${cmd.editForm.policyInfoVO.spajNo} / ${cmd.editForm.policyInfoVO.policyNo}
                            </td>
                            <td style="width: 100%;"></td>
                        </tr>
                        <tr id="editForm.totalPremiDikembalikanDisplayId" style="display: ${cmd.editForm.totalPremiDikembalikanDisplay}">
                            <td style="text-align: left;">
                                Nama Pemegang Polis
                            </td>
                            <td style="text-align: left;">
                                : ${cmd.editForm.policyInfoVO.namaPp}
                            </td>
                            <td></td>
                        </tr>
                        <tr id="editForm.totalPremiDikembalikanDisplayId" style="display: ${cmd.editForm.totalPremiDikembalikanDisplay}">
                            <td style="text-align: left;">
                                Nama Tertanggung
                            </td>
                            <td style="text-align: left;">
                                : ${cmd.editForm.policyInfoVO.namaTt}
                            </td>
                            <td></td>
                        </tr>
                        <tr id="editForm.totalPremiDikembalikanDisplayId" style="display: ${cmd.editForm.totalPremiDikembalikanDisplay}">
                            <td style="text-align: left;">
                                Produk
                            </td>
                            <td style="text-align: left;">
                                : ${cmd.editForm.policyInfoVO.namaProduk}
                            </td>
                            <td></td>
                        </tr>
                        <tr id="editForm.totalPremiDikembalikanDisplayId" style="display: ${cmd.editForm.totalPremiDikembalikanDisplay}">
                            <td style="text-align: left;">
                                Posisi
                            </td>
                            <td style="text-align: left;">
                                : ${cmd.editForm.policyInfoVO.posisi}
                            </td>
                            <td></td>
                        </tr>
                        <tr id="editForm.totalPremiDikembalikanDisplayId" style="display: ${cmd.editForm.totalPremiDikembalikanDisplay}">
                            <td style="text-align: left;">
                                Next Follow Up
                            </td>
                            <td style="text-align: left;">
                                : ${cmd.editForm.policyInfoVO.fu}
                            </td>
                            <td></td>
                        </tr>
                    </table>

                    <table class="entry2" style="100%;">
                        <tr>
                            <td colspan="2" style="width: 600px;">
                                <hr/>
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align: right; width: 400px;">No. Reg Spaj</td>
							<td style="width: 200px;">
                                <form:input path="editForm.spaj" id="spaj" cssStyle="width: 150px;" title="klik 'Cari' untuk mempermudah pencarian" disabled="${cmd.editForm.spajIsDisabled}" onblur="triggerEvent('onBlurSpaj');"/>
                                <input type="button" value="Cari" name="searchSpaj"
                                       onclick="popWin('${path}/refund/refund_spaj.htm?win=refund', 350, 450);"
                                       onmouseout="nd();"
                                       ${cmd.editForm.spajIsDisabled}>
                                <form:errors path="editForm.spaj" cssStyle="color: red;"/>
                            </td>
						</tr>
                        <tr>
                            <td style="text-align: right;">
                                Alasan Batal
                            </td>
                            <td style="width: 850px;">
                                <form:select path="editForm.alasanCd"
                                             items="${cmd.editForm.alasanList}"
                                             itemLabel="value" itemValue="key"
                                             onchange="updateAlasan();" 
                                             disabled="${cmd.editForm.alasanListIsDisabled}" 
                                        />
                                <form:errors path="editForm.alasanCd" cssStyle="color: red;"/>
                            </td>
                        </tr>

                        <tr id="editForm.alasanDisplayId" style="display: ${cmd.editForm.alasanDisplay}">
							<td style="text-align: right;">Detail Alasan Lain-lain</td>
							<td>
                                <form:input path="editForm.alasan" cssStyle="width: 300px;" disabled="${cmd.editForm.alasanIsDisabled}"/>
                                <form:errors path="editForm.alasan" cssStyle="color: red;"/>
                            </td>
						</tr>

                        <tr>
                            <td style="text-align: right;">
                                Tindakan
                            </td>
                            <td>
                                <form:select path="editForm.tindakanCd"
                                             items="${cmd.editForm.tindakanList}"
                                             itemLabel="value" itemValue="key"
                                             onchange="triggerEvent( 'onChangeTindakanCd' );"
                                             disabled="${cmd.editForm.tindakanListIsDisabled}"
                                        />
                                <label id="ketTindakan"/>
                                <form:errors path="editForm.tindakanCd" cssStyle="color: red;"/>
                            </td>
                        </tr>

                        <tr id="editForm.spajBaruDisplayId" style="display: ${cmd.editForm.spajBaruDisplay}">
                            <td style="text-align: right;">No. Reg Spaj Baru Yang Sudah Terdaftar</td>
							<td>
                                <form:input path="editForm.spajBaru" cssStyle="width: 250px;" title="klik search untuk mempermudah pencarian" disabled="${cmd.editForm.spajBaruIsDisabled}" onblur="triggerEvent('onBlurNewSpaj');"/>
                                <input type="button" value="Cari" name="searchSpajBaru"
                                       onclick="popWin('${path}/refund/refund_spaj.htm?win=refund&isNewSpaj=true', 350, 450);"
                                       onmouseout="nd();"
                                       ${cmd.editForm.spajBaruIsDisabled}>
                                       
                                       <div id="tooltip2" style="position:absolute;visibility:hidden;clip:rect(0 420 150 0);width:420px;background-color:ffffc0;z-index:10"></div>
								<a href="javascript:void(0);" style="color: BLUE;" name="keterangan" id="keterangan"
									onmouseover="showtip2(this,event,'<LI>penginputan untuk REG SPAJ BARU bisa diinput lebih dari 1 SPAJ, <br> &nbsp;&nbsp;&nbsp;&nbsp; diinput dengan menggunakan koma sebagai pemisah antar SPAJ<br>&nbsp;&nbsp;&nbsp;&nbsp;, contoh format pengisian jika SPAJ lbh dari 1 :  <DD><LI>012010xxxxx, 012009xxxxx, 012008xxxxx, dst...<DD>');"
									onmouseout="hidetip2();">Keterangan Pengisian</a>
                                <form:errors path="editForm.spajBaru" cssStyle="color: red;"/>
                            </td>
						</tr>

                        <tr id="editForm.spajBaruDisplayId" style="display: ${cmd.editForm.spajBaruDisplay}">
                            <td style="text-align: right;">Nama Pemegang Polis Baru</td>
							<td>
                                <form:input path="editForm.PPBaru" cssStyle="width: 250px;" disabled="disabled" onblur="triggerEvent('onBlurNewSpaj');"/>
                            </td>
						</tr>
						
						<tr id="editForm.spajBaruDisplayId" style="display: ${cmd.editForm.spajBaruDisplay}">
                            <td style="text-align: right;">Nama Tertanggung Baru</td>
							<td>
                                <form:input path="editForm.TTBaru" cssStyle="width: 250px;" disabled="disabled" onblur="triggerEvent('onBlurNewSpaj');"/>
                            </td>
						</tr>
						
						<tr id="editForm.spajBaruDisplayId" style="display: ${cmd.editForm.spajBaruDisplay}">
                            <td style="text-align: right;">Produk Baru</td>
							<td>
                                <form:input path="editForm.produkBaru" cssStyle="width: 250px;" disabled="disabled" onblur="triggerEvent('onBlurNewSpaj');"/>
                            </td>
						</tr>

                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>

                        <tr style="display: ${cmd.editForm.penarikanUlinkVOListDisplay}">
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>


                        <!--todo-->

                            <tr style="display: ${cmd.editForm.hasUnitDisplay}">
                                <td style="text-align: right;">
                                    Sudah membeli unit
                                </td>
                                <td>
                                    <form:select path="editForm.hasUnitFlag" id="unitFlagId"
                                                 onchange="triggerEvent( 'onChangeHasUnitFlag' );"
                                                 disabled="${cmd.editForm.hasUnitListIsDisabled}" 
                                            >
                                        <form:option value="" label=""/>
                                        <form:option value="0" label="belum"/>
                                        <form:option value="1" label="sudah"/>
                                     </form:select>

                                    <form:errors path="editForm.hasUnitFlag" cssStyle="color: red;"/>
                                </td>
                                <td style="display: none;">
                                
                                <form:input path="editForm.statusUnit" id="statusUnitId"/>
                                </td>
                            </tr>

                        <!--end todo-->

                        <c:forEach var="penarikanUlinkVO" items="${cmd.editForm.penarikanUlinkVOList}" varStatus="xt">
                            <tr id="editForm.penarikanUlinkVOListDisplayId${xt.index}" style="display: ${cmd.editForm.penarikanUlinkVOListDisplay}">
                                <td style="text-align: right;">
                                        ${penarikanUlinkVO.deskripsiPenarikan} Rp.
                                </td>
                                <td>
                                    <form:input path="editForm.penarikanUlinkVOList[${xt.index}].jumlah" id="penarikanUlinkVOList[${xt.index}].jumlah" cssClass="fontForm" disabled="${cmd.editForm.biayaIsDisabled}" onfocus="showForm( 'jumlahHelper${xt.index}', 'true' );" onblur="showForm( 'jumlahHelper${xt.index}', 'false' );" onchange="this.value=formatCurrency( this.value );" onkeyup="showFormatCurrency('jumlahHelper${xt.index}', this.value);"/>
                                    <input type="text" id="jumlahHelper${xt.index}" disabled="disabled" style="display: none;" class="fontForm" tabindex="-1"/>
                                    <form:errors path="editForm.penarikanUlinkVOList[${xt.index }].jumlah" cssStyle="color: red;"/>
                                </td>
                            </tr>
                        </c:forEach>

                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <c:if test="${cmd.editForm.biayaMerchantDisplay eq ''}">
	                        <tr>
	                            <td style="text-align: right;">
	                                Jenis Merchant
	                            </td>
	                            <td>
	                                <form:select path="editForm.merchantCd"
	                                             items="${cmd.editForm.merchantList}"
	                                             itemLabel="value" itemValue="key"
	                                             onchange="triggerEvent( 'onChangeMerchantCd' );"
	                                             disabled="${cmd.editForm.merchantListIsDisabled}"
	                                        />
	                                <form:errors path="editForm.merchantCd" cssStyle="color: red;"/>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td style="text-align: right;">
	                                Biaya Merchant, ${currency}
	                            </td>
	                            <td>
	                                <form:input path="editForm.biayaMerchant" cssStyle="width: 100px;" disabled="disabled"/>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td colspan="2">
	                                &nbsp;
	                            </td>
	                        </tr>
						</c:if>

                        <tr id="editForm.biayaAdminDisplayId" style="display: ${cmd.editForm.biayaAdminDisplay}">
							<td style="text-align: right;">
                                Biaya Admin Pembatalan, ${currency}
                            </td>
                            <td>
                                <form:input path="editForm.biayaAdmin" cssStyle="width: 100px;" onfocus="showForm( 'biayaAdminHelper', 'true' );" onblur="showForm( 'biayaAdminHelper', 'false' );" onchange="this.value=formatCurrency( this.value );" onkeyup="showFormatCurrency('biayaAdminHelper', this.value);" disabled="${cmd.editForm.biayaAdminIsDisabled}"/>
                                <input type="text" id="biayaAdminHelper" disabled="disabled" style="display: none;" class="fontForm" tabindex="-1"/>
                                <form:errors path="editForm.biayaAdmin" cssStyle="color: red;"/>
                            </td>
						</tr>

                        <tr id="editForm.biayaMedisDisplayId" style="display: ${cmd.editForm.biayaMedisDisplay}">
							<td style="text-align: right;">
                                Biaya Medis, ${currency}
                            </td>
                            <td>
                                <form:input path="editForm.biayaMedis" cssStyle="width: 100px;" onfocus="showForm( 'biayaMedisHelper', 'true' );" onblur="showForm( 'biayaMedisHelper', 'false' );" onchange="this.value=formatCurrency( this.value );" onkeyup="showFormatCurrency('biayaMedisHelper', this.value);" disabled="${cmd.editForm.biayaMedisIsDisabled}"/>
                                <input type="text" id="biayaMedisHelper" disabled="disabled" style="display: none;" class="fontForm" tabindex="-1"/>
                                <form:errors path="editForm.biayaMedis" cssStyle="color: red;"/>
                            </td>
						</tr>

                        <tr id="editForm.biayaLainDisplayId" style="display: ${cmd.editForm.biayaLainDisplay}">
							<td style="text-align: right;">
                                Biaya Lain2, ${currency}
                            </td>
                            <td>
                                <form:input path="editForm.biayaLain" cssStyle="width: 100px;" onfocus="showForm( 'biayaLainHelper', 'true' );" onblur="showForm( 'biayaLainHelper', 'false' );" onchange="this.value=formatCurrency( this.value );" onkeyup="showFormatCurrency('biayaLainHelper', this.value);" disabled="${cmd.editForm.biayaLainIsDisabled}"/>
                                <input type="text" id="biayaLainHelper" disabled="disabled" style="display: none;" class="fontForm" tabindex="-1"/>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                Deskripsi:
                                <form:input path="editForm.biayaLainDescr" disabled="${cmd.editForm.biayaLainIsDisabled}"/>
                                <form:errors path="editForm.biayaLain" cssStyle="color: red;"/>
                                <form:errors path="editForm.biayaLainDescr" cssStyle="color: red;"/>
                            </td>
						</tr>

                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>

                        <tr id="editForm.atasNamaDisplayId" style="display: ${cmd.editForm.atasNamaDisplay}">
                            <td style="text-align: right;">Rekening Atas Nama</td>
							<td>
                                <form:input path="editForm.atasNama" cssStyle="width: 300px;" disabled="${cmd.editForm.atasNamaIsDisabled}"/>
                                <form:errors path="editForm.atasNama" cssStyle="color: red;"/>
                            </td>
						</tr>

                        <tr id="editForm.norekDisplayId" style="display: ${cmd.editForm.norekDisplay}">
							<td style="text-align: right;">Nomor Rekening Klien</td>
							<td>
                                <form:input path="editForm.norek" cssStyle="width: 300px;" disabled="${cmd.editForm.norekIsDisabled}"/>
                                <form:errors path="editForm.norek" cssStyle="color: red;"/>
                            </td>
						</tr>

                        <tr id="editForm.namaBankDisplayId" style="display: ${cmd.editForm.namaBankDisplay}">
							<td style="text-align: right;">Nama Bank</td>
							<td>
                                <form:input path="editForm.namaBank" cssStyle="width: 300px;" disabled="${cmd.editForm.namaBankIsDisabled}"/>
                                <form:errors path="editForm.namaBank" cssStyle="color: red;"/>
                            </td>
						</tr>

                        <tr id="editForm.cabangBankDisplayId" style="display: ${cmd.editForm.cabangBankDisplay}">
							<td style="text-align: right;">Cabang Bank</td>
							<td>
                                <form:input path="editForm.cabangBank" cssStyle="width: 300px;" disabled="${cmd.editForm.cabangBankIsDisabled}"/>
                                <form:errors path="editForm.cabangBank" cssStyle="color: red;"/>
                            </td>
						</tr>

                        <tr id="editForm.kotaBankDisplayId" style="display: ${cmd.editForm.kotaBankDisplay}">
							<td style="text-align: right;">Kota Bank</td>
							<td>
                                <form:input path="editForm.kotaBank" cssStyle="width: 300px;" disabled="${cmd.editForm.kotaBankIsDisabled}"/>
                                <form:errors path="editForm.kotaBank" cssStyle="color: red;"/>
                            </td>
                        </tr>
                        
                     <c:set var="sendPhysicalDocIsDisabled" value=""/>
                        <c:choose>
                            <c:when test="${cmd.editForm.sendPhysicalDocIsDisabled=='disabled'}">
                                <c:set var="sendPhysicalDocIsDisabled" value="true"/>
                            </c:when>
                            <c:otherwise>
                                <c:set var="sendPhysicalDocIsDisabled" value="false"/>
                            </c:otherwise>
                        </c:choose>

                        <tr id="editForm.sendPhysicalDocDateDisplayId" style="display: ${cmd.editForm.sendPhysicalDocDateDisplay}">
							<td style="text-align: right;">Tanggal Kirim Dokumen Fisik</td>
							<td>
                                <spring:bind path="cmd.editForm.sendPhysicalDocDate" >
        		                    <script>
                                        var isDisabled;
                                        var status = '${sendPhysicalDocIsDisabled}';
                                        if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
                                </spring:bind>
                                  <input type="button" name="limit" value="Update Tgl Kirim"
                        onclick="return triggerEvent('onPressButtonSendPhysicalDoc');"
                        accesskey="B"
                       onmouseout="nd();"
                        ${cmd.editForm.buttonSendPhysicalDocIsDisabled} />
                       <form:errors path="editForm.sendPhysicalDocDate" cssStyle="color: red;"/>
                            </td>
                         
                        </tr>
                        
                        <c:set var="paymentIsDisabled" value=""/>
                        <c:choose>
                            <c:when test="${cmd.editForm.paymentIsDisabled=='disabled'}">
                                <c:set var="paymentIsDisabled" value="true"/>
                            </c:when>
                            <c:otherwise>
                                <c:set var="paymentIsDisabled" value="false"/>
                            </c:otherwise>
                        </c:choose>

                        <tr id="editForm.paymentDateDisplayId" style="display: ${cmd.editForm.paymentDisplay}">
							<td style="text-align: right;">Tanggal Pembayaran</td>
							<td>
                                <spring:bind path="cmd.editForm.paymentDate" >
        		                    <script>
                                        var isDisabled;
                                        var status = '${paymentIsDisabled}';
                                        if( status == 'true' ) isDisabled = true; else isDisabled = false;
                                        inputDate('${status.expression}', '${status.value}', isDisabled);
                                    </script>
                                </spring:bind>
                                <form:errors path="editForm.paymentDate" cssStyle="color: red;"/>
                            </td>
                        </tr>

                        <tr id="editForm.paymentDisplayId" style="display: ${cmd.editForm.paymentDisplay}">
							<td style="text-align: right;">
                                Jumlah Pembayaran, ${currency}
                            </td>
                            <td>
                                <form:input path="editForm.payment" cssStyle="width: 100px;" onfocus="showForm( 'paymentHelper', 'true' );" onblur="showForm( 'paymentHelper', 'false' );" onchange="this.value=formatCurrency( this.value );" onkeyup="showFormatCurrency('paymentHelper', this.value);" disabled="${cmd.editForm.paymentIsDisabled}"/>
                                <input type="text" id="paymentHelper" disabled="disabled" style="display: none;" class="fontForm" tabindex="-1"/>
                                <form:errors path="editForm.payment" cssStyle="color: red;"/>
                            </td>
						</tr>
						
						
				<c:forEach var="lampiranAdditional" items="${cmd.editForm.lampiranAddList}" varStatus="xt">
						<tr>
							<td style="text-align: right;">
								<input type="button" value="DELETE" onclick="deleteLampiran('${xt.index }')" 
								<c:if test="${cmd.editForm.lampiranAddList[xt.index].isDisabled eq 'disabled'}"> disabled </c:if> />
							</td>
							<td>
						
				    <form:checkbox path="editForm.lampiranAddList[${xt.index }].checkBox" value="true" disabled = "${cmd.editForm.lampiranAddList[xt.index].isDisabled}" id="checkBox${xt.index}" />
					<label for="lampiranAdditional[${xt.index}].lampiranLabel">
                        ${lampiranAdditional.lampiranLabel}
                    </label>		
							</td>
							</tr>
						</c:forEach>
						
					 <tr style="display: ${cmd.editForm.addLampiranDisplay}">
					 		<td style="text-align: right;">
					 		Tambah Lampiran :
							</td>
                            <td colspan="2">
                               <form:input path="editForm.addLampiran" id="addLampiran" size="80" />
                               <input type="button" value="Add" onclick="return triggerEvent('onPressButtonAddLampiran');" ${cmd.editForm.addLampiranDisabled}/>
                            </td>
                        </tr>
						
                        <tr>
                            <td colspan="2" style="text-align: center;">
                                &nbsp;
                            </td>
                        </tr>

                        <tr>
                            <td></td>
                            <td style="text-align: left;">
                                <input type="button" name="limit" value="Back"
                                       onclick="return triggerEvent('onPressButtonBack');"
                                       accesskey="B"
                                       onmouseout="nd();"
                                       >
                                <input type="button" name="limit" value="Next"
                                       onclick="return triggerEvent('onPressButtonNext');"
                                       accesskey="N"
                                       onmouseout="nd();"
                                       style="display: ${cmd.editForm.buttonNextDisplay}" 
                                       >
                                <input type="button" name="limit" value="Save Draft"
                                       onclick="return triggerEvent('onPressButtonSaveDraft');"
                                       onmouseout="nd();"
                                       style="display: ${cmd.editForm.buttonSaveDraftDisplay}"
                                       >
                                <input type="button" name="limit" value="Batalkan SPAJ"
                                       onclick="return triggerEvent('onPressButtonBatalkanSpaj');"
                                       onmouseout="nd();"
                                       style="display: ${cmd.editForm.buttonBatalkanSpajDisplay}"
                                       >
 								<input type="button" name="limit" value="Preview Surat Batal"
                                       onclick="return triggerEvent('onPressButtonPreviewSuratBatal');"
                                       accesskey="V"
                                       onmouseout="nd();"
                                       style="display: ${cmd.editForm.buttonPreviewSuratBatalDisplay}"
                                       ${cmd.editForm.buttonPreviewSuratBatalIsDisabled}  
                                       >
                            
                            </td>
                        </tr>

                    </table>
</form:form>
			</div>
		</div>

	</div>

    <script type="text/javascript">
        if( '' != '${pageMessage}' )
        {
            alert( '${pageMessage}');
        }
   
		setDisabledBgColor();
     
    </script>
    <script type="text/javascript">
        var downloadFlag = document.getElementById('downloadFlag').value;
        if( downloadFlag == 'newPage' )
        {
            document.getElementById('downloadFlag').value = '';
            document.getElementById('formpost').target = '_blank';
            submitForm( STD_DOWNLOAD_JSP );
            document.getElementById('formpost').target = '_self';
            submitForm( REFUND_EDIT );
        }
        if( downloadFlag == 'samePage' )
        {
            document.getElementById('downloadFlag').value = '';
            document.getElementById('formpost').action = '${downloadUrlSession}';
            document.getElementById('formpost').submit();
            document.getElementById('formpost').target = '_self';
        }
         setDisabledBgColor();
    </script>
</body>
<%@ include file="/include/page/footer.jsp"%>