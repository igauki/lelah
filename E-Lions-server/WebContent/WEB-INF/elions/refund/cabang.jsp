<%--
/**********************************************************************
 * Program History
 *
 * Project Name      	: E-Lions
 * Function Id         	:
 * Program Name   		: cabang_edit
 * Description         	:
 * Environment      	: Java  1.5.0_06
 * Author               : samuel
 * Version              : 1.0
 * Creation Date    	: Oct 13, 2008 4:58:36 PM
 *
 * Update history   Re-fix date      Person in charge      Description
 *
 *
 *
 * Copyright(C) 2008-Asuransi Jiwa Sinarmas. All Rights Reserved.
 ***********************************************************************/
--%>

<%@ include file="/include/page/header.jsp"%>

<script type="text/javascript" src="${path }/include/js/refund/refund_common.js"></script>
<style type="text/css">
    html, body {
    }

    }

</style>

<script type="text/javascript">

    function triggerEvent( theEvent )
    {
        document.forms[ 0 ].theEvent.value = theEvent;
        checkEvent();
    }

    function checkEvent()
    {
        var theEvent = document.forms[ 0 ].theEvent.value;
        if( theEvent == 'onBlurSpaj' )
        {
            submitForm( CABANG_EDIT );
        }
        if( theEvent == 'onPressButtonBatalkanSpaj' )
        {
            var isOk = window.confirm( "Anda yakin untuk membatalkan SPAJ?" )
            if( isOk )submitForm( CABANG_EDIT );
        }
    }

    function updateAlasan()
    {
        var alasanCd = document.getElementById('editForm.alasanCd').value;
        triggerEvent("onChangeAlasanCd");
        if( alasanCd == alasanLain2 )
        {
            document.getElementById('editForm.alasan').value = '';
            document.getElementById('editForm.alasanDisplayId').style.display = '';
            document.getElementById('editForm.alasan').focus();
        }
        else
        {
            document.getElementById('editForm.alasan').value = showLabelOfDropDown('editForm.alasanCd');
            document.getElementById('editForm.alasanDisplayId').style.display = 'none';
        }
        submitForm( CABANG_EDIT );
    }


</script>

<body style="height: 100%;" onload="setupPanes('container1', 'tab1'); ">

<div class="tab-container" id="container1">

<ul class="tabs">
    <li>
        <a href="#" onClick="return showPane('pane1', this)" id="tab1">Pembatalan SPAJ Cabang ${pageSubTitle}</a>
    </li>
</ul>

<div class="tab-panes">
<div id="pane1" class="panes" style="">
<form:form id="formpost" commandName="cmd" name="">

                    <input type="hidden" name="theEvent" id="theEvent"/>

                    <form:hidden path="editForm.alasanDisplay"/>

                    <table style="width: 100%;">
                        <tr><td colspan="2"><input type="text" id="textMsg" name="textMsg" style="font-weight: bold; border: thin; width: 15cm; background-color: #F7F7F7;" disabled="disabled" /></td></tr>
                		<tr> 
                            <td style="text-align: left; width: 150px;">
                                No. Blanko / Seri
                            </td>
                            <td style="text-align: left; width: 400px;">
                                : ${cmd.editForm.policyInfoVO.blankoNo}
                            </td>
                            <td style="width: 100%;"></td>
                        </tr>
                        <tr>
                            <td style="text-align: left; width: 150px;">
                                No. Reg SPAJ / No Polis
                            </td>
                            <td style="text-align: left; width: 400px;">
                                : ${cmd.editForm.policyInfoVO.spajNo} / ${cmd.editForm.policyInfoVO.policyNo}
                            </td>
                            <td style="width: 100%;"></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">
                                Nama Pemegang Polis
                            </td>
                            <td style="text-align: left;">
                                : ${cmd.editForm.policyInfoVO.namaPp}
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">
                                Nama Tertanggung
                            </td>
                            <td style="text-align: left;">
                                : ${cmd.editForm.policyInfoVO.namaTt}
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">
                                Produk
                            </td>
                            <td style="text-align: left;">
                                : ${cmd.editForm.policyInfoVO.namaProduk}
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">
                                Premi
                            </td>
                            <td style="text-align: left;">
                                : ${cmd.editForm.policyInfoVO.premiDisplay}
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">
                                UP
                            </td>
                            <td style="text-align: left;">
                                : ${cmd.editForm.policyInfoVO.uangPertanggunganDisplay}
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">
                               	Nama Agen
                            </td>
                            <td style="text-align: left;">
                                : ${cmd.editForm.policyInfoVO.namaAgent}
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">
                                Kode Agen
                            </td>
                            <td style="text-align: left;">
                                : ${cmd.editForm.policyInfoVO.kodeAgent}
                            </td>
                            <td></td>
                        </tr>
                    </table>

                    <table class="entry2" style="100%;">
                        <tr>
                            <td colspan="2" style="width: 600px;">
                                <hr/>
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align: right; width: 400px;">No. Reg Spaj</td>
							<td style="width: 200px;">
                                <form:input path="editForm.spaj" id="spaj" cssStyle="width: 150px;" title="klik 'Cari' untuk mempermudah pencarian" disabled="${cmd.editForm.spajIsDisabled}" onblur="triggerEvent('onBlurSpaj');"/>
                                <input type="button" value="Cari" name="searchSpaj"
                                       onclick="popWin('${path}/refund/refund_spaj.htm?win=refund&spaj='+document.getElementById('spaj').value, 350, 450);"
                                       accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);"
                                       onmouseout="nd();"
                                       ${cmd.editForm.spajIsDisabled}>
                                <form:errors path="editForm.spaj" cssStyle="color: red;"/>
                            </td>
						</tr>
                        <tr>
                            <td style="text-align: right;">
                                Alasan Batal
                            </td>
                            <td style="width: 850px;">
                                <form:select path="editForm.alasanCd"
                                             items="${cmd.editForm.alasanList}"
                                             itemLabel="value" itemValue="key"
                                             onchange="updateAlasan();"
                                             disabled="${cmd.editForm.alasanListIsDisabled}"
                                        />
                                <form:errors path="editForm.alasanCd" cssStyle="color: red;"/>
                            </td>
                        </tr>

                        <tr id="editForm.alasanDisplayId" style="display: ${cmd.editForm.alasanDisplay}">
							<td style="text-align: right;">Detail Alasan Lain-lain</td>
							<td>
                                <form:input path="editForm.alasan" cssStyle="width: 300px;" disabled="${cmd.editForm.alasanIsDisabled}"/>
                                <form:errors path="editForm.alasan" cssStyle="color: red;"/>
                            </td>
						</tr>




                    

                        <tr>
                            <td></td>
                            <td style="text-align: left;">
                                <input type="button" name="limit" value="Batalkan SPAJ"
                                        onclick="return triggerEvent('onPressButtonBatalkanSpaj');"
                                        accesskey="T" onmouseover="return overlib('Alt-T > Batalkan SPAJ', AUTOSTATUS, WRAP);"
                                        onmouseout="nd();"
                                        style="display: ${cmd.editForm.buttonBatalkanSpajDisplay}"
                                                >
                            </td>
                        </tr>

                    </table>
</form:form>
			</div>
		</div>

	</div>

    <script type="text/javascript">
        if( '' != '${pageMessage}' )
        {
            alert( '${pageMessage}');
        }

		setDisabledBgColor();

    </script>
</body>
<%@ include file="/include/page/footer.jsp"%>