<%--
/**********************************************************************
 * Program History
 *
 * Project Name      	: E-Lions
 * Function Id         	:
 * Program Name   		: preview_lamp_1
 * Description         	:
 * Environment      	: Java  1.5.0_06
 * Author               : samuel
 * Version              : 1.0
 * Creation Date    	: Oct 13, 2008 4:58:36 PM
 *
 * Update history   Re-fix date      Person in charge      Description
 *
 *
 *
 * Copyright(C) 2008-Asuransi Jiwa Sinarmas. All Rights Reserved.
 ***********************************************************************/
--%>

<%@ include file="/include/page/header.jsp"%>

<style type="text/css">
    html, body {
    }

</style>

<script type="text/javascript" src="${path }/include/js/refund/refund_common.js"></script>
<script type="text/javascript">

    function triggerEvent( theEvent )
    {
        document.forms[ 0 ].theEvent.value = theEvent;
        checkEvent();
    }

    function checkEvent()
    {
        var theEvent = document.forms[ 0 ].theEvent.value;

        if( theEvent == 'onPressButtonSaveDraft' )
        {
            submitForm( PREVIEW_LAMP_1_JSP );
        }
        else if( theEvent == 'onPressButtonViewAttachment' )
        {
            submitForm( PREVIEW_LAMP_1_JSP );
        }
        else if( theEvent == 'onPressButtonPreviewIntruksiRedempt' )
        {
            submitForm( PREVIEW_INSTRUKSI_REDEMPT_JSP );
        }
        else if( theEvent == 'onPressButtonBatalkanSpaj' )
        {
            submitForm( PREVIEW_LAMP_1_JSP );
        }
        else if( theEvent == 'onPressAccBatalSpaj' )
        {
            submitForm( PREVIEW_LAMP_1_JSP );
        }
        else if( theEvent == 'onPressButtonUpdatePayment' )
        {
            submitForm( PREVIEW_LAMP_1_JSP );
        }
        else if( theEvent == 'onPressButtonBack' )
        {
            if( document.getElementById('editForm.tindakanCd').value == tindakanRefundPremi && document.getElementById('statusUnitId').value == 'sudah' && document.getElementById('posisiNo').value < 2 )
            {
                submitForm( PREVIEW_EDIT_JSP );
            }
            else if( document.getElementById('editForm.tindakanCd').value == tindakanRefundPremi && document.getElementById('statusUnitId').value == 'belum' && document.getElementById('posisiNo').value < 1 )
            {
                submitForm( PREVIEW_EDIT_SETORAN );
            }
            else
            {
           	    submitForm( REFUND_EDIT );
            }
        }
    }

</script>

<body style="" onload="setupPanes('container1', 'tab1'); ">

    <div class="tab-container" id="container1">

        <ul class="tabs">
            <li>
                <a href="#" onClick="return showPane('pane1', this)" id="tab1">Pembatalan Polis --> Preview Refund Premi</a>
            </li>
        </ul>

        <div class="tab-panes">
            <div id="pane1" class="panes" style="">
                <form:form id="formpost" commandName="cmd" name="">

                    <input type="hidden" name="theEvent" id="theEvent"/>
                    <form:hidden path="lamp1Form.downloadFlag" id="downloadFlag"/>
                    <form:hidden path="editForm.tindakanCd" id="editForm.tindakanCd"/>
                 	<form:hidden path="editForm.statusUnit" id="statusUnitId"/>
                    <form:hidden path="editForm.posisiNo" id="posisiNo"/>

                    <table style="width: 570px;" class="viewTable">
                    <tr><td><input type="text" id="textMsg" name="textMsg" style="font-weight: bold; border: thin; width: 15cm; background-color: #F7F7F7;" disabled="disabled" /></td></tr>
                    <tr>
                        <td style="text-align: center;"><b>PT ASURANSI JIWA SINARMAS MSIG</b><br/><br/></td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                   <tr>
                                    <td style="text-align: left; width: 25%; text-transform: none;">
                                        No Surat
                                    </td>
                                    <td style="text-align: left; width: 75%; text-transform: none;">
                                        : ${cmd.refundDocumentVO.params.noSurat}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 25%; text-transform: none;">
                                        Kepada
                                    </td>
                                    <td style="text-align: left; width: 75%; text-transform: none;">
                                        : Bagian Keuangan
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; text-transform: none;">
                                        Dari
                                    </td>
                                    <td style="text-align: left; text-transform: none;">
                                        : Bagian Underwriting
                                    </td>
                                </tr>
                                <!--  <tr>
                                    <td style="text-align: left; text-transform: none;">
                                        No. urut memo
                                    </td>
                                    <td style="text-align: left; text-transform: none;">
                                        : ${cmd.refundDocumentVO.params.noUrutMemo}
                                    </td>
                                </tr>
                                -->
                                <tr>
                                    <td style="text-align: left; text-transform: none;">
                                        Hal
                                    </td>
                                    <td style="text-align: left; text-transform: none;">
                                        : ${cmd.refundDocumentVO.params.hal}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; text-transform: none;">
                                        Tanggal
                                    </td>
                                    <td style="text-align: left; text-transform: none;">
                                        : ${cmd.refundDocumentVO.params.tanggal}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; text-transform: none;">
                                        Cc
                                    </td>
                                    <td style="text-align: left; text-transform: none;">
                                        : Accounting, Agen, Cabang
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; text-transform: none;">
                                        Hal
                                    </td>
                                    <td style="text-align: left; text-transform: none;">
                                        : <b>Pembatalan Polis dan Pengembalian Premi</b>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <hr/>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="text-align: left; width: 50%; text-transform: none;">
                                        No. Reg SPAJ
                                    </td>
                                    <td style="text-align: left; width: 50%; text-transform: none;">
                                        : ${cmd.refundDocumentVO.params.spajNo}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; text-transform: none;">
                                        Polis No.
                                    </td>
                                    <td style="text-align: left; text-transform: none;">
                                        : ${cmd.refundDocumentVO.params.polisNo}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; text-transform: none;">
                                        Produk
                                    </td>
                                    <td style="text-align: left; text-transform: none;">
                                        : ${cmd.refundDocumentVO.params.produk}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; text-transform: none;">
                                        Pemegang Polis
                                    </td>
                                    <td style="text-align: left; text-transform: none;">
                                        : ${cmd.refundDocumentVO.params.pemegangPolis}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; text-transform: none;">
                                        Tertanggung
                                    </td>
                                    <td style="text-align: left; text-transform: none;">
                                        : ${cmd.refundDocumentVO.params.tertanggung}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="text-align: left; text-transform: none;">
                                        <br/>
                                            ${cmd.refundDocumentVO.params.statement}
                                        <br/>
                                        <br/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table>
                                <c:forEach var="rincianMap" items="${cmd.refundDocumentVO.params.dataSource}" varStatus="xt">
                                    <tr>
                                        <td style="text-align: left; text-transform: none; width:80%;">
                                                ${rincianMap.descr}
                                        </td>
                                        <td style="text-align: left; text-transform: none; width:20%;">
                                                ${rincianMap.jumlah}
                                        </td>
                                    </tr>
                                </c:forEach>
                                <tr>
                                    <td colspan="2" style="text-align: left; text-transform: none; font-weight: bold;">
                                            ${cmd.refundDocumentVO.params.jumlahTerbilang}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td colspan="3" style="text-align: left; text-transform: none;">
                                        Uang Tersebut agar ditransfer ke:
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 20%;"></td>
                                    <td style="text-align: left; width: 60%; text-transform: none;">
                                        Atas Nama
                                    </td>
                                    <td style="text-align: left; width: 20%; text-transform: none;">
                                        : ${cmd.refundDocumentVO.params.atasNama}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 20%;"></td>
                                    <td style="text-align: left; width: 60%; text-transform: none;">
                                        Nomor Rekening
                                    </td>
                                    <td style="text-align: left; width: 20%; text-transform: none;">
                                        : ${cmd.refundDocumentVO.params.rekeningNo}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 20%;"></td>
                                    <td style="text-align: left; width: 60%; text-transform: none;">
                                        Nama Bank
                                    </td>
                                    <td style="text-align: left; width: 20%; text-transform: none;">
                                        : ${cmd.refundDocumentVO.params.bankName}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 20%;"></td>
                                    <td style="text-align: left; width: 60%; text-transform: none;">
                                        Cabang
                                    </td>
                                    <td style="text-align: left; width: 20%; text-transform: none;">
                                        : ${cmd.refundDocumentVO.params.cabang}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 20%;"></td>
                                    <td style="text-align: left; width: 60%; text-transform: none;">
                                        Kota
                                    </td>
                                    <td style="text-align: left; width: 20%; text-transform: none;">
                                        : ${cmd.refundDocumentVO.params.kota}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table>
                                <c:forEach var="lampiranMap" items="${cmd.refundDocumentVO.params.dsLampiranRefund}" varStatus="xt">
	                                <c:if test="${xt.index eq 0}">
										<tr>
		                                    <td colspan="2" style="text-align: left; text-transform: none;">
		                                            Terlampir kami sampaikan:
		                                    </td>
		                                </tr>
	                                </c:if>
	                                <tr>
	                                    <td style="text-align: right; text-transform: none; width:3%;">
	                                           ${lampiranMap.dash}
	                                    </td>
	                                    <td style="text-align: left; text-transform: none; width:97%;">
	                                            ${lampiranMap.content}
	                                    </td>
	                                </tr>
                                </c:forEach>
                                
								<c:forEach var="addLampiranMap" items="${cmd.refundDocumentVO.params.dsAddLampiranRefund}" varStatus="xt">
                                	<c:if test="${xt.index eq 0}">
	                                    <tr>
	                                    	<td colspan="2" style="text-align: left; text-transform: none;"></td>
	                                	</tr>
                                	</c:if>
                                    <tr>
                                        <td style="text-align: right; text-transform: none; width:3%;">
                                               ${addLampiranMap.dashAdd}
                                        </td>
                                        <td style="text-align: left; text-transform: none; width:97%;">
                                                ${addLampiranMap.contentAdd}
                                        </td>
                                    </tr>
                                </c:forEach>
                                <tr><td colspan="2">&nbsp;</td></tr>
                                <tr>
                                    <td colspan="2" style="text-align: left; text-transform: none;">
                                        Demikian kami sampaikan, atas bantuan dan kerjasamanya kami ucapkan terima kasih
                                        <br/><br/>
                                        Hormat kami,
                                        <br/><br/>
                                        <!-- helpdesk [143790] perubahan tanda tangan suryanto lim, sistikarsinah, inge ke anna yulia -->
                                        <%-- ${cmd.refundDocumentVO.params.signer} --%>
                                        <fmt:bundle basename="ekalife"><fmt:message key="printing.namaRefundPremi"></fmt:message></fmt:bundle>
                                        <br/>
                                        <!-- helpdesk [143790] perubahan tanda tangan suryanto lim, sistikarsinah, inge ke anna yulia -->
                                        <!-- Underwriting Department -->
                                        <fmt:bundle basename="ekalife"><fmt:message key="printing.JabatanRefundPremi"></fmt:message></fmt:bundle>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="text-align: center;">
                                        <br/>
                                        <input type="button" name="limit" value="Save Draft"
                                               onclick="return triggerEvent('onPressButtonSaveDraft');"
                                               accesskey="S" onmouseover="return overlib('Alt-S > Save Draft Form Pembatalan', AUTOSTATUS, WRAP);"
                                               onmouseout="nd();"
                                               ${cmd.lamp1Form.buttonSaveDraftIsDisabled}
                                               >
                                        <input type="button" name="limit" value="Preview Redempt"
                                               onclick="return triggerEvent('onPressButtonPreviewIntruksiRedempt');"
                                               accesskey="B" onmouseover="return overlib('Alt-B > Batalkan SPAJ', AUTOSTATUS, WRAP);" 
                                               onmouseout="nd();"
                                               style="display: ${cmd.lamp1Form.buttonPreviewRedemptDisplay}"
                                               >
                                        <input type="button" name="limit" value="Transfer Ke Akseptasi Pembatalan"
                                               onclick="return triggerEvent('onPressAccBatalSpaj');"
                                               accesskey="A" onmouseover="return overlib('Alt-A > Aksep Pembatalan', AUTOSTATUS, WRAP);"
                                               onmouseout="nd();"
                                               ${cmd.lamp1Form.buttonAccBatalSpajIsDisabled}
                                               >
                                        <input type="button" name="limit" value="Batalkan SPAJ"
                                               onclick="return triggerEvent('onPressButtonBatalkanSpaj');"
                                               accesskey="T" onmouseover="return overlib('Alt-T > Batalkan SPAJ', AUTOSTATUS, WRAP);"
                                               onmouseout="nd();"
                                               ${cmd.lamp1Form.buttonBatalkanSpajIsDisabled}
                                               >
                                        <input type="button" name="limit" value="Update Payment"
                                               onclick="return triggerEvent('onPressButtonUpdatePayment');"
                                               accesskey="Y" onmouseover="return overlib('Alt-Y > Update Payment and Send Surat Pemberitahuan Pembayaran', AUTOSTATUS, WRAP);"
                                               onmouseout="nd();"
                                               ${cmd.lamp1Form.buttonUpdatePaymentIsDisabled}
                                               >
                                        <input type="button" name="limit" value="View Attachment"
                                               onclick="return triggerEvent('onPressButtonViewAttachment');"
                                               accesskey="V"
                                               onmouseover="return overlib('Alt-V > View Attachment', AUTOSTATUS, WRAP);"
                                               onmouseout="nd();"
                                               ${cmd.lamp1Form.buttonViewAttachmentIsDisabled}  
                                               >                                        
                                        <input type="button" name="limit" value="Back"
                                               onclick="return triggerEvent('onPressButtonBack');"
                                               accesskey="B"
                                               onmouseover="return overlib('Alt-B > Back', AUTOSTATUS, WRAP);"
                                               onmouseout="nd();"
                                               >
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    </table>

                </form:form>
			</div>
		</div>

	</div>

    <script type="text/javascript">
        if( '' != '${pageMessage}' )
        {
            alert( '${pageMessage}');
        }
        if( '' != '${errMessageFailed}' )
        {
            alert( '${errMessageFailed}');
        }
    </script>

    <script type="text/javascript">
        var downloadFlag = document.getElementById('downloadFlag').value;
        if( downloadFlag == 'newPage' )
        {
            document.getElementById('downloadFlag').value = '';
            document.getElementById('formpost').target = '_blank';
            submitForm( STD_DOWNLOAD_JSP );
            document.getElementById('formpost').target = '_self';
            submitForm( PREVIEW_LAMP_1_JSP );
        }
        if( downloadFlag == 'samePage' )
        {
            document.getElementById('downloadFlag').value = '';
            document.getElementById('formpost').action = '${downloadUrlSession}';
            document.getElementById('formpost').submit();
            document.getElementById('formpost').target = '_self';
        }
         setDisabledBgColor();
    </script>
</body>
<%@ include file="/include/page/footer.jsp"%>