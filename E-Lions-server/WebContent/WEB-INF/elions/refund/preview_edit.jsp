<%--
/**********************************************************************
 * Program History
 *
 * Project Name      	: E-Lions
 * Function Id         	:
 * Program Name   		: preview_edit
 * Description         	:
 * Environment      	: Java  1.5.0_06
 * Author               : fadly mathendra
 * Version              : 1.0
 * Creation Date    	: Oct 13, 2008 4:58:36 PM
 *
 * Update history   Re-fix date      Person in charge      Description
 *
 *
 *
 * Copyright(C) 2008-Asuransi Jiwa Sinarmas. All Rights Reserved.
 ***********************************************************************/
--%>

<%@ include file="/include/page/header.jsp"%>

<style type="text/css">
    html, body {
    }

</style>

<script type="text/javascript" src="${path }/include/js/refund/refund_common.js"></script>
<script type="text/javascript">

    function triggerEvent( theEvent )
    {
        document.forms[ 0 ].theEvent.value = theEvent;
        checkEvent();
    }

    function checkEvent()
    {
        var theEvent = document.forms[ 0 ].theEvent.value;

        if( theEvent == 'onPressButtonNext' )
        {
            submitForm( PREVIEW_LAMP_1_JSP );
        }
        else if( theEvent == 'onPressButtonBack' )
        {
            submitForm( REFUND_EDIT );
        }
        else if( theEvent == 'onPressButtonDelete' ||  theEvent == 'onPressButtonAddNewRow' || theEvent == 'onPressButtonDeleteNew' || theEvent == 'onPressButtonCount')
        {
            submitForm( PREVIEW_EDIT_JSP );
        }
    }

</script>

<body style="" onload="setupPanes('container1', 'tab1'); ">

    <div class="tab-container" id="container1">

        <ul class="tabs">
            <li>
                <a href="#" onClick="return showPane('pane1', this)" id="tab1">Pembatalan Polis --> Preview Refund Premi</a>
            </li>
        </ul>

        <div class="tab-panes">
            <div id="pane1" class="panes" style="">
                <form:form id="formpost" commandName="cmd" name="">

                    <input type="hidden" name="theEvent" id="theEvent"/>
                    <form:hidden path="lamp1Form.downloadFlag" id="downloadFlag"/>
                    <table style="width: 950px;" class="viewTable">
                    <tr><td><input type="text" id="textMsg" name="textMsg" style="font-weight: bold; border: thin; width: 15cm; background-color: #F7F7F7;" disabled="disabled" /></td></tr>
                    <tr>
                        <td style="text-align: center;"><b>PT ASURANSI JIWA SINARMAS MSIG</b><br/><br/></td>
                    </tr>
                                        
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="text-align: left; text-transform: none;">
                                        <br/>
                                            ${cmd.refundDocumentVO.params.statement}
                                        <br/>
                                        <br/>
                                    </td>
                                   
                                </tr>
                                <tr>
                                <td>
                                 <input type="button" value="ADD NEW ROW" onclick="return triggerEvent('onPressButtonAddNewRow')"/>
                                 </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                    
                        <td>
                            <table>
                            <tr>
                          
                            <td> &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
							DESKRIPSI
                            </td>
                            <td style="size: 5px;">
                            </td>
                            <td>
                            	SETORAN
                            </td>
                            <td>
                         DEBET
                            </td>
                            <td>
                           KREDIT
                            </td>
                            </tr>
                                <c:forEach var="rincianMap" items="${cmd.editForm.tempDescrDanJumlah}" varStatus="xt">
                                    <tr>
                                        <td>
                                             <input type="button" value="DELETE" name="delete" id="delete"  onclick="document.getElementById('helpIndex').value = '${xt.index}'; return triggerEvent('onPressButtonDelete');" />
                                              <form:input
													path="editForm.tempDescrDanJumlah[${xt.index}].descr"
													id="descr${xt.index}" size="65" disabled="${cmd.editForm.tempDescrDanJumlah[xt.index].disabled}" />
                                        </td>
                                        <td style="size: 5px;">
                                        <form:select path="editForm.tempDescrDanJumlah[${xt.index}].lkuId" 
                                        items="${cmd.editForm.lkuList}" 
                                        itemLabel="value" itemValue="key"/>
                                        </td>
                                        <td>                        
                                         <form:input
													path="editForm.tempDescrDanJumlah[${xt.index}].jumlahPremi"
													id="jumlahPremi${xt.index}"
													onfocus=" showForm( 'totalSumInsuredHelper${xt.index}', 'true' );"
													onblur="showForm( 'totalSumInsuredHelper${xt.index}', 'false' );"
													onchange="this.value=formatCurrency( this.value );"
													onkeyup="showFormatCurrency('totalSumInsuredHelper${xt.index}', this.value);"/>
											</td>
										 <td>                        
                                         <form:input
													path="editForm.tempDescrDanJumlah[${xt.index}].jumlahDebet"
													id="jumlahDebet${xt.index}"
													onfocus=" showForm( 'totalSumInsuredHelper${xt.index}', 'true' );"
													onblur="showForm( 'totalSumInsuredHelper${xt.index}', 'false' );"
													onchange="this.value=formatCurrency( this.value );"
													onkeyup="showFormatCurrency('totalSumInsuredHelper${xt.index}', this.value);"/>
											</td>
											                     <td>                        
                                         <form:input
													path="editForm.tempDescrDanJumlah[${xt.index}].jumlahKredit"
													id="jumlahKredit${xt.index}"
													onfocus=" showForm( 'totalSumInsuredHelper${xt.index}', 'true' );"
													onblur="showForm( 'totalSumInsuredHelper${xt.index}', 'false' );"
													onchange="this.value=formatCurrency( this.value );"
													onkeyup="showFormatCurrency('totalSumInsuredHelper${xt.index}', this.value);"/>
											</td>
											<td>
												<input type="text" id="totalSumInsuredHelper${xt.index}"
													disabled="disabled" style="display: none;" tabindex="-1"  />
												<input type="hidden" id="helpIndex" name="helpIndex" />
												
                                        </td>
                                        
                                    </tr>
                                </c:forEach>
                                
                                     <c:forEach var="rincianPenarikan" items="${cmd.editForm.penarikanUlinkVOList}" varStatus="xt">
                                        <tr>
                              <td> 
                              &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;
                               ${cmd.editForm.penarikanUlinkVOList[0].deskripsiPenarikan}
                                </td>
                                 <td>
                                </td>
                                <td>
                                <form:input path="editForm.penarikanUlinkVOList[0].jumlah" id="jumlahPenarikan" disabled = 'true'/>

                                </td>
                               
                                <td>
                                <input type="text" disabled="disabled"/>
                                </td>
                                <td>
                                <input type="text" disabled="disabled"/>
                                </td>
                                <td>
                                </td>
                            </tr> 
                                     
                                     </c:forEach>
                         <tr>
                              <td> 
                              &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;
                                BIAYA ADMINISTRASI PEMBATALAN POLIS
                                </td>
                                <td>
                                </td>
                                                                <td>
                                <input type="text" disabled="disabled"/>
                                </td>
                                <td>
                                <input type="text" disabled="disabled"/>
                                </td>
                                <td>
                                         <form:input path="editForm.biayaAdmin" id="biayaAdmin" disabled = 'true'/>
                                </td>
                                <td>
                                </td>
                            </tr> 
                            
                            <c:if test="${cmd.editForm.biayaMedis != '' && cmd.editForm.biayaMedis != null}">
                      <tr>
                        
                              <td> 
                                
                              &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;
                                BIAYA MEDIS
                                
                                </td>
                                                                <td>
                                </td>
                                                                <td>
                                <input type="text" disabled="disabled"/>
                                </td>
                                <td>
                                <input type="text" disabled="disabled"/>
                                </td>
                                <td>
                                         <form:input path="editForm.biayaMedis" id="biayaMedis" disabled = 'true'/>
                                </td>
                                <td>
                                </td>
                            </tr> 
                          
                          </c:if>
                          
                          <c:if test="${cmd.editForm.biayaLain != '' && cmd.editForm.biayaLain != null}">
                      <tr>
                              <td> 
                              &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;
                                ${cmd.editForm.biayaLainDescr}
                                
                                </td>
                                                                <td>
                                </td>
                                                                <td>
                                <input type="text" disabled="disabled"  />
                                </td>
                                <td>
                                <input type="text" disabled="disabled" />
                                </td>
                                <td>
                                         <form:input path="editForm.biayaLain" id="biayaLain" disabled = 'true'/>
                                </td>
                                <td>
                                </td>
                            </tr> 
                          
                          </c:if>
                          
            <tr>
                              <td> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;
                                PREMI DIKEMBALIKAN
                                </td>
                                                                <td>
                                </td>
                                <td>
                                <input type="text" disabled="disabled"/>
                                </td>
                                <td>
                                <input type="text" disabled="disabled"/>
                                </td>
                                <td>
                               	 <form:input path="editForm.premiDikembalikan" id="premiDikembalikan" disabled = 'true'/>
                                </td>
                                <td>
                                <input type="button" value="HITUNG" onclick="return triggerEvent('onPressButtonCount')"/> 
                                </td>
                            </tr> 
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="text-align: center;">
                                        <br/>

                                        <input type="button" name="limit" value="Back"
                                               onclick="return triggerEvent('onPressButtonBack');"
                                               accesskey="B"
                                               onmouseover="return overlib('Alt-B > Back', AUTOSTATUS, WRAP);"
                                               onmouseout="nd();">
                                               
                             		 <input type="button" name="limit" value="Next"
                                       onclick="return triggerEvent('onPressButtonNext');"
                                       accesskey="N"
                                       onmouseover="return overlib('Alt-N > Next to Preview Document', AUTOSTATUS, WRAP);"
                                       onmouseout="nd();"
                                        >
                                               

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    </table>

                </form:form>
			</div>
		</div>

	</div>

    <script type="text/javascript">
        if( '' != '${pageMessage}' )
        {
            alert( '${pageMessage}');
        }
    </script>

    <script type="text/javascript">
        var downloadFlag = document.getElementById('downloadFlag').value;
        if( downloadFlag == 'newPage' )
        {
            document.getElementById('downloadFlag').value = '';
            document.getElementById('formpost').target = '_blank';
            submitForm( STD_DOWNLOAD_JSP );
            document.getElementById('formpost').target = '_self';
            submitForm( PREVIEW_LAMP_1_JSP );
        }
        if( downloadFlag == 'samePage' )
        {
            document.getElementById('downloadFlag').value = '';
            document.getElementById('formpost').action = '${downloadUrlSession}';
            document.getElementById('formpost').submit();
            document.getElementById('formpost').target = '_self';
        }
         setDisabledBgColor();
    </script>
</body>
<%@ include file="/include/page/footer.jsp"%>