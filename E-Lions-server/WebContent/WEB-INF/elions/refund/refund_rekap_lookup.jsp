<%--
/**********************************************************************
 * Program History
 *
 * Project Name      	: E-Lions
 * Function Id         	: 
 * Program Name   		: refund_lookup
 * Description         	:
 * Environment      	: Java  1.5.0_06
 * Author               : samuel
 * Version              : 1.0
 * Creation Date    	: Oct 27, 2008 4:27:49 PM
 *
 * Update history   Re-fix date      Person in charge      Description
 *
 *
 *
 * Copyright(C) 2008-Asuransi Jiwa Sinarmas. All Rights Reserved.
 ***********************************************************************/
--%>

<%@ include file="/include/page/header.jsp"%>

<style type="text/css">
    html, body {
    }

</style>

<script type="text/javascript" src="${path }/include/js/refund/refund_common.js"></script>
<script type="text/javascript">

    function triggerEvent( theEvent )
    {
        document.forms[ 0 ].theEvent.value = theEvent;
        checkEvent();
    }

    function checkEvent()
    {
        var theEvent = document.forms[ 0 ].theEvent.value;
        if( theEvent == 'onPressLinkGotoPage'                
                || theEvent == 'onChangeNoOfRowsPerPage'
                || theEvent == 'onPressLinkFirst'
                || theEvent == 'onPressLinkPrev'
                || theEvent == 'onPressLinkNext'
                || theEvent == 'onPressLinkLast'
                )
        {
            document.getElementById('btnEnter').disabled = true;

            submitForm(REFUND_REKAP_LOOKUP_JSP);
        }
        else if( theEvent == 'onPressLinkSearch')
        {
        	if( document.getElementById('lookupForm.tglKirimDokFisikFrom').value == '' || document.getElementById('lookupForm.tglKirimDokFisikFrom').value == null)
        	{
        		alert(' TGL. KIRIM FROM harus diisi ');
        	}
        	else if( document.getElementById('lookupForm.tglKirimDokFisikTo').value == '' || document.getElementById('lookupForm.tglKirimDokFisikTo').value == null)
        	{
 		       	alert(' TGL. KIRIM TO harus diisi ');
        	}
        	else
        	{
	            document.getElementById('btnEnter').disabled = true;
	            submitForm(REFUND_REKAP_LOOKUP_JSP);
            }
        }
        else if( theEvent == 'onPressLinkSuratRekapKeAccFinance' )
        {
            if( document.getElementById('lookupForm.tglKirimDokFisikFrom').value == '' || document.getElementById('lookupForm.tglKirimDokFisikFrom').value == null)
        	{
        		alert(' TGL. KIRIM FROM harus diisi ');
        	}
        	else if( document.getElementById('lookupForm.tglKirimDokFisikTo').value == '' || document.getElementById('lookupForm.tglKirimDokFisikTo').value == null)
        	{
 		       	alert(' TGL. KIRIM TO harus diisi ');
        	}
        	else
        	{
        	submitForm(REFUND_REKAP_LOOKUP_JSP);
        	}
        }
        else if( theEvent == 'onPressButtonBack' )
        {
        	submitForm(REFUND_LOOK_UP);
        }
        
    }

    function onEnter()
    {
        if( event.keyCode == 13 )
        {
            document.getElementById('btnEnter').click();
        }
    }
      function check()
    {
    	triggerEvent( 'onPressLinkSuratRekapKeAccFinance' );
    }
  
</script>

<body style="height: 100%;" onload="setupPanes('container1', 'tab1'); ">

<div class="tab-container" id="container1">

<ul class="tabs">
    <li>
        <a href="#" onClick="return showPane('pane1', this)" id="tab1">Pembatalan Polis --> View</a>
    </li>
</ul>

<div class="tab-panes">
<div id="pane1" class="panes" style="">
<form:form id="formpost" commandName="cmd" name="">

            <table style="width: 720px;">
                <tr>
                    <td colspan="5">&nbsp;</td>
                </tr>
               
                <tr>
                    <td style="text-align: right; font-size: 10px; font-family: sans-serif;">
                        Tgl. Kirim From
                    </td>
                    <td>
                      <spring:bind path="cmd.lookupForm.tglKirimDokFisikFrom" >
        		            <script>inputDate('${status.expression}', '${status.value}', false);</script>
                        </spring:bind>
                    </td>
                    <td style="text-align: right; font-size: 10px; font-family: sans-serif;">
                        Tgl. Kirim To
                    </td>
                    <td>
                    <spring:bind path="cmd.lookupForm.tglKirimDokFisikTo" >
        		            <script>inputDate('${status.expression}', '${status.value}', false);</script>
                        </spring:bind>
                    </td>
                    <td><a href="#" id="btnEnter" onclick="triggerEvent( 'onPressLinkSearch' );"> Next</a></td>
                </tr>
            </table>

             <table style="width: 880px;">
                <tr>
                    <td colspan="7">
                        <hr width="100%"/>
                    </td>
               </tr>
               </table>
               
                <table>
                <tr >
                    <td colspan="5">
                        <input type="text" id="textMsg" name="textMsg" style="font-weight: bold; border: thin; width: 15cm; background-color: #F7F7F7;" disabled="disabled" />
                    </td>
                    <td>
                      <a href="#" onclick="check();">Cetak Surat Rekap</a>
                      &nbsp; &nbsp;
                      <a href="#" onclick="triggerEvent( 'onPressButtonBack' );" > Back </a>
                    </td>
                </tr>
                <tr style="background-color: burlywood;">                  
                    <td style="text-align: center; width: 100px; font-weight: bold;">
                        No. Reg SPAJ
                    </td>
                    <td style="text-align: center; width: 100px; font-weight: bold;">
                        No. Polis
                    </td>
                    <td style="text-align: center; width: 110px; font-weight: bold;">
                        Tindakan
                    </td>
                    <td style="text-align: center; width: 100px; font-weight: bold;">
                        Posisi
                    </td>
                       <td style="text-align: center; width: 120px; font-weight: bold;">
                        Tgl Kirim Dok. Fisik
                    </td>
                    <td style="text-align: center; width: 150px; font-weight: bold; width: 170;">
                        Last Update
                    </td>
                </tr>
                <c:set var="backGroundColor" value="#EAEAEA"/>
                <c:forEach var="refundViewVO" items="${resultList}" varStatus="xt">

                    <c:choose>
                        <c:when test='${"#EAEAEA" == backGroundColor}'>
                            <c:set var="backGroundColor" value="#FFFFFF"/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="backGroundColor" value="#EAEAEA"/>
                        </c:otherwise>
                    </c:choose>


                    <tr style="background-color: ${backGroundColor};"
                        onMouseOver="change_bgcolor( this, '#FFD6CA' );"
                        onMouseOut="change_bgcolor(this, '${backGroundColor}');"
                            >

                        <td style="text-align: center;">
                                ${refundViewVO.spajLabel}
                        </td>
                        <td style="text-align: center;">
                                ${refundViewVO.polisLabel}
                        </td>
                        <td style="text-align: left;">
                                ${refundViewVO.tindakan}
                        </td>
                        <td style="text-align: left;" title="${refundViewVO.deskripsi}">
                                ${refundViewVO.label}
                        </td>
                            <td style="text-align: left;">
                                ${refundViewVO.tglKirimDokFisikLabel}
                        </td>
                        <td style="text-align: left;">
                                ${refundViewVO.lastUpdate} - ${refundViewVO.updateWho}
                        </td>
                    </tr>
                </c:forEach>
                <tr style="background-color: tan;">
                    <td colspan="3" style="text-align:left;">
                        page ${cmd.lookupForm.currentPage} of ${cmd.lookupForm.totalOfPages}
                    </td>
                    <td colspan="5">
                        <span style="float:right;">
                            <a href="#" onclick="triggerEvent( 'onPressLinkGotoPage' );">goto page</a>
                            <form:input path="lookupForm.gotoPage" maxlength="4" size="4" cssStyle="width: 30px;height: 17px; font-size: 10px; font-family: sans-serif;" onkeypress="onEnter();"/>
                            <span style="display: ${cmd.lookupForm.linkFirstDisplay};">
                                <a href="#" onclick="triggerEvent( 'onPressLinkFirst' );">first</a>
                            </span>
                            <span style="display: ${cmd.lookupForm.linkPrevDisplay};">
                                <a href="#" onclick="triggerEvent( 'onPressLinkPrev' );">prev</a>
                            </span>
                            <span style="display: ${cmd.lookupForm.linkNextDisplay};">
                                <a href="#" onclick="triggerEvent( 'onPressLinkNext' );">next</a>
                            </span>
                            <span style="display: ${cmd.lookupForm.linkLastDisplay};">
                                <a href="#" onclick="triggerEvent( 'onPressLinkLast' );">last</a>
                            </span>
                        </span>
                    </td>
                </tr>
            </table>

            <spring:hasBindErrors name="cmd">
				<div class="errorBox">
					Harap lengkapi informasi berikut:<br/>
					<form:errors path="*" />
				</div>
			</spring:hasBindErrors>

            <br/>
            <c:forEach var="msg" items="${pageMessageList}" varStatus="xt">
                    <span style="vertical-align: text-top; color:brown;">
                            ${msg}
                    </span>
                <br/>
            </c:forEach>

        <input type="hidden" id="targetParam"/>
        <input type="hidden" name="theEvent" id="theEvent"/>
        <input type="hidden" name="isCheckEvent" id="isCheckEvent"/>
        <form:hidden path="lookupForm.selectedRowCd" />
        <form:hidden path="lookupForm.selectedRowNewSpaj" />
        <form:hidden path="lookupForm.selectedRowTindakan" />
        <form:hidden path="lookupForm.selectedUpdateWho" />
        <form:hidden path="lookupForm.downloadFlag" id="downloadFlag"/>

    </form:form>

</div>
</div>
</div>

    <script type="text/javascript">
        if( '' != '${pageMessage}' )
        {
            alert( '${pageMessage}');
        }
    </script>

    <script type="text/javascript">
        var downloadFlag = document.getElementById('downloadFlag').value;
        if( downloadFlag == 'newPage' )
        {
            document.getElementById('downloadFlag').value = '';
            document.getElementById('formpost').target = '_blank';
            submitForm( STD_DOWNLOAD_JSP );
            document.getElementById('formpost').target = '_self';
            submitForm( REFUND_REKAP_LOOKUP_JSP );
        }
        if( downloadFlag == 'samePage' )
        {
            document.getElementById('downloadFlag').value = '';
            document.getElementById('formpost').action = '${downloadUrlSession}';
            document.getElementById('formpost').submit();
            document.getElementById('formpost').target = '_self';
        }
        setDisabledBgColor();
    </script>
</body>
<%@ include file="/include/page/footer.jsp"%>