<%--
/**********************************************************************
 * Program History
 *
 * Project Name      	: E-Lions
 * Function Id         	: 
 * Program Name   		: preview_instruksi_redempt.jsp
 * Description         	:
 * Environment      	: Java  1.5.0_06
 * Author               : samuel
 * Version              : 1.0
 * Creation Date    	: Oct 24, 2008 4:01:18 PM
 *
 * Update history   Re-fix date      Person in charge      Description
 *
 *
 *
 * Copyright(C) 2008-Asuransi Jiwa Sinarmas. All Rights Reserved.
 ***********************************************************************/
--%>

<%@ include file="/include/page/header.jsp"%>

<style type="text/css">
    html, body {
    }

</style>

<script type="text/javascript" src="${path }/include/js/refund/refund_common.js"></script>
<script type="text/javascript">
    
    function checkEvent( event )
    {
        document.formpost.theEvent.value = event;

        if( event == 'onPressButtonViewAttachment' || event == 'onPressButtonViewAttachmentExcel' )
        {
            submitForm( PREVIEW_INSTRUKSI_REDEMPT_JSP );
        }
        else if( event == 'onPressButtonKirim' )
        {
            submitForm( PREVIEW_INSTRUKSI_REDEMPT_JSP );
        }
        else if( event == 'onPressButtonBack' )
        {
        	if( document.getElementById('currentPage').value == PREVIEW_LAMP_1_JSP )
        	{
        	  submitForm( PREVIEW_LAMP_1_JSP );
        	}
        	else if ( document.getElementById('currentPage').value == PREVIEW_LAMP_3_JSP )
        	{
        	  submitForm( PREVIEW_LAMP_3_JSP );
        	}
        }
    }

</script>

<body style="" onload="setupPanes('container1', 'tab1'); ">

    <div class="tab-container" id="container1">

        <ul class="tabs">
            <li>
                <a href="#" onClick="return showPane('pane1', this)" id="tab1">Pembatalan Polis --> Preview Refund Premi --> Preview Instruksi Redempt</a>
            </li>
        </ul>

        <div class="tab-panes">
            <div id="pane1" class="panes" style="">
                <form:form id="formpost" commandName="cmd" name="">

                    <input type="hidden" name="theEvent" id="theEvent"/>
                    <form:hidden path="redemptForm.downloadFlag" id="downloadFlag"/>
                    <form:hidden path="redemptForm.currentPage" id="currentPage"/>

                    <table style="width: 570px;" class="viewTable">
                    <tr><td>
                        <input type="text" id="textMsg" name="textMsg" style="font-weight: bold; border: thin; width: 15cm; background-color: #F7F7F7;" disabled="disabled" />
                    </td></tr>
                    <tr>
                        <td style="text-align: left; text-transform: none;">
                            <b>PT ASURANSI JIWA SINARMAS MSIG</b>
                        </td>
                    </tr>
                    <tr><td style="text-align: left; text-transform: none;">&nbsp;</td></tr>
                    <tr>
                        <td style="text-align: left; text-transform: none;">
                            Laporan kustodian Per KREDIT (INTERNAL - to Finance)
                        </td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr>
                        <td>
                            <table style="width: 500px;">
                                <tr>
                                    <td style="text-align: left; width: 10%; text-transform: none;">
                                        No.
                                    </td>
                                    <td style="text-align: left; width: 30%; text-transform: none;">
                                        Policy No
                                    </td>
                                    <td style="text-align: left; width: 30%; text-transform: none;">
                                        Deskripsi
                                    </td>
                                    <td style="text-align: left; width: 30%; text-transform: none;">
                                        Unit
                                    </td>
                                </tr>


                                <c:forEach var="rincianMap" items="${cmd.refundDocumentVO.params.dataSource}" varStatus="xt">
                                    <tr><td colspan="4">&nbsp;</td></tr>
                                    <tr>
                                        <td colspan="4" style="text-align: left; text-transform: none; text-decoration: underline; font-weight: bold;">
                                            ${rincianMap.ljiInvest}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; text-transform: none;">
                                            ${rincianMap.no}
                                        </td>
                                        <td style="text-align: left; text-transform: none;">
                                            ${rincianMap.policyNo}
                                        </td>
                                        <td style="text-align: left; text-transform: none;">
                                            ${rincianMap.description}
                                        </td>
                                        <td style="text-align: left; text-transform: none;">
                                            ${rincianMap.unit}
                                        </td>
                                    </tr>
                                    <tr><td colspan="4">&nbsp;</td></tr>
                                    <tr>
                                        <td colspan="3" style="text-align: left; text-transform: none; font-weight: bold;">
                                            Total per Investasi
                                        </td>
                                        <td style="text-align: left; text-transform: none; font-weight: bold;">
                                            ${rincianMap.unit}
                                        </td>
                                    </tr>
                                    <tr><td colspan="4">&nbsp;</td></tr>
                                </c:forEach>

                                

                                <tr>
                                    <td colspan="4" style="text-align: left; text-transform: none; font-weight: bold;">
                                        Note
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="text-align: left; text-transform: none; font-weight: bold;">
                                        ${cmd.refundDocumentVO.params.note}<br/>
                                       ${cmd.refundDocumentVO.params.bankName}<br/>
                                        ${cmd.refundDocumentVO.params.bankAccount}<br/>
                                    </td>
                                </tr>
                                <tr><td colspan="4">&nbsp;</td></tr>
                                <tr>
                                    <td colspan="2" style="text-align: left; text-transform: none; font-weight: bold;">
                                        Hormat Kami,
                                    </td>
                                    <td colspan="2" style="text-align: left; text-transform: none; font-weight: bold;">
                                        Mengetahui
                                    </td>
                                </tr>
                                <tr><td colspan="4">&nbsp;</td></tr>
                                <tr><td colspan="4">&nbsp;</td></tr>
                                <tr>
                                    <td colspan="2" style="text-align: left; text-transform: none; font-weight: bold;">
										${cmd.redemptForm.namaUnderwriter}
                                    </td>
                                    <td colspan="2" style="text-align: left; text-transform: none; font-weight: bold;">
                                        ( V. Inge )
                                    </td>
                                </tr>
                                <tr><td colspan="4">&nbsp;<hr/></td></tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="text-align: center;">
                                        <br/>
                                        <input type="button" name="limit" value="View Attachment PDF"
                                               onclick="return checkEvent('onPressButtonViewAttachment');"
                                               accesskey="P"
                                               onmouseover="return overlib('Alt-P > Print Dokumen', AUTOSTATUS, WRAP);"
                                               onmouseout="nd();"
                                               style="display: ${cmd.redemptForm.buttonViewAttachmentDisplay}"
                                               ${cmd.redemptForm.buttonViewAttachmentIsDisabled}
                                                >
                                          <input type="button" name="limit" value="View Attachment Excel"
                                               onclick="return checkEvent('onPressButtonViewAttachmentExcel');"
                                               accesskey="P"
                                               onmouseover="return overlib('Alt-P > Print Dokumen', AUTOSTATUS, WRAP);"
                                               onmouseout="nd();"
                                               style="display: ${cmd.redemptForm.buttonViewAttachmentExcelDisplay}"
                                               ${cmd.redemptForm.buttonViewAttachmentExcelIsDisabled}
                                                >
                                          <input type="button" name="limit" value="Kirim"
                                               onclick="return checkEvent('onPressButtonKirim');"
                                               accesskey="P"
                                               onmouseover="return overlib('Alt-B > Kirim Surat Instruksi Redempt ke Finance', AUTOSTATUS, WRAP);"
                                               onmouseout="nd();"
                                               style="display: ${cmd.redemptForm.buttonKirimDisplay}"
                                               ${cmd.redemptForm.buttonKirimIsDisabled}
                                                 >
                                         <input type="button" name="limit" value="Back"
                                               onclick="return checkEvent('onPressButtonBack');"
                                               accesskey="P"
                                               onmouseover="return overlib('Alt-B > Batalkan SPAJ', AUTOSTATUS, WRAP);"
                                               onmouseout="nd();">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    </table>

                </form:form>
			</div>
		</div>

	</div>

    <script type="text/javascript">
        if( '' != '${pageMessage}' )
        {
            alert( '${pageMessage}');
        }
        if( '' != '${errMessageFailed}' )
        {
            alert( '${errMessageFailed}');
        }
    </script>

    <script type="text/javascript">
        var downloadFlag = document.getElementById('downloadFlag').value;
        if( downloadFlag == 'newPage' )
        {
            document.getElementById('downloadFlag').value = '';
            document.getElementById('formpost').target = '_blank';
            submitForm( STD_DOWNLOAD_JSP );
            document.getElementById('formpost').target = '_self';
            submitForm( PREVIEW_INSTRUKSI_REDEMPT_JSP );
        }
        if( downloadFlag == 'samePage' )
        {
            document.getElementById('downloadFlag').value = '';
            document.getElementById('formpost').action = '${downloadUrlSession}';
            document.getElementById('formpost').submit();
            document.getElementById('formpost').target = '_self';
        }
         setDisabledBgColor();
    </script>
</body>
<%@ include file="/include/page/footer.jsp"%>