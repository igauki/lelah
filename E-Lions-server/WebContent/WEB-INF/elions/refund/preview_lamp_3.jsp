<%--
/**********************************************************************
 * Program History
 *
 * Project Name      	: E-Lions
 * Function Id         	: 
 * Program Name   		: preview_lamp_3
 * Description         	:
 * Environment      	: Java  1.5.0_06
 * Author               : samuel
 * Version              : 1.0
 * Creation Date    	: Oct 23, 2008 4:40:44 PM
 *
 * Update history   Re-fix date      Person in charge      Description
 *
 *
 *
 * Copyright(C) 2008-Asuransi Jiwa Sinarmas. All Rights Reserved.
 ***********************************************************************/
--%>

<%@ include file="/include/page/header.jsp"%>

<style type="text/css">
    html, body {
    }

</style>

<script type="text/javascript" src="${path }/include/js/refund/refund_common.js"></script>
<script type="text/javascript">

    function triggerEvent( theEvent )
    {
        document.forms[ 0 ].theEvent.value = theEvent;
        checkEvent();
    }

    function checkEvent( event )
    {
        var theEvent = document.forms[ 0 ].theEvent.value;

        if( theEvent == 'onPressButtonViewAttachment' )
        {
            submitForm( PREVIEW_LAMP_3_JSP );
        }
        else if( theEvent == 'onPressButtonSaveDraft' )
        {
            var isOk = window.confirm( "Anda yakin untuk save draft?" )
            if( isOk )submitForm( PREVIEW_LAMP_3_JSP );
        }
        else if( theEvent == 'onPressAccBatalSpaj' )
        {
            submitForm( PREVIEW_LAMP_3_JSP );
        }
        else if( theEvent == 'onPressButtonBatalkanSpaj' )
        {
            submitForm( PREVIEW_LAMP_3_JSP );
        }
        else if( theEvent == 'onPressButtonBack' )
        {
            submitForm( REFUND_EDIT );
        }
        else if( theEvent == 'onPressButtonPreviewIntruksiRedempt' )
        {
            submitForm( PREVIEW_INSTRUKSI_REDEMPT_JSP );
        }
    }
</script>

<body style="" onload="setupPanes('container1', 'tab1'); ">

    <div class="tab-container" id="container1">

        <ul class="tabs">
            <li>
                <a href="#" onClick="return showPane('pane1', this)" id="tab1">Pembatalan Polis --> Preview Dokumen</a>
            </li>
        </ul>

        <div class="tab-panes">
            <div id="pane1" class="panes" style="">
                <form:form id="formpost" commandName="cmd" name="">

                    <input type="hidden" name="theEvent" id="theEvent"/>
                    <form:hidden path="lamp3Form.downloadFlag" id="downloadFlag"/>

                    <table style="width: 570px;" class="viewTable">
	                    <tr><td><input type="text" id="textMsg" name="textMsg" style="font-weight: bold; border: thin; width: 15cm; background-color: #F7F7F7;" disabled="disabled" /></td></tr>
	                    <tr>
	                        <td style="text-align: center;"><b>PT ASURANSI JIWA SINARMAS MSIG</b><br/><br/></td>
	                    </tr>
	                    <tr>
	                        <td>
	                            <table>
	                               <tr>
	                                    <td style="text-align: left; width: 25%; text-transform: none;">
	                                        No Surat
	                                    </td>
	                                    <td style="text-align: left; width: 75%; text-transform: none;">
	                                        : ${cmd.refundDocumentVO.params.noSurat}
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td style="text-align: left; width: 25%; text-transform: none;">
	                                       Kepada
	                                    </td>
	                                    <td style="text-align: left; width: 75%; text-transform: none;">
	                                        : Bagian Keuangan
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td style="text-align: left; text-transform: none;">
	                                        Dari
	                                    </td>
	                                    <td style="text-align: left; text-transform: none;">
	                                        : Bagian Underwriting
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td style="text-align: left; text-transform: none;">
	                                        No. urut memo
	                                    </td>
	                                    <td style="text-align: left; text-transform: none;">
	                                        : ${cmd.refundDocumentVO.params.noUrutMemo}
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td style="text-align: left; text-transform: none;">
	                                        Hal
	                                    </td>
	                                    <td style="text-align: left; text-transform: none;">
	                                        : ${cmd.refundDocumentVO.params.hal}
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td style="text-align: left; text-transform: none;">
	                                        Tanggal
	                                    </td>
	                                    <td style="text-align: left; text-transform: none;">
	                                        : ${cmd.refundDocumentVO.params.tanggal}
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td style="text-align: left; text-transform: none;">
	                                        Cc
	                                    </td>
	                                    <td style="text-align: left; text-transform: none;">
	                                        : Accounting, Agen, Cabang
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td style="text-align: left; text-transform: none;">
	                                        Hal
	                                    </td>
	                                    <td style="text-align: left; text-transform: none;">
	                                        : <b>${cmd.refundDocumentVO.params.keterangan}
	                                        </b>
	                                    </td>
	                                </tr>
	                            </table>
	                        </td>
	                    </tr>
	
	                    <tr>
	                        <td>
	                            <hr/>
	                        </td>
	                    </tr>
	
	                    <tr>
	                        <td>
	                            <table>
	                                <tr>
	                                    <td style="text-align: left; width: 50%; text-transform: none;">
	                                         No. Reg SPAJ
	                                    </td>
	                                    <td style="text-align: left; width: 50%; text-transform: none;">
	                                        : ${cmd.refundDocumentVO.params.spajNo}
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td style="text-align: left; text-transform: none;">
	                                        Polis No.
	                                    </td>
	                                    <td style="text-align: left; text-transform: none;">
	                                        : ${cmd.refundDocumentVO.params.policyNo}
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td style="text-align: left; text-transform: none;">
	                                        Produk
	                                    </td>
	                                    <td style="text-align: left; text-transform: none;">
	                                        : ${cmd.refundDocumentVO.params.productName}
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td style="text-align: left; text-transform: none;">
	                                        Pemegang Polis
	                                    </td>
	                                    <td style="text-align: left; text-transform: none;">
	                                        : ${cmd.refundDocumentVO.params.policyHolderName}
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td style="text-align: left; text-transform: none;">
	                                        Tertanggung
	                                    </td>
	                                    <td style="text-align: left; text-transform: none;">
	                                        : ${cmd.refundDocumentVO.params.insuredName}
	                                    </td>
	                                </tr>
	                                  
	                            </table>
	                        </td>
	                    </tr>
	
	                    <tr>
	                        <td>
	                            <table>
	                                <tr>
	                                    <td style="text-align: left; text-transform: none;">
	                                        <br/>
	                                            ${cmd.refundDocumentVO.params.statement}
	                                        <br/>
	                                        <br/>
	                                    </td>
	                                </tr>
	                            </table>
	                        </td>
	                    </tr>
	
	                    <tr>
	                        <td>
	                            <table>
	                                <c:forEach var="rincianMap" items="${cmd.refundDocumentVO.params.dataSource}" varStatus="xt">
	                                    <tr>
	                                        <td style="text-align: left; text-transform: none; width:80%;">
	                                                ${rincianMap.descr}
	                                        </td>
	                                        <td style="text-align: left; text-transform: none; width:20%;">
	                                                ${rincianMap.jumlah}
	                                        </td>
	                                    </tr>
	                                </c:forEach>
	                            </table>
	                        </td>
	                    </tr>
	
	                    <tr>
	                        <td>
	                            <table>
	                                <tr>
	                                    <td style="text-align: left; text-transform: none;">
	                                        Premi tersebut akan digunakan untuk No. Reg SPAJ: : ${cmd.refundDocumentVO.params.newSpajNo}
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td style="text-align: left; text-transform: none;">
	                                        atas nama Pemegang Polis/Tertanggung:
	                                        ${cmd.refundDocumentVO.params.newPolicyHolderName} / ${cmd.refundDocumentVO.params.newInsuredName}
	                                    </td>
	                                </tr>
	                            </table>
	                        </td>
	                    </tr>
	
	                    <tr>
	                        <td>
	                        	<c:if test="${cmd.refundDocumentVO.params.statementAvailableOrNot eq 'available'}">
		                            <table>
		                                <tr>
		                                    <td colspan="2" style="text-align: left; text-transform: none;">
		                                        Terlampir kami sampaikan:
		                                    </td>
		                                </tr>
		                                <c:if test="${cmd.refundDocumentVO.params.statementLamp1 != null}">
			                                <tr>
			                                    <td style="text-align: right; width: 10px;">
			                                        -
			                                    </td>
			                                    <td style="text-align: left; text-transform: none;">
			                                        ${cmd.refundDocumentVO.params.statementLamp1}
			                                    </td>
			                                </tr>
		                                </c:if>
		                                <c:if test="${cmd.refundDocumentVO.params.statementLamp2 != null}">
			                                <tr>
			                                    <td style="text-align: right; width: 10px;">
			                                        -
			                                    </td>
			                                    <td style="text-align: left; text-transform: none;">
			                                        ${cmd.refundDocumentVO.params.statementLamp2}
			                                    </td>
			                                </tr>
		                                </c:if>
		                                
		                              	<c:forEach var="addLampiranMap" items="${cmd.refundDocumentVO.params.dsAddLampiranRefund}" varStatus="xt">
		                                	<c:if test="${xt.index eq 0}">
		                                    	<tr>
		                                    		<td colspan="2" style="text-align: left; text-transform: none;"></td>
		                                		</tr>
		                                	</c:if>
		                                    <tr>
		                                        <td style="text-align: right; text-transform: none; width:3%;">
		                                               ${addLampiranMap.dashAdd}
		                                        </td>
		                                        <td style="text-align: left; text-transform: none; width:97%;">
		                                                ${addLampiranMap.contentAdd}
		                                        </td>
		                                    </tr>
		                                </c:forEach>
		                            </table>
	                            </c:if>
	                        </td>
	                    </tr>
	
	                    <tr>
	                        <td>
	                            <table>
	                                <tr>
	                                    <td style="text-align: left; text-transform: none;">
	                                        Demikian kami sampaikan, atas bantuan dan kerjasamanya kami ucapkan terima kasih
	                                        <br/><br/>
	                                        Jakarta, ${cmd.refundDocumentVO.params.tanggal}
	                                        <br/><br/>
                                        	<!-- helpdesk [143790] perubahan tanda tangan suryanto lim, sistikarsinah, inge ke anna yulia -->
	                                        <%-- ${cmd.refundDocumentVO.params.signer} --%>
                                        	<fmt:bundle basename="ekalife"><fmt:message key="printing.namaRefundPremi"></fmt:message></fmt:bundle>
	                                        <br/>
                                        	<!-- helpdesk [143790] perubahan tanda tangan suryanto lim, sistikarsinah, inge ke anna yulia -->
	                                        <!-- Underwriting Department -->
                                        	<fmt:bundle basename="ekalife"><fmt:message key="printing.JabatanRefundPremi"></fmt:message></fmt:bundle>
	                                    </td>
	                                </tr>
	                            </table>
	                        </td>
	                    </tr>
	
	                    <tr>
	                        <td>
	                            <table>
	                                <tr>
	                                    <td style="text-align: center;">
	                                        <br/>
											<input type="button" name="limit" value="Save Draft"
	                                               onclick="return triggerEvent('onPressButtonSaveDraft');"
	                                               accesskey="S" onmouseover="return overlib('Alt-S > Save Draft', AUTOSTATUS, WRAP);"
	                                               onmouseout="nd();"
	                                               ${cmd.lamp3Form.buttonSaveDraftIsDisabled}
	                                               >
	                                        <input type="button" name="limit" value="Preview Redempt"
	                                               onclick="return triggerEvent('onPressButtonPreviewIntruksiRedempt');"
	                                               accesskey="P" onmouseover="return overlib('Alt-B > Batalkan SPAJ', AUTOSTATUS, WRAP);" 
	                                               onmouseout="nd();"
	                                               style="display: ${cmd.lamp3Form.buttonPreviewRedemptDisplay}"
	                                               >
	                                        <input type="button" name="limit" value="Transfer Ke Akseptasi Pembatalan"
	                                               onclick="return triggerEvent('onPressAccBatalSpaj');"
	                                               accesskey="A" onmouseover="return overlib('Alt-A > Aksep Pembatalan', AUTOSTATUS, WRAP);"
	                                               onmouseout="nd();"
	                                               ${cmd.lamp3Form.buttonAccBatalSpajIsDisabled}
	                                               >
	                                        <input type="button" name="limit" value="Batalkan SPAJ"
	                                               onclick="return triggerEvent('onPressButtonBatalkanSpaj');"
	                                               accesskey="T" onmouseover="return overlib('Alt-T > Batalkan SPAJ', AUTOSTATUS, WRAP);"
	                                               onmouseout="nd();"
	                                               ${cmd.lamp3Form.buttonBatalkanSpajIsDisabled}
	                                               >
	                                        <input type="button" name="limit" value="View Attachment"
	                                               onclick="return triggerEvent('onPressButtonViewAttachment');"
	                                               accesskey="V"
	                                               onmouseover="return overlib('Alt-V > View Attachment', AUTOSTATUS, WRAP);"
	                                               onmouseout="nd();"
	                                               ${cmd.lamp3Form.buttonViewAttachmentIsDisabled}
	                                               >
											<input type="button" name="limit" value="Back"
	                                               onclick="return triggerEvent('onPressButtonBack');"
	                                               accesskey="B"
	                                               onmouseover="return overlib('Alt-B > Kembali ke halaman sebelumnya', AUTOSTATUS, WRAP);"
	                                               onmouseout="nd();"
	                                               >
	                                    </td>
	                                </tr>
	                            </table>
	                        </td>
	                    </tr>
					</table>
                </form:form>
			</div>
		</div>
	</div>

    <script type="text/javascript">
        if( '' != '${pageMessage}' )
        {
            alert( '${pageMessage}');
        }
        if( '' != '${errMessageFailed}' )
        {
            alert( '${errMessageFailed}');
        }
    </script>

    <script type="text/javascript">
        var downloadFlag = document.getElementById('downloadFlag').value;
        if( downloadFlag == 'newPage' )
        {
            document.getElementById('downloadFlag').value = '';
            document.getElementById('formpost').target = '_blank';
            submitForm( STD_DOWNLOAD_JSP );
            document.getElementById('formpost').target = '_self';
            submitForm( PREVIEW_LAMP_3_JSP );
        }
        if( downloadFlag == 'samePage' )
        {
            document.getElementById('downloadFlag').value = '';
            document.getElementById('formpost').action = '${downloadUrlSession}';
            document.getElementById('formpost').submit();
            document.getElementById('formpost').target = '_self';
        }
         setDisabledBgColor();
    </script>
</body>
<%@ include file="/include/page/footer.jsp"%>