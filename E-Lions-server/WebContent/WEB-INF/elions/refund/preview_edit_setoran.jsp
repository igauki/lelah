<%--
/**********************************************************************
 * Program History
 *
 * Project Name      	: E-Lions
 * Function Id         	:
 * Program Name   		: preview_lamp_1
 * Description         	:
 * Environment      	: Java  1.5.0_06
 * Author               : samuel
 * Version              : 1.0
 * Creation Date    	: Oct 13, 2008 4:58:36 PM
 *
 * Update history   Re-fix date      Person in charge      Description
 *
 *
 *
 * Copyright(C) 2008-Asuransi Jiwa Sinarmas. All Rights Reserved.
 ***********************************************************************/
--%>

<%@ include file="/include/page/header.jsp"%>

<style type="text/css">
    html, body {
    }

</style>

<script type="text/javascript" src="${path }/include/js/refund/refund_common.js"></script>
<script type="text/javascript">

    function triggerEvent( theEvent )
    {
        document.forms[ 0 ].theEvent.value = theEvent;
        checkEvent();
    }

    function checkEvent()
    {
        var theEvent = document.forms[ 0 ].theEvent.value;


        if( theEvent == 'onPressButtonNext' )
        {
            submitForm( PREVIEW_LAMP_1_JSP );
        }
        else if( theEvent == 'onPressButtonBack' )
        {
			submitForm( REFUND_EDIT );
        }
    }

</script>

<body style="" onload="setupPanes('container1', 'tab1'); ">

    <div class="tab-container" id="container1">

        <ul class="tabs">
            <li>
                <a href="#" onClick="return showPane('pane1', this)" id="tab1">Pembatalan Polis --> Preview Refund Premi</a>
            </li>
        </ul>

        <div class="tab-panes">
            <div id="pane1" class="panes" style="">
                <form:form id="formpost" commandName="cmd" name="">

                    <input type="hidden" name="theEvent" id="theEvent"/>
                    <form:hidden path="lamp1Form.downloadFlag" id="downloadFlag"/>
                    <form:hidden path="editForm.tindakanCd" id="editForm.tindakanCd"/>
                 	<form:hidden path="editForm.statusUnit" id="statusUnitId"/>
                    <form:hidden path="editForm.posisiNo" id="posisiNo"/>

                    <table style="width: 760px;" class="viewTable">
                    <tr><td><input type="text" id="textMsg" name="textMsg" style="font-weight: bold; border: thin; width: 15cm; background-color: #F7F7F7;" disabled="disabled" /></td></tr>
                    <tr>
                        <td style="text-align: center;"><b>PT ASURANSI JIWA SINARMAS MSIG</b><br/><br/></td>
                    </tr>
                 

                    <tr>
                        <td>
                            <hr/>
                        </td>
                    </tr>

        

                    <tr>
                        <td>
                            <table width="100%">
                                <c:forEach var="detailSetoran" items="${cmd.editForm.setoranPokokAndTopUp}" varStatus="xt">
                                    <tr>
                                       <td style="text-align: left;" width="55%">
                                        <form:input path="editForm.setoranPokokAndTopUp[${xt.index}].descrPokok"
										id="descrPokok${xt.index}" size="80" disabled="${cmd.editForm.setoranPokokAndTopUp[xt.index].isDisabled}" readonly="true"/>
										</td>
										 <td style="text-align: left;" width="45%">
										 <form:input path="editForm.setoranPokokAndTopUp[${xt.index}].jumlahPokok"
											id="jumlahPokok${xt.index}" size="20" onfocus=" showForm( 'jumlahPokokHelper${xt.index}', 'true' );"
											onblur="showForm( 'jumlahPokokHelper${xt.index}', 'false' );"
											onchange="this.value=formatCurrency( this.value );"
											onkeyup="showFormatCurrency('jumlahPokokHelper${xt.index}', this.value);"
											disabled="${cmd.editForm.setoranPokokAndTopUp[xt.index].isDisabled}"/>
											
                                      <input type="text" id="jumlahPokokHelper${xt.index}"
											disabled="disabled" style="display: none;" tabindex="-1"  />
											<input type="hidden" id="helpIndex" name="helpIndex" />
											
									<form:errors path="editForm.setoranPokokAndTopUp[${xt.index }].jumlahPokok" cssStyle="color: red;"/>
											</td>
                                    </tr>
                                    <tr>
                                       <td>
                                        <form:input path="editForm.setoranPokokAndTopUp[${xt.index}].descrTopUp"
										id="descrTopUp${xt.index}" size="80" disabled="${cmd.editForm.setoranPokokAndTopUp[xt.index].isDisabled}" readonly="true"/>
                                       </td>
                                        <td>
                                           <form:input path="editForm.setoranPokokAndTopUp[${xt.index}].jumlahTopUp"
											id="jumlahTopUp${xt.index}" size="20" onfocus=" showForm( 'jumlahTopUpHelper${xt.index}', 'true' );"
											onblur="showForm( 'jumlahTopUpHelper${xt.index}', 'false' );"
											onchange="this.value=formatCurrency( this.value );"
											onkeyup="showFormatCurrency('jumlahTopUpHelper${xt.index}', this.value);"
											disabled="${cmd.editForm.setoranPokokAndTopUp[xt.index].isDisabled}"/>
                                      
                                      <input type="text" id="jumlahTopUpHelper${xt.index}"
											disabled="disabled" style="display: none;" tabindex="-1"  />
											<input type="hidden" id="helpIndex" name="helpIndex" />
											
									<form:errors path="editForm.setoranPokokAndTopUp[${xt.index }].jumlahTopUp" cssStyle="color: red;"/>
											</td>
                                    </tr>
                                    <tr>
                                  	  <td width="55%">
                                   		 <hr/>
                                   	  </td>
                                      <td width="45%">
                                      	 <hr/>
                                      </td>
                                    </tr>
                                </c:forEach>
                                <tr>
                                <td>
                                </td>
                                    <td colspan="2" style="text-align: left; text-transform: none; font-weight: bold;">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    
                    <tr>
                        <td>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                        </td>
                    </tr>
                    
                    
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="text-align: center;">
                                        <br/>

                                        <input type="button" name="limit" value="Back"
                                               onclick="return triggerEvent('onPressButtonBack');"
                                               accesskey="B"
                                               onmouseover="return overlib('Alt-B > Back', AUTOSTATUS, WRAP);"
                                               onmouseout="nd();">
                                               
                                      <input type="button" name="limit" value="Next"
                                       onclick="return triggerEvent('onPressButtonNext');"
                                       accesskey="N"
                                       onmouseover="return overlib('Alt-N > Next to Preview Document', AUTOSTATUS, WRAP);"
                                       onmouseout="nd();"
                                        >
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    </table>

                </form:form>
			</div>
		</div>

	</div>

    <script type="text/javascript">
        if( '' != '${pageMessage}' )
        {
            alert( '${pageMessage}');
        }
    </script>

    <script type="text/javascript">
        var downloadFlag = document.getElementById('downloadFlag').value;
        if( downloadFlag == 'newPage' )
        {
            document.getElementById('downloadFlag').value = '';
            document.getElementById('formpost').target = '_blank';
            submitForm( STD_DOWNLOAD_JSP );
            document.getElementById('formpost').target = '_self';
            submitForm( PREVIEW_LAMP_1_JSP );
        }
        if( downloadFlag == 'samePage' )
        {
            document.getElementById('downloadFlag').value = '';
            document.getElementById('formpost').action = '${downloadUrlSession}';
            document.getElementById('formpost').submit();
            document.getElementById('formpost').target = '_self';
        }
         setDisabledBgColor();
    </script>
    <script type="text/javascript">
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		</script>
		<script type="text/javascript">
		try {
		var pageTracker = _gat._getTracker("UA-8107264-1");
		pageTracker._trackPageview();
		} catch(err) {}</script>
</body>
<%@ include file="/include/page/footer.jsp"%>