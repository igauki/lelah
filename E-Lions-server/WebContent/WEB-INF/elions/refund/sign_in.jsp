<%--
/**********************************************************************
 * Program History
 *
 * Project Name      	: E-Proposal
 * Function Id         	: 
 * Program Name   		: JobStreet
 * Description         	:
 * Environment      	: Java  1.5.0_06
 * Author               : samuel
 * Version              : 1.0
 * Creation Date    	: Dec 2, 2008 4:08:41 PM
 *
 * Update history   Re-fix date      Person in charge      Description
 *
 *
 *
 * Copyright(C) 2008-Asuransi Jiwa Sinarmas. All Rights Reserved.
 ***********************************************************************/
--%>

<%@ include file="/include/page/header.jsp"%>

<style type="text/css">
    html, body {
    }

</style>

<script type="text/javascript" src="${path }/include/js/refund/refund_common.js"></script>
<script type="text/javascript">
	function triggerEvent( theEvent )
    {
        document.forms[ 0 ].theEvent.value = theEvent;
        checkEvent();
    }

    function checkEvent()
    {
        var theEvent = document.forms[ 0 ].theEvent.value;
        if( theEvent == 'onPressButtonSignIn' )
        {
            submitForm( REFUND_LOOK_UP );
        }
        else if( theEvent == 'onPressButtonCancel' )
        {
            submitForm( REFUND_LOOK_UP );
        }
    }

    function onEnter()
    {
        if( event.keyCode == 13 )
        {
//            document.getElementById('btnEnter').click();
            triggerEvent('onPressButtonSignIn');
        }
    }

</script>

<body style="height: 100%;" onload="setupPanes('container1', 'tab1'); ">

<div class="tab-container" id="container1">

<ul class="tabs">
    <li>
        <a href="#" onClick="return showPane('pane1', this)" id="tab1">Pembatalan Polis --> View --> Sign In</a>
    </li>
</ul>

<div class="tab-panes">
<div id="pane1" class="panes">
<form:form id="formpost" commandName="cmd" name="">

            <table style="width: 620px;">
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>

                <tr>
                    <td style="text-align: right; font-size: 10px; font-family: sans-serif;">
                        Password
                    </td>
                    <td>
                        <form:password path="signInForm.password" cssStyle="height: 17px; font-size: 10px; font-family: sans-serif;" onkeypress="if(event.keyCode==13) {return false;}"/>
                        <form:errors path="signInForm.password" cssStyle="height: 17px; font-size: 10px; font-family: sans-serif; color: red;"/>
                    </td>
                    <td></td>
                </tr>

                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>

                <tr>
                    <td colspan="3" style="text-align: center;"> 
                        <input type="button" name="btnEnterName" value="Sign In"
                                        onclick="return triggerEvent('onPressButtonSignIn');"
                                        accesskey="S" onmouseover="return overlib('Alt-S > Sign In', AUTOSTATUS, WRAP);"
                                        onmouseout="nd();"
                                        id="btnEnter"
                                >
                        <input type="button" name="limit" value="Cancel"
                                        onclick="return triggerEvent('onPressButtonSignIn');"
                                        accesskey="C" onmouseover="return overlib('Alt-C > Cancel', AUTOSTATUS, WRAP);"
                                        onmouseout="nd();"
                                                >
                    </td>
                </tr>

            </table>

        <input type="hidden" id="targetParam"/>
        <input type="hidden" name="theEvent" id="theEvent"/>

    </form:form>

</div>
</div>
</div>

    <script type="text/javascript">
        if( '' != '${pageMessage}' )
        {
            alert( '${pageMessage}');
        }
        setDisabledBgColor();
    </script>
</body>
<%@ include file="/include/page/footer.jsp"%>