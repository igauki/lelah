<%--
/**********************************************************************
 * Program History
 *
 * Project Name      	: E-Lions
 * Function Id         	: 
 * Program Name   		: refund_lookup
 * Description         	:
 * Environment      	: Java  1.5.0_06
 * Author               : samuel
 * Version              : 1.0
 * Creation Date    	: Oct 27, 2008 4:27:49 PM
 *
 * Update history   Re-fix date      Person in charge      Description
 *
 *
 *
 * Copyright(C) 2008-Asuransi Jiwa Sinarmas. All Rights Reserved.
 ***********************************************************************/
--%>

<%@ include file="/include/page/header.jsp"%>

<style type="text/css">
    html, body {
    }

</style>

<script type="text/javascript" src="${path }/include/js/refund/refund_common.js"></script>
<script type="text/javascript">

    function triggerEvent( theEvent )
    {
        document.forms[ 0 ].theEvent.value = theEvent;
        checkEvent();
    }

    function checkEvent()
    {
        var theEvent = document.forms[ 0 ].theEvent.value;
        if( theEvent == 'onPressLinkAdd'
                || theEvent == 'onPressImageEdit' )
        {
            submitForm(REFUND_EDIT);
        }
        else if( theEvent == 'onPressLinkSignIn' )
        {
            submitForm(SIGN_IN_JSP);
        }
        else if( theEvent == 'onPressImageSuratBatal' ||  theEvent == 'onPressBatalSpaj' || theEvent == 'onPressMoveToMstDetRefundLamp' )
        {
            submitForm(REFUND_LOOK_UP);
        }
       /* else if( theEvent == 'onPressLinkSuratRekap' )
        {
         	submitForm(REFUND_LOOK_UP);
        }
        */
        else if( theEvent == 'onPressCreatRekapLink' )
        {
         	submitForm(REFUND_REKAP_LOOKUP_JSP);
        }
        else if( theEvent == 'onPressImageSuratRedempt' )
        {
            submitForm(REFUND_LOOK_UP);
        }
        else if( theEvent == 'onPressLinkClear' )
        {
            document.getElementById('lookupForm.spajFilter').value = '';
            document.getElementById('lookupForm.posisiFilter').value = '';
            document.getElementById('_lookupForm.updateFromFilter').value = '';
            document.getElementById('_lookupForm.updateToFilter').value = '';
            setSelectListToValue('', 'lookupForm.tindakanFilterCd');
        }
        else if( theEvent == 'onPressLinkGotoPage'
                || theEvent == 'onPressLinkSearch'
                || theEvent == 'onChangeNoOfRowsPerPage'
                || theEvent == 'onPressLinkFirst'
                || theEvent == 'onPressLinkPrev'
                || theEvent == 'onPressLinkNext'
                || theEvent == 'onPressLinkLast'
                )
        {
            document.getElementById('btnEnter').disabled = true;

            submitForm(REFUND_LOOK_UP);
        }
        else if( theEvent == 'onPressAcceptBatalSpaj' )
        {
            submitForm(REFUND_LOOKUP_AKSEPTASI_JSP);
        }
        else if( theEvent == 'onPressImageDelete' )
        {
            submitForm(REFUND_LOOK_UP);
        }
    }

    function doView( rowCd )
    {
        document.getElementById('lookupForm.selectedRowCd').value = rowCd;
        triggerEvent('onPressImageView');
    }

    function doEdit( rowCd, newSpaj, tindakan, updateWho )
    {
        document.getElementById('lookupForm.selectedRowCd').value = rowCd;
        document.getElementById('lookupForm.selectedRowNewSpaj').value = newSpaj;
        document.getElementById('lookupForm.selectedRowTindakan').value = tindakan;
        document.getElementById('lookupForm.selectedUpdateWho').value = updateWho;
        triggerEvent('onPressImageEdit');
    }
    
    function doDelete( rowCd, newSpaj, tindakan, updateWho )
    {
        if(confirm("Apakah anda yakin akan menghapus draft pembatalan ini?")){
	        document.getElementById('lookupForm.selectedRowCd').value = rowCd;
	        document.getElementById('lookupForm.selectedRowNewSpaj').value = newSpaj;
	        document.getElementById('lookupForm.selectedRowTindakan').value = tindakan;
	        document.getElementById('lookupForm.selectedUpdateWho').value = updateWho;
	        triggerEvent('onPressImageDelete');
        }
    }

    function doViewSuratBatal( rowCd, newSpaj, tindakan, updateWho )
    {
        document.getElementById('lookupForm.selectedRowCd').value = rowCd;
        document.getElementById('lookupForm.selectedRowNewSpaj').value = newSpaj;
        document.getElementById('lookupForm.selectedRowTindakan').value = tindakan;
        document.getElementById('lookupForm.selectedUpdateWho').value = updateWho;
        triggerEvent('onPressImageSuratBatal');
    }

    function doViewSuratRedempt( rowCd, newSpaj, tindakan, updateWho )
    {
        document.getElementById('lookupForm.selectedRowCd').value = rowCd;
        document.getElementById('lookupForm.selectedRowNewSpaj').value = newSpaj;
        document.getElementById('lookupForm.selectedRowTindakan').value = tindakan;
        document.getElementById('lookupForm.selectedUpdateWho').value = updateWho;
        triggerEvent('onPressImageSuratRedempt');
    }

    function onEnter()
    {
        if( event.keyCode == 13 )
        {
            document.getElementById('btnEnter').click();
        }
    }
      function check()
    {
    if( document.getElementById('lookupForm.tglKirimDokFisikFrom').value == '' && document.getElementById('lookupForm.tglKirimDokFisikTo').value =='' )
    {
    	alert('TGL KIRIM DOK.FISIK harus diisi terlebih dahulu');
    }
    else
    {
    	triggerEvent( 'onPressLinkSuratRekapKeAccFinance' );
    }
      
    }
  
</script>

<body style="height: 100%;" onload="setupPanes('container1', 'tab1'); ">

<div class="tab-container" id="container1">

<ul class="tabs">
    <li>
        <a href="#" onClick="return showPane('pane1', this)" id="tab1">Pembatalan Polis --> View</a>
    </li>
</ul>

<div class="tab-panes">
<div id="pane1" class="panes" style="">
<form:form id="formpost" commandName="cmd" name="">

            <table style="width: 800px;">
                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: right; font-size: 10px; font-family: sans-serif;">
                        No. Reg SPAJ
                    </td>
                    <td>
                        <form:input path="lookupForm.spajFilter" cssStyle="height: 17px; font-size: 10px; font-family: sans-serif;" onkeypress="onEnter();"/>
                        <form:errors path="lookupForm.spajFilter" cssStyle="height: 17px; font-size: 10px; font-family: sans-serif; color: red;"/>
                    </td>
                    <td style="text-align: right; font-size: 10px; font-family: sans-serif;">
                        Records/page
                    </td>
                    <td>
                        <form:select path="lookupForm.noOfRowsPerPage"
                                     items="${cmd.lookupForm.noOfRowsPerPageList}"
                                     itemLabel="value" itemValue="key"
                                     cssStyle="height: 17px; font-size: 10px; font-family: sans-serif;"
                                     onchange="triggerEvent( 'onChangeNoOfRowsPerPage' );"
                                />
                    </td>
                    <td><a href="#" id="btnEnter" onclick="triggerEvent( 'onPressLinkSearch' );">search</a></td>
                </tr>
                <tr>
                    <td style="text-align: right; font-size: 10px; font-family: sans-serif;">
                        Tindakan
                    </td>
                    <td>
                        <form:select path="lookupForm.tindakanFilterCd"
                                     items="${cmd.lookupForm.tindakanFilterList}"
                                     itemLabel="value" itemValue="key"
                                     cssStyle="height: 17px; font-size: 10px; font-family: sans-serif;"
                                />
                    </td>
                    <td style="text-align: right; font-size: 10px; font-family: sans-serif;">
                        Posisi
                    </td>
                    <td>
                        <form:input path="lookupForm.posisiFilter" cssStyle="height: 17px; font-size: 10px; font-family: sans-serif;" onkeypress="onEnter();"/>
                        <form:errors path="lookupForm.posisiFilter" cssStyle="height: 17px; font-size: 10px; font-family: sans-serif; color: red;"/>
                    </td>
                    <td><a href="#" onclick="triggerEvent( 'onPressLinkClear' );">clear</a></td>
                </tr>
                <tr>
                    <td style="text-align: right; font-size: 10px; font-family: sans-serif;">
                        Last Update by
                    </td>
                    <td>
                        <form:input path="lookupForm.lastUpdateFilter" cssStyle="height: 17px; font-size: 10px; font-family: sans-serif;" onkeypress="onEnter();"/>
                        <form:errors path="lookupForm.lastUpdateFilter" cssStyle="height: 17px; font-size: 10px; font-family: sans-serif; color: red;"/>
                    </td>
                    <td style="text-align: right; font-size: 10px; font-family: sans-serif;">
                        No. Polis
                    </td>
                    <td>
                        <form:input path="lookupForm.polisFilter" cssStyle="height: 17px; font-size: 10px; font-family: sans-serif;" onkeypress="onEnter();"/>
                        <form:errors path="lookupForm.polisFilter" cssStyle="height: 17px; font-size: 10px; font-family: sans-serif; color: red;"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td style="text-align: right; font-size: 10px; font-family: sans-serif;">
                        Update From
                    </td>
                    <td>
                        <spring:bind path="cmd.lookupForm.updateFromFilter" >
        		            <script>inputDate('${status.expression}', '${status.value}', false);</script>
                        </spring:bind>
                    </td>
                    <td style="text-align: right; font-size: 10px; font-family: sans-serif;">
                        Update To
                    </td>
                    <td>
                        <spring:bind path="cmd.lookupForm.updateToFilter" >
        		            <script>inputDate('${status.expression}', '${status.value}', false);</script>
                        </spring:bind>
                    </td>
                    <td></td>
                </tr>
            </table>

             <table style="width: 1160px;">
                <tr>
                    <td colspan="8">
                        <hr width="100%"/>
                    </td>
               </tr>               
               </table>
               
                <table>
                <tr >
                    <td colspan="6">
                        <input type="text" id="textMsg" name="textMsg" style="font-weight: bold; border: thin; width: 15cm; background-color: #F7F7F7;" disabled="disabled" />
                    </td>
                    <td>
                    	<a href="#" onclick="triggerEvent( 'onPressAcceptBatalSpaj' );" style="display: ${cmd.lookupForm.acceptBatalDisplay}">Akseptasi Pembatalan</a>
                    </td>
                    <td>
                    	<a href="#" onclick="triggerEvent( 'onPressBatalSpaj' );" style="display: ${cmd.lookupForm.batalSpajDisplay}">Batal Otomatis</a>
                    </td>
                    <td>
                   		<a href="#" onclick="triggerEvent( 'onPressCreatRekapLink' );" style="display: ${cmd.lookupForm.linkAddDisplay}" >Membuat Rekap Ke Finance</a>
                    	&nbsp;
                                      
                        <a href="#" onclick="triggerEvent( 'onPressLinkAdd' );" style="display: ${cmd.lookupForm.linkAddDisplay}">add new</a>
                        <a href="#" onclick="triggerEvent( 'onPressLinkSignIn' );" style="display: ${cmd.lookupForm.linkSignInDisplay}">sign in to get access</a>
                    </td>
                </tr>
                <tr style="background-color: burlywood;">
                    <td style="text-align: center; width: 30px;">
                        &nbsp;
                    </td>
                    <td style="text-align: center; width: 100px; font-weight: bold;">
                        No. Reg SPAJ
                    </td>
                    <td style="text-align: center; width: 100px; font-weight: bold;">
                        No. Polis
                    </td>
                    <td style="text-align: center; width: 200px; font-weight: bold;">
                        Alasan
                    </td>
                    <td style="text-align: center; width: 110px; font-weight: bold;">
                        Tindakan
                    </td>
                    <td style="text-align: center; width: 100px; font-weight: bold;">
                        Posisi
                    </td>
                       <td style="text-align: center; width: 120px; font-weight: bold;">
                        Tgl Kirim Dok. Fisik
                    </td>
                     <td style="text-align: center; width: 100px; font-weight: bold;">
                        Cancel User
                    </td>
                    <td style="text-align: center; width: 130px; font-weight: bold;">
                        Last Update
                    </td>
                </tr>
                <c:set var="editDisplay" value=""/>
                <c:choose>
                    <c:when test="${cmd.signInForm.signIn=='true'}">
                        <c:set var="editDisplay" value=""/>
                    </c:when>
                    <c:otherwise>
                        <c:set var="editDisplay" value="none"/>
                    </c:otherwise>
                </c:choose>

                <c:set var="backGroundColor" value="#EAEAEA"/>
                <c:forEach var="refundViewVO" items="${resultList}" varStatus="xt">

                    <c:choose>
                        <c:when test='${"#EAEAEA" == backGroundColor}'>
                            <c:set var="backGroundColor" value="#FFFFFF"/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="backGroundColor" value="#EAEAEA"/>
                        </c:otherwise>
                    </c:choose>
                
	                <c:set var="deleteDisplay" value=""/>
	                <c:choose>
	                    <c:when test="${refundViewVO.aksesHapusDraft=='true' && cmd.signInForm.signIn=='true' && cmd.signInForm.superuser=='true'}">
	                        <c:set var="deleteDisplay" value=""/>
	                    </c:when>
	                    <c:otherwise>
	                        <c:set var="deleteDisplay" value="none"/>
	                    </c:otherwise>
	                </c:choose>

                    <c:set var="suratBatalDisplay" value=""/>
                    <c:choose>
                        <c:when test="${refundViewVO.suratBatalExist=='true' && cmd.signInForm.signIn=='true'}">
                            <c:set var="suratBatalDisplay" value=""/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="suratBatalDisplay" value="none"/>
                        </c:otherwise>
                    </c:choose>

                    <c:set var="suratRedemptDisplay" value=""/>
                    <c:choose>
                        <c:when test="${refundViewVO.suratRedemptExist=='true' && cmd.signInForm.signIn=='true'}">
                            <c:set var="suratRedemptDisplay" value=""/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="suratRedemptDisplay" value="none"/>
                        </c:otherwise>
                    </c:choose>

                    <tr style="background-color: ${backGroundColor};"
                        onMouseOver="change_bgcolor( this, '#FFD6CA' );"
                        onMouseOut="change_bgcolor(this, '${backGroundColor}');"
                            >

                        <c:set var="disabledAddition" value=""/>
                        <td onclick="document.getElementById( 'isCheckEvent' ).value = 'true';" style="text-align: left; vertical-align: text-top;">
                            <%--<a href="#" title="view" onclick="doView( '${refundViewVO.spaj}' );"><img style="border: 0" src="${path}/include/image/view_tts.gif" alt="view"/></a>--%>
                            <a href="#" title="view" onclick="doEdit( '${refundViewVO.spaj}','${refundViewVO.newSpaj}','${refundViewVO.tindakanCd}','${refundViewVO.updateWho}' );" style="display: ${editDisplay};"><img style="border: 0" src="${path}/include/image/edit.gif" alt="edit" title="Edit/View"/></a>
                            <a href="#" title="view" onclick="doDelete( '${refundViewVO.spaj}','${refundViewVO.newSpaj}','${refundViewVO.tindakanCd}','${refundViewVO.updateWho}' );" style="display: ${deleteDisplay};"><img style="border: 0" src="${path}/include/image/delete.gif" alt="delete" title="Delete"/></a>
                            <a href="#" title="view" onclick="doViewSuratRedempt( '${refundViewVO.spaj}','${refundViewVO.newSpaj}','${refundViewVO.tindakanCd}','${refundViewVO.updateWho}' );" style="display: ${suratRedemptDisplay};"><img style="border: 0" src="${path}/include/image/page.gif" alt="document pdf" title="View Surat Redempt"/></a>
                            <a href="#" title="view" onclick="doViewSuratBatal( '${refundViewVO.spaj}','${refundViewVO.newSpaj}','${refundViewVO.tindakanCd}','${refundViewVO.updateWho}' );" style="display: ${suratBatalDisplay};"><img style="border: 0" src="${path}/include/image/page.gif" alt="document pdf" title="View Surat Batal"/></a>
                        </td>
                        <td style="text-align: center;">
                                ${refundViewVO.spajLabel}
                        </td>
                        <td style="text-align: center;">
                                ${refundViewVO.polisLabel}
                        </td>
                         <td style="text-align: left; width: 200px; overflow: hidden;" >
                        		${refundViewVO.alasan}
                        </td>
                        <td style="text-align: left;">
                                ${refundViewVO.tindakan}
                        </td>
                        <td style="text-align: left;" title="${refundViewVO.deskripsi}">
                                ${refundViewVO.label}
                        </td>
                            <td style="text-align: left;">
                                ${refundViewVO.tglKirimDokFisikLabel}
                        </td>
                       <td style="text-align: left;">
                                ${refundViewVO.cancelFullName}
                        </td>
                        <td style="text-align: left;">
                                ${refundViewVO.lastUpdate} - ${refundViewVO.updateWho}
                        </td>
                    </tr>
                </c:forEach>
                <tr style="background-color: tan;">
                    <td colspan="4" style="text-align:left;">
                        page ${cmd.lookupForm.currentPage} of ${cmd.lookupForm.totalOfPages}
                    </td>
                    <td colspan="6">
                        <span style="float:right;">
                            <a href="#" onclick="triggerEvent( 'onPressLinkGotoPage' );">goto page</a>
                            <form:input path="lookupForm.gotoPage" maxlength="4" size="4" cssStyle="width: 30px;height: 17px; font-size: 10px; font-family: sans-serif;" onkeypress="onEnter();"/>
                            <span style="display: ${cmd.lookupForm.linkFirstDisplay};">
                                <a href="#" onclick="triggerEvent( 'onPressLinkFirst' );">first</a>
                            </span>
                            <span style="display: ${cmd.lookupForm.linkPrevDisplay};">
                                <a href="#" onclick="triggerEvent( 'onPressLinkPrev' );">prev</a>
                            </span>
                            <span style="display: ${cmd.lookupForm.linkNextDisplay};">
                                <a href="#" onclick="triggerEvent( 'onPressLinkNext' );">next</a>
                            </span>
                            <span style="display: ${cmd.lookupForm.linkLastDisplay};">
                                <a href="#" onclick="triggerEvent( 'onPressLinkLast' );">last</a>
                            </span>
                        </span>
                    </td>
                </tr>
            </table>

            <spring:hasBindErrors name="cmd">
				<div class="errorBox">
					Harap lengkapi informasi berikut:<br/>
					<form:errors path="*" />
				</div>
			</spring:hasBindErrors>

            <br/>
            <c:forEach var="msg" items="${pageMessageList}" varStatus="xt">
                    <span style="vertical-align: text-top; color:brown;">
                            ${msg}
                    </span>
                <br/>
            </c:forEach>

        <input type="hidden" id="targetParam"/>
        <input type="hidden" name="theEvent" id="theEvent"/>
        <input type="hidden" name="isCheckEvent" id="isCheckEvent"/>
        <form:hidden path="lookupForm.selectedRowCd" />
        <form:hidden path="lookupForm.selectedRowNewSpaj" />
        <form:hidden path="lookupForm.selectedRowTindakan" />
        <form:hidden path="lookupForm.selectedUpdateWho" />
        <form:hidden path="lookupForm.downloadFlag" id="downloadFlag"/>

    </form:form>

</div>
</div>
</div>

    <script type="text/javascript">
        if( '' != '${pageMessage}' )
        {
            alert( '${pageMessage}');
        }
        if( '' != '${errMessageFailed}' )
        {
            alert( '${errMessageFailed}');
        }
        if( '' != '${errMessageAlreadySave}' )
        {
            alert( '${errMessageAlreadySave}');
        }
    </script>

    <script type="text/javascript">
        var downloadFlag = document.getElementById('downloadFlag').value;
        if( downloadFlag == 'newPage' )
        {
            document.getElementById('downloadFlag').value = '';
            document.getElementById('formpost').target = '_blank';
            submitForm( STD_DOWNLOAD_JSP );
            document.getElementById('formpost').target = '_self';
            submitForm( REFUND_LOOK_UP );
        }
        if( downloadFlag == 'samePage' )
        {
            document.getElementById('downloadFlag').value = '';
            document.getElementById('formpost').action = '${downloadUrlSession}';
            document.getElementById('formpost').submit();
            document.getElementById('formpost').target = '_self';
        }
        setDisabledBgColor();
    </script>
</body>
<%@ include file="/include/page/footer.jsp"%>