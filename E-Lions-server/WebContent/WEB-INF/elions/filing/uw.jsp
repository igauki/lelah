<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>
<script src="${path}/include/js/jquery-1.3.2.min.js"> 
</script>
<script>

	$(document).ready(function(){ 
	  $("#reg_spaj").focus(); 
	  $('#reg_spaj').keypress( function(e) {
	  $('#submitMode').val('cari');
	  	if (e.keycode==13) $('#formpost').submit();
	  });
	});
	
</script>

<body style="height: 100%;">
	<form:form id="formpost" name="formpost" commandName="cmd">
		<table class="entry2">
			<br/>
			<tr>
				<th colspan="2" style="font-size: 14pt;">FILING UNDERWRITING</th>
			</tr>
			<tr>
				<td colspan="2" >
					<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
								 Informasi:<br>
									<c:forEach var="error" items="${status.errorMessages}">
												 - <c:out value="${error}" escapeXml="false" />
										<br/>
									</c:forEach>
							</div>
						</c:if>									
					</spring:bind>
				</td>
			</tr>
			<tr>
				<th colspan="2"> Masukkan Nomor SPAJ/ Nomor polis : 
					&nbsp;
					<input id="reg_spaj" name="reg_spaj" onfocus="this.select();"  value="${cmd.filing.reg_spaj}"  /> 
					&nbsp;
					<input type="submit"  id="cari" name="cari" value="Cari" />
					<input type="hidden" id="submitMode" name="submitMode" />
				</th>
			</tr>
			<c:if test="${not empty cmd.filing}">
				<tr>
					<th width="50%" style="height: 100%;"> 
						<fieldset style="height: 80%;position: fixed;">
							<legend>Detail Informasi</legend>
							<table class="entry2" style="height: 100%;position: fixed;">
								<tr>
									<th align="left" width="50%">
										Nomor Spaj : ${cmd.filing.reg_spaj}
									</th>
									<th align="left" width="50%">
										UP : <fmt:formatNumber value="${cmd.filing.mspr_tsi}" type="currency" 
												currencySymbol="${cmd.filing.lku_symbol} " maxFractionDigits="2" minFractionDigits="0" />
									</th>	
								</tr>
								<tr>
									<th align="left" width="50%">Nomor Polis : ${cmd.filing.mspo_policy_no}</th>
									<th align="left" width="50%">Premi : <fmt:formatNumber value="${cmd.filing.mspr_premium}" type="currency" 
												currencySymbol="${cmd.filing.lku_symbol} " maxFractionDigits="2" minFractionDigits="0" /></th>
								</tr>
								<tr>
									<th align="left" width="50%">Nama Pemegang polis : ${cmd.filing.pemegang_polis}</th>
									<th align="left" width="50%">Tanggal Mulai : <fmt:formatDate value="${cmd.filing.mste_beg_date}" pattern="dd/MM/yyyy"/></th>
								</tr>
								<tr>
									<th align="left" width="50%">Nama Produk : ${cmd.filing.lsdbs_name}</th>
									<th align="left" width="50%">Tanggal Berakhir : <fmt:formatDate value="${cmd.filing.mste_end_date}" pattern="dd/MM/yyyy"/></th>
								</tr>
							</table>
						</fieldset>
					</th>
					<th width="50%" style="height: 100%;">
						<fieldset>
							<legend>Input Filing</legend>
							<table class="entry2">
								<tr>
									<th align="left" width="50%">
										Pilih Box : 
									</th>
									<th align="left" width="50%">
										<form:select id="box" path="filing.kd_box">
											<form:option label="" value=""/>
											<form:options items="${lstBox}" itemLabel="value" itemValue="key"/>
										</form:select>
										<br/>
									</th>
								</tr>
								<tr>
									<th align="left" width="50%">Nama Bundle :</th>
									<th align="left" width="50%"><form:input cssStyle="border-color:#6893B0;" id="nama_bundle" path="filing.nama_bundle" disabled="disabled" size="24"  /></th>
								</tr>
								<tr>
									<th align="left" width="50%">Status Bundle :</th>
									<th align="left" width="50%"><form:input cssStyle="border-color:#6893B0;" id="status_bundle" path="filing.sts_dokumen" disabled="disabled" size="24"  /></th>
								</tr>
								<tr>
									<th align="left" width="50%">Posisi Bundle :</th>
									<th align="left" width="50%"><form:input cssStyle="border-color:#6893B0;" id="posisi_bundle" path="filing.pos_dokumen" disabled="disabled" size="24"  /></th>
								</tr>
								<tr>
									<th align="left" width="50%">Tanggal Pemusnahan: </th>
									<th align="left" width="50%"><script>inputDate('tgl', '${cmd.filing.flag_tgl_destroyed}', false, '', 9);</script></th>
								</tr>
								<tr>
									<th align="left" width="50%">Created :</th>
									<th align="left" width="50%"><fmt:formatDate value="${cmd.filing.tgl_created}" pattern="dd/MM/yyyy"/></th>
								</tr>
								<tr>
									<th align="left" width="50%">Activated :</th>
									<th align="left" width="50%">&nbsp;<fmt:formatDate value="${cmd.filing.tgl_aktif}" pattern="dd/MM/yyyy"/></th>
								</tr>
								<tr>
									<th width="50%" colspan="2">
										<input type="submit" class="button" name="save" id="save" value="save" />
									</th>
								</tr>
							</table>
						</fieldset>
					</th>
				</tr>
				<tr>
					<c:if test="${submitSuccess eq true}">
						<script>
							alert("${cmd.filing.success}");
						</script>
					</c:if>
				</tr>
			</c:if>
		</table>
	</form:form>
	
</body>

<%@ include file="/include/page/footer.jsp"%>