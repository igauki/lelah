<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>

<script>
	function ubahPilih(tipe){
		if(tipe == 0){
		document.getElementById("gantung").style.visibility = "visible";
		document.getElementById("gantung").style.height = "0";
		document.getElementById("trans").style.visibility = "hidden"
		document.getElementById('titipan_gantung_yes').checked = true;
		}else if (tipe==1){
		document.getElementById("trans").style.visibility = "visible";
		document.getElementById("trans").style.height = "0";
		document.getElementById("gantung").style.visibility = "hidden";
		
		}
	}
	
	function pesan(pepesan){
		if(pepesan != ''){
			alert(pepesan);
		}
	}
	
	function awal(tipe){
		no=document.formpost.nomor.value;
		noOld=document.formpost.no_pre_old.value;
		pilih=document.getElementById('titipan_gantung_yes').checked;
		tanggal=document.formpost.tgl_input.value;
		mtb_gl_no=document.formpost.mtb_gl_no.value;
		var ttp_value=0;
		//alert(tanggal);
		//alert(mtb_gl_no);
		if(pilih){
			ttp_value=1
		}
		//alert(ttp_value +" "+ pilih);
		//alert(tipe);
		if(tipe==0){
			window.location='${path}/accounting/pre.htm?window=trans_pre&nomor='+no+'&nomorLama='+noOld+'&titipan='+ttp_value+'&flag=0';
		}else if(tipe==1){
			window.location='${path}/accounting/pre.htm?window=trans_pre&nomor='+no+'&tanggal='+tanggal+'&mtb_gl_no='+mtb_gl_no+'&flag=1';
		}else if(tipe==2){
			window.location='${path}/accounting/pre.htm?window=trans_pre&nomor='+no+'&tanggal='+tanggal+'&mtb_gl_no='+mtb_gl_no+'&flag=2';
		}
	}
</script>


<body style="height: 50%;" onload="pesan('${pesan}');">
	<form id="formpost" name="formpost" commandName="cmd" method="post">
		<table class="entry2">
			<tr>
				<th align="left" width="10%">
					<label for="pilihGantung">
					<input align="middle" id="pilihGantung" type="radio" name="pilih" value="0" class="noBorder" onclick="ubahPilih(0);"> Gantung</label>
				</th>
				<th align="left" width="10%">
				<label for="pilihTrans">
				<input align="middle" id="pilihTrans" type="radio" name="pilih" value="1" class="noBorder" onclick="ubahPilih(1);"> Trans</label>
				<input id="nomor" name="nomor" type="hidden" value="${nomor}">
				</th>
			</tr>
			<tr>
				<th>
					<div id="gantung" align="left" style="visibility: hidden; height: 0;">
						<fieldset style="width: 100%; ">
						<legend> Window Titipan</legend>
							<table class="entry2">
								<tr>
									<th align="left">
										Gantungan Pre Titipan
									</th>
									<th align="left">
										<label for="titipan_gantung_yes">
										<input id="titipan_gantung_yes" name="titipan_gantung" type="radio" value="1" class="noBorder" <c:if test="${cmd.titipan_gantung eq 1}">checked</c:if>>YES
										</label>
										
										<label for="titipan_gantung_no" >
										<input id="titipan_gantung_no" name="titipan_gantung" type="radio" value="0" class="noBorder" <c:if test="${cmd.titipan_gantung eq 0}">checked</c:if>>NO
										</label>
									</th>
								</tr>
								<tr>
									<th>
									</th>
									<th align="left">
										<input type="text" id="no_pre_old" name="no_pre_old" maxlength="9" size="15" />
									</th>
								</tr>
							</table>
							<input type="button" id="save" name="save" value="Save" onclick="awal(0)"></input>
						</fieldset>
					</div>
				</th>
				<th>
					<div id="trans" align="left" style="visibility: hidden; height: 0;">
						<fieldset style="width: 50%; ">
						<legend>Window Transfer</legend>
							<table class="entry2">
								<tr>
									<th>
										Masukkan Tanggal
									</th>
									<th align="left">
										<input id="tgl_input" name="tgl_input" value="${sysdate}" />
									</th>
								</tr>
								<tr>
									<th>
										Data Bank
									</th>
									<th align="left">
										<select name=mtb_gl_no>
											<c:forEach items="${lstBank}" var="x">
												<option value="${x.key}">${x.value}</option>
											</c:forEach>
										</select>
									</th>
								</tr>
							</table>
							<input type="button" id="Accounting" name="Accounting" value="Accounting" onclick="awal(1); document.getElementById('in').value = 'in'; "></input>
							<input type="button" id="BankBook" name="BankBook" value="BankBook" onclick="awal(2); document.getElementById('in').value = 'in'; " ></input>
						</fieldset>
					</div>
				</th>
			</tr>
		</table>
	</form>
	<input type="hidden" id="in" value="${in}" />
	<input type="hidden" id="no_voucher" value="${no_voucher}" />
	<script>
	var x = document.getElementById("in").value;
	var y = document.getElementById("no_voucher").value;
	//alert(x);
	if(x == 'in' ){
		window.parent.tes(y);
	}
	</script>
</body>
<%@ include file="/include/page/footer.jsp"%>