<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>

<script>
function pesan(){
	var pesan=document.formpost.pesan.value;
	if(pesan != ""){
		alert(pesan);
	}
}

function tampil(){
	no=parent.document.formpost.nomor.value;
	//alert(no);
	f=1;
	row=prompt("Masukkan row yang akan didelete  ","");
	//alert(row);
	if(row==null){
		alert("Tidak memasukkan row yang ingin didelete");
		f=0;
	}
	
	if(f==1){
		window.location='${path }/accounting/input_pre.htm?nomor='+no+'&flag=4&row='+row;
	}
}

</script>

<c:if test="${not empty pesan} ">
	<script type="text/javascript">
		alert('${pesan}');
	</script>
</c:if>

<BODY style="height: 100%;" onload="pesan();">
	<form:form id="formpost" name="formpost" commandName="cmd" method="post">
		<table class="entry2">
			<tr>
				<th width="50%" align="left">
					Input User : ${cmd.dbank.lus_login_name}
				</th>
				<th width="50%" align="left">
					   Debet    : <fmt:formatNumber value="${cmd.dbank.debet}" type="number"/>
				</th>
			</tr>
			<tr>
				<th width="50%" align="left">
					Input Date : <fmt:formatDate value="${cmd.dbank.tgl_input}" pattern="dd/MM/yyyy" />
				</th>
				<th width="50%" align="left">
					Kredit    : <fmt:formatNumber value="${cmd.dbank.kredit}" type="number"/>
				</th>
			</tr>
			<tr>
				<th width="50%" align="left">
					Voucher    : <fmt:formatNumber value="${cmd.dbank.listDBank[0].jumlah}" type="number"/>
				</th>
				<th width="50%" align="left">
					Balance : <fmt:formatNumber value="${cmd.dbank.balance}" type="number"/>
				</th>
			</tr>
		</table>
		<c:if test="${not empty cmd.dbank}">
			<table class="entry2">
				<thead>
					<tr>
						<th>
							No
						</th>
						<th>
							Keterangan
						</th>
						<th>
							Giro
						</th>
						<th>
							C/D
						</th>
						<th>
							Jumlah
						</th>
						<th>
							Flow
						</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${cmd.dbank.listDBank}" var="d" varStatus="st">
						<tr>
							<td>
								${st.count}. 
							</td>
							<td>
								<form:input id="keterangan" path="dbank.listDBank[${st.index}].keterangan" size="60" />
							</td>
							<td>
								<form:input cssStyle="text-align: center;" id="giro" path="dbank.listDBank[${st.index}].giro" size="10" />
							</td>
							<td>
								<form:select path="dbank.listDBank[${st.index}].kas" cssStyle="width: 180px;">
									<c:choose>
										<c:when test="${st.index eq 0}">
											<form:option label="" value="" />
											<form:options items="${lstKasRowFirst}" itemLabel="value" itemValue="key" />
										</c:when>
										<c:otherwise>
											<form:option label="" value="" />
											<form:options items="${lstKas}" itemLabel="value" itemValue="key" />
										</c:otherwise>
									</c:choose>
								</form:select>
							</td>
							<td> 
								<form:input cssStyle="text-align: right;" id="jumlah" path="dbank.listDBank[${st.index}].jumlah" size="20"/> 
							</td>
							<td>
								<c:choose>
									<c:when test="${st.index eq 0}">
										<input type="hidden" />
									</c:when>
									<c:otherwise>
										<form:select path="dbank.listDBank[${st.index}].kode_cash_flow">
											<form:option label="" value="" />
											<form:options items="${lstFlow}" itemLabel="value" itemValue="key" />
										</form:select>
									</c:otherwise>
								</c:choose>
							</td>
						</tr>	
					</c:forEach>
				</tbody>
			</table>
			<td colspan="4">
				<c:if test="${submitSuccess eq true}">
					<c:choose>
						<c:when test="${cmd.flagAdd eq 2}">
							<script>
								alert("Data Berhasil Disimpan");
								addOptionToSelect(parent.document, parent.document.formpost.nomor, '${cmd.dbank.no_pre}', '${cmd.dbank.no_pre}');
								window.location='${path }/accounting/input_pre.htm?nomor=${cmd.dbank.no_pre}&flag=3';
							</script>
						</c:when>
						<c:when test="${cmd.dbank.flag eq 1}">
							<script>
								alert("Data Berhasil Disimpan");
								addOptionToSelect(parent.document, parent.document.formpost.nomor, '${cmd.dbank.no_pre}', '${cmd.dbank.no_pre}');
								window.location='${path }/accounting/input_pre.htm?nomor=${cmd.dbank.no_pre}&flag=3';
							</script>	
						</c:when>
						<c:otherwise>
							<script>
								alert("Data Berhasil Diupdate");
								addOptionToSelect(parent.document, parent.document.formpost.nomor, '${cmd.dbank.listDBank[0].no_pre}', '${cmd.dbank.listDBank[0].no_pre}');
								window.location='${path }/accounting/input_pre.htm?nomor=${cmd.dbank.listDBank[0].no_pre}&flag=1';
							</script>
						</c:otherwise>
					</c:choose>
			       	<div id="success">
				       	 Berhasil
				   	</div>
		      	</c:if>	
		  		<spring:bind path="cmd.*">
					<c:if test="${not empty status.errorMessages}">
						<div id="error">
							 Informasi:<br>
								<c:forEach var="error" items="${status.errorMessages}">
											 - <c:out value="${error}" escapeXml="false" />
									<br/>
								</c:forEach>
						</div>
					</c:if>									
				</spring:bind>
			</td>
		</c:if>
		<table class="entry2">
			<tr>
				<th align="left">
					<input type="submit" id="save" name="save" value="save"></input>
					<input type="submit" id="addRow" name="addRow" value="addRow"></input>
					<input type="button" id="deleteRow" name="deleteRow" value="deleteRow" onclick="tampil();">
				</th>
			</tr>
		</table>
		
		<input type="hidden" value="${pesan}" name="pesan" />
	</form:form>
</BODY>

<%@ include file="/include/page/footer.jsp"%>