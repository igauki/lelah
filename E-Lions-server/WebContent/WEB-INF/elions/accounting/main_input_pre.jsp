<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>
<script>
	function tampil(flag){
		var noRef='';
		no=document.formpost.nomor.value;
		nocari=document.formpost.nomorcari.value;
				
		if(flag=='2'){//input
			document.getElementById('infoFrame').src='${path }/accounting/input_pre.htm?flag='+flag;
			document.formpost.nomor.value=0;
		}else if(flag=='0'){//show
			document.getElementById('infoFrame').src='${path }/accounting/input_pre.htm?nomor='+no+'&nomorcari='+nocari+'&flag='+flag;
		}else if(flag=='1'){//trans
			document.getElementById('infoFrame').src='${path}/accounting/pre.htm?window=trans_pre&nomor='+no;
		}else if(flag=='3'){
			window.location='${path}/accounting/pre.htm?window=main_input_pre&nomor='+no+'&flag='+flag;
		}	
		
	}
	
	function tes(no_voucher){//bagian ini untuk merefresh parent saat melakukan submit di iframe(childnya).Yang jadi child ada di halaman trans_pre.jsp
		if(no_voucher==""){
			window.location='${path}/accounting/pre.htm?window=main_input_pre';
		}else {
			window.location='${path}/accounting/pre.htm?window=main_input_pre&no_voucher='+no_voucher+'&flag=2';
	 	}
	}
	
	function prosesList(){
		no=document.formpost.nomor.value;
		//alert(no);
		window.location='${path}/accounting/pre.htm?window=main_input_pre&nomor='+no+'&flag=1';
	}
	
	function pesan(pepesan){
		if(pepesan != ''){
			alert(pepesan);
		}
	}
	
</script>


<body onload="setFrameSize('infoFrame', 45); pesan('${pesan}');" onresize="setFrameSize('infoFrame', 45);" style="height: 100%;" >

<form:form id="formpost" name="formpost" commandName="cmd">
	<table class="entry2" width="98%">
		<tr>
			<th>Nomor Pre</th>
			<td>
				<select onchange="prosesList();" id="nomor" name="nomor" >
					<c:forEach items="${lsNoPre}" var="x" varStatus="xt">
						<option value="${x.no_pre}">${x.no_pre}</option>
					</c:forEach>
<!--					<option value="0">NEW</option>-->
				</select>
<!--				<input type="button" style="visibility: hidden"  value="Cari" onClick="popWin('${path}/accounting/pre.htm?window=cari_pre', 500, 500);" />-->
				<input type="button" value="Insert" onClick="tampil('2');" />
				<input type="button" value="Trans" onclick="tampil('1');" />
				<input type="button" value="Delete" onclick="tampil('3');"/>
				<input type="hidden" value="${no_pre}" name="no_pre" id="no_pre" />
			</td>
		</tr>
		<tr>
			<th>Cari No Pre</th>
			<td>
				<input type="text" size="12" maxlength="9" name="nomorcari" id="nomorcari" value="${nomorcari}" />
				<input type="hidden" size="12" maxlength="9" name="nomortampung" id="nomortampung" value="${nomortampung}" />
				<input type="hidden" id="in" value="${in}" />
				<input type="button" value="Show" onClick="tampil('0');" />
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<c:if test=""></c:if>
				<iframe src="${path}/accounting/input_pre.htm?nomor=${nomor}&flag=0" name="infoFrame" id="infoFrame"
					width="100%"  > Please Wait... </iframe>
			</td>
		</tr>
	</table>
</form:form>
</body>
<script>
	var no_pre=document.getElementById('no_pre').value;
	//alert(no_pre);
	if(no_pre != "" ){
		document.getElementById('nomor').value = no_pre;	
	}
</script>

<%@ include file="/include/page/footer.jsp"%>