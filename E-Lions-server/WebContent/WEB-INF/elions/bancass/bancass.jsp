<%@ include file="/include/page/header.jsp"%>
<script>

	function showPage(hal){
		var spaj = document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value;
		var lsbs = document.formpost.kodebisnis.value;
		var lsdbs = document.formpost.numberbisnis.value;
		
		if(spaj == ''){
			alert('Harap pilih SPAJ terlebih dahulu!');
		}else{
			createLoadingMessage();
			if('tampil' == hal){
				document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj;
			}else if('checklist' == hal){
				document.getElementById('infoFrame').src='${path}/bancass/checklist.htm?lspd_id=2&reg_spaj='+spaj;
			}
		}
	}

	/*
	function simultan(){
		createLoadingMessage();
		var spaj=document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value
		document.getElementById('infoFrame').src='${path }/uw/simultan.htm?spaj='+spaj;
	}
	function reas(){
		createLoadingMessage();
		var spaj=document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value
		var result=confirm("Apakah Data Anda Sudah Benar ?\nCek Extra Premi, Medis dll..!", "U/W");
		if(result==true)
			document.getElementById('infoFrame').src='${path }/uw/reas.htm?spaj='+spaj;
	}
	*/
	
	function cariregion(spaj,nama){
		if(spaj != ''){
			if(document.getElementById('docFrame')) document.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+spaj;
			if(document.getElementById('infoFrame')) document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj;
	
			ajaxManager.listcollect(spaj , nama,
			{callback:function(map) {
		
				DWRUtil.useLoadingMessage();
				document.formpost.koderegion.value=map.LSRG_NAMA;
				document.formpost.kodebisnis.value = map.LSBS_ID;
				document.formpost.numberbisnis.value = map.LSDBS_NUMBER;
				document.formpost.jml_peserta.value = map.jml_peserta;
			   },
			  timeout:15000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
			});
		}	
	}
	
	function awal(){
		cariregion(document.formpost.spaj.value,'region');
		setFrameSize('infoFrame', 90);
		setFrameSize('docFrame', 90);
	}
	
	function copyToClipboard(txt){
	
		if( window.clipboardData && clipboardData.setData ){
		
			clipboardData.setData("Text", txt);
		}
		else{
		 alert("Internet Explorer required"); 
		}
	}
	
	function run(){
		var shell = new ActiveXObject("WScript.Shell"); 
		var commandtoRun = "\\\\aplikasi\\lions\\secan4400f.exe";
	   
		if (shell){		
		 shell.run(commandtoRun); 
		} 
		else{ 
			alert("program or file doesn't exist on your system."); 
		}
	}
	
	function copyAndRun(){ 
	 	txt = document.getElementById("spaj").value;
	 	copyToClipboard(txt);
	 	run();		
	}

</script>
<body  onload="awal();" onresize="setFrameSize('infoFrame', 90); setFrameSize('docFrame', 90);" style="height: 100%;">
<form name="formpost" method="post">
<div class="tabcontent">
<table class="entry2" style="width: 98%;">
	<tr>
		<th>Cari SPAJ</th>
		<td> <input type="hidden" name="koderegion" > 
          <input type="hidden" name="kodebisnis" >
          <input type="hidden" name="numberbisnis">
          <input type="hidden" name="jml_peserta">
			<select name="spaj" id="spaj" onChange="cariregion(this.options[this.selectedIndex].value,'region');">
				<option value="">[--- Silahkan Pilih/Cari SPAJ ---]</option>
				<c:forEach var="s" items="${daftarSPAJ}">
					<option value="${s.REG_SPAJ }" style="background-color: ${s.BG};" 
						<c:if test="${s.REG_SPAJ eq param.spaj }">selected<c:set var="ada" value="ada" /></c:if>>${s.SPAJ_FORMATTED}
					- ${s.POLICY_FORMATTED }</option>
				</c:forEach>
			</select>

			<input type="button" value="Info" name="info" 				onclick="showPage('tampil');" accesskey="I" onmouseover="return overlib('Alt-I', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Cari" name="search" 			onclick="popWin('${path}/bancass/spaj.htm?posisi=2&win=bancass', 600, 800); " accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">
		</td>
	</tr>
	<tr>
		<th>Proses Utama</th>
		<td>
			<!--
			<input name="btn_simultan" type="button" value="Simultan" onClick="simultan()" accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input disabled name="btn_reas"  type="button" value="Reas" onClick="reas()" accesskey="R" onmouseover="return overlib('Alt-R', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input disabled name="btn_transfer" type="button" value="Transfer" onClick="transfer('0')" accesskey="T" onmouseover="return overlib('Alt-T', AUTOSTATUS, WRAP);" onmouseout="nd();">
			-->
			<input name="btn_checklist" type="button" value="Checklist" 			onClick="showPage('checklist');" onmouseover="return overlib('Checklist Dokumen Polis', AUTOSTATUS, WRAP);" onmouseout="nd();">
		</td>
	</tr>
	<tr>
		<th>Proses Tambahan</th>
		<td><%--<input name="btn_batal" type="button" value="Batalkan" 			onClick="showPage('batal');" accesskey="B" onmouseover="return overlib('Alt-B	', AUTOSTATUS, WRAP);" onmouseout="nd();">--%>
			
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<table style="margin: 0 0 0 0; padding: 0 0 0 0; width: 100%;">
				<tr>
					<c:choose>
						<c:when test="${sessionScope.currentUser.wideScreen}">
							<td style="width: 60%;">
								<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
									width="100%"  height="100%"> Please Wait... </iframe>
							</td>
							<td style="width: 40%;">
								<iframe src="" name="docFrame" id="docFrame" width="100%">E-Lions</iframe>
							</td>
						</c:when>
						<c:otherwise>
							<td>
								<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
									width="100%"  height="100%"> Please Wait... </iframe>
							</td>
						</c:otherwise>
					</c:choose>
				</tr>
			</table>		
		</td>	
	</tr>
</table>
</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>