<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<head>
	<title>Simple Layout Demo</title>
	<link rel="stylesheet" href="${path}/include/js/jquery/css/smoothness/jquery-ui-1.8.8.custom.css" />
	<script type="text/javascript" src="${path}/include/js/jquery/jquery-1.4.4.min.js"></script> <!-- main jquery lib -->
	<script type="text/javascript" src="${path}/include/js/jquery/jquery-ui-1.8.8.custom.min.js"></script> <!-- jquery UI lib -->
	<script type="text/javascript" src="${path}/include/js/jquery/jquery.layout.min.js"></script> <!-- jquery UI layout -->

	<script type="text/javascript">

	// accordion menu di west pane
	$(function() {
		var icons = {
			header: "ui-icon-circle-arrow-e",
			headerSelected: "ui-icon-circle-arrow-s"
		};
		$( "#accordion" ).accordion({
			icons: icons, //icons
			fillSpace: true //height = 100%
		});
	});	

	// init panes
	var myLayout;
	
	$(document).ready(function () {
		myLayout = $('body').layout({
			north__resizable: false, //north: tdk bisa resize
			east__initClosed: true, //east: closed
			south__resizable: false //south: tdk bisa resize
		});
 	});
	
	//buttons
	$(function() {
	
		$( "#logout" ).button({
			icons: {primary: "ui-icon-closethick"}
		})
		.click(function() {
			alert( "Logout" );
		});

		$( "#inbox" ).button({
			icons: {primary: "ui-icon-mail-closed"}
		})
		.click(function() {
			alert( "Inbox" );
		});
		

		$( "#menu" ).button({
			icons: {primary: "ui-icon-lightbulb"}
		})
		.click(function() {
			alert( "Main Menu" );
		});			
		
		$( "#report1" ).button({
			icons: {primary: "ui-icon-document"}
		})
		.click(function() {
			alert( "Report 1" );
		});			
		
		$( "#support" ).button({
			icons: {primary: "ui-icon-person"}
		})
		.click(function() {
			myLayout.toggle('east');
		});			

		$( "#docs" ).button({
			icons: {primary: "ui-icon-help"}
		})
		.click(function() {
			alert( "docs" );
		});			
		
		$( "#closeeast" ).button({
			icons: {primary: "ui-icon-close"}
		})
		.click(function() {
			myLayout.close('east');
		});			
		
	});

	</script>

	<style type="text/css">
	/* Tambahan Yusuf */
	body {
		font-size: 8pt;
		font-family: "Trebuchet MS", "Helvetica", "Arial",  "Verdana", "sans-serif";
	}

	/* DISABLE dulu, ini gunanya biar semua tombol ukurannya sama (perlu gak?)
	button {
		width: 140px;
	}
	*/
	
	/**
	 *	Styling untuk pane
	 */

	.ui-layout-pane { /* all 'panes' */ 
		background: #FFF; 
		border: 1px solid #BBB; 
		padding: 5px;
		overflow: auto;
	} 

	.ui-layout-resizer { /* all 'resizer-bars' */ 
		background: #DDD; 
	} 

	.ui-layout-toggler { /* all 'toggler-buttons' */ 
		background: #AAA; 
	} 

	.ui-layout-north { /* Tambahan Yusuf, untuk styling pane */
		background-image:url('${path}/include/js/jquery/header-bg.png');
		background-repeat:repeat-x;
	}
	
	.ui-layout-south { /* Tambahan Yusuf, untuk styling pane */
		background-image:url('${path}/include/js/jquery/footer-bg.png');
		background-repeat:repeat-x;
	}

	</style>

</head>
<body>

<!-- NORTH PANE -->
<div class="ui-layout-north" onmouseover="myLayout.allowOverflow('north')" onmouseout="myLayout.resetOverflow(this)">
	<img src="${path}/include/js/jquery/header-logo.png"></img>
	<div id="headerbuttons" style="float: right">
		<button id="docs">Docs</button>
		<button id="support">Support</button>
		<button id="logout">Logout</button>
	</div>
</div>

<!-- WEST PANE -->
<div class="ui-layout-west" id="accordion">

	<h3><a href="#">My Dashboard</a></h3>
	<div>
		<button id="inbox">Inbox</button>
	</div>
	
	<h3><a href="#">Main Menu</a></h3>
	<div>
		<button id="menu">Main Menu</button>
	</div>
	
	<h3><a href="#">My Reports</a></h3>
	<div>
		<button id="report1">Report 1</button>
	</div>
	
	<h3><a href="#">Help</a></h3>
	<div>
	</div>

</div>

<!-- SOUTH PANE -->
<div class="ui-layout-south">
	This is the south pane, closable, slidable and resizable &nbsp;
	<button onclick="myLayout.toggle('north')">Toggle North Pane</button>
</div>

<!-- EAST PANE -->
<div class="ui-layout-east">
	<button id="closeeast">Close</button>
</div>

<!-- CENTER PANE -->
<div class="ui-layout-center">
	This center pane auto-sizes to fit the space <i>between</i> the 'border-panes'
</div>

</body>
</html>