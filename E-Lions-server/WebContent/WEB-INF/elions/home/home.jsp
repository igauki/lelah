<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" 
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<%@ page contentType="text/html" language="java" import="com.ekalife.utils.EncryptUtils" errorPage="" %>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<title>[E-Lions] PT Asuransi Jiwa Sinarmas MSIG</title>

	<link rel="stylesheet" href="${path}/include/js/jquery/css/smoothness/jquery-ui-1.8.8.custom.css" />

	<style type="text/css">

	/* remove padding and scrolling from elements that contain an Accordion OR a content-div */
	.ui-layout-center ,	/* has content-div */
	.ui-layout-west ,	/* has Accordion */
	.ui-layout-east ,	/* has content-div ... */
	.ui-layout-east .ui-layout-content { /* content-div has Accordion */
		padding: 0;
		overflow: hidden;
	}
	.ui-layout-center P.ui-layout-content {
		line-height:	1.4em;
		margin:			0; /* remove top/bottom margins from <P> used as content-div */
	}
	h3, h4 { /* Headers & Footer in Center & East panes */
		font-size:		1.1em;
		background:		#EEF;
		border:			1px solid #BBB;
		border-width:	0 0 1px;
		padding:		7px 10px;
		margin:			0;
	}
	.ui-layout-east h4 { /* Footer in East-pane */
		font-size:		0.9em;
		font-weight:	normal;
		border-width:	1px 0 0;
	}
	
	/* Tambahan Yusuf */
	body {
		font-size: 8pt;
		font-family: "Trebuchet MS", "Helvetica", "Arial",  "Verdana", "sans-serif";
	}
	
	.ui-layout-resizer { /* all 'resizer-bars' */ 
		background: #DDD; 
	} 

	.ui-layout-toggler { /* all 'toggler-buttons' */ 
		background: #AAA; 
	} 
	
	.ui-layout-north { /* styling pane */
		padding: 4px;
	}
	
	div.support{
		margin: 5px;
	}
	
	div.support ul{
		margin: 5px;
	}
	
	div.support li{
		margin-bottom: 5px;
	}
	
	</style>

	<script type="text/javascript" src="${path}/include/js/jquery/jquery-1.4.4.min.js"></script> <!-- main jquery lib -->
	<script type="text/javascript" src="${path}/include/js/jquery/jquery-ui-1.8.8.custom.min.js"></script> <!-- jquery UI lib -->
	<script type="text/javascript" src="${path}/include/js/jquery/jquery.layout.min.js"></script> <!-- jquery UI layout -->

	<script type="text/javascript">

	var myLayout, $westAccordion; // init global vars

	$(document).ready( function() {

		myLayout = $('body').layout({
			north__resizable: false //north: tdk bisa resize
		,	east__initClosed: true //east: closed
		,	south__resizable: false //south: tdk bisa resize
		,	west__size:			200 //init size in pixels
		,	east__size:			300 //init size in pixels
		,	north__size:		40
		,	west__onresize:		function () { $("#accordion1").accordion("resize"); } // RESIZE Accordion widget when panes resize
		});

		var icons = {
			header: "ui-icon-circle-arrow-e",
			headerSelected: "ui-icon-circle-arrow-s"
		};

		// ACCORDION - in the West pane
		$westAccordion = $("#accordion1").accordion({
			icons: icons, //icons
			fillSpace:	true
		});

	});

	//buttons
	$(function() {
	
		$( "#logout" ).button({
			icons: {primary: "ui-icon-closethick"}
		})
		.click(function() {
			alert( "Logout" );
		});

		$( "#inbox" ).button({
			icons: {primary: "ui-icon-mail-closed"}
		})
		.click(function() {
			alert( "Inbox" );
		});
		

		$( "#menu" ).button({
			icons: {primary: "ui-icon-lightbulb"}
		})
		.click(function() {
			$("#appFrame").attr('src', "${path}/common/menu.htm?frame=treemenu");
		});			
		
		$( "#report1" ).button({
			icons: {primary: "ui-icon-document"}
		})
		.click(function() {
			alert( "Report 1" );
		});			
		
		$( "#support" ).button({
			icons: {primary: "ui-icon-person"}
		})
		.click(function() {
			myLayout.toggle('east');
		});			

		$( "#docs" ).button({
			icons: {primary: "ui-icon-help"}
		})
		.click(function() {
			$("#appFrame").attr('src', "${path}/docs");
		});			
		
		$( "#closeeast" ).button({
			icons: {primary: "ui-icon-close"}
		})
		.click(function() {
			myLayout.close('east');
		});			
		
	});
	
	</script>

</head>
<body>

<div class="ui-layout-north ui-widget-header" style="display: none;">
	<img src="${path}/include/js/jquery/header-logo.png" style="float:left"></img>
	<div id="headerbuttons" style="float: right">
		<button id="docs">Docs</button>
		<button id="support">Support</button>
		<button id="logout">Logout</button>
	</div>
</div>

<div class="ui-layout-center" style="display: none;"> 
	<%-- <h3 class="ui-widget-header">Center Pane</h3> --%>

	<iframe src="${path}/common/menu.htm?frame=treemenu" id="appFrame" style="width: 100%; height: 100%;" frameborder="0">
	</iframe>

	<p class="ui-layout-content ui-widget-content">
		<b>NOTE</b>: As of UI v1.7.1, the Accordion widget has a 'resize' method.
		If you were previously using the patched version of ui.accordion, 
		you should upgrade to the latest versions of jQuery and jQuery UI.
	</p>

</div>

<div class="ui-layout-south ui-widget-header" style="display: none; padding: 3px">
	<p style="float: left; vertical-align: middle;">
		<c:if test="${sessionScope.currentUser.jn_bank ne 2 and sessionScope.currentUser.jn_bank ne 3}">
			<a href="http://202.43.181.35/bas?auth=<%=EncryptUtils.encode("14041985".getBytes())%>" target="_blank"><img style="border: none;" alt="Bas Manual Book (external)" src="${path}/include/icons/bas-meb.png"></a>
			<a href="http://128.21.32.14/bas?auth=<%=EncryptUtils.encode("14041985".getBytes())%>" target="_blank"><img style="border: none;" alt="Bas Manual Book (internal)" src="${path}/include/icons/bas-meb.png"></a>
			<a href="/E-Agency" target="_blank"><img style="border: none;" alt="E-Agency" src="${path}/include/icons/e-agency.png"></a>
			<a href="${cmd.intranet}/simaslifehrd/webform/login.aspx" target="_blank"><img style="border: none;" alt="E-HRD" src="${path}/include/icons/e-hrd.png"></a>
			<a href="/E-Policy" target="_blank"><img style="border: none;" alt="E-Policy" src="${path}/include/icons/e-policy.png"></a>
			<a href="/EB" target="_blank"><img style="border: none;" alt="EB" src="${path}/include/icons/eb-online.png"></a>
			<a href="${cmd.intranet}/flash/pageFlip/fc.html" target="_blank"><img style="border: none;" alt="Brosur Produk" src="${path}/include/icons/web-bp.png"></a>
			<a href="${cmd.intranet}/faq.asp" target="_blank"><img style="border: none;" alt="FAQ" src="${path}/include/icons/web-faq.png"></a>
			<a href="${cmd.intranet}/productinfo.asp" target="_blank"><img style="border: none;" alt="Product Info" src="${path}/include/icons/web-pi.png"></a>
		</c:if>
		Menu [eka8i]: Entry > U/W > BAC
	</p>
	<p style="float: right">Logged in as ${sessionScope.currentUser.name} [${sessionScope.currentUser.dept}] since <fmt:formatDate value="${sessionScope.currentUser.loginTime}" pattern="[hh:mm]"/></p>
</div>

<div class="ui-layout-west" style="display: none;">
	<div id="accordion1" class="basic">
		<h3><a href="#">My Dashboard</a></h3>
		<div>
			<button id="inbox">Inbox</button>
		</div>
		
		<h3><a href="#">Main Menu</a></h3>
		<div>
			<button id="menu">Main Menu</button>
		</div>
		
		<h3><a href="#">My Reports</a></h3>
		<div>
			<button id="report1">Report 1</button>
		</div>
		
	</div>
</div>

<div class="ui-layout-east" style="display: none;">
	<h3 class="ui-widget-header">Support</h3>
	<div class="support">
		<strong>Kantor Pusat</strong>
		<br />PT Asuransi Jiwa Sinarmas MSIG
		<br />Wisma Eka Jiwa Lt. 8
		<br />Jl. Mangga Dua Raya, Jakarta 10730 - Indonesia
		<br />Telp: 021 6257808 / Fax: 021 6257837
		
		<br /><br /><strong>IT Department - Web Development Division</strong>
		<ul>
			<li>
				<strong>Yusuf S.</strong>
				<br />Telp: 021 6257808 ext 8109
				<br />HP: 0881 234 1981
				<br />Email: <a href="mailto:yusuf@sinarmasmsiglife.co.id">yusuf@sinarmasmsiglife.co.id</a>
			</li>
			<li>
				<strong>Deddy S.</strong>
				<br />Telp: 021 6257808 ext 8109
				<br />Email: <a href="mailto:deddy@sinarmasmsiglife.co.id">deddy@sinarmasmsiglife.co.id</a>
			</li>
			<li>
				<strong>Bertho R.</strong>
				<br />Telp: 021 6257808 ext 8109
				<br />Email: <a href="mailto:berto@sinarmasmsiglife.co.id">berto@sinarmasmsiglife.co.id</a>
			</li>
		</ul>
	</div>
	<%-- <h4 class="ui-widget-content ui-state-hover">Footer</h4> --%>
</div>

</body>
</html> 