<%@ include file="/include/page/header.jsp"%>
<script>

</script>
<body style="height: 100%;" onload="setupPanes('container1', 'tab1'); ">
	<div class="tab-container" id="container1">
		
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Update Proposal dan Absensi</a>
			</li>
		</ul>

		<div class="tab-panes">
			<div id="pane1" class="panes">
			<c:choose>
				<c:when test="${ sessionScope.currentUser.lca_id eq '55' }">
							<table class="entry2">
							<tr>
							<th>
							</th>
							<td>
							data tidak ditampilkan
							</td>
				</c:when>
				<c:otherwise>
				<form method="post" name="formpost">
					<table class="entry2">
						<tr>
							<th>
							</th>
							<td>

								<table class="displaytag" style="width: auto;">
									<caption>Silahkan Pilih File:</caption>
									<tr>
										<th>File Name</th>
										<th>Description</th>
										<th>Last Modified</th>
									</tr>
									<c:forEach var="dok" items="${daftarFile}">
										<tr>
											<td style="text-align: left;"><a href="${path}/update_proposal/update.htm?window=proposal&download=${dok.key}">${dok.key}</a></td>
											<td style="text-align: left;">${dok.desc}</td>
											<td style="text-align: left;">${dok.value}</td>
										</tr>
									</c:forEach>
								</table>
								
								<br/>
								<strong>Terdapat dua pilihan Untuk melakukan update ini yaitu:</strong>
								<p>
									Pilihan pertama, Langsung menjalankan file update setelah selesai download. Berikut petunjuknya:
									<ul>
										<li>Tunggu beberapa saat sampai muncul kotak dialog untuk mendownload file.</li>
										<li>Setelah muncul kotak dialog, klik tombol OPEN.</li>
										<li>Tunggu sampai download file selesai (100% completed), tekan tombol UNZIP untuk memulai update.</li>
									</ul>
								</p>
								
								<p>
									Pilihan kedua, Simpan file update hasil download untuk arsip lalu jalankan file tsb untuk update. Berikut petunjuknya:
									<ul>
										<li>Tunggu beberapa saat sampai muncul kotak dialog untuk mendownload file.</li>
										<li>Setelah muncul kotak dialog, klik tombol SAVE.</li>
										<li>Tunggu sampai download file selesai (100% completed).</li>
										<li>Jalankan Windows Explorer, cari file update_XXXXXX.exe atau upas_XXXXXX.exe atau update_absenXXXX.exe (sesuai kebutuhan) yang baru saja disave.</li>
										<li>Jalankan file tersebut.</li>
										<li>Setelah file dijalankan, tekan tombol UNZIP untuk memulai update.</li>
									</ul>

								</p>
								
								<p>
									<strong>Catatan Tambahan</strong>
									<ul>
										<li>Kami sarankan pilihan pertama untuk melakukan update ini.</li> 
										<li>Cara untuk mengupdate proposal Agency System adalah sama dengan cara mengupdate Proposal Regional, cuma link nya saja yang berbeda.</li>
										<li>Jika sebelumnya adalah regional, maka tetap menggunakan update regional</li>
									</ul>
								</p>							

								<p>
									<strong>Cara Instalasi Proposal Khusus Bank Sinarmas</strong>
									<ol>
										<li>Download dan Install versi full nya terlebih dahulu (FULL REGIONAL.zip)</li> 
										<li>Download dan Install update versi terbaru (UPBSM_YYMMDD.exe dimana YYMMDD adalah tanggal update terbaru)</li>
									</ol>
								</p>							
							</td>
						</tr>

					</table>
				</form>	
				</c:otherwise>
				</c:choose>		
			</div>
		</div>
		
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>