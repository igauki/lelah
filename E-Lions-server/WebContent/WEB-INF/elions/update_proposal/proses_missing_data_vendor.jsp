<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path}/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path}/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path}/include/js/ajaxtags/overlibmws.js"></script>
<script>
	hideLoadingMessage();
	function awal(){
		if('${suc}'!=''){
			alert("${suc} \n data Vendor yang berhasil di Update sebanyak ${count}");
		}
		if('${err}'!=''){
			alert("${err}");
		}	
		
		if('${access}'!=''){
			alert("${access}");
		}
	}
	function proses(){
		indicator.style.display='';
		formpost.submit();
	}
	
	
</script>
</head>
<BODY onload="setupPanes('container1', 'tab1'); awal();" style="height: 100%;">

	<div class="tab-container" id="container1">
		<ul class="tabs">
			<li>
				<a href="#" onClick="return showPane('pane1', this)" id="tab1">Upload File</a>
			</li>
		</ul>

		<div class="tab-panes">

			<div id="pane1" class="panes">
				<form method="post" name="formpost" style="text-align: center;" enctype="multipart/form-data">
			
					<table class="entry2">
						<tr>
							<th style="vertical-align: top;">
								<fieldset>
									<legend>Proces Update Missing Data -- File (Max: 1 Mb)</legend>
									<table class="entry2">
										<c:if test="${access eq null}">
											<tr>
												<th>Please Select User want to be Update</th>
												<td>
													<select name="lusId">
														<c:forEach items="${lsUser}" var="x"> 
															<option value="${x.KEY}"  >${x.VALUE }</option>
														</c:forEach>
													</select>
												</td>
											</tr>
											<tr>
												<th>Add Your File</th>
												<td>
													<input type="file" name="file1" size="70">
													<input type="button" name="upload" onClick="proses();" value="Upload" >
													<span id="indicator" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
												</td>
											</tr>
										</c:if>
									</table>
								</fieldset>
							</th>
						</tr>
					</table>			

				</form>
			</div>
		</div>
	</div>

</form>
</body>
<%@ include file="/include/page/footer.jsp"%>