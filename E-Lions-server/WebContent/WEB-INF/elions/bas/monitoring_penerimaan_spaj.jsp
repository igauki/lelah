<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/header_jquery.jsp"%>
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script>
			$().ready(function() {
			
			$('#no_blanko1').change(function() {	
				var url_cek = "${path}/bas/hadiah.htm?window=json&t=no_blanko&no_blanko1=" + this.value;
				var val = this.value;
				$.getJSON(url_cek, function(result_cek){
					if(result_cek.length == 0){
					var url = "${path}/bas/hadiah.htm?window=json&t=no_blanko&no_blanko1=" + val;
					$.getJSON(url, function(result) {
						if(result.length == 0){
							alert('No Blanko tidak ditemukan. Harap cek ulang nomor yang dimasukkan');
						}
						else{
							$.each(result, function() {
								$('#id_dok').val(this.id_dok);
								$('#msag_id').val(this.msag_id);
								$('#tgl_kembali_ke_agen').val(this.tgl_kembali_ke_agen);
								$('#tgl_terima_dari_agen').val(this.tgl_terima_dari_agen);
								$('#jenis_further').val(this.jenis_further);
								$('#keterangan_further').val(this.keterangan_further);
								$('#mh_alamat').val(this.mh_alamat);
								$('#mh_kodepos').val(this.mh_kodepos);
								$('#mh_kota').val(this.mh_kota);
								$('#mh_telepon').val(this.mh_telepon);
															
							});							
						}				
					});
					}				
				});				
			});			
		
				$( "#tabs" ).tabs();
			   
				$(".datepicker").datepicker({
					changeMonth: true,
					changeYear: true,
					dateFormat: "dd/mm/yy" 
				});
				
				var pesan = '${pesan}';
				if(pesan!=null && pesan!=''){
					alert(pesan);
				}
				
				$('#btnSave').click(function(){ 
			     	if($('#no_blanko').val()==null || $('#no_blanko').val()==''){
			     		alert('Harap masukkan no blanko!');
			     		$('#no_blanko').focus();
			     		return false;
			     	}
			     	if($('#pemegang').val()==null || $('#pemegang').val()==''){
			     		alert('Harap masukkan nama pemegang!');
			     		$('#pemegang').focus();
			     		return false;
			     	}
			   	});
			   	
			   	$('#btnSave1').click(function(){ 
			     	if($('#no_blanko1').val()==null || $('#no_blanko1').val()==''){
			     		alert('Harap masukkan no blanko!');
			     		$('#no_blanko1').focus();
			     		return false;
			     	}
			     	
			     	if($('input[name=status]:checked', '#formpost5').val()=='f'){
			     	 	if($('#kd_agen1').val()==null || $('#kd_agen1').val()==''){
				     		alert('Harap masukkan kode agen!');
				     		$('#kd_agen1').focus();
				     		return false;
				     	}
				     	
// 				     	if($('#tgl_kembali_agen').val()==null || $('#tgl_kembali_agen').val()==''){
				     	
// 				     		if($('#statEditToFurt').val() !='1'){
// 			  					alert('Harap masukkan tgl kembali ke agen!');
// 			  					}
			  		   	 	
// 				     		$('#tgl_kembali_agen').focus();
// 				     		return false;
// 				     	}
				     	
				     	if($('#keterangan_further').val()==null || $('#keterangan_further').val()==''){
				     		alert('Harap masukkan keterangan!');
				     		$('#keterangan_further').focus();
				     		return false;
				     	}
				     	
				    /*  	if($('#cek').val()==1){
				     		if($('#tgl_terima_agen').val()==null || $('#tgl_terima_agen').val()==''){
					     		alert('Harap masukkan tgl terima dari agen!');
					     		$('#tgl_terima_agen').focus();
					     		return false;
					     	}
				     	} */
				     }
			   	});
			   	
			   	$('#btnTampil').click(function(){
					window.location.href = '${path}/bas/spaj.htm?window=monitoring_penerimaan_spaj&btnTampil=1&bdate='+$("#bdate2").val()+'&edate='+$("#edate2").val();			
				});
				
				
				$('#btnSearch').click(function(){
// 					alert(document.getElementById("searchList").value +" "+ $("#searchText").val());
				window.location.href = '${path}/bas/spaj.htm?window=monitoring_penerimaan_spaj&btnSearch=1&searchList='+document.getElementById("searchList").value+'&searchText='+$("#searchText").val();	
						
				});
				
				
							
				
			});
			
			var fieldName='chbox'; 
	
			function selectall(){
			  var i=document.formpost8.elements.length;
			  var e=document.formpost8.elements;
			  var name=new Array();
			  var value=new Array();
			  var j=0;
			  for(var k=0;k<i;k++)
			  {
			    if(document.formpost8.elements[k].name==fieldName)
			    {
			      if(document.formpost8.elements[k].checked==true){
			        value[j]=document.formpost8.elements[k].value;
			        j++;
			      }
			    }
			  }
			  checkSelect();
			}
				
			function selectCheck(obj)
			{
			 var i=document.formpost8.elements.length;
			  for(var k=0;k<i;k++)
			  {
			    if(document.formpost8.elements[k].name==fieldName)
			    {
			      document.formpost8.elements[k].checked=obj;
			    }
			  }
			  selectall();
			}
				
			function selectallMe()
			{
			  if(document.formpost8.allCheck.checked==true)
			  {
			   selectCheck(true);
			  }
			  else
			  {
			    selectCheck(false);
			  }
			}
				
			function checkSelect()
			{
			 var i=document.formpost8.elements.length;
			 var berror=true;
			  for(var k=0;k<i;k++)
			  {
			    if(document.formpost8.elements[k].name==fieldName)
			    {
			      if(document.formpost8.elements[k].checked==false)
			      {
			        berror=false;
			        break;
			      }
			    }
			  }
			  if(berror==false)
			  {
			    document.formpost8.allCheck.checked=false;
			  }else
			  {
			    document.formpost8.allCheck.checked=true;
			  }
			}
			
		      function selectall2(){
			  var i=document.formpost7.elements.length;
			  var e=document.formpost7.elements;
			  var name=new Array();
			  var value=new Array();
			  var j=0;
			  for(var k=0;k<i;k++)
			  {
			    if(document.formpost7.elements[k].name==fieldName)
			    {
			      if(document.formpost7.elements[k].checked==true){
			        value[j]=document.formpost7.elements[k].value;
			        j++;
			      }
			    }
			  }
			  checkSelect2();
			}
				
			function selectCheck2(obj)
			{
			 var i=document.formpost7.elements.length;
			  for(var k=0;k<i;k++)
			  {
			    if(document.formpost7.elements[k].name==fieldName)
			    {
			      document.formpost7.elements[k].checked=obj;
			    }
			  }
			  selectall2();
			}
				
			function selectallMe2()
			{
			  if(document.formpost7.allCheck2.checked==true)
			  {
			   selectCheck2(true);
			  }
			  else
			  {
			    selectCheck2(false);
			  }
			}
				
			function checkSelect2()
			{
			 var i=document.formpost7.elements.length;
			 var berror2=true;
			  for(var k=0;k<i;k++)
			  {
			    if(document.formpost7.elements[k].name==fieldName)
			    {
			      if(document.formpost7.elements[k].checked==false)
			      {
			        berror2=false;
			        break;
			      }
			    }
			  }
			  if(berror2==false)
			  {
			    document.formpost7.allCheck2.checked=false;
			  }else
			  {
			    document.formpost7.allCheck2.checked=true;
			  }
			}
			
			function further()
			{
				kategori_further=document.formpost5.kategori_further.value;
				if(kategori_further=="LAIN-LAIN"){
					var other=document.getElementById("other");
					other.style.visibility='visible';  
				}
			}
			
			function handleEnter(inField, e) {
			    var charCode;
			    
			    if(e && e.which){
			        charCode = e.which;
			    }else if(window.event){
			        e = window.event;
			        charCode = e.keyCode;
			    }		   
			}	
			
			
			hideLoadingMessage();
			$(document).ready(function() {

		     	if($('#flag_form').val()=='2'){		     	  	
		   			   		 
		   		  if($('#pilihan').val()=='1'){
		     	  	
// 		     	  	 if($('#statEditToFurt').val()=='1'){
// 			  			$('#kembali_agen').hide();}
// 			  		 else{ 
// 			  	 		$('#kembali_agen').show();};
			  	 	
			  	 	if($('#cek').val()=='1'){
			  			$('#kembali_agen').show();}
			  		 else{ 
			  	 		$('#kembali_agen').hide();};
			  	 	
				     $('#terima_agen').hide(); 
				     $('#further').show(); 
				     $('#keterangan').show();
				     $('#kode_agen').show(); 
				     $('#e_cc').show();
				     $('#btnCek').show();
				     $('#formpost6').show();
				     $('#formpost7').show();			     
				     $('#formpost8').hide();
   				     $('#formpost2').hide();
   				     $('#formpostSearch').hide();	
	     	 		}
	     	 	  if($('#pilihan').val()=='2'){
	     	 		 $('#kembali_agen').hide();
				     $('#terima_agen').hide(); 
				     $('#further').hide(); 
				     $('#keterangan').hide();
				     $('#kode_agen').hide(); 
				     $('#e_cc').hide();
				     $('#btnCek').hide();
				     $('#formpost6').hide();
				     $('#formpost7').hide();
				     $('#formpost8').show();
				     $('#formpost2').show();
				     $('#formpostSearch').show();	
	     	 		}
	     	 						
		     	} 
			   if($('#1').click(function(){
			     $('#formpost').show();
			     $('#formpost2').show();
		         $('#formpost3').show();
			     $('#formpost5').hide();
			     $('#formpost6').hide();
			     $('#formpost7').hide();
			     $('#formpost8').hide();
			     	     
			   }));
			   if($('#2').click(function(){
			      $('#formpost5').show();
			      $('#formpost').hide();
			      $('#formpost2').hide();
			      $('#formpost3').hide();
			   }));			   

			   if($('#f').click(function(){
			  		
// 			  	if($('#statEditToFurt').val()=='1'){
// 			  		$('#kembali_agen').hide();}
// 			  	 else{ 
// 			  	 	$('#kembali_agen').show();};

				if($('#cek').val()=='1'){
			  		$('#kembali_agen').show();}
			  	 else{ 
			  	 	$('#kembali_agen').hide();};
			  	 
			     $('#terima_agen').hide(); 
			     $('#further').show(); 
			     $('#keterangan').show();
			     $('#kode_agen').show(); 
			     $('#btnCek').show();
			     $('#formpost3').hide();
			     $('#formpost6').show();
			     $('#formpost7').show();
			     $('#formpost8').hide();
			     $('#formpost2').hide();
			     $('#e_cc').show();
			     $('#formpostSearch').hide();
			   }));
			   if($('#nf').click(function(){
			     $('#kembali_agen').hide();
			     $('#terima_agen').hide(); 
			     $('#further').hide(); 
			     $('#keterangan').hide();
			     $('#kode_agen').hide(); 
			     $('#btnCek').hide();
			     $('#formpost3').hide();
			     $('#formpost6').hide();
			     $('#formpost7').hide();
			     $('#formpost8').show();
			     $('#formpost2').show();
			     $('#e_cc').hide();
			     $('#formpostSearch').show();
			   }));
			 });	 
			 
			 var aas;
			function EditToFurt(btnEditToFurtA) {
				alert("No Blanko : " + btnEditToFurtA + " akan di ubah menjadi Further. Silahkan Menunggu sampai Halaman berubah menjadi halaman Pengeditan Further !");
				window.location.href = '${path}/bas/spaj.htm?window=monitoring_penerimaan_spaj&btnEditToFurt=1&statEditToFurt=1&btnEditToFurtA='+btnEditToFurtA;	
				
				 
				 
				return true;
			}
		</script>
		
	</head>
	
	<body>
		<div id="tabs">
			<ul>
				<li><a href="#tabs-1">Input</a></li>
<!-- 				<li><a href="#tabs-2">SPAJ Belum Input</a></li> -->
<!-- 				<li><a href="#tabs-4">SPAJ Further</a></li> -->
<!-- 				<li><a href="#tabs-3">Report</a></li> -->
			</ul>
			
		<div id="tabs-1">
			<table>
				<tr>
					<td>

						<form name="formpost5" id="formpost5" method="post"
							style="display: visible;">
							<fieldset>
								<legend>Info Yang Harus Diisi</legend>
								<table class="entry2">
									<tr>
										<th width="200">No Blanko*</th>
										<td><input type="text" id="no_blanko1" name="no_blanko1"
											size="50" value="${no_blanko1}" 
											<c:if test="${cek eq 1 or statEditToFurt eq 1 }"> readonly="readonly" style="background-color: #E0E0E0"</c:if> />
											<input type="submit"
											name="btnCek" id="btnCek" title="Search No Blanko"
											value="Search" style="display: none;"></td>
									</tr>
									<tr style="display: none;" id="kode_agen">
										<th width="200">Kode Agen*</th>
										<td><input type="text" id="kd_agen1" name="kd_agen1"
											size="50" value="${kd_agen1}"
											<c:if test="${cek eq 1 }"> readonly="readonly" style="background-color: #E0E0E0"</c:if> />
<!-- 											<c:if test="${kd_agen1 ne null and btnCek ne null}"> readonly="readonly" style="background-color: #E0E0E0"</c:if> /> -->
										</td>
									</tr>
									<tr>
										<th width="200">Pemegang Polis</th>
										<td><input type="text" id="nama_pemegang"
											name="nama_pemegang" size="50" value="${nama_pemegang}"/>
											
<!-- 											<c:if test="${kd_agen1 ne null and btnCek ne null}"> readonly="readonly" style="background-color: #E0E0E0" </c:if> /> -->
												
												
										</td>
									</tr>
									<tr>
										<th width="200">Informasi</th>
										<td><input type="text" id="informasi"
											name="informasi" size="50" value="${informasi}"/>
										</td>
									</tr>
									<tr>
										<th width="200">Status SPAJ</th>
										<td><input type="radio" name="status" id="nf" value="nf"
											checked="checked"
											<c:if test="${status eq 'nf'}"> checked</c:if> /><b>NON FURTHER</b> 
											<input type="radio" name="status" id="f" value="f"
											<c:if test="${status eq 'f'}"> checked</c:if> /><b>FURTHER</b>
										
										</td>
									</tr>
									<tr style="display: none;" id="kembali_agen">
										<th width="200">Tgl Kembali ke Agen</th>
										<td><input type="text" id="tgl_kembali_agen"
											name="tgl_kembali_agen" value="${tgl_kembali_agen}"<c:choose>
								<c:when test="${cek eq 1}">
									readonly="readonly" style="background-color: #E0E0E0"
								</c:when>			
								<c:when test="${tgl_kembali_agen ne null}">
									readonly="readonly" style="background-color: #E0E0E0"
								</c:when>
								<c:otherwise>
									 class="datepicker"
								</c:otherwise>
							</c:choose>
							<%-- <c:if test="${tgl_kembali_agen ne null}"> 
								disabled="disabled" style="background-color: #E0E0E0"
							</c:if> --%> >
										</td>
									</tr>
									<tr style="display: none;" id="terima_agen">
										<th width="200">Tgl Terima dari Agen</th>
										<td><input type="text" id="tgl_terima_agen"
											name="tgl_terima_agen" value="${tgl_terima_agen}"<c:choose>
								<c:when test="${tgl_kembali_agen eq null}">
									readonly="readonly" style="background-color: #E0E0E0"
								</c:when>
								<c:otherwise>
									 class="datepicker"
								</c:otherwise>
							</c:choose>
							<%-- <c:if test="${tgl_kembali_agen eq null}"> 
								disabled="disabled" style="background-color: #E0E0E0" 
							</c:if> --%> >
										</td>
									</tr>
									<tr style="display: none;" id="e_cc">
										<th width="200">E-mail cc</th>
										<td><input type="text" name="emailcc" id="emailcc"
											size="50" value="${email_cc }"/>
<!-- 											<c:if test="${email_cc ne null and btnCek eq null}"> readonly="readonly" style="background-color: #E0E0E0" </c:if>> -->
											<em>*pemisah
												e-mail gunakan tanda ';'</em></td>
									</tr>
									<tr style="display: none;" id="further">
										<th width="200">Kategori Further*</th>
										<td><select name="kategori_further" id="kategori_further"
											title="Silahkan pilih kategori further" onchange="further()">
												<c:forEach var="c" items="${kategori_further}" varStatus="s">
													<option
														<c:if test="${select_kategori_further eq c.KATEGORI}"> SELECTED style="background-color: #E0E0E0" readonly="readonly"</c:if>
														value="${c.KATEGORI}">${c.KATEGORI}</option>
												</c:forEach>
										</select> <input type="text" name="other" id="other"
											style="visibility: hidden;" size="50"
											onkeyup="handleEnter(this, event);" />
										</td>
									</tr>
									<tr style="display: none;" id="keterangan">
										<th width="200">Keterangan*</th>
										<td><textarea cols="50" rows="7"
												name="keterangan_further" id="keterangan_further">${keterangan_further}</textarea>
<!-- 												<c:if test="${keterangan_further ne null and btnCek eq null}"> readonly="readonly" style="background-color: #E0E0E0"</c:if>>${keterangan_further}</textarea> -->
										</td>
									</tr>
									<tr>
										<td colspan="2">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="2"><input type="submit" id="btnSave1"
											name="btnSave1" value="Save" /> <input type="submit"
											id="btnCancel1" name="btnCancel1" value="Cancel" /> <input
											type="hidden" name="pilihan" id="pilihan" value="${pilihan}">
											<input type="hidden" name="flag_form" id="flag_form"
											value="${flag_form}"> <input type="hidden" name="cek"
											id="cek" value="${cek}"> <%-- <input type="hidden" name="flag_awal" id="flag_awal" value="${flag_awal}">   --%>
										</td>
									</tr>
								</table>
							</fieldset>
						</form></td>
					<td valign="top">
						<form name="formpostSearch" id="formpostSearch" method="post"
							style="display: visible;">
							<fieldset>

								<legend>Cari SPAJ Untuk Edit Ke Further</legend>

								<table class="entry2">
									<tr>
										<th width="200">
										<select name="searchList" id="searchList"
											title="Pilih Salah satu">											
												<option value="noBlankoSL" <c:if test="${searchList eq 'noBlankoSL'}">selected="selected"</c:if>>No Blanko</option>
												<option value="pemegangPolSL" <c:if test="${searchList eq 'pemegangPolSL'}">selected="selected"</c:if>>Pemegang Polis</option>

										</select></th>
										<td><input type="text" id="searchText" name="searchText"
											size="50" value="${searchText}" />
										</td>
									</tr>

									<tr>
										<td colspan="2">&nbsp;</td>
									</tr>

									<tr>
										<td>&nbsp;</td>
										<td align="right"><input type="button" id="btnSearch"
											name="btnSearch" value="Search" />
										</td>
									</tr>
										<tr>
										<td colspan="2">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="2" height="25">&nbsp;</td>
									</tr>

								</table>

							</fieldset>
						</form></td>
				</tr>
			</table>

			<form name="formpost2" id="formpost2" method="post" target="_blank" style="display: visible;">
				<fieldset>
				<legend>Report SPAJ Masuk dan Status SPAJ</legend>
				<table class="entry2">
					<tr>
						<th width="200">Periode</th>
						<td width="300">
							<input type="text" id="bdate2" name="bdate2" value="${bdate}" class="datepicker"> s/d 
							<input type="text" id="edate2" name="edate2" value="${edate}" class="datepicker">
						</td>
						<th width="150">
							<input type="radio" style="border: none;" name="jn_report3" id="jn_report3" value="pdf" size="10" checked="checked" />PDF
							<input type="radio" style="border: none;" name="jn_report3" id="jn_report3" value="xls" size="10" />Excel 
						</th>
						<td>
							<input type="submit" id="btnReport2" name="btnReport2" value="View Report"/>
							<input type="button" id="btnTampil" name="btnTampil" value="View Data"/>
							
						</td>
					</tr>
				</table>
				</fieldset>	
				<br/>
			</form>
			
			<form name="formpost6" id="formpost6" method="post" target="_blank" style="display: none;">
				<fieldset>
				<legend>Report SPAJ Masuk dan Status SPAJ</legend>
				<table class="entry2">
					<tr>
						<th width="200">Periode</th>
						<td width="300">
							<input type="text" id="bdate1" name="bdate1" value="${bdate1}" class="datepicker"> s/d 
							<input type="text" id="edate1" name="edate1" value="${edate1}" class="datepicker">
						</td>
						<th width="150">
							<input type="radio" style="border: none;" name="jn_report1" id="jn_report1" value="pdf" size="10" checked="checked" />PDF
							<input type="radio" style="border: none;" name="jn_report1" id="jn_report1" value="xls" size="10" />Excel 
						</th>
						<td>
							<input type="submit" id="btnReport1" name="btnReport1" value="View Report"/>
						</td>
					</tr>
				</table>
				</fieldset>	
				<br/>
			</form>				
				
			<c:if test="${not empty daftar_spaj_agency_further}">
				<form name="formpost7" id="formpost7" method="post" style="display: none;">
					<fieldset>
					<legend>List SPAJ Masuk Hari Ini</legend>
					<table class="entry2">
						<tr>
							<th><input type="checkbox" name="allCheck2" onclick="selectallMe2()" style="border: none;"></th>
						    <th>NO BLANKO</th>
						    <th>KODE AGEN</th>
						    <th>NAMA AGEN</th>
						    <th>NAMA PEMEGANG</th>
						    
						    <th>TGL FURTHER</th>
						    <th>JENIS FURTHER</th>
						    <th>TGL BERKAS DIKEMBALIKAN KE AGEN</th>
						    
						    <th>NAMA CABANG</th>
						    <th>KETERANGAN</th>
						    
						  </tr>
						  <c:forEach items="${daftar_spaj_agency_further }" var="d" varStatus="s">
						 	  <tr>
						 	  	<th align="center"><input name="chbox" type="checkbox" id="chbox" value="${d.ID_DOK}" style="border: none;"></th>
							    <td>${d.ID_DOK }</td>
							    <td>${d.MSAG_ID }</td>
							    <td>${d.MCL_FIRST }</td>
							    <td>${d.NAME_CUSTOMER }</td>
							    
							    <td>${d.TGL_FURTHER }</td>
							    <td>${d.JENIS_FURTHER }</td>
							    <td>${d.TGL_KEMBALI_KE_AGEN }</td>
							    
							    <td>${d.LSRG_NAMA }</td>
							     <td>${d.KETERANGAN_FURTHER }</td>
							    
							  </tr>
						  </c:forEach>
					</table>
					<br/>
					<input type="submit" id="btnDel1" name="btnDel1" value="Delete"/>
					</fieldset>
				</form>
			</c:if>			
			
			<c:if test="${not empty daftar_spaj_agency_nonfurther}">
				<form name="formpost8" id="formpost8" method="post" style="display: none;">
					<fieldset>
					<legend>List SPAJ Masuk Hari Ini</legend>
					<table class="entry2">
						<tr>
							<th rowspan="2"><input type="checkbox" name="allCheck" onclick="selectallMe()" style="border: none;"></th>
						    <th rowspan="2">NO</th>
						    <th rowspan="2">NO BLANKO</th>
						    <th rowspan="2">TGL INPUT</th>
						    <th rowspan="2">USER INPUT</th>
						    <th rowspan="2">INFORMASI</th>
						    <th rowspan="2">KODE AGEN</th>
						    <th rowspan="2">NAMA AGENT</th>
						    <th rowspan="2">PEMEGANG POLIS</th>
						    <th rowspan="2">TERTANGGUNG</th>
						    <th rowspan="2">NAMA LEADER</th>
						    <th rowspan="2">PLAN</th>
						    <th colspan="2">UP</th>
						    <th rowspan="2">CARA BAYAR</th>
						    <th rowspan="2">PREMI</th>
						    <th rowspan="2">TOP UP</th>
						    <th rowspan="2">TOP UP SINGLE</th>
						    
						    <th rowspan="2">TGL PRODUCTION</th>
						    <th rowspan="2">TANGGAL BAYAR</th>
						    <th rowspan="2">NOMOR REGISTRASI</th>
						    <th rowspan="2">NOMOR POLIS</th>
						    <th rowspan="2">TANGGAL STATUS SPAJ</th>
						    <th rowspan="2">STATUS SPAJ</th>
						    <th rowspan="2">TGL FURTHER AWAL</th>
						    <th rowspan="2">HISTORY FURTHER AWAL</th>
						  </tr>
						  <tr>
						    <th>US$</th>
						    <th>Rp</th>
						  </tr>
						  <c:forEach items="${daftar_spaj_agency_nonfurther }" var="d" varStatus="s">
						 	  <tr>
						 	  	<th align="center"><input name="chbox" type="checkbox" id="chbox" value="${d.ID_DOK}" style="border: none;"></th>
							    <td>${s.index+1 }.</td>
							    <td>${d.ID_DOK }</td>
							    <td>${d.TGL_INPUT }</td>
							    <td>${d.USER_MONITORING }</td>
							    <td>${d.INFORMASI }</td>
							    <td>${d.MSAG_ID }</td>
							    <td>${d.NAMA_AGEN }</td>
							    <td>${d.NAME_CUSTOMER }</td>
							    <td>${d.TERTANGGUNG }</td>
							    <td>${d.FDM }</td>
							    <td>${d.PLAN }</td>
							    <td><fmt:formatNumber value="${d.UP_DOLLAR }" /></td>
							    <td><fmt:formatNumber value="${d.UP_RUPIAH }" /></td>
							    <td>${d.CARA_BAYAR }</td>
							    <td><fmt:formatNumber value="${d.PREMI }" /></td>
							    <td><fmt:formatNumber value="${d.TOPUP }" /></td>
							    <td><fmt:formatNumber value="${d.TOPUP_SINGLE }" /></td>
							   
							    <td>${d.TGL_PROD }</td>
							    <td><fmt:formatDate value="${d.MSTE_BEG_DATE }" pattern="dd/MM/yyyy" /></td>
							    <td>${d.NO_REGISTRASI }</td>
							    <td>${d.POLIS }</td>
							     <td>${d.TGL_KETERANGAN}</td>
							    <td>${d.KETERANGAN}</td>
							     <td>${d.TGL_FURTHER} </td>
							     <td>${d.KETERANGAN_FURTHER} </td>
							    <td>
							    
							    <input type="hidden" name="statEditToFurt" id="statEditToFurt" value="${statEditToFurt}" >
							     <input type="hidden" name="btnEditToFurtA+${d}" id="btnEditToFurtA+${d}" title="Edit To Further" value="${d.ID_DOK}" >
							    <input type="button" name="btnEditToFurt" id="btnEditToFurt" title="Edit To Further" value="Edit To Further"  onclick="return EditToFurt(document.getElementById('btnEditToFurtA+${d}').value);">	
							   				   					    
<!-- 							     <input type="button" name="btnEditToFurtb" id="btnEditToFurtb" title="Edit To FurtherB" value="${d.ID_DOK }" > -->
							   
							    <td>
							   					    
							  	
								</td>
							  </tr>
						  </c:forEach>
					</table>
					<br/>
					<input type="submit" id="btnDel2" name="btnDel2" value="Delete"/>
<!-- 					<input type="submit" id="btnEdit" name="btnEdit" value="Edit to Further"/> -->
					</fieldset>
				</form>
			</c:if>
				
		</div>	
			
<!-- 			<div id="tabs-2"> -->
<!-- 				<c:if test="${not empty daftar_spaj_blm_input }"> -->
<!-- 					<form name="formpost4" id="formpost4" method="post"> -->
<!-- 						<fieldset> -->
<!-- 						<legend>List SPAJ belum diinput</legend> -->
<!-- 						<table class="entry2"> -->
<!-- 							<tr> -->
<!-- 								<th rowspan="2">TGL INPUT</th> -->
<!-- 								<th rowspan="2">CABANG</th> -->
<!-- 								<th rowspan="2">USER</th> -->
<!-- 							    <th rowspan="2">NO BLANKO</th> -->
<!-- 							    <th rowspan="2">KODE AGEN</th> -->
<!-- 							    <th rowspan="2">NAMA AGENT</th> -->
<!-- 							    <th rowspan="2">PEMEGANG POLIS</th> -->
<!-- 							    <th rowspan="2">TERTANGGUNG</th> -->
<!-- 							    <th rowspan="2">NAMA LEADER</th> -->
<!-- 							    <th rowspan="2">PLAN</th> -->
<!-- 							    <th colspan="2">UP</th> -->
<!-- 							    <th rowspan="2">CARA BAYAR</th> -->
<!-- 							    <th rowspan="2">PREMI</th> -->
<!-- 							    <th rowspan="2">TOP UP</th> -->
<!-- 							    <th rowspan="2">TOP UP SINGLE</th> -->
<!-- 							    <th rowspan="2">APE</th> -->
<!-- 							    <th rowspan="2">UPP EVALUASI</th> -->
<!-- 							    <th rowspan="2">TGL PRODUCTION</th> -->
<!-- 							    <th rowspan="2">TANGGAL BAYAR</th> -->
<!-- 							    <th rowspan="2">NOMOR REGISTRASI</th> -->
<!-- 							    <th rowspan="2">NOMOR POLIS</th> -->
<!-- 							    <th rowspan="2">KETERANGAN</th> -->
<!-- 							  </tr> -->
<!-- 							  <tr> -->
<!-- 							    <th>US$</th> -->
<!-- 							    <th>Rp</th> -->
<!-- 							  </tr> -->
<!-- 							  <c:forEach items="${daftar_spaj_blm_input }" var="d" varStatus="s"> -->
<!-- 							 	  <tr> -->
<!-- 							 	  	<td><fmt:formatDate value="${d.CREATE_DATE }" pattern="dd/MM/yyyy" /></td> -->
<!-- 							 	  	<td>${d.CABANG }</td> -->
<!-- 								    <td>${d.USER_MONITORING }</td> -->
<!-- 								    <td>${d.ID_DOK }</td> -->
<!-- 								    <td>${d.MSAG_ID }</td> -->
<!-- 								    <td>${d.NAMA_AGEN }</td> -->
<!-- 								    <td>${d.NAME_CUSTOMER }</td> -->
<!-- 								    <td>${d.TERTANGGUNG }</td> -->
<!-- 								    <td>${d.FDM }</td> -->
<!-- 								    <td>${d.PLAN }</td> -->
<!-- 								    <td><fmt:formatNumber value="${d.UP_DOLLAR }" /></td> -->
<!-- 								    <td><fmt:formatNumber value="${d.UP_RUPIAH }" /></td> -->
<!-- 								    <td>${d.CARA_BAYAR }</td> -->
<!-- 								    <td><fmt:formatNumber value="${d.PREMI }" /></td> -->
<!-- 								    <td><fmt:formatNumber value="${d.TOPUP }" /></td> -->
<!-- 								    <td><fmt:formatNumber value="${d.TOPUP_SINGLE }" /></td> -->
<!-- 								    <td><fmt:formatNumber value="${d.APE }" /></td> -->
<!-- 								    <td><fmt:formatNumber value="${d.UPP_EVA }" /></td> -->
<!-- 								    <td><fmt:formatDate value="${d.TGL_PROD }" pattern="dd/MM/yyyy" /></td> -->
<!-- 								    <td><fmt:formatDate value="${d.MSTE_BEG_DATE }" pattern="dd/MM/yyyy" /></td> -->
<!-- 								    <td>${d.NO_REGISTRASI }</td> -->
<!-- 								    <td>${d.POLIS }</td> -->
<!-- 								    <td>${d.KETERANGAN }</td> -->
<!-- 								  </tr> -->
<!-- 							  </c:forEach> -->
<!-- 						</table> -->
<!-- 						</fieldset> -->
<!-- 					</form> -->
<!-- 				</c:if> -->
<!-- 			</div> -->
			
<!-- 			<div id="tabs-3"> -->
<!-- 				<form name="formpost9" id="formpost9" method="post"> -->
<!-- 					<fieldset> -->
<!-- 					<legend>Report Penerimaan Berkas Lengkap dan Pending</legend> -->
<!-- 					<table class="entry2"> -->
<!-- 						<tr> -->
<!-- 							<th width="200">Periode</th> -->
<!-- 							<td width="300"> -->
<!-- 								<input type="text" id="bdate" name="bdate" value="${bdate}" class="datepicker"> s/d  -->
<!-- 								<input type="text" id="edate" name="edate" value="${edate}" class="datepicker"> -->
<!-- 							</td> -->
<!-- 							<th width="150"> -->
<!-- 								<input type="radio" style="border: none;" name="jn_report" id="jn_report2" value="pdf" size="10" checked="checked" />PDF -->
<!-- 								<input type="radio" style="border: none;" name="jn_report" id="jn_report1" value="xls" size="10" />Excel  -->
<!-- 							</th> -->
<!-- 							<td> -->
<!-- 								<input type="submit" id="btnReport" name="btnReport" value="View Report"/> -->
<!-- 							</td> -->
<!-- 						</tr> -->
<!-- 					</table> -->
<!-- 					</fieldset> -->
<!-- 				</form> -->
<!-- 			</div> -->
			
<!-- 			<div id="tabs-4"> -->
<!-- 				<c:if test="${not empty daftar_spaj_agency_further2}"> -->
<!-- 				<form name="formpost9" id="formpost9" method="post"> -->
<!-- 					<fieldset> -->
<!-- 					<legend>List SPAJ Masuk Hari Ini</legend> -->
<!-- 					<table class="entry2"> -->
<!-- 						<tr> -->
<!-- 						    <th>NO BLANKO</th> -->
<!-- 						    <th>KODE AGEN</th> -->
<!-- 						    <th>NAMA AGEN</th> -->
<!-- 						    <th>NAMA PEMEGANG</th> -->
<!-- 						    <th>TGL TERIMA BERKAS</th> -->
<!-- 						    <th>TGL FURTHER</th> -->
<!-- 						    <th>JENIS FURTHER</th> -->
<!-- 						    <th>TGL BERKAS DIKEMBALIKAN KE AGEN</th> -->
<!-- 						    <th>TGL TERIMA BERKAS PENDING DARI AGEN</th> -->
<!-- 						    <th>TGL INPUT SPAJ</th> -->
<!-- 						    <th>NAMA CABANG</th> -->
<!-- 						  </tr> -->
<!-- 						  <c:forEach items="${daftar_spaj_agency_further2 }" var="d" varStatus="s"> -->
<!-- 						 	  <tr> -->
<!-- 							    <td>${d.ID_DOK }</td> -->
<!-- 							    <td>${d.MSAG_ID }</td> -->
<!-- 							    <td>${d.MCL_FIRST }</td> -->
<!-- 							    <td>${d.NAME_CUSTOMER }</td> -->
<!-- 							    <td>${d.MSTE_TGL_TERIMA_ADMIN }</td> -->
<!-- 							    <td>${d.TGL_FURTHER }</td> -->
<!-- 							    <td>${d.JENIS_FURTHER }</td> -->
<!-- 							    <td>${d.TGL_KEMBALI_KE_AGEN }</td> -->
<!-- 							    <td>${d.TGL_TERIMA_DARI_AGEN }</td> -->
<!-- 							    <td>${d.MSTE_INPUT_DATE }</td> -->
<!-- 							    <td>${d.NAMA_CABANG }</td> -->
<!-- 							  </tr> -->
<!-- 						  </c:forEach> -->
<!-- 					</table> -->
<!-- 					<br/> -->
<!-- 					</fieldset> -->
<!-- 				</form> -->
<!-- 				</c:if>					 -->
<!-- 			</div> -->
		</div>
	</body>
</html>
