<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<script type="text/javascript" src="${path }/include/js/refund/refund_common.js"></script>
		<script>
			function tampilkan(idx, btn){
				if(idx >= 0){
					if(btn.name == 'save')		document.getElementById('daftarTravelIns['+idx+'].flag').value = 1;
					if(btn.name == 'transfer')	document.getElementById('daftarTravelIns['+idx+'].flag').value = 2;
					if(btn.name == 'aksep')		document.getElementById('daftarTravelIns['+idx+'].flag').value = 3;
				}
				document.getElementById('submitMode').value = btn.name;
				btn.disabled=true;
				document.getElementById('formpost').submit();
			}
			
			function calculateNominals(idx){
				var prm_str	= document.getElementById('daftarTravelIns['+idx+'].msti_premi_setor').value;
				prm_str		= prm_str.replace(/,/g, '');
				var jml		= document.getElementById('daftarTravelIns['+idx+'].msti_jml_peserta').value;
				jml			= jml.replace(/,/g, '');
				if(!(isNaN(prm_str) || isNaN(jml))){
					document.getElementById('daftarTravelIns['+idx+'].msti_premi').value	= formatCurrency(prm_str * 2);
					document.getElementById('daftarTravelIns['+idx+'].msti_up').value 		= formatCurrency(prm_str * 10000);
				}else{
					alert('Mohon masukkan nilai yang valid pada kolom JUMLAH PESERTA dan PREMI DISETOR');
				}
			}
			
			function awal(){
				<c:if test="${not empty param.sukses}">
					alert('${param.sukses}');
				</c:if>
			}
			
			hideLoadingMessage();
		</script>
	</head>

	<body onload="setupPanes('container1', 'tab1'); awal();" style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Input Data Global Travel Insurance</a>
				</li>
			</ul>
	
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form:form commandName="cmd" id="formpost" method="post" cssStyle="text-align: left">
						<form:hidden id="submitMode" path="submitMode"/>
						<table class="entry2" style="width: auto">
							<tr>
								<th style="width: 135px;">Tanggal:</th>
								<td>
									<spring:bind path="cmd.tglAwal"><script>inputDate('${status.expression}', '${status.value}', false);</script></spring:bind> s/d 
									<spring:bind path="cmd.tglAkhir"><script>inputDate('${status.expression}', '${status.value}', false);</script></spring:bind> 
								</td>
								<th style="width: 120px;">Posisi:</th>
								<td>
									<form:select path="i" items="${listPosisi}" itemValue="key" itemLabel="value" />
								</td>
							</tr>
							<tr>
								<th></th>
								<td colspan="3">
									<spring:hasBindErrors  name="cmd">
										<div id="error" style="text-transform: none;">
											<form:errors path="*" />
										</div>
									</spring:hasBindErrors>
									<input type="button" name="show" value="Show" onclick="tampilkan(-1, this);">
								</td>
							</tr>
						</table>
						<c:forEach var="t" items="${cmd.daftarTravelIns}" varStatus="s">
							<fieldset style="text-align: left;">
								<c:choose>
									<c:when test="${not empty t.msti_id}">
										<legend title="[Bandara | Tanggal RK | Jumlah Peserta | Total Premi]">[${t.lsb_code} | <fmt:formatDate value="${t.msti_tgl_rk}" pattern="dd/MM/yyyy"/> | ${t.msti_jml_peserta} | <fmt:formatNumber value="${t.msti_premi}" />]</legend>
									</c:when>
									<c:otherwise><legend>[INPUT BARU]</legend></c:otherwise>
								</c:choose>
								<table class="entry2" style="width:auto;">
									<tr>
										<th style="width: 135px;">Bandara:<font class="error">*</font></th>
										<td>
											<form:select path="daftarTravelIns[${s.index}].lsb_id" cssErrorClass="inpError">
												<form:option value=""/>
												<form:options items="${listBandara}" itemValue="key" itemLabel="value"/>
											</form:select>
										</td>
										<th>Premi Disetor:<font class="error">*</font></th>
										<td>Rp. <form:input path="daftarTravelIns[${s.index}].msti_premi_setor" size="20" cssStyle="text-align: right" cssErrorClass="inpError" onchange="calculateNominals(${s.index});" /></td>
										<th style="width: 100px;">User Input:</th>
										<td><form:input path="daftarTravelIns[${s.index}].user_input" size="12" readonly="true" cssClass="readOnly" tabindex="-1"/></td>
									</tr>
									<tr>
										<th>Tgl Mulai Asuransi:<font class="error">*</font></th>
										<td>
											<spring:bind path="cmd.daftarTravelIns[${s.index}].msti_tgl"><script>inputDate('${status.expression}', '${status.value}', false);</script></spring:bind>
										</td>
										<th style="width: 100px;">Premi:</th>
										<td>Rp. <form:input path="daftarTravelIns[${s.index}].msti_premi" size="20" readonly="true" cssClass="readOnly" tabindex="-1" cssStyle="text-align: right"/></td>
										<th style="width: 100px;">Tgl Input:</th>
										<td><form:input path="daftarTravelIns[${s.index}].msti_tgl_input" size="12" readonly="true" cssClass="readOnly" tabindex="-1"/></td>
									</tr>
									<tr>
										<th>Tgl RK:<font class="error">*</font></th>
										<td>
											<spring:bind path="cmd.daftarTravelIns[${s.index}].msti_tgl_rk"><script>inputDate('${status.expression}', '${status.value}', false);</script></spring:bind>
										</td>
										<th>UP:</th>
										<td>Rp. <form:input path="daftarTravelIns[${s.index}].msti_up" size="20" readonly="true" cssClass="readOnly" tabindex="-1" cssStyle="text-align: right"/></td>
										<th style="width: 100px;">Tgl Aksep:</th>
										<td><form:input path="daftarTravelIns[${s.index}].msti_tgl_aksep" size="12" readonly="true" cssClass="readOnly" tabindex="-1"/></td>
									</tr>
									<tr>
										<th>Jumlah Peserta:<font class="error">*</font></th>
										<td><form:input path="daftarTravelIns[${s.index}].msti_jml_peserta" size="5" onchange="calculateNominals(${s.index});" cssErrorClass="inpError"/> orang <c:if test="${not empty t.msti_id}"><font class="error">(${t.validasi.jml_peserta_terinput} sudah diinput)</font></c:if></td>
									</tr>
									<tr>
										<th></th>
										<td colspan="5">
											<input id="daftarTravelIns[${s.index}].flag" name="daftarTravelIns[${s.index}].flag" type="hidden" value="0"/>
											<form:hidden path="daftarTravelIns[${s.index}].msti_id" />

											<c:choose>
												<c:when test="${t.msti_posisi eq 1}">
													<input type="button" name="save" value="Simpan" title="Simpan Data" onclick="tampilkan(${s.index}, this);">
													<input type="button" name="inputpeserta" value="Input Peserta" title="Input Peserta Travel Insurance" onclick="popWin('${path}/bas/travelinsdet.htm?msti_id=${t.msti_id}&msti_jenis=${t.msti_jenis}&info=[${t.lsb_code} | <fmt:formatDate value="${t.msti_tgl_rk}" pattern="dd/MM/yyyy"/> | ${t.msti_jml_peserta} | <fmt:formatNumber value="${t.msti_premi}" />]', 600, 800);" <c:if test="${empty t.msti_id}">disabled</c:if>>
													<c:choose>
														<c:when test="${sessionScope.currentUser.lde_id eq '11'}">
															<input type="button" name="aksep" value="Akseptasi" title="Akseptasi (masuk produksi)" onclick="tampilkan(${s.index}, this);" <c:if test="${empty t.msti_id}">disabled</c:if>>
														</c:when>
														<c:otherwise>
															<input type="button" name="transfer" value="Transfer" title="Transfer ke Filling (Tidak bisa diedit lagi!)" onclick="tampilkan(${s.index}, this);" <c:if test="${empty t.msti_id}">disabled</c:if>>
														</c:otherwise>
													</c:choose>
												</c:when>
												<c:otherwise>
													<input type="button" name="inputpeserta" value="Data Peserta" title="Data Peserta Travel Insurance" onclick="popWin('${path}/bas/travelinsdet.htm?msti_id=${t.msti_id}&msti_jenis=${t.msti_jenis}&info=[${t.lsb_code} | <fmt:formatDate value="${t.msti_tgl_rk}" pattern="dd/MM/yyyy"/> | ${t.msti_jml_peserta} | <fmt:formatNumber value="${t.msti_premi}" />]', 600, 800);" <c:if test="${empty t.msti_id}">disabled</c:if>>
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
								</table>
							</fieldset>
						</c:forEach>
					</form:form>
				</div>
			</div>
		</div>
	</body>
	<script>
	</script>
</html>