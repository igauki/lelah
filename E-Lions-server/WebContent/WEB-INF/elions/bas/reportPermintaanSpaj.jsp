<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-blue.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript">
			function cari(){
				lca_id=formpost.lca_id.value;
				lus_id=formpost.lus_id.value;
				tgl1=formpost.tanggalAwal.value;
				tgl2=formpost.tanggalAkhir.value;
				jn_report=document.getElementById('jn_report').value; 
				jenis=document.getElementById('lsJenis').value; 
				popWin('${path}/report/bas.pdf?window=report_permintaanSpaj&lca_id='+lca_id+'&lus_id='+lus_id+'&tanggalAwal='+tgl1+'&tanggalAkhir='+tgl2+"&jenis="+jenis+"&jn_report="+jn_report,600,800);
			}
			
			function lock(value) {
				if(value != 0) {
					document.getElementById('lsJenis')[0].selected = 'selected';
					document.getElementById('lsJenis').disabled = true;
				}else {
					document.getElementById('lsJenis').disabled = false;
					
				}
			}	
			
			hideLoadingMessage();	
		</script>
		
	</head>
  
	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Permintaan Spaj dari Cabang ke Pusat</a>
				</li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form id="formpost" method="post">
						<table class="entry2">
							<tr>
								<th width="10%">Tanggal :</th>
								<td width="30%" colspan="2">
									<script>inputDate('tanggalAwal', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>
									 s/d 
									 <script>inputDate('tanggalAkhir', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>
								</td>
								<td align="left" rowSpan="3"></td>
							</tr>
							<tr>
								<th>Nama Cabang</th>
								<td colspan="2">
									<select name="lca_id" <c:if test="${lock eq 1 }">disabled</c:if> onchange="lock(this.value)">
										<c:forEach items="${lsCabang}" var="x" varStatus="xt">
											<option value="${x.KEY}" 
													<c:if test="${x.KEY eq lca_id}">selected</c:if> >
													${x.VALUE}
											</option>
										</c:forEach>
									</select>&nbsp;&nbsp;&nbsp;
									<select id="lsJenis" name="lsJenis" <c:if test="${lock eq 1 }">disabled</c:if>>
										<option value="0">All</option>
										<c:forEach var="d" items="${lsJenis}">
											<option value="${d.LSJS_PREFIX}">${d.LSJS_DESC}</option>
										</c:forEach>
									</select>	
								</td>
								
							</tr>
							<tr>
								<th>Nama Admin</th>
								<td>
									<input type="hidden" name="pil">
									<select name="lus_id">
										<option value="0">---All---</option>
										<c:forEach items="${lsAdmin}" var="x">
											<%-- <option value="${x.KEY}" <c:if test="${x.KEY eq lus_id}">selected</c:if> >${x.VALUE}</option> --%>
											<option value="${x.KEY}" >${x.VALUE}</option>
										</c:forEach>
									</select>	
								</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<th>Jenis</th>
								<td>
									<select name="jn_report" id="jn_report">
										<option value="0">All</option>
										<option value="1">Rekap</option>
									</select>	
								</td>
								<td><input type="button" name="btnCari" value="Cari" onClick="cari();"></td>
								<td>&nbsp;</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>
