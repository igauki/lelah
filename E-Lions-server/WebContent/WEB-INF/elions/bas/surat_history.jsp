<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>	
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-blue.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<script type="text/javascript">
			hideLoadingMessage();
			
			function tampilkan(btn) {
				document.getElementById('submitMode').value = btn.name;
				document.getElementById('formpost').submit();	
			}
			
		    function maxChar(e,string){
				var count = string.length + 1;
// 				document.getElementById('charCount').value = count; 
				if(count > 150 ) {
					alert('input tidak bisa lebih dari 150 karakter');
					return false;
				}
				return true;
			}			
			
			<c:if test="${not empty cmd.pesan}">
				alert('${cmd.pesan}');
			</c:if>
		</script>	
		
	</head>
	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Input Surat History</a>
				</li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form id="formpost" method="post" enctype="multipart/form-data">
						<table class="entry2" width="100%" border="0">
							<tr>
								<th width="10%">Tgl Kirim :</th>
								<td width="40%">
									<spring:bind  path="cmd.daftarSuratHist[0].tanggalAwal">
										<script>inputDate('${status.expression}', '${status.value}', false);</script>
									</spring:bind>
									<%--<script>inputDate('tanggalAwal', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>--%>
									 s/d
									 <spring:bind  path="cmd.daftarSuratHist[0].tanggalAkhir">
										<script>inputDate('${status.expression}', '${status.value}', false);</script>
									</spring:bind> 
									 <%--<script>inputDate('tanggalAkhir', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>--%>
								</td>
								<td width="50%">
									&nbsp;
								</td>
							</tr>
							<tr>
								<th width="10%">Jenis</th>
								<td width="90%">
								<spring:bind  path="cmd.daftarSuratHist[0].jenis">
									<input type="radio" name="${status.expression}" id="jenis1" value="0" <c:if test="${cmd.daftarSuratHist[0].jenis eq 0}">checked</c:if>>Outstanding
									<input type="radio" name="${status.expression}" id="jenis2" value="1" <c:if test="${cmd.daftarSuratHist[0].jenis eq 1}">checked</c:if>>All
									<input type="radio" name="${status.expression}" id="jenis3" value="2" <c:if test="${cmd.daftarSuratHist[0].jenis eq 2}">checked</c:if>>Retur Kwitansi Premi Lanjutan (KPL)
								</spring:bind>
								</td>
							</tr>
							<tr>
								<th width="10%"></th>
								<td width="90%">
									<input type="button" name="btnCari" value="Retrieve" onclick="return tampilkan(this);">
									<input type="button" name="btnReport" value="Report" onclick="return tampilkan(this);">
									<input type="hidden" name="submitMode" id="submitMode">
								</td>
							</tr>
							<c:if test="${not empty cmd.daftarSuratHist[0].mssg}">
								<tr>
									<td colspan="3">
										<div id="error" style="width: 20%;margin-left:3px;border: 1px solid #006FB5;background-color: #D7EBFF;color: #000000;">
											${cmd.daftarSuratHist[0].mssg}
										</div>
									</td>
								</tr>
							</c:if>	
							<tr>
								<td colspan="3">
									<c:choose>
										<c:when test="${cmd.daftarSuratHist[0].mode eq 2 }">
											<c:if test="${not empty cmd.daftarSuratHist[0].tanggalAwal and not empty cmd.daftarSuratHist[0].tanggalAkhir}">
												<input type="button" id="btnShow" name="btnShow" value="Show" onclick="return tampilkan(this);"><br>
												<form:form commandName="cmd" id="formpost" method="post" enctype="multipart/form-data">
													<table class="displaytag" style="margin-top: 5px; width: 800px;">
														<tr>
															<th style="width: 120px;">No Polis</th>
															<th>Description</th>
														</tr>
														<tr>
															<th style="width: 120px;" valign="top">
																<select id="polis" name="polis" style="width: 110px;" size="${sessionScope.currentUser.comboBoxSize}" onclick="document.getElementById('btnShow').click();">
																	<c:forEach items="${cmd.daftarSuratHist2 }" var="x" varStatus="s">
																		<option value="${x.mspo_policy_no}" <c:if test="${x.mspo_policy_no eq cmd.daftarSuratHist[0].mspo_policy_no}"> selected </c:if>>${x.mspo_policy_no}</option>
																	</c:forEach>
																</select>
																<br><br>Cari Nopol :<input type="text" id="polis2" name="polis2" />
																<br><br><input type="button" id="btnShowByPolis" name="btnShowByPolis" value="Cari" onclick="return tampilkan(this);">
															</th>
															<td valign="top">
																<c:forEach items="${cmd.daftarSuratHist}" var="x" varStatus="s" end="0">
																<!-- design baru -->
																<fieldset>
																  <legend>Data Retur KPL No Polis ${x.mspo_policy_no}</legend>
																  <table class="entry2">
																    <tr>
																      <th style="text-align: left; width: 200px;">No. Polis</th>
																      <td colspan="3" style="text-align: left;">${x.mspo_policy_no}</td>
																    </tr>
																    <tr>
																      <th style="text-align: left; width: 200px;">Nama Pempol</th>
																      <td colspan="3" style="text-align: left;">${x.nm_pp}</td>
																    </tr>
																    <tr>
																      <th style="text-align: left; width: 200px;">Cabang</th>
																      <td colspan="3" style="text-align: left;">${x.lca_nama}</td>
																    </tr>
																    <tr>
																      <th style="text-align: left; width: 200px;">Periode</th>
																      <td colspan="3" style="text-align: left;"><fmt:formatDate value="${x.periode}" pattern="dd/MM/yyyy"/></td>
																    </tr>
																    <tr>
																      <th style="text-align: left; width: 200px;">Tgl. Print</th>
																      <td style="text-align: left;"><fmt:formatDate value="${x.tgl_print}" pattern="dd/MM/yyyy"/></td>
																      <th style="text-align: left; width: 200px;">Tgl Kirim</th>
																      <td style="text-align: left;"><fmt:formatDate value="${x.tgl_kirim}" pattern="dd/MM/yyyy"/></td>
																    </tr>
																    <tr>
																      <th style="text-align: left; width: 200px;">User Kirim</th>
																      <td colspan="3" style="text-align: left;">${x.nama_user_kirim}</td>
																    </tr>
																  </table>
																</fieldset>
																
																<br/>
																
																<fieldset>
																  <table class="entry2">
																    <tr>
																      <th style="text-align: left; width: 200px;">Tgl. Retur</th>
																      <td style="text-align: left;"><fmt:formatDate value="${x.tgl_back}" pattern="dd/MM/yyyy"/></td>
																    </tr>
																    <tr>
																      <th style="text-align: left; width: 200px;">Alasan Retur</th>
																      <td style="text-align: left;">${x.alasan_back}</td>
																    </tr>
																    <tr>
																      <th style="text-align: left; width: 200px;">User Retur</th>
																      <td style="text-align: left;">${x.nama_user_back}</td>
																    </tr>
																  </table>
																</fieldset>
																
																<br/>
																<!-- end -->
																<fieldset>
																<legend>Followup KPL Retur</legend>
																<table class="entry2">
																	<!-- <tr>
																		<th style="text-align: left; width: 200px;">Email akan dikirim ke</th>
																		<td style="text-align: left;">
																			<input type="text" id="email" name="email" />
																		</td>
																	</tr> -->
																	<tr>
																		<th style="text-align: left; width: 200px;">Tanggal FU1</th>
																		<td style="text-align: left;"><fmt:formatDate value="${x.tgl_fu1}" pattern="dd/MM/yyyy"/></td>
																	</tr>
																	<tr>
																		<th style="text-align: left; width: 200px;">User FU1</th>
																		<td style="text-align: left;">${x.nama_user_fu1}</td>
																	</tr>
																	<tr>
																		<th style="text-align: left; width: 200px;">Kategori Followup</th>
																		<td style="text-align: left;">
																			<form:select id="ket_fu1_${s.index}" path="daftarSuratHist[${s.index}].ket_fu1" title="alasan">
																				<form:option label="" value=""/>
																				<form:options items="${lsAlasan}" itemLabel="key" itemValue="value"/>  
																			</form:select>
																		</td>
																	</tr>
																	<tr>
																		<th style="text-align: left; width: 200px;">Keterangan hasil followup</th>
																		<td style="text-align: left;">
																			<form:textarea rows="5" cols="50" id="ket2_fu1_${s.index}" path="daftarSuratHist[${s.index}].ket2_fu1" onkeypress="return maxChar(event,this.value)"/>
																		</td>
																	</tr>
																	<!-- Follow up 2 -->
																	<tr>
																		<th style="text-align: left; width: 200px;">Tanggal FU2</th>
																		<td style="text-align: left;"><fmt:formatDate value="${x.tgl_fu2}" pattern="dd/MM/yyyy"/></td>
																	</tr>
																	<tr>
																		<th style="text-align: left; width: 200px;">User FU2</th>
																		<td style="text-align: left;">${x.nama_user_fu2}</td>
																	</tr>
																	<tr>
																		<th style="text-align: left; width: 200px;">Kategori Followup</th>
																		<td style="text-align: left;">
																			<form:select id="ket_fu2_${s.index}" path="daftarSuratHist[${s.index}].ket_fu2" title="alasan">
																				<form:option label="" value=""/>
																				<form:options items="${lsAlasan}" itemLabel="key" itemValue="value"/>  
																			</form:select>
																		</td>
																	</tr>
																	<tr>
																		<th style="text-align: left; width: 200px;">Keterangan hasil followup</th>
																		<td style="text-align: left;">
																			<form:textarea rows="5" cols="50" id="ket2_fu2_${s.index}" path="daftarSuratHist[${s.index}].ket2_fu2" onkeypress="return maxChar(event,this.value)"/>
																		</td>
																	</tr>
																	<!-- Follow up 3 -->
																	<tr>
																		<th style="text-align: left; width: 200px;">Tanggal FU3</th>
																		<td style="text-align: left;"><fmt:formatDate value="${x.tgl_fu3}" pattern="dd/MM/yyyy"/></td>
																	</tr>
																	<tr>
																		<th style="text-align: left; width: 200px;">User FU3</th>
																		<td style="text-align: left;">${x.nama_user_fu3}</td>
																	</tr>
																	<tr>
																		<th style="text-align: left; width: 200px;">Kategori Followup</th>
																		<td style="text-align: left;">
																			<form:select id="ket_fu3_${s.index}" path="daftarSuratHist[${s.index}].ket_fu3" title="alasan">
																				<form:option label="" value=""/>
																				<form:options items="${lsAlasan}" itemLabel="key" itemValue="value"/>  
																			</form:select>
																		</td>
																	</tr>
																	<tr>
																		<th style="text-align: left; width: 200px;">Keterangan hasil followup</th>
																		<td style="text-align: left;">
																			<form:textarea rows="5" cols="50" id="ket2_fu3_${s.index}" path="daftarSuratHist[${s.index}].ket2_fu3" onkeypress="return maxChar(event,this.value)"/>
																		</td>
																	</tr>
																	<!-- end follow up -->
																	<tr>
																		<th style="text-align: left; width: 200px;">Jenis Surat</th>
																		<td style="text-align: left;">${x.nama}</td>
																	</tr>
																	<tr>
																		<th style="text-align: left; width: 200px;">Upload Bukti Pendukung</th>
																		<td style="text-align: left;"><input name="file1" type="file"/></td>
																	</tr>
																	<tr>
																		<th style="text-align: left;">&nbsp;</th>
																		<td style="text-align: left;">
																			<c:if test="${not empty cmd.daftarSuratHist[s.index].ls_attachment }">
																	    		<c:forEach items="${cmd.daftarSuratHist[s.index].ls_attachment }" var="at" varStatus="s">
																	    			<div style="font-style: italic;">
																	    				<a href="${path}/bas/kplanjutan.htm?window=downloadKpl&file=${at.dok}&spaj=${x.reg_spaj}" target="_blank">${at.dok }</a></div>
																	    		</c:forEach>
																    		</c:if>
																		</td>
																	</tr>
																	<tr>
																		<th>&nbsp;</th>
																		<td style="text-align: left;">
																			<input type="button" name="btnSimpan" value="Save" onclick="return tampilkan(this);">
																		</td>
																	</tr>
																</table>
																</fieldset>
																</c:forEach>
															</td>
														</tr>
													</table>
												</form:form>
												<script>
													<c:if test="${not empty cmd.daftarSuratHist[0].successMsg}">
														alert('${cmd.daftarSuratHist[0].successMsg}');
													</c:if>
												</script>
											</c:if>
										</c:when>
										<c:otherwise>
											<c:if test="${not empty cmd.daftarSuratHist[0].tanggalAwal and not empty cmd.daftarSuratHist[0].tanggalAkhir}">
												<form:form commandName="cmd" id="formpost" method="post">
													<table class="displaytag" style="margin-top: 5px">
														<tr>
															<td colspan="${fn:length(cmd.daftarSuratHist)}" style="text-align: left;padding: 5px;"><input type="button" name="btnSimpan" value="Save" onclick="return tampilkan(this);"></td>
														</tr>
														<tr>
															<th>No. Polis</th>
															<th>Nama Pemegang Polis</th>
															<th>Cabang</th>
															<th>Periode</th>
															<th>Tgl Print</th>
															<th>Tgl Kirim</th>
															<th>User Kirim</th>
															<th>Tgl Back</th>
															<th>User Back</th>
															<th>Alasan Back</th>
															<th>Tgl Kirim Back</th>
															<th>Ket Kirim Back</th>
															<th>Kirim Via</th>
															<th>Kirim Kembali Via</th>
															<th>Tgl Fu1</th>
															<th>User Fu1</th>
															<th>Ket Fu1</th>
															<th>Ket2 Fu1</th>
															<th>Tgl Fu2</th>
															<th>User Fu2</th>
															<th>Ket Fu2</th>
															<th>Ket2 Fu2</th>
															<th>Tgl Fu3</th>
															<th>User Fu3</th>
															<th>Ket Fu3</th>
															<th>Ket2 Fu3</th>
															<th>Jenis Surat</th>
														</tr>
														<c:forEach items="${cmd.daftarSuratHist}" var="x" varStatus="s">
															<tr>
																<td style="padding: 0px 3px 0px 3px">${x.mspo_policy_no}</td>
																<td style="padding: 0px 3px 0px 3px">${x.nm_pp}</td>
																<td style="padding: 0px 3px 0px 3px">${x.lca_nama}</td>
																<td style="padding: 0px 3px 0px 3px"><fmt:formatDate value="${x.periode}" pattern="dd/MM/yyyy"/></td>
																<td style="padding: 0px 3px 0px 3px"><fmt:formatDate value="${x.tgl_print}" pattern="dd/MM/yyyy"/></td>
																<td style="padding: 0px 3px 0px 3px"><fmt:formatDate value="${x.tgl_kirim}" pattern="dd/MM/yyyy"/></td>
																<td style="padding: 0px 3px 0px 3px">${x.nama_user_kirim}</td>
																<td style="padding: 0px 3px 0px 3px"><fmt:formatDate value="${x.tgl_back}" pattern="dd/MM/yyyy"/></td>
																<td style="padding: 0px 3px 0px 3px">${x.nama_user_back}</td>
																<td style="padding: 0px 3px 0px 3px">${x.alasan_back}</td>
																<td style="padding: 0px 3px 0px 3px"><fmt:formatDate value="${x.tgl_kirim}" pattern="dd/MM/yyyy"/></td>
																<td style="padding: 0px 3px 0px 3px">${x.ket_kirim_back}</td>
																<td style="padding: 0px 3px 0px 3px">${x.kirim_via}</td>
																<td style="padding: 0px 3px 0px 3px">${x.kirim_back_via}</td>
																<td style="padding: 0px 3px 0px 3px"><fmt:formatDate value="${x.tgl_fu1}" pattern="dd/MM/yyyy"/></td>
																<td style="padding: 0px 3px 0px 3px">${x.nama_user_fu1}</td>
																<td style="padding: 0px 3px 0px 3px">
																	<%--<form:input id="ket_fu1_${s.index}" path="daftarSuratHist[${s.index}].ket_fu1" />--%>
																	<form:select id="ket_fu1_${s.index}" path="daftarSuratHist[${s.index}].ket_fu1" title="alasan">
																		<form:option label="" value=""/>
																		<form:options items="${lsAlasan}" itemLabel="key" itemValue="value"/>  
																	</form:select>
																</td>
																<td style="padding: 0px 3px 0px 3px"><form:textarea rows="5" cols="20" id="ket2_fu1_${xt.index}" path="daftarSuratHist[${s.index}].ket2_fu1" onkeypress="return maxChar(event,this.value)"/></td>
																<td style="padding: 0px 3px 0px 3px"><fmt:formatDate value="${x.tgl_fu2}" pattern="dd/MM/yyyy"/></td>
																<td style="padding: 0px 3px 0px 3px">${x.nama_user_fu2}</td>
																<td style="padding: 0px 3px 0px 3px">
																	<%--<form:input id="ket_fu2_${s.index}" path="daftarSuratHist[${s.index}].ket_fu2" />--%>
																	<form:select id="ket_fu2_${s.index}" path="daftarSuratHist[${s.index}].ket_fu2" title="alasan">
																		<form:option label="" value=""/>
																		<form:options items="${lsAlasan}" itemLabel="key" itemValue="value"/>  
																	</form:select>
																</td>
																<td style="padding: 0px 3px 0px 3px"><form:textarea rows="5" cols="20" id="ket2_fu2_${xt.index}" path="daftarSuratHist[${s.index}].ket2_fu2" onkeypress="return maxChar(event,this.value)"/></td>
																<td style="padding: 0px 3px 0px 3px"><fmt:formatDate value="${x.tgl_fu3}" pattern="dd/MM/yyyy"/></td>
																<td style="padding: 0px 3px 0px 3px">${x.nama_user_fu3}</td>
																<td style="padding: 0px 3px 0px 3px">
																	<%--<form:input id="ket_fu3_${s.index}" path="daftarSuratHist[${s.index}].ket_fu3" />--%>
																	<form:select id="ket_fu3_${s.index}" path="daftarSuratHist[${s.index}].ket_fu3" title="alasan">
																		<form:option label="" value=""/>
																		<form:options items="${lsAlasan}" itemLabel="key" itemValue="value"/>  
																	</form:select>
																</td>
																<td style="padding: 0px 3px 0px 3px"><form:textarea rows="5" cols="20" id="ket2_fu3_${xt.index}" path="daftarSuratHist[${s.index}].ket2_fu3" onkeypress="return maxChar(event,this.value)"/></td>
																<td style="padding: 0px 3px 0px 3px">${x.nama}</td>
															</tr>
														</c:forEach>
													</table>
												</form:form>
												<script>
													<c:if test="${not empty cmd.daftarSuratHist[0].successMsg}">
														alert('${cmd.daftarSuratHist[0].successMsg}');
													</c:if>
												</script>
											</c:if>
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
						</table>
					</form>					
				</div>
			</div>				
		</div>	
	</body>
</html>
