<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<script>
			hideLoadingMessage();
		</script>
	</head>

	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Download Form</a>
				</li>
			</ul>
	
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<table class="entry2">
						<tr>	
							<th>Download:</th>
							<td>
								<ul style="text-transform: none;">
									<c:forEach items="${cmd.daftarForm}" var="f">
										<li><a href="${f.key}">${f.value}</a></li>
									</c:forEach>
								</ul>
							</td>
						</tr>	
						<tr>
							<th></th>
							<td>
								<div id="success" style="text-transform: none;">
									INFO:<br />
									- Untuk menyimpan, silahkan KLIK KANAN, lalu pilih "Save Link As" atau "Save Target As"
								</div>
							</td>
						</tr>				
					</table>
				</div>
			</div>
			
		</div>

	</body>
</html>