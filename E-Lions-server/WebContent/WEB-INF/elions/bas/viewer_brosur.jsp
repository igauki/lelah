<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<script>
				
			function tampilkan(btn){
				if(btn.name=='cariNew'){
					popWin('${path}/bas/spaj.htm?window=cariBrosur', 400, 600);
					return false
				}
			}
			
			hideLoadingMessage();
		</script>
	</head>

	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Viewer Brosur</a>
					<a href="#" onClick="return showPane('pane2', this)" id="tab2">History Posisi</a>
					<a href="#" onClick="return showPane('pane3', this)" id="tab3">Legend</a>
				</li>
			</ul>
	
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form id="formpost" method="post" action="${path}/bas/spaj.htm?window=viewerBrosur">
						<table class="entry2">
							<tr>
								<td style="vertical-align: top;">								
									<table class="entry2 width="100%">
										<tr>
											<td>
												<input type="hidden" name="posisi" id="posisi">
												Nomor Form: <input type="text" readOnly name="msf_id" id="msf_id" value="${cmd.msf_id}">
												<input type="button" id="cariNew" name="cariNew" value="CariNew" onclick="tampilkan(this);">
											</td>	
										</tr>	
										<tr>
											<td>
												<c:if test="${not empty cmd.daftarFormBrosur}">
													<fieldset>
														<legend>Detail Form</legend>
														<table class="entry2">
															<tr>
																<th>Nomor Form</th>
																<td>
																	<input type="text" value="${cmd.daftarFormBrosur[0].msf_id}" class="readOnly" readonly="true" size="20"/>
																</td>
															</tr>
															<tr>
																<th>Status</th>
																<td>
																	<input type="text" value="${cmd.daftarFormBrosur[0].status_form}" readonly="true" size="20" value="${cmd.daftarFormBrosur[0].status_form}" style="background-color: ${cmd.daftarFormBrosur[0].warna};">
																</td>
															</tr>
															<tr>
																<th>Cabang</th>
																<td>
																	<input type="text" class="readOnly" readonly="true" size="40" value="${cmd.daftarFormBrosur[0].lca_nama}"/>
																</td>
															</tr>
															<tr>
																<th>Pemohon</th>
																<td>
																	<input type="text" class="readOnly" readonly="true" size="40" value="${cmd.formHistUser.lus_login_name}"/>
																</td>
															</tr>
															<tr>
																<th>Detail Permintaan</th>
																<td>
																	<table class="displaytag" style="width:auto;">
																		<thead>
																			<tr>
																				<th rowspan="2">No.</th>
																				<th rowspan="2">Jenis Brosur</th>
																				<th rowspan="2">Prefix</th>
																				<th colspan="2">Jumlah Brosur</th>
																				<th rowspan="2">No. Blanko</th>	
																				<th rowspan="2">Status</th>
																			</tr>
																			<tr>
																				<th>Diminta</th>
																				<th>Disetujui</th>
																			</tr>
																		</thead>
																		<tbody>
																		<c:forEach var="daftar" items="${cmd.daftarFormBrosur}" varStatus="st">
																			<tr>
																				<td style="text-align: left;">${st.count}.</td>
																				<td style="text-align: left;">${daftar.lsjs_desc}</td>
																				<td style="text-align: center;">${daftar.lsjs_prefix}</td>
																				<td style="text-align: center;">
																					<input type="text" value="${daftar.msf_amount_req}" size="10" style="text-align: right;" class="readOnly" readonly="true"/>
																				</td>
																				<td style="text-align: center;">
																					<input type="text" value="${daftar.msf_amount}" size="10" style="text-align: right;" class="readOnly" readonly="true"/>
																				</td>
																				<td style="text-align: center;">
																					<c:if test="${daftar.start_no_blanko ne null }">
																						<input type="text" value="${daftar.start_no_blanko}" size="7" class="readOnly" readonly="true"/>
																						s/d
																						<input type="text" value="${daftar.end_no_blanko}" size="7" class="readOnly" readonly="true"/>
																					</c:if>	
																					<c:if test="${daftar.start_no_blanko eq null }">
																						<input type="text" value="${daftar.no_blanko_req}" size="50" class="readOnly" readonly="true"/>
																					</c:if>
																				</td>
																				<td style="text-align: center;">${daftar.status_form}</td>
																			</tr>
																		</c:forEach>
				
																		<tr>
																			<td colspan="3" style="text-align: center;">Total Brosur: </td>
																			<td style="text-align: center;">
																				?
																			</td>
																		</tr>
																		
																		</tbody>
																	</table>
																</td>
															</tr>
														</table>											
				
													</fieldset>									
												</c:if>
											</td>
										</tr>
									</table>
								</td>
							</tr>

						</table>
					</form>
				</div>
				<div id="pane2" class="panes">
					<fieldset>
						<legend>History Permintaan SPAJ</legend>
						<display:table id="detail" name="cmd.daftarFormHistory" class="displaytag">
							<display:column property="msf_urut" title="No." style="text-align: left;"/>
							<display:column property="msf_id" title="No. Form" style="text-align: center;" />
							<display:column property="status_form" title="Posisi" style="text-align: center;" />
							<display:column property="lus_login_name" title="User" style="text-align: center;" />
							<display:column property="msfh_dt" title="Tanggal" format="{0, date, dd/MM/yyyy (hh:mm)}" style="text-align: center;" />
							<display:column property="msfh_desc" title="Keterangan" style="text-align: center;" />
						</display:table>
					</fieldset>
				</div>
				<div id="pane3" class="panes">
					<jsp:include flush="true" page="legend_brosur.jsp" />
				</div>
			</div>
			
		</div>

	</body>

</html>
