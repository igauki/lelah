<%@ include file="/include/page/header.jsp"%><c:set var="path" value="${pageContext.request.contextPath}" />
<style type="text/css">
html,body {
	overflow: hidden;
}
</style>
<script> 
function tampil(){
	//Yusuf (10/02/2010) - Req Himmia, Print Slip Pajak di disable dulu
	//Yusuf (15/02/2010) - Enable lagi
	//alert('Mohon maaf, menu ini untuk sementara belum dapat digunakan');
	var bulan 	= document.formpost.bulan.value;
	var tahun 	= document.formpost.tahun.value;
	var peragen = document.formpost.peragen;
	var msag_id = document.formpost.msag_id.value;
	var frm 	= document.getElementById('infoFrame');
	if(!peragen.checked)
		frm.src = 
			'${path}/bas/slip_pajak.htm?' +
			'bulan=' + bulan +
			'&tahun=' + tahun +
			'&window=pdf';
	else
		frm.src = 
			'${path}/bas/slip_pajak.htm?' +
			'bulan=' + bulan +
			'&tahun=' + tahun +
			'&msag_id=' + msag_id +
			'&window=pdf';
}
</script>
<body onload="setFrameSize('infoFrame', 65);"
	onresize="setFrameSize('infoFrame', 65);" style="height: 100%;">

	<form name="formpost" method="post">
		<div class="tabcontent">
			<table class="entry2" style="width: 98%;">
				<tr>
					<th>
						Periode
					</th>
					<td>
						<select name="bulan">
							<c:forEach var="b" items="${daftarBulan}">
								<option <c:if test="${currentMonth eq b.key}"> selected </c:if> value="${b.key}">${b.value}</option>
							</c:forEach>
						</select>
						<select name="tahun">
							<c:forEach var="t" items="${daftarTahun}">
								<option <c:if test="${currentYear eq t}"> selected </c:if> value="${t}">${t}</option>
							</c:forEach>
						</select>
						<input type="button" value="Show" name="show" onclick="tampil();">
					</td>
				</tr>
				<tr>
					<th>
						Per Agen
					</th>
					<td>
						<label for="peragen"><input type="checkbox" class="noBorder" name="peragen" id="peragen"> Per Agen</label>
						<input type="text" name="msag_id" maxlength="6" size="7">
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<iframe name="infoFrame" id="infoFrame" width="100%" frameborder="YES"/>
					</td>
				</tr>
			</table>
		</div>
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>