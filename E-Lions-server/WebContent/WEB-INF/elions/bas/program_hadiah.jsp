<%@ include file="/include/page/header_jquery.jsp"%>

<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<!-- Ajax Related -->

<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (jQueryUI Tabs) init tab2 Utama
		$("#tabs").tabs();

		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title], label[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		
		// button icons
		$( "#btnShow" ).button({icons: {primary: "ui-icon-search"}});
		$( "#btnDetail" ).button({icons: {primary: "ui-icon-note"}});
		$( "#btnHistory" ).button({icons: {primary: "ui-icon-script"}});		

		//bila ada pesan
	    //var pesan = '${command.pesan}';
	    var pesan = '${pesan}';
		if(pesan != '') alert(pesan);
		
		$("#btnShow").click(function() {
		
			var rd0,rd1,rd2,jenis;
			rd0 = document.getElementById('radioTanggal0').checked;
			rd1 = document.getElementById('radioTanggal1').checked;
			rd2 = document.getElementById('radioTanggal2').checked;
						
			if(rd0==1){
				jenis=1;
				var url = "${path}/bas/hadiah.htm?window=json&t=followup&begdate=" +$('#begdate').val() + "&enddate=" +$('#enddate').val() + "&lspd_id=" +$('#selectPosisi').val() + "&jenis="+jenis+"&program_hadiah=1";
				$.getJSON(url, function(result) {
					$("#btnShow").button("option", "disabled", true);
					$("#btnShow").button("option", "label", "Silahkan tunggu...");
					var options = $("#followup");
					var count = 0;
					var select;
					// clear options agar tidak berulang (line 75-76)
					select=document.getElementById('spn');
					select.options.length=0;
					options.empty();
					$.each(result, function() {
						count++;
						$('#reg_spaj').val(this.reg_spaj);
						$('#mspo_policy_no').val(this.policy_no);
						$('#mh_no').val(this.mh_no);
						
						options.append($("<option/>").val(this.reg_spaj).html(this.reg_spaj+" | "+this.policy_no+" | "+this.mh_no).appendTo('#spn'));
					});
					if(count == 0){
						alert("Tidak ada data.");
						$('#fieldsetInput').fadeOut();
					}else{
						$('#fieldsetInput').fadeIn();
					}
					$("#btnShow").button("option", "disabled", false);
					$("#btnShow").button("option", "label", "Tampilkan");
					var a = $('#selectPosisi').val();
					$('#posisiID').val(a);
				});
			}else if(rd1==1){
				jenis=2;
				var url = "${path}/bas/hadiah.htm?window=json&t=followup&begdate=" +$('#begdate').val() + "&enddate=" +$('#enddate').val() + "&lspd_id=" +$('#selectPosisi').val() + "&jenis="+jenis+"&program_hadiah=1";
				$.getJSON(url, function(result) {
					$("#btnShow").button("option", "disabled", true);
					$("#btnShow").button("option", "label", "Silahkan tunggu...");
					var options = $("#followup");
					var count = 0;
					var select;
					select=document.getElementById('spn');
					select.options.length=0;
					options.empty();
					$.each(result, function() {
						count++;
						$('#reg_spaj').val(this.reg_spaj);
						$('#mspo_policy_no').val(this.policy_no);
						$('#mh_no').val(this.mh_no);
						
						options.append($("<option/>").val(this.reg_spaj).html(this.reg_spaj+" | "+this.policy_no+" | "+this.mh_no).appendTo('#spn'));
					});
					if(count == 0){
						alert("Tidak ada data.");
						$('#fieldsetInput').fadeOut();
					}else{
						$('#fieldsetInput').fadeIn();
					}
					$("#btnShow").button("option", "disabled", false);
					$("#btnShow").button("option", "label", "Tampilkan");
					var a = $('#selectPosisi').val();
					$('#posisiID').val(a);
				});
			}else if(rd2==1){
				jenis=3;
				var url = "${path}/bas/hadiah.htm?window=json&t=followup&begdate=" +$('#begdate').val() + "&enddate=" +$('#enddate').val() + "&lspd_id=" +$('#selectPosisi').val() + "&jenis="+jenis+"&program_hadiah=1";
				$.getJSON(url, function(result) {
					$("#btnShow").button("option", "disabled", true);
					$("#btnShow").button("option", "label", "Silahkan tunggu...");
					var options = $("#followup");
					var count = 0;
					var select;
					select=document.getElementById('spn');
					select.options.length=0;
					options.empty();
					$.each(result, function() {
						count++;
						$('#reg_spaj').val(this.reg_spaj);
						$('#mspo_policy_no').val(this.policy_no);
						$('#mh_no').val(this.mh_no);
						
						options.append($("<option/>").val(this.reg_spaj).html(this.reg_spaj+" | "+this.policy_no+" | "+this.mh_no).appendTo('#spn'));
					});
					if(count == 0){
						alert("Tidak ada data.");
						$('#fieldsetInput').fadeOut();
					}else{
						$('#fieldsetInput').fadeIn();
					}
					$("#btnShow").button("option", "disabled", false);
					$("#btnShow").button("option", "label", "Tampilkan");
					var a = $('#selectPosisi').val();
					$('#posisiID').val(a);
				});
			}			
		});
		
		// user memilih spaj pada select
		$("#spn").click(function(){
			
			var spaj = document.getElementById('spn').value;
			
			$("#sel_hadiah").empty();
			var url2 = "${path}/bas/hadiah.htm?window=json&t=sel_hadiah&reg_spaj=" + spaj;
			$.getJSON(url2, function(result2) {
				$.each(result2, function() {
					//$("<option/>").val(this.key).html(this.value).appendTo("#lh_id");
					$("#sel_hadiah").append($('<option></option>').attr("value", this.key).text(this.value));
					
				});
				$("#sel_hadiah option:first").attr('selected','selected');
			});
			
			var url = "${path}/bas/hadiah.htm?window=json&t=rFrame&reg_spaj=" +spaj;
			$.getJSON(url, function(result){
				$.each(result, function(){
					//for validate
					var supplier_id = this.supplier_id;
					var lbn_id = this.lbn_id;
					$('#supplier_id').val(supplier_id);
					$('#lbn_id').val(lbn_id);
					var pegang = this.pemegang;
					var bd = this.beg_date;
					var ed = this.end_date;
					var ta = this.tgl_aksep;
					$('#pemegang').val(pegang);
					$('#beg_date').val(bd);
					$('#end_date').val(ed);
					$('#tgl_aksep').val(ta);
					var jp = this.mh_flag_kirim;
					var lhc = this.lhc_nama;
					var lh = this.lh_nama;
					var qty = this.mh_quantity;
					var hrg = this.mh_harga;
					$('#spj').val(spaj);
					var s = $('#spj').val();
					$('#lhc').val(lhc);
					$('#lh').val(lh);
					$('#qty').val(qty);
					$('#hrg').val(hrg); 
					$('#hadiah').val('Kategori : ' +lhc +'\nHadiah : '+ lh +'\nJumlah : '+ qty);
					$('#sn').val(this.mh_sn);
					$('#tgl_kirim_hadiah').val(this.mh_tgl_kirim_hadiah_str);
					$('#alamat').val(this.mh_alamat_kirim+' '+this.mh_kota_kirim+' '+this.mh_kodepos_kirim);
					var k = this.lspd_id;
					var t = null;					
					
					document.getElementById('hideup').style.display = 'none';
					document.getElementById('hidesn').style.display = 'none';
					document.getElementById('hidekh').style.display = 'none';
					document.getElementById('hideih').style.display = 'none';
					document.getElementById('hideih2').style.display = 'none';
					$('#sn').attr('readonly', false); 
					$("#btnPrint").button("option","disabled",true);
					if (k==84){
						k=91;
						$('#lspdID').val(k);
						t='TRANFER UW KE PURCHASING DAN DISTRIBUTION';
						$("#btnPrint").button("option","disabled",true);
						$("#btnTransfer").button("option","disabled",false);
						$("#btnCancel").button("option","disabled",true);
						$("#btnEdit").button("option","disabled",false);
					}else if(k==91){
						var lde_id = '${lde_id}';
						k=92;
						$('#lspdID').val(k);
						t='HADIAH READY';
						$("#btnPrint").button("option","disabled",true);
						if(lde_id=='05'){
							$("#btnTransfer").button("option","disabled",false);
						}else{
							$("#btnTransfer").button("option","disabled",false);
						} 
						$("#btnCancel").button("option","disabled",true);
						$("#btnEdit").button("option","disabled",false);
						document.getElementById('hideih').style.display = 'block';
						//document.getElementById('hide').style.display = 'block';
						//$("#tgl_proses").attr("disabled", true);
					}else if(k==92){
						k=93;
						var lde_id = '${lde_id}';
						$('#lspdID').val(k);
						t='CONFIRM MARKETING';
						$("#btnPrint").button("option","disabled",true);
						if(lde_id=='05'){
							$("#btnTransfer").button("option","disabled",false);
						}else{
							$("#btnTransfer").button("option","disabled",false);
						}
						$("#btnCancel").button("option","disabled",true);
						$("#tgl_proses").attr("option","disabled", false);
						$("#btnEdit").button("option","disabled",true);
						document.getElementById('hidesn').style.display = 'block';
						document.getElementById('hideih2').style.display = 'block';
						//$("#tgl_proses").attr('disabled', true);
					}else if(k==93){
						k=94;
						$('#lspdID').val(k);
						t='PACKAGE ORDER';
						$("#btnPrint").button("option","disabled",true);
						var lde_id = '${lde_id}';
						if(lde_id=='13'){
							$("#btnTransfer").button("option","disabled",true);
						}else{
							$("#btnTransfer").button("option","disabled",true);
						}
						$("#btnCancel").button("option","disabled",true);
						$("#btnEdit").button("option","disabled",true);
						document.getElementById('hidesn').style.display = 'block';
						document.getElementById('hidekh').style.display = 'block';
						document.getElementById('hideih2').style.display = 'block';
						//$("#tgl_proses").attr('disabled', false);
					}else if(k==94){
						k=96;
						$('#lspdID').val(k);
						t='HADIAH DITERIMA GA';
						$("#btnPrint").button("option","disabled",false);
						var lde_id = '${lde_id}';
						if(lde_id=='13'){
							$("#btnTransfer").button("option","disabled",false);
						}else{
							$("#btnTransfer").button("option","disabled",false);
						}
						$("#btnCancel").button("option","disabled",true);
						$("#btnEdit").button("option","disabled",true);
						document.getElementById('hidesn').style.display = 'block';
						document.getElementById('hidekh').style.display = 'block';
						document.getElementById('hideih2').style.display = 'block';
						//$("#tgl_proses").attr('disabled', false);
					}else if(k==96){
						k=90;
						$('#lspdID').val(k);
						t='PENERIMAAN HADIAH KE NASABAH';
						$("#btnPrint").button("option","disabled",true);
						var lde_id = '${lde_id}';
						if(lde_id=='13'){
							$("#btnTransfer").button("option","disabled",false);
						}else{
							$("#btnTransfer").button("option","disabled",false);
						}
						$("#btnCancel").button("option","disabled",true);
						$("#btnEdit").button("option","disabled",true);
						document.getElementById('hidesn').style.display = 'block';
						document.getElementById('hidekh').style.display = 'block';
						document.getElementById('hideih2').style.display = 'block';
						//$("#tgl_proses").attr('disabled', false);
					}else if(k==90){
						k=99;
						$('#lspdID').val(k);
						t='FILLING';
						$("#btnPrint").button("option","disabled",true);
						var lde_id = '${lde_id}';
						if(lde_id=='13'){
							$("#btnTransfer").button("option","disabled",false);
						}else{
							$("#btnTransfer").button("option","disabled",false);
						}
						$("#btnCancel").button("option","disabled",true);
						$("#btnEdit").button("option","disabled",true);
						//$("#tgl_proses").attr('disabled', false);
						document.getElementById('hideup').style.display = 'block';
						document.getElementById('hidekh').style.display = 'block';
						document.getElementById('hideih2').style.display = 'block';
						$('#sn').attr('readonly', true); 
					}else if(k==95){
						k=95;
						$('#lspdID').val(k);
						t='CANCEL';
						$("#btnPrint").button("option","disabled",true);
						var lde_id = '${lde_id}';
						if(lde_id=='13'){
							$("#btnTransfer").button("option","disabled",true);
						}else{
							$("#btnTransfer").button("option","disabled",true);
						}
						$("#btnCancel").button("option","disabled",true);
						$("#btnEdit").button("option","disabled",true);
						//$("#tgl_proses").attr('disabled', false);
						document.getElementById('hidesn').style.display = 'block';
						document.getElementById('hidekh').style.display = 'block';
						document.getElementById('hideih2').style.display = 'block';
						$('#sn').attr('readonly', true); 
					}else if(k==99){
						k=99;
						$('#lspdID').val(k);
						t='FILLING';
						$("#btnPrint").button("option","disabled",true);
						$("#btnTransfer").button("option","disabled",true);
						$("#btnCancel").button("option","disabled",true);
						$("#btnPrint").button("option","disabled",true);
						$("#btnEdit").button("option","disabled",true);
						//$("#tgl_proses").attr('disabled', true);
						document.getElementById('hideih2').style.display = 'block';
						document.getElementById('hidekh').style.display = 'block';
						document.getElementById('hidesn').style.display = 'block';
					} 
					$('#tKey').val('['+k+'] '+t);
					
					if(jp==0){
						$('#jkirim').val('Langsung ke Nasabah');
					}else{
						$('#jkirim').val('Via AJS MSIG');
					}
					$('#mh_no').val(this.mh_no);
				});
			});
		});
		
		// Click button Cancel
		$("#btnCancel").click(function(){
			var spaj = document.getElementById('spn').value;
			var res = confirm("Cancel hadiah untuk no. spaj "+spaj+"?");
			if (res){
				return true;
			}else{
				return false;
			}
		});
			
		// click button Detail
		$("#btnDetail").click(function(){
			var spaj = document.getElementById('spn').value;
			var mh = document.getElementById('mh_no').value;
			$("#histBody").empty();
			var url = "${path}/bas/hadiah.htm?window=json&t=fDetail&reg_spaj=" + spaj + "&mh_no=" + mh+ "&program_hadiah=1";
			$.getJSON(url, function(result) {
				$.each(result, function() {
					var jns = this.jenisK;
					var ket = null;
					if(jns==1) {ket = "Via AJS MSIG";}
					else {ket = "Langsung ke Nasabah";}
					//$('#histTable > tbody:last').append(
						$('#spajT').empty();
						$('#pegangT').empty();
						$('#beg_dateT').empty();
						$('#end_dateT').empty();
						$('#tgl_aksepT').empty();
						$('#hadiahT').empty();
						$('#jenisT').empty();
						$('#alamatT').empty();
						$('#telpT').empty();
						$('#vendorT').empty();
						$('#bankT').empty();
						$('#tgl_inputT').empty();
						$('#usernameT').empty();
						$('#mh_tgl_aksepT').empty();
						$('#mh_tgl_paidT').empty();
						$('#mh_tgl_kirim_vendorT').empty();
						$('#mh_tgl_terima_ajsT').empty();
						$('#mh_tgl_kirim_ajsT').empty();
						$('#mh_tgl_terima_nsbhT').empty();
						$('#mh_tgl_kirim_uwT').empty();
						$('#mh_tgl_order_purchasingT').empty();
						$('#mh_tgl_barang_readyT').empty();
						$('#mh_tgl_kirim_gaT').empty();
						$('#mh_snT').empty();
					
						$('#spajT').val(this.reg_spaj);
						$('#pegangT').val(this.pemegang);
						$('#beg_dateT').val(this.beg_date); 
						$('#end_dateT').val(this.end_date);
						if(this.tgl_aksep!=null)$('#tgl_aksepT').val($.datepicker.formatDate('dd/mm/yy', new Date(this.tgl_aksep))); 
						$('#hadiahT').val('Kategori : '+this.lhc_nama+'\n'+'Hadiah : '+this.lh_nama+'\n'+'Quantity : '+this.mh_quantity);
						$('#jenisT').val(ket);
						$('#alamatT').val(this.mh_alamat+' '+this.mh_kota+' '+this.mh_kodepos);
						$('#telpT').val(this.mh_telepon) ;
						$('#vendorT').val(this.supplier_name);
						$('#bankT').val(this.bank_name);
						if(this.mh_tgl_input!=null)$('#tgl_inputT').val($.datepicker.formatDate('dd/mm/yy', new Date(this.mh_tgl_input)));
						$('#usernameT').val(this.username);
						if(this.mh_tgl_aksep!=null)$('#mh_tgl_aksepT').val($.datepicker.formatDate('dd/mm/yy', new Date(this.mh_tgl_aksep)));
						//$('#mh_tgl_paidT').val($.datepicker.formatDate('dd/mm/yy', new Date(this.mh_tgl_paid)));
						//$('#mh_tgl_kirim_vendorT').val($.datepicker.formatDate('dd/mm/yy', new Date(this.mh_tgl_kirim_vendor)));
						//$('#mh_tgl_terima_ajsT').val($.datepicker.formatDate('dd/mm/yy', new Date(this.mh_tgl_terima_ajs)));
						//$('#mh_tgl_kirim_ajsT').val($.datepicker.formatDate('dd/mm/yy', new Date(this.mh_tgl_kirim_ajs)));
						if(this.mh_tgl_terima_nsbh!=null)$('#mh_tgl_terima_nsbhT').val($.datepicker.formatDate('dd/mm/yy', new Date(this.mh_tgl_terima_nsbh)));
						if(this.mh_tgl_kirim_uw!=null)$('#mh_tgl_kirim_uwT').val($.datepicker.formatDate('dd/mm/yy', new Date(this.mh_tgl_kirim_uw)));
						if(this.mh_tgl_order_purchasing!=null)$('#mh_tgl_order_purchasingT').val($.datepicker.formatDate('dd/mm/yy', new Date(this.mh_tgl_order_purchasing)));
						if(this.mh_tgl_barang_ready!=null)$('#mh_tgl_barang_readyT').val($.datepicker.formatDate('dd/mm/yy', new Date(this.mh_tgl_barang_ready)));
						if(this.mh_tgl_kirim_ga!=null)$('#mh_tgl_kirim_gaT').val($.datepicker.formatDate('dd/mm/yy', new Date(this.mh_tgl_kirim_ga)));
						//);
				});
			});				
		});		
		$("#btnDetail").qtip({
			style: {
				classes: 'ui-tooltip-light ui-tooltip-rounded',
				width: 800
			},
			content: {
    			text: $('#historyHadiah'),
    			title: {
       				text: 'Detail Nasabah',
       				button: true}
			},
			position: {
				my: 'center', // ...at the center of the viewport
				at: 'center',
				target: $(window),
				adjust: {scroll: false}
			},
			show: {
				event: 'click', // Show it on click...
				solo: true, // ...and hide all other tooltips...
				modal: true // ...and make it modal
			},
			hide: false
		});
		
		$("#btnEdit").click(function(){
			window.open("${path}/bas/hadiah.htm?window=input&rs="+$("#spj").val()+"&no="+$("#mh_no").val());
		});
		
		//Open Page
		$("#btnPrint").button("option","disabled",true);
		$("#btnTransfer").button("option","disabled",true);
		$("#btnCancel").button("option","disabled",true);
		$("#btnEdit").button("option","disabled",true);
		
	});
	
	function ValidateForm(){
			
		var upload = document.formpost.file1;
		var sel = document.getElementById('selectPosisi'); 
		
		var ext = upload.value.substr(upload.value.length -4);
		//var ext = upload.value.substr(upload.value.indexOf('.'),upload.value.length);
		ext = ext.substr(ext.indexOf('.'),ext.length);

		if (sel.options[sel.selectedIndex].value == '90'){
		
			if((upload.value==null)||(upload.value=="")){
				alert("Harap di isi bukti penerimaan hadiah oleh nasabah.");
				upload.focus();
				return false;
			}
			
			//".jpg", ".jpeg", ".bmp", ".gif", ".png"
			//if((ext!='.jpg') && (ext!='.jpeg') && (ext!='.bmp') && (ext!='.gif') && (ext!='.png')){
			if(ext!='.pdf'){
				alert("Format file salah.");
				upload.focus();
				return false;
			} 
		}
		return true;
	 }	
	 
	 function ubah(){
		var spaj = document.getElementById('spn').value;
		var val = $('#sel_hadiah option:selected').val();
		
		ajaxManager.updateHadiahStableSave(spaj,val,
		{callback:function(map) {

			var pesan = map.pesan;
			
			if(pesan!=null)alert(pesan);
		},
		  timeout:180000,
		  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
		});
	}
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 23em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 23.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 36em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

	/* untuk tombol2 kecil di report */
	ul#icons {margin: 0; padding: 0;}
	ul#icons li {margin: 0 1px 0 0; position: relative; padding: 0px; cursor: pointer; float: left;  list-style: none;}
	ul#icons span.ui-icon {float: left; margin: 0px;}

</style>

<body>
	
	<!-- Tabs -->
	<form method="post" name="formpost" id="formpost" commandName="command" action="${path}/bas/hadiah.htm?window=program_hadiah" enctype="multipart/form-data" onsubmit="return ValidateForm()">
	<!-- <form method="post" name="formpost" id="formpost" commandName="command">-->
	<div id="tabs">
		<ul>
			<li><a href="#tab-1">Program Hadiah</a></li>
		</ul>
		
		<div id="tab-1">
			<fieldset class="ui-widget ui-widget-content">
				<legend class="ui-widget-header ui-corner-all"><div>Program Hadiah</div></legend>
				
				<div class="rowElem">
					<label title="Periode penarikan data">Periode:</label>
					<input name="begdate" id="begdate" type="text" class="datepicker" title="Awal" value="${begdate}"> s/d 
					<input name="enddate" id="enddate" type="text" class="datepicker" title="Akhir" value="${enddate}">
				</div>
			
				<div class="rowElem">
					<label>Berdasarkan Tanggal:</label>
					<c:forEach var="f" items="${listTanggal}" varStatus="s">
						<input type="radio" id="radioTanggal${s.index}" name="radioTanggal" value="${f.key}" title="${f.value}"
							<c:if test="${f.key eq 1}">checked="checked"</c:if>rdo = ${s.index}
						><label class="radioLabel" for="radioTanggal${s.index}">${f.value}</label>
					</c:forEach>
				</div>
				
				<div class="rowElem">
					<label>Berdasarkan Posisi:</label>
					<select name="selectPosisi" id="selectPosisi" title="Posisi Input">
						<option value="91">[91] PURCHASING ORDER HADIAH</option>
						<option value="92">[92] HADIAH READY</option>
						<option value="93">[93] CONFIRM MARKETING</option>
						<option value="94">[94] PACKAGE ORDER</option>
						<option value="96">[96] HADIAH DITERIMA GA/KIRIM KE NASABAH</option>
						<option value="90">[90] PENERIMAAN BARANG OLEH NASABAH</option>
						<option value="95">[95] CANCEL FROM BEGINING DATE</option>
						<option value="99">[99] FILLING</option>
						<%-- <c:forEach var="f" items="${listPosisi}" varStatus="s">
							<option value="${f.key}">[${f.key}] ${f.value}</option>
						</c:forEach> --%>
					</select>
				</div>
			
				<div class="rowElem">
					<label></label>
					<button id="btnShow" title="Tampilkan data">Tampilkan</button>
				</div>
			
			</fieldset>
			
			<fieldset id="fieldsetInput" class="ui-widget ui-widget-content">
			
				<!-- <form id="formPost" name="formPost" method="post" action="${path}/bas/hadiah.htm?window=proses"> -->
				
				<input id="submitType" name="submitType" type="hidden">
				<input id="mh_no" name="mh_no" type="hidden">
				<input id="lspdID" name="lspdID" type="hidden">
				<input type="hidden" name="posisiId" id="posisiId">
				<input type="hidden" name="lhc" id="lhc">
				<input type="hidden" name="lh" id="lh">
				<input type="hidden" name="qty" id="qty">
				<input type="hidden" name="hrg" id="hrg">
				<input type="hidden" name="emailto" id="emailto">
				<input type="hidden" name="spj" id="spj">
				<input type="hidden" name="supplier_id" id="supplier_id">
				<input type="hidden" name="lbn_id" id="lbn_id">
				<table class="jtable" width="100%">
					<tr>
						<th width="30%">SPAJ | Polis | No</th>
						<th>Deskripsi Polis</th>
					</tr>
					<tr>
						<th>
							<select name="spn" id="spn" size="20" title="Silahkan pilih Polis yang ingin di follow up">
								<option>--- Silahkan cari terlebih dahulu ---</option>
							</select>
						</th>
						<td id="deskripsiPolis" valign="top">
							<table class="plain" width="100%">
                            <tr>
                                <th>Pemegang Polis : </th>
                                <td><input id="pemegang" type="text" class="lebar ui-state-default" readonly="readonly" name="pemegang"></td>
                            </tr>
                            <tr>
                              <th>Periode Polis : </th>
                              <td><input id="beg_date" type="text" class="ui-state-default" readonly="readonly" width="200" name="beg_date"> s/d <input id="end_date" type="text" class="ui-state-default" readonly="readonly" width="200" name="end_date"></td>
                      		</tr>
                            <tr>
                              <th>Tanggal Aksep UW :</th>
                              <td><input id="tgl_aksep" type="text" class="ui-state-default" readonly="readonly" width="200" name="tgl_aksep"></td>
                      		</tr>
                      		<tr id="hideih" style="display: none;">
                              <th>Info Hadiah :</th>
                              <td>
								<select name="sel_hadiah" id="sel_hadiah" title="Unit Hadiah">
									<c:forEach var="c" items="${listHadiah}" varStatus="s">
										<option value="${c.key}" <c:if test="${lh_id eq c.key}"> selected="selected"</c:if>>${c.value}</option>
									</c:forEach>
								</select>
								<input type="button" id="ubah_hadiah" name="ubah_hadiah" value="Change" onclick="ubah();">
                              </td>
                      		</tr>
                            <tr id="hideih2" style="display: none;">
                              <th>Info Hadiah :</th>
                              <td>
                                <textarea name="hadiah" id="hadiah" cols="45" rows="5" readonly="readonly"></textarea>
                              </td>
                      		</tr>
                      		<tr>
                              <th>Alamat :</th>
                              <td>
                                <textarea name="alamat" id="alamat" cols="45" rows="5" readonly="readonly"></textarea>
                              </td>
                      		</tr>
                      		<tr>
                              <th>Tanggal proses :</th>
                              <td><span class="rowElem">
                                <input name="tgl_proses" id="tgl_proses" type="text" class="datepicker" title="Tanggal Proses" value="${prosdate}" readonly>
                              </span></td>
                      		</tr>
                      		<tr id="hidekh" style="display: none;">
                              <th>Tanggal Kirim :</th>
                              <td><span class="rowElem">
                                <input name="tgl_kirim_hadiah" id="tgl_kirim_hadiah" type="text" title="Tanggal Kirim hadiah" readonly="readonly">
                              </span></td>
                      		</tr>
                      		<tr id="hidesn" style="display: none;">
                              <th>Serial Number :</th>
                              <td><span class="rowElem">
                                <input name="sn" id="sn" type="text" title="Serial Number Hadiah">
                              </span></td>
                      		</tr>
                      		<%-- <tr id="hide" style="display: none;">
                              <th>Deadline Pembayaran :</th>
                              <td><span class="rowElem">
                                <input name="tgl_pembayaran" id="tgl_pembayaran" type="text" class="datepicker" title="Tanggal Proses" value="${begdate}" readonly>
                              </span></td>
                      		</tr> --%>
                      		<tr id="hideup" style="display: none;">
								<th>Upload : </th>
								<td>
									<input name="file1" id="file1" type="file"/><em> *format file "*.pdf"</em>
								</td>
							</tr>
                      		<tr>
                                <th>&nbsp;</th>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>
                                    <input type="submit" name="btnPrint" id="btnPrint" title="Print Surat Hadiah" value="Print">
                                    <input type="submit" name ="btnTransfer" id="btnTransfer" title="Transfer proses" value="Transfer">
                                    <input type="submit" name="btnCancel" id="btnCancel" title="Cancel Proses" value="Cancel">
                                    <button id="btnDetail" name="btnDetail" title="View detail polis">View Detail</button>
                                    <!-- <button id="btnEdit" name="btnEdit" title="Edit Data Hadiah">Edit</button> -->
                                </td>
                            </tr>
                        </table>
						</td>
					</tr>
				</table>
			</fieldset>
		</div>
	</div>
	</form>
	<div id="historyHadiah" style="display:none; font-size: 1.2em;">
	<div style="overflow: auto; height: 400px;">
		<table id="histTable" class="jtable">
			<thead>
				<tr>
					<th>No SPAJ</th><td><input type="text" id="spajT" name="spajT" readonly="readonly" size="77"></td>
				</tr>
				<tr>
					<th>Pemegang Polis</th><td><input type="text" id="pegangT" name="pegangT" readonly="readonly" size="77"></td>
				</tr>
				<tr>
					<th>Periode Polis</th><td><input type="text" id="beg_dateT" name="beg_dateT" readonly="readonly"> s/d <input type="text" id="end_dateT" name="end_dateT" readonly="readonly"></td>
				</tr>
				<tr>
					<th>Tanggal Aksep UW</th><td><input type="text" id="tgl_aksepT" name="tgl_aksepT" readonly="readonly" size="77"></td>
				</tr>
				<tr>
					<th>Info Hadiah</th><td><textarea name="hadiahT" id="hadiahT" cols="45" rows="5" readonly="readonly"></textarea></td>
				</tr>
				<tr>
					<th>Jenis Pengiriman</th><td><input type="text" id="jenisT" name="jenisT" readonly="readonly" size="77"></td>
				</tr>
				<tr>
					<th>Alamat Nasabah</th><td><textarea name="alamatT" id="alamatT" cols="45" rows="5" readonly="readonly"></textarea></td>
				</tr>
				<tr>
					<th>Telp. Nasabah</th><td><input type="text" id="telpT" name="telpT" readonly="readonly" size="77"></td>
				</tr>
				<!-- <tr>
					<th>Vendor</th><td><input type="text" id="vendorT" name="vendorT" readonly="readonly" size="77"></td>
				</tr>
				<tr>
					<th>Bank</th><td><input type="text" id="bankT" name="bankT" readonly="readonly" size="77"></td>
				</tr> -->
				<tr>
					<th>Tanggal Input</th><td><input type="text" id="tgl_inputT" name="tgl_inputT" readonly="readonly" size="77"></td>
				</tr>
				<tr>
					<th>User Input</th><td><input type="text" id="usernameT" name="usernameT" readonly="readonly" size="77"></td>
				</tr>
				<tr>
					<th>Tanggal Aksep</th><td><input type="text" id="mh_tgl_aksepT" name="mh_tgl_aksepT" readonly="readonly" size="77"></td>
				</tr>
				<tr>
					<th>Tanggal Kirim UW</th><td><input type="text" id="mh_tgl_kirim_uwT" name="mh_tgl_kirim_uwT" readonly="readonly" size="77"></td>
				</tr>
				<tr>
					<th>Tanggal Order Purchasing</th><td><input type="text" id="mh_tgl_order_purchasingT" name="mh_tgl_order_purchasingT" readonly="readonly" size="77"></td>
				</tr>
				<tr>
					<th>Tanggal Hadiah Ready</th><td><input type="text" id="mh_tgl_barang_readyT" name="mh_tgl_barang_readyT" readonly="readonly" size="77"></td>
				</tr>
				<tr>
					<th>Tanggal Kirim GA</th><td><input type="text" id="mh_tgl_kirim_gaT" name="mh_tgl_kirim_ajsT" readonly="readonly" size="77"></td>
				</tr>
				<tr>
					<th>Tanggal Terima Nasabah</th><td><input type="text" id="mh_tgl_terima_nsbhT" name="mh_tgl_terima_nsbhT" readonly="readonly" size="77"></td>
				</tr>
			</thead>
			<!-- <tbody id="histBody">
			</tbody> --> 
		</table>
	</div>
	</div>
</body>
</html>