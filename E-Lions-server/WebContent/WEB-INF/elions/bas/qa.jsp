<%@ include file="/include/page/header.jsp"%>

<script>

	function showPage(hal){
		var spaj = document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value;
		var lsbs = document.formpost.kodebisnis.value;
		var lsdbs = document.formpost.numberbisnis.value;
		var copy_reg_spaj = document.formpost.copy_reg_spaj.value;
		if(spaj == ''){
			alert('Harap pilih SPAJ terlebih dahulu!');
		}else{
			createLoadingMessage();
			if('tampil' == hal){
				document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj+'&halForBlacklist=uw';
			}else if('edit' == hal){
				document.getElementById('infoFrame').src='${path}/bac/multi.htm?window=testRedirect&spaj='+spaj;
			}else if('editagen' == hal){
				document.getElementById('infoFrame').src='${path}/bac/editagenpenutup.htm?spaj='+spaj;			
			}else if('reffbii' == hal){
			if(confirm("Apakah anda akan input referral others BII?\nApabila anda adalah user/spaj ini dari BANK SINARMAS / ASURANSI SINARMAS / SINARMAS SEKURITAS/ BANK SINARMAS SYARIAH / BANK VICTORIA / BANK BRI, harap pilih CANCEL!")){
					//alert('${path}/bac/reff_bank.htm?window=main&reffothers=y&spaj='+spaj);
					popWin('${path}/bac/reff_bank.htm?window=main&reffothers=y&spaj='+spaj, 600, 700);
				}else{
					//alert('${path}/bac/reff_bank.htm?window=main&reffothers=n&spaj='+spaj);
					popWin('${path}/bac/reff_bank.htm?window=main&reffothers=n&spaj='+spaj, 600, 700);
				}
			}else if('uwinfo' == hal){
				document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=uwinfo&spaj='+spaj;			
 			}else if('upload_nb' == hal){
 				document.getElementById('infoFrame').src='${path}/uw/upload_nb.htm?reg_spaj='+document.formpost.spaj.value;
			}else if('backtoadmin' == hal){
				document.getElementById('infoFrame').src='${path}/report/bas.htm?window=backtoAdmin&spaj='+spaj;
			}else if('questionare' == hal){
				 document.getElementById('infoFrame').src='${path}/bac/questionarenew.htm?flag=1&spaj='+spaj+'&mode=1';
			}else if('profile_risk' == hal){
				document.getElementById('infoFrame').src='${path}/bac/multi.htm?window=profile_risk&spaj=' + spaj + '&viewonly=0';
			}else if('blacklist' == hal){				
				popWin('${path}/uw/blacklist.htm', 400, 900);
			/* }else if('transfer_qa' == hal){
				var owner = check('owner_sign');
				var parent = check('parent_sign');
				var agent = check('agent_sign');
				var health_quest = check('health_quest');
				var further_qa = document.getElementById("further_qa").value;
				var r = confirm("Transfer ke QA 2 ? ");
				if (r == true) {						
					if(owner==null){
						alert("Harap isi Owner's & Insured's Signature");
					}else if(parent==null){
						alert("Harap isi Parent's Signature if Insured age < 17 yo");
					}else if(agent==null){
						alert("Harap isi Agent's Signature");
					}else if(health_quest==null){
						alert("Harap isi Health Quest");
					}else{
						document.getElementById('infoFrame').src='${path}/report/bas.htm?window=transferTo_Qa2&spaj='+spaj+'&owner='+owner+'&parent='+parent+'&agent='+agent+'&health_quest='+health_quest+'&further_qa='+further_qa;
						//parent.location.href='${path}/report/bas.htm?window=qa_1';			
					}	    
				} */
			}else if('transfer_nb' == hal){
				var owner = check('owner_sign');
				var parent = check('parent_sign');
				var agent = check('agent_sign');
				var health_quest = check('health_quest');
				var r = confirm("Transfer ke Speedy ? ");
				if (r == true) {						
					if(owner==null){
						alert("Harap isi Owner's & Insured's Signature");
					}else if(parent==null){
						alert("Parent's Signature if Insured age < 17 yo");
					}else if(agent==null){
						alert("Harap isi Agent's Signature");
					}else if(health_quest==null){
						alert("Harap isi Further");
					}else{
						document.getElementById('infoFrame').src='${path}/report/bas.htm?window=transferTo_Qa2&spaj='+spaj+'&owner='+owner+'&parent='+parent+'&agent='+agent+'&health_quest='+health_quest;
						//parent.location.href='${path}/report/bas.htm?window=qa_1';			
					}	    
				}
			}
			
		}
	}
	
	function cariregion(spaj,nama){
		if(spaj != ''){
			if(document.getElementById('docFrame')) document.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+spaj;
			if(document.getElementById('infoFrame')) document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj+'&halForBlacklist=uw';
			ajaxManager.listcollect(spaj , nama,
			{callback:function(map) {
				DWRUtil.useLoadingMessage();
				document.formpost.koderegion.value=map.LSRG_NAMA;
				document.formpost.kodebisnis.value = map.LSBS_ID;
				document.formpost.numberbisnis.value = map.LSDBS_NUMBER;
				document.formpost.jml_peserta.value = map.jml_peserta;
				document.formpost.flag_spaj.value = map.flag_spaj;
				document.formpost.flag_produk.value = map.flag_produk;
				
				/*if(document.formpost.flag_spaj.value==2 || document.formpost.flag_spaj.value==3 || document.formpost.flag_spaj.value==4){
					document.getElementById("questionare").disabled = false; 
				}else{*/
					document.getElementById("questionare").disabled = false; 
				//}
			   },
			  timeout:15000,
			  errorHandler:function(message) { alert("ERROR: " + message + "\nHarap ulangi halaman ini, atau hubungi EDP apabila tetap terjadi masalah."); }
			});
			
		}	
	}
	
	function awal(){
		if('${snow_spaj}'!=''){ 
			document.formpost.spaj.value='${snow_spaj}';
			document.formpost.spaj.options[document.formpost.spaj.selectedIndex].value='${snow_spaj}';
			cariregion('${snow_spaj}','region');
		}else{
			cariregion(document.formpost.spaj.value,'region');
		}
		setFrameSize('infoFrame', 90);
		setFrameSize('docFrame', 90);
	}
	
	function copyToClipboard(txt){
	
		if( window.clipboardData && clipboardData.setData ){
		
			clipboardData.setData("Text", txt);
		}
		else{
		 alert("Internet Explorer required"); 
		}
	}
	
	function run(){
		var shell = new ActiveXObject("WScript.Shell"); 
		var commandtoRun = "\\\\aplikasi\\lions\\secan4400f.exe";
	   
		if (shell){		
		 shell.run(commandtoRun); 
		} 
		else{ 
			alert("program or file doesn't exist on your system."); 
		}
	}
	
	function copyAndRun(){ 
	 	txt = document.getElementById("spaj").value;
	 	copyToClipboard(txt);
	 	run();		
	}
	
	function check(name){
	    var res = null;
	    var currObj = document.formpost[name];
	    
	    for (var i=0;i<currObj.length;i++) {
			if (currObj[i].checked) {
				res = currObj[i].value;
			}
		}
	    
	    return res;
	}
	
	/* $(document).ready(function() {
	   if($('#health_quest_no').click(function(){
			$('#keterangan').show();
	   }));
	    if($('#health_quest_yes').click(function(){
			$('#keterangan').hide();
	   }));
	 }); */
	 
	 	/* function further(flag){
	 	var e = document.getElementById("keterangan");
	 	if(flag==1&&'${posisi}'==218){
	 		if(e.style.display == 'none')
         		e.style.display = 'block';
	 	}else{
	 		e.style.display = 'none';
	 	} 
	 } */

</script>
<body  onload="awal();" onresize="setFrameSize('infoFrame', 90); setFrameSize('docFrame', 90);" style="height: 100%;">
<form name="formpost" method="post">
<div class="tabcontent">
<table class="entry2" style="width: 98%;">
	<tr>
		<th>Cari SPAJ</th>
		<td> <input type="hidden" name="halForBlacklist" value="uw"> 
		  <input type="hidden" name="koderegion" > 
          <input type="hidden" name="kodebisnis">
          <input type="hidden" name="numberbisnis">
          <input type="hidden" name="jml_peserta">
          <input type="hidden" name="flag_spaj">
          <input type="hidden" name="flag_produk">
          <input type="hidden" name="copy_reg_spaj" value="" />
			<select name="spaj" id="spaj" onChange="cariregion(this.options[this.selectedIndex].value,'region');">
				<option value="">[--- Silahkan Pilih/Cari SPAJ ---]</option>
				<c:forEach var="s" items="${daftarSPAJ}">
					<option value="${s.REG_SPAJ }" style="background-color: ${s.BG};" 
						<c:if test="${s.REG_SPAJ eq param.spaj }">selected<c:set var="ada" value="ada" /></c:if>>${s.SPAJ_FORMATTED}
					- ${s.POLICY_FORMATTED }</option>
				</c:forEach>
			</select>
			
			<input type="button" value="Info" name="info" 				onclick="showPage('tampil');" accesskey="I" onmouseover="return overlib('Alt-I', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<%-- <c:choose>
			<c:when test="${posisi  eq 218 }">
				<input type="button" value="Cari" name="search" 			onclick="popWin('${path}/uw/spaj.htm?posisi=218&win=uw', 600, 800); " accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">
			</c:when>
			<c:when test="${posisi  eq 219 }">
				<input type="button" value="Cari" name="search" 			onclick="popWin('${path}/uw/spaj.htm?posisi=219&win=uw', 600, 800); " accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">
			</c:when>
			</c:choose> --%>
			<input type="button" value="Cari" name="search" 			onclick="popWin('${path}/uw/spaj.htm?posisi=218&win=uw', 600, 800); " accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Edit" name="info" 				onclick="showPage('edit');" accesskey="J" onmouseover="return overlib('Alt-J', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Edit Agen" name="agen" 			onclick="showPage('editagen');" accesskey="F">
			<input type="button" value="Referral" name="reffbii" 			onclick="showPage('reffbii');" accesskey="R">
			<input type="button" value="U/W Info" name="uwinfo" 		onclick="showPage('uwinfo');" accesskey="W" onmouseover="return overlib('Alt-W', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="Upload Scan" name="upload_nb"	onclick="showPage('upload_nb');">	
			<input type="button" value="Questionare" id="questionare" name="questionare" onclick="showPage('questionare');">
			<input type="button" value="Attention List" name="blacklist" onclick="showPage('blacklist');" >
			<%-- <c:choose>
				<c:when test="${posisi  eq 219 }">
					<input type="button" value="Back To Admin" name="backtoadmin" onclick="showPage('backtoadmin');">
				</c:when>
			</c:choose>	
			<c:choose>
				<c:when test="${posisi  eq 218 }">
					<input type="button" value="Transfer To QA 2" name="transfer_qa" onclick="showPage('transfer_qa');" id="transfer_qa"/>
				</c:when>
				<c:when test="${posisi  eq 219 }">
					<input type="button" value="Transfer To Speedy" name="transfer_nb" onclick="showPage('transfer_nb');" id="transfer_nb"/>
				</c:when>
			</c:choose>	 --%>
			<input type="button" value="Back To Admin" name="backtoadmin" onclick="showPage('backtoadmin');">
			<input type="button" value="Profile Risiko Nasabah" name="profile_risiko_nasabah" onClick="showPage('profile_risk');"  id="profile_risk"/>
			<input type="button" value="Transfer To Speedy" name="transfer_nb" onclick="showPage('transfer_nb');" id="transfer_nb"/>
		
		</td>
	</tr>
	<tr>
		<%-- <c:choose>
			<c:when test="${posisi  eq 218 }">
				<th>QA 1</th>
			</c:when>
			<c:when test="${posisi  eq 219 }">
				<th>QA 2</th>
			</c:when>
		</c:choose>	 --%>
		<th>QA </th>
		<td>
			<table class="entry2" style="width: 100%;">
				<tr>
					<th style="width: 220px">Owner's & Insured's Signature</th>
					<td style="width: 100px">
						<input name="owner_sign" id="owner_sign_yes" type="radio" class="radio" value="1" style="border: none;" >Ada &nbsp;	   
	    				<input name="owner_sign" id="owner_sign_no" type="radio" class="radio" value="0" style="border: none;" >Tidak
					</td>
					<th style="width: 100px">Agent's Signature</th>
					<td style="width: 100px">
						<input name="agent_sign" id="agent_sign_yes" type="radio" class="radio" value="1" style="border: none;" >Ada &nbsp;	   
	    				<input name="agent_sign" id="agent_sign_no" type="radio" class="radio" value="0" style="border: none;" >Tidak
					</td>
				</tr>
				<tr>
					<th style="width: 220px">Parent's Signature if Insured age < 17 yo</th>
					<td style="width: 100px">
						<input name="parent_sign" id="parent_sign_yes" type="radio" class="radio" value="1" style="border: none;" >Ada &nbsp;	   
	    				<input name="parent_sign" id="parent_sign_no" type="radio" class="radio" value="0" style="border: none;" >Tidak
					</td>
					<th style="width: 100px">Further</th>
					<td style="width: 500px">
						<input name="health_quest" id="health_quest_yes" type="radio" class="radio" value="1" style="border: none;" onclick="further(2)">Clean &nbsp;	   
		  				<input name="health_quest" id="health_quest_no" type="radio" class="radio" value="0" style="border: none;" onclick="further(1)">Unclean &nbsp;	 
					</td>
				</tr>
			<!-- 	<tr style="display: none" id="keterangan">
					<th style="width: 220px" colspan="3">&nbsp;</th>
					<td style="width: 500px">
						<textarea rows="5" cols="70" style="text-transform: uppercase" name="further_qa" id="further_qa"></textarea>
					</td>
				</tr> -->
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table style="margin: 0 0 0 0; padding: 0 0 0 0; width: 100%;">
				<tr>
					<c:choose>
						<c:when test="${sessionScope.currentUser.wideScreen}">
							<td style="width: 60%;">
								<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
									width="100%" height="100%"> Please Wait... </iframe>
							</td>
							<td style="width: 40%;">
								<iframe src="" name="docFrame" id="docFrame" width="100%">E-Lions</iframe>
							</td>
						</c:when>
						<c:otherwise>
							<td>
								<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
									width="100%"  height="100%"> Please Wait... </iframe>
							</td>
						</c:otherwise>
					</c:choose>
				</tr>
			</table>		
		</td>	
	</tr>
		<c:if test="${not empty error}">
			<tr>
				<td colspan="4" align="center">
					<div id="error">
						${error}
					</div>	
				</td>
			</tr>
		</c:if>
</table>
</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>