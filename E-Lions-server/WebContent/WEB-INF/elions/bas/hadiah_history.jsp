<%@ include file="/include/page/header_jquery.jsp"%>
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		// user menekan tombol View History, ada 2 step (pertama populate tabel dulu, kedua tampilkan dalam bentuk modal window)
		//$('#reg_spaj').change(function(){
			//tarik history
			$("#histBody").empty();
			var spaj = document.getElementById('reg_spaj').value;
			var no = document.getElementById('mh_no').value;
			
			var url3 = "${path}/bas/hadiah.htm?window=json&t=hist&reg_spaj=" + spaj + "&mh_no=" + no;
			$.getJSON(url3, function(result) {
				var hit = 0;
				$.each(result, function() {
					$('#histTable > tbody:last').append(
							'<tr><td align=center>' + (++hit) + 
							'</td><td align=center>' + this.TGL + 
							'</td><td align=center>' + this.USERNAME + 
							'</td><td align=center>' + this.POSISI + 
							'</td><td align=center>' + this.KETERANGAN + '</td></tr>');
				});
			});	
			
		function gup( name )
		{
		  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
		  var regexS = "[\\?&]"+name+"=([^&#]*)";
		  var regex = new RegExp( regexS );
		  var results = regex.exec( window.location.href );
		  if( results == null )
		    return "";
		  else
		    return results[1];
		}	
	});
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 23em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 23.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 23em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

	/* untuk tombol2 kecil di report */
	ul#icons {margin: 0; padding: 0;}
	ul#icons li {margin: 0 1px 0 0; position: relative; padding: 0px; cursor: pointer; float: left;  list-style: none;}
	ul#icons span.ui-icon {float: left; margin: 0px;}

	/* styling untuk server-side validation error */
	.errorField { border: 1px solid red; }
	.errorMessage { color: red; display: block;}
	
	/* styling untuk table input */
	table.inputTable{
		width: 700px;
	}
	table.inputTable tr th{
		width: 200px;
	}
	
	/* styling untuk table info tanggal */
	div#infoTanggal{
		text-align: center;
		background-color: white;
		width: 345px;
		padding: 8px;
		border: 1px solid gray;
		position: fixed;
		left: 580px;
		top: 50px;
	}

</style>

History
<%
	String a = request.getParameter("spaj");
	String b = request.getParameter("no");
%>
<form:form>
	<input name="reg_spaj" id="reg_spaj" value="<%=a%>" type="hidden"/>
	<input name="mh_no" id="mh_no" value="<%=b%>" type="hidden"/>
</form:form>
<div id="historyHadiah" style="font-size: 1.2em;">
	<div style="overflow: auto; height: 400px;">
		<table id="histTable" class="jtable">
			<thead>
				<tr>
					<th width="10">No.</th>
					<th width="100">Tanggal</th>
					<th width="100">User</th>
					<th width="200">Posisi</th>
					<th width="200">Keterangan</th>
				</tr>
			</thead>
			<tbody id="histBody">
			</tbody>
		</table>
	</div>
	</div>