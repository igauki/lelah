<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/dwr/interface/ajaxManager.js"></script><!-- DWR (Ajax) Exposed Spring Services -->
<script type="text/javascript" src="${path }/dwr/engine.js"></script><!-- DWR (Ajax) Engine -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript">
    hideLoadingMessage();
    
    function cari(){
        createLoadingMessage();    
    }
    
    function backToParent(id){
        createLoadingMessage();
        var dok;
        if(self.opener)
            dok = self.opener.document;
        else
            dok = parent.document;
        
        dok.getElementById('infoFrame').src='${path}/bas/view_detail_ssp.htm?id='+id; 
        if(self.opener) window.close();
        
        
    }

</script>
</head>
<BODY onload="resizeCenter(800,400); document.title='PopUp :: Cari SIMAS SAVING PLAN'; setupPanes('container1', 'tab1'); document.formpost.kata.select(); " style="height: 100%;">

    <div class="tab-container" id="container1">
        <ul class="tabs">
            <li>
                <a href="#" onClick="return showPane('pane1', this)" id="tab1">Cari SPAJ</a>
            </li>
        </ul>

        <div class="tab-panes">

            <div id="pane1" class="panes">
                <form method="post" name="formpost" action="${path }/bas/cari_ssp.htm" style="text-align: center;">
                    <table class="entry2">
                        <tr>
                            <th>No. ID:</th>
                            <th class="left">
                                <input type="text" name="id" size="10" value="${param.id}" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}" />
                            </th>
                        </tr>
                        <tr>
                            <th>Nama:</th>
                            <th class="left">
                                <input type="text" name="nama" size="35" value="${param.nama}" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return flase;}" />
                            </th>
                        </tr>
                        <tr>
                            <th>Kanwil:</th>
                            <th class="left">
                                <input type="text" name="no_kanwil" size="10" value="${param.no_kanwil}" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}" />
                            </th>
                        </tr>
                        <tr>
                            <th>No Rek:</th>
                            <th class="left">
                                <input type="text" name="no_rek" size="35" value="${param.no_rek}" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}" />
                            </th>
                        </tr>
                        <tr>
                            <th colspan="2">
                                <input type="submit" name="search" value="Search" onclick="return cari();"
                                    accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
                            </th>
                        </tr>
                    </table>
                    <table class="simple">
                        <thead>
                            <tr>
                                <th class="center">No.</th>
                                <th class="center">No. ID</th>
                                <th class="center">Full Name</th>
                                <th class="center">No Rekening</th>
                                <th class="center">Nama Cabang</th>
                                <th class="center">Date of Birth</th>
                                <th class="center">Kurs</th>
                                <th class="center">Set Bulanan</th>
                                <th class="center">No Polis</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="ssp" items="${cmd.listSsp}" varStatus="stat">
                                <tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"   onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
                                    onclick="backToParent('${ssp.NO_ID}');">
                                    <td>${stat.count}</td>
                                    <td>${ssp.NO_ID}</td>
                                    <td>${ssp.FULL_NAME}</td>
                                    <td>${ssp.NO_REK}</td>
                                    <td>${ssp.NAMA_CABANG}</td>
                                    <td><fmt:formatDate value="${ssp.MCP_TGL_LAHIR}" pattern="dd/MM/yyyy" /></td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${ssp.LKU_ID eq '01'}">Rp</c:when>
                                            <c:otherwise>US$</c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td><fmt:formatNumber value="${ssp.SET_BULANAN}" type="currency" currencySymbol="" minFractionDigits="2" maxFractionDigits="2" /></td>
                                    <td><elions:polis nomor="${ssp.NO_POLIS}" /></td>
                                </tr>
                                <c:set var="jml" value="${stat.count}"/>
                                <c:if test="${stat.count eq 1}">
                                    <c:set var="v1" value="${ssp.NO_ID}"/>
                                </c:if>
                            </c:forEach>
                        </tbody>
                    </table>
                    <br>
                    <input type="button" name="close" value="Close" onclick="window.close();"
                        accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">

                </form>
            </div>
        </div>
    </div>
<c:if test="${jml eq 1}">
<script>backToParent('${v1}');</script>
</c:if>
</body>
<%@ include file="/include/page/footer.jsp"%>