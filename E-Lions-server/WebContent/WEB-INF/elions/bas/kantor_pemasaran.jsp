<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/header_jquery.jsp"%>
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-blue.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<script>
			$().ready(function() {
				//autocomplete kota
				$( "#kota" ).autocomplete({
					minLength: 3,
					source: "${path}/bas/hadiah.htm?window=json&t=kota",
					focus: function( event, ui ) {
						$( "#kota" ).val( ui.item.key );
						return false;
					},
					select: function( event, ui ) {
						$( "#kota" ).val( ui.item.key);
						return false;
					}
				})
				.data( "autocomplete" )._renderItem = function( ul, item ) {
					return $( "<li></li>" )
						.data( "item.autocomplete", item )
						.append( "<a>" + item.key + "</a>" )
						.appendTo( ul );
				};		
				
			}); 
			
			function tampilkan(btn){
				
				if(btn.name=='cari'){
					var kota = $("#kota").val();
					var lar_id = $("#lar_id").val();
					var index = $("#index").val();
					if(kota=='') {
						alert('Silahkan pilih salah kota terlebih dahulu');				
					}else{
						document.getElementById('formFrame').src = '${path}/bas/spaj.htm?window=daftarKantor&kota='+kota+'&lar_id='+lar_id+'&index='+index;
					}
					return false;
				}else if(btn.name=='update') {
					if(!confirm('Apakah Anda ingin mengupdate data ini?')) {
						return false;
					}

				}
					
				document.getElementById('submitMode').value = btn.name;
				document.getElementById('formpost').submit();
			}
			
			hideLoadingMessage();
		</script>
	</head>

	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Daftar Kantor Pemasaran</a>
				</li>
			</ul>
	
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form:form commandName="cmd" id="formpost" method="post" enctype="multipart/form-data">
						<form:hidden id="submitMode" path="submitMode"/>
						<form:hidden id="index" path="index"/>
						<form:hidden id="lar_id" path="lar_id"/>
						<table class="entry2">
							<tr>
								<th>Pilih kota</th>
								<td>
									<form:input id="kota" path="kota"/>
									<input type="button" id="cari" name="cari" value="Cari" style="width: 55px;" onclick="return tampilkan(this);">
								</td>
							</tr>
							<tr>
								<td colspan="2" style="vertical-align: top;">								
									<fieldset>
										<legend>Daftar Kantor Pemasaran</legend>
										<iframe id="formFrame" src="${path}/bas/spaj.htm?window=daftarKantor&kota=${cmd.kota}&lar_id=${cmd.lar_id}&index=${cmd.index}"></iframe>
									</fieldset>
									<c:if test="${not empty cmd.daftarKantor }">
										
										<c:forEach items="${cmd.daftarKantor }" var="d" varStatus="s">
										<fieldset>
										<legend>Status Gedung Kantor Pemasaran</legend>
										<table class="entry2">
											<tr>
												<th width="200">Leader</th>
												<td width="500">${d.LAR_HEAD }</td>
												<td></td>
											</tr>
											<tr>
												<th width="200">Nama Admin</th>
												<td width="500">${d.LAR_NAMA }</td>
												<td></td>
											</tr>
											<tr>
												<th width="200">Region Admin</th>
												<td width="500">${d.LAR_ADMIN }</td>
												<td><form:hidden path="lar_admin"/></td>
											</tr>
											<tr>
												<th width="200">Alamat Kantor</th>
												<td width="500">${d.LAR_ALAMAT }</td>
												<td><form:textarea path="lar_alamat" cols="100"/></td>
											</tr>
											<tr>
												<th>Luas (m2)</th>
												<td>${d.LAR_LUAS }</td>
												<td><form:input path="lar_luas"/></td>
											</tr>
											<%-- <tr>
												<th>Distribusi</th>
												<td>${d.LAR_ADMIN }</td>
												<td><form:input path="lar_admin"/></td>
											</tr> --%>
											<tr>
												<th>Telepon</th>
												<td>${d.LAR_TELPON }</td>
												<td><form:input path="lar_telpon"/></td>
											</tr>
											<tr>
												<th>Fax</th>
												<td>${d.LAR_FAX }</td>
												<td><form:input path="lar_fax"/></td>
											</tr>
											<tr>
												<th>Status Gedung</th>
												<td>${d.LAR_STATUS_GEDUNG }</td>
												<td><form:input path="lar_status_gedung"/></td>
											</tr>
											<tr>
												<th>Sewa Mulai</th>
												<td><fmt:formatDate value="${d.LAR_BEG_DATE_SEWA }" pattern="dd/MM/yyyy"/></td>
												<td>
													<spring:bind  path="cmd.lar_beg_date_sewa">
														<script>inputDate('${status.expression}', '${status.value}', false);</script>
													</spring:bind> 
												</td>
											</tr>
											<tr>
												<th>Sewa Berakhir</th>
												<td><fmt:formatDate value="${d.LAR_END_DATE_SEWA }" pattern="dd/MM/yyyy"/></td>
												<td>
													<spring:bind  path="cmd.lar_end_date_sewa">
														<script>inputDate('${status.expression}', '${status.value}', false);</script>
													</spring:bind> 
												</td>
											</tr>
										</table>
										</fieldset>
										<br>
										<!-- hanya admin cabangnya yg bisa melihat -->
										<c:if test="${cmd.akses eq 1 or (sessionScope.currentUser.lus_id eq 3732)}">
										<fieldset>
										<legend>Surat Domisili</legend>
										<table class="entry2">
											<tr>
												<th width="200">Nomor Surat</th>
												<td width="500">${d.LAR_NO }</td>
												<td><form:input path="lar_no"/></td>
											</tr>
											<tr>
												<th>Tanggal Mulai</th>
												<td><fmt:formatDate value="${d.LAR_BEG_DATE_DOMISILI }" pattern="dd/MM/yyyy"/></td>
												<td>
													<spring:bind  path="cmd.lar_beg_date_domisili">
														<script>inputDate('${status.expression}', '${status.value}', false);</script>
													</spring:bind> 
												</td>
											</tr>
											<tr>
												<th>Tanggal Berakhir</th>
												<td><fmt:formatDate value="${d.LAR_END_DATE_DOMISILI }" pattern="dd/MM/yyyy"/></td>
												<td>
													<spring:bind  path="cmd.lar_end_date_domisili">
														<script>inputDate('${status.expression}', '${status.value}', false);</script>
													</spring:bind> 
												</td>
											</tr>
											<tr>
												<th>Upload Dokumen </th>
												<td>
													<c:if test="${not empty cmd.ls_attachment }">
											    		<c:forEach items="${cmd.ls_attachment }" var="at" varStatus="s">
											    			<c:if test="${at.dok eq \"surat_domisili.pdf\" }">
											    				<div style="font-style: italic;">
											    				<a href="${path}/bas/spaj.htm?window=daftarKantor&file=${at.dok}&lar_id=${cmd.lar_id}" target="_blank">${at.dok }</a></div>
											    			</c:if>
											    		</c:forEach>
										    		</c:if>
												</td>
												<td><input type="file" name="file1" id="file1"></td>
											</tr>
										</table>
										</fieldset>
										<fieldset>
										<legend>Neon Box</legend>
										<table class="entry2">
											<tr>
												<th width="200">Tgl Jatuh Tempo Pembayaran Pajak Reklame</th>
												<td width="500"><fmt:formatDate value="${d.LAR_END_DATE_REKLAME }" pattern="dd/MM/yyyy"/></td>
												<td>
													<spring:bind  path="cmd.lar_end_date_reklame">
														<script>inputDate('${status.expression}', '${status.value}', false);</script>
													</spring:bind>
												</td>
											</tr>
											<tr>
												<th>Upload Foto Neonbox/Billboard </th>
												<td>
													<c:if test="${not empty cmd.ls_attachment }">
											    		<c:forEach items="${cmd.ls_attachment }" var="at" varStatus="s">
											    			<c:if test="${at.dok eq 'foto_neonbox.pdf' }">
											    				<div style="font-style: italic;">
											    				<a href="${path}/bas/spaj.htm?window=daftarKantor&file=${at.dok}&lar_id=${cmd.lar_id}" target="_blank">${at.dok }</a></div>
											    			</c:if>
											    		</c:forEach>
										    		</c:if>
												</td>
												<td><input type="file" name="file2" id="file2"></td>
											</tr>
											<tr>
												<th>Upload Foto Backdrop </th>
												<td>
													<c:if test="${not empty cmd.ls_attachment }">
											    		<c:forEach items="${cmd.ls_attachment }" var="at" varStatus="s">
											    			<c:if test="${at.dok eq \"foto_backdrop.pdf\" }">
											    				<div style="font-style: italic;">
											    				<a href="${path}/bas/spaj.htm?window=daftarKantor&file=${at.dok}&lar_id=${cmd.lar_id}" target="_blank">${at.dok }</a></div>
											    			</c:if>
											    		</c:forEach>
										    		</c:if>
												</td>
												<td><input type="file" name="file3" id="file3"></td>
											</tr>
											<tr>
												<th>Upload List Asset </th>
												<td>
													<c:if test="${not empty cmd.ls_attachment }">
											    		<c:forEach items="${cmd.ls_attachment }" var="at" varStatus="s">
											    			<c:if test="${at.dok eq \"list_asset.pdf\" }">
											    				<div style="font-style: italic;">
											    				<a href="${path}/bas/spaj.htm?window=daftarKantor&file=${at.dok}&lar_id=${cmd.lar_id}" target="_blank">${at.dok }</a></div>
											    			</c:if>
											    		</c:forEach>
										    		</c:if>
												</td>
												<td><input type="file" name="file4" id="file4"></td>
											</tr>
										</table>
										</fieldset>
										</c:if>
										<!-- end -->
										<br>
										<fieldset>
										<table class="entry2">
											<tr>
												<td>
													<input id="lastUpdate" type="button" value="Last Update : <fmt:formatDate value="${d.LAR_UPDATE_DATE }" pattern="dd/MM/yyyy"/>" name="lastUpdate" disabled="disabled">
													<c:if test="${(sessionScope.currentUser.lus_bas eq 1) or (sessionScope.currentUser.lus_id eq 3732)}">
														<input id="history" type="button" value="Summary" name="history" onclick="return tampilkan(this);">
													</c:if>
													<c:if test="${(cmd.akses eq 1) or (sessionScope.currentUser.lus_id eq 3732)}">
														<input id="update" type="button" value="Update" name="update" onclick="return tampilkan(this);">
													</c:if>
												</td>
											</tr>
										</table>
										</fieldset>
										
										<c:if test="${cmd.submitMode eq \"history\" or sessionScope.currentUser.lus_id eq 3732}">
											<br/>
											<fieldset>
											<legend>Summary</legend>
											<table class="entry2">
												<tr>
													<th>No</th>
													<th>Tanggal</th>
													<th>User</th>
													<th>Keterangan</th>
												</tr>
												<c:forEach items="${cmd.history }" var="h" varStatus="s">
													<tr>
														<td align="center">${s.index+1 }.</td>
														<td align="center"><fmt:formatDate value="${h.CREATE_DATE }" pattern="dd/MM/yyyy"/></td>
														<td align="center">${h.USER_UPDATE }</td>
														<td>${h.KETERANGAN }</td>
													</tr>
												</c:forEach>
											</table>
											</fieldset>							
										</c:if>
										</c:forEach>
									</c:if>
								</td>
							</tr>
						</table>
					</form:form>
				</div>
			</div>
		</div>
	</body>
	<script>
		<c:if test="${not empty param.sukses}">
			alert('${param.sukses}');
		</c:if>
		
		<c:if test="${not empty param.kota}">
			$("#kota").val('${param.kota}');
			$("#cari").click();
		</c:if>
	</script>
</html>