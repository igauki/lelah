<%@ include file="/include/page/header.jsp"%>
<script>
	function awal_show(){
		setFrameSize('infoFrame', 45);
		var h=parent.document.getElementById('infoFrame').height;
		if(h==1)
			parent.document.getElementById('infoFrame').height=800-45;
		
	}
	
	function cari_batal(nomor){
		addOptionToSelect(parent.document, parent.document.formpost.nomor, nomor, nomor+'~');
		parent.tampildata();
	}
	
</script>
<body onload="awal_show();" onresize="setFrameSize('infoFrame', 45);" style="height: 100%;">
<!--<body>-->
<table width="100%">
		<tr align="center"> ${cmd.nomor}
			<th>BUKTI TTIPAN PREMI PERTAMA</th>
		</tr>
		<tr align="center">
			<th><font size="1">(Tidak berlaku sebagai bukti pembayaran premi lanjutan)</font></th>
		</tr>
		<tr align="center">
			<th>NO. BTPP: &nbsp;${btpp.mst_no}<th>
			<th>
				<c:if test="${btpp.mst_flag_batal eq 1}">
				<font size="1" COLOR="RED" bold;>Belum dilakukan Penginputan ulang</font>
				</c:if>
			</th>
		</tr>
</table>
<form name="formpost" method="post">
<table class="entry2" width="100%">
		<tr align="left">
			<th><input type="hidden"></th>
			<th><input type="hidden"></th>
			<th align="rigth">Tgl Input: </th>
			<th><fmt:formatDate value="${btpp.mst_tgl_input}" pattern="dd/MM/yyyy"/>
			</th>
			
		</tr>
		<tr align="left">
			<th>Sudah Terima Dari </th>
			<td>
				${btpp.mst_nm_pemegang }
			</td>
		
			<th>
				<c:if test="${btpp.mst_flag_batal eq 5}">
				No.Refferensi BTPP batal: 
				<th>${btpp.mst_no_reff_btl}<th>
				</c:if>
				<c:if test="${btpp.mst_flag_batal eq 0}">
				<input type="hidden">
				</c:if>
			</th>
		</tr>
		<tr align="left">	
			<th rowspan="2">Uang sejumlah</th>
			<td rowspan="2">
				<fmt:formatNumber value="${btpp.tot_byr}" type="currency" currencySymbol="Rp." maxFractionDigits="2" minFractionDigits="0" />
				&nbsp;&nbsp; Untuk pembayaran premi pertama
			</td>
			<th>
				<c:if test="${btpp.mst_flag_batal eq 5}">
				No.BTPP baru: 
				<th>${btpp.mst_no}<th>
				</c:if>
				<c:if test="${btpp.mst_flag_batal eq 0}">
				<input type="hidden">
				</c:if>
			</th>
			<tr>
				<th><input type="hidden"></th>	
			</tr>
		</tr>	
		<tr align="left">
			<th>Status BTPP </th>
			<td>
				<c:if test="${btpp.mst_flag_batal eq 5}">
				${btpp.mst_no_reff_btl}&nbsp; &nbsp; &nbsp;<font size="1" COLOR="RED" bold;>Batal</font>
				</c:if>
				<c:if test="${btpp.mst_flag_batal eq 0}">
				${btpp.mst_no}&nbsp;&nbsp;&nbsp; Ok
				</c:if>
				<c:if test="${btpp.mst_flag_batal eq 1}">
				${btpp.mst_no}&nbsp;&nbsp;&nbsp; BTPP Penganti Ok
				</c:if>
			</td>		
		</tr>
	
</table>
<table width="100%">	
	<tr align="left">
	<td>
		<fieldset>
		<legend>Premi </legend>
		<display:table id="baris" name="lsBtpp" class="displayTag" decorator="org.displaytag.decorator.TotalTableDecorator">   
			<display:column property="premi" title="PREMI" format="{0, number, #,##0.00;(#,##0.00)}"/>                                                                                        
			<display:column property="extra_premi" title="EXTRA PREMI" format="{0, number, #,##0.00;(#,##0.00)}"/>                                                                                        
			<display:column property="biaya_polis" title="BIAYA POLIS" format="{0, number, #,##0.00;(#,##0.00)}"/>  
			<display:column property="tot_byr" title="JUMLAH PEMBAYARAN" format="{0, number, #,##0.00;(#,##0.00)}"/>  
			<display:column property="prde_byr_awal" title="Periode awal" format="{0, date, dd/MM/yyyy}"/> 
			<display:column property="prde_byr_akhr" title="Periode akhir" format="{0, date, dd/MM/yyyy}" /> 	                                                                                                                                             
		</display:table>       
		</fieldset>
	</td>	
	</tr>
</table>

<table width="100%" >
	<tr><td width="50%">
	<table width="100%" border="1">
		<tr align="left">
			<tr>
				<td class="caption">Uang Pertanggungan</td>
				<td colspan="3">
					<fmt:formatNumber value="${btpp.up}" type="currency" currencySymbol="Rp." maxFractionDigits="2" minFractionDigits="0" /> 
				</td>
			</tr>
			<tr>
				<td class="caption">Kode Agen</td>
				<td colspan="3">
					<textarea name="textarea" cols="20" rows="1" onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); ">${btpp.kd_agen}</textarea> &nbsp;&nbsp;&nbsp;
				</td>
			</tr>
			<tr>
				<td class="caption">Nama Penutup</td>
				<td colspan="3">
					<textarea name="textarea" cols="20" rows="1" onkeyup="textCounter(this, 200); "
						onkeydown="textCounter(this, 200); ">${btpp.nm_penutup}</textarea> 
				</td>
			</tr>
		</tr>
	</table>
	</td><td width="50%">
	<table width="100%" >
		<tr align="center">
			<td>
				<p>${btpp.lst_nm_cab},  ${commandBtpp.s_tgl_rk}</p></br>
			</td>
		</tr>
		<tr>
			<td><td>
		</tr>
		<tr></tr>
		<tr></tr>
		<tr></tr>
		<tr></tr>
		<tr align="center">
			<td>(                ${btpp.lst_nm_admin}                    )</td>
		<tr align="center">
			<td>Tanda tangan & nama jelas branch admin </td>
		</tr>
	</table>
	</td></tr>
	<tr>
	<td>
		<UL TYPE=DISC>
		<LI><font size="1" ;>Pembayaran dengan giro/bilyet/ cheque atas nama PT. Asuransi Jiwa Sinarmas MSIG
		<LI><font size="1" ;>Pembayaran diatas dianggap sah, apabila sudah diterima direkening PT. Asuransi Jiwa Sinarmas MSIG
		</UL>
	</td>
	<tr>

</table>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>