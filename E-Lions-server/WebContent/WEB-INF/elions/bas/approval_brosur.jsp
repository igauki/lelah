<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<script>
			/*
			popWin('${path}/bas/spaj.htm?window=form_spaj', 350, 450);
			showFrame('${path}/report/bas.htm?window=form_spaj');
			function showFrame(x){
				document.getElementById('infoFrame').src=x;
			}
			*/
			
			function tampilkan(btn){

				if(btn.name=='approve') {
					if(!confirm('Setujui Permintaan Ini? \nPermintaan akan diteruskan ke PIC masing-masing busdev untuk dikirim ke cabang.')) {
						return false;
					}
				} else if(btn.name=='reject') {
					if(!confirm('Tolak permintaan ini?')) {
						return false;
					}
				
				} else if(btn.name=='show') {
					var lca_id = document.getElementById('lca_id').value;
					if(lca_id=='') {
						alert('Silahkan pilih salah satu cabang terlebih dahulu');				
					}else{
						document.getElementById('formFrame').src = '${path}/bas/spaj.htm?window=daftarFormBrosur&posisi=0&lca_id=' + lca_id;
					}
					return false;
				}
					
				document.getElementById('submitMode').value = btn.name;
				btn.disabled=true;
				document.getElementById('formpost').submit();
			}
			
			hideLoadingMessage();
		</script>
	</head>

	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Approval Permintaan Brosur</a>
					<a href="#" onClick="return showPane('pane2', this)" id="tab2">History Posisi</a>
					<a href="#" onClick="return showPane('pane3', this)" id="tab3">Legend</a>
				</li>
			</ul>
	
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form:form commandName="cmd" id="formpost" method="post">
						<form:hidden id="submitMode" path="submitMode"/>
						<form:hidden id="index" path="index"/>
						<table class="entry2">
							<tr>
								<th style="width: 150px;">
									<input type="hidden" name="posisi" >
									<select id="lca_id" name="lca_id" size="${sessionScope.currentUser.comboBoxSize}
											" style="width: 140px;" onclick="document.getElementById('show').click();">
										<c:forEach var="d" items="${cmd.daftarCabang}">
											<option value="${d.KEY}" 
												<c:if test="${d.KEY eq cmd.lca_id}"> selected </c:if>
											>${d.VALUE}</option>
										</c:forEach>
									</select>
									<input type="button" id="show" name="show" value="Show" style="width: 55px;" onclick="return tampilkan(this);">
								</th>
								<td style="vertical-align: top;">								
									<fieldset>
										<legend>Daftar Permintaan Brosur</legend>
										<iframe id="formFrame" src="${path}/bas/spaj.htm?window=daftarFormBrosur&posisi=0&lca_id=${cmd.lca_id}&msf_id=${cmd.msf_id}&index=${cmd.index}"></iframe>
									</fieldset>
									<fieldset>
										<legend>Form Approval Permintaan Brosur</legend>
										
										<table class="entry2">
											<tr>
												<th>Stok Brosur Cabang</th>
												<td colspan="3">
													<c:choose>
														<c:when test="${not empty cmd.daftarStokBrosur}">
															<table class="displaytag" style="width:auto;">
																<thead>
																	<tr>
																		<c:forEach var="s" items="${cmd.daftarStokBrosur}">
																			<th style="width: 50px; " title="${s.lsjs_desc}">${s.lsjs_prefix}</th>
																		</c:forEach>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<c:forEach var="s" items="${cmd.daftarStokBrosur}">
																			<td title="${s.lsjs_desc}">${s.mss_amount}</td>
																		</c:forEach>
																	</tr>
																</tbody>
															</table>
														</c:when>
														<c:otherwise>-</c:otherwise>
													</c:choose>
												</td>
											</tr>
											<tr>
												<th>Nomor Form</th>
												<td colspan="3">
													<form:input id="msf_id" path="msf_id" cssClass="readOnly" readonly="true" size="20"/>												
												</td>
											</tr>
											<c:if test="${not empty cmd.daftarFormBrosur}">
												<tr>
													<th>Status</th>
													<td colspan="3">
														<input type="text" value="${cmd.daftarFormBrosur[0].status_form}" readonly="true" size="20" value="${cmd.daftarFormBrosur[0].status_form}" style="background-color: ${cmd.daftarFormBrosur[0].warna};">
													</td>
												</tr>
												<tr>
													<th>Cabang</th>
													<td colspan="3">
														<input type="text" class="readOnly" readonly="true" size="40" value="${cmd.daftarFormBrosur[0].lca_nama}"/>
													</td>
												</tr>
												<tr>
													<th>Pemohon</th>
													<td>
														<input type="text" class="readOnly" readonly="true" size="40" value="${cmd.formHistUser.lus_login_name}"/>
													</td>
													<th width="97px">Travel Ins</th>
													<td>
														<select id=admTravIns name="admTravIns">
															<option value="">Pilih jenis</option>
															<c:forEach items="${cmd.typeTravelIns}" var="x">
																<option value="${x.key}" <c:if test="${x.key eq cmd.daftarFormBrosur[0].trav_ins_type}">selected="selected"</c:if>>${x.value}</option>
															</c:forEach>
														</select>
													</td>
												</tr>
												<tr>
													<th>BusDev</th>
													<td colspan="3">
														<%-- <input type="text" value="${cmd.daftarFormBrosur[0].status_form}" readonly="true" size="20" style="background-color: ${cmd.daftarFormBrosur[0].warna};"> --%>
														<form:select path="daftarFormBrosur[0].busdev" disabled="disabled">
															<form:option value="" label="--Pilih--"/>
															<form:option value="1" label="Agency"/>
															<form:option value="2" label="Corporate"/>
															<form:option value="3" label="Bancass 1 (Team Ibu Yanti S) Bancass Support"/>
															<form:option value="4" label="Bancass 2 & Bancass 1 BusDev"/>
														</form:select>
													</td>
												</tr>
												<tr>
													<th>Detail Permintaan</th>
													<td colspan="3">
														<table class="displaytag" style="width:auto;">
															<thead>
																<tr>
																	<th rowspan="2">No.</th>
																	<th rowspan="2">Jenis Brosur</th>
																	<th rowspan="2">Prefix</th>
																	<th colspan="2">Jumlah Brosur</th>
																	<!-- <th rowspan="2">No. Blanko</th> -->
																	<th rowspan="2">Status</th>
																</tr>
																<tr>
																	<th>Diminta</th>
																	<th>Disetujui</th>
																</tr>
															</thead>
															<tbody>
															<c:forEach var="daftar" items="${cmd.daftarFormBrosur}" varStatus="st">
																<tr>
																	<td style="text-align: left;">${st.count}.</td>
																	<td style="text-align: left;">${daftar.lsjs_desc}</td>
																	<td style="text-align: center;">${daftar.lsjs_prefix}</td>
																	<td style="text-align: center;">
																		<form:input path="daftarFormBrosur[${st.index}].msf_amount_req" size="10" cssStyle="text-align: right;" cssClass="readOnly" readonly="true"/>
																	</td>
																	<td style="text-align: center;">
																		<form:input path="daftarFormBrosur[${st.index}].msf_amount" size="10" cssStyle="text-align: right;" cssClass="readOnly" readonly="true"/>
																	</td>
																	<%-- <td style="text-align: center;">
																		<form:input path="daftarFormBrosur[${st.index}].no_blanko_req" size="50" cssClass="readOnly" readonly="true"/>
																	</td> --%>
																	<td style="text-align: center;">${daftar.status_form}</td>
																</tr>
															</c:forEach>
	
															<tr>
																<td colspan="3" style="text-align: center;">Total Brosur: </td>
																<td style="text-align: center;">
																	?
																</td>
															</tr>
															
															</tbody>
														</table>
													</td>
												</tr>
												<tr>
													<th>Keterangan</th>
													<td>
														<form:textarea path="formHist.msfh_desc" cols="70" rows="2" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "/>
														<span class="error">*</span>
													</td>
												</tr>
												<tr>
													<th>Approve / Reject</th>
													<td>
														<input type="button" value="Approve" name="approve" onclick="return tampilkan(this);">
														<input type="button" value="Reject" name="reject" onclick="return tampilkan(this);">
														<c:choose>
															<c:when test="${not empty infoMessage }"><div id="success" style="text-transform: none;">${infoMessage}</div></c:when>
															<c:otherwise>
																<spring:hasBindErrors name="cmd">
																	<div id="error" style="text-transform: none;">
																		<form:errors path="*"/>
																	</div>
																</spring:hasBindErrors>
															</c:otherwise>
														</c:choose>
													</td>
												</tr>
											</c:if>
										</table>								
									</fieldset>
								</td>
							</tr>
						</table>
					</form:form>
				</div>
				<div id="pane2" class="panes">
					<fieldset>
						<legend>History Permintaan Brosur</legend>
						<display:table id="detail" name="cmd.daftarFormHistory" class="displaytag">
							<display:column property="msf_urut" title="No." style="text-align: left;"/>
							<display:column property="msf_id" title="No. Form" style="text-align: center;" />
							<display:column property="status_form" title="Posisi" style="text-align: center;" />
							<display:column property="lus_login_name" title="User" style="text-align: center;" />
							<display:column property="msfh_dt" title="Tanggal" format="{0, date, dd/MM/yyyy (hh:mm)}" style="text-align: center;" />
							<display:column property="msfh_desc" title="Keterangan" style="text-align: center;" />
						</display:table>
					</fieldset>
				</div>		
				<div id="pane3" class="panes">
					<jsp:include flush="true" page="legend_brosur.jsp" />
				</div>
			</div>
		</div>
	</body>
	<script>
		<c:if test="${not empty param.sukses}">
			alert('${param.sukses}');
			
			var direct = '${param.direct}';
			if(direct==1)window.location.href = "http://elions.sinarmasmsiglife.co.id";
			
		</c:if>
	</script>
</html>