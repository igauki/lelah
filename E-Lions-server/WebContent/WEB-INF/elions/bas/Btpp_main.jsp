<%@ include file="/include/page/header.jsp"%>
<script>
	
	function tampildata(){
		nomor=trim(formpost.nomor.value);
		nomor=nomor.substring(0,nomor.indexOf('~'));
		document.getElementById('infoFrame').src='${path }/bas/btpp.htm?window=show_btpp&nomor='+nomor;
		
	}
	
	function cetak_btpp(){
	alert('Cetak');
		nomor=trim(formpost.nomor.value);
		nomor=nomor.substring(0,nomor.indexOf('~'));
		document.getElementById('infoFrame').src='${path }/report/uw.htm?window=cetak_btpp&mst_no='+nomor+'&print=true';
	}
	
	function edit_btpp(){
	alert('EDIT');
		//proses=1==>input proses=2==>edit proses=3==>edit tgl setor
		nomor=trim(formpost.nomor.value);
		desc=prompt("Silahkan Masukan Alasan Untuk Edit BTPP","");
		if(desc!=null){
			document.getElementById('infoFrame').src='${path}/bas/input_btppNew.htm?nomor='+nomor+'&proses=2&desc='+desc; 
		}
		
	}
	
	function input_btpp(){
		document.getElementById('infoFrame').src='${path }/bas/input_btppNew.htm?proses=1';
	}


	function batal(){
		nomor=trim(formpost.nomor.value);
		nomorNew=nomor.substring(0,nomor.indexOf('~'));	
		if(confirm("Apakah Anda ingin membatalkan No BTPP ("+nomorNew+") ?")){
			alasan=prompt("Masukan ALasan Pembatalan Polis?","");
			if(alasan!=null){
				document.getElementById('infoFrame').src='${path}/bas/input_btppNew.htm?nomor='+nomorNew+'&proses=4&alasan='+alasan; 
			}
		}
	}
	
	
</script>
<body onload="setFrameSize('infoFrame', 45); input_btpp();" onresize="setFrameSize('infoFrame', 45);" style="height: 100%;" >
<form name="formpost" method="post">
<table class="entry2" width="98%">
	<tr align="left">
		<th>Nomer</th>
		<td>
			<select name="nomor">
				<c:forEach var="s" items="${lsBtpp}">
					<option value="${s.mst_no}~
						<c:choose>
							<c:when test="${s.flag_print eq null || s.flag_print eq 0}">0</c:when>
						<c:otherwise>${s.flag_print }</c:otherwise>
						</c:choose>	
					" >
						${s.mst_no}
					</option>
				</c:forEach>
			</select>	
													
<!--		perlu di benerin niy...
			onclick="popWin('${path}/bas/btpp.htm?window=cari_nomorBtpp', 350, 500);"-->
			<input type="button" name="cari" value="Cari Nomor BTPP" onclick="popWin('${path}/bas/btpp.htm?window=cari_nomorBtpp', 350, 500);"
				accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();" >
<!--			<input type="button" name="cari" value="Cari Nomor" onclick="popWin('${path}/bas/cari_nomorBtpp.htm?window=cari_nomor', 350, 500);"-->
<!--				accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();" >-->
			<input type="button" name="tampil" value="Tampilkan Data" onClick="tampildata()"
			accesskey="T" onmouseover="return overlib('Alt-T', AUTOSTATUS, WRAP);" onmouseout="nd();">
		</td>
	</tr>
	<tr>
		<th align="left">Proses</th>
		<td>
			<input type="button" name="input_b" value="Input Btpp" onClick="input_btpp();"
			accesskey="I" onmouseover="return overlib('Alt-I', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" name="edit" value="Edit" onclick="edit_btpp();"
			accesskey="E" onmouseover="return overlib('Alt-E', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" name="print" value="Print" onclick="cetak_btpp();"
			accesskey="P" onmouseover="return overlib('Alt-P', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" name="btn_batal" value="Batal" onclick="batal();"
			accesskey="B" onmouseover="return overlib('Alt-B', AUTOSTATUS, WRAP);" onmouseout="nd();">
		</td>
	</tr>
	<tr align="left">
	<td colspan="2">
		<iframe src="" name="infoFrame" id="infoFrame"
			width="100%"  > Please Wait... </iframe>
	</td>
	</tr>
</table>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>