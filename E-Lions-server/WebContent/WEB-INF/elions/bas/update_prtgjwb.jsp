<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script>
			function tampilkan(btn){

				if(btn.name=='save') {
					if(!confirm('Simpan Perubahan?')) {
						return false;
					}
				} else if(btn.name=='show') {
					if(document.getElementById('msf_id').value=='') {
						alert('Silahkan pilih salah satu nomor terlebih dahulu');				
						return false;
					}
				}
					
				document.getElementById('submitMode').value = btn.name;
				btn.disabled=true;
				document.getElementById('formpost').submit();
				
			}
			
			hideLoadingMessage();
		</script>		
	</head>
	<body>
		<form:form commandName="cmd" id="formpost" method="post">
		 	<table class="entry2">
				<tr height="150 px">
					<td style="vertical-align: top;" colspan="2" height="150 px">		 
						<div id="list1">
						<fieldset>
							<legend>Viewer Agen</legend>
							<iframe  style="height=150 px"  id="formFrame" src="${path}/bas/spaj.htm?window=viewerAgenNew"></iframe>
						</fieldset>	
						</div>				
					</td>
					<form:hidden id="msab_id" path="agen.msab_id"/>
					<form:hidden id="lca_id" path="agen.lca_id"/>
					<form:hidden id="lwk_id" path="agen.lwk_id"/>
					<form:hidden id="lsrg_id" path="agen.lsrg_id"/>
					<form:hidden id="msag_id" path="agen.msag_id"/>
					<form:hidden id="submitMode" path="submitMode"/>					
				</tr>
				<c:choose>
					<c:when test="${not empty cmd.agen.msab_id}">
						<tr height="650 px">
							<td style="vertical-align: top;" colspan="2" height="650 px">		 
								<div id="list">
									<fieldset>
										<legend>Daftar Pertanggungjawaban Agen</legend>
										<iframe style="height=650 px;width: 100%;"  id="formFrame2" src="${path}/bas/kontrol_tgjwb.htm?flag=2&lca_id=${cmd.agen.lca_id}&msag_id=${cmd.agen.msag_id}&msab_id=${cmd.agen.msab_id}"></iframe>
									</fieldset>
								</div>
							</td>	
						</tr>
					</c:when>
				</c:choose>					
		 	</table>
		 	<!--<input type="button" value="test" onclick="test();">-->
		</form:form>
	</body>
</html>
