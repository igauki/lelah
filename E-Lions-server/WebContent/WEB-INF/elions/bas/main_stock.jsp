<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<script type="text/javascript" src="${path}/include/jquery/jquery-1.5.1.min.js"></script>
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen"> <!-- CSS untuk DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script><!-- DatePicker Script (jscalendar) -->
		
		<script>
			function isNumberKey(evt){
	      		var charCode = (evt.which) ? evt.which : event.keyCode;
	          	if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
		             return false;
		
	          	return true;
			}
			
			function showReport(format){
				var edate=$('#_tanggalAkhir').val();
				var bdate=$('#_tanggalAwal').val();
				var flag=1
					if(document.getElementById('filter').checked) {
					    flag=2;
					}
				popWin('${path}/report/uw.htm?window=reportStockSpaj&bdate='+bdate+'&edate='+edate+'&format='+format+'&flag='+flag, 600, 900);
			}
			
			var fieldName='chbox';
	
			function selectall(){
			  var i=document.formpost.elements.length;
			  var e=document.formpost.elements;
			  var name=new Array();
			  var value=new Array();
			  var j=0;
			  for(var k=0;k<i;k++)
			  {
			    if(document.formpost.elements[k].name==fieldName)
			    {
			      if(document.formpost.elements[k].checked==true){
			        value[j]=document.formpost.elements[k].value;
			        j++;
			      }
			    }
			  }
			  checkSelect();
			}
				
			function selectCheck(obj)
			{
			 var i=document.formpost.elements.length;
			  for(var k=0;k<i;k++)
			  {
			    if(document.formpost.elements[k].name==fieldName)
			    {
			      document.formpost.elements[k].checked=obj;
			    }
			  }
			  selectall();
			}
				
			function selectallMe()
			{
			  if(document.formpost.allCheck.checked==true)
			  {
			   selectCheck(true);
			  }
			  else
			  {
			    selectCheck(false);
			  }
			}
				
			function checkSelect()
			{
			 var i=document.formpost.elements.length;
			 var berror=true;
			  for(var k=0;k<i;k++)
			  {
			    if(document.formpost.elements[k].name==fieldName)
			    {
			      if(document.formpost.elements[k].checked==false)
			      {
			        berror=false;
			        break;
			      }
			    }
			  }
			  if(berror==false)
			  {
			    document.formpost.allCheck.checked=false;
			  }else
			  {
			    document.formpost.allCheck.checked=true;
			  }
			}
			hideLoadingMessage();
			
		</script>
	</head>

	<body onload="setupPanes('container1', 'tab1');" style="height: 100%;">
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
				<c:choose>
						<c:when test="${tab eq \"1\" }">
							<a href="#" onClick="return showPane('pane1', this)" id="tab1">Update Stok SPAJ</a>
						</c:when>
						<c:when test="${tab eq \"3\" }">
							<a href="#" onClick="return showPane('pane1', this)" id="tab1">View Summary Stock SPAJ</a>
						</c:when>
						<c:otherwise>
							<a href="#" onClick="return showPane('pane1', this)" id="tab1">Input SPAJ keluaran (Manual UW NB)</a>
						</c:otherwise>
					</c:choose>
				</li>
			</ul>

		<div class="tab-panes">
			<c:choose>
				<c:when test="${tab eq \"1\" }">
					<div id="pane1" class="panes">
						<form id="formpost" name="formpost" method="post">
							<table class="entry" style="width: 700px;" align="left">
								<tr>
									<th colspan="6">Tanggal Cetakan&nbsp;&nbsp;&nbsp;<script>inputDate('tanggalAwal', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>
									</th>
								</tr>
								<tr>
									<th><input type="checkbox" name="allCheck"
										onclick="selectallMe()" style="border: none;">
									</th>
									<th>ID</th>
									<th>Pre-Fix</th>
									<th>Diskripsi</th>
									<th>Stock</th>
									<th>Update Stok</th>
								</tr>
								<c:forEach items="${brosur}" var="s">
									<tr>
										<th align="center"><input name="chbox" type="checkbox"
											id="chbox" value="${s.LSJS_ID}" style="border: none;">
										</th>
										<th>${s.LSJS_ID } <input type="hidden"
											id="lsjs_${s.LSJS_ID }" name="lsjs_${s.LSJS_ID }"
											value="${s.LSJS_ID}"></th>
										<th>${s.LSJS_PREFIX }</th>
										<th>${s.LSJS_DESC }</th>
										<th>${s.LSJS_STOCK }</th>
										<th><input type="text" style="text-align: right;"
											name="stock_${s.LSJS_ID}" id="stock_${s.LSJS_ID}" value="0"
											onkeypress="return isNumberKey(event)" />
										</th>
									</tr>
								</c:forEach>
								<tr>
									<td colspan="4" align="center"><input type="submit"
										name="update" id="update" value="Update" />
									</td>
								</tr>
							</table>
						</form>
					</div>
				</c:when>
				<c:when test="${tab eq \"3\" }">
					<div id="pane1" class="panes">
						<form id="formpost" name="formpost" method="post">
							<table class="entry2" style="width: 100%;" align="left">
								<tr>
									<th colspan="8">Periode Tanggal &nbsp;&nbsp;&nbsp;<script>inputDate('tanggalAwal', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>
										s/d &nbsp;&nbsp;&nbsp;<script>inputDate('tanggalAkhir', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>
										&nbsp;&nbsp;&nbsp;Open Detail <input name="filter"
										type="checkbox" id="filter" value="1" style="border: none;">
									</th>
								</tr>
								<c:choose>
									<c:when test="${filter eq \"1\" }">
										<tr>
											<th rowspan="2">Jenis SPAJ </th>
											<th rowspan="2">Prefix</th>
											<th colspan="3">Jumlah SPAJ</th>
											<th rowspan="2">Sisa Stock</th>
										</tr>
										<tr>
											<th>Cetakan</th>
											<th>Keluar ( Manual )</th>
											<th>Keluar ( Otomatis)</th>
										</tr>
										<c:forEach items="${brosur2}" var="s">
											<tr>
												<th>${s.DISKRIPSI }</th>
												<th>${s.PREFIX }</th>
												<th><input type="text" value="${s.STOCK_CETAK }"
													size="15" readonly>
												</th>
												<th><input type="text" value="${s.SPAJ_OUT_MANUAL }"
													size="15" readonly>
												</th>
												<th><input type="text" value="${s.SPAJ_OUT_AUTO }"
													size="15" readonly>
												</th>
												<th>${s.STOCK_NOW }</th>
											</tr>
										</c:forEach>
									</c:when>
									<c:when test="${filter eq \"2\" }">
										<tr>
											<th rowspan="2">Jenis SPAJ</th>
											<th rowspan="2">Prefix</th>
											<th colspan="3">Jumlah SPAJ</th>
											<th rowspan="2">Sisa Stock</th>
											<th rowspan="2">No Blanko</th>
											<th rowspan="2">No Pengiriman</th>
										</tr>
										<tr>
											<th>Cetakan</th>
											<th>Keluar ( Manual )</th>
											<th>Keluar ( Otomatis)</th>
										</tr>
										<c:forEach items="${brosur2}" var="s">
											<tr>
												<th>${s.DISKRIPSI }</th>
												<th>${s.PREFIX }</th>
												<th><input type="text" value="${s.STOCK_CETAK }"size="15" readonly></th>
												<th><input type="text" value="${s.SPAJ_OUT_MANUAL }"size="15" readonly></th>
												<th><input type="text" value="${s.SPAJ_OUT_AUTO }"size="15" readonly></th>
												<th><input type="text" value="${s.STOCK_NOW }"size="15" readonly></th>
												<th>${s.NO_BLANKO }</th>
												<th>${s.NO_PENGIRIMAN }</th>
											</tr>
										</c:forEach>
									</c:when>
									<c:otherwise>
									</c:otherwise>
								</c:choose>
								<tr>
									<td colspan="8" align="center"><input type="submit"
										name="view" id="view" value="view" />
										&nbsp;&nbsp;&nbsp;&nbsp; 
										<a href="#" onclick="return showReport('excel');"
										onmouseover="return overlib('PRINT EXCELL', AUTOSTATUS, WRAP);"
										onmouseout="nd();"> <img style="border:none;" alt="EXCEL"
											src="${path}/include/image/excel.gif" />
									</a> &nbsp;&nbsp;&nbsp;<a href="#"
										onclick="return showReport('pdf');"
										onmouseover="return overlib('PRINT PDF', AUTOSTATUS, WRAP);"
										onmouseout="nd();"> <img style="border:none;" alt="PDF"
											src="${path}/include/image/pdf.gif" /> </a>
									</td>
								</tr>
							</table>
						</form>
					</div>
				</c:when>
				<c:otherwise>
					<div id="pane1" class="panes">
						<form id="formpost" name="formpost" method="post">
							<table class="entry2" style="width: 100%;" align="left">
								<tr>
									<th colspan="6">Tanggal Proses&nbsp;&nbsp;&nbsp;<script>inputDate('tanggalAwal', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>
									</th>
								</tr>
								<tr>
									<th><input type="checkbox" name="allCheck"
										onclick="selectallMe()" style="border: none;">
									</th>
									<th>ID</th>
									<th>Pre-Fix</th>
									<th>Diskripsi</th>
									<th>Stock</th>
									<th>Jumlah SPAJ yg keluar(Manual)</th>
									<th>Keterangan</th>
								</tr>
								<c:forEach items="${brosur}" var="s">
									<tr>
										<th align="center"><input name="chbox" type="checkbox"
											id="chbox" value="${s.LSJS_ID}" style="border: none;">
										</th>
										<th>${s.LSJS_ID } <input type="hidden"
											id="lsjs_${s.LSJS_ID }" name="lsjs_${s.LSJS_ID }"
											value="${s.LSJS_ID}"></th>
										<th>${s.LSJS_PREFIX }</th>
										<th>${s.LSJS_DESC }</th>
										<th>${s.LSJS_STOCK }</th>
										<th><input type="text" style="text-align: right;"
											name="stock_${s.LSJS_ID}" id="stock_${s.LSJS_ID}" value="0"
											onkeypress="return isNumberKey(event)" />
										</th>
										<th><input type="text" style="text-align: left;"
											name="keterangan_${s.LSJS_ID}" id="keterangan_${s.LSJS_ID}" size="100" value=""/>
										</th>
									</tr>
								</c:forEach>
								<tr>
									<td colspan="4" align="center"><input type="submit"
										name="save" id="save" value="Save" />
									</td>
								</tr>
							</table>
						</form>
					</div>
				</c:otherwise>
			</c:choose>
		</div>
	</div>

	</body>
	<script>
		<c:if test="${not empty pesan}">
			alert('${pesan}');
		</c:if>
		$('#jenis').val('${jenis}');
		$('#busdev').val('${busdev}');
		
		$('#jenis2').val('${jenis}');
		$('#busdev2').val('${busdev}');
		
	</script>
</html>