<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-blue.css" media="screen">
		<script type="text/javascript">
			var contextPath = "${path}";
		</script>
		<script type="text/javascript" src="${path }/include/js/calendarDateInput.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>		
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<c:set var="totalAgen" value="0" />
		<c:forEach var="s" items="${cmd.daftarStokSpajAgen}">
			<c:set var="totalAgen" value="${totalAgen+s.mss_amount}" />
		</c:forEach>
		<script>
			/*
			popWin('${path}/bas/spaj.htm?window=form_spaj', 350, 450);
			showFrame('${path}/report/bas.htm?window=form_spaj');
			function showFrame(x){
				document.getElementById('infoFrame').src=x;
			}
			*/

			function tampilkan(btn){
				if(btn.name=='save_agen') {
					var totalAgen = ${totalAgen};
					var tanggal = document.getElementById('tanggal').value;
					if(tanggal == '') {
						var myDate = new Date(); 
						//tanggal = myDate.toLocaleDateString();
						tanggal = myDate.getDate()+'/'+(myDate.getMonth()+1)+'/'+myDate.getFullYear();
					}
					if(totalAgen > 0) pesan = 'Agen ini masih mempunyai ' +totalAgen+ ' blanko SPAJ dari permintaan sebelumnya, \nblanko-blanko tersebut harus dipertanggungjawabkan pada form baru ini.\nSimpan Perubahan untuk permintaan tanggal '+tanggal+' ?';
					else pesan = 'Simpan perubahan untuk permintaan tanggal '+tanggal+' ?';
					if(!confirm(pesan)) {
						return false;
					}
				} else if(btn.name=='detail_blanko') {
					popWin('${path}/bas/spaj.htm?window=daftarBlanko', 200, 200);
					return false;
				} else if(btn.name=='form_tgjwb') {
					//popWin('${path}/bas/kontrol_tgjwb.htm?flag=1&msag_id=${cmd.agen.msag_id}&lca_id=${cmd.lca_id}&msab_id=${cmd.agen.msab_id}', 500, 700);
					//alert("msf_id : ${cmd.msf_id}");
					popWin('${path}/bas/kontrol_tgjwb.htm?msf_id=${cmd.msf_id}&flag=0&msag_id=${cmd.agen.msag_id}&lca_id=${cmd.lca_id}&msab_id=${cmd.agen.msab_id}', 500, 700);
					return false;
				} else if(btn.name=='btnPrint') {
					popWin('${path}/report/bas.htm?window=form_permintaan_spaj_cabang&msf_id=${cmd.msf_id_bef}', 600, 800);
					return false;
				} else if(btn.name=='fitrah') {
					var msf_id = document.getElementById('msf_id_bef').value;
					var msag_id = document.getElementById('msag_id').value;
					var no_blanko = document.getElementById('no_blanko').value;
					popWin('${path}/report/bas.pdf?window=fitrah_card&jenis=agen&no_blanko='+no_blanko+'&msf_id='+msf_id+'&msag_id='+msag_id, 600, 800);
					return false;
				}	
				document.getElementById('submitMode').value = btn.name;
				btn.disabled=true;
				document.getElementById('formpost').submit();
			}
			function showBlanko(lsjsId,ke){
				popWin('${path}/bas/spaj.htm?window=detailBlankoCabang&lsjsId='+lsjsId+'&ke='+ke, 250, 175);
			}
			

			hideLoadingMessage();
		</script>
	</head>

	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

		<c:choose>
			<c:when test="${not empty cmd.daftarFormOverdue}">
				<jsp:include flush="true" page="warning.jsp">
					<jsp:param value="Lanjut ke Halaman Form Agen" name="link1"/>
					<jsp:param value="${path}/bas/form_agen_new.htm?nowarning=true" name="link2"/>
				</jsp:include>
			</c:when>
			<c:otherwise>
				<div class="tab-container" id="container1">
					<ul class="tabs">
						<li>
							<a href="#" onClick="return showPane('pane1', this)" id="tab1">Form Permintaan SPAJ</a>
							<a href="#" onClick="return showPane('pane2', this)" id="tab2">History Posisi</a>
							<a href="#" onClick="return showPane('pane3', this)" id="tab3">Legend</a>
						</li>
					</ul>
			
					<div class="tab-panes">
						<div id="pane1" class="panes">
							<form:form commandName="cmd" id="formpost" method="post">
								<form:hidden id="submitMode" path="submitMode"/>
								<table class="entry2">
									<tr>
										<td style="vertical-align: top;" rowspan="2">
											<table class="entry2">
												<tr>
												<tr height="150 px">
													<td style="vertical-align: top;" colspan="2" height="150 px">
														<fieldset>
															<legend>Viewer Agen</legend>
															<iframe  style="height=150 px"  id="formFrame" src="${path}/bas/spaj.htm?window=viewerAgen"></iframe>
														</fieldset>
													</td>
												</tr>
												<tr>
													<td style="vertical-align: top;" colspan="2">				
														<fieldset>
															<legend>Form Pencatatan Permintaan SPAJ oleh Agen</legend>
															<table class="entry2">
																<tr>
																	<th>Stok SPAJ</th>
																	<td>
																		<table class="displaytag" style="width:auto;">
																			<caption>Cabang</caption>
																			<thead>
																				<tr>
																					<c:forEach var="s" items="${cmd.daftarStokSpaj}">
																						<th style="width: 50px; " title="${s.lsjs_desc}">${s.lsjs_prefix}</th>
																					</c:forEach>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<c:forEach var="s" items="${cmd.daftarStokSpaj}">
																						<td title="${s.lsjs_desc}">${s.mss_amount}</td>
																					</c:forEach>
																				</tr>
																			</tbody>
																		</table>
																		<input type="button" value="Detail" name="detail_blanko" onclick="return tampilkan(this);">
																		<table class="displaytag" style="width:auto;">
																			<caption>Agen</caption>
																			<thead>
																				<tr>
																					<c:forEach var="s" items="${cmd.daftarStokSpajAgen}">
																						<th style="width: 50px; " title="${s.lsjs_desc}">${s.lsjs_prefix}</th>
																					</c:forEach>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<c:forEach var="s" items="${cmd.daftarStokSpajAgen}">
																						<td title="${s.lsjs_desc}">${s.mss_amount}</td>
																					</c:forEach>
																				</tr>
																			</tbody>
																		</table>
																		 
																	</td>
																</tr>
																<tr>
																	<th>Agen</th>
																	<td>
																		<form:hidden id="msab_id" path="agen.msab_id"/>
																		<form:hidden id="lca_id" path="agen.lca_id"/>
																		<form:hidden id="lwk_id" path="agen.lwk_id"/>
																		<form:hidden id="lsrg_id" path="agen.lsrg_id"/>
																		<form:input id="msag_id" path="agen.msag_id" cssClass="readOnly" readonly="true" size="8"/>
																		<c:choose>
																			<c:when test="${cmd.agen.msag_id eq \"000000\" and empty cmd.agen.msab_id}">
																				<form:input id="msab_nama" path="agen.msab_nama" size="30"/>
																			</c:when>
																			<c:otherwise>
																				<form:input id="msab_nama" path="agen.msab_nama" cssClass="readOnly" readonly="true" size="30"/>
																			</c:otherwise>
																		</c:choose>
																	</td>
																</tr>
																<c:choose>
																	<c:when test="${not empty cmd.daftarFormSpaj}">
																		<% /*
																		<tr>
																			<th>Status Permintaan</th>
																			<td>
																				<form:hidden path="daftarFormSpaj[0].status_permintaan" />
																			</td>
																		</tr>
																		*/ %>
																		<tr>
																			<th>Nomor Form Sebelumnya</th>
																			<td>
																				<form:input id="msf_id_bef" path="msf_id_bef" cssClass="readOnly" readonly="true" size="20"/>
																			</td>
																		</tr>
																		<tr>
																			<th>Cabang</th>
																			<td>
																				<input type="text" class="readOnly" readonly="true" size="40" value="${cmd.daftarFormSpaj[0].lca_nama}"/>
																			</td>
																		</tr>
																		<tr>
																			<th>User</th>
																			<td>
																				<input type="text" class="readOnly" readonly="true" size="40" value="${sessionScope.currentUser.name}"/>
																			</td>
																		</tr>
																		<tr>
																			<th>Tanggal Permintaan</th>
																			<td>
																				<spring:bind path="cmd.tanggal">
																					<script>inputDate('${status.expression}', '${status.value}', false);</script>
																				</spring:bind>	
																				<span style="font-size: 7pt;color: red;">* Wajib diisi jika tanggal permintaan bukan hari ini</span>
																			</td>
																		</tr>
																		<tr>
																			<th>No Bukti Pembayaran</th>
																			<td>
																				<form:input path="daftarFormSpaj[0].bukti_pembayaran" size="40" maxlength="12"/>	
																				<span style="font-size: 7pt;color: red;">* Wajib diisi untuk produk PAS</span>
																			</td>
																		</tr>
																		<tr>
																			<th>Detail Permintaan</th>
																			<td>
																				<table class="displaytag" style="width:auto;">
																					<thead>
																						<tr>
																							<th rowspan="2">No.</th>
																							<th rowspan="2">Jenis SPAJ</th>
																							<th rowspan="2">Prefix</th>
																							<th>Jumlah SPAJ</th>
																							<th rowspan="2">No. Blanko</th>
																						</tr>
																						<tr>
																							<th>Diberikan</th>
																						</tr>
																					</thead>
																					<tbody>
																					<c:forEach var="daftar" items="${cmd.daftarFormSpaj}" varStatus="st">
																						<tr>
																							<td style="text-align: left;">${st.count}.</td>
																							<td style="text-align: left;">${daftar.lsjs_desc}</td>
																							<td style="text-align: center;">${daftar.lsjs_prefix}</td>
																							<td style="text-align: center;">
																								<form:input readonly="true" path="daftarFormSpaj[${st.index}].msf_amount_req" size="10" cssStyle="text-align: right;" onfocus="this.select();"/>
																								<span class="error">*</span>
																							</td>
																							<td style="text-align: center;">
																								<form:input readonly="true" id="no_blanko" path="daftarFormSpaj[${st.index}].no_blanko_req" size="50" onfocus="this.select();"/>
																								<span class="error">*</span>
																							</td>
																							<td>
																								<input type="button" value="V" onClick="showBlanko('${daftar.lsjs_id}','${st.index}')">
																							</td>
																						</tr>
																					</c:forEach>
							
																					<tr>
																						<td colspan="3" style="text-align: center;">Total SPAJ: </td>
																						<td style="text-align: center;">
																							?
																						</td>
																					</tr>
																					
																					</tbody>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<th></th>
																			<td>
																				<input type="button" value="Save" name="save_agen" onclick="return tampilkan(this);">
																				<input type="button" value="Pertanggung Jawaban" name="form_tgjwb" onclick="return tampilkan(this);" <c:if test="${empty cmd.msf_id_bef}">disabled="disabled"</c:if>>
																				<input type="button" value="Print" name="btnPrint" onclick="return tampilkan(this);">
																				<input type="button" value="Fitrah" id="fitrah" name="fitrah" onclick="return tampilkan(this);" <c:if test="${empty cmd.msf_id_bef}">disabled="disabled"</c:if>>
																				<c:choose>
																					<c:when test="${not empty infoMessage }"><div id="success" style="text-transform: none;">${infoMessage}</div></c:when>
																					<c:otherwise>
																						<spring:hasBindErrors name="cmd">
																							<div id="error" style="text-transform: none;">
																								<form:errors path="*"/>
																							</div>
																						</spring:hasBindErrors>
																					</c:otherwise>
																				</c:choose>
																			</td>
																		</tr>
																	</c:when>
																</c:choose>
															</table>
														</fieldset>									
													</td>
												</tr>																			
											</table>
										</td>
									</tr>
								</table>
							</form:form>
						</div>
						<div id="pane2" class="panes">
							<fieldset>
								<legend>History Permintaan SPAJ</legend>
								<display:table id="detail" name="cmd.daftarFormHistory" class="displaytag">
									<display:column property="msf_urut" title="No." style="text-align: left;"/>
									<display:column property="msf_id" title="No. Form" style="text-align: center;" />
									<display:column property="status_form" title="Posisi" style="text-align: center;" />
									<display:column property="lus_login_name" title="User" style="text-align: center;" />
									<display:column property="msfh_dt" title="Tanggal" format="{0, date, dd/MM/yyyy (hh:mm)}" style="text-align: center;" />
									<display:column property="msfh_desc" title="Keterangan" style="text-align: center;" />
								</display:table>
							</fieldset>
						</div>
						<div id="pane3" class="panes">
							<jsp:include flush="true" page="legend.jsp" />
						</div>
					</div>
					
				</div>
			</c:otherwise>
		</c:choose>

	</body>
	<script>
		<c:if test="${not empty param.sukses}">
			alert('${param.sukses}');
		</c:if>
	</script>
</html>