<%@ include file="/include/page/header_jquery.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		
// 		user memilih cabang, maka tampilkan wakil dan region nya
// 		$("#cabang").change(function() {
// 			$("#userinput").empty();
// 			var url = "${path}/report/bas.htm?window=summary_input_agen&json=1&cabang=" +$("#cabang").val();
// 			$.getJSON(url, function(result) {
// 				$("<option/>").val("ALL").html("ALL").appendTo("#userinput");
// 				$.each(result, function() {
// 					$("<option/>").val(this.key).html(this.value).appendTo("#userinput");
// 				});
// 				$("#userinput option:first").attr('selected','selected');
// 			});
// 			kopi untuk disubmit (isinya, bukan valuenya)
// 			$("#cabang2").val($("#cabang option:selected").text());
// 			$("#userinput2").val("ALL");
	
// 		});
		
// 		$("#userinput").change(function() {
// 			//kopi untuk disubmit (isinya, bukan valuenya)
// 			$("#userinput2").val($("#userinput option:selected").text());
// 		});		
		
	});
	
	
	
	function printPdf(){
// 	var url = 'http://${pageContext.request.serverName}:8080${pageContext.request.contextPath}/bas/kplanjutan.htm?window=doc_kuitansi\&kode='+document.getElementById('kode').value;
// 	alert(url);
// 		alert("http://${pageContext.request.serverName}${pageContext.request.contextPath}/bas/kplanjutan.htm?window=printSilent&urlnya="+url);
// 		document.getElementById("infoFrame").src="http://${pageContext.request.serverName}:8080${pageContext.request.contextPath}/bas/kplanjutan.htm?window=printSilent&urlnya="+url;
		if(confirm("Proses Print Akan segera berjalan, Pastikan Printer sudah dalam keadaan nyala dan kertas sudah tersedia.Apabila sudah SIAP, silakan klik tombol YES!")){
			window.location='${path }/bas/kplanjutan.htm?window=main&kode='+document.getElementById('kode').value+'&printKuitansi=true';
			
// 			document.getElementById('infoFrame').src='${path }/bas/kplanjutan.htm?window=prosesSilentPrintKuitansi&kode='+document.getElementById('kode').value;
		}
	}
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 12.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 24em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

</style>

<body>

<form id="formPost" name="formPost" method="post" target="_blank">
<div>
<fieldset class="ui-widget ui-widget-content">
	<legend class="ui-widget-header ui-corner-all"><div>Kuitansi Premi Lanjutan</div></legend>
	<br>
	<table width="100%">
		<tr>
			<th  class="plain" width="30%">Silakan Pilih Polis/ SPAJ untuk proses Print Kuitansi Kemudian Klik Print.</th>
		</tr>
	</table>
	<br>
	<c:if test="${not empty lsError or not empty submitSuccess}">
		<table class="ui-state-default"  width="40%" style="text-align: center;margin: auto;">
			<tr>
				 <td colspan="4" >
				 		<c:if test="${not empty submitSuccess }">
				        	<div id="success">
				        		<c:out  value="${submitSuccess}" escapeXml="false" />
					        	<script type="text/javascript">
									alert('${submitSuccess}');
								</script>
				        	</div>
				        </c:if>	
						<c:if test="${not empty lsError}">
							<div id="error" >
								Informasi:<br>
												- <c:out  value="${lsError}" escapeXml="false" />
										<br/>
							</div>
						</c:if>									
				</td>
			</tr>
		</table>
	</c:if>
	<hr>
	<table class="jtable" width="50%">
		<tr>
			<th width="30%">No. Polis || No. SPAJ</th>
			<td></td>
		</tr>
		<tr>
			<th valign="top">
				<select name="kode" id="kode"  size="30" title="Silahkan pilih No Polis/SPAJ">
					<option>--- Silahkan cari terlebih dahulu ---</option>
					<c:forEach var="s" items="${listSpaj}">
					<option value="${s.KODE }" 
						<c:if test="${s.KODE eq param.spaj }">selected<c:set var="ada" value="ada" /></c:if>>${s.TAMPIL}
					</option>
				</c:forEach>
				</select>
			</th>
			<td style="border:0px;">
				<input type="button" name="printKuitansi" id="printKuitansi" value="Print" onclick="printPdf();">
			</td>
		</tr>
		<tr>
			<th valign="top"></th>
			<td style="border:0px;">
			<iframe name="infoFrame" id="infoFrame" style="display: none;"
				width="100%"> Please Wait... </iframe>
			</td>
		</tr>
	</table>
</fieldset>
</div>
</form>
<script type="text/javascript">
	var success = '${submitSuccess}';
	if(success != ''){
		document.getElementById('infoFrame').src='${path }/bas/kplanjutan.htm?window=prosesSilentPrintKuitansi&kode=${kode_success}';
	}
</script>
</body>
</html>