<%@ include file="/include/page/taglibs.jsp"%><c:set var="path" value="${pageContext.request.contextPath}" />
<fieldset>
	<legend>Legend</legend>
	<table class="entry2">
		<tr>
			<th>Flow Permintaan Cabang</th>
			<td>
										
				<c:forEach var="warna" items="${cmd.daftarWarna}" varStatus="st">
					<c:if test="${st.count ne 1}">
						&nbsp;
						<img src="${path}/include/image/arrow.gif">
						&nbsp;
					</c:if>
					<input type="button" value="${warna.key}" style="background-color: ${warna.value}; ">
				</c:forEach>
										
			</td>
		</tr>
		<tr>
			<th>Flow Permintaan Agen</th>
			<td>
	
				<c:forEach var="warna" items="${cmd.daftarWarnaAgen}" varStatus="st">
					<c:if test="${st.count ne 1}">
					&nbsp;
					<img src="${path}/include/image/arrow.gif">
					&nbsp;
					</c:if>
					<input type="button" value="${warna.key}" style="background-color: ${warna.value}; ">
				</c:forEach>
				
			</td>
		</tr>
		<tr>
			<th>Jenis Blanko SPAJ</th>
			<td>
				<display:table id="detail" name="cmd.daftarJenisSpaj" class="displaytag" style="width: auto;">
					<display:column property="LSJS_PREFIX" title="Kode" style="text-align: center;" />
					<display:column property="LSJS_DESC" title="Jenis" style="text-align: left;" />
				</display:table>
			</td>
		</tr>
	</table>
</fieldset>