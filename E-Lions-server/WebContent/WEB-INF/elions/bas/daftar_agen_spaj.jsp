<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script>
			function tampilkan(msf_id, msag_id, msab_id, msab_nama, lca_id, lwk_id, lsrg_id, index){	
				if(parent) {
					if(!lwk_id) lwk_id='';
					if(!lsrg_id) lsrg_id='';
					parent.document.getElementById('msf_id').value = msf_id;
					parent.document.getElementById('msag_id').value = msag_id;
					parent.document.getElementById('msab_id').value = msab_id;
					parent.document.getElementById('msab_nama').value = msab_nama;
					parent.document.getElementById('lca_id').value = lca_id;
					parent.document.getElementById('lwk_id').value = lwk_id;
					parent.document.getElementById('lsrg_id').value = lsrg_id;
					parent.document.getElementById('index').value = index;
					parent.document.getElementById('submitMode').value = 'show';
					parent.document.getElementById('formpost').submit();
				}
			}
			
		</script>
	</head>

	<body style="height: 100%;">
		<div class="tabcontent" style="border: none;">

		<c:choose>
			<c:when test="${not empty cmd.pesan}">
				<div class="tabcontent" style="border: none;">
					<div id="success" style="text-transform: none">
						${cmd.pesan}
					</div>
				</div>
			</c:when>
			<c:otherwise>

				<table class="displaytag" style="width: 90%;">
					<thead>
						<th>Region</th>
						<th>Kode Agen</th>
						<% /*
						<th>Kode Agen Cabang</th>
						 */ %>
						<th>Nama Agen</th>
						<th>Nomor Form</th>
						<th>Posisi Form</th>
					</thead>
					<input type="hidden" name="msf_id" id="msf_id" value="${cmd.msf_id}">
					
					<tbody>
						<c:forEach items="${cmd.daftarAgen}" var="f" varStatus="st">
							<tr id="daftarAgen${st.index}" style='cursor: pointer;'
								onclick="Javascript:this.bgColor='#FFFFCC';tampilkan('${f.msf_id}', '${f.agen.msag_id}', '${f.agen.msab_id}', '${f.agen.msab_nama}', '${f.agen.lca_id}', '${f.agen.lwk_id}', '${f.agen.lsrg_id}', '${st.index}');"
							>
								<td style="text-align: center; white-space: nowrap">${f.agen.lsrg_nama}</td>
								<td style="text-align: center; white-space: nowrap">${f.agen.msag_id}</td>
								<% /*
								<td style="text-align: center; white-space: nowrap">${f.agen.msab_id}</td>
								*/ %>
								<td style="text-align: left; white-space: nowrap">${f.agen.msab_nama}</td>
								<td style="text-align: center; white-space: nowrap">${f.msf_id}</td>
								<td style="text-align: center; white-space: nowrap">${f.status_form}</td>
							</tr>
							<input type="hidden" value="${f.msf_id}" id="tampung${st.index}" name="tampung${st.index}">
							<input type="hidden" id="index" name="index" value="${st.index}">
						</c:forEach>
					</tbody>
				</table>
			</c:otherwise>
		</c:choose>
		</div>
	</body>
	
<c:if test="${cmd.msf_id ne ''}">
	<script type="text/javascript">
		var a = ${cmd.index};
		var el = document.getElementById('daftarAgen'+a);
		 if(el.currentStyle) { 
		  // for msie 
		  el.style.backgroundColor = '#FFFFCC'; 
		 } else { 
		  // for real browsers ;) 
		  el.style.setProperty("background-color", "#FFFFCC"); 
		 } 
	</script>
</c:if>

</html>