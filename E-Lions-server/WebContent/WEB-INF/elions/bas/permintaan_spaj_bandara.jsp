<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-blue.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript">
			function tampilkan(btn) {
				if(btn.name=='print') {
					var msf_id = document.getElementById('no_permintaan').value;
					//alert(msf_id);
					popWin('${path}/report/bas.pdf?window=perm_bandara&msf_id='+msf_id, 600, 800);
					return false;
				}			
			}
			hideLoadingMessage();
		</script>		
	</head>
	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">  
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Permintaan Spaj Bandara</a>
					<a href="#" onClick="return showPane('pane2', this)" id="tab2">Lap. Blanko Gagal</a>
				</li>
			</ul>
			<c:if test="${not empty mssg}">
				<script type="text/javascript">
					alert("${mssg}");
				</script>
			</c:if>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form id="formpost" name="formpost" method="post">
						<table class="entry2" width="100%">
							<tr>
								<th width="100">No. Permintaan</th>
								<td>
									<select id="no_permintaan" name="no_permintaan">
										<option value="">Pilih No Permintaan</option>
										<c:forEach items="${lsPermintaan}" var="x">
											<option value="${x.key}" <c:if test="${x.key eq ID}">selected="selected"</c:if>>${x.value}</option>
										</c:forEach>
									</select>							
								</td>
							</tr>
							<tr>
								<th>&nbsp;</th>
								<td>
									<input type="submit" name="view" value="view">
									<input type="submit" name="new" value="new">								
								</td>
							</tr>
							<c:if test="${not empty show}">
								<tr>
									<th>&nbsp;</th>
									<td>
										<fieldset>
											<legend>Permintaan Blanko</legend>
											<table class="entry2">
												<tr>
													<th width="130">Tanggal Permintaan</th>
													<td><script>inputDate('tglMinta', '${TGLMINTA}', false, '', 9);</script></td>
												</tr>
												<tr>	
													<th>Nama Pemohon</th>
													<td><input type="text" name="nama" size="40" value="${NAMA}"></td>
												</tr>
												<tr>
													<th width="97px">Jenis</th>
													<td>
														<select id="jnsTravIns" name="jnsTravIns">
															<option value="">Pilih jenis</option>
															<c:forEach items="${typeTravelIns}" var="x">
																<option value="${x.key}" <c:if test="${x.key eq JENIS}">selected="selected"</c:if>>${x.value}</option>
															</c:forEach>
														</select>
													</td>
												</tr>
												<tr>	
													<th>Juml. Blanko</th>
													<td><input type="text" name="jumlBlanko" size="5" value="${JUMLH}" <c:if test="${not empty ID}">readonly="readonly"  class="readOnly"</c:if>></td>
												</tr>
												<tr>	
													<th>No. Blanko</th>
													<td><input type="text" name="noBlanko" size="100" value="${NOBLANKO}" <c:if test="${not empty ID}">readonly="readonly"  class="readOnly"</c:if>></td>
												</tr>
												<tr>	
													<th>&nbsp;</th>
													<td>
														<input type="submit" name="save" value="Simpan">
														<input type="button" id="print" name="print" value="Print" onclick="return tampilkan(this);" <c:if test="${empty view}">disabled</c:if>>
													</td>
												</tr>
												<tr>
													<td colspan="2">
													<c:choose>
	                        							<c:when test="${not empty errors}">
															<div id="error" style="text-transform: none;">ERROR:<br>
																<c:forEach var="err" items="${errors}">
																	- <c:out value="${err}" escapeXml="false" />
																	<br />
																	<script>
																		pesan += '${err}\n';
																	</script>
																</c:forEach>
															</div>
														</c:when>
														<c:otherwise>
															<span class="info">*Semua kolom harus diisi, Tidak boleh kosong</span>
														</c:otherwise>
													</c:choose>		
													</td>
												</tr>
											</table>
										</fieldset>
									</td>
								</tr>
							</c:if>
						</table>
					</form>
				</div>
				<div id="pane2" class="panes">
					<form id="formpost" name="formpost" method="post">
						<table class="entry2" width="100%" border="0">
							<tr>
								<th width="100">Jenis</th>
								<td>
									<select id="jnsTravIns2" name="jnsTravIns2">
										<option value="">Pilih jenis</option>
										<c:forEach items="${typeTravelIns}" var="x">
											<option value="${x.key}" <c:if test="${x.key eq JENIS}">selected="selected"</c:if>>${x.value}</option>
										</c:forEach>
									</select>
								</td>
							</tr>
							<tr>
								<th width="100">No. Blanko</th>
								<td>
									<select id="no" name="no">
										<option value="">Pilih No Blanko</option>
										<c:forEach items="${lsBlanko}" var="x">
											<option value="${x.key}" <c:if test="${x.key eq ID}">selected="selected"</c:if>>${x.value}</option>
										</c:forEach>
									</select>								
								</td>
							</tr>
							<tr>
								<th width="100">Status</th>
								<td>
									<select id="stat" name="stat">
										<option value="">Pilih Status</option>
										<option value="4">Rusak</option>
										<option value="5">Hilang</option>
										<option value="6">Salah</option>
									</select>
								</td>
							</tr>
							<tr>	
								<th width="100">&nbsp;</th>
								<td>
									<input type="submit" name="save2" value="Simpan">
								</td>
							</tr>
							<tr>
								<th width="100">&nbsp;</th>
								<td><span class="info">*Semua kolom harus diisi, Tidak boleh kosong</span></td>
							</tr>
						</table>
					</form>
				</div>		
			</div>				
		</div>	
	</body>
</html>

