<%@ include file="/include/page/header_jquery.jsp"%>
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (jQueryUI Tabs) init tab2 Utama
		$("#tabs").tabs();

		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title], label[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		
		// button icons
		//$( "#btnShow" ).button({icons: {primary: "ui-icon-search"}});
		$( "#btnSimpan" ).button({icons: {primary: "ui-icon-note"}});
		//$( "#btnHistory" ).button({icons: {primary: "ui-icon-script"}});		
		
		var lspd_id = '${lspd_id}';
		if(lspd_id=='93'){
			$("#btnSimpan").button("option","disabled",false);
		}else{
			$("#btnSimpan").button("option","disabled",true);
		}

		//bila ada pesan
	    //var pesan = '${command.pesan}';
	    var pesan = '${pesan}';
		if(pesan != '') alert(pesan);
		
		//bila mengganti nama kota
		$("#kota").change(function(){
			$("#re_tgl_kirim_hadiah_jam").empty();
			var url = "${path}/bas/hadiah.htm?window=json&t=wKota&kota=" + $("#kota").val();
			$.getJSON(url, function(result) {
				$.each(result, function() {
					$("<option/>").val(this.value).html(this.label).appendTo("#re_tgl_kirim_hadiah_jam");
				});
				$("#re_tgl_kirim_hadiah_jam option:first").attr('selected','selected');
			}); 
		});
		
	});
	
	function ValidateForm(){
			
		var spaj = document.getElementById('hadiah_spaj'); 
		
		if((spaj.value==null)||(spaj.value=="")){
			alert("Masukkan no SPAJ dengan benar!");
			spaj.focus();
			return false;
		}
		
		return true;
	 }	
	
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 23em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 23.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 36em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

	/* untuk tombol2 kecil di report */
	ul#icons {margin: 0; padding: 0;}
	ul#icons li {margin: 0 1px 0 0; position: relative; padding: 0px; cursor: pointer; float: left;  list-style: none;}
	ul#icons span.ui-icon {float: left; margin: 0px;}

</style>

<body>
	
	<!-- Tabs -->
	<form method="post" name="formpost" id="formpost" commandName="command" action="${path}/bas/hadiah.htm?window=reschedule" onsubmit="return ValidateForm()" >
	<!-- <form method="post" name="formpost" id="formpost" commandName="command">-->
	<div id="tabs">
		<ul>
			<li><a href="#tab-1">Reschedule Hadiah</a></li>
		</ul>
		
		<div id="tab-1">
			<fieldset class="ui-widget ui-widget-content">
				<legend class="ui-widget-header ui-corner-all"><div>Reschedule Hadiah</div></legend>
				
				<%-- <div class="rowElem">
					<label title="Tanggal Kirim Hadiah">Tanggal Kirim Hadiah :</label>
					<input name="tgl_kirim_hadiah" id="tgl_kirim_hadiah" type="text" class="datepicker" title="Tanggal Kirim" value="${begdate}">
				</div> --%>
				
				<c:if test="${(spaj eq \"\") or (spaj eq null)}">
				<div class="rowElem">
					<label title="SPAJ">SPAJ</label>
					<input name="hadiah_spaj" id="hadiah_spaj" type="text" title="SPAJ" value="">
					<input type="submit" value="Cari">
				</div>
				</c:if>
				
				<div class="rowElem">
					<label title="Reschedule tanggal kirim">Reschedule Tanggal Kirim Hadiah :</label>
					<input name="re_tgl_kirim_hadiah" id="re_tgl_kirim_hadiah" type="text" class="datepicker" title="Tanggal Kirim" value="${tgl }">
					<%-- &nbsp;Jam&nbsp;<input size="3" name="re_tgl_kirim_hadiah_jam" id="re_tgl_kirim_hadiah_jam" type="text" title="Jam Kirim" value="${jam }">
					&nbsp;Menit&nbsp;<input size="3" name="re_tgl_kirim_hadiah_mnt" id="re_tgl_kirim_hadiah_mnt" type="text" title="Menit Kirim" value="${mnt }">
					&nbsp;<em>Tanggal Pengiriman +3 hari kerja</em> --%>
					&nbsp;Jam&nbsp;
						<select name="re_tgl_kirim_hadiah_jam" id="re_tgl_kirim_hadiah_jam" title="Jam Kirim">
							<c:forEach items="${jam }" var="j">
							<option value="${j.value }" <c:if test="${j.value eq jams }">selected="selected"</c:if>>${j.label }</option>
							</c:forEach>
						</select>
					&nbsp;Menit&nbsp;
						<select name="re_tgl_kirim_hadiah_mnt" id="re_tgl_kirim_hadiah_mnt" title="Menit Kirim">
							<c:forEach items="${menit }" var="m">
							<option value="${m.value }" <c:if test="${m.value eq mnts }">selected="selected"</c:if>>${m.label }</option>
							</c:forEach>
						</select>
					&nbsp;<em>Tanggal Pengiriman +3 hari kerja</em>
				</div>
				<div class="rowElem">
					<label title="Alamat">Alamat :</label>
					<input size="50" name="alamat" id="alamat" type="text" title="Alamat Kirim" value="${almt_kirim }">
				</div>
				<div class="rowElem">
					<label title="Kota">Kota :</label>
					<input size="20" name="kota" id="kota" type="text" title="Kota Kirim" value="${kota_kirim }">
				</div>
				<div class="rowElem">
					<label title="KodePos">Kode Pos :</label>
					<input size="6" name="kodepos" id="kodepos" type="text" title="Kodepos Kirim" value="${kdpos_kirim }">
				</div>
				<div class="rowElem">
					<label title="Keterangan">Keterangan :</label>
					<input size="50" name="keterangan" id="keterangan" type="text" title="Keterangan" value="${ket }">
				</div>
				<div class="rowElem">
					<label>&nbsp;</label>
					<input type="hidden" name="hadiah_spaj" id="hadiah_spaj" value="${spaj }">
					<input type="hidden" name="lspd_id" id="lspd_id" value="${lspd_id }">
					<input type="hidden" name="tgl_sebelum" id="tgl_sebelum" value="${tgl_sebelum }">
					<input type="submit" name ="btnSimpan" id="btnSimpan" title="Simpan" value="Simpan">
					<input type="button" name="btnClose" id="btnClose" value="Close" onClick="window.close();return false;">
					<!-- <input type="reset" name ="btnReset" id="btnReset" title="Reset" value="Reset"> -->
				</div>
				
			</fieldset>
			
		</div>
	</div>
	</form>
</body>
</html>