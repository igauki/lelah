<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript" src="${path}/include/js/jquery-1.3.2.min.js"></script>
        <script>
            function backToParent(id){
                window.opener.open('${path}/bas/updatebridge.htm?msag_id='+id,'_self');
                window.close(); 
            }
        </script>
    </head>
    <body onload="document.formpost.nama.focus();" style="height: 100%;">
        <form method="post" name="formpost">
            <fieldset>
                <table class="entry2" width="100%">
                    <tr>
                        <th>
                            <select id="search_data" name="search_data" title="search data">
                                <c:forEach items="${lsSearch}" var="x">
                                    <option value="${x.value}">${x.key}</option>
                                </c:forEach>
                            </select>
                        </th>
                        <th width="330px">  
                            <input type="text" name="searchInput" style="width: 100%">
                        </th>
                        <th>    
                            <input type="submit" class="button" value="Cari" name="btnCari">
                            <input type="button" class="button" value="Tutup" onclick="window.close()">
                        </th>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <c:if test="${not empty lsBridge}">
                                <table class="displaytag" id="daftar" name="daftar" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>ID Agen</th>
                                            <th>Nama Agen</th>
                                            <th>Tgl Lahir</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="x" items="${lsBridge}" varStatus="xt">
                                        <tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;" onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
                                            onclick="backToParent('${x.MSAG_ID}');">
                                            <td align="center">${x.MSAG_ID}</td>
                                            <td align="center">${x.MCL_FIRST}</td>
                                            <td align="center">${x.BIRTH_DATE}</td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>    
                            </c:if>
                            <div class="info">
                                * Klik untuk mencari data agen <br/>
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </body>
<%@ include file="/include/page/footer.jsp"%>