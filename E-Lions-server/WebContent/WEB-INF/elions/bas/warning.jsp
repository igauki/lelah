<%@ include file="/include/page/taglibs.jsp"%><c:set var="path" value="${pageContext.request.contextPath}" />
<div class="tab-container" id="container1">
	<ul class="tabs">
		<li>
			<a href="#" onClick="return showPane('pane1', this)" id="tab1">Daftar Form Agen yang sudah lewat 30 hari</a>
		</li>
	</ul>
	
	<div class="tab-panes">
		<div id="pane1" class="panes">
			<table class="displaytag" style="width: 90%;">
				<thead>
					<th>No. Form</th>
					<th>Tanggal</th>
					<th>Agen</th>
					<th>Hari</th>
				</thead>
				<tbody>
					<c:forEach items="${cmd.daftarFormOverdue}" var="f">
						<tr>
							<td style="text-align: center; white-space: nowrap">${f.msf_id}</td>
							<td style="text-align: center; white-space: nowrap"><fmt:formatDate value="${f.msfh_dt}" pattern="dd/MM/yyyy"/></td>
							<td style="text-align: center; white-space: nowrap">${f.agen.msab_nama}</td>
							<td style="text-align: center; white-space: nowrap">${f.selisih}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			
			<input value="Kontrol SPAJ" type="button" onclick="window.location='${path}/bas/kontrol_agen.htm?nowarning=true';">
			<input value="${param.link1}" type="button" onclick="window.location='${param.link2}';">
			
		</div>
	</div>
</div>