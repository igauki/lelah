<%@ include file="/include/page/header_jquery.jsp"%>
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<script type="text/javascript" src="${path}/include/js/default.js"></script>

<script type="text/javascript">
	hideLoadingMessage();
	
	//Perintah untuk menjalankan jQuery setelah seluruh dokumen selesai di load.
	$().ready(function(){
	
		// (jQueryUI Tabs) init tab2 Utama
		$("#tabs").tabs();

		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title], label[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
	
		$("#btnCari").click(function(){
			var spaj = $("#spajNo").val();
			if(spaj == null || spaj == ""){
				alert("Harap masukkan No SPAJ terlebih dahulu.");
				return false;
			}else{
				return true;
			}
		});
		
		//Bila tidak ada data maka non aktifkan tombol Submit
		if($("#policyNo").val() != '') {
			$('input[name="kliAtasNama"]').attr('enabled','enabled');
			$('input[name="kliNoRek"]').attr('enabled','enabled');
			$('select[name="kliNamaBank"]').attr('enabled','enabled');
			$('input[name="kliCabangBank"]').attr('enabled','enabled');
			$('input[name="kliKotaBank"]').attr('enabled','enabled');
			$('select[name="lkuId"]').attr('enabled','enabled');
			$('input[name="premi_ke"]').attr('enabled','enabled');
			$('input[name="premirefundtmp"]').attr('enabled','enabled');
			$('select[name="tindakan"]').attr('enabled','enabled');
			$('textarea[name="keterangan"]').attr('enabled','enabled');
			$('input[name="file1"]').attr('enabled','enabled');
			$('input[name="file2"]').attr('enabled','enabled');
			$('input[name="file3"]').attr('enabled','enabled');
			$('input[name="btnSubmit"]').attr('enabled','enabled');
		}else{
			$('input[name="kliAtasNama"]').attr('disabled','disabled');
			$('input[name="kliNoRek"]').attr('disabled','disabled');
			$('select[name="kliNamaBank"]').attr('disabled','disabled');
			$('input[name="kliCabangBank"]').attr('disabled','disabled');
			$('input[name="kliKotaBank"]').attr('disabled','disabled');
			$('select[name="lkuId"]').attr('disabled','disabled');
			$('input[name="premi_ke"]').attr('disabled','disabled');
			$('input[name="premirefundtmp"]').attr('disabled','disabled');
			$('select[name="tindakan"]').attr('disabled','disabled');
			$('textarea[name="keterangan"]').attr('disabled','disabled');
			$('input[name="file1"]').attr('disabled','disabled');
			$('input[name="file2"]').attr('disabled','disabled');
			$('input[name="file3"]').attr('disabled','disabled');
			$('input[name="btnSubmit"]').attr('disabled','disabled');
		}
		
	});
	
	function isNumberKey(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode;
		if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	}
</script>

	<style type="text/css">
		/* fieldset */
		fieldset { margin-bottom: 1em; padding: 0.5em; }
		fieldset legend { width: 99%; }
		fieldset legend div { margin: 0.3em 0.5em; }
		fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
		fieldset .rowElem label { margin-right: 0.4em; width: 12em; display: inline-block; }
		fieldset .rowElem .jtable { position: relative; left: 12.5em; }
	
		/* tanda bintang (*) untuk menandakan required field */
		em { color: red; font-weight: bold; }
	
		/* agar semua datepicker align center, dan ukurannya fix */
		.datepicker { text-align: center; width: 7em; }
	
		/* styling untuk client-side validation error message */
		#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }
	
		/* styling untuk label khusus checkbox dan radio didalam rowElement */
		fieldset .rowElem label.radioLabel { width: auto; }
		
		/* styling untuk client-side validation error message */
		#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }
		
		/* styling untuk server-side validation error */
		.errorField { border: 1px solid red; }
		.errorMessage { color: red; display: block;}
		
		/* lebar untuk form elements */
		.lebar { width: 7em; }
		
		/* untuk align center */
		.tengah { text-align: center; }
	</style>

	<body>
		<form:form method="post" name="formpost" id="formpost" enctype="multipart/form-data" commandName="cmd">
			<fieldset class="ui-widget ui-widget-content">
				<legend class="ui-widget-header ui-corner-all"><div>Permintaan Refund Polis Successive</div></legend>
				<br><br>
				<table id="tab1">
					<tr>
						<th align="left">No. SPAJ</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="No SPAJ" path="spajNo" cssErrorClass="errorField"/>
						</td>
						<td>
							<form:errors path="spajNo" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th></th>
						<th></th>
						<td>
							<input type="submit" name="btnCari" id="btnCari" title="Cari" value="Cari"/>
						</td>
					</tr>
					<tr>
						<th align="left">No. Polis</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="No Polis" path="policyNo" cssErrorClass="errorField" readonly="true"/>
						</td>
						<td>
							<form:errors path="policyNo" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th align="left">Pemegang Polis</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="Pemegang Polis" path="namaPp" size="55" cssErrorClass="errorField" readonly="true"/>
						</td>
						<td>
							<form:errors path="namaPp" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th align="left">Atas Nama Rekening</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="Atas Nama Rekening" path="kliAtasNama" size="55" cssErrorClass="errorField"/>
						</td>
						<td>
							<form:errors path="kliAtasNama" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th align="left">No. Rekening</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="No. Rekening" path="kliNoRek" cssErrorClass="errorField"/>
						</td>
						<td>
							<form:errors path="kliNoRek" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th align="left">Nama Bank</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:select path="kliNamaBank" cssErrorClass="errorField">
								<form:option value="" label="-- Pilih Bank --"/>
								<form:options items="${listBankPusat}" itemValue="key" itemLabel="value"/>
							</form:select>
						</td>
						<td>
							<form:errors path="kliNamaBank" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th align="left">Cabang Bank</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="Cabang Bank" path="kliCabangBank" size="55" cssErrorClass="errorField"/>
						</td>
						<td>
							<form:errors path="kliCabangBank" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th align="left">Kota Bank</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="Kota Bank" path="kliKotaBank" size="55" cssErrorClass="errorField"/>
						</td>
						<td>
							<form:errors path="kliKotaBank" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">Kurs</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:select path="lkuId" cssErrorClass="errorField">
								<form:option value="" label="-- Pilih Kurs --"/>
								<form:options items="${listKurs}" itemValue="key" itemLabel="value"/>
							</form:select>
						</td>
						<td>
							<form:errors path="lkuId" cssClass="errorMessage"/>
						</td>
					<tr>
					<tr>
						<th align="left">Premi Ke</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="Premi Ke" path="premi_ke" cssErrorClass="errorField"/>
						</td>
						<td>
							<form:errors path="premi_ke" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">Jumlah</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:input title="Jumlah Premi" path="premirefundtmp" maxlength="15" cssErrorClass="errorField" onkeypress="return isNumberKey(event)"/>
						</td>
						<td>
							<form:errors path="premirefundtmp" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">Tindakan</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:select path="tindakan" cssErrorClass="errorField">
								<form:option value="" label="-- Pilih Tindakan --"/>
								<form:option value="1" label="Double Bayar"/>
								<form:option value="2" label="Putus Kontrak"/>
								<form:option value="3" label="Lebih Bayar"/>
								<form:option value="4" label="Cuti Premi"/>
								<form:option value="5" label="Lain-lain"/>
							</form:select>
						</td>
						<td>
							<form:errors path="tindakan" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">Keterangan</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<form:textarea title="Keterangan" path="keterangan" rows="3" cols="54" cssErrorClass="errorField"/>
						</td>
						<td>
							<form:errors path="keterangan" cssClass="errorMessage"/>
						</td>
					</tr>
					<tr>
						<th style="width: 200px;" align="left">Upload Document</th>
						<th>:&nbsp;&nbsp;&nbsp;</th>
						<td>
							<input type="file" name="file1" id="file1" title="Browse" size="52"/>
						</td>
					</tr>
<!-- 					<tr> -->
<!-- 						<th style="width: 200px;" align="left">Upload BSB</th> -->
<!-- 						<th>:&nbsp;&nbsp;&nbsp;</th> -->
<!-- 						<td> -->
<!-- 							<input type="file" name="file1" id="file1" title="Browse" size="52"/> -->
<!-- 						</td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<th style="width: 200px;" align="left">Upload Surat Pernyataan Refund</th> -->
<!-- 						<th>:&nbsp;&nbsp;&nbsp;</th> -->
<!-- 						<td> -->
<!-- 							<input type="file" name="file2" id="file2" title="Browse" size="52"/> -->
<!-- 						</td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<th style="width: 200px;" align="left">Upload Cover Buku Tabungan</th> -->
<!-- 						<th>:&nbsp;&nbsp;&nbsp;</th> -->
<!-- 						<td> -->
<!-- 							<input type="file" name="file3" id="file3" title="Browse" size="52"/> -->
<!-- 						</td> -->
<!-- 					</tr> -->
					<tr>
						<th></th>
						<th></th>
						<td>
							<input type="submit" name="btnSubmit" id="btnSubmit" title="Submit" value="Submit"/>
						</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>
							<em><form:errors path="pesan" cssClass="errorMessage"/></em>
						</td>
					</tr>
				</table>
			</fieldset>
		</form:form>
	</body>
</html>
