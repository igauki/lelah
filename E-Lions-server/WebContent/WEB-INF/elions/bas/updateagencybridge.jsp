<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript" src="${path}/include/js/jquery-1.3.2.min.js"></script>
<script>
    $(document).ready(function() {
        <c:if test="${not empty param.pesan}">
            alert('${param.pesan}');
        </c:if>
        <c:if test="${cmd.mia.status eq 0}">
            $('#searchSpan').animate({ opacity: 'hide' }, 'fast');
        </c:if>
        
        $("#region").change(function(e) {
            var data = this.value.toString().split(".");
            $('#lca_id').val(data[0]);
            $('#lwk_id').val(data[1]);
            $('#lsrg_id').val(data[2]);
           
        });
        
        $('.noBorder').click(function(e) {
            //e.preventDefault();
            if(this.id == "statusLihat") {
                $('#searchSpan').animate({ opacity: 'show' }, 'medium');
            }
        }); 
        
        $('#search').click(function (e) {
            popWin('${path}/bas/spaj.htm?&window=cari_bridge', 480, 640);
        });     
    });
</script>
    <body>
        <form:form id="formpost" name="formpost" commandName="cmd" method="post">
            <table width="70%" class="entry" border="0">
                <tr>
                    <th width="200px">Agen Bridge Agency</th>
                   <td> <label for="statusLihat"><radiobutton  value="1" id="statusLihat" cssClass="noBorder"/></label>
                        <span id="searchSpan">
                            <input type="button" class="button" id="search" value="Search Agen Bridge" style="width:150px;">
                        </span></td>
                </tr>
                <tr>
                    <th>&nbsp;</th>
                    <td>
                        <fieldset>
                            <legend>Data Agen</legend>
                            <table width="10%" class="entry2" border="0">
                                <tr>
                                    <th width="150px">No Sertifikat</th>
                                    <td>:&nbsp;${cmd.mapBridge.MSAG_SERTIFIKAT_NO}</td>
                                </tr>
                                <tr>
                                    <th>Region</th>
                                    <td>:&nbsp;${cmd.mapBridge.LSRG_NAMA}</td>
                                </tr>
                                <tr>
                                    <th>Kode Agen</th>
                                    <td>:&nbsp;${cmd.mapBridge.MSAG_ID}</td>
                                </tr>
                                <tr>
                                    <th>Nama</th>
                                    <td>:&nbsp;${cmd.mapBridge.MCL_FIRST}</td>
                                </tr>
                                <tr>
                                    <th>Alamat</th>
                                    <td>:&nbsp;${cmd.mapBridge.ALAMAT_RUMAH}</td>
                                </tr>
                                <tr>
                                    <th>Tempat Lahir</th>
                                    <td>:&nbsp;${cmd.mapBridge.MSPE_PLACE_BIRTH}</td>
                                </tr>
                                <tr>
                                    <th>Tgl. Lahir</th>
                                    <td>:&nbsp;${cmd.mapBridge.TANGGAL_LAHIR}</td>
                                </tr>
                                <tr>
                                    <th>Tgl Aktif</th>
                                    <td>:&nbsp;${cmd.mapBridge.MSAG_ACTIVE_DATE}</td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>:&nbsp;${cmd.mapBridge.STATUS}</td>
                                </tr>
                                <tr>
                                    <th>Awal Kontrak</th>
                                    <td>:&nbsp;${cmd.mapBridge.MSAG_BEG_DATE}</td>
                                </tr>
                                <tr>
                                    <th>Akhir Kontrak</th>
                                    <td>:&nbsp;${cmd.mapBridge.MSAG_END_DATE}</td>
                                </tr>
                            </table>
                        </fieldset>
                        <fieldset>
                        <legend>Proses Update</legend>
                        <table width="20%" class="entry2" border="0">
                        <c:if test ="${not empty lsBridge}">
                            <tr>
                               <th>Daftar Cabang Bridge</th>
                                 <td>
                                    <div id="nama">
                                  <select name="lsBridge">
                                   <c:forEach items="${lsBridge}" var="c">
                                     <option 
                                     <c:if test="${cmd.mapBridge.LSRG_ID eq c.LSRG_ID}"> SELECTED </c:if>
                                     value="${c.LSRG_ID}">${c.LSRG_NAMA}</option>
                                </c:forEach>
                            </select>
                          </div>
                       </td>
                    </tr>
                  </c:if>
                  <tr>
                <th>
                    <input type="submit" class="button" id="bridgeSimpan" value="Simpan" style="width:100px;">
                    <input type="hidden" name="submitMode" id="submitMode">
               </th>
               <td>&nbsp;<td>
            </tr>
                  </table>
                </fieldset>
             </tr>
            </table>
        </form:form>
    </body>
<%@ include file="/include/page/footer.jsp"%>
