<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<script>
			function tampilkan(btn){
				if(btn.name=='save') {
					if(!confirm('Simpan permintaan kartu nama?')) {
						return false;
					}
				} else if(btn.name=='cancel') {
					if(!confirm('Batalkan Permintaan?')) {
						return false;
					}
				} else if(btn.name=='received') {
					if(!confirm('Tandai permintaan sebagai RECEIVED?\nJumlah Brosur akan bertambah secara otomatis di sistem. Lanjutkan?')) {
						return false;
					}
				} else if(btn.name=='show') {
					if(document.getElementById('mkn_id').value=='') {
						alert('Silahkan pilih salah satu nomor terlebih dahulu');				
						return false;
					}
				}
					
				document.getElementById('submitMode').value = btn.name;
				btn.disabled=true;
				document.getElementById('formpost').submit();
				
			}
			
			hideLoadingMessage();
		</script>
	</head>

	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">
			<div class="tab-container" id="container1">
				<ul class="tabs">
					<li>
						<a href="#" onClick="return showPane('pane1', this)" id="tab1">Form Permintaan Kartu Nama</a>
						<a href="#" onClick="return showPane('pane2', this)" id="tab2">History Posisi</a>
					</li>
				</ul>
		
				<div class="tab-panes">
					<div id="pane1" class="panes">
						<form:form commandName="cmd" id="formpost" method="post" enctype="multipart/form-data">
							<form:hidden id="submitMode" path="submitMode"/>
							<form:hidden id="mode" path="mode"/>
							<input type="hidden" name="posisi" id="posisi" value="${cmd.mode }"/>
							<table class="entry2">
								<tr>
									<th>
										<input type="button" id="show" name="show" value="Show" style="width: 55px;" onclick="return tampilkan(this);">
										<c:if test="${cmd.mode eq 0 }">
											<input type="button" id="new" name="new" value="New" style="width: 55px;" onclick="return tampilkan(this);">
										</c:if>
									</th>
									<td style="vertical-align: top;" rowspan="2">
										<fieldset>
											<c:if test="${cmd.mode eq 0 }">
												<legend>Form Permintaan Kartu Nama</legend>
												<table class="entry2" border="0">
													<tr>
														<th width="150">Jenis</th>
														<td width="150"><select name="mkn_type" id="mkn_type">
																<option value="1">Konvensional</option>
																<option value="2">Syariah</option>
															</select></td>
														<td>&nbsp;</td>
													</tr>
													<tr>
														<th width="150">Upload Daftar Agent</th>
														<td width="150"><input type="file" name="file1" size="70"></td>
														<td style="text-align: left;"><input type="button" value="Upload" name="upload_agent" onclick="return tampilkan(this);" />
															<em> *file xls</em></td>
													</tr>
													<tr>
														<th width="150">Upload Bukti</th>
														<td width="150"><input type="file" name="file2" size="70"></td>
														<td style="text-align: left;"><em> *file pdf</em></td>
													</tr>
													<tr>
														<th width="150">&nbsp;</th>
														<td width="150"><input type="button" value="Simpan" name="save" onclick="return tampilkan(this);" /></td>
														<td style="text-align: left;">&nbsp;</td>
													</tr>
												</table>
												
												<c:if test="${not empty sessionScope.resultDaftarAgent}">
												<table class="entry2">
													<tr>
														<th>No</th>
														<th>Kode Agent</th>
														<th>Nama Agent</th>
														<th>No Telp</th>
													</tr>
													<c:forEach var="da" items="${sessionScope.resultDaftarAgent}" varStatus="st">
														<tr>
															<td nowrap="nowrap" align="center">${st.index +1}</td>
															<td nowrap="nowrap" align="center">${da.MSAG_ID}</td>
															<td nowrap="nowrap" align="center">${da.NAMA}</td>
															<td nowrap="nowrap" align="center">${da.TELP}</td>
											         </c:forEach>
												</table>
												</c:if>
											</c:if>
											
											<!-- view permintaan -->
											<c:if test="${not empty cmd.daftarKartuNama }">
												<br>
												<strong>Document :</strong> <a href="#" onclick="getPdf();">${cmd.daftarKartuNama[0].mkn_document }</a>
												<table class="entry2">
													<tr>
														<th>No</th>
														<th>Kode Agent</th>
														<th>Nama Agent</th>
														<th>Tgl Create</th>
														<th>Jenis Kartu Nama</th>
														<th>User Input</th>
														<th>Approve</th>
														<th>Posisi</th>
														<th>Total Permintaan (1 thn)</th>
													</tr>
													<c:forEach items="${cmd.daftarKartuNama }" var="d" varStatus="s">
														<tr>
															<td>${s.index +1 }</td>
															<td>${d.msag_id }</td>
															<td>${d.mcl_first }</td>
															<td><fmt:formatDate value="${d.mkn_create_date }" pattern="dd/MM/yyyy"/></td>
															<td><c:choose>
																	<c:when test="${d.mkn_type eq 1 }">
																		Konvensional
																	</c:when>
																	<c:otherwise>
																		Syariah
																	</c:otherwise>
																</c:choose></td>
															<td>${d.user_input }</td>
															<td>${d.approve }</td>
															<td>${d.position }</td>
															<td>${d.total_permintaan }</td>
														</tr>
													</c:forEach>
												</table>
											</c:if>
											
											
											<c:if test="${cmd.mode eq 2 }">
												<br>
												<legend>Approval Permintaan Kartu Nama [Distribution Support]</legend>
												<table class="entry2">
													<tr>
														<th>Keterangan</th>
														<td><input type="text" name="keterangan" id="keterangan" size="70" /></td>
													</tr>
													<tr>
														<td></td>
														<td><input type="button" value="Approve" name="approve_ds" onclick="return tampilkan(this);" />
															<input type="button" value="Reject" name="reject" onclick="return tampilkan(this);" />
														</td>
													</tr>
												</table>
											</c:if>
											
											<c:if test="${cmd.mode eq 1 }">
												<br>
												<legend>Approval Permintaan Kartu Nama [Bussiness Development]</legend>
												<table class="entry2">
													<tr>
														<th>Keterangan</th>
														<td><input type="text" name="keterangan" id="keterangan" size="70" /></td>
													</tr>
													<tr>
														<td></td>
														<td><input type="button" value="Approve" name="approve_bd" onclick="return tampilkan(this);" />
															<input type="button" value="Reject" name="reject" onclick="return tampilkan(this);" /></td>
													</tr>
												</table>
											</c:if>
											
											<c:if test="${cmd.mode eq 3 }">
												<br>
												<legend>Permintaan Kartu Nama [Purchasing]</legend>
												<table class="entry2">
													<tr>
														<th>Keterangan</th>
														<td><input type="text" name="keterangan" id="keterangan" size="70" /></td>
													</tr>
													<tr>
														<td></td>
														<td><input type="button" value="Send" name="send_purchasing" onclick="return tampilkan(this);" /></td>
													</tr>
												</table>
											</c:if>
											
											<c:if test="${cmd.mode eq 4 }">
												<br>
												<legend>Pengiriman Permintaan Kartu Nama [GA]</legend>
												<table class="entry2">
													<tr>
														<th>Keterangan</th>
														<td><input type="text" name="keterangan" id="keterangan" size="70" /></td>
													</tr>
													<tr>
														<td></td>
														<td><input type="button" value="Send" name="send_ga" onclick="return tampilkan(this);" /></td>
													</tr>
												</table>
											</c:if>
											
											<c:choose>
												<c:when test="${not empty infoMessage }"><div id="success" style="text-transform: none;">${infoMessage}</div></c:when>
												<c:otherwise>
													<spring:hasBindErrors name="cmd">
														<div id="error" style="text-transform: none;">
															<form:errors path="*"/>
														</div>
													</spring:hasBindErrors>
												</c:otherwise>
											</c:choose>
											
										</fieldset>									
									</td>										
								</tr>
								<tr>
									<th style="width: 120px;">
										<select id="mkn_id" name="mkn_id" size="${sessionScope.currentUser.comboBoxSize}
												" style="width: 110px;" onclick="document.getElementById('show').click();">
											<c:forEach var="d" items="${cmd.daftarNoKartuNama}">
												<option  value="${d.mkn_id}" <c:if test="${d.mkn_position eq 5 }"> style="background-color: red;" </c:if> <c:if test="${d.mkn_id eq cmd.mkn_id}"> selected </c:if>
												>${d.mkn_id}</option>
											</c:forEach>
										</select>
									</th>
								</tr>
							</table>
						</form:form>
					</div>
					
					<div id="pane2" class="panes">
						<fieldset>
							<legend>History Permintaan Kartu Nama</legend>
							<c:if test="${not empty cmd.historyKartuNama }">
								<table class="entry2">
									<tr>
										<th>No</th>
										<th>ID</th>
										<th>Posisi</th>
										<th>User</th>
										<th>Tanggal</th>
										<th>Keterangan</th>
									</tr>
									<c:forEach items="${cmd.historyKartuNama }" var="d" varStatus="s">
										<tr>
											<td>${s.index +1 }</td>
											<td>${d.mkn_id }</td>
											<td>${d.position }</td>
											<td>${d.user_input }</td>
											<td><fmt:formatDate value="${d.mkn_create_date }" pattern="dd/MM/yyyy"/></td>
											<td>${d.keterangan }</td>
										</tr>
									</c:forEach>
								</table>
							</c:if>
						</fieldset>
					</div>
				</div>
				
			</div>
	</body>
	<script>
		<c:if test="${not empty param.sukses}">
			alert('${param.sukses}');
		</c:if>
		
		function getPdf(){
			popWin('${path}/common/util.htm?window=download&file=${cmd.daftarKartuNama[0].mkn_document }&dir=\\\\ebserver\\pdfind\\form_kartu_nama\\', 600, 800);
		}
	</script>
</html>