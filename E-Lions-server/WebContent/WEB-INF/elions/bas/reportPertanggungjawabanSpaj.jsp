<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-blue.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<script type="text/javascript">
			function cari(){
				lcaId = document.getElementById('lca_id').value;
				msf_id=formpost.msf_id.value;
				prefix = document.getElementById('lsJenis').value;
				
				if(lcaId == '0' && prefix != 'F') 
					alert("mode ALL hanya untuk report fitrah > 21 hari"); 
				else 
					popWin('${path}/report/bas.htm?window=report_pertanggungjawabanSpaj&msf_id='+msf_id+'&prefix='+prefix+'&flag='+lcaId,600,800);
			}
		</script>
	</head>
	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Pertanggungjawaban
						SPAJ agent</a>
				</li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form id="formpost" method="post">
						<table class="entry2">
							<tr>
								<th width="10%">
									Nama Admin
								</th>
								<td colspan="2" width="55%">
									<input type="hidden" name="pil">
									<select name="lus_id" disabled>
										<option value="0">
											---All---
										</option>
										<c:forEach items="${lsAdmin}" var="x">
											<option value="${x.KEY}"
												<c:if test="${x.KEY eq lus_id}">selected</c:if>>
												${x.VALUE}
											</option>
										</c:forEach>
									</select>
								</td>
								<td align="left" rowSpan="4">
									&nbsp;
								</td>
							</tr>
							<tr>
								<th>
									Nama Cabang
								</th>
								<td>
									<select name="lca_id" id="lca_id" onChange="javascript:formpost.submit();"
										<c:if test="${lock eq 1 }">disabled</c:if>>
										<option value="0">
											All
										</option>
										<c:forEach items="${lsCabang}" var="x" varStatus="xt">
											<option value="${x.KEY}"
												<c:if test="${x.KEY eq lca_id}">selected</c:if>>
												${x.VALUE}
											</option>
										</c:forEach>
									</select>
								</td>
								<td >
									&nbsp;
								</td>
							</tr>
							<tr>
								<th>
									Nomor Form
								</th>
								<td>
									<select id="msf_id" name="msf_id">
										<c:forEach var="d" items="${daftarNomorFormSpaj}">
											<option value="${d.msf_id}"
												<c:if test="${d.msf_id eq cmd.msf_id}">selected</c:if>>
												${d.msf_id}
											</option>
										</c:forEach>
									</select>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<th>
									Jenis SPAJ
								</th>
								<td>
									<select id="lsJenis" name="lsJenis" onchange="showJenis(this.value)">
										<c:forEach var="d" items="${lsJenis}">
											<option value="${d.LSJS_PREFIX}">
												${d.LSJS_DESC}
											</option>
										</c:forEach>
									</select>
								</td>
								<td>
									<!--<span id="tipeFitrah" style="font-weight: bold;font-size: 9px;visibility: hidden;">
										<label for='all'><input type='radio' name='jenisFitrah' id='all' value='0' checked="checked"> All</label> 
										&nbsp;&nbsp;
										<label for='21Hari'><input type='radio' name='jenisFitrah' id='21Hari' value='1'> &gt; 21 Hari</label>
									</span>-->
								</td>								
							</tr>
							<tr>
								<th></th>
								<td>
									<input type="button" name="btnCari" value="Cari"
										onClick="cari();">
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>
