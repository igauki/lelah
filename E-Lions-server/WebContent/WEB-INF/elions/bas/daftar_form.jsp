<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script>
			function tampilkan(msf_id, index){	
				if(parent) {
					parent.document.getElementById('msf_id').value = msf_id;
					if(parent.document.getElementById('submitMode'))
						parent.document.getElementById('submitMode').value = 'show';
					parent.document.getElementById('lca_id').value='${cmd.lca_id}';
					parent.document.getElementById('posisi').value='${cmd.posisi}';
					parent.document.getElementById('index').value = index;
					parent.document.getElementById('formpost').submit();
				}
			}
			
		</script>
	</head>

	<body style="height: 100%;">

		<c:choose>
			<c:when test="${empty cmd.daftarForm}">
				<div class="tabcontent" style="border: none;">
					<div id="success" style="text-transform: none">
						${cmd.pesan}
					</div>
				</div>
			</c:when>
			<c:otherwise>
				<table class="displaytag">
					<thead>
						<th>Cabang</th>
						<th>Nomor</th>
						<th>Tanggal Input</th>
						<th>Status</th>
					</thead>
					<tbody>
						<c:forEach items="${cmd.daftarForm}" var="f" varStatus="st">
							<tr id="daftarForm${st.index}" style="cursor: pointer; background-color: ${f.warna};" onclick="tampilkan('${f.msf_id}', '${st.index}');">
								<td style="text-align: center; white-space: nowrap">${f.lca_nama}</td>
								<td style="text-align: center; white-space: nowrap">${f.msf_id}</td>
								<td style="text-align: center; white-space: nowrap"><fmt:formatDate value="${f.msfh_dt}" pattern="dd/MM/yyyy"/></td>
								<td style="text-align: center; white-space: nowrap">${f.status_form}</td>
							</tr>
							<input type="hidden" id="index" name="index" value="${st.index}">
						</c:forEach>
						<input type="hidden" name="lca_id" value="${cmd.lca_id}">
						<input type="hidden" name="msf_id" id="msf_id" value="${cmd.msf_id}">	
					</tbody>
				</table>
			</c:otherwise>
		</c:choose>

	</body>

<c:if test="${cmd.msf_id ne ''}">
	<script type="text/javascript">
		var a = ${cmd.index};
		var el = document.getElementById('daftarForm'+a);
		 if(el.currentStyle) { 
		  // for msie 
		  el.style.backgroundColor = '#FFFFCC'; 
		 } else { 
		  // for real browsers ;) 
		  el.style.setProperty("background-color", "#FFFFCC"); 
		 } 
	</script>
</c:if>
	
</html>