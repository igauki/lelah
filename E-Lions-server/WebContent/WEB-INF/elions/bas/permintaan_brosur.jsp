<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<script>
			/*
			popWin('${path}/bas/spaj.htm?window=form_brosur', 350, 450);
			showFrame('${path}/report/bas.htm?window=form_brosur');
			function showFrame(x){
				document.getElementById('infoFrame').src=x;
			}
			*/
			
			function tampilkan(btn){
				if(btn.name=='save') {
					if(!confirm('Simpan Perubahan?')) {
						return false;
					}
				} else if(btn.name=='cancel') {
					if(!confirm('Batalkan Permintaan?')) {
						return false;
					}
				} else if(btn.name=='received') {
					if(!confirm('Tandai permintaan sebagai RECEIVED?\nJumlah Marketing Tools akan bertambah secara otomatis di sistem. Lanjutkan?')) {
						return false;
					}
				} else if(btn.name=='show') {
					if(document.getElementById('msf_id').value=='') {
						alert('Silahkan pilih salah satu nomor terlebih dahulu');				
						return false;
					}
				} else if(btn.name=='fitrah') {
					var msf_id = document.getElementById('msf_id').value;
					popWin('${path}/report/bas.pdf?window=fitrah_card&msf_id='+msf_id, 600, 800);
					return false;
				} else if(btn.name=='pas') {
					var msf_id = document.getElementById('msf_id').value;
					popWin('${path}/report/bas.pdf?window=pas&jenis=admin&msf_id='+msf_id, 600, 800);
					return false;
				}
					
				document.getElementById('submitMode').value = btn.name;
				btn.disabled=true;
				document.getElementById('formpost').submit();
			}
			
			hideLoadingMessage();
		</script>
	</head>

	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">
		<%-- <c:choose>
			<c:when test="${not empty cmd.daftarFormOverdue}">
				<jsp:include flush="true" page="warning.jsp">
					<jsp:param value="Lanjut ke Halaman Form Marketing Tools" name="link1"/>
					<jsp:param value="${path}/bas/form_brosur.htm?nowarning=true" name="link2"/>
				</jsp:include>
			</c:when>
			<c:otherwise> --%>
				<div class="tab-container" id="container1">
					<ul class="tabs">
						<li>
							<a href="#" onClick="return showPane('pane1', this)" id="tab1">Form Permintaan Martketing Tools</a>
							<a href="#" onClick="return showPane('pane2', this)" id="tab2">History Posisi</a>
							<a href="#" onClick="return showPane('pane3', this)" id="tab3">Legend</a>
						</li>
					</ul>
			
					<div class="tab-panes">
						<div id="pane1" class="panes">
							<form:form commandName="cmd" id="formpost" method="post">
								<form:hidden id="submitMode" path="submitMode"/>
								<table class="entry2">
									<tr>
										<th>
											<input type="button" id="show" name="show" value="Show" style="width: 55px;" onclick="return tampilkan(this);">
											<input type="button" id="new" name="new" value="New" style="width: 55px;" onclick="return tampilkan(this);">
										</th>
										<td style="vertical-align: top;" rowspan="2">
											<fieldset>
												<legend>Form Permintaan Marketing Tools ke Kantor Pusat</legend>
												<c:choose>
													<c:when test="${not empty cmd.daftarFormBrosur}">
														<table class="entry2" border="0">
															<tr>
																<th>Stok Marketing Tools</th>
																<td colspan="3">
																	<table class="displaytag" style="width:auto;">
																		<thead>
																			<tr>
																				<c:forEach var="s" items="${cmd.daftarStokBrosur}">
																					<th style="width: 50px; " title="${s.lsjs_desc}">${s.lsjs_prefix}</th>
																				</c:forEach>
																			</tr>
																		</thead>
																		<tbody>
																			<tr>
																				<c:forEach var="s" items="${cmd.daftarStokBrosur}">
																					<td title="${s.lsjs_desc}">${s.mss_amount}</td>
																				</c:forEach>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
															<tr>
																<th>Jenis</th>
																<td colspan="3">
																	<form:select path="jn_brosur">
																		<form:option value="1" label="Brosur"/>
																		<form:option value="2" label="Poster"/>
																		<form:option value="3" label="Banner"/>
																		<form:option value="4" label="Flayer"/>
																	</form:select>
																	<input type="button" value="Ganti" name="ganti" onclick="return tampilkan(this);">
																</td>
															</tr>
															<tr>
																<th>Nomor Form</th>
																<td colspan="3">
																	<form:hidden path="daftarFormBrosur[0].status_permintaan" />
																	<form:input id="msf_id" path="daftarFormBrosur[0].msf_id" cssClass="readOnly" readonly="true" size="20"/>
																</td>
															</tr>
															<tr>
																<th>Status</th>
																<td colspan="3">
																	<input type="text" value="${cmd.daftarFormBrosur[0].status_form}" readonly="true" size="20" style="background-color: ${cmd.daftarFormBrosur[0].warna};">
																</td>
															</tr>
															<tr>
																<th>Cabang</th>
																<td width="224px">
																	<input type="text" class="readOnly" readonly="true" size="40" value="${cmd.daftarFormBrosur[0].lca_nama}"/>
																</td>
																<c:if test="${sessionScope.currentUser.lus_id eq \'721\'}">
																	<th width="97px">Cabang BMI</th>
																	<td>
																		<select id="bmi_id" name="bmi_id">
																			<option value="">Pilih Cabang BMI</option>
																			<c:forEach items="${cmd.daftarCabBmi}" var="x">
																				<option value="${x.key}" <c:if test="${x.key eq cmd.daftarFormBrosur[0].bmi_id}">selected="selected"</c:if>>${x.value}</option>
																			</c:forEach>
																		</select>
																	</td>
																</c:if>
															</tr>
															<tr>
																<th>Pemohon</th>
																<td>
																	<input type="text" class="readOnly" readonly="true" size="40" value="${cmd.formHistUser.lus_login_name}"/>
																</td>
																<c:if test="${cmd.admTravIns gt 0}">
																	<th width="97px">Travel Ins</th>
																	<td>
																		<select id=admTravIns name="admTravIns">
																			<option value="">Pilih jenis</option>
																			<c:forEach items="${cmd.typeTravelIns}" var="x">
																				<option value="${x.key}" <c:if test="${x.key eq cmd.daftarFormBrosur[0].trav_ins_type}">selected="selected"</c:if>>${x.value}</option>
																			</c:forEach>
																		</select>
																	</td>
																</c:if>
															</tr>
															<tr>
																<th>BusDev</th>
																<td colspan="3">
																	<%-- <input type="text" value="${cmd.daftarFormBrosur[0].status_form}" readonly="true" size="20" style="background-color: ${cmd.daftarFormBrosur[0].warna};"> --%>
																	<form:select path="busdev">
																		<form:option value="1" label="Agency"/>
																		<!-- <form:option value="2" label="Corporate"/>
																		<form:option value="3" label="Bancass 1 (Team Ibu Yanti S) Bancass Support"/>
																		<form:option value="4" label="Bancass 2 & Bancass 1 BusDev"/> -->
																	</form:select>
																	<!-- <select name="busdev" id="busdev">
																		<option value="">--Pilih--</option>
																		<option value="1">Agency</option>
																		<option value="2">Regional</option>
																	</select> -->
																</td>
															</tr>
															<tr>
																<th>Detail Permintaan</th>
																<td colspan="3">
																	<table class="displaytag" style="width:auto;">
																		<thead>
																			<tr>
																				<th rowspan="2">No.</th>
																				<th rowspan="2">Jenis Marketing Tools</th>
																				<th rowspan="2">Prefix</th>
																				<th colspan="2">Jumlah Marketing Tools</th>
																				<!-- <th rowspan="2">No. Blanko</th> -->
																				<th rowspan="2">Status</th>
																			</tr>
																			<tr>
																				<th>Diminta</th>
																				<th>Disetujui</th>
																			</tr>
																		</thead>
																		<tbody>
																		<c:forEach var="daftar" items="${cmd.daftarFormBrosur}" varStatus="st">
																			<tr>
																				<td style="text-align: left;">${st.count}.</td>
																				<td style="text-align: left;">${daftar.lsjs_desc}</td>
																				<td style="text-align: center;">${daftar.lsjs_prefix}</td>
																				<td style="text-align: center;">
																					<form:input path="daftarFormBrosur[${st.index}].msf_amount_req" size="10" cssStyle="text-align: right;" onfocus="this.select();"/>
																					<span class="error">*</span>
																				</td>
																				<td style="text-align: center;">
																					<form:input path="daftarFormBrosur[${st.index}].msf_amount" size="10" cssStyle="text-align: right;" cssClass="readOnly" readonly="true"/>
																				</td>
																				<%-- <td>
																					<form:input path="daftarFormBrosur[${st.index}].no_blanko_req" size="50" cssClass="readOnly" readonly="true" />
																				</td> --%>	
																				<td style="text-align: center;">${daftar.status_form}</td>
																			</tr>
																		</c:forEach>
				
																		<tr>
																			<td colspan="3" style="text-align: center;">Total Marketing Tools: </td>
																			<td style="text-align: center;">
																				?
																			</td>
																		</tr>
																		
																		</tbody>
																	</table>
																</td>
															</tr>
															<tr>
																<th></th>
																<td colspan="3">
																	<input type="button" value="Simpan" name="save" onclick="return tampilkan(this);"
																		<c:if test="${cmd.daftarFormBrosur[0].posisi ne 0}">disabled</c:if>>
																	<input type="button" value="Batalkan" name="cancel" onclick="return tampilkan(this);"
																		<c:if test="${cmd.daftarFormBrosur[0].posisi ne 0 and cmd.daftarFormBrosur[0].posisi ne 1}">disabled</c:if>>
																	
																	<input type="button" value="Telah Diterima" name="received" onclick="return tampilkan(this);"
																		<c:if test="${cmd.daftarFormBrosur[0].posisi ne 4}">disabled</c:if>>
																	
																	<%-- <input type="button" value="Fitrah" id="fitrah" name="fitrah" onclick="return tampilkan(this);"
																		<c:if test="${cmd.button eq 1}">disabled</c:if>>
																	<input type="button" value="PAS" id="pas" name="pas" onclick="return tampilkan(this);" <c:if test="${cmd.button eq 1}">disabled</c:if>> --%>
																	<c:choose>
																		<c:when test="${not empty infoMessage }"><div id="success" style="text-transform: none;">${infoMessage}</div></c:when>
																		<c:otherwise>
																			<spring:hasBindErrors name="cmd">
																				<div id="error" style="text-transform: none;">
																					<form:errors path="*"/>
																				</div>
																			</spring:hasBindErrors>
																		</c:otherwise>
																	</c:choose>
																</td>
															</tr>
														</table>											
													</c:when>
													<c:otherwise>
														<div id="success">
															Silahkan pilih tombol SHOW atau NEW di sebelah kiri
														</div>
													</c:otherwise>
												</c:choose>
												
											</fieldset>									
										</td>										
									</tr>
									<tr>
										<th style="width: 120px;">
											<select id="msf_id" name="msf_id" size="${sessionScope.currentUser.comboBoxSize}
													" style="width: 110px;" onclick="document.getElementById('show').click();">
												<c:forEach var="d" items="${cmd.daftarNomorFormBrosur}">
													<option value="${d.msf_id}" style="background-color: ${d.warna};"
														<c:if test="${d.msf_id eq cmd.msf_id}"> selected </c:if>
													>${d.msf_id}</option>
												</c:forEach>
											</select>
										</th>
									</tr>
								</table>
							</form:form>
						</div>
						<div id="pane2" class="panes">
							<fieldset>
								<legend>History Permintaan Marketing Tools</legend>
								<display:table id="detail" name="cmd.daftarFormHistory" class="displaytag">
									<display:column property="msf_urut" title="No." style="text-align: left;"/>
									<display:column property="msf_id" title="No. Form" style="text-align: center;" />
									<display:column property="status_form" title="Posisi" style="text-align: center;" />
									<display:column property="lus_login_name" title="User" style="text-align: center;" />
									<display:column property="msfh_dt" title="Tanggal" format="{0, date, dd/MM/yyyy (hh:mm)}" style="text-align: center;" />
									<display:column property="msfh_desc" title="Keterangan" style="text-align: center;" />
								</display:table>
							</fieldset>
						</div>
						<div id="pane3" class="panes">
							<jsp:include flush="true" page="legend_brosur.jsp" />
						</div>
					</div>
					
				</div>
			<%-- </c:otherwise>
		</c:choose> --%>

	</body>
	<script>
		<c:if test="${not empty param.sukses}">
			alert('${param.sukses}');
		</c:if>
	</script>
</html>