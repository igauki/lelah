<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<script>
			/*
			popWin('${path}/bas/spaj.htm?window=form_spaj', 350, 450);
			showFrame('${path}/report/bas.htm?window=form_spaj');
			function showFrame(x){
				document.getElementById('infoFrame').src=x;
			}
			*/
			
			function tampilkan(btn){
				//alert(""+document.getElementById('msf_id'));
				if(btn.name=='show') {
					var lca_lwk_lsrg = document.getElementById('lca_lwk_lsrg').value;
					if(lca_lwk_lsrg=='') {
						alert('Silahkan pilih salah satu region terlebih dahulu');				
					}else{
						if(lca_lwk_lsrg=='ALL_AGEN') if(!confirm('Anda yakin menampilkan seluruh agen?')) return false;
						document.getElementById('formFrame').src = '${path}/bas/spaj.htm?window=daftarAgenSpaj&lca_lwk_lsrg=' + lca_lwk_lsrg;
					}
					return false;
					
				} else if(btn.name=='detail_blanko') {
					popWin('${path}/bas/spaj.htm?window=daftarBlanko', 200, 350);
					return false;
				} else if(btn.name=='form_tgjwb') {
					popWin('${path}/bas/kontrol_tgjwb.htm?msf_id=${cmd.msf_id}&msab_id=${cmd.agen.msab_id}', 500, 700);
					return false;
				} else if(btn.name=='pas') {
					var msf_id = document.getElementById('msf_id').value;
					popWin('${path}/report/bas.pdf?window=pas&jenis=agen&msf_id='+msf_id, 600, 800);
					return false;
				}
				
			}
			
			hideLoadingMessage();
		</script>
	</head>

	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

		<c:choose>
			<c:when test="${not empty cmd.daftarFormOverdue}">
				<jsp:include flush="true" page="warning.jsp">
					<jsp:param value="Lanjut ke Halaman Kontrol Agen" name="link1"/>
					<jsp:param value="${path}/bas/kontrol_agen.htm?nowarning=true" name="link2"/>
				</jsp:include>
			</c:when>
			<c:otherwise>
				<div class="tab-container" id="container1">
					<ul class="tabs">
						<li>
							<a href="#" onClick="return showPane('pane1', this)" id="tab1">Kontrol Agen</a>
							<a href="#" onClick="return showPane('pane2', this)" id="tab2">History Posisi</a>
							<a href="#" onClick="return showPane('pane3', this)" id="tab3">Legend</a>
						</li>
					</ul>
					
					<div class="tab-panes">
						<div id="pane1" class="panes">
							<form:form commandName="cmd" id="formpost" method="post">
								<form:hidden id="submitMode" path="submitMode"/>
								<form:hidden id="msf_id" path="msf_id"/>
								<form:hidden id="index" path="index"/>
								<table class="entry2">
									<tr>
										<th style="width: 185px; vertical-align: top;" rowspan="3">
											<form:select path="lca_lwk_lsrg" items="${cmd.daftarRegion}" itemLabel="lsrg_nama" itemValue="lca_lwk_lsrg" 
												size="${sessionScope.currentUser.comboBoxSize}" cssStyle="width: 180px; overflow: scroll;" 
												onclick="document.getElementById('show').click();" />
		
											<input type="button" id="show" name="show" value="Show" style="width: 150px;" onclick="return tampilkan(this);">
										</th>
										<td style="vertical-align: top;">
											<table class="entry2">
												<tr>
													<td style="vertical-align: top;" colspan="2">
														<fieldset>
															<legend>Daftar Agen yang belum Mempertanggungjawabkan Form SPAJ</legend>
															<iframe id="formFrame" src="${path}/bas/spaj.htm?window=daftarAgenSpaj&lca_lwk_lsrg=${cmd.lca_lwk_lsrg}&msf_id=${cmd.msf_id}&index=${cmd.index}"></iframe>
														</fieldset>
													</td>
												</tr>
												<tr>
													<td style="vertical-align: top;" colspan="2">
														<fieldset>
															<legend>Detail Form Agen</legend>
															<table class="entry2">
																<tr>
																	<th>Stok SPAJ</th>
																	<td>
																		<table class="displaytag" style="width:auto;">
																			<caption>Cabang</caption>
																			<thead>
																				<tr>
																					<c:forEach var="s" items="${cmd.daftarStokSpaj}">
																						<th style="width: 50px; " title="${s.lsjs_desc}">${s.lsjs_prefix}</th>
																					</c:forEach>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<c:forEach var="s" items="${cmd.daftarStokSpaj}">
																						<td title="${s.lsjs_desc}">${s.mss_amount}</td>
																					</c:forEach>
																				</tr>
																			</tbody>
																		</table>
																		<input type="button" value="Detail" name="detail_blanko" onclick="return tampilkan(this);">
																		<table class="displaytag" style="width:auto;">
																			<caption>Agen</caption>
																			<thead>
																				<tr>
																					<c:forEach var="s" items="${cmd.daftarStokSpajAgen}">
																						<th style="width: 50px; " title="${s.lsjs_desc}">${s.lsjs_prefix}</th>
																					</c:forEach>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<c:forEach var="s" items="${cmd.daftarStokSpajAgen}">
																						<td title="${s.lsjs_desc}">${s.mss_amount}</td>
																					</c:forEach>
																				</tr>
																			</tbody>
																		</table>
																	</td>
																</tr>
																<tr>
																	<th>Agen</th>
																	<td>
																		<form:hidden id="msab_id" path="agen.msab_id"/>
																		<form:hidden id="lca_id" path="agen.lca_id"/>
																		<form:hidden id="lwk_id" path="agen.lwk_id"/>
																		<form:hidden id="lsrg_id" path="agen.lsrg_id"/>
																		<form:input id="msag_id" path="agen.msag_id" cssClass="readOnly" readonly="true" size="8"/>
																		<c:choose>
																			<c:when test="${cmd.agen.msag_id eq \"000000\" and empty cmd.agen.msab_id}">
																				<form:input id="msab_nama" path="agen.msab_nama" size="30"/>
																			</c:when>
																			<c:otherwise>
																				<form:input id="msab_nama" path="agen.msab_nama" cssClass="readOnly" readonly="true" size="30"/>
																			</c:otherwise>
																		</c:choose>
																	</td>
																</tr>
																<c:choose>
																	<c:when test="${not empty cmd.daftarFormSpaj}">
																		<% /*
																		<tr>
																			<th>Status Permintaan</th>
																			<td>
																				<form:select path="daftarFormSpaj[0].status_permintaan">
																					<form:option value="0" label="Permanen"/>
																					<form:option value="1" label="Titipan"/>
																				</form:select>
																			</td>
																		</tr>
																		*/ %>
																		<tr>
																			<th>Nomor Form</th>
																			<td>
																				<input type="text" id="msf_id" class="readOnly" readonly="true" size="20" value="${cmd.daftarFormSpaj[0].msf_id}"/>
																			</td>
																		</tr>
																		<tr>
																			<th>Cabang</th>
																			<td>
																				<input type="text" class="readOnly" readonly="true" size="40" value="${cmd.daftarFormSpaj[0].lca_nama}"/>
																			</td>
																		</tr>
																		<tr>
																			<th>User</th>
																			<td>
																				<input type="text" class="readOnly" readonly="true" size="40" value="${sessionScope.currentUser.name}"/>
																			</td>
																		</tr>
																		<tr>
																			<th>No Bukti Pembayaran</th>
																			<td>
																				<input type="text" class="readOnly" readonly="true" size="40" value="${cmd.daftarFormSpaj[0].bukti_pembayaran}"/>	
																			</td>
																		</tr>
																		<tr>
																			<th>Detail Permintaan</th>
																			<td>
																				<table class="displaytag" style="width:auto;">
																					<thead>
																						<tr>
																							<th>No.</th>
																							<th>Jenis SPAJ</th>
																							<th>Prefix</th>
																							<th>No. Blanko</th>
																							<th>Jumlah</th>
																						</tr>
																					</thead>
																					<tbody>
																					<c:forEach var="daftar" items="${cmd.daftarFormSpaj}" varStatus="st">
																						<tr>
																							<td style="text-align: left;">${st.count}.</td>
																							<td style="text-align: left;">${daftar.lsjs_desc}</td>
																							<td style="text-align: center;">${daftar.lsjs_prefix}</td>
																							<td style="text-align: center;">
																								<form:input readonly="true" path="daftarFormSpaj[${st.index}].no_blanko_req" size="50" />
																							</td>
																							<td style="text-align: center;">
																								<form:input path="daftarFormSpaj[${st.index}].msf_amount" size="4" cssStyle="text-align: right;" cssClass="readOnly"  readonly="true"/>
																							</td>
																						</tr>
																					</c:forEach>
							
																					<tr>
																						<td colspan="3" style="text-align: center;">Total SPAJ: </td>
																						<td style="text-align: center;">
																						</td>
																						<td>?</td>
																					</tr>
																					
																					</tbody>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<th></th>
																			<td>
																				<input type="button" value="Form Pertanggungjawaban" name="form_tgjwb" onclick="return tampilkan(this);">
																				<input type="button" value="PAS" id="pas" name="pas" onclick="return tampilkan(this);">
																			</td>
																		</tr>
																	</c:when>
																</c:choose>
															</table>
														</fieldset>									
													</td>
												</tr>
											</table>								
										</td>
									</tr>
								</table>
								
		
							</form:form>
						</div>
						<div id="pane2" class="panes">
							<fieldset>
								<legend>History Permintaan SPAJ</legend>
								<display:table id="detail" name="cmd.daftarFormHistory" class="displaytag">
									<display:column property="msf_urut" title="No." style="text-align: left;"/>
									<display:column property="msf_id" title="No. Form" style="text-align: center;" />
									<display:column property="status_form" title="Posisi" style="text-align: center;" />
									<display:column property="lus_login_name" title="User" style="text-align: center;" />
									<display:column property="msfh_dt" title="Tanggal" format="{0, date, dd/MM/yyyy (hh:mm)}" style="text-align: center;" />
									<display:column property="msfh_desc" title="Keterangan" style="text-align: center;" />
								</display:table>
							</fieldset>
						</div>
						<div id="pane3" class="panes">
							<jsp:include flush="true" page="legend.jsp" />
						</div>
					</div>
					
				</div>
			</c:otherwise>
		</c:choose>

	</body>
	<script>
		<c:if test="${not empty param.sukses}">
			alert('${param.sukses}');
		</c:if>
	</script>
</html>