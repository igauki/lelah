<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<script>
			function tampilkan(){
				formpost.submit();
			}
			function ganti(id,idx){
				s=formpost.status[idx].value
				window.location="${path}/bas/spaj.htm?window=agent_status&up=1&msag_id="+id+"&status="+s+'&lca_id=${cmd.lca_id}';
			}
			function awal(){
				if('${cmd.suc}'=='1')
					alert("Berhasil Update \nStatus Kode Agen ${cmd.agen.msag_id} telah di update.");
			}
			hideLoadingMessage();
		</script>
	</head>

	<body onload="setupPanes('container1', 'tab1'); awal();" style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Agent Status</a>
				</li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form:form commandName="cmd" id="formpost" method="post" action="${path}/bas/spaj.htm?window=agent_status">
						<form:hidden id="submitMode" path="submitMode"/>
						<table class="entry2">
							<tr>
								<th style="width: 150px;vertical-align: top;">
									<input type="hidden" name="posisi" >
									<select id="lca_id" name="lca_id" size="${sessionScope.currentUser.comboBoxSize}
											" style="width: 140px;" onclick="document.getElementById('show').click();">
										<c:forEach var="d" items="${cmd.daftarCabang}">
											<option value="${d.KEY}" 
												<c:if test="${d.KEY eq cmd.lca_id}"> selected </c:if>
											>${d.VALUE}</option>
										</c:forEach>
									</select>
									<input type="button" id="show" name="show" value="Show" style="width: 55px;" onclick="return tampilkan();">
								</th>
								<td style="vertical-align: top;">								
									<fieldset>
										<legend>Daftar Agen Cabang ${cmd.nama_cabang }</legend>
										<table class="entry2">
											<tr>	
												<th width="5%">Kode Agen</th>
												<th width="30%">Nama Agen</th>
												<th width="5%">Tingkat</th>
												<th width="20%">Cabang</th>
												<th width="5%">Status</th>
											</tr>
											<c:forEach items="${cmd.daftarAgen}" var="x" varStatus="xt">
												<tr>
													<td>${x.msag_id}</td>
													<td>${x.mcl_first}</td>
													<td>${x.tingkat}</td>
													<td>${x.lsrg_nama}</td>
													<td>
														<select name="status" id="status">
															<c:forEach items="${cmd.daftarStatus}" var="y">
																<option value="${y.KEY}" <c:if test="${y.KEY eq x.msag_sp}">selected</c:if>>${y.VALUE}</option>
															</c:forEach>
														</select>
													</td>
													<td>
														<input type="button" name="update" id="update" value="Update" onClick="ganti('${x.msag_id}','${xt.index}');"> 
													</td>
												</tr>
											</c:forEach>
										</table>
									</fieldset>
									
								</td>
							</tr>
						</table>
					</form:form>
				</div>
			</div>
		</div>
	</body>
	<script>
		<c:if test="${not empty param.sukses}">
			alert('${param.sukses}');
		</c:if>
	</script>
</html>