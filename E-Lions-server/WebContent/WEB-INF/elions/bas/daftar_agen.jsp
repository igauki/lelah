<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script>
			function tampilkan(msag_id, msab_id, msab_nama, lca_id, lwk_id, lsrg_id){	
				if(parent) {
					if(!lwk_id) lwk_id='';
					if(!lsrg_id) lsrg_id='';
					parent.document.getElementById('msag_id').value = msag_id;
					parent.document.getElementById('msab_id').value = msab_id;
					parent.document.getElementById('msab_nama').value = msab_nama;
					parent.document.getElementById('lca_id').value = lca_id;
					parent.document.getElementById('lwk_id').value = lwk_id;
					parent.document.getElementById('lsrg_id').value = lsrg_id;
					parent.document.getElementById('submitMode').value = 'show';
					parent.document.getElementById('formpost').submit();
				}
			}
		</script>
	</head>

	<body style="height: 100%;">
		<div class="tabcontent" style="border: none;">
		<%
		/*
		<input type="button" value="Agen Baru" onclick="tampilkan('000000', '', '', '${sessionScope.currentUser.lca_id}');">
		<br/>
		*/
		 %>
		
		<c:choose>
			<c:when test="${not empty cmd.pesan}">
				<div class="tabcontent" style="border: none;">
					<div id="success" style="text-transform: none">
						${cmd.pesan}
					</div>
				</div>
			</c:when>
			<c:otherwise>

				<%
				/*
				<table class="displaytag" style="width: 90%;">
					<caption>Agen Baru</caption>
					<thead>
						<th>Region</th>
						<th>Kode Agen Cabang</th>
						<th>Nama Agen</th>
					</thead>
					<tbody>
						<c:forEach items="${cmd.daftarAgenBaru}" var="f">
							<tr style='cursor: pointer;' onclick="tampilkan('${f.msag_id}', '${f.msab_id}', '${f.msab_nama}', '${f.lca_id}', '${f.lwk_id}', '${f.lsrg_id}');">
								<td style="text-align: center; white-space: nowrap">${f.lsrg_nama}</td>
								<td style="text-align: center; white-space: nowrap">${f.msab_id}</td>
								<td style="text-align: left; white-space: nowrap">${f.msab_nama}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				*/
				 %>

				<table class="displaytag" style="width: 90%;">
					<caption>Agen</caption>
					<thead>
						<th>Region</th>
						<th>Kode Agen</th>
						<%/*
						<th>Kode Agen Cabang</th>
						*/ %>
						<th>Nama Agen</th>
						<th>Tingkat</th>
					</thead>
					<tbody>
						<c:forEach items="${cmd.daftarAgen}" var="f">
							<tr style="cursor: pointer;" onclick="tampilkan('${f.msag_id}', '${f.msab_id}', '${f.msab_nama}', '${f.lca_id}', '${f.lwk_id}', '${f.lsrg_id}');">
								<td style="text-align: center; white-space: nowrap">${f.lsrg_nama}</td>
								<td style="text-align: center; white-space: nowrap">${f.msag_id}</td>
								<%/*
								<td style="text-align: center; white-space: nowrap">${f.msab_id}</td>
								*/ %>
								<td style="text-align: left; white-space: nowrap">${f.msab_nama}</td>
								<td style="text-align: center; white-space: nowrap">${f.tingkat}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</c:otherwise>
		</c:choose>
		</div>
	</body>
</html>