<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script>
			function tampilkan(lar_id, index){	
				if(parent) {
					parent.document.getElementById('lar_id').value = lar_id;
					if(parent.document.getElementById('submitMode'))
						parent.document.getElementById('submitMode').value = 'show';
					parent.document.getElementById('index').value = index;
					parent.document.getElementById('kota').value='${cmd.kota}';
					parent.document.getElementById('formpost').submit();
				}
			}
			
		</script>
	</head>

	<body style="height: 100%;">

		<c:choose>
			<c:when test="${empty cmd.daftarKantor}">
				<div class="tabcontent" style="border: none;">
					<div id="success" style="text-transform: none">
						${cmd.pesan}
					</div>
				</div>
			</c:when>
			<c:otherwise>
				<table class="displaytag">
					<thead>
						<th>Admin</th>
						<th>Alamat</th>
						<th>Telpon</th>
						<th>E-mail</th>
					</thead>
					<tbody>
						<c:forEach items="${cmd.daftarKantor}" var="f" varStatus="st">
							<tr id="daftarKantor${st.index}" style="cursor: pointer; background-color: #6CF;" onclick="tampilkan('${f.LAR_ID}', '${st.index}');">
								<td style="text-align: left; white-space: nowrap">${f.LAR_ADMIN}</td>
								<td style="text-align: left; white-space: nowrap">${f.LAR_ALAMAT}</td>
								<td style="text-align: left; white-space: nowrap">${f.LAR_TELPON }</td>
								<td style="text-align: left; white-space: nowrap">${f.LAR_EMAIL}</td>
							</tr>
							<input type="hidden" id="index" name="index" value="${st.index}">
						</c:forEach>
						<input type="hidden" name="lar_id" id="lar_id" value="${cmd.lar_id}">	
					</tbody>
				</table>
			</c:otherwise>
		</c:choose>

	</body>
	
	<c:if test="${cmd.lar_id ne ''}">
		<script type="text/javascript">
			var a = ${cmd.index};
			var el = document.getElementById('daftarKantor'+a);
			 if(el.currentStyle) { 
			  // for msie 
			  el.style.backgroundColor = '#FFFFCC'; 
			 } else { 
			  // for real browsers ;) 
			  el.style.setProperty("background-color", "#FFFFCC"); 
			 } 
		</script>
	</c:if>

</html>