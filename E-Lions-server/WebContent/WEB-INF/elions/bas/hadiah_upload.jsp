<%@ include file="/include/page/header_jquery.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (jQueryUI Tabs) init tab2 Utama
		$("#tabs").tabs();
	
		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title], label[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});
	
		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});
	
		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});
	
		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				//window.scrollbars = false;
			}
		}
		
		// button icons
		/* $( "#btnShow" ).button({icons: {primary: "ui-icon-search"}});
		$( "#btnDetail" ).button({icons: {primary: "ui-icon-note"}});
		$( "#btnHistory" ).button({icons: {primary: "ui-icon-script"}});	 */	
	
		//bila ada pesan
	    function pesan(){
			var pesan = '${pesan}';
			if(pesan != '') alert(pesan);
		}
		
		function gup( name )
		{
		  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
		  var regexS = "[\\?&]"+name+"=([^&#]*)";
		  var regex = new RegExp( regexS );
		  var results = regex.exec( window.location.href );
		  if( results == null )
		    return "";
		  else
		    return results[1];
		}	
		/* var a = gup('_spaj');
		var b = gub('lspdID');
		var c = gub('mh_no');
		
		$("#reg_spaj").val(a);
		$("#lspd_id").val(b);
		$("mh_no").val(c); */
		
		var pesan = '${command.pesan}';
		if(pesan != '') alert(pesan);
		
		$("#btnTransfer").button("option","disabled",true);
		$("#btnSave").click(function(){
			$("#btnTransfer").button("option","disabled",false);
		});
	});
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 23em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 23.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 23em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

	/* untuk tombol2 kecil di report */
	ul#icons {margin: 0; padding: 0;}
	ul#icons li {margin: 0 1px 0 0; position: relative; padding: 0px; cursor: pointer; float: left;  list-style: none;}
	ul#icons span.ui-icon {float: left; margin: 0px;}

	/* styling untuk server-side validation error */
	.errorField { border: 1px solid red; }
	.errorMessage { color: red; display: block;}
	
	/* styling untuk table input */
	table.inputTable{
		width: 700px;
	}
	table.inputTable tr th{
		width: 200px;
	}
	
	/* styling untuk table info tanggal */
	div#infoTanggal{
		text-align: center;
		background-color: white;
		width: 345px;
		padding: 8px;
		border: 1px solid gray;
		position: fixed;
		left: 580px;
		top: 50px;
	}
</style>

<body>
	<c:choose>
		<c:when test="${lspd_pos eq \"86\" }">
			<form:form method="post" name="formpost" id="formpost" commandName="command" action="${path}/bas/hadiah.htm?window=upload" enctype="multipart/form-data">
				<fieldset class="ui-widget ui-widget-content">
					<legend class="ui-widget-header ui-corner-all"><em>Upload Bukti Bayar</em></legend>
				
					<table class="inputTable">
						<tr>
							<th>Nomor SPAJ : </th>
							<td>
								<form:input maxlength="11" title="Nomor SPAJ" path="reg_spaj" cssErrorClass="errorField" readonly="true" /><form:errors path="reg_spaj" cssClass="errorMessage"/>
								<form:hidden path="mh_no"/>
								<form:hidden path="lspd_id"/>
							</td>
						</tr>
						<tr>
							<th>Tanggal Bayar : </th>
							<td>
								<input class="datepicker" name="tgl_paid" id="tgl_paid"/>
							</td>
						</tr>
						<tr>
							<th>Upload bukti bayar : </th>
							<td>
								<input name="file1" type="file"/>
								<em>* dalam format pdf (Max 5 MB)</em>
							</td>
						</tr>
					</table>		
				</fieldset>
				<fieldset>
					<input type="submit" name="btnSave" id="btnSave" title="Simpan" value="Simpan" >
					<input type="hidden" name="_spaj" id="_spaj" value="${spaj }">
					<!-- <input type="submit" name="btnTransfer" id="btnTransfer" title="Transfer" value="Transfer"> -->
					<input type="submit" name="btnLogOff" id="btnLogOff" title="Log Off" value="Log Off" hidden="true">		
				</fieldset>
			</form:form>
		</c:when>
		<c:otherwise>
			<form:form method="post" name="formpost" id="formpost" commandName="command" action="${path}/bas/hadiah.htm?window=upload" enctype="multipart/form-data">
			<em>Pembayaran hadiah untuk spaj ${spaj } sudah di kofirmasi</em>
			</form:form>
		</c:otherwise>
	</c:choose>
</body>