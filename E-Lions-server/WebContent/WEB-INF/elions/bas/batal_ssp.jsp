<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path}/include/js/ajax.js"></script><!-- Custom Ajax scripts -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<!-- DatePicker Script (jscalendar) -->
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
<script type="text/javascript">
    hideLoadingMessage();
    
    function updateParent(){
        var dok;
        if(self.opener)
            dok = self.opener.document;
        else
            dok = parent.document;
            
        var frame = dok.getElementById('infoFrame');
        frame.src = frame.src;
        window.close();
    }
</script>
<script type="text/css">
    .readOnly {
        background-color:#999999;
    }
</script>
</head>
<BODY onload="resizeCenter(800,400); document.title='PopUp :: BATAL/TUTUP SIMAS SAVING PLAN'; setupPanes('container1', 'tab1'); document.formpost.kata.select();" style="height: 100%;">

    <div class="tab-container" id="container1">
        <ul class="tabs">
            <li>
                <a href="#" onClick="return showPane('pane1', this)" id="tab1">Batalkan Account</a>
            </li>
        </ul>

        <div class="tab-panes">

            <div id="pane1" class="panes">
                <form method="post" name="formpost" action="${path }/bas/batal_ssp.htm?id=${ssp.NO_ID}" style="text-align: center;">
                    <c:if test="${not empty successMessage}">
                        <script type="text/javascript">
                            alert('${successMessage}');
                            updateParent();
                        </script>
                    </c:if>
                    <c:if test="${not empty saveErrorMessage}">
                        <script type="text/javascript">
                            alert('${saveErrorMessage}');
                        </script>
                    </c:if>
                    <table class="entry2">
                        <tr>
                            <th>No Rekening</th>
                            <td style="width:2px;">:</td>
                            <td><input class="readOnly" type="text" value="${ssp.NO_REK}" readonly /></td>
                            <td style="text-decoration:underline;color:black;">${ssp.LC_ID}</td>
                        </tr>
                        <tr>
                            <th>Full Name</th>
                            <td style="width:2px;">:</td>
                            <td colspan="4"><input class="readOnly" type="text" size="55" value="${ssp.FULL_NAME}" readonly /></td>
                        </tr>
                        <tr>
                            <th>Tgl Lahir</th>
                            <td style="width:2px;">:</td>
                            <td><input class="readOnly" type="text" value="<fmt:formatDate value='${ssp.MCP_TGL_LAHIR}' pattern='dd/MM/yyyy' />" readonly /></td>
                            <th>Usia</th>
                            <td><input class="readOnly" type="text" size="2" value="${ssp.MCP_UMUR}" readonly /> Tahun</td>
                            <th>Sex</th>
                            <td><input class="readOnly" type="text" size="2" value="${ssp.MCP_SEX}" readonly /></td>
                        </tr>
                        <tr>
                            <th>Beg Date</th>
                            <td style="width:2px;">:</td>
                            <td><input class="readOnly" type="text" value="<fmt:formatDate value='${ssp.MCP_BEG_DATE}' pattern='dd/MM/yyyy' />" readonly /></td>
                            <td>s/d</td>
                            <td><input class="readOnly" type="text" value="<fmt:formatDate value='${ssp.MCP_END_DATE}' pattern='dd/MM/yyyy' />" readonly /></td>
                            <th>Jangka Waktu</th>
                            <td><input class="readOnly" type="text" size="2" value="${ssp.MCP_INSPER}" readonly /></td>
                        </tr>
                        <tr>
                            <th>Set Bulanan</th>
                            <td style="width:2px;">:</td>
                            <td><input class="readOnly" type="text" value="<fmt:formatNumber value='${ssp.SET_BULANAN}' type='currency' currencySymbol='' minFractionDigits='2' maxFractionDigits='2' />" readonly /></td>
                            <th>Set Cplan</th>
                            <td><input class="readOnly" type="text" value="<fmt:formatNumber value='${ssp.SET_CPLAN}' type='currency' currencySymbol='' minFractionDigits='2' maxFractionDigits='2' />" readonly /></td>
                            <th>Total Premi</th>
                            <td><input class="readOnly" type="text" value="<fmt:formatNumber value='${ssp.TOTAL_PREMI}' type='currency' currencySymbol='' minFractionDigits='2' maxFractionDigits='2' />" readonly /></td>
                        </tr>
                        <tr>
                            <th>Status Polis</th>
                            <td style="width:2px;">:</td>
                            <td><input class="readOnly" type="text" value="POLICY CANCELLED" readonly /></td>
                            <th>Posisi</th>
                            <td><input class="readOnly" type="text" value="POLICY CANCELLED" readonly /></td>
                        </tr>
                        <tr>
                            <th>Tgl Batal <span style="color:red">*</span></th>
                            <td style="width:2px;">:</td>
                            <td>
                                <c:choose>
                                    <c:when test="${not empty errorStatus}">
                                        <c:set var="tgl_btl" value="${tgl}" />
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:formatDate value='${ssp.MCP_TGL_BATAL}' pattern='dd/MM/yyyy' var="tgl_btl" />
                                    </c:otherwise>
                                </c:choose>
                                <input id="tgl_batal" <c:if test="${not empty errorStatus.tgl}">style="color:red;background-color:#FFE1FD;"</c:if> size="15" maxlength="10" type="text" name="tgl_batal" value="${tgl_btl}" />
                                <img id="btn_tgl_batal" style="cursor:pointer;" src="${path}/include/image/calendar.jpg" alt="Klik disini untuk memilih tanggal" />
                            </td>
                        </tr>
                        <tr>
                            <th>Alasan Batal <span style="color:red">*</span></th>
                            <td style="width:2px;">:</td>
                            <td colspan="4"><input <c:if test="${not empty errorStatus.alasan}">style="color:red;background-color:#FFE1FD;"</c:if> type="text" name="alasan" size="75" value="${alasan}" /></td>
                        </tr>
                    </table>
                    <c:if test="${not empty errorMessage}">
                        <script type="text/javascript">
                            alert("Ada Kesalahan Pengisian!");
                        </script>
                        <br />
                        <div style="text-align:left;font-weight:bold;">Error:</div>
                        <div style="text-align:left;color:red;background-color:#FFE1FD;border:1px solid red;padding:5px;">
                            <c:forEach items="${errorMessage}" var="em" varStatus="stat">
                                ${stat.count}. ${em}<br />
                            </c:forEach>
                        </div>
                    </c:if>
                    <br>
                    <input type="submit" name="submit" value="Save" accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();" />
                    <input type="button" name="close" value="Close" onclick="window.close();"
                        accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">
                </form>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    Calendar.setup({
        inputField : "tgl_batal",
        button : "btn_tgl_batal",
        ifFormat : "%d/%m/%Y"
    });
</script>
<%@ include file="/include/page/footer.jsp"%>