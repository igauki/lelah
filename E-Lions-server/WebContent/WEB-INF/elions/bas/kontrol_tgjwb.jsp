<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<script>
			/*
			popWin('${path}/bas/spaj.htm?window=form_spaj', 350, 450);
			showFrame('${path}/report/bas.htm?window=form_spaj');
			function showFrame(x){
				document.getElementById('infoFrame').src=x;
			}
			*/
			
			function tampilkan(btn){

				if(btn.name=='save_tgjwb') {
					if(!confirm('Pastikan data yang anda masukkan sudah benar!\nAnda TIDAK BISA melakukan perubahan lagi. Lanjutkan?')) {
						return false;
					}
				}
				document.getElementById('submitMode').value = btn.name;
				btn.disabled=true;
				document.getElementById('formpost').submit();
			}
			function awal(){
				if('${cmd.size}'=='0'){
					alert("Form tersebut Telah dipertanggungjawabkan\nSilahkan masukan permintaan yang baru.");
					window.close();
				}
				if('${cmd.proses}'=='0'){
					alert("Form Tersebut Harus DiPertanggungjawabkan di Branch Admin Asal Agen Tersebut..");
					window.close();
				}	
			}
			hideLoadingMessage();
		</script>
	</head>

	<body onload="setupPanes('container1', 'tab1'); awal();" style="height: 100%;">

		<div class="tab-container" id="container1">
		
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Pertanggungjawaban Form SPAJ</a>
				</li>
			</ul>
			
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form:form commandName="cmd" id="formpost" method="post">
						<spring:hasBindErrors name="cmd">
							<div id="error" style="text-transform: none;">
								ERROR:<br/>
								<form:errors path="*"/>
							</div>
						</spring:hasBindErrors>
						<form:hidden id="submitMode" path="submitMode"/>
						<form:hidden id="msf_id" path="msf_id"/>
	
							<table class="displaytag">
								<thead>
									<tr>
										<th>Jenis SPAJ</th>
										<th>No. Blanko</th>
										<th>Status</th>
										<th>Keterangan</th>
										<th>No. Register SPAJ</th>
										<th>No. Polis</th>
									</tr>
								</thead>
								<tbody>
								<c:choose>
									<c:when test="${cmd.posisi eq 6}">
										<c:forEach var="daftar" items="${cmd.daftarPertanggungjawaban}" varStatus="st">
											<tr>
												<td style="text-align: left;">${daftar.lsjs_desc}</td>
												<td style="text-align: center;">${daftar.lsjs_prefix}${daftar.no_blanko}</td>
												<td style="text-align: center;">${daftar.lsp_jenis}</td>
												<td style="text-align: center;">${daftar.mssd_desc}</td>
												<td style="text-align: center;">${daftar.reg_spaj}</td>
												<td style="text-align: center;">${daftar.mspo_policy_no}</td>
											</tr>
										</c:forEach>
									</c:when>
									<c:otherwise>
										<c:forEach var="daftar" items="${cmd.daftarPertanggungjawaban}" varStatus="st">
											<tr>
												<td style="text-align: left;">${daftar.lsjs_desc}</td>
												<td style="text-align: center;">${daftar.lsjs_prefix}${daftar.no_blanko}</td>
												<td style="text-align: center;">
													<form:select path="daftarPertanggungjawaban[${st.index}].lsp_id" items="${cmd.daftarJenisPertanggungjawaban}" itemLabel="value" itemValue="key" />
												</td>
												<td style="text-align: center;">
													<form:input path="daftarPertanggungjawaban[${st.index}].mssd_desc" maxlength="100" size="40"/>
												</td>
												<td style="text-align: center;">
													<form:input path="daftarPertanggungjawaban[${st.index}].reg_spaj" maxlength="11" size="14" cssClass="readOnly" readonly="true"/>
												</td>
												<td style="text-align: center;">
													<form:input path="daftarPertanggungjawaban[${st.index}].mspo_policy_no" maxlength="14" size="18" cssClass="readOnly" readonly="true"/>
												</td>
											</tr>
										</c:forEach>
									</c:otherwise>
								</c:choose>
								</tbody>
							</table>
							
							<c:if test="${cmd.posisi ne 6}">
								<br/>
								<input type="button" value="Simpan Pertanggungjawaban " name="save_tgjwb" onclick="return tampilkan(this);">
							</c:if>
							
							<spring:hasBindErrors name="cmd">
								<div id="error" style="text-transform: none;">
									ERROR:<br/>
									<form:errors path="*"/>
								</div>
							</spring:hasBindErrors>
							
					</form:form>
				</div>
			</div>
		</div>
	</body>
	<script>
		<c:if test="${not empty sukses}">
			<c:choose>
				<c:when test="${sukses eq \"Form pertanggungjawaban sudah di update\"}">
					if(parent) {
						parent.document.getElementById('list').innerHTML = "";
						alert('${sukses}');
					}
				</c:when>
				<c:otherwise>
					if(confirm('${sukses}\nTutup window ini?')) {
						window.close();
					}
				</c:otherwise>
			</c:choose>			
		</c:if>
	</script>
</html>