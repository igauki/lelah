<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-blue.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<script type="text/javascript">
			function cari(id){
				ag = document.getElementById('msag_id').value;
				ab = document.getElementById('msab_id').value;
				jn1 = document.getElementById('lsJenis1').value;
				jn2 = document.getElementById('lsJenis2').value;
				jn3 = document.getElementById('lsJenis3').value;
				bl = document.getElementById('nama_blanko').value;
				st = document.getElementById('tanggalAwal').value;
				en = document.getElementById('tanggalAkhir').value;
				cab = document.getElementById('lsCabang').value;
				st2 = document.getElementById('tanggalAwal2').value;
				en2 = document.getElementById('tanggalAkhir2').value;
				
				//alert("cab : "+cab+"\n"+"st2 : "+st2+"\n"+"en2 : "+en2+"\n");
				popWin('${path}/report/bas.pdf?window=report_history_fitrah&ag='+ag+"&ab="+ab+"&jn1="+jn1+"&jn2="+jn2+"&jn3="+jn3+"&bl="+bl+"&st="+st+"&en="+en+"&flag="+id+"&cab="+cab+"&st2="+st2+"&en2="+en2,600,800);
				document.formpost.reset();
			}
			hideLoadingMessage();
		</script>
	</head>
	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Per Agen</a>
					<a href="#" onClick="return showPane('pane2', this)" id="tab2">Per No Kartu</a>
					<a href="#" onClick="return showPane('pane3', this)" id="tab3">Per tanggal permintaan</a>
					<a href="#" onClick="return showPane('pane4', this)" id="tab4">Per cabang</a>
				</li>
			</ul>
			<form:form commandName="cmd" id="formpost" name="formpost" method="post">
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<table class="entry2">
						<tr>
							<th width="10%" rowspan="3">&nbsp;</th>
							<td>
								<input type="button" value="viewer agen" onclick="popWin('${path}/bas/spaj.htm?window=viewerAgen&mode=popUp', 480, 700);">
							</td>
						</tr>
						<tr>
							<td>
								<table class="entry2" style="width:auto;">
									<tr>
										<th>Kode Agen</th>
										<td>
											<input type="hidden" id="msab_id" name="msab_id">
											<input type="text" id="msag_id" name="msag_id" size="8" style="background-color: #D0D0D0;" readonly="true">
											<input type="text" id="msab_nama" name="msab_nama" style="background-color: #D0D0D0;" readonly="true" size="30">
										</td>
									</tr>
									<tr>
										<th>Region</th>
										<td><input type="text" id="lsrg_nama" name="lsrg_nama" style="background-color: #D0D0D0;" readonly="true" size="41"></td>
									</tr>
									<tr>
										<th>Jenis SPAJ</th>
										<td>
											<select id="lsJenis1" name="lsJenis">
												<option value="0">All</option>
												<option value="1">Non-Fitrah</option>
												<c:forEach var="d" items="${lsJenis}">
													<option value="${d.LSJS_PREFIX}">${d.LSJS_DESC}</option>
												</c:forEach>
											</select>										
										</td>
									</tr>									
								</table>
							</td>
						</tr>
						<tr>
							<td><input type="button" id="flag1" value="Cari" onClick="cari(this.id);"></td>
						</tr>
					</table>
				</div>	
				<div id="pane2" class="panes">
					<table class="entry2">
						<tr>
							<th width="10%" rowspan="2">&nbsp;</th>
							<td>
								<table class="entry2" style="width:auto;">
									<tr>
										<th>Jenis SPAJ</th>
										<td>
											<select id="lsJenis2" name="lsJenis">
												<option value="0">All</option>
												<option value="1">Non-Fitrah</option>
												<c:forEach var="d" items="${lsJenis}">
													<option value="${d.LSJS_PREFIX}">${d.LSJS_DESC}</option>
												</c:forEach>
											</select>										
										</td>
									</tr>
									<tr>
										<th>No Blanko</th>
										<td>
											<input type="text" id="no_blanko" name="nama_blanko">										
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td><input type="button" id="flag2" value="Cari" onClick="cari(this.id);"></td>
						</tr>						
					</table>
				</div>				
				<div id="pane3" class="panes">
					<table class="entry2">
						<tr>
							<th width="10%" rowspan="2">&nbsp;</th>
							<td>
								<table class="entry2" style="width:auto;">
									<tr>
										<th>Jenis SPAJ</th>
										<td>
											<select id="lsJenis3" name="lsJenis">
												<option value="0">All</option>
												<option value="1">Non-Fitrah</option>
												<c:forEach var="d" items="${lsJenis}">
													<option value="${d.LSJS_PREFIX}">${d.LSJS_DESC}</option>
												</c:forEach>
											</select>										
										</td>
									</tr>
									<tr>
										<th>Periode</th>
										<td>
											 <script>inputDate('tanggalAwal', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>
											 s/d 
											 <script>inputDate('tanggalAkhir', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>
										</td>
									</tr>
								</table>							
							</td>
						</tr>
						<tr>
							<td><input type="button" id="flag3" value="Cari" onClick="cari(this.id);"></td>
						</tr>						
					</table>
				</div>
				<div id="pane4" class="panes">
					<table class="entry2">
						<tr>
							<th width="10%" rowspan="2">&nbsp;</th>
							<td>
								<table class="entry2" style="width:auto;">
									<tr>
										<th>Cabang</th>
										<td>
											<select id="lsCabang" name="lsCabang">
												<option value="0">All</option>
												<c:forEach var="d" items="${lsCabang}">
													<option value="${d.KEY}">${d.VALUE}</option>
												</c:forEach>
											</select>										
										</td>
									</tr>
									<tr>
										<th>Periode</th>
										<td>
											 <script>inputDate('tanggalAwal2', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>
											 s/d 
											 <script>inputDate('tanggalAkhir2', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>
										</td>
									</tr>
								</table>							
							</td>
						</tr>
						<tr>
							<td><input type="button" id="flag4" value="Cari" onClick="cari(this.id);"></td>
						</tr>						
					</table>
				</div>
			</div>
			</form:form>
		</div>
	</body>
</html>
