<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript">
	var sukses = '${sukses}';
	if(sukses==1){
		alert('${successMessage}');
		parent.location.href='${path}/report/bas.htm?window=qa_1';
	}
	
	function toSubmit(){
      	var result = confirm("Yakin akan di transfer?");
		if(result==true){
			return true;
		}else{
			return false; 
		}
		
		return false;
   }
</script>
<body style="height: 100%;">
	<c:choose>
		<c:when test="${speedy  eq 1 }" >
			<div id="success">${successMessage }</div>
		</c:when>
		<c:otherwise>
			<form method="post" name="formpost" action="" onsubmit="return toSubmit();">
				<fieldset>
					<legend>Back To Admin</legend>
					<table class="entry2">
						<tr>
							<th>To:</th>
							<td><input type="text" value="${to }" name="to" id="to" style="width: 420px;"></td>
						</tr>
						<tr>
							<th>Cc:</th>
							<td><input type="text" value="${cc }" value="" name="cc" id="cc" style="width: 420px;"></td>
						</tr>
						<tr>
							<th>Subject:</th>
							<td><input type="text" value="${subject }" name="subject" id="subject" style="width: 420px;"></td>
						</tr>
						<tr>
							<th>Kategori:</th>
							<td>
							<select name="flagKategori" id="flagKategori" title="Pilih Salah satu" style="width: 420px;">											
												<option value="1" <c:if test="${flagKategori eq '1'}">selected="selected"</c:if>>Kekurangan Dokumen</option>
												<option value="2" <c:if test="${flagKategori eq '2'}">selected="selected"</c:if>>Kekurangan/kesalahan pengisian data</option>
												<option value="3" <c:if test="${flagKategori eq '3'}">selected="selected"</c:if>>Kekurangan Dokumen dan Kekurangan/kesalahan pengisian data </option>
										</select>
							</td>
						</tr>
						<tr>
							<th>Isi</th>
							<td><textarea rows="7" cols="83" name="isi" id="isi">${message}</textarea></td>
						</tr>
						<tr>
							<th></th>
							<td>
								<input type="submit" name="btnKirim" id="btnKirim" value="Kirim & Transfer">
							</td>	
						</tr>
					</table>
				</fieldset>
			</form>
		</c:otherwise>
	</c:choose>
</body>
<%@ include file="/include/page/footer.jsp"%>