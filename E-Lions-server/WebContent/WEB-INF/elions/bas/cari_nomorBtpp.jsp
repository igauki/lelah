<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/header.jsp"%>
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript">
	hideLoadingMessage();
	function backToParent(nomor){
		self.opener.document.getElementById('infoFrame').src='${path}/bas/btpp.htm?window=show_btpp&nomor='+nomor; 
		addOptionToSelect(self.opener.document, self.opener.document.formpost.nomor, nomor, nomor+'~');
		window.close();	
	}

</script>
</head>
<BODY onload="document.title='PopUp :: Cari Nomor';formpost.value.focus()" style="height: 100%;">

<form method="post" name="formpost" action="${path }/bas/btpp.htm?window=cari_nomorBtpp" style="text-align: center;">
	<div id="contents">
	<fieldset>
		<legend>Cari Nomor</legend>
		<table class="entry2">
				<th>Cari:</th>
				<th>
					<select name="tipe">
						<option <c:if test="${tipe eq 1}">selected</c:if> value="1">Nomor</option>
						<option <c:if test="${tipe eq 2}">selected</c:if> value="2">Nama Penyetor</option>
					</select>
					<select name="filter">
						<option <c:if test="${\"LIKE%\" eq filter}">selected</c:if> value="LIKE%">LIKE%</option>
						<option <c:if test="${\"%LIKE\" eq filter}">selected</c:if> value="%LIKE">%LIKE</option>
						<option <c:if test="${\"%LIKE%\" eq filter}">selected</c:if> value="%LIKE%">%LIKE%</option>
					</select>					
					<input type="text" name="value" value="${value}" size="30">
				</th>
			</tr>
			<tr>
				<td colspan="2">
					<div align="center">
					<input type="submit" value="Search" 
						accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();">
					</div>
				</td>
			</tr>
		</table>
		<table class="simple">
			<thead>	
				<tr align="left">
					<th>Nomor</th>
					<th>Nama Pembayar Premi</th>
					<th>Nama Cabang</th>
					<th>Nama Admin</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="id" items="${lsBtpp}" varStatus="stat">
					<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
						onclick="backToParent('${id.mst_no}');">
						<td>${id.mst_no}</td>
						<td>${id.mst_nm_pemegang }</td>
						<td>${kd_cab}</td>
						<td>${id.lst_nm_admin}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<br>
		<input type="button" name="close" value="Close" onclick="window.close();"
		accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">
		<br>
		<br>
	</fieldset>
	</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>