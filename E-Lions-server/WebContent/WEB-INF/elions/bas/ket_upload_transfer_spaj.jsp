<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<script type="text/javascript" src="${path}/include/jquery/jquery-1.5.1.min.js"></script>
		
		<script>
			hideLoadingMessage();
		</script>
	</head>

	<body onload="setupPanes('container1', 'tab1');" style="height: 100%;">
		<fieldset>
		<legend>Input keterangan belum upload scan atau transfer SPAJ</legend>
		<form id="formpost" name="formpost" method="post">
			<table class="entry2" style="width: 700px;" align="left">
				<tr>
					<th>Reg Spaj</th>
					<td>
						<input type="text" id="spaj" name="spaj" value="${spaj }" readonly="readonly">
						<input type="hidden" id="id" name="id" value="${lus_id }">						
					</td>
				</tr>
				<tr>
					<th>Keterangan</th>
					<td>
						<textarea rows="5" cols="45" id="keterangan" name="keterangan"></textarea>					
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<input type="submit" name="simpan" id="simpan" value="Simpan" />
						<input type="button" name="close" id="close" value="Close" />
					</td>
				</tr>
			</table>
		</form>
		</fieldset>
	</body>
	<script>
		<c:if test="${not empty pesan}">
			alert('${pesan}');
		</c:if>
	</script>
</html>