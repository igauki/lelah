<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-blue.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript">
			function cari(){
				jenis= document.getElementById('bmi_id').value; 
				tgl1 = document.getElementById('tanggalAwal').value;
				tgl2 = document.getElementById('tanggalAkhir').value;  
				popWin('${path}/report/bas.htm?window=report_fitrah_bmi&jenis='+jenis+'&tanggalAwal='+tgl1+'&tanggalAkhir='+tgl2,600,800);
			}
					
			hideLoadingMessage();
		</script>
	</head>
	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">  
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Fitrah Muamalat</a>
				</li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<table class="entry2">
						<tr>
							<th>Cabang</th>
							<td>
								<select id="bmi_id" name="bmi_id">
									<option value="0">All</option>
									<c:forEach items="${lsCabBmi}" var="x">
										<option value="${x.key}">${x.value}</option>
									</c:forEach>
								</select>							
							</td>
						</tr>
						<tr>
							<th width="10%">Periode</th>
							<td width="30%" colspan="2">
								<script>inputDate('tanggalAwal', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>
								 s/d 
								 <script>inputDate('tanggalAkhir', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>
							</td>
						</tr>						
						<tr>
							<th></th>
							<td><input type="button" name="btnCari" value="View" onClick="cari();"></td>
						</tr>
					</table>
				</div>
			</div>				
		</div>	
	</body>
</html>
