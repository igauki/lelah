<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<script>
			/*
			popWin('${path}/bas/spaj.htm?window=form_spaj', 350, 450);
			showFrame('${path}/report/bas.htm?window=form_spaj');
			function showFrame(x){
				document.getElementById('infoFrame').src=x;
			}
			*/
			
			function setLcaid(btn) {
				document.getElementById('lca_id')[0].selected = 'selected';
				document.getElementById('submitMode').value = btn.name;
				document.getElementById('msf_id').value = document.getElementById('msf_id_search').value;
				document.getElementById('posisi').value= 1;
				btn.disabled=true;
				document.getElementById('formpost').submit();				
			}
			
			function tampilkan(btn){

				if(btn.name=='send') {
					if(!confirm('Dengan menekan tombol kirim, maka cabang akan diinformasikan bahwa SPAJ sedang dalam pengiriman. Lanjutkan?')) {
						return false;
					}

				} else if(btn.name=='show') {
					var lca_id = document.getElementById('lca_id').value;
					if(lca_id=='') {
						alert('Silahkan pilih salah satu cabang terlebih dahulu');				
					}else{
						document.getElementById('formFrame').src = '${path}/bas/spaj.htm?window=daftarForm&posisi=1&pos=1,4&lca_id=' + lca_id;
					}
					return false;
				}
					
				document.getElementById('submitMode').value = btn.name;
				btn.disabled=true;
				document.getElementById('formpost').submit();
			}
			
			function cetak(){
				
				popWin('${path}/report/bas.htm?window=covering_letter_ga&msf_id=${cmd.msf_id}', 600, 800);
			}
			
			function showBlanko(lsjsId,ke){
				popWin('${path}/bas/spaj.htm?window=detailBlankoSimasCard&lsjsId='+lsjsId+'&ke='+ke, 250, 175);
			}
			
			function Calculate()
			{
			  /* var jumlah_umum = document.getElementById('jml_umum').value;	
			  var total_spaj=document.getElementById('total'); 
			  var jumlah_umum=0;
		  	 <c:forEach var="daftar" items="${cmd.daftarFormSpaj}" varStatus="st">		  	  
		  	  	<c:if test="${daftar.lsjs_prefix ne 'K' && daftar.lsjs_prefix ne 'TB' && daftar.lsjs_prefix ne 'U'}">		  	  	
			  	  var jumlah = document.getElementById('jml[${st.index}]').value;
			  		
			  		
				  if(isNaN(document.getElementById('jml_umum').value=jumlah_umum))
			  		document.getElementById('jml_umum').value="";
			  		
			  	  else				  	    
			  	   jumlah_umum= Math.round(parseInt(jumlah_umum)+parseInt(jumlah));
			  	 </c:if>
			  	 document.getElementById('jml_umum').value=jumlah_umum;			  	 
		  	 </c:forEach> */
		  	 
		  	 	//var jumlah_umum_U = document.getElementById('jml_umum_U').value;				 
				var jumlah_umum_U = 0 ; //umum
				var jumlah_umum_UFBK = 0 ; //umum full konvensional
				var jumlah_umum_UFS = 0 ; //umum full syariah
				var jumlah_umum_UGS = 0 ; //umum Gio Syariah
				var jumlah_umum_UGK = 0 ; //umum Gio Konvensional
				var jumlah_umum_USK = 0 ; //umum Sio Konvensional
				//var jumlah_umum_USS = 0 ; //umum Sio Syariah
			  
			  	//umum  - ${daftar.lsjs_prefix eq 'SPL' || daftar.lsjs_prefix eq 'VFP' || daftar.lsjs_prefix eq 'SLST' || daftar.lsjs_prefix eq 'UL' || daftar.lsjs_prefix eq 'NUL' || daftar.lsjs_prefix eq 'PRS' || daftar.lsjs_prefix eq 'SPR' }
	  			<c:forEach var="daftar" items="${cmd.daftarFormSpaj}" varStatus="st">		  	  
			  	  	/* <c:if test="${daftar.lsjs_prefix eq 'UL' || daftar.lsjs_prefix eq 'VFP' || daftar.lsjs_prefix eq 'NUL' || daftar.lsjs_prefix eq 'SLST' || daftar.lsjs_prefix eq 'SPL' }">		  	  	
				  	  var jumlah = document.getElementById('jml[${st.index}]').value;			  		
				  		
					  if(isNaN(document.getElementById('jml_umum_U').value=jumlah_umum_U))
						document.getElementById('jml_umum_U').value="";
				  		
				  	  else				  	    
				  	   jumlah_umum_U = Math.round(parseInt(jumlah_umum_U)+parseInt(jumlah));				  	  
	
				  	</c:if> */
				  	
				  	//umum full konvensional
				  	<c:if test="${daftar.lsjs_prefix eq 'UL' || daftar.lsjs_prefix eq 'VFP' || daftar.lsjs_prefix eq 'NUL' || daftar.lsjs_prefix eq 'SLST' || daftar.lsjs_prefix eq 'SPL'}">		  	  	
				  	  var jumlah_UFBK = document.getElementById('jml[${st.index}]').value;			  		
				  		
					  if(isNaN(document.getElementById('jml_umum_UFBK').value=jumlah_umum_UFBK))
						document.getElementById('jml_umum_UFBK').value="";
				  		
				  	  else				  	    
				  	   jumlah_umum_UFBK = Math.round(parseInt(jumlah_umum_UFBK)+parseInt(jumlah_UFBK));				  	  
	
				  	</c:if>
				  	
				  	//umum full syariah
				  	<c:if test="${daftar.lsjs_prefix eq 'NULS' || daftar.lsjs_prefix eq 'ULS' || daftar.lsjs_prefix eq 'SMS' }">		  	  	
				  	  var jumlah_UFS = document.getElementById('jml[${st.index}]').value;			  		
				  		
					  if(isNaN(document.getElementById('jml_umum_UFS').value=jumlah_umum_UFS))
						document.getElementById('jml_umum_UFS').value="";
				  		
				  	  else				  	    
				  	   jumlah_umum_UFS = Math.round(parseInt(jumlah_umum_UFS)+parseInt(jumlah_UFS));				  	  
	
				  	</c:if>
				  	
				  	//umum Gio Syariah
				  	<c:if test="${daftar.lsjs_prefix eq 'PSS' || daftar.lsjs_prefix eq 'POSS' || daftar.lsjs_prefix eq 'PSYN' }">		  	  	
				  	  var jumlah_UGS = document.getElementById('jml[${st.index}]').value;			  		
				  		
					  if(isNaN(document.getElementById('jml_umum_UGS').value=jumlah_umum_UGS))
						document.getElementById('jml_umum_UGS').value="";
				  		
				  	  else				  	    
				  	   jumlah_umum_UGS = Math.round(parseInt(jumlah_umum_UGS)+parseInt(jumlah_UGS));				  	  
	
				  	</c:if>
				  	
				  	//umum Gio Konvensional
				  	<c:if test="${daftar.lsjs_prefix eq 'SPR' || daftar.lsjs_prefix eq 'PRS' || daftar.lsjs_prefix eq 'PDP' }">		  	  	
				  	  var jumlah_UGK = document.getElementById('jml[${st.index}]').value;		
				  		
					  if(isNaN(document.getElementById('jml_umum_UGK').value=jumlah_umum_UGK))
						document.getElementById('jml_umum_UGK').value="";
				  		
				  	  else				  	    
				  	   jumlah_umum_UGK = Math.round(parseInt(jumlah_umum_UGK)+parseInt(jumlah_UGK));				  	  
	
				  	</c:if>
				  	
				  	//umum Sio Konvensional
				  	<c:if test="${daftar.lsjs_prefix eq 'EV' || daftar.lsjs_prefix eq 'MV' }">		  	  	
				  	  var jumlah_USK = document.getElementById('jml[${st.index}]').value;			  		
				  		
					  if(isNaN(document.getElementById('jml_umum_USK').value=jumlah_umum_USK))
						document.getElementById('jml_umum_USK').value="";
				  		
				  	  else				  	    
				  	   jumlah_umum_USK = Math.round(parseInt(jumlah_umum_USK)+parseInt(jumlah_USK));				  	  
	
				  	</c:if>
				  	
				  	//umum Sio Syariah
				  	/* <c:if test="${daftar.lsjs_prefix eq 'SPL' || daftar.lsjs_prefix eq 'VFP' || daftar.lsjs_prefix eq 'SLST' || daftar.lsjs_prefix eq 'UL' || daftar.lsjs_prefix eq 'NUL' || daftar.lsjs_prefix eq 'PRS' || daftar.lsjs_prefix eq 'SPR' }">		  	  	
				  	  var jumlah_USS = document.getElementById('jml[${st.index}]').value;			  		
				  		
					  if(isNaN(document.getElementById('jml_umum_USS').value=jumlah_umum_USS))
						document.getElementById('jml_umum_USS').value="";
				  		
				  	  else				  	    
				  	   jumlah_umum_USS = Math.round(parseInt(jumlah_umum_USS)+parseInt(jumlah_USS));				  	  
	
				  	</c:if> */
			  	 	
			  	 	document.getElementById('jml_umum_U').value=jumlah_umum_U;
			  	 	document.getElementById('jml_umum_UFBK').value=jumlah_umum_UFBK;
			  	 	document.getElementById('jml_umum_UFS').value=jumlah_umum_UFS;
			  	 	document.getElementById('jml_umum_UGS').value=jumlah_umum_UGS;
			  	 	document.getElementById('jml_umum_UGK').value=jumlah_umum_UGK;
			  	 	document.getElementById('jml_umum_USK').value=jumlah_umum_USK;
			  	 	/*document.getElementById('jml_umum_USS').value=jumlah_umum_USS; */
			  	 	
			  	 				  	 		  	 
		  	 	</c:forEach>	
		  	
			 }
			hideLoadingMessage();
		</script>
	</head>

	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Pengiriman Permintaan SPAJ</a>
					<a href="#" onClick="return showPane('pane2', this)" id="tab2">History Posisi</a>
					<a href="#" onClick="return showPane('pane4', this)" id="tab4">Search</a>
					<a href="#" onClick="return showPane('pane3', this)" id="tab3">Legend</a>
				</li>
			</ul>
	
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form:form commandName="cmd" id="formpost" method="post">
						<form:hidden id="submitMode" path="submitMode"/>
						<form:hidden id="index" path="index"/>
						<table class="entry2">
							<tr>
								<th style="width: 150px;">
									<form:hidden id="posisi" path="posisi"/>
									<select id="lca_id" name="lca_id" size="${sessionScope.currentUser.comboBoxSize}
											" style="width: 140px;" onclick="document.getElementById('show').click();">
										<c:forEach var="d" items="${cmd.daftarCabang}">
											<option value="${d.KEY}" 
												<c:if test="${d.KEY eq cmd.lca_id}"> selected </c:if>
											>${d.VALUE}</option>
										</c:forEach>
									</select>
									<input type="button" id="show" name="show" value="Show" style="width: 55px;" onclick="return tampilkan(this);">
								</th>
								<td style="vertical-align: top;">								
									<fieldset>
										<legend>Daftar Permintaan Spaj</legend>
										<iframe id="formFrame" src="${path}/bas/spaj.htm?window=daftarForm&posisi=1&pos=1,4&lca_id=${cmd.lca_id}"></iframe>
									</fieldset>
									<fieldset>
										<legend>Pengiriman Permintaan SPAJ</legend>
										
										<table class="entry2">
											<tr>
												<th>Stok SPAJ Cabang</th>
												<td colspan="2">
													<table class="displaytag" style="width:auto;">
														<thead>
															<tr>
																<c:forEach var="s" items="${cmd.daftarStokSpaj}">
																	<th style="width: 50px; " title="${s.lsjs_desc}">${s.lsjs_prefix}</th>
																</c:forEach>
															</tr>
														</thead>
														<tbody>
															<tr>
																<c:forEach var="s" items="${cmd.daftarStokSpaj}">
																	<td title="${s.lsjs_desc}">${s.mss_amount}</td>
																</c:forEach>
															</tr>
														</tbody>
													</table>
												</td>
												<th align="left">
													 Tanggal<fmt:formatDate value="${cmd.formHist.msfh_dt}" pattern="dd/MM/yyyy"/>
												</th>
											</tr>
											<tr>
												<th>Nomor Form</th>
												<td colspan="3">
													<form:input id="msf_id" path="msf_id" cssClass="readOnly" readonly="true" size="20"/>												
												</td>
											</tr>
											<c:if test="${not empty cmd.daftarFormSpaj}">
												<tr>
													<th>Status</th>
													<td colspan="3">
														<input type="text" value="${cmd.daftarFormSpaj[0].status_form}" readonly="true" size="20" value="${cmd.daftarFormSpaj[0].status_form}" style="background-color: ${cmd.daftarFormSpaj[0].warna};">
													</td>
												</tr>
												<tr>
													<th>Cabang</th>
													<td colspan="3">
														<input type="text" class="readOnly" readonly="true" size="40" value="${cmd.daftarFormSpaj[0].lca_nama}"/>
													</td>
												</tr>
												<tr>
													<th>User</th>
													<td>
														<input type="text" class="readOnly" readonly="true" size="40" value="${cmd.formHist.lus_login_name}"/>
													</td>
													<th width="97px">Travel Ins</th>
													<td>
														<select id=admTravIns name="admTravIns">
															<option value="">Pilih jenis</option>
															<c:forEach items="${cmd.typeTravelIns}" var="x">
																<option value="${x.key}" <c:if test="${x.key eq cmd.daftarFormSpaj[0].trav_ins_type}">selected="selected"</c:if>>${x.value}</option>
															</c:forEach>
														</select>
													</td>
												</tr>
												<tr>
													<th>Detail Permintaan</th>
													<td colspan="3">
														<table class="displaytag" style="width:auto;">
															<thead>
																<tr>
																	<th rowspan="2">No.</th>
																	<th rowspan="2">Jenis SPAJ</th>
																	<th rowspan="2">Prefix</th>
																	<th colspan="2">Jumlah SPAJ</th>
																	<th rowspan="2">No. Blanko</th>
																	<th rowspan="2">Status</th>
																</tr>
																<tr>
																	<th>Diminta</th>
																	<th>Disetujui</th>
																</tr>
															</thead>
															<tbody>
															<c:forEach var="daftar" items="${cmd.daftarFormSpaj}" varStatus="st">
																<tr>
																	<td style="text-align: left;">${st.count}.</td>
																	<td style="text-align: left;">${daftar.lsjs_desc}</td>
																	<td style="text-align: center;">${daftar.lsjs_prefix}</td>
																	<td style="text-align: center;">
																		<form:input path="daftarFormSpaj[${st.index}].msf_amount_req" size="10" cssStyle="text-align: right;" cssClass="readOnly" readonly="true"/>
																	</td>
																	<td style="text-align: center;">
																		<c:if test="${cmd.daftarFormSpaj[0].status_form ne 'SENT'}">
																			<c:choose>
																					<c:when test="${daftar.lsjs_prefix eq 'U' or daftar.lsjs_prefix eq 'UFBK' or daftar.lsjs_prefix eq 'UFS' or daftar.lsjs_prefix eq 'UGS' or daftar.lsjs_prefix eq 'UGK' or daftar.lsjs_prefix eq 'USK' or daftar.lsjs_prefix eq 'USS'}">
																						<form:input path="daftarFormSpaj[${st.index}].msf_amount" size="10" cssStyle="text-align: right;" cssClass="readOnly" readonly="true" onfocus="this.select();" id ="jml_umum_${daftar.lsjs_prefix}" onchange="Calculate()" onkeyup="Calculate()"/>
																					</c:when>
																					<c:otherwise>
																						<form:input path="daftarFormSpaj[${st.index}].msf_amount" size="10" cssStyle="text-align: right;" onfocus="this.select();" id="jml[${st.index}]" onchange="Calculate()" onkeyup="Calculate()"/>
																					</c:otherwise>
																			</c:choose>
																			<%-- <c:if test="${daftar.lsjs_prefix ne 'U'}"> 
																				<form:input path="daftarFormSpaj[${st.index}].msf_amount" size="10" cssStyle="text-align: right;" onfocus="this.select();" id="jml[${st.index}]" onchange="Calculate()" onkeyup="Calculate()"/>
																			</c:if>
																			<c:if test="${daftar.lsjs_prefix eq 'U'}">
																				<form:input path="daftarFormSpaj[${st.index}].msf_amount" size="10" cssStyle="text-align: right;" cssClass="readOnly" readonly="true" onfocus="this.select();" id ="jml_umum" onchange="Calculate()" onkeyup="Calculate()"/>
																			</c:if> --%>
																		</c:if>
																		<c:if test="${cmd.daftarFormSpaj[0].status_form eq 'SENT'}"> 
																			<form:input path="daftarFormSpaj[${st.index}].msf_amount" size="10" cssStyle="text-align: right;" cssClass="readOnly" readonly="true" onfocus="this.select();" />
																		</c:if>
																		<span class="error">*</span>
																	</td>
																	<td style="text-align: center;">
																		<c:choose>
																			<c:when test="${not empty daftar.msf_id}">
																			<c:if test="${cmd.daftarFormSpaj[0].status_form ne 'SENT'}">
																				<c:choose>
																					<c:when test="${daftar.lsjs_prefix eq 'U' or daftar.lsjs_prefix eq 'UFBK' or daftar.lsjs_prefix eq 'UFS' or daftar.lsjs_prefix eq 'UGS' or daftar.lsjs_prefix eq 'UGK' or daftar.lsjs_prefix eq 'USK' or daftar.lsjs_prefix eq 'USS' or daftar.lsjs_prefix eq 'TB'}">
																						<form:input path="daftarFormSpaj[${st.index}].no_blanko_req" size="50" onfocus="this.select();" cssClass="readOnly" readonly="true"/>
																					</c:when>
																					<c:otherwise>
																						<form:input path="daftarFormSpaj[${st.index}].no_blanko_req" size="50" onfocus="this.select();" />
																					</c:otherwise>
																				</c:choose>
																			
																				<%-- <c:if test="${daftar.lsjs_prefix ne 'U' && daftar.lsjs_prefix ne 'TB'}"> 
																					<form:input path="daftarFormSpaj[${st.index}].no_blanko_req" size="50" onfocus="this.select();" />
																				</c:if>
																				<c:if test="${daftar.lsjs_prefix eq 'U' || daftar.lsjs_prefix eq 'TB'}"> 
																					<form:input path="daftarFormSpaj[${st.index}].no_blanko_req" size="50" onfocus="this.select();" cssClass="readOnly" readonly="true"/>
																				</c:if> --%>
																			</c:if>	
																			<c:if test="${cmd.daftarFormSpaj[0].status_form eq 'SENT'}"> 
																				<form:input path="daftarFormSpaj[${st.index}].no_blanko_req" size="50" onfocus="this.select();" cssClass="readOnly" readonly="true"/>
																			</c:if>	
																				<span class="error">*</span>
																			</c:when>
																			<c:otherwise>-</c:otherwise>
																		</c:choose>
																	</td>
																	<td style="text-align: center;">${daftar.status_form}</td>
																	<c:if test="${daftar.lsjs_prefix eq \'SC\'}">
																		<td><input type="button" value="V" onClick="showBlanko('${daftar.lsjs_id}','${st.index}')"></td>
																	</c:if>
																</tr>
															</c:forEach>
	
															<tr>
																<td colspan="3" style="text-align: center;">Total SPAJ: </td>
																<td style="text-align: center;">
																	?
																</td>
															</tr>
															
															</tbody>
														</table>
													</td>
												</tr>
												<tr>
													<th>Keterangan</th>
													<td>
														<form:textarea path="formHist.msfh_desc" cols="70" rows="2" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "/>
														<span class="error">*</span>
													</td>
												</tr>
												<tr>
													<th>Kirim</th>
													<td> 
														<input id="send"  <c:if test="${cmd.daftarFormSpaj[0].status_form ne 'APPROVED'}">  disabled </c:if> type="button" value="Kirim" name="send" onclick="return tampilkan(this);">
														<input id="btnCetak" <c:if test="${cmd.daftarFormSpaj[0].status_form ne  'SENT'}">  disabled </c:if> type="button" name="btnCetak" value="cetak" onClick="cetak();">
														<c:choose>
															<c:when test="${not empty infoMessage }"><div id="success" style="text-transform: none;">${infoMessage}</div></c:when>
															<c:otherwise>
																<spring:hasBindErrors name="cmd">
																	<div id="error" style="text-transform: none;">
																		<form:errors path="*"/>
																	</div>
																</spring:hasBindErrors>
															</c:otherwise>
														</c:choose>
													</td>
												</tr>
											</c:if>
										</table>								
									</fieldset>
								</td>
							</tr>
						</table>
					</form:form>
				</div>
				<div id="pane2" class="panes">
					<fieldset>
						<legend>History Permintaan SPAJ</legend>
						<display:table id="detail" name="cmd.daftarFormHistory" class="displaytag">
							<display:column property="msf_urut" title="No." style="text-align: left;"/>
							<display:column property="msf_id" title="No. Form" style="text-align: center;" />
							<display:column property="status_form" title="Posisi" style="text-align: center;" />
							<display:column property="lus_login_name" title="User" style="text-align: center;" />
							<display:column property="msfh_dt" title="Tanggal" format="{0, date, dd/MM/yyyy (hh:mm)}" style="text-align: center;" />
							<display:column property="msfh_desc" title="Keterangan" style="text-align: center;" />
						</display:table>
					</fieldset>
				</div>
				<div id="pane3" class="panes">
					<jsp:include flush="true" page="legend.jsp" />
				</div>
				<div id="pane4" class="panes">
					<table class="entry2">
						<tr>
							<th>No Form</th>
							<td><input type="text" id="msf_id_search" class="readOnly" size="20"/>
								<input type="button" id="show" name="show" value="Search" style="width: 55px;" onclick="return setLcaid(this);">
							</td>
						</tr>
					</table>
				</div>				
			</div>
		</div>
	</body>
	<script>
		<c:if test="${not empty param.sukses}">
			alert('${param.sukses}');
		</c:if>
	</script>
</html>