<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<script>
			jml='${cmd.size}';		
			function pilAll(){
				flag=formpost.cekAll.checked;
				//jika cuma satu data
				if(jml==1){
					if(flag)
						formpost.elements['daftarBlanko[0].check'].checked=true;			
					else
						formpost.elements['daftarBlanko[0].check'].checked=false;						
				}else{
					if(flag){
						
						for(i=0;i<jml;i++){
							formpost.elements['daftarBlanko['+i+'].check'].checked=true;			
						}
					}else{
						for(i=0;i<jml;i++){
							formpost.elements['daftarBlanko['+i+'].check'].checked=false;			
						}
					}
				}	
			}
			function kirim(){
				var hasil='';
				var tot=0;
				for(i=0;i<jml;i++){
					if(formpost.elements['daftarBlanko['+i+'].check'].checked){				
						if(hasil==''){
							if(jml==1)
								hasil=formpost.noBlanko.value;
							else
								hasil=formpost.noBlanko[i].value;			
						}else
							hasil=hasil+';'+formpost.noBlanko[i].value;			
						tot++;
					}	
				}
				self.opener.document.getElementById('daftarFormSpaj['+${cmd.ke}+'].no_blanko_req').value =hasil;
				self.opener.document.getElementById('daftarFormSpaj['+${cmd.ke}+'].msf_amount_req').value =tot;
				window.close();
			}
			hideLoadingMessage();
		</script>
	</head>
<form:form commandName="cmd" id="formpost" method="post">
	<body style="height: 100%;">
		<fieldset>		
		<legend>${cmd.jnsSpaj }</legend>
		<table class="entry2">
			<tr>
				<th>No.</th>
				<th>No Blanko</th>
				<th>Pilih</th>
			</tr>
			<c:forEach var="s" items="${cmd.daftarBlanko}" varStatus="st">
				<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;">
					<td>${st.index+1}</td>	
					<td><input type="hidden" name="noBlanko" id="noBlanko"  value="${s.no_blanko }">${s.no_blanko }</td>
					<td>
						<form:checkbox cssClass="noborder" path="daftarBlanko[${st.index }].check" value="1"/>
					</td>
				</tr>	
			</c:forEach>
				<tr>
					<th></th>
					<th>Check All/Uncheck All</th>
					<td>
						<input type="checkbox" class="noBorder" name="cekAll" onclick="pilAll();"  >
						
					</td>
				</tr>
				<tr>
					<th colspan="3">
						<input type="button" name="btnOk" onclick="kirim();" value="Ok" >
					</th>
				</tr>
		</table>
		</fieldset>
	</body>
</form:form>
</html>