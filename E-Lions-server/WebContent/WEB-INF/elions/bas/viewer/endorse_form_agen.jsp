<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
			<script>
			function awal(){
				formpost.msfId.select();
				formpost.msfId.focus();
			}
			function tampil(){
				indicator.style.display='';
				window.location='${path}/bas/spaj.htm?window=endorse_form_agen&msfId='+formpost.msfId.value;
			}
			function endorse(){
				if(confirm("Anda Yakin Ingin Mengganti Status Pertanggungjawaban Agen di Admin anda?")){
					indicator.style.display='';
					window.location='${path}/bas/spaj.htm?window=endorse_form_agen&msfId='+formpost.msfId.value+'&flagEndorse=1';
				}
			}
			
			hideLoadingMessage();
		</script>
	</head>
	<form:form commandName="cmd" id="formpost" method="post">
	<body style="height: 100%;" onLoad="awal()">
		<fieldset>
			<legend>Masukan No Form Agen Yang Ingin Di Ubah Status PertanggungJawabannya</legend>
			<table class="entry2">
				<tr>
					<th width="20%" >No Form Agen </th>
					<td width="30%">
						<input type="text" name="msfId" value="${cmd.msfId}" onkeypress="if(event.keyCode==13) {document.formpost.btnShow.click(); return false;}">
						<span id="indicator" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
					</td>
					<td></td>
				</tr>
				<tr>
					<th>User Yang Memberikan SPAJ</th>
					<td>${cmd.nama}</td>
					<td></td>
				</tr>
				<tr>
					<th>Cabang</th>
					<td>${cmd.cabang}</td>
					<td></td>
				</tr>
				<tr>
					<th>Kode Agen</th>
					<td>${cmd.msag_id}</td>
					<td></td>
				</tr>
				<tr>
					<th>Nama Agen</th>
					<td>${cmd.nama_agen}</td>
					<td></td>
				</tr>
				<tr>
					<th colspan="2">
						<input type="button" name="btnShow" value="show" onClick="tampil();">
					  	<input type="button" name="btnEndorse" value="Endorse" onClick="endorse();">
					</th>
					<td></td>
				</tr>
			</table>
		</fieldset>	
		<fieldset>
			<legend>Data No Form ${cmd.msfId}</legend>
			<display:table id="baris" name="${cmd.lsDaftar}" class="displaytag"  >
				<display:column property="lsjs_prefix" title="lsjs_prefix"  sortable="true"/>                                                                                 
				<display:column property="lsjs_desc" title="lsjs_desc"  sortable="true"/>                                                                                     
				<display:column property="msf_amount_req" title="msf_amount_req"  sortable="true"/>                                                                           
				<display:column property="no_blanko_req" title="no_blanko_req"  sortable="true"/>                                                                             
			</display:table>   
		</fieldset>	
		
	</body>
	</form:form>
</html>