<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script>
			function tampilkan(msag_id, msab_id, msab_nama, lca_id, lwk_id, lsrg_id,lsrg_nama){	
				if(formpost.flag.length == 3) {
					if(parent) {
						//alert(lsrg_nama);
						var mode = document.getElementById('mode').value;
						if(mode != '') {
							parentForm = self.opener.document.forms['formpost'];
							if(parentForm) {
								parentForm.elements['msag_id'].value = msag_id; 
								parentForm.elements['msab_nama'].value = msab_nama; 
								parentForm.elements['msab_id'].value = msab_id; 
								parentForm.elements['lsrg_nama'].value = lsrg_nama;
							}
							window.close();
						}
						else {						
							if(!lwk_id) lwk_id='';
							if(!lsrg_id) lsrg_id='';
							parent.document.getElementById('msag_id').value = msag_id;
							parent.document.getElementById('msab_id').value = msab_id;
							parent.document.getElementById('msab_nama').value = msab_nama;
							parent.document.getElementById('lca_id').value = lca_id;
							parent.document.getElementById('lwk_id').value = lwk_id;
							parent.document.getElementById('lsrg_id').value = lsrg_id;
							parent.document.getElementById('submitMode').value = 'show';
							parent.document.getElementById('formpost').submit();
						}	
					}				
				}else if(formpost.flag.length == 4) {
					if(parent) {
						parent.document.getElementById('msag_id').value = msag_id;
						parent.document.getElementById('msab_id').value = msab_id;
						parent.document.getElementById('lca_id').value = lca_id;
						parent.document.getElementById('lwk_id').value = lwk_id;
						parent.document.getElementById('lsrg_id').value = lsrg_id;
						//parent.document.getElementById('msab_nama').value = msab_nama;
						parent.document.getElementById('submitMode').value = 'show';
						parent.document.getElementById('formpost').submit();
						//parent.document.getElementById('new').disabled = '';
					}
				}

			}
			function cari(){
				indicator.style.display='';
				f=formpost.flag.value;
				q=formpost.query.value;
				l=formpost.lca_id.value;
				m=formpost.mode.value;
				if(formpost.flag.length == 3) {
					window.location='${path}/bas/spaj.htm?window=viewerAgen&flag='+f+'&query='+q+'&lcaId='+l+'&mode='+m;
				}else if(formpost.flag.length == 4) {
					if(l == "00" && f == '0') {
					 	indicator.style.display = 'none';
						alert("tidak bisa menampilkan semua nama agent dari semua cabang");
					}else 
						window.location='${path}/bas/spaj.htm?window=viewerAgenNew&flag='+f+'&query='+q+'&lcaId='+l;
				}
			}
		</script>
	</head>
	<form:form commandName="cmd" id="formpost" method="post">
	<body style="height: 100%;" onLoad="formpost.query.select();formpost.query.focus();">
		<div class="tabcontent" style="border: none;">
			<table class="entry2">
				<tr>
					<td>Nama Cabang
						<select name="lca_id" >
							<c:forEach items="${cmd.lsCabang}" var="x">
								<option value="${x.KEY}" <c:if test="${cmd.lca_id == x.KEY}">selected</c:if>> ${x.VALUE}</option>
							</c:forEach>
						</select>
						Jenis Cari
						<select name="flag" >
							<c:forEach items="${cmd.lsCari}" var="x">
								<option value="${x.KEY}" 
									<c:if test="${cmd.flag == x.KEY}">selected</c:if>>
									${x.VALUE}
								</option>
							</c:forEach>
						</select>
						<input type="text" size="30" name="query"  onkeypress="if(event.keyCode==13) {document.formpost.btnCari.click(); return false;}" value="${cmd.query}">
						<input type="button" name="btnCari" value="Cari" onClick="cari();" >
					  	<span id="indicator" style="display:none;"><img src="${path}/include/image/indicator.gif" /></span>
					  	<input type="hidden" id="mode" name="mode" value="${cmd.mode}">
					</td>
				</tr>	
				<tr>
					<td>
					<table class="entry2" style="width: 90%;" >
						<tr>
							<th>Region</th>
							<th>Kode Agen</th>
							<th>Nama Agen</th>
							<th>Jenis Identitas</th>
							<th>No. Identitas</th>
							<th>Tingkat</th>
							<th>Status</th>
						</tr>
						<c:forEach items="${cmd.lsAgen}" var="f">
							<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='pointer';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
							<c:if test="${f.msag_active eq 0}">bgcolor="#d6d6d6"</c:if> style="cursor: pointer;" onclick="tampilkan('${f.msag_id}', '${f.msab_id}', '${f.msab_nama}', '${f.lca_id}', '${f.lwk_id}', '${f.lsrg_id}', '${f.lsrg_nama}');">
								<td style="text-align: center; white-space: nowrap">${f.lsrg_nama}</td>
								<td style="text-align: center; white-space: nowrap">${f.msag_id}</td>
								<td style="text-align: left; white-space: nowrap">${f.msab_nama}</td>
								<td style="text-align: left; white-space: nowrap">${f.lside_name}</td>
								<td style="text-align: left; white-space: nowrap">${f.mspe_no_identity}</td>
								<td style="text-align: center; white-space: nowrap">${f.tingkat}</td>
								<td style="text-align: center; white-space: nowrap">
									<c:if test="${f.msag_active eq 1}">Aktif</c:if>
									<c:if test="${f.msag_active eq 0}">Tidak Aktif</c:if>
								</td>
							</tr>
						</c:forEach>
						<tr>
						<td colspan="7">
						<c:if test="${not empty cmd.pesan}">
							<div class="tabcontent" style="border: none;">
								<div id="success" style="text-transform: none">
									${cmd.pesan}
								</div>
							</div>
						</c:if>
						</td>
						</tr>
					</table>
					</td>
				</tr>	
			</table>
		</div>
	</body>
	</form:form>
</html>