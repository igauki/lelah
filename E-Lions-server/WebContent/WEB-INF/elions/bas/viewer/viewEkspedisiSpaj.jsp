<%@ include file="/include/page/header.jsp"%>

<script type="text/javascript">
hideLoadingMessage();

function searchEkspedisiSpaj()
{
    var form = document.frmParam;
    form.target="";
}

function printEkspedisiSpaj()
{
    var form = document.frmParam;
    form.target="_blank";
}

</script>

<body style="height: 100%;">
<div id="contents">
	<form name="frmParam" method="post">
		<fieldset>
			<legend>Report Ekspedisi Spaj - Kantor Pusat</legend>
			<table class="entry2" style="width: auto;">
            <tr>
                <th style="width: 100px;">Tanggal Awal</th>
                <td>
                    <script>inputDate('tanggalAwal', '${tanggalAwal}', false,'');</script>
                </td>
            </tr>
            <tr>
                <th >Tanggal Akhir</th>
                <td>
                    <script>inputDate('tanggalAkhir', '${tanggalAkhir}', false, '');</script>
                </td>
            </tr>
            <tr>
                <th colspan="2">
                    <input type="submit" value="Cari" name="search" onClick="searchEkspedisiSpaj();">
                    <input type="submit" value="Print" name="print" onClick="printEkspedisiSpaj();">
                </th>
            </tr>
			<tr>
				<td colspan="2">
			        <table class="displaytag">
			        	<tr>
			        		<th><input type="checkbox" onclick="checkAll('checkFlag');" class="noBorder"></th>
			        		<th>No. Register</th>
			        		<th>Pemegang Polis</th>
			        	</tr>
			            <c:forEach items="${resultList}" var="pemegangPolisVO">
			            	<c:choose>
			            		<c:when test="${pemegangPolisVO.MSPO_CASH_BACK eq 1}">
				                <tr>
				                    <td style="text-align: center; background-color: #FFFFC0;">
				                        <input class="noBorder" type="checkbox" name="checkFlag" value="${pemegangPolisVO.REG_SPAJ}">
				                    </td>
				                    <td style="text-align: center; background-color: #FFFFC0;"><elions:spaj nomor="${pemegangPolisVO.REG_SPAJ}"/></td>
				                    <td style="text-align: left; background-color: #FFFFC0;">${pemegangPolisVO.MCL_FIRST}</td>
				                </tr>
				                </c:when>
				                <c:otherwise>
				                <tr>
				                    <td style="text-align: center;">${pemegangPolisVO.MSPO_CASH_BACK}
				                        <input class="noBorder" type="checkbox" name="checkFlag" value="${pemegangPolisVO.REG_SPAJ}">
				                    </td>
				                    <td style="text-align: center;"><elions:spaj nomor="${pemegangPolisVO.REG_SPAJ}"/></td>
				                    <td style="text-align: left;">${pemegangPolisVO.MCL_FIRST}</td>
				                </tr>
				                </c:otherwise>
			                </c:choose>
			            </c:forEach>
			        </table>
			        <br>
			        Keterangan:<br/>
					<input type="button" value="&nbsp;&nbsp;&nbsp;" style="background-color: #FFFFC0;"> Sudah pernah dicetak
	       		</td>
	       	</tr>
		</table>
	</form>
</div>
</body>

<%@ include file="/include/page/footer.jsp"%>