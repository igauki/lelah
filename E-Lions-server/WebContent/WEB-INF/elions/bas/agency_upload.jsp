<%@ include file="/include/page/header.jsp"%>
<%@ page import="com.ekalife.elions.model.User"%>
<%User currentUser = (User) session.getAttribute("currentUser");%>
<script type="text/javascript" src="${path }/include/js/ajaxtags/prototype-1.4.0.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/scriptaculous.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
<script type="text/javascript" src="${path }/include/js/ajaxtags/ajaxtags.js"></script>

<script type="text/javascript">
	hideLoadingMessage();
	
</script>

</head>

<body onload="" style="height: 100%;">
<div id="contents">
<div class="tab-container" id="container1">

		<div class="tab-panes">

			<div id="pane1" class="panes">
<form method="post" name="formpost" style="text-align: center;" enctype="multipart/form-data">
<fieldset>
<legend>Agency Upload File</legend>
									<table class="entry2">
										<tr>
											<th>File</th>
								<td>
												<input type="file" name="file1" id="file1" size="70" />
												<input type="hidden" name="file_fp" id="file_fp"/>
											</td>
										</tr>
									</table>
						</fieldset>
						<fieldset>
					   <legend>-</legend>
					   <table class="entry2">
						<tr>
							<th colspan="2" align="left">
								<font class="error">* FILE YANG DIUPLOAD ADALAH FILE EXCEL</font>
							</th>
						</tr>
						<tr>
							<th colspan="2">
								<input type="submit" name="upload" value="Upload" onclick="document.getElementById('file_fp').value=document.getElementById('file1').value" />
							</th>
						</tr>
					</table>
					</fieldset>
					<fieldset style="text-align: left;">
<legend>Status Upload</legend>
<c:forEach items="${successMessage}" var="d" varStatus="st">
<c:choose>
<c:when test="${d.sts eq 'FAILED'}">
<font color="red">${st.index+1}. ${d.msg}</font>
</c:when>
<c:otherwise>
<font color="blue">${st.index+1}. ${d.msg}</font>
</c:otherwise>
</c:choose>

<br/>
</c:forEach>
</fieldset>
					</div>
		</div>
	</div>	
</form>
	</div>
</body>
</html>