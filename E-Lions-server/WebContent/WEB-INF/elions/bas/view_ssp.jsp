<%@ include file="/include/page/header.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
    hideLoadingMessage();

    function awal(){
        setFrameSize('infoFrame', 64);
    }

    function setSspId(id,lssp){
        var sspId = document.getElementById("sspId");
        sspId.value = id;
        var statusPolis = document.getElementById("statusPolis");
        statusPolis.value = lssp;
    }
    
    function batalSsp(){
        var sspId = document.getElementById("sspId").value;
        var stPol = document.getElementById("statusPolis").value;
        
        if(sspId == '' && stPol == ''){
                alert('Harap pilih Account yang ingin dibatalkan terlebih dahulu');
                return false;
        }
        
        if(confirm('Batalkan account ini?')){
            if(stPol == '8'){
                alert('Status Account Sudah Batal !');
                return false;
            }
            popWin('${path}/bas/batal_ssp.htm?id='+sspId, 350, 450);
        }
    }


</script>
<body 
    onload="awal(); setFrameSize('infoFrame', 85);"
    onresize="setFrameSize('infoFrame', 85);" style="height: 100%;">

<form name="frmParam" method="post">
<div class="tabcontent">
<table class="entry2" style="width: 98.4%;">
    <tr>
        <th style="width:10%;">SSP</th>
        <td>
            <input type="button" value="Refresh" name="refresh" onclick="document.location.reload()" style="background-color: yellow;" />
            <input type="button" value="Cari" name="search"
                onclick="popWin('${path}/bas/cari_ssp.htm', 350, 450);"
                accesskey="C" onmouseover="return overlib('Cari Simas Saving Plan', AUTOSTATUS, WRAP);" onmouseout="nd();" />
        </td>
    </tr>
    <tr>
        <th>Proses</th>
        <td>
            <input type="button" value="Batal" name="info" onclick="return batalSsp();" 
                accesskey="B" onmouseover="return overlib('Batal Simas Saving Plan', AUTOSTATUS, WRAP);" onmouseout="nd();" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="hidden" id="sspId" value="" />
            <input type="hidden" id="statusPolis" value="" />
            <iframe src="${path}/bas/view_detail_ssp.htm" name="infoFrame" id="infoFrame"
                        width="100%" style="width: 100%;"> Please Wait... </iframe>
        </td>
    </tr>
</table>
</div>
</form>

</body>
<%@ include file="/include/page/footer.jsp"%>