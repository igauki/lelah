<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<script type="text/javascript" src="${path}/include/jquery/jquery-1.5.1.min.js"></script>
		
		<script>
			function isNumberKey(evt){
	      		var charCode = (evt.which) ? evt.which : event.keyCode;
	          	if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
		             return false;
		
	          	return true;
			}
			
			var fieldName='chbox';
	
			function selectall(){
			  var i=document.formpost.elements.length;
			  var e=document.formpost.elements;
			  var name=new Array();
			  var value=new Array();
			  var j=0;
			  for(var k=0;k<i;k++)
			  {
			    if(document.formpost.elements[k].name==fieldName)
			    {
			      if(document.formpost.elements[k].checked==true){
			        value[j]=document.formpost.elements[k].value;
			        j++;
			      }
			    }
			  }
			  checkSelect();
			}
				
			function selectCheck(obj)
			{
			 var i=document.formpost.elements.length;
			  for(var k=0;k<i;k++)
			  {
			    if(document.formpost.elements[k].name==fieldName)
			    {
			      document.formpost.elements[k].checked=obj;
			    }
			  }
			  selectall();
			}
				
			function selectallMe()
			{
			  if(document.formpost.allCheck.checked==true)
			  {
			   selectCheck(true);
			  }
			  else
			  {
			    selectCheck(false);
			  }
			}
				
			function checkSelect()
			{
			 var i=document.formpost.elements.length;
			 var berror=true;
			  for(var k=0;k<i;k++)
			  {
			    if(document.formpost.elements[k].name==fieldName)
			    {
			      if(document.formpost.elements[k].checked==false)
			      {
			        berror=false;
			        break;
			      }
			    }
			  }
			  if(berror==false)
			  {
			    document.formpost.allCheck.checked=false;
			  }else
			  {
			    document.formpost.allCheck.checked=true;
			  }
			}
			hideLoadingMessage();
		</script>
	</head>

	<body onload="setupPanes('container1', 'tab1');" style="height: 100%;">
		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Update Stok Brosur</a>
					<c:if test="${currentUser.lus_id eq \"3041\" or currentUser.lus_id eq \"2664\"}">
						<a href="#" onClick="return showPane('pane2', this)" id="tab2">Tambah Brosur</a>
					</c:if>
				</li>
			</ul>
	
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form id="formpost" name="formpost" method="post">
						<table class="entry2" style="width: 700px;" align="left">
							<tr>
								<th>Jenis</th>
								<td>
									<select id="jenis" name="jenis" onchange="jenis(this);">
										<option value="1">Brosur</option>
										<option value="2">Poster</option>
										<option value="3">Banner</option>
										<option value="4">Flayer</option>
									</select>								
								</td>
								<th>Busdev</th>
								<td>
									<select id="busdev" name="busdev" onchange="busdev(this);">
										<option value="1">Agency</option>
										<!-- <option value="2">Corporate</option>
										<option value="3">Bancass 1 (Team Ibu Yanti S) Bancass Support</option>
										<option value="4">Bancass 2 & Bancass 1 BusDev</option> -->
									</gselect>
								</td>
								<td><input type="submit" name="ganti" id="ganti" value="Ganti" /></td>
							</tr>
							<tr>
								<td colspan="5">&nbsp;</td>
							</tr>
							<tr>
								<th><input type="checkbox" name="allCheck" onclick="selectallMe()" style="border: none;"></th>
								<th>Kode</th>
								<th>Brosur</th>
								<th>Stok</th>
								<th>Tambah Stok</th>
							</tr>
							<c:forEach items="${brosur }" var="s">
							<tr>
								<th align="center"><input name="chbox" type="checkbox" id="chbox" value="${s.STOCK_ID}" style="border: none;"></th>
								<th>
									${s.CODE_BROSUR }
									<input type="hidden" id="prefix_${s.STOCK_ID}" name="prefix_${s.STOCK_ID}" value="${s.CODE_BROSUR }"> 
								</th>
								<th>${s.STOCK_NAME }</th>
								<th>${s.STOCK }</th>
								<th><input type="text" style="text-align: right;" name="stok_${s.STOCK_ID}" id="stok_${s.STOCK_ID}" value="0" onkeypress="return isNumberKey(event)" /></th>
							</tr>
							</c:forEach>
							<tr>
								<td colspan="4" align="center"><input type="submit" name="update" id="update" value="Update" /></td>
							</tr>
						</table>
					</form>
				</div>
				<div id="pane2" class="panes">
				<c:if test="${currentUser.lus_id eq \"3041\" or currentUser.lus_id eq \"2664\"}">
					<table class="entry2">
						<tr>
							<th>Kode</th>
							<th>Brosur</th>
							<!-- <th>Stok</th>
							<th>Busdev</th> -->
						</tr>
						<c:forEach items="${brosur }" var="s">
						<tr>
							<td>${s.CODE_BROSUR }</td>
							<td>${s.STOCK_NAME }</td>
							<%-- <td>${s.STOCK }</td>
							<td>${s.BUSDEV }</td> --%>
						</tr>
						</c:forEach>
					</table>
					<br><br>
					<fieldset>
					<legend>Tambah Brosur</legend>
					<form method="post" id="formpost2" name="formpost2">
						<table class="entry2">
							<tr>
								<th width="200">Prefix/Kode</th>
								<td><input type="text" name="prefix" id="prefix" />
									<input type="hidden" id="jenis2" name="jenis2">
									<input type="hidden" id="busdev2" name="busdev2">
								</td>
							</tr>
							<tr>
								<th width="200">Nama Brosur</th>
								<td><input type="text" name="nm_brosur" id="nm_brosur" /></td>
							</tr>
							<%-- <tr>
								<th width="200">Busdev</th>
								<td><select name="busdev" id="busdev">
										<c:forEach items="${busdev }" var="l">
											<option  value="${l.key }">${l.value }</option>
										</c:forEach>
									</select></td>
							</tr> --%>
							<tr>
								<td colspan="2"><input type="submit" name="simpan" id="simpan" value="Simpan" /></td>
							</tr>
						</table>
					</form>
					</fieldset>
				</c:if>
				</div>
			</div>
			
		</div>

	</body>
	<script>
		<c:if test="${not empty pesan}">
			alert('${pesan}');
		</c:if>
		$('#jenis').val('${jenis}');
		$('#busdev').val('${busdev}');
		
		$('#jenis2').val('${jenis}');
		$('#busdev2').val('${busdev}');
		
	</script>
</html>