<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript" src="${path}/include/js/jquery-1.3.2.min.js"></script>
<script>
	$(document).ready(function() {
		<c:if test="${not empty param.pesan}">
			alert('${param.pesan}');
		</c:if>
		<c:if test="${cmd.mia.status eq 0}">
			$('#searchSpan').animate({ opacity: 'hide' }, 'fast');
		</c:if>
		
		$("#region").change(function(e) {
			var data = this.value.toString().split(".");
			$('#lca_id').val(data[0]);
			$('#lwk_id').val(data[1]);
			$('#lsrg_id').val(data[2]);
			
			//$('#submitMode').val(this.id);
			//$('#formpost').submit();
		});
		
		$('.noBorder').click(function(e) {
			//e.preventDefault();
			if(this.id == "statusBaru") { window.location='${path}/bas/agency.htm'; }
			else if(this.id == "statusLihat") {
				$('#searchSpan').animate({ opacity: 'show' }, 'medium');
			}
		});	
		
		$('#search').click(function (e) {
			popWin('${path}/bas/spaj.htm?&window=cari_mia', 480, 640);
		});		
		
		$('#miaDelete').click( function (e) {
			if(confirm("Are you sure want to delete this row ?")) {
				$('#submitMode').val(this.id);
				$('#formpost').submit();
			}
			
		});
	});
</script>
	<body>
		<form:form id="formpost" name="formpost" commandName="cmd" method="post">
			<table width="100%" class="entry2" border="0">
		  		<tr>
		  			<th width="150px">Agen</th>
		  			<td>
						<label for="statusBaru"><form:radiobutton path="mia.status" value="0" id="statusBaru" cssClass="noBorder"/>Baru</label>
						<label for="statusLihat"><form:radiobutton path="mia.status" value="1" id="statusLihat" cssClass="noBorder"/>Lihat</label>
						<span id="searchSpan">
					 		<input type="button" class="button" id="search" value="Search" style="width:100px;">
				 		</span>
		  			</td>
		  		</tr>
		  		<tr>
		  			<th>&nbsp;</th>
		  			<td>
		  				<fieldset>
		  					<legend>Data Agen</legend>
		  					<table width="100%" class="entry2" border="0">
		  						<tr>
		  							<th width="150px">Register</th>
		  							<td><form:input id="mia_agensys_id" size="40" readonly="readonly" cssClass="readOnly" path="mia.mia_agensys_id" cssErrorClass="errField" /></td>
		  						</tr>
		  						<tr>
		  							<th>No Kontrak</th>
		  							<td><form:input id="mia_no_kontrak" size="40" path="mia.mia_no_kontrak" cssErrorClass="errField" /></td>
		  						</tr>
		  						<tr>
		  							<th>Region</th>
		  							<td>
		  								<form:select path="mia.region" id="region" title="region" cssErrorClass="errField">
								 			<form:option label="" value=""/>
								   			<form:options items="${lsRegion}" itemLabel="key" itemValue="value"/>
								 		</form:select>
								 		<form:hidden path="mia.lca_id" id="lca_id"/>
								 		<form:hidden path="mia.lwk_id" id="lwk_id"/>
								 		<form:hidden path="mia.lsrg_id" id="lsrg_id"/>
		  							</td>
		  						</tr>
		  						<tr>
		  							<th>Nama</th>
		  							<td><form:input id="mia_nama" size="40" path="mia.mia_nama" cssErrorClass="errField" /> ${cmd.mia.msag_id}</td>
		  							<th width="150px">Level</th>
		  							<td width="300px">
		  								<form:select path="mia.mia_level" title="level" cssErrorClass="errField">
								 			<form:option label="" value=""/>
								   			<c:forEach items="${cmd.mia.lsLevel}" var="x">
												<form:option label="${x.key}" value="${x.value}"/>
											</c:forEach>
								 		</form:select>
		  							</td>
		  						</tr>
		  						
		  						<tr>
		  							<th>No ID</th>
		  							<td><form:input id="mia_ktp" size="40" path="mia.mia_ktp" cssErrorClass="errField" /></td>
		  						</tr>
		  						<tr>
		  							<th>Recruiter</th>
		  							<td><form:input id="mia_birth_place" size="12" path="mia.mia_recruiter" cssErrorClass="errField" /> ${cmd.mia.nama_recruiter}</td>
		  							<th>Level Recruit</th>
		  							<td>
		  								<form:select path="mia.mia_level_recruit" title="level" cssErrorClass="errField">
								 			<form:option label="" value=""/>
								   			<c:forEach items="${cmd.mia.lsLevel}" var="x">
												<form:option label="${x.key}" value="${x.value}"/>
											</c:forEach>
								 		</form:select>
		  							</td>
		  						</tr>
		  						<tr>
		  							<th>Alamat</th>
		  							<td colspan="3"><form:input id="mia_alamat" path="mia.mia_alamat" size="126" cssErrorClass="errField" /></td>
		  						</tr>
		  						<tr>
		  							<th>Tempat Lahir</th>
		  							<td><form:input id="mia_birth_place" size="40" path="mia.mia_birth_place" cssErrorClass="errField" /></td>
		  							<th>Tgl. Lahir</th>
		  							<td>
		  								<spring:bind path="cmd.mia.mia_birth_date">
											<script>inputDate('${status.expression}', '${status.value}', false);</script>
										</spring:bind>
		  							</td>
		  						</tr>
		  						<tr>
		  							<th>Tgl Aktif</th>
		  							<td>
		  								<spring:bind path="cmd.mia.mia_tgl_aktif">
											<script>inputDate('${status.expression}', '${status.value}', false);</script>
										</spring:bind>
		  							</td>
		  							<th>Status</th>
		  							<td>
		  								<form:select path="mia.mia_aktif" title="status" cssErrorClass="errField">
								 			<form:option label="" value=""/>
								   			<form:options items="${lsStatus}" itemLabel="key" itemValue="value"/>
								 		</form:select>
		  							</td>
		  						</tr>
		  						<tr>
		  							<th>Awal Kontrak</th>
		  							<td>
		  								<spring:bind path="cmd.mia.mia_awal_kontrak">
											<script>inputDate('${status.expression}', '${status.value}', false);</script>
										</spring:bind>
		  							</td>
		  							<th>Akhir Kontrak</th>
		  							<td>
		  								<spring:bind path="cmd.mia.mia_akhir_kontrak">
											<script>inputDate('${status.expression}', '${status.value}', false);</script>
										</spring:bind>
		  							</td>
		  						</tr>
		  						<tr>
		  							<th>Nama Bank</th>
		  							<td>
		  								<form:select path="mia.lbn_id" title="bank" cssErrorClass="errField">
								 			<form:option label="" value=""/>
								   			<form:options items="${lsBank}" itemLabel="key" itemValue="value"/>
								 		</form:select>
		  							</td>
		  						</tr>
		  						<tr>
		  							<th>No Rek</th>
		  							<td><form:input id="mia_no_rek" size="40" path="mia.mia_no_rek" cssErrorClass="errField" /></td>
		  						</tr>
		  						<tr>
		  							<th>Tgl Terima Berkas</th>
		  							<td>
		  								<spring:bind path="cmd.mia.mia_tgl_berkas">
											<script>inputDate('${status.expression}', '${status.value}', false);</script>
										</spring:bind>
		  							</td>
		  						</tr>
		  						
		  						<tr>
		  							<th>&nbsp;</th>
		  							<td>
		  								<input type="submit" class="button" id="miaSimpan" value="Simpan" style="width:100px;">
		  								<c:if test="${cmd.mia.status eq 1}">
											<input type="button" class="button" id="miaDelete" value="Delete" style="width:100px;">
										</c:if>
		  							</td>
		  						</tr>
		  						<tr>
									<td colspan="4">
										<spring:hasBindErrors name="cmd">
											<div id="error" style="margin: 5px 5px 5px 3px;">
												HARAP LENGKAPI DATA DIBAWAH:			
												<br>- <form:errors path="*" delimiter="<br>- "/>
											</div>
										</spring:hasBindErrors>
									</td>
								</tr>
		  							
		  					</table>
		  				</fieldset>
		  				<input type="hidden" name="submitMode" id="submitMode">
		  			</td>
		  		</tr>
			</table>
		</form:form>
	</body>
<%@ include file="/include/page/footer.jsp"%>
