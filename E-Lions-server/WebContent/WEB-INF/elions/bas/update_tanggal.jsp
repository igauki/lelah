<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-blue.css" media="screen">
		<script type="text/javascript">
			var contextPath = "${path}";
		</script>
		<script type="text/javascript" src="${path }/include/js/calendarDateInput.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>		
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript">
			function submit() {
				window.location='${path}/bas/spaj.htm?window=update_tanggal';
			}
			hideLoadingMessage();
		</script>
	</head>
	<body>
		<form:form commandName="cmd" id="formpost" method="post" action="${path}/bas/spaj.htm?window=update_tanggal">
		 	<table class="entry2">
		 		<tr>
		 			<th>Kode Agent</th>
		 			<td><input type="text" name="agent"></td>
		 		</tr>
		 		<tr>
		 			<th>No Blanko</th>
		 			<td><input type="text" name="blanko"> </td>
		 		</tr>
		 		<tr>
		 			<th>Tanggal update</th>
		 			<td><script type="text/javascript">inputDate('tanggal', '', false);</script></td>
		 		</tr>
		 		<tr>
		 			<th>&nbsp;</th>
		 			<td><input type="submit" name="update" value="update"></td>
		 		</tr>
		 		<tr>
					<td colspan="2">
						<c:if test="${not empty cmd}">
				        	<div id="success">
					        	${cmd.pesan}
				        	</div>
				        </c:if>						
					</td>		 		
		 		</tr>		 		
		 	</table>
		 </form:form>	
	</body>
</html>
