<%@ include file="/include/page/header.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
    hideLoadingMessage();

    function awal(){
        setFrameSize('infoFrame', 64);
        var noSsp = document.getElementById("noSsp").value;
        var lssp_id = document.getElementById("lssp_id").value;
        window.parent.setSspId(noSsp,lssp_id);
    }


</script>
<body 
    onload="awal();" style="height: 100%;">

<form name="frmParam" method="post">
<div class="tabcontent">
<table class="entry2" style="width: 98.4%;">
    <tr>
        <th>No. ID</th>
        <td>
            <input id="noSsp" type="text" value="${ssp.NO_ID}" readonly />
            <input type="hidden" id="lssp_id" value="${ssp.LSSP_ID}" />
        </td>
        <th>No. Acc</th>
        <td><input type="text" value="${ssp.NO_REK}" readonly /></td>
        <td colspan="3"><input type="text" size="75" value="${ssp.NAMA_CABANG}" readonly /></td>
        <td style="color:#000;">Kode : ${ssp.KANWILL_ID}</td>
    </tr>
    <tr>
        <th>Full Name</th>
        <td><input type="text" value="${ssp.FULL_NAME}" readonly /></td>
        <th>Sex</th>
        <td><input type="text" size="2" value="${ssp.MCP_SEX}" readonly /></td>
        <td style="text-decoration:underline;color:black;">${ssp.LC_ID}</td>
        <th>TypeReas</th>
        <td><input type="text" value="${ssp.MCP_FLAG_REAS}" /></td>
    </tr>
    <tr>
        <th>Tgl. Lahir</th>
        <td><input type="text" value="<fmt:formatDate value='${ssp.MCP_TGL_LAHIR}' pattern='dd/MM/yyyy' />" readonly /></td>
        <th>Usia</th>
        <td><input type="text" value="${ssp.MCP_UMUR}" readonly /></td>
        <th>Beg Date</th>
        <td><input type="text" value="<fmt:formatDate value='${ssp.MCP_BEG_DATE}' pattern='dd/MM/yyyy' />" readonly /></td>
        <th>End Date</th>
        <td><input type="text" value="<fmt:formatDate value='${ssp.MCP_END_DATE}' pattern='dd/MM/yyyy' />" readonly /></td>
        <th>Premi EL</th>
        <td><input style="background-color:grey;" type="text" value="" readonly /></td>
    </tr>
    <tr>
        <th>Set Bulanan</th>
        <td>
            <input type="text" value="<c:if test='${not empty ssp.LKU_ID}'><c:choose><c:when test='${ssp.LKU_ID eq "01"}'>Rp</c:when><c:otherwise>US$</c:otherwise></c:choose></c:if>" size="3" readonly /> 
            <input type="text" value="<fmt:formatNumber value='${ssp.SET_BULANAN}' type='currency' currencySymbol='' minFractionDigits='2' maxFractionDigits='2' />" readonly />
        </td>
        <th>Set Cplan</th>
        <td><input type="text" value="<fmt:formatNumber value='${ssp.SET_CPLAN}' type='currency' currencySymbol='' minFractionDigits='2' maxFractionDigits='2' />" readonly /></td>
        <th>Total Premi</th>
        <td><input type="text" value="<fmt:formatNumber value='${ssp.TOTAL_PREMI}' type='currency' currencySymbol='' minFractionDigits='2' maxFractionDigits='2' />" readonly /></td>
        <th>Discount</th>
        <td><input style="background-color:grey;" type="text" value="" readonly /></td>
    </tr>
    <tr>
        <th>Jangka Waktu</th>
        <td><input type="text" size="2" value="${ssp.MCP_INSPER}" readonly /> Tahun</td>
        <th>Lump Sump</th>
        <td><input type="text" value="<fmt:formatNumber value='${ssp.LUMP_SUM}' type='currency' currencySymbol='' minFractionDigits='2' maxFractionDigits='2' />" readonly /></td>
        <th>No Td Terima</th>
        <td><input type="text" value="${ssp.NOTTERIMA}" readonly /></td>
        <th>H. Fee</th>
        <td><input style="background-color:grey;" type="text" value="" readonly /></td>
    </tr>
    <tr>
        <th>Status Aksep</th>
        <td><input type="text" value="${ssp.MCP_STS_AKSEP}" readonly /></td>
        <th>Std</th>
        <td><input type="text" value="<c:if test='${not empty ssp.MCP_STANDARD}'><c:choose><c:when test='${ssp.MCP_STANDARD eq "0"}'>Std</c:when><c:otherwise>Non</c:otherwise></c:choose></c:if>" readonly /></td>
        <th>Flag Bill</th>
        <td><input type="text" value="<c:if test='${not empty ssp.MCP_FLAG_BILL}'><c:choose><c:when test='${ssp.MCP_FLAG_BILL eq "1"}'>Yes</c:when><c:otherwise>No</c:otherwise></c:choose></c:if>" readonly /></td>
        <th>Next Bill</th>
        <td><input type="text" value="<fmt:formatDate value='${ssp.MCP_NEXT_BILL}' pattern='dd/MM/yyyy' />" readonly /></td>
    </tr>
    <tr>
        <th>Ket Aksep</th>
        <td><input type="text" value="${ssp.MCP_KET_AKSEP}" readonly /></td>
        <th>Flag Tahapan</th>
        <td><input type="text" value="<c:if test='${not empty ssp.FLAG_BAYAR_THP}'><c:choose><c:when test='${ssp.FLAG_BAYAR_THP eq "1"}'>Yes</c:when><c:otherwise>No</c:otherwise></c:choose></c:if>" readonly /></td>
        <th>Next Thp</th>
        <td><input type="text" value="<fmt:formatDate value='${ssp.TGL_NEXT_THP}' pattern='dd/MM/yyyy' />" readonly /></td>
    </tr>
    <tr>
        <th>Posisi</th>
        <td><input type="text" value="${ssp.POSISI}" readonly /></td>
        <th>Tgl Death</th>
        <td><input type="text" value="<fmt:formatDate value='${ssp.TGL_DEATH}' pattern='dd/MM/yyyy' />" readonly /></td>
        <th>Bayar Claim</th>
        <td><input type="text" value="${ssp.BAYAR_CLAIM}" readonly /></td>
    </tr>
    <tr>
        <th>Status Polis</th>
        <td><input type="text" value="${ssp.STATUS_POLIS}" readonly /></td>
        <th>Tgl Aksep</th>
        <td><input type="text" value="<fmt:formatDate value='${ssp.MCP_TGL_AKSEP}' pattern='dd/MM/yyyy' />" readonly /></td>
        <th>Tgl Input</th>
        <td><input type="text" value="<fmt:formatDate value='${ssp.MCP_TGL_INPUT}' pattern='dd/MM/yyyy' />" readonly /></td>
    </tr>
</table>
<hr />
<table class="entry2" style="width: 98.4%;">
    <tr>
        <th>Ke</th>
        <th>Premi</th>
        <th>Discount</th>
        <th>Hfee</th>
        <th>No Batch</th>
        <th>Tgl Proses</th>
        <th>No Kwitansi</th>
        <th>Tgl Prod</th>
        <th>Tahun</th>
        <th>Reas</th>
        <th>Pos</th>
    </tr>
    <c:forEach items="${sspBill}" var="sb" varStatus="stat">
        <tr>
            <td>${sb.PAYMENT_KE}</td>
            <td><fmt:formatNumber value="${sb.JUMLAH_PREMI}" type="currency" currencySymbol="" minFractionDigits="2" maxFractionDigits="2" /></td>
            <td><fmt:formatNumber value="${sb.MCB_KOMISI_CENTURY}" type="currency" currencySymbol="" minFractionDigits="2" maxFractionDigits="2" /></td>
            <td><fmt:formatNumber value="${sb.MCB_HFEE}" type="currency" currencySymbol="" minFractionDigits="2" maxFractionDigits="2" /></td>
            <td>${sb.NO_BATCH}</td>
            <td><fmt:formatDate value="${sb.MCB_TGL_PROSES}" pattern="dd/MM/yyyy" /></td>
            <td>${sb.NO_KWITANSI}</td>
            <td><fmt:formatDate value="${sb.MCB_TGL_PROD}" pattern="dd/MM/yyyy" /></td>
            <td>${sb.MCB_TAHUN_KE}</td>
            <td><input type="checkbox" disabled <c:if test="${sb.MCB_PROSES_REAS eq '1'}">checked</c:if> /></td>
            <td>${sb.LSPD_ID}</td>
        </tr>
    </c:forEach>
</table>
</div>
</form>

</body>
<%@ include file="/include/page/footer.jsp"%>