<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<script>
			function awal(){	
				if('${cmd.daftarFormSpaj}'=='')
					setupPanes('container1', 'tab1'); 
				else
					setupPanes('container1', 'tab3'); 
			}
			
			function tampilkan(btn){
				
				if(btn.name=='searchExpress'){
					formpost.flag.value=1;
				}else if(btn.name=='searchDetail'){
					formpost.flag.value=2;
				}
				formpost.submit();
			}
			
			function pilih(msfId){
				self.opener.document.getElementById('msf_id').value=msfId;
				self.opener.document.getElementById('formpost').submit();
				window.close();
			}
			
			hideLoadingMessage();
		</script>
	</head>
<form:form commandName="cmd" id="formpost" method="post" action="${path}/bas/spaj.htm?window=cariBlanko">
	<body onload="awal();" style="height: 100%;">
				<div class="tab-container" id="container1">
					<ul class="tabs">
						<li>
							<a href="#" onClick="return showPane('pane1', this)" id="tab1">Express Search</a>
							<a href="#" onClick="return showPane('pane2', this)" id="tab2">Detail Search</a>
							<c:if test="${cmd.daftarFormSpaj ne null }">
								<a href="#" onClick="return showPane('pane3', this)" id="tab3">Show Search</a>
							</c:if>
						</li>
					</ul>
					
					<div class="tab-panes">
						<div id="pane1" class="panes">
							<fieldset>		
							<legend>Cari Blanko</legend>
								<table class="entry2">
									<tr>
										<input type="hidden" name="flag" >
										<th>
											Nomor Form <input type="text" name="msf_id" value="${cmd.msf_id}">
											<input type="button" name="searchExpress" value="search" onClick="return tampilkan(this)">
										</th>
									</tr>
								</table>
							</fieldset>
						</div>
						<div id="pane2" class="panes">
							<fieldset>		
							<legend>Cari Blanko </legend>
								<table class="entry2">
									<tr>
										<th>Nama Cabang
											<select id="lca_id" name="lca_id" >
												<option value="ALL_BRANCH">All</option>
												<c:forEach var="d" items="${cmd.daftarCabang}">
													<option <c:if test="${cmd.lca_id eq d.KEY}">selected</c:if> value="${d.KEY}">${d.VALUE}</option>
												</c:forEach>
											</select>
											Daftar Form
											<input type="radio" name="cabang" id="radio1" checked class="noborder" value="0">
											Cabang
											<input type="radio" name="cabang" id="radio2" class="noborder"  value="1">
											Agen
											</select>
											<input type="button" name="searchDetail" value="search" onClick="return tampilkan(this);">
										</th>
									</tr>
								</table>
							</fieldset>
							
						</div>
						<c:if test="${cmd.daftarFormSpaj ne null }">
						<div id="pane3" class="panes">
							<fieldset>
								<legend>Daftar Form SPAJ</legend>
								<table class="entry2">
									<tr>
										<th>Cabang</th>
										<th>Kode Agen</th>
										<th>Nama Agen</th>
										<th>Nomor Form</th>
										<th>Status</th>
									</tr>
									<c:forEach items="${cmd.daftarFormSpaj}" var="x" varStatus="xt">
										<tr onClick="pilih('${x.msf_id}')"; onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;">
											<td>${x.lca_nama}</td>
											<td>${x.msag_id}</td>
											<td>${x.mcl_first}</td>
											<td>${x.msf_id}</td>
											<td>${x.status_form}</td>
										</tr>
									</c:forEach>
								</table>
							</fieldset>
						</div>
						</c:if>
					
					</div>
					
				</div>
	

	</body>
</form:form>
</html>