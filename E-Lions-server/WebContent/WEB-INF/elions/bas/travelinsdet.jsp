<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen">
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script>
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script>
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<script type="text/javascript" src="${path }/include/js/refund/refund_common.js"></script>
		<script>
			function calculateNominals(){
				//var pilih = document.getElementById('peserta.kode_premi').selectedIndex;
				var cek = document.getElementById('kode1').checked;
				var cek2 = document.getElementById('kode2').checked;
				
				if(cek){
					document.getElementById('peserta.msid_premi_setor').value = 5000;
				}else if(cek2){
					document.getElementById('peserta.msid_premi_setor').value = 10000;
				}else{
					document.getElementById('peserta.msid_premi_setor').value = 0;
				}
				
				var prm_str	= document.getElementById('peserta.msid_premi_setor').value;
				prm_str		= prm_str.replace(/,/g, '');
				if(!isNaN(prm_str)){
					document.getElementById('peserta.msid_premi').value	= formatCurrency(prm_str * 2);
					document.getElementById('peserta.msid_up').value 	= formatCurrency(prm_str * 10000);
				}else{
					alert('Mohon masukkan nilai yang valid pada kolom PREMI DISETOR');
				}
			}
		
			function tindakan(aksi, msti_id, msti_jenis, msid_no){
				if(aksi != 'new'){
					document.getElementById('msti_id').value = msti_id;
					document.getElementById('msti_jenis').value = msti_jenis;
					document.getElementById('msid_no').value = msid_no;
				}
				document.getElementById('submitMode').value = aksi;
				document.getElementById('formpost').submit();
			}
		
			hideLoadingMessage();
		</script>
	</head>

	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Input Peserta Travel Insurance ${cmd.pesan}</a>
				</li>
			</ul>
	
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form:form commandName="cmd" id="formpost" method="post" cssStyle="text-align: left">
						<fieldset>
							<legend>Data Peserta</legend>
							<display:table id="ti" name="cmd.daftarTravelIns" class="displaytag">
								<display:column property="msid_no" title="No" style="text-align: left"/>
								<display:column title="Tindakan" style="text-align: center">
									<c:choose>
										<c:when test="${cmd.peserta.msti_posisi eq 1}">
											<img alt="Edit" src="${path}/include/image/18_imagedraw.gif" style="cursor: pointer;" onclick="tindakan('edit', ${ti.msti_id}, ${ti.msti_jenis}, ${ti.msid_no});" />
											<img alt="Delete" src="${path}/include/image/delete.gif" style="cursor: pointer;" onclick="if(confirm('Hapus data peserta ${ti.msid_nama}?')) tindakan('delete', ${ti.msti_id}, ${ti.msti_jenis}, ${ti.msid_no});" />
										</c:when>
										<c:otherwise>
											<img alt="Detail" src="${path}/include/image/18_imagedraw.gif" style="cursor: pointer;" onclick="tindakan('edit', ${ti.msti_id}, ${ti.msti_jenis}, ${ti.msid_no});" />
										</c:otherwise>
									</c:choose>
								</display:column>
								<display:column title="No. Kupon" style="text-align: center">
									${ti.lsb_code}-<fmt:formatDate value="${ti.msid_beg_date}" pattern="yy"/>-${ti.msid_blanko}
								</display:column>
								<display:column property="msid_nama" title="Nama" style="text-align: left"/>
								<display:column title="Sex" style="text-align: center">
									<c:choose>
										<c:when test="${ti.msid_sex eq 1}" >M</c:when>
										<c:when test="${ti.msid_sex eq 0}" >F</c:when>
									</c:choose>
								</display:column>
								<display:column title="Tempat / Tgl Lahir">
									${ti.msid_place_birth} / <fmt:formatDate value="${ti.msid_date_birth}" pattern="dd-MM-yyyy"/>
								</display:column>
								<display:column property="msid_beg_date" title="Beg Date" format="{0, date, dd/MM/yyyy}" style="text-align: center" />
								<display:column title="Telp Rumah" >
									${ti.msid_kd_telp_rmh} - ${ti.msid_telp_rmh}
								</display:column>
								<display:column property="msid_hp" title="Handphone" />
							</display:table>		
							<c:if test="${cmd.peserta.msti_posisi eq 1}">
								<input style="margin: 5 5 5 0" type="button" name="new" value="Input Baru" title="Input Data Baru" onclick="tindakan('new', 0, 0, 0);">
							</c:if>
						</fieldset>
						<form:hidden id="submitMode" path="submitMode"/>
						<form:hidden id="msti_id" 	 path="msti_id"/>
						<form:hidden id="msti_jenis" path="msti_jenis"/>
						<form:hidden id="msid_no" 	 path="msid_no"/>
						<c:if test="${not empty cmd.peserta}">
							<fieldset>
								<legend>Input / Edit Data Peserta</legend>
								<table class="entry2" style="width: auto">
									<tr>
										<th style="width: 150px">No Kupon:<font class="error">*</font></th>
										<th class="left" colspan="3">
											<table>
												<tr>
													<td>
														<form:input path="peserta.lsb_code" readonly="true" cssClass="readOnly" size="3" maxlength="8" /> - 
													</td>
													<td>
														<label for="kode1"><form:radiobutton cssClass="noBorder" path="peserta.kode_premi" id="kode1" value="1" cssErrorClass="inpError" onclick="calculateNominals();"/> Premi Rp. 10.000,-</label><br>
														<label for="kode2"><form:radiobutton cssClass="noBorder" path="peserta.kode_premi" id="kode2" value="2" cssErrorClass="inpError" onclick="calculateNominals();"/> Premi Rp. 20.000,-</label>
													</td>
													<td>
														 - <form:input path="peserta.msid_blanko" cssErrorClass="inpError" maxlength="6" size="7"/> 
														<span class="info">* KODE BANDARA - KODE PREMI - URUT</span>
													</td>
												</tr>
											</table>
											<%--
											<form:select path="peserta.kode_premi" title="1 untuk premi 10000, 2 untuk premi 20000" cssErrorClass="inpError" onchange="calculateNominals();">
												<form:option value="" label=""/>
												<form:option value="1" label="1"/>
												<form:option value="2" label="2"/>
											</form:select>
											--%>
										</th>
									</tr>
									<tr>
										<th>Nama Lengkap:<font class="error">*</font></th>
										<th class="left" colspan="3">
											<form:input path="peserta.msid_nama" size="37" maxlength="100" cssErrorClass="inpError"/><br/>
											<label for="pria"><form:radiobutton id="pria" path="peserta.msid_sex" cssClass="noBorder" value="1"/> Pria </label>
											<label for="wanita"><form:radiobutton id="wanita" path="peserta.msid_sex" cssClass="noBorder" value="0"/> Wanita </label>
										</th>
									</tr>
									<tr>
										<th>Tempat / Tgl Lahir:<font class="error">*</font></th>
										<th class="left" colspan="3">
											<form:input path="peserta.msid_place_birth" maxlength="50" cssErrorClass="inpError"/> / 
											<spring:bind path="cmd.peserta.msid_date_birth"><script>inputDate('${status.expression}', '${status.value}', false);</script></spring:bind>
										</th>
									</tr>
									<tr>
										<th>Alamat:</th>
										<th class="left" colspan="3">
											<form:textarea path="peserta.msid_alamat" rows="3" cols="70" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200);" cssErrorClass="inpError"/>
										</th>
									</tr>
									<tr>
										<th>Kota:</th>
										<th class="left">
											<form:input path="peserta.msid_kota" maxlength="30" size="30" cssErrorClass="inpError"/> 
										</th>
										<th>Kode Pos:</th>
										<th>
											<form:input path="peserta.msid_kd_pos" maxlength="10" size="15" cssErrorClass="inpError"/> 
										</th>
									</tr>
									<tr>
										<th>Telp Rumah:<font class="error">*</font></th>
										<th class="left">
											<form:input path="peserta.msid_kd_telp_rmh" size="4" maxlength="4" cssErrorClass="inpError"/> / 
											<form:input path="peserta.msid_telp_rmh" maxlength="15" cssErrorClass="inpError"/>
										</th>
										<th>Handphone:<font class="error">*</font></th>
										<th>
											<form:input path="peserta.msid_hp" maxlength="15" size="15" cssErrorClass="inpError"/>
										</th>
									</tr>
									<tr>
										<th>Premi Disetor:</th>
										<th class="left" colspan="3">
											Rp. <form:input path="peserta.msid_premi_setor" size="20" cssStyle="text-align: right" cssErrorClass="inpError" readonly="true" cssClass="readOnly"/>
										</th>
									</tr>
									<tr>
										<th>Premi:</th>
										<th class="left" colspan="3">
											Rp. <form:input path="peserta.msid_premi" size="20" readonly="true" cssClass="readOnly" tabindex="-1" cssStyle="text-align: right"/>
										</th>
									</tr>
									<tr>
										<th>UP:</th>
										<th class="left" colspan="3">
											Rp. <form:input path="peserta.msid_up" size="20" readonly="true" cssClass="readOnly" tabindex="-1" cssStyle="text-align: right"/>
										</th>
									</tr>
									<tr>
										<th>Masa Asuransi:</th>
										<th class="left" colspan="3">
											<%-- <spring:bind path="cmd.peserta.msid_beg_date"><script>inputhate('${status.expression}', '${status.value}', false);</script></spring:bind> --%>
											<form:input path="peserta.msid_beg_date" cssClass="readOnly" readonly="true" size="12"/>
											 s/d 
											<form:input path="peserta.msid_end_date" cssClass="readOnly" readonly="true" size="12"/>
										</th>
									</tr>
									<tr>
										<th></th>
										<td colspan="3">
											<spring:hasBindErrors name="cmd">
												<div id="error" style="text-transform: none;">
													<form:errors path="*"/>
												</div>
											</spring:hasBindErrors>
											<c:if test="${cmd.peserta.msti_posisi eq 1}">
												<input type="button" name="save" value="Simpan" title="Simpan Data" onclick="if(confirm('Simpan Data?')) tindakan('save', ${cmd.peserta.msti_id}, ${cmd.peserta.msti_jenis}, ${cmd.peserta.msid_no});">
											</c:if>
										</td>
									</tr>
								</table>
							</fieldset>
						</c:if>
					</form:form>
				</div>
			</div>
		</div>
	</body>
	<script>
		<c:if test="${not empty param.sukses}">
			alert('${param.sukses}');
		</c:if>
	</script>
</html>