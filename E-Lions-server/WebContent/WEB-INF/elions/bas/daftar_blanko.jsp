<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
	</head>

	<body style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Detail Stok Blanko SPAJ</a>
				</li>
			</ul>
			
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<c:if test="${not empty cmd.daftarBlanko}">
						<table class="displaytag">
							<thead>
								<th>Jenis</th>
								<th>Start</th>
								<th>End</th>
							</thead>
							<tbody>
								<c:forEach items="${cmd.daftarBlanko}" var="f">
									<tr>
										<td style="text-align: center; white-space: nowrap">${f.lsjs_desc}</td>
										<td style="text-align: center; white-space: nowrap">${f.start_no_blanko}</td>
										<td style="text-align: center; white-space: nowrap">${f.end_no_blanko}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</c:if>
				</div>
				
				<br/><br/>
				<input type="button" onclick="window.close();" value="Close">
				
			</div>
		</div>

	</body>
</html>