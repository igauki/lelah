<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
	<head>
		<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta NAME="Description" CONTENT="EkaLife">
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
		<script type="text/javascript" src="${path}/include/js/default.js"></script>
		<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>
		<script type="text/javascript" src="${path }/dwr/util.js"></script>	
		<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/js/jscalendar/css/calendar-win2k-1.css" media="screen"> <!-- CSS untuk DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-en.js"></script><!-- DatePicker Script (jscalendar) -->
		<script type="text/javascript" src="${path }/include/js/jscalendar/calendar-setup.js"></script><!-- DatePicker Script (jscalendar) -->
	</head>
	<script>
		function cari(){
			lca_id=formpost.lca_id.value;
			lus_id=formpost.lus_id.value;
			tgl1=formpost.tanggalAwal.value;
			tgl2=formpost.tanggalAkhir.value;
			popWin('${path}/report/bas.htm?window=outstanding_cabang&lca_id='+lca_id+'&lus_id='+lus_id+'&tanggalAwal='+tgl1+'&tanggalAkhir='+tgl2,600,800);
		}
	</script>
	<body onload="setupPanes('container1', 'tab1'); " style="height: 100%;">

		<div class="tab-container" id="container1">
			<ul class="tabs">
				<li>
					<a href="#" onClick="return showPane('pane1', this)" id="tab1">Outstanding Cabang</a>
				</li>
			</ul>
			<div class="tab-panes">
				<div id="pane1" class="panes">
					<form id="formpost" method="post">
						<table class="entry2">
							<tr>
								<th width="10%">Tanggal :</th>
								<td width="20%">
									<script>inputDate('tanggalAwal', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>
									 s/d 
									 <script>inputDate('tanggalAkhir', '<fmt:formatDate value="${tgl}" pattern="dd/MM/yyyy"/>', false);</script>
								</td>
								<td align="left" rowSpan="3"><input type="button" name="btnCari" value="Cari" onClick="cari();"></th>
							</tr>
							<tr>
								<th>Nama Cabang</th>
								<td>
									<select name="lca_id" onChange="javascript:formpost.submit();" <c:if test="${lock eq 1 }">disabled</c:if>>
										<option value="0">---Pilih---</option>
										<c:forEach items="${lsCabang}" var="x" varStatus="xt">
											<option value="${x.KEY}" 
													<c:if test="${x.KEY eq lca_id}">selected</c:if> >
													${x.VALUE}
											</option>
										</c:forEach>
								</td>
							</tr>
							<tr>
								<th>Nama Admin</th>
								<td>
									<input type="hidden" name="pil">
									<select name="lus_id" <c:if test="${lock eq 1 }">disabled</c:if>>
										<option value="0">---All---</option>
										<c:forEach items="${lsAdmin}" var="x">
											<option value="${x.KEY}" <c:if test="${x.KEY eq lus_id}">selected</c:if> >${x.VALUE}</option>
										</c:forEach>
									</select>	
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>