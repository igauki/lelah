<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	// Script khusus halaman ini
	$().ready(function() {
		
		// button icons
		$( "#btnShow3" ).button({icons: {primary: "ui-icon-search"}});
		$( "#btnShow4" ).button({icons: {primary: "ui-icon-print"}});
		var rep = '${rep}';
		if(rep==0){
			$('input:radio[name=jn_report]').attr('disabled', 'disabled'); 
			$('input:radio[name=jn_report]')[1].checked = true;
			
			//$("<option/>").val("ALL").html("ALL").appendTo("#admin2");
			//$('input:option[name=admin2]').attr('disabled', 'disabled'); 
			$("#hide").hide();
			$("#hide2").hide();
			$("#hide3").hide();
		}else if(rep==2){
			
			$("#hide2").hide();
			$("#hideC").hide();
		}
		
		// user memilih cabang, maka tampilkan admin2nya
		$("#cabang2").change(function() {
			$("#admin2").empty();
			var url = "${path}/bas/fu_premi.htm?window=json&t=admin&lca=" +$("#cabang2").val();
			$.getJSON(url, function(result) {
				$("<option/>").val("ALL").html("ALL").appendTo("#admin2");
				$.each(result, function() {
					$("<option/>").val(this.key).html('[' + this.desc + '] ' + this.value).appendTo("#admin2");
				});
			});
		});
			
		// user menekan tombol Tampilkan
		$("#btnShow3").click(function() {
				var jn = $("input:radio[name=jn_report]:checked").val();
				var tgl = $("input:radio[name=jn_tgl2]:checked").val();
										
				if(rep==2){
					jn=2;
				}
				
				if(jn==0){
					var url2 = "${path}/bas/fu_premi.htm?window=json&t=report&begdate=" +$('#begdate4').val()+ "&enddate=" +$('#enddate4').val() + "&kat=" + $('#kategori2').val() + "&stfu3=" + $('input:radio[name=stfu3]:checked').val() + '&cabang=' + $('#cabang2').val() + '&admin=' + $('#admin2').val()+'&jn_tgl='+tgl;
				}else{
					var url2 = "${path}/bas/fu_premi.htm?window=json&t=report_peruser&begdate=" +$('#begdate4').val()+ "&enddate=" +$('#enddate4').val() + "&kat=" + $('#kategori2').val() + "&stfu3=" + $('input:radio[name=stfu3]:checked').val() + '&cabang=' + $('#cabang2').val() + '&admin=' + $('#admin2').val()+'&rep='+rep+'&jn_tgl='+tgl;
				}
				
				//document.getElementById('testing').innerHTML = url2;
				
				//var url2 = "${path}/bas/fu_premi.htm?window=json&t=report&begdate=" +$('#begdate4').val()+ "&enddate=" +$('#enddate4').val() + "&kat=" + $('#kategori2').val() + "&stfu3=" + $('input:radio[name=stfu3]:checked').val() + '&cabang=' + $('#cabang2').val() + '&admin=' + $('#admin2').val();
				$.getJSON(url2, function(result) {
					$("#btnShow3").button("option", "disabled", true);
					$("#btnShow3").button("option", "label", "Silahkan tunggu...");
					$("#reportBody").empty();
					var count = 0;
					var i = 1;
					$.each(result, function() {
						var attach = '';
						if(this.msfu_attachment) attach = '<li class="ui-state-default ui-corner-all" title="Attachment" id="a' + count + '"><span class="ui-icon ui-icon-disk"></span></li></ul>';
						$('#reportTable > tbody:last').append(
								'<tr><td>' + i +'.'+
								'</td><td>' + this.msbi_beg_date + 
								'</td><td>' + this.msbi_end_date + 
								'</td><td>' + this.mspo_policy_no_format + 
								'</td><td>' + this.pemegang + 
								'</td><td>' + this.lsfu_desc +
								'</td><td>' + this.msfu_ket +
								'</td><td>' + this.status +
								'</td><td>' + this.create_date +
								'</td><td>' + this.user_name +
								'</td><td>' + '' +
								'</td><td>' + this.msbi_due_date +
								'</td><td>' + this.cara_bayar +
								'</td><td>' + this.agen +
								'</td><td>' + this.agency +
								'</td><td>' + this.jumlah_premi +
								'</td><td>' + this.jenis_tabungan +
								'</td><td>' +
									'<input type="hidden" id="tampol' + count + '" value="' + this.reg_spaj + '~' + this.msbi_tahun_ke + '`' + this.msbi_premi_ke + '">' +
									'<ul id="icons" class="ui-widget ui-helper-clearfix">' + 
									//'<li class="ui-state-default ui-corner-all" title="Detail" id="d' + count + '"><span class="ui-icon ui-icon-note"></span></li>' + 
									//'<li class="ui-state-default ui-corner-all" title="History" id="h' + count + '"><span class="ui-icon ui-icon-script"></span></li>' +
									attach +
								'</td></tr>');
						count++;
						i++;
					});
					if(count == 0){
						alert("Tidak ada data.");
					}
					$("#btnShow3").button("option", "disabled", false);				
					$("#btnShow3").button("option", "label", "Tampilkan");
				});
				
			
		});

		$('#reportTable').delegate('li', 'click', function () {
			var jenis = this.id.substring(0, 1);
			var urut = this.id.substring(1);

			if('d' == jenis){ //view detail
				
			}else if('h' == jenis){ //view history
				
			}else if('a' == jenis){ //view attachment
				window.open("${path}/bas/fu_premi.htm?window=download&fu=" + $('#tampol' + urut).val(), "", "width=800, height=600");
			}
		});
		
		// user menekan tombol Save To excell
		$("#btnShow4").click(function() {
			var jn = $("input:radio[name=jn_report]:checked").val();
			var tgl = $("input:radio[name=jn_tgl2]:checked").val();
			
			if(rep==2){
				jn=2;
			}
			
			//var test = "window=savetoexcell&t=report&begdate=" +$('#begdate4').val()+ "&enddate=" +$('#enddate4').val() + "&kat=" + $('#kategori2').val() + "&stfu3=" + $('input:radio[name=stfu3]:checked').val() + '&cabang=' + $('#cabang2').val() + '&admin=' + $('#admin2').val()+'&jn_tgl='+tgl+"&krirep=" + $('input:radio[name=krirep]:checked').val()+"   jn="+jn;
			//document.getElementById('testing').innerHTML = test;
		
			
			if(jn==0){
		//	window.open("${path}/report/bas.htm?window=report_followup_premilan&t=report&begdate=" +$('#begdate4').val()+ "&enddate=" +$('#enddate4').val() + "&kat=" + $('#kategori2').val() + "&stfu3=" + $('input:radio[name=stfu3]:checked').val() + '&cabang=' + $('#cabang2').val() + '&admin=' + $('#admin2').val()+'&jn_tgl='+tgl, "", "width=800, height=600");
// 			window.open("${path}/bas/fu_premi.htm?window=savetoexcell&t=report&begdate=" +$('#begdate4').val()+ "&enddate=" +$('#enddate4').val() + "&kat=" + $('#kategori2').val() + "&stfu3=" + $('input:radio[name=stfu3]:checked').val() + '&cabang=' + $('#cabang2').val() + '&admin=' + $('#admin2').val()+'&jn_tgl='+tgl+"&krirep=" + $('input:radio[name=krirep]:checked').val(), "", "width=800, height=600");
			window.location = "${path}/bas/fu_premi.htm?window=savetoexcell&t=report&begdate=" +$('#begdate4').val()+ "&enddate=" +$('#enddate4').val() + "&kat=" + $('#kategori2').val() + "&stfu3=" + $('input:radio[name=stfu3]:checked').val() + '&cabang=' + $('#cabang2').val() + '&admin=' + $('#admin2').val()+'&jn_tgl='+tgl+"&krirep=" + $('input:radio[name=krirep]:checked').val();
				//var url2 = "${path}/bas/fu_premi.htm?window=json&t=report&begdate=" +$('#begdate4').val()+ "&enddate=" +$('#enddate4').val() + "&kat=" + $('#kategori2').val() + "&stfu3=" + $('input:radio[name=stfu3]:checked').val() + '&cabang=' + $('#cabang2').val() + '&admin=' + $('#admin2').val()+'&jn_tgl='+tgl;
			}else{
// 			window.open("${path}/bas/fu_premi.htm?window=savetoexcell&t=report_peruser&begdate=" +$('#begdate4').val()+ "&enddate=" +$('#enddate4').val() + "&kat=" + $('#kategori2').val() + "&stfu3=" + $('input:radio[name=stfu3]:checked').val() + '&cabang=' + $('#cabang2').val() + '&admin=' + $('#admin2').val()+'&rep='+rep+'&jn_tgl='+tgl +"&krirep=" + $('input:radio[name=krirep]:checked').val(), "", "width=800, height=600");
			window.location = "${path}/bas/fu_premi.htm?window=savetoexcell&t=report_peruser&begdate=" +$('#begdate4').val()+ "&enddate=" +$('#enddate4').val() + "&kat=" + $('#kategori2').val() + "&stfu3=" + $('input:radio[name=stfu3]:checked').val() + '&cabang=' + $('#cabang2').val() + '&admin=' + $('#admin2').val()+'&rep='+rep+'&jn_tgl='+tgl +"&krirep=" + $('input:radio[name=krirep]:checked').val();
			
				//var url2 = "${path}/bas/fu_premi.htm?window=json&t=report_peruser&begdate=" +$('#begdate4').val()+ "&enddate=" +$('#enddate4').val() + "&kat=" + $('#kategori2').val() + "&stfu3=" + $('input:radio[name=stfu3]:checked').val() + '&cabang=' + $('#cabang2').val() + '&admin=' + $('#admin2').val()+'&rep='+rep+'&jn_tgl='+tgl;
			}
		});
		
		// Summary Total Follow Up
		$("#btnShow5").click(function() {
			var jn = $("input:radio[name=jn_report]:checked").val();
			var tgl = $("input:radio[name=jn_tgl2]:checked").val();
			
			if(rep==2){
				jn=2;
			}
				
			window.open("${path}/bas/fu_premi.htm?window=summary&jn_tgl="+tgl+"&begdate=" +$('#begdate4').val()+ "&enddate=" +$('#enddate4').val()+"&admin=" + $('#admin2').val()+"&rep="+rep +"&stfu3=" + $('input:radio[name=stfu3]:checked').val() + "&kat=" + $('#kategori2').val()  , "", "width=600, height=350");
			
		});

	});
</script>

<form id="formPost3" name="formPost3" method="post">
<input id="submitType3" name="submitType3" type="hidden">
<input id="rep" name="rep" type="hidden" value="${rep }">

<fieldset class="ui-widget ui-widget-content">
	<legend class="ui-widget-header ui-corner-all"><div>Summary Follow Up Premi Lanjutan</div></legend>
	
	<div class="rowElem" id="hide2">
		<label>Report</label>
		<input type="radio" name="jn_report" id="jn_report0" value="0" checked="checked"/>PerAdmin
		<input type="radio" name="jn_report" id="jn_report1" value="1" />PerUser
	</div>
		
	<div class="rowElem" id="hideC">
		<label>Cabang</label>
		<select name="cabang2" id="cabang2" title="Silahkan pilih Cabang">
			<option>-- Silahkan pilih Cabang --</option>
			<%-- <option value="ALL">ALL</option> disabled karena lama banget --%>
			<c:forEach var="c" items="${listCab}" varStatus="s">
				<option value="${c.key}">${c.value}</option>
			</c:forEach>
		</select>
	</div>
	
	<div id="testing">
	
	</div>
	
	<div class="rowElem" id="hide">
		<label>Admin/User</label>
		<select name="admin2" id="admin2" title="Silahkan pilih Admin">
			<option value="ALL">ALL</option>
			<c:forEach var="c" items="${listAdmin}" varStatus="s">
				<option value="${c.key}">${c.value}</option>
			</c:forEach>
		</select>
	</div>
	
	<div class="rowElem">
		<label>Kategori</label>
		<select name="kategori2" id="kategori2" title="Silahkan pilih kategori">
			<option value="ALL">ALL</option>
			<c:forEach var="k" items="${listKat2}" varStatus="s">
				<option value="${k.key}">${k.value}</option>
			</c:forEach>
		</select>
	</div>
	
	<div class="rowElem">
		<label>Tanggal:</label>
		<input type="radio" name="jn_tgl2" id="jn_tgl20" value="0" checked="checked"/>Tgl Tagihan
		<input type="radio" name="jn_tgl2" id="jn_tgl21" value="1" />Tgl Followup
<!-- 		<input type="radio" name="jn_tgl2" id="jn_tgl22" value="2" />Tgl Jatuh Tempo -->
	</div>
	<div class="rowElem">
		<label>&nbsp;</label>
		<input name="begdate4" id="begdate4" type="text" class="datepicker" title="Awal" value="${begdate}"> s/d 
		<input name="enddate4" id="enddate4" type="text" class="datepicker" title="Akhir" value="${enddate}">
	</div>
	
	<div class="rowElem">
		<label>Status Follow Up:</label>
		<c:forEach var="f" items="${listStatus2}" varStatus="s">
			<input type="radio" id="radioSFU${s.index}" name="stfu3" value="${f.key}" title="${f.value}"
				<c:if test="${f.key eq 2}">checked="checked"</c:if>
			><label class="radioLabel" for="radioSFU${s.index}">${f.value}</label>
		</c:forEach>
	</div>
	
	<div class="rowElem">
		<label>Kriteria Report (Show Excel Only)  : </label>
		<c:forEach var="f" items="${listKriteriaRep}" varStatus="s">
			<input type="radio" id="radiokrirep${s.index}" name="krirep" value="${f.key}" title="${f.value}"
				<c:if test="${f.key eq 0}">checked="checked"</c:if>
			><label class="radioLabel" for="radiokrirep${s.index}">${f.value}</label>
		</c:forEach>
	</div>

	<div class="rowElem">
		<label></label>
		<button id="btnShow3" title="Tampilkan data" onclick="return false;">Tampilkan</button>
		<button id="btnShow4" title="Save To Excel" onclick="return false;">Show (Excell)</button>
		<button id="btnShow5" title="Total Follow Up" onclick="return false;">Total Follow Up</button>
	</div>
</fieldset>

<fieldset id="fieldsetReport" class="ui-widget ui-widget-content">

	<table id="reportTable" class="jtable" width="100%">
		<thead>
			<tr>
				<th>No</th>
				<th>Tgl Tagihan</th>
				<th>Tgl Jatuh Tempo</th>
				<th>No. Polis</th>
				<th>Pemegang</th>
				<th>Kategori</th>
				<th>Keterangan</th>
				<th>Status</th>
				<th>Tgl Follow Up</th>
				<th>User</th>
				<th>Attachment</th>
				<th>Due Date</th>
				<th>Cara Bayar</th>
				<th>Nama Agen Penutup</th>
				<th>Agency Penutup</th>
				<th>Jumlah Premi</th>
				<th>Jenis Tabungan</th>
			</tr>
		</thead>
		<tbody id="reportBody">
		</tbody>
	</table>
	
</fieldset>

</form>