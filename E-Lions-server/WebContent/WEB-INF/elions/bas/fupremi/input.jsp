<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<link type="text/css" href="${path}/include/jquery/stopwatch/jquery.stopwatch.css" rel="stylesheet" />
<script type="text/javascript" src="${path}/include/jquery/stopwatch/jquery.stopwatch.js"></script>
<script type="text/javascript">
	// Script khusus halaman ini
	$(document).ready(function() {
		
		//stopwatch
		$('#stopwatch-1').stopwatch();
	 
		$('.stop').click(function(e) {
			var hr = $('.hr').text();
			var min = $('.min').text();
			var sec = $('.sec').text();
			var lama = hr+":"+min+":"+sec;
			
			$("#waktu").val(lama);
			
		});
		
		$('.reset').click(function(e) {
			$("#waktu").val("00:00:00");
		});
		
		// button icons
		$( "#btnShow" ).button({icons: {primary: "ui-icon-search"}});
		$( "#btnShowExcel" ).button({icons: {primary: "ui-icon-print"}});
		//$( "#btnSave" ).button({icons: {primary: "ui-icon-disk"}});
		//$( "#btnEmail" ).button({icons: {primary: "ui-icon-mail-closed"}});
		$( "#btnDetail" ).button({icons: {primary: "ui-icon-note"}});
		$( "#btnHistory" ).button({icons: {primary: "ui-icon-script"}});
		$( "#btnAutoDebet" ).button({icons: {primary: "ui-icon-note"}});
		
		// user menekan tombol Tampilkan
		
		$("#followup4").empty();
		$("#btnShow").click(function() {
			$("#btnReset").click();
			
			if($("input:radio[name=pilih]:checked").val() == 0) {
				var url = "${path}/bas/fu_premi.htm?window=json&t=followup&jenis=" +$('input:radio[name=jenong]:checked').val()+ "&begdate=" +$('#begdate').val()+ "&enddate=" +$('#enddate').val() + "&stfu=" + $('input:radio[name=stfu]:checked').val() + "&jn=0&no_polis="+$('#no_polis').val();
			}else if($("input:radio[name=pilih]:checked").val() == 1){
				var url = "${path}/bas/fu_premi.htm?window=json&t=followup&jenis=" +$('input:radio[name=jenong]:checked').val()+ "&aging=" + $("input:radio[name=ra]:checked").val() + "&stfu=" + $('input:radio[name=stfu]:checked').val()+"&no_polis="+$('#no_polis').val();
			}else{
				var url = "${path}/bas/fu_premi.htm?window=json&t=followup&jenis=" +$('input:radio[name=jenong]:checked').val()+ "&begdate=" +$('#begdatejt').val()+ "&enddate=" +$('#enddatejt').val() + "&stfu=" + $('input:radio[name=stfu]:checked').val() + "&jn=1&no_polis="+$('#no_polis').val();
			}
			
			//tambahan
			$("#followup2").val($('#followup').val());
			$("#begdate2").val($('#begdate').val());
			$("#enddate2").val($('#enddate').val());
			$("#aging2").val($('input[name=ra]:radio:checked').val());
			$("#status2").val($('input[name=stfu]:radio:checked').val());
			$("#jenis2").val($('input[name=jenong]:radio:checked').val());
			$("#pilih2").val($('input[name=pilih]:radio:checked').val());
			$("#followup3").val($("#followup option:selected").text());
			
			$("#deskripsiPolis").hide();			
			$.getJSON(url, function(result) {
				$("#btnShow").button("option", "disabled", true);
				$("#btnShow").button("option", "label", "Silahkan tunggu...");
				var options = $("#followup");
				var count = 0;
				options.empty();
				$.each(result, function() {
					count++;
					
					//Req Edi Sartana, 12/01/12, Tdk sanggup bayar/tdk bisa dihub kasih warna merah!
					//4 dan 7, warna merah
					//FIXME belum nih !!!!!!!!!!!!!!!!!!
					
					var warna = "";
					if(this.lsfu_id == 4 || this.lsfu_id == 7) warna = "style=\"color:red;\"";
					
				    options.append($("<option " +warna+ "/>")
				    		.val(this.reg_spaj + "~" + this.msbi_tahun_ke + "`" + this.msbi_premi_ke)
				    		.text(count + ". " + this.msbi_beg_date + " | " + this.mspo_policy_no_format));
				    		//.text(this.msbi_beg_date + " | " + this.msbi_due_date + " | " + this.mspo_policy_no_format));
				});

				if(count == 0){
					alert("Tidak ada data.");
					$("#fieldsetInput").fadeOut();
				}else{
					$("#fieldsetInput").fadeIn();
				}
				$("#btnShow").button("option", "disabled", false);
				$("#btnShow").button("option", "label", "Tampilkan");
			});
		});
		
		// user menekan tombol Save To excell
		$("#btnShowExcel").click(function() {
			
				window.open("${path}/bas/fu_premi.htm?window=savetoexcell&t=reportFUPL&jenis=" +$('input:radio[name=jenong]:checked').val()+ "&begdate=" +$('#begdate').val()+ "&enddate=" +$('#enddate').val() + "&stfu=" + $('input:radio[name=stfu]:checked').val() + "&jn=0&no_polis="+$('#no_polis').val(), "", "width=800, height=600");
			
		});
		
		// user memilih salah satu polis dari dropdown, maka tampilkan detailnya
		$("#followup").change(function() {
			var fv = $("#followup").val();
			$("#btnReset").click();
			$("#followup").val(fv);
			
			if($("#followup").val()==null || $("#followup").val()==""){
				alert("Terjadi kesalahan pada polis ini. Mohon refresh halaman ini");
				return;
			}
			//tambahan
			$("#followup2").val($('#followup').val());
			$("#begdate2").val($('#begdate').val());
			$("#enddate2").val($('#enddate').val());
			$("#begdate3").val($('#begdatejt').val());
			$("#enddate3").val($('#enddatejt').val());
			$("#aging2").val($('input[name=ra]:radio:checked').val());
			$("#status2").val($('input[name=stfu]:radio:checked').val());
			$("#jenis2").val($('input[name=jenong]:radio:checked').val());
			$("#pilih2").val($('input[name=pilih]:radio:checked').val());
			$("#followup3").val($("#followup option:selected").text());
			$("#followup4").empty();
			
			//1. tarik data detail
			
			$("#vacc").empty();//clear nilai vacc
			$("#jen_kel").empty();//clear nilai jenis kelamin
			$("#e_mail").empty();//clear nilai e-mail
			var url = "${path}/bas/fu_premi.htm?window=json&t=detail&fu=" +$("#followup").val();
			$.getJSON(url, function(result) {
				$.each(result, function() {
				var alamat2 = '';
				if(this.alamat2 != null){
					alamat2 = this.alamat2;
				}
					$("#lssp_status").val(this.lssp_status);
					$("#title").val(this.title);
					$("#pemegang").val(this.pemegang);
					$("#alamat").val(this.alamat1 + "\n" + alamat2);
					$("#telp").val(this.telp1 + " | " + this.telp2 + " | " + this.no_hp + " | " + this.no_hp2);
					
					$("#telp1").val(this.telp1);
					$("#telp2").val(this.telp2);
					$("#telp3").val(this.no_hp);
					$("#telp4").val(this.no_hp2);
					
					
					$("#tgl_tagihan").val(this.msbi_beg_date + " s/d " + this.msbi_end_date);
					$("#premi").val(this.kurs + " " + (this.premi).formatMoney(0, '.', ','));
					
					//topup
					$("#topup").val(this.kurs + " " + (this.topup).formatMoney(0, '.', ','));
					$("#jumlah").val(this.kurs + " " + (this.premi+this.topup).formatMoney(0, '.', ','));
					
					$("#tertanggung").html(this.title_tertanggung+" "+this.tertanggung);
					$("#produk").html(this.produk);
					
					$("#nm_produk").val(this.produk);
					
					$("#periode").html(this.beg_date + " s/d " + this.end_date);
					$("#pay_mode").html(this.pay_mode);
					$("#vacc").html(this.vacc);
					$("#pay_period").html(this.pay_period + " Thn");
					$("#tsi").html(this.kurs + " " + (this.tsi).formatMoney(0, '.', ','));
					
					$("#jen_kel").val(this.jenkel);
					$("#e_mail").val(this.email);
					
					$("#regional").val(this.lca_id+this.lwk_id+"."+this.lsrg_id+" ["+this.lsrg_nama+" / "+this.lwk_nama+" / "+this.lca_nama+"]");
					
					$("#spajThnPremi").val($("#followup").val());
					
					$("#deskripsiPolis").slideDown();
					$("#tglKategori").hide();
				});
			});
			
			//2. tarik history payment
			$("#paymentBody").empty();
			var url2 = "${path}/bas/fu_premi.htm?window=json&t=payment&fu=" +$("#followup").val();
			$.getJSON(url2, function(result) {
				$.each(result, function() {
					$("#paymentTable > tbody:last").append(
							"<tr><td>" + this.tahun_ke + 
							"</td><td>" + this.premi_ke + 
							"</td><td>" + this.beg_date + 
							"</td><td>" + this.end_date + 
							"</td><td>" + (this.premi).formatMoney(0, '.', ',') + 
							"</td><td>" + this.tgl_rk + 
							"</td><td>" + this.tgl_payment + 
							"</td><td>" + this.paid + 
							"</td><td>" + this.status_paid + "</td></tr>");
				});
			});
			
			//3. tarik history followup
			$("#histBody").empty();
			var url3 = "${path}/bas/fu_premi.htm?window=json&t=hist&fu=" +$("#followup").val();
			$.getJSON(url3, function(result) {
				$("#msfu_status").val("Outstanding");
				$.each(result, function() {
					$("#histTable > tbody:last").append(
							"<tr><td>" + this.msfu_urut + 
							"</td><td>" + this.create_date + 
							"</td><td>" + this.user_name + 
							"</td><td>" + this.status + 
							"</td><td>" + this.lsfu_desc + 
							"</td><td>" + this.msfu_ket + "</td></tr>");
					if(this.lsfu_id != 99) $("#msfu_status").val(this.status);
				});
				
				if($("#msfu_status").val() == "Closed") {
					$("#btnSave").fadeOut();
					$("#btnEmail").fadeOut();
				}else{
					$("#btnSave").fadeIn();
					$("#btnEmail").fadeIn();
				}
			});

			//4. tarik info tahapan (bila ada)
			$("#tahapan").val("Tidak");
			var url3 = "${path}/bas/fu_premi.htm?window=json&t=tahapan&fu=" +$("#followup").val();
			$.getJSON(url3, function(result) {
				$.each(result, function() {
					$("#tahapan").val("Ya (" + this.key + ")");
				});
			});
			
			//5. tarik view auto debet
			$("#autoBody").empty();
			$("#sedang").empty();
			$("#total_sedang").empty();
			
			$("#gagal").empty();
			$("#total_gagal").empty();
			
			$("#berhasil").empty();
			$("#total_berhasil").empty();
			var url2 = "${path}/bas/fu_premi.htm?window=json&t=autodebet&fu=" +$("#followup").val();
			$.getJSON(url2, function(result) {
				$.each(result, function() {
					/* $("#sedang").html("("+this.SDG+")");
					$("#total_sedang").html(this.TOT_SDG); */
					
					$("#gagal").html("("+this.GAL+")");
					$("#total_gagal").html(this.TOT_GAL);
					
					$("#berhasil").html("("+this.NYOSS+")");
					$("#total_berhasil").html(this.TOT_NYOS);
					
					$("#autoTable > tbody:last").append(
							"<tr><td>" + this.NO + 
							"</td><td>" + this.TGL_TAGIHAN + 
							"</td><td>" + this.TGL_TERIMA + 
							"</td><td>" + this.JENIS + 
							"</td><td>" + this.LSBP_NAMA + 
							"</td><td>" + this.NO_REKENING + 
							"</td><td><center>" + this.TAHUN + 
							"</center></td><td>" + this.KE + 
							"</td><td>" + this.FREQ + 
							"</td><td>" + (this.TOTAL_INVOICE).formatMoney(0, '.', ',') + 
							"</td><td>" + this.KET + "</td></tr>");
				});
			});
			
			//6. tarik view billing
			$("#billingBody").empty();
			var urlbilling = "${path}/bas/fu_premi.htm?window=json&t=billing&fu=" +$("#followup").val();
			$.getJSON(urlbilling, function(resultBilling) {
				$.each(resultBilling, function() {
					
					$("#billingTable > tbody:last").append(
							"<tr><td>" + this.MSBI_PAID + 
							"</td><td><center>" + this.MSBI_TAHUN_KE + 
							"</center></td><td><center>" + this.MSBI_PREMI_KE + 
							"</center></td><td>" + this.MSBI_BEG_DATE + 
							"</td><td>" + this.MSBI_END_DATE + 
							"</td><td>" + this.MSBI_DUE_DATE + 
							"</td><td>" + this.LKU_ID + 
							"</td><td>" + this.MSPRO_NILAI_KURS+ 
							"</td><td>" + (this.PREMI_STD).formatMoney(0, '.', ',') + 
							"</td><td>" + (this.PREMI_RIDER).formatMoney(0, '.', ',') + 
							"</td><td>" + (this.PREMI_EXTRA).formatMoney(0, '.', ',') + 
							"</td><td>" + (this.PREMI_HCR).formatMoney(0, '.', ',') + 
							"</td><td>" + (this.DISCOUNT).formatMoney(0, '.', ',') +
							"</td><td>" + (this.MSBI_STAMP).formatMoney(0, '.', ',') +  
							"</td><td>" + (this.TOTAL_TAGIH).formatMoney(0, '.', ',') + "</td></tr>");
				});
			});
			
			//7. tarik view tahapan
			$("#tahapanBody").empty();
			var urltahapan = "${path}/bas/fu_premi.htm?window=json&t=viewtahapan&fu=" +$("#followup").val();
			$.getJSON(urltahapan, function(resultTahapan) {
				$.each(resultTahapan, function() {
					$("#tahapanTable > tbody:last").append(
							"<tr><td><center>" + this.MSTAH_TAHAPAN_KE + 
							"</center></td><td>" + this.MSTAH_NO_TAHAPAN + 
							"</td><td>" + this.MSTAH_BEG_ACTIVE + 
							"</td><td>" + (this.MSTAH_JUMLAH).formatMoney(0, '.', ',') + 
							"</td><td>" + this.MSTAH_PERSEN +" %"+ 
							"</td><td>" + this.MSTAH_JT_TEMPO + 
							"</td><td>" + this.MSTAH_TGL_PROSES + 
							"</td><td>" + this.MSTAH_TGL_PRINT + 
							"</td><td>" + this.MSTAH_TGL_KONFIRMASI + 
							"</td><td>" + this.MSTAH_TGL_TRANS + 
							"</td><td>" + this.LSJTA + 
							"</td><td>" + (this.MSBAT_JUMLAH).formatMoney(0, '.', ',') +  
							"</td><td>" + this.MSTAH_TGL_BAYAR + "</td></tr>");
				});
			});
			
			//8. tarik view simpanan
			$("#simpananBody").empty();
			var urlsimpanan = "${path}/bas/fu_premi.htm?window=json&t=viewsimpanan&fu=" +$("#followup").val();
			$.getJSON(urlsimpanan, function(resultSimpanan) {
				$.each(resultSimpanan, function() {
					$("#simpananTable > tbody:last").append(
							"<tr><td><center>" + this.MSSIM_NUMBER + 
							"</center></td><td>" + this.MSSIM_TGL_INPUT + 
							"</td><td>" + this.MSSIM_BEG_DATE + 
							"</td><td>" + this.MSTAH_NO_TAHAPAN + 
							"</td><td>" + this.MSSIM_NO_DEPOSITO + 
							"</td><td>" + (this.MSSIM_JUMLAH).formatMoney(0, '.', ',') + 
							"</td><td>" + this.MSSIM_PERSEN +
							"</td><td>" + this.BUNGA + 
							"</td><td>" + this.MSSIM_BEG_DATE + 
							"</td><td>" + this.MSSIM_END_DATE + 
							"</td><td>" + (this.MBS_JUMLAH).formatMoney(0, '.', ',') +  
							"</td><td>" + this.MSSIM_TGL_INPUT +  
							"</td><td>" + no_reg(this.MSSIM_NO_REG,this.MSSIM_FLAG) +  
							"</td><td>" + this.MSSIM_TGL_KLAIM +
							"</td><td>" + this.MSSIM_TGL_TRANS +    
							"</td><td>" + this.LSCSI_ID + "</td></tr>");
				});
			});
			
			//9. tarik call summary
			$("#callsummaryBody").empty();
			var urlcallsummary = "${path}/bas/fu_premi.htm?window=json&t=viewcallsummary&fu=" +$("#followup").val();
			$.getJSON(urlcallsummary, function(resultCallSummary) {
				$.each(resultCallSummary, function() {
					
					$("#callsummaryTable > tbody:last").append(
							"<tr><td><center>" + this.NO + 
							"</center></td><td>" + this.FLAG_INOUT + 
							"</td><td>" + this.USER_NAME + 
							"</td><td>" + this.LCA_ID + 
							"</td><td>" + this.LSCSF_NAMA + 
							"</td><td>" + this.MSCSF_BEG_TGL_DIAL + 
							"</td><td>" + this.MSCSF_END_TGL_DIAL +
							"</td><td>" + this.MSCSF_TGL_BACK + 
							"</td><td>" + "\""+this.MSCSF_KET +"\""+ 
							"</td><td>" + this.MSCSF_OCR +    
							"</td><td>" + this.MSCSF_DIR + "</td></tr>");
				});
			});
			
			//10. tarik Detail Power Save
			$("#detailpowersaveBody").empty();
			var urldetailpowersave = "${path}/bas/fu_premi.htm?window=json&t=detailpowersave&fu=" +$("#followup").val();
			$.getJSON(urldetailpowersave, function(resultDetailPowerSave) {
				$.each(resultDetailPowerSave, function() {
					
					$("#detailpowersaveTable > tbody:last").append(
							"<tr><td><center>" + this.PERIODE + 
							"</center></td><td>" + this.MATURITY_DATE + 
							"</td><td><center>" + this.RO_KE + 
							"</center></td><td><center>" + this.MGI +" Bln"+ 
							"</center></td><td><center>" + this.JENIS_RATE + 
							"</center></td><td><center>" + this.RATE +
							"</center></td><td><center>" + this.KURS + 
							"</center></td><td>" + this.PREMI_DEPOSIT +
							"</td><td>" + this.PREMI_INTEREST +
							"</td><td>" + this.PREMI_RIDER +
							"</td><td>" + this.PREMI_DEBIT +    
							"</td><td>" + this.PREMI_INSURANCE +
							"</td><td>" + this.PREMI_TAX +
							"</td><td>" + this.PREMI_DIBAYAR + "</td></tr>");
				});
			});
			
			//11. tarik Detail Stable Link
			$("#detailstablelinkBody").empty();
			var urldetailstablelink = "${path}/bas/fu_premi.htm?window=json&t=detailstablelink&fu=" +$("#followup").val();
			$.getJSON(urldetailstablelink, function(resultDetailStableLink) {
				$.each(resultDetailStableLink, function() {
					
					$("#detailstablelinkTable > tbody:last").append(
							"<tr><td><center>" + this.PERIODE + 
							"</center></td><td>" + this.DESCRIPTION + 
							"</td><td><center>" + this.RO_KE + 
							"</center></td><td><center>" + this.KURS + 
							"</center></td><td>" + this.PREMI + 
							"</td><td>" + this.INTEREST + 
							"</td><td>" + this.PREMI_BUNGA +
							"</td><td><center>" + this.MTI + 
							"</center></td><td><center>" + this.ROLL_OVER +
							"</center></td><td>" + this.PAID_DATE +
							"</td><td>" + this.NAB + "</td></tr>");
				});
			});
			
			$('textarea[maxlength]').live('keyup change', function() {
				var str = $(this).val();
				var mx = parseInt($(this).attr('maxlength'));
				if (str.length > mx) {
					$(this).val(str.substr(0, mx));
					return false;
				}
			});
			
		});	
		
		//Bila Kategori follow up : nasabah berjanji akan melakukan pembayaran, munculkan inputan tanggal reminder.
		// bila user merubah kategori, tarik data email dan status closed nya
		$("#kategori").change(function() {
			$("#emailto").empty();
			$("#lsfustatus").empty();
			var url = "${path}/bas/fu_premi.htm?window=json&t=kat&kat=" +$("#kategori").val()+"&fu=" +$("#followup").val();
			$.getJSON(url, function(result) {
				$.each(result, function() {
					$("#emailto").val(this.key);
					$("#lsfustatus").val(this.value);
				});
			});
			
			var flagKategori = $("#kategori").val();
			if(flagKategori == 5 || flagKategori == 11){
				$("#tglKategori").show();
			}else{
				$("#tglKategori").hide();
			}
		});		

		// user menekan tombol View Detail Polis
		$("#btnDetail").qtip({
			style: {
				classes: 'ui-tooltip-light ui-tooltip-rounded',
				width: 800
			},
			content: {
    			text: $("#detailPolis"),
    			title: {
       				text: 'Detail Polis',
       				button: true}
			},
			position: {
				my: 'center', // ...at the center of the viewport
				at: 'center',
				target: $(window),
				adjust: {scroll: false}
			},
			show: {
				event: 'click', // Show it on click...
				solo: true, // ...and hide all other tooltips...
				modal: true // ...and make it modal
			},
			hide: false
		});
		
		// user menekan tombol View History Followup
		$("#btnHistory").qtip({
			style: {
				classes: 'ui-tooltip-light ui-tooltip-rounded',
				width: 800
			},
			content: {
    			text: $("#historyFollowup"),
    			title: {
       				text: 'History Followup',
       				button: true}
			},
			position: {
				my: 'center', // ...at the center of the viewport
				at: 'center',
				target: $(window),
				adjust: {scroll: false}
			},
			show: {
				event: 'click', // Show it on click...
				solo: true, // ...and hide all other tooltips...
				modal: true // ...and make it modal
			},
			hide: false
		});
		
		// user menekan tombol View Auto Debet
		$("#btnAutoDebet").qtip({
			style: {
				classes: 'ui-tooltip-light ui-tooltip-rounded',
				width: 900
			},
			content: {
    			text: $("#viewAutoDebet"),
    			title: {
       				text: 'View Auto Debet',
       				button: true}
			},
			position: {
				my: 'center', // ...at the center of the viewport
				at: 'center',
				target: $(window),
				adjust: {scroll: false}
			},
			show: {
				event: 'click', // Show it on click...
				solo: true, // ...and hide all other tooltips...
				modal: true // ...and make it modal
			},
			hide: false
		});
		
		// user menekan tombol View Billing
		$("#btnBilling").qtip({
			style: {
				classes: 'ui-tooltip-light ui-tooltip-rounded',
				width: 900
			},
			content: {
    			text: $("#viewBilling"),
    			title: {
       				text: 'View Billing',
       				button: true}
			},
			position: {
				my: 'center', // ...at the center of the viewport
				at: 'center',
				target: $(window),
				adjust: {scroll: false}
			},
			show: {
				event: 'click', // Show it on click...
				solo: true, // ...and hide all other tooltips...
				modal: true // ...and make it modal
			},
			hide: false
		});
		
		// user menekan tombol View Call Summary
		$("#btnCallSummary").qtip({
			style: {
				classes: 'ui-tooltip-light ui-tooltip-rounded',
				width: 900
			},
			content: {
    			text: $("#viewCallSummary"),
    			title: {
       				text: 'View Call Summary',
       				button: true}
			},
			position: {
				my: 'center', // ...at the center of the viewport
				at: 'center',
				target: $(window),
				adjust: {scroll: false}
			},
			show: {
				event: 'click', // Show it on click...
				solo: true, // ...and hide all other tooltips...
				modal: true // ...and make it modal
			},
			hide: false
		});
		
		// user menekan tombol View Detail Power Save
		$("#btnDetailPowerSave").qtip({
			style: {
				classes: 'ui-tooltip-light ui-tooltip-rounded',
				width: 900
			},
			content: {
    			text: $("#viewDetailPowerSave"),
    			title: {
       				text: 'View Detail Power Save',
       				button: true}
			},
			position: {
				my: 'center', // ...at the center of the viewport
				at: 'center',
				target: $(window),
				adjust: {scroll: false}
			},
			show: {
				event: 'click', // Show it on click...
				solo: true, // ...and hide all other tooltips...
				modal: true // ...and make it modal
			},
			hide: false
		});
		
		// user menekan tombol View Detail Stable Link
		$("#btnDetailStableLink").qtip({
			style: {
				classes: 'ui-tooltip-light ui-tooltip-rounded',
				width: 900
			},
			content: {
    			text: $("#viewDetailStableLink"),
    			title: {
       				text: 'View Detail Stable Link',
       				button: true}
			},
			position: {
				my: 'center', // ...at the center of the viewport
				at: 'center',
				target: $(window),
				adjust: {scroll: false}
			},
			show: {
				event: 'click', // Show it on click...
				solo: true, // ...and hide all other tooltips...
				modal: true // ...and make it modal
			},
			hide: false
		});
		
		// user menekan tombol submit (ada 2 macam, save biasa dan save+email)
		$("#btnSave").click(function() {
			if(confirm("Simpan Followup?")){
				$("#submitType").val(1);
				//$("#formPost").submit();
			}else{
				return false;
			}
		});
		$("#btnEmail").click(function() {
			if(confirm("Simpan Followup sekaligus email ke bagian terkait?")){
				$("#submitType").val(2);
			
				if($("#lsfustatus").val() == 1){
					if(confirm("Update Status menjadi CLOSED?\nPilih OK bila YA, pilih CANCEL bila TIDAK")){
						$("#lsfustatus").val(1);
					}else{
						$("#lsfustatus").val(0);
					}
				}				
				//$("#formPost").submit();
			}else{
				return false;
			}
		});
		
		$("#btnEmailNasabah").click(function() {
			if(confirm("Kirim E-mail ke nasabah?")){
				$("#submitType").val(3);
				
				if($("#e_mail").val()=='' || $("#e_mail").val()==null){
					alert("email tidak ada");
					return false;
				}else{
					return true;
				}
			}else{
				return false;
			}
		});
		
		// onclick untuk pilihan penarikan tanggal berdasarkan tgl billing atau berdasarkan aging
		$("#begdatejt").attr("disabled", true);
		$("#enddatejt").attr("disabled", true);
		$("input[name='pilih']").click(function() {
			if(this.value == 0){ //berdasarkan tgl billing
				$("#begdate").attr("disabled", false);
				$("#enddate").attr("disabled", false);
				$("#begdatejt").attr("disabled", true);
				$("#enddatejt").attr("disabled", true);
				$("input[name='ra']").attr("disabled", true);
			}else if(this.value == 1){ //berdasarkan tgl aging
				$("#begdate").attr("disabled", true);
				$("#enddate").attr("disabled", true);
				$("#begdatejt").attr("disabled", true);
				$("#enddatejt").attr("disabled", true);
				$("input[name='ra']").attr("disabled", false);
			}else{
				$("#begdate").attr("disabled", true);
				$("#enddate").attr("disabled", true);
				$("#begdatejt").attr("disabled", false);
				$("#enddatejt").attr("disabled", false);
				$("input[name='ra']").attr("disabled", true);
			}
		});		
		
		// hide dulu bagian detail bawah + bagian deskripsi polis
		$("#fieldsetInput").hide();
		$("#deskripsiPolis").hide();
		
		// (jQuery Validation Plugin) client-side validation untuk formPost on keyup and on submit
		$.validator.setDefaults({
			highlight: function(input) {
				$(input).addClass("ui-state-error");
			},
			unhighlight: function(input) {
				$(input).removeClass("ui-state-error");
			}
		});
		
		// validasi
		$("#formPost").validate({
			rules: {
				kategori: "required",
				keterangan: "required",
				remindDate: ($("#kategori").val()==5 || $("#kategori").val()==11) ? "required" : "notrequired"
			}
		});
	
		// (jQuery Form Plugin) rubah form jadi ajax
		var options = {
			success : showResponse
		};
		$("#formPost").ajaxForm(options); // bind form using "ajaxForm"
	
		function showResponse(responseText, statusText, xhr, $form) { // post-submit callback 
		    alert(responseText);
			//tarik ulang history followup
			$("#histBody").empty();
			var url3 = "${path}/bas/fu_premi.htm?window=json&t=hist&fu=" +$("#followup").val();
			$.getJSON(url3, function(result) {
				$("#msfu_status").val("Outstanding");
				$.each(result, function() {
					$("#histTable > tbody:last").append(
							"<tr><td>" + this.msfu_urut + 
							"</td><td>" + this.create_date + 
							"</td><td>" + this.user_name + 
							"</td><td>" + this.status + 
							"</td><td>" + this.lsfu_desc + 
							"</td><td>" + this.msfu_ket + "</td></tr>");
					$("#msfu_status").val(this.status);
				});
				
				if($("#msfu_status").val() == "Closed") {
					$("#btnSave").fadeOut();
					$("#btnEmail").fadeOut();
				}else{
					$("#btnSave").fadeIn();
					$("#btnEmail").fadeIn();
				}				
			});
		    
		}		
		
		$("#formPost").bind("submit",function(){
			var submitType = 2;
			var fu = $("#spajThnPremi").val();
			var kategori = $("#kategori").val();
			var lsfustatus = $("#lsfustatus").val();
			var keterangan = $("#keterangan").val();
			var emailto = $("#emailto").val();
			var pemegang = $("#pemegang").val();
			var remindDate = $("#remindDate").val();
			
		});
		
		//batas 200 char
		$("#keterangan").keypress(function(e) {
		    var tval = $("#keterangan").val(),
		        tlength = tval.length,
		        set = 500,
		        remain = parseInt(set - tlength);
		    if (remain <= 0 && e.which !== 0 && e.charCode !== 0) {
		        $("#keterangan").val((tval).substring(0, tlength - 1))
		    }
		})
		
		//test
		var mode = '${mode}';
		if(mode==1){
			var pesan = '${pesan}';
			var pilih2 = '${pilih2}';
			var aging2 = '${aging2}';
			var status2 = '${status2}';
			var jenis2 = '${jenis2}';
			var followup2 = '${followup2}';
			
			var polis = '${followup3}';
			$("#followup4").html("Deskripsi data polis saat ini untuk polis : <strong>"+polis.substring(polis.indexOf(" "), polis.length)+"</strong>");
			
			if(pilih2==0){
				$('input:radio[name=pilih]')[0].checked = true;
				$("#begdatejt").attr("disabled", true);
				$("#enddatejt").attr("disabled", true);
				$("#begdate").attr("disabled", false);
				$("#enddate").attr("disabled", false);
			}
			if(pilih2==1){
				$('input:radio[name=pilih]')[2].checked = true;
			}
			if(pilih2==2){
				$('input:radio[name=pilih]')[1].checked = true;
				$("#begdate").attr("disabled", true);
				$("#enddate").attr("disabled", true);
				$("#begdatejt").attr("disabled", false);
				$("#enddatejt").attr("disabled", false);
			}
			
			if(aging2==7){
				$('input:radio[name=ra]')[0].checked = true;
			}
			if(aging2==30){
				$('input:radio[name=ra]')[1].checked = true;
			}
			if(aging2==90){
				$('input:radio[name=ra]')[2].checked = true;
			}
			
			if(status2==0){
				$('input:radio[name=stfu]')[2].checked = true;
			}
			if(status2==1){
				$('input:radio[name=stfu]')[3].checked = true;
			}
			if(status2==2){
				$('input:radio[name=stfu]')[0].checked = true;
			}
			if(status2==3){
				$('input:radio[name=stfu]')[1].checked = true;
			}
			
			if(jenis2=='all'){
				$('input:radio[name=jenong]')[0].checked = true;
			}
			if(jenis2=='jatuhtempo'){
				$('input:radio[name=jenong]')[1].checked = true;
			}
			if(jenis2=='gagaldebet'){
				$('input:radio[name=jenong]')[2].checked = true;
			}
			if(jenis2=='powersave'){
				$('input:radio[name=jenong]')[3].checked = true;
			}
			if(jenis2=='stablelink'){
				$('input:radio[name=jenong]')[4].checked = true;
			}
			
			$("#btnShow").click();
			
			//followup
			$("#btnReset").click();
			//$("#followup").val(followup2);
			
			//tambahan
			$("#followup2").val(followup2);
			$("#followup3").val(polis);
			$("#begdate2").val($('#begdate').val());
			$("#enddate2").val($('#enddate').val());
			$("#begdate3").val($('#begdatejt').val());
			$("#enddate3").val($('#enddatejt').val());
			$("#aging2").val($('input[name=ra]:radio:checked').val());
			$("#status2").val($('input[name=stfu]:radio:checked').val());
			$("#jenis2").val($('input[name=jenong]:radio:checked').val());
			$("#pilih2").val($('input[name=pilih]:radio:checked').val());
			
			//1. tarik data detail
			
			$("#vacc").empty();//clear nilai vacc
			$("#jen_kel").empty();//clear nilai jenis kelamin
			$("#e_mail").empty();//clear nilai e-mail
			var url = "${path}/bas/fu_premi.htm?window=json&t=detail&fu=" +followup2;
			$.getJSON(url, function(result) {
				$.each(result, function() {
				var alamat2 = '';
				if(this.alamat2 != null){
					alamat2 = this.alamat2;
				}
					$("#lssp_status").val(this.lssp_status);
					$("#title").val(this.title);
					$("#pemegang").val(this.pemegang);
					$("#alamat").val(this.alamat1 + "\n" + alamat2);
					$("#telp").val(this.telp1 + " | " + this.telp2 + " | " + this.no_hp + " | " + this.no_hp2);
					
					$("#telp1").val(this.telp1);
					$("#telp2").val(this.telp2);
					$("#telp3").val(this.no_hp);
					$("#telp4").val(this.no_hp2);
					
					$("#tgl_tagihan").val(this.msbi_beg_date + " s/d " + this.msbi_end_date);
					$("#premi").val(this.kurs + " " + (this.premi).formatMoney(0, '.', ','));
					
					//topup
					$("#topup").val(this.kurs + " " + (this.topup).formatMoney(0, '.', ','));
					$("#jumlah").val(this.kurs + " " + (this.premi+this.topup).formatMoney(0, '.', ','));
					
					$("#tertanggung").html(this.title_tertanggung+" "+this.tertanggung);
					$("#produk").html(this.produk);
					$("#nm_produk").val(this.produk);
					$("#periode").html(this.beg_date + " s/d " + this.end_date);
					$("#pay_mode").html(this.pay_mode);
					$("#vacc").html(this.vacc);
					$("#pay_period").html(this.pay_period + " Thn");
					$("#tsi").html(this.kurs + " " + (this.tsi).formatMoney(0, '.', ','));
					
					$("#jen_kel").val(this.jenkel);
					$("#e_mail").val(this.email);
					
					$("#regional").val(this.lca_id+this.lwk_id+"."+this.lsrg_id+" ["+this.lsrg_nama+" / "+this.lwk_nama+" / "+this.lca_nama+"]");
					
					$("#spajThnPremi").val(followup2);
					
					$("#deskripsiPolis").slideDown();
					$("#tglKategori").hide();
				});
			});
			
			//2. tarik history payment
			$("#paymentBody").empty();
			var url2 = "${path}/bas/fu_premi.htm?window=json&t=payment&fu=" +followup2;
			$.getJSON(url2, function(result) {
				$.each(result, function() {
					$("#paymentTable > tbody:last").append(
							"<tr><td>" + this.tahun_ke + 
							"</td><td>" + this.premi_ke + 
							"</td><td>" + this.beg_date + 
							"</td><td>" + this.end_date + 
							"</td><td>" + (this.premi).formatMoney(0, '.', ',') + 
							"</td><td>" + this.tgl_rk + 
							"</td><td>" + this.tgl_payment + 
							"</td><td>" + this.paid + 
							"</td><td>" + this.status_paid + "</td></tr>");
				});
			});
			
			//3. tarik history followup
			$("#histBody").empty();
			var url3 = "${path}/bas/fu_premi.htm?window=json&t=hist&fu=" +followup2;
			$.getJSON(url3, function(result) {
				$("#msfu_status").val("Outstanding");
				$.each(result, function() {
					$("#histTable > tbody:last").append(
							"<tr><td>" + this.msfu_urut + 
							"</td><td>" + this.create_date + 
							"</td><td>" + this.user_name + 
							"</td><td>" + this.status + 
							"</td><td>" + this.lsfu_desc + 
							"</td><td>" + this.msfu_ket + "</td></tr>");
					if(this.lsfu_id != 99) $("#msfu_status").val(this.status);
				});
				
				if($("#msfu_status").val() == "Closed") {
					$("#btnSave").fadeOut();
					$("#btnEmail").fadeOut();
				}else{
					$("#btnSave").fadeIn();
					$("#btnEmail").fadeIn();
				}
			});

			//3. tarik info tahapan (bila ada)
			$("#tahapan").val("Tidak");
			var url3 = "${path}/bas/fu_premi.htm?window=json&t=tahapan&fu=" +followup2;
			$.getJSON(url3, function(result) {
				$.each(result, function() {
					$("#tahapan").val("Ya (" + this.key + ")");
				});
			});
			
			//4. tarik view auto debet
			$("#autoBody").empty();
			$("#sedang").empty();
			$("#total_sedang").empty();
			
			$("#gagal").empty();
			$("#total_gagal").empty();
			
			$("#berhasil").empty();
			$("#total_berhasil").empty();
			var url2 = "${path}/bas/fu_premi.htm?window=json&t=autodebet&fu=" +followup2;
			$.getJSON(url2, function(result) {
				$.each(result, function() {
					/* $("#sedang").html("("+this.SDG+")");
					$("#total_sedang").html(this.TOT_SDG); */
					
					$("#gagal").html("("+this.GAL+")");
					$("#total_gagal").html(this.TOT_GAL);
					
					$("#berhasil").html("("+this.NYOSS+")");
					$("#total_berhasil").html(this.TOT_NYOS);
					
					$("#autoTable > tbody:last").append(
							"<tr><td>" + this.NO + 
							"</td><td>" + this.TGL_TAGIHAN + 
							"</td><td>" + this.TGL_TERIMA + 
							"</td><td>" + this.JENIS + 
							"</td><td>" + this.LSBP_NAMA + 
							"</td><td>" + this.NO_REKENING + 
							"</td><td><center>" + this.TAHUN + 
							"</center></td><td>" + this.KE + 
							"</td><td>" + this.FREQ + 
							"</td><td>" + (this.TOTAL_INVOICE).formatMoney(0, '.', ',') + 
							"</td><td>" + this.KET + "</td></tr>");
				});
			});
			
			//5. tarik view billing
			$("#billingBody").empty();
			var urlbilling = "${path}/bas/fu_premi.htm?window=json&t=billing&fu=" +followup2;
			$.getJSON(urlbilling, function(resultBilling) {
				$.each(resultBilling, function() {
					
					$("#billingTable > tbody:last").append(
							"<tr><td>" + this.MSBI_PAID + 
							"</td><td><center>" + this.MSBI_TAHUN_KE + 
							"</center></td><td><center>" + this.MSBI_PREMI_KE + 
							"</center></td><td>" + this.MSBI_BEG_DATE + 
							"</td><td>" + this.MSBI_END_DATE + 
							"</td><td>" + this.LKU_ID + 
							"</td><td>" + this.MSPRO_NILAI_KURS+ 
							"</td><td>" + (this.PREMI_STD).formatMoney(0, '.', ',') + 
							"</td><td>" + (this.PREMI_RIDER).formatMoney(0, '.', ',') + 
							"</td><td>" + (this.PREMI_EXTRA).formatMoney(0, '.', ',') + 
							"</td><td>" + (this.PREMI_HCR).formatMoney(0, '.', ',') + 
							"</td><td>" + (this.DISCOUNT).formatMoney(0, '.', ',') +
							"</td><td>" + (this.MSBI_STAMP).formatMoney(0, '.', ',') +  
							"</td><td>" + (this.TOTAL_TAGIH).formatMoney(0, '.', ',') + "</td></tr>");
				});
			});
			
			//6. tarik view tahapan
			$("#tahapanBody").empty();
			var urltahapan = "${path}/bas/fu_premi.htm?window=json&t=viewtahapan&fu=" +followup2;
			$.getJSON(urltahapan, function(resultTahapan) {
				$.each(resultTahapan, function() {
					
					$("#tahapanTable > tbody:last").append(
							"<tr><td><center>" + this.MSTAH_TAHAPAN_KE + 
							"</center></td><td>" + this.MSTAH_NO_TAHAPAN + 
							"</td><td>" + this.MSTAH_BEG_ACTIVE + 
							"</td><td>" + (this.MSTAH_JUMLAH).formatMoney(0, '.', ',') + 
							"</td><td>" + this.MSTAH_PERSEN +" %"+ 
							"</td><td>" + this.MSTAH_JT_TEMPO + 
							"</td><td>" + this.MSTAH_TGL_PROSES + 
							"</td><td>" + this.MSTAH_TGL_PRINT + 
							"</td><td>" + this.MSTAH_TGL_KONFIRMASI + 
							"</td><td>" + this.MSTAH_TGL_TRANS + 
							"</td><td>" + this.LSJTA + 
							"</td><td>" + (this.MSBAT_JUMLAH).formatMoney(0, '.', ',') +  
							"</td><td>" + this.MSTAH_TGL_BAYAR + "</td></tr>");
				});
			});
			
			//7. tarik view simpanan
			$("#simpananBody").empty();
			var urlsimpanan = "${path}/bas/fu_premi.htm?window=json&t=viewsimpanan&fu=" +followup2;
			$.getJSON(urlsimpanan, function(resultSimpanan) {
				$.each(resultSimpanan, function() {
					
					$("#simpananTable > tbody:last").append(
							"<tr><td><center>" + this.MSSIM_NUMBER + 
							"</center></td><td>" + this.MSSIM_TGL_INPUT + 
							"</td><td>" + this.MSSIM_BEG_DATE + 
							"</td><td>" + this.MSTAH_NO_TAHAPAN + 
							"</td><td>" + this.MSSIM_NO_DEPOSITO + 
							"</td><td>" + (this.MSSIM_JUMLAH).formatMoney(0, '.', ',') + 
							"</td><td>" + this.MSSIM_PERSEN +
							"</td><td>" + this.BUNGA + 
							"</td><td>" + this.MSSIM_BEG_DATE + 
							"</td><td>" + this.MSSIM_END_DATE + 
							"</td><td>" + (this.MBS_JUMLAH).formatMoney(0, '.', ',') +  
							"</td><td>" + this.MSSIM_TGL_INPUT +  
							"</td><td>" + no_reg(this.MSSIM_NO_REG,this.MSSIM_FLAG) +  
							"</td><td>" + this.MSSIM_TGL_KLAIM +
							"</td><td>" + this.MSSIM_TGL_TRANS +    
							"</td><td>" + this.LSCSI_ID + "</td></tr>");
				});
			});
			
			//8. tarik call summary
			$("#callsummaryBody").empty();
			var urlcallsummary = "${path}/bas/fu_premi.htm?window=json&t=viewcallsummary&fu=" +followup2;
			$.getJSON(urlcallsummary, function(resultCallSummary) {
				$.each(resultCallSummary, function() {
					
					$("#callsummaryTable > tbody:last").append(
							"<tr><td><center>" + this.NO + 
							"</center></td><td>" + this.FLAG_INOUT + 
							"</td><td>" + this.USER_NAME + 
							"</td><td>" + this.LCA_ID + 
							"</td><td>" + this.LSCSF_NAMA + 
							"</td><td>" + this.MSCSF_BEG_TGL_DIAL + 
							"</td><td>" + this.MSCSF_END_TGL_DIAL +
							"</td><td>" + this.MSCSF_TGL_BACK + 
							"</td><td>" + "\""+this.MSCSF_KET +"\""+ 
							"</td><td>" + this.MSCSF_OCR +    
							"</td><td>" + this.MSCSF_DIR + "</td></tr>");
				});
			});
			
			//10. tarik Detail Power Save
			$("#detailpowersaveBody").empty();
			var urldetailpowersave = "${path}/bas/fu_premi.htm?window=json&t=detailpowersave&fu=" +followup2;
			$.getJSON(urldetailpowersave, function(resultDetailPowerSave) {
				$.each(resultDetailPowerSave, function() {
					
					$("#detailpowersaveTable > tbody:last").append(
							"<tr><td><center>" + this.PERIODE + 
							"</center></td><td>" + this.MATURITY_DATE + 
							"</td><td><center>" + this.RO_KE + 
							"</center></td><td><center>" + this.MGI +" Bln"+ 
							"</center></td><td><center>" + this.JENIS_RATE + 
							"</center></td><td><center>" + this.RATE +
							"</center></td><td><center>" + this.KURS + 
							"</center></td><td>" + this.PREMI_DEPOSIT +
							"</td><td>" + this.PREMI_INTEREST +
							"</td><td>" + this.PREMI_RIDER +
							"</td><td>" + this.PREMI_DEBIT +    
							"</td><td>" + this.PREMI_INSURANCE +
							"</td><td>" + this.PREMI_TAX +
							"</td><td>" + this.PREMI_DIBAYAR + "</td></tr>");
				});
			});
			
			//11. tarik Detail Stable Link
			$("#detailstablelinkBody").empty();
			var urldetailstablelink = "${path}/bas/fu_premi.htm?window=json&t=detailstablelink&fu=" +followup2;
			$.getJSON(urldetailstablelink, function(resultDetailStableLink) {
				$.each(resultDetailStableLink, function() {
					
					$("#detailstablelinkTable > tbody:last").append(
							"<tr><td><center>" + this.PERIODE + 
							"</center></td><td>" + this.DESCRIPTION + 
							"</td><td><center>" + this.RO_KE + 
							"</center></td><td><center>" + this.KURS + 
							"</center></td><td>" + this.PREMI + 
							"</td><td>" + this.INTEREST + 
							"</td><td>" + this.PREMI_BUNGA +
							"</td><td><center>" + this.MTI + 
							"</center></td><td><center>" + this.ROLL_OVER +
							"</center></td><td>" + this.PAID_DATE +
							"</td><td>" + this.NAB + "</td></tr>");
				});
			});
			
			$("#followup").append($("<option />",{selected:'selected'})
				    		.val(followup2)
				    		.text(polis));
						
			$('textarea[maxlength]').live('keyup change', function() {
				var str = $(this).val();
				var mx = parseInt($(this).attr('maxlength'));
				if (str.length > mx) {
					$(this).val(str.substr(0, mx));
					return false;
				}
			});
			
			alert(pesan);
			//end followup
		}
		//end
		
		//kategori lain-lain bisa isi email
		$('#emailto').attr('readonly', false); 
		
	});
	
	function no_reg(reg, flag){
		var result = '';
		if(reg!=' '){
			if(flag==1){
				result = reg[0]+reg[1]+reg[2]+reg[3]+reg[4]+'/IVA/'+reg[5]+reg[6]+'/'+reg[7]+reg[8]+reg[9]+reg[10];
			}else{
				result = reg[0]+reg[1]+reg[2]+reg[3]+reg[4]+'/IVT/'+reg[5]+reg[6]+'/'+reg[7]+reg[8]+reg[9]+reg[10];
			}
		}
		return result;
	}
	
	Number.prototype.formatMoney = function(c, d, t){ 
		var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0; 
	   	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : ""); 
	 }; 
	 
	 function email(cat){
	 	if(cat==0 || cat==11 || cat==20 || cat==21 || cat==24 || cat==26 || cat==27
	 			|| cat== 5 || cat==15 || cat==25 || cat==28 || cat==29 || cat==6 || cat==32){
	 		$('#emailto').attr('readonly', false);
	 	}else{
	 		$('#emailto').attr('readonly', true);
	 	}
	 }
	 
</script>

<fieldset class="ui-widget ui-widget-content">
	<legend class="ui-widget-header ui-corner-all"><div>Input Follow Up Premi Lanjutan Jatuh Tempo Per Cabang</div></legend>
	
	<div class="rowElem">
		<label title="Berdasarkan Tanggal Tagihan (Untuk Premi Lanjutan yang akan Jatuh Tempo)"><input type="radio" name="pilih" value="0" checked="checked">Berdasarkan Tanggal Tagihan:</label>
		<input name="begdate" id="begdate" type="text" class="datepicker" title="Awal" value="${begdate}"> s/d 
		<input name="enddate" id="enddate" type="text" class="datepicker" title="Akhir" value="${enddate}">
		<!-- biar ga error -->
		<input type="hidden" name="begdatejt" id="begdatejt" value="${begdate2}"/>
		<input type="hidden" name="enddatejt" id="enddatejt" value="${enddate2}"/>
		<!-- end -->
	</div>

	<%-- <div class="rowElem">
		<label>&nbsp;</label>
		Atau
	</div>
	
	<div class="rowElem">
		<label title="Berdasarkan Tanggal Jatuh Tempo (Untuk Premi Lanjutan yang akan Jatuh Tempo)"><input type="radio" name="pilih" value="2">Berdasarkan Tanggal Jatuh Tempo:</label>
		<input name="begdatejt" id="begdatejt" type="text" class="datepicker" title="Awal" value="${begdate2}"> s/d 
		<input name="enddatejt" id="enddatejt" type="text" class="datepicker" title="Akhir" value="${enddate2}">
	</div> --%>
<%--	
	<div class="rowElem">
		<label>&nbsp;</label>
		Atau 
	</div>
	
	<div class="rowElem">
		<label title="Berdasarkan Tanggal Aging (Untuk Premi Lanjutan yang telah Jatuh Tempo)"><input type="radio" name="pilih" value="1">Berdasarkan Tanggal Aging:</label>
		<input type="radio" id="radioAging0" name="ra" value="7" title="Aging 7 Hari" checked="checked" disabled="true"><label class="radioLabel" for="radioAging0">7 Hari</label>
		<input type="radio" id="radioAging1" name="ra" value="30" title="Aging 30 Hari" disabled="true"><label class="radioLabel" for="radioAging1">30 Hari</label>
		<input type="radio" id="radioAging2" name="ra" value="90" title="Aging 3 Bulan" disabled="true"><label class="radioLabel" for="radioAging2">3 Bulan</label>
	</div>
--%>
	<hr>
	
	<div class="rowElem">
		<label>Status Follow Up:</label>
		<c:forEach var="f" items="${listStatus}" varStatus="s">
			<input type="radio" id="radioS${s.index}" name="stfu" value="${f.key}" title="${f.value}"
				<c:if test="${f.key eq 2}">checked="checked"</c:if>
			><label class="radioLabel" for="radioS${s.index}">${f.value}</label>
		</c:forEach>
	</div>

	<hr>

	<div class="rowElem">
		<label>Jenis:</label>
		<c:forEach var="f" items="${listJenis}" varStatus="s" end="3">
			<input type="radio" id="radioJ${s.index}" name="jenong" value="${f.key}" title="${f.value}"
				<c:if test="${f.key eq \"all\"}">checked="checked"</c:if>
			><label class="radioLabel" for="radioJ${s.index}">${f.value}</label>
		</c:forEach>
		
	</div>
<%--	<div class="rowElem">
		<label>&nbsp;</label>
		<label class="radioLabel"><em>*untuk visa camp list akan diambil 1 tahun kedepan dari tanggal awal yg dipilih</em></label>
	</div>
	
	<hr>
	
	<c:if test="${sessionScope.currentUser.lus_id ne 1128 && sessionScope.currentUser.lus_id ne 3179 && sessionScope.currentUser.lus_id ne 3815}">
		<div class="rowElem">
			<label>Power Save / Stable Save:</label>
			<c:forEach var="f" items="${listJenis}" varStatus="s" begin="6">
				<input type="radio" id="radioJ${s.index}" name="jenong" value="${f.key}" title="${f.value}"
					<c:if test="${f.key eq \"all\"}">checked="checked"</c:if>
				><label class="radioLabel" for="radioJ${s.index}">${f.value}</label>
			</c:forEach>
		</div>
	</c:if>
--%>		
	<hr>

	<div class="rowElem">
		<label>No Polis:</label>
		<input type="text" id="no_polis" name="no_polis"/>
	</div>

	<hr>

	<div class="rowElem">
		<label></label>
		<button id="btnShow" title="Tampilkan data">Tampilkan</button>
		<button id="btnShowExcel" title="Save To Excell" onclick="return false;">Show (Excell)</button>
	</div>

</fieldset>

<fieldset id="fieldsetInput" class="ui-widget ui-widget-content">

	<form id="formPost" name="formPost" method="post" enctype="multipart/form-data">
	<input id="spajThnPremi" name="spajThnPremi" type="hidden">
	<input id="submitType" name="submitType" type="hidden">
	<!-- tambahan -->
	<input id="begdate2" name="begdate2" type="hidden">
	<input id="enddate2" name="enddate2" type="hidden">
	<input id="begdate3" name="begdate3" type="hidden">
	<input id="enddate3" name="enddate3" type="hidden">
	<input id="aging2" name="aging2" type="hidden">
	<input id="status2" name="status2" type="hidden">
	<input id="jenis2" name="jenis2" type="hidden">
	<input id="pilih2" name="pilih2" type="hidden">
	<input id="followup2" name="followup2" type="hidden">
	<input id="followup3" name="followup3" type="hidden">
	<input id="nm_produk" name="nm_produk" type="hidden">
	<input id="telp1" name="telp1" type="hidden">
	<input id="telp2" name="telp2" type="hidden">
	<input id="telp3" name="telp3" type="hidden">
	<input id="telp4" name="telp4" type="hidden">
	<!-- end -->
	<table class="jtable" width="100%">
		<tr>
			<th width="30%">Tgl Tagihan | No. Polis</th>
			<th>Deskripsi Polis</th>
		</tr>
		<tr>
			<th valign="top">
				<select name="followup" id="followup" size="30" title="Silahkan pilih Polis yang ingin di follow up">
					<option>--- Silahkan cari terlebih dahulu ---</option>
				</select>
			</th>
			<td id="deskripsiPolis" rowspan="2">
				<table class="plain" width="100%">
					<center>
						<div id="followup4"></div>
					</center>
					<tr>
						<th>Status Follow Up:</th>
						<td><input id="msfu_status" type="text" class="lebar ui-state-default" readonly="readonly">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Status Polis: </b>&nbsp;&nbsp;
							<input id="lssp_status" name="lssp_status" type="text" class="lebar ui-state-default" readonly="readonly" style="width: 100px;">
						</td>
						
					</tr>
					<tr>
						<th>Regional:</th>
						<td><input id="regional" type="text" class="lebar ui-state-default" readonly="readonly"></td>
					</tr>
					<tr>
						<th>Nama Pemegang Polis:</th>
						<td>
							<input id="title" name="title" type="text" class="lebar ui-state-default" readonly="readonly" style="width: 50px;">
							<input id="pemegang" name="pemegang" type="text" class="lebar ui-state-default" readonly="readonly"></td>
					</tr>
					<tr>
						<th>Jenis Kelamin:</th>
						<td><input id="jen_kel" name="jen_kel" type="text" class="lebar ui-state-default" readonly="readonly"></td>
					</tr>
					<tr>
						<th>E-Mail:</th>
						<td><input id="e_mail" name="e_mail" type="text" class="lebar ui-state-default" readonly="readonly"></td>
					</tr>
					<tr>
						<th>Alamat:</th>
						<td><textarea id="alamat" rows="3" cols="50" class="lebar ui-state-default" readonly="readonly"></textarea></td>
					</tr>
					<tr>
						<th>No. Telp:</th>
						<td><input id="telp" type="text" class="lebar ui-state-default" readonly="readonly"></td>
					</tr>
					<tr>
						<th>Periode Tagihan:</th>
						<td><input id="tgl_tagihan" name="tgl_tagihan" type="text" class="lebar ui-state-default" readonly="readonly"></td>
					</tr>
					<tr>
						<th>Premi:</th>
						<td><input id="premi" type="text" class="lebar ui-state-default" readonly="readonly"></td>
					</tr>
					<tr>
					  <th>TopUp:</th>
					  <td><input id="topup" type="text" class="lebar ui-state-default" readonly="readonly"></td>
			    	</tr>
					<tr>
					  <th>Jumlah:</th>
					  <td><input id="jumlah" name="jumlah" type="text" class="lebar ui-state-default" readonly="readonly"></td>
			    	</tr>
					<tr>
						<th>Nasabah Mempunyai Tahapan:</th>
						<td><input id="tahapan" type="text" class="lebar ui-state-default" readonly="readonly"></td>
					</tr>
					<tr>
						<th></th>
						<td>
							<button id="btnDetail" title="View detail polis" onclick="return false;">View Detail Polis</button>
							<button id="btnHistory" title="View history follow up" onclick="return false;">View History Follow Up</button>
							<button id="btnAutoDebet" title="View Auto Debet" onclick="return false;">View Auto Debet</button>
							<br><button id="btnBilling" title="View Billing" onclick="return false;">View Billing</button>
							<button id="btnCallSummary" title="View Call Summary" onclick="return false;">View Call Summary</button>
							<!-- <button id="btnSimpanan" title="View Simpanan" onclick="return false;">View Simpanan</button> -->
							<button id="btnDetailPowerSave" title="View Detail Power Save" onclick="return false;">View Detail Power Save</button>
							<button id="btnDetailStableLink" title="View Detail Stable Link" onclick="return false;">View Detail Stable Link</button>
						</td>
					</tr>
					<tr>
						<th colspan="2"><hr></th>
					</tr>
					<tr>
						<th colspan="2">
							<table>
								<tr>
									<th><fieldset style="text-align: left;">
										<legend style="background-color: #e6e6e6;vertical-align: middle;padding: 2px 0 2px 2px;">JIKA NASABAH MENYATAKAN TITIP PREMI KE AGEN, HARAP MENANYAKAN :</legend>
											<p style="color: red;">1.	Siapa nama Agen  ?<br>
											2.	Berapa jumlah Pembayaran Premi yang dititipkan ?<br>
											3.	Pembayaran dalam bentuk  Cash / Cek / Giro ?<br>
											4.	Kapan Disetorkan ke Agen ? Jika nasabah tidak ingat tanggal tanyakan di Awal, Pertengahan atau Akhir Bulan apa ?<br>
											5.	Apakah ada tanda terima dari agen ?</p>
																			
										</fieldset></th>
									<th><div id="stopwatch-1"></div></th>
								</tr>
							</table>
						</th>
					</tr>
					<tr>
						<th colspan="2"><hr></th>
					</tr>
					<tr>
						<th>Waktu:</th>
						<td><input name="waktu" id="waktu" type="text" title="Waktu followup" value="00:00:00" class="lebar ui-state-default" readonly="readonly"></td>
					</tr>
					<tr>
						<th>Kategori Hasil Follow Up:<em>*</em></th>
						<td>
							<select name="kategori" id="kategori" title="Silahkan pilih kategori hasil follow up" class="lebar" onchange="email(this.value);" >
								<c:forEach var="k" items="${listKat}" varStatus="s">
									<option value="${k.key}">${k.value}</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr id="tglKategori">
						<th>Tanggal Reminder:<em>*</em></th>
						<td>
							<input name="remindDate" id="remindDate" type="text" class="datepicker" title="Reminder" value="${remindDate}">
						</td>
					</tr>
					<tr>
						<th>Email akan Dikirimkan ke:</th>
						<td>
							<input id="emailto" name="emailto" type="text" class="lebar ui-state-default" readonly="readonly">
							<input id="lsfustatus" name="lsfustatus" type="hidden" value="0">
						</td>
					</tr>
					<tr>
						<th>Keterangan Hasil Follow Up:<em>*</em><br><em>(max 500 char)</em></th>
						<td><textarea id="keterangan" name="keterangan" maxlength="500" rows="7" cols="50" title="Silahkan masukkan keterangan hasil follow up" class="lebar"></textarea></td>
					</tr>
					<tr>
						<th>Upload Bukti Pendukung:</th>
						<td><input id="file1" name="file1" type="file" title="File bukti pendukung untuk dikirimkan ke bagian terkait" class="lebar"></td>
					</tr>
					<tr>
						<th></th>
						<td>
							<!-- <input type="submit" id="btnSave" title="Simpan follow up" value="Simpan"> -->
							<input type="submit" id="btnEmail" title="Simpan follow up dan email ke bagian terkait (Audit)" value="Simpan dan Email">
							<input type="reset" id="btnReset" title="Reset Form">
							<input type="submit" id="btnEmailNasabah" title="Kirim E-Mail ke Nasabah" value="Kirim E-Mail ke Nasabah">
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<th><em>*Warna merah: Tidak bisa dihubungi/Tidak sanggup bayar premi</em></th>
		</tr>
	</table>
	</form>

</fieldset>