<%@ include file="/include/page/header_jquery.jsp"%>
<script type="text/javascript">
	// Jalankan semua script jquery, setelah seluruh document selesai loading
	$().ready(function() {
		
		// (jQueryUI Tabs) init tab2 Utama (Pemegang, Tertanggung, dll)
		$("#tabs").tabs();

		// (Qtip2) init tooltip untuk semua element menggunakan title nya
		$("input[title], select[title], textarea[title], button[title], label[title]").qtip();
		
		// (jQueryUI datepicker) init semua field datepicker
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "dd/mm/yy" 
		});

		// (jQueryUI themes) Styling untuk table
		$(".jtable th").each(function() {
			$(this).addClass("ui-widget-header"); //bisa juga ui-state-default
		});
		$(".jtable td").each(function() {
			$(this).addClass("ui-widget-content");
		});

		// (jQueryUI themes) Remove Styling untuk table selain jtable
		$(".plain th").each(function() {
			$(this).removeClass();
		});
		$(".plain td").each(function() {
			$(this).removeClass();
		});

		// (jQueryUI themes) Styling untuk form elements
		$("input:text, textarea, file").addClass("ui-widget-content"); //styling untuk semua input elements, tidak termasuk radio dan checkbox
		//$(":input").addClass("ui-widget-content"); //styling untuk semua input elements
		
		$("input:submit, input:button, button,  input:reset, input:file").button(); //styling untuk semua tombol
		
		if(parent){
			if(parent.document){
				var dZone = parent.document.getElementById('dZone');
				if(dZone) dZone.style.visibility = 'hidden';
				window.scrollbars = false;
			}
		}
		
	});
</script>

<style type="text/css">
	/* font utama */
	body { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 0.7em; }

	/* fieldset */
	fieldset { margin-bottom: 1em; padding: 0.5em; }
	fieldset legend { width: 99%; }
	fieldset legend div { margin: 0.3em 0.5em; }
	fieldset .rowElem { margin-left: 0.5em; padding: 0.3em; }
	fieldset .rowElem label { margin-right: 0.4em; width: 23em; display: inline-block; }
	fieldset .rowElem .jtable { position: relative; left: 23.5em; }

	/* tanda bintang (*) untuk menandakan required field */
	em { color: red; font-weight: bold; }

	/* agar semua datepicker align center, dan ukurannya fix */
	.datepicker { text-align: center; width: 7em; }

	/* styling untuk client-side validation error message */
	#formPost label.error { margin-left: 0.4em; color: red; font-size: 0.9em; font-style: italic; }

	/* styling untuk label khusus checkbox dan radio didalam rowElement */
	fieldset .rowElem label.radioLabel { width: auto; }
	
	/* lebar untuk form elements */
	.lebar { width: 36em; }
	
	/* untuk align center */
	.tengah { text-align: center; }

	/* untuk tombol2 kecil di report */
	ul#icons {margin: 0; padding: 0;}
	ul#icons li {margin: 0 1px 0 0; position: relative; padding: 0px; cursor: pointer; float: left;  list-style: none;}
	ul#icons span.ui-icon {float: left; margin: 0px;}

</style>

<body>

	<%-- FIXME: nanti remove ini untuk development saja
	<script type="text/javascript"
	  src="http://jqueryui.com/themeroller/themeswitchertool/">
	</script>
	<div id="switcher"></div>
	--%>
	
	<!-- Tabs -->
	<div id="tabs">
		<c:choose>
			<c:when test="${sessionScope.currentUser.lus_id eq 574}">
				<ul>
					<li><a href="#tab-1">Input</a></li>
					<li><a href="#tab-2">Kontrol</a></li>
					<li><a href="#tab-3">Report</a></li>
				</ul>
				<div id="tab-1"><jsp:include page="input.jsp" /></div>
				<div id="tab-2"><jsp:include page="kontrol.jsp" /></div>
				<div id="tab-3"><jsp:include page="report.jsp" /></div>
			</c:when>
			<c:when test="${sessionScope.currentUser.lus_bas eq 1}">
				<ul>
					<li><a href="#tab-1">Input</a></li>
					<li><a href="#tab-3">Report</a></li>
				</ul>
				<div id="tab-1"><jsp:include page="input.jsp" /></div>
				<div id="tab-3"><jsp:include page="report.jsp" /></div>
			</c:when>
			<c:when test="${sessionScope.currentUser.lus_bas eq 0}">
				<ul>
					<li><a href="#tab-2">Kontrol</a></li>
					<li><a href="#tab-3">Report</a></li>
				</ul>
				<div id="tab-2"><jsp:include page="kontrol.jsp" /></div>
				<div id="tab-3"><jsp:include page="report.jsp" /></div>
			</c:when>
		</c:choose>
	</div>

	<div id="detailPolis" style="display: none; font-size: 1.2em;">
	<div style="overflow: auto; height: 400px;">
		<table>
			<tr>
				<th>Nama Tertanggung:</th>
				<td id="tertanggung"></td>
			</tr>
			<tr>
				<th>Nama Produk:</th>
				<td id="produk"></td>
			</tr>
			<tr>
				<th>Periode Polis:</th>
				<td id="periode"></td>
			</tr>
			<tr>
				<th>Cara Pembayaran:</th>
				<td id="pay_mode"></td>
			</tr>
			<tr>
				<th>Billing Account:</th>
				<td id="vacc"></td>
			</tr>
			<tr>
				<th>Lama Pembayaran:</th>
				<td id="pay_period"></td>
			</tr>
			<tr>
				<th>Uang Pertanggungan:</th>
				<td id="tsi"></td>
			</tr>
			<tr>
				<th>Report Pembayaran:</th>
				<td>
					<table id="paymentTable" class="jtable">
						<thead>
							<tr>
								<th>Thn Ke</th>
								<th>Premi Ke</th>
								<th>Begin Date</th>
								<th>End Date</th>
								<th>Premi</th>
								<th>Tgl RK</th>
								<th>Tgl Payment</th>
								<th>Jumlah Bayar</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody id="paymentBody">
						</tbody>
					</table>
				</td>
			</tr>
		</table>
	</div>
	</div>

	<div id="historyFollowup" style="display: none; font-size: 1.2em;">
	<div style="overflow: auto; height: 400px;">
		<table id="histTable" class="jtable">
			<thead>
				<tr>
					<th>No.</th>
					<th>Tanggal</th>
					<th>User</th>
					<th>Status</th>
					<th>Kategori</th>
					<th>Keterangan</th>
				</tr>
			</thead>
			<tbody id="histBody">
			</tbody>
		</table>
	</div>
	</div>
	
	<!-- View Auto Debet -->
	<div id="viewAutoDebet" style="display: none; font-size: 1.2em;">
	<div style="overflow: auto; height: 400px;">
		<table id="autoTable" class="jtable">
			<thead>
				<tr>
					<th>No.</th>
					<th>Tanggal Debet</th>
					<th>Tanggal Terima</th>
					<th>Jenis</th>
					<th>Bank</th>
					<th>Account No.</th>
					<th>Tahun</th>
					<th>Ke</th>
					<th>Freq</th>
					<th>Premi</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody id="autoBody">
			</tbody>
		</table>
		<table>
		<tr>
				<td colspan="3">&nbsp;</td>
			</tr>
			<tr>
				<th colspan="3">TOTAL</th>
			</tr>
			<!-- <tr>
				<th>Sedang : </th>
				<td id="sedang"></td>
				<td id="total_sedang"></td>
			</tr> -->
			<tr>
				<th>Gagal : </th>
				<td id="gagal"></td>
				<td id="total_gagal"></td>
			</tr>
			<tr>
				<th>Berhasil : </th>
				<td id="berhasil"></td>
				<td id="total_berhasil"></td>
			</tr>
		</table>
	</div>
	</div>
	
	<!-- View Billing -->
	<div id="viewBilling" style="display: none; font-size: 1.2em;">
	<div style="overflow: auto; height: 500px;">
		<!-- gabung -->
		<strong>Tahapan</strong>
		<table id="tahapanTable" class="jtable">
			<thead>
				<tr>
					<th>Ke</th>
					<th>No Tahapan</th>
					<th>Beg Aktif</th>
					<th>Jumlah</th>
					<th>Persen</th>
					<th>Jt. Tempo</th>
					<th>Tgl Proses</th>
					<th>Tgl Surat</th>
					<th>Tgl Input</th>
					<th>Transfer</th>
					<th>Jenis Klaim</th>
					<th>Jlh Bayar</th>
					<th>Perintah Bayar</th>
				</tr>
			</thead>
			<tbody id="tahapanBody">
			</tbody>
		</table>
		
		<br><br><strong>Simpanan</strong>
		<table id="simpananTable" class="jtable">
			<thead>
				<tr>
					<th>No</th>
					<th>Tgl Transfer dari Thp ke Simpanan</th>
					<th>Simpanan dari Jt.Tempo Tahapan</th>
					<th>No. Tahapan</th>
					<th>No. Deposito</th>
					<th>Jumlah Simpanan Awal</th>
					<th>Persen</th>
					<th>Jumlah Bunga</th>
					<th>Beg Date</th>
					<th>End Date</th>
					<th>Jlh Simpanan dan Bunga Akhir Periode Simpanan</th>
					<th>Input</th>
					<th>No. Register Simpanan Roll Over</th>
					<th>Tgl Klaim</th>
					<th>Transfer</th>
					<th>Jenis Klaim</th>
				</tr>
			</thead>
			<tbody id="simpananBody">
			</tbody>
		</table>
		<!-- End -->
		
		<br><br><strong>Billing</strong>
		<table id="billingTable" class="jtable">
			<thead>
				<tr>
					<th>Status</th>
					<th>Thn Ke</th>
					<th>Prm Ke</th>
					<th>Begin Date</th>
					<th>End Date</th>
					<th>Due Date</th>
					<th>Cur.</th>
					<th>Nilai Kurs</th>
					<th>Premi Std</th>
					<th>Premi Rider</th>
					<th>Premi Extra</th>
					<th>Premi HCR</th>
					<th>Discount</th>
					<th>Stamp</th>
					<th>Total Tagih</th>
				</tr>
			</thead>
			<tbody id="billingBody">
			</tbody>
		</table>
	</div>
	</div>
	
	<!-- View Call Summary -->
	<div id="viewCallSummary" style="display: none; font-size: 1.2em;">
	<div style="overflow: auto; height: 400px;">
		<table id="callsummaryTable" class="jtable" style="width: 1450px">
			<thead>
				<tr>
					<th style="width: 50px">No</th>
				    <th style="width: 100px">Tipe</th>
				    <th style="width: 100px">User</th>
				    <th style="width: 150px">Cabang</th>
				    <th style="width: 150px">Kategori</th>
				    <th style="width: 100px">Tanggal Call</th>
				    <th style="width: 100px">Akhir Call</th>
				    <th style="width: 100px">Call Back</th>
				    <th style="width: 300px">Keterangan</th>
				    <th style="width: 100px">ORC</th>
				    <th style="width: 200px">URL Download</th>
				</tr>
			</thead>
			<tbody id="callsummaryBody">
			</tbody>
		</table>
	</div>
	</div>
	
	<!-- View Detail Power Save -->
	<div id="viewDetailPowerSave" style="display: none; font-size: 1.2em;">
	<div style="overflow: auto; height: 400px;">
		<table id="detailpowersaveTable" class="jtable" style="width: 1450px">
			<thead>
				<tr>
					<th style="width: 400px">Periode</th>
				    <th style="width: 100px">Maturity Date</th>
				    <th style="width: 100px">RO Ke</th>
				    <th style="width: 150px">MGI</th>
				    <th style="width: 150px">Jenis Rate</th>
				    <th style="width: 100px">Rate %</th>
				    <th style="width: 100px">Kurs</th>
				    <th style="width: 150px">Premi Deposit</th>
				    <th style="width: 150px">Premi Interest</th>
				    <th style="width: 150px">Premi Rider</th>
				    <th style="width: 150px">Premi Debit</th>
				    <th style="width: 150px">Premi Insurance</th>
				    <th style="width: 150px">Tax</th>
				    <th style="width: 200px">Premi Dibayar Roll Over</th>
				</tr>
			</thead>
			<tbody id="detailpowersaveBody">
			</tbody>
		</table>
	</div>
	</div>
	
	<!-- View Detail Stable Link -->
	<div id="viewDetailStableLink" style="display: none; font-size: 1.2em;">
	<div style="overflow: auto; height: 400px;">
		<table id="detailstablelinkTable" class="jtable" style="width: 1450px">
			<thead>
				<tr>
					<th style="width: 400px">Periode</th>
				    <th style="width: 400px">Description</th>
				    <th style="width: 100px">RO Ke</th>
				    <th style="width: 150px">Kurs</th>
				    <th style="width: 150px">Premi</th>
				    <th style="width: 150px">Interest</th>
				    <th style="width: 150px">Premi+Bunga</th>
				    <th style="width: 100px">MTI</th>
				    <th style="width: 150px">Roll Over</th>
				    <th style="width: 150px">Paid Date</th>
				    <th style="width: 200px">NAV</th>
				</tr>
			</thead>
			<tbody id="detailstablelinkBody">
			</tbody>
		</table>
	</div>
	</div>

</body>
</html>