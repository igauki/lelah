<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%><%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	// Script khusus halaman ini
	$().ready(function() {
		
		// button icons
		$( "#btnShow2" ).button({icons: {primary: "ui-icon-search"}});
		$( "#btnCopy" ).button({icons: {primary: "ui-icon-copy"}});
		
		// user memilih cabang, maka tampilkan admin2nya
		$("#cabang").change(function() {
			$("#admin").empty();
			var url = "${path}/bas/fu_premi.htm?window=json&t=admin&lca=" +$("#cabang").val();
			$.getJSON(url, function(result) {
				$("<option/>").val("ALL").html("ALL").appendTo("#admin");
				$.each(result, function() {
					$("<option/>").val(this.key).html('[' + this.desc + '] ' + this.value).appendTo("#admin");
				});
			});
		});
		//$("#cabang").change();
		
		// user menekan tombol Tampilkan
		$("#btnShow2").click(function() {
			var jn = $("input:radio[name=jn_tgl]:checked").val();
			var url2 = "${path}/bas/fu_premi.htm?window=json&t=kontrol&begdate=" +$('#begdate2').val()+ "&enddate=" +$('#enddate2').val() + "&stfu2=" + $('input:radio[name=stfu2]:checked').val() + '&cabang=' + $('#cabang').val() + '&admin=' + $('#admin').val() + '&jn=' +jn;
			$.getJSON(url2, function(result) {
				$("#btnShow2").button("option", "disabled", true);
				$("#btnShow2").button("option", "label", "Silahkan tunggu...");
				$("#kontBody").empty();
				var count = 0;
				$.each(result, function() {
					$('#kontTable > tbody:last').append(
							'<tr><td>' + this.msbi_beg_date + 
							'</td><td>' + this.msbi_end_date +
							'</td><td><input size="20" class="tengah ui-state-default" readonly="readonly" type="text" name="polis" value="'+this.mspo_policy_no_format+'">' + //this.mspo_policy_no_format + 
							'</td><td><input size="40" class="ui-state-default" readonly="readonly" type="text" name="pp" value="'+this.pemegang+'">' + //this.pemegang + 
							'</td><td>' + 
								'<input type="hidden" name="spaj" value="' +this.reg_spaj+ '">' + 
								'<input type="hidden" name="thnke" value="' +this.msbi_tahun_ke+ '">' + 
								'<input type="hidden" name="premike" value="' +this.msbi_premi_ke+ '">' + 
								'<textarea rows="2" cols="50" name="ket" title="Silahkan masukkan keterangan"></textarea>' + 
							'</td><td class="tengah">' + this.jml_fu + '</td></tr>');
					count++;
				});
				if(count == 0){
					alert("Tidak ada data.");
				}
				$("#btnShow2").button("option", "disabled", false);				
				$("#btnShow2").button("option", "label", "Tampilkan");
			});
		});

		// user menekan tombol Copy
		$("#btnCopy").click(function() {
			$("#kontBody textarea").val($("#ketGlobal").val());
		});
		
		// user menekan tombol submit
		$('#btnSave2').click(function() {
			if(confirm('Simpan Kontrol Followup sekaligus email ke Branch Admin bersangkutan?')){
				$('#submitType2').val(1);
				//$('#formPost2').submit();
			}else{
				return false;
			}
		});
		
		// (jQuery Form Plugin) rubah form jadi ajax
		var options2 = {
			success : showResponse2
		};
		$('#formPost2').ajaxForm(options2); // bind form using 'ajaxForm'
	
		function showResponse2(responseText, statusText, xhr, $form) { // post-submit callback 
		    alert(responseText);
		}				
		
	});
</script>

<form id="formPost2" name="formPost2" method="post">
<input id="submitType2" name="submitType2" type="hidden">
<fieldset class="ui-widget ui-widget-content">
	<legend class="ui-widget-header ui-corner-all"><div>Monitoring BAS Pusat untuk Follow Up Premi Lanjutan</div></legend>
	
	<div class="rowElem">
		<label>Cabang</label>
		<select name="cabang" id="cabang" title="Silahkan pilih Cabang">
			<option>-- Silahkan pilih Cabang --</option>
			<%-- <option value="ALL">ALL</option> disabled karena lama banget --%>
			<c:forEach var="c" items="${listCab}" varStatus="s">
				<option value="${c.key}">${c.value}</option>
			</c:forEach>
		</select>
	</div>
	
	<div class="rowElem">
		<label>Admin</label>
		<select name="admin" id="admin" title="Silahkan pilih Admin">
		</select>
	</div>
	
	<div class="rowElem">
		<label>Tanggal:</label>
		<input type="radio" name="jn_tgl" id="jn_tgl0" value="0" checked="checked"/>Tgl Tagihan
		<input type="radio" name="jn_tgl" id="jn_tgl1" value="1" />Tgl Jatuh Tempo
	</div>
	
	<div class="rowElem">
		<label>&nbsp;</label>
		<input name="begdate2" id="begdate2" type="text" class="datepicker" title="Awal" value="${begdate}"> s/d 
		<input name="enddate2" id="enddate2" type="text" class="datepicker" title="Akhir" value="${enddate}">
	</div>
	
	<div class="rowElem">
		<label>Status Follow Up:</label>
		<c:forEach var="f" items="${listStatus}" varStatus="s">
			<input type="radio" id="radioSF${s.index}" name="stfu2" value="${f.key}" title="${f.value}"
				<c:if test="${f.key eq 2}">checked="checked"</c:if>
			><label class="radioLabel" for="radioSF${s.index}">${f.value}</label>
		</c:forEach>
	</div>

	<div class="rowElem">
		<label></label>
		<button id="btnShow2" title="Tampilkan data" onclick="return false;">Tampilkan</button>
	</div>
</fieldset>

<fieldset id="fieldsetKontrol" class="ui-widget ui-widget-content">
	
	<div class="rowElem">
		<label>Keterangan:</label>
		<textarea id="ketGlobal" name="ketGlobal" rows="3" cols="25" title="Silahkan masukkan keterangan"></textarea>
		&nbsp;<button id="btnCopy" title="Copy keterangan ke seluruh data di bawah" onclick="return false;">Copy</button>
	</div>

	<table id="kontTable" class="jtable" width="100%">
		<thead>
			<tr>
				<th>Tgl Tagihan</th>
				<th>Tgl Jatuh Tempo</th>
				<th>No. Polis</th>
				<th>Pemegang</th>
				<th>Keterangan Follow Up</th>
				<th>Folow Up Ke</th>
			</tr>
		</thead>
		<tbody id="kontBody">
		</tbody>
	</table>
	
	<div class="tengah"><input type="submit" id="btnSave2" title="Simpan" value="Simpan"></div>
	
</fieldset>

</form>