<%@ include file="/include/page/header_jquery.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	// Script khusus halaman ini
	$().ready(function() {
		$( "#btnShowSumXl" ).button({icons: {primary: "ui-icon-print"}});
		var rep = '${rep}';
		//save to excel
		$("#btnShowSumXl").click(function() {
			window.open("${path}/bas/fu_premi.htm?window=savetoexcell&t=report_summary&rep="+$('#rep').val()+"&begdate=" +$('#begdate').val()+ "&enddate=" +$('#enddate').val()+"&admin=" + $('#admin').val()+"&stfu3=" + $('#stfu3').val()+"&cabang=" + $('#cabang').val()+"&kat=" + $('#kat').val()+"&jn_tgl=" + $('#jn_tgl').val(),"", "width=100, height=100");
		});

	});
</script>



<form id="formPost3" name="formPost3" method="post">
<input id="submitType3" name="submitType3" type="hidden">

<input id="begdate" name="begdate" type="hidden" value="${begdate}">
<input id="enddate" name="enddate" type="hidden" value="${enddate}"> 
<input id="stfu3" name="stfu3" type="hidden" value="${stfu3}">
<input id="cabang" name="cabang" type="hidden" value="${cabang}">
<input id="admin" name="admin" type="hidden" value="${admin}">
<input id="kat" name="kat" type="hidden" value="${kat}">
<input id="rep" name="rep" type="hidden" value="${rep}">
<input id="jn_tgl" name="jn_tgl" type="hidden" value="${jn_tgl}">

<fieldset >
	<legend class="ui-widget-header ui-corner-all"><div><b>Summary Total Follow Up User</b></div></legend>
	&nbsp;
	<div class="rowElem">
		<legend >
		<label>Kriteria Tanggal ${kriteria} ${begdate} s/d ${enddate}</label></legend>	
	</div>
	&nbsp;&nbsp;&nbsp;	

	<table id="kontTable" class="jtable" width="100%">
		<thead>
			<tr>
				<th>USER INPUT</th>
				<th>JUMLAH FOLLOW UP</th>				
			</tr>			
		</thead>
		<tbody id="kontBody">
			<c:forEach var="n" items="${listcountuserfu}" varStatus="stat">
			<tr>			
				<td align="center"> ${n.USER_NAME}</td>
				<td align="center">${n.TOTAL_SPAJ}</td>		
			</tr>	
			</c:forEach>
						
		</tbody>
	</table>
	<hr>
	<table id="kontTable" class="jtable" width="100%" >	
			<tr>
				<td align="center">Total <b> ${total}</b> Follow Up</td>				
			</tr>			
	
		
	</table>
	&nbsp;&nbsp;
	<div class="tengah" align="center">
	<button id="btnShowSumXl" title="Show(save Excel)" onclick="return false;" >Show(save Excel)</button>
	</div>
			
</fieldset>
	
</form>