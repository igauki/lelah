<%@ include file="/include/page/taglibs.jsp"%>
<%@ include file="/include/page/header.jsp"%>
<head>
	<jsp:include page="/include/page/header_jquery.jsp">
		<jsp:param value="${path}" name="path"/>
	</jsp:include>
</head>
<body>
<form method="post" name="formpost">
<div id="tabs">
<fieldset>
<legend>Speedy Regional</legend>
	<ul>
		<li><a href="#noSpeedy">Update No Speedy</a></li>
		<li><a href="#statusSpeedy">Update Status Bayar</a></li>
		<li><a href="#reportSpeedy">Report Status Bayar</a></li>
	</ul>
	<div id="noSpeedy">
		<table class="entry2">
			<tr>
				<th>Kota</th>
				<td><input type="text" id="nm_kota" name="nm_kota" maxlength="30" size="30" title="Kota (Silahkan masukkan minimal 3 huruf untuk pencarian Nama Kota)">
					<input type="submit" id="cari" name="cari" value="Cari" onclick="javascript:$('#noSpeedy').tabs({ selected: 1 });"> 
				</td>
			</tr>
		</table>
		<table class="entry2" id="data">
			<tr>
	 			<th align="center"><input type="checkbox" id="checkAll" name="checkAll"></th>
				<th>NAMA</th>
				<th>ALAMAT</th>
				<th>ADMIN</th>
				<th>TELP.</th>
				<th>EMAIL</th>
				<th>AKTIF</th>
				<th>NO. SPEEDY</th>
<!-- 				<th>&nbsp;</th> -->
			</tr>
			<c:forEach items="${daftarCabang}" var="d" varStatus="s">
				<tr>
	 				<td align="center"><input type="checkbox" id="chbox" name="chbox" value="${d.LAR_ID}"></td>
					<td>
						<input type="hidden" id="lar_id_${d.LAR_ID }" name="lar_id_${d.LAR_ID }" value="${d.LAR_ID}">
						<input type="text" id="nama_${d.LAR_ID }" name="nama_${d.LAR_ID }" value="${d.LAR_NAMA}" size="20" readonly="readonly">
					</td>
					<td><input type="text" id="alamat_${d.LAR_ID }" name="alamat_${d.LAR_ID }" value="${d.LAR_ALAMAT}" size="50" readonly="readonly"> </td>
					<td><input type="text" id="admin_${d.LAR_ID }" name="admin_${d.LAR_ID }" value="${d.LAR_ADMIN}" size="30" readonly="readonly"> </td>
					<td><input type="text" id="telp_${d.LAR_ID }" name="telp_${d.LAR_ID }" value="${d.LAR_TELPON}" size="30" readonly="readonly"> </td>
					<td><input type="text" id="email_${d.LAR_ID }" name="email_${d.LAR_ID }" value="${d.LAR_EMAIL}" size="30" readonly="readonly"> </td>
					<td><input type="text" id="aktif_${d.LAR_ID }" name="aktif_${d.LAR_ID }" value="${d.FLAG_AKTIF}" title="1 : Aktif | 0 : Tidak Aktif" max="1" size="2" readonly="readonly"> </td>
					<c:choose>
						<c:when test="${d.LAR_SPEEDY ne null}">
							<td><input type="text" id="lar_speedy_${d.LAR_ID }" name="lar_speedy_${d.LAR_ID }" value="${d.LAR_SPEEDY}" size="30"> </td>
						</c:when>
						<c:otherwise>
							<td><input type="text" id="lar_speedy_${d.LAR_ID }" name="lar_speedy_${d.LAR_ID }" size="30"> </td>
						</c:otherwise>
					</c:choose>
<!-- 					<td><input type="submit" id="update" name="update" value="Update"></td> -->
				</tr>
			</c:forEach>
		</table>
		<input type="submit" id="update" name="update" value="Update">
	</div> <!-- End noSpeedy -->
	<div id="statusSpeedy">
		<table class="entry2">
			<tr>
				<th>Kota</th>
				<td><input type="text" id="nm_kotaSB" name="nm_kotaSB" maxlength="30" size="30" title="Kota (Silahkan masukkan minimal 3 huruf untuk pencarian Nama Kota)">
					<input type="submit" id="cariSB" name="cariSB" value="Cari" onclick="javascript:$('#statusSpeedy').tabs({ selected: 8 });"> 
				</td>
			</tr>
		</table>
		<table class="entry2" id="data">
			<tr>
				<th align="center"><input type="checkbox" id="checkAllSB" name="checkAllSB"></th>
				<th>ALAMAT</th>
				<th>ADMIN</th>
				<th>TELPON</th>
				<th>NO. SPEEDY</th>
				<th>TGL BAYAR</th>
				<th>Status Bayar</th>
				<th>KET</th>
			</tr>
			<c:forEach items="${daftarCabangSB}" var="e">
				<tr>
					<td align="center"><input type="checkbox" id="chboxSB" name="chboxSB" value="${e.LAR_ID}"></td>
					<td><input type="hidden" id="lar_idSB_${e.LAR_ID}" name="lar_idSB_${e.LAR_ID}" value="${e.LAR_ID}">
						<input type="text" id="alamatSB_${e.LAR_ID}" name="alamatSB_${e.LAR_ID}" value="${e.LAR_ALAMAT}" size="50" readonly="readonly"> 
					</td>
					<td><input type="text" id="adminSB_${e.LAR_ID}" name="adminSB_${e.LAR_ID}" value="${e.LAR_ADMIN}" size="30" readonly="readonly"> </td>
					<td><input type="text" id="telpSB_${e.LAR_ID}" name="telpSB_${e.LAR_ID}" value="${e.LAR_TELPON}" size="30" readonly="readonly"> </td>
					<td><input type="text" id="speedySB_${e.LAR_ID}" name="speedySB_${e.LAR_ID}" value="${e.LAR_SPEEDY}" size="30" readonly="readonly"> </td>
					<td><input type="text" id="tglSB_${e.LAR_ID}" name="tglSB_${e.LAR_ID}" class="tgl"> </td>
					<td>
						<select name="statusSB_${e.LAR_ID}" id="statusSB_${e.LAR_ID}">
							<option value="1" selected="selected">Sukses</option>
							<option value="2">Tunda</option>
							<option value="3">Gagal</option>
						</select>
					</td>
					<td><input type="text" id="ketSB_${e.LAR_ID}" name="ketSB_${e.LAR_ID}" size="30"> </td>
				</tr>
			</c:forEach>
		</table>
		<input type="submit" id="updateSB" name="updateSB" value="Update">
	</div> <!-- End statusSpeedy -->
	<div id="reportSpeedy">
		<table class="entry2" id="data">
			<tr>
				<th>Tanggal</th>
				<td>
					<input type="text" id="tglAwal" name="tglAwal" class="tgl">
						s/d
					<input type="text" id="tglAkhir" name="tglAkhir" class="tgl">
				</td>
			</tr>
			<tr>
				<th>Status</th>
				<td>
					<select id="statusRS" name="statusRS">
						<option value="0" selected="selected">All</option>
						<option value="1">Sukses</option>
						<option value="2">Tunda</option>
						<option value="3">Gagal</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
					<input type="submit" name="report" id="report" value="Cetak">
				</td>
			</tr>
		</table>
	</div> <!-- End reportSpeedy -->
	
</fieldset>
</div> <!-- End div tabs -->
</form>

<!-- All script put here -->
<script type="text/javascript">
	$(document).ready(function(){
		// Check all
		$('#checkAll').click(function () {   
			$('input:checkbox').attr('checked', this.checked);    
		});
		$('#checkAllSB').click(function () {   
			$('input:checkbox').attr('checked', this.checked);    
		});
		
		// (jQueryUI Tabs) init tab2 Utama
		$("#tabs").tabs();
		var ind = '${tabs}';
		if(ind!=null && ind!=''){
			$("#tabs").tabs({selected: ind});
		}
		
		
		$(".tgl").datepicker({
			changeMonth: true,
			changeYear: true
		});
		
		//autocomplete kota
		$( "#nm_kota" ).autocomplete({
			minLength: 3,
			source: "${path}/bas/hadiah.htm?window=json&t=kota",
			focus: function( event, ui ) {
				$( "#nm_kota" ).val( ui.item.key );
				return false;
			},
			select: function( event, ui ) {
				$( "#nm_kota" ).val( ui.item.key);
				return false;
			}
		})
		.data( "autocomplete" )._renderItem = function( ul, item ) {
			return $( "<li></li>" )
				.data( "item.autocomplete", item )
				.append( "<a>" + item.key + "</a>" )
				.appendTo( ul );
		};	
		
		$( "#nm_kotaSB" ).autocomplete({
			minLength: 3,
			source: "${path}/bas/hadiah.htm?window=json&t=kota",
			focus: function( event, ui ) {
				$( "#nm_kotaSB" ).val( ui.item.key );
				return false;
			},
			select: function( event, ui ) {
				$( "#nm_kotaSB" ).val( ui.item.key);
				return false;
			}
		})
		.data( "autocomplete" )._renderItem = function( ul, item ) {
			return $( "<li></li>" )
				.data( "item.autocomplete", item )
				.append( "<a>" + item.key + "</a>" )
				.appendTo( ul );
		};	
		
		var pesan = '${pesan}';
		if(pesan!='' && pesan!=null){
			alert(pesan);
		}
	});
</script>
</body>