<%@ include file="/include/page/header.jsp"%>
<script>
	function awal(){
		var info="${cmd.info}"	;
		if(info=='0'){
			alert("Proses Transfer Berhasil");
		}else if(info=='1'){
			alert("Surat Konfirmasi Belum Dicetak. TIDAK BISA DITRANSFER");
		}else if(info=='2'){
			alert("Reinstate Polis ini di tolak.TIDAK BISA DITRANSFER");
		}else if(info=='3'){
			alert("Proses REAS belum dijalankan ");
		}else if(info=='4'){
			alert("Tanggal Batas Pembayaran Belum di INPUT ");
		}else if(info=='5'){
			alert("Posisi Polis tidak ada Pada UW reinstate");
		}else if(info=='6'){
			alert("Tanggal SPAJ Doc belum dimasukkan.");
		}
		window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
	}
	
</script>
<body onload="awal();">
	<form>
		<table>
			<tr>
				<td>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
								ERROR:<br>
									<c:forEach var="error" items="${status.errorMessages}">
												- <c:out value="${error}" escapeXml="false" />
										<br/>
									</c:forEach>
							</div>
						</c:if>									
					</spring:bind>
		          
				</td>
			</tr>
			
		</table>
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>