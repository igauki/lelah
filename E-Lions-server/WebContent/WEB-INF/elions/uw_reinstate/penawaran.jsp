<%@ include file="/include/page/header.jsp"%>
<script type="text/javascript">
	function tampilkan2(forminput, report, format,flag){
		var em = document.getElementById('em').value;
		var premi = document.getElementById('premi').value;
		var email = document.getElementById('email').value;
		var spaj = document.getElementById('spaj').value;;
		//alert(em + ' ' + premi + ' ' + email);
		document.getElementById('show').value = format;
		//document.getElementById(forminput).action='${path}/report/uw.'+format+'?window='+report;
		//popWin('${path}/report/uw.'+format+'?window='+report+'&spaj='+spaj+'&em='+em+'&premi='+premi,500,800);
		
		if(flag != null) {
			document.getElementById('send').value = flag;
			document.getElementById(forminput).submit();
		}
		else 
			popWin('${path}/report/uw.'+format+'?window='+report+'&spaj='+spaj+'&em='+em+'&premi='+premi,600,800);
		
		return false;
	}
</script>	 
<body style="height: 100%;">
	<div id="contents">
		<c:if test="${not empty success}">
			<script>
				alert("${success}");
			</script>
		</c:if>
		<fieldset>
			<legend>Surat Penawaran</legend>
			<form name="formpost" id="formpost" method="post" action="reinstate.htm?window=penawaran">
				<table class="entry2">
					<tr>
						<th style="width: 150px; " valign="top">EM</th>
						<td>
							<input type="text" name="em" value="${em}" id="em" size="4"> %
						</td>
					</tr>
					<tr>
						<th style="width: 150px; " valign="top">Premi</th>
						<td>
							<input type="text" name="premi" value="${premi}" id="premi">
						</td>
					</tr>
					<tr>
						<th style="width: 150px; " valign="top">Email PP/TTG</th>
						<td>
							<input type="text" name="email" value="${email}" id="email">
						</td>
					</tr>
					<tr>
						<th>&nbsp;</th>
						<td>
							<input type="hidden" name="lampiran" id="lampiran">
							<a href="#" onclick="return tampilkan2('formpost', 'surat_penawaran', 'html');"
								onmouseover="return overlib('VIEW', AUTOSTATUS, WRAP);" onmouseout="nd();">
								<img style="border:none;" alt="HTML" src="${path}/include/image/page_html.gif"/></a>
							<a href="#" onclick="return tampilkan2('formpost', 'surat_penawaran', 'pdf');"
								onmouseover="return overlib('PRINT', AUTOSTATUS, WRAP);" onmouseout="nd();">
								<img style="border:none;" alt="PDF" src="${path}/include/image/pdf.gif"/></a>
							<input type="button" name="sendPDF" value="send pdf" onclick="return tampilkan2('formpost', 'surat_penawaran', 'pdf', 'sendPDF');">	
							<input type="hidden" name="show" id="show">
							<input type="hidden" name="send" id="send">
							<input type="hidden" name="spaj" id="spaj" value="${spaj}">
						</td>
					</tr>
				</table>
			</form>
		</fieldset>	
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>
