<%@ include file="/include/page/header.jsp"%>
<script>
	function awal(){
		var info='${cmd.info}';
		if(info=='1')
			alert("Polis Sudah di REAS. Tidak Bisa Back to Reinstate.");
		else if(info=='2')
			alert("Polis sudah dilakukan Back to reinstate");
		else
			alert("Proses Back to Reinstate Berhasil");
		window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';			
	}	
	
</script>
<body onload="awal();">
	<form>
		<table>
				<tr>
					<td colspan="2">
						<c:if test="${not empty submitSuccess }">
				        	<div id="success">
					        	Berhasil
				        	</div>
				        </c:if>	
			  			<spring:bind path="cmd.*">
							<c:if test="${not empty status.errorMessages}">
								<div id="error">
									ERROR:<br>
										<c:forEach var="error" items="${status.errorMessages}">
													- <c:out value="${error}" escapeXml="false" />
											<br/>
										</c:forEach>
								</div>
							</c:if>									
						</spring:bind>
			          
					</td>
				</tr>
		</table>	
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>