<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/taglibs.jsp"%>
<c:set var="path" value="${pageContext.request.contextPath}" />
<html>
<head>
<title>PT. Asuransi Jiwa Sinarmas MSIG</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta NAME="Description" CONTENT="EkaLife">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<link REL="Stylesheet" TYPE="text/css" HREF="${path }/include/css/default.css" media="screen">
<link href="${path}/include/js/aw/runtime/styles/xp/grid.css" rel="stylesheet" type="text/css" ><!-- Active Widgets -->
<link href="${path}/include/image/eas.ico" rel="shortcut icon">
<script type="text/javascript" src="${path }/include/js/default.js"></script><!-- Common Javascripts -->
<script type="text/javascript" src="${path }/dwr/util.js"></script><!-- DWR (Ajax) Utils -->
<script type="text/javascript" src="${path }/include/js/ajaxtags/overlibmws.js"></script>

<script>
hideLoadingMessage();
	function backToParent(spaj,nopolis, reinsNo){
		
		if(self.opener.document.getElementById('infoFrame') && self.opener.document.formpost.nomor){
			self.opener.document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj; 
			addOptionToSelect(self.opener.document, self.opener.formpost.nomor, nopolis + " -- " + reinsNo, spaj+"-"+nopolis+"~"+reinsNo);
				
		}else if(self.opener.document.formCari.txtnospaj){
			self.opener.document.frm_search.txtnospaj.value=spaj;
		}
		
		window.close();	
	}
	
	function cari(){
		if(trim(document.formpost.kata.value)=='') 
			return false;
		else 
			createLoadingMessage();	
	}		
</script>
<BODY onload="document.formpost.kata.select(); document.title='PopUp :: Cari Reinstate';  setupPanes('container1', 'tab1'); document.formpost.kata.select(); " style="height: 100%;">
<div class="tab-container" id="container1">
	<ul class="tabs">
		<li>
			<a href="#" onClick="return showPane('pane1', this)" id="tab1">Cari Reinstate</a>
		</li>
	</ul>
	<div class="tab-panes">
		<div id="pane1" class="panes">
			<form method="post" name="formpost" action="${path }/uw/cari_reinstate.htm?window=cari">
					<input type="hidden" name="posisi" value="${param.posisi}">
					<table class="entry2">
						<tr>
							<th>Cari:</th>
							<th>
							<script>
							</script>
								<select name="kategori">
									<option value="1" <c:if test="${param.kategori eq \"1\" }">selected</c:if>>No. Polis</option>
									<option value="2" <c:if test="${param.kategori eq \"2\" }">selected</c:if>>No. Reinstate</option>
								</select>
								<input type="text" name="kata" size="50" value="${param.kata }" onkeypress="if(event.keyCode==13) {document.formpost.search.click(); return false;}">
								<input type="submit" name="search" value="Search"onClick="cari();" accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();" >
							</th>
						</tr>
					</table>
					<table class="simple">
						<thead>
							<tr>
								<th>Nama</th>
								<th>No. SPAJ</th>
								<th>No. Polis</th>
								<th>No. Reinstate</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="spaj" items="${cmd.listSpaj}" varStatus="stat">
								<tr onMouseOver="Javascript:this.bgColor='#FFFFCC';this.style.cursor='hand';return true;"	onMouseOut="Javascript:this.bgColor='#FFFFFF';return true;"
									onclick="backToParent('${spaj.noSpaj}','${spaj.poPolicyNo }', '${spaj.reinsNo }');">
									<td>${spaj.name}</td>
									<td><elions:spaj nomor="${spaj.noSpaj }"/></td>
									<td><elions:polis nomor="${spaj.poPolicyNo }"/></td>
									<td><elions:spaj nomor="${spaj.reinsNo }"/></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<br>
					<input type="button" name="close" value="Close" onclick="window.close();"
						accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();">
			</form>
			<c:if test="${jml eq 1}">
				<script>backToParent('${v1}', '${v2}' , '${v3}');</script>
			</c:if>
		</div>
	</div>	
</div>	
</body>
<%@ include file="/include/page/footer.jsp"%>