<%@ include file="/include/page/header.jsp"%>
<body style="height: 100%;">
<script>
	function ref(){
		formpost.flag.value=1;
		formpost.submit();
	}
</script>
	<div id="contents">
	<form:form commandName="cmd" id="formpost" method="post">
		<c:if test="${cmd.lsReinstatement[0] ne null}" >
		<table class="entry2">
			<tr>
				<th colspan="3">
					UW  WORK SHEET  -  REINSTATEMENT  POLICY
					
				</th>
			</tr>
			<tr>
				<td colspan="3">
					<fieldset>
						<legend>Tanggal Bayar</legend>
						<table class="entry2">
							<tr>
								<td>
									Tanggal Pembayaran
									<script>inputDate('tglBayar', '${cmd.tglBayar}', false, '', 9);</script>
									<input type="button" name="refresh" value="refresh" onClick="ref();">										
								</td>
							</tr>
						</table>
					</fieldset>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<fieldset>
						<legend>Tanggal</legend>
						<table class="entry2">
							<tr>
								<td>
									Tanggal Terima dari Life Benefit
										<script>inputDate('mste_tgl_terima_LB', '${cmd.lsReinstatement[0].mste_tgl_terima_LB}', false, '', 9);</script>
									
									
								</td>
								<td align="right" >
									Tanggal Kirim ke Life Benefit
										<script>inputDate('mste_tgl_kirim_LB', '${cmd.lsReinstatement[0].mste_tgl_kirim_LB}', false, '', 9);</script>
									
								</td>
							</tr>
						</table>
					</fieldset>
				</td>
			</tr>
			<tr>
				<th colspan="3">
					<fieldset>
						<legend>Data Polis</legend>
						<table class="entry2">
							<tr>
								<th width="30%">No. Polis</th>
								<td width="3%">:</td>
								<td width="67%">${cmd.lsReinstatement[0].mspo_policy_no_format}</td>
							</tr>
							<tr>
								<th width="30%">Produk</th>
								<td width="1%">:</td>
								<td width="67%">${cmd.lsReinstatement[0].lsdbs_name}</td>
							</tr>
							<c:forEach items="${cmd.lsReinstatement}" var="x" varStatus="xt">
								<c:choose>
									<c:when test="${x.no==null || x.no==1}" >
										<tr>
											<th width="30%">Rider</th>
											<td width="1%">:</td>
											<td width="67%">
												<c:if test="${x.rider_name ==null}">-</c:if>
												<c:if test="${x.rider_name !=null}">${x.rider_name}</c:if>
											</td>
										</tr>
									</c:when>
									<c:when test="${x.no>1}" >
										<th width="30%"></th>
										<td width="1%"></td>
										<td width="67%">${x.rider_name}</td>
									</c:when>
								</c:choose>	
							</c:forEach>
							<tr>
								<th width="30%">Uang Pertanggungan</th>
								<td width="1%">:</td>
								<td width="67%">	
									${cmd.lsReinstatement[0].lku_symbol}
									<fmt:formatNumber value="${cmd.lsReinstatement[0].mspr_tsi}" 
										type="currency" currencySymbol="" maxFractionDigits="2" minFractionDigits="0" />								
								</td>	
							</tr>
							<tr>
								<th width="30%">Premi</th>
								<td width="1%">:</td>
								<td width="67%">${cmd.lsReinstatement[0].lku_symbol}
									<fmt:formatNumber value="${cmd.lsReinstatement[0].mspr_premium}" 
										type="currency" currencySymbol="" maxFractionDigits="2" minFractionDigits="0" />								
								</td>
							</tr>
							<tr>
								<th width="30%">Nama Pemegang Polis</th>
								<td width="1%">:</td>
								<td width="67%">${cmd.lsReinstatement[0].nama_pp}</td>
							</tr>
							<tr>
								<th width="30%">Tgl.Lahir PP ( usia )</th>
								<td width="1%">:</td>
								<td width="67%">${cmd.lsReinstatement[0].ttl_pp} (${cmd.lsReinstatement[0].mspo_age})</td>
							</tr>
							<tr>
								<th width="30%">Nama Tertanggung</th>
								<td width="1%">:</td>
								<td width="67%">${cmd.lsReinstatement[0].nama_tt}</td>
							</tr>
							<tr>
								<th width="30%">Tgl. Lahir TTG ( usia )</th>
								<td width="1%">:</td>
								<td width="67%">${cmd.lsReinstatement[0].ttl_tt} (${cmd.lsReinstatement[0].mste_age})</td>
							</tr>
							<tr>
								<th width="30%">Masa Asuransi</th>
								<td width="1%">:</td>
								<td width="67%">${cmd.lsReinstatement[0].mspo_ins_period} tahun</td>
							</tr>
							<tr>
								<th width="30%">Masa Pembayaran Premi</th>
								<td width="1%">:</td>
								<td width="67%">${cmd.lsReinstatement[0].mspo_pay_period} tahun</td>
							</tr>
							<tr>
								<th width="30%">Tanggal Polis Lapse</th>
								<td width="1%">:</td>
								<td width="67%">
									<fmt:formatDate value="${cmd.lsReinstatement[0].tgl_lapse}" pattern="dd/MM/yyyy"/>
								</td>
							</tr>
							<tr>
								<th width="30%">Lama Polis Lapse</th>
								<td width="1%">:</td>
								<td width="67%">
								${cmd.lsReinstatement[0].lama_lapse} Hari  (${cmd.year} Tahun ${cmd.month} Bulan  ${cmd.day} Hari)
								</td>
							</tr>
							<tr>
								<th width="30%">Keputusan UW yang lalu</th>
								<td width="1%">:</td>
								<td width="67%">
								<form:textarea path="lsReinstatement[0].put_uw_old" cols="70" rows="2" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "/>
								</td>
							</tr>
						</table>
					</fieldset>
				</th>
			</tr>				
			<tr>
				<td width="30%">
					<fieldset>
						<legend>Komentar</legend>
						<table class="entry2">
							<tr>
								<td>
									<form:textarea path="lsReinstatement[0].put_uw_new" cols="70" rows="10" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "/>
								</td>
							</tr>
						</table>
					</fieldset>
					
				</td>
				<td width="30%">
					<fieldset>
						<legend>Keputusan UW </legend>
						<table class="entry3">
							<tr>
								<td>
									<form:textarea path="lsReinstatement[0].put_uw_kep" cols="70" rows="10" onkeyup="textCounter(this, 200); " onkeydown="textCounter(this, 200); "/>
								</td>
							</tr>
						</table>
					</fieldset>
					
				</td>
				<td colspan="2">
				</td>
			</tr>
			<tr> 
		        <th nowrap="nowrap" colspan="4">
				    <input type="hidden" name="reg_spaj" value="${cmd.reg_spaj}" >
				    <input type="hidden" name="flag" value="0" >
				    <input type="submit" name="Ok" value="Save" >
				    <input type="button" name="Cancel" value="Cancel" >
		        	
		        	<c:if test="${not empty submitSuccess }">
			        	<div id="success">
				        	<script type="text/javascript">
				        		alert("Telah Berhasil di Update, Silahkan Cetak WorkSheet tsb.");
								window.location="${path}/report/uw.htm?window=reportReinstatementWorkSheet&nospaj=${cmd.reg_spaj}&lamaLapse=${cmd.lsReinstatement[0].lama_lapse}&tglBayar=${cmd.tglBayar}";
				        	</script>
			        	</div>
			        </c:if>	
				</th>
    	  	</tr>
    	  	<tr>
    	  		<td colspan="3">
    	  			<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
								info:<br>
									<c:forEach var="error" items="${status.errorMessages}">
												- <c:out value="${error}" escapeXml="false" />
										<br/>
									</c:forEach>
							</div>
						</c:if>									
					</spring:bind>
    	  		</td>
    	  	</tr>
		</table>
		</c:if>
		<c:if test="${cmd.lsReinstatement[0] eq null}" >
			<script>
				alert("Polis Tidak dapat ditampilkan, silahkan hubungi EDP");
			</script>
		</c:if>
		
	</form:form>
	</div>
</body>
<%@ include file="/include/page/footer.jsp"%>