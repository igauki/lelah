<%@ include file="/include/page/header.jsp"%>
<script>
	function awal(){
		var info="${cmd.info}"	;
		if(info=='0'){
			alert("Proses Reas Berhasil");
		}else if(info=='1'){
			alert("Polis dengan Spaj (${cmd.spaj}) ini NON-REAS");
		}else if(info=='2'){
			alert("Reas Polis Ini Masih Inforce");
		}else if(info=='3'){
			alert("Polis ini di posisi MATURITY");
		}else if(info=='4'){
			alert("Posisi Polis ini ada di ${cmd.posisi}");
		}
		window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';	
	}
	
</script>
<body onload="awal();">
	<form>
		<table>
			<tr>
				<td>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
								ERROR:<br>
									<c:forEach var="error" items="${status.errorMessages}">
												- <c:out value="${error}" escapeXml="false" />
										<br/>
									</c:forEach>
							</div>
						</c:if>									
					</spring:bind>
		          
				</td>
			</tr>
			
		</table>
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>