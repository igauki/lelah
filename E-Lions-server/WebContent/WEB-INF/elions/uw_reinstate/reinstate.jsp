<%@ include file="/include/page/header.jsp"%>
<style type="text/css">
	html,body{
	     overflow: hidden;
	} 
</style>
<script>
	function tampil(nomor){
		bts=nomor.indexOf("-");
		spaj=nomor.substring(0,bts);
		bts=nomor.indexOf("~");
		reins=nomor.substring(bts+1,nomor.length);
		if(document.getElementById('infoFrame')){
			document.getElementById('infoFrame').src='${path }/uw/view.htm?showSPAJ='+spaj+'&reins='+reins;	
			
		}
		if(document.getElementById('docFrame')) document.getElementById('docFrame').src='${path}/common/util.htm?window=doc_list&spaj='+spaj;
	}
	

	function cari(){
		popWin('${path}/uw/cari_reinstate.htm?window=cari', 350, 450);
	}
	
	function reas(){
	
		if(confirm("Anda Ingin Proses Reas Polis ini?")){
			var nomor=document.formpost.nomor.options[document.formpost.nomor.selectedIndex].value;
			document.getElementById('infoFrame').src='${path }/uw/reins_reas.htm?nomor='+nomor;
		}
	}

	function back(){
		if(confirm("Anda Ingin Back to Reinstate?")){
			var nomor=document.formpost.nomor.options[document.formpost.nomor.selectedIndex].value;
			document.getElementById('infoFrame').src='${path }/uw/backtoreins.htm?window=back_to_reins&nomor='+nomor;
		}
	}
	
	function transfer(){
		if(confirm("Anda Yakin Tranfer Ke Pembayaran ?")){
			var nomor=document.formpost.nomor.options[document.formpost.nomor.selectedIndex].value;
			document.getElementById('infoFrame').src='${path }/uw/transreins.htm?window=transfer&nomor='+nomor;
					
		}	
	}

	function ubah(){
		document.getElementById('infoFrame').src="${path}/uw/edit.htm?nomor="+document.formpost.nomor.options[document.formpost.nomor.selectedIndex].value+"&info=1"
				
	}

	function awal(){
		setFrameSize('infoFrame', 90);
		setFrameSize('docFrame', 90);
		tampil(document.formpost.nomor.options[document.formpost.nomor.selectedIndex].value);
	
	}
	
	function tekan(btn){
		nomor=document.formpost.nomor.options[document.formpost.nomor.selectedIndex].value;
		bts=nomor.indexOf("-");
		nospaj=nomor.substring(0,bts);

		if(btn=='c'){
			popWin('${path}/uw/cari_reinstate.htm?window=cari', 350, 450);
		}else if(btn=='v'){
			if(nospaj!=''){
				document.getElementById('infoFrame').src="${path}/uw_reinstate/reinstatement_work_sheet.htm?reg_spaj="+nospaj;
			}else{
				alert("Silahkan cari spaj Terlebih Dahulu..");
				popWin('${path}/uw/cari_reinstate.htm?window=cari', 350, 450);
			}	
		}else if(btn=='further'){
			if(nospaj!=''){
				document.getElementById('infoFrame').src='${path }/report/uw.htm?window=reportFurtherRequirement';
			}else{
				alert("Silahkan cari spaj Terlebih Dahulu..");
				popWin('${path}/uw/cari_reinstate.htm?window=cari', 350, 450);
			}	
		}else if(btn=='bancass'){
            if(nospaj!=''){
                document.getElementById('infoFrame').src='${path }/report/uw.htm?window=reportFurtherRBanca';
            }else{
                alert("Silahkan cari spaj Terlebih Dahulu..");
                popWin('${path}/uw/cari_reinstate.htm?window=cari', 350, 450);
            }   
		}else if(btn=='s'){
            if(nospaj!=''){
                document.getElementById('infoFrame').src="${path}/report/uw.htm?window=suratKonfirmasiPemulihanPolis&nomor="+nomor;
            }else{
                alert("Silahkan cari spaj Terlebih Dahulu..");
                popWin('${path}/uw/cari_reinstate.htm?window=cari', 350, 450);
            }   
	   }else if(btn=='tanggal_spaj_doc'){
			if(nospaj!=''){
				//alert('Mohon mangap. Menu ini belum dapat digunakan.');
				//return false;
				document.getElementById('infoFrame').src="${path}/uw/reinstate.htm?window=update_tanggal&flag=1&nomor="+nomor;
			}else{
				alert("Silahkan cari spaj Terlebih Dahulu..");
				popWin('${path}/uw/cari_reinstate.htm?window=cari', 350, 450);
			}
		}else if(btn=='claim'){
			if(nospaj!=''){
				popWin('${path}/uw/uw.htm?window=healthClaim&spaj='+nospaj, 400, 900,'yes','yes','yes');
			}else{
				alert("Silahkan cari spaj Terlebih Dahulu..");
				popWin('${path}/uw/cari_reinstate.htm?window=cari', 350, 450);
			}
			
		}else if(btn=='penawaran'){
			if(nospaj!=''){
				//popWin('${path}/uw/uw.htm?window=penawaran&spaj='+nospaj, 400, 900,'yes','yes','yes');
				document.getElementById('infoFrame').src='${path}/uw/reinstate.htm?window=penawaran&spaj='+nospaj;	
			}else{
				alert("Silahkan cari spaj Terlebih Dahulu..");
				popWin('${path}/uw/cari_reinstate.htm?window=cari', 350, 450);
			}
			
		}else if('akum_new'==btn) {
			document.getElementById('infoFrame').src='${path}/uw/viewer.htm?window=akum_new';
		}else if(btn=='back_to_lb') {
			document.getElementById('infoFrame').src='${path}/uw/uw.htm?window=back_to_lb&flag=1';
		}
	}
	
</script>

<body onload="awal();" onresize="setFrameSize('infoFrame', 90); setFrameSize('docFrame', 90);" style="height: 100%;">
<form name="formpost" method="post">
<div class="tabcontent">
<table class="entry2" width="98%">
	<tr>
		<th>Cari SPAJ </th>
		<td>
			<select name="nomor" >
				<c:forEach var="s" items="${cmd.daftarSpaj}">
				<option value="${s.noSpaj }-${s.poPolicyNo}~${s.reinsNo}" >
					<elions:polis nomor="${s.poPolicyNo}"/> -- <elions:spaj nomor="${s.reinsNo}"/></option>
				</c:forEach>
			</select>
			<input type="button" value="Tampilkan Info" name="info"
				accesskey="I" onmouseover="return overlib('Alt-I', AUTOSTATUS, WRAP);" onmouseout="nd();"
				onclick="tampil(document.formpost.nomor.options[document.formpost.nomor.selectedIndex].value);">
			<input type="button" value="Cari SPAJ" name="search"
				accesskey="C" onmouseover="return overlib('Alt-C', AUTOSTATUS, WRAP);" onmouseout="nd();" onclick="cari() "> <!-- &search=yes -->
				
		</td>
	</tr>
	<tr>
		<th>Proses</th>
		<td>
			<input name="flagSurat" type="hidden" value="${flagSurat}">
			<input name="btn_reas" type="button"  
			value="Reas" onClick="reas()" accesskey="R" onmouseover="return overlib('Alt-R', AUTOSTATUS, WRAP);" onmouseout="nd();" >
			<input name="btn_back" accesskey="B" onmouseover="return overlib('Alt-B', AUTOSTATUS, WRAP);" onmouseout="nd();" 
				type="button" value="Back To Reinstate" onClick="back()">
			<input name="btn_transfer" accesskey="T" onmouseover="return overlib('Alt-T', AUTOSTATUS, WRAP);" onmouseout="nd();"
				 type="button" value="Transfer" onClick="transfer()">
			<input name="btn_surat" accesskey="S" onmouseover="return overlib('Alt-S', AUTOSTATUS, WRAP);" onmouseout="nd();"
				type="button" value="Surat" onclick="tekan('s');">
			<input name="btn_edit" accesskey="E" onmouseover="return overlib('Alt-E', AUTOSTATUS, WRAP);" onmouseout="nd();"
				type="button" value="Edit" onClick="ubah()">
			<input type="button" value="View Work Sheet" name="view"
				onclick="tekan('v');"
				accesskey="P" onmouseover="return overlib('Alt-U', AUTOSTATUS, WRAP);" onmouseout="nd();">
			<input type="button" value="TGL REINST DOC" name="tanggal_spaj_doc" onclick="tekan('tanggal_spaj_doc');">
			<input type="button" value="View Kesehatan" name="claim" 
				onclick="tekan('claim');" style="width: 120px;">
			<input type="button" value="Surat Penawaran" name="penawaran" 
				onclick="tekan('penawaran');" style="width: 120px;">
			<input type="button" value="Further Req" name="further" 
                onclick="tekan('further');" style="width: 100px;">
            <input type="button" value="Further Req(Bancass)" name="bancass" 
                onclick="tekan('bancass');" style="width: 140px;">
            <input type="button" value="Akum New" name="akum" ${viewDokumenPolisNew}
				onclick="tekan('akum_new');" style="width: 80px;">
			<input type="button" value="Back To Life Benefit" name="back_to_lb" 
				onclick="tekan('back_to_lb');" style="width: 140px;">
		</td>
	</tr>

	<tr>
		<td colspan="2">
			<c:if test="${not empty submitSuccess }">
	        	<div id="success">
		        	Berhasil
	        	</div>
	        </c:if>	
  			<spring:bind path="cmd.*">
				<c:if test="${not empty status.errorMessages}">
					<div id="error">
						Informasi:<br>
							<c:forEach var="error" items="${status.errorMessages}">
										- <c:out value="${error}" escapeXml="false" />
								<br/>
							</c:forEach>
					</div>
				</c:if>									
			</spring:bind>
          
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<table style="margin: 0 0 0 0; padding: 0 0 0 0; width: 100%;">
				<tr>
					<c:choose>
						<c:when test="${sessionScope.currentUser.wideScreen}">
							<td style="width: 60%;">
								<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
									width="100%"  height="100%"> Please Wait... </iframe>
							</td>
							<td style="width: 40%;">
								<iframe src="" name="docFrame" id="docFrame" width="100%">E-Lions</iframe>
							</td>
						</c:when>
						<c:otherwise>
							<td>
								<iframe src="${path}/uw/view.htm" name="infoFrame" id="infoFrame"
									width="100%"  height="100%"> Please Wait... </iframe>
							</td>
						</c:otherwise>
					</c:choose>
				</tr>
			</table>		
		</td>	
	</tr>
</table>
</div>
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>