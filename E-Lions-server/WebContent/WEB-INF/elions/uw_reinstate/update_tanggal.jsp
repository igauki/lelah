<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/include/page/header.jsp"%>
<script>
</script>
<body>
	<form method="post" name="formpost">
		<table class="entry2" style="width: auto;">
			<tr>
				<th>
					Edit Tanggal Spaj Doc
				</th>
				<th>
					<script>inputDate('tanggal', '${tanggal}', false, '', 9);</script>
				</th>
			</tr>
			<tr>
				<th>
					Waktu
				</th>
				<th style="text-align: left">
					<select name="hh">
						<c:forEach items="${listHH}" var="h">
							<option value="${h}" <c:if test="${h eq hh}">selected</c:if>>
								${h}
							</option>
						</c:forEach>
					</select>
					:
					<select name="mm">
						<c:forEach items="${listMM}" var="m">
							<option value="${m}" <c:if test="${m eq mm}">selected</c:if>>
								${m}
							</option>
						</c:forEach>
					</select>
				</th>
			</tr>
			<tr>
				<td colspan="2">
					<c:if test="${not empty pesan}">
						<div id="success">
							${pesan }
						</div>
					</c:if>
					<c:if test="${not empty pesanError}">
						<div id="error">
							Informasi:
							<br>
							<c:forEach var="error" items="${lsError}">
								 - <c:out value="${error}" escapeXml="false" />
								<br />
							</c:forEach>
						</div>
					</c:if>
				</td>
			</tr>
			<tr>
				<th colspan="2">
					<input type="submit" name="save" value="Save">
					<input type="hidden" name="flag" value="${flag}">
				</th>
			</tr>
		</table>
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>