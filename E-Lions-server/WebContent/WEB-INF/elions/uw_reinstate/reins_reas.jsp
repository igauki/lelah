<%@ include file="/include/page/header.jsp"%>
<script>
	function awal(){
		var flag="${cmd.flag}";
		if(flag=="1"){
			formPost.submit();
		}
		
	}
</script>
<body onload="awal();">
	<form name="formPost" method="post">
		<table>
			<tr>
				<td>
						<c:if test="${empty status.errorMessages}">
							<c:choose>
							<c:when test="${not empty submitSuccess }">
								<div id="success">
					        		Proses Reas Berhasil. ${pesan}
				        		</div>
				        	</c:when>	
							<c:otherwise>
								<div id="success">Silahkan Tunggu... Sedang dilakukan Proses Reas...</div>
				        	</c:otherwise>
				        	</c:choose>
						</c:if>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
								ERROR:<br>
									<c:forEach var="error" items="${status.errorMessages}">
												- <c:out value="${error}" escapeXml="false" />
										<br/>
									</c:forEach>
							</div>
						</c:if>									
					</spring:bind>
		          
				</td>
			</tr>
			
		</table>
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>