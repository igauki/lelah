<%@ include file="/include/page/header.jsp"%>
<script>
	function awal(){
		var info="${cmd.info}"	;
		if(info=='1'){
			alert("Anda Tidak Mempunyai Akses.");
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';
		}else if(info=='2'){
			alert("Password Salah");
			formpost.inpass.focus();
			formpost.inpass.select();
		}else if(info=='3'){
			formpost.desc.select();
			alert("Alasan Tidak Boleh Kosong");
		}else if(info=='4'){
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';
			alert("Surat Konfirmasi Telah Berhasil Di edit ");
		}
	}
	function proses(){
		formpost.flag.value="1";
		formpost.submit();
	}
	
</script>
<body onload="awal();">
	<form name="formpost" method="post" >
		<table>
			<tr>
				<th>Password</th>
				<td><input type="password" name="inpass" value=""></td>
			</tr>
			<tr>
				<th>Alasan</th>
				<td><textarea name="desc" cols="50" rows="3"  ></textarea></td>
			</tr>
			<tr>
				<td><input type="button" name="btbok" value="Ok" onclick="proses();"></td>
				<td><input type="hidden" name="flag" value="0"></td>
			</tr>
			<tr>
				<td colspan="2">
					<spring:bind path="cmd.*">
						<c:if test="${not empty status.errorMessages}">
							<div id="error">
								ERROR:<br>
									<c:forEach var="error" items="${status.errorMessages}">
												- <c:out value="${error}" escapeXml="false" />
										<br/>
									</c:forEach>
							</div>
						</c:if>									
					</spring:bind>
		          
				</td>
			</tr>
			
		</table>
	</form>
</body>
<%@ include file="/include/page/footer.jsp"%>