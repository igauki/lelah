<%@ include file="/include/page/header.jsp"%>
<script>
	function aksep(){
		val=document.frm_post.selectAksep.value;
		//document.frm_post.terima.type=hidden;
		kondisi()
		
		if(val==1){//terima
			document.frm_post.pesanTolak.value="-";
			document.frm_post.pesanTerima.value="Diterima";
			document.frm_post.selectKondisi.disabled=false;
			//document.frm_post.terima.value="efektif sejak tanggal (dd-mm-yyyy)  "+document.frm_post.tgl_terima.value+
			//" dengan kondisi sebagai berikut :";
			document.frm_post.terima1.value="";
			document.frm_post.selectKondisi.disabled=false;
			document.frm_post.tolakNote.readOnly=true;
			document.frm_post.terima1.value="Efektif sejak tanggal ";
			//document.frm_post.tglAksep.disabled=false;
			document.frm_post.terima2.value="dengan kondisi sebagai berikut :";
			//document.frm_post.tglAksep.value=document.frm_post.tgl_terima.value;
			document.frm_post.tolakNote.value="-"
			
		}else if(val==0){ //tolak
			document.frm_post.terima1.value="-";
			document.frm_post.terima2.value="-";
			//document.frm_post.tglAksep.value="DD/MM/yyyy";
			//document.frm_post.tglAksep.disabled=true;
			document.frm_post.pesanTolak.value="Ditolak";
			document.frm_post.selectKondisi.disabled=true;
			document.frm_post.pesanTerima.value="-";
			document.frm_post.tolakNote.readOnly=false;
			document.frm_post.tolakNote.value=document.frm_post.tolakNoteTemp.value
			document.frm_post.terimaNote.value="-";
/*			Calendar.setup({
			        inputField     :    "startDate",
			        ifFormat       :    "%d/%m/%Y",
			        button         :    "_startDate",
			        align          :    "Tl"
			});*/
			
		}
	}
	function kondisi(){
		if(document.frm_post.selectKondisi.value==0){//kondisi semula
			document.frm_post.terimaNote.readOnly=true;
			document.frm_post.terimaNote.value="-";
		}else if(document.frm_post.selectKondisi.value==1){//kondisi khusus
			document.frm_post.terimaNote.readOnly=false;
			document.frm_post.terimaNote.value=document.frm_post.terimaNoteTemp.value;			
		}
		
	}
	
	function awal(){
		var info='${cmd.flagAdd}';
		if(info=='1'){
			frm_post.btn_ok.disabled=true;
			frm_post.btn_cancel.disabled=true;
			frm_post.btn_save.disabled=true;
			if(confirm("Surat Konfirmasi Sudah Di Print \n Edit Ulang?")){
				window.location='${path }/uw/transreins.htm?window=cetakulang&spaj=${cmd.spaj}&reins=${cmd.reins}';
			}else{
				window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';
			}	
			
		}else if(info=='2'){
			alert("Posisi Polis tidak ada di UW reinstate");
			window.location='${path }/uw/view.htm?showSPAJ=${cmd.spaj}';
		}else{
			if(document.frm_post.selectAksep.value==0){//ditolak
				document.frm_post.terima1.value="-";
				document.frm_post.terima2.value="-";
				//document.frm_post.tglAksep.value="DD/MM/yyyy";
				//document.frm_post.tglAksep.disabled=true;
				document.frm_post.selectKondisi.disabled=true;
				document.frm_post.tolakNote.readOnly=false;
				document.frm_post.tolakNote.value=document.frm_post.tolakNoteTemp.value
			}else if(document.frm_post.selectAksep.value==1){//diterima
				document.frm_post.terima1.value="Efektif sejak tanggal ";
				//document.frm_post.tglAksep.disabled=false;
				document.frm_post.terima2.value="dengan kondisi sebagai berikut :";
				//document.frm_post.tglAksep.value=document.frm_post.tgl_terima.value;
				document.frm_post.selectKondisi.disabled=false;
				document.frm_post.tolakNote.readOnly=true;
				document.frm_post.tolakNote.value="-"
			}	
			kondisi();		
		}
	}
	
	function batal(){
		alert(document.frm_post.nomor.value);
		window.location="${path}/uw/edit.htm?nomor="+document.frm_post.nomor.value;
	}
	
	function view(){
		document.frm_post.proses.value="1";
		frm_post.submit();
	}
	function save(){
		document.frm_post.proses.value="2";
		frm_post.submit();
	}
	
	
</script>
<body onload="awal();">
<form name="frm_post" method="post">
<c:if test="${cmd.edit ne null }">
  <table>
    <tr> 
      <td colspan="3">Maka dengan ini PT. Asuransi Jiwa Sinarmas MSIG memutuskan bahwa 
        Pemulihan Polis yang telah Bapak / Ibu ajukan :</td>
      <td>
      	<select name="selectAksep" onChange="aksep();">
			<spring:bind path="cmd.edit.msur_accept"> 
          		<c:forEach var="s" items="${lsPilih}"> <option value="${s.ID }" 
				 <c:if test="${s.ID eq status.value}">selected</c:if>>
					${s.VALUE} </option> 
				</c:forEach> 
			</spring:bind>  	
		</select>
      <td width="77">&nbsp; </td>
    </tr>
    <tr>
    	<td>
    		<input type="text" name="pesanTolak" readonly
	    		<spring:bind path="cmd.edit.msur_accept"> 
	    			<c:choose>
						<c:when test="${status.value eq 0}" > 
							value="Ditolak" 
						</c:when>
						<c:when test="${status.value eq 1}" > 
							value="-" 
						</c:when>
					</c:choose>
				</spring:bind>	
    		>
    	</td>
    </tr>
    <tr>
    	<td>
		    <spring:bind path="cmd.edit.msur_accept_note"> 
	    		<input type="hidden" name="tolakNoteTemp"  value="${status.value }">
	    	</spring:bind>
		    <spring:bind path="cmd.edit.msur_accept_note"> 
	    		<input type="text" name="tolakNote" size="100" >
	    	</spring:bind>
	    </td>
    </tr>
    <tr>
     	<td>
    		<input type="text" name="pesanTerima" readonly
	    		<spring:bind path="cmd.edit.msur_accept"> 
	    			<c:choose>
						<c:when test="${status.value eq 0}" > 
							value="-" 
						</c:when>
						<c:when test="${status.value eq 1}" > 
							value="Diterima"
						</c:when>
					</c:choose>
				</spring:bind>	
    		>
    	</td>
    </tr>
    <tr>
    	<td colspan="5">
	    	<input type="text" name="terima1" size="40" readonly>
	    	<spring:bind path="cmd.edit.msur_tanggal_acc"> 
	    	      		<input type="text" readonly name="tglAksep" value="<fmt:formatDate value="${status.value}" pattern="dd/MM/yyyy"/>" id="startDate" size="12">
			<img src="${path}/include/image/calendar.jpg" align="baseline" title="Calendar" border="0" id="_startDate">
			<script type="text/javascript">
			    Calendar.setup({
			        inputField     :    "startDate",
			        ifFormat       :    "%d/%m/%Y",
			        button         :    "_startDate",
			        align          :    "Tl"
			    });
			</script>	
 	        <!--<input type="text" readonly name="tglAksep" value="${status.value}" onclick="scwShow(this, this);">  -->	
	        </spring:bind> 
	    	<input type="text" name="terima2" size="40" readonly>
	    	<select name="selectKondisi" onChange="kondisi();">
		    	<spring:bind path="cmd.edit.msur_kondisi_polis"> 
		    		<c:forEach var="s" items="${lsKondisi}"> <option value="${s.ID }" 
					 <c:if test="${s.ID eq status.value}">selected</c:if>>
						${s.VALUE} </option> 
					</c:forEach> 
		    	</spring:bind>
			</select>	    	
    	</td>
    </tr>
    <tr>
    	<td>	
	    	<spring:bind path="cmd.edit.msur_kondisi_note"> 
				<input type="hidden" name="terimaNoteTemp"  value="${status.value }">
	    	</spring:bind>
	    	<spring:bind path="cmd.edit.msur_kondisi_note"> 
				<input type="text" name="terimaNote" value="-" readonly size="100">
	    	</spring:bind>
	    </td>	
	   
    </tr>
    <tr> 
      <td>Total Premi Yang Tertunggak</td>
      <td>:</td>
      <td colspan="2"> 
			<spring:bind path="cmd.edit.lku_id"> 
				<input name="kurs" type="text" 
	          		<c:forEach var="s" items="${lsKurs}"> 
	          			<c:if test="${s.ID eq status.value}">value="${s.VALUE }"</c:if> 
	          		</c:forEach> 
	          	size="3" maxlength="3"> 
	        </spring:bind> 
	        <spring:bind path="cmd.edit.msur_total_unbayar"> 
        		<input type="text" name="${status.expression}" value="${status.value }">
        	</spring:bind>
        </td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td width="426">Total Bunga Tertunggak</td>
      <td width="15">: </td>
      <td colspan="2"> 
			<spring:bind path="cmd.edit.lku_id"> 
				<input name="kurs" type="text" 
	          		<c:forEach var="s" items="${lsKurs}"> 
	          			<c:if test="${s.ID eq status.value}">value="${s.VALUE }"</c:if> 
	          		</c:forEach> 
	          	size="3" maxlength="3"> 
	        </spring:bind> 
	        <spring:bind path="cmd.edit.msur_total_bunga_unbayar"> 
		        <input type="text" name="${status.expression}" value="${status.value }">
		    </spring:bind>
	  </td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td width="426">Total Premi + Bunga Tertunggak</td>
      <td width="15">: </td>
      <td colspan="2"> 
			<spring:bind path="cmd.edit.lku_id"> 
				<input name="kurs" type="text" 
	          	<c:forEach var="s" items="${lsKurs}"> 
	          		<c:if test="${s.ID eq status.value}">value="${s.VALUE }"</c:if> 
	          	</c:forEach> size="3" maxlength="3"> 
	        </spring:bind> 
	        <spring:bind path="cmd.edit.msur_total"> 
        		<input type="text" name="${status.expression}" value="${status.value }">
        	</spring:bind>
        </td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>Pembayaran Premi berikut Bunga Tunggakan harus cair sebelum tanggal</td>
      <td>:</td>
      <td> 
      	<spring:bind path="cmd.edit.msur_tgl_batas_paid"> 
      		<input type="text" readonly name="tgl_paid" value="<fmt:formatDate value="${status.value}" pattern="dd/MM/yyyy"/>" id="endDate" size="12">
			<img src="${path}/include/image/calendar.jpg" align="baseline" title="Calendar" border="0" id="_endDate">
			<script type="text/javascript">
			    Calendar.setup({
			        inputField     :    "endDate",
			        ifFormat       :    "%d/%m/%Y",
			        button         :    "_endDate",
			        align          :    "Tl"
			    });
			</script>	
        	<!--<input type="text" readonly name="tgl_paid" value="${status.value}" onclick="scwShow(this, this);">  -->
        </spring:bind> 
      	<spring:bind path="cmd.edit.msur_tanggal_acc">         
        	<input type="hidden" readonly name="tgl_terima" value="${status.value}" >
        </spring:bind>
      </td>
      
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="4"><div align="center"> 
	      <input type="hidden" name="proses" value="0" >
          <input type="button" name="btn_ok" value="Change" onClick="view();">
          <input type="button" name="btn_cancel" value="Cancel" onClick="batal();">
          <input type="button" name="btn_save" value="Save" onClick="save();">
          </div>
	       	<c:if test="${not empty submitSuccess }">
	        	<div id="success">
		        	Berhasil
	        	</div>
	        </c:if>	
  			<spring:bind path="cmd.*">
				<c:if test="${not empty status.errorMessages}">
					<div id="error">
						ERROR:<br>
							<c:forEach var="error" items="${status.errorMessages}">
										- <c:out value="${error}" escapeXml="false" />
								<br/>
							</c:forEach>
					</div>
				</c:if>									
			</spring:bind>
          
       </td>
    </tr>
  </table> 
</c:if> 
</form>
</body>
<%@ include file="/include/page/footer.jsp"%>