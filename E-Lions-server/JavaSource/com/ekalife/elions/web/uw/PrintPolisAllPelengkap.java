package com.ekalife.elions.web.uw;

import id.co.sinarmaslife.std.model.vo.DropDown;
import id.co.sinarmaslife.std.spring.util.Email;
import id.co.sinarmaslife.std.spring.util.EmailSender;
import id.co.sinarmaslife.std.util.FileUtil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.security.Principal;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.util.PDFMergerUtility;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.ui.jasperreports.JasperReportsUtils;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;

import com.ekalife.elions.dao.BasDao;
import com.ekalife.elions.model.Account_recur;
import com.ekalife.elions.model.AddressBilling;
import com.ekalife.elions.model.Agen;
import com.ekalife.elions.model.Benefeciary;
import com.ekalife.elions.model.CommandUploadBac;
import com.ekalife.elions.model.Datarider;
import com.ekalife.elions.model.Datausulan;
import com.ekalife.elions.model.DetilInvestasi;
import com.ekalife.elions.model.DetilTopUp;
import com.ekalife.elions.model.InvestasiUtama;
import com.ekalife.elions.model.MedQuest;
import com.ekalife.elions.model.MstQuestionAnswer;
import com.ekalife.elions.model.Pas;
import com.ekalife.elions.model.PembayarPremi;
import com.ekalife.elions.model.Pemegang;
import com.ekalife.elions.model.PesertaPlus;
import com.ekalife.elions.model.Powersave;
import com.ekalife.elions.model.Rekening_client;
import com.ekalife.elions.model.Simcard;
import com.ekalife.elions.model.Tertanggung;
import com.ekalife.elions.model.User;
import com.ekalife.elions.service.AjaxManager;
import com.ekalife.elions.service.BacManager;
import com.ekalife.elions.service.ElionsManager;
import com.ekalife.elions.service.UwManager;
import com.ekalife.elions.web.uw.PrintPolisMultiController;
import com.ekalife.elions.web.uw.PrintPolisPrintingController;
import com.ekalife.elions.web.uw.support.WordingPdfViewer;
import com.ekalife.utils.CekPelengkap;
import com.ekalife.utils.Common;
import com.ekalife.utils.EmailPool;
import com.ekalife.utils.FileUtils;
import com.ekalife.utils.FormatDate;
import com.ekalife.utils.FormatNumber;
import com.ekalife.utils.FormatString;
import com.ekalife.utils.ITextPdf;
import com.ekalife.utils.MergePDF;
import com.ekalife.utils.PdfUtils;
import com.ekalife.utils.Print;
import com.ekalife.utils.PrintPolisPerjanjianAgent;
import com.ekalife.utils.Products;
import com.ekalife.utils.QrUtils;
import com.ekalife.utils.StringUtil;
import com.ekalife.utils.jasper.JasperScriptlet;
import com.ekalife.utils.jasper.JasperUtils;
import com.ekalife.utils.jasper.Report;
import com.ekalife.utils.parent.ParentJasperReportingController;
import com.ekalife.utils.parent.ParentMultiController;
import com.ekalife.utils.view.PDFViewer;
import com.ibatis.common.resources.Resources;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.PdfWriter;
import com.ekalife.utils.view.PDFViewer;
import com.lowagie.text.pdf.PdfCopy;
import com.sun.pdfview.PDFFile;
/**
 * @author Mark Valentino
 * 
 * 
 */
@SuppressWarnings("unchecked")
public class PrintPolisAllPelengkap extends ParentJasperReportingController{
	protected final Log logger = LogFactory.getLog( getClass() );

	private Properties props;
	private ElionsManager elionsManager;
	private UwManager uwManager;
	private BasDao basDao;
	private BacManager bacManager;
	private Products products;
	private User user;	
	private PrintPolisPrintingController pppc;
	
	public PrintPolisPrintingController getPppc() {
		return pppc;
	}

	public void setPppc(PrintPolisPrintingController pppc) {
		this.pppc = pppc;
	}

	public Properties getProps() {
		return props;
	}

	public void setProps(Properties props) {
		this.props = props;
	}

	public ElionsManager getElionsManager() {
		return elionsManager;
	}

	public void setElionsManager(ElionsManager elionsManager) {
		this.elionsManager = elionsManager;
	}

	public UwManager getUwManager() {
		return uwManager;
	}

	public void setUwManager(UwManager uwManager) {
		this.uwManager = uwManager;
	}

	public BacManager getBacManager() {
		return bacManager;
	}

	public void setBacManager(BacManager bacManager) {
		this.bacManager = bacManager;
	}	

	public Products getProducts() {
		return products;
	}

	public void setProducts(Products products) {
		this.products = products;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}		
	
	Errors errors;
	DateFormat df2 = new SimpleDateFormat("yyyyMMdd");
	
	public  PrintPolisAllPelengkap (ServletContext servletContext){
		this.setServletContext(servletContext);
	}
	
	public PrintPolisAllPelengkap(){
		
	}
	
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
		    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);		
	
	//Mark Valentino 20180906
	public ModelAndView generatePolisAllPelengkap(HttpServletRequest request, HttpServletResponse response, ElionsManager elionsManager, UwManager uwManager, BacManager bacManager, Properties props, Products products) throws Exception {
		String success  = ""; 
		User currentUser = (User) request.getSession().getAttribute("currentUser");
		List errors = new ArrayList();
		String spaj = ServletRequestUtils.getStringParameter(request, "spaj");	
		Integer flagFrom = ServletRequestUtils.getIntParameter(request, "flag", 2);//1 print dan kirim email(khusus produk dari click for life) 2: print all tanpa mengirim email
		Integer mspo_provider=uwManager.selectGetMspoProvider(spaj);
		ServletOutputStream out = response.getOutputStream();		
	    Document document = new Document();	
		List pdfFiles = new ArrayList();
		try{
			//Mark Valentino 2018090908 jangan lupa aktifkan lagi
			elionsManager.updatePolicyAndInsertPositionSpaj(spaj, "mspo_date_print", currentUser.getLus_id(), 6, 1, "PRINT POLIS ALL(E-LIONS)", true, currentUser);
			
			//(HttpServletRequest request,Integer mspoProvider, int flagPrePrinted, ElionsManager elionsManager, UwManager uwManager, BacManager bacManager, Properties props, Products products)
			generateReport3(request, mspo_provider, 2, elionsManager, uwManager, bacManager, props, products);
				
		}catch (Exception e) {			
			 errors.add("Terjadi kesalahan dalam generate Polis");
			 logger.error("ERROR", e);
			 bacManager.updateMst_policyEmptyPrintDate(spaj);
		}
	    if(errors.isEmpty()){
	    	Map detBisnis = (Map) elionsManager.selectDetailBisnis(spaj).get(0);
			String businessId = (String) detBisnis.get("BISNIS");
			String lsbs_name = (String) detBisnis.get("LSBS_NAME");
			String lsdbs_name = (String) detBisnis.get("LSDBS_NAME");
			String lsdbs = (String) detBisnis.get("DETBISNIS");
	    	if(products.unitLink(businessId) && !products.stableLink(businessId)) {
				elionsManager.updateUlink(3, spaj, df2.format(elionsManager.selectMuTglTrans(spaj)));
			}
	    	
		    String cabang = elionsManager.selectCabangFromSpaj(spaj);
		    String dir = props.getProperty("pdf.dir.export")+"\\"+cabang+"\\"+spaj;	    

		    String file = "polis_all.pdf";	    
		    if(flagFrom==1){
		    	//kirim email dari click for life
		    }else if(flagFrom==2){
			    Document documentPolisAll = new Document();
				try {
					
					//baca source pdf nya
					Boolean itextReaderOld = true;
					PdfReader reader = null;
					com.itextpdf.text.pdf.PdfReader readerNew = null;
					
					try{
						reader = new PdfReader(dir + "\\" + file, "ekalife".getBytes());
					} catch(IOException ioe1){
						try{
							reader = new PdfReader(dir + "\\" + file);
						}catch(IOException ioe2){
							itextReaderOld=false;
							try {
								readerNew = new com.itextpdf.text.pdf.PdfReader(dir + "\\" + file, "ekalife".getBytes());
							} catch (IOException ioe) {
								try {
									readerNew = new com.itextpdf.text.pdf.PdfReader(dir + "\\" + file);
								} catch (IOException e) {		
									response.setContentType("text/html");					
									ServletOutputStream out2 = response.getOutputStream();
						    		out2.println("<script>alert('File tidak ada. Harap cek kembali data Anda masukkan.');</script>");
						    		out2.flush();
						    		document.close();
						    		return null;	
								}
							}
						}
					}					
		//			response header
					response.setHeader("Content-Disposition", "inline;filename=file.pdf");
					response.setHeader("Expires", "0");
					response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
					response.setHeader("Pragma", "public");
					response.setContentType("application/pdf");		
					//"stamp" source pdf nya ke output stream tanpa menambah apa2
		            ServletOutputStream sos = response.getOutputStream();
		            if(itextReaderOld){
		            	PdfStamper stamper = new PdfStamper(reader, sos);
		            	//disable printing apabila ada flag
		    			if(ServletRequestUtils.getIntParameter(request, "readonly", 0) == 1){
		    				stamper.setEncryption(false, null, null, PdfWriter.ALLOW_MODIFY_ANNOTATIONS);
		    			}
		    			stamper.close();	
		            }else{
		            	com.itextpdf.text.pdf.PdfStamper stamperNew = new com.itextpdf.text.pdf.PdfStamper(readerNew, sos);
		            	//disable printing apabila ada flag
		    			if(ServletRequestUtils.getIntParameter(request, "readonly", 0) == 1){
		    				stamperNew.setEncryption(false, null, null, PdfWriter.ALLOW_MODIFY_ANNOTATIONS);
		    			}
		    			stamperNew.close();
		            }
					
		            if(sos!=null){
						sos.flush();
						sos.close();
					}
		            if(reader!=null)reader.close();
		            if(readerNew!=null)readerNew.close();			
				}catch(Exception de) {
					logger.error(de);
		    		ServletOutputStream out2 = response.getOutputStream();
		    		out2.println("<script>alert('Halaman tidak ada. Harap cek kembali data yang bersangkutan.');</script>");
		    		out2.flush();
				}
				document.close();
		    }
		    
			return null;
	    }else{
	    	return new ModelAndView("uw/printpolis/printpolis_err", "error", errors);
	    }
	}	
	
	// GENERATE OUTSOURCE (MANUAL), GENERATE OUTSOURCE SCHEDULER, GENERATE OUTSOURCE Hit URL
	// GENERATE OUTSOURCE SCHEDULER
	// GENERATE OUTSOURCE Hit URL
	public String generateFileRDS(HttpServletRequest request, HttpServletResponse response, String spaj, int flagPrePrinted, ElionsManager elionsManager, UwManager uwManager, BacManager bacManager, Properties props, Products products, User user, String jenis, String covid) throws Exception{
		
		String reg_spaj = "";
		int mspoProvider = 0;
		if((jenis.equals("scheduler")) || (jenis.equals("hit"))){
			reg_spaj = spaj;
		}else if(jenis.equals("manual") || jenis.equals("regenerate")){
			reg_spaj = request.getParameter("spaj");	
		}
		Map paramsPolis = null;		
		mspoProvider = uwManager.selectGetMspoProvider(reg_spaj);
		User currentUser = new User();
		currentUser = user;
		Integer countSoftcopy = bacManager.selectCountSoftcopyPolis(reg_spaj);
		String hasilGenerate = "";
		String keterangan = ""; 
		if(jenis.equals("regenerate")){
			keterangan = request.getParameter("keterangan");
		}
		
		Connection conn = null;
		try {
			String judulPosisi = "";
			if(jenis.equals("scheduler") || jenis.equals("hit")) {
				judulPosisi = "GENERATE OUTSOURCE SCHEDULER (E-LIONS)";
			}else if(jenis.equals("manual")) {
				judulPosisi = "GENERATE OUTSOURCE (E-LIONS)";
			}else if(jenis.equals("regenerate")) {
				judulPosisi = "RE-GENERATE OUTSOURCE (E-LIONS): "+keterangan;
			}else if(jenis.equals("softcopy")) {
				judulPosisi = "GENERATE SOFTCOPY (E-LIONS)";
			}
			
		//0. e-SPAJ Dmtm / Online Gadget
		List listSpajTemp = bacManager.selectReferensiTempSpaj(reg_spaj);
		if (listSpajTemp.size() > 0){
			String cabang = elionsManager.selectCabangFromSpaj(reg_spaj);			
			if (cabang.equals("40")){
				espajdmtm3(this.getServletContext(), reg_spaj, request, 0, elionsManager, uwManager, bacManager, props, products);
			}else{
				espajonlinegadget(request, response, elionsManager, uwManager,bacManager, props, products, reg_spaj);				
			}
		}
			elionsManager.updatePolicyAndInsertPositionSpaj(spaj, "mspo_date_print", user.getLus_id(), 6, 1, judulPosisi, true, user);	

		
		conn = uwManager.getUwDao().getDataSource().getConnection();
		if(spaj.equals(null)) {
			spaj = ServletRequestUtils.getStringParameter(request, "spaj", "");
		}
		String cabang = elionsManager.selectCabangFromSpaj(spaj);
		if(user == null) {
			currentUser = (User) request.getSession().getAttribute("currentUser");
			user = currentUser;
		}
		String jpu = "";
		if(jenis.equals("manual")){
			jpu = ServletRequestUtils.getStringParameter(request, "seq", "");
		}
		Integer punyaSimascard = 0;
		String flagUlink ="0";
		
		List detBisnis = elionsManager.selectDetailBisnis(spaj);
		String lsbs = (String) ((Map) detBisnis.get(0)).get("BISNIS");
		String detbisnis = (String) ((Map) detBisnis.get(0)).get("DETBISNIS");			
		String lus_id = currentUser.getLus_id();
	
		Boolean listExclude = false;
//		boolean isKirimSoftcopy = (lsbs.equals("142") && detbisnis.equals("013"));		
		
		//1. Polis
		paramsPolis = printPolis(spaj, request, PrintPolisMultiController.POLIS_QUADRUPLEX, elionsManager, uwManager, bacManager, props, products);
		String businessId = FormatString.rpad("0", uwManager.selectBusinessId(spaj), 3);
		Integer businessNumber = uwManager.selectBusinessNumber(spaj);

		//helpdesk [133346] produk baru 142-13 Smart Investment Protection
		if(businessId.equalsIgnoreCase("142") && businessNumber == 13){
			paramsPolis.remove("tipePolis");
			paramsPolis.put("tipePolis", "");
		}			
		
		//2. Manfaat
		//Integer businessNumber = uwManager.selectBusinessNumber(spaj);
		Map paramsManfaat = elionsManager.prosesCetakManfaat(spaj, currentUser.getLus_id(), request);
		String pathManfaat = (String) paramsManfaat.get("reportPath");
		pathManfaat = pathManfaat.substring(17);
		paramsManfaat.put("pathManfaat", pathManfaat + ".jasper");
		paramsManfaat.remove("reportPath");
		paramsManfaat.put("lsdbs", businessNumber);
		paramsPolis.putAll(paramsManfaat);
		
		paramsPolis.put("koneksi", conn);
		List temp = new ArrayList(); Map map = new HashMap();
		map.put("halaman", "1"); temp.add(map);//polis
		map.put("halaman", "2"); temp.add(map);//manfaat
		map.put("halaman", "3"); temp.add(map);//surat
		if(products.unitLink(uwManager.selectBusinessId(spaj))) {
			map.put("halaman", "4"); temp.add(map);//kosong
			map.put("halaman", "5"); temp.add(map);//alokasi dana (bisa 1 / 2 halaman)
		}
		if(products.unitLink(uwManager.selectBusinessId(spaj))) {//surat simas card
			map.put("halaman", "7"); temp.add(map);
		}else{
			map.put("halaman", "4"); temp.add(map);
		}
		map.put("halaman", "6"); temp.add(map);
		map.put("halaman", "8"); temp.add(map);
		map.put("halaman", "9"); temp.add(map);
		map.put("halaman", "10"); temp.add(map);
		paramsPolis.put("dsManfaat", JasperReportsUtils.convertReportData(temp));
		
		//String businessId = FormatString.rpad("0", uwManager.selectBusinessId(spaj), 3);
		
		//3. Surat Polis
//			if("001,045,053,054,130,131,132".indexOf(businessId)==-1) {

			String va = uwManager.selectVirtualAccountSpaj(spaj);
			if(cabang.equals("58")){
				if(elionsManager.selectValidasiTransferPbp(spaj)==0){//ini mste_flag_cc
					if(va==null){
						uwManager.updateVirtualAccountBySpaj(spaj);
					}
				}
			}
			
			Map data 		= uwManager.selectDataVirtualAccount(spaj);
			int lscb_id 	= ((BigDecimal) data.get("LSCB_ID")).intValue();
			String lku_id 	= (String) data.get("LKU_ID");
			
			Integer flag_cc = elionsManager.select_flag_cc(spaj);
			
//				if(va != null){
			if(lku_id.equals("01") && !products.syariah(businessId,businessNumber.toString()) && lscb_id != 0 //&& flag_cc ==0 
					&& !businessId.equals("196") && !(businessId.equals("217") && businessNumber ==2)&& !(businessId.equals("190") && "5,6".indexOf(businessNumber.toString())>=0)){
//				if(flag_cc ==0){
				
				List cekSpajPromo = bacManager.selectCekSpajPromo(  null , spaj,  "1"); // cek spaj free sudah terdaftar atau belum MST_FREE_SPAJ
				
				if(!cekSpajPromo.isEmpty()){
					paramsPolis.put("pathSurat", props.getProperty("report.surat_polis.va_promo") + ".jasper");
				}else if(businessId.equals("217")&& businessNumber==2){ //untuk ERbe di set menggunakan surat_polis
					paramsPolis.put("pathSurat", props.getProperty("report.surat_polis") + ".jasper");
				}else{
					paramsPolis.put("pathSurat", props.getProperty("report.surat_polis.va") + ".jasper");
				}
				
			//	paramsPolis.put("pathSurat", props.getProperty("report.surat_polis.va") + ".jasper");
			}else if(products.syariah(businessId, businessNumber.toString())){
				if(flag_cc==0 && !businessId.equals("175")){
					if(businessId.equals("215")&& businessNumber==003){
						paramsPolis.put("pathSurat", props.getProperty("report.surat_polis.syariah") + ".jasper");
					}else{
						paramsPolis.put("pathSurat", props.getProperty("report.surat_polis.syariah_va") + ".jasper");
					}
				}else{
					paramsPolis.put("pathSurat", props.getProperty("report.surat_polis.syariah") + ".jasper");
				}
			}else if(cabang.equals("58")){
				paramsPolis.put("pathSurat", props.getProperty("report.surat_polis.mall") + ".jasper");
			}
			else{
				if(businessId.equals("196")&& businessNumber==002){
					paramsPolis.put("pathSurat", props.getProperty("report.surat_polis.term") + ".jasper");
				}else{
				paramsPolis.put("pathSurat", props.getProperty("report.surat_polis") + ".jasper");
				}
			}
			
			paramsPolis.put("hamid", props.get("images.ttd.direksi")); //ttd pak hamid (Yusuf - 04/05/2006)
//			}
		
		//4. Alokasi Dana
		List viewUlink = elionsManager.selectViewUlink(spaj);
		if(viewUlink.size()!=0){
			Map paramsAlokasi = elionsManager.cetakSuratUnitLink(viewUlink, spaj, true, 1, 1,0);
			paramsAlokasi.put("elionsManager", elionsManager);
			
			String pathAlokasi = (String) paramsAlokasi.get("reportPath");
			pathAlokasi = pathAlokasi.substring(17);
			paramsAlokasi.put("pathAlokasiDana", pathAlokasi + ".jasper");
			paramsAlokasi.remove("reportPath");
			paramsAlokasi.put("dsAlokasiDana", JasperReportsUtils.convertReportData(viewUlink));
			
			paramsPolis.putAll(paramsAlokasi);
		}
		
		// 5 Tanda Terima Polis
		// Integer referal = ServletRequestUtils.getIntParameter(request,
		// "referal", 0);

		Integer referal = 0;

		// List detBisnis = elionsManager.selectDetailBisnis(spaj);
		String lsbs_id = (String) ((Map) detBisnis.get(0)).get("BISNIS");
		String lsdbs = (String) ((Map) detBisnis.get(0)).get("DETBISNIS");
		// String lsbs_id = FormatString.rpad("0",
		// uwManager.selectBusinessId(spaj), 3);

		String reportPath;
		String namaFile = props.getProperty("pdf.tanda_terima_polis");
		int isInputanBank = elionsManager.selectIsInputanBank(spaj);
		if (((isInputanBank == 2 || isInputanBank == 3) && referal == 0 && !products
				.productBsmFlowStandardIndividu(Integer.parseInt(lsbs_id),
						Integer.parseInt(lsdbs)))
				|| (lsbs_id.equals("175") && lsdbs.equals("002"))) {

			reportPath = props.getProperty("report.tandaterimasertifikat");
			namaFile = props.getProperty("pdf.tanda_terima_sertifikat");
		} else if (products.unitLink(lsbs_id)
				&& !products.stableLink(lsbs_id)) {

			if (products.syariah(lsbs_id, lsdbs)) {

				reportPath = props
						.getProperty("report.tandaterimapolis.syariah");
			} else {

				reportPath = props
						.getProperty("report.tandaterimapolis.link");
			}
		} else if (lsbs_id.equals("187")) {
			reportPath = props.getProperty("report.tandaterimapolis.pas");
		} else {
			reportPath = props.getProperty("report.tandaterimapolis"); // ini
																		// udah
																		// include
																		// biasa
																		// +
																		// syariah

		}

		paramsPolis.put("referal", referal);
		paramsPolis.put("pathTandaTerimaPolis", reportPath + ".jasper");

		// flagPrePrinted : 1 = direct print, 2 = Polis All
//		if (flagPrePrinted == 2) {
//
//			List daftarSebelumnya = uwManager.selectSimasCard(spaj);
//			List isAgen = uwManager.selectIsSimasCardClientAnAgent(spaj);
//
//			if (!daftarSebelumnya.isEmpty() && isAgen.isEmpty()) {
//				Map SimasCardSebelumnya = (Map) daftarSebelumnya.get(0);
//				if (!Common.isEmpty(SimasCardSebelumnya.get("REG_SPAJ"))) {
//					if (SimasCardSebelumnya.get("REG_SPAJ").equals(spaj)) {
//						punyaSimascard = 1;
//					} else {
//						elionsManager.insertMstPositionSpaj(
//								"01",
//								"SUDAH PERNAH DAPAT SIMAS CARD DENGAN NO SPAJ"
//										+ SimasCardSebelumnya
//												.get("REG_SPAJ"), spaj, 0);
//
//					}
//				}
//
//			}
//
//		}
//		String reportPathSimasCard = props.get("report.surat_simcard")
//				.toString();
//
//		paramsPolis.put("pathSuratSimasCard", reportPathSimasCard
//				+ ".jasper");
//
//		for (Iterator iter = paramsPolis.keySet().iterator(); iter
//				.hasNext();) {
//
//			String nama = (String) iter.next();
//			logger.info(nama + " = " + paramsPolis.get(nama));
//		}

		// SSU-SSK
		// update 7 September 2018 Mark Valentino SSU-SSK diikutsertakan
		if (PDFViewer.checkFileProduct(elionsManager, uwManager, props,
				spaj))
			;
		{

			List<File> pdfFiles = new ArrayList<File>();

			// String exportDirectory =
			// props.getProperty("pdf.dir.export")+"\\"+cabang+"\\"+spaj;

			String exportDirTemp = props.getProperty("pdf.dir.export.temp")
					+ "\\" + spaj;

			File exportDirTemp1 = new File(
					props.getProperty("pdf.dir.export.temp") + "\\" + spaj);

			if (!exportDirTemp1.exists()) {
				exportDirTemp1.mkdirs();

			}
			File fileDirZip = new File(exportDirTemp1 + "\\" + spaj);
			if (!fileDirZip.exists()) {
				fileDirZip.mkdirs();

			}

			String dirSsk = props.getProperty("pdf.dir.syaratpolis");
			PDFMergerUtility SskBefore = new PDFMergerUtility();
			File file = null;
			File fileSsk1 = null;
			for (int i = 0; i < detBisnis.size(); i++) {
				Map m = (HashMap) detBisnis.get(i);
				// Mark Valentino 20180906
				// SSU
				Integer lsbsId = Integer.parseInt(m.get("BISNIS")
						.toString());

				if (lsbsId <= 300) {
					File fileSSU = PDFViewer.productFile(elionsManager,
							uwManager, dirSsk, spaj, m, props);

					if (fileSSU != null)
						if (fileSSU.exists()) {
							pdfFiles.add(fileSSU);
							String pathSsu = fileSSU.toString();
							FileUtil.copyfile(pathSsu,fileDirZip.toString() + "\\" + "6. ssu.pdf");
						}							
				}
				// SSK
				if ((lsbsId > 800) && (lsbsId < 900)) {
					if (i == 1) {
						fileSsk1 = PDFViewer.riderFile(elionsManager,uwManager, dirSsk, lsbs_id, lsdbs, m, spaj,props);
						if (fileSsk1 != null){
							if (fileSsk1.exists()){
								SskBefore.addSource(fileSsk1);
								pdfFiles.add(fileSsk1);
								System.out.println("File SSK Rider pertama telah ditambahkan.");											
							}
						}	

					}else if (i >= 2) {
						File fileSskN = PDFViewer.riderFile(elionsManager,uwManager, dirSsk, lsbs_id, lsdbs, m, spaj,props);

						// Jika file name SSK ke-2 berbeda dgn filename SSK pertama, maka diambil juga									
						if (!fileSsk1.getName().toString().equals(fileSskN.getName().toString())){									
							if (fileSskN != null){
								if (fileSskN.exists()){
										SskBefore.addSource(fileSskN);
										pdfFiles.add(fileSskN);
										System.out.println("File SSK Rider ke-"+ i + " telah ditambahkan.");												
									}
								}
						}
					}else {
						file = null;
					}
					// if(file!=null) if(file.exists()) {
					// String pathSsk = file.toString();
					// SskBefore.addSource(file);
					// FileUtil.copyfile(pathSsk, fileDirZip.toString() +
					// "\\" + "7. ssk.pdf");

					// }
				}

			}
			SskBefore.setDestinationFileName(fileDirZip.toString() + "\\"
					+ "7. ssk.pdf");

			SskBefore.mergeDocuments();
			//pdfFiles.add(SskBefore);
			// PdfUtils.combinePdf(pdfFiles, exportDirectory, "pathSS.pdf");
			PdfUtils.combinePdf(pdfFiles, exportDirTemp, "pathSS.pdf");

		}

		// ENDORS SMILE MEDICAL

		if (mspoProvider == 2) {
			PrintPolisPerjanjianAgent printPolis = new PrintPolisPerjanjianAgent();
			List<String> pdfs = new ArrayList<String>();
			Boolean suksesMerge = false;
			Boolean scFile = false;
			String endorsPolis = "";
			String Kartuadmedika = "";
			String Pesertaadmedika = "";
			String provider = "";
			String filename = "pathAdmedika";
			Date sysdate = elionsManager.selectSysdate();
			Boolean syariah = products.syariah(lsbs_id, lsdbs);

			Integer ekaSehatBaru = uwManager.selectCountEkaSehatAdmedikaNew(spaj, 0);
			Integer ekaSehatHCP = uwManager.selectCountEkaSehatAdmedikaHCP(spaj);
			Integer ekaSehatPlus = uwManager.selectCountEkaSehatAdmedikaNew(spaj, 1);
			Integer s = Integer.parseInt(lsdbs.substring(1));
			Integer punyaEndorsAdmedika = bacManager.selectPunyaEndorsEkaSehatAdmedika(spaj);

			if (punyaEndorsAdmedika == 0)
				bacManager.prosesEndorsKetinggalanNew(spaj,Integer.parseInt(lsbs_id));
			
			if(syariah){
				Kartuadmedika = props.getProperty("pdf.template.admedika")+ "\\PETUNJUK_PENGGUNAAN_KARTU_PESERTA_syariah.pdf";					
			}else{
				Kartuadmedika = props.getProperty("pdf.template.admedika")+ "\\PETUNJUK_PENGGUNAAN_KARTU_PESERTA_konven.pdf";
			}
			OutputStream output = new FileOutputStream(props.getProperty("pdf.dir.export.temp") + "\\"
					+ spaj + "\\" + filename + ".pdf");
			
			pdfs.add(0,Kartuadmedika);
			
			if (pdfs.size() > 0){
				suksesMerge = MergePDF.concatPDFs(pdfs, output, false);					
			}
			
		// File PETUNJUK_PENGGUNAAN_KARTU_PESERTA, Ad Medika Tidak Diproses
//				if (ekaSehatBaru >= 1) {
//					if (lsbs_id.equals("189")
//							|| products.syariah(lsbs_id, lsdbs)) {
//
//						if (lsbs_id.equals("189")
//								&& Integer.parseInt(lsdbs.substring(1)) > 15) {
//							// endorsPolis =
//							// props.getProperty("pdf.template.admedika")+"\\EndorsemenPolisSyariahSMP.pdf";
//
//							endorsPolis = props.getProperty("pdf.template.admedika")
//									+ "\\PETUNJUK_PENGGUNAAN_KARTU_PESERTA_syariah.pdf";
//
//						} else {
//							// endorsPolis =
//							// props.getProperty("pdf.template.admedika")+"\\EndorsemenPolisSyariah.pdf";
//							endorsPolis = props.getProperty("pdf.template.admedika")
//									+ "\\PETUNJUK_PENGGUNAAN_KARTU_PESERTA_syariah.pdf";
//
//						}
//					} else {
//						// endorsPolis =
//						// props.getProperty("pdf.template.admedika")+"\\EndorsementSmileMedical.pdf";
//						// // EndorsemenPolisBaru
//						endorsPolis = props.getProperty("pdf.template.admedika")
//								+ "\\PETUNJUK_PENGGUNAAN_KARTU_PESERTA_konven.pdf"; // EndorsemenPolisBaru
//
//					}
//				} else if (ekaSehatPlus >= 1) {
//					// endorsPolis =
//					// props.getProperty("pdf.template.admedika")+"\\EndorsementSmileMedicalPlus.pdf";
//					endorsPolis = props.getProperty("pdf.template.admedika")
//							+ "\\PETUNJUK_PENGGUNAAN_KARTU_PESERTA_konven.pdf";
//
//				} else {
//					// endorsPolis =
//					// props.getProperty("pdf.template.admedika")+"\\EndorsemenPolis.pdf";
//					endorsPolis = props.getProperty("pdf.template.admedika")
//							+ "\\PETUNJUK_PENGGUNAAN_KARTU_PESERTA_konven.pdf";
//
//				}
//				
//				if (ekaSehatHCP >= 1 && ekaSehatBaru < 1 && ekaSehatPlus < 1) {
//					// Kartuadmedika =
//					// props.getProperty("pdf.template.admedika")+"\\KartuHCP.pdf";
//					Kartuadmedika = props.getProperty("pdf.template.admedika")
//							+ "\\PETUNJUK_PENGGUNAAN_KARTU_PESERTA_konven.pdf";
//
//					provider = props.getProperty("pdf.template.admedika")
//							+ "\\ProviderHCP.pdf";
//
//				} else {
//					// Kartuadmedika =
//					// props.getProperty("pdf.template.admedika")+"\\KartuAdmedika.pdf";
//					Kartuadmedika = props.getProperty("pdf.template.admedika")
//							+ "\\PETUNJUK_PENGGUNAAN_KARTU_PESERTA_konven.pdf";
//
//					provider = props.getProperty("pdf.template.admedika")
//							+ "\\Provider.pdf";
//
//				}
//				
//				Pesertaadmedika = props.getProperty("pdf.template.admedika")
//						+ "\\PesertaAdmedika.pdf";
//
//				if (ekaSehatBaru >= 1 || ekaSehatPlus >= 1) {
//					pdfs.add(endorsPolis);
//				}
//				if (flagPrePrinted == 1 || flagPrePrinted == 2) {
//					// pdfs.add(Kartuadmedika);
//					// pdfs.add(provider);
//					// 20190304 Mark Valentino - Request Tim Printing : Jika
//					// SPAJ centang provider, maka exclude file Endorse
//					if ((mspoProvider == 2) && (pdfs.size() > 0)) {
//						pdfs.remove(0);
//						pdfs.add(0, Kartuadmedika);
//					} else {
//						pdfs.add(0, Kartuadmedika);
//					}
//
//				}
//				Integer total_ekasehat = uwManager.getJumlahEkaSehat(spaj);
//				List<Map> datapeserta = uwManager.selectDataPeserta(spaj);
//
//				if (ekaSehatHCP >= 1) {
//
//					OutputStream output;
//					// OutputStream output = new
//					// FileOutputStream(props.getProperty("pdf.template.admedika")+"\\MergeAdmedikaHCP.pdf");
//					if (mspoProvider == 2) {
//						output = new FileOutputStream(
//								props.getProperty("pdf.dir.export.temp") + "\\"
//										+ spaj + "\\" + filename + ".pdf");
//					} else {
//						output = new FileOutputStream(
//								props.getProperty("pdf.template.admedika")
//										+ "\\KartuHCP.pdf");
//					}
//
//					suksesMerge = MergePDF.concatPDFs(pdfs, output, false);
//				} else if (lsbs_id.equals("189")
//						|| products.syariah(lsbs_id, lsdbs)) {
//
//					OutputStream output;
//					// OutputStream output = new
//					// FileOutputStream(props.getProperty("pdf.template.admedika")+"\\MergeAdmedikaEkaSehatSyariah.pdf");
//					if (mspoProvider == 2) {
//						output = new FileOutputStream(
//								props.getProperty("pdf.dir.export.temp") + "\\"
//										+ spaj + "\\" + filename + ".pdf");
//					} else {
//						output = new FileOutputStream(
//								props.getProperty("pdf.template.admedika")
//										+ "\\KartuAdmedikaSyariah.pdf");
//					}
//
//					suksesMerge = MergePDF.concatPDFs(pdfs, output, false);
//				} else if (ekaSehatPlus >= 1) {
//					OutputStream output = new FileOutputStream(
//							props.getProperty("pdf.template.admedika")
//									+ "\\MergeAdmedikaMedicalPlus.pdf");
//
//					suksesMerge = MergePDF.concatPDFs(pdfs, output, false);
//				} else {
//					// 20190304 Mark Valentino - Request Tim Printing : Jika
//					// SPAJ centang provider, maka exclude file Endorse
//
//					// OutputStream output = new
//					// FileOutputStream(props.getProperty("pdf.template.admedika")+"\\MergeAdmedikaEkaSehatNew.pdf");
//					OutputStream output;
//					if (mspoProvider == 2) {
//						output = new FileOutputStream(
//								props.getProperty("pdf.dir.export.temp") + "\\"
//										+ spaj + "\\" + filename + ".pdf");
//					} else {
//						output = new FileOutputStream(
//								props.getProperty("pdf.template.admedika")
//										+ "\\MergeAdmedikaEkaSehatNew.pdf");
//					}
//
//					suksesMerge = MergePDF.concatPDFs(pdfs, output, false);
//				}

//				// File userDirTemp = new
//				// File(props.getProperty("pdf.dir.export.temp")+"\\"+cabang+"\\"+spaj);
//				File userDirTemp = new File(
//						props.getProperty("pdf.dir.export.temp") + "\\" + spaj);
//				if (!userDirTemp.exists()) {
//					userDirTemp.mkdirs();
//				}
//
//				// String outputName =
//				// props.getProperty("pdf.dir.export.temp")+"\\"+cabang+"\\"+spaj+"\\"+filename+".pdf";
//				String outputName = props.getProperty("pdf.dir.export.temp")
//						+ "\\" + spaj + "\\" + filename + ".pdf";

//				if (!scFile) {
//					Map dataAdmedika = uwManager.selectDataAdmedika(spaj);
//					String ingrid = props.getProperty("pdf.template.admedika2")
//							+ "\\hamid.bmp";
//					if (mspoProvider != 2) {
//
//						printPolis.generateEndorseAdmedikaEkaSehat(spaj,
//								total_ekasehat, outputName, dataAdmedika,
//								datapeserta, sysdate, ingrid, lsbs_id, lsdbs,
//								syariah, ekaSehatHCP, ekaSehatBaru,
//								ekaSehatPlus);
//					}
//
//				}
		}
			
				// // iga rds phase 3: Vip Card
//				Simcard vip = bacManager.selectVipCardBySpaj(spaj);
//				if (vip != null && (vip.getReg_spaj().equals(spaj) || !vip.getReg_spaj().isEmpty()) ) {
////					reportPath = props.getProperty("report.vipcard.surat.ver2");
////					namaFile = props.getProperty("pdf.surat_vipcard_new");
////					paramsPolis.put("pathVipCardSyariah", reportPath + ".jasper");
//					Boolean suksesMerge = false;
//					List<String> pdfs = new ArrayList<String>();
//					PdfReader reader = new PdfReader(
//							props.getProperty("pdf.template.vip")
//									+ "\\VIPSyariah.pdf");
//					String dir = props.getProperty("pdf.template.vip");
//					String getDir = dir + "\\VIPSyariah.pdf";
////					paramsPolis.put("pathVipCardSyariah", getDir + ".jasper");
//					OutputStream output = new FileOutputStream(props.getProperty("pdf.dir.export.temp") + "\\"
//							+ spaj + "\\" + "pathVipCardSyariah" + ".pdf");
//					
//					pdfs.add(0,getDir);
//					
//					if (pdfs.size() > 0){
//						suksesMerge = MergePDF.concatPDFs(pdfs, output, false);					
//					}
//					String outputName = props.getProperty("pdf.dir.export") + "\\"
//							+ cabang + "\\" + reg_spaj + "\\" + "pathVipCardSyariah"
//							+".pdf";
//					PdfStamper stamp = new PdfStamper(reader, new FileOutputStream(
//							outputName));
//					Date sysdate = elionsManager.selectSysdate();
//					PdfContentByte over;
//					BaseFont times_new_roman = BaseFont.createFont(
//							"C:\\WINDOWS\\FONTS\\ARIAL.TTF", BaseFont.CP1252,
//							BaseFont.NOT_EMBEDDED);
//					
//					over = stamp.getOverContent(1);
//					over.beginText();
//					over.setFontAndSize(times_new_roman, 8);
//					
//					over.showTextAligned(PdfContentByte.ALIGN_LEFT, FormatDate.toString(sysdate), 160, 516, 0);
//					over.showTextAligned(PdfContentByte.ALIGN_LEFT, vip.getAlamat().toUpperCase(), 160, 505, 0);
//					over.showTextAligned(
//							PdfContentByte.ALIGN_LEFT,
//							Common.isEmpty(vip.getKode_pos())? "-" : 
//								vip.getKode_pos().toUpperCase(),160, 500,0);
////					over.showTextAligned(PdfContentByte.ALIGN_LEFT, vip.getKode_pos().toUpperCase(), 160, 500, 0);
//					over.showTextAligned(PdfContentByte.ALIGN_LEFT, vip.getNama().toUpperCase(), 160, 490, 0);
//					over.showTextAligned(PdfContentByte.ALIGN_LEFT, vip.getNo_kartu().toUpperCase(), 190, 470, 0);
//					over.endText();
//					stamp.close();
//					
//				}
				
				Simcard vip = bacManager.selectVipCardBySpaj(spaj);
				Date sysdate = elionsManager.selectSysdate();
				if (vip != null && (vip.getReg_spaj().equals(spaj) || !vip.getReg_spaj().isEmpty()) ) {
					Boolean suksesMerge = false;
					List<String> pdfs = new ArrayList<String>();
				String exportDirectory = props.getProperty("pdf.dir.export") + "\\"
						+ cabang + "\\" + reg_spaj;
				String dir = props.getProperty("pdf.template.vip");
				OutputStream output;
				PdfReader reader;
				File userDir = new File(props.getProperty("pdf.dir.export") + "\\"
						+ cabang + "\\" + reg_spaj);
				if (!userDir.exists()) {
					userDir.mkdirs();
				}

				PdfContentByte over;
				BaseFont times_new_roman = BaseFont.createFont(
						"C:\\WINDOWS\\FONTS\\ARIAL.TTF", BaseFont.CP1252,
						BaseFont.NOT_EMBEDDED);
				BaseFont italic = BaseFont.createFont("C:\\WINDOWS\\FONTS\\ariali.ttf",
						BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

		
					reader = new PdfReader(
							props.getProperty("pdf.template.vip")
									+ "\\VIPSyariah.pdf");
//					output = new FileOutputStream(exportDirectory + "\\"
//							+ "pathVipCardSyariah.pdf");
					output = new FileOutputStream(props.getProperty("pdf.dir.export.temp") + "\\"
							+ spaj + "\\"+"pathVipCardSyariah.pdf");
					String hasil = dir + "\\VIPSyariah.pdf";
				
					pdfs.add(hasil);
					suksesMerge = MergePDF.concatPDFs(pdfs, output, false);
					String outputName = props.getProperty("pdf.dir.export.temp") + "\\"
							+ spaj + "\\"+"pathVipCardSyariah.pdf";
					PdfStamper stamp = new PdfStamper(reader, new FileOutputStream(
							outputName));

					over = stamp.getOverContent(1);
					over.beginText();
//					over.setFontAndSize(times_new_roman, 8);
//					over.showTextAligned(PdfContentByte.ALIGN_LEFT,
//							FormatString.nomorSPAJ(reg_spaj), 380, 627, 0);
//					over.showTextAligned(PdfContentByte.ALIGN_LEFT,
//							FormatDate.toString(sysdate), 85, 617, 0);

					over.setFontAndSize(times_new_roman, 8);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT,
							FormatDate.toString(sysdate), 100, 725, 0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT,
							"Bapak/Ibu " +vip.getNama(), 72, 700, 0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT,
							vip.getAlamat(), 72, 689, 0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT,
							vip.getKota(), 72, 679, 0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT,
							vip.getNo_kartu(), 320, 656, 0);
					over.endText();
					stamp.close();
					
					
//					File l_file = new File(outputName);
//					FileInputStream in = null;
//					ServletOutputStream ouputStream = null;
//					try {
//
//						response.setContentType("application/pdf");
//						response.setHeader("Content-Disposition", "Inline");
//						response.setHeader("Expires", "0");
//						response.setHeader("Cache-Control",
//								"must-revalidate, post-check=0, pre-check=0");
//						response.setHeader("Pragma", "public");
//
//						in = new FileInputStream(l_file);
//						ouputStream = response.getOutputStream();
//
//						IOUtils.copy(in, ouputStream);
//					} catch (Exception e) {
//						logger.error("ERROR :", e);
//					} finally {
//						try {
//							if (in != null) {
//								in.close();
//							}
//							if (ouputStream != null) {
//								ouputStream.flush();
//								ouputStream.close();
//							}
//						} catch (Exception e) {
//							logger.error("ERROR :", e);
//						}
//					}
				}
			String path ="";
			String pathTemp = "";
			path = props.getProperty("pdf.dir.export")+"\\"+cabang+"\\"+spaj;
			pathTemp = props.getProperty("pdf.dir.export.temp")+"\\"+spaj;
			String pathTemplate1 = props.getProperty("pdf.template")+"\\BlankPaper.pdf";
			String pathTemplate2 = props.getProperty("pdf.template")+"\\BlankPaper2.pdf";				
	
			JasperUtils.exportReportToPdfNoLock(paramsPolis.get("pathSurat").toString(), pathTemp , "pathSurat.pdf", paramsPolis, conn);
			JasperUtils.exportReportToPdfNoLock(paramsPolis.get("pathManfaat").toString(),pathTemp , "pathManfaat.pdf",paramsPolis, conn);
			
			
			if (!listExclude){
				JasperUtils.exportReportToPdfNoLock(paramsPolis.get("pathPolis").toString(),pathTemp , "pathPolis.pdf", paramsPolis, conn);
			}
			JasperUtils.exportReportToPdfNoLock(paramsPolis.get("pathTandaTerimaPolis").toString(),pathTemp, "pathTandaTerimaPolis.pdf", paramsPolis, conn);
			if(viewUlink.size()!=0)JasperUtils.exportReportToPdfNoLock(paramsPolis.get("pathAlokasiDana").toString(),pathTemp, "pathAlokasiDana.pdf", paramsPolis, conn);
//			if(flagPrePrinted==2){
//				List daftarSebelumnya = uwManager.selectSimasCard(spaj);
//				List isAgen = uwManager.selectIsSimasCardClientAnAgent(spaj);
//				
//				if((!daftarSebelumnya.isEmpty()) && isAgen.isEmpty()){
//					String spaj2 = (String) ((Map) daftarSebelumnya.get(0)).get("REG_SPAJ");
//					if(spaj2.equals(spaj)){
//						JasperUtils.exportReportToPdfNoLock(paramsPolis.get("pathSuratSimasCard").toString(),pathTemp, "pathSuratSimasCard.pdf", paramsPolis, conn);
//					}
//				}
//			}
			// iga rds phase 3: vip card
//			if (vip != null && (vip.getReg_spaj().equals(spaj) || !vip.getReg_spaj().isEmpty()) ) {
//			JasperUtils.exportReportToPdfNoLock(paramsPolis.get("pathVipCardSyariah").toString(),pathTemp , "pathVipCardSyariah.pdf",paramsPolis, conn);
//			}
			
			CekPelengkap.copyAndRenameFiles(cabang,pathTemp,path,pathTemplate1,pathTemplate2,lsbs_id,mspoProvider,flagUlink,punyaSimascard,flagPrePrinted,request,props,elionsManager,jenis);
	
			// KIRIM SOFTCOPY POLIS
			// untuk saat ini (20191209) fungsi ini hanya untuk produk SIP - Helpdesk : 138052
			HashMap m = Common.serializableMap(uwManager.selectInformasiEmailSoftcopy(reg_spaj));
			String emailStr = (String) m.get("MSPE_EMAIL");
			
				if(emailStr != null && !emailStr.equals("-")){
					if(covid != null && covid.trim().equals("1") && countSoftcopy > 0){
						hasilGenerate = "Polis ini sudah pernah dikirimkan softcopy polis dan covid_flag = 1 ";
						return hasilGenerate;
					}else{
						hasilGenerate = emailSoftcopySpaj(request, response, elionsManager, uwManager, bacManager, reg_spaj, products, props, currentUser, jenis);	
					}
				}else{
					hasilGenerate = "Email Nasabah NULL";
					return hasilGenerate;				
				}
			
					
		}catch(Exception e){
			System.out.println(e);
			//TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
			throw e;
		}
		conn.close();
		return hasilGenerate;
	}
	
	// Menyesuaikan request Tim UW Printing
	public void generateReport3(HttpServletRequest request,Integer mspoProvider, int flagPrePrinted, ElionsManager elionsManager, UwManager uwManager, BacManager bacManager, Properties props, Products products) throws Exception{
		
		Connection conn = null;
		try {
			//conn = getDataSource().getConnection();
			conn = uwManager.getUwDao().getDataSource().getConnection();
			//conn = getUwManager().getUwDao().getDataSource().getConnection();
			String spaj = ServletRequestUtils.getStringParameter(request, "spaj", "");
			String cabang = elionsManager.selectCabangFromSpaj(spaj);
			User currentUser = (User) request.getSession().getAttribute("currentUser");
			String jpu = ServletRequestUtils.getStringParameter(request, "seq", "");
			Integer punyaSimascard = 0;
			String flagUlink ="0";

			//1. Polis
			Map paramsPolis = printPolis(spaj, request, PrintPolisMultiController.POLIS_QUADRUPLEX, elionsManager, uwManager, bacManager, props, products);
			String businessId = FormatString.rpad("0", uwManager.selectBusinessId(spaj), 3);
			Integer businessNumber = uwManager.selectBusinessNumber(spaj);

			//helpdesk [133346] produk baru 142-13 Smart Investment Protection
			if(businessId.equalsIgnoreCase("142") && businessNumber == 13){
				paramsPolis.remove("tipePolis");
				paramsPolis.put("tipePolis", "");
			}			
			
			//2. Manfaat
			//Integer businessNumber = uwManager.selectBusinessNumber(spaj);
			Map paramsManfaat = elionsManager.prosesCetakManfaat(spaj, currentUser.getLus_id(), request);
			String pathManfaat = (String) paramsManfaat.get("reportPath");
			pathManfaat = pathManfaat.substring(17);
			paramsManfaat.put("pathManfaat", pathManfaat + ".jasper");
			paramsManfaat.remove("reportPath");
			paramsManfaat.put("lsdbs", businessNumber);
			paramsPolis.putAll(paramsManfaat);
			
			paramsPolis.put("koneksi", conn);
			List temp = new ArrayList(); Map map = new HashMap();
			map.put("halaman", "1"); temp.add(map);//polis
			map.put("halaman", "2"); temp.add(map);//manfaat
			map.put("halaman", "3"); temp.add(map);//surat
			if(products.unitLink(uwManager.selectBusinessId(spaj))) {
				map.put("halaman", "4"); temp.add(map);//kosong
				map.put("halaman", "5"); temp.add(map);//alokasi dana (bisa 1 / 2 halaman)
			}
			if(products.unitLink(uwManager.selectBusinessId(spaj))) {//surat simas card
				map.put("halaman", "7"); temp.add(map);
			}else{
				map.put("halaman", "4"); temp.add(map);
			}
			map.put("halaman", "6"); temp.add(map);
			map.put("halaman", "8"); temp.add(map);
			map.put("halaman", "9"); temp.add(map);
			map.put("halaman", "10"); temp.add(map);
			paramsPolis.put("dsManfaat", JasperReportsUtils.convertReportData(temp));
			
			//String businessId = FormatString.rpad("0", uwManager.selectBusinessId(spaj), 3);
			
			//3. Surat Polis
				String va = uwManager.selectVirtualAccountSpaj(spaj);
				if(cabang.equals("58")){
					if(elionsManager.selectValidasiTransferPbp(spaj)==0){//ini mste_flag_cc
						if(va==null){
							uwManager.updateVirtualAccountBySpaj(spaj);
						}
					}
				}
				
				Map data 		= uwManager.selectDataVirtualAccount(spaj);
				Integer lscb_id 	= ((BigDecimal) data.get("LSCB_ID")).intValue();
				String lku_id 	= (String) data.get("LKU_ID");
				
				Integer flag_cc = elionsManager.select_flag_cc(spaj);
				
				if(lku_id.equals("01") && !products.syariah(businessId,businessNumber.toString()) && lscb_id != 0 && flag_cc ==0 && !businessId.equals("196")&& !(businessId.equals("190") && "5,6".indexOf(businessNumber.toString())>=0) ){
					
					List cekSpajPromo = bacManager.selectCekSpajPromo(  null , spaj,  "1"); // cek spaj free sudah terdaftar atau belum MST_FREE_SPAJ
					
					if(!cekSpajPromo.isEmpty()){
						paramsPolis.put("pathSurat", props.getProperty("report.surat_polis.va_promo") + ".jasper");
					}else if(businessId.equals("217")&& businessNumber== 2){ //untuk ERbe di set menggunakan surat_polis
						paramsPolis.put("pathSurat", props.getProperty("report.surat_polis") + ".jasper");
					}else{
						paramsPolis.put("pathSurat", props.getProperty("report.surat_polis.va") + ".jasper");
					}		
					
					//paramsPolis.put("pathSurat", props.getProperty("report.surat_polis.va") + ".jasper");
				}else if(products.syariah(businessId, businessNumber.toString())){
					if(flag_cc==0 && !businessId.equals("175")){
						paramsPolis.put("pathSurat", props.getProperty("report.surat_polis.syariah_va") + ".jasper");
					}else{
						paramsPolis.put("pathSurat", props.getProperty("report.surat_polis.syariah") + ".jasper");
					}
				}else if(cabang.equals("58")){
					paramsPolis.put("pathSurat", props.getProperty("report.surat_polis.mall") + ".jasper");
				}
				else{
					if(businessId.equals("196")&& businessNumber==002){
						paramsPolis.put("pathSurat", props.getProperty("report.surat_polis.term") + ".jasper");
					}else{
					paramsPolis.put("pathSurat", props.getProperty("report.surat_polis") + ".jasper");
					}
				}
				
				paramsPolis.put("hamid", props.get("images.ttd.direksi")); //ttd pak hamid (Yusuf - 04/05/2006)

			
			//4. Alokasi Dana
			List viewUlink = elionsManager.selectViewUlink(spaj);
			if(viewUlink.size()!=0){
				Map paramsAlokasi = elionsManager.cetakSuratUnitLink(viewUlink, spaj, true, 1, 1,0);
				paramsAlokasi.put("elionsManager", elionsManager);
				
				String pathAlokasi = (String) paramsAlokasi.get("reportPath");
				pathAlokasi = pathAlokasi.substring(17);
				paramsAlokasi.put("pathAlokasiDana", pathAlokasi + ".jasper");
				paramsAlokasi.remove("reportPath");
				paramsAlokasi.put("dsAlokasiDana", JasperReportsUtils.convertReportData(viewUlink));
				
				paramsPolis.putAll(paramsAlokasi);
			}
			
		
			//5 Tanda Terima Polis
			Integer referal = ServletRequestUtils.getIntParameter(request, "referal", 0);
			
			List detBisnis = elionsManager.selectDetailBisnis(spaj);
			String lsbs_id = (String) ((Map) detBisnis.get(0)).get("BISNIS");
			String lsdbs = (String) ((Map) detBisnis.get(0)).get("DETBISNIS");
//			String lsbs_id = FormatString.rpad("0", uwManager.selectBusinessId(spaj), 3);
			String reportPath;
			String namaFile = props.getProperty("pdf.tanda_terima_polis");
			int isInputanBank = elionsManager.selectIsInputanBank(spaj); 
			if(((isInputanBank == 2 || isInputanBank == 3) && referal == 0 && !products.productBsmFlowStandardIndividu(Integer.parseInt(lsbs_id), Integer.parseInt(lsdbs))) || (lsbs_id.equals("175")&&lsdbs.equals("002"))) {
				reportPath = props.getProperty("report.tandaterimasertifikat");
				namaFile = props.getProperty("pdf.tanda_terima_sertifikat");
			}else if(products.unitLink(lsbs_id) && !products.stableLink(lsbs_id)) {
				if(products.syariah(lsbs_id, lsdbs)){
					reportPath = props.getProperty("report.tandaterimapolis.syariah");
				}else{
					reportPath = props.getProperty("report.tandaterimapolis.link");
				}
			}else if(lsbs_id.equals("187")){
				reportPath = props.getProperty("report.tandaterimapolis.pas");
			}else {
				reportPath = props.getProperty("report.tandaterimapolis"); //ini udah include biasa + syariah
			}
			
			paramsPolis.put("referal", referal);
			paramsPolis.put("pathTandaTerimaPolis", reportPath + ".jasper");
			
			 	
			if(flagPrePrinted==2){
				
				List daftarSebelumnya = uwManager.selectSimasCard(spaj);
				List isAgen = uwManager.selectIsSimasCardClientAnAgent(spaj);
				
				if(!daftarSebelumnya.isEmpty() && isAgen.isEmpty()){
					Map SimasCardSebelumnya = (Map) daftarSebelumnya.get(0);
					if(!Common.isEmpty(SimasCardSebelumnya.get("REG_SPAJ"))){
						if(SimasCardSebelumnya.get("REG_SPAJ").equals(spaj)){
							punyaSimascard=1;
						}else{
							elionsManager.insertMstPositionSpaj(currentUser.getLus_id(), "SUDAH PERNAH DAPAT SIMAS CARD DENGAN NO SPAJ"+ SimasCardSebelumnya.get("REG_SPAJ"), spaj, 0);
						}
					}
				}	
					
			}
			String reportPathSimasCard = props.get("report.surat_simcard").toString();
			paramsPolis.put("pathSuratSimasCard", reportPathSimasCard + ".jasper");
			
			for(Iterator iter = paramsPolis.keySet().iterator(); iter.hasNext();){
				String nama = (String) iter.next();
				logger.info(nama + " = " + paramsPolis.get(nama));
			}
			
			//SSU-SSK	
			//update 7 September 2018 Mark Valentino SSU-SSK diikutsertakan
			if(	PDFViewer.checkFileProduct(elionsManager, uwManager, props, spaj));{
				List<File> pdfFiles = new ArrayList<File>();
				
				String exportDirectory = props.getProperty("pdf.dir.export")+"\\"+cabang+"\\"+spaj;
				String exportDirTemp = props.getProperty("pdf.dir.export.temp")+"\\"+spaj;
				
		        File exportDirTemp1 = new File(props.getProperty("pdf.dir.export.temp")+"\\"+spaj);					
		        if(!exportDirTemp1.exists()) {
		        	exportDirTemp1.mkdirs();
		        }					
				
			    String dirSsk = props.getProperty("pdf.dir.syaratpolis");					
			    for(int i=0; i<detBisnis.size(); i++) {
					Map m = (HashMap) detBisnis.get(i);
					//Mark Valentino 20180906
					File fileSSU = PDFViewer.productFile(elionsManager, uwManager, dirSsk, spaj, m, props);
					File file = PDFViewer.riderFile(elionsManager,uwManager, dirSsk, lsbs_id, lsdbs, m,spaj, props);
					if(fileSSU!=null) if(fileSSU.exists()) pdfFiles.add(fileSSU);						
					if(file!=null) if(file.exists()) pdfFiles.add(file);
				}
				//PdfUtils.combinePdf(pdfFiles, exportDirectory, "pathSS.pdf");
				PdfUtils.combinePdf(pdfFiles, exportDirTemp, "pathSS.pdf");
			}	
				
			//ENDORS SMILE MEDICAL
			
			if(mspoProvider==2){
				PrintPolisPerjanjianAgent printPolis = new PrintPolisPerjanjianAgent();
				List<String> pdfs = new ArrayList<String>();
				Boolean suksesMerge = false;
				Boolean scFile=false;
				String endorsPolis = "";
				String Kartuadmedika = "";
				String Pesertaadmedika = "";
				String provider = "";
				String filename = "pathAdmedika";
				Date sysdate = elionsManager.selectSysdate();
				Boolean syariah = products.syariah(lsbs_id, lsdbs);
				
				Integer ekaSehatBaru = uwManager.selectCountEkaSehatAdmedikaNew(spaj,0);
				Integer ekaSehatHCP = uwManager.selectCountEkaSehatAdmedikaHCP(spaj);
				Integer ekaSehatPlus = uwManager.selectCountEkaSehatAdmedikaNew(spaj,1);
				Integer s=Integer.parseInt(lsdbs.substring(1));
				Integer punyaEndorsAdmedika = bacManager.selectPunyaEndorsEkaSehatAdmedika(spaj);
				
				if(punyaEndorsAdmedika == 0)bacManager.prosesEndorsKetinggalanNew(spaj, Integer.parseInt(lsbs_id));
				
				if(ekaSehatBaru>=1){
					if(lsbs_id.equals("189")|| products.syariah(lsbs_id, lsdbs)){
						if(lsbs_id.equals("189") && Integer.parseInt(lsdbs.substring(1))>15){
							endorsPolis = props.getProperty("pdf.template.admedika")+"\\EndorsemenPolisSyariahSMP.pdf";
						}else{
							endorsPolis = props.getProperty("pdf.template.admedika")+"\\EndorsemenPolisSyariah.pdf";
						}
					}else{
						endorsPolis = props.getProperty("pdf.template.admedika")+"\\EndorsementSmileMedical.pdf"; // EndorsemenPolisBaru
					}
				}else if(ekaSehatPlus>=1){				
					endorsPolis = props.getProperty("pdf.template.admedika")+"\\EndorsementSmileMedicalPlus.pdf";
				}else{
					endorsPolis = props.getProperty("pdf.template.admedika")+"\\EndorsemenPolis.pdf";
				}
				if(ekaSehatHCP>=1 && ekaSehatBaru<1 && ekaSehatPlus<1){
					Kartuadmedika = props.getProperty("pdf.template.admedika")+"\\KartuHCP.pdf";
					provider = props.getProperty("pdf.template.admedika")+"\\ProviderHCP.pdf";
				}else{
					Kartuadmedika = props.getProperty("pdf.template.admedika")+"\\KartuAdmedika.pdf";
					provider = props.getProperty("pdf.template.admedika")+"\\Provider.pdf";
				}
				Pesertaadmedika = props.getProperty("pdf.template.admedika")+"\\PesertaAdmedika.pdf";
				
				if (ekaSehatBaru>=1 || ekaSehatPlus>=1){
					pdfs.add(endorsPolis);
				}
				if(flagPrePrinted==1){
					//pdfs.add(Kartuadmedika);
					//pdfs.add(provider);
				}
				Integer total_ekasehat = uwManager.getJumlahEkaSehat(spaj);
				List<Map> datapeserta = uwManager.selectDataPeserta(spaj);
				
				
				if(ekaSehatHCP>=1){
					OutputStream output = new FileOutputStream(props.getProperty("pdf.template.admedika")+"\\MergeAdmedikaHCP.pdf");
					suksesMerge = MergePDF.concatPDFs(pdfs, output, false);
				}else if(lsbs_id.equals("189") || products.syariah(lsbs_id, lsdbs)){
					OutputStream output = new FileOutputStream(props.getProperty("pdf.template.admedika")+"\\MergeAdmedikaEkaSehatSyariah.pdf");
					suksesMerge = MergePDF.concatPDFs(pdfs, output, false);
				}else if(ekaSehatPlus>=1){
					OutputStream output = new FileOutputStream(props.getProperty("pdf.template.admedika")+"\\MergeAdmedikaMedicalPlus.pdf");
					suksesMerge = MergePDF.concatPDFs(pdfs, output, false);
				}else{
					// Andhika
					OutputStream output = new FileOutputStream(props.getProperty("pdf.template.admedika")+"\\MergeAdmedikaEkaSehatNew.pdf");
					suksesMerge = MergePDF.concatPDFs(pdfs, output, false);
				}
				
				//File userDirTemp = new File(props.getProperty("pdf.dir.export.temp")+"\\"+cabang+"\\"+spaj);
				File userDirTemp = new File(props.getProperty("pdf.dir.export.temp")+"\\"+spaj);				
		        if(!userDirTemp.exists()) {
		        	userDirTemp.mkdirs();
		        }
				
				//String outputName = props.getProperty("pdf.dir.export.temp")+"\\"+cabang+"\\"+spaj+"\\"+filename+".pdf";
				String outputName = props.getProperty("pdf.dir.export.temp")+"\\"+spaj+"\\"+filename+".pdf";				
				
				if(!scFile) {
					Map dataAdmedika = uwManager.selectDataAdmedika(spaj);
					String ingrid = props.getProperty("pdf.template.admedika2")+"\\hamid.bmp";
					printPolis.generateEndorseAdmedikaEkaSehat(spaj,total_ekasehat, outputName, dataAdmedika, datapeserta, sysdate, ingrid,lsbs_id,lsdbs,syariah,ekaSehatHCP,ekaSehatBaru,ekaSehatPlus);
				}
			}
			if(products.unitLink(uwManager.selectBusinessId(spaj))) {
				flagUlink="1";
			}
			
//			//randy - direct print e-spaj dmtm (req. timmy) - 07/09/2016
//			if (cabang.equals("40")){
//				espajdmtm3(spaj);
//			}
			
				String path ="";
				String pathTemp = "";
				path = props.getProperty("pdf.dir.export")+"\\"+cabang+"\\"+spaj;
				pathTemp = props.getProperty("pdf.dir.export.temp")+"\\"+spaj;
				String pathTemplate1 = props.getProperty("pdf.template")+"\\BlankPaper.pdf";
				String pathTemplate2 = props.getProperty("pdf.template")+"\\BlankPaper2.pdf";				
				try {
					JasperUtils.exportReportToPdfNoLock(paramsPolis.get("pathSurat").toString(), pathTemp , "pathSurat.pdf", paramsPolis, conn);
					JasperUtils.exportReportToPdfNoLock(paramsPolis.get("pathManfaat").toString(),pathTemp , "pathManfaat.pdf",paramsPolis, conn);
					JasperUtils.exportReportToPdfNoLock(paramsPolis.get("pathPolis").toString(),pathTemp , "pathPolis.pdf", paramsPolis, conn);
					JasperUtils.exportReportToPdfNoLock(paramsPolis.get("pathTandaTerimaPolis").toString(),pathTemp, "pathTandaTerimaPolis.pdf", paramsPolis, conn);
					if(viewUlink.size()!=0)JasperUtils.exportReportToPdfNoLock(paramsPolis.get("pathAlokasiDana").toString(),pathTemp, "pathAlokasiDana.pdf", paramsPolis, conn);
					if(flagPrePrinted==2)JasperUtils.exportReportToPdfNoLock(paramsPolis.get("pathSuratSimasCard").toString(),pathTemp, "pathSuratSimasCard.pdf", paramsPolis, conn);
					//Mark Valentino 20180908 Buat 2 file : PolisAll & Pelengkap
					CekPelengkap.generate2File(cabang,pathTemp,path,pathTemplate1,pathTemplate2,lsbs_id,mspoProvider,flagUlink,punyaSimascard,flagPrePrinted,request,props);
				}catch(Exception e){
			        throw e;
				}
		}finally{
				closeConnection(conn);
		}
	}
	
	
	/** PRINT POLIS **/
	private Map printPolis(String spaj, HttpServletRequest request, int singleDuplexQuadruplex, ElionsManager elionsManager, UwManager uwManager, BacManager bacManager, Properties props, Products products) throws Exception {
		Map params = new HashMap();
		User currentUser = new User();
		//20190216 Mark Valentino - request == null artinya dari Scheduler
		if(request != null){
			currentUser = (User) request.getSession().getAttribute("currentUser");
		}else{
			currentUser.setJn_bank(0);
		}
		params.put("spaj", spaj);
		params.put("props", props);
		String validasiMeterai = uwManager.validasiBeaMeterai(currentUser.getJn_bank());
		if(validasiMeterai != null){
			params.put("meterai", null);
			params.put("izin", "");
		}else{
			params.put("meterai", "Rp. 6.000,-");
			params.put("izin", elionsManager.selectIzinMeteraiTerakhir());
		}
		ServletContext servletContext = getServletContext();
		
		//jenis print ulang
		int jpu;
		if(request == null){
			jpu = -1;
		}else{
			jpu = ServletRequestUtils.getIntParameter(request, "jpu", -1);
		}	
		
		if(jpu == 1) { //PRINT ULANG POLIS
			params.put("tipePolis", "O R I G I N A L");
		}else if(jpu == 2) { //PRINT DUPLIKAT POLIS
			String seq = ServletRequestUtils.getStringParameter(request, "seq", "");
			params.put("tipePolis", "D U P L I C A T E " + "("+seq+")");
		}else if(jpu == 3) { //PRINT ULANG POLIS
			params.put("tipePolis", "O R I G I N A L");
		}
		params.put("ingrid", props.get("images.ttd.direksi")); //ttd dr. ingrid (Yusuf - 04/05/2006)
		
		String kategori = products.kategoriPrintPolis(spaj);
		String policyNumber = "09208201800001";
		 	   policyNumber = uwManager.getUwDao().selectNoPolisFromSpaj(spaj);
		PrintPolisPrintingController pppc = new PrintPolisPrintingController();
		String logoQr = "";
		if(!this.getServletContext().equals(null)) {
			logoQr = pppc.getQrBasedOnPolicy(this.getServletContext(),props,policyNumber, spaj, bacManager);
		}else {
			logoQr = pppc.getQrBasedOnPolicy(this.getServletContext(),props,policyNumber, spaj, bacManager);	
		}
		
		if( !logoQr.contains("09208201800001") ){
			params.put("Print", "cetak");
		}			
		params.put("logoQr", logoQr);			
		
		String context = this.getServletContext().getRealPath("/");
		params.put("context", context);
		
		//String kategori = "guthrie";
		logger.info("JENIS POLIS : " + kategori);	
		if(PrintPolisMultiController.POLIS == singleDuplexQuadruplex) { //single
			params.put("reportPath", "/WEB-INF/classes/" + props.getProperty("report.polis."+kategori));
		}else if(PrintPolisMultiController.POLIS_DUPLEX == singleDuplexQuadruplex) { //duplex(cetak BOLAK-BALIK)
			params.put("pathPolis", props.getProperty("report.polis."+kategori)+".jasper");
			params.put("reportPath", "/WEB-INF/classes/" + props.getProperty("report.polis.duplex"));
		}else if(PrintPolisMultiController.POLIS_QUADRUPLEX == singleDuplexQuadruplex) { //quadruplex
			params.put("pathPolis", props.getProperty("report.polis."+kategori)+".jasper");
			params.put("reportPath", "/WEB-INF/classes/" + props.getProperty("report.polis.quadruplex"));
		}else if(PrintPolisMultiController.POLIS_QUADRUPLEX_PLUS_HADIAH == singleDuplexQuadruplex) { //quadruplex
			params.put("pathPolis", props.getProperty("report.polis."+kategori)+".jasper");
			params.put("reportPath", "/WEB-INF/classes/" + props.getProperty("report.polis.quadruplex_plus_hadiah"));
		}else if(PrintPolisMultiController.POLIS_QUADRUPLEX_MEDICAL_PLUS== singleDuplexQuadruplex) { //quadruplex				
			params.put("pathPolis", props.getProperty("report.polis."+kategori+".medplus")+".jasper");
			params.put("reportPath", "/WEB-INF/classes/" + props.getProperty("report.polis.quadruplex_medical_plus"));
		}
		return params;
	}
			
	//Mark Valentino 20181030 : dicopy dari class 'PrintPolisPrintingController.java'
	//Jika ada perubahan di 'PrintPolisPrintingController.java' mohon diupdate jg disini.
	public void espajdmtm3(ServletContext servletContext, String reg_spaj, HttpServletRequest request, int singleDuplexQuadruplex, ElionsManager elionsManager, UwManager uwManager, BacManager bacManager, Properties props, Products products) throws Exception {
		
		Date sysdate = elionsManager.selectSysdate();

		String cabang = elionsManager.selectCabangFromSpaj(reg_spaj);
//		String exportDirectory = props.getProperty("pdf.dir.export")+"\\"+cabang+"\\"+reg_spaj;
		String exportDirectory = props.getProperty("pdf.dir.export.temp")+"\\"+reg_spaj;		
		Integer data2 = uwManager.selectFlagQuestionare(reg_spaj);
		String dir = props.getProperty("pdf.template.espajdmtm2");
//		File userDir = new File(props.getProperty("pdf.dir.export")+"\\"+cabang+"\\"+reg_spaj);
		File userDir = new File(props.getProperty("pdf.dir.export.temp")+"\\"+reg_spaj);		
        if(!userDir.exists()) {
            userDir.mkdirs();
        }
        String espajFile = dir + "\\espaj_"+reg_spaj+".pdf";
        
        HashMap moreInfo = new HashMap();
		moreInfo.put("Author", "PT ASURANSI JIWA SINARMAS MSIG Tbk.");
		moreInfo.put("Title", "DMTM");
		moreInfo.put("Subject", "E-SPAJ DMTM DANA SEJAHTERA");
		
		PdfContentByte over;
		BaseFont times_new_roman = BaseFont.createFont("C:\\WINDOWS\\FONTS\\ARIAL.TTF", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
		
        PdfReader reader = new PdfReader(props.getProperty("pdf.template.espajdmtm2")+"\\espajdmtm2.pdf");
//      String outputName = props.getProperty("pdf.dir.export")+"\\"+cabang+"\\"+reg_spaj+"\\"+"espajDMTM2_"+reg_spaj+".pdf";
        String outputName = props.getProperty("pdf.dir.export.temp")+"\\"+reg_spaj+"\\"+"espajDMTM2_"+reg_spaj+".pdf"; 
        PdfStamper stamp = new PdfStamper(reader,new FileOutputStream(outputName));
        
        Pemegang dataPP = elionsManager.selectpp(reg_spaj);
        Tertanggung dataTT = elionsManager.selectttg(reg_spaj);
        AddressBilling addrBill = elionsManager.selectAddressBilling(reg_spaj);
        Datausulan dataUsulan = elionsManager.selectDataUsulanutama(reg_spaj);
        InvestasiUtama inv  = elionsManager.selectinvestasiutama(reg_spaj);
        Rekening_client rekClient = elionsManager.select_rek_client(reg_spaj);
        Account_recur accRecur = elionsManager.select_account_recur(reg_spaj);//ada isinya
        List detInv = uwManager.selectdetilinvestasimallspaj(reg_spaj);
        List benef = elionsManager.select_benef(reg_spaj);
        List medQuest=uwManager.selectquestionareDMTM(reg_spaj);
        List peserta=uwManager.select_all_mst_peserta(reg_spaj);
        Integer lsre_id = uwManager.selectPolicyRelation(reg_spaj);
        List<MedQuest> mq = uwManager.selectMedQuest(reg_spaj,null);
        Agen agen =elionsManager.select_detilagen(reg_spaj);
        dataUsulan.setDaftaRider(elionsManager.selectDataUsulan_rider(reg_spaj));
        List namaBank =uwManager.namaBank(accRecur.getLbn_id());
		Map premiProdukUtama = elionsManager.selectPremiProdukUtama(reg_spaj);
		String kurs = (String) premiProdukUtama.get("LKU_ID");
        
        if(!dataUsulan.getLca_id().equals("40")){
//				ServletOutputStream sos = response.getOutputStream();
//    			sos.println("<script>alert('E-SPAJ DMTM , Hanya Untuk jalur distribusi DMTM');window.close();</script>");
//    			sos.close();
		}
        
        try {
        stamp.setMoreInfo(moreInfo);
		
		over = stamp.getOverContent(1);
		over.beginText();
		over.setFontAndSize(times_new_roman,7);
	    over.showTextAligned(PdfContentByte.ALIGN_LEFT, FormatString.nomorSPAJ(reg_spaj), 90, 646, 0);
	    over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common.isEmpty(dataPP.getMspo_no_blanko())?"-":dataPP.getMspo_no_blanko().toUpperCase(), 400, 646, 0);
	    
	    over.setFontAndSize(times_new_roman,5);
	  //Data PP
	    over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP.getMcl_first().toUpperCase(), 160, 619, 0);
	    if(dataPP.getMspe_mother() != null){
		    over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP.getMspe_mother().toUpperCase(), 160, 606, 0);	    	
	    }
	    over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP.getLside_name(), 160, 592, 0);
	    over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP.getMspe_no_identity(), 246, 592, 0);
	    over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common.isEmpty(dataPP.getLsne_note())?"-":dataPP.getLsne_note().toUpperCase(), 160, 578, 0);
	    over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP.getMspe_place_birth().toUpperCase() + ", " + FormatDate.toIndonesian(dataPP.getMspe_date_birth()), 160, 565, 0);
	    over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP.getMspe_sex2().toUpperCase(), 160, 551, 0);
	    over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP.getMspe_sts_mrt().equals("1")?"BELUM MENIKAH":(dataPP.getMspe_sts_mrt().equals("2")?"MENIKAH":(dataPP.getMspe_sts_mrt().equals("3")?"JANDA":"DUDA") ), 160, 537, 0);
	   
	    int monyong = 0;
    	String[] alamat = StringUtil.pecahParagraf(dataPP.getAlamat_rumah().toUpperCase(), 55);
    	for(int i=0; i<alamat.length; i++) {
    		monyong = 7 * i;
    		over.showTextAligned(PdfContentByte.ALIGN_LEFT, alamat[i],							160, 527-monyong, 0);
    	}
    	
    	monyong = 0;
		if(!Common.isEmpty(dataPP.getAlamat_kantor())){
			String[] alamat_kantor =  StringUtil.pecahParagraf(dataPP.getAlamat_kantor().toUpperCase(), 55);
        	for(int i=0; i<alamat_kantor.length; i++) {
        		monyong = 7 * i;
        		over.showTextAligned(PdfContentByte.ALIGN_LEFT, alamat_kantor[i],							160, 514-monyong, 0);
        	}
		}
    	
    	monyong = 0;
    	String[] alamat_billing = StringUtil.pecahParagraf(addrBill.getMsap_address(), 55);
    	for(int i=0; i<alamat_billing.length; i++) {
    		monyong = 7 * i;;
    		over.showTextAligned(PdfContentByte.ALIGN_LEFT, alamat_billing[i],							160, 500-monyong, 0);
    	}
    	
    	over.showTextAligned(PdfContentByte.ALIGN_LEFT,(Common.isEmpty( dataPP.getNo_hp())?"":dataPP.getNo_hp()) , 160, 482, 0);
    	over.showTextAligned(PdfContentByte.ALIGN_LEFT,(Common.isEmpty( dataPP.getEmail())?"":dataPP.getEmail()) , 160, 468, 0);
    	over.showTextAligned(PdfContentByte.ALIGN_LEFT,(Common.isEmpty( dataPP.getMkl_penghasilan())?"":dataPP.getMkl_penghasilan().toUpperCase()), 160, 455, 0);
    	over.showTextAligned(PdfContentByte.ALIGN_LEFT,dataPP.getMkl_pendanaan().toUpperCase(), 160, 441, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,dataPP.getMkl_kerja().toUpperCase(), 160, 427, 0);
//		over.showTextAligned(PdfContentByte.ALIGN_LEFT,dataPP.getMkl_kerja(), 160, 427, 0);
		
		over.setFontAndSize(times_new_roman,6);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,dataPP.getLsre_relation().toUpperCase(), 290, 414, 0);
		
	if(dataPP.getLsre_id()!=1){
			over.setFontAndSize(times_new_roman,5);
    		//Data tertanggung
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT.getMcl_first().toUpperCase(), 350, 619, 0);
		    over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT.getMspe_mother().toUpperCase(), 350, 606, 0);
		    over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT.getLside_name(), 350, 592, 0);
		    over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT.getMspe_no_identity(), 436, 592, 0);
		    if(dataTT.getLsne_note() == null){
		    	dataTT.setLsne_note("-");
		    }else{
			    over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT.getLsne_note().toUpperCase(), 350, 578, 0);		    	
		    }
		    over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT.getMspe_place_birth().toUpperCase() + ", " + FormatDate.toIndonesian(dataTT.getMspe_date_birth()), 350, 565, 0);
		    over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT.getMspe_sex2().toUpperCase(), 350, 551, 0);
		    if(dataTT.getMspe_sts_mrt() == null){
		    	dataTT.setMspe_sts_mrt("1");
		    }else{
			    over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT.getMspe_sts_mrt().equals("1")?"BELUM MENIKAH":(dataTT.getMspe_sts_mrt().equals("2")?"MENIKAH":(dataTT.getMspe_sts_mrt().equals("3")?"JANDA":"DUDA") ), 350, 537, 0);
		    }

			 
		    monyong = 0;
	        	String[] alamat2 = StringUtil.pecahParagraf(dataTT.getAlamat_rumah().toUpperCase(), 55);
	        	for(int i=0; i<alamat.length; i++) {
	        		monyong = 7 * i;
	        		over.showTextAligned(PdfContentByte.ALIGN_LEFT, alamat[i],							350, 527-monyong, 0);
	        	}
	        	monyong = 0;
	 			if(!Common.isEmpty(dataPP.getAlamat_kantor())){
	 				String[] alamat_kantor =  StringUtil.pecahParagraf(dataTT.getAlamat_rumah().toUpperCase()/*getAlamat_kantor().toUpperCase()*/, 55);
	 	        	for(int i=0; i<alamat_kantor.length; i++) {
	 	        		monyong = 7 * i;
	 	        		over.showTextAligned(PdfContentByte.ALIGN_LEFT, alamat_kantor[i],							350, 514-monyong, 0);
	 	        	}
	 			}
	        	
	        	monyong = 0;
	        	String[] alamat_billing2 = StringUtil.pecahParagraf(addrBill.getMsap_address(), 55);
	        	for(int i=0; i<alamat_billing.length; i++) {
	        		monyong = 7 * i;
	        		over.showTextAligned(PdfContentByte.ALIGN_LEFT, alamat_billing[i],							350, 500-monyong, 0);
	        	}
	        	
	        	over.showTextAligned(PdfContentByte.ALIGN_LEFT,(Common.isEmpty( dataTT.getNo_hp())?"":dataTT.getNo_hp()) , 350, 482, 0);
	        	over.showTextAligned(PdfContentByte.ALIGN_LEFT,(Common.isEmpty( dataTT.getEmail())?"":dataTT.getEmail()) , 350, 468, 0);
	        	over.showTextAligned(PdfContentByte.ALIGN_LEFT,(Common.isEmpty( dataTT.getMkl_penghasilan())?"":dataTT.getMkl_penghasilan().toUpperCase()), 350, 455, 0);
	        	over.showTextAligned(PdfContentByte.ALIGN_LEFT,dataTT.getMkl_pendanaan().toUpperCase(), 350, 441, 0);
//	    		over.showTextAligned(PdfContentByte.ALIGN_LEFT,dataTT.getMkl_kerja().toUpperCase(), 350, 427, 0);
	    		over.showTextAligned(PdfContentByte.ALIGN_LEFT,StringUtils.defaultString(dataTT.getMkl_kerja()).toUpperCase(), 350, 427, 0);
    	}
	    		
	    		over.setFontAndSize(times_new_roman,6);
	    	//	if (dataTT.getMste_flag_cc()==1){
		    if (accRecur!=null)
		    	{
		    			String bank_pusat = "";
		    			String bank_cabang= "";
		    			
		    			if(!namaBank.isEmpty()){
		        			HashMap m = (HashMap)namaBank.get(0);
		        			bank_pusat = (String)m.get("LSBP_NAMA");
		        			bank_cabang = (String)m.get("LBN_NAMA");
		    			}
		    			
					over.setFontAndSize(times_new_roman,5);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common.isEmpty(bank_pusat)?"":bank_pusat.toUpperCase()+" - "+(Common.isEmpty(bank_cabang)?"":bank_cabang.toUpperCase()), 110, 392, 0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common.isEmpty(bank_cabang)?"":bank_cabang.toUpperCase()/*accRecur.getLbn_nama().toUpperCase()*/, 110, 381, 0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common.isEmpty(accRecur.getMar_acc_no())?"":accRecur.getMar_acc_no().toUpperCase(), 396, 392, 0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common.isEmpty(accRecur.getMar_holder())?"":accRecur.getMar_holder().toUpperCase(), 396, 381, 0);
		    		}
			//	}
		    over.showTextAligned(PdfContentByte.ALIGN_LEFT,dataUsulan.getLsdbs_name(),155, 338, 0);
		    over.showTextAligned(PdfContentByte.ALIGN_RIGHT,FormatNumber.convertToTwoDigit(new BigDecimal(dataUsulan.getMspr_tsi())),379, 338, 0);
		    over.showTextAligned(PdfContentByte.ALIGN_RIGHT,FormatNumber.convertToTwoDigit(new BigDecimal(dataUsulan.getMspr_premium())),520, 338, 0);//dataUsulan.getMspr_ins_period().toString()
		    over.showTextAligned(PdfContentByte.ALIGN_RIGHT,dataUsulan.getMspr_ins_period().toString()+" Tahun",180, 324, 0);
		    over.showTextAligned(PdfContentByte.ALIGN_LEFT,dataUsulan.getLscb_pay_mode(),162, 310, 0);
		    over.showTextAligned(PdfContentByte.ALIGN_LEFT,dataUsulan.getMste_flag_cc()==0?"TUNAI":(dataUsulan.getMste_flag_cc()==2?"TABUNGAN":"KARTU KREDIT"),222, 299, 0);
		    over.showTextAligned(PdfContentByte.ALIGN_LEFT, FormatDate.toIndonesian(dataPP.getMste_tgl_recur()), 162, 288, 0);

			//data manfaat yang ditunjuk
    		if(benef.size()>0){
			int j=0;
			for(int i=0;i<benef.size();i++){
			Benefeciary benefi = (Benefeciary) benef.get(i);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,benefi.getMsaw_first(),86, 236-j, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,(benefi.getMsaw_sex()==1?"L":"P"),325, 236-j, 0);
			over.showTextAligned(PdfContentByte.ALIGN_CENTER,benefi.getSmsaw_birth(),375, 236-j, 0);
			over.showTextAligned(PdfContentByte.ALIGN_CENTER,benefi.getLsre_relation().toUpperCase(),463, 236-j, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,benefi.getMsaw_persen().toString(),532, 236-j, 0);
			j+=11;}
		}
    		int x=0;
			int y=0;
			int y1=0;
    	//Pertanyaan MEDIS 
			if (!mq.isEmpty()){
			if(mq.size()>2) {
//					for(int i=0;i<mq.size();i++){
		    		for(int i=0;i<2;i++){
						Integer kelamin = null;
						if(mq.get(i).getMste_insured_no()==0){
							kelamin = dataPP.getMspe_sex();
						}else if (mq.get(i).getMste_insured_no()==1){
							kelamin = dataTT.getMspe_sex();
						}
						
					// Questionare SPAJ ONLINE SYARIAH
						//Berat dan Tinggi, 1a, 1b
						over.setFontAndSize(times_new_roman,6);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, mq.get(i).getMsadm_berat().toString() +" Kg", 447+x, 198, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, mq.get(i).getMsadm_tinggi().toString()+" Cm",447+x, 189, 0);
						//Pertanyaan 2
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, (mq.get(i).getMsadm_berat_berubah()==1?"YA" : "TIDAK"), 447+x, 180, 0);
						over.setFontAndSize(times_new_roman,5);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, (Common.isEmpty(mq.get(i).getMste_insured_no())?"":(mq.get(i).getMste_insured_no()==0?"PP : ":mq.get(i).getMste_insured_no()==1?"TT : ":mq.get(i).getMste_insured_no()==2?"PT- I : ":mq.get(i).getMste_insured_no()==3?"PT- II : ":mq.get(i).getMste_insured_no()==3?"PT- III : ":""))+
						(Common.isEmpty(mq.get(i).getMsadm_berubah_desc())?"":mq.get(i).getMsadm_berubah_desc()), 76, 176-y, 0);
						//Pertanyaan 3
					    over.setFontAndSize(times_new_roman,6);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,(mq.get(i).getMsadm_penyakit()==1?"YA" : "TIDAK"),447+x, 154, 0);
						over.setFontAndSize(times_new_roman,5);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, (Common.isEmpty(mq.get(i).getMste_insured_no())?"":(mq.get(i).getMste_insured_no()==0?"PP : ":mq.get(i).getMste_insured_no()==1?"PU : ":mq.get(i).getMste_insured_no()==2?"PT- I : ":mq.get(i).getMste_insured_no()==3?"PT- II : ":mq.get(i).getMste_insured_no()==3?"PT- III : ":""))+
						(Common.isEmpty(mq.get(i).getMsadm_penyakit_desc())?"getMsadm_penyakit_desc":mq.get(i).getMsadm_penyakit_desc()), 76, 143-y, 0);
						//pertanyaan 4
						over.setFontAndSize(times_new_roman,6);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, (mq.get(i).getMsadm_medis()==1?"YA" : "TIDAK"), 447+x, 122, 0);
						over.setFontAndSize(times_new_roman,5);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, (Common.isEmpty(mq.get(i).getMste_insured_no())?"":(mq.get(i).getMste_insured_no()==0?"PP : ":mq.get(i).getMste_insured_no()==1?"PU : ":mq.get(i).getMste_insured_no()==2?"PT- I : ":mq.get(i).getMste_insured_no()==3?"PT- II : ":mq.get(i).getMste_insured_no()==3?"PT- III : ":""))+
						(Common.isEmpty(mq.get(i).getMsadm_medis_desc())?"getMsadm_medis_desc":mq.get(i).getMsadm_medis_desc()), 76, 97-y, 0);
						//Pertanyaan 5 Khusus Wanita
						if(kelamin==0){
							over.setFontAndSize(times_new_roman,6);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,(Common.isEmpty(mq.get(i).getMsadm_pregnant())?"TIDAK":(mq.get(i).getMsadm_pregnant()==1?"YA" : "TIDAK")),447+x, 76, 0);
							over.setFontAndSize(times_new_roman,5);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,(Common.isEmpty(mq.get(i).getMste_insured_no())?"":(mq.get(i).getMste_insured_no()==0?"PP : ":mq.get(i).getMste_insured_no()==1?"PU : ":mq.get(i).getMste_insured_no()==2?"PT- I : ":mq.get(i).getMste_insured_no()==3?"PT- II : ":mq.get(i).getMste_insured_no()==3?"PT- III : ":""))+
							(Common.isEmpty(mq.get(i).getMsadm_pregnant_desc())?"":mq.get(i).getMsadm_pregnant_desc()), 76, 71-y1, 0);
							y1+=6;
						}
						x+=68;
						y+=9;
					}
			}else{
				for(int i=0;i<mq.size();i++){
//			    		for(int i=0;i<2;i++){
						Integer kelamin = null;
						if(mq.get(i).getMste_insured_no()==0){
							kelamin = dataPP.getMspe_sex();
						}else if (mq.get(i).getMste_insured_no()==1){
							kelamin = dataTT.getMspe_sex();
						}
						
					// Questionare SPAJ ONLINE SYARIAH
						//Berat dan Tinggi, 1a, 1b
						over.setFontAndSize(times_new_roman,6);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, mq.get(i).getMsadm_berat().toString() +" Kg", 447+x, 198, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, mq.get(i).getMsadm_tinggi().toString()+" Cm",447+x, 189, 0);
						//Pertanyaan 2
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, (mq.get(i).getMsadm_berat_berubah()==1?"YA" : "TIDAK"), 447+x, 180, 0);
						over.setFontAndSize(times_new_roman,5);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, (Common.isEmpty(mq.get(i).getMste_insured_no())?"":(mq.get(i).getMste_insured_no()==0?"PP : ":mq.get(i).getMste_insured_no()==1?"TT : ":mq.get(i).getMste_insured_no()==2?"PT- I : ":mq.get(i).getMste_insured_no()==3?"PT- II : ":mq.get(i).getMste_insured_no()==3?"PT- III : ":""))+
						(Common.isEmpty(mq.get(i).getMsadm_berubah_desc())?"":mq.get(i).getMsadm_berubah_desc()), 76, 176-y, 0);
						//Pertanyaan 3
					    over.setFontAndSize(times_new_roman,6);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,(mq.get(i).getMsadm_penyakit()==1?"YA" : "TIDAK"),447+x, 154, 0);
						over.setFontAndSize(times_new_roman,5);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, (Common.isEmpty(mq.get(i).getMste_insured_no())?"":(mq.get(i).getMste_insured_no()==0?"PP : ":mq.get(i).getMste_insured_no()==1?"PU : ":mq.get(i).getMste_insured_no()==2?"PT- I : ":mq.get(i).getMste_insured_no()==3?"PT- II : ":mq.get(i).getMste_insured_no()==3?"PT- III : ":""))+
						(Common.isEmpty(mq.get(i).getMsadm_penyakit_desc())?"getMsadm_penyakit_desc":mq.get(i).getMsadm_penyakit_desc()), 76, 143-y, 0);
						//pertanyaan 4
						over.setFontAndSize(times_new_roman,6);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, (mq.get(i).getMsadm_medis()==1?"YA" : "TIDAK"), 447+x, 122, 0);
						over.setFontAndSize(times_new_roman,5);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, (Common.isEmpty(mq.get(i).getMste_insured_no())?"":(mq.get(i).getMste_insured_no()==0?"PP : ":mq.get(i).getMste_insured_no()==1?"PU : ":mq.get(i).getMste_insured_no()==2?"PT- I : ":mq.get(i).getMste_insured_no()==3?"PT- II : ":mq.get(i).getMste_insured_no()==3?"PT- III : ":""))+
						(Common.isEmpty(mq.get(i).getMsadm_medis_desc())?"getMsadm_medis_desc":mq.get(i).getMsadm_medis_desc()), 76, 97-y, 0);
						//Pertanyaan 5 Khusus Wanita
						if(kelamin==0){
							over.setFontAndSize(times_new_roman,6);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,(Common.isEmpty(mq.get(i).getMsadm_pregnant())?"TIDAK":(mq.get(i).getMsadm_pregnant()==1?"YA" : "TIDAK")),447+x, 76, 0);
							over.setFontAndSize(times_new_roman,5);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,(Common.isEmpty(mq.get(i).getMste_insured_no())?"":(mq.get(i).getMste_insured_no()==0?"PP : ":mq.get(i).getMste_insured_no()==1?"PU : ":mq.get(i).getMste_insured_no()==2?"PT- I : ":mq.get(i).getMste_insured_no()==3?"PT- II : ":mq.get(i).getMste_insured_no()==3?"PT- III : ":""))+
							(Common.isEmpty(mq.get(i).getMsadm_pregnant_desc())?"":mq.get(i).getMsadm_pregnant_desc()), 76, 71-y1, 0);
							y1+=6;
						}
						x+=68;
						y+=9;
					}
			}
			}
    		
    		 over.showTextAligned(PdfContentByte.ALIGN_LEFT,agen.getMcl_first(),140, 47, 0);
    		 over.showTextAligned(PdfContentByte.ALIGN_LEFT,agen.getMsag_id(),108, 47, 0);
	    over.endText();
	    reader.close();
		stamp.close();

//		in = new FileInputStream(l_file);
//	    ouputStream = response.getOutputStream();		
//		if(in != null) {
//			    in.close();
//		}
//		if(ouputStream != null) {
//			ouputStream.flush();
//		    ouputStream.close();
//		}		
		
        }catch (Exception e) {
			stamp.close();
//				logger.error("ERROR :", e);
			FileUtils.deleteFile(props.getProperty("pdf.dir.export")+"\\"+cabang+"\\"+reg_spaj, "espajDMTM2_"+reg_spaj+".pdf");
			return;
		}
        
        
        //Method baca file
//		File l_file = new File(outputName);
//		try{
//			FileInputStream in = new FileInputStream(l_file);
//		    int length = in.available();
//		    byte[] pdfbytes = new byte[length];
//		    in.read(pdfbytes);
//		    in.close();
//
//		}catch (Exception e) {
//			logger.error("ERROR :", e);
//		}
        
	}	

	// SPAJ ONLINE UNTUK GADGET
	// 20181015 Mark Valentino : dicopy dari class
	// 'PrintPolisPrintingController.java'
	// Jika ada perubahan di 'PrintPolisPrintingController.java' mohon diupdate
	// jg disini.
	public void espajonlinegadget(HttpServletRequest request,
			HttpServletResponse response, ElionsManager elionsManager,
			UwManager uwManager, BacManager bacManager, Properties props,
			Products products, String spaj) throws Exception {
		String reg_spaj = spaj;
		Integer syariah = elionsManager.getUwDao().selectSyariah(reg_spaj);
		String mspo_flag_spaj = bacManager.selectMspoFLagSpaj(reg_spaj);
		String lcaId = elionsManager.selectCabangFromSpaj(reg_spaj);
		if(lcaId.equals("09") || lcaId.equals("37") || lcaId.equals("52")){
			if (mspo_flag_spaj.equals("3")){
				if(syariah == 1){
					espajonlinegadgetfullsyariah(request, response, elionsManager, uwManager,bacManager, props, products, reg_spaj);
				}else{
					espajonlinegadgetfullkonven(request, response, elionsManager, uwManager,bacManager, props, products, reg_spaj);
				}
			}else if (mspo_flag_spaj.equals("4")){
				if(syariah == 1){
					espajonlinegadgetsiosyariah(request, response, elionsManager, uwManager,bacManager, props, products, reg_spaj);
				}else{
					espajonlinegadgetsiokonven(request, response, elionsManager, uwManager,bacManager, props, products, reg_spaj);
				}
			}
		}		
		else{
			espajonlinegadgetexisting(request, response, elionsManager, uwManager,bacManager, props, products, reg_spaj);
		}
	}
	
	public ModelAndView espajonlinegadgetfullkonven(HttpServletRequest request,
			HttpServletResponse response, ElionsManager elionsManager,
			UwManager uwManager, BacManager bacManager, Properties props,
			Products products, String spaj) throws Exception {
		String reg_spaj = spaj;
		Integer type = null;
		Integer question;
		Date sysdate = elionsManager.selectSysdate();
		List<String> pdfs = new ArrayList<String>();
		Boolean suksesMerge = false;
		HashMap<String, Object> cmd = new HashMap<String, Object>();
		ArrayList data_answer = new ArrayList();
		Integer index = null;
		Integer index2 = null;
		String spaj_gadget = "";
		String mspo_flag_spaj = bacManager.selectMspoFLagSpaj(reg_spaj);
		String cabang = elionsManager.selectCabangFromSpaj(reg_spaj);

		Map map= uwManager.selectProdInsured(reg_spaj);
		BigDecimal lsbs = (BigDecimal) map.get("LSBS_ID");
		BigDecimal lsdbs = (BigDecimal) map.get("LSDBS_NUMBER");
		String lsbs_id = lsbs.toString();
		String lsdbs_number = lsdbs.toString();
		String spajTempDirectory = props.getProperty("pdf.dir.spajtemp");
		String exportDirectory = props.getProperty("pdf.dir.export") + "\\"
				+ cabang + "\\" + reg_spaj;
		System.out.print(mspo_flag_spaj);
		String dir = props.getProperty("pdf.template.espajonlinegadget");
		OutputStream output;
		PdfReader reader;
		File userDir = new File(props.getProperty("pdf.dir.export") + "\\"
				+ cabang + "\\" + reg_spaj);
		if (!userDir.exists()) {
			userDir.mkdirs();
		}
		Datausulan dataUsulan = elionsManager
				.selectDataUsulanutama(reg_spaj);
		//--> konfigurasi prod agency NCR/2020/08/021
		HashMap confTempAgency = uwManager.getUwDao().selectMstConfig(16, "RDS","CONF_AGENCY");
		String[] conf_lca = confTempAgency.get("NAME")!=null?confTempAgency.get("NAME").toString().split(","):null;
		String[] conf_prod = confTempAgency.get("NAME2")!=null?confTempAgency.get("NAME2").toString().split(","):null;
		String prod = lsbs+"-"+lsdbs;
		boolean ConfLcaID = false;
		for(String s: conf_lca){
			if(s.equals(cabang))
				ConfLcaID=true;
		}
		
		boolean ConfProdID = false;
		for(String s: conf_prod){
			if(s.equals(prod))
				ConfProdID=true;
		}		
		//<-- done
		HashMap moreInfo = new HashMap();
		moreInfo.put("Author", "PT ASURANSI JIWA SINARMAS MSIG Tbk.");
		moreInfo.put("Title", "GADGET");
		moreInfo.put("Subject", "E-SPAJ ONLINE");

		PdfContentByte over;
		PdfContentByte over2;
		BaseFont times_new_roman = BaseFont.createFont(
				"C:\\WINDOWS\\FONTS\\ARIAL.TTF", BaseFont.CP1252,
				BaseFont.NOT_EMBEDDED);
		BaseFont italic = BaseFont.createFont("C:\\WINDOWS\\FONTS\\ariali.ttf",
				BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

		if (lsbs_id.equals("134") && lsdbs_number.equals("13")) {
			reader = new PdfReader(
					props.getProperty("pdf.template.espajonlinegadget")
							+ "\\spajonlinegadgetfullkonvenBTN.pdf");
			output = new FileOutputStream(exportDirectory + "\\"
					+ "espajonlinegadget_" + reg_spaj + ".pdf");

			spaj_gadget = dir + "\\spajonlinegadgetfullkonvenBTN.pdf";
		}else if(ConfLcaID || (ConfProdID && dataUsulan.getTipeproduk().equals(30))){ //NCR/2020/08/021
			reader = new PdfReader(
					props.getProperty("pdf.template.espajonlinegadget")
							+ "\\spajonlinegadgetfullkonvenAgency.pdf");
			output = new FileOutputStream(exportDirectory + "\\"
					+ "espajonlinegadget_" + reg_spaj + ".pdf");

			spaj_gadget = dir + "\\spajonlinegadgetfullkonvenAgency.pdf";
		} //done
		else {
			reader = new PdfReader(
					props.getProperty("pdf.template.espajonlinegadget")
							+ "\\spajonlinegadgetfullkonven.pdf");
			output = new FileOutputStream(exportDirectory + "\\"
					+ "espajonlinegadget_" + reg_spaj + ".pdf");

			spaj_gadget = dir + "\\spajonlinegadgetfullkonven.pdf";
		}
			pdfs.add(spaj_gadget);
			suksesMerge = MergePDF.concatPDFs(pdfs, output, false);
			String outputName = props.getProperty("pdf.dir.export") + "\\"
					+ cabang + "\\" + reg_spaj + "\\" + "espajonlinegadget_"
					+ reg_spaj + ".pdf";
			PdfStamper stamp = new PdfStamper(reader, new FileOutputStream(
					outputName));

			Pemegang dataPP = elionsManager.selectpp(reg_spaj);
			Tertanggung dataTT = elionsManager.selectttg(reg_spaj);
			PembayarPremi pembPremi = bacManager.selectP_premi(reg_spaj);
			if (pembPremi == null)
				pembPremi = new PembayarPremi();
			AddressBilling addrBill = elionsManager
					.selectAddressBilling(reg_spaj);

			dataUsulan.setDaftaRider(elionsManager
					.selectDataUsulan_rider(reg_spaj));
			InvestasiUtama inv = elionsManager
					.selectinvestasiutama(reg_spaj);
			Rekening_client rekClient = elionsManager
					.select_rek_client(reg_spaj);
			Account_recur accRecur = elionsManager
					.select_account_recur(reg_spaj);
			List detInv = bacManager.selectdetilinvestasi2(reg_spaj);
			List benef = elionsManager.select_benef(reg_spaj);
			List peserta = uwManager.select_all_mst_peserta(reg_spaj);
			List dist = elionsManager.select_tipeproduk();
			List listSpajTemp = bacManager.selectReferensiTempSpaj(reg_spaj);
			HashMap spajTemp = (HashMap) listSpajTemp.get(0);
			String idgadget = (String) spajTemp.get("NO_TEMP");
			Map agen = bacManager.selectAgenESPAJSimasPrima(reg_spaj);
			HashMap agenAgency= Common.serializableMap(uwManager.selectInfoAgen2(reg_spaj));
			List namaBank = uwManager.namaBank(accRecur.getLbn_id());
			String namaProduct = StringUtils.substring(dataUsulan.getLsdbs_name(), 0, 21);
			if (lsbs_id.equals("118") && (lsdbs_number.equals("3")||lsdbs_number.equals("4"))){
				dataUsulan.setLsdbs_name(namaProduct);
			}
			// --Question Full Konven/Syariah
			List rslt = bacManager.selectQuestionareGadget(reg_spaj, 2, 1, 15);	
			List rslt2 = bacManager.selectQuestionareGadget(reg_spaj, 1, 16, 18); 
			List rslt3 = bacManager.selectQuestionareGadget(reg_spaj, 3, 106, 136); 
			List rslt4 = bacManager.selectQuestionareGadget(reg_spaj, 3, 137, 145);
			List rslt5 = bacManager.selectQuestionareGadget(reg_spaj, 3, 146, 155);
			
			//Sio
			List rslt6 = bacManager.selectQuestionareGadget(reg_spaj, 12, 81, 104);
			
			String s_channel = "";
			for (int i = 0; i < dist.size(); i++) {
				HashMap dist2 = (HashMap) dist.get(i);
				Integer i_lstp_id = (Integer) dist2.get("lstp_id");
				if (i_lstp_id.intValue() == dataUsulan.getTipeproduk()
						.intValue()) {
					s_channel = (String) dist2.get("lstp_produk");
				}
			}

			Double d_premiRider = 0.;
			if (dataUsulan.getDaftaRider().size() > 0) {
				for (int i = 0; i < dataUsulan.getDaftaRider().size(); i++) {
					Datarider rider = (Datarider) dataUsulan.getDaftaRider()
							.get(i);
					d_premiRider = rider.getMspr_premium();
				}
			}
			Double d_topUpBerkala = new Double(0);
			Double d_topUpTunggal = new Double(0);
			Double d_totalTopup = new Double(0);
			if (inv != null) {
				DetilTopUp daftarTopup = inv.getDaftartopup();
				d_topUpBerkala = Common.isEmpty(daftarTopup.getPremi_berkala()) ? new Double(
						0) : daftarTopup.getPremi_berkala();
				d_topUpTunggal = Common.isEmpty(daftarTopup.getPremi_tunggal()) ? new Double(
						0) : daftarTopup.getPremi_tunggal();
				d_totalTopup = d_topUpBerkala + d_topUpTunggal;
			}
			Double d_premiExtra = (Common.isEmpty(uwManager
					.selectSumPremiExtra(reg_spaj)) ? 0. : uwManager
					.selectSumPremiExtra(reg_spaj));
			Double d_totalPremi = dataUsulan.getMspr_premium() + d_totalTopup
					+ d_premiRider + d_premiExtra;

			stamp.setMoreInfo(moreInfo);

			// ---------- Data Halaman Pertama ----------

			over = stamp.getOverContent(1);
			over.beginText();
			over.setFontAndSize(times_new_roman, 8);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					FormatString.nomorSPAJ(reg_spaj), 380, 627, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					FormatDate.toString(sysdate), 85, 617, 0);

			over.setFontAndSize(times_new_roman, 6);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP
					.getMcl_first().toUpperCase(), 160, 516, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
					.getMcl_first().toUpperCase(), 160, 506, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataUsulan.getLsdbs_name(), 160, 496, 0);

			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					dataUsulan.getLku_symbol()
							+ " "
							+ FormatNumber.convertToTwoDigit(new BigDecimal(
									dataUsulan.getMspr_tsi())), 160, 486, 0);

			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					dataUsulan.getLku_symbol()
							+ " "
							+ FormatNumber.convertToTwoDigit(new BigDecimal(
									dataUsulan.getMspr_premium())), 290, 476, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					d_topUpBerkala.doubleValue() == new Double(0) ? "-"
							: (dataUsulan.getLku_symbol() + " " + FormatNumber
									.convertToTwoDigit(new BigDecimal(
											d_topUpBerkala))), 290, 467, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					d_topUpTunggal.doubleValue() == new Double(0) ? "-"
							: (dataUsulan.getLku_symbol() + " " + FormatNumber
									.convertToTwoDigit(new BigDecimal(
											d_topUpTunggal))), 290, 457, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					d_premiRider.doubleValue() == new Double(0) ? "-"
							: (dataUsulan.getLku_symbol() + " " + FormatNumber
									.convertToTwoDigit(new BigDecimal(
											d_premiRider))), 290, 447, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					dataUsulan.getLku_symbol()
							+ " "
							+ FormatNumber.convertToTwoDigit(new BigDecimal(
									d_totalPremi)), 290, 437, 0);
			
			if (ConfLcaID){
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						agenAgency.get("MCL_FIRST").toString().toUpperCase(), 160, 409,
						0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						agenAgency.get("MSAG_ID").toString(), 160, 399, 0);
			}else if((ConfProdID && dataUsulan.getTipeproduk().equals(30))){ //NCR/2020/08/021
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						agen.get("NM_AGEN").toString().toUpperCase(), 160, 409,
						0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						agen.get("KD_AGEN").toString(), 160, 399, 0);
			}else{
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						agen.get("NM_AGEN").toString().toUpperCase(), 160, 409,
						0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						agen.get("KD_AGEN").toString(), 160, 399, 0);
			}
			

			over.endText();

			// ---------- Data Halaman Ketiga ----------
			over = stamp.getOverContent(3);
			over.beginText();
			//--pencarian file ttd
			File ttdPp = null;
		    File ttdTu = null;
		    File ttdAgen = null;
		    File ttdPenutup = null;
		    File ttdReff = null;
			
		    String pathTTD = exportDirectory;
		    File fileDirTemp = new File(pathTTD);
			File[] files = fileDirTemp.listFiles();
			if (files != null){
				for (int z = 0; z < files.length; z++) {
				       if ((files[z].toString().toUpperCase().contains("_TTD_PP_")) && (files[z].toString().toUpperCase().contains(".JPG"))) {
				    	   ttdPp = files[z];
				       }else if ((files[z].toString().toUpperCase().contains("_TTD_TU_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
				    	   ttdTu = files[z];
				       }else if ((files[z].toString().toUpperCase().contains("_TTD_AGEN_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
				    	   ttdAgen = files[z];
				       }else if ((files[z].toString().toUpperCase().contains("_TTD_PENUTUP_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
				    	   ttdPenutup = files[z];
				       }else if ((files[z].toString().toUpperCase().contains("_TTD_REF_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
				    	   ttdReff = files[z];
				       }
				    }
			}
				
		    if (ttdPp==null || ttdTu == null || ttdAgen == null || ttdPenutup == null || ttdReff == null) {
				pathTTD = spajTempDirectory+"\\"+idgadget+"\\Spaj\\";
				fileDirTemp = new File(pathTTD);
				files = fileDirTemp.listFiles();
				if (files != null){
					for (int z = 0; z < files.length; z++) {
			    	 	if ((ttdPp==null && files[z].toString().toUpperCase().contains("_TTD_PP_")) && (files[z].toString().toUpperCase().contains(".JPG"))) {
				    	   ttdPp = files[z];
				       }else if ((ttdTu==null && files[z].toString().toUpperCase().contains("_TTD_TU_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
				    	   ttdTu = files[z];
				       }else if ((ttdAgen==null && files[z].toString().toUpperCase().contains("_TTD_AGEN_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
				    	   ttdAgen = files[z];
				       }else if ((ttdPenutup==null && files[z].toString().toUpperCase().contains("_TTD_PENUTUP_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
				    	   ttdPenutup = files[z];
				       }else if ((ttdReff==null && files[z].toString().toUpperCase().contains("_TTD_REF_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
				    	   ttdReff = files[z];
				       }
				    }
				}
			    
			    if (ttdPp==null || ttdTu == null || ttdAgen == null || ttdPenutup == null || ttdReff == null) {
			    	pathTTD = props.getProperty("pdf.dir.ftp") + "\\"
							+ cabang + "\\" + reg_spaj;
					fileDirTemp = new File(pathTTD);
					files = fileDirTemp.listFiles();
					if (files != null){
						  for (int z = 0; z < files.length; z++) {
						       if ((ttdPp==null && files[z].toString().toUpperCase().contains("_TTD_PP_")) && (files[z].toString().toUpperCase().contains(".JPG"))) {
						    	   ttdPp = files[z];
						       }else if ((ttdTu==null && files[z].toString().toUpperCase().contains("_TTD_TU_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
						    	   ttdTu = files[z];
						       }else if ((ttdAgen==null && files[z].toString().toUpperCase().contains("_TTD_AGEN_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
						    	   ttdAgen = files[z];
						       }else if ((ttdPenutup==null && files[z].toString().toUpperCase().contains("_TTD_PENUTUP_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
						    	   ttdPenutup = files[z];
						       }else if ((ttdReff==null && files[z].toString().toUpperCase().contains("_TTD_REF_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
						    	   ttdReff = files[z];
						       }
						    }
					}				  
			    }
			}
			
			try {
				Image img = Image.getInstance(ttdPp.toString());
				img.scaleAbsolute(30, 30);
				over.addImage(img, img.getScaledWidth(), 0, 0,
						img.getScaledHeight(), 438, 643);
				over.stroke();
				
				if (dataTT.getMste_age() < 17 || dataPP.getLsre_id() == 1)
					ttdTu = ttdPp;
				Image img2 = Image.getInstance(ttdTu.toString());
				img2.scaleAbsolute(30, 30);
				over.addImage(img2, img2.getScaledWidth(), 0, 0,
						img2.getScaledHeight(), 438, 593);
				over.stroke();
			} catch (FileNotFoundException e) {
				logger.error("ERROR :", e );
				ServletOutputStream sos = response.getOutputStream();
				sos.println("<script>alert('TTD Pemegang Polis / Tertanggung Utama Tidak Ditemukan');window.close();</script>");
				sos.close();
			}

			over.setFontAndSize(times_new_roman, 6);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					FormatDate.toString(dataPP.getMspo_spaj_date()), 370, 715, 0);
			over.showTextAligned(PdfContentByte.ALIGN_CENTER, dataPP
					.getMcl_first().toUpperCase(), 295, 655, 0);
			over.showTextAligned(PdfContentByte.ALIGN_CENTER, dataTT
					.getMcl_first().toUpperCase(), 295, 605, 0);
			if (peserta.size() > 0) {
				Integer vertikal = 655;
				for (int i = 0; i < peserta.size(); i++) {
					vertikal = vertikal - 50;
					PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(i);
					if (pesertaPlus.getFlag_jenis_peserta() > 0){
						over.showTextAligned(PdfContentByte.ALIGN_CENTER,
								pesertaPlus.getNama().toUpperCase(), 290, vertikal,
								0);
						vertikal = vertikal + 2;
					}
				}
			}
			if (ConfLcaID){
				if (ttdAgen != null){
					Image img5 = Image.getInstance(ttdAgen.toString());
					img5.scaleAbsolute(30, 30);
					over.addImage(img5, img5.getScaledWidth(), 0, 0,
							img5.getScaledHeight(), 120, 280);
					over.stroke();
				}
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agenAgency.get("MCL_FIRST")) ? "-" : 
							agenAgency.get("MCL_FIRST").toString().toUpperCase(),100, 260,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agenAgency.get("MSAG_ID")) ? "-" : 
							agenAgency.get("MSAG_ID").toString().toUpperCase(),100, 250,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agenAgency.get("TEAM")) ? "-" : 
							agenAgency.get("TEAM").toString().toUpperCase(),100, 240,0);
				
			}else if((ConfProdID && dataUsulan.getTipeproduk().equals(30))){ //NCR/2020/08/021
				if(ttdAgen != null){
					Image img5 = Image.getInstance(ttdAgen.toString());
					img5.scaleAbsolute(30, 30);
					over.addImage(img5, img5.getScaledWidth(), 0, 0,
							img5.getScaledHeight(), 120, 280);
					over.stroke();
				}
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("NM_AGEN")) ? "-" : 
							agen.get("NM_AGEN").toString().toUpperCase(),100, 260,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("KD_AGEN")) ? "-" : 
							agen.get("KD_AGEN").toString().toUpperCase(),100, 250,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agenAgency.get("TEAM")) ? "-" : 
							agenAgency.get("TEAM").toString().toUpperCase(),100, 240,0);
			}else{
					if(ttdPenutup != null){
						Image img3 = Image.getInstance(ttdPenutup.toString());
						img3.scaleAbsolute(30, 30);
						over.addImage(img3, img3.getScaledWidth(), 0, 0,
								img3.getScaledHeight(), 120, 280);
						over.stroke();
					}
					
					if (ttdReff != null){
						Image img4 = Image.getInstance(ttdReff.toString());
						img4.scaleAbsolute(30, 30);
						over.addImage(img4, img4.getScaledWidth(), 0, 0,
								img4.getScaledHeight(), 290, 280);
						over.stroke();
					}
					
					if(ttdAgen != null){
						Image img5 = Image.getInstance(ttdAgen.toString());
						img5.scaleAbsolute(30, 30);
						over.addImage(img5, img5.getScaledWidth(), 0, 0,
								img5.getScaledHeight(), 440, 280);
						over.stroke();
					}
					
				over.setFontAndSize(times_new_roman, 6);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("NM_PENUTUP")) ? "-" : 
							agen.get("NM_PENUTUP").toString().toUpperCase(),100, 260,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("KD_PENUTUP")) ? "-" : 
							agen.get("KD_PENUTUP").toString().toUpperCase(),100, 250,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("CB_PENUTUP")) ? "-" : 
							agen.get("CB_PENUTUP").toString().toUpperCase(),100, 240,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("NM_REFFERAL")) ? "-" : 
							agen.get("NM_REFFERAL").toString().toUpperCase(),270, 260,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("KD_REFFERAL")) ? "-" : 
							agen.get("KD_REFFERAL").toString().toUpperCase(),270, 250,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("CB_REFFERAL")) ? "-" : 
							agen.get("CB_REFFERAL").toString().toUpperCase(),270, 240,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("NM_AGEN")) ? "-" : 
							agen.get("NM_AGEN").toString().toUpperCase(),440, 260,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("KD_AGEN")) ? "-" : 
							agen.get("KD_AGEN").toString().toUpperCase(),440, 250,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("CB_AGEN")) ? "-" : 
							agen.get("CB_AGEN").toString().toUpperCase(),440, 240,0);
			}
			over.endText();

			// //---------- Data Halaman Keempat ----------
			over = stamp.getOverContent(4);
			over.beginText();
			over.setFontAndSize(times_new_roman, 6);

			over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP
					.getMcl_first().toUpperCase(), 250, 725, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataPP.getMcl_gelar()) ? "-" : dataPP
							.getMcl_gelar(), 250, 715, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataPP.getMspe_mother(), 250, 705, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataPP.getLsne_note(), 250, 695, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataPP.getMcl_green_card()) ? "TIDAK"
							: dataPP.getMcl_green_card(), 250, 685, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataPP.getLside_name(), 250, 675, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataPP.getMspe_no_identity(), 250, 665, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataPP.getMspe_no_identity_expired()) ? "-"
							: FormatDate.toString(dataPP
									.getMspe_no_identity_expired()), 250, 656,
					0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					dataPP.getMspe_place_birth() + ", "
							+ FormatDate.toString(dataPP.getMspe_date_birth()),
					250, 646, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataPP.getMspo_age() + " Tahun", 250, 636, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP
					.getMspe_sex2().toUpperCase(), 250, 626, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP
					.getMspe_sts_mrt().equals("1") ? "BELUM MENIKAH" : (dataPP
					.getMspe_sts_mrt().equals("2") ? "MENIKAH" : (dataPP
					.getMspe_sts_mrt().equals("3") ? "JANDA" : "DUDA")), 250,
					617, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataPP.getLsag_name(), 250, 607, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataPP.getLsed_name(), 250, 596, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataPP.getMcl_company_name()) ? "-" : dataPP
							.getMcl_company_name(), 250, 587, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataPP.getMkl_kerja(), 250, 568, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP.getKerjab(),
					250, 559, 0);
			int monyong = 0;
			
			String[] uraian_tugas;
			if(dataTT.getMkl_kerja_ket() != null){
				uraian_tugas = StringUtil.pecahParagraf(dataTT
						.getMkl_kerja_ket().toUpperCase(), 70);
				
					for (int i = 0; i < uraian_tugas.length; i++) {
						monyong = 7 * i;
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								uraian_tugas[i], 250, 549 - monyong, 0);
				}
			}
			else{
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						"-", 250, 549 - monyong, 0);
			}
			// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
			// Common.isEmpty(dataPP.getMkl_kerja_ket())?"-":dataPP.getMkl_kerja_ket(),
			// 250, 549, 0);
			monyong = 0;
			if(!Common.isEmpty(dataTT.getAlamat_kantor())){
				String[] alamat = StringUtil.pecahParagraf(dataTT.getAlamat_kantor().toUpperCase(), 75);
	        	if(!Common.isEmpty(alamat)){
		        	for(int i=0; i<alamat.length; i++) {
		        		monyong = 7 * i;
		        		over.showTextAligned(PdfContentByte.ALIGN_LEFT, alamat[i], 250,
								529 - monyong, 0);
		        	}
	        	}
			}
//			monyong = 0;
//			String[] alamat = StringUtil.pecahParagraf(dataPP
//					.getAlamat_kantor().toUpperCase(), 70);
//			for (int i = 0; i < alamat.length; i++) {
//				monyong = 7 * i;
//				over.showTextAligned(PdfContentByte.ALIGN_LEFT, alamat[i], 250,
//						529 - monyong, 0);
//			}
			// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
			// Common.isEmpty(dataPP.getAlamat_kantor())?"-":dataPP.getAlamat_kantor(),
			// 250, 529, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataPP.getKota_kantor()) ? "-" : dataPP
							.getKota_kantor(), 250, 509, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataPP.getKd_pos_kantor()) ? "-" : dataPP
							.getKd_pos_kantor(), 250, 500, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataPP.getTelpon_kantor()) ? "-" : dataPP
							.getTelpon_kantor(), 250, 490, 0);
			// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
			// Common.isEmpty(dataPP.getTelpon_kantor2())?"-":dataPP.getTelpon_kantor2(),
			// 250, 505, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
					.isEmpty(dataPP.getNo_fax()) ? "-" : dataPP.getNo_fax(),
					250, 480, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataPP.getAlamat_rumah()) ? "-" : dataPP
							.getAlamat_rumah(), 250, 470, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataPP.getKota_rumah()) ? "-" : dataPP
							.getKota_rumah(), 250, 460, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataPP.getKd_pos_rumah()) ? "-" : dataPP
							.getKd_pos_rumah(), 250, 451, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataPP.getTelpon_rumah()) ? "-" : dataPP
							.getTelpon_rumah(), 250, 441, 0);
			// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
			// Common.isEmpty(dataPP.getTelpon_rumah2())?"-":dataPP.getTelpon_rumah2(),
			// 250, 445, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
					.isEmpty(dataPP.getNo_fax()) ? "-" : dataPP.getNo_fax(),
					250, 432, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataPP.getAlamat_tpt_tinggal()) ? "-"
							: dataPP.getAlamat_tpt_tinggal(), 250, 422, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataPP.getKota_tpt_tinggal()) ? "-" : dataPP
							.getKota_tpt_tinggal(), 250, 412, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataPP.getKd_pos_tpt_tinggal()) ? "-"
							: dataPP.getKd_pos_tpt_tinggal(), 250, 402, 0);
//			over.showTextAligned(
//					PdfContentByte.ALIGN_LEFT,
//					Common.isEmpty(dataPP.getTelpon_rumah()) ? "-" : dataPP
//							.getTelpon_rumah(), 250, 393, 0);
			 over.showTextAligned(PdfContentByte.ALIGN_LEFT,
			 Common.isEmpty(dataPP.getTelpon_rumah2())?"-":dataPP.getTelpon_rumah2(),
					 250, 393, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
					.isEmpty(dataPP.getNo_fax()) ? "-" : dataPP.getNo_fax(),
					250, 383, 0);
//			over.showTextAligned(
//					PdfContentByte.ALIGN_LEFT,
//					Common.isEmpty(addrBill.getMsap_address()) ? "-" : addrBill
//							.getMsap_address(), 250, 373, 0);
//			over.showTextAligned(
//					PdfContentByte.ALIGN_LEFT,
//					Common.isEmpty(addrBill.getMsap_address()) ? "-" : addrBill
//							.getKota(), 250, 353, 0);
//			over.showTextAligned(
//					PdfContentByte.ALIGN_LEFT,
//					Common.isEmpty(addrBill.getMsap_address()) ? "-" : addrBill
//							.getMsap_zip_code(), 250, 343, 0);
//			over.showTextAligned(
//					PdfContentByte.ALIGN_LEFT,
//					Common.isEmpty(addrBill.getMsap_address()) ? "-" : addrBill
//							.getMsap_phone1(), 250, 334, 0);
//			over.showTextAligned(
//					PdfContentByte.ALIGN_LEFT,
//					Common.isEmpty(addrBill.getMsap_address()) ? "-" : addrBill
//							.getMsap_fax1(), 250, 323, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataPP.getNo_hp()) ? "-" : dataPP.getNo_hp(),
					250, 313, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataPP.getEmail()) ? "-" : dataPP.getEmail(),
					250, 303, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataPP.getMcl_npwp()) ? "-" : dataPP
							.getMcl_npwp(), 250, 294, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataPP.getMkl_penghasilan()) ? "-" : dataPP
							.getMkl_penghasilan(), 250, 285, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataPP.getMkl_pendanaan()) ? "-" : dataPP
							.getMkl_pendanaan(), 250, 273, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataPP.getMkl_tujuan()) ? "-" : dataPP
							.getMkl_tujuan(), 250, 264, 0);

			over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
					.getLsre_relation().toUpperCase(), 250, 234, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
					.getMcl_first().toUpperCase(), 250, 215, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataTT.getMcl_gelar()) ? "-" : dataTT
							.getMcl_gelar(), 250, 206, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataTT.getMspe_mother(), 250, 196, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataTT.getLsne_note(), 250, 186, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataTT.getMcl_green_card()) ? "TIDAK"
							: dataTT.getMcl_green_card(), 250, 176, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataTT.getLside_name(), 250, 166, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataTT.getMspe_no_identity(), 250, 156, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataTT.getMspe_no_identity_expired()) ? "-"
							: FormatDate.toString(dataTT
									.getMspe_no_identity_expired()), 250, 146,
					0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					dataTT.getMspe_place_birth() + ", "
							+ FormatDate.toString(dataTT.getMspe_date_birth()),
					250, 137, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataTT.getMste_age() + " Tahun", 250, 127, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
					.getMspe_sex2().toUpperCase(), 250, 118, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
					.getMspe_sts_mrt().equals("1") ? "BELUM MENIKAH" : (dataTT
					.getMspe_sts_mrt().equals("2") ? "MENIKAH" : (dataTT
					.getMspe_sts_mrt().equals("3") ? "JANDA" : "DUDA")), 250,
					108, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataTT.getLsag_name(), 250, 98, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataTT.getLsed_name(), 250, 88, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataTT.getMcl_company_name()) ? "-" : dataTT
							.getMcl_company_name(), 250, 78, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataTT.getMkl_kerja(), 250, 58, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT.getKerjab(),
					250, 49, 0);
			over.endText();

			//
			// //---------- Data Halaman Kelima ----------
			over = stamp.getOverContent(5);
			over.beginText();
			over.setFontAndSize(times_new_roman, 6);

			monyong = 0;
			if(dataTT.getMkl_kerja_ket() != null){
				uraian_tugas = StringUtil.pecahParagraf(dataTT.getMkl_kerja_ket()
						.toUpperCase(), 70);
				for (int i = 0; i < uraian_tugas.length; i++) {
					monyong = 7 * i;
					over.showTextAligned(PdfContentByte.ALIGN_LEFT,
							uraian_tugas[i], 250, 734 - monyong, 0);
				}	
			}
			

			// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
			// Common.isEmpty(dataTT.getMkl_kerja_ket())?"-":dataTT.getMkl_kerja_ket(),
			// 250, 734, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataTT.getAlamat_kantor()) ? "-" : dataTT
							.getAlamat_kantor(), 250, 714, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataTT.getKota_kantor()) ? "-" : dataTT
							.getKota_kantor(), 250, 695, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataTT.getKd_pos_kantor()) ? "-" : dataTT
							.getKd_pos_kantor(), 250, 685, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataTT.getTelpon_kantor()) ? "-" : dataTT
							.getTelpon_kantor(), 250, 675, 0);
			// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
			// Common.isEmpty(dataTT.getTelpon_kantor2())?"-":dataTT.getTelpon_kantor2(),
			// 153, 105, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
					.isEmpty(dataTT.getNo_fax()) ? "-" : dataTT.getNo_fax(),
					250, 665, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataTT.getAlamat_rumah()) ? "-" : dataTT
							.getAlamat_rumah(), 250, 655, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataTT.getKota_rumah()) ? "-" : dataTT
							.getKota_rumah(), 250, 645, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataTT.getKd_pos_rumah()) ? "-" : dataTT
							.getKd_pos_rumah(), 250, 635, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataTT.getTelpon_rumah()) ? "-" : dataTT
							.getTelpon_rumah(), 250, 625, 0);
			// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
			// Common.isEmpty(dataTT.getTelpon_rumah2())?"-":dataTT.getTelpon_rumah2(),
			// 153, 46, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
					.isEmpty(dataTT.getNo_fax()) ? "-" : dataTT.getNo_fax(),
					250, 615, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataTT.getAlamat_tpt_tinggal()) ? "-"
							: dataTT.getAlamat_tpt_tinggal(), 250, 607, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataTT.getKota_tpt_tinggal()) ? "-" : dataTT
							.getKota_tpt_tinggal(), 250, 597, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataTT.getKd_pos_tpt_tinggal()) ? "-"
							: dataTT.getKd_pos_tpt_tinggal(), 250, 587, 0);
			// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
			// Common.isEmpty(dataTT.getTelpon_rumah())?"-":dataTT.getTelpon_rumah(),
			// 250, 597, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataTT.getTelpon_rumah2()) ? "-" : dataTT
							.getTelpon_rumah2(), 250, 578, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
					.isEmpty(dataTT.getNo_fax()) ? "-" : dataTT.getNo_fax(),
					250, 567, 0);
			// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
			// Common.isEmpty(addrBill.getMsap_address())?"-":addrBill.getMsap_address(),
			// 208, 739, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataTT.getNo_hp()) ? "-" : dataTT.getNo_hp(),
					250, 557, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataTT.getEmail()) ? "-" : dataTT.getEmail(),
					250, 547, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataTT.getMcl_npwp()) ? "-" : dataTT
							.getMcl_npwp(), 250, 537, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataTT.getMkl_penghasilan()) ? "-" : dataTT
							.getMkl_penghasilan(), 250, 529, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataTT.getMkl_pendanaan()) ? "-" : dataTT
							.getMkl_pendanaan(), 250, 519, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(dataTT.getMkl_tujuan()) ? "-" : dataTT
							.getMkl_tujuan(), 250, 509, 0);
			//
			// //Data Pembayar Premi
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(pembPremi.getRelation_payor()) ? "-"
							: pembPremi.getRelation_payor(), 250, 478, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
					.isEmpty(pembPremi.getNama_pihak_ketiga()) ? "-"
					: pembPremi.getNama_pihak_ketiga(), 250, 468, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(pembPremi.getKewarganegaraan()) ? "-"
							: pembPremi.getKewarganegaraan(), 250, 458, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(pembPremi.getAlamat_lengkap()) ? "-"
							: pembPremi.getAlamat_lengkap(), 250, 450, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(pembPremi.getTelp_rumah()) ? "-" : pembPremi
							.getTelp_rumah(), 250, 440, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(pembPremi.getTelp_kantor()) ? "-"
							: pembPremi.getTelp_kantor(), 250, 430, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(pembPremi.getEmail()) ? "-" : pembPremi
							.getEmail(), 250, 420, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(pembPremi.getTempat_lahir()) ? "-"
							: (pembPremi.getTempat_lahir() + ", " + FormatDate.toString(pembPremi
									.getMspe_date_birth_3rd_pendirian())), 250,
					410, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(pembPremi.getMkl_kerja()) ? "-" : pembPremi
							.getMkl_kerja(), 250, 401, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 392, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 382, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 372, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(pembPremi.getNo_npwp()) ? "-" : pembPremi
							.getNo_npwp(), 250, 362, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(pembPremi.getSumber_dana()) ? "-"
							: pembPremi.getSumber_dana(), 250, 352, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(pembPremi.getTujuan_dana_3rd()) ? "-"
							: pembPremi.getTujuan_dana_3rd(), 250, 342, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 332, 0);
			//
			// //Data Usulan
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					s_channel.toUpperCase(), 250, 291, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataUsulan.getLsdbs_name(), 250, 281, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataUsulan.getLsdbs_name(), 250, 271, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataUsulan.getMspr_ins_period() + " Tahun", 250, 261, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataUsulan.getMspo_pay_period() + " Tahun", 250, 251, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
					.isEmpty(dataUsulan.getMspo_installment()) ? "-"
					: dataUsulan.getMspo_installment() + "", 250, 242, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataUsulan.getLscb_pay_mode(), 250, 232, 0);
			// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
			// dataUsulan.getLku_symbol() + " " +
			// FormatNumber.convertToTwoDigit(new
			// BigDecimal(dataUsulan.getMspr_premium())), 250, 286, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					dataUsulan.getLku_symbol()
							+ " "
							+ FormatNumber.convertToTwoDigit(new BigDecimal(
									dataUsulan.getMspr_tsi())), 250, 222, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					dataUsulan.getMste_flag_cc() == 0 ? "TUNAI" : (dataUsulan
							.getMste_flag_cc() == 2 ? "TABUNGAN"
							: "KARTU KREDIT"), 250, 211, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					FormatDate.toIndonesian(dataUsulan.getMste_beg_date()),
					250, 202, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					FormatDate.toIndonesian(dataUsulan.getMste_end_date()),
					250, 193, 0);
			// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
			// FormatDate.toIndonesian(dataUsulan.getLsdbs_number()>800?dataUsulan.getLsdbs_name():"-"),
			// 250, 237, 0);
			// over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 221,
			// 0);

			if (dataUsulan.getDaftaRider().size() > 0) {
				Integer j = 0;
				for (int i = 0; i < dataUsulan.getDaftaRider().size(); i++) {
					Datarider rider = (Datarider) dataUsulan.getDaftaRider()
							.get(i);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT,
							rider.getLsdbs_name(), 270, 163 - j, 0);
					over.showTextAligned(
							PdfContentByte.ALIGN_LEFT,
							dataUsulan.getLku_symbol()
									+ " "
									+ FormatNumber
											.convertToTwoDigit(new BigDecimal(
													rider.getMspr_tsi())), 440,
													163 - j, 0);
					j += 10;
				}
			}

			// //Data Investasi
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					dataUsulan.getLku_symbol()
							+ " "
							+ FormatNumber.convertToTwoDigit(new BigDecimal(
									dataUsulan.getMspr_premium())), 250, 97, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					d_topUpBerkala.doubleValue() == new Double(0) ? "-"
							: (dataUsulan.getLku_symbol() + " " + FormatNumber
									.convertToTwoDigit(new BigDecimal(
											d_topUpBerkala))), 250, 87, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					d_topUpTunggal.doubleValue() == new Double(0) ? "-"
							: (dataUsulan.getLku_symbol() + " " + FormatNumber
									.convertToTwoDigit(new BigDecimal(
											d_topUpTunggal))), 250, 77, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					d_premiExtra.doubleValue() == new Double(0) ? "-"
							: (dataUsulan.getLku_symbol() + " " + FormatNumber
									.convertToTwoDigit(new BigDecimal(
											d_premiExtra))), 250, 68, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					dataUsulan.getLku_symbol()
							+ " "
							+ FormatNumber.convertToTwoDigit(new BigDecimal(
									d_totalPremi)), 250, 58, 0);
			Double d_jmlinves = new Double(0);
			String s_jnsinves = "";
			for (int i = 0; i < detInv.size(); i++) {
				DetilInvestasi detInves = (DetilInvestasi) detInv.get(i);
				d_jmlinves = d_jmlinves + detInves.getMdu_jumlah1();
				s_jnsinves = s_jnsinves
						+ detInves.getLji_invest1().toUpperCase() + " "
						+ detInves.getMdu_persen1() + "%";
				if (i != (detInv.size() - 1))
					s_jnsinves = s_jnsinves + ", ";
			}
			// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
			// dataUsulan.getLku_symbol() + " " +
			// FormatNumber.convertToTwoDigit(new BigDecimal(d_jmlinves)), 208,
			// 183, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, s_jnsinves, 250,
					47, 0);
			over.endText();

			// ---------- Data Halaman Keenam ----------

			over = stamp.getOverContent(6);
			over.beginText();
			over.setFontAndSize(times_new_roman, 6);

			// //Data Rekening
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					rekClient.getLsbp_nama(), 250, 724, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					rekClient.getMrc_cabang(), 250, 714, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					rekClient.getMrc_no_ac(), 250, 704, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					rekClient.getMrc_nama(), 250, 694, 0);

			if (dataTT.getMste_flag_cc() == 1 || dataTT.getMste_flag_cc() == 2) {
				if (accRecur != null) {
					String bank_pusat = "";
					String bank_cabang = "";

					if (!namaBank.isEmpty()) {
						HashMap m = (HashMap) namaBank.get(0);
						bank_pusat = (String) m.get("LSBP_NAMA");
						bank_cabang = (String) m.get("LBN_NAMA");
					}
					over.showTextAligned(
							PdfContentByte.ALIGN_LEFT,
							dataUsulan.getMste_flag_cc() == 0 ? "TUNAI"
									: (dataUsulan.getMste_flag_cc() == 2 ? "TABUNGAN"
											: "KARTU KREDIT"), 250, 631, 0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT,
							Common.isEmpty(bank_pusat) ? "-"
									: bank_pusat.toUpperCase()/*
															 * accRecur.getLbn_nama
															 * ().toUpperCase()
															 */, 250, 621, 0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
							.isEmpty(bank_cabang) ? "-"
							: bank_cabang.toUpperCase()/*
																 * accRecur.
																 * getLbn_nama
																 * ().
																 * toUpperCase()
																 */, 250, 611,
							0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
							.isEmpty(accRecur.getMar_acc_no()) ? "-" : accRecur
							.getMar_acc_no().toUpperCase()/*
														 * accRecur.getLbn_nama()
														 * .toUpperCase()
														 */, 250, 601, 0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
							.isEmpty(accRecur.getMar_holder()) ? "-" : accRecur
							.getMar_holder().toUpperCase()/*
														 * accRecur.getLbn_nama()
														 * .toUpperCase()
														 */, 250, 592, 0);
				}
			} else {
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 631,
						0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 621,
						0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 611,
						0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 601,
						0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 592,
						0);
			}

			if (peserta.size() > 0) {
				Integer j = 0;
				for (int i = 0; i < peserta.size(); i++) {

					PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(i);
					if (pesertaPlus.getFlag_jenis_peserta() > 0){
						int pesertatambahan = 0;
						String[] nama = StringUtil.pecahParagraf(pesertaPlus.getNama().toUpperCase(), 20);
						for (int z = 0; z < nama.length; z++) {
							pesertatambahan = 7 * z;
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									nama[z], 76, 517 - pesertatambahan, 0);
						}
//						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
//								pesertaPlus.getNama().toUpperCase(), 76, 517 - j,
//								0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								pesertaPlus.getLsre_relation(), 190, 517 - j, 0);
						over.showTextAligned(
								PdfContentByte.ALIGN_LEFT,
								pesertaPlus.getTinggi() + "/"
										+ pesertaPlus.getBerat(), 260, 517 - j, 0);
						String[] kerjaan = StringUtil.pecahParagraf(pesertaPlus.getKerja().toUpperCase(), 20);
						for (int z = 0; z < kerjaan.length; z++) {
							pesertatambahan = 7 * z;
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									kerjaan[z], 290, 517 - pesertatambahan, 0);
						}
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								pesertaPlus.getSex(), 375, 517 - j, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								FormatDate.toString(pesertaPlus.getTgl_lahir()),
								430, 517 - j, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								dataPP.getLsne_note(), 480, 517 - j, 0);
						j += 10;
					}	
				}
			}
			if (benef.size() > 0) {
				Integer j = 0;
				for (int i = 0; i < benef.size(); i++) {
					Benefeciary benefit = (Benefeciary) benef.get(i);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT,
							benefit.getMsaw_first(), 55, 380 - j, 0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT,
							benefit.getSmsaw_birth(), 200, 380 - j, 0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT,
							benefit.getMsaw_sex() == 1 ? "LAKI-LAKI" : "PEREMPUAN",
							270, 380 - j, 0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT,
							benefit.getMsaw_persen() + "%", 331, 380 - j, 0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, benefit
							.getLsre_relation().toUpperCase(), 375, 380 - j, 0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT,
							dataPP.getLsne_note(), 475, 380 - j, 0);
					j += 10;
				}
			}
			// -----------data tertanggung-----------
			String jawab = "";
			if (mspo_flag_spaj.equals("3")){
				if (rslt.size() > 0) {
					Integer j = 0;
					for (int i = 0; i < rslt.size(); i++) {
						MstQuestionAnswer ans = (MstQuestionAnswer) rslt.get(i);
						if (ans.getQuestion_id() == 1) {
							if (ans.getOption_type() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "v";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 510, 308, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("0")) {
								jawab = "-";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 510, 308, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "v";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 530, 308, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("0")) {
								jawab = "-";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 530, 308, 0);
							} else {
								jawab = ans.getAnswer2();
								over.setFontAndSize(italic, 6);
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										Common.isEmpty(jawab) ? "-" : jawab, 100,
										290, 0);
							}
						}
						if (ans.getQuestion_id() == 2) {
							if (ans.getOption_type() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "v";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 510, 279, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("0")) {
								jawab = "-";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 510, 279, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "v";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 530, 279, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("0")) {
								jawab = "-";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 530, 279, 0);
							} else {
								jawab = ans.getAnswer2();
								over.setFontAndSize(italic, 6);
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										Common.isEmpty(jawab) ? "-" : jawab, 100,
										270, 0);
							}
						}
						if (ans.getQuestion_id() == 3) {
							if (ans.getOption_type() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "v";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 510, 260, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("0")) {
								jawab = "-";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 510, 260, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "v";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 530, 260, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("0")) {
								jawab = "-";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 530, 260, 0);
							} else {
								jawab = ans.getAnswer2();
								over.setFontAndSize(italic, 6);
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										Common.isEmpty(jawab) ? "-" : jawab, 100,
										230, 0);
							}
						}
						if (ans.getQuestion_id() == 4) {
							if (ans.getOption_type() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "v";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 510, 220, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("0")) {
								jawab = "-";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 510, 220, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "v";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 530, 220, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("0")) {
								jawab = "-";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 530, 220, 0);
							} else {
								jawab = ans.getAnswer2();
								over.setFontAndSize(italic, 6);
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										Common.isEmpty(jawab) ? "-" : jawab, 100,
										190, 0);
							}
						}
						if (ans.getQuestion_id() == 5) {
							if (ans.getOption_type() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "v";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 510, 180, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("0")) {
								jawab = "-";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 510, 180, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "v";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 530, 180, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("0")) {
								jawab = "-";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 530, 180, 0);
							} else {
								jawab = ans.getAnswer2();
								over.setFontAndSize(italic, 6);
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										Common.isEmpty(jawab) ? "-" : jawab, 100,
										163, 0);
							}
						}
						if (ans.getQuestion_id() == 6) {
							if (ans.getOption_type() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "v";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 510, 153, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("0")) {
								jawab = "-";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 510, 153, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "v";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 530, 153, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("0")) {
								jawab = "-";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 530, 153, 0);
							} else {
								jawab = ans.getAnswer2();
								over.setFontAndSize(italic, 6);
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										Common.isEmpty(jawab) ? "-" : jawab, 100,
										143, 0);
							}
						}	
						if (ans.getQuestion_id() == 7) {
							if (ans.getOption_type() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "v";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 510, 113, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("0")) {
								jawab = "-";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 510, 113, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "v";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 530, 113, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("0")) {
								jawab = "-";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 530, 113, 0);
							} else {
								jawab = ans.getAnswer2();
								over.setFontAndSize(italic, 6);
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										Common.isEmpty(jawab) ? "-" : jawab, 100,
										84, 0);
							}
						}
						if (ans.getQuestion_id() == 8) {
							if (ans.getOption_type() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "v";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 510, 73, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("0")) {
								jawab = "-";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 510, 73, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "v";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 530, 73, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("0")) {
								jawab = "-";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 530, 73, 0);
							}
						}
						if (ans.getQuestion_id() == 9) {
							if (ans.getOption_type() == 0
									&& ans.getOption_order() == 1) {
								jawab = ans.getAnswer2();
								over.setFontAndSize(italic, 6);
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 115, 62, 0);
							} else if (ans.getOption_type() == 0
									&& ans.getOption_order() == 2) {
								jawab = ans.getAnswer2();
								over.setFontAndSize(italic, 6);
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 160, 52, 0);
							}
						}
					}	
			}
			}
			over.endText();

			// ------------Halaman tujuh-----------
			over = stamp.getOverContent(7);
			over.beginText();
			over.setFontAndSize(times_new_roman, 6);

			if (rslt.size() > 0) {
				Integer j = 0;
				for (int i = 0; i < rslt.size(); i++) {
					MstQuestionAnswer ans = (MstQuestionAnswer) rslt.get(i);
					if (ans.getQuestion_id() == 10) {
						if (ans.getOption_type() == 1
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "v";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 510, 735, 0);
						} else if (ans.getOption_type() == 1
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("0")) {
							jawab = "-";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 510, 735, 0);
						} else if (ans.getOption_type() == 1
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "v";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 530, 735, 0);
						} else if (ans.getOption_type() == 1
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("0")) {
							jawab = "-";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 530, 735, 0);
						}
					}
					if (ans.getQuestion_id() == 11) {
						if (ans.getOption_type() == 1
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "v";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 510, 715, 0);
						} else if (ans.getOption_type() == 1
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("0")) {
							jawab = "-";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 510, 715, 0);
						} else if (ans.getOption_type() == 1
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "v";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 530, 715, 0);
						} else if (ans.getOption_type() == 1
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("0")) {
							jawab = "-";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 530, 715, 0);
						}
					}
					if (ans.getQuestion_id() == 12) {
						if (ans.getOption_type() == 1
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "v";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 510, 695, 0);
						} else if (ans.getOption_type() == 1
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("0")) {
							jawab = "-";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 510, 695, 0);
						} else if (ans.getOption_type() == 1
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "v";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 530, 695, 0);
						} else if (ans.getOption_type() == 1
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("0")) {
							jawab = "-";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 530, 695, 0);
						} else {
							jawab = ans.getAnswer2();
							over.setFontAndSize(italic, 6);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									Common.isEmpty(jawab) ? "-" : jawab, 100,
									685, 0);
						}
					}
					if (ans.getQuestion_id() == 13) {
						if (ans.getOption_type() == 4
								&& ans.getOption_order() == 1) {
							jawab = ans.getAnswer2();
							over.setFontAndSize(italic, 6);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 150, 667, 0);
						} else if (ans.getOption_type() == 4
								&& ans.getOption_order() == 2) {
							jawab = ans.getAnswer2();
							over.setFontAndSize(italic, 6);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 250, 667, 0);
						}
					}
					if (ans.getQuestion_id() == 15) {
						if (ans.getOption_type() == 4
								&& ans.getOption_order() == 1) {
							jawab = ans.getAnswer2();
							over.setFontAndSize(italic, 6);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 150, 645, 0);
						} else if (ans.getOption_type() == 4
								&& ans.getOption_order() == 2) {
							jawab = ans.getAnswer2();
							over.setFontAndSize(italic, 6);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 250, 645, 0);
						}
					}
				}
			}

			// -------------data pemegang polis--------------
			if (rslt2.size() > 0) {
				Integer j = 0;
				for (int i = 0; i < rslt2.size(); i++) {
					MstQuestionAnswer ans = (MstQuestionAnswer) rslt2.get(i);
					if (ans.getQuestion_id() == 16) {
						if (ans.getOption_type() == 1
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "v";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 510, 615, 0);
						} else if (ans.getOption_type() == 1
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("0")) {
							jawab = "-";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 510, 615, 0);
						} else if (ans.getOption_type() == 1
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "v";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 530, 615, 0);
						} else if (ans.getOption_type() == 1
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("0")) {
							jawab = "-";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 530, 615, 0);
						} else if (ans.getOption_type() == 0
								&& ans.getOption_order() == 1) {
							jawab = ans.getAnswer2();
							over.setFontAndSize(italic, 6);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									Common.isEmpty(jawab) ? "-" : jawab, 100,
									595, 0);
						} else {
							jawab = ans.getAnswer2();
							over.setFontAndSize(italic, 6);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									Common.isEmpty(jawab) ? "-" : jawab, 100,
									595, 0);
						}
					}
					if (ans.getQuestion_id() == 17) {
						if (ans.getOption_type() == 1
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "v";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 510, 587, 0);
						} else if (ans.getOption_type() == 1
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("0")) {
							jawab = "-";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 510, 587, 0);
						} else if (ans.getOption_type() == 1
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "v";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 530, 587, 0);
						} else if (ans.getOption_type() == 1
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("0")) {
							jawab = "-";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 530, 587, 0);
						} else if (ans.getOption_type() == 0
								&& ans.getOption_order() == 1) {
							jawab = ans.getAnswer2();
							over.setFontAndSize(italic, 6);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									Common.isEmpty(jawab) ? "-" : jawab, 100,
									577, 0);
						} else {
							jawab = ans.getAnswer2();
							over.setFontAndSize(italic, 6);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									Common.isEmpty(jawab) ? "-" : jawab, 100,
									577, 0);
						}
					}
					if (ans.getQuestion_id() == 18) {
						if (ans.getOption_type() == 4
								&& ans.getOption_order() == 1) {
							jawab = ans.getAnswer2();
							over.setFontAndSize(italic, 6);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 150, 558, 0);
						} else if (ans.getOption_type() == 4
								&& ans.getOption_order() == 2) {
							jawab = ans.getAnswer2();
							over.setFontAndSize(italic, 6);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 250, 558, 0);
						}
					}
				}
			}
			
			over.endText();
			
			
			// ------------Halaman kedelapan-----------	
			
			over = stamp.getOverContent(8);
			over.beginText();
			over.setFontAndSize(times_new_roman, 6);

			if (rslt3.size() > 0 ) {
				Integer j = 0;
				for (int i = 0; i < rslt3.size(); i++) {
					MstQuestionAnswer ans = (MstQuestionAnswer) rslt3.get(i);
						if ((ans.getOption_group() == 1
								|| ans.getOption_group() == 2
								|| ans.getOption_group() == 3
								|| ans.getOption_group() == 4 || ans
								.getOption_group() == 5)
								&& (ans.getOption_order() == 1 || ans
										.getOption_order() == 2)
								&& ans.getAnswer2() == null) {
							jawab = null;
							over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
									360, 700 - j, 0);
							// --pp
							
						} else if (ans.getOption_group() == 1
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "Ya";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
									360, 681 - j, 0);
						} else if (ans.getOption_group() == 1
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "Tidak";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
									360, 681 - j, 0);
						}else if (ans.getOption_group() == 2
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "Ya";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
									395, 683 - j, 0);
						} else if (ans.getOption_group() == 2
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "Tidak";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
									395, 683 - j, 0);
						}
						
						if (peserta.size() > 0 && ans.getAnswer2() != null) {
							for (int x = 0; x < peserta.size(); x++) {
								PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);	
								// --tt1/pt1
								if (pesertaPlus.getFlag_jenis_peserta()==1 
										&& ans.getOption_group() == 3
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
											425, 685 - j, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
											425, 685 - j, 0);
								}
								// --tt2/pt2
								else if (pesertaPlus.getFlag_jenis_peserta()==2 
										&& ans.getOption_group() == 4
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
											463, 688 - j, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
											463, 688 - j, 0);
								}
								// --tt3/pt3
								else if (pesertaPlus.getFlag_jenis_peserta()==3 
										&& ans.getOption_group() == 5
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
											493, 690 - j, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
											493, 690 - j, 0);
								}
								// --tt4/pt4
								else if (pesertaPlus.getFlag_jenis_peserta()==4 
										&& ans.getOption_group() == 6
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
											540, 692 - j, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
											540, 692 - j, 0);
								}
								
							}
						}else if (dataTT.getLsre_id()==1){
							if (ans.getOption_group() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
										395, 680 - j, 0);
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
										395, 680 - j, 0);
							}
						}
					
					j += 1;
				}
			}

			if (rslt4.size() > 0) {
				Integer j = 0;
				for (int i = 0; i < rslt4.size(); i++) {
					MstQuestionAnswer ans = (MstQuestionAnswer) rslt4.get(i);
					if (ans.getQuestion_id() == 137) {
						if ((ans.getOption_group() == 1
								|| ans.getOption_group() == 2
								|| ans.getOption_group() == 3
								|| ans.getOption_group() == 4 || ans
								.getOption_group() == 5)
								&& (ans.getOption_order() == 1 || ans
										.getOption_order() == 2)
								&& ans.getAnswer2() == null) {
							jawab = null;
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 100, 700, 0);
							// --pp
						} else if (ans.getOption_group() == 1
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "Ya";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 360, 356, 0);
							
						} else if (ans.getOption_group() == 1
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "Tidak";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 360, 356, 0);
						}
						// --tu/pu
						else if (ans.getOption_group() == 2
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "Ya";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 395, 356, 0);
						}
						else if (ans.getOption_group() == 2
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "Tidak";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 395, 356, 0);
						}
						 
						 else if (ans.getOption_group() == 0) {
							jawab = ans.getAnswer2();
							over.setFontAndSize(italic, 6);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									Common.isEmpty(jawab) ? "-" : jawab, 100,
									320, 0);
						}
						
						
						if (peserta.size() > 0 && ans.getAnswer2() != null) {
							for (int x = 0; x < peserta.size(); x++) {
								PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
								
								// --tt1
								if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 425, 356, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 425, 356, 0);
								}
								// --tt2
								else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 463, 356, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 463, 356, 0);
								}
								// --tt3
								else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 493, 356, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 493, 356, 0);
								}
								// --tt4
								else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 540, 356, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 540, 356, 0);
								}
								
							}
						}else if (dataTT.getLsre_id()==1){
							if (ans.getOption_group() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 356, 0);
								
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 356, 0);
							}
						}

					}
							
					if (ans.getQuestion_id() == 139) {
						if ((ans.getOption_group() == 1
								|| ans.getOption_group() == 2
								|| ans.getOption_group() == 3
								|| ans.getOption_group() == 4 || ans
								.getOption_group() == 5)
								&& (ans.getOption_order() == 1 || ans
										.getOption_order() == 2)
								&& ans.getAnswer2() == null) {
							jawab = null;
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 100, 676, 0);
							// --pp
						} else if (ans.getOption_group() == 1
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "Ya";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 360, 288, 0);
						} else if (ans.getOption_group() == 1
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "Tidak";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 360, 288, 0);
						}
						// --tu
						else if (ans.getOption_group() == 2
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "Ya";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 395, 288, 0);
						} else if (ans.getOption_group() == 2
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "Tidak";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 395, 288, 0);
						}
						if (peserta.size() > 0 && ans.getAnswer2() != null) {
							for (int x = 0; x < peserta.size(); x++) {
								PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
								// --tt1
								if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 425, 288, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 425, 288, 0);
								}
								// --tt2
								else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 463, 288, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 463, 288, 0);
								}
								// --tt3
								else if (pesertaPlus.getFlag_jenis_peserta()==3 &&ans.getOption_group() == 5
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 493, 288, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 493, 288, 0);
								}
								// --tt4
								else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 540, 288, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 540, 288, 0);
								}
							}
						}else if (dataTT.getLsre_id()==1){
							if (ans.getOption_group() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 288, 0);
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 288, 0);
							}
						}
						
					}
					// j += 3;
					if (ans.getQuestion_id() >= 140
							&& ans.getQuestion_id() <= 143) {
						if ((ans.getOption_group() == 1
								|| ans.getOption_group() == 2
								|| ans.getOption_group() == 3
								|| ans.getOption_group() == 4 || ans
								.getOption_group() == 5)
								&& (ans.getOption_order() == 1 || ans
										.getOption_order() == 2)
								&& ans.getAnswer2() == null) {
							jawab = null;
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 100, 700, 0);
							// --pp
						} else if (ans.getOption_group() == 1
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "Ya";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 360, 323 - j, 0);
						} else if (ans.getOption_group() == 1
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "Tidak";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 360, 323 - j, 0);
						}
						// --tu
						else if (ans.getOption_group() == 2
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "Ya";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 395, 327 - j, 0);
						} else if (ans.getOption_group() == 2
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "Tidak";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 395, 327 - j, 0);
						}
						if (peserta.size() > 0 && ans.getAnswer2() != null) {
							for (int x = 0; x < peserta.size(); x++) {
								PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
								// --tt1
								 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 425, 331 - j, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 425, 331 - j, 0);
								}
								// --tt2
								else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 463, 335 - j, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 463, 335 - j, 0);
								}
								// --tt3
								else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 493, 339 - j, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 493, 339 - j, 0);
								}
								// --tt4
								else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 540, 343 - j, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 540, 343 - j, 0);
								}
							}
							}else if (dataTT.getLsre_id()==1){
								 if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 395, 324 - j, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 395, 324 - j, 0);
									}
							}
						}
						
					j += 2;
					if (ans.getQuestion_id() == 144) {
						if ((ans.getOption_group() == 1
								|| ans.getOption_group() == 2
								|| ans.getOption_group() == 3
								|| ans.getOption_group() == 4 || ans
								.getOption_group() == 5)
								&& (ans.getOption_order() == 1 || ans
										.getOption_order() == 2)
								&& ans.getAnswer2() == null) {
							jawab = null;
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 100, 676, 0);
							// --pp
						} else if (ans.getOption_group() == 1
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "Ya";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 360, 188, 0);
						} else if (ans.getOption_group() == 1
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "Tidak";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 360, 188, 0);
						}
						// --tu
						else if (ans.getOption_group() == 2
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "Ya";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 395, 188, 0);
						} else if (ans.getOption_group() == 2
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "Tidak";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 395, 188, 0);
						}
						if (peserta.size() > 0 && ans.getAnswer2() != null) {
							for (int x = 0; x < peserta.size(); x++) {
								PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
								// --tt1
								 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 425, 188, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 425, 188, 0);
								}
								// --tt2
								else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 463, 188, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 463, 188, 0);
								}
								// --tt3
								else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 493, 188, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 493, 188, 0);
								}
								// --tt4
								else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 540, 188, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 540, 188, 0);
								}
							}
							}else if (dataTT.getLsre_id()==1){
								if (ans.getOption_group() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 395, 186, 0);
								} else if (ans.getOption_group() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 395, 186, 0);
								}
							}
						}
						
							
					if (ans.getQuestion_id() == 145) {
						if ((ans.getOption_group() == 1
								|| ans.getOption_group() == 2
								|| ans.getOption_group() == 3
								|| ans.getOption_group() == 4 || ans
								.getOption_group() == 5)
								&& (ans.getOption_order() == 1 || ans
										.getOption_order() == 2)
								&& ans.getAnswer2() == null) {
							jawab = null;
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 100, 676, 0);
							// --pp
						} else if (ans.getOption_group() == 1
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "Ya";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 360, 178, 0);
						} else if (ans.getOption_group() == 1
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "Tidak";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 360, 168, 0);
						}
						// --tu
						else if (ans.getOption_group() == 2
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "Ya";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 395, 168, 0);
						} else if (ans.getOption_group() == 2
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "Tidak";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 395, 168, 0);
						}
						if (peserta.size() > 0 && ans.getAnswer2() != null) {
							for (int x = 0; x < peserta.size(); x++) {
								PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
								
								// --tt1
								if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 425, 168, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 425, 168, 0);
								}
								// --tt2
								else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 463, 168, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 463, 168, 0);
								}
								// --tt3
								else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 493, 168, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 493, 168, 0);
								}
								// --tt4
								else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 540, 168, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 540, 168, 0);
								}
							}
						}else  if (dataTT.getLsre_id()==1){
							if (ans.getOption_group() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 166, 0);
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 166, 0);
							}
						}
					}
				}
			}

			over.endText();

			// --------------Halaman sembilan--------------//
			over = stamp.getOverContent(9);
			over.beginText();
			over.setFontAndSize(times_new_roman, 6);

			if (rslt5.size() > 0) {
				Integer j = 0;
				for (int i = 0; i < rslt5.size(); i++) {
					MstQuestionAnswer ans = (MstQuestionAnswer) rslt5.get(i);
					if (ans.getQuestion_id() == 146) {
						if ((ans.getOption_group() == 1
								|| ans.getOption_group() == 2
								|| ans.getOption_group() == 3
								|| ans.getOption_group() == 4 || ans
								.getOption_group() == 5)
								&& (ans.getOption_order() == 1 || ans
										.getOption_order() == 2)
								&& ans.getAnswer2() == null) {
							jawab = null;
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 100, 676, 0);
							// --pp
						} else if (ans.getOption_group() == 1
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "Ya";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 360, 735, 0);
						} else if (ans.getOption_group() == 1
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "Tidak";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 360, 735, 0);
						}
						// --tu
						else if ( ans.getOption_group() == 2
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "Ya";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 395, 735, 0);
						} else if (ans.getOption_group() == 2
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "Tidak";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 395, 735, 0);
						}
						if (peserta.size() > 0 && ans.getAnswer2() != null) {
							for (int x = 0; x < peserta.size(); x++) {
								PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
								
								// --tt1
								 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 425, 735, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 425, 735, 0);
								}
								// --tt2
								else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 463, 735, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 463, 735, 0);
								}
								// --tt3
								else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 493, 735, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 493, 735, 0);
								}
								// --tt4
								else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 540, 735, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 540, 735, 0);
								}
							}
						}else  if (dataTT.getLsre_id()==1){
							if (ans.getOption_group() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 735, 0);
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 735, 0);
							}
						}
					}
					// j += 3;
					if (ans.getQuestion_id() == 147) {
						jawab = ans.getAnswer2();
						over.setFontAndSize(italic, 6);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								Common.isEmpty(jawab) ? "-" : jawab, 120, 697,
								0);
					} else if (ans.getQuestion_id() == 148) {
						jawab = ans.getAnswer2();
						over.setFontAndSize(italic, 6);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								Common.isEmpty(jawab) ? "-" : jawab, 120, 667,
								0);
					} else if (ans.getQuestion_id() == 149) {
						jawab = ans.getAnswer2();
						over.setFontAndSize(italic, 6);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								Common.isEmpty(jawab) ? "-" : jawab, 120, 637,
								0);
					} else if (ans.getQuestion_id() == 150) {
						jawab = ans.getAnswer2();
						over.setFontAndSize(italic, 6);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								Common.isEmpty(jawab) ? "-" : jawab, 120, 607,
								0);
					} else if (ans.getQuestion_id() == 151) {
						jawab = ans.getAnswer2();
						over.setFontAndSize(italic, 6);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								Common.isEmpty(jawab) ? "-" : jawab, 120, 578,
								0);
					}
					if (ans.getQuestion_id() == 152) {
						if ((ans.getOption_group() == 1
								|| ans.getOption_group() == 2
								|| ans.getOption_group() == 3
								|| ans.getOption_group() == 4 || ans
								.getOption_group() == 5)
								&& (ans.getOption_order() == 1 || ans
										.getOption_order() == 2)
								&& ans.getAnswer2() == null) {
							jawab = null;
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 100, 676, 0);
							// --pp
						} else if (ans.getOption_group() == 1
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "Ya";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 360, 588, 0);
						} else if (ans.getOption_group() == 1
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "Tidak";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 360, 588, 0);
						}
						// --tu
						else if (ans.getOption_group() == 2
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "Ya";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 395, 588, 0);
						} else if (ans.getOption_group() == 2
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "Tidak";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 395, 588, 0);
						}
						
						if (peserta.size() > 0 && ans.getAnswer2() != null) {
							for (int x = 0; x < peserta.size(); x++) {
								PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);						
								// --tt1
								if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 425, 588, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 425, 588, 0);
								}
								// --tt2
								else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 463, 588, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 463, 588, 0);
								}
								// --tt3
								else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 493, 588, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 493, 588, 0);
								}
								// --tt4
								else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 540, 588, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 540, 588, 0);
								}
							}
						}else if(ans.getAnswer2() != null && dataTT.getLsre_id()==1){
							if (ans.getOption_group() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 588, 0);
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 588, 0);
							}
						}
							
					}
					if (ans.getQuestion_id() == 153) {
						if ((ans.getOption_group() == 1
								|| ans.getOption_group() == 2
								|| ans.getOption_group() == 3
								|| ans.getOption_group() == 4 || ans
								.getOption_group() == 5)
								&& (ans.getOption_order() == 1 || ans
										.getOption_order() == 2)
								&& ans.getAnswer2() == null) {
							jawab = null;
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 100, 676, 0);
							// --pp
						} else if (ans.getOption_group() == 1
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "Ya";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 360, 568, 0);
						} else if (ans.getOption_group() == 1
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "Tidak";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 360, 568, 0);
						}
						// --tu
						else if (ans.getOption_group() == 2
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "Ya";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 395, 568, 0);
						} else if (ans.getOption_group() == 2
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "Tidak";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 395, 568, 0);
						}
						if (peserta.size() > 0 && ans.getAnswer2() != null) {
							for (int x = 0; x < peserta.size(); x++) {
								PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
								
								// --tt1
								 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 425, 568, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 425, 568, 0);
								}
								// --tt2
								else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 463, 568, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 463, 568, 0);
								}
								// --tt3
								else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 493, 568, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 493, 568, 0);
								}
								// --tt4
								else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 540, 568, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 540, 568, 0);
								}
							}
						}else if(ans.getAnswer2() != null && dataTT.getLsre_id()==1){
							if (ans.getOption_group() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 568, 0);
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 568, 0);
							}
						}
						
					}
					if (ans.getQuestion_id() == 154) {
						if ((ans.getOption_group() == 1
								|| ans.getOption_group() == 2
								|| ans.getOption_group() == 3
								|| ans.getOption_group() == 4 || ans
								.getOption_group() == 5)
								&& (ans.getOption_order() == 1 || ans
										.getOption_order() == 2)
								&& ans.getAnswer2() == null) {
							jawab = null;
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 100, 676, 0);
							// --pp
						} else if (ans.getOption_group() == 1
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "Ya";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 360, 548, 0);
						} else if (ans.getOption_group() == 1
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "Tidak";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 360, 548, 0);
						}
						// --tu
						else if (ans.getOption_group() == 2
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "Ya";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 395, 548, 0);
						} else if (ans.getOption_group() == 2
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "Tidak";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 395, 548, 0);
						}
						if (peserta.size() > 0 && ans.getAnswer2() != null) {
							for (int x = 0; x < peserta.size(); x++) {
								PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
								
								// --tt1
								 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 425, 548, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 425, 548, 0);
								}
								// --tt2
								else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 463, 548, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 463, 548, 0);
								}
								// --tt3
								else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 493, 548, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 493, 548, 0);
								}
								// --tt4
								else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 540, 548, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 540, 548, 0);
								}
							}
						}else if(ans.getAnswer2() != null && dataTT.getLsre_id()==1){
							if (ans.getOption_group() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 548, 0);
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 548, 0);
							}
						}
						
					}
					if (ans.getQuestion_id() == 155) {
						if ((ans.getOption_group() == 1
								|| ans.getOption_group() == 2
								|| ans.getOption_group() == 3
								|| ans.getOption_group() == 4 || ans
								.getOption_group() == 5)
								&& (ans.getOption_order() == 1 || ans
										.getOption_order() == 2)
								&& ans.getAnswer2() == null) {
							jawab = null;
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 100, 676, 0);
							// --pp
						} else if (ans.getOption_group() == 1
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "Ya";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 360, 528, 0);
						} else if (ans.getOption_group() == 1
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "Tidak";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 360, 528, 0);
						}
						// --tu
						else if (ans.getOption_group() == 2
								&& ans.getOption_order() == 1
								&& ans.getAnswer2().equals("1")) {
							jawab = "Ya";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 395, 528, 0);
						} else if (ans.getOption_group() == 2
								&& ans.getOption_order() == 2
								&& ans.getAnswer2().equals("1")) {
							jawab = "Tidak";
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									jawab, 395, 528, 0);
						}
						if (peserta.size() > 0 && ans.getAnswer2() != null) {
							for (int x = 0; x < peserta.size(); x++) {
								PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
								
								// --tt1
								 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 425, 528, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 425, 528, 0);
								}
								// --tt2
								else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 463, 528, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 463, 528, 0);
								}
								// --tt3
								else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 493, 528, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 493, 528, 0);
								}
								// --tt4
								else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 540, 528, 0);
								} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 540, 528, 0);
								}
							}
						}else if (dataTT.getLsre_id()==1){
							if (ans.getOption_group() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 528, 0);
							} else if ( ans.getOption_group() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 528, 0);
							}
						}	
					}
				}
				j += 2;
			}
			over.endText();
			stamp.close();
			
		return null;
	}
	
	public ModelAndView espajonlinegadgetsiokonven(HttpServletRequest request,
			HttpServletResponse response, ElionsManager elionsManager,
			UwManager uwManager, BacManager bacManager, Properties props,
			Products products, String spaj) throws Exception {
		String reg_spaj = spaj;
	Integer type = null;
	Integer question;
	Date sysdate = elionsManager.selectSysdate();
	List<String> pdfs = new ArrayList<String>();
	Boolean suksesMerge = false;
	HashMap<String, Object> cmd = new HashMap<String, Object>();
	ArrayList data_answer = new ArrayList();
	Integer index = null;
	Integer index2 = null;
	String spaj_gadget = "";
	String mspo_flag_spaj = bacManager.selectMspoFLagSpaj(reg_spaj);
	String cabang = elionsManager.selectCabangFromSpaj(reg_spaj);
	Map map= uwManager.selectProdInsured(reg_spaj);
	BigDecimal lsbs = (BigDecimal) map.get("LSBS_ID");
	BigDecimal lsdbs = (BigDecimal) map.get("LSDBS_NUMBER");
	String lsbs_id = lsbs.toString();
	String lsdbs_number = lsdbs.toString();
	Datausulan dataUsulan = elionsManager
			.selectDataUsulanutama(reg_spaj);
	//--> konfigurasi prod agency
	HashMap agenAgency= Common.serializableMap(uwManager.selectInfoAgen2(reg_spaj));
	HashMap confTempAgency = uwManager.getUwDao().selectMstConfig(16, "RDS","CONF_AGENCY");
	String[] conf_lca = confTempAgency.get("NAME")!=null?confTempAgency.get("NAME").toString().split(","):null;
	String[] conf_prod = confTempAgency.get("NAME2")!=null?confTempAgency.get("NAME2").toString().split(","):null;
	String prod = lsbs+"-"+lsdbs;
	boolean ConfLcaID = false;
	for(String s: conf_lca){
		if(s.equals(cabang))
			ConfLcaID=true;
		}
			
	boolean ConfProdID = false;
	for(String s: conf_prod){
		if(s.equals(prod))
			ConfProdID=true;
		}		
	//<-- done
			
	String exportDirectory = props.getProperty("pdf.dir.export") + "\\"
			+ cabang + "\\" + reg_spaj;
	String spajTempDirectory = props.getProperty("pdf.dir.spajtemp");
	String storagespajtemp = props.getProperty("pdf.dir.ftp");
	System.out.print(mspo_flag_spaj);
	String dir = props.getProperty("pdf.template.espajonlinegadget");
	OutputStream output;
	PdfReader reader;
	File userDir = new File(props.getProperty("pdf.dir.export") + "\\"
			+ cabang + "\\" + reg_spaj);
	if (!userDir.exists()) {
		userDir.mkdirs();
	}

	HashMap moreInfo = new HashMap();
	moreInfo.put("Author", "PT ASURANSI JIWA SINARMAS MSIG Tbk.");
	moreInfo.put("Title", "GADGET");
	moreInfo.put("Subject", "E-SPAJ ONLINE");

	PdfContentByte over;
	PdfContentByte over2;
	BaseFont times_new_roman = BaseFont.createFont(
			"C:\\WINDOWS\\FONTS\\ARIAL.TTF", BaseFont.CP1252,
			BaseFont.NOT_EMBEDDED);
	BaseFont italic = BaseFont.createFont("C:\\WINDOWS\\FONTS\\ariali.ttf",
			BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			if(ConfLcaID || (ConfProdID && dataUsulan.getTipeproduk().equals(30))){ //NCR/2020/08/021
				reader = new PdfReader(
						props.getProperty("pdf.template.espajonlinegadget")
								+ "\\spajonlinegadgetsiokonvenAgency.pdf");
				output = new FileOutputStream(exportDirectory + "\\"
						+ "espajonlinegadget_" + reg_spaj + ".pdf");
		
				spaj_gadget = dir + "\\spajonlinegadgetsiokonvenAgency.pdf";
			}else{
			reader = new PdfReader(
					props.getProperty("pdf.template.espajonlinegadget")
							+ "\\spajonlinegadgetsiokonven.pdf");
			output = new FileOutputStream(exportDirectory + "\\"
					+ "espajonlinegadget_" + reg_spaj + ".pdf");

			spaj_gadget = dir + "\\spajonlinegadgetsiokonven.pdf";
			}
		pdfs.add(spaj_gadget);
		suksesMerge = MergePDF.concatPDFs(pdfs, output, false);
		String outputName = props.getProperty("pdf.dir.export") + "\\"
				+ cabang + "\\" + reg_spaj + "\\" + "espajonlinegadget_"
				+ reg_spaj + ".pdf";
		PdfStamper stamp = new PdfStamper(reader, new FileOutputStream(
				outputName));

		Pemegang dataPP = elionsManager.selectpp(reg_spaj);
		Tertanggung dataTT = elionsManager.selectttg(reg_spaj);
		PembayarPremi pembPremi = bacManager.selectP_premi(reg_spaj);
		if (pembPremi == null)
			pembPremi = new PembayarPremi();
		AddressBilling addrBill = elionsManager
				.selectAddressBilling(reg_spaj);
		
		dataUsulan.setDaftaRider(elionsManager
				.selectDataUsulan_rider(reg_spaj));
		InvestasiUtama inv = elionsManager
				.selectinvestasiutama(reg_spaj);
		Rekening_client rekClient = elionsManager
				.select_rek_client(reg_spaj);
		Account_recur accRecur = elionsManager
				.select_account_recur(reg_spaj);
		List detInv = bacManager.selectdetilinvestasi2(reg_spaj);
		List benef = elionsManager.select_benef(reg_spaj);
		List peserta = uwManager.select_all_mst_peserta(reg_spaj);
		List dist = elionsManager.select_tipeproduk();
		List listSpajTemp = bacManager.selectReferensiTempSpaj(reg_spaj);
		HashMap spajTemp = (HashMap) listSpajTemp.get(0);
		String idgadget = (String) spajTemp.get("NO_TEMP");
		Map agen = bacManager.selectAgenESPAJSimasPrima(reg_spaj);
		List namaBank = uwManager.namaBank(accRecur.getLbn_id());

		// --Question Full Konven/Syariah
		List rslt = bacManager.selectQuestionareGadget(reg_spaj, 2, 1, 15);	
		List rslt2 = bacManager.selectQuestionareGadget(reg_spaj, 1, 16, 18); 
		List rslt3 = bacManager.selectQuestionareGadget(reg_spaj, 3, 106, 136); 
		List rslt4 = bacManager.selectQuestionareGadget(reg_spaj, 3, 137, 145);
		List rslt5 = bacManager.selectQuestionareGadget(reg_spaj, 3, 146, 155);
		
		//Sio
		List rslt6 = bacManager.selectQuestionareGadget(reg_spaj, 12, 81, 104);
		
		String s_channel = "";
		for (int i = 0; i < dist.size(); i++) {
			HashMap dist2 = (HashMap) dist.get(i);
			Integer i_lstp_id = (Integer) dist2.get("lstp_id");
			if (i_lstp_id.intValue() == dataUsulan.getTipeproduk()
					.intValue()) {
				s_channel = (String) dist2.get("lstp_produk");
			}
		}

		Double d_premiRider = 0.;
		if (dataUsulan.getDaftaRider().size() > 0) {
			for (int i = 0; i < dataUsulan.getDaftaRider().size(); i++) {
				Datarider rider = (Datarider) dataUsulan.getDaftaRider()
						.get(i);
				d_premiRider = rider.getMspr_premium();
			}
		}
		Double d_topUpBerkala = new Double(0);
		Double d_topUpTunggal = new Double(0);
		Double d_totalTopup = new Double(0);
		if (inv != null) {
			DetilTopUp daftarTopup = inv.getDaftartopup();
			d_topUpBerkala = Common.isEmpty(daftarTopup.getPremi_berkala()) ? new Double(
					0) : daftarTopup.getPremi_berkala();
			d_topUpTunggal = Common.isEmpty(daftarTopup.getPremi_tunggal()) ? new Double(
					0) : daftarTopup.getPremi_tunggal();
			d_totalTopup = d_topUpBerkala + d_topUpTunggal;
		}
		Double d_premiExtra = (Common.isEmpty(uwManager
				.selectSumPremiExtra(reg_spaj)) ? 0. : uwManager
				.selectSumPremiExtra(reg_spaj));
		Double d_totalPremi = dataUsulan.getMspr_premium() + d_totalTopup
				+ d_premiRider + d_premiExtra;

		stamp.setMoreInfo(moreInfo);

		// ---------- Data Halaman Pertama ----------

		over = stamp.getOverContent(1);
		over.beginText();
		over.setFontAndSize(times_new_roman, 8);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				FormatString.nomorSPAJ(reg_spaj), 380, 627, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				FormatDate.toString(sysdate), 85, 617, 0);

		over.setFontAndSize(times_new_roman, 6);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP
				.getMcl_first().toUpperCase(), 160, 516, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
				.getMcl_first().toUpperCase(), 160, 506, 0);
		if (lsbs_id.equals("118") && lsdbs_number.equals("3")){
			dataUsulan.setLsdbs_name("SMiLe LINK 88 SINGLE");
		}else if(lsbs_id.equals("118") && lsdbs_number.equals("4")){
			dataUsulan.setLsdbs_name("SMiLe LINK 88 REGULER");
		}else{
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataUsulan.getLsdbs_name(), 160, 496, 0);
		}
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				dataUsulan.getLku_symbol()
						+ " "
						+ FormatNumber.convertToTwoDigit(new BigDecimal(
								dataUsulan.getMspr_tsi())), 160, 486, 0);

		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				dataUsulan.getLku_symbol()
						+ " "
						+ FormatNumber.convertToTwoDigit(new BigDecimal(
								dataUsulan.getMspr_premium())), 290, 476, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				d_topUpBerkala.doubleValue() == new Double(0) ? "-"
						: (dataUsulan.getLku_symbol() + " " + FormatNumber
								.convertToTwoDigit(new BigDecimal(
										d_topUpBerkala))), 290, 467, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				d_topUpTunggal.doubleValue() == new Double(0) ? "-"
						: (dataUsulan.getLku_symbol() + " " + FormatNumber
								.convertToTwoDigit(new BigDecimal(
										d_topUpTunggal))), 290, 457, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				d_premiRider.doubleValue() == new Double(0) ? "-"
						: (dataUsulan.getLku_symbol() + " " + FormatNumber
								.convertToTwoDigit(new BigDecimal(
										d_premiRider))), 290, 447, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				dataUsulan.getLku_symbol()
						+ " "
						+ FormatNumber.convertToTwoDigit(new BigDecimal(
								d_totalPremi)), 290, 437, 0);

		if (ConfLcaID){
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					agenAgency.get("MCL_FIRST").toString().toUpperCase(), 160, 409,
					0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					agenAgency.get("MSAG_ID").toString(), 160, 399, 0);
		}else if ((ConfProdID && dataUsulan.getTipeproduk().equals(30))){ //NCR/2020/08/021
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					agen.get("NM_AGEN").toString().toUpperCase(), 160, 409,
					0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					agen.get("KD_AGEN").toString(), 160, 399, 0);
		}else{
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					agen.get("NM_AGEN").toString().toUpperCase(), 160, 409,
					0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					agen.get("KD_AGEN").toString(), 160, 399, 0);
		}

		over.endText();

				// ---------- Data Halaman Ketiga ----------
				over = stamp.getOverContent(3);
				over.beginText();
				//--pencarian file ttd
				File ttdPp = null;
			    File ttdTu = null;
			    File ttdAgen = null;
			    File ttdPenutup = null;
			    File ttdReff = null;
				
			    String pathTTD = exportDirectory;
			    File fileDirTemp = new File(pathTTD);
				File[] files = fileDirTemp.listFiles();
				if(files != null){
					for (int z = 0; z < files.length; z++) {
					       if ((files[z].toString().toUpperCase().contains("_TTD_PP_")) && (files[z].toString().toUpperCase().contains(".JPG"))) {
					    	   ttdPp = files[z];
					       }else if ((files[z].toString().toUpperCase().contains("_TTD_TU_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdTu = files[z];
					       }else if ((files[z].toString().toUpperCase().contains("_TTD_AGEN_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdAgen = files[z];
					       }else if ((files[z].toString().toUpperCase().contains("_TTD_PENUTUP_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdPenutup = files[z];
					       }else if ((files[z].toString().toUpperCase().contains("_TTD_REF_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdReff = files[z];
					       }
					    }
				}
					
				
			    if (ttdPp==null || ttdTu == null || ttdAgen == null || ttdPenutup == null || ttdReff == null) {
					pathTTD = spajTempDirectory+"\\"+idgadget+"\\Spaj\\";
					fileDirTemp = new File(pathTTD);
					files = fileDirTemp.listFiles();
					if(files != null){
						for (int z = 0; z < files.length; z++) {
				    	 	if ((ttdPp==null && files[z].toString().toUpperCase().contains("_TTD_PP_")) && (files[z].toString().toUpperCase().contains(".JPG"))) {
					    	   ttdPp = files[z];
					       }else if ((ttdTu==null && files[z].toString().toUpperCase().contains("_TTD_TU_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdTu = files[z];
					       }else if ((ttdAgen==null && files[z].toString().toUpperCase().contains("_TTD_AGEN_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdAgen = files[z];
					       }else if ((ttdPenutup==null && files[z].toString().toUpperCase().contains("_TTD_PENUTUP_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdPenutup = files[z];
					       }else if ((ttdReff==null && files[z].toString().toUpperCase().contains("_TTD_REF_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdReff = files[z];
					       }
					    }	
					}
				    
				    if (ttdPp==null || ttdTu == null || ttdAgen == null || ttdPenutup == null || ttdReff == null) {
				    	pathTTD = props.getProperty("pdf.dir.ftp") + "\\"
								+ cabang + "\\" + reg_spaj;
						fileDirTemp = new File(pathTTD);
						files = fileDirTemp.listFiles();
						if(files != null){
							 for (int z = 0; z < files.length; z++) {
							       if ((ttdPp==null && files[z].toString().toUpperCase().contains("_TTD_PP_")) && (files[z].toString().toUpperCase().contains(".JPG"))) {
							    	   ttdPp = files[z];
							       }else if ((ttdTu==null && files[z].toString().toUpperCase().contains("_TTD_TU_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
							    	   ttdTu = files[z];
							       }else if ((ttdAgen==null && files[z].toString().toUpperCase().contains("_TTD_AGEN_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
							    	   ttdAgen = files[z];
							       }else if ((ttdPenutup==null && files[z].toString().toUpperCase().contains("_TTD_PENUTUP_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
							    	   ttdPenutup = files[z];
							       }else if ((ttdReff==null && files[z].toString().toUpperCase().contains("_TTD_REF_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
							    	   ttdReff = files[z];
							       }
							    }
						}
				    }
				}
				
				try {
					Image img = Image.getInstance(ttdPp.toString());
					img.scaleAbsolute(30, 30);
					over.addImage(img, img.getScaledWidth(), 0, 0,
							img.getScaledHeight(), 438, 643);
					over.stroke();

					if (dataTT.getMste_age() < 17 || dataPP.getLsre_id() == 1)
						ttdTu = ttdPp;
					Image img2 = Image.getInstance(ttdTu.toString());
					img2.scaleAbsolute(30, 30);
					over.addImage(img2, img2.getScaledWidth(), 0, 0,
							img2.getScaledHeight(), 438, 593);
					over.stroke();
				} catch (FileNotFoundException e) {
					logger.error("ERROR :", e);
					ServletOutputStream sos = response.getOutputStream();
					sos.println("<script>alert('TTD Pemegang Polis / Tertanggung Utama Tidak Ditemukan');window.close();</script>");
					sos.close();
				}

				over.setFontAndSize(times_new_roman, 6);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						FormatDate.toString(dataPP.getMspo_spaj_date()), 370, 715, 0);
				over.showTextAligned(PdfContentByte.ALIGN_CENTER, dataPP
						.getMcl_first().toUpperCase(), 295, 655, 0);
				over.showTextAligned(PdfContentByte.ALIGN_CENTER, dataTT
						.getMcl_first().toUpperCase(), 295, 605, 0);
				if (peserta.size() > 0) {
					Integer vertikal = 605;
					for (int i = 0; i < peserta.size(); i++) {
						vertikal = vertikal - 50;
						PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(i);
						if (pesertaPlus.getFlag_jenis_peserta() > 0){
							over.showTextAligned(PdfContentByte.ALIGN_CENTER,
									pesertaPlus.getNama().toUpperCase(), 290, vertikal,
									0);
							vertikal = vertikal + 2;
						}
						
					}
				}if (ConfLcaID){
					if (ttdAgen != null){
						Image img5 = Image.getInstance(ttdAgen.toString());
						img5.scaleAbsolute(30, 30);
						over.addImage(img5, img5.getScaledWidth(), 0, 0,
								img5.getScaledHeight(), 120, 280);
						over.stroke();
					}
					over.showTextAligned(
							PdfContentByte.ALIGN_LEFT,
							Common.isEmpty(agenAgency.get("MCL_FIRST")) ? "-" : 
								agenAgency.get("MCL_FIRST").toString().toUpperCase(),100, 260,0);
					over.showTextAligned(
							PdfContentByte.ALIGN_LEFT,
							Common.isEmpty(agenAgency.get("MSAG_ID")) ? "-" : 
								agenAgency.get("MSAG_ID").toString().toUpperCase(),100, 250,0);
					over.showTextAligned(
							PdfContentByte.ALIGN_LEFT,
							Common.isEmpty(agenAgency.get("TEAM")) ? "-" : 
								agenAgency.get("TEAM").toString().toUpperCase(),100, 240,0);
				}else if((ConfProdID && dataUsulan.getTipeproduk().equals(30))){ //NCR/2020/08/021
					if(ttdAgen != null){
						Image img5 = Image.getInstance(ttdAgen.toString());
						img5.scaleAbsolute(30, 30);
						over.addImage(img5, img5.getScaledWidth(), 0, 0,
								img5.getScaledHeight(), 120, 280);
						over.stroke();
					}
					over.showTextAligned(
							PdfContentByte.ALIGN_LEFT,
							Common.isEmpty(agen.get("NM_AGEN")) ? "-" : 
								agen.get("NM_AGEN").toString().toUpperCase(),100, 260,0);
					over.showTextAligned(
							PdfContentByte.ALIGN_LEFT,
							Common.isEmpty(agen.get("KD_AGEN")) ? "-" : 
								agen.get("KD_AGEN").toString().toUpperCase(),100, 250,0);
					over.showTextAligned(
							PdfContentByte.ALIGN_LEFT,
							Common.isEmpty(agenAgency.get("TEAM")) ? "-" : 
								agenAgency.get("TEAM").toString().toUpperCase(),100, 240,0);
				}else{
				
					if(ttdPenutup != null){
						Image img3 = Image.getInstance(ttdPenutup.toString());
						img3.scaleAbsolute(30, 30);
						over.addImage(img3, img3.getScaledWidth(), 0, 0,
								img3.getScaledHeight(), 120, 280);
						over.stroke();
					}
						
					if(ttdReff != null){
						Image img4 = Image.getInstance(ttdReff.toString());
						img4.scaleAbsolute(30, 30);
						over.addImage(img4, img4.getScaledWidth(), 0, 0,
								img4.getScaledHeight(), 290, 280);
						over.stroke();
					}
					
					if(ttdAgen != null){
						Image img5 = Image.getInstance(ttdAgen.toString());
						img5.scaleAbsolute(30, 30);
						over.addImage(img5, img5.getScaledWidth(), 0, 0,
								img5.getScaledHeight(), 440, 280);
						over.stroke();
					}

					over.setFontAndSize(times_new_roman, 6);
					over.showTextAligned(
							PdfContentByte.ALIGN_LEFT,
							Common.isEmpty(agen.get("NM_PENUTUP")) ? "-" : 
								agen.get("NM_PENUTUP").toString().toUpperCase(),100, 260,0);
					over.showTextAligned(
							PdfContentByte.ALIGN_LEFT,
							Common.isEmpty(agen.get("KD_PENUTUP")) ? "-" : 
								agen.get("KD_PENUTUP").toString().toUpperCase(),100, 250,0);
					over.showTextAligned(
							PdfContentByte.ALIGN_LEFT,
							Common.isEmpty(agen.get("CB_PENUTUP")) ? "-" : 
								agen.get("CB_PENUTUP").toString().toUpperCase(),100, 240,0);
					over.showTextAligned(
							PdfContentByte.ALIGN_LEFT,
							Common.isEmpty(agen.get("NM_REFFERAL")) ? "-" : 
								agen.get("NM_REFFERAL").toString().toUpperCase(),270, 260,0);
					over.showTextAligned(
							PdfContentByte.ALIGN_LEFT,
							Common.isEmpty(agen.get("KD_REFFERAL")) ? "-" : 
								agen.get("KD_REFFERAL").toString().toUpperCase(),270, 250,0);
					over.showTextAligned(
							PdfContentByte.ALIGN_LEFT,
							Common.isEmpty(agen.get("CB_REFFERAL")) ? "-" : 
								agen.get("CB_REFFERAL").toString().toUpperCase(),270, 240,0);
					over.showTextAligned(
							PdfContentByte.ALIGN_LEFT,
							Common.isEmpty(agen.get("NM_AGEN")) ? "-" : 
								agen.get("NM_AGEN").toString().toUpperCase(),440, 260,0);
					over.showTextAligned(
							PdfContentByte.ALIGN_LEFT,
							Common.isEmpty(agen.get("KD_AGEN")) ? "-" : 
								agen.get("KD_AGEN").toString().toUpperCase(),440, 250,0);
					over.showTextAligned(
							PdfContentByte.ALIGN_LEFT,
							Common.isEmpty(agen.get("CB_AGEN")) ? "-" : 
								agen.get("CB_AGEN").toString().toUpperCase(),440, 240,0);
				}
				over.endText();

				// //---------- Data Halaman Keempat ----------
				over = stamp.getOverContent(4);
				over.beginText();
				over.setFontAndSize(times_new_roman, 6);

				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP
						.getMcl_first().toUpperCase(), 250, 725, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMcl_gelar()) ? "-" : dataPP
								.getMcl_gelar(), 250, 715, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getMspe_mother(), 250, 705, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getLsne_note(), 250, 695, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMcl_green_card()) ? "TIDAK"
								: dataPP.getMcl_green_card(), 250, 685, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getLside_name(), 250, 675, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getMspe_no_identity(), 250, 665, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMspe_no_identity_expired()) ? "-"
								: FormatDate.toString(dataPP
										.getMspe_no_identity_expired()), 250, 656,
						0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						dataPP.getMspe_place_birth() + ", "
								+ FormatDate.toString(dataPP.getMspe_date_birth()),
						250, 646, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getMspo_age() + " Tahun", 250, 636, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP
						.getMspe_sex2().toUpperCase(), 250, 626, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP
						.getMspe_sts_mrt().equals("1") ? "BELUM MENIKAH" : (dataPP
						.getMspe_sts_mrt().equals("2") ? "MENIKAH" : (dataPP
						.getMspe_sts_mrt().equals("3") ? "JANDA" : "DUDA")), 250,
						617, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getLsag_name(), 250, 607, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getLsed_name(), 250, 596, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMcl_company_name()) ? "-" : dataPP
								.getMcl_company_name(), 250, 587, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getMkl_kerja(), 250, 568, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP.getKerjab(),
						250, 559, 0);
				int monyong = 0;
				
				String[] uraian_tugas;
				if(dataTT.getMkl_kerja_ket() != null){
					uraian_tugas = StringUtil.pecahParagraf(dataTT
							.getMkl_kerja_ket().toUpperCase(), 70);
						for (int i = 0; i < uraian_tugas.length; i++) {
							monyong = 7 * i;
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									uraian_tugas[i], 250, 549 - monyong, 0);
						}
				}
				else{
					over.showTextAligned(PdfContentByte.ALIGN_LEFT,
							"-", 250, 549 - monyong, 0);
				}
				
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataPP.getMkl_kerja_ket())?"-":dataPP.getMkl_kerja_ket(),
				// 250, 549, 0);
				monyong = 0;
				if(!Common.isEmpty(dataTT.getAlamat_kantor())){
					String[] alamat = StringUtil.pecahParagraf(dataTT.getAlamat_kantor().toUpperCase(), 75);
		        	if(!Common.isEmpty(alamat)){
			        	for(int i=0; i<alamat.length; i++) {
			        		monyong = 7 * i;
			        		over.showTextAligned(PdfContentByte.ALIGN_LEFT, alamat[i], 250,
									529 - monyong, 0);
			        	}
		        	}
				}
				
//				monyong = 0;
//				String[] alamat = StringUtil.pecahParagraf(dataPP
//						.getAlamat_kantor().toUpperCase(), 70);
//				for (int i = 0; i < alamat.length; i++) {
//					monyong = 7 * i;
//					over.showTextAligned(PdfContentByte.ALIGN_LEFT, alamat[i], 250,
//							529 - monyong, 0);
//				}
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataPP.getAlamat_kantor())?"-":dataPP.getAlamat_kantor(),
				// 250, 529, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getKota_kantor()) ? "-" : dataPP
								.getKota_kantor(), 250, 509, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getKd_pos_kantor()) ? "-" : dataPP
								.getKd_pos_kantor(), 250, 500, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getTelpon_kantor()) ? "-" : dataPP
								.getTelpon_kantor(), 250, 490, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataPP.getTelpon_kantor2())?"-":dataPP.getTelpon_kantor2(),
				// 250, 505, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(dataPP.getNo_fax()) ? "-" : dataPP.getNo_fax(),
						250, 480, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getAlamat_rumah()) ? "-" : dataPP
								.getAlamat_rumah(), 250, 470, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getKota_rumah()) ? "-" : dataPP
								.getKota_rumah(), 250, 460, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getKd_pos_rumah()) ? "-" : dataPP
								.getKd_pos_rumah(), 250, 451, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getTelpon_rumah()) ? "-" : dataPP
								.getTelpon_rumah(), 250, 441, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataPP.getTelpon_rumah2())?"-":dataPP.getTelpon_rumah2(),
				// 250, 445, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(dataPP.getNo_fax()) ? "-" : dataPP.getNo_fax(),
						250, 432, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getAlamat_tpt_tinggal()) ? "-"
								: dataPP.getAlamat_tpt_tinggal(), 250, 422, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getKota_tpt_tinggal()) ? "-" : dataPP
								.getKota_tpt_tinggal(), 250, 412, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getKd_pos_tpt_tinggal()) ? "-"
								: dataPP.getKd_pos_tpt_tinggal(), 250, 402, 0);
//				over.showTextAligned(
//						PdfContentByte.ALIGN_LEFT,
//						Common.isEmpty(dataPP.getTelpon_rumah()) ? "-" : dataPP
//								.getTelpon_rumah(), 250, 393, 0);
				 over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				 Common.isEmpty(dataPP.getTelpon_rumah2())?"-":dataPP.getTelpon_rumah2(),
						 250, 393, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(dataPP.getNo_fax()) ? "-" : dataPP.getNo_fax(),
						250, 383, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(addrBill.getMsap_address()) ? "-" : addrBill
								.getMsap_address(), 250, 373, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(addrBill.getMsap_address()) ? "-" : addrBill
								.getKota(), 250, 353, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(addrBill.getMsap_address()) ? "-" : addrBill
								.getMsap_zip_code(), 250, 343, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(addrBill.getMsap_address()) ? "-" : addrBill
								.getMsap_phone1(), 250, 334, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(addrBill.getMsap_address()) ? "-" : addrBill
								.getMsap_fax1(), 250, 323, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getNo_hp()) ? "-" : dataPP.getNo_hp(),
						250, 313, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getEmail()) ? "-" : dataPP.getEmail(),
						250, 303, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMcl_npwp()) ? "-" : dataPP
								.getMcl_npwp(), 250, 294, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMkl_penghasilan()) ? "-" : dataPP
								.getMkl_penghasilan(), 250, 285, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMkl_pendanaan()) ? "-" : dataPP
								.getMkl_pendanaan(), 250, 273, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMkl_tujuan()) ? "-" : dataPP
								.getMkl_tujuan(), 250, 264, 0);

				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
						.getLsre_relation().toUpperCase(), 250, 234, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
						.getMcl_first().toUpperCase(), 250, 215, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMcl_gelar()) ? "-" : dataTT
								.getMcl_gelar(), 250, 206, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getMspe_mother(), 250, 196, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getLsne_note(), 250, 186, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMcl_green_card()) ? "TIDAK"
								: dataTT.getMcl_green_card(), 250, 176, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getLside_name(), 250, 166, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getMspe_no_identity(), 250, 156, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMspe_no_identity_expired()) ? "-"
								: FormatDate.toString(dataTT
										.getMspe_no_identity_expired()), 250, 146,
						0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						dataTT.getMspe_place_birth() + ", "
								+ FormatDate.toString(dataTT.getMspe_date_birth()),
						250, 137, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getMste_age() + " Tahun", 250, 127, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
						.getMspe_sex2().toUpperCase(), 250, 118, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
						.getMspe_sts_mrt().equals("1") ? "BELUM MENIKAH" : (dataTT
						.getMspe_sts_mrt().equals("2") ? "MENIKAH" : (dataTT
						.getMspe_sts_mrt().equals("3") ? "JANDA" : "DUDA")), 250,
						108, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getLsag_name(), 250, 98, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getLsed_name(), 250, 88, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMcl_company_name()) ? "-" : dataTT
								.getMcl_company_name(), 250, 78, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getMkl_kerja(), 250, 58, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT.getKerjab(),
						250, 49, 0);
				over.endText();

				//
				// //---------- Data Halaman Kelima ----------
				over = stamp.getOverContent(5);
				over.beginText();
				over.setFontAndSize(times_new_roman, 6);

				monyong = 0;
				if(dataTT.getMkl_kerja_ket() != null){
					uraian_tugas = StringUtil.pecahParagraf(dataTT.getMkl_kerja_ket()
							.toUpperCase(), 70);
					for (int i = 0; i < uraian_tugas.length; i++) {
						monyong = 7 * i;
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								uraian_tugas[i], 250, 734 - monyong, 0);
					}	
				}
				
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataTT.getMkl_kerja_ket())?"-":dataTT.getMkl_kerja_ket(),
				// 250, 734, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getAlamat_kantor()) ? "-" : dataTT
								.getAlamat_kantor(), 250, 714, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getKota_kantor()) ? "-" : dataTT
								.getKota_kantor(), 250, 695, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getKd_pos_kantor()) ? "-" : dataTT
								.getKd_pos_kantor(), 250, 685, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getTelpon_kantor()) ? "-" : dataTT
								.getTelpon_kantor(), 250, 675, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataTT.getTelpon_kantor2())?"-":dataTT.getTelpon_kantor2(),
				// 153, 105, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(dataTT.getNo_fax()) ? "-" : dataTT.getNo_fax(),
						250, 665, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getAlamat_rumah()) ? "-" : dataTT
								.getAlamat_rumah(), 250, 655, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getKota_rumah()) ? "-" : dataTT
								.getKota_rumah(), 250, 645, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getKd_pos_rumah()) ? "-" : dataTT
								.getKd_pos_rumah(), 250, 635, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getTelpon_rumah()) ? "-" : dataTT
								.getTelpon_rumah(), 250, 625, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataTT.getTelpon_rumah2())?"-":dataTT.getTelpon_rumah2(),
				// 153, 46, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(dataTT.getNo_fax()) ? "-" : dataTT.getNo_fax(),
						250, 615, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getAlamat_tpt_tinggal()) ? "-"
								: dataTT.getAlamat_tpt_tinggal(), 250, 607, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getKota_tpt_tinggal()) ? "-" : dataTT
								.getKota_tpt_tinggal(), 250, 597, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getKd_pos_tpt_tinggal()) ? "-"
								: dataTT.getKd_pos_tpt_tinggal(), 250, 587, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataTT.getTelpon_rumah())?"-":dataTT.getTelpon_rumah(),
				// 250, 597, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getTelpon_rumah2()) ? "-" : dataTT
								.getTelpon_rumah2(), 250, 578, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(dataTT.getNo_fax()) ? "-" : dataTT.getNo_fax(),
						250, 567, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(addrBill.getMsap_address())?"-":addrBill.getMsap_address(),
				// 208, 739, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getNo_hp()) ? "-" : dataTT.getNo_hp(),
						250, 557, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getEmail()) ? "-" : dataTT.getEmail(),
						250, 547, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMcl_npwp()) ? "-" : dataTT
								.getMcl_npwp(), 250, 537, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMkl_penghasilan()) ? "-" : dataTT
								.getMkl_penghasilan(), 250, 529, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMkl_pendanaan()) ? "-" : dataTT
								.getMkl_pendanaan(), 250, 519, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMkl_tujuan()) ? "-" : dataTT
								.getMkl_tujuan(), 250, 509, 0);
				//
				// //Data Pembayar Premi
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getRelation_payor()) ? "-"
								: pembPremi.getRelation_payor(), 250, 478, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(pembPremi.getNama_pihak_ketiga()) ? "-"
						: pembPremi.getNama_pihak_ketiga(), 250, 468, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getKewarganegaraan()) ? "-"
								: pembPremi.getKewarganegaraan(), 250, 458, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getAlamat_lengkap()) ? "-"
								: pembPremi.getAlamat_lengkap(), 250, 450, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getTelp_rumah()) ? "-" : pembPremi
								.getTelp_rumah(), 250, 440, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getTelp_kantor()) ? "-"
								: pembPremi.getTelp_kantor(), 250, 430, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getEmail()) ? "-" : pembPremi
								.getEmail(), 250, 420, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getTempat_lahir()) ? "-"
								: (pembPremi.getTempat_lahir() + ", " + FormatDate.toString(pembPremi
										.getMspe_date_birth_3rd_pendirian())), 250,
						410, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getMkl_kerja()) ? "-" : pembPremi
								.getMkl_kerja(), 250, 401, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 392, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 382, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 372, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getNo_npwp()) ? "-" : pembPremi
								.getNo_npwp(), 250, 362, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getSumber_dana()) ? "-"
								: pembPremi.getSumber_dana(), 250, 352, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getTujuan_dana_3rd()) ? "-"
								: pembPremi.getTujuan_dana_3rd(), 250, 342, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 332, 0);
				//
				// //Data Usulan
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						s_channel.toUpperCase(), 250, 291, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataUsulan.getLsdbs_name(), 250, 281, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataUsulan.getLsdbs_name(), 250, 271, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataUsulan.getMspr_ins_period() + " Tahun", 250, 261, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataUsulan.getMspo_pay_period() + " Tahun", 250, 251, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(dataUsulan.getMspo_installment()) ? "-"
						: dataUsulan.getMspo_installment() + "", 250, 242, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataUsulan.getLscb_pay_mode(), 250, 232, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// dataUsulan.getLku_symbol() + " " +
				// FormatNumber.convertToTwoDigit(new
				// BigDecimal(dataUsulan.getMspr_premium())), 250, 286, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						dataUsulan.getLku_symbol()
								+ " "
								+ FormatNumber.convertToTwoDigit(new BigDecimal(
										dataUsulan.getMspr_tsi())), 250, 222, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						dataUsulan.getMste_flag_cc() == 0 ? "TUNAI" : (dataUsulan
								.getMste_flag_cc() == 2 ? "TABUNGAN"
								: "KARTU KREDIT"), 250, 211, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						FormatDate.toIndonesian(dataUsulan.getMste_beg_date()),
						250, 202, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						FormatDate.toIndonesian(dataUsulan.getMste_end_date()),
						250, 193, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// FormatDate.toIndonesian(dataUsulan.getLsdbs_number()>800?dataUsulan.getLsdbs_name():"-"),
				// 250, 237, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 221,
				// 0);

				if (dataUsulan.getDaftaRider().size() > 0) {
					Integer j = 0;
					for (int i = 0; i < dataUsulan.getDaftaRider().size(); i++) {
						Datarider rider = (Datarider) dataUsulan.getDaftaRider()
								.get(i);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								rider.getLsdbs_name(), 250, 185, 0);
						over.showTextAligned(
								PdfContentByte.ALIGN_LEFT,
								dataUsulan.getLku_symbol()
										+ " "
										+ FormatNumber
												.convertToTwoDigit(new BigDecimal(
														rider.getMspr_tsi())), 250,
														175, 0);
						j += 10;
					}
				}

				// //Data Investasi
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						dataUsulan.getLku_symbol()
								+ " "
								+ FormatNumber.convertToTwoDigit(new BigDecimal(
										dataUsulan.getMspr_premium())), 250, 95, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						d_topUpBerkala.doubleValue() == new Double(0) ? "-"
								: (dataUsulan.getLku_symbol() + " " + FormatNumber
										.convertToTwoDigit(new BigDecimal(
												d_topUpBerkala))), 250, 85, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						d_topUpTunggal.doubleValue() == new Double(0) ? "-"
								: (dataUsulan.getLku_symbol() + " " + FormatNumber
										.convertToTwoDigit(new BigDecimal(
												d_topUpTunggal))), 250, 75, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						d_premiExtra.doubleValue() == new Double(0) ? "-"
								: (dataUsulan.getLku_symbol() + " " + FormatNumber
										.convertToTwoDigit(new BigDecimal(
												d_premiExtra))), 250, 65, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						dataUsulan.getLku_symbol()
								+ " "
								+ FormatNumber.convertToTwoDigit(new BigDecimal(
										d_totalPremi)), 250, 55, 0);
				Double d_jmlinves = new Double(0);
				String s_jnsinves = "";
				for (int i = 0; i < detInv.size(); i++) {
					DetilInvestasi detInves = (DetilInvestasi) detInv.get(i);
					d_jmlinves = d_jmlinves + detInves.getMdu_jumlah1();
					s_jnsinves = s_jnsinves
							+ detInves.getLji_invest1().toUpperCase() + " "
							+ detInves.getMdu_persen1() + "%";
					if (i != (detInv.size() - 1))
						s_jnsinves = s_jnsinves + ", ";
				}
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, s_jnsinves, 250,
						45, 0);
				over.endText();

				// ---------- Data Halaman Keenam ----------

				over = stamp.getOverContent(6);
				over.beginText();
				over.setFontAndSize(times_new_roman, 6);

				// //Data Rekening
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						rekClient.getLsbp_nama(), 250, 724, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						rekClient.getMrc_cabang(), 250, 714, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						rekClient.getMrc_no_ac(), 250, 704, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						rekClient.getMrc_nama(), 250, 694, 0);

				if (dataTT.getMste_flag_cc() == 1 || dataTT.getMste_flag_cc() == 2) {
					if (accRecur != null) {
						String bank_pusat = "";
						String bank_cabang = "";

						if (!namaBank.isEmpty()) {
							HashMap m = (HashMap) namaBank.get(0);
							bank_pusat = (String) m.get("LSBP_NAMA");
							bank_cabang = (String) m.get("LBN_NAMA");
						}
						over.showTextAligned(
								PdfContentByte.ALIGN_LEFT,
								dataUsulan.getMste_flag_cc() == 0 ? "TUNAI"
										: (dataUsulan.getMste_flag_cc() == 2 ? "TABUNGAN"
												: "KARTU KREDIT"), 250, 631, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								Common.isEmpty(bank_pusat) ? "-"
										: bank_pusat.toUpperCase()/*
																 * accRecur.getLbn_nama
																 * ().toUpperCase()
																 */, 250, 621, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
								.isEmpty(bank_cabang) ? "-"
								: bank_cabang.toUpperCase()/*
																	 * accRecur.
																	 * getLbn_nama
																	 * ().
																	 * toUpperCase()
																	 */, 250, 611,
								0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
								.isEmpty(accRecur.getMar_acc_no()) ? "-" : accRecur
								.getMar_acc_no().toUpperCase()/*
															 * accRecur.getLbn_nama()
															 * .toUpperCase()
															 */, 250, 601, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
								.isEmpty(accRecur.getMar_holder()) ? "-" : accRecur
								.getMar_holder().toUpperCase()/*
															 * accRecur.getLbn_nama()
															 * .toUpperCase()
															 */, 250, 592, 0);
					}
				} else {
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 631,
							0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 621,
							0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 611,
							0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 601,
							0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 592,
							0);
				}

				if (peserta.size() > 0) {
					Integer j = 0;
					for (int i = 0; i < peserta.size(); i++) {

						PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(i);
						if (pesertaPlus.getFlag_jenis_peserta() > 0){
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									pesertaPlus.getNama().toUpperCase(), 80, 517 - j,
									0);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									pesertaPlus.getLsre_relation(), 190, 517 - j, 0);
							over.showTextAligned(
									PdfContentByte.ALIGN_LEFT,
									pesertaPlus.getTinggi() + "/"
											+ pesertaPlus.getBerat(), 260, 517 - j, 0);
//							String[] pekerjaan = StringUtil.pecahParagraf(pesertaPlus.
//									getKerja().toUpperCase(), 15);
//								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
//										pekerjaan[i], 290, 519 - j, 0);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									pesertaPlus.getKerja(), 290, 517 - j, 0);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									pesertaPlus.getSex(), 375, 517 - j, 0);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									FormatDate.toString(pesertaPlus.getTgl_lahir()),
									430, 517 - j, 0);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									dataPP.getLsne_note(), 480, 517 - j, 0);
							j += 10;
						}	
					}
				}
				if (benef.size() > 0) {
					Integer j = 0;
					for (int i = 0; i < benef.size(); i++) {
						Benefeciary benefit = (Benefeciary) benef.get(i);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								benefit.getMsaw_first(), 60, 420 - j, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								benefit.getSmsaw_birth(), 200, 420 - j, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								benefit.getMsaw_sex() == 1 ? "LAKI-LAKI" : "PEREMPUAN",
								275, 420 - j, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								benefit.getMsaw_persen() + "%", 333, 420 - j, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, benefit
								.getLsre_relation().toUpperCase(), 380, 420 - j, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								dataPP.getLsne_note(), 480, 420 - j, 0);
						j += 10;
					}
				}
				over.endText();
				
				// ------------Halaman tujuh-----------
				over = stamp.getOverContent(7);
				over.beginText();
				over.setFontAndSize(times_new_roman, 6);
				String jawab;
					 if (rslt6.size() > 0) {
							// /81, 104
							Integer j = 0;
							Integer k = 0;
							Integer l = 0;
							for (int i = 0; i < rslt6.size(); i++) {
								MstQuestionAnswer ans = (MstQuestionAnswer) rslt6.get(i);
								// j += 3;
								if (ans.getQuestion_id() > 80 && ans.getQuestion_id() < 83) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 745 - j, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 745 - j, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 765 - j, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 765 - j, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 765 - j, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 765 - j, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 765 - j, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 765 - j, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 765 - j, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 765 - j, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 765 - j, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 765 - j, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 745 - j, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 745 - j, 0);
										}
									}

								}
								j += 10;
								if (ans.getQuestion_id() == 84) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 595, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 595, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 595, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 595, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 595, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 595, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 595, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 595, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 595, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 595, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 595, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 595, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 595, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 595, 0);
										}
									}
								}
								if (ans.getQuestion_id() == 85) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 585, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 585, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 585, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 585, 0);
									}

									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 585, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 585, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 585, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 585, 0);
											}
											//--tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 585, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 585, 0);
											}
											//--tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 585, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 585, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 585, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 585, 0);
										}
									}

								}
								if (ans.getQuestion_id() == 86) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 575, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 575, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 575, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 575, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 575, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 575, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 575, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 575, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 575, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 575, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 575, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 575, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if ( ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 575, 0);
										} else if ( ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 575, 0);
										}
									}
								}
								if (ans.getQuestion_id() == 87) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 565, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 565, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 565, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 565, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 565, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 565, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 565, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 565, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 565, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 565, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 565, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 565, 0);
											}
											
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 565, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 565, 0);
										}
									}

								}
								if (ans.getQuestion_id() == 88) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 555, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 555, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 555, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 555, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 555, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 555, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 555, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 555, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 555, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 555, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 555, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 555, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 555, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 555, 0);
										}
									}
								}
								if (ans.getQuestion_id() == 89) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 545, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 545, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 545, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 545, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 545, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 545, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 545, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 545, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 545, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 545, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 545, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 545, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										 if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 545, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 545, 0);
											}
									}
								
								}
								if (ans.getQuestion_id() == 90) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 535, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 535, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 535, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 535, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 535, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 535, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 535, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 535, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 535, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 535, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 535, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 535, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 535, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 535, 0);
										}
									}
								}
								if (ans.getQuestion_id() == 91) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 525, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 525, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 525, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 525, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
										
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 525, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 525, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 525, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 525, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 525, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 525, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 525, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 525, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 525, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 525, 0);
										}
									}
								}
								if (ans.getQuestion_id() == 92) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 515, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 515, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 515, 0);
										} else if (ans.getOption_group() == 2
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 515, 0);
										}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 515, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 515, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 515, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 515, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 515, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 515, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 515, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 515, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										 if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 515, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 515, 0);
											}
									}
									

								}
								if (ans.getQuestion_id() == 93) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 425, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 505, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 505, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 505, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 505, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 505, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 505, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 505, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 505, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 505, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 505, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 505, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 505, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 505, 0);
										}
									}
									
								}
								if (ans.getQuestion_id() == 94) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 495, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 495, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 495, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 495, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 495, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 495, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 495, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 495, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 495, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 495, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 495, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 495, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 495, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 495, 0);
										}
									}
								}
								if (ans.getQuestion_id() == 95) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 485, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 485, 0);
									}// --tu
									else if (ans.getOption_group() == 2
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 485, 0);
										} else if (ans.getOption_group() == 2
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 485, 0);
										}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 485, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 485, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 485, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 485, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 485, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 485, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 485, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 485, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										 if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 485, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 485, 0);
											}
									}
								}
								
								if (ans.getQuestion_id() == 96) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 475, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 475, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 475, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 475, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 475, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 475, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 475, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 475, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 475, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 475, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 475, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 475, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 475, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 475, 0);
										}
									}
								}
										
								if (ans.getQuestion_id() == 97) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 465, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 465, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 465, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 465, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 465, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 465, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 465, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 465, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 465, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 465, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 465, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 465, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if ( ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 465, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 465, 0);
										}
									}
								}
								if (ans.getQuestion_id() == 98) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 455, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 455, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 455, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 455, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 455, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 455, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 455, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 455, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 455, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 455, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 455, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 455, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 455, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 455, 0);
										}
									}
									
								}
								if (ans.getQuestion_id() == 99) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 445, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 445, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 445, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 445, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 445, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 445, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 445, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 445, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 445, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 445, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 445, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 445, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 445, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 445, 0);
										}
									}

								}
								if (ans.getQuestion_id() == 100) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 435, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 435, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 435, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 435, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 435, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 435, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 435, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 435, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 435, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 435, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 435, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 435, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 435, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 435, 0);
										}
									}
									
								}
								if (ans.getQuestion_id() == 101) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 430, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 430, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 430, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 430, 0);
									}
									else if (ans.getOption_group() == 0
											&& ans.getOption_order() == 1) {
										jawab = ans.getAnswer2();
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												Common.isEmpty(jawab) ? "-" : jawab, 110,
												400, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 430, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 430, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 430, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 430, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 430, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 430, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 430, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 430, 0);
											}
										}
									}
									 else if (dataTT.getLsre_id()==1){
										 if ( ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 430, 0);
											} else if ( ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 430, 0);
											}
									}
								}
								
								if (ans.getQuestion_id() == 102) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 390, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 390, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 390, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 390, 0);
									}
									else if (ans.getOption_group() == 0
											&& ans.getOption_order() == 1) {
										jawab = ans.getAnswer2();
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												Common.isEmpty(jawab) ? "-" : jawab, 110,
												352, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 390, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 390, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 390, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 390, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 390, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 390, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 390, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 390, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										 if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 390, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 390, 0);
											}
									}
									 
								}
								if (ans.getQuestion_id() == 103) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 342, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 342, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 342, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 342, 0);
									}
									 else if (ans.getOption_group() == 0
												&& ans.getOption_order() == 1) {
											jawab = ans.getAnswer2();
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													Common.isEmpty(jawab) ? "-" : jawab, 110,
													282, 0);
										}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 342, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 342, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 342, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 342, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 342, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 342, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 342, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 342, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 342, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 342, 0);
										}
									}
									
								}
								if (ans.getQuestion_id() == 104) {
									if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1) {
										jawab = ans.getAnswer2();
										over.setFontAndSize(italic, 6);
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												Common.isEmpty(jawab) ? "-" : jawab, 370,
												272, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2) {
										jawab = ans.getAnswer2();
										over.setFontAndSize(italic, 6);
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												Common.isEmpty(jawab) ? "-" : jawab, 370,
												262, 0);
									}
									else if (ans.getOption_group() == 2
												&& ans.getOption_order() == 1) {
											jawab = ans.getAnswer2();
											over.setFontAndSize(italic, 6);
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													Common.isEmpty(jawab) ? "-" : jawab, 400,
													272, 0);
									} else if (ans.getOption_group() == 2
												&& ans.getOption_order() == 2) {
											jawab = ans.getAnswer2();
											over.setFontAndSize(italic, 6);
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													Common.isEmpty(jawab) ? "-" : jawab, 400,
													262, 0);
									}

									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 425,
														272, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 425,
														262, 0);
											}

											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 463,
														272, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 463,
														262, 0);
											}

											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 493,
														272, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 493,
														262, 0);
											}

											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 540,
														272, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 540,
														262, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1) {
											jawab = ans.getAnswer2();
											over.setFontAndSize(italic, 6);
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													Common.isEmpty(jawab) ? "-" : jawab, 400,
													272, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2) {
											jawab = ans.getAnswer2();
											over.setFontAndSize(italic, 6);
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													Common.isEmpty(jawab) ? "-" : jawab, 400,
													262, 0);
										}
									}
								}
							}
						}	
				
				over.endText();
				stamp.close();
				
			return null;
}
	
	public ModelAndView espajonlinegadgetfullsyariah(HttpServletRequest request,
			HttpServletResponse response, ElionsManager elionsManager,
			UwManager uwManager, BacManager bacManager, Properties props,
			Products products, String spaj) throws Exception {
		String reg_spaj = spaj;
		Integer type = null;
		Integer question;
		Date sysdate = elionsManager.selectSysdate();
		List<String> pdfs = new ArrayList<String>();
		Boolean suksesMerge = false;
		HashMap<String, Object> cmd = new HashMap<String, Object>();
		ArrayList data_answer = new ArrayList();
		Integer index = null;
		Integer index2 = null;
		String spaj_gadget = "";
		String mspo_flag_spaj = bacManager.selectMspoFLagSpaj(reg_spaj);
		String cabang = elionsManager.selectCabangFromSpaj(reg_spaj);
		String exportDirectory = props.getProperty("pdf.dir.export") + "\\"
				+ cabang + "\\" + reg_spaj;
		String spajTempDirectory = props.getProperty("pdf.dir.spajtemp");
		String storagespajtemp = props.getProperty("pdf.dir.ftp");
		System.out.print(mspo_flag_spaj);
		String dir = props.getProperty("pdf.template.espajonlinegadget");
		OutputStream output;
		PdfReader reader;
		File userDir = new File(props.getProperty("pdf.dir.export") + "\\"
				+ cabang + "\\" + reg_spaj);
		if (!userDir.exists()) {
			userDir.mkdirs();
		}

		HashMap moreInfo = new HashMap();
		moreInfo.put("Author", "PT ASURANSI JIWA SINARMAS MSIG Tbk.");
		moreInfo.put("Title", "GADGET");
		moreInfo.put("Subject", "E-SPAJ ONLINE");

		PdfContentByte over;
		PdfContentByte over2;
		BaseFont times_new_roman = BaseFont.createFont(
				"C:\\WINDOWS\\FONTS\\ARIAL.TTF", BaseFont.CP1252,
				BaseFont.NOT_EMBEDDED);
		BaseFont italic = BaseFont.createFont("C:\\WINDOWS\\FONTS\\ariali.ttf",
				BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

				reader = new PdfReader(
						props.getProperty("pdf.template.espajonlinegadget")
								+ "\\spajonlinegadgetfullsyariah.pdf");
				output = new FileOutputStream(exportDirectory + "\\"
						+ "espajonlinegadget_" + reg_spaj + ".pdf");

				spaj_gadget = dir + "\\spajonlinegadgetfullsyariah.pdf";
			
			pdfs.add(spaj_gadget);
			suksesMerge = MergePDF.concatPDFs(pdfs, output, false);
			String outputName = props.getProperty("pdf.dir.export") + "\\"
					+ cabang + "\\" + reg_spaj + "\\" + "espajonlinegadget_"
					+ reg_spaj + ".pdf";
			PdfStamper stamp = new PdfStamper(reader, new FileOutputStream(
					outputName));

			Pemegang dataPP = elionsManager.selectpp(reg_spaj);
			Tertanggung dataTT = elionsManager.selectttg(reg_spaj);
			PembayarPremi pembPremi = bacManager.selectP_premi(reg_spaj);
			if (pembPremi == null)
				pembPremi = new PembayarPremi();
			AddressBilling addrBill = elionsManager
					.selectAddressBilling(reg_spaj);
			Datausulan dataUsulan = elionsManager
					.selectDataUsulanutama(reg_spaj);
			dataUsulan.setDaftaRider(elionsManager
					.selectDataUsulan_rider(reg_spaj));
			InvestasiUtama inv = elionsManager
					.selectinvestasiutama(reg_spaj);
			Rekening_client rekClient = elionsManager
					.select_rek_client(reg_spaj);
			Account_recur accRecur = elionsManager
					.select_account_recur(reg_spaj);
			List detInv = bacManager.selectdetilinvestasi2(reg_spaj);
			List benef = elionsManager.select_benef(reg_spaj);
			List peserta = uwManager.select_all_mst_peserta(reg_spaj);
			List dist = elionsManager.select_tipeproduk();
			List listSpajTemp = bacManager.selectReferensiTempSpaj(reg_spaj);
			HashMap spajTemp = (HashMap) listSpajTemp.get(0);
			String idgadget = (String) spajTemp.get("NO_TEMP");
			Map agen = bacManager.selectAgenESPAJSimasPrima(reg_spaj);
			List namaBank = uwManager.namaBank(accRecur.getLbn_id());

			// --Question Full Konven/Syariah
			List rslt = bacManager.selectQuestionareGadget(reg_spaj, 2, 1, 15);	
			List rslt2 = bacManager.selectQuestionareGadget(reg_spaj, 1, 16, 18); 
			List rslt3 = bacManager.selectQuestionareGadget(reg_spaj, 3, 106, 136); 
			List rslt4 = bacManager.selectQuestionareGadget(reg_spaj, 3, 137, 145);
			List rslt5 = bacManager.selectQuestionareGadget(reg_spaj, 3, 146, 155);
			
			//Sio
			List rslt6 = bacManager.selectQuestionareGadget(reg_spaj, 12, 81, 104);
			
			String s_channel = "";
			for (int i = 0; i < dist.size(); i++) {
				HashMap dist2 = (HashMap) dist.get(i);
				Integer i_lstp_id = (Integer) dist2.get("lstp_id");
				if (i_lstp_id.intValue() == dataUsulan.getTipeproduk()
						.intValue()) {
					s_channel = (String) dist2.get("lstp_produk");
				}
			}

			Double d_premiRider = 0.;
			if (dataUsulan.getDaftaRider().size() > 0) {
				for (int i = 0; i < dataUsulan.getDaftaRider().size(); i++) {
					Datarider rider = (Datarider) dataUsulan.getDaftaRider()
							.get(i);
					d_premiRider = rider.getMspr_premium();
				}
			}
			Double d_topUpBerkala = new Double(0);
			Double d_topUpTunggal = new Double(0);
			Double d_totalTopup = new Double(0);
			if (inv != null) {
				DetilTopUp daftarTopup = inv.getDaftartopup();
				d_topUpBerkala = Common.isEmpty(daftarTopup.getPremi_berkala()) ? new Double(
						0) : daftarTopup.getPremi_berkala();
				d_topUpTunggal = Common.isEmpty(daftarTopup.getPremi_tunggal()) ? new Double(
						0) : daftarTopup.getPremi_tunggal();
				d_totalTopup = d_topUpBerkala + d_topUpTunggal;
			}
			Double d_premiExtra = (Common.isEmpty(uwManager
					.selectSumPremiExtra(reg_spaj)) ? 0. : uwManager
					.selectSumPremiExtra(reg_spaj));
			Double d_totalPremi = dataUsulan.getMspr_premium() + d_totalTopup
					+ d_premiRider + d_premiExtra;

			stamp.setMoreInfo(moreInfo);

			// ---------- Data Halaman Pertama ----------

			over = stamp.getOverContent(1);
			over.beginText();
			over.setFontAndSize(times_new_roman, 8);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					FormatString.nomorSPAJ(reg_spaj), 380, 627, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					FormatDate.toString(sysdate), 85, 617, 0);

			over.setFontAndSize(times_new_roman, 6);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP
					.getMcl_first().toUpperCase(), 160, 516, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
					.getMcl_first().toUpperCase(), 160, 506, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataUsulan.getLsdbs_name(), 160, 496, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					dataUsulan.getLku_symbol()
							+ " "
							+ FormatNumber.convertToTwoDigit(new BigDecimal(
									dataUsulan.getMspr_tsi())), 160, 486, 0);

			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					dataUsulan.getLku_symbol()
							+ " "
							+ FormatNumber.convertToTwoDigit(new BigDecimal(
									dataUsulan.getMspr_premium())), 290, 476, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					d_topUpBerkala.doubleValue() == new Double(0) ? "-"
							: (dataUsulan.getLku_symbol() + " " + FormatNumber
									.convertToTwoDigit(new BigDecimal(
											d_topUpBerkala))), 290, 467, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					d_topUpTunggal.doubleValue() == new Double(0) ? "-"
							: (dataUsulan.getLku_symbol() + " " + FormatNumber
									.convertToTwoDigit(new BigDecimal(
											d_topUpTunggal))), 290, 457, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					d_premiRider.doubleValue() == new Double(0) ? "-"
							: (dataUsulan.getLku_symbol() + " " + FormatNumber
									.convertToTwoDigit(new BigDecimal(
											d_premiRider))), 290, 447, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					dataUsulan.getLku_symbol()
							+ " "
							+ FormatNumber.convertToTwoDigit(new BigDecimal(
									d_totalPremi)), 290, 437, 0);

			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(agen.get("NM_AGEN").toString().toUpperCase()) ? "-" : 
						agen.get("NM_AGEN").toString().toUpperCase(),160, 409,0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(agen.get("KD_AGEN").toString().toUpperCase()) ? "-" : 
						agen.get("KD_AGEN").toString().toUpperCase(),160, 399,0);
			over.endText();

				// ---------- Data Halaman keempat ----------
				over = stamp.getOverContent(4);
				over.beginText();
				
				//--pencarian file ttd
				File ttdPp = null;
			    File ttdTu = null;
			    File ttdAgen = null;
			    File ttdPenutup = null;
			    File ttdReff = null;
				
			    String pathTTD = exportDirectory;
			    File fileDirTemp = new File(pathTTD);
				File[] files = fileDirTemp.listFiles();
				if (files != null){
					for (int z = 0; z < files.length; z++) {
					       if ((files[z].toString().toUpperCase().contains("_TTD_PP_")) && (files[z].toString().toUpperCase().contains(".JPG"))) {
					    	   ttdPp = files[z];
					       }else if ((files[z].toString().toUpperCase().contains("_TTD_TU_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdTu = files[z];
					       }else if ((files[z].toString().toUpperCase().contains("_TTD_AGEN_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdAgen = files[z];
					       }else if ((files[z].toString().toUpperCase().contains("_TTD_PENUTUP_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdPenutup = files[z];
					       }else if ((files[z].toString().toUpperCase().contains("_TTD_REF_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdReff = files[z];
					       }
					    }
				}
				
			    if (ttdPp==null || ttdTu == null || ttdAgen == null || ttdPenutup == null || ttdReff == null) {
					pathTTD = spajTempDirectory+"\\"+idgadget+"\\Spaj\\";
					fileDirTemp = new File(pathTTD);
					files = fileDirTemp.listFiles();
					if (files != null){
					    for (int z = 0; z < files.length; z++) {
				    	 	if ((ttdPp==null && files[z].toString().toUpperCase().contains("_TTD_PP_")) && (files[z].toString().toUpperCase().contains(".JPG"))) {
					    	   ttdPp = files[z];
					       }else if ((ttdTu==null && files[z].toString().toUpperCase().contains("_TTD_TU_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdTu = files[z];
					       }else if ((ttdAgen==null && files[z].toString().toUpperCase().contains("_TTD_AGEN_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdAgen = files[z];
					       }else if ((ttdPenutup==null && files[z].toString().toUpperCase().contains("_TTD_PENUTUP_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdPenutup = files[z];
					       }else if ((ttdReff==null && files[z].toString().toUpperCase().contains("_TTD_REF_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdReff = files[z];
					       }
					    }	
					}

				    if (ttdPp==null || ttdTu == null || ttdAgen == null || ttdPenutup == null || ttdReff == null) {
				    	pathTTD = props.getProperty("pdf.dir.ftp") + "\\"
								+ cabang + "\\" + reg_spaj;
						fileDirTemp = new File(pathTTD);
						files = fileDirTemp.listFiles();
						if (files != null){
							for (int z = 0; z < files.length; z++) {
							       if ((ttdPp==null && files[z].toString().toUpperCase().contains("_TTD_PP_")) && (files[z].toString().toUpperCase().contains(".JPG"))) {
							    	   ttdPp = files[z];
							       }else if ((ttdTu==null && files[z].toString().toUpperCase().contains("_TTD_TU_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
							    	   ttdTu = files[z];
							       }else if ((ttdAgen==null && files[z].toString().toUpperCase().contains("_TTD_AGEN_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
							    	   ttdAgen = files[z];
							       }else if ((ttdPenutup==null && files[z].toString().toUpperCase().contains("_TTD_PENUTUP_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
							    	   ttdPenutup = files[z];
							       }else if ((ttdReff==null && files[z].toString().toUpperCase().contains("_TTD_REF_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
							    	   ttdReff = files[z];
							       }
							    }	
						}
				    }
				}
			    
				try {
					Image img = Image.getInstance(ttdPp.toString());
					img.scaleAbsolute(30, 30);
					over.addImage(img, img.getScaledWidth(), 0, 0,
							img.getScaledHeight(), 438, 643);
					over.stroke();

					if (dataTT.getMste_age() < 17 || dataPP.getLsre_id() == 1)
						ttdTu = ttdPp;
					Image img2 = Image.getInstance(ttdTu.toString());
					img2.scaleAbsolute(30, 30);
					over.addImage(img2, img2.getScaledWidth(), 0, 0,
							img2.getScaledHeight(), 438, 593);
					over.stroke();
				} catch (FileNotFoundException e) {
					logger.error("ERROR :", e);
					ServletOutputStream sos = response.getOutputStream();
					sos.println("<script>alert('TTD Pemegang Polis / Tertanggung Utama Tidak Ditemukan');window.close();</script>");
					sos.close();
				}

				over.setFontAndSize(times_new_roman, 6);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						FormatDate.toString(dataPP.getMspo_spaj_date()), 370, 715, 0);
				over.showTextAligned(PdfContentByte.ALIGN_CENTER, dataPP
						.getMcl_first().toUpperCase(), 295, 655, 0);
				over.showTextAligned(PdfContentByte.ALIGN_CENTER, dataTT
						.getMcl_first().toUpperCase(), 295, 605, 0);
				if (peserta.size() > 0 ) {
					Integer vertikal = 605;
					for (int i = 0; i < peserta.size(); i++) {
						vertikal = vertikal - 50;
						PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(i);
						if (pesertaPlus.getFlag_jenis_peserta() > 0){
							over.showTextAligned(PdfContentByte.ALIGN_CENTER,
									pesertaPlus.getNama().toUpperCase(), 290, vertikal,
									0);
							vertikal = vertikal + 2;
						}
						
					}
				}
				if(ttdPenutup != null){
					Image img3 = Image.getInstance(ttdPenutup.toString());
					img3.scaleAbsolute(30, 30);
					over.addImage(img3, img3.getScaledWidth(), 0, 0,
							img3.getScaledHeight(), 120, 280);
					over.stroke();
				}
					
				if(ttdReff != null){
					Image img4 = Image.getInstance(ttdReff.toString());
					img4.scaleAbsolute(30, 30);
					over.addImage(img4, img4.getScaledWidth(), 0, 0,
							img4.getScaledHeight(), 290, 280);
					over.stroke();
				}
					
				if(ttdAgen != null){
					Image img5 = Image.getInstance(ttdAgen.toString());
					img5.scaleAbsolute(30, 30);
					over.addImage(img5, img5.getScaledWidth(), 0, 0,
							img5.getScaledHeight(), 440, 280);
					over.stroke();
				}
					
				over.setFontAndSize(times_new_roman, 6);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("NM_PENUTUP")) ? "-" : 
							agen.get("NM_PENUTUP").toString().toUpperCase(),100, 260,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("KD_PENUTUP")) ? "-" : 
							agen.get("KD_PENUTUP").toString().toUpperCase(),100, 250,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("CB_PENUTUP")) ? "-" : 
							agen.get("CB_PENUTUP").toString().toUpperCase(),100, 240,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("NM_REFFERAL")) ? "-" : 
							agen.get("NM_REFFERAL").toString().toUpperCase(),270, 260,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("KD_REFFERAL")) ? "-" : 
							agen.get("KD_REFFERAL").toString().toUpperCase(),270, 250,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("CB_REFFERAL")) ? "-" : 
							agen.get("CB_REFFERAL").toString().toUpperCase(),270, 240,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("NM_AGEN")) ? "-" : 
							agen.get("NM_AGEN").toString().toUpperCase(),440, 260,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("KD_AGEN")) ? "-" : 
							agen.get("KD_AGEN").toString().toUpperCase(),440, 250,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("CB_AGEN")) ? "-" : 
							agen.get("CB_AGEN").toString().toUpperCase(),440, 240,0);

				over.endText();

				// //---------- Data Halaman kelima ----------
				over = stamp.getOverContent(5);
				over.beginText();
				over.setFontAndSize(times_new_roman, 6);

				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP
						.getMcl_first().toUpperCase(), 250, 725, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMcl_gelar()) ? "-" : dataPP
								.getMcl_gelar(), 250, 715, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getMspe_mother(), 250, 705, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getLsne_note(), 250, 695, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMcl_green_card()) ? "TIDAK"
								: dataPP.getMcl_green_card(), 250, 685, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getLside_name(), 250, 675, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getMspe_no_identity(), 250, 665, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMspe_no_identity_expired()) ? "-"
								: FormatDate.toString(dataPP
										.getMspe_no_identity_expired()), 250, 656,
						0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						dataPP.getMspe_place_birth() + ", "
								+ FormatDate.toString(dataPP.getMspe_date_birth()),
						250, 646, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getMspo_age() + " Tahun", 250, 636, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP
						.getMspe_sex2().toUpperCase(), 250, 626, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP
						.getMspe_sts_mrt().equals("1") ? "BELUM MENIKAH" : (dataPP
						.getMspe_sts_mrt().equals("2") ? "MENIKAH" : (dataPP
						.getMspe_sts_mrt().equals("3") ? "JANDA" : "DUDA")), 250,
						617, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getLsag_name(), 250, 607, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getLsed_name(), 250, 596, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMcl_company_name()) ? "-" : dataPP
								.getMcl_company_name(), 250, 587, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getMkl_kerja(), 250, 568, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP.getKerjab(),
						250, 559, 0);
				int monyong = 0;
				
				String[] uraian_tugas;
				if(dataTT.getMkl_kerja_ket() != null){
					uraian_tugas = StringUtil.pecahParagraf(dataTT
							.getMkl_kerja_ket().toUpperCase(), 70);
					
						for (int i = 0; i < uraian_tugas.length; i++) {
							monyong = 7 * i;
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									uraian_tugas[i], 250, 549 - monyong, 0);
						}
				}
				else{
					over.showTextAligned(PdfContentByte.ALIGN_LEFT,
							"-", 250, 549 - monyong, 0);
				}
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataPP.getMkl_kerja_ket())?"-":dataPP.getMkl_kerja_ket(),
				// 250, 549, 0);
				monyong = 0;
				if(!Common.isEmpty(dataTT.getAlamat_kantor())){
					String[] alamat = StringUtil.pecahParagraf(dataTT.getAlamat_kantor().toUpperCase(), 75);
		        	if(!Common.isEmpty(alamat)){
			        	for(int i=0; i<alamat.length; i++) {
			        		monyong = 7 * i;
			        		over.showTextAligned(PdfContentByte.ALIGN_LEFT, alamat[i], 250,
									529 - monyong, 0);
			        	}
		        	}
				}

				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataPP.getAlamat_kantor())?"-":dataPP.getAlamat_kantor(),
				// 250, 529, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getKota_kantor()) ? "-" : dataPP
								.getKota_kantor(), 250, 509, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getKd_pos_kantor()) ? "-" : dataPP
								.getKd_pos_kantor(), 250, 500, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getTelpon_kantor()) ? "-" : dataPP
								.getTelpon_kantor(), 250, 490, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataPP.getTelpon_kantor2())?"-":dataPP.getTelpon_kantor2(),
				// 250, 505, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(dataPP.getNo_fax()) ? "-" : dataPP.getNo_fax(),
						250, 480, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getAlamat_rumah()) ? "-" : dataPP
								.getAlamat_rumah(), 250, 470, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getKota_rumah()) ? "-" : dataPP
								.getKota_rumah(), 250, 460, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getKd_pos_rumah()) ? "-" : dataPP
								.getKd_pos_rumah(), 250, 451, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getTelpon_rumah()) ? "-" : dataPP
								.getTelpon_rumah(), 250, 441, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataPP.getTelpon_rumah2())?"-":dataPP.getTelpon_rumah2(),
				// 250, 445, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(dataPP.getNo_fax()) ? "-" : dataPP.getNo_fax(),
						250, 432, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getAlamat_tpt_tinggal()) ? "-"
								: dataPP.getAlamat_tpt_tinggal(), 250, 422, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getKota_tpt_tinggal()) ? "-" : dataPP
								.getKota_tpt_tinggal(), 250, 412, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getKd_pos_tpt_tinggal()) ? "-"
								: dataPP.getKd_pos_tpt_tinggal(), 250, 402, 0);
//				over.showTextAligned(
//						PdfContentByte.ALIGN_LEFT,
//						Common.isEmpty(dataPP.getTelpon_rumah()) ? "-" : dataPP
//								.getTelpon_rumah(), 250, 393, 0);
				 over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				 Common.isEmpty(dataPP.getTelpon_rumah2())?"-":dataPP.getTelpon_rumah2(),
						 250, 393, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(dataPP.getNo_fax()) ? "-" : dataPP.getNo_fax(),
						250, 383, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(addrBill.getMsap_address()) ? "-" : addrBill
								.getMsap_address(), 250, 373, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(addrBill.getMsap_address()) ? "-" : addrBill
								.getKota(), 250, 353, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(addrBill.getMsap_address()) ? "-" : addrBill
								.getMsap_zip_code(), 250, 343, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(addrBill.getMsap_address()) ? "-" : addrBill
								.getMsap_phone1(), 250, 334, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(addrBill.getMsap_address()) ? "-" : addrBill
								.getMsap_fax1(), 250, 323, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getNo_hp()) ? "-" : dataPP.getNo_hp(),
						250, 313, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getEmail()) ? "-" : dataPP.getEmail(),
						250, 303, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMcl_npwp()) ? "-" : dataPP
								.getMcl_npwp(), 250, 294, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMkl_penghasilan()) ? "-" : dataPP
								.getMkl_penghasilan(), 250, 285, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMkl_pendanaan()) ? "-" : dataPP
								.getMkl_pendanaan(), 250, 273, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMkl_tujuan()) ? "-" : dataPP
								.getMkl_tujuan(), 250, 264, 0);

				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
						.getLsre_relation().toUpperCase(), 250, 234, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
						.getMcl_first().toUpperCase(), 250, 215, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMcl_gelar()) ? "-" : dataTT
								.getMcl_gelar(), 250, 206, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getMspe_mother(), 250, 196, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getLsne_note(), 250, 186, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMcl_green_card()) ? "TIDAK"
								: dataTT.getMcl_green_card(), 250, 176, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getLside_name(), 250, 166, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getMspe_no_identity(), 250, 156, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMspe_no_identity_expired()) ? "-"
								: FormatDate.toString(dataTT
										.getMspe_no_identity_expired()), 250, 146,
						0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						dataTT.getMspe_place_birth() + ", "
								+ FormatDate.toString(dataTT.getMspe_date_birth()),
						250, 137, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getMste_age() + " Tahun", 250, 127, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
						.getMspe_sex2().toUpperCase(), 250, 118, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
						.getMspe_sts_mrt().equals("1") ? "BELUM MENIKAH" : (dataTT
						.getMspe_sts_mrt().equals("2") ? "MENIKAH" : (dataTT
						.getMspe_sts_mrt().equals("3") ? "JANDA" : "DUDA")), 250,
						108, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getLsag_name(), 250, 98, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getLsed_name(), 250, 88, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMcl_company_name()) ? "-" : dataTT
								.getMcl_company_name(), 250, 78, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getMkl_kerja(), 250, 58, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT.getKerjab(),
						250, 49, 0);
				over.endText();

				//
				// //---------- Data Halaman keenam ----------
				over = stamp.getOverContent(6);
				over.beginText();
				over.setFontAndSize(times_new_roman, 6);

				monyong = 0;
				if(dataTT.getMkl_kerja_ket() != null){
					uraian_tugas = StringUtil.pecahParagraf(dataTT.getMkl_kerja_ket()
							.toUpperCase(), 70);
					for (int i = 0; i < uraian_tugas.length; i++) {
						monyong = 7 * i;
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								uraian_tugas[i], 250, 734 - monyong, 0);
					}
				}
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataTT.getMkl_kerja_ket())?"-":dataTT.getMkl_kerja_ket(),
				// 250, 734, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getAlamat_kantor()) ? "-" : dataTT
								.getAlamat_kantor(), 250, 714, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getKota_kantor()) ? "-" : dataTT
								.getKota_kantor(), 250, 695, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getKd_pos_kantor()) ? "-" : dataTT
								.getKd_pos_kantor(), 250, 685, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getTelpon_kantor()) ? "-" : dataTT
								.getTelpon_kantor(), 250, 675, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataTT.getTelpon_kantor2())?"-":dataTT.getTelpon_kantor2(),
				// 153, 105, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(dataTT.getNo_fax()) ? "-" : dataTT.getNo_fax(),
						250, 665, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getAlamat_rumah()) ? "-" : dataTT
								.getAlamat_rumah(), 250, 655, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getKota_rumah()) ? "-" : dataTT
								.getKota_rumah(), 250, 645, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getKd_pos_rumah()) ? "-" : dataTT
								.getKd_pos_rumah(), 250, 635, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getTelpon_rumah()) ? "-" : dataTT
								.getTelpon_rumah(), 250, 625, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataTT.getTelpon_rumah2())?"-":dataTT.getTelpon_rumah2(),
				// 153, 46, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(dataTT.getNo_fax()) ? "-" : dataTT.getNo_fax(),
						250, 615, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getAlamat_tpt_tinggal()) ? "-"
								: dataTT.getAlamat_tpt_tinggal(), 250, 607, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getKota_tpt_tinggal()) ? "-" : dataTT
								.getKota_tpt_tinggal(), 250, 597, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getKd_pos_tpt_tinggal()) ? "-"
								: dataTT.getKd_pos_tpt_tinggal(), 250, 587, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataTT.getTelpon_rumah())?"-":dataTT.getTelpon_rumah(),
				// 250, 597, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getTelpon_rumah2()) ? "-" : dataTT
								.getTelpon_rumah2(), 250, 578, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(dataTT.getNo_fax()) ? "-" : dataTT.getNo_fax(),
						250, 567, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(addrBill.getMsap_address())?"-":addrBill.getMsap_address(),
				// 208, 739, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getNo_hp()) ? "-" : dataTT.getNo_hp(),
						250, 557, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getEmail()) ? "-" : dataTT.getEmail(),
						250, 547, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMcl_npwp()) ? "-" : dataTT
								.getMcl_npwp(), 250, 537, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMkl_penghasilan()) ? "-" : dataTT
								.getMkl_penghasilan(), 250, 529, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMkl_pendanaan()) ? "-" : dataTT
								.getMkl_pendanaan(), 250, 519, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMkl_tujuan()) ? "-" : dataTT
								.getMkl_tujuan(), 250, 509, 0);
				//
				// //Data Pembayar Premi
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getRelation_payor()) ? "-"
								: pembPremi.getRelation_payor(), 250, 478, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(pembPremi.getNama_pihak_ketiga()) ? "-"
						: pembPremi.getNama_pihak_ketiga(), 250, 468, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getKewarganegaraan()) ? "-"
								: pembPremi.getKewarganegaraan(), 250, 458, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getAlamat_lengkap()) ? "-"
								: pembPremi.getAlamat_lengkap(), 250, 450, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getTelp_rumah()) ? "-" : pembPremi
								.getTelp_rumah(), 250, 440, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getTelp_kantor()) ? "-"
								: pembPremi.getTelp_kantor(), 250, 430, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getEmail()) ? "-" : pembPremi
								.getEmail(), 250, 420, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getTempat_lahir()) ? "-"
								: (pembPremi.getTempat_lahir() + ", " + FormatDate.toString(pembPremi
										.getMspe_date_birth_3rd_pendirian())), 250,
						410, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getMkl_kerja()) ? "-" : pembPremi
								.getMkl_kerja(), 250, 401, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 392, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 382, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 372, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getNo_npwp()) ? "-" : pembPremi
								.getNo_npwp(), 250, 362, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getSumber_dana()) ? "-"
								: pembPremi.getSumber_dana(), 250, 352, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getTujuan_dana_3rd()) ? "-"
								: pembPremi.getTujuan_dana_3rd(), 250, 342, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 332, 0);
				//
				// //Data Usulan
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						s_channel.toUpperCase(), 250, 291, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataUsulan.getLsdbs_name(), 250, 281, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataUsulan.getLsdbs_name(), 250, 271, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataUsulan.getMspr_ins_period() + " Tahun", 250, 261, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataUsulan.getMspo_pay_period() + " Tahun", 250, 251, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(dataUsulan.getMspo_installment()) ? "-"
						: dataUsulan.getMspo_installment() + "", 250, 242, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataUsulan.getLscb_pay_mode(), 250, 232, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// dataUsulan.getLku_symbol() + " " +
				// FormatNumber.convertToTwoDigit(new
				// BigDecimal(dataUsulan.getMspr_premium())), 250, 286, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						dataUsulan.getLku_symbol()
								+ " "
								+ FormatNumber.convertToTwoDigit(new BigDecimal(
										dataUsulan.getMspr_tsi())), 250, 222, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						dataUsulan.getMste_flag_cc() == 0 ? "TUNAI" : (dataUsulan
								.getMste_flag_cc() == 2 ? "TABUNGAN"
								: "KARTU KREDIT"), 250, 211, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						FormatDate.toIndonesian(dataUsulan.getMste_beg_date()),
						250, 202, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						FormatDate.toIndonesian(dataUsulan.getMste_end_date()),
						250, 193, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// FormatDate.toIndonesian(dataUsulan.getLsdbs_number()>800?dataUsulan.getLsdbs_name():"-"),
				// 250, 237, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 221,
				// 0);

				if (dataUsulan.getDaftaRider().size() > 0) {
					Integer j = 0;
					for (int i = 0; i < dataUsulan.getDaftaRider().size(); i++) {
						Datarider rider = (Datarider) dataUsulan.getDaftaRider()
								.get(i);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								rider.getLsdbs_name(), 270, 163 - j, 0);
						over.showTextAligned(
								PdfContentByte.ALIGN_LEFT,
								dataUsulan.getLku_symbol()
										+ " "
										+ FormatNumber
												.convertToTwoDigit(new BigDecimal(
														rider.getMspr_tsi())), 440,
														163 - j, 0);
						j += 10;
					}
				}

				// //Data Investasi
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						dataUsulan.getLku_symbol()
								+ " "
								+ FormatNumber.convertToTwoDigit(new BigDecimal(
										dataUsulan.getMspr_premium())), 250, 97, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						d_topUpBerkala.doubleValue() == new Double(0) ? "-"
								: (dataUsulan.getLku_symbol() + " " + FormatNumber
										.convertToTwoDigit(new BigDecimal(
												d_topUpBerkala))), 250, 87, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						d_topUpTunggal.doubleValue() == new Double(0) ? "-"
								: (dataUsulan.getLku_symbol() + " " + FormatNumber
										.convertToTwoDigit(new BigDecimal(
												d_topUpTunggal))), 250, 77, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						d_premiExtra.doubleValue() == new Double(0) ? "-"
								: (dataUsulan.getLku_symbol() + " " + FormatNumber
										.convertToTwoDigit(new BigDecimal(
												d_premiExtra))), 250, 68, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						dataUsulan.getLku_symbol()
								+ " "
								+ FormatNumber.convertToTwoDigit(new BigDecimal(
										d_totalPremi)), 250, 58, 0);
				Double d_jmlinves = new Double(0);
				String s_jnsinves = "";
				for (int i = 0; i < detInv.size(); i++) {
					DetilInvestasi detInves = (DetilInvestasi) detInv.get(i);
					d_jmlinves = d_jmlinves + detInves.getMdu_jumlah1();
					s_jnsinves = s_jnsinves
							+ detInves.getLji_invest1().toUpperCase() + " "
							+ detInves.getMdu_persen1() + "%";
					if (i != (detInv.size() - 1))
						s_jnsinves = s_jnsinves + ", ";
				}
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// dataUsulan.getLku_symbol() + " " +
				// FormatNumber.convertToTwoDigit(new BigDecimal(d_jmlinves)), 208,
				// 183, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, s_jnsinves, 250,
						47, 0);
				over.endText();

				// ---------- Data Halaman ketujuh----------

				over = stamp.getOverContent(7);
				over.beginText();
				over.setFontAndSize(times_new_roman, 6);

				// //Data Rekening
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						rekClient.getLsbp_nama(), 250, 724, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						rekClient.getMrc_cabang(), 250, 714, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						rekClient.getMrc_no_ac(), 250, 704, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						rekClient.getMrc_nama(), 250, 694, 0);

				if (dataTT.getMste_flag_cc() == 1 || dataTT.getMste_flag_cc() == 2) {
					if (accRecur != null) {
						String bank_pusat = "";
						String bank_cabang = "";

						if (!namaBank.isEmpty()) {
							HashMap m = (HashMap) namaBank.get(0);
							bank_pusat = (String) m.get("LSBP_NAMA");
							bank_cabang = (String) m.get("LBN_NAMA");
						}
						over.showTextAligned(
								PdfContentByte.ALIGN_LEFT,
								dataUsulan.getMste_flag_cc() == 0 ? "TUNAI"
										: (dataUsulan.getMste_flag_cc() == 2 ? "TABUNGAN"
												: "KARTU KREDIT"), 250, 631, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								Common.isEmpty(bank_pusat) ? "-"
										: bank_pusat.toUpperCase()/*
																 * accRecur.getLbn_nama
																 * ().toUpperCase()
																 */, 250, 621, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
								.isEmpty(bank_cabang) ? "-"
								: bank_cabang.toUpperCase()/*
																	 * accRecur.
																	 * getLbn_nama
																	 * ().
																	 * toUpperCase()
																	 */, 250, 611,
								0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
								.isEmpty(accRecur.getMar_acc_no()) ? "-" : accRecur
								.getMar_acc_no().toUpperCase()/*
															 * accRecur.getLbn_nama()
															 * .toUpperCase()
															 */, 250, 601, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
								.isEmpty(accRecur.getMar_holder()) ? "-" : accRecur
								.getMar_holder().toUpperCase()/*
															 * accRecur.getLbn_nama()
															 * .toUpperCase()
															 */, 250, 592, 0);
					}
				} else {
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 631,
							0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 621,
							0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 611,
							0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 601,
							0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 592,
							0);
				}

				if (peserta.size() > 0) {
					Integer j = 0;
					for (int i = 0; i < peserta.size(); i++) {

						PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(i);
						if (pesertaPlus.getFlag_jenis_peserta() > 0){
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									pesertaPlus.getNama().toUpperCase(), 80, 517 - j,
									0);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									pesertaPlus.getLsre_relation(), 190, 517 - j, 0);
							over.showTextAligned(
									PdfContentByte.ALIGN_LEFT,
									pesertaPlus.getTinggi() + "/"
											+ pesertaPlus.getBerat(), 260, 517 - j, 0);
//							String[] pekerjaan = StringUtil.pecahParagraf(pesertaPlus.
//									getKerja().toUpperCase(), 15);
//								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
//										pekerjaan[i], 290, 519 - j, 0);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									pesertaPlus.getKerja(), 290, 517 - j, 0);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									pesertaPlus.getSex(), 375, 517 - j, 0);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									FormatDate.toString(pesertaPlus.getTgl_lahir()),
									430, 517 - j, 0);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									dataPP.getLsne_note(), 480, 517 - j, 0);
							j += 10;
						}
						
					}
				}
				if (benef.size() > 0) {
					Integer j = 0;
					for (int i = 0; i < benef.size(); i++) {
						Benefeciary benefit = (Benefeciary) benef.get(i);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								benefit.getMsaw_first(), 60, 420 - j, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								benefit.getSmsaw_birth(), 200, 420 - j, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								benefit.getMsaw_sex() == 1 ? "LAKI-LAKI" : "PEREMPUAN",
								275, 420 - j, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								benefit.getMsaw_persen() + "%", 333, 420 - j, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, benefit
								.getLsre_relation().toUpperCase(), 380, 420 - j, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								dataPP.getLsne_note(), 480, 420 - j, 0);
						j += 10;
					}
				}
				// -----------data tertanggung-----------
				String jawab = "";
				
					if (rslt.size() > 0) {
						Integer j = 0;
						for (int i = 0; i < rslt.size(); i++) {
							MstQuestionAnswer ans = (MstQuestionAnswer) rslt.get(i);
							if (ans.getQuestion_id() == 1) {
								if (ans.getOption_type() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "v";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 510, 348, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("0")) {
									jawab = "-";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 510, 348, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "v";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 530, 348, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("0")) {
									jawab = "-";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 530, 348, 0);
								} else {
									jawab = ans.getAnswer2();
									over.setFontAndSize(italic, 6);
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											Common.isEmpty(jawab) ? "-" : jawab, 100,
											330, 0);
								}
							}
							if (ans.getQuestion_id() == 2) {
								if (ans.getOption_type() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "v";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 510, 319, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("0")) {
									jawab = "-";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 510, 319, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "v";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 530, 319, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("0")) {
									jawab = "-";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 530, 319, 0);
								} else {
									jawab = ans.getAnswer2();
									over.setFontAndSize(italic, 6);
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											Common.isEmpty(jawab) ? "-" : jawab, 100,
											310, 0);
								}
							}
							if (ans.getQuestion_id() == 3) {
								if (ans.getOption_type() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "v";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 510, 300, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("0")) {
									jawab = "-";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 510, 300, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "v";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 530, 300, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("0")) {
									jawab = "-";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 530, 300, 0);
								} else {
									jawab = ans.getAnswer2();
									over.setFontAndSize(italic, 6);
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											Common.isEmpty(jawab) ? "-" : jawab, 100,
											270, 0);
								}
							}
							if (ans.getQuestion_id() == 4) {
								if (ans.getOption_type() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "v";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 510, 260, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("0")) {
									jawab = "-";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 510, 260, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "v";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 530, 260, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("0")) {
									jawab = "-";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 530, 260, 0);
								} else {
									jawab = ans.getAnswer2();
									over.setFontAndSize(italic, 6);
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											Common.isEmpty(jawab) ? "-" : jawab, 100,
											230, 0);
								}
							}
							if (ans.getQuestion_id() == 5) {
								if (ans.getOption_type() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "v";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 510, 220, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("0")) {
									jawab = "-";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 510, 220, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "v";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 530, 220, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("0")) {
									jawab = "-";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 530, 220, 0);
								} else {
									jawab = ans.getAnswer2();
									over.setFontAndSize(italic, 6);
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											Common.isEmpty(jawab) ? "-" : jawab, 100,
											203, 0);
								}
							}
							if (ans.getQuestion_id() == 6) {
								if (ans.getOption_type() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "v";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 510, 193, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("0")) {
									jawab = "-";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 510, 193, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "v";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 530, 193, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("0")) {
									jawab = "-";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 530, 193, 0);
								} else {
									jawab = ans.getAnswer2();
									over.setFontAndSize(italic, 6);
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											Common.isEmpty(jawab) ? "-" : jawab, 100,
											183, 0);
								}
							}	
							if (ans.getQuestion_id() == 7) {
								if (ans.getOption_type() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "v";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 510, 153, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("0")) {
									jawab = "-";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 510, 153, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "v";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 530, 153, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("0")) {
									jawab = "-";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 530, 153, 0);
								} else {
									jawab = ans.getAnswer2();
									over.setFontAndSize(italic, 6);
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											Common.isEmpty(jawab) ? "-" : jawab, 100,
											124, 0);
								}
							}
							if (ans.getQuestion_id() == 8) {
								if (ans.getOption_type() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "v";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 510, 113, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("0")) {
									jawab = "-";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 510, 113, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "v";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 530, 113, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("0")) {
									jawab = "-";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 530, 113, 0);
								}
							}
							if (ans.getQuestion_id() == 9) {
								if (ans.getOption_type() == 0
										&& ans.getOption_order() == 1) {
									jawab = ans.getAnswer2();
									over.setFontAndSize(italic, 6);
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 115, 102, 0);
								} else if (ans.getOption_type() == 0
										&& ans.getOption_order() == 2) {
									jawab = ans.getAnswer2();
									over.setFontAndSize(italic, 6);
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 160, 92, 0);
								}
							}
							if (ans.getQuestion_id() == 10) {
								if (ans.getOption_type() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "v";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 510, 86, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("0")) {
									jawab = "-";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 510, 86, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "v";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 530, 86, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("0")) {
									jawab = "-";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 530, 86, 0);
								}
							}
							if (ans.getQuestion_id() == 11) {
								if (ans.getOption_type() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "v";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 510, 63, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("0")) {
									jawab = "-";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 510, 63, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "v";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 530, 63, 0);
								} else if (ans.getOption_type() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("0")) {
									jawab = "-";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 530, 63, 0);
								}
							}

						}	
				}
				
				over.endText();

				// ------------Halaman delapan-----------
				over = stamp.getOverContent(8);
				over.beginText();
				over.setFontAndSize(times_new_roman, 6);
				if (mspo_flag_spaj.equals("3")){
				if (rslt.size() > 0) {
					Integer j = 0;
					for (int i = 0; i < rslt.size(); i++) {
						MstQuestionAnswer ans = (MstQuestionAnswer) rslt.get(i);
						if (ans.getQuestion_id() == 12) {
							if (ans.getOption_type() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "v";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 510, 735, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("0")) {
								jawab = "-";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 510, 735, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "v";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 530, 735, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("0")) {
								jawab = "-";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 530, 735, 0);
							} else {
								jawab = ans.getAnswer2();
								over.setFontAndSize(italic, 6);
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										Common.isEmpty(jawab) ? "-" : jawab, 100,
										725, 0);
							}
						}
						if (ans.getQuestion_id() == 13) {
							if (ans.getOption_type() == 4
									&& ans.getOption_order() == 1) {
								jawab = ans.getAnswer2();
								over.setFontAndSize(italic, 6);
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 150, 705, 0);
							} else if (ans.getOption_type() == 4
									&& ans.getOption_order() == 2) {
								jawab = ans.getAnswer2();
								over.setFontAndSize(italic, 6);
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 250, 705, 0);
							}
						}
						if (ans.getQuestion_id() == 15) {
							if (ans.getOption_type() == 4
									&& ans.getOption_order() == 1) {
								jawab = ans.getAnswer2();
								over.setFontAndSize(italic, 6);
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 150, 685, 0);
							} else if (ans.getOption_type() == 4
									&& ans.getOption_order() == 2) {
								jawab = ans.getAnswer2();
								over.setFontAndSize(italic, 6);
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 250, 685, 0);
							}
						}
					}
				}

				// -------------data pemegang polis--------------
				if (rslt2.size() > 0) {
					Integer j = 0;
					for (int i = 0; i < rslt2.size(); i++) {
						MstQuestionAnswer ans = (MstQuestionAnswer) rslt2.get(i);
						if (ans.getQuestion_id() == 16) {
							if (ans.getOption_type() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "v";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 510, 655, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("0")) {
								jawab = "-";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 510, 655, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "v";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 530, 655, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("0")) {
								jawab = "-";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 530, 655, 0);
							} else if (ans.getOption_type() == 0
									&& ans.getOption_order() == 1) {
								jawab = ans.getAnswer2();
								over.setFontAndSize(italic, 6);
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										Common.isEmpty(jawab) ? "-" : jawab, 100,
										635, 0);
							} else {
								jawab = ans.getAnswer2();
								over.setFontAndSize(italic, 6);
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										Common.isEmpty(jawab) ? "-" : jawab, 100,
										635, 0);
							}
						}
						if (ans.getQuestion_id() == 17) {
							if (ans.getOption_type() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "v";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 510, 625, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("0")) {
								jawab = "-";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 510, 625, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "v";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 530, 625, 0);
							} else if (ans.getOption_type() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("0")) {
								jawab = "-";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 530, 625, 0);
							} else if (ans.getOption_type() == 0
									&& ans.getOption_order() == 1) {
								jawab = ans.getAnswer2();
								over.setFontAndSize(italic, 6);
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										Common.isEmpty(jawab) ? "-" : jawab, 100,
										615, 0);
							} else {
								jawab = ans.getAnswer2();
								over.setFontAndSize(italic, 6);
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										Common.isEmpty(jawab) ? "-" : jawab, 100,
										615, 0);
							}
						}
						if (ans.getQuestion_id() == 18) {
							if (ans.getOption_type() == 4
									&& ans.getOption_order() == 1) {
								jawab = ans.getAnswer2();
								over.setFontAndSize(italic, 6);
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 150, 595, 0);
							} else if (ans.getOption_type() == 4
									&& ans.getOption_order() == 2) {
								jawab = ans.getAnswer2();
								over.setFontAndSize(italic, 6);
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 250, 595, 0);
							}
						}
					}
				}
				}
				else{
					 if (rslt6.size() > 0) {
						// /81, 104
									Integer j = 0;
									Integer k = 0;
									Integer l = 0;
									for (int i = 0; i < rslt6.size(); i++) {
										MstQuestionAnswer ans = (MstQuestionAnswer) rslt6.get(i);
										// j += 3;
										if (ans.getQuestion_id() > 80 && ans.getQuestion_id() < 83) {
											if ((ans.getOption_group() == 1
													|| ans.getOption_group() == 2
													|| ans.getOption_group() == 3
													|| ans.getOption_group() == 4 || ans
													.getOption_group() == 5)
													&& (ans.getOption_order() == 1 || ans
															.getOption_order() == 2)
													&& ans.getAnswer2() == null) {
												jawab = null;
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 100, 700, 0);
												// --pp
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 745 - j, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 745 - j, 0);
											}
											// --tu
											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 765 - j, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 765 - j, 0);
											}
											// --tt1
											else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 765 - j, 0);
											} else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 765 - j, 0);
											}
											// --tt2
											else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 765 - j, 0);
											} else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 765 - j, 0);
											}
											// --tt3
											else if (ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 765 - j, 0);
											} else if (ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 765 - j, 0);
											}
											// --tt4
											else if (ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 765 - j, 0);
											} else if (ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 765 - j, 0);
											}
										}
										j += 10;
										if (ans.getQuestion_id() == 84) {
											if ((ans.getOption_group() == 1
													|| ans.getOption_group() == 2
													|| ans.getOption_group() == 3
													|| ans.getOption_group() == 4 || ans
													.getOption_group() == 5)
													&& (ans.getOption_order() == 1 || ans
															.getOption_order() == 2)
													&& ans.getAnswer2() == null) {
												jawab = null;
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 100, 700, 0);
												// --pp
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 595, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 595, 0);
											}
											// --tu
											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 595, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 595, 0);
											}
											// --tt1
											else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 595, 0);
											} else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 595, 0);
											}
											// --tt2
											else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 595, 0);
											} else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 595, 0);
											}
											// --tt3
											else if (ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 595, 0);
											} else if (ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 595, 0);
											}
											// --tt4
											else if (ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 595, 0);
											} else if (ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 595, 0);
											}
										}
										if (ans.getQuestion_id() == 85) {
											if ((ans.getOption_group() == 1
													|| ans.getOption_group() == 2
													|| ans.getOption_group() == 3
													|| ans.getOption_group() == 4 || ans
													.getOption_group() == 5)
													&& (ans.getOption_order() == 1 || ans
															.getOption_order() == 2)
													&& ans.getAnswer2() == null) {
												jawab = null;
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 100, 700, 0);
												// --pp
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 585, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 585, 0);
											}
											// --tu
											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 585, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 585, 0);
											}
											// --tt1
											else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 585, 0);
											} else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 585, 0);
											}
											// --tt2
											else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 585, 0);
											} else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 585, 0);
											}

										}
										if (ans.getQuestion_id() == 86) {
											if ((ans.getOption_group() == 1
													|| ans.getOption_group() == 2
													|| ans.getOption_group() == 3
													|| ans.getOption_group() == 4 || ans
													.getOption_group() == 5)
													&& (ans.getOption_order() == 1 || ans
															.getOption_order() == 2)
													&& ans.getAnswer2() == null) {
												jawab = null;
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 100, 700, 0);
												// --pp
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 575, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 575, 0);
											}
											// --tu
											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 575, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 575, 0);
											}
											// --tt1
											else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 575, 0);
											} else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 575, 0);
											}
											// --tt2
											else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 575, 0);
											} else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 575, 0);
											}

										}
										if (ans.getQuestion_id() == 87) {
											if ((ans.getOption_group() == 1
													|| ans.getOption_group() == 2
													|| ans.getOption_group() == 3
													|| ans.getOption_group() == 4 || ans
													.getOption_group() == 5)
													&& (ans.getOption_order() == 1 || ans
															.getOption_order() == 2)
													&& ans.getAnswer2() == null) {
												jawab = null;
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 100, 700, 0);
												// --pp
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 565, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 565, 0);
											}
											// --tu
											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 565, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 565, 0);
											}
											// --tt1
											else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 565, 0);
											} else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 565, 0);
											}
											// --tt2
											else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 565, 0);
											} else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 565, 0);
											}

										}
										if (ans.getQuestion_id() == 88) {
											if ((ans.getOption_group() == 1
													|| ans.getOption_group() == 2
													|| ans.getOption_group() == 3
													|| ans.getOption_group() == 4 || ans
													.getOption_group() == 5)
													&& (ans.getOption_order() == 1 || ans
															.getOption_order() == 2)
													&& ans.getAnswer2() == null) {
												jawab = null;
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 100, 700, 0);
												// --pp
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 555, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 555, 0);
											}
											// --tu
											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 555, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 555, 0);
											}
											// --tt1
											else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 555, 0);
											} else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 555, 0);
											}
											// --tt2
											else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 555, 0);
											} else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 555, 0);
											}

										}
										if (ans.getQuestion_id() == 89) {
											if ((ans.getOption_group() == 1
													|| ans.getOption_group() == 2
													|| ans.getOption_group() == 3
													|| ans.getOption_group() == 4 || ans
													.getOption_group() == 5)
													&& (ans.getOption_order() == 1 || ans
															.getOption_order() == 2)
													&& ans.getAnswer2() == null) {
												jawab = null;
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 100, 700, 0);
												// --pp
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 545, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 545, 0);
											}
											// --tu
											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 545, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 545, 0);
											}
											// --tt1
											else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 545, 0);
											} else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 545, 0);
											}
											// --tt2
											else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 545, 0);
											} else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 545, 0);
											}

										}
										if (ans.getQuestion_id() == 90) {
											if ((ans.getOption_group() == 1
													|| ans.getOption_group() == 2
													|| ans.getOption_group() == 3
													|| ans.getOption_group() == 4 || ans
													.getOption_group() == 5)
													&& (ans.getOption_order() == 1 || ans
															.getOption_order() == 2)
													&& ans.getAnswer2() == null) {
												jawab = null;
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 100, 700, 0);
												// --pp
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 535, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 535, 0);
											}
											// --tu
											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 535, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 535, 0);
											}
											// --tt1
											else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 535, 0);
											} else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 535, 0);
											}
											// --tt2
											else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 535, 0);
											} else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 535, 0);
											}

										}
										if (ans.getQuestion_id() == 91) {
											if ((ans.getOption_group() == 1
													|| ans.getOption_group() == 2
													|| ans.getOption_group() == 3
													|| ans.getOption_group() == 4 || ans
													.getOption_group() == 5)
													&& (ans.getOption_order() == 1 || ans
															.getOption_order() == 2)
													&& ans.getAnswer2() == null) {
												jawab = null;
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 100, 700, 0);
												// --pp
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 525, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 525, 0);
											}
											// --tu
											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 525, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 525, 0);
											}
											// --tt1
											else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 525, 0);
											} else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 525, 0);
											}
											// --tt2
											else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 525, 0);
											} else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 525, 0);
											}

										}
										if (ans.getQuestion_id() == 92) {
											if ((ans.getOption_group() == 1
													|| ans.getOption_group() == 2
													|| ans.getOption_group() == 3
													|| ans.getOption_group() == 4 || ans
													.getOption_group() == 5)
													&& (ans.getOption_order() == 1 || ans
															.getOption_order() == 2)
													&& ans.getAnswer2() == null) {
												jawab = null;
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 100, 700, 0);
												// --pp
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 515, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 515, 0);
											}
											// --tu
											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 515, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 515, 0);
											}
											// --tt1
											else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 515, 0);
											} else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 515, 0);
											}
											// --tt2
											else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 515, 0);
											} else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 515, 0);
											}

										}
										if (ans.getQuestion_id() == 93) {
											if ((ans.getOption_group() == 1
													|| ans.getOption_group() == 2
													|| ans.getOption_group() == 3
													|| ans.getOption_group() == 4 || ans
													.getOption_group() == 5)
													&& (ans.getOption_order() == 1 || ans
															.getOption_order() == 2)
													&& ans.getAnswer2() == null) {
												jawab = null;
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 100, 700, 0);
												// --pp
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 425, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 505, 0);
											}
											// --tu
											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 505, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 505, 0);
											}
											// --tt1
											else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 505, 0);
											} else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 505, 0);
											}
											// --tt2
											else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 505, 0);
											} else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 505, 0);
											}

										}
										if (ans.getQuestion_id() == 94) {
											if ((ans.getOption_group() == 1
													|| ans.getOption_group() == 2
													|| ans.getOption_group() == 3
													|| ans.getOption_group() == 4 || ans
													.getOption_group() == 5)
													&& (ans.getOption_order() == 1 || ans
															.getOption_order() == 2)
													&& ans.getAnswer2() == null) {
												jawab = null;
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 100, 700, 0);
												// --pp
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 495, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 495, 0);
											}
											// --tu
											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 495, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 495, 0);
											}
											// --tt1
											else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 495, 0);
											} else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 495, 0);
											}
											// --tt2
											else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 495, 0);
											} else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 495, 0);
											}

										}
										if (ans.getQuestion_id() == 95) {
											if ((ans.getOption_group() == 1
													|| ans.getOption_group() == 2
													|| ans.getOption_group() == 3
													|| ans.getOption_group() == 4 || ans
													.getOption_group() == 5)
													&& (ans.getOption_order() == 1 || ans
															.getOption_order() == 2)
													&& ans.getAnswer2() == null) {
												jawab = null;
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 100, 700, 0);
												// --pp
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 485, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 485, 0);
											}
											// --tu
											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 485, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 485, 0);
											}
											// --tt1
											else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 485, 0);
											} else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 485, 0);
											}
											// --tt2
											else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 485, 0);
											} else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 485, 0);
											}

										}
										if (ans.getQuestion_id() == 96) {
											if ((ans.getOption_group() == 1
													|| ans.getOption_group() == 2
													|| ans.getOption_group() == 3
													|| ans.getOption_group() == 4 || ans
													.getOption_group() == 5)
													&& (ans.getOption_order() == 1 || ans
															.getOption_order() == 2)
													&& ans.getAnswer2() == null) {
												jawab = null;
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 100, 700, 0);
												// --pp
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 475, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 475, 0);
											}
											// --tu
											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 475, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 475, 0);
											}
											// --tt1
											else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 475, 0);
											} else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 475, 0);
											}
											// --tt2
											else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 475, 0);
											} else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 475, 0);
											}

										}
										if (ans.getQuestion_id() == 97) {
											if ((ans.getOption_group() == 1
													|| ans.getOption_group() == 2
													|| ans.getOption_group() == 3
													|| ans.getOption_group() == 4 || ans
													.getOption_group() == 5)
													&& (ans.getOption_order() == 1 || ans
															.getOption_order() == 2)
													&& ans.getAnswer2() == null) {
												jawab = null;
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 100, 700, 0);
												// --pp
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 465, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 465, 0);
											}
											// --tu
											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 465, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 465, 0);
											}
											// --tt1
											else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 465, 0);
											} else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 465, 0);
											}
											// --tt2
											else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 465, 0);
											} else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 465, 0);
											}
										}
										if (ans.getQuestion_id() == 98) {
											if ((ans.getOption_group() == 1
													|| ans.getOption_group() == 2
													|| ans.getOption_group() == 3
													|| ans.getOption_group() == 4 || ans
													.getOption_group() == 5)
													&& (ans.getOption_order() == 1 || ans
															.getOption_order() == 2)
													&& ans.getAnswer2() == null) {
												jawab = null;
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 100, 700, 0);
												// --pp
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 455, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 455, 0);
											}
											// --tu
											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 455, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 455, 0);
											}
											// --tt1
											else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 455, 0);
											} else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 455, 0);
											}
											// --tt2
											else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 455, 0);
											} else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 455, 0);
											}
										}
										if (ans.getQuestion_id() == 99) {
											if ((ans.getOption_group() == 1
													|| ans.getOption_group() == 2
													|| ans.getOption_group() == 3
													|| ans.getOption_group() == 4 || ans
													.getOption_group() == 5)
													&& (ans.getOption_order() == 1 || ans
															.getOption_order() == 2)
													&& ans.getAnswer2() == null) {
												jawab = null;
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 100, 700, 0);
												// --pp
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 445, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 445, 0);
											}
											// --tu
											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 445, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 445, 0);
											}
											// --tt1
											else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 445, 0);
											} else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 445, 0);
											}
											// --tt2
											else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 445, 0);
											} else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 445, 0);
											}

										}
										if (ans.getQuestion_id() == 100) {
											if ((ans.getOption_group() == 1
													|| ans.getOption_group() == 2
													|| ans.getOption_group() == 3
													|| ans.getOption_group() == 4 || ans
													.getOption_group() == 5)
													&& (ans.getOption_order() == 1 || ans
															.getOption_order() == 2)
													&& ans.getAnswer2() == null) {
												jawab = null;
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 100, 700, 0);
												// --pp
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 435, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 435, 0);
											}
											// --tu
											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 435, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 435, 0);
											}
											// --tt1
											else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 435, 0);
											} else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 435, 0);
											}
											// --tt2
											else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 435, 0);
											} else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 435, 0);
											}
											// --tt3
											else if (ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 435, 0);
											} else if (ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 435, 0);
											}
											// --tt4
											else if (ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 435, 0);
											} else if (ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 435, 0);
											}
										}
										if (ans.getQuestion_id() == 101) {
											if ((ans.getOption_group() == 1
													|| ans.getOption_group() == 2
													|| ans.getOption_group() == 3
													|| ans.getOption_group() == 4 || ans
													.getOption_group() == 5)
													&& (ans.getOption_order() == 1 || ans
															.getOption_order() == 2)
													&& ans.getAnswer2() == null) {
												jawab = null;
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 100, 700, 0);
												// --pp
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 430, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 430, 0);
											}
											// --tu
											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 430, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 430, 0);
											}
											// --tt1
											else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 430, 0);
											} else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 430, 0);
											}
											// --tt2
											else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 430, 0);
											} else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 430, 0);
											}
											// --tt3
											else if (ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 430, 0);
											} else if (ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 430, 0);
											}
											// --tt4
											else if (ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 430, 0);
											} else if (ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 430, 0);
											} else if (ans.getOption_group() == 0
													&& ans.getOption_order() == 1) {
												jawab = ans.getAnswer2();
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 110,
														400, 0);
											}
										}
										if (ans.getQuestion_id() == 102) {
											if ((ans.getOption_group() == 1
													|| ans.getOption_group() == 2
													|| ans.getOption_group() == 3
													|| ans.getOption_group() == 4 || ans
													.getOption_group() == 5)
													&& (ans.getOption_order() == 1 || ans
															.getOption_order() == 2)
													&& ans.getAnswer2() == null) {
												jawab = null;
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 100, 700, 0);
												// --pp
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 390, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 390, 0);
											}
											// --tu
											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 390, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 390, 0);
											}
											// --tt1
											else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 390, 0);
											} else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 390, 0);
											}
											// --tt2
											else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 390, 0);
											} else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 390, 0);
											}
											// --tt3
											else if (ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 390, 0);
											} else if (ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 390, 0);
											}
											// --tt4
											else if (ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 390, 0);
											} else if (ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 390, 0);
											} else if (ans.getOption_group() == 0
													&& ans.getOption_order() == 1) {
												jawab = ans.getAnswer2();
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 110,
														352, 0);
											}
										}
										if (ans.getQuestion_id() == 103) {
											if ((ans.getOption_group() == 1
													|| ans.getOption_group() == 2
													|| ans.getOption_group() == 3
													|| ans.getOption_group() == 4 || ans
													.getOption_group() == 5)
													&& (ans.getOption_order() == 1 || ans
															.getOption_order() == 2)
													&& ans.getAnswer2() == null) {
												jawab = null;
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 100, 700, 0);
												// --pp
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 342, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 370, 342, 0);
											}
											// --tu
											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 342, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 342, 0);
											}
											// --tt1
											else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 342, 0);
											} else if (ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 342, 0);
											}
											// --tt2
											else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 342, 0);
											} else if (ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 342, 0);
											}
											// --tt3
											else if (ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 342, 0);
											} else if (ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 342, 0);
											}
											// --tt4
											else if (ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 342, 0);
											} else if (ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 342, 0);
											} else if (ans.getOption_group() == 0
													&& ans.getOption_order() == 1) {
												jawab = ans.getAnswer2();
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 110,
														282, 0);
											}
										}
										if (ans.getQuestion_id() == 104) {
											if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 370,
														272, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 370,
														262, 0);
											}

											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 400,
														272, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 400,
														262, 0);
											}

											else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 425,
														272, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 425,
														262, 0);
											}

											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 463,
														272, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 463,
														262, 0);
											}

											else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 493,
														272, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 493,
														262, 0);
											}

											else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 1) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 540,
														272, 0);
											} else if (ans.getOption_group() == 2
													&& ans.getOption_order() == 2) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 540,
														262, 0);
											}
										}
									}
								}	
				}
				over.endText();
				
				
				// ------------Halaman sembilan-----------
				if (mspo_flag_spaj.equals("3")){
					
				
				over = stamp.getOverContent(9);
				over.beginText();
				over.setFontAndSize(times_new_roman, 6);

				if (rslt3.size() > 0 ) {
					Integer j = 0;
					for (int i = 0; i < rslt3.size(); i++) {
						MstQuestionAnswer ans = (MstQuestionAnswer) rslt3.get(i);
							if ((ans.getOption_group() == 1
									|| ans.getOption_group() == 2
									|| ans.getOption_group() == 3
									|| ans.getOption_group() == 4 || ans
									.getOption_group() == 5)
									&& (ans.getOption_order() == 1 || ans
											.getOption_order() == 2)
									&& ans.getAnswer2() == null) {
								jawab = null;
								over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
										360, 700 - j, 0);
								// --pp
								
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
										360, 681 - j, 0);
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
										360, 681 - j, 0);
							}
							else if (ans.getOption_group() == 2
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
										395, 683 - j, 0);
							} else if (ans.getOption_group() == 2
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
										395, 683 - j, 0);
							}
							if (peserta.size() > 0 && ans.getAnswer2() != null) {
								for (int x = 0; x < peserta.size(); x++) {
									PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
									
									// --tt1/pt1
									if (pesertaPlus.getFlag_jenis_peserta()==1 
											&& ans.getOption_group() == 3
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
												425, 685 - j, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
												425, 685 - j, 0);
									}
									// --tt2/pt2
									else if (pesertaPlus.getFlag_jenis_peserta()==2 
											&& ans.getOption_group() == 4
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
												463, 688 - j, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
												463, 688 - j, 0);
									}
									// --tt3/pt3
									else if (pesertaPlus.getFlag_jenis_peserta()==3 
											&& ans.getOption_group() == 5
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
												493, 690 - j, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
												493, 690 - j, 0);
									}
									// --tt4/pt4
									else if (pesertaPlus.getFlag_jenis_peserta()==4 
											&& ans.getOption_group() == 6
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
												540, 692 - j, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
												540, 692 - j, 0);
									}
									
								}
							}else if (dataTT.getLsre_id()==1){
								if (ans.getOption_group() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
											395, 683 - j, 0);
								} else if (ans.getOption_group() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT, jawab,
											395, 683 - j, 0);
								}
							}
						
						j += 1;
					}
				}

				if (rslt4.size() > 0) {
					Integer j = 0;
					for (int i = 0; i < rslt4.size(); i++) {
						MstQuestionAnswer ans = (MstQuestionAnswer) rslt4.get(i);
						if (ans.getQuestion_id() == 137) {
							if ((ans.getOption_group() == 1
									|| ans.getOption_group() == 2
									|| ans.getOption_group() == 3
									|| ans.getOption_group() == 4 || ans
									.getOption_group() == 5)
									&& (ans.getOption_order() == 1 || ans
											.getOption_order() == 2)
									&& ans.getAnswer2() == null) {
								jawab = null;
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 100, 700, 0);
								// --pp
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 360, 356, 0);
								
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 360, 356, 0);
							}
							 
							 else if (ans.getOption_group() == 0) {
								jawab = ans.getAnswer2();
								over.setFontAndSize(italic, 6);
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										Common.isEmpty(jawab) ? "-" : jawab, 100,
										320, 0);
							}
							
							if (peserta.size() > 0 && ans.getAnswer2() != null) {
								for (int x = 0; x < peserta.size(); x++) {
									PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
									// --tu/pu
									if (pesertaPlus.getFlag_jenis_peserta()==0 && ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 395, 356, 0);
									}
									else if (pesertaPlus.getFlag_jenis_peserta()==0 && ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 395, 356, 0);
									}
									// --tt1
									else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 425, 356, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 425, 356, 0);
									}
									// --tt2
									else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 463, 356, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 463, 356, 0);
									}
									// --tt3
									else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 493, 356, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 493, 356, 0);
									}
									// --tt4
									else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 540, 356, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 540, 356, 0);
									}
									
								}
							}else if (dataTT.getLsre_id()==1){
								if (ans.getOption_group() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 395, 356, 0);
									
								} else if (ans.getOption_group() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 395, 356, 0);
								}
							}

						}
								
						if (ans.getQuestion_id() == 139) {
							if ((ans.getOption_group() == 1
									|| ans.getOption_group() == 2
									|| ans.getOption_group() == 3
									|| ans.getOption_group() == 4 || ans
									.getOption_group() == 5)
									&& (ans.getOption_order() == 1 || ans
											.getOption_order() == 2)
									&& ans.getAnswer2() == null) {
								jawab = null;
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 100, 676, 0);
								// --pp
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 360, 288, 0);
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 360, 288, 0);
							}

							// --tu
							else if (ans.getOption_group() == 2
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 288, 0);
							} else if (ans.getOption_group() == 2
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 288, 0);
							}
							if (peserta.size() > 0 && ans.getAnswer2() != null) {
								for (int x = 0; x < peserta.size(); x++) {
									PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
									
									// --tt1
									 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 425, 288, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 425, 288, 0);
									}
									// --tt2
									else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 463, 288, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 463, 288, 0);
									}
									// --tt3
									else if (pesertaPlus.getFlag_jenis_peserta()==3 &&ans.getOption_group() == 5
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 493, 288, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 493, 288, 0);
									}
									// --tt4
									else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 540, 288, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 540, 288, 0);
									}
								}
							}else if (dataTT.getLsre_id()==1){
								if (ans.getOption_group() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 395, 288, 0);
								} else if (ans.getOption_group() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 395, 288, 0);
								}
							}
							
						}
						// j += 3;
						if (ans.getQuestion_id() >= 140
								&& ans.getQuestion_id() <= 143) {
							if ((ans.getOption_group() == 1
									|| ans.getOption_group() == 2
									|| ans.getOption_group() == 3
									|| ans.getOption_group() == 4 || ans
									.getOption_group() == 5)
									&& (ans.getOption_order() == 1 || ans
											.getOption_order() == 2)
									&& ans.getAnswer2() == null) {
								jawab = null;
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 100, 700, 0);
								// --pp
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 360, 323 - j, 0);
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 360, 323 - j, 0);
							}
							// --tu
							else if (ans.getOption_group() == 2
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 327 - j, 0);
							} else if (ans.getOption_group() == 2
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 327 - j, 0);
							}
							if (peserta.size() > 0 && ans.getAnswer2() != null) {
								for (int x = 0; x < peserta.size(); x++) {
									PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
									
									// --tt1
									if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 425, 331 - j, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 425, 331 - j, 0);
									}
									// --tt2
									else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 463, 335 - j, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 463, 335 - j, 0);
									}
									// --tt3
									else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 493, 339 - j, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 493, 339 - j, 0);
									}
									// --tt4
									else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 540, 343 - j, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 540, 343 - j, 0);
									}
								}
								}else if (dataTT.getLsre_id()==1){
									 if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 395, 324 - j, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 395, 324 - j, 0);
										}
								}
							}
							
						j += 2;
						if (ans.getQuestion_id() == 144) {
							if ((ans.getOption_group() == 1
									|| ans.getOption_group() == 2
									|| ans.getOption_group() == 3
									|| ans.getOption_group() == 4 || ans
									.getOption_group() == 5)
									&& (ans.getOption_order() == 1 || ans
											.getOption_order() == 2)
									&& ans.getAnswer2() == null) {
								jawab = null;
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 100, 676, 0);
								// --pp
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 360, 188, 0);
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 360, 188, 0);
							}
							// --tu
							else if (ans.getOption_group() == 2
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 188, 0);
							} else if (ans.getOption_group() == 2
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 188, 0);
							}
							if (peserta.size() > 0 && ans.getAnswer2() != null) {
								for (int x = 0; x < peserta.size(); x++) {
									PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
									
									// --tt1
									 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 425, 188, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 425, 188, 0);
									}
									// --tt2
									else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 463, 188, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 463, 188, 0);
									}
									// --tt3
									else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 493, 188, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 493, 188, 0);
									}
									// --tt4
									else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 540, 188, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 540, 188, 0);
									}
								}
								}else if (dataTT.getLsre_id()==1){
									if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 395, 186, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 395, 186, 0);
									}
								}
							}
							
								
						if (ans.getQuestion_id() == 145) {
							if ((ans.getOption_group() == 1
									|| ans.getOption_group() == 2
									|| ans.getOption_group() == 3
									|| ans.getOption_group() == 4 || ans
									.getOption_group() == 5)
									&& (ans.getOption_order() == 1 || ans
											.getOption_order() == 2)
									&& ans.getAnswer2() == null) {
								jawab = null;
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 100, 676, 0);
								// --pp
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 360, 178, 0);
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 360, 168, 0);
							}
							// --tu
							else if (ans.getOption_group() == 2
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 168, 0);
							} else if (ans.getOption_group() == 2
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 168, 0);
							}
							if (peserta.size() > 0 && ans.getAnswer2() != null) {
								for (int x = 0; x < peserta.size(); x++) {
									PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
									
									// --tt1
									 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 425, 168, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 425, 168, 0);
									}
									// --tt2
									else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 463, 168, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 463, 168, 0);
									}
									// --tt3
									else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 493, 168, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 493, 168, 0);
									}
									// --tt4
									else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 540, 168, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 540, 168, 0);
									}
								}
							}else if (dataTT.getLsre_id()==1){
								if (ans.getOption_group() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 395, 166, 0);
								} else if (ans.getOption_group() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 395, 166, 0);
								}
							}
						}
					}
				}

				over.endText();

				// --------------Halaman sepuluh--------------//
				over = stamp.getOverContent(10);
				over.beginText();
				over.setFontAndSize(times_new_roman, 6);

				if (rslt5.size() > 0) {
					Integer j = 0;
					for (int i = 0; i < rslt5.size(); i++) {
						MstQuestionAnswer ans = (MstQuestionAnswer) rslt5.get(i);
						if (ans.getQuestion_id() == 146) {
							if ((ans.getOption_group() == 1
									|| ans.getOption_group() == 2
									|| ans.getOption_group() == 3
									|| ans.getOption_group() == 4 || ans
									.getOption_group() == 5)
									&& (ans.getOption_order() == 1 || ans
											.getOption_order() == 2)
									&& ans.getAnswer2() == null) {
								jawab = null;
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 100, 676, 0);
								// --pp
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 360, 735, 0);
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 360, 735, 0);
							}
							// --tu
							else if ( ans.getOption_group() == 2
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 735, 0);
							} else if (ans.getOption_group() == 2
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 735, 0);
							}
							if (peserta.size() > 0 && ans.getAnswer2() != null) {
								for (int x = 0; x < peserta.size(); x++) {
									PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
									
									// --tt1
									 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 425, 735, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 425, 735, 0);
									}
									// --tt2
									else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 463, 735, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 463, 735, 0);
									}
									// --tt3
									else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 493, 735, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 493, 735, 0);
									}
									// --tt4
									else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 540, 735, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 540, 735, 0);
									}
								}
							}else  if (dataTT.getLsre_id()==1){
								if (ans.getOption_group() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 395, 735, 0);
								} else if (ans.getOption_group() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 395, 735, 0);
								}
							}
						}
						// j += 3;
						if (ans.getQuestion_id() == 147) {
							jawab = ans.getAnswer2();
							over.setFontAndSize(italic, 6);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									Common.isEmpty(jawab) ? "-" : jawab, 120, 697,
									0);
						} else if (ans.getQuestion_id() == 148) {
							jawab = ans.getAnswer2();
							over.setFontAndSize(italic, 6);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									Common.isEmpty(jawab) ? "-" : jawab, 120, 667,
									0);
						} else if (ans.getQuestion_id() == 149) {
							jawab = ans.getAnswer2();
							over.setFontAndSize(italic, 6);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									Common.isEmpty(jawab) ? "-" : jawab, 120, 637,
									0);
						} else if (ans.getQuestion_id() == 150) {
							jawab = ans.getAnswer2();
							over.setFontAndSize(italic, 6);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									Common.isEmpty(jawab) ? "-" : jawab, 120, 607,
									0);
						} else if (ans.getQuestion_id() == 151) {
							jawab = ans.getAnswer2();
							over.setFontAndSize(italic, 6);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									Common.isEmpty(jawab) ? "-" : jawab, 120, 578,
									0);
						}
						if (ans.getQuestion_id() == 152) {
							if ((ans.getOption_group() == 1
									|| ans.getOption_group() == 2
									|| ans.getOption_group() == 3
									|| ans.getOption_group() == 4 || ans
									.getOption_group() == 5)
									&& (ans.getOption_order() == 1 || ans
											.getOption_order() == 2)
									&& ans.getAnswer2() == null) {
								jawab = null;
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 100, 676, 0);
								// --pp
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 360, 588, 0);
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 360, 588, 0);
							}
							// --tu
							else if (ans.getOption_group() == 2
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 588, 0);
							} else if (ans.getOption_group() == 2
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 588, 0);
							}
							
							if (peserta.size() > 0 && ans.getAnswer2() != null) {
								for (int x = 0; x < peserta.size(); x++) {
									PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);						
									// --tt1
									if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 425, 588, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 425, 588, 0);
									}
									// --tt2
									else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 463, 588, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 463, 588, 0);
									}
									// --tt3
									else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 493, 588, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 493, 588, 0);
									}
									// --tt4
									else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 540, 588, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 540, 588, 0);
									}
								}
							}else if(ans.getAnswer2() != null && dataTT.getLsre_id()==1){
								if (ans.getOption_group() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 395, 588, 0);
								} else if (ans.getOption_group() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 395, 588, 0);
								}
							}
								
						}
						if (ans.getQuestion_id() == 153) {
							if ((ans.getOption_group() == 1
									|| ans.getOption_group() == 2
									|| ans.getOption_group() == 3
									|| ans.getOption_group() == 4 || ans
									.getOption_group() == 5)
									&& (ans.getOption_order() == 1 || ans
											.getOption_order() == 2)
									&& ans.getAnswer2() == null) {
								jawab = null;
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 100, 676, 0);
								// --pp
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 360, 568, 0);
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 360, 568, 0);
							}
							// --tu
							else if (ans.getOption_group() == 2
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 568, 0);
							} else if (ans.getOption_group() == 2
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 568, 0);
							}
							if (peserta.size() > 0 && ans.getAnswer2() != null) {
								for (int x = 0; x < peserta.size(); x++) {
									PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
									
									// --tt1
									 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 425, 568, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 425, 568, 0);
									}
									// --tt2
									else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 463, 568, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 463, 568, 0);
									}
									// --tt3
									else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 493, 568, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 493, 568, 0);
									}
									// --tt4
									else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 540, 568, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 540, 568, 0);
									}
								}
							}else if(ans.getAnswer2() != null && dataTT.getLsre_id()==1){
								if (ans.getOption_group() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 395, 568, 0);
								} else if (ans.getOption_group() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 395, 568, 0);
								}
							}
							
						}
						if (ans.getQuestion_id() == 154) {
							if ((ans.getOption_group() == 1
									|| ans.getOption_group() == 2
									|| ans.getOption_group() == 3
									|| ans.getOption_group() == 4 || ans
									.getOption_group() == 5)
									&& (ans.getOption_order() == 1 || ans
											.getOption_order() == 2)
									&& ans.getAnswer2() == null) {
								jawab = null;
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 100, 676, 0);
								// --pp
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 360, 548, 0);
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 360, 548, 0);
							}
							// --tu
							else if (ans.getOption_group() == 2
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 548, 0);
							} else if (ans.getOption_group() == 2
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 548, 0);
							}
							if (peserta.size() > 0 && ans.getAnswer2() != null) {
								for (int x = 0; x < peserta.size(); x++) {
									PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
									
									// --tt1
									 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 425, 548, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 425, 548, 0);
									}
									// --tt2
									else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 463, 548, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 463, 548, 0);
									}
									// --tt3
									else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 493, 548, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 493, 548, 0);
									}
									// --tt4
									else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 540, 548, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 540, 548, 0);
									}
								}
							}else if(ans.getAnswer2() != null && dataTT.getLsre_id()==1){
								if (ans.getOption_group() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 395, 548, 0);
								} else if (ans.getOption_group() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 395, 548, 0);
								}
							}
							
						}
						if (ans.getQuestion_id() == 155) {
							if ((ans.getOption_group() == 1
									|| ans.getOption_group() == 2
									|| ans.getOption_group() == 3
									|| ans.getOption_group() == 4 || ans
									.getOption_group() == 5)
									&& (ans.getOption_order() == 1 || ans
											.getOption_order() == 2)
									&& ans.getAnswer2() == null) {
								jawab = null;
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 100, 676, 0);
								// --pp
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 360, 528, 0);
							} else if (ans.getOption_group() == 1
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 360, 528, 0);
							}
							// --tu
							else if (ans.getOption_group() == 2
									&& ans.getOption_order() == 1
									&& ans.getAnswer2().equals("1")) {
								jawab = "Ya";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 528, 0);
							} else if (ans.getOption_group() == 2
									&& ans.getOption_order() == 2
									&& ans.getAnswer2().equals("1")) {
								jawab = "Tidak";
								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
										jawab, 395, 528, 0);
							}
							if (peserta.size() > 0 && ans.getAnswer2() != null) {
								for (int x = 0; x < peserta.size(); x++) {
									PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
									
									// --tt1
									 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 425, 528, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 425, 528, 0);
									}
									// --tt2
									else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 463, 528, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 463, 528, 0);
									}
									// --tt3
									else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 493, 528, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 493, 528, 0);
									}
									// --tt4
									else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 540, 528, 0);
									} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 540, 528, 0);
									}
								}
							}else if (dataTT.getLsre_id()==1){
								if (ans.getOption_group() == 1
										&& ans.getOption_order() == 1
										&& ans.getAnswer2().equals("1")) {
									jawab = "Ya";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 395, 528, 0);
								} else if ( ans.getOption_group() == 1
										&& ans.getOption_order() == 2
										&& ans.getAnswer2().equals("1")) {
									jawab = "Tidak";
									over.showTextAligned(PdfContentByte.ALIGN_LEFT,
											jawab, 395, 528, 0);
								}
							}	
						}
					}
					j += 2;
				}
				over.endText();
				stamp.close();
				}
				
			return null;
		}
	
	public ModelAndView espajonlinegadgetsiosyariah(HttpServletRequest request,
			HttpServletResponse response, ElionsManager elionsManager,
			UwManager uwManager, BacManager bacManager, Properties props,
			Products products, String spaj) throws Exception {

		String reg_spaj = spaj;
		Integer type = null;
		Date sysdate = elionsManager.selectSysdate();
		List<String> pdfs = new ArrayList<String>();
		Boolean suksesMerge = false;
		HashMap<String, Object> cmd = new HashMap<String, Object>();
		ArrayList data_answer = new ArrayList();
		Integer index = null;
		Integer index2 = null;
		String spaj_gadget = "";
		String mspo_flag_spaj = bacManager.selectMspoFLagSpaj(reg_spaj);
		String cabang = elionsManager.selectCabangFromSpaj(reg_spaj);
		String exportDirectory = props.getProperty("pdf.dir.export") + "\\"
				+ cabang + "\\" + reg_spaj;
		String spajTempDirectory = props.getProperty("pdf.dir.spajtemp");
		String storagespajtemp = props.getProperty("pdf.dir.ftp");
		System.out.print(mspo_flag_spaj);
		String dir = props.getProperty("pdf.template.espajonlinegadget");
		OutputStream output;
		PdfReader reader;
		File userDir = new File(props.getProperty("pdf.dir.export") + "\\"
				+ cabang + "\\" + reg_spaj);
		if (!userDir.exists()) {
			userDir.mkdirs();
		}

		HashMap moreInfo = new HashMap();
		moreInfo.put("Author", "PT ASURANSI JIWA SINARMAS MSIG Tbk.");
		moreInfo.put("Title", "GADGET");
		moreInfo.put("Subject", "E-SPAJ ONLINE");

		PdfContentByte over;
		PdfContentByte over2;
		BaseFont times_new_roman = BaseFont.createFont(
				"C:\\WINDOWS\\FONTS\\ARIAL.TTF", BaseFont.CP1252,
				BaseFont.NOT_EMBEDDED);
		BaseFont italic = BaseFont.createFont("C:\\WINDOWS\\FONTS\\ariali.ttf",
				BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

				reader = new PdfReader(
						props.getProperty("pdf.template.espajonlinegadget")
								+ "\\spajonlinegadgetsiosyariah.pdf");
				output = new FileOutputStream(exportDirectory + "\\"
						+ "espajonlinegadget_" + reg_spaj + ".pdf");

				spaj_gadget = dir + "\\spajonlinegadgetsiosyariah.pdf";
			
			pdfs.add(spaj_gadget);
			suksesMerge = MergePDF.concatPDFs(pdfs, output, false);
			String outputName = props.getProperty("pdf.dir.export") + "\\"
					+ cabang + "\\" + reg_spaj + "\\" + "espajonlinegadget_"
					+ reg_spaj + ".pdf";
			PdfStamper stamp = new PdfStamper(reader, new FileOutputStream(
					outputName));

			Pemegang dataPP = elionsManager.selectpp(reg_spaj);
			Tertanggung dataTT = elionsManager.selectttg(reg_spaj);
			PembayarPremi pembPremi = bacManager.selectP_premi(reg_spaj);
			if (pembPremi == null)
				pembPremi = new PembayarPremi();
			AddressBilling addrBill = elionsManager
					.selectAddressBilling(reg_spaj);
			Datausulan dataUsulan = elionsManager
					.selectDataUsulanutama(reg_spaj);
			dataUsulan.setDaftaRider(elionsManager
					.selectDataUsulan_rider(reg_spaj));
			InvestasiUtama inv = elionsManager
					.selectinvestasiutama(reg_spaj);
			Rekening_client rekClient = elionsManager
					.select_rek_client(reg_spaj);
			Account_recur accRecur = elionsManager
					.select_account_recur(reg_spaj);
			List detInv = bacManager.selectdetilinvestasi2(reg_spaj);
			List benef = elionsManager.select_benef(reg_spaj);
			List peserta = uwManager.select_all_mst_peserta(reg_spaj);
			List dist = elionsManager.select_tipeproduk();
			List listSpajTemp = bacManager.selectReferensiTempSpaj(reg_spaj);
			HashMap spajTemp = (HashMap) listSpajTemp.get(0);
			String idgadget = (String) spajTemp.get("NO_TEMP");
			Map agen = bacManager.selectAgenESPAJSimasPrima(reg_spaj);
			List namaBank = uwManager.namaBank(accRecur.getLbn_id());

			// --Question Full Konven/Syariah
			List rslt = bacManager.selectQuestionareGadget(reg_spaj, 2, 1, 15);	
			List rslt2 = bacManager.selectQuestionareGadget(reg_spaj, 1, 16, 18); 
			List rslt3 = bacManager.selectQuestionareGadget(reg_spaj, 3, 106, 136); 
			List rslt4 = bacManager.selectQuestionareGadget(reg_spaj, 3, 137, 145);
			List rslt5 = bacManager.selectQuestionareGadget(reg_spaj, 3, 146, 155);
			
			//Sio
			List rslt6 = bacManager.selectQuestionareGadget(reg_spaj, 12, 81, 104);
			
			String s_channel = "";
			for (int i = 0; i < dist.size(); i++) {
				HashMap dist2 = (HashMap) dist.get(i);
				Integer i_lstp_id = (Integer) dist2.get("lstp_id");
				if (i_lstp_id.intValue() == dataUsulan.getTipeproduk()
						.intValue()) {
					s_channel = (String) dist2.get("lstp_produk");
				}
			}

			Double d_premiRider = 0.;
			if (dataUsulan.getDaftaRider().size() > 0) {
				for (int i = 0; i < dataUsulan.getDaftaRider().size(); i++) {
					Datarider rider = (Datarider) dataUsulan.getDaftaRider()
							.get(i);
					d_premiRider = rider.getMspr_premium();
				}
			}
			Double d_topUpBerkala = new Double(0);
			Double d_topUpTunggal = new Double(0);
			Double d_totalTopup = new Double(0);
			if (inv != null) {
				DetilTopUp daftarTopup = inv.getDaftartopup();
				d_topUpBerkala = Common.isEmpty(daftarTopup.getPremi_berkala()) ? new Double(
						0) : daftarTopup.getPremi_berkala();
				d_topUpTunggal = Common.isEmpty(daftarTopup.getPremi_tunggal()) ? new Double(
						0) : daftarTopup.getPremi_tunggal();
				d_totalTopup = d_topUpBerkala + d_topUpTunggal;
			}
			Double d_premiExtra = (Common.isEmpty(uwManager
					.selectSumPremiExtra(reg_spaj)) ? 0. : uwManager
					.selectSumPremiExtra(reg_spaj));
			Double d_totalPremi = dataUsulan.getMspr_premium() + d_totalTopup
					+ d_premiRider + d_premiExtra;

			stamp.setMoreInfo(moreInfo);

			// ---------- Data Halaman Pertama ----------

			over = stamp.getOverContent(1);
			over.beginText();
			over.setFontAndSize(times_new_roman, 8);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					FormatString.nomorSPAJ(reg_spaj), 380, 627, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					FormatDate.toString(sysdate), 85, 617, 0);

			over.setFontAndSize(times_new_roman, 6);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP
					.getMcl_first().toUpperCase(), 160, 516, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
					.getMcl_first().toUpperCase(), 160, 506, 0);
			over.showTextAligned(PdfContentByte.ALIGN_LEFT,
					dataUsulan.getLsdbs_name(), 160, 496, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					dataUsulan.getLku_symbol()
							+ " "
							+ FormatNumber.convertToTwoDigit(new BigDecimal(
									dataUsulan.getMspr_tsi())), 160, 486, 0);

			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					dataUsulan.getLku_symbol()
							+ " "
							+ FormatNumber.convertToTwoDigit(new BigDecimal(
									dataUsulan.getMspr_premium())), 290, 476, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					d_topUpBerkala.doubleValue() == new Double(0) ? "-"
							: (dataUsulan.getLku_symbol() + " " + FormatNumber
									.convertToTwoDigit(new BigDecimal(
											d_topUpBerkala))), 290, 467, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					d_topUpTunggal.doubleValue() == new Double(0) ? "-"
							: (dataUsulan.getLku_symbol() + " " + FormatNumber
									.convertToTwoDigit(new BigDecimal(
											d_topUpTunggal))), 290, 457, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					d_premiRider.doubleValue() == new Double(0) ? "-"
							: (dataUsulan.getLku_symbol() + " " + FormatNumber
									.convertToTwoDigit(new BigDecimal(
											d_premiRider))), 290, 447, 0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					dataUsulan.getLku_symbol()
							+ " "
							+ FormatNumber.convertToTwoDigit(new BigDecimal(
									d_totalPremi)), 290, 437, 0);

			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(agen.get("NM_AGEN").toString().toUpperCase()) ? "-" : 
						agen.get("NM_AGEN").toString().toUpperCase(),160, 409,0);
			over.showTextAligned(
					PdfContentByte.ALIGN_LEFT,
					Common.isEmpty(agen.get("KD_AGEN").toString().toUpperCase()) ? "-" : 
						agen.get("KD_AGEN").toString().toUpperCase(),160, 399,0);
			over.endText();

				// ---------- Data Halaman keempat ----------
				over = stamp.getOverContent(4);
				over.beginText();
				//--pencarian file ttd
				File ttdPp = null;
			    File ttdTu = null;
			    File ttdAgen = null;
			    File ttdPenutup = null;
			    File ttdReff = null;
				
			    String pathTTD = exportDirectory;
			    File fileDirTemp = new File(pathTTD);
				File[] files = fileDirTemp.listFiles();
				if (files != null){
					for (int z = 0; z < files.length; z++) {
					       if ((files[z].toString().toUpperCase().contains("_TTD_PP_")) && (files[z].toString().toUpperCase().contains(".JPG"))) {
					    	   ttdPp = files[z];
					       }else if ((files[z].toString().toUpperCase().contains("_TTD_TU_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdTu = files[z];
					       }else if ((files[z].toString().toUpperCase().contains("_TTD_AGEN_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdAgen = files[z];
					       }else if ((files[z].toString().toUpperCase().contains("_TTD_PENUTUP_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdPenutup = files[z];
					       }else if ((files[z].toString().toUpperCase().contains("_TTD_REF_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdReff = files[z];
					       }
					    }
				}
					
			    if (ttdPp==null || ttdTu == null || ttdAgen == null || ttdPenutup == null || ttdReff == null) {
					pathTTD = spajTempDirectory+"\\"+idgadget+"\\Spaj\\";
					fileDirTemp = new File(pathTTD);
					files = fileDirTemp.listFiles();
				    if (files != null){
				    	for (int z = 0; z < files.length; z++) {
				    	 	if ((ttdPp==null && files[z].toString().toUpperCase().contains("_TTD_PP_")) && (files[z].toString().toUpperCase().contains(".JPG"))) {
					    	   ttdPp = files[z];
					       }else if ((ttdTu==null && files[z].toString().toUpperCase().contains("_TTD_TU_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdTu = files[z];
					       }else if ((ttdAgen==null && files[z].toString().toUpperCase().contains("_TTD_AGEN_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdAgen = files[z];
					       }else if ((ttdPenutup==null && files[z].toString().toUpperCase().contains("_TTD_PENUTUP_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdPenutup = files[z];
					       }else if ((ttdReff==null && files[z].toString().toUpperCase().contains("_TTD_REF_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
					    	   ttdReff = files[z];
					       }
					    }
				    }
					
				    if (ttdPp==null || ttdTu == null || ttdAgen == null || ttdPenutup == null || ttdReff == null) {
				    	pathTTD = props.getProperty("pdf.dir.ftp") + "\\"
								+ cabang + "\\" + reg_spaj;
						fileDirTemp = new File(pathTTD);
						files = fileDirTemp.listFiles();
						if (files != null){
							for (int z = 0; z < files.length; z++) {
							       if ((ttdPp==null && files[z].toString().toUpperCase().contains("_TTD_PP_")) && (files[z].toString().toUpperCase().contains(".JPG"))) {
							    	   ttdPp = files[z];
							       }else if ((ttdTu==null && files[z].toString().toUpperCase().contains("_TTD_TU_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
							    	   ttdTu = files[z];
							       }else if ((ttdAgen==null && files[z].toString().toUpperCase().contains("_TTD_AGEN_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
							    	   ttdAgen = files[z];
							       }else if ((ttdPenutup==null && files[z].toString().toUpperCase().contains("_TTD_PENUTUP_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
							    	   ttdPenutup = files[z];
							       }else if ((ttdReff==null && files[z].toString().toUpperCase().contains("_TTD_REF_")) && (files[z].toString().toUpperCase().contains(".JPG"))){
							    	   ttdReff = files[z];
							       }
							    }
						}   
				    }
				}
				
				try {
					Image img = Image.getInstance(ttdPp.toString());
					img.scaleAbsolute(30, 30);
					over.addImage(img, img.getScaledWidth(), 0, 0,
							img.getScaledHeight(), 438, 643);
					over.stroke();

					if (dataTT.getMste_age() < 17 || dataPP.getLsre_id() == 1)
						ttdTu = ttdPp;
					Image img2 = Image.getInstance(ttdTu.toString());
					img2.scaleAbsolute(30, 30);
					over.addImage(img2, img2.getScaledWidth(), 0, 0,
							img2.getScaledHeight(), 438, 593);
					over.stroke();
				} catch (FileNotFoundException e) {
					logger.error("ERROR :", e);
					ServletOutputStream sos = response.getOutputStream();
					sos.println("<script>alert('TTD Pemegang Polis / Tertanggung Utama Tidak Ditemukan');window.close();</script>");
					sos.close();
				}

				over.setFontAndSize(times_new_roman, 6);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						FormatDate.toString(dataPP.getMspo_spaj_date()), 370, 715, 0);
				over.showTextAligned(PdfContentByte.ALIGN_CENTER, dataPP
						.getMcl_first().toUpperCase(), 295, 655, 0);
				over.showTextAligned(PdfContentByte.ALIGN_CENTER, dataTT
						.getMcl_first().toUpperCase(), 295, 605, 0);
				if (peserta.size() > 0) {
					Integer vertikal = 605;
					for (int i = 0; i < peserta.size(); i++) {
						vertikal = vertikal - 50;
						PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(i);
						if (pesertaPlus.getFlag_jenis_peserta() > 0){
							over.showTextAligned(PdfContentByte.ALIGN_CENTER,
									pesertaPlus.getNama().toUpperCase(), 290, vertikal,
									0);
							vertikal = vertikal + 2;
						}
						
					}
				}

				if(ttdPenutup != null){
					Image img3 = Image.getInstance(ttdPenutup.toString());
					img3.scaleAbsolute(30, 30);
					over.addImage(img3, img3.getScaledWidth(), 0, 0,
							img3.getScaledHeight(), 120, 280);
					over.stroke();
				}

				if(ttdReff != null){
					Image img4 = Image.getInstance(ttdReff.toString());
					img4.scaleAbsolute(30, 30);
					over.addImage(img4, img4.getScaledWidth(), 0, 0,
							img4.getScaledHeight(), 290, 280);
					over.stroke();	
				}
					
				if(ttdAgen != null){
					Image img5 = Image.getInstance(ttdAgen.toString());
					img5.scaleAbsolute(30, 30);
					over.addImage(img5, img5.getScaledWidth(), 0, 0,
							img5.getScaledHeight(), 440, 280);
					over.stroke();
				}
					
				over.setFontAndSize(times_new_roman, 6);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("NM_PENUTUP")) ? "-" : 
							agen.get("NM_PENUTUP").toString().toUpperCase(),100, 260,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("KD_PENUTUP")) ? "-" : 
							agen.get("KD_PENUTUP").toString().toUpperCase(),100, 250,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("CB_PENUTUP")) ? "-" : 
							agen.get("CB_PENUTUP").toString().toUpperCase(),100, 240,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("NM_REFFERAL")) ? "-" : 
							agen.get("NM_REFFERAL").toString().toUpperCase(),270, 260,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("KD_REFFERAL")) ? "-" : 
							agen.get("KD_REFFERAL").toString().toUpperCase(),270, 250,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("CB_REFFERAL")) ? "-" : 
							agen.get("CB_REFFERAL").toString().toUpperCase(),270, 240,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("NM_AGEN")) ? "-" : 
							agen.get("NM_AGEN").toString().toUpperCase(),440, 260,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("KD_AGEN")) ? "-" : 
							agen.get("KD_AGEN").toString().toUpperCase(),440, 250,0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(agen.get("CB_AGEN")) ? "-" : 
							agen.get("CB_AGEN").toString().toUpperCase(),440, 240,0);

				over.endText();

				// //---------- Data Halaman kelima ----------
				over = stamp.getOverContent(5);
				over.beginText();
				over.setFontAndSize(times_new_roman, 6);

				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP
						.getMcl_first().toUpperCase(), 250, 725, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMcl_gelar()) ? "-" : dataPP
								.getMcl_gelar(), 250, 715, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getMspe_mother(), 250, 705, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getLsne_note(), 250, 695, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMcl_green_card()) ? "TIDAK"
								: dataPP.getMcl_green_card(), 250, 685, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getLside_name(), 250, 675, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getMspe_no_identity(), 250, 665, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMspe_no_identity_expired()) ? "-"
								: FormatDate.toString(dataPP
										.getMspe_no_identity_expired()), 250, 656,
						0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						dataPP.getMspe_place_birth() + ", "
								+ FormatDate.toString(dataPP.getMspe_date_birth()),
						250, 646, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getMspo_age() + " Tahun", 250, 636, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP
						.getMspe_sex2().toUpperCase(), 250, 626, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP
						.getMspe_sts_mrt().equals("1") ? "BELUM MENIKAH" : (dataPP
						.getMspe_sts_mrt().equals("2") ? "MENIKAH" : (dataPP
						.getMspe_sts_mrt().equals("3") ? "JANDA" : "DUDA")), 250,
						617, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getLsag_name(), 250, 607, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getLsed_name(), 250, 596, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMcl_company_name()) ? "-" : dataPP
								.getMcl_company_name(), 250, 587, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getMkl_kerja(), 250, 568, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP.getKerjab(),
						250, 559, 0);
				int monyong = 0;
				String[] uraian_tugas;
				if(dataTT.getMkl_kerja_ket() != null){
					uraian_tugas = StringUtil.pecahParagraf(dataTT
							.getMkl_kerja_ket().toUpperCase(), 70);
					
						for (int i = 0; i < uraian_tugas.length; i++) {
							monyong = 7 * i;
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									uraian_tugas[i], 250, 549 - monyong, 0);
						}
						
				}
				else{
					over.showTextAligned(PdfContentByte.ALIGN_LEFT,
							"-", 250, 549 - monyong, 0);
				}
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataPP.getMkl_kerja_ket())?"-":dataPP.getMkl_kerja_ket(),
				// 250, 549, 0);
				monyong = 0;
				if(!Common.isEmpty(dataTT.getAlamat_kantor())){
					String[] alamat = StringUtil.pecahParagraf(dataTT.getAlamat_kantor().toUpperCase(), 75);
		        	if(!Common.isEmpty(alamat)){
			        	for(int i=0; i<alamat.length; i++) {
			        		monyong = 7 * i;
			        		over.showTextAligned(PdfContentByte.ALIGN_LEFT, alamat[i], 250,
									529 - monyong, 0);
			        	}
		        	}
				}
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataPP.getAlamat_kantor())?"-":dataPP.getAlamat_kantor(),
				// 250, 529, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getKota_kantor()) ? "-" : dataPP
								.getKota_kantor(), 250, 509, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getKd_pos_kantor()) ? "-" : dataPP
								.getKd_pos_kantor(), 250, 500, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getTelpon_kantor()) ? "-" : dataPP
								.getTelpon_kantor(), 250, 490, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataPP.getTelpon_kantor2())?"-":dataPP.getTelpon_kantor2(),
				// 250, 505, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(dataPP.getNo_fax()) ? "-" : dataPP.getNo_fax(),
						250, 480, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getAlamat_rumah()) ? "-" : dataPP
								.getAlamat_rumah(), 250, 470, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getKota_rumah()) ? "-" : dataPP
								.getKota_rumah(), 250, 460, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getKd_pos_rumah()) ? "-" : dataPP
								.getKd_pos_rumah(), 250, 451, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getTelpon_rumah()) ? "-" : dataPP
								.getTelpon_rumah(), 250, 441, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataPP.getTelpon_rumah2())?"-":dataPP.getTelpon_rumah2(),
				// 250, 445, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(dataPP.getNo_fax()) ? "-" : dataPP.getNo_fax(),
						250, 432, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getAlamat_tpt_tinggal()) ? "-"
								: dataPP.getAlamat_tpt_tinggal(), 250, 422, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getKota_tpt_tinggal()) ? "-" : dataPP
								.getKota_tpt_tinggal(), 250, 412, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getKd_pos_tpt_tinggal()) ? "-"
								: dataPP.getKd_pos_tpt_tinggal(), 250, 402, 0);
//				over.showTextAligned(
//						PdfContentByte.ALIGN_LEFT,
//						Common.isEmpty(dataPP.getTelpon_rumah()) ? "-" : dataPP
//								.getTelpon_rumah(), 250, 393, 0);
				 over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				 Common.isEmpty(dataPP.getTelpon_rumah2())?"-":dataPP.getTelpon_rumah2(),
						 250, 393, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(dataPP.getNo_fax()) ? "-" : dataPP.getNo_fax(),
						250, 383, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(addrBill.getMsap_address()) ? "-" : addrBill
								.getMsap_address(), 250, 373, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(addrBill.getMsap_address()) ? "-" : addrBill
								.getKota(), 250, 353, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(addrBill.getMsap_address()) ? "-" : addrBill
								.getMsap_zip_code(), 250, 343, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(addrBill.getMsap_address()) ? "-" : addrBill
								.getMsap_phone1(), 250, 334, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(addrBill.getMsap_address()) ? "-" : addrBill
								.getMsap_fax1(), 250, 323, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getNo_hp()) ? "-" : dataPP.getNo_hp(),
						250, 313, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getEmail()) ? "-" : dataPP.getEmail(),
						250, 303, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMcl_npwp()) ? "-" : dataPP
								.getMcl_npwp(), 250, 294, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMkl_penghasilan()) ? "-" : dataPP
								.getMkl_penghasilan(), 250, 285, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMkl_pendanaan()) ? "-" : dataPP
								.getMkl_pendanaan(), 250, 273, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataPP.getMkl_tujuan()) ? "-" : dataPP
								.getMkl_tujuan(), 250, 264, 0);

				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
						.getLsre_relation().toUpperCase(), 250, 234, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
						.getMcl_first().toUpperCase(), 250, 215, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMcl_gelar()) ? "-" : dataTT
								.getMcl_gelar(), 250, 206, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getMspe_mother(), 250, 196, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getLsne_note(), 250, 186, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMcl_green_card()) ? "TIDAK"
								: dataTT.getMcl_green_card(), 250, 176, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getLside_name(), 250, 166, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getMspe_no_identity(), 250, 156, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMspe_no_identity_expired()) ? "-"
								: FormatDate.toString(dataTT
										.getMspe_no_identity_expired()), 250, 146,
						0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						dataTT.getMspe_place_birth() + ", "
								+ FormatDate.toString(dataTT.getMspe_date_birth()),
						250, 137, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getMste_age() + " Tahun", 250, 127, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
						.getMspe_sex2().toUpperCase(), 250, 118, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
						.getMspe_sts_mrt().equals("1") ? "BELUM MENIKAH" : (dataTT
						.getMspe_sts_mrt().equals("2") ? "MENIKAH" : (dataTT
						.getMspe_sts_mrt().equals("3") ? "JANDA" : "DUDA")), 250,
						108, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getLsag_name(), 250, 98, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getLsed_name(), 250, 88, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMcl_company_name()) ? "-" : dataTT
								.getMcl_company_name(), 250, 78, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataTT.getMkl_kerja(), 250, 58, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT.getKerjab(),
						250, 49, 0);
				over.endText();

				//
				// //---------- Data Halaman keenam ----------
				over = stamp.getOverContent(6);
				over.beginText();
				over.setFontAndSize(times_new_roman, 6);

				monyong = 0;
				if(dataTT.getMkl_kerja_ket() != null){
					uraian_tugas = StringUtil.pecahParagraf(dataTT.getMkl_kerja_ket()
							.toUpperCase(), 70);
					for (int i = 0; i < uraian_tugas.length; i++) {
						monyong = 7 * i;
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								uraian_tugas[i], 250, 734 - monyong, 0);
					}	
				}
				
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataTT.getMkl_kerja_ket())?"-":dataTT.getMkl_kerja_ket(),
				// 250, 734, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getAlamat_kantor()) ? "-" : dataTT
								.getAlamat_kantor(), 250, 714, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getKota_kantor()) ? "-" : dataTT
								.getKota_kantor(), 250, 695, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getKd_pos_kantor()) ? "-" : dataTT
								.getKd_pos_kantor(), 250, 685, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getTelpon_kantor()) ? "-" : dataTT
								.getTelpon_kantor(), 250, 675, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataTT.getTelpon_kantor2())?"-":dataTT.getTelpon_kantor2(),
				// 153, 105, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(dataTT.getNo_fax()) ? "-" : dataTT.getNo_fax(),
						250, 665, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getAlamat_rumah()) ? "-" : dataTT
								.getAlamat_rumah(), 250, 655, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getKota_rumah()) ? "-" : dataTT
								.getKota_rumah(), 250, 645, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getKd_pos_rumah()) ? "-" : dataTT
								.getKd_pos_rumah(), 250, 635, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getTelpon_rumah()) ? "-" : dataTT
								.getTelpon_rumah(), 250, 625, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataTT.getTelpon_rumah2())?"-":dataTT.getTelpon_rumah2(),
				// 153, 46, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(dataTT.getNo_fax()) ? "-" : dataTT.getNo_fax(),
						250, 615, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getAlamat_tpt_tinggal()) ? "-"
								: dataTT.getAlamat_tpt_tinggal(), 250, 607, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getKota_tpt_tinggal()) ? "-" : dataTT
								.getKota_tpt_tinggal(), 250, 597, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getKd_pos_tpt_tinggal()) ? "-"
								: dataTT.getKd_pos_tpt_tinggal(), 250, 587, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(dataTT.getTelpon_rumah())?"-":dataTT.getTelpon_rumah(),
				// 250, 597, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getTelpon_rumah2()) ? "-" : dataTT
								.getTelpon_rumah2(), 250, 578, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(dataTT.getNo_fax()) ? "-" : dataTT.getNo_fax(),
						250, 567, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// Common.isEmpty(addrBill.getMsap_address())?"-":addrBill.getMsap_address(),
				// 208, 739, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getNo_hp()) ? "-" : dataTT.getNo_hp(),
						250, 557, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getEmail()) ? "-" : dataTT.getEmail(),
						250, 547, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMcl_npwp()) ? "-" : dataTT
								.getMcl_npwp(), 250, 537, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMkl_penghasilan()) ? "-" : dataTT
								.getMkl_penghasilan(), 250, 529, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMkl_pendanaan()) ? "-" : dataTT
								.getMkl_pendanaan(), 250, 519, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(dataTT.getMkl_tujuan()) ? "-" : dataTT
								.getMkl_tujuan(), 250, 509, 0);
				//
				// //Data Pembayar Premi
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getRelation_payor()) ? "-"
								: pembPremi.getRelation_payor(), 250, 478, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(pembPremi.getNama_pihak_ketiga()) ? "-"
						: pembPremi.getNama_pihak_ketiga(), 250, 468, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getKewarganegaraan()) ? "-"
								: pembPremi.getKewarganegaraan(), 250, 458, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getAlamat_lengkap()) ? "-"
								: pembPremi.getAlamat_lengkap(), 250, 450, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getTelp_rumah()) ? "-" : pembPremi
								.getTelp_rumah(), 250, 440, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getTelp_kantor()) ? "-"
								: pembPremi.getTelp_kantor(), 250, 430, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getEmail()) ? "-" : pembPremi
								.getEmail(), 250, 420, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getTempat_lahir()) ? "-"
								: (pembPremi.getTempat_lahir() + ", " + FormatDate.toString(pembPremi
										.getMspe_date_birth_3rd_pendirian())), 250,
						410, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getMkl_kerja()) ? "-" : pembPremi
								.getMkl_kerja(), 250, 401, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 392, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 382, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 372, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getNo_npwp()) ? "-" : pembPremi
								.getNo_npwp(), 250, 362, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getSumber_dana()) ? "-"
								: pembPremi.getSumber_dana(), 250, 352, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						Common.isEmpty(pembPremi.getTujuan_dana_3rd()) ? "-"
								: pembPremi.getTujuan_dana_3rd(), 250, 342, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 332, 0);
				//
				// //Data Usulan
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						s_channel.toUpperCase(), 250, 291, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataUsulan.getLsdbs_name(), 250, 281, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataUsulan.getLsdbs_name(), 250, 271, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataUsulan.getMspr_ins_period() + " Tahun", 250, 261, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataUsulan.getMspo_pay_period() + " Tahun", 250, 251, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
						.isEmpty(dataUsulan.getMspo_installment()) ? "-"
						: dataUsulan.getMspo_installment() + "", 250, 242, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataUsulan.getLscb_pay_mode(), 250, 232, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// dataUsulan.getLku_symbol() + " " +
				// FormatNumber.convertToTwoDigit(new
				// BigDecimal(dataUsulan.getMspr_premium())), 250, 286, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						dataUsulan.getLku_symbol()
								+ " "
								+ FormatNumber.convertToTwoDigit(new BigDecimal(
										dataUsulan.getMspr_tsi())), 250, 222, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						dataUsulan.getMste_flag_cc() == 0 ? "TUNAI" : (dataUsulan
								.getMste_flag_cc() == 2 ? "TABUNGAN"
								: "KARTU KREDIT"), 250, 211, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						FormatDate.toIndonesian(dataUsulan.getMste_beg_date()),
						250, 202, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						FormatDate.toIndonesian(dataUsulan.getMste_end_date()),
						250, 193, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// FormatDate.toIndonesian(dataUsulan.getLsdbs_number()>800?dataUsulan.getLsdbs_name():"-"),
				// 250, 237, 0);
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 221,
				// 0);

				if (dataUsulan.getDaftaRider().size() > 0) {
					Integer j = 0;
					for (int i = 0; i < dataUsulan.getDaftaRider().size(); i++) {
						Datarider rider = (Datarider) dataUsulan.getDaftaRider()
								.get(i);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								rider.getLsdbs_name(), 250, 183, 0);
						over.showTextAligned(
								PdfContentByte.ALIGN_LEFT,
								dataUsulan.getLku_symbol()
										+ " "
										+ FormatNumber
												.convertToTwoDigit(new BigDecimal(
														rider.getMspr_tsi())), 250,
														173, 0);
						j += 10;
					}
				}

				// //Data Investasi
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						dataUsulan.getLku_symbol()
								+ " "
								+ FormatNumber.convertToTwoDigit(new BigDecimal(
										dataUsulan.getMspr_premium())), 250, 105, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						d_topUpBerkala.doubleValue() == new Double(0) ? "-"
								: (dataUsulan.getLku_symbol() + " " + FormatNumber
										.convertToTwoDigit(new BigDecimal(
												d_topUpBerkala))), 250, 95, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						d_topUpTunggal.doubleValue() == new Double(0) ? "-"
								: (dataUsulan.getLku_symbol() + " " + FormatNumber
										.convertToTwoDigit(new BigDecimal(
												d_topUpTunggal))), 250, 85, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						d_premiExtra.doubleValue() == new Double(0) ? "-"
								: (dataUsulan.getLku_symbol() + " " + FormatNumber
										.convertToTwoDigit(new BigDecimal(
												d_premiExtra))), 250, 75, 0);
				over.showTextAligned(
						PdfContentByte.ALIGN_LEFT,
						dataUsulan.getLku_symbol()
								+ " "
								+ FormatNumber.convertToTwoDigit(new BigDecimal(
										d_totalPremi)), 250, 65, 0);
				Double d_jmlinves = new Double(0);
				String s_jnsinves = "";
				for (int i = 0; i < detInv.size(); i++) {
					DetilInvestasi detInves = (DetilInvestasi) detInv.get(i);
					d_jmlinves = d_jmlinves + detInves.getMdu_jumlah1();
					s_jnsinves = s_jnsinves
							+ detInves.getLji_invest1().toUpperCase() + " "
							+ detInves.getMdu_persen1() + "%";
					if (i != (detInv.size() - 1))
						s_jnsinves = s_jnsinves + ", ";
				}
				// over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				// dataUsulan.getLku_symbol() + " " +
				// FormatNumber.convertToTwoDigit(new BigDecimal(d_jmlinves)), 208,
				// 183, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, s_jnsinves, 250,
						55, 0);
				over.endText();

				// ---------- Data Halaman ketujuh----------

				over = stamp.getOverContent(7);
				over.beginText();
				over.setFontAndSize(times_new_roman, 6);

				// //Data Rekening
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						rekClient.getLsbp_nama(), 250, 724, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						rekClient.getMrc_cabang(), 250, 714, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						rekClient.getMrc_no_ac(), 250, 704, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						rekClient.getMrc_nama(), 250, 694, 0);

				if (dataTT.getMste_flag_cc() == 1 || dataTT.getMste_flag_cc() == 2) {
					if (accRecur != null) {
						String bank_pusat = "";
						String bank_cabang = "";

						if (!namaBank.isEmpty()) {
							HashMap m = (HashMap) namaBank.get(0);
							bank_pusat = (String) m.get("LSBP_NAMA");
							bank_cabang = (String) m.get("LBN_NAMA");
						}
						over.showTextAligned(
								PdfContentByte.ALIGN_LEFT,
								dataUsulan.getMste_flag_cc() == 0 ? "TUNAI"
										: (dataUsulan.getMste_flag_cc() == 2 ? "TABUNGAN"
												: "KARTU KREDIT"), 250, 631, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								Common.isEmpty(bank_pusat) ? "-"
										: bank_pusat.toUpperCase()/*
																 * accRecur.getLbn_nama
																 * ().toUpperCase()
																 */, 250, 621, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
								.isEmpty(bank_cabang) ? "-"
								: bank_cabang.toUpperCase()/*
																	 * accRecur.
																	 * getLbn_nama
																	 * ().
																	 * toUpperCase()
																	 */, 250, 611,
								0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
								.isEmpty(accRecur.getMar_acc_no()) ? "-" : accRecur
								.getMar_acc_no().toUpperCase()/*
															 * accRecur.getLbn_nama()
															 * .toUpperCase()
															 */, 250, 601, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
								.isEmpty(accRecur.getMar_holder()) ? "-" : accRecur
								.getMar_holder().toUpperCase()/*
															 * accRecur.getLbn_nama()
															 * .toUpperCase()
															 */, 250, 592, 0);
					}
				} else {
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 631,
							0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 621,
							0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 611,
							0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 601,
							0);
					over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 250, 592,
							0);
				}

				if (peserta.size() > 0) {
					Integer j = 0;
					for (int i = 0; i < peserta.size(); i++) {

						PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(i);
						if (pesertaPlus.getFlag_jenis_peserta() > 0){
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									pesertaPlus.getNama().toUpperCase(), 80, 517 - j,
									0);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									pesertaPlus.getLsre_relation(), 190, 517 - j, 0);
							over.showTextAligned(
									PdfContentByte.ALIGN_LEFT,
									pesertaPlus.getTinggi() + "/"
											+ pesertaPlus.getBerat(), 260, 517 - j, 0);
//							String[] pekerjaan = StringUtil.pecahParagraf(pesertaPlus.
//									getKerja().toUpperCase(), 15);
//								over.showTextAligned(PdfContentByte.ALIGN_LEFT,
//										pekerjaan[i], 290, 519 - j, 0);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									pesertaPlus.getKerja(), 290, 517 - j, 0);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									pesertaPlus.getSex(), 375, 517 - j, 0);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									FormatDate.toString(pesertaPlus.getTgl_lahir()),
									430, 517 - j, 0);
							over.showTextAligned(PdfContentByte.ALIGN_LEFT,
									dataPP.getLsne_note(), 480, 517 - j, 0);
							j += 10;
						}
						
					}
				}
				if (benef.size() > 0) {
					Integer j = 0;
					for (int i = 0; i < benef.size(); i++) {
						Benefeciary benefit = (Benefeciary) benef.get(i);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								benefit.getMsaw_first(), 60, 410 - j, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								benefit.getSmsaw_birth(), 200, 410 - j, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								benefit.getMsaw_sex() == 1 ? "LAKI-LAKI" : "PEREMPUAN",
								270, 410 - j, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								benefit.getMsaw_persen() + "%", 330, 410 - j, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT, benefit
								.getLsre_relation().toUpperCase(), 370, 410 - j, 0);
						over.showTextAligned(PdfContentByte.ALIGN_LEFT,
								dataPP.getLsne_note(), 480, 410 - j, 0);
						j += 10;
					}
				}
				over.endText();
				
				// ------------Halaman delapan-----------
				over = stamp.getOverContent(8);
				over.beginText();
				over.setFontAndSize(times_new_roman, 6);
				String jawab;

					 if (rslt6.size() > 0) {
							// /81, 104
							Integer j = 0;
							Integer k = 0;
							Integer l = 0;
							for (int i = 0; i < rslt6.size(); i++) {
								MstQuestionAnswer ans = (MstQuestionAnswer) rslt6.get(i);
								// j += 3;
								if (ans.getQuestion_id() > 80 && ans.getQuestion_id() < 83) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 745 - j, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 745 - j, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 765 - j, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 765 - j, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 765 - j, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 765 - j, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 765 - j, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 765 - j, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 765 - j, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 765 - j, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 765 - j, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 765 - j, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 745 - j, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 745 - j, 0);
										}
									}

								}
								j += 10;
								if (ans.getQuestion_id() == 84) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 595, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 595, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 595, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 595, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 595, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 595, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 595, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 595, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 595, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 595, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 595, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 595, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 595, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 595, 0);
										}
									}
								}
								if (ans.getQuestion_id() == 85) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 585, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 585, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 585, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 585, 0);
									}

									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 585, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 585, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 585, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 585, 0);
											}
											//--tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 585, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 585, 0);
											}
											//--tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 585, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 585, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 585, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 585, 0);
										}
									}

								}
								if (ans.getQuestion_id() == 86) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 575, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 575, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 575, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 575, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 575, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 575, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 575, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 575, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 575, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 575, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 575, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 575, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if ( ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 575, 0);
										} else if ( ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 575, 0);
										}
									}
								}
								if (ans.getQuestion_id() == 87) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 565, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 565, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 565, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 565, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 565, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 565, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 565, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 565, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 565, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 565, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 565, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 565, 0);
											}
											
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 565, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 565, 0);
										}
									}

								}
								if (ans.getQuestion_id() == 88) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 555, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 555, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 555, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 555, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 555, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 555, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 555, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 555, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 555, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 555, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 555, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 555, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 555, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 555, 0);
										}
									}
								}
								if (ans.getQuestion_id() == 89) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 545, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 545, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 545, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 545, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 545, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 545, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 545, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 545, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 545, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 545, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 545, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 545, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										 if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 545, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 545, 0);
											}
									}
								
								}
								if (ans.getQuestion_id() == 90) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 535, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 535, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 535, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 535, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 535, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 535, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 535, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 535, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 535, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 535, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 535, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 535, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 535, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 535, 0);
										}
									}
								}
								if (ans.getQuestion_id() == 91) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 525, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 525, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 525, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 525, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
										
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 525, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 525, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 525, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 525, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 525, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 525, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 525, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 525, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 525, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 525, 0);
										}
									}
								}
								if (ans.getQuestion_id() == 92) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 515, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 515, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 515, 0);
										} else if (ans.getOption_group() == 2
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 515, 0);
										}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 515, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 515, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 515, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 515, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 515, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 515, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 515, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 515, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										 if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 515, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 515, 0);
											}
									}
									

								}
								if (ans.getQuestion_id() == 93) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 425, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 505, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 505, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 505, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 505, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 505, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 505, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 505, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 505, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 505, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 505, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 505, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 505, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 505, 0);
										}
									}
									
								}
								if (ans.getQuestion_id() == 94) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 495, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 495, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 495, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 495, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 495, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 495, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 495, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 495, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 495, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 495, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 495, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 495, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 495, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 495, 0);
										}
									}
								}
								if (ans.getQuestion_id() == 95) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 485, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 485, 0);
									}// --tu
									else if (ans.getOption_group() == 2
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 485, 0);
										} else if (ans.getOption_group() == 2
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 485, 0);
										}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 485, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 485, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 485, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 485, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 485, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 485, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 485, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 485, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										 if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 485, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 485, 0);
											}
									}
								}
								
								if (ans.getQuestion_id() == 96) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 475, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 475, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 475, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 475, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 475, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 475, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 475, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 475, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 475, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 475, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 475, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 475, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 475, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 475, 0);
										}
									}
								}
										
								if (ans.getQuestion_id() == 97) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 465, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 465, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 465, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 465, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 465, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 465, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 465, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 465, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 465, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 465, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 465, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 465, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if ( ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 465, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 465, 0);
										}
									}
								}
								if (ans.getQuestion_id() == 98) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 455, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 455, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 455, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 455, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 455, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 455, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 455, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 455, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 455, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 455, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 455, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 455, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 455, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 455, 0);
										}
									}
									
								}
								if (ans.getQuestion_id() == 99) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 445, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 445, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 445, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 445, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 445, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 445, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 445, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 445, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 445, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 445, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 445, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 445, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 445, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 445, 0);
										}
									}

								}
								if (ans.getQuestion_id() == 100) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 435, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 435, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 435, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 435, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 435, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 435, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 435, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 435, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 435, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 435, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 435, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 435, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 435, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 435, 0);
										}
									}
									
								}
								if (ans.getQuestion_id() == 101) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 430, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 430, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 430, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 430, 0);
									}
									else if (ans.getOption_group() == 0
											&& ans.getOption_order() == 1) {
										jawab = ans.getAnswer2();
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												Common.isEmpty(jawab) ? "-" : jawab, 110,
												400, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 430, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 430, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 430, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 430, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 430, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 430, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 430, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 430, 0);
											}
										}
									}
									 else if (dataTT.getLsre_id()==1){
										 if ( ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 430, 0);
											} else if ( ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 430, 0);
											}
									}
								}
								
								if (ans.getQuestion_id() == 102) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 390, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 390, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 390, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 390, 0);
									}
									else if (ans.getOption_group() == 0
											&& ans.getOption_order() == 1) {
										jawab = ans.getAnswer2();
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												Common.isEmpty(jawab) ? "-" : jawab, 110,
												352, 0);
									}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 390, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 390, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 390, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 390, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 390, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 390, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 390, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 390, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										 if (ans.getOption_group() == 1
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 390, 0);
											} else if (ans.getOption_group() == 1
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 400, 390, 0);
											}
									}
									 
								}
								if (ans.getQuestion_id() == 103) {
									if ((ans.getOption_group() == 1
											|| ans.getOption_group() == 2
											|| ans.getOption_group() == 3
											|| ans.getOption_group() == 4 || ans
											.getOption_group() == 5)
											&& (ans.getOption_order() == 1 || ans
													.getOption_order() == 2)
											&& ans.getAnswer2() == null) {
										jawab = null;
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 100, 700, 0);
										// --pp
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 342, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 370, 342, 0);
									}
									// --tu
									else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 1
											&& ans.getAnswer2().equals("1")) {
										jawab = "Ya";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 342, 0);
									} else if (ans.getOption_group() == 2
											&& ans.getOption_order() == 2
											&& ans.getAnswer2().equals("1")) {
										jawab = "Tidak";
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												jawab, 400, 342, 0);
									}
									 else if (ans.getOption_group() == 0
												&& ans.getOption_order() == 1) {
											jawab = ans.getAnswer2();
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													Common.isEmpty(jawab) ? "-" : jawab, 110,
													282, 0);
										}
									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											
											// --tt1
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 342, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 425, 342, 0);
											}
											// --tt2
											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 342, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 463, 342, 0);
											}
											// --tt3
											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 342, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 493, 342, 0);
											}
											// --tt4
											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1
													&& ans.getAnswer2().equals("1")) {
												jawab = "Ya";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 342, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2
													&& ans.getAnswer2().equals("1")) {
												jawab = "Tidak";
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														jawab, 540, 342, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1
												&& ans.getAnswer2().equals("1")) {
											jawab = "Ya";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 342, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2
												&& ans.getAnswer2().equals("1")) {
											jawab = "Tidak";
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													jawab, 400, 342, 0);
										}
									}
									
								}
								if (ans.getQuestion_id() == 104) {
									if (ans.getOption_group() == 1
											&& ans.getOption_order() == 1) {
										jawab = ans.getAnswer2();
										over.setFontAndSize(italic, 6);
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												Common.isEmpty(jawab) ? "-" : jawab, 370,
												272, 0);
									} else if (ans.getOption_group() == 1
											&& ans.getOption_order() == 2) {
										jawab = ans.getAnswer2();
										over.setFontAndSize(italic, 6);
										over.showTextAligned(PdfContentByte.ALIGN_LEFT,
												Common.isEmpty(jawab) ? "-" : jawab, 370,
												262, 0);
									}
									else if (ans.getOption_group() == 2
												&& ans.getOption_order() == 1) {
											jawab = ans.getAnswer2();
											over.setFontAndSize(italic, 6);
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													Common.isEmpty(jawab) ? "-" : jawab, 400,
													272, 0);
									} else if (ans.getOption_group() == 2
												&& ans.getOption_order() == 2) {
											jawab = ans.getAnswer2();
											over.setFontAndSize(italic, 6);
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													Common.isEmpty(jawab) ? "-" : jawab, 400,
													262, 0);
									}

									if (peserta.size() > 0 && ans.getAnswer2() != null) {
										for (int x = 0; x < peserta.size(); x++) {
											PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(x);
											 if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 1) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 425,
														272, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==1 && ans.getOption_group() == 3
													&& ans.getOption_order() == 2) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 425,
														262, 0);
											}

											else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 1) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 463,
														272, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==2 && ans.getOption_group() == 4
													&& ans.getOption_order() == 2) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 463,
														262, 0);
											}

											else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 1) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 493,
														272, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==3 && ans.getOption_group() == 5
													&& ans.getOption_order() == 2) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 493,
														262, 0);
											}

											else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 1) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 540,
														272, 0);
											} else if (pesertaPlus.getFlag_jenis_peserta()==4 && ans.getOption_group() == 6
													&& ans.getOption_order() == 2) {
												jawab = ans.getAnswer2();
												over.setFontAndSize(italic, 6);
												over.showTextAligned(PdfContentByte.ALIGN_LEFT,
														Common.isEmpty(jawab) ? "-" : jawab, 540,
														262, 0);
											}
										}
									}else if (dataTT.getLsre_id()==1){
										if (ans.getOption_group() == 1
												&& ans.getOption_order() == 1) {
											jawab = ans.getAnswer2();
											over.setFontAndSize(italic, 6);
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													Common.isEmpty(jawab) ? "-" : jawab, 400,
													272, 0);
										} else if (ans.getOption_group() == 1
												&& ans.getOption_order() == 2) {
											jawab = ans.getAnswer2();
											over.setFontAndSize(italic, 6);
											over.showTextAligned(PdfContentByte.ALIGN_LEFT,
													Common.isEmpty(jawab) ? "-" : jawab, 400,
													262, 0);
										}
									}
								}
							}
						}	
				
				over.endText();
				stamp.close();
				
			return null;	
	}
	
	public ModelAndView espajonlinegadgetexisting(HttpServletRequest request,
			HttpServletResponse response, ElionsManager elionsManager,
			UwManager uwManager, BacManager bacManager, Properties props,
			Products products, String spaj) throws Exception {
		String reg_spaj = spaj;
		Integer type = null;
		Integer question;
		Date sysdate = elionsManager.selectSysdate();
		List<String> pdfs = new ArrayList<String>();
		Boolean suksesMerge = false;
		HashMap<String, Object> cmd = new HashMap<String, Object>();
		ArrayList data_answer = new ArrayList();
		Integer index = null;
		Integer index2 = null;
		String spaj_gadget = "";
		String mspo_flag_spaj = bacManager.selectMspoFLagSpaj(reg_spaj);
		String cabang = elionsManager.selectCabangFromSpaj(reg_spaj);
		String exportDirectory = props.getProperty("pdf.dir.export") + "\\"
				+ cabang + "\\" + reg_spaj;
		System.out.print(mspo_flag_spaj);
		String dir = props.getProperty("pdf.template.espajonlinegadget");
		OutputStream output;
		PdfReader reader;
		File userDir = new File(props.getProperty("pdf.dir.export") + "\\"
				+ cabang + "\\" + reg_spaj);
		if (!userDir.exists()) {
			userDir.mkdirs();
		}

		HashMap moreInfo = new HashMap();
		moreInfo.put("Author", "PT ASURANSI JIWA SINARMAS MSIG Tbk.");
		moreInfo.put("Title", "GADGET");
		moreInfo.put("Subject", "E-SPAJ ONLINE");

		PdfContentByte over;
		PdfContentByte over2;
		BaseFont times_new_roman = BaseFont.createFont(
				"C:\\WINDOWS\\FONTS\\ARIAL.TTF", BaseFont.CP1252,
				BaseFont.NOT_EMBEDDED);
		BaseFont italic = BaseFont.createFont("C:\\WINDOWS\\FONTS\\ariali.ttf",
				BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
		
		 // --------------spaj template existing
		reader = new PdfReader(
				props.getProperty("pdf.template.espajonlinegadget")
						+ "\\spajonlinegadget.pdf");
		output = new FileOutputStream(exportDirectory + "\\"
				+ "espajonlinegadget_" + reg_spaj + ".pdf");

		spaj_gadget = dir + "\\spajonlinegadget.pdf";
		pdfs.add(spaj_gadget);
		suksesMerge = MergePDF.concatPDFs(pdfs, output, false);
		String outputName = props.getProperty("pdf.dir.export") + "\\"
				+ cabang + "\\" + reg_spaj + "\\" + "espajonlinegadget_"
				+ reg_spaj + ".pdf";
		PdfStamper stamp = new PdfStamper(reader, new FileOutputStream(
				outputName));

		Pemegang dataPP = elionsManager.selectpp(reg_spaj);
		Tertanggung dataTT = elionsManager.selectttg(reg_spaj);
		PembayarPremi pembPremi = bacManager.selectP_premi(reg_spaj);
		if (pembPremi == null)
			pembPremi = new PembayarPremi();
		AddressBilling addrBill = this.elionsManager
				.selectAddressBilling(reg_spaj);
		Datausulan dataUsulan = this.elionsManager
				.selectDataUsulanutama(reg_spaj);
		dataUsulan.setDaftaRider(elionsManager
				.selectDataUsulan_rider(reg_spaj));
		InvestasiUtama inv = this.elionsManager
				.selectinvestasiutama(reg_spaj);
		Rekening_client rekClient = this.elionsManager
				.select_rek_client(reg_spaj);
		Account_recur accRecur = this.elionsManager
				.select_account_recur(reg_spaj);
		List detInv = this.bacManager.selectdetilinvestasi2(reg_spaj);
		List benef = this.elionsManager.select_benef(reg_spaj);
		List peserta = this.uwManager.select_all_mst_peserta(reg_spaj);
		List dist = this.elionsManager.select_tipeproduk();
		List listSpajTemp = bacManager.selectReferensiTempSpaj(reg_spaj);
		HashMap spajTemp = (HashMap) listSpajTemp.get(0);
		String idgadget = (String) spajTemp.get("NO_TEMP");
		String submitGadget = (String) spajTemp.get("TGL_UPLOAD");
		Map agen = this.bacManager.selectAgenESPAJSimasPrima(reg_spaj);

		String s_channel = "";
		for (int i = 0; i < dist.size(); i++) {
			HashMap dist2 = (HashMap) dist.get(i);
			Integer i_lstp_id = (Integer) dist2.get("lstp_id");
			if (i_lstp_id.intValue() == dataUsulan.getTipeproduk()
					.intValue()) {
				s_channel = (String) dist2.get("lstp_produk");
			}
		}

		Double d_premiRider = 0.;
		if (dataUsulan.getDaftaRider().size() > 0) {
			for (int i = 0; i < dataUsulan.getDaftaRider().size(); i++) {
				Datarider rider = (Datarider) dataUsulan.getDaftaRider()
						.get(i);
				d_premiRider = rider.getMspr_premium();
			}
		}
		Double d_topUpBerkala = new Double(0);
		Double d_topUpTunggal = new Double(0);
		Double d_totalTopup = new Double(0);
		if (inv != null) {
			DetilTopUp daftarTopup = inv.getDaftartopup();
			d_topUpBerkala = Common.isEmpty(daftarTopup.getPremi_berkala()) ? new Double(
					0) : daftarTopup.getPremi_berkala();
			d_topUpTunggal = Common.isEmpty(daftarTopup.getPremi_tunggal()) ? new Double(
					0) : daftarTopup.getPremi_tunggal();
			d_totalTopup = d_topUpBerkala + d_topUpTunggal;
		}
		Double d_premiExtra = (Common.isEmpty(uwManager
				.selectSumPremiExtra(reg_spaj)) ? 0. : uwManager
				.selectSumPremiExtra(reg_spaj));
		Double d_totalPremi = dataUsulan.getMspr_premium() + d_totalTopup
				+ d_premiRider + d_premiExtra;

		stamp.setMoreInfo(moreInfo);

		// ---------- Data Halaman Pertama ----------
		over = stamp.getOverContent(1);
		over.beginText();
		over.setFontAndSize(times_new_roman, 8);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				FormatString.nomorSPAJ(reg_spaj), 370, 706, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				FormatDate.toString(sysdate), 80, 693, 0);

		over.setFontAndSize(times_new_roman, 6);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP
				.getMcl_first().toUpperCase(), 200, 601, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
				.getMcl_first().toUpperCase(), 200, 588, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataUsulan.getLsdbs_depkeu(), 200, 575, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				dataUsulan.getLku_symbol()
						+ " "
						+ FormatNumber.convertToTwoDigit(new BigDecimal(
								dataUsulan.getMspr_tsi())), 200, 562, 0);

		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				dataUsulan.getLku_symbol()
						+ " "
						+ FormatNumber.convertToTwoDigit(new BigDecimal(
								dataUsulan.getMspr_premium())), 280, 551, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				d_totalTopup.doubleValue() == new Double(0) ? "-"
						: (dataUsulan.getLku_symbol() + " " + FormatNumber
								.convertToTwoDigit(new BigDecimal(
										d_totalTopup))), 280, 541, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				d_premiRider.doubleValue() == new Double(0) ? "-"
						: (dataUsulan.getLku_symbol() + " " + FormatNumber
								.convertToTwoDigit(new BigDecimal(
										d_premiRider))), 280, 531, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				dataUsulan.getLku_symbol()
						+ " "
						+ FormatNumber.convertToTwoDigit(new BigDecimal(
								d_totalPremi)), 280, 521, 0);

		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				agen.get("NM_PENUTUP").toString().toUpperCase(), 200, 492,
				0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				agen.get("KD_PENUTUP").toString(), 200, 479, 0);

		over.endText();

		// ---------- Data Halaman Kedua ----------
		over = stamp.getOverContent(2);
		over.beginText();

		// String ttdPp = exportDirectory + "\\" + idgadget + "_TTD_PP_" +
		// (dataPP.getMcl_first().toUpperCase()).replace(" ", "") + ".jpg";
		String ttdPp = exportDirectory + "\\" + idgadget + "_TTD_PP_"
				+ ".jpg";
		try {
			Image img = Image.getInstance(ttdPp);
			img.scaleAbsolute(40, 40);
			over.addImage(img, img.getScaledWidth(), 0, 0,
					img.getScaledHeight(), 458, 705);
			over.stroke();

			// String ttdTu = exportDirectory + "\\" + idgadget + "_TTD_TU_"
			// + (dataTT.getMcl_first().toUpperCase()).replace(" ", "") +
			// ".jpg";
			String ttdTu = exportDirectory + "\\" + idgadget + "_TTD_TU_"
					+ ".jpg";
			if (dataTT.getMste_age() < 17 || dataPP.getLsre_id() == 1)
				ttdTu = ttdPp;
			Image img2 = Image.getInstance(ttdTu);
			img2.scaleAbsolute(40, 40);
			over.addImage(img2, img2.getScaledWidth(), 0, 0,
					img2.getScaledHeight(), 458, 658);
			over.stroke();
		} catch (FileNotFoundException e) {
			logger.error("ERROR :", e);
			ServletOutputStream sos = response.getOutputStream();
			sos.println("<script>alert('TTD Pemegang Polis / Tertanggung Utama Tidak Ditemukan');window.close();</script>");
			sos.close();
		}

		over.setFontAndSize(times_new_roman, 8);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				FormatDate.toString(sysdate), 280, 790, 0);
		over.showTextAligned(PdfContentByte.ALIGN_CENTER, dataPP
				.getMcl_first().toUpperCase(), 300, 723, 0);
		over.showTextAligned(PdfContentByte.ALIGN_CENTER, dataTT
				.getMcl_first().toUpperCase(), 300, 676, 0);
		if (peserta.size() > 0) {
			Integer vertikal = 676;
			for (int i = 0; i < peserta.size(); i++) {
				vertikal = vertikal - 47;
				PesertaPlus pesertaPlus = (PesertaPlus) peserta.get(i);
				over.showTextAligned(PdfContentByte.ALIGN_CENTER,
						pesertaPlus.getNama().toUpperCase(), 300, vertikal,
						0);
				vertikal = vertikal + 2;
			}
		}

		try {
			// String ttdAgen = exportDirectory + "\\" + idgadget +
			// "_TTD_AGEN_" +
			// (agen.get("NM_PENUTUP").toString().toUpperCase()).replace(" ",
			// "") + ".jpg";
			String ttdAgen = exportDirectory + "\\" + idgadget
					+ "_TTD_AGEN_" + ".jpg";
			Image img3 = Image.getInstance(ttdAgen);
			img3.scaleAbsolute(40, 40);
			over.addImage(img3, img3.getScaledWidth(), 0, 0,
					img3.getScaledHeight(), 100, 420);
			over.stroke();
		} catch (FileNotFoundException e) {
			logger.error("ERROR :", e);
			ServletOutputStream sos = response.getOutputStream();
			sos.println("<script>alert('TTD Agen Penutup Tidak Ditemukan');window.close();</script>");
			sos.close();
		}

		try {
			// String ttdReff = exportDirectory + "\\" + idgadget +
			// "_TTD_REF_" +
			// (agen.get("NM_REFFERAL").toString().toUpperCase()).replace(" ",
			// "") + ".jpg";
			String ttdReff = exportDirectory + "\\" + idgadget
					+ "_TTD_REF_" + ".jpg";
			Image img4 = Image.getInstance(ttdReff);
			img4.scaleAbsolute(40, 40);
			over.addImage(img4, img4.getScaledWidth(), 0, 0,
					img4.getScaledHeight(), 280, 420);
			over.stroke();
		} catch (FileNotFoundException e) {
			logger.error("ERROR :", e);
			ServletOutputStream sos = response.getOutputStream();
			sos.println("<script>alert('TTD Agen Refferal Tidak Ditemukan');window.close();</script>");
			sos.close();
		}

		over.setFontAndSize(times_new_roman, 6);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				agen.get("NM_PENUTUP").toString().toUpperCase(), 110, 415,
				0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 61, 405, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				agen.get("KD_PENUTUP").toString(), 81, 395, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				agen.get("NM_REFFERAL").toString().toUpperCase(), 290, 415,
				0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				agen.get("KD_REFFERAL").toString(), 261, 405, 0);
		over.endText();

		// ---------- Data Halaman Ketiga ----------
		over = stamp.getOverContent(3);
		over.beginText();
		over.setFontAndSize(times_new_roman, 6);
		String flag_spaj = "";
		if (dataPP.getMspo_flag_spaj() == 0) {
			flag_spaj = "SPAJ OLD";
		} else if (dataPP.getMspo_flag_spaj() == 1) {
			flag_spaj = "SPAJ NEW VA";
		} else if (dataPP.getMspo_flag_spaj() == 2) {
			flag_spaj = "SPAJ NEW VA & PEMBAYAR PREMI";
		} else if (dataPP.getMspo_flag_spaj() == 3) {
			flag_spaj = "SPAJ FULL (MARET 2015)";
		} else if (dataPP.getMspo_flag_spaj() == 4) {
			flag_spaj = "SPAJ SIO (MARET 2015)";
		} else {
			flag_spaj = "SPAJ GIO (MARET 2015)";
		}
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, flag_spaj, 190,
				788, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				s_channel.toUpperCase(), 190, 778, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataUsulan.getLsdbs_depkeu(), 190, 768, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataUsulan.getLsdbs_name(), 190, 758, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP
				.getMcl_first().toUpperCase(), 190, 748, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getMcl_gelar()) ? "-" : dataPP
						.getMcl_gelar(), 190, 739, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataPP.getMspe_mother(), 190, 730, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataPP.getLsne_note(), 190, 720, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getMcl_green_card()) ? "TIDAK"
						: dataPP.getMcl_green_card(), 190, 710, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataPP.getLside_name(), 190, 700, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataPP.getMspe_no_identity(), 190, 690, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getMspe_no_identity_expired()) ? "-"
						: FormatDate.toString(dataPP
								.getMspe_no_identity_expired()), 190, 680,
				0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				dataPP.getMspe_place_birth() + ", "
						+ FormatDate.toString(dataPP.getMspe_date_birth()),
				190, 671, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataPP.getMspo_age() + " Tahun", 190, 661, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP
				.getMspe_sex2().toUpperCase(), 190, 651, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP
				.getMspe_sts_mrt().equals("1") ? "BELUM MENIKAH" : (dataPP
				.getMspe_sts_mrt().equals("2") ? "MENIKAH" : (dataPP
				.getMspe_sts_mrt().equals("3") ? "JANDA" : "DUDA")), 190,
				642, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataPP.getLsag_name(), 190, 632, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataPP.getLsed_name(), 190, 622, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getMcl_company_name()) ? "-" : dataPP
						.getMcl_company_name(), 246, 613, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataPP.getMkl_kerja(), 190, 603, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP.getKerjab(),
				198, 593, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getMkl_kerja_ket()) ? "-" : dataPP
						.getMkl_kerja_ket(), 190, 583, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getAlamat_kantor()) ? "-" : dataPP
						.getAlamat_kantor(), 250, 573, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getKota_kantor()) ? "-" : dataPP
						.getKota_kantor(), 153, 563, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getKd_pos_kantor()) ? "-" : dataPP
						.getKd_pos_kantor(), 153, 553, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getTelpon_kantor()) ? "-" : dataPP
						.getTelpon_kantor(), 153, 544, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getTelpon_kantor2()) ? "-" : dataPP
						.getTelpon_kantor2(), 153, 535, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
				.isEmpty(dataPP.getNo_fax()) ? "-" : dataPP.getNo_fax(),
				153, 525, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getAlamat_rumah()) ? "-" : dataPP
						.getAlamat_rumah(), 153, 515, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getKota_rumah()) ? "-" : dataPP
						.getKota_rumah(), 153, 505, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getKd_pos_rumah()) ? "-" : dataPP
						.getKd_pos_rumah(), 153, 495, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getTelpon_rumah()) ? "-" : dataPP
						.getTelpon_rumah(), 153, 486, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getTelpon_rumah2()) ? "-" : dataPP
						.getTelpon_rumah2(), 153, 476, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getAlamat_tpt_tinggal()) ? "-"
						: dataPP.getAlamat_tpt_tinggal(), 177, 466, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getKota_tpt_tinggal()) ? "-" : dataPP
						.getKota_tpt_tinggal(), 153, 456, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getKd_pos_tpt_tinggal()) ? "-"
						: dataPP.getKd_pos_tpt_tinggal(), 153, 446, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getTelpon_rumah()) ? "-" : dataPP
						.getTelpon_rumah(), 153, 437, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getTelpon_rumah2()) ? "-" : dataPP
						.getTelpon_rumah2(), 153, 427, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
				.isEmpty(dataPP.getNo_fax()) ? "-" : dataPP.getNo_fax(),
				153, 417, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(addrBill.getMsap_address()) ? "-" : addrBill
						.getMsap_address(), 208, 407, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getNo_hp()) ? "-" : dataPP.getNo_hp(),
				153, 397, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getEmail()) ? "-" : dataPP.getEmail(),
				153, 387, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getMcl_npwp()) ? "-" : dataPP
						.getMcl_npwp(), 153, 378, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getMkl_penghasilan()) ? "-" : dataPP
						.getMkl_penghasilan(), 226, 368, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getMkl_pendanaan()) ? "-" : dataPP
						.getMkl_pendanaan(), 153, 359, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataPP.getMkl_tujuan()) ? "-" : dataPP
						.getMkl_tujuan(), 190, 349, 0);

		over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
				.getMcl_first().toUpperCase(), 190, 319, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getMcl_gelar()) ? "-" : dataTT
						.getMcl_gelar(), 190, 309, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataTT.getMspe_mother(), 190, 300, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataTT.getLsne_note(), 190, 290, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getMcl_green_card()) ? "TIDAK"
						: dataTT.getMcl_green_card(), 190, 281, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataTT.getLside_name(), 190, 271, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataTT.getMspe_no_identity(), 190, 261, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getMspe_no_identity_expired()) ? "-"
						: FormatDate.toString(dataTT
								.getMspe_no_identity_expired()), 190, 251,
				0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				dataTT.getMspe_place_birth() + ", "
						+ FormatDate.toString(dataTT.getMspe_date_birth()),
				190, 241, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataTT.getMste_age() + " Tahun", 190, 231, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
				.getMspe_sex2().toUpperCase(), 190, 221, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT
				.getMspe_sts_mrt().equals("1") ? "BELUM MENIKAH" : (dataTT
				.getMspe_sts_mrt().equals("2") ? "MENIKAH" : (dataTT
				.getMspe_sts_mrt().equals("3") ? "JANDA" : "DUDA")), 190,
				212, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataTT.getLsag_name(), 190, 202, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataTT.getLsed_name(), 190, 192, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getMcl_company_name()) ? "-" : dataTT
						.getMcl_company_name(), 246, 183, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataTT.getMkl_kerja(), 190, 173, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT.getKerjab(),
				198, 163, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getMkl_kerja_ket()) ? "-" : dataTT
						.getMkl_kerja_ket(), 190, 154, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getAlamat_kantor()) ? "-" : dataTT
						.getAlamat_kantor(), 250, 144, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getKota_kantor()) ? "-" : dataTT
						.getKota_kantor(), 153, 134, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getKd_pos_kantor()) ? "-" : dataTT
						.getKd_pos_kantor(), 153, 124, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getTelpon_kantor()) ? "-" : dataTT
						.getTelpon_kantor(), 153, 115, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getTelpon_kantor2()) ? "-" : dataTT
						.getTelpon_kantor2(), 153, 105, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
				.isEmpty(dataTT.getNo_fax()) ? "-" : dataTT.getNo_fax(),
				153, 95, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getAlamat_rumah()) ? "-" : dataTT
						.getAlamat_rumah(), 153, 85, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getKota_rumah()) ? "-" : dataTT
						.getKota_rumah(), 153, 75, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getKd_pos_rumah()) ? "-" : dataTT
						.getKd_pos_rumah(), 153, 65, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getTelpon_rumah()) ? "-" : dataTT
						.getTelpon_rumah(), 153, 56, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getTelpon_rumah2()) ? "-" : dataTT
						.getTelpon_rumah2(), 153, 46, 0);
		over.endText();

		// ---------- Data Halaman Keempat ----------
		over = stamp.getOverContent(4);
		over.beginText();
		over.setFontAndSize(times_new_roman, 6);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getAlamat_tpt_tinggal()) ? "-"
						: dataTT.getAlamat_tpt_tinggal(), 177, 798, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getKota_tpt_tinggal()) ? "-" : dataTT
						.getKota_tpt_tinggal(), 153, 788, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getKd_pos_tpt_tinggal()) ? "-"
						: dataTT.getKd_pos_tpt_tinggal(), 153, 779, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getTelpon_rumah()) ? "-" : dataTT
						.getTelpon_rumah(), 153, 769, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getTelpon_rumah2()) ? "-" : dataTT
						.getTelpon_rumah2(), 153, 759, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
				.isEmpty(dataTT.getNo_fax()) ? "-" : dataTT.getNo_fax(),
				153, 749, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(addrBill.getMsap_address()) ? "-" : addrBill
						.getMsap_address(), 208, 739, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getNo_hp()) ? "-" : dataTT.getNo_hp(),
				153, 729, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getEmail()) ? "-" : dataTT.getEmail(),
				153, 719, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getMcl_npwp()) ? "-" : dataTT
						.getMcl_npwp(), 153, 710, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getMkl_penghasilan()) ? "-" : dataTT
						.getMkl_penghasilan(), 226, 700, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getMkl_pendanaan()) ? "-" : dataTT
						.getMkl_pendanaan(), 153, 690, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(dataTT.getMkl_tujuan()) ? "-" : dataTT
						.getMkl_tujuan(), 190, 681, 0);

		// Data Pembayar Premi
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, Common
				.isEmpty(pembPremi.getNama_pihak_ketiga()) ? "-"
				: pembPremi.getNama_pihak_ketiga(), 190, 652, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(pembPremi.getKewarganegaraan()) ? "-"
						: pembPremi.getKewarganegaraan(), 190, 642, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(pembPremi.getAlamat_lengkap()) ? "-"
						: pembPremi.getAlamat_lengkap(), 190, 632, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(pembPremi.getTelp_rumah()) ? "-" : pembPremi
						.getTelp_rumah(), 190, 622, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(pembPremi.getTelp_kantor()) ? "-"
						: pembPremi.getTelp_kantor(), 190, 613, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(pembPremi.getEmail()) ? "-" : pembPremi
						.getEmail(), 190, 603, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(pembPremi.getTempat_lahir()) ? "-"
						: (pembPremi.getTempat_lahir() + ", " + FormatDate.toString(pembPremi
								.getMspe_date_birth_3rd_pendirian())), 190,
				593, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(pembPremi.getMkl_kerja()) ? "-" : pembPremi
						.getMkl_kerja(), 190, 584, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 190, 574, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 190, 564, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 190, 554, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(pembPremi.getNo_npwp()) ? "-" : pembPremi
						.getNo_npwp(), 190, 544, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(pembPremi.getLsre_relation()) ? "-"
						: pembPremi.getLsre_relation(), 215, 535, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(pembPremi.getSumber_dana()) ? "-"
						: pembPremi.getSumber_dana(), 190, 525, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				Common.isEmpty(pembPremi.getTujuan_dana_3rd()) ? "-"
						: pembPremi.getTujuan_dana_3rd(), 190, 515, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 190, 505, 0);

		// Data Usulan
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				s_channel.toUpperCase(), 190, 476, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataUsulan.getLsdbs_name(), 190, 466, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataUsulan.getLsdbs_name(), 190, 456, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataUsulan.getMspr_ins_period() + " Tahun", 190, 446, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				dataUsulan.getLscb_pay_mode(), 190, 437, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				dataUsulan.getLku_symbol()
						+ " "
						+ FormatNumber.convertToTwoDigit(new BigDecimal(
								dataUsulan.getMspr_premium())), 190, 427, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				dataUsulan.getLku_symbol()
						+ " "
						+ FormatNumber.convertToTwoDigit(new BigDecimal(
								dataUsulan.getMspr_tsi())), 190, 417, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				dataUsulan.getMste_flag_cc() == 0 ? "TUNAI" : (dataUsulan
						.getMste_flag_cc() == 2 ? "TABUNGAN"
						: "KARTU KREDIT"), 190, 407, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				FormatDate.toIndonesian(dataUsulan.getMste_beg_date()),
				190, 397, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				FormatDate.toIndonesian(dataUsulan.getMste_end_date()),
				190, 387, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 190, 378, 0);

		// Data Investasi
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				dataUsulan.getLku_symbol()
						+ " "
						+ FormatNumber.convertToTwoDigit(new BigDecimal(
								dataUsulan.getMspr_premium())), 190, 349, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				d_topUpBerkala.doubleValue() == new Double(0) ? "-"
						: (dataUsulan.getLku_symbol() + " " + FormatNumber
								.convertToTwoDigit(new BigDecimal(
										d_topUpBerkala))), 190, 339, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				d_topUpTunggal.doubleValue() == new Double(0) ? "-"
						: (dataUsulan.getLku_symbol() + " " + FormatNumber
								.convertToTwoDigit(new BigDecimal(
										d_topUpTunggal))), 190, 329, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				d_premiExtra.doubleValue() == new Double(0) ? "-"
						: (dataUsulan.getLku_symbol() + " " + FormatNumber
								.convertToTwoDigit(new BigDecimal(
										d_premiExtra))), 190, 319, 0);
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				dataUsulan.getLku_symbol()
						+ " "
						+ FormatNumber.convertToTwoDigit(new BigDecimal(
								d_totalPremi)), 190, 309, 0);

		// Data Rekening
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				dataUsulan.getMste_flag_cc() == 0 ? "TUNAI" : (dataUsulan
						.getMste_flag_cc() == 2 ? "TABUNGAN"
						: "KARTU KREDIT"), 190, 280, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				rekClient.getLsbp_nama(), 190, 270, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				rekClient.getMrc_cabang(), 190, 260, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 190, 252, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 190, 242, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				rekClient.getMrc_no_ac(), 190, 232, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				rekClient.getMrc_nama(), 190, 222, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, rekClient
				.getKuasa().toUpperCase(), 190, 212, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT,
				FormatDate.toIndonesian(rekClient.getTgl_surat()), 190,
				202, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, "-", 190, 193, 0);

		Double d_jmlinves = new Double(0);
		String s_jnsinves = "";
		for (int i = 0; i < detInv.size(); i++) {
			DetilInvestasi detInves = (DetilInvestasi) detInv.get(i);
			d_jmlinves = d_jmlinves + detInves.getMdu_jumlah1();
			s_jnsinves = s_jnsinves
					+ detInves.getLji_invest1().toUpperCase() + " "
					+ detInves.getMdu_persen1() + "%";
			if (i != (detInv.size() - 1))
				s_jnsinves = s_jnsinves + ", ";
		}
		over.showTextAligned(
				PdfContentByte.ALIGN_LEFT,
				dataUsulan.getLku_symbol()
						+ " "
						+ FormatNumber.convertToTwoDigit(new BigDecimal(
								d_jmlinves)), 208, 183, 0);
		over.showTextAligned(PdfContentByte.ALIGN_LEFT, s_jnsinves, 190,
				173, 0);

		if (benef.size() > 0) {
			Integer j = 0;
			for (int i = 0; i < benef.size(); i++) {
				Benefeciary benefit = (Benefeciary) benef.get(i);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						benefit.getMsaw_first(), 60, 123 - j, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						benefit.getSmsaw_birth(), 208, 123 - j, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						benefit.getMsaw_sex() == 1 ? "PRIA" : "WANITA",
						255, 123 - j, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT,
						dataPP.getLsne_note(), 310, 123 - j, 0);
				over.showTextAligned(PdfContentByte.ALIGN_LEFT, benefit
						.getLsre_relation().toUpperCase(), 400, 123 - j, 0);
				j += 10;
			}
		}
		over.endText();

		stamp.close();

		return null;
	}
			
		    private Boolean deleteAllFile(File pFile){
		        if(pFile.exists()){
		            if(pFile.isDirectory()){
		                if(pFile.list().length > 0){
		                    String[] strFiles = pFile.list();
		                    for(String strFileName : strFiles){
		                        if(strFileName.contains("path") || strFileName.contains("TP.pdf")){
		                             File fileToDelete = new File(pFile, strFileName);
		                             deleteAllFile(fileToDelete);
		                        }
		                    }
		                }
		            }
		        }
		        return pFile.delete();
		    }			

		    //20181030 Mark Valentino : copy dari class 'PDFViewer.java'
		    //Jika ada perubahan di 'PDFViewer.java' mohon diupdate jg disini.
			public static Map genItextTemplate1(ElionsManager elionsManager, BacManager bacManager, Properties props, String fileName, boolean sertificateFlag, int benefitType, String spaj, String lsbs_id, String lsdb_number) throws DocumentException, IOException {
				HashMap m = new HashMap();
				String reg_spaj = spaj;
				String lsbs = lsbs_id;
				String detbisnis = lsdb_number;
				
				String cabang = elionsManager.selectCabangFromSpaj(reg_spaj);
				String wakil = bacManager.selectWakilFromSpaj(reg_spaj);
				//String exportDirectory = props.getProperty("pdf.dir.export")+"\\"+cabang+"\\"+reg_spaj;
				
				File userDir = new File(props.getProperty("pdf.dir.export.temp")+"\\"+reg_spaj);
		        if(!userDir.exists()) {
		            userDir.mkdirs();
		        }
		        
				
				PdfContentByte over;
				BaseFont fonts = BaseFont.createFont("C:\\WINDOWS\\FONTS\\ARIAL.TTF", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
//				Font font = FontFactory.getFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED, 0.8f, Font.NORMAL, BaseColor.BLACK);
				
				if(StringUtil.isEmpty(fileName)) {
					fileName = "SERTIFIKAT.pdf";
				}
				
				String pathFile = props.getProperty("pdf.dir.syaratpolis")+"\\"+lsbs+"-"+FormatString.rpad("0", String.valueOf(detbisnis), 3)+"-NT.pdf";
//		        PdfReader reader = new PdfReader(props.getProperty("pdf.dir")+"\\ss_smile\\"+lsbs+"-"+FormatString.rpad("0", detbisnis, 3)+".pdf");
		        PdfReader reader = new PdfReader(pathFile);
		        int pages = reader.getNumberOfPages();
		        String hal = "1-" + (pages-1);
				
		        reader.selectPages(hal);
		        
//		        String outputName = props.getProperty("pdf.dir.export")+"\\"+cabang+"\\"+reg_spaj+"\\"+fileName;
		        String outputName = props.getProperty("pdf.dir.export.temp")+"\\"+reg_spaj+"\\"+fileName;		        
		        PdfStamper stamp = new PdfStamper(reader,new FileOutputStream(outputName));
		        
		        Pemegang dataPP = elionsManager.selectpp(reg_spaj);
		        Tertanggung dataTT = elionsManager.selectttg(reg_spaj);
		        Datausulan dataUsulan = elionsManager.selectDataUsulanutama(reg_spaj);
		        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		        
		        String alamat_1 = "";
		        String alamat_2 = "";
		        String no_sertifikat = "";
		        try {
		        	if(sertificateFlag) {
				        no_sertifikat = bacManager.selectNoSertifikat(reg_spaj);
				        
				        if("".equals(no_sertifikat) || null == (no_sertifikat)){
				        	String lsbsId_3dg = FormatString.rpad("0", lsbs.toString(), 3);
				        	no_sertifikat = bacManager.selectSertifikatTemp(cabang, wakil, lsbsId_3dg);
				        	
				        	HashMap param = new HashMap();
				        	param.put("name_pp", dataPP.getMcl_first());
				        	param.put("bod_pp", sdf.format(dataPP.getMspe_date_birth()));
				        	param.put("age_pp", dataPP.getMspo_age());
				        	param.put("begdate", sdf.format(dataUsulan.getMste_beg_date()));
				        	param.put("insperiod", dataUsulan.getMspo_ins_period());
				        	param.put("address", dataPP.getKota_rumah());
				        	param.put("name_tt", dataTT.getMcl_first());
				        	param.put("bod_tt", sdf.format(dataTT.getMspe_date_birth()));
				        	param.put("age_tt", dataTT.getMste_age());
				        	param.put("lsbs_id", lsbs);
				        	param.put("lsdbs_number", detbisnis);
				        	param.put("premi", dataUsulan.getMspr_premium());
				        	param.put("up", dataUsulan.getMspr_tsi());
				        	param.put("msag_id", dataPP.getMsag_id());
				        	param.put("no_policy", no_sertifikat);
				        	param.put("reg_spaj", reg_spaj);
				        	
				        	bacManager.insertMstSpajCrt(param);
				        }
			        }else {
			        	no_sertifikat = elionsManager.selectPolicyNumberFromSpaj(reg_spaj);
			        }
		        	
		        	JasperScriptlet jasper = new JasperScriptlet();
		        	
		            if (dataPP.getAlamat_rumah().length() > 46) {
		                alamat_1 = dataPP.getAlamat_rumah().substring(0, 45);
		                alamat_2 = dataPP.getAlamat_rumah().substring(46, dataPP.getAlamat_rumah().length());
		            } else {
		                alamat_1 = dataPP.getAlamat_rumah();
		            }
					over = stamp.getOverContent(1);
					over.beginText();
					over.setFontAndSize(fonts,8f);
					
					String tglbdate = sdf.format(dataUsulan.getMste_beg_date());
					String tgledate = sdf.format(dataUsulan.getMste_end_date());
					
		            over.showTextAligned(PdfContentByte.ALIGN_LEFT, no_sertifikat, 115, 744, 0);
		            over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP.getMcl_first().toString(), 115, 730, 0);
		            over.showTextAligned(PdfContentByte.ALIGN_LEFT, sdf.format(dataPP.getMspe_date_birth())  + " / " + dataPP.getMspo_age() + " Tahun", 115, 715, 0);
		            over.showTextAligned(PdfContentByte.ALIGN_LEFT, tglbdate, 115, 700, 0);
		            over.showTextAligned(PdfContentByte.ALIGN_LEFT, tglbdate + " s/d " + tgledate, 115, 683, 0);
		            over.showTextAligned(PdfContentByte.ALIGN_LEFT, alamat_1, 115, 670, 0);
		            if (alamat_2.equals("")) {
		            	if(dataPP.getKd_pos_rumah()==null || "".equals(dataPP.getKd_pos_rumah()) ){
		            		over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP.getKota_rumah(), 115, 660, 0);
		            	}else{
		            		over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP.getKota_rumah() + ", " + dataPP.getKd_pos_rumah(), 115, 660, 0);
		            	}
		            } else {
		            	if(dataPP.getKd_pos_rumah()==null || "".equals(dataPP.getKd_pos_rumah()) ){
		            		over.showTextAligned(PdfContentByte.ALIGN_LEFT, alamat_2, 115, 660, 0);
			            	over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP.getKota_rumah(), 115, 650, 0);
		            	}else{
			            	over.showTextAligned(PdfContentByte.ALIGN_LEFT, alamat_2, 115, 660, 0);
			            	over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataPP.getKota_rumah() + ", " + dataPP.getKd_pos_rumah(), 115, 650, 0);
		            	}

		            }

		            over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataTT.getMcl_first(), 398, 744, 0);
		            over.showTextAligned(PdfContentByte.ALIGN_LEFT, sdf.format(dataTT.getMspe_date_birth())  + " / " + dataTT.getMste_age() + " Tahun", 398, 730, 0);
		            over.showTextAligned(PdfContentByte.ALIGN_LEFT, dataUsulan.getLsdbs_depkeu(), 398, 715, 0);
		            
		            String cbyr = "";
		            int faktor = 1;
		            if(dataUsulan.getLscb_id()==1){
		            	cbyr = "triwulan";
		            	faktor = 3;
		            }else if(dataUsulan.getLscb_id()==2){
		            	cbyr = "semester";
		            	faktor = 6;
		            }else if(dataUsulan.getLscb_id()==3){
		            	cbyr = "tahun";
		            	faktor = 12;
		            }else if(dataUsulan.getLscb_id()==6){
		            	cbyr = "bulan";
		            }
		            
		            over.showTextAligned(PdfContentByte.ALIGN_LEFT, jasper.formatCurrency("Rp ",BigDecimal.valueOf(dataUsulan.getMspr_premium())) + ",- per "+cbyr, 398, 700, 0);
		            if(benefitType == 0) {
		            	over.showTextAligned(PdfContentByte.ALIGN_LEFT, jasper.formatCurrency("Rp ",BigDecimal.valueOf(dataUsulan.getMspr_tsi())), 398, 685, 0);
		            }else {
		            	over.showTextAligned(PdfContentByte.ALIGN_LEFT, "Dibayarkan sekaligus " + jasper.formatCurrency("Rp ",BigDecimal.valueOf(dataUsulan.getMspr_tsi())) + "; Atau", 398, 685, 0);
		                over.showTextAligned(PdfContentByte.ALIGN_LEFT, "Dibayarkan bulanan " + jasper.formatCurrency("Rp ",BigDecimal.valueOf((dataUsulan.getMspr_premium()*10)/faktor)) + " selama 5 tahun", 398, 673, 0);
		            }
		//
		            over.showTextAligned(PdfContentByte.ALIGN_LEFT, tglbdate, 433, 659, 0);
		            
				    String ttd = Resources.getResourceURL(props.getProperty("images.ttd.direksi")).getPath();
				    Image img = Image.getInstance(ttd);					
					img.setAbsolutePosition(380, 300);		
//					img.scaleAbsolute(90, 34);
					img.scalePercent(27);
					if("223".equals(lsbs) &&  "001".equals(detbisnis)){
						over.addImage(img,img.getScaledWidth(), 0, 0, img.getScaledHeight(), 400, 616);
					}else{
						over.addImage(img,img.getScaledWidth(), 0, 0, img.getScaledHeight(), 400, 624);
					}
		            
		            over.endText();
		            stamp.close();
		            reader.close();

		        } catch (Exception e) {
		            e.printStackTrace();
					FileUtils.deleteFile(props.getProperty("pdf.dir.export")+"\\"+cabang+"\\"+reg_spaj,fileName);
		        } 
		        
		        return m;
			}
			
			private Map printPolisExclude(ServletContext servletContext, String spaj, HttpServletRequest request, int singleDuplexQuadruplex, ElionsManager elionsManager, UwManager uwManager, BacManager bacManager, Properties props, Products products) {
				Map params = new HashMap();
				User currentUser = (User) request.getSession().getAttribute("currentUser");
				params.put("spaj", spaj);
				params.put("props", props);
				String validasiMeterai = uwManager.validasiBeaMeterai(currentUser.getJn_bank());
				if(validasiMeterai != null){
					params.put("meterai", null);
					params.put("izin", "");
				}else{
					params.put("meterai", "Rp. 6.000,-");
					params.put("izin", elionsManager.selectIzinMeteraiTerakhir());
				}
				
				//jenis print ulang
				int jpu = ServletRequestUtils.getIntParameter(request, "jpu", -1);
				
				if(jpu == 1) { //PRINT ULANG POLIS
					params.put("tipePolis", "O R I G I N A L");
				}else if(jpu == 2) { //PRINT DUPLIKAT POLIS
					String seq = ServletRequestUtils.getStringParameter(request, "seq", "");
					params.put("tipePolis", "D U P L I C A T E " + "("+seq+")");
				}else if(jpu == 3) { //PRINT ULANG POLIS
					params.put("tipePolis", "O R I G I N A L");
				}
				params.put("ingrid", props.get("images.ttd.direksi")); //ttd dr. ingrid (Yusuf - 04/05/2006)
				
				String kategori = products.kategoriPrintPolis(spaj);
//				String policyNumber = "09208201800001";
//				 	   policyNumber = uwManager.getUwDao().selectNoPolisFromSpaj(spaj);
//				PrintPolisPrintingController ppc = new PrintPolisPrintingController();
//				params.put("logoQr", ppc.getQrBasedOnPolicy(servletContext,props,policyNumber));
//				params.put("Print", "cetak");
				
				//String kategori = "guthrie";
				logger.info("JENIS POLIS : " + kategori);	
				if(PrintPolisMultiController.POLIS == singleDuplexQuadruplex) { //single
					params.put("reportPath", "/WEB-INF/classes/" + props.getProperty("report.polis."+kategori));
				}else if(PrintPolisMultiController.POLIS_DUPLEX == singleDuplexQuadruplex) { //duplex(cetak BOLAK-BALIK)
					params.put("pathPolis", props.getProperty("report.polis."+kategori)+".jasper");
					params.put("reportPath", "/WEB-INF/classes/" + props.getProperty("report.polis.duplex"));
				}else if(PrintPolisMultiController.POLIS_QUADRUPLEX == singleDuplexQuadruplex) { //quadruplex
					params.put("pathPolis", props.getProperty("report.polis."+kategori)+".jasper");
					params.put("reportPath", "/WEB-INF/classes/" + props.getProperty("report.polis.quadruplex"));
				}else if(PrintPolisMultiController.POLIS_QUADRUPLEX_PLUS_HADIAH == singleDuplexQuadruplex) { //quadruplex
					params.put("pathPolis", props.getProperty("report.polis."+kategori)+".jasper");
					params.put("reportPath", "/WEB-INF/classes/" + props.getProperty("report.polis.quadruplex_plus_hadiah"));
				}else if(PrintPolisMultiController.POLIS_QUADRUPLEX_MEDICAL_PLUS== singleDuplexQuadruplex) { //quadruplex				
					params.put("pathPolis", props.getProperty("report.polis."+kategori+".medplus")+".jasper");
					params.put("reportPath", "/WEB-INF/classes/" + props.getProperty("report.polis.quadruplex_medical_plus"));
				}
				return params;
			}
			
			//Menggunakan class EmailPool
			public String emailPaKonven(HttpServletRequest request,HttpServletResponse response, ElionsManager elionsManager, UwManager uwManager, Properties props, String spaj) throws Exception {
				
				Date tanggal = elionsManager.selectSysdateSimple();
				//String sysdate = defaultDateFormat.format(new Date());
				Map params = new HashMap();
				User currentUser=(User)request.getSession().getAttribute("currentUser");
				//String spaj = ServletRequestUtils.getStringParameter(request, "spaj", "");
				if (spaj.equals(null)){
					spaj = request.getAttribute("spaj").toString();
				}
				//Integer show= ServletRequestUtils.getIntParameter(request, "show",0);
				//String flag= ServletRequestUtils.getStringParameter(request, "flag","");
				Pemegang pmg = elionsManager.selectpp(spaj);
				List<File> attachments = new ArrayList<File>();
				List<String> error = new ArrayList<String>();
				String hasilEmail = "";
				
				//Uploader
				CommandUploadBac uploader = new CommandUploadBac();
				ServletRequestDataBinder binder = new ServletRequestDataBinder(uploader);
				binder.bind(request);
				uploader.setErrorMessages(new ArrayList<String>());				
				
				//Cari File
				String msp_id = uwManager.getUwDao().selectMspIdFromSpaj(spaj);
				String businessId = uwManager.selectBusinessId(spaj);
				String product_sub_code = uwManager.selectLsdbsNumber(spaj);
				List<Pas> pas = new ArrayList<Pas>();
				//pas = uwManager.selectAllPasList(msp_id, null, null, null, null, "pabsm", null, "pabsm", null, null, null);
				pas = uwManager.getUwDao().selectPasBySpaj(spaj, null);
				Pas p = pas.get(0);
				
				HashMap<String, Object> kartu = uwManager.selectDetailKartuPas(p.getNo_kartu());
				String no_polis_induk = kartu.get("NO_POLIS_INDUK").toString();
				//String outputName = pdfPolisPath + "\\bsm\\73\\" + no_polis_induk + "-" + kode_plan + "-" + no_sertifikat + ".pdf";
				String fileName = no_polis_induk + "-" + "073" + "-" + p.getNo_kartu() + ".pdf";		
				File sourceFile = new File("\\\\ebserver\\pdfind\\Polis\\bsm\\73\\" + fileName);
//				if(sourceFile.exists()){
//					attachments.add(sourceFile);
//				}else{
					try {
						String nama_plan = kartu.get("NAMA_PLAN").toString();
						String product_code = "73";						
						product_sub_code = "073";
						ITextPdf.generateSertifikatPaBsmV2(p.getNo_kartu(), no_polis_induk, product_code, product_sub_code, nama_plan, p.getMsp_up(), p.getMsp_premi(), elionsManager);
					}catch(Exception e){
						
						FileInputStream in = null;
						ServletOutputStream ouputStream = null;
						in = new FileInputStream(fileName);
						ouputStream = response.getOutputStream();
						if(ouputStream != null) {
							in.close();
							ouputStream.flush();
						    ouputStream.close();							
						}							
//						PdfReader pdfReader = new PdfReader("123");						
//						PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileOutputStream(sourceFile));						
						
						logger.error("ERROR :", e);
						hasilEmail = "Error pada saat generate file Sertifikat.";
						return hasilEmail;						
					}					
//				}
//				if(!currentUser.getLde_id().equals("11")){
//				    params.put("main", "true");
//			     }

				if(!sourceFile.exists()){
					error.add("Data SOFTCOPY Kosong, Pengiriman GAGAL");
				}else{
					attachments.add(sourceFile);
					try{
						EmailPool.send("SMiLe E-Lions", 1, 1, 0, 0,
								null, 0, Integer.parseInt(currentUser.getLus_id()), new Date(), null, true,
								props.getProperty("admin.ajsjava"),
								new String[]{pmg.getMspe_email()},
								//cc
								(currentUser.getEmail().trim().equals("")? null : currentUser.getEmail().replaceAll(" ", "").split(";")),						
								//bcc
								//new String[]{"mark.valentino@sinarmasmsiglife.co.id;titis@sinarmasmsiglife.co.id"},
								null,
								"Softcopy Sertifikat Polis Asuransi Personal Accident atas nama " + pmg.getMcl_first() + " no. " + pmg.getMspo_policy_no(),
								"Kepada Yth." + "\n" +
								"Bapak/Ibu " + pmg.getMcl_first() + "\n" +
								"di tempat" + 
								"\n" +
								"\n" +
								"Nasabah Terhormat," + "\n" +
								"Selamat, Anda telah terdaftar sebagai peserta Personal Accident Risiko ABD dari Sinarmas MSIG Life dengan manfaat :" + 
								"\n" +
								"    1.    Manfaat Asuransi risiko meninggal dunia akibat Kecelakaan/Risiko A." + "\n" +
								"    2.    Manfaat Asuransi risiko cacat tetap total atau sebagian akibat Kecelakaan/Risiko B." + "\n" +								
								"    3.    Manfaat Asuransi risiko biaya pengobatan dan perawatan di Rumah Sakit akibat Kecelakaan/Risiko D." +
								"\n" + 
								"\n" +								
								"Terlampir adalah sertifikat polis sebagai panduan Anda dalam memahami ketentuan produk secara ringkas." + "\n" +
								"\n" +						
								"Terima kasih" + 
								"\n" +
								"\n" +							
								"Salam Hangat," + "\n" +
								"Sinarmas MSIG Life",
								attachments,
								spaj);
						error.add("Kirim Sertifikat Ke Nasabah (Email) Berhasil!");
						hasilEmail = error.toString();				
					}catch(Exception e){
						logger.error("ERROR :", e);
						hasilEmail = "Proses Pengiriman Email Gagal.";
						return hasilEmail;
					}
					
				if(!error.equals("")){
					params.put("pesanError", error);
				}
				params.put("pmg", pmg);
			}	
				return hasilEmail;
		}
	
	//Mark Valentino 20180906 - MENU : GENERATE_ALL
	public ModelAndView printall2(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return generatePolisAllPelengkap(request, response, elionsManager, uwManager, bacManager, props, products);
		//return null;
	}
	
	public String emailSoftcopySpaj(HttpServletRequest request,HttpServletResponse response, ElionsManager elionsManager, UwManager uwManager, BacManager bacManager, String reg_spaj, Products products, Properties props, User user, String jenis) throws Exception {
		//Iga 25 Maret 2020 Update wording email
		PrintPolisMultiController ppmc = new PrintPolisMultiController();
		String hasilEmailSoftcopy = "";
		String lsbs = "";
		String lsdbs = "";
		String duaDigit = "";
		String spaj = "";
		User currentUser = new User();
		try{
		 currentUser = (User) request.getSession().getAttribute("currentUser");
		if(currentUser == null){
			currentUser = new User();
			currentUser.setLus_id("0");
		}
		}catch(NullPointerException e){
			currentUser = new User();
			currentUser.setLus_id("0");
		}
		Date tanggal = elionsManager.selectSysdateSimple();
		Map params = new HashMap();
		if((jenis.equals("scheduler")) || (jenis.equals("hit")) || (jenis.equals("softcopy"))){
			spaj = reg_spaj;
		}else if(jenis.equals("manual") || jenis.equals("regenerate")){
			spaj = request.getParameter("spaj");	
		}		
		Pemegang pmg = elionsManager.selectpp(spaj);		
		
		//ambil data spaj yang akan diprint
		List dataSoftcopy = uwManager.getUwDao().selectPendingPrintPolisSpaj(spaj);	
		String noPolis = (String) ((Map) dataSoftcopy.get(0)).get("MSPO_POLICY_NO");		
		List detBisnis = elionsManager.selectDetailBisnis(spaj);
		lsbs = (String) ((Map) detBisnis.get(0)).get("BISNIS");
		lsdbs = (String) ((Map) detBisnis.get(0)).get("DETBISNIS");
		duaDigit = StringUtils.substring(spaj, 0, 2);							
		boolean isDmtm = duaDigit.equals("40");
		// SIMPOL
		String namaProduk = (String) ((Map) detBisnis.get(0)).get("LSDBS_NAME");
		if(lsbs.trim().equals("120") && (lsdbs.trim().equals("022") || lsdbs.trim().equals("023") || lsdbs.trim().equals("024"))){
			namaProduk = "SIMAS POWER LINK";
		}else{
			namaProduk = (String) ((Map) detBisnis.get(0)).get("LSDBS_NAME");
		}
		
		List<File> attachments = new ArrayList<File>();
		List<String> error = new ArrayList<String>();
		
		//Uploader
		CommandUploadBac uploader = new CommandUploadBac();
		ServletRequestDataBinder binder = new ServletRequestDataBinder(uploader);
		//binder.bind(request);
		uploader.setErrorMessages(new ArrayList<String>());				
		
		//Cari File
		String msp_id = uwManager.getUwDao().selectMspIdFromSpaj(spaj);
		String businessId = uwManager.selectBusinessId(spaj);
		String product_sub_code = uwManager.selectLsdbsNumber(spaj);

		String path = "";
		String pathTemp = "";
		String cabang = elionsManager.selectCabangFromSpaj(spaj);
		path = props.getProperty("pdf.dir.export") + "\\" + cabang + "\\" + spaj +"\\";					
		File sourceFile = new File(props.getProperty("pdf.dir.ftp")+"\\"+cabang+"\\"+spaj+"\\"+spaj+".zip");

		List<DropDown> fileList = new ArrayList<DropDown>();
		fileList.add(new DropDown(path+"pathSurat.pdf", "Surat Ucapan Terima Kasih", "ALL"));
		fileList.add(new DropDown(path+"pathPolis.pdf", "Cover Polis", "ALL"));
		fileList.add(new DropDown(path+"pathManfaat.pdf", "Manfaat Polis", "ALL"));
		fileList.add(new DropDown(path+"pathAlokasiDana.pdf", "Alokasi Dana", "ALL"));
		fileList.add(new DropDown(path+"pathSS.pdf", "Syarat-syarat Polis", "ALL"));
		File fileDir = new File(path);
		File[] files = fileDir.listFiles(); 
		
		if (!isDmtm){
			File fileSpaj = null;
			for (int i = 0; i < files.length; i++) {
			       if ((files[i].toString().toUpperCase().contains("ESPAJONLINEGADGET")) || (files[i].toString().toUpperCase().contains("ESPAJDMTM"))) {
			    	   fileSpaj = files[i];
			       }
			    }
		    if (fileSpaj == null){
			    int jumlahfileSpaj = 0;				    	
			    for (int i = 0; i < files.length; i++) {
				       if ((files[i].toString().toUpperCase().contains("SPAJ"))) {
				    	   fileSpaj = files[i];
				    	   jumlahfileSpaj++;
				       }
			    }
			    if ((fileSpaj != null) && (jumlahfileSpaj > 1)){
			    	   for (int j = 0; j < files.length; j++){
			    		   if (files[j].toString().toUpperCase().contains("SPAJ ")){
				    		   if (fileSpaj.lastModified() < files[j].lastModified()){
				    			   fileSpaj = files[j];	   
				    		   }   
			    		   }
			    	   }
			    }			    
		    }	    
		    if (fileSpaj!=null) if(fileSpaj.exists()){
		    	fileList.add(new DropDown(path+fileSpaj.getName(), "SPAJ", "NONE"));
		    }
		    
			File fileProposal = null;
			    for (int i = 0; i < files.length; i++) {
			       if ((files[i].toString().toUpperCase().contains("PROPOSAL"))) {
			    	   fileProposal = files[i];
			    	   if (fileProposal!=null) if(fileProposal.exists()){
			    		   fileList.add(new DropDown(path+fileProposal.getName(), "PROPOSAL", "NONE"));	    		   
			    	   }
			       }
			    }
		}
		
		if (products.syariah(lsbs, lsdbs)){
			PdfUtils.addTableOfContentsSyariah(fileList, props, path);
		}
		else{
			PdfUtils.addTableOfContents(fileList, props, path);
		}
		
		String userPassword = elionsManager.selectTanggalLahirPemegangPolis(spaj);
		File softcopy = PdfUtils.combinePdfWithOutline(fileList, path, "softcopy.pdf", props, userPassword);
		if (products.syariah(lsbs, lsdbs)){
			softcopy = PdfUtils.addBarcodeAndLogoSyariah(fileList, softcopy, props, userPassword);
		}else{
			softcopy = PdfUtils.addBarcodeAndLogo(fileList, softcopy, props, userPassword);
		}
		
		if(softcopy.exists()){
			attachments.add(softcopy);
//			attachments.add(new File(path + "\\" + softcopy));
		}
		
		Map info = uwManager.selectInformasiEmailSoftcopy(spaj);
		
		//penambahan telemarketing@banksinarmas.com di TO email (dmtm) - (101623)
		if(isDmtm){
			if ((String) info.get("MSPE_EMAIL")==null){
				uploader.setEmailto("telemarketing@banksinarmas.com");
			}else{
				uploader.setEmailto("telemarketing@banksinarmas.com;"+(String) info.get("MSPE_EMAIL"));
			}
		}else{
			uploader.setEmailto((String) info.get("MSPE_EMAIL"));
		}
		
		uploader.setEmailsubject("Softcopy Polis " + namaProduk + " atas nama " + info.get("GELAR") + " " + info.get("MCL_FIRST") + " no. " + info.get("MSPO_POLICY_NO"));
		StringBuffer pesan = new StringBuffer();

		pesan.append("Kepada Yth.\n");
		pesan.append(info.get("GELAR") + " " + info.get("MCL_FIRST")+"\n");
		pesan.append("di tempat\n\n");
		pesan.append("Dengan Hormat,\n");
		pesan.append("Bersama ini kami kirimkan softcopy Polis " + namaProduk + " untuk Nomor Polis. " + info.get("MSPO_POLICY_NO")+"\n");
		pesan.append("dengan password : tanggal lahir format dd-mm-yyyy \n");
		pesan.append("Informasi lebih lanjut dapat disampaikan ke : " + "cs@sinarmasmsiglife.co.id");
		uploader.setEmailmessage(pesan.toString().replaceAll("\n", "</br>"));
		
		Map infoAgen = elionsManager.selectEmailAgen(spaj);
		String emailAgen = (String) infoAgen.get("MSPE_EMAIL");
		if(emailAgen!=null) {
			if(!emailAgen.trim().equals("")) {
				if(emailAgen.toLowerCase().matches("^.+@[^\\.].*\\.[a-z]{2,}$")) {
					uploader.setEmailcc(emailAgen);
				}
			}
		}
		
		//kalo agen gak punya email, dikirim ke branch admin nya
		if(uploader.getEmailcc()==null) {
			String emailCabang = uwManager.selectEmailCabangFromSpaj(spaj);
			if(emailCabang!=null) {
				if(!emailCabang.trim().equals("")) {
					if(emailCabang.toLowerCase().matches("^.+@[^\\.].*\\.[a-z]{2,}$")) {
						uploader.setEmailcc(emailCabang);
					}
				}
			}
		}
		
		//Req Sari Sutini via Helpdesk 27539 - khusus Mall, email CC ditambahkan ke inge, ningrum, apriyani, dan sutini.
		if(elionsManager.selectLcaIdBySpaj(spaj).equals("58")){
			uploader.setEmailcc(uploader.getEmailcc()+";apriyani@sinarmasmsiglife.co.id;sutini@sinarmasmsiglife.co.id");
		}
		//Tambahan khusus DMTM (Req Sari Sutini)
		else if(elionsManager.selectLcaIdBySpaj(spaj).equals("40")){
			if(lsbs.equals("163")){
				uploader.setEmailcc(uploader.getEmailcc()+";Maria_P@sinarmasmsiglife.co.id;Lylianty@sinarmasmsiglife.co.id;paul.a.tarigan@banksinarmas.com;tri.w.utami@banksinarmas.com");
			}else{
				uploader.setEmailcc(uploader.getEmailcc()+";Maria_P@sinarmasmsiglife.co.id;Lylianty@sinarmasmsiglife.co.id");
			}
		}
		//Tambahan khusus SIAP2U Agency (37 M1 05)
		else if( "37M105".equals(elionsManager.selectCabangFromSpaj_lar(spaj)) ){
			if((lsbs.equals("190") && lsdbs.equals("009")) || (lsbs.equals("200") && lsdbs.equals("007"))){
				uploader.setEmailcc(uploader.getEmailcc()+";lyna@smileultimate.co.id");
				uploader.setEmailto(uploader.getEmailto()+";polis.siap2u@gmail.com");
			}
		}
		
		//ERBE (Helpdesk 137909)
		if(elionsManager.selectLcaIdBySpaj(spaj).equals("73")){
			uploader.setEmailcc("spaj.msig@erbecorp.com;efesty@erbecorp.com");
		}
		
			try{

				HashMap mapEmail = uwManager.getUwDao().selectMstConfig(16, "RDS", "SOFTCOPY_EMAIL");
				String[] bcc = mapEmail.get("NAME")!=null?mapEmail.get("NAME").toString().split(";"):null;
				EmailPool.send("SMiLe E-Lions", 1, 1, 0, 0, 
						null, 0, 0, new Date(), null, true,
						"policy_service@sinarmasmsiglife.co.id", 
						uploader.getEmailto().replaceAll(" ", "").split(";"), 
						(uploader.getEmailcc().trim().equals("")? null : uploader.getEmailcc().replaceAll(" ", "").split(";")), 
						bcc,
						uploader.getEmailsubject(), uploader.getEmailmessage(), attachments, spaj);
//						attachments.add(sourceFile);
			
//			EmailPool.send("SMiLe E-Lions", 1, 1, 0, 0,
//					null, 0, Integer.parseInt(user.getLus_id()), new Date(), null, true,
//					props.getProperty("admin.ajsjava"),
//					new String[]{pmg.getMspe_email()},
//					//cc
//					cc,
//					//(user.getEmail().trim().equals("")? null : user.getEmail().replaceAll(" ", "").split(";")),
//					//bcc
//					new String[]{"iga.ukiarwan@sinarmasmsiglife.co.id;tities@sinarmasmsiglife.co.id"},
//					//null,
//					//Judul Email
//					"Softcopy Polis " + namaProduk + " atas nama " + pmg.getMcl_first() + " no. " + pmg.getMspo_policy_no(),
//					//Body Email
//					"Kepada Yth." + "\n" +
//					"Bapak/Ibu " + pmg.getMcl_first() + "\n" +
//					"di tempat" + 
//					"\n" +
//					"\n" +
//					"Dengan Hormat," + "\n" +
//					"Bersama ini kami kirimkan softcopy " + namaProduk + " untuk Nomor Polis. " + pmg.getMspo_policy_no() +
//					//" dengan ketentuan user login: " + pmg.getMcl_first() + " password : tanggal lahir format : dd-mm-yyyy" +
//					"\n" +
//					"\n" +										
//					"Email ini terkirim secara otomatis oleh sistem, harap jangan mereply email ini. Informasi lebih lanjut dapat disampaikan ke : " + "cs@sinarmasmsiglife.co.id",							
////										"Salam Hangat," + "\n" +
////										"Sinarmas MSIG Life",
//					attachments,
//					spaj);
			
			elionsManager.insertMstPositionSpaj(currentUser.getLus_id(), "KIRIM SOFTCOPY POLIS (E-LIONS) KE "+uploader.getEmailto(), spaj,0);
			hasilEmailSoftcopy = "Softcopy Terkirim ('" + pmg.getMspe_email() + "')";			
			}catch(Exception e){
				logger.error("ERROR :", e);
				//bacManager.insertMstSchedulerHist(InetAddress.getLocalHost().getHostName(), msh_name, new Date(), new Date(), "ERROR", "Error generate softcopy SPAJ " + spaj + " : " + e.toString());
				return "Terjadi kesalahan saat email Softcopy.";							
			}
//		}
		return hasilEmailSoftcopy;
	}	
	
	public static boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.find();
	}	
}			