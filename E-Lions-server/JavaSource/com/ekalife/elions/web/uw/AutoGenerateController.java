package com.ekalife.elions.web.uw;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import com.ekalife.elions.dao.SchedulerDao;
import com.ekalife.elions.model.AksesHist;
import com.ekalife.elions.model.CekValidPrintPolis;
import com.ekalife.elions.model.Command;
import com.ekalife.elions.model.CommandUploadBac;
import com.ekalife.elions.model.Datausulan;
import com.ekalife.elions.model.Endors;
import com.ekalife.elions.model.MstInboxHist;
import com.ekalife.elions.model.Pemegang;
import com.ekalife.elions.model.Policy;
import com.ekalife.elions.model.Position;
import com.ekalife.elions.model.Product;
import com.ekalife.elions.model.Simcard;
import com.ekalife.elions.model.Tertanggung;
import com.ekalife.elions.model.User;
import com.ekalife.elions.service.BacManager;
import com.ekalife.elions.service.ElionsManager;
import com.ekalife.elions.service.UwManager;
import com.ekalife.elions.web.uw.support.WordingPdfViewer;
import com.ekalife.utils.CekPelengkap;
import com.ekalife.utils.Common;
import com.ekalife.utils.EmailPool;
import com.ekalife.utils.EncryptUtils;
import com.ekalife.utils.FileUtils;
import com.ekalife.utils.FormatDate;
import com.ekalife.utils.FormatString;
import com.ekalife.utils.MergePDF;
import com.ekalife.utils.PdfUtils;
import com.ekalife.utils.Print;
import com.ekalife.utils.Products;
import com.ekalife.utils.StringUtil;
import com.ekalife.utils.jasper.JasperScriptlet;
import com.ekalife.utils.jasper.JasperUtils;
import com.ekalife.utils.parent.ParentMultiController;
import com.ekalife.utils.view.PDFViewer;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

import id.co.sinarmaslife.std.model.vo.DropDown;
import id.co.sinarmaslife.std.spring.util.Email;

import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ekalife.elions.model.Datausulan;
import com.ekalife.elions.model.Simcard;
import com.ekalife.elions.service.BacManager;
import com.ekalife.elions.service.ElionsManager;
import com.ekalife.elions.service.UwManager;
import com.ekalife.elions.web.uw.PrintPolisAllPelengkap;
import com.ekalife.utils.CekPelengkap;
import com.ekalife.utils.Products;
import id.co.sinarmaslife.std.spring.util.Email;
import id.co.sinarmaslife.std.util.FileUtil;

import org.apache.commons.lang.StringUtils;
import org.apache.pdfbox.util.PDFMergerUtility;

import com.ekalife.utils.StringUtil;
import com.google.gson.Gson;

/**
 * 
 * @author Mark Valentino Panjaitan
 * AutoGenerate is for generate All File Polis and Merge PDF
 * First commit ke CVS : 20191030 test
 */
@SuppressWarnings("unchecked")
public class AutoGenerateController extends ParentMultiController{
	protected final Log logger = LogFactory.getLog( getClass() );
	DateFormat df2 = new SimpleDateFormat("yyyyMMdd");
	
	protected PrintPolisAllPelengkap ppap;
	protected ElionsManager elionsManager;
	protected UwManager uwManager;
	protected BacManager bacManager;
	protected Email email;
	protected NumberFormat numberFormat;
	protected Properties props;
	protected String jdbcName;	
	protected Products products;
	private SchedulerDao schedulerDao;	
	public PrintPolisAllPelengkap getPpap() {
		return ppap;
	}

	public void setPpap(PrintPolisAllPelengkap ppap) {
		this.ppap = ppap;
	}
	
	public AutoGenerateController (ServletContext servletContext){
		this.setServletContext(servletContext);
	}
	
	public AutoGenerateController (){
		
	}	
	
	public List generateOutsource(HttpServletRequest request, HttpServletResponse response, UwManager uwManager, BacManager bacManager, ElionsManager elionsManager, Properties props, Products products, String jenis)  throws Exception {
		
	User user = new User();
	user.setLus_id("0");
	if(jenis.equals("manual") || jenis.equals("regenerate")){
		user = (User) request.getSession().getAttribute("currentUser");		
	}else if(jenis.equals("scheduler") || jenis.equals("hit")){
		
		user.setJn_bank(0);
		user.setEmail("ajsjava@sinarmasmsiglife.co.id");
		user.setLus_id("1");					
	}
	
	if(user == null){
		 user  = new User();
		 user.setLus_id("0");
		 
	}

	ppap = new PrintPolisAllPelengkap(this.getServletContext());

	String resultGenerateOutsource = "";
	List listResultAll = new ArrayList();		
	String spaj = "";
	String desc = "OK";
	String err = "";	
	int jumlahSuksesGenerate = 0;
	int jumlahError = 0;
	int jumlahExclude = 0;
	int jumlahEmail = 0;			
	Date bdate 	= new Date();
	
	//HOST INFORMATION
	String computerName = InetAddress.getLocalHost().getHostName().toString().trim().toUpperCase();	
	InetAddress ip;
	String ipAddress = "";
	try {
		ip = InetAddress.getLocalHost();
		ipAddress = ip.getHostAddress().toString();
	}catch (UnknownHostException e){
		logger.error("ERROR :", e);
	}
	Date today = uwManager.selectSysdateTruncated(0);
	Integer countEmailGenerate = uwManager.getUwDao().selectCountNotSentEmailGenerate(FormatDate.toString(today));
	if(countEmailGenerate < 1 || jenis.equals("regenerate")|| jenis.equals("manual")){
		if(user != null) {
			System.out.println("--- GENERATE OUTSOURCE STARTED!! --- " + new Date().toString() + " --- " + computerName + " / (" + ipAddress + ") ---");
			
			String msh_name = "";
			if(jenis.equals("scheduler") || jenis.equals("hit")){
				msh_name = "SCHEDULER GENERATE OUTSOURCE (E-LIONS)";
			}else if(jenis.equals("manual")){
				msh_name = "GENERATE OUTSOURCE (E-LIONS)";
			}else if(jenis.equals("regenerate")){
				msh_name = "RE-GENERATE OUTSOURCE (E-LIONS)";
			}			
			
			//String lsbs_id = "";
			boolean oke = true;	
			String errorValidasi = "";

			String listSukses = "";			
			String listExcludeMsg = "";
			String listError = "";
			String listEmail = "";
			
			long waktuMulai = System.nanoTime();
			String waktuMulai2 = new Date().toString();
			String pattern2 = "dd-MM-yyyy-hh-mm-ss";
			SimpleDateFormat sdf2 = new SimpleDateFormat(pattern2);
			String tanggalNow2 = sdf2.format(new Date());
			String outputDir = props.getProperty("pdf.dir.export.temp");				
			File fileList = new File(outputDir + "\\" + "list_spaj_scheduler_go_" + tanggalNow2 + ".txt");
			int jumlahTerproses = 0;						
			
			//--START-- IGA RDS PHASE 3 : select lcaid include dan produk exclude 
			HashMap mapLcaid = uwManager.getUwDao().selectMstConfig(16, "RDS", "RDS_LCA");
			String[] list_lca = mapLcaid.get("NAME")!=null?mapLcaid.get("NAME").toString().split(","):null;

			HashMap mapProdExc = uwManager.getUwDao().selectMstConfig(16, "RDS","PROD_EXCLUDE");
			String[] BancassExc = mapProdExc.get("NAME")!=null?mapProdExc.get("NAME").toString().split(","):null;
			String[] DMTMExc = mapProdExc.get("NAME2")!=null?mapProdExc.get("NAME2").toString().split(","):null;
			String[] AgencyExc = mapProdExc.get("NAME3")!=null?mapProdExc.get("NAME3").toString().split(","):null;
			
			HashMap mapProPrimeLink = uwManager.getUwDao().selectMstConfig(6, "suratAutosales","PROD_EXCLUDE_SP_AUTOSALES");
			String[] AutosalesExc = mapProPrimeLink.get("NAME")!=null?mapProPrimeLink.get("NAME").toString().split(","):null;
			String[] nonaktif = mapProPrimeLink.get("NAME3")!=null?mapProPrimeLink.get("NAME3").toString().split(","):null;
			
			
			//--END-- IGA RDS PHASE 3 : select lcaid include dan produk exclude 
			
			String textList = "";		
			List dataPrint = new ArrayList();				
			if(jenis.equals("scheduler") || jenis.equals("hit")){
				dataPrint = uwManager.getUwDao().selectPendingPrintPolis();					
			}else if(jenis.equals("manual") || jenis.equals("regenerate")){
				spaj = request.getParameter("spaj");
				dataPrint = uwManager.getUwDao().selectPendingPrintPolisSpaj(spaj);
			}
			
		if(dataPrint.size() > 0){
			//Export list spaj ke file TXT
			textList = "Total SPAJ = " + dataPrint.size() + "\r\n" + "\r\n";		
			if(jenis.equals("hit") || jenis.equals("scheduler")){		
				for(int i=0; i<dataPrint.size();i++){
					spaj = (String) ((Map) dataPrint.get(i)).get("SPAJ");
					textList = textList + spaj + "\r\n";
				}
				try{
					PrintWriter outputStream = new PrintWriter(fileList);
					outputStream.print(textList);
					outputStream.close();
				}catch(Exception e){
					logger.error("ERROR :", e);					
				}
			}		
			
			for(int i=0; i<dataPrint.size();i++){
				oke = true;
				spaj = (String) ((Map) dataPrint.get(i)).get("SPAJ");	
				String noPolis = (String) ((Map) dataPrint.get(i)).get("MSPO_POLICY_NO");		
				List detBisnis = elionsManager.selectDetailBisnis(spaj);
				String lsbs = (String) ((Map) detBisnis.get(0)).get("BISNIS");
				String lsdbs = (String) ((Map) detBisnis.get(0)).get("DETBISNIS");
				String duaDigit = StringUtils.substring(spaj, 0, 2);							
				boolean isDmtm = duaDigit.equals("40");
				int jnBank = bacManager.selectJnBankDetBisnis(Integer.parseInt(lsbs), Integer.parseInt(lsdbs));
				
				//--START-- IGA RDS PHASE 3 : lca include
				boolean lcaInclude = false;
				for(String s: list_lca){
					if(s.equals(duaDigit))
						lcaInclude=true;
				}//--END-- IGA RDS PHASE 3 : lca include
				
				try{
					
					//--START-- IGA RDS PHASE 3 : product exclude
					String prod = lsbs+"-"+lsdbs;
//					String dataExclude = lrds+"-"+lrds_num;
					boolean Bancassexclude = false;
					for(String s: BancassExc){
						if(s.equals(prod))
							Bancassexclude=true;
					}
					boolean DMTMexclude = false;
					for(String s: DMTMExc){
						if(s.equals(prod))
							DMTMexclude=true;
					}
					boolean Agencyexclude = false;
					for(String s: AgencyExc){
						if(s.equals(prod))
							Agencyexclude=true;
					}
					
					// cek mspo_date_print
					if(!(((Map) dataPrint.get(i)).get("DATE_PRINT") == null)) {
						 if (!Agencyexclude && !DMTMexclude && !Bancassexclude){
							errorValidasi = "Sudah pernah generate.";
							jumlahError++;
							listError = listError + spaj + "     " + ((String) ((Map) detBisnis.get(0)).get("LSDBS_NAME")) + "     " + errorValidasi + "\r\n";
							continue;	
						}
					}	
					
					if (Bancassexclude || DMTMexclude || Agencyexclude || !lcaInclude){
						jumlahExclude++;
						//listError + spaj + "     " + errorValidasi + "\n";
						listExcludeMsg = listExcludeMsg + spaj + "     " + ((String) ((Map) detBisnis.get(0)).get("LSDBS_NAME")) + "\r\n";
						continue;	
					}
					//--END-- IGA RDS PHASE 3 : product exclude		
					
					//Validasi Email NULL
					HashMap m = Common.serializableMap(uwManager.selectInformasiEmailSoftcopy(spaj));
//					if(!isKirimSoftcopy){
						String email = (String) m.get("MSPE_EMAIL");
						if(email == null){
							email = "-";
						}
//					}	
					
					//Validasi Pembatalan
					Integer cekProsesBatal = bacManager.selectCountSpajMstRefund(spaj);
					if(cekProsesBatal>0){
						errorValidasi = "Polis tidak dapat dicetak karena dalam proses Pembatalan.";
						jumlahError++;
						listError = listError + spaj + "     " + ((String) ((Map) detBisnis.get(0)).get("LSDBS_NAME")) + "     " + errorValidasi + "\r\n";
						continue;								
					}
					if(!jenis.equals("regenerate")){
						if(elionsManager.validationPositionSPAJ(spaj)!=6) {
							errorValidasi = "Harap cek posisi SPAJ.";
							jumlahError++;
							listError = listError + spaj + "     " + ((String) ((Map) detBisnis.get(0)).get("LSDBS_NAME")) + "     " + errorValidasi + "\r\n";
							continue;										
						}	
					}
												
					
					if(products.unitLink(lsbs)) {
						List<Date> asdf = uwManager.selectSudahProsesNab(spaj);
						for(Date d : asdf){
							if(d == null) {
								jumlahExclude++;
								listExcludeMsg = listExcludeMsg + spaj + "     " + ((String) ((Map) detBisnis.get(0)).get("LSDBS_NAME")) + "     " + "Belum Proses NAB" + "\r\n";									
								oke = false;
								continue;
							}else{
								oke = true;
							}
						}
						if(asdf.size()==0){
							jumlahExclude++;
							listExcludeMsg = listExcludeMsg + spaj + "     " + ((String) ((Map) detBisnis.get(0)).get("LSDBS_NAME")) + "     " + "Belum Proses NAB" + "\r\n";								
							oke = false;
							continue;
						}						
					}
					
					boolean Autosalesexclude = false;
					for(String s: AutosalesExc){
						if(s.equals(prod))
							Autosalesexclude=true;
					}
					boolean nonaktifValidasi = false;
					if(nonaktif != null){
						for(String s: nonaktif){
							if(s.equals("1"))
								nonaktifValidasi=true;
						}
					}
					List listSpajTemp = bacManager.selectReferensiTempSpaj(spaj);
					
					if (listSpajTemp.size() > 0 && nonaktifValidasi && Autosalesexclude){
						String cabang = elionsManager.selectCabangFromSpaj(spaj);
						String pathAutosales = props.getProperty("pdf.dir.export")+"\\"+ cabang + "\\" + spaj;
						File fileDirTemp = new File(pathAutosales);
						File[] files = fileDirTemp.listFiles();
					    File suratAutosales = null;
					    for (int z = 0; z < files.length; z++) {
					       if (files[z].toString().toUpperCase().contains("FT_SP_AUTOSALES")) {
					    	   suratAutosales = files[z];
					    	  
					       }
					    }
					    if (suratAutosales==null) {
			    		    jumlahExclude++;
							listExcludeMsg = listExcludeMsg + spaj + "     " + ((String) ((Map) detBisnis.get(0)).get("LSDBS_NAME")) + "     " + "Belum Upload File Surat Pernyataan Autosales" + "\r\n";								
							oke = false;
							continue;	    		   
			    	   }
					}
					
						
					//if ok -> generate Simascard & Generate Files
					if(oke){
						
						//GENERATE FILES;
						int mspoProvider=uwManager.selectGetMspoProvider(spaj);
						String covid = bacManager.selectCovidFlag(spaj);
						resultGenerateOutsource = ppap.generateFileRDS(request, response, spaj, mspoProvider, elionsManager, uwManager, bacManager, props, products, user, jenis,covid);
					
						if((resultGenerateOutsource.isEmpty())){
							//insert eka.mst_eksternal									
							String cabang = elionsManager.selectCabangFromSpaj(spaj);
						    String pathUploadZip = props.getProperty("pdf.dir.ftp")+"\\"+cabang+"\\"+spaj+"\\"+spaj+".zip";
					        Datausulan dataUsulan = elionsManager.selectDataUsulanutama(spaj);
							String lcaId = dataUsulan.getLca_id();
							String lusId;
							if(jenis.equals("regenerate")){
								lusId = user.getLus_id();
							}else{
								lusId = "01";
							}
							//handling duplicate
							bacManager.updateFlagKonfirmasiMstEksternal(spaj);
							//insert new data
							uwManager.getUwDao().insertMst_eksternal_print(spaj, pathUploadZip, lcaId, lusId);
							if (covid != null && covid.trim().equals("1")){
								bacManager.updateFlagKonfirmasiMstEksternal(spaj);
							}
//							elionsManager.updatePolicyAndInsertPositionSpaj(spaj, "mspo_date_print", user.getLus_id(), 6, 1, msh_name, true, user);	
							listSukses = listSukses + spaj + "     " 
										+ ((String) ((Map) detBisnis.get(0)).get("LSDBS_NAME")) 
										+ " / " + email
										+ " / " + noPolis
										+ " / " + jnBank
										+ "\r\n";
							jumlahSuksesGenerate++;
						}
						else{
							//insert eka.mst_eksternal									
							String cabang = elionsManager.selectCabangFromSpaj(spaj);
						    String pathUploadZip = props.getProperty("pdf.dir.ftp")+"\\"+cabang+"\\"+spaj+"\\"+spaj+".zip";
					        Datausulan dataUsulan = elionsManager.selectDataUsulanutama(spaj);
							String lcaId = dataUsulan.getLca_id();
							String lusId;
							if(jenis.equals("regenerate")){
								lusId = user.getLus_id();
							}else{
								lusId = "01";
							}

							//handling duplicate
							bacManager.updateFlagKonfirmasiMstEksternal(spaj);
							//insert new data
							uwManager.getUwDao().insertMst_eksternal_print(spaj, pathUploadZip, lcaId, lusId);
							if (covid != null && covid.trim().equals("1")){
								bacManager.updateFlagKonfirmasiMstEksternal(spaj);
							}
//							elionsManager.updatePolicyAndInsertPositionSpaj(spaj, "mspo_date_print", user.getLus_id(), 6, 1, msh_name, true, user);
							listSukses = listSukses + spaj + "     " 
										+ ((String) ((Map) detBisnis.get(0)).get("LSDBS_NAME")) 
										+ " / " + resultGenerateOutsource
										+ " / " + noPolis
										+ " / " + jnBank
										+ "\r\n";
							jumlahSuksesGenerate++;						
						}
					}
				}catch(Exception e){
					logger.error("ERROR :", e);
					jumlahError++;
					listError = listError + spaj + "     " 
							+ ((String) ((Map) detBisnis.get(0)).get("LSDBS_NAME")) 
							+ " / " 
							+ e.toString()
							+ "\r\n";
					System.out.println(listError);
					CekPelengkap cp = new CekPelengkap();
					String pathTemp = "";
					pathTemp = props.getProperty("pdf.dir.export.temp") + "\\" + spaj;
					File dirTemp = new File(pathTemp);
					if(dirTemp.exists()){
						cp.deleteDirectoryWithFiles(dirTemp);								
					}
					if(jenis.equals("manual") || jenis.equals("regenerate")){
						throw e;
					}else{
						continue;
					}
				}
			}
			
		if(jenis.equals("scheduler") || jenis.equals("hit")){
			//insert history ke database
			bacManager.insertMstSchedulerHist(InetAddress.getLocalHost().getHostName(), msh_name, new Date(), new Date(), desc, 
					"Computer Name            = " + computerName + "\n"
							+ "IP Address               = " + ipAddress + "\n\n"
							+ "Jumlah List SPAJ         = " + dataPrint.size() + "\n"
							+ "Jumlah Sukses Generate   = " + jumlahSuksesGenerate + "\n" //+ listSukses + "\n" 
							+ "Jumlah Error             = " + jumlahError + "\n" //+ listError + "\n" 
							+ "Jumlah Exclude           = " + jumlahExclude); //+ "\n" + listExcludeMsg + "\n"
							//+ "Jumlah Softcopy (e-mail) = " + jumlahEmail); //+ listEmail);
			
			//Create TXT file untuk email attachment
			String pattern = "dd-MM-yyyy";
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			String tanggalNow = sdf.format(new Date());											
			File fileTxt = new File(outputDir + "\\" + "scheduler_go_attachment_" + tanggalNow + ".txt");
			jumlahTerproses = jumlahSuksesGenerate + jumlahEmail + jumlahError + jumlahExclude;
			String textMsg = "Jumlah List SPAJ = " + jumlahTerproses + "\r\n" + "\r\n"
					+ "Jumlah Sukses Generate = " + jumlahSuksesGenerate + "\r\n" + listSukses +  "\r\n"
					+ "Jumlah Error = " + jumlahError + "\r\n" + listError + "\r\n" 
					+ "Jumlah Exclude = " + jumlahExclude + "\r\n" + listExcludeMsg; //+ "\r\n"
					//+ "Jumlah Softcopy (e-mail) = " + jumlahEmail + "\r\n" + listEmail;
			try{
				PrintWriter outputStream = new PrintWriter(fileTxt);
				outputStream.print(textMsg);
				outputStream.close();
			}catch(Exception e){
				//e.printStackTrace();
				logger.error("ERROR :", e);					
				EmailPool.send("SMiLe E-Lions", 1, 1, 0, 0, 
						null, 0, 0, new Date(), null, false, props.getProperty("admin.ajsjava"), new String[]{"iga.ukiarwan@sinarmasmsiglife.co.id"}, null, null,  
						"Error GENERATE OUTSOURCE SCHEDULER (E-LIONS)", 
						e+"", null, spaj);
			}						
			
			//kirim email Result Scheduler Generate Outsource
			long waktuSelesai = System.nanoTime();
			String waktuSelesai2 = new Date().toString();
			long lamaProses = (waktuSelesai - waktuMulai) / 1000000;  //divide by 1000000 to get milliseconds.						
			List<File> attachments = new ArrayList<File>();
			if(fileTxt.exists()){
				attachments.add(fileTxt);
			}
			// iga rds phase 3: hilangkan harcode email
			HashMap mapEmail = uwManager.getUwDao().selectMstConfig(16, "RDS", "SCHEDULER_GENERATE_RDS_EMAIL");
			String[] to = mapEmail.get("NAME")!=null?mapEmail.get("NAME").toString().split(";"):null;
			String[] cc = mapEmail.get("NAME2")!=null?mapEmail.get("NAME2").toString().split(";"):null;
			String[] bcc = mapEmail.get("NAME3")!=null?mapEmail.get("NAME3").toString().split(";"):null;
			
			EmailPool.send("SMiLe E-Lions", 1, 1, 0, 0,
					null, 0, Integer.parseInt(user.getLus_id()), new Date(), null, true,
					props.getProperty("admin.ajsjava"),
					to,
					cc,
					bcc,							
					//Judul Email
					"SCHEDULER GENERATE OUTSOURCE (E-LIONS) " + new Date(),
					//Body Email,	
					"Dear All," + "\n\n"
					+ "Berikut adalah hasil dari Scheduler Generate Outsource : " + "\n\n"
//														+ "Jumlah List SPAJ         = " + dataPrint.size() + "\n"
							+ "Jumlah List SPAJ         = " + jumlahTerproses + "\n"								
							+ "Jumlah Sukses Generate   = " + jumlahSuksesGenerate + "\n" //+ listSukses + "\n" 
							+ "Jumlah Error             = " + jumlahError + "\n" //+ listError + "\n" 
							+ "Jumlah Exclude           = " + jumlahExclude + "\n\n" //+ listExcludeMsg + "\n"
							//+ "Jumlah Softcopy (e-mail) = " + jumlahEmail + "\n\n"
							+ "[Cek attachment untuk detail]" + "\n\n"
							+ "Waktu Mulai              = " + waktuMulai2 + "\n"
							+ "Waktu Selesai            = " + waktuSelesai2 + "\n"
							+ "Total Waktu Proses       = " + lamaProses + " ms.",
					attachments,
					null);
				
				fileTxt.delete();
			}
		}else{
			// Data Kosong
			listResultAll.add("Tidak ada data yang diproses.");
		}	
			System.out.println("");
			System.out.println("----------------Scheduler Generate Outsource-------------------------");
			System.out.println("Jumlah SPAJ              = " + dataPrint.size());						
			System.out.println("Jumlah Sukses Generate   = " + jumlahSuksesGenerate);						
			System.out.println("Jumlah Error             = " + jumlahError);
			System.out.println("Jumlah Exclude           = " + jumlahExclude);
			System.out.println("Jumlah Softcopy (e-mail) = " + jumlahEmail);					
			System.out.println("---------------------------------------------------------------------");					

		
			}else{
				// login dulu..
				listResultAll.add("User belum login.");
			}
	}
	else{
		listResultAll.add("Sudah selesai proses scheduler generate outsource");
	}
	
		return listResultAll;
	}
	
	
	public void schedulerPegaPrint(HttpServletRequest request, HttpServletResponse response, UwManager uwManager, BacManager bacManager, ElionsManager elionsManager, Properties props, Products products) {
		Date bDate = new Date();
		String desc = "OK";
		String err = "";
		String spaj = "";	
		
		try {			
			Date today = uwManager.selectSysdateTruncated(0);
			List dataPrint = new ArrayList();
			dataPrint = uwManager.getUwDao().selectPegaPrint(FormatDate.toString(today));
			if(dataPrint.size() > 0){
				for(int x=0; x<dataPrint.size();x++){
					spaj = (String) ((Map) dataPrint.get(x)).get("SPAJ");	
					List detBisnis = elionsManager.selectDetailBisnis(spaj);
					String lsbs = (String) ((Map) detBisnis.get(0)).get("BISNIS");
					String lsdbs = (String) ((Map) detBisnis.get(0)).get("DETBISNIS");
					String duaDigit = StringUtils.substring(spaj, 0, 2);							
					boolean isDmtm = duaDigit.equals("40");
					String namaProduk = (String) ((Map) detBisnis.get(0)).get("LSDBS_NAME");
					String hasilEmailSoftcopy = "";
					
						HashMap m = Common.serializableMap(uwManager.selectInformasiEmailSoftcopy(spaj));
						String email = (String) m.get("MSPE_EMAIL");

						if(email != null){
							List<File> attachments = new ArrayList<File>();
							List<String> error = new ArrayList<String>();
							
							//Uploader
							CommandUploadBac uploader = new CommandUploadBac();
							ServletRequestDataBinder binder = new ServletRequestDataBinder(uploader);
							//binder.bind(request);
							uploader.setErrorMessages(new ArrayList<String>());				
							
							//Cari File
							String msp_id = uwManager.getUwDao().selectMspIdFromSpaj(spaj);
							String businessId = uwManager.selectBusinessId(spaj);
							String product_sub_code = uwManager.selectLsdbsNumber(spaj);

							String path = "";
							String pathTemp = "";
							String cabang = elionsManager.selectCabangFromSpaj(spaj);
							path = props.getProperty("pdf.dir.ftp") + "\\" + cabang + "\\" + spaj +"\\";					
							Pemegang pmg = elionsManager.selectpp(spaj);	
							
							List<DropDown> fileList = new ArrayList<DropDown>();
							fileList.add(new DropDown(path+"2.surat_polis.pdf", "Surat Ucapan Terima Kasih", "ALL"));
							fileList.add(new DropDown(path+"3.polis_utama.pdf", "Cover Polis", "ALL"));
							fileList.add(new DropDown(path+"4.manfaat.pdf", "Manfaat Polis", "ALL"));
							fileList.add(new DropDown(path+"5.alokasi_dana.pdf", "Alokasi Dana", "ALL"));
							fileList.add(new DropDown(path+"SSU_SSK.pdf", "Syarat-syarat Polis", "ALL"));
							File fileDir = new File(path);
							File[] files = fileDir.listFiles(); 
							
							File fileSpaj = null;
							for (int i = 0; i < files.length; i++) {
							       if ((files[i].toString().toUpperCase().contains("ESPAJONLINEGADGET")) || (files[i].toString().toUpperCase().contains("ESPAJDMTM"))) {
							    	   fileSpaj = files[i];
							       }
							    }
						    if (fileSpaj == null){
							    int jumlahfileSpaj = 0;				    	
							    for (int i = 0; i < files.length; i++) {
								       if ((files[i].toString().toUpperCase().contains("SPAJ"))) {
								    	   fileSpaj = files[i];
								    	   jumlahfileSpaj++;
								       }
							    }
							    if ((fileSpaj != null) && (jumlahfileSpaj > 1)){
							    	   for (int j = 0; j < files.length; j++){
							    		   if (files[j].toString().toUpperCase().contains("SPAJ ")){
								    		   if (fileSpaj.lastModified() < files[j].lastModified()){
								    			   fileSpaj = files[j];	   
								    		   }   
							    		   }
							    	   }
							    }			    
						    }	    
						    if ((fileSpaj!=null) && (!isDmtm)) if(fileSpaj.exists()){
						    	fileList.add(new DropDown(path+fileSpaj.getName(), "SPAJ", "NONE"));
						    }
						    
							File fileProposal = null;
							    for (int i = 0; i < files.length; i++) {
							       if ((files[i].toString().toUpperCase().contains("PROPOSAL"))) {
							    	   fileProposal = files[i];
							    	   if ((fileProposal!=null) && (!isDmtm)) if(fileProposal.exists()){
							    		   fileList.add(new DropDown(path+fileProposal.getName(), "PROPOSAL", "NONE"));	    		   
							    	   }
							       }
							    }
							
							if (products.syariah(lsbs, lsdbs)){
								PdfUtils.addTableOfContentsSyariah(fileList, props, path);
							}
							else{
								PdfUtils.addTableOfContents(fileList, props, path);
							}
							
							String userPassword = elionsManager.selectTanggalLahirPemegangPolis(spaj);
							File softcopy = PdfUtils.combinePdfWithOutline(fileList, path, "softcopy.pdf", props, userPassword);
							if (products.syariah(lsbs, lsdbs)){
								softcopy = PdfUtils.addBarcodeAndLogoSyariah(fileList, softcopy, props, userPassword);
							}else{
								softcopy = PdfUtils.addBarcodeAndLogo(fileList, softcopy, props, userPassword);
							}
							
							if(softcopy.exists()){
								attachments.add(softcopy);
//								attachments.add(new File(path + "\\" + softcopy));
							}
							
							Map info = uwManager.selectInformasiEmailSoftcopy(spaj);
							
							//penambahan telemarketing@banksinarmas.com di TO email (dmtm) - (101623)
							if(isDmtm){
								if ((String) info.get("MSPE_EMAIL")==null){
									uploader.setEmailto("telemarketing@banksinarmas.com");
								}else{
									uploader.setEmailto("telemarketing@banksinarmas.com;"+(String) info.get("MSPE_EMAIL"));
								}
							}else{
								uploader.setEmailto((String) info.get("MSPE_EMAIL"));
							}
							
							uploader.setEmailsubject("Softcopy Polis " + namaProduk + " atas nama " + info.get("GELAR") + " " + info.get("MCL_FIRST") + " no. " + info.get("MSPO_POLICY_NO"));
							StringBuffer pesan = new StringBuffer();

							pesan.append("Kepada Yth.\n");
							pesan.append(info.get("GELAR") + " " + info.get("MCL_FIRST")+"\n");
							pesan.append("di tempat\n\n");
							pesan.append("Dengan Hormat,\n");
							pesan.append("Bersama ini kami kirimkan softcopy Polis " + namaProduk + " untuk Nomor Polis. " + info.get("MSPO_POLICY_NO")+"\n");
							pesan.append("dengan password : tanggal lahir format dd-mm-yyyy \n");
							pesan.append("Informasi lebih lanjut dapat disampaikan ke : " + "cs@sinarmasmsiglife.co.id");
							uploader.setEmailmessage(pesan.toString().replaceAll("\n", "</br>"));
							
							Map infoAgen = elionsManager.selectEmailAgen(spaj);
							String emailAgen = (String) infoAgen.get("MSPE_EMAIL");
							if(emailAgen!=null) {
								if(!emailAgen.trim().equals("")) {
									if(emailAgen.toLowerCase().matches("^.+@[^\\.].*\\.[a-z]{2,}$")) {
										uploader.setEmailcc(emailAgen);
									}
								}
							}
							
							//kalo agen gak punya email, dikirim ke branch admin nya
							if(uploader.getEmailcc()==null) {
								String emailCabang = uwManager.selectEmailCabangFromSpaj(spaj);
								if(emailCabang!=null) {
									if(!emailCabang.trim().equals("")) {
										if(emailCabang.toLowerCase().matches("^.+@[^\\.].*\\.[a-z]{2,}$")) {
											uploader.setEmailcc(emailCabang);
										}
									}
								}
							}
							
							//Req Sari Sutini via Helpdesk 27539 - khusus Mall, email CC ditambahkan ke inge, ningrum, apriyani, dan sutini.
							if(elionsManager.selectLcaIdBySpaj(spaj).equals("58")){
								uploader.setEmailcc(uploader.getEmailcc()+";apriyani@sinarmasmsiglife.co.id;sutini@sinarmasmsiglife.co.id");
							}
							//Tambahan khusus DMTM (Req Sari Sutini)
							else if(elionsManager.selectLcaIdBySpaj(spaj).equals("40")){
								if(lsbs.equals("163")){
									uploader.setEmailcc(uploader.getEmailcc()+";Maria_P@sinarmasmsiglife.co.id;Lylianty@sinarmasmsiglife.co.id;paul.a.tarigan@banksinarmas.com;tri.w.utami@banksinarmas.com");
								}else{
									uploader.setEmailcc(uploader.getEmailcc()+";Maria_P@sinarmasmsiglife.co.id;Lylianty@sinarmasmsiglife.co.id");
								}
							}
							//Tambahan khusus SIAP2U Agency (37 M1 05)
							else if( "37M105".equals(elionsManager.selectCabangFromSpaj_lar(spaj)) ){
								if((lsbs.equals("190") && lsdbs.equals("009")) || (lsbs.equals("200") && lsdbs.equals("007"))){
									uploader.setEmailcc(uploader.getEmailcc()+";lyna@smileultimate.co.id");
									uploader.setEmailto(uploader.getEmailto()+";polis.siap2u@gmail.com");
								}
							}
							
							//ERBE (Helpdesk 137909)
							if(elionsManager.selectLcaIdBySpaj(spaj).equals("73")){
								uploader.setEmailcc("spaj.msig@erbecorp.com;efesty@erbecorp.com");
							}
							
								

									HashMap mapEmail = uwManager.getUwDao().selectMstConfig(16, "RDS", "SOFTCOPY_EMAIL");
									String[] bcc = mapEmail.get("NAME")!=null?mapEmail.get("NAME").toString().split(";"):null;
									EmailPool.send("SMiLe E-Lions", 1, 1, 0, 0, 
											null, 0, 0, new Date(), null, true,
											"policy_service@sinarmasmsiglife.co.id", 
											uploader.getEmailto().replaceAll(" ", "").split(";"), 
											(uploader.getEmailcc().trim().equals("")? null : uploader.getEmailcc().replaceAll(" ", "").split(";")), 
											bcc,
											uploader.getEmailsubject(), uploader.getEmailmessage(), attachments, spaj);
											hasilEmailSoftcopy = "Softcopy Terkirim ('" + pmg.getMspe_email() + "')";			
						}else{
							hasilEmailSoftcopy = "Alamat email nasabah tidak ditemukan";
						}
				}
			}
							
		} catch(Exception e) {
			logger.error("ERROR :", e);
			desc = "ERROR";
			err = e.getLocalizedMessage();
			HashMap mapEmail = uwManager.getUwDao().selectMstConfig(16, "RDS", "SOFTCOPY_EMAIL");
			String[] bcc = mapEmail.get("NAME")!=null?mapEmail.get("NAME").toString().split(";"):null;
			if(Common.isEmpty(err)) {
				err = "- Exception : \n";
				err += ExceptionUtils.getStackTrace(e);
			}
			
			EmailPool.send("Pega", 1, 0, 0, 0, null, 0, 0, new Date(), null, true, 
					props.getProperty("admin.ajsjava"), 
					bcc, 
					null, 
					null, 
					"ERROR SCHEDULER SOFTCOPY PEGA", 
					err, 
					null, 
					null);
		}
		
		
	}
	
}
