/**
 * Scheduler untuk Generate Outsource
 * @author Mark Valentino
 * @since 13-02-2019
 */

package com.ekalife.utils.scheduler;

import java.io.File;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;

import com.ekalife.elions.dao.BasDao;
import com.ekalife.elions.dao.UwDao;
import com.ekalife.elions.model.Datausulan;
import com.ekalife.elions.model.Simcard;
import com.ekalife.elions.model.User;
import com.ekalife.elions.service.BacManager;
import com.ekalife.elions.service.ElionsManager;
import com.ekalife.elions.service.UwManager;
import com.ekalife.elions.web.uw.AutoGenerateController;
import com.ekalife.elions.web.uw.PrintPolisAllPelengkap;
import com.ekalife.elions.web.uw.PrintPolisPrintingController;
import com.ekalife.elions.web.uw.PrintPolisMultiController;
import com.ekalife.utils.CekPelengkap;
import com.ekalife.utils.Common;
import com.ekalife.utils.EmailPool;
import com.ekalife.utils.FormatString;
import com.ekalife.utils.Print;
import com.ekalife.utils.Products;
import com.ekalife.utils.StringUtil;
import com.ekalife.utils.parent.ParentScheduler;
import com.google.gwt.http.client.RequestBuilder.Method;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class GenerateOutsourceScheduler extends ParentScheduler {
	
	//private PrintPolisMultiController ppmc;	
	//protected PrintPolisPrintingController ppc;
	protected PrintPolisAllPelengkap ppap;
	//private Properties props;
	//private ElionsManager elionsManager;
	//private UwManager uwManager;
	private BasDao basDao;
	//private BacManager bacManager;
	private Products products;
	private User user;
	private AutoGenerateController agc;

	private Log logger = LogFactory.getLog( getClass() );	
	
	public PrintPolisAllPelengkap getPpap() {
		return ppap;
	}

	public void setPpap(PrintPolisAllPelengkap ppap) {
		this.ppap = ppap;
	}

	public Properties getProps() {
		return props;
	}

	public void setProps(Properties props) {
		this.props = props;
	}

	public ElionsManager getElionsManager() {
		return elionsManager;
	}

	public void setElionsManager(ElionsManager elionsManager) {
		this.elionsManager = elionsManager;
	}

	public UwManager getUwManager() {
		return uwManager;
	}

	public void setUwManager(UwManager uwManager) {
		this.uwManager = uwManager;
	}

	public BacManager getBacManager() {
		return bacManager;
	}

	public void setBacManager(BacManager bacManager) {
		this.bacManager = bacManager;
	}	

	public Products getProducts() {
		return products;
	}

	public void setProducts(Products products) {
		this.products = products;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public AutoGenerateController getAgc() {
		return agc;
	}

	public void setAgc(AutoGenerateController agc) {
		this.agc = agc;
	}	

	//main method
	public void main() throws Exception{
		
		//HOST INFORMATION
		String computerName = InetAddress.getLocalHost().getHostName().toString().trim().toUpperCase();	
		InetAddress ip;
		String ipAddress = "";
		try {
			ip = InetAddress.getLocalHost();
			ipAddress = ip.getHostAddress().toString();
		}catch (UnknownHostException e){
			logger.error("ERROR :", e);
		}
		
		if((jdbcName.equals("eka8i") || jdbcName.equals("ajsmuat")) && 
				(
						computerName.toString().trim().toUpperCase().equals("IGALAPTOP")
						//InetAddress.getLocalHost().getHostName().toString().trim().toUpperCase().equals("AJSJAVAI64"))
						||computerName.toString().trim().toUpperCase().equals("AJSJAVAI643"))
//						||computerName.toString().trim().toUpperCase().equals("DEVNODE2"))
						//ipAddress.equals("128.21.30.47") || ipAddress.equals("192.168.9.99"))
		  ){
//			agc.generateOutsource(null, null, uwManager, bacManager, elionsManager, props, products, "scheduler");
		}
	}

}
