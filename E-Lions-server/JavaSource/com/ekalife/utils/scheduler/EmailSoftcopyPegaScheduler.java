package com.ekalife.utils.scheduler;

import java.net.InetAddress;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ekalife.elions.dao.BasDao;
import com.ekalife.elions.model.User;
import com.ekalife.elions.service.BacManager;
import com.ekalife.elions.service.ElionsManager;
import com.ekalife.elions.service.UwManager;
import com.ekalife.elions.web.uw.AutoGenerateController;
import com.ekalife.elions.web.uw.PrintPolisAllPelengkap;
import com.ekalife.utils.Common;
import com.ekalife.utils.EmailPool;
import com.ekalife.utils.Products;
import com.ekalife.utils.parent.ParentScheduler;

/**
 * @spring.bean
 * 
 * @author Iga
 * @since 
 */

public class EmailSoftcopyPegaScheduler extends ParentScheduler{
	//main method
	
	//private PrintPolisMultiController ppmc;	
	//protected PrintPolisPrintingController ppc;
	protected PrintPolisAllPelengkap ppap;
	//private Properties props;
	//private ElionsManager elionsManager;
	//private UwManager uwManager;
	private BasDao basDao;
	//private BacManager bacManager;
	private Products products;
	private User user;
	private AutoGenerateController agc;

	private Log logger = LogFactory.getLog( getClass() );	
	
	public PrintPolisAllPelengkap getPpap() {
		return ppap;
	}

	public void setPpap(PrintPolisAllPelengkap ppap) {
		this.ppap = ppap;
	}

	public Properties getProps() {
		return props;
	}

	public void setProps(Properties props) {
		this.props = props;
	}

	public ElionsManager getElionsManager() {
		return elionsManager;
	}

	public void setElionsManager(ElionsManager elionsManager) {
		this.elionsManager = elionsManager;
	}

	public UwManager getUwManager() {
		return uwManager;
	}

	public void setUwManager(UwManager uwManager) {
		this.uwManager = uwManager;
	}

	public BacManager getBacManager() {
		return bacManager;
	}

	public void setBacManager(BacManager bacManager) {
		this.bacManager = bacManager;
	}	

	public Products getProducts() {
		return products;
	}

	public void setProducts(Products products) {
		this.products = products;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public AutoGenerateController getAgc() {
		return agc;
	}

	public void setAgc(AutoGenerateController agc) {
		this.agc = agc;
	}
	public void main() throws Exception{
			
		String computerName = InetAddress.getLocalHost().getHostName().toString().trim().toUpperCase();	
		if((jdbcName.equals("eka8i") || jdbcName.equals("ajsmuat")) && 
				(
						computerName.toString().trim().toUpperCase().equals("IGALAPTOP")
						//InetAddress.getLocalHost().getHostName().toString().trim().toUpperCase().equals("AJSJAVAI64"))
						||computerName.toString().trim().toUpperCase().equals("AJSJAVAI643"))
						//||computerName.toString().trim().toUpperCase().equals("DEVNODE2"))
						//ipAddress.equals("128.21.30.47") || ipAddress.equals("192.168.9.99"))
		  ){			
//				agc.schedulerPegaPrint(null, null, uwManager, bacManager, elionsManager, props, products);
		}
	}
}
